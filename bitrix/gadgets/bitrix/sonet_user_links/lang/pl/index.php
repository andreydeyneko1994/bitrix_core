<?
$MESS["GD_SONET_USER_LINKS_ABSENT"] = "poza biurem";
$MESS["GD_SONET_USER_LINKS_BIRTHDAY"] = "Urodziny";
$MESS["GD_SONET_USER_LINKS_CODES"] = "Kody odzyskiwania";
$MESS["GD_SONET_USER_LINKS_EDIT_EXTMAIL"] = "Opcje integracji e-maila";
$MESS["GD_SONET_USER_LINKS_EDIT_FEATURES"] = "Edytuj ustawienia";
$MESS["GD_SONET_USER_LINKS_EDIT_PROFILE"] = "Edytuj Profil";
$MESS["GD_SONET_USER_LINKS_EDIT_REQUESTS"] = "Zaproszenia i podania";
$MESS["GD_SONET_USER_LINKS_EDIT_SETTINGS"] = "Edytuj ustawienia prywatności";
$MESS["GD_SONET_USER_LINKS_FR_ADD"] = "Dodaj do znajomych";
$MESS["GD_SONET_USER_LINKS_FR_DEL"] = "Nieznajomy";
$MESS["GD_SONET_USER_LINKS_HONOURED"] = "Uhonorowani";
$MESS["GD_SONET_USER_LINKS_INV_GROUP"] = "Zaproś do grupy";
$MESS["GD_SONET_USER_LINKS_ONLINE"] = "online";
$MESS["GD_SONET_USER_LINKS_PASSWORDS"] = "Hasła aplikacji";
$MESS["GD_SONET_USER_LINKS_SECURITY"] = "Zabezpieczenia";
$MESS["GD_SONET_USER_LINKS_SEND_MESSAGE"] = "Wyślij wiadomość";
$MESS["GD_SONET_USER_LINKS_SHOW_MESSAGES"] = "Pokaż log wiadomości";
$MESS["GD_SONET_USER_LINKS_SUBSCR"] = "Subskrypcja";
$MESS["GD_SONET_USER_LINKS_SUBSCR1"] = "Mój subskrypcja";
$MESS["GD_SONET_USER_LINKS_SYNCHRONIZE"] = "Synchronizacja";
$MESS["GD_SONET_USER_SONET_ADMIN_ON"] = "Tryb administratora";
$MESS["GD_SONET_USER_VIDEOCALL"] = "Połączenie wideo";
?>