<?
$MESS["GD_CRM_COLUMN_CHANGED_DATE"] = "Zmodyfikowany";
$MESS["GD_CRM_COLUMN_CLOSED_DATE"] = "Zamknięte";
$MESS["GD_CRM_COLUMN_CREATED_DATE"] = "Utworzony";
$MESS["GD_CRM_COLUMN_DATE_START"] = "Rozpocząło się";
$MESS["GD_CRM_ONLY_MY"] = "Tylko Moje";
$MESS["GD_CRM_SORT"] = "Sortowanie";
$MESS["GD_CRM_SORT_ASC"] = "Rosnąco";
$MESS["GD_CRM_SORT_BY"] = "Przez";
$MESS["GD_CRM_SORT_DESC"] = "Malejąco";
$MESS["GD_CRM_TASK_ENTITY_TYPE"] = "Jednostki Na Liście";
$MESS["GD_CRM_TASK_LIST_EVENT_COUNT"] = "Zadań Na Stronę";
$MESS["GD_CRM_TASK_TYPE_COMPANY"] = "Firma";
$MESS["GD_CRM_TASK_TYPE_CONTACT"] = "Kontakt";
$MESS["GD_CRM_TASK_TYPE_DEAL"] = "Deal";
$MESS["GD_CRM_TASK_TYPE_LEAD"] = "Lead";
?>