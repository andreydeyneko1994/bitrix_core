<?
$MESS["GD_SONET_USER_FRIENDS_ALL_FRIENDS"] = "Wszyscy znajomi";
$MESS["GD_SONET_USER_FRIENDS_FR_SEARCH"] = "Szukaj znajomego";
$MESS["GD_SONET_USER_FRIENDS_FR_UNAVAIL"] = "Lista znajomych jest niedostępna.";
$MESS["GD_SONET_USER_FRIENDS_LOG_USERS"] = "Aktualizacje";
$MESS["GD_SONET_USER_FRIENDS_NOT_ALLOWED"] = "Ta funkcja jest wyłączona.";
$MESS["GD_SONET_USER_FRIENDS_NO_FRIENDS"] = "Brak znajomych.";
?>