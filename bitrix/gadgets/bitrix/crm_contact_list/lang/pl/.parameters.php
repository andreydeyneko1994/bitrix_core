<?php
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Utworzony";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["GD_CRM_COLUMN_TYPE"] = "Rodzaj";
$MESS["GD_CRM_CONTACT_LIST_CONTACT_COUNT"] = "Kontaktów Na Stronę";
$MESS["GD_CRM_ONLY_MY"] = "Tylko Moje";
$MESS["GD_CRM_SORT"] = "Sortowanie";
$MESS["GD_CRM_SORT_ASC"] = "Rosnąco";
$MESS["GD_CRM_SORT_BY"] = "Zamówienie";
$MESS["GD_CRM_SORT_DESC"] = "Malejąco";
