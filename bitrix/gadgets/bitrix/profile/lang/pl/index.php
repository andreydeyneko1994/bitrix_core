<?php
$MESS["GD_PROFILE_BLOG"] = "Konwersacje";
$MESS["GD_PROFILE_BLOG_NEW"] = "Post na blogu";
$MESS["GD_PROFILE_CAL"] = "Kalendarz";
$MESS["GD_PROFILE_CH_PROFILE"] = "Edytuj Profil";
$MESS["GD_PROFILE_FORUM"] = "Forum";
$MESS["GD_PROFILE_GENERAL"] = "Główne";
$MESS["GD_PROFILE_GROUPS"] = "Grupy";
$MESS["GD_PROFILE_GROUP_NEW"] = "Utwórz Grupę";
$MESS["GD_PROFILE_LIB"] = "Pliki";
$MESS["GD_PROFILE_LOG"] = "Aktualności";
$MESS["GD_PROFILE_MSG"] = "Moje wiadomości";
$MESS["GD_PROFILE_PHOTO"] = "Galeria";
$MESS["GD_PROFILE_PHOTO_NEW"] = "Załadowane zdjęcia";
$MESS["GD_PROFILE_SUBSCR"] = "Mój subskrypcja";
$MESS["GD_PROFILE_TASKS"] = "Zadania";
$MESS["GD_PROFILE_TASK_NEW"] = "Nowe zadanie";
