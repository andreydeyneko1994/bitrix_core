<?php
$MESS["GD_ACTIVITY_BLOG"] = "Blogi/raporty";
$MESS["GD_ACTIVITY_CALENDAR"] = "Kalendarze";
$MESS["GD_ACTIVITY_FILES"] = "Pliki";
$MESS["GD_ACTIVITY_FORUM"] = "forum/Dyskusje";
$MESS["GD_ACTIVITY_MORE"] = "Aktualności autora";
$MESS["GD_ACTIVITY_PHOTO"] = "Galeria";
$MESS["GD_ACTIVITY_SYSTEM"] = "system";
$MESS["GD_ACTIVITY_SYSTEM_FRIENDS"] = "Znajomi";
$MESS["GD_ACTIVITY_SYSTEM_GROUPS"] = "Grupy";
$MESS["GD_ACTIVITY_TASKS"] = "Zadania";
$MESS["GD_ACTIVITY_TITLE"] = "Logi aktywności";
