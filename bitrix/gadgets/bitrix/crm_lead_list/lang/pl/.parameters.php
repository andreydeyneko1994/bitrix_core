<?php
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Utworzony";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["GD_CRM_COLUMN_STATUS"] = "Status";
$MESS["GD_CRM_LEAD_LIST_LEAD_COUNT"] = "Leadów na stronę";
$MESS["GD_CRM_ONLY_MY"] = "Tylko Moje";
$MESS["GD_CRM_SORT"] = "Sortowanie";
$MESS["GD_CRM_SORT_ASC"] = "Rosnąco";
$MESS["GD_CRM_SORT_BY"] = "Zamówienie";
$MESS["GD_CRM_SORT_DESC"] = "Malejąco";
