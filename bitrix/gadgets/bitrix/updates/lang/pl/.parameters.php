<?
$MESS["GD_LOG_AVATAR_SIZE"] = "Rozmiar obrazu avatara na Tablicy (rozmowa)";
$MESS["GD_LOG_AVATAR_SIZE_COMMENT"] = "Rozmiar avatara na Tablicy (do wykorzystania w komentarzach)";
$MESS["GD_LOG_P_ENTITY_TYPE"] = "Rodzaj subskrypcji";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_ALL"] = "Wszystkie";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_GROUP"] = "Grupy";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_USER"] = "Użytkownicy";
$MESS["GD_LOG_P_EVENT_ID"] = "Typ Wiadomości";
$MESS["GD_LOG_P_EVENT_ID_VALUE_ALL"] = "Wszystkie";
$MESS["GD_LOG_P_EVENT_ID_VALUE_BLOG"] = "Blogi/raporty";
$MESS["GD_LOG_P_EVENT_ID_VALUE_CALENDAR"] = "Kalendarze";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FILES"] = "Pliki";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FORUM"] = "forum/Dyskusje";
$MESS["GD_LOG_P_EVENT_ID_VALUE_PHOTO"] = "Galeria";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM"] = "system";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "przyjaciele (tylko użytkownicy)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "grupy (tylko użytkownicy)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_TASKS"] = "Zadania";
$MESS["GD_LOG_P_LOG_CNT"] = "Liczba elementów";
$MESS["GD_LOG_P_URL"] = "Komunikat aktualizacji strony URL";
?>