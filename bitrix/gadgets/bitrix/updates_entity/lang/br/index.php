<?php
$MESS["GD_LOG_ENTITY_MORE"] = "Feed do grupo";
$MESS["GD_LOG_ENTITY_TITLE"] = "Feed";
$MESS["GD_UPDATES_ENTITY_BLOG"] = "relatórios";
$MESS["GD_UPDATES_ENTITY_CALENDAR"] = "calendários";
$MESS["GD_UPDATES_ENTITY_FILES"] = "arquivos";
$MESS["GD_UPDATES_ENTITY_FORUM"] = "discussões";
$MESS["GD_UPDATES_ENTITY_PHOTO"] = "foto";
$MESS["GD_UPDATES_ENTITY_SYSTEM"] = "sistema";
$MESS["GD_UPDATES_ENTITY_TASKS"] = "tarefas";
