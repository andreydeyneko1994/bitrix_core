<?
$MESS["CRM_BP_CONTACT_TASK_NAME"] = "Traitement du contact : {= Document : NOM} {= Document : LAST_NAME} (Créé par processus d'entreprise)";
$MESS["CRM_BP_CONTACT_TASK_TEXT"] = "Effectuer le traitement du contact : {=Document:NAME} {=Document:LAST_NAME}";
$MESS["CRM_BP_CONTACT_TITLE"] = "Traitement du contact";
$MESS["CRM_BP_CONTACT_TYPE"] = "Créer un processus d'entreprise consécutif";
?>