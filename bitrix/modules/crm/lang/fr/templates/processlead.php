<?
$MESS["CRM_BP_LEAD_LEADTASK_NAME"] = "Traitement du prospect : {=Document:TITLE} (Fournie par le processus d'entreprise)";
$MESS["CRM_BP_LEAD_TASK_TEXT"] = "Effectuer le traitement du prospect : {=Document:TITLE}";
$MESS["CRM_BP_LEAD_TITLE"] = "Traitement du prospect";
$MESS["CRM_BP_LEAD_TYPE"] = "Créer un processus d'entreprise consécutif";
?>