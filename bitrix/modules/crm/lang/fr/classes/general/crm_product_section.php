<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "Vous ne pouvez pas supprimer la section, car elle contient des produits qui ont référence à une transaction, à un prospect, à un devis ou à une facture.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "Nom de la section non renseignée.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "Section n'a pas été trouvé.";
$MESS["CRM_PRODUCT_SECTION_FIELD_ID"] = "ID";
?>