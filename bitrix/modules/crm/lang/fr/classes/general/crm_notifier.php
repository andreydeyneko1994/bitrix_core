<?php
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Nouvel e-mail reçu";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Rappel demandé";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_ASSIGNED_BY"] = "Vous avez été désigné comme responsable";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_STAGE"] = "Étape modifiée";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Vous avez été mentionné dans le message";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Vous avez été spécifié comme destinataire du message";
$MESS["CRM_NOTIFY_SCHEME_MERGE"] = "Notifications de contrôle en double";
$MESS["CRM_NOTIFY_SCHEME_OTHER"] = "Autres notifications";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulaire CRM soumis";
