<?
$MESS["CRM_EVENT_ERR_ENTITY_DATE_NOT_VALID"] = "Contient la date incorrecte d'évènement.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "Identificateur d'un élément d'entité n'est pas indiqué.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "Le type de l'évènement n'est pas indiqué.";
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "Identifiant de l'entité inconnu.";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Changements";
$MESS["CRM_EVENT_TYPE_DELETE"] = "Supprimer";
$MESS["CRM_EVENT_TYPE_EXPORT"] = "Exporter";
$MESS["CRM_EVENT_TYPE_SNS"] = "Lettre du client";
$MESS["CRM_EVENT_TYPE_USER"] = "Évènements personnalisés";
$MESS["CRM_EVENT_TYPE_VIEW"] = "Vue";
?>