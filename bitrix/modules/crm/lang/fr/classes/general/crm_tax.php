<?php
$MESS["CRM_CATALOG_MODULE_IS_NOT_INSTALLED"] = "Le module CATALOG n'est pas trouvé! Installez, s'il vous plaît, le module CATALOG.";
$MESS["CRM_MODULE_IS_NOT_INSTALLED"] = "Le module CRM n'est pas retrouvé! Veuillez installer le module CRM, s'il vous plaît.";
$MESS["CRM_SALE_MODULE_IS_NOT_INSTALLED"] = "Le module e-magasin n'a pas été trouvé. S'il vous plaît installer ce module.";
$MESS["CRM_VAT_EMPTY_VALUE"] = "Prix HT";
$MESS["CRM_VAT_NOT_SELECTED"] = "[non indiqué]";
