<?php
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "Annulée";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "Expédiée";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "En attente de traitement";
$MESS["CRM_ORDER_STATUS_D"] = "Annulée";
$MESS["CRM_ORDER_STATUS_F"] = "Réalisée";
$MESS["CRM_ORDER_STATUS_N"] = "Commande acceptée, en attente du paiement";
$MESS["CRM_ORDER_STATUS_P"] = "Payée, préparée pour la livraison";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Groupe d'utilisateurs autorisés à modifier les préférences de la boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administrateurs de boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "Groupe de clients contenant tous les clients de la boutique";
$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "Tous les clients";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Groupe d'utilisateurs autorisés à utiliser les fonctionnalités de la boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Responsables des boutiques en ligne";
$MESS["SMAIL_FOOTER_BR"] = "Cordialement,<br /> l'assistance.";
$MESS["SMAIL_FOOTER_SHOP"] = "Boutique en ligne";
$MESS["UP_TYPE_SUBJECT"] = "Retour à la notification de stock";
$MESS["UP_TYPE_SUBJECT_DESC"] = "#USER_NAME# - nom de l'utilisateur
#EMAIL# - adresse email de l'utilisateur
#NAME# - nom de l'article
#PAGE_URL# - page de la description détaillée de l'article";
