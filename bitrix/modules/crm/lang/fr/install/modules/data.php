<?php
$MESS["CRM_CATALOG_NOT_INSTALLED"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_ORD_PROP_6_2"] = "Nom complet";
$MESS["CRM_ORD_PROP_8"] = "Nom de l'entreprise";
$MESS["CRM_ORD_PROP_40"] = "Nom de l'entreprise";
$MESS["CRM_SALE_NOT_INSTALLED"] = "Le module Boutique en ligne n'est pas installé.";
