<?
$MESS["CRM_BP"] = "Processus d'entreprise";
$MESS["CRM_CONFIG"] = "Autres paramètres";
$MESS["CRM_CURRENCIES"] = "Devises";
$MESS["CRM_EXTERNAL_SALE"] = "Boutiques en ligne";
$MESS["CRM_FIELDS"] = "Champs d'utilisateur";
$MESS["CRM_GUIDES"] = "Indicatrices";
$MESS["CRM_PERMS"] = "Droits d'accès";
$MESS["CRM_SENDSAVE"] = "Intégration avec E-mail";
?>