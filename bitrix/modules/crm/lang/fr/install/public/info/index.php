<?
$MESS["CRM_PAGE_CONTENT"] = "<h3>Qu'est-ce que c'est le CRM ?</h3>
 
<p><b>Le système de gestion des relations clients</b> (ou <b>CRM</b>) &mdash; est un système conçu pour augmenter les ventes, optimiser la commercialisation et améliorer le service à la clientèle. CRM stocke l'information sur les clients et l'historique de relations avec eux pour une analyse plus approfondie des résultats.</p>
  
<p> CRM Bitrix24 est construit sur les concepts suivants :</p>
 
<ul>
 <li><b>Contact</b> est un enregistrement contenant les données d'une personne avec laquelle votre entreprise aura ou a déjà une relation.</li>
 
 <li><b>Entreprise</b> est un enregistrement contenant des informations de l'entreprise qui a une certaine forme de coopération (ou peut avoir la coopération) avec la vôtre. </li>
 
 <li><b>Prospect</b> 
est un enregistrement contenant les données d'un contact potentiel pour lequel certaines informations (e-mail, téléphone) sont disponibles mais avec lesquelles aucune interaction formelle n'a encore eu lieu. </li>
 
 <li><b>Evènement</b> est tout changement dans les contacts, les prospects ou les entreprises. Par exemple : l'ajout d'un nouveau numéro de téléphone.</li>
 
 <li><b>Transaction</b> est un enregistrement d'une transaction financière avec un client ou une entreprise. </li>
 </ul>
 
<h3>Comment cela fonctionne ?</h3>
 
<p><b>Le CRM</b> peut être utilisée en 2 façons : </p>
 
<ol>
 <li>comme <b>base de données</b> de Contacts et Entreprises,</li>
 
 <li>comme <b>CRM</b></li> classique 
 </ol>
 
<h4>1. CRM comme base de données</h4>
 
<p>L'utilisation de CRM comme une base de données de contacts et d'entreprises permet mener l'historique de relations avec eux. L'objectif principal de cette affaire sont le Contact et l'Entreprise où à l'aide d'un Evènement on mène l'historique de relations. Dans ce régime, il est toujours possible de créer des prospects et des événements qui s'appliquent à eux afin qu'ils puissent être convertis en transactions.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png' /></p>
 
<h4>2. CRM classique</h4>
 
<p>L'objectif principal du CRM classique est le Prospect qui est ajouté au système à la main par le manager automatiquement à partir de &quot; Bitrix24 : Gestion du site &quot; ou d'autres moyens. Une fois créé, le prospect peut être traité et éventuellement converti en contact, en entreprise ou en transaction. Dans les deux premiers cas, le Prospect devient un élément ordinaire de la base de données. Dans le deuxième cas, en passant par l'Entonnoir de vente il devient une véritable Transaction.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png' /></p>
";
$MESS["CRM_PAGE_TITLE"] = "Aide";
?>