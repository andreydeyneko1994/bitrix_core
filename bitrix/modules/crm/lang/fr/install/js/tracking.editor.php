<?
$MESS["CRM_TRACKING_EDITOR_END_CHECKING"] = "Terminer";
$MESS["CRM_TRACKING_EDITOR_FOUND_ITEMS"] = "Numéros de téléphone - %phones%, adresses e-mail - %emails%";
$MESS["CRM_TRACKING_EDITOR_NOT_FOUND"] = "Aucune entrée n'a été trouvée.";
$MESS["CRM_TRACKING_EDITOR_NOT_SELECTED"] = "Non sélectionné";
$MESS["CRM_TRACKING_EDITOR_REPLACEMENT"] = "Échange de numéro de téléphone et d'e-mail";
$MESS["CRM_TRACKING_EDITOR_SOURCES_NOT_CONFIGURED"] = "Sources non spécifiées";
$MESS["CRM_TRACKING_EDITOR_VIEW_ITEMS"] = "Source de numéros de téléphone et d'adresses e-mail";
?>