<?
$MESS["CRM_ENTITY_REGEXP"] = "Modèle du thème pour déterminer l'entité";
$MESS["CRM_ENTITY_REGEXP_NOTES"] = "(expressions régulières, entre premières parenthèses doit être l'identificateur de l'entité)";
$MESS["CRM_MAIL_ENTITY"] = "Entité : ";
$MESS["CRM_MAIL_ENTITY_ALL"] = "Aléatoire";
$MESS["CRM_MAIL_ENTITY_COMPANY"] = "Entreprise";
$MESS["CRM_MAIL_ENTITY_CONTACT"] = "Contact";
$MESS["CRM_MAIL_ENTITY_DEAL"] = "Transaction";
$MESS["CRM_MAIL_ENTITY_LEAD"] = "Prospect";
$MESS["CRM_MAIL_MAIL"] = "adresse email dans CRM";
?>