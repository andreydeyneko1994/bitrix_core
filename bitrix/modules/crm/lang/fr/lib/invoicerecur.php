<?php
$MESS["CRM_INVOICE_RECURRING_ENTITY_ACTIVE_FIELD"] = "Activité";
$MESS["CRM_INVOICE_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Catégorie de la nouvelle transaction";
$MESS["CRM_INVOICE_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Nombre d'instances créées";
$MESS["CRM_INVOICE_RECURRING_ENTITY_EMAIL_ID_FIELD"] = "ID de l'e-mail";
$MESS["CRM_INVOICE_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_INVOICE_RECURRING_ENTITY_INVOICE_ID_FIELD"] = "ID de facture récurrente";
$MESS["CRM_INVOICE_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Méthode de restriction";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Date de la dernière facture récurrente";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Par date";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Par nombre d'instances";
$MESS["CRM_INVOICE_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Date à laquelle la prochaine facture récurrente sera créée";
$MESS["CRM_INVOICE_RECURRING_ENTITY_PARAMS_FIELD"] = "Paramètres pour calculer la facture récurrente suivante";
$MESS["CRM_INVOICE_RECURRING_ENTITY_SEND_BILL_FIELD"] = "Envoyer la facture par e-mail";
$MESS["CRM_INVOICE_RECURRING_ENTITY_START_DATE_FIELD"] = "Date de la première facture récurrente";
