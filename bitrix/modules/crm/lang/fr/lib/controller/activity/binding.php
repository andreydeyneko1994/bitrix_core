<?php
$MESS["CRM_ACTIVITY_BINDING_ALREADY_BOUND_ERROR"] = "L'activité est déjà associée à cette entité";
$MESS["CRM_ACTIVITY_BINDING_LAST_BINDING_ERROR"] = "Impossible de supprimer le seul lien activité-entité.";
$MESS["CRM_ACTIVITY_BINDING_NOT_BOUND_ERROR"] = "L'activité n'est pas associée à cette entité";
