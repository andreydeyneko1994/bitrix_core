<?php
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ACCESS_DENIED"] = "Droits insuffisants pour effectuer l'opération.";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_DELETE_DEDUCTED_ERROR"] = "L'\"Ordre de vente ##ID#\" ne peut être supprimé car il a déjà été traité";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_NOT_USED_INVENTORY_MANAGEMENT"] = "Vous devez activer la gestion de l'inventaire pour pouvoir traiter l'objet d'inventaire";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ORDER_NOT_FOUND_ERROR"] = "La commande est introuvable";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_PRODUCT_NOT_FOUND"] = "Veuillez saisir au moins un produit";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIPMENT_NOT_FOUND_ERROR"] = "Livraison ##ID# introuvable";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIP_DEDUCTED_ERROR"] = "Erreur de traitement de l'\"Ordre de vente ##ID#\" : il a déjà été traité";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_UNSHIP_UNDEDUCTED_ERROR"] = "Erreur d'annulation de l'\"Ordre de vente ##ID#\" : il n'a pas été traité.";
