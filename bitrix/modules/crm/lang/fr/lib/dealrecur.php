<?php
$MESS["CRM_DEAL_RECURRING_ENTITY_ACTIVE_FIELD"] = "Activité";
$MESS["CRM_DEAL_RECURRING_ENTITY_BASED_ID_FIELD"] = "Création à partir de la transaction";
$MESS["CRM_DEAL_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Catégorie de la nouvelle transaction";
$MESS["CRM_DEAL_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Nombre d'instances créées";
$MESS["CRM_DEAL_RECURRING_ENTITY_DEAL_ID_FIELD"] = "ID de transaction récurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_DEAL_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Méthode de restriction";
$MESS["CRM_DEAL_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Date de la dernière transaction récurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Par date";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Par nombre d'instances";
$MESS["CRM_DEAL_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Date à laquelle la prochaine transaction récurrente sera créée";
$MESS["CRM_DEAL_RECURRING_ENTITY_PARAMS_FIELD"] = "Paramètres pour calculer la transaction récurrente suivante";
$MESS["CRM_DEAL_RECURRING_ENTITY_START_DATE_FIELD"] = "Date de la transaction récurrente";
