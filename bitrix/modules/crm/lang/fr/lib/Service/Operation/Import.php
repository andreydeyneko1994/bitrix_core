<?php
$MESS["CRM_FIELD_CRATED_TIME_VALUE_IN_FUTURE_ERROR"] = "La valeur de \"#FIELD#\" ne peut pas être dans le futur";
$MESS["CRM_FIELD_CRATED_TIME_VALUE_NOT_MONOTONE_ERROR"] = "La valeur de \"#FIELD#\" ne peut être inférieure à celle de tout autre article";
$MESS["CRM_FIELD_VALUE_CAN_NOT_BE_GREATER_ERROR"] = "La valeur de \"#FIELD1#\" ne peut pas être supérieure à \"#FIELD2#\"";
$MESS["CRM_FIELD_VALUE_ONLY_ADMIN_CAN_SET_ERROR"] = "Seul l'administrateur peut modifier la valeur de \"#FIELD#\"";
