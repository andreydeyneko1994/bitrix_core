<?php
$MESS["CRM_TIMELINE_ACTIVITY_CREATION"] = "Activité créée : #TITLE#";
$MESS["CRM_TIMELINE_COMPANY_CREATION"] = "Entreprise créée";
$MESS["CRM_TIMELINE_CONTACT_CREATION"] = "Contact créé";
$MESS["CRM_TIMELINE_DEAL_CREATION"] = "Transaction créée";
$MESS["CRM_TIMELINE_DEAL_ORDER_TITLE"] = "Pour la commande : <a href='#HREF#'>##ORDER_ID#</a> de #DATE_TIME# (pour #PRICE_WITH_CURRENCY#)";
$MESS["CRM_TIMELINE_INVOICE_CREATION"] = "Facture créée";
$MESS["CRM_TIMELINE_LEAD_CREATION"] = "Prospect créé";
$MESS["CRM_TIMELINE_ORDER_CREATION"] = "Commande créée";
$MESS["CRM_TIMELINE_ORDER_PAYMENT_CREATION"] = "Paiement créé";
$MESS["CRM_TIMELINE_ORDER_SHIPMENT_CREATION"] = "Expédition créée";
$MESS["CRM_TIMELINE_QUOTE_CREATION"] = "Devis créé";
$MESS["CRM_TIMELINE_RECURRING_DEAL_CREATION"] = "Modèle de transaction créé";
$MESS["CRM_TIMELINE_TASK_CREATION"] = "Tâche créée : #TITLE#";
