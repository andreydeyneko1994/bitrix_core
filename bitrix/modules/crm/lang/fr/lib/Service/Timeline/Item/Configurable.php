<?php
$MESS["CRM_TIMELINE_CLIENT_TITLE"] = "Client";
$MESS["CRM_TIMELINE_MARKET_PANEL_TEXT"] = "Plus de 1000 extensions complémentaires pour votre CRM";
$MESS["CRM_TIMELINE_MARKET_PANEL_TEXT_DETAILS"] = "Plus d'informations";
$MESS["CRM_TIMELINE_MENU_FASTEN"] = "Épingler";
$MESS["CRM_TIMELINE_MENU_UNFASTEN"] = "Détacher";
