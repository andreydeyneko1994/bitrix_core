<?php
$MESS["CRM_TRACKED_OBJECT_PRODUCT_DEPENDANT_UPDATE_TEXT"] = "Champ \"#FIELD_NAME#\" de \"#PRODUCT_NAME#\" modifié";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_DISCOUNT_SUM"] = "Valeur de la réduction";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_MEASURE_NAME"] = "Unité de mesure";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_PRICE"] = "Prix";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_PRODUCT_NAME"] = "Nom du produit";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_QUANTITY"] = "Quantité";
$MESS["CRM_TRACKED_OBJECT_PRODUCT_FIELD_NAME_TAX_RATE"] = "Taux de TVA";
