<?
$MESS["CRM_RQ_IMP_HLPR_ERR_ADDRESS_TYPE_ALREADY_EXISTS"] = "Le type d'adresse a été spécifié, mais il existe déjà dans les mentions";
$MESS["CRM_RQ_IMP_HLPR_ERR_BD_KEY_FIELDS_NOT_PRESENT"] = "Les champs clés des coordonnées bancaires ne sont pas définis";
$MESS["CRM_RQ_IMP_HLPR_ERR_BD_NAME_IS_NOT_SET"] = "Le nom de coordonnées bancaires n'est pas spécifié";
$MESS["CRM_RQ_IMP_HLPR_ERR_CREATE_BANK_DETAIL"] = "Erreur lors de la création des coordonnées bancaires pour #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_CREATE_REQUISITE"] = "Erreur lors de la création #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_DEF_IMP_PRESET_NOT_DEFINED"] = "Le modèle d'importation de mentions #ENTITY_TYPE_NAME_GENITIVE# n'est pas défini";
$MESS["CRM_RQ_IMP_HLPR_ERR_EMPTY_BD_KEY_FIELDS"] = "Les champs clés de coordonnées bancaires sont manquants";
$MESS["CRM_RQ_IMP_HLPR_ERR_EMPTY_RQ_KEY_FIELDS"] = "Les champs clés de mentions sont manquants";
$MESS["CRM_RQ_IMP_HLPR_ERR_IMP_PRESET_HAS_NO_ADDR_FIELD"] = " Le modèle d'importation de mentions n'inclut pas le champ d'adresse (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_IMP_PRESET_NOT_EXISTS"] = "Le modèle d'importation de mentions n'a pas été trouvé (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_ENTITY_ID"] = "Aucune entité spécifiée pour les mentions";
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_ENTITY_TYPE"] = "Type d'entité incorrect des mentions";
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_IMP_PRESET_ID"] = "ID de modèle incorrect pour l'importation de mentions";
$MESS["CRM_RQ_IMP_HLPR_ERR_PRESET_ASSOC"] = "Impossible de faire correspondre le modèle pour importer les mentions";
$MESS["CRM_RQ_IMP_HLPR_ERR_RQ_KEY_FIELDS_NOT_PRESENT"] = "Les mots clés de mentions ne sont pas spécifiés";
$MESS["CRM_RQ_IMP_HLPR_ERR_RQ_NAME_IS_NOT_SET"] = "Le nom de mentions n'est pas spécifié";
$MESS["CRM_RQ_IMP_HLPR_ERR_UPDATE_BANK_DETAIL"] = "Erreur lors de la mise à jour des coordonnées bancaires #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_UPDATE_REQUISITE"] = "Erreur lors de la mise à jour des mentions #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
?>