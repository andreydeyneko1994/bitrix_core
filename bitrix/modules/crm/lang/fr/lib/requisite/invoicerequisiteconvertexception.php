<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Erreur générale.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Type de payeur introuvable.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "Le modèle sélectionné n'a pas de champ pouvant être rempli en utilisant des mentions existantes.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Impossible de transférer des données, car les mentions de la facture existante ont été supprimées.";
?>