<?php
$MESS["CRM_COMPONENT_FACTORYBASED_COPY_PAGE_URL"] = "Copier le lien de l'article dans le Presse-papiers";
$MESS["CRM_COMPONENT_FACTORYBASED_EDITOR_MAIN_SECTION_TITLE"] = "À propos de l'article";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TEXT"] = "Par défaut, le montant de l'article est calculé comme la somme des prix des produits. Cependant, l'article spécifie déjà un montant différent. Vous pouvez laisser la valeur actuelle inchangée ou confirmer le nouveau calcul.";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TITLE"] = "Modifier le mode de calcul du montant de l'article";
$MESS["CRM_COMPONENT_FACTORYBASED_NEW_ITEM_TITLE"] = "Créer #ENTITY_NAME#";
$MESS["CRM_COMPONENT_FACTORYBASED_PAGE_URL_COPIED"] = "Le lien de l'article a été copié dans le Presse-papiers";
$MESS["CRM_COMPONENT_FACTORYBASED_TIMELINE_HISTORY_STUB"] = "Vous créez un article...";
