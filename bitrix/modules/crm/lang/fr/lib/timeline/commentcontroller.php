<?php
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "Vous a mentionné(e) dans un commentaire à \"#ENTITY_TITLE#\", texte du commentaire : \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "Vous a mentionnée dans un commentaire à \"#ENTITY_TITLE#\", texte du commentaire : \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "Vous a mentionné dans un commentaire à \"#ENTITY_TITLE#\", texte du commentaire : \"#COMMENT#\"";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "société #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "contact #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "transaction #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DYNAMIC"] = "l'article #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "facture #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "prospect #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_ORDER"] = "commande #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_QUOTE"] = "devis #ENTITY_NAME#";
