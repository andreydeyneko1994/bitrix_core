<?php
$MESS["CRM_LEAD_CONVERSION"] = "Créer sur base de";
$MESS["CRM_LEAD_CREATION"] = "Prospect ajouté";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Mode de calcul du montant total modifié";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Calculer automatiquement en utilisant les prix des produits";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manuel";
$MESS["CRM_LEAD_MODIFICATION_STATUS"] = "Statut modifié";
$MESS["CRM_LEAD_MOVING_TO_RECYCLEBIN"] = "Le prospect a été envoyé à la Corbeille";
$MESS["CRM_LEAD_RESTORATION"] = "Le prospect a été récupéré depuis la Corbeille.";
