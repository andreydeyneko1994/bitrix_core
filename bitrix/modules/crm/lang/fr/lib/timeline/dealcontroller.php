<?php
$MESS["CRM_DEAL_BASE_CAPTION_DEAL_RECURRING"] = "Créé à partir d'un modèle";
$MESS["CRM_DEAL_BASE_CAPTION_LEAD"] = "Basé sur le prospect";
$MESS["CRM_DEAL_CHECK_TITLE"] = "Reçu #NAME# du #DATE_PRINT#";
$MESS["CRM_DEAL_CREATION"] = "Transaction ajoutée";
$MESS["CRM_DEAL_CREATION_BASED_ON"] = "Créé sur la base de";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Mode de calcul du montant total modifié";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Calcul automatique en utilisant les éléments de la transaction";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manuel";
$MESS["CRM_DEAL_MODIFICATION_STAGE"] = "Étape modifiée";
$MESS["CRM_DEAL_MOVING_TO_RECYCLEBIN"] = "La transaction a été envoyée à la Corbeille";
$MESS["CRM_DEAL_RESTORATION"] = "La transaction a été récupérée depuis la Corbeille.";
$MESS["CRM_DEAL_SUMMARY_ORDER"] = "Commande ##ORDER_ID# du #ORDER_DATE#";
