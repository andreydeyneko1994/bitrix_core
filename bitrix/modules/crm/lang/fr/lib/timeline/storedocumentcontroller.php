<?php
$MESS["STORE_DOCUMENT_ARRIVAL_STATUS_CHANGE"] = "Statut de la réception en stock modifié";
$MESS["STORE_DOCUMENT_DEDUCT_STATUS_CHANGE"] = "Statut de la radiation modifié";
$MESS["STORE_DOCUMENT_MOVING_STATUS_CHANGE"] = "Statut du transfert modifié";
$MESS["STORE_DOCUMENT_STATUS_CANCELLED"] = "Annulé";
$MESS["STORE_DOCUMENT_STATUS_CONDUCTED"] = "Traité";
$MESS["STORE_DOCUMENT_STATUS_DRAFT"] = "Brouillon";
$MESS["STORE_DOCUMENT_STORE_ADJUSTMENT_STATUS_CHANGE"] = "Statut de l'ajustement du stock modifié";
$MESS["STORE_DOCUMENT_TITLE"] = "#TITLE# du #DATE# pour #PRICE_WITH_CURRENCY#";
