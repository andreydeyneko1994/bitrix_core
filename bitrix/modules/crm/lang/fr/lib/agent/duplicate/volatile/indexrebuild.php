<?php
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_ALREADY_RUNNING"] = "Impossible de lancer la réindexation car elle est déjà en cours.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_RUNNING"] = "Impossible d'arrêter la réindexation car elle n'a jamais été démarrée.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_STOPPED"] = "Impossible de reprendre la réindexation car elle n'est ni en cours ni en pause.";
