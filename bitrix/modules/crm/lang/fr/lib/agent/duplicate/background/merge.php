<?php
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_ALREADY_RUNNING"] = "Impossible de démarrer la fusion car elle est déjà en cours.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_RUNNING"] = "Impossible d'arrêter la fusion car elle n'a pas été démarrée.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_STOPPED"] = "Impossible de reprendre la fusion car elle n'a jamais été démarrée ou arrêtée.";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_050"] = "50 % des entreprises en double fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_090"] = "90 % des entreprises en double fusionnées";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_100"] = "Les entreprises en double ont été fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_050"] = "50 % des contacts en double fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_090"] = "90 % des contacts en double fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_100"] = "Les contacts en double ont été fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_050"] = "50 % des prospects en double fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_090"] = "90 % des prospects en double fusionnés";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_100"] = "Les prospects en double ont été fusionnés";
