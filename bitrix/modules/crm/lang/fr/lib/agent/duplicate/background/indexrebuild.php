<?php
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_ALREADY_RUNNING"] = "Impossible de lancer la réindexation car elle est déjà en cours.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_INDEX_TYPES"] = "Les types d'index ne sont pas définis ou ont des valeurs non valides.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_RUNNING"] = "Impossible d'arrêter la réindexation car elle n'a pas été démarrée.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_STOPPED"] = "Impossible de reprendre la réindexation car elle n'a jamais été démarrée ou arrêtée.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_SCOPE"] = "Le code pays n'est pas spécifié ou a une valeur non valide.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_TYPE_INDEX"] = "Le type d'index actuel est incorrect.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_050"] = "50 % des doublons d'entreprises scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_090"] = "90 % des doublons d'entreprises scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_100"] = "Les doublons d'entreprises ont été scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_050"] = "50 % des doublons de contacts scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_090"] = "90 % des doublons de contacts scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_100"] = "Les doublons de contacts ont été scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_050"] = "50 % des doublons de prospects scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_090"] = "90 % des doublons de prospects scannés";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_100"] = "Les doublons de prospects ont été scannés";
