<?
$MESS["CRM_AGENT_NOTICE_NOTIFY_AGENT_ABOUT_REPEAT_LEAD"] = "Les prospects répétés sont activés dans votre Bitrix24 ! Quand un client existant vous contact, le CRM le reconnait et crée un prospect répété pour que le responsable puisse le traiter et créer une nouvelle vente. <a href=\"https://helpdesk.bitrix24.com/open/7348303/\">Détails</a>";
?>