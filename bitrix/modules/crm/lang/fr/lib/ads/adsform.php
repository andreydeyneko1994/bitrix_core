<?
$MESS["CRM_ADS_FORM_TYPE_ERR_DISABLED_FACEBOOK"] = "En raison de récents changements dans la politique de confidentialité de Facebook, l'intégration entre les formulaires CRM de Bitrix24 et Facebook Lead Ads est temporairement indisponible. Les techniciens de Facebook travaillent sur la nouvelle API et ont promis de rendre bientôt cette fonctionnalité disponible aux utilisateurs de Bitrix24.";
$MESS["CRM_ADS_FORM_TYPE_NAME_FACEBOOK"] = "Publicités Facebook";
?>