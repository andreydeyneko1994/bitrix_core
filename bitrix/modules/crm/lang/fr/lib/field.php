<?php
$MESS["CRM_FIELD_NOT_UNIQUE_ERROR"] = "La valeur de \"#FIELD#\" n'est pas unique";
$MESS["CRM_FIELD_NOT_VALID_ERROR"] = "La valeur de \"#FIELD#\" est incorrecte";
$MESS["CRM_FIELD_VALUE_REQUIRED_ERROR"] = "Le champ \"#FIELD#\" est requis";
