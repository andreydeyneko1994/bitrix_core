<?php
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Aucune entité de coordonnées bancaires n'a été spécifiée";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Type d'entité de coordonnées bancaires incorrect.";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Impossible de trouver les coordonnées bancaires à supprimer";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Erreur lors de la suppression des coordonnées bancaires";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ACTIVE"] = "Activité";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COMMENTS"] = "Commentaire";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_ID"] = "ID du pays";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_NAME"] = "Pays";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ID"] = "ID";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_NAME"] = "Prénom";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_SORT"] = "Classification";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MAX"] = "La taille de \"#FIELD_TITLE#\" ne doit pas dépasser #MAX_LENGTH# caractères.";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MIN"] = "La taille de \"#FIELD_TITLE#\" doit être d'au moins #MAX_LENGTH# caractères.";
