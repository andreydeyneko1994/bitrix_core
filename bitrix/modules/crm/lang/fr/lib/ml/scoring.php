<?
$MESS["CRM_SCORING_LICENSE_TEXT_P1"] = "Quel est le critère utilisé par votre responsable commercial lors du traitement des transactions ? La file d'attente ? L'intuition ? Il vous suffit de prendre et d'amener vos clients les plus prometteurs en haut de votre liste. AI Forecast analysera les transactions existantes pour afficher leurs probabilités de réussite.";
$MESS["CRM_SCORING_LICENSE_TEXT_P2"] = "Le système aidera vos employés à identifier les domaines qui nécessitent le plus d'attention.";
$MESS["CRM_SCORING_LICENSE_TEXT_P3"] = "AI Forecast est disponible avec toutes les offres majeures. Maintenant, mettez à jour votre offre pour améliorer l'efficacité de votre service commercial et réduire les coûts de publicité.";
$MESS["CRM_SCORING_LICENSE_TITLE"] = "AI Scoring";
?>