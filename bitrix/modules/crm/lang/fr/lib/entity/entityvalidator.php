<?
$MESS["CRM_ENTITY_VALIDATOR_FIELD_IS_MISSING"] = "Le champ obligatoire \"%FIELD_NAME%\" n'a pas été renseigné.";
$MESS["CRM_ENTITY_VALIDATOR_FIELD_MUST_BE_GREATER_THEN_ZERO"] = "Le champ \"%FIELD_NAME%\" doit être supérieur à zéro.";
?>