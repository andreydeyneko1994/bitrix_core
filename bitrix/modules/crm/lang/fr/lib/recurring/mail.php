<?
$MESS["CRM_ERROR_SAVING_BILL"] = "Erreur lors de l'enregistrement de la facture sur le lecteur.";
$MESS["CRM_RECUR_DEFAULT_EMAIL_SUBJECT"] = "Facture #ACCOUNT_NUMBER#";
$MESS["CRM_RECUR_EMPTY_BODY_MESSAGE"] = "[sans texte]";
$MESS["CRM_RECUR_INVALID_EMAIL"] = "Destinataire de l'e-mail incorrect";
?>