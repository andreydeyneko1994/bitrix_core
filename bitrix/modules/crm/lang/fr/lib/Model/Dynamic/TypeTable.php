<?php
$MESS["CRM_TYPE_TABLE_DELETE_ERROR_ITEMS"] = "Impossible de supprimer le type d'entité car il existe encore des éléments de ce type.";
$MESS["CRM_TYPE_TABLE_DISABLING_CATEGORIES_IF_MORE_THAN_ONE"] = "Vous ne pouvez pas désactiver les pipelines s'il y en a plusieurs.";
$MESS["CRM_TYPE_TABLE_DISABLING_RECYCLEBIN_WHILE_NOT_EMPTY"] = "Supprimez tous les éléments de ce type de la Corbeille avant de le désactiver.";
$MESS["CRM_TYPE_TABLE_FIELD_NOT_CHANGEABLE_ERROR"] = "Impossible de changer la valeur de \"#FIELD#\".";
