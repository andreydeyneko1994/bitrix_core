<?php
$MESS["CRM_ADDRESS_TYPE_BENEFICIARY"] = "Adresse du bénéficiaire";
$MESS["CRM_ADDRESS_TYPE_DELIVERY"] = "Adresse de la livraison";
$MESS["CRM_ADDRESS_TYPE_HOME"] = "Adresse enregistrée";
$MESS["CRM_ADDRESS_TYPE_PRIMARY"] = "Adresse postale";
$MESS["CRM_ADDRESS_TYPE_REGISTERED"] = "Adresse juridique";
