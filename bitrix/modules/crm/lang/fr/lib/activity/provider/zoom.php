<?php
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_ENDED_TITLE"] = "La réunion Zoom s'est terminée";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_JOINED_TITLE"] = "Le client a cliqué sur un lien d'invitation Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_START_TITLE"] = "Réunion Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_ERROR_INCORRECT_DATETIME"] = "La réunion ne peut pas être programmée pour l'heure que vous avez sélectionnée.";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_MESSAGE_SENT_TITLE"] = "Notification de réunion Zoom envoyée au client";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_TITLE"] = "Réunion Zoom";
