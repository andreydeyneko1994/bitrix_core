<?
$MESS["CRM_INVOICE_COMPAT_HELPER_CANCEL_ERROR"] = "Erreur lors de l'annulation de la facture. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DELETE_ERROR"] = "Erreur lors de la suppression de la facture. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_CANCEL"] = "La facture ID ##ID# a déjà été annulée";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_PAY"] = "La facture ID ##ID# a déjà été payée";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_STATUS"] = "La facture ID ##ID# a déjà le statut requis";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_ACCOUNT_NUMBER"] = "La facture # ne peut pas être vide";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_CURRENCY"] = "Devise non spécifiée";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_PERS_TYPE"] = "Le type de payeur n'est pas spécifié";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_SITE"] = "Le site de la facture n'est pas spécifié";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_USER_ID"] = "L'ID du client n'est pas spécifié";
$MESS["CRM_INVOICE_COMPAT_HELPER_EXISTING_ACCOUNT_NUMBER"] = "La facture spécifiée # est déjà utilisée";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE"] = "La facture ID ##ID# est introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE_ID"] = "L'ID de facture manque";
$MESS["CRM_INVOICE_COMPAT_HELPER_PAY_ERROR"] = "Erreur lors du paiement de la facture. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_CURRENCY"] = "Devise ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_DELIVERY"] = "Service de livraison ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PERSON_TYPE"] = "Type de payeur ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PS"] = "Système de paiement ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_SITE"] = "Site ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_STATUS"] = "Statut ##ID# introuvable";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_USER"] = "Utilisateur ##ID# introuvable";
?>