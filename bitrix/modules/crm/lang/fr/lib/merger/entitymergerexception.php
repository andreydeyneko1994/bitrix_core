<?
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_OCCURRED"] = "Impossible de fusionner automatiquement en raison d'un conflit de données.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_RESOLUTION_NOT_SUPPORTED"] = "La méthode de résolution de conflit \"#RESOLUTION_CAPTION#\" n'est pas prise en charge.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_DENIED"] = "Pas d'accès à la suppression pour '#TITLE#' [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_FAILED"] = "Impossible de supprimer '#TITLE#' [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_ERROR"] = "Une erreur est survenue lors de la fusion. Message d'erreur : #ERROR#";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_NOT_FOUND"] = "Article [#ID#] introuvable.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_READ_DENIED"] = "Pas d'accès en lecture pour '#TITLE#' [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_DENIED"] = "Pas d'accès de mise à jour pour '#TITLE#' [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_FAILED"] = "Impossible d'enregistrer '#TITLE#' [#ID#].";
?>