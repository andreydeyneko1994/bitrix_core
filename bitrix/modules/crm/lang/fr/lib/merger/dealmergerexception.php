<?
$MESS["CRM_DEAL_MERGER_EXCEPTION_CONFLICT_OCCURRED_CATEGORY"] = "Les transactions que vous voulez fusionner doivent être dans le même pipeline.";
$MESS["CRM_DEAL_MERGER_EXCEPTION_CONFLICT_OCCURRED_RECURRENCE"] = "Les transactions que vous voulez fusionner doivent avoir les mêmes propriétés de répétition.";
?>