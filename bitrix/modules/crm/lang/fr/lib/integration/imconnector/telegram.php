<?php
$MESS["CRM_IMCONNECTOR_TELEGRAM_ORDER"] = "La commande ##ORDER_ID# de #SUM_WITH_CURRENCY# du #DATE# a été passée";
$MESS["CRM_IMCONNECTOR_TELEGRAM_PAYMENT_PAID"] = "La commande ##ORDER_ID# de #SUM_WITH_CURRENCY# du #DATE# a été payée";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_DEDUCTED"] = "La commande ##ORDER_ID# de #SUM_WITH_CURRENCY# du #DATE# a été expédiée";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_READY"] = "La commande ##ORDER_ID# de #SUM_WITH_CURRENCY# du #DATE# peut être récupérée à l'entrepôt au \"#STORE_ADDRESS#\"";
