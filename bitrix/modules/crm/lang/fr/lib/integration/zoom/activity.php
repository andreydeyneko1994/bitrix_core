<?php
$MESS["CRM_ZOOM_ACTIVITY_CONFERENCE_ENDED_TITLE"] = "Zoom terminé";
$MESS["CRM_ZOOM_ACTIVITY_CONFERENCE_TITLE"] = "Réunion Zoom";
$MESS["CRM_ZOOM_ACTIVITY_USER_JOINED_TITLE"] = "Le client a cliqué sur un lien d'invitation Zoom";
