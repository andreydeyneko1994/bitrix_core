<?
$MESS["CRM_RECYCLE_BIN_CONTACT_ENTITY_NAME"] = "Contact";
$MESS["CRM_RECYCLE_BIN_CONTACT_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer le contact sélectionné ?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVAL_CONFIRMATION"] = "Le contact sera définitivement supprimé. Voulez-vous poursuivre ?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVED"] = "Le contact a été supprimé.";
$MESS["CRM_RECYCLE_BIN_CONTACT_RESTORED"] = "Le contact a bien été récupéré.";
?>