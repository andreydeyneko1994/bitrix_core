<?
$MESS["CRM_RECYCLE_BIN_COMPANY_ENTITY_NAME"] = "Société";
$MESS["CRM_RECYCLE_BIN_COMPANY_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer la société sélectionnée ?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVAL_CONFIRMATION"] = "La société sera définitivement supprimée. Voulez-vous poursuivre ?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVED"] = "La société a été supprimée.";
$MESS["CRM_RECYCLE_BIN_COMPANY_RESTORED"] = "La société a bien été récupérée.";
?>