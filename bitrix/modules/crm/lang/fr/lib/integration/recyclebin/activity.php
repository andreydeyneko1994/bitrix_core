<?
$MESS["CRM_RECYCLE_BIN_ACTIVITY_ENTITY_NAME"] = "Activité";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer l'activité sélectionnée ?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVAL_CONFIRMATION"] = "L'activité sera définitivement supprimée. Voulez-vous poursuivre ?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVED"] = "L'activité a été supprimée.";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RESTORED"] = "L'activité a bien été récupérée.";
?>