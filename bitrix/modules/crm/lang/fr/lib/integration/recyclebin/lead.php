<?
$MESS["CRM_RECYCLE_BIN_LEAD_ENTITY_NAME"] = "Prospect";
$MESS["CRM_RECYCLE_BIN_LEAD_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer le prospect sélectionné ?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVAL_CONFIRMATION"] = "Le prospect sera définitivement supprimé. Voulez-vous poursuivre ?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVED"] = "Le prospect a été supprimé.";
$MESS["CRM_RECYCLE_BIN_LEAD_RESTORED"] = "Le prospect a bien été récupéré.";
?>