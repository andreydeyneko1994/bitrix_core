<?php
$MESS["CRM_RECYCLE_BIN_DYNAMIC_ENTITY_NAME"] = "Élément";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer l'élément sélectionné ?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVAL_CONFIRMATION"] = "L'élément sera supprimé définitivement. Voulez-vous continuer ?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVED"] = "L'élément a été supprimé.";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RESTORED"] = "L'élément a bien été récupéré.";
