<?php
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_ENTITY_NAME"] = "Facture";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer la facture sélectionnée ?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVAL_CONFIRMATION"] = "La facture sera supprimée définitivement. Voulez-vous continuer ?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVED"] = "La facture a été supprimée.";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RESTORED"] = "La facture a bien été récupérée.";
