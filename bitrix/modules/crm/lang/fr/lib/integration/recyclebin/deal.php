<?
$MESS["CRM_RECYCLE_BIN_DEAL_ENTITY_NAME"] = "Transaction";
$MESS["CRM_RECYCLE_BIN_DEAL_RECOVERY_CONFIRMATION"] = "Voulez-vous récupérer la transaction sélectionnée ?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVAL_CONFIRMATION"] = "La transaction sera définitivement supprimée. Voulez-vous poursuivre ?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVED"] = "La transaction a été supprimée.";
$MESS["CRM_RECYCLE_BIN_DEAL_RESTORED"] = "La transaction a bien été récupérée.";
?>