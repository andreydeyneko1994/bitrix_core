<?php
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_ADDRESS"] = "Adresse du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_COMPANY"] = "Adresse de l'entreprise du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_INN"] = "ID de contribuable du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_KPP"] = "KPP du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_NAME"] = "Fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_PERSON_NAME"] = "Nom complet du représentant du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_PHONE"] = "Numéro de téléphone du fournisseur";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_TITLE"] = "Réception en stock";
