<?php
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_CODE"] = "Vente automatique des articles non expédiés lors de la conclusion de la transaction";
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_DESCRIPTION"] = "Lorsque la transaction est conclue, le système vérifie les articles non expédiés et crée une commande de vente.";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE"] = "Mode de réservation";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_MANUAL"] = "Manuel";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_ADD_TO_DOCUMENT"] = "Lors de l'ajout d'un produit à une transaction";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_PAYMENT"] = "Lors du paiement";
$MESS["CRM_SALE_RESERVATION_CONFIG_PERIOD"] = "Réserver pour, jours";
$MESS["CRM_SALE_RESERVATION_ENTITY_DEAL"] = "Transaction";
$MESS["CRM_SALE_RESERVATION_ENTITY_INVOICE"] = "Facture";
$MESS["CRM_SALE_RESERVATION_ENTITY_OFFER"] = "Devis";
$MESS["CRM_SALE_RESERVATION_ENTITY_SMARTPROCESS"] = "Automatisation intelligente de processus";
