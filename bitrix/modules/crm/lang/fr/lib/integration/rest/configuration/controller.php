<?php
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Il y a eu une ou plusieurs erreurs lors de la suppression des données : entité ignorée (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Il y a eu une ou plusieurs erreurs lors de l'exportation des données : entité ignorée (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Il y a eu une ou plusieurs erreurs lors de l'importation des données : entité ignorée (#CODE#)";
