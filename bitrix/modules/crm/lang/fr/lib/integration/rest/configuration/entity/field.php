<?php
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Un champ avec le code #CODE# d'un autre type existe déjà. Veuillez supprimer ce champ et recommencer l'importation.";
