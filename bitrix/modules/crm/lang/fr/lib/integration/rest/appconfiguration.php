<?php
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "Cliquez sur \"Exporter\" pour enregistrer vos formulaires CRM dans un fichier";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "Cliquez sur \"Exporter\" pour enregistrer votre CRM préinstallé dans un fichier";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "Exporter les formulaires CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "Exporter un CRM préinstallé";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "Récupérez des formulaires CRM prêts à l'emploi pour votre entreprise en seulement quelques minutes !";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "Recevez un CRM préinstallé pour votre entreprise en quelques minutes !";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "Exporter les formulaires CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "Exporter un CRM préinstallé";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "Formulaires CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "Solution préinstallée CRM";
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Des erreurs se sont produites lors de la suppression des données : entité ignorée (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Des erreurs se sont produites lors de l'exportation des données : entité ignorée (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Un champ avec le code #CODE# d'un autre type existe déjà. Supprimez ce champ et recommencez l'importation.";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Des erreurs se sont produites lors de l'importation des données : entité ignorée (#CODE#)";
