<?
$MESS["CRM_DOCUMENTGENERATOR_ADD_NEW_TEMPLATE"] = "Ajouter un nouveau modèle";
$MESS["CRM_DOCUMENTGENERATOR_DOCUMENTS_LIST"] = "Documents";
$MESS["CRM_DOCUMENTGENERATOR_SPOTLIGHT_TEXT"] = "Vous pouvez maintenant travailler avec les modèles de document. Créez un document, puis imprimez-le ou envoyez-le via e-mail.";
?>