<?
$MESS["CRM_CALENDAR_HELP_LINK"] = "En savoir plus";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_DEAL"] = "Sélectionnez le mode d'affichage de transaction préféré à utiliser dans la calendrier : par date de création, par autre date ou par disponibilité des ressources.";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_LEAD"] = "Sélectionnez le mode d'affichage de prospect préféré à utiliser dans la calendrier : par date de création, par autre date ou par disponibilité des ressources.";
$MESS["CRM_CALENDAR_VIEW_SPOTLIGHT"] = "Vous pouvez à présent afficher les prospects et transactions en mode calendrier. Planifiez vos relations client en gardant un œil sur la disponibilité des ressources.";
?>