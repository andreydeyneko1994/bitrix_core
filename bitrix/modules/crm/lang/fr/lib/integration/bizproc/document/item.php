<?php
$MESS["CRM_BP_DOCUMENT_ITEM_ENTITY_TYPE_ERROR"] = "Aucune SPA de ce type n'a été trouvée";
$MESS["CRM_BP_DOCUMENT_ITEM_FIELD_IS_MANUAL_OPPORTUNITY"] = "Calculer le montant manuellement";
$MESS["CRM_ENTITY_EXISTENCE_ERROR"] = "L'ID d'entité #DOCUMENT_ID# n'existe pas";
