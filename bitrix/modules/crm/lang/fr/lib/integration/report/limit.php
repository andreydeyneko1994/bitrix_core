<?
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK"] = "Pour retirer la limite, supprimez les entreprises inutiles ou passez à l'une des offres les plus élevées.";
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK_DELETE"] = "Veuillez supprimer les entreprises inutiles pour retirer la restriction.<br> <a href=\"#MORE_INFO_LINK#\">Details</a>";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK"] = "Pour retirer la limite, supprimez les contacts inutiles ou passez à l'une des offres les plus élevées.";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK_DELETE"] = "Veuillez supprimer les contatcs inutiles pour retirer la restriction.<br> <a href=\"#MORE_INFO_LINK#\">Details</a>";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK"] = "Pour retirer la limite, supprimez les transactions inutiles ou passez à l'une des offres les plus élevées.";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK_DELETE"] = "Veuillez supprimer les transactions inutiles pour retirer la restriction.<br> <a href=\"#MORE_INFO_LINK#\">Détails</a>";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK"] = "Pour retirer la limite, supprimez les prospects inutiles ou passez à l'une des offres les plus élevées.";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK_DELETE"] = "Veuillez supprimer les prospects inutiles pour retirer la restriction.<br> <a href=\"#MORE_INFO_LINK#\">Détails</a>";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_ACTUAL_COUNT_MASK"] = "Nombre actuel d'entreprises : #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_MAX_COUNT_MASK"] = "Vous pouvez inclure jusqu'à #MAX_COUNT# entreprises dans un rapport.";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_ACTUAL_COUNT_MASK"] = "Nombre de contacts actuels : #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_MAX_COUNT_MASK"] = "Vous pouvez inclure jusqu'à #MAX_COUNT# contacts dans un rapport.";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_ACTUAL_COUNT_MASK"] = "Nombre actuel de transactions : #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_MAX_COUNT_MASK"] = "Vous pouvez inclure jusqu'à #MAX_COUNT# transactions dans un rapport.";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_ACTUAL_COUNT_MASK"] = "Nombre actuel de prospects : #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_MAX_COUNT_MASK"] = "Vous pouvez inclure jusqu'à #MAX_COUNT# prospects dans un rapport.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT"] = "Seules quelques entités CRM peuvent être incluses dans un rapport analytique.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT_FOR_MORE_INFO"] = "Pour en savoir plus, consultez la page de comparaison des offres.";
?>