<?php
$MESS["CRM_ENTITY_COUNTER_TYPE_FILTER_IDLE"] = "Aucune activité";
$MESS["CRM_ENTITY_COUNTER_TYPE_FILTER_INCOMING_CHANNEL"] = "Entrant";
$MESS["CRM_ENTITY_COUNTER_TYPE_FILTER_OVERDUE"] = "Dépassé";
$MESS["CRM_ENTITY_COUNTER_TYPE_FILTER_PENDING"] = "Pour aujourd'hui";
