<?php
$MESS["CRM_ORDER_FILTER_ACCOUNT_NUMBER"] = "Numéro";
$MESS["CRM_ORDER_FILTER_ACTIVITY_COUNTER"] = "Activités";
$MESS["CRM_ORDER_FILTER_ALL"] = "(tout)";
$MESS["CRM_ORDER_FILTER_CANCELED"] = "Annulée";
$MESS["CRM_ORDER_FILTER_CHECK_PRINTED"] = "Reçu imprimé";
$MESS["CRM_ORDER_FILTER_COMPANY_ID"] = "Entreprises";
$MESS["CRM_ORDER_FILTER_CONTACT_ID"] = "Contacts";
$MESS["CRM_ORDER_FILTER_COUPON"] = "Coupon utilisé";
$MESS["CRM_ORDER_FILTER_CREATED_BY"] = "Créateur";
$MESS["CRM_ORDER_FILTER_CURRENCY"] = "Devise";
$MESS["CRM_ORDER_FILTER_DATE_INSERT"] = "Date de création";
$MESS["CRM_ORDER_FILTER_DATE_UPDATE"] = "Date de modification";
$MESS["CRM_ORDER_FILTER_DEDUCTED"] = "Expédiée";
$MESS["CRM_ORDER_FILTER_DELIVERY_SERVICE"] = "Services de livraison";
$MESS["CRM_ORDER_FILTER_HAS_ASSOCIATED_DEAL"] = "A une transaction";
$MESS["CRM_ORDER_FILTER_ORDER_TOPIC"] = "Sujet de la commande";
$MESS["CRM_ORDER_FILTER_PAYED"] = "Payée";
$MESS["CRM_ORDER_FILTER_PAY_SYSTEM"] = "Systèmes de paiement";
$MESS["CRM_ORDER_FILTER_PERSON_TYPE_ID"] = "Type de payeur";
$MESS["CRM_ORDER_FILTER_PRICE"] = "Montant";
$MESS["CRM_ORDER_FILTER_RESPONSIBLE_ID"] = "Responsable";
$MESS["CRM_ORDER_FILTER_SHIPMENT_DELIVERY_DOC_DATE"] = "Date du document de livraison";
$MESS["CRM_ORDER_FILTER_SHIPMENT_TRACKING_NUMBER"] = "Numéro de suivi";
$MESS["CRM_ORDER_FILTER_SOURCE_ID"] = "Source";
$MESS["CRM_ORDER_FILTER_STATUS_ID"] = "Statut";
$MESS["CRM_ORDER_FILTER_USER"] = "Client";
$MESS["CRM_ORDER_FILTER_XML_ID"] = "ID de commande externe";
