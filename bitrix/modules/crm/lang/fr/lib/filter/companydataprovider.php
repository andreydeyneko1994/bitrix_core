<?
$MESS["CRM_COMPANY_FILTER_ACTIVITY_COUNTER"] = "Activités";
$MESS["CRM_COMPANY_FILTER_ALL"] = "(tous)";
$MESS["CRM_COMPANY_FILTER_COMMUNICATION_TYPE"] = "Communication";
$MESS["CRM_COMPANY_FILTER_EMAIL"] = "E-mail";
$MESS["CRM_COMPANY_FILTER_IM"] = "Messagerie";
$MESS["CRM_COMPANY_FILTER_PHONE"] = "Téléphone";
$MESS["CRM_COMPANY_FILTER_WEB"] = "Site web";
?>