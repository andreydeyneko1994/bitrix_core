<?php
$MESS["CRM_PRESET_ALL_COMPANIES"] = "Toutes les entreprises";
$MESS["CRM_PRESET_ALL_CONTACTS"] = "Tous les contacts";
$MESS["CRM_PRESET_CLOSED_DEALS"] = "Transactions fermées";
$MESS["CRM_PRESET_CLOSED_LEADS"] = "Prospects fermés";
$MESS["CRM_PRESET_CLOSED_QUOTES"] = "Devis fermés";
$MESS["CRM_PRESET_CLOSED_SI"] = "Factures fermées";
$MESS["CRM_PRESET_DEALS_IN_ROBOT_DEBUGGER"] = "Offres en débogage";
$MESS["CRM_PRESET_IN_WORK_DEALS"] = "Transactions en cours";
$MESS["CRM_PRESET_IN_WORK_LEADS"] = "Prospects en cours";
$MESS["CRM_PRESET_IN_WORK_QUOTES"] = "En cours";
$MESS["CRM_PRESET_IN_WORK_SI"] = "Factures en cours";
$MESS["CRM_PRESET_MY_COMPANIES"] = "Mes entreprises";
$MESS["CRM_PRESET_MY_CONTACTS"] = "Mes contacts";
$MESS["CRM_PRESET_MY_DEALS"] = "Mes transactions";
$MESS["CRM_PRESET_MY_LEADS"] = "Mes prospects";
$MESS["CRM_PRESET_MY_QUOTES"] = "Mes devis";
$MESS["CRM_PRESET_MY_SI"] = "Mes factures";
