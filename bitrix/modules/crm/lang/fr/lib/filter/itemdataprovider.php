<?php
$MESS["CRM_FILTER_ITEMDATAPROVIDER_COMPANY_TITLE"] = "Nom de l’entreprise";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_CONTACTS_FULL_NAME"] = "Nom du contact";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_MYCOMPANY_TITLE"] = "Nom de votre entreprise";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_OPPORTUNITY_WITH_CURRENCY"] = "Montant/Devise";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FAIL"] = "Échec";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FILTER_NAME"] = "Groupe d'étape";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_IN_WORK"] = "En cours";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_SUCCESS"] = "Succès";
