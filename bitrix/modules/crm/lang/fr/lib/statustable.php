<?php
$MESS["CRM_STATUS_ENTITY_ENTITY_ID_FIELD"] = "ID d'entité";
$MESS["CRM_STATUS_ENTITY_NAME_FIELD"] = "Titre";
$MESS["CRM_STATUS_ENTITY_STATUS_ID_FIELD"] = "ID du statut";
$MESS["CRM_STATUS_FIELD_UPDATE_ERROR"] = "Impossible de mettre à jour le champ #STATUS_FIELD#";
$MESS["CRM_STATUS_MORE_THAN_ONE_SUCCESS_ERROR"] = "Un seul statut de réussite peut exister pour un même type";
$MESS["CRM_STATUS_STAGE_WITH_ITEMS_ERROR"] = "Impossible de supprimer une étape contenant des éléments";
$MESS["CRM_STATUS_SUCCESS_SEMANTIC_UPDATE_ERROR"] = "Impossible de changer la sémantique du statut de réussite";
