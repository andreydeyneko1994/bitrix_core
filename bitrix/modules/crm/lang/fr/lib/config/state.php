<?php
$MESS["CRM_STATE_CATALOG_PRODUCT_PRICE_CHANGING_BLOCKED"] = "À l'heure actuelle, vous ne pouvez pas modifier le prix d'un produit dans une transaction, une facture, etc. Appliquez une remise pour vendre un produit à un prix inférieur. Vous pouvez activer la modification du prix dans les paramètres du CRM.";
$MESS["CRM_STATE_ERR_CATALOG_PRODUCT_LIMIT"] = "Le nombre maximum de produits du catalogue CRM est dépassé. Il y a actuellement #COUNT# produits (pour un maximum de #LIMIT#).";
