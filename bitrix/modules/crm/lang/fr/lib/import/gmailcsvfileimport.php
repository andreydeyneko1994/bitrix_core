<?php
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Aucun des champs suivants : #FIELD_LIST# n'a pas été retrouvé";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Pour importer avec succès il faut effectuer des exigences suivantes. Codage du fichier : UTF-16. Langue pour les noms des champs : anglais. Séparateur des champs : virgule.";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS_NEW"] = "Votre fichier doit respecter les exigences suivantes pour être importé : encodage : UTF-8 ; langue du nom de champ : anglais ; séparateur de champ : virgule.";
