<?php
$MESS["CRM_ACCESS_NOTIFY_MESSAGE"] = "Je vous prie de bien vouloir <a href=\"#URL#\">configurer les étapes kanban du CRM</a> pour moi, ou de me donner l'autorisation requise afin de pouvoir le faire moi-même.";
$MESS["CRM_KANBAN_BOOLEAN_0"] = "Non";
$MESS["CRM_KANBAN_BOOLEAN_1"] = "Oui";
$MESS["CRM_KANBAN_CHAR_N"] = "Non";
$MESS["CRM_KANBAN_CHAR_Y"] = "Oui";
$MESS["CRM_KANBAN_COMPANY"] = "Entreprise";
$MESS["CRM_KANBAN_CONTACT"] = "Contact";
$MESS["CRM_KANBAN_CRM_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_KANBAN_DELETE_CANCEL"] = "Annuler";
$MESS["CRM_KANBAN_DELETE_RESTORE_SUCCESS"] = "Les éléments ont bien été récupérés.";
$MESS["CRM_KANBAN_DELETE_SUCCESS"] = "L'élément #ELEMENT_NAME# a été supprimé.";
$MESS["CRM_KANBAN_DELETE_SUCCESS_MULTIPLE"] = "Les éléments ont été supprimés.";
$MESS["CRM_KANBAN_DELIVERY_STAGE"] = "Statut de livraison";
$MESS["CRM_KANBAN_EMAIL_TYPE_HOME"] = "Privé";
$MESS["CRM_KANBAN_EMAIL_TYPE_OTHER"] = "Autre";
$MESS["CRM_KANBAN_EMAIL_TYPE_WORK"] = "Travail";
$MESS["CRM_KANBAN_ERROR_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_KANBAN_ERROR_DEAL_STAGE_MISMATCH"] = "Impossible de changer l'étape de la transaction parce qu'un pipeline de transaction a été modifié.";
$MESS["CRM_KANBAN_ERROR_LEAD_ALREADY_CONVERTED"] = "Impossible de modifier le statut d'un prospect converti.";
$MESS["CRM_KANBAN_ERROR_SESSION_EXPIRED"] = "Votre session a expiré.";
$MESS["CRM_KANBAN_ERROR_STAGE_IS_NOT_EMPTY"] = "Cette étape n'est pas vide.";
$MESS["CRM_KANBAN_ERROR_STAGE_IS_SYSTEM"] = "L'étape système ne peut être supprimée.";
$MESS["CRM_KANBAN_ERROR_STAGE_IS_WIN"] = "L'étape finale ne peut pas être supprimée.";
$MESS["CRM_KANBAN_FIELD"] = "Observateurs";
$MESS["CRM_KANBAN_HEADER_OTHER_FIELDS"] = "Autre";
$MESS["CRM_KANBAN_ORDER_TITLE"] = "Commande ##ACCOUNT_NUMBER#";
$MESS["CRM_KANBAN_PHONE_TYPE_FAX"] = "Fax";
$MESS["CRM_KANBAN_PHONE_TYPE_HOME"] = "Fixe";
$MESS["CRM_KANBAN_PHONE_TYPE_MOBILE"] = "Portable";
$MESS["CRM_KANBAN_PHONE_TYPE_OTHER"] = "Autre";
$MESS["CRM_KANBAN_PHONE_TYPE_PAGER"] = "Bipeur";
$MESS["CRM_KANBAN_PHONE_TYPE_WORK"] = "Travail";
$MESS["CRM_KANBAN_SELECT_BIRTHDATE"] = "Date de naissance";
$MESS["CRM_KANBAN_SELECT_SOURCE_ID"] = "Source";
$MESS["CRM_KANBAN_SELECT_STATUS_ID"] = "Statut";
$MESS["CRM_KANBAN_SELECT_TYPE_ID"] = "Type";
$MESS["CRM_KANBAN_SYS_STATUS_DELETE"] = "Supprimer";
$MESS["CRM_KANBAN_TITLE2_DEAL"] = "Transactions";
$MESS["CRM_KANBAN_TITLE2_INVOICE"] = "Factures";
$MESS["CRM_KANBAN_TITLE2_LEAD"] = "Prospects";
$MESS["CRM_KANBAN_TITLE2_ORDER"] = "Commandes";
$MESS["CRM_KANBAN_TITLE2_QUOTE"] = "Devis";
$MESS["CRM_KANBAN_TITLE_DEAL"] = "Kanban, transactions";
$MESS["CRM_KANBAN_TITLE_INVOICE"] = "Kanban, factures";
$MESS["CRM_KANBAN_TITLE_LEAD"] = "Kanban, prospects";
$MESS["CRM_KANBAN_TITLE_QUOTE"] = "Kanban, devis";
$MESS["CRM_KANBAN_TRACKING_SOURCE_ID"] = "Sales Intelligence";
