<?php
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Les prospects ont été convertis.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Échec de la conversion : #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Conversions réussies : #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "Sans nom";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# sur #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Convertir des prospects";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "Le mode CRM simple convertira les prospects en transactions. <br/><br/>
Les prospects ouverts seront convertis en transactions et en clients dans ce mode. <br/><br/>
Toutes les règles d'automatisation et les processus d'entreprise configurés pour la création de prospects et de transactions seront automatiquement et immédiatement exécutés pour les entités nouvellement créées.";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Convertir des prospects";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de prospect";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Liste des prospects";
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Analytique des prospects";
$MESS["CRM_ROBOTS_TEXT"] = "En mode CRM simple, Bitrix24 convertit automatiquement les nouveaux prospects en transactions en utilisant des règles d'automatisation.<br/><br/> Vous avez déjà des règles d'automatisation des prospects actuellement actives. L'activation de ce mode supprimera ces règles. <br/><br/>Voulez-vous vraiment activer le mode CRM simple ?";
$MESS["CRM_ROBOTS_TITLE"] = "Mode CRM simple";
$MESS["CRM_TYPE_CANCEL"] = "Annuler";
$MESS["CRM_TYPE_CONTINUE"] = "Continuer";
$MESS["CRM_TYPE_SAVE"] = "Enregistrer";
$MESS["CRM_TYPE_TITLE"] = "Choisissez la façon dont vous souhaitez travailler avec votre CRM";
$MESS["CRM_TYPE_TURN_ON"] = "Activer";
