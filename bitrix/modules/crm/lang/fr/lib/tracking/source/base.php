<?php
$MESS["CRM_TRACKING_SOURCE_BASE_ADS_DESC"] = "Source de publicités %name%";
$MESS["CRM_TRACKING_SOURCE_BASE_DESC_OTHER"] = "L'autre trafic comprend tous les clients qui n'appartiennent à aucune des sources existantes dans Sales Intelligence.";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_FB"] = "Facebook";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_GOOGLE"] = "Google Ads";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_INSTAGRAM"] = "Instagram";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_ORGANIC"] = "Trafic organique";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_OTHER"] = "Autre trafic";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_SENDER-MAIL"] = "E-Mail Marketing";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_VK"] = "VK";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_YANDEX"] = "Yandex.Direct";
$MESS["CRM_TRACKING_SOURCE_BASE_SHORT_NAME_ORGANIC"] = "Organique";
$MESS["CRM_TRACKING_SOURCE_BASE_TRAFFIC_DESC"] = "Source de trafic %name%";
