<?php
$MESS["CRM_WEBFORM_FIELD_SYNCHRONIZER_ERR_RES_BOOK"] = "Le champ de réservation de ressources « %fieldCaption% » ne peut pas être créé automatiquement. Veuillez créer un champ de réservation de ressources dans %entityCaption% et sélectionnez le champ dans le formulaire.";
