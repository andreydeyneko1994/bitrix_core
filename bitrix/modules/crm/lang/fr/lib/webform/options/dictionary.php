<?php
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_HIDE"] = "Cacher";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_SHOW"] = "Montrer le filtre";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_EVENT_CHANGE"] = "Modifications";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY"] = "Aléatoire";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY1"] = "N'est pas vide";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_CONTAIN"] = "Contient";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY"] = "Vide";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY1"] = "Vide";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EQUAL"] = "Égal à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATER"] = "Supérieur à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATEROREQUAL"] = "Supérieur ou égal à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESS"] = "Inférieur à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESSOREQUAL"] = "Inférieur ou égal à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTCONTAIN"] = "Ne contient pas";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTEQUAL"] = "Pas égal à";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_AUDIO"] = "Son";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_IMAGE"] = "Images";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_VIDEO"] = "Vidéo";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_COMPANY_NAME"] = "Nom de l'entreprise";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_LASTNAME"] = "Nom de famille";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_NAME"] = "Prénom";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_PHONE"] = "Téléphone";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_SECOND_NAME"] = "Deuxième prénom";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_BUTTON"] = "Bouton";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_LINK"] = "Lien";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_CLASSIC"] = "Classique";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_ELEGANT"] = "Oblique";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_MODERN"] = "Moderne";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_CENTER"] = "Centre";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_INLINE"] = "Intégré";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_LEFT"] = "Gauche";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_RIGHT"] = "Droite";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_PANEL"] = "Barre latérale";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_POPUP"] = "Popup";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_BOTTOM"] = "Bas";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_TOP"] = "Haut";
