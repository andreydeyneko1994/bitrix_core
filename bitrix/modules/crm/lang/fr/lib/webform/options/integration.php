<?php
$MESS["CRM_WEBFORM_OPTIONS_LINK_EMPTY_FIELD_MAPPING"] = "Aucun champ sélectionné pour la conversion.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_FORM_DUPLICATE_ERROR"] = "Le formulaire est déjà connecté au CRM";
$MESS["CRM_WEBFORM_OPTIONS_LINK_REGISTER_FAILED"] = "Impossible de s'abonner aux évènements du formulaire.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_TYPE_DUPLICATE"] = "Impossible de reconnecter le formulaire au même service";
$MESS["CRM_WEBFORM_OPTIONS_LINK_UNREGISTER_FAILED"] = "Impossible de se désabonner aux évènements du formulaire.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_WRONG_TYPE"] = "Service de liaison de formulaires inconnu";
