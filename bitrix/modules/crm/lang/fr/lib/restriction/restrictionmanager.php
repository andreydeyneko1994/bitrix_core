<?php
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Des Canaux ouverts supplémentaires sont disponibles uniquement dans les abonnements commerciaux.";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class=\"crm-conditionally-required-field-tab-content\">
	<div class=\"crm-conditionally-required-field-tab-text\">
		Les champs obligatoires en fonction de l'étape sont réservés aux <a href=\"/settings/license_all.php\" target=\"_blank\">offres commerciales sélectionnées</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class=\"crm-deal-category-tab-content\">
	<div class=\"crm-deal-category-tab-text\">
		L'offre que vous utilisez actuellement limite le nombre de pipelines de transaction que vous pouvez utiliser. Veuillez passer à une <a href=\"/settings/license_all.php\" target=\"_blank\">offre commerciale sélectionnée</a> pour créer des pipelines impliquant plusieurs produits et stratégies de vente afin d'analyser diverses voies commerciales.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Pipelines CRM multiples";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "Laissez Bitrix24 créer des transactions pour vous ! 
Les transactions récurrentes vous feront souvent gagner du temps et des ressources. Par exemple, si vous proposez des contrats hebdomadaires pendant toute une année, les transactions récurrentes vont vous sauver.
Créez une transaction récurrente, et définissez le programme et un pipeline. Les nouvelles transactions seront créées à l'heure spécifiée sans que vous ayez besoin de bouger le petit doigt.
		<ul class=\"hide-features-list\">
			<li class=\"hide-features-list-item\">Création automatique de transactions</li>
			<li class=\"hide-features-list-item\">Amélioration des performances et de l'efficacité de vos employés</li>
			<li class=\"hide-features-list-item\">Accès rapide aux transactions récurrentes en cours- <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Plus d'informations</a></li>
		</ul>		
		<strong>Les transactions récurrentes sont disponibles avec les offres commerciales sélectionnées</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "Les transactions récurrentes sont disponibles avec les offres commerciales";
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Recherche de doublons avancée</h3>
<div class=\"crm-duplicate-tab-text\">
Quand un nouveau contact est créé (ou un prospect, ou une société), Bitrix24 trouve et affiche les doublons potentiels, ce qui peut permettre d'empêcher leur création.
</div>
<div class=\"crm-duplicate-tab-text\">
Avec la recherche de doublons avancée, le CRM peut également trouver les doublons logés dans les données importées et les données déjà saisies dans la base de données. Ces instances sont alors fusionnées. 
</div>
<div class=\"crm-duplicate-tab-text\">
Ajoutez cette puissante fonctionnalités, et d'autres encore, à votre Bitrix24 ! La téléphonie avancée, le CRM avancée et d'autres fonctionnalités utiles sont disponibles avec les offres commerciales sélectionnées. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Plus d'informations</a>
</div>
<div class=\"ui-btn-container ui-btn-container-center\">
<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Profitez d'une offre étendue</span>
<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Accédez à une version d'essai pendant 30 jours</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">La modification d'historique de CRM n'est disponible qu'avec le CRM avancé</h3>
	<div class=\"crm-history-tab-text\">
		Bitrix24 conserve un journal détaillé de toutes les modifications apportées au CRM. Quand vous passez en CRM avancé, vous pouvez accéder à l'historique de ces modifications (par ex. : qui a accédé à ou modifié une entrée du CRM) et restaurer des valeurs antérieures, si nécessaire.
	</div>
	<div class=\"crm-history-tab-text\">Voici à quoi cela ressemble : </div>
	<img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	<div class=\"crm-history-tab-text\">
		Ajoutez cette puissante fonctionnalités, et d'autres encore, à votre Bitrix24 ! La téléphonie avancée, le CRM avancé et d'autres fonctionnalités utiles sont disponibles avec les offres commerciales sélectionnées. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Plus d'informations</a>
	</div>
	<div class=\"ui-btn-container ui-btn-container-center\">
		<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Profitez d'une offre étendue</span>
		<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Accédez à une version d'essai pendant 30 jours</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "Utilisez la facturation automatique pour facturer régulièrement vos clients et gagner du temps.
	Créez une facture et indiquez la fréquence à laquelle vous souhaitez l'envoyer. Le système va alors en créer une nouvelle et l'envoyer au client selon la fréquence choisie.
		<ul class=\"hide-features-list\">
			<li class=\"hide-features-list-item\">Gagnez du temps grâce à la facturation automatique</li>
			<li class=\"hide-features-list-item\">Accédez rapidement à tous les modèles de facturation automatique</li>
			<li class=\"hide-features-list-item\">Envoyez une facture à l'adresse e-mail du client <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Plus d'informations</a></li>
		</ul>
		<strong>La facturation automatique est disponible avec les offres commerciales sélectionnées.</strong>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "La facturation automatique est disponible avec les offres commerciales.";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
 		L'offre gratuite attribue la même autorisation d'accès à tous les employés. Envisagez de passer à l'une des offres commerciales pour attribuer différents rôles, actions et données à divers utilisateurs.	
		Pour en savoir plus sur les différentes offres, veuillez consulter la <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">page de comparaison des offres</a> sur le site.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		Avec votre offre actuelle, tous les employés partagent des autorisations unifiées. Passez à l'une des principales offres pour définir les rôles, attribuer les autorisations et spécifier les données auxquelles vos employés peuvent accéder.
		<a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">Apprenez-en plus sur les offres et comparez les caractéristiques incluses</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "Pour attribuer à vos employés des autorisations d'accès différentes, passez sur l'une des offres commerciales.";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "Ajoutez les fonctionnalités suivantes à votre CRM :
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversions entre transactions, factures et devis</li>
<li class=\"hide-features-list-item\">Recherche de doubles avancée</li>
<li class=\"hide-features-list-item\">Récupération et annulation des modifications apportées aux entrées CRM via l'historique</li>
<li class=\"hide-features-list-item\">Journal d'accès CRM</li>
<li class=\"hide-features-list-item\">Consultation de plus de 5000 enregistrements dans le CRM</li>
<li class=\"hide-features-list-item\">Appels basés sur une liste<sup class=\"hide-features-soon\">bientôt</sup></li>
<li class=\"hide-features-list-item\">Envoi de groupes d'e-mails aux clients<sup class=\"hide-features-soon\">bientôt</sup></li>
<li class=\"hide-features-list-item\">Prise en charge de la vente de services<sup class=\"hide-features-soon\">bientôt</sup>
<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Découvrez-en plus</a>
</li>
</ul>
<strong>Téléphonie avancée + CRM avancé et d'autres fonctionnalités utiles sont disponibles avec les offres commerciales sélectionnées.</strong>
";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM avancé dans Bitrix24";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Ventes automatisées du CRM Bitrix24<br><br>
	Créez des scénarios pour CRM afin de faire progresser vos transactions : assignez des tâches, prévoyez des réunions, lancez des campagnes publicitaires ciblées et créez des factures. Les règles d'automatisation guideront votre gestionnaire aux prochaines étapes de chaque transaction afin de finaliser le travail.<br><br>
	Les déclencheurs réagissent automatiquement aux actions du client (quand le site est consulté, quand un formulaire est rempli, quand un commentaire de réseau social est publié ou quand un appel est passé) et lancent une règle d'automatisation qui aide à transformer le prospect en transaction.<br><br>
	Les règles d'automatisation et les déclencheurs s'occupent de toutes les opérations routinières sans formation pour vos responsables : configurez une fois et c'est bon !<br><br>
	Ajoutez des règles d'automatisation et améliorez vos ventes !<br><br>
	Cet outil est réservé aux abonnements commerciaux Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Règles d'automatisation";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "La signature peut être enlevée uniquement dans les abonnements commerciaux.";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "Formulaires CRM étendus";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Votre abonnements actuel limite le nombre de formulaire CRM que vous pouvez avoir. Pour pouvoir ajouter plus de formulaires, veuillez changer d'abonnement. 
<br><br>
ASTUCE : les offres commerciales sélectionnées proposent un nombre illimité de formulaires CRM.
<br><br>
Essayez Bitrix24 Professional gratuitement pendant 30 jours.";
