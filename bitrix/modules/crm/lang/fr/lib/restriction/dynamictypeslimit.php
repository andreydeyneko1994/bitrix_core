<?php
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_CREATE_RESTRICTED"] = "Vous ne pouvez pas créer de nouvelle Automatisation intelligente de processus en raison des restrictions de l'offre que vous utilisez";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_CREATE_RESTRICTED"] = "Vous ne pouvez pas créer de nouvel article en raison des restrictions de l'offre que vous utilisez";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_UPDATE_RESTRICTED"] = "Vous ne pouvez pas modifier d'article en raison des restrictions de l'offre que vous utilisez";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_UPDATE_RESTRICTED"] = "Vous ne pouvez pas modifier les paramètres d'Automatisation intelligente de processus en raison des restrictions de l'offre que vous utilisez";
