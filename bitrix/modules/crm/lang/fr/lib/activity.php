<?
$MESS["CRM_ACTIVITY_ENTITY_ACTIVITY_ID_FIELD"] = "Activité";
$MESS["CRM_ACTIVITY_ENTITY_ASSIGNED_BY_FIELD"] = "Responsable";
$MESS["CRM_ACTIVITY_ENTITY_ASSOCIATED_ENTITY_ID_FIELD"] = "ID d'entité associée";
$MESS["CRM_ACTIVITY_ENTITY_AUTHOR_BY_FIELD"] = "Créé par";
$MESS["CRM_ACTIVITY_ENTITY_AUTHOR_ID_FIELD"] = "Créé par";
$MESS["CRM_ACTIVITY_ENTITY_AUTOCOMPLETE_RULE_FIELD"] = "Saisie automatique";
$MESS["CRM_ACTIVITY_ENTITY_BINDINGS_FIELD"] = "Fixations";
$MESS["CRM_ACTIVITY_ENTITY_COMPLETED_FIELD"] = "Achevé(e)s";
$MESS["CRM_ACTIVITY_ENTITY_CREATED_FIELD"] = "Créé le";
$MESS["CRM_ACTIVITY_ENTITY_DATE_CREATED_SHORT_FIELD"] = "Créé le";
$MESS["CRM_ACTIVITY_ENTITY_DATE_FINISHED_SHORT_FIELD"] = "Date de clôture";
$MESS["CRM_ACTIVITY_ENTITY_DEADLINE_FIELD"] = "Date limite";
$MESS["CRM_ACTIVITY_ENTITY_DESCRIPTION_FIELD"] = "Description";
$MESS["CRM_ACTIVITY_ENTITY_DESCRIPTION_TYPE_FIELD"] = "Type de description";
$MESS["CRM_ACTIVITY_ENTITY_DIRECTION_FIELD"] = "Direction";
$MESS["CRM_ACTIVITY_ENTITY_EDITOR_BY_FIELD"] = "Modifié(e)s par";
$MESS["CRM_ACTIVITY_ENTITY_EDITOR_ID_FIELD"] = "Modifié(e)s par";
$MESS["CRM_ACTIVITY_ENTITY_END_TIME_FIELD"] = "Date limite";
$MESS["CRM_ACTIVITY_ENTITY_END_TIME_SHORT_FIELD"] = "Date limite";
$MESS["CRM_ACTIVITY_ENTITY_ENTITY_ID_FIELD"] = "ID d'entité";
$MESS["CRM_ACTIVITY_ENTITY_ENTITY_TYPE_ID_FIELD"] = "Type d'entité";
$MESS["CRM_ACTIVITY_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_FIELD"] = "Appel";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_IN_FIELD"] = "Appel reçu";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_OUT_FIELD"] = "Appel sortant";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_FIELD"] = "E-mail";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_IN_FIELD"] = "Lettre reçue";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_OUT_FIELD"] = "Adresse email sortant";
$MESS["CRM_ACTIVITY_ENTITY_IS_MEETING_FIELD"] = "Rencontre";
$MESS["CRM_ACTIVITY_ENTITY_IS_TASK_FIELD"] = "La tâche";
$MESS["CRM_ACTIVITY_ENTITY_LAST_UPDATED_FIELD"] = "Date de modification";
$MESS["CRM_ACTIVITY_ENTITY_LAST_UPDATED_SHORT_FIELD"] = "Date de modification";
$MESS["CRM_ACTIVITY_ENTITY_LOCATION_FIELD"] = "Emplacement";
$MESS["CRM_ACTIVITY_ENTITY_NOTIFY_TYPE_FIELD"] = "Type de notification";
$MESS["CRM_ACTIVITY_ENTITY_NOTIFY_VALUE_FIELD"] = "Paramètre des notifications";
$MESS["CRM_ACTIVITY_ENTITY_ORIGINATOR_ID_FIELD"] = "Source externe";
$MESS["CRM_ACTIVITY_ENTITY_ORIGIN_ID_FIELD"] = "ID externe";
$MESS["CRM_ACTIVITY_ENTITY_OWNER_ID_FIELD"] = "ID du propriétaire";
$MESS["CRM_ACTIVITY_ENTITY_OWNER_TYPE_ID_FIELD"] = "Type de propriétaire";
$MESS["CRM_ACTIVITY_ENTITY_PRIORITY_FIELD"] = "Criticité";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_DATA_FIELD"] = "Fournisseur de données";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_GROUP_ID_FIELD"] = "Type de connecteur";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_ID_FIELD"] = "ID du fournisseur";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_PARAMS_FIELD"] = "Paramètres du fournisseur";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_TYPE_ID_FIELD"] = "Type de fournisseur";
$MESS["CRM_ACTIVITY_ENTITY_RESPONSIBLE_ID_FIELD"] = "Responsable";
$MESS["CRM_ACTIVITY_ENTITY_SETTINGS_FIELD"] = "Paramètres";
$MESS["CRM_ACTIVITY_ENTITY_START_TIME_FIELD"] = "A partir de la date";
$MESS["CRM_ACTIVITY_ENTITY_START_TIME_SHORT_FIELD"] = "Commencer";
$MESS["CRM_ACTIVITY_ENTITY_STATUS_FIELD"] = "Statut";
$MESS["CRM_ACTIVITY_ENTITY_SUBJECT_FIELD"] = "En-tête";
$MESS["CRM_ACTIVITY_ENTITY_TYPE_FIELD"] = "Type";
$MESS["CRM_ACTIVITY_ENTITY_TYPE_ID_FIELD"] = "Type";
$MESS["CRM_ACTIVITY_ENTITY_VALUE_FIELD"] = "Valeur";
?>