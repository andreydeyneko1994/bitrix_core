<?
$MESS["CRM_CH_TRACKER_WGT_ACTIVITY_DYNAMIC"] = "Aperçu de la communication";
$MESS["CRM_CH_TRACKER_WGT_AMOUNT_OF_SALE"] = "Évaluation par valeur des transactions conclues";
$MESS["CRM_CH_TRACKER_WGT_COMMUNICATION_DYNAMIC"] = "Charge de communication ";
$MESS["CRM_CH_TRACKER_WGT_COMMUNICATION_DYNAMIC_EMAIL"] = "E-mail";
$MESS["CRM_CH_TRACKER_WGT_COMMUNICATION_DYNAMIC_FORM"] = "Formulaires CRM";
$MESS["CRM_CH_TRACKER_WGT_COMMUNICATION_DYNAMIC_OPENLINES"] = "Messageries et services sociaux";
$MESS["CRM_CH_TRACKER_WGT_COMMUNICATION_DYNAMIC_TELEPHONY"] = "Téléphonie";
$MESS["CRM_CH_TRACKER_WGT_DEAL_PROCESS_SUM"] = "Total des ventes";
$MESS["CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM"] = "Valeur des transactions conclues";
$MESS["CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM_PER_DAY"] = "Ventes aujourd'hui";
$MESS["CRM_CH_TRACKER_WGT_DEAL_SUCCESS_SUM_PER_MONTH"] = "Ventes du mois";
$MESS["CRM_CH_TRACKER_WGT_MANAGERS_FAILS"] = "Compteurs du manager";
$MESS["CRM_CH_TRACKER_WGT_MANAGERS_SUCCESSES"] = "Ventes du manager";
$MESS["CRM_CH_TRACKER_WGT_NEW_CLIENTS"] = "Nouveaux clients";
$MESS["CRM_CH_TRACKER_WGT_NEW_CLIENTS_PER_DAY"] = "Nouveaux clients aujourd'hui";
$MESS["CRM_CH_TRACKER_WGT_PERSONAL_LOAD"] = "Charge de travail du manager";
$MESS["CRM_CH_TRACKER_WGT_PERSONAL_LOAD_EMAIL"] = "E-mail";
$MESS["CRM_CH_TRACKER_WGT_PERSONAL_LOAD_FORM"] = "Formulaires CRM";
$MESS["CRM_CH_TRACKER_WGT_PERSONAL_LOAD_OPENLINES"] = "Messageries et services sociaux";
$MESS["CRM_CH_TRACKER_WGT_PERSONAL_LOAD_TELEPHONY"] = "Téléphonie";
$MESS["CRM_CH_TRACKER_WGT_RATING_BY_SUCCESSFUL_DEALS"] = "Évaluation par valeur des transactions conclues";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET_NAME"] = "Objectif des ventes";
?>