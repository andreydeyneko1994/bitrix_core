<?php
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL"] = "#QTY# correspondances par la valeur #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL_EXCEEDED"] = "Plus de #QTY# correspondances par la valeur #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_SUMMARY"] = "Correspondent par la valeur \"#VALUE#\" de \"#FIELD#\"";
