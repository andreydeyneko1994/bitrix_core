<?
$MESS["CRM_DUP_CRITERION_RQ_ENTITY_TOTAL"] = "#QTY# par mentions d'entreprise \"#TITLE#\" avec la valeur #DESCR#";
$MESS["CRM_DUP_CRITERION_RQ_ENTITY_TOTAL_EXCEEDED"] = "Plus de #QTY# par mentions d'entreprise \"#TITLE#\" avec la valeur #DESCR#";
$MESS["CRM_DUP_CRITERION_RQ_SUMMARY"] = "Les mentions \"#TITLE#\" correspondent à #DESCR#";
?>