<?
$MESS["CRM_DUP_CRITERION_BD_ENTITY_TOTAL"] = "#QTY# par coordonnées bancaires \"#TITLE#\" avec la valeur #DESCR#";
$MESS["CRM_DUP_CRITERION_BD_ENTITY_TOTAL_EXCEEDED"] = "Plus de #QTY# par coordonnées bancaires \"#TITLE#\" avec la valeur #DESCR#";
$MESS["CRM_DUP_CRITERION_BD_SUMMARY"] = "Les coordonnées bancaires \"#TITLE#\" correspondent à #DESCR#";
?>