<?
$MESS["CRM_INVOICE_ST_UTS_ENTITY_COMPANY_BY_FIELD"] = "Entreprise";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_CONTACT_BY_FIELD"] = "Contact";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_DEAL_BY_FIELD"] = "Transaction";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_UF_COMPANY_ID_FIELD"] = "Entreprise";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_UF_CONTACT_ID_FIELD"] = "Contact";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_UF_DEAL_ID_FIELD"] = "Transaction";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_UF_MYCOMPANY_ID_FIELD"] = "Mentions de ma société";
$MESS["CRM_INVOICE_ST_UTS_ENTITY_VALUE_ID_FIELD"] = "ID du facture";
?>