<?
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "Impossible d'ajouter une règle d'automatisation au flux de travail";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "Impossible de créer un funnel dans l'entonnoir";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_TITLE"] = "Copier la transaction";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "La création de funnels n'est pas disponible dans votre plan";
?>