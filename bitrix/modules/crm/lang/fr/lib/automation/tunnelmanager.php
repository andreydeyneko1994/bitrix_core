<?php
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "Impossible d'ajouter une règle d'automatisation au flux de travail";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "Impossible de créer un tunnel dans l'entonnoir";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DEAL_TITLE"] = "Copier la transaction";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DYNAMIC_TITLE"] = "Copier la SPA";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DEAL_TITLE"] = "Déplacer la transaction";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DYNAMIC_TITLE"] = "Déplacer la SPA";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "La création de tunnel est indisponible avec votre offre";
