<?php
$MESS["CRM_ORDER_STATUS_FINISHED"] = "Réalisée";
$MESS["CRM_ORDER_STATUS_INITIAL"] = "Commande acceptée, en attente du paiement";
$MESS["CRM_ORDER_STATUS_PAID"] = "Payée, préparée pour la livraison";
$MESS["CRM_ORDER_STATUS_REFUSED"] = "Annulée";
$MESS["CRM_ORDER_STATUS_SEND"] = "Envoyée";
