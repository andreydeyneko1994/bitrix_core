<?
$MESS["CRM_BP_SHOP_10000"] = "Una negociación se ha creado en la tienda online. Hay que ponerse en contacto con los clientes dentro de una hora. La negociación es por más de 10000 {=Document:ACCOUNT_CURRENCY_ID}.";
$MESS["CRM_BP_SHOP_10000_1"] = "Una nueva negociación ha sido creada en la tienda online.";
$MESS["CRM_BP_SHOP_PBP"] = "Proceso de negocio secuencial";
$MESS["CRM_BP_SHOP_SS"] = "Mensaje de red social";
$MESS["CRM_BP_SHOP_TITLE"] = "Negociaciones en la tienda online";
$MESS["CRM_BP_SHOP_US"] = "Condición";
$MESS["CRM_BP_SHOP_US_10000"] = "10000";
?>