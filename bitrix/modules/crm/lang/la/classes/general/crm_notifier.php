<?php
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Hay nuevos mensajes de correo electrónico";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Devolución de llamada solicitada";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_ASSIGNED_BY"] = "Se le designó como persona responsable";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_STAGE"] = "Etapa cambiada";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Le mencionaron en una publicación";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Fue especificado como destinatario de una publicación";
$MESS["CRM_NOTIFY_SCHEME_MERGE"] = "Notificaciones de control duplicadas";
$MESS["CRM_NOTIFY_SCHEME_OTHER"] = "Otras notificaciones";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulario del CRM enviado";
