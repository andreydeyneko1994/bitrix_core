<?
$MESS["CRM_ADD_MESSAGE"] = "nuevo mensaje de CRM";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "Uno o más empleados desactivados existen en la cola del buzón de distribución de correo electrónico \"#EMAIL#\". Estos empleados no recibirán nuevos correos electrónicos, pero todas las cadenas de correos ya existentes, seguirán activas y enviadas a estos empleados. <a href=\"#CONFIG_URL#\">Configurar parámetros de la cola</a>.";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Archivos no disponibles (tamaño máximo excedido: %MAX_SIZE% MB)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Añadir al cuerpo del mensaje";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "No agregar";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Agregar el tema del mensaje";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(sin asunto)";
$MESS["CRM_EMAIL_EMAILS"] = "E-Mail";
$MESS["CRM_EMAIL_FROM"] = "De";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Enviar&Guardar Mensaje";
$MESS["CRM_EMAIL_SUBJECT"] = "Título";
$MESS["CRM_EMAIL_TO"] = "Para";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Nombre de la Compañía: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "creado desde el mensaje del REMITENTE que no puede relacionarse con cualquier enlace existente, contacto o compañías.";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Lo principal del mesnsaje es el REMITENTE ";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Mensaje de prospecto reenviado desde %SENDER%";
?>