<?
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupo de usuarios autorizado para editar las preferencias de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administradores de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupo de usuarios autorizado para utilizar las funciones de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Personal de la tienda online.";
?>