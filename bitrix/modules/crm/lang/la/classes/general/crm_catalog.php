<?
$MESS["CRM_CATALOG_FIELD_ID"] = "ID";
$MESS["CRM_ERR_REGISTER_CATALOG"] = "Se ha producido un error al registrar el catálogo comercial.";
$MESS["CRM_ERR_REGISTER_OFFERS"] = "Error al registrar un block de información para cotizaciones";
$MESS["CRM_PRODUCT_CATALOG_OFFERS_TITLE_FORMAT"] = "#CATALOG# (cotizaciones)";
$MESS["CRM_PRODUCT_CATALOG_PRODUCT_NAME"] = "Productos";
$MESS["CRM_PRODUCT_CATALOG_SECTION_NAME"] = "Categoria";
$MESS["CRM_PRODUCT_CATALOG_TITLE"] = "Catalogo de productos CRM";
$MESS["CRM_PRODUCT_CATALOG_TYPE_TITLE"] = "Catálogos de CRM";
?>