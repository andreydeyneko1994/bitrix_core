<?
$MESS["CRM_ACTIVITY_ENTITY_ACTIVITY_ID_FIELD"] = "Actividad";
$MESS["CRM_ACTIVITY_ENTITY_ASSIGNED_BY_FIELD"] = "Persona responsable";
$MESS["CRM_ACTIVITY_ENTITY_ASSOCIATED_ENTITY_ID_FIELD"] = "ID de la entidad asociada";
$MESS["CRM_ACTIVITY_ENTITY_AUTHOR_BY_FIELD"] = "Creado por";
$MESS["CRM_ACTIVITY_ENTITY_AUTHOR_ID_FIELD"] = "Creado por";
$MESS["CRM_ACTIVITY_ENTITY_AUTOCOMPLETE_RULE_FIELD"] = "Autocompletar";
$MESS["CRM_ACTIVITY_ENTITY_BINDINGS_FIELD"] = "Vinculaciones";
$MESS["CRM_ACTIVITY_ENTITY_COMPLETED_FIELD"] = "Completado";
$MESS["CRM_ACTIVITY_ENTITY_CREATED_FIELD"] = "Creado el";
$MESS["CRM_ACTIVITY_ENTITY_DATE_CREATED_SHORT_FIELD"] = "Creado el";
$MESS["CRM_ACTIVITY_ENTITY_DATE_FINISHED_SHORT_FIELD"] = "Fecha de finalización";
$MESS["CRM_ACTIVITY_ENTITY_DEADLINE_FIELD"] = "Fecha límite";
$MESS["CRM_ACTIVITY_ENTITY_DESCRIPTION_FIELD"] = "Descripción";
$MESS["CRM_ACTIVITY_ENTITY_DESCRIPTION_TYPE_FIELD"] = "Tipo de descripción";
$MESS["CRM_ACTIVITY_ENTITY_DIRECTION_FIELD"] = "Dirección";
$MESS["CRM_ACTIVITY_ENTITY_EDITOR_BY_FIELD"] = "Modificado por";
$MESS["CRM_ACTIVITY_ENTITY_EDITOR_ID_FIELD"] = "Modificado por";
$MESS["CRM_ACTIVITY_ENTITY_END_TIME_FIELD"] = "Declinar";
$MESS["CRM_ACTIVITY_ENTITY_END_TIME_SHORT_FIELD"] = "Declinar";
$MESS["CRM_ACTIVITY_ENTITY_ENTITY_ID_FIELD"] = "ID de la entidad";
$MESS["CRM_ACTIVITY_ENTITY_ENTITY_TYPE_ID_FIELD"] = "Tipo de entidad";
$MESS["CRM_ACTIVITY_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_FIELD"] = "Llamada";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_IN_FIELD"] = "Llamada entrante";
$MESS["CRM_ACTIVITY_ENTITY_IS_CALL_OUT_FIELD"] = "Llamada saliente";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_FIELD"] = "Correo electrónico";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_IN_FIELD"] = "Correo electrónico entrante";
$MESS["CRM_ACTIVITY_ENTITY_IS_EMAIL_OUT_FIELD"] = "Correo electrónico saliente";
$MESS["CRM_ACTIVITY_ENTITY_IS_MEETING_FIELD"] = "Reunión";
$MESS["CRM_ACTIVITY_ENTITY_IS_TASK_FIELD"] = "Tarea";
$MESS["CRM_ACTIVITY_ENTITY_LAST_UPDATED_FIELD"] = "Modificado el ";
$MESS["CRM_ACTIVITY_ENTITY_LAST_UPDATED_SHORT_FIELD"] = "Modificado el ";
$MESS["CRM_ACTIVITY_ENTITY_LOCATION_FIELD"] = "Ubicación";
$MESS["CRM_ACTIVITY_ENTITY_NOTIFY_TYPE_FIELD"] = "Tipo de notificación";
$MESS["CRM_ACTIVITY_ENTITY_NOTIFY_VALUE_FIELD"] = "Parámetro de notificación";
$MESS["CRM_ACTIVITY_ENTITY_ORIGINATOR_ID_FIELD"] = "Fuente externa";
$MESS["CRM_ACTIVITY_ENTITY_ORIGIN_ID_FIELD"] = "ID externo";
$MESS["CRM_ACTIVITY_ENTITY_OWNER_ID_FIELD"] = "ID del propietario";
$MESS["CRM_ACTIVITY_ENTITY_OWNER_TYPE_ID_FIELD"] = "Tipo de propietario";
$MESS["CRM_ACTIVITY_ENTITY_PRIORITY_FIELD"] = "Prioridad";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_DATA_FIELD"] = "Datos del proveedor";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_GROUP_ID_FIELD"] = "Tipo de conector";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_ID_FIELD"] = "ID del proveedor";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_PARAMS_FIELD"] = "Parámetros del proveedor";
$MESS["CRM_ACTIVITY_ENTITY_PROVIDER_TYPE_ID_FIELD"] = "Tipo de proveedor";
$MESS["CRM_ACTIVITY_ENTITY_RESPONSIBLE_ID_FIELD"] = "Persona responsable";
$MESS["CRM_ACTIVITY_ENTITY_SETTINGS_FIELD"] = "Ajustes";
$MESS["CRM_ACTIVITY_ENTITY_START_TIME_FIELD"] = "Fecha de incio";
$MESS["CRM_ACTIVITY_ENTITY_START_TIME_SHORT_FIELD"] = "Inicio";
$MESS["CRM_ACTIVITY_ENTITY_STATUS_FIELD"] = "Estados";
$MESS["CRM_ACTIVITY_ENTITY_SUBJECT_FIELD"] = "Asunto";
$MESS["CRM_ACTIVITY_ENTITY_TYPE_FIELD"] = "Tipo";
$MESS["CRM_ACTIVITY_ENTITY_TYPE_ID_FIELD"] = "Tipo";
$MESS["CRM_ACTIVITY_ENTITY_VALUE_FIELD"] = "Valor";
?>