<?
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Error al activar el pedido porque se alcanzó la última fecha de factura recurrente. Es posible que desee cambiar la fecha.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Error al activar el pedidoç porque se superó el recuento máximo de repetición. Puede que desee aumentar el valor máximo.";
$MESS["CRM_RECUR_WRONG_ID"] = "ID de la cuenta inválido";
?>