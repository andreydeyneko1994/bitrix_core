<?
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_OCCURRED"] = "No se puede fusionar automáticamente debido a un conflicto de datos.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_RESOLUTION_NOT_SUPPORTED"] = "El método de resolución de conflictos \"#RESOLUTION_CAPTION#\" no es compatible.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_DENIED"] = "No hay acceso de eliminación a \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_FAILED"] = "No se puede eliminar \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_ERROR"] = "Se produjo un error al fusionar. Mensaje de error: #ERROR#";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_NOT_FOUND"] = "No se puede encontrar el elemento [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_READ_DENIED"] = "No hay acceso de lectura a \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_DENIED"] = "No hay acceso de actualización a \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_FAILED"] = "No se puede guardar \"#TITLE#\" [#ID#].";
?>