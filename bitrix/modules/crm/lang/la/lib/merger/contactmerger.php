<?
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_PERMISSION"] = "#USER_NAME# ha fusionado la compañía \"#SEED_TITLE#\" [#SEED_ID#] con \"#TARG_TITLE#\" [#TARG_ID#] que no puede ver debido a las preferencias de permisos de acceso.";
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "#USER_NAME# ha fusionado la compañía \"#SEED_TITLE#\" [#SEED_ID#] con \"#TARG_TITLE#\" [#TARG_ID#] que no puede ver o editar debido a las preferencias de permisos de acceso.";
$MESS["CRM_CONTACT_MERGER_COLLISION_UPDATE_PERMISSION"] = "#USER_NAME# ha fusionado la compañía \"#SEED_TITLE#\" [#SEED_ID#] con \"#TARG_TITLE#\" [#TARG_ID#] que no puede editar debido a las preferencias de permisos de acceso.";
?>