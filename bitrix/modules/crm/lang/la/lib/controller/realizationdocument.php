<?php
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ACCESS_DENIED"] = "Permisos insuficientes para efectuar la operación.";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_DELETE_DEDUCTED_ERROR"] = "El \"pedido de venta ##ID#\" no se puede eliminar porque ya fue procesado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_NOT_USED_INVENTORY_MANAGEMENT"] = "Debe habilitar la administración del inventario para procesar el objeto del inventario";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ORDER_NOT_FOUND_ERROR"] = "No se encontró el pedido";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_PRODUCT_NOT_FOUND"] = "Introduzca por lo menos un producto";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIPMENT_NOT_FOUND_ERROR"] = "No se encontró el envío ##ID#";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIP_DEDUCTED_ERROR"] = "Error al procesar el \"Pedido de venta ##ID#\": Ya fue procesado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_UNSHIP_UNDEDUCTED_ERROR"] = "Error al cancelar el\"Pedido de venta ##ID#\": No se ha procesado.";
