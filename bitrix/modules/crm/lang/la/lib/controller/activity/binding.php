<?php
$MESS["CRM_ACTIVITY_BINDING_ALREADY_BOUND_ERROR"] = "La actividad ya está vinculada a esta entidad";
$MESS["CRM_ACTIVITY_BINDING_LAST_BINDING_ERROR"] = "No se puede eliminar el único enlace actividad-entidad.";
$MESS["CRM_ACTIVITY_BINDING_NOT_BOUND_ERROR"] = "La actividad no está vinculada a esta entidad";
