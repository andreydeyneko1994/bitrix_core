<?php
$MESS["CRM_COMPONENT_FACTORYBASED_COPY_PAGE_URL"] = "Copiar el enlace del elemento en el Portapapeles";
$MESS["CRM_COMPONENT_FACTORYBASED_EDITOR_MAIN_SECTION_TITLE"] = "Acerca del elemento";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TEXT"] = "De manera predeterminada, el importe del elemento se calcula como la suma de los precios de los productos. Sin embargo, el elemento ya especifica una cantidad diferente. Puede dejar el valor actual sin cambios o confirmar el recálculo.";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TITLE"] = "Cambiar el modo de calcular el importe del elemento";
$MESS["CRM_COMPONENT_FACTORYBASED_NEW_ITEM_TITLE"] = "Crear #ENTITY_NAME#";
$MESS["CRM_COMPONENT_FACTORYBASED_PAGE_URL_COPIED"] = "El enlace del elemento se copió en el Portapapeles";
$MESS["CRM_COMPONENT_FACTORYBASED_TIMELINE_HISTORY_STUB"] = "Está creando un elemento...";
