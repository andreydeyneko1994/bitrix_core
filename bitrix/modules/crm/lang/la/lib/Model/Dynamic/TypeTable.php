<?php
$MESS["CRM_TYPE_TABLE_DELETE_ERROR_ITEMS"] = "No se puede eliminar el tipo de entidad porque aún hay elementos de este tipo.";
$MESS["CRM_TYPE_TABLE_DISABLING_CATEGORIES_IF_MORE_THAN_ONE"] = "No puede deshabilitar los canales si hay más de uno.";
$MESS["CRM_TYPE_TABLE_DISABLING_RECYCLEBIN_WHILE_NOT_EMPTY"] = "Elimine todos los elementos de este tipo de la Papelera de reciclaje antes de desactivarlo.";
$MESS["CRM_TYPE_TABLE_FIELD_NOT_CHANGEABLE_ERROR"] = "No se puede cambiar el valor de \"#FIELD#\".";
