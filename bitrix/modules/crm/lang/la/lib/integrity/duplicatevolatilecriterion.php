<?php
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL"] = "#QTY# coincidencias para el valor #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL_EXCEEDED"] = "Más de #QTY# coincidencias para el valor #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_SUMMARY"] = "Coincidencia para el valor \"#VALUE#\" de \"#FIELD#\"";
