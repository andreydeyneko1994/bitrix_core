<?
$MESS["CRM_SCORING_LICENSE_TEXT_P1"] = "¿Cuál es el criterio que utiliza su gerente de ventas cuando maneja las negociaciones? ¿Cola? ¿Intuición? Todo lo que necesita es capturar y llevar a sus clientes más prometedores a la cima de su lista. la predicción de IA analizará las negociaciones existentes para mostrar su probabilidad de éxito.";
$MESS["CRM_SCORING_LICENSE_TEXT_P2"] = "El sistema ayudará a sus empleados a identificar las áreas que requieren más atención.";
$MESS["CRM_SCORING_LICENSE_TEXT_P3"] = "La predicción de IA está disponible en los principales planes. Actualice ahora para mejorar la eficiencia de su departamento de ventas y reduzca los costos de los anuncios.";
$MESS["CRM_SCORING_LICENSE_TITLE"] = "AI Scoring";
?>