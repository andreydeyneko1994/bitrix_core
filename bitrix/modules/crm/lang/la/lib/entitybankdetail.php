<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "No hay datos bancarios de la entidad especificada";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidad bancaria incorrecta";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "No se han encontrado detalles bancarios para eliminarse";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Error al eliminar los datos bancarios";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ACTIVE"] = "Activo";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_ID"] = "ID del país";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_NAME"] = "País";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ID"] = "ID";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_NAME"] = "Nombre";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_SORT"] = "Orden de clasificación";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MAX"] = "La longitud de \"#FIELD_TITLE#\" no debe exceder de #MAX_LENGTH# caracteres.";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MIN"] = "La longitud de \"#FIELD_TITLE#\" debe tener por lo menos #MIN_LENGTH# caracteres.";
?>