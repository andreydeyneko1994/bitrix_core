<?php
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_CSV_IMPORT_NAME"] = "Importación de productos";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_CSV_IMPORT_TITLE"] = "Importación de productos";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_NAME_N"] = "Deshabilitar la administración del inventario";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_NAME_Y"] = "Habilitar la administración del inventario";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_TITLE_N"] = "Deshabilitar la administración del inventario";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_TITLE_Y"] = "Habilitar la administración del inventario";
