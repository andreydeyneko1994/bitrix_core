<?php
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Los canales abiertos adicionales están disponibles solo en los planes comerciales.";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class=\"crm-conditionally-required-field-tab-content\">
	<div class=\"crm-conditionally-required-field-tab-text\">
		Los campos obligatorios específicos de la etapa solo están disponibles en los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class=\"crm-deal-category-tab-content\">
	<div class=\"crm-deal-category-tab-text\">
		Su plan actual restringe la cantidad de líneas de compra que puede usar. Actualice a los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a> para crear tuberías que incluyan múltiples productos y estrategias de ventas para analizar diversas vías comerciales.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Múltiples Pipelines del CRM";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "¡Deje que Bitrix24 cree negociaciones para usted! 
Las negociaciones recurrentes le ahorrarán tiempo y recursos en muchas ocasiones. Por ejemplo, si su plan durante el próximo año es firmar contratos semanales, las negociaciones recurrentes sin duda serán su salvación.
Genere una negociación recurrente y establezca el cronograma y el pipeline. Las nuevas negociaciones se crearán a la hora que especificó y no tendrá que mover un dedo.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">Genere negociaciones automáticamente</li>
		<li class=\"hide-features-list-item\">Mejore el rendimiento y la eficiencia de sus empleados</li>
		<li class=\"hide-features-list-item\">Acceso rápido a negociaciones recurrentes actuales- <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Obtenga más información</a></li>
	</ul>		
	<strong>Las negociaciones recurrentes están disponibles en los planes comerciales seleccionados</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "Las negociaciones recurrentes están disponibles en los planes comerciales";
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Búsqueda avanzada de duplicados</h3>
<div class=\"crm-duplicate-tab-text\">
Cuando se crea un nuevo contacto (o líder o compañía), Bitrix24 encuentra y muestra posibles duplicados &mdash; evitando preventivamente que se creen duplicados.
</div>
<div class=\"crm-duplicate-tab-text\">
En la búsqueda avanzada de duplicados, el CRM también puede encontrar duplicados en datos importados y en datos ya ingresados en la base de datos. Estos duplicados se pueden combinar juntos.
</div>
<div class=\"crm-duplicate-tab-text\">
¡Agregue estas y otras funciones excelentes a su Bitrix24! La Telefonía avanzada + el CRM avanzado y otras funciones útiles están disponibles en los planes comerciales seleccionados. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Find out more</a>
</div>
<div class=\"ui-btn-container ui-btn-container-center\">
<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Obtenga un plan extendido</span>
<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Obtenga una prueba gratuita de 30 días</span>
</div>
</div>";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">El cambio de historial de CRM solo está disponible en CRM avanzado</h3>
	<div class=\"crm-history-tab-text\">
		Bitrix24 mantiene un registro detallado de todos los cambios del CRM. Cuando actualice a CRM avanzado, verá el historial de cambios (es decir, quién accedió o cambió la entrada de CRM) y podrá restaurar los valores anteriores, si es necesario.
	</div>
	<div class=\"crm-history-tab-text\">Esto es lo que parece:</div>
	<img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	<div class=\"crm-history-tab-text\">
		Agregue estas y otras funciones excelentes a su Bitrix24! Advanced Telephony + Advanced CRM y otras funciones útiles están disponibles en los planes comerciales seleccionados. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Saber más</a>
	</div>
	<div class=\"ui-btn-container ui-btn-container-center\">
		<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Ir a un plan extendido</span>
		<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Gratis por 30 días</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "Utilice la función de facturación automática para facturar a sus clientes de manera regular y ahorrar tiempo.
Cree una factura y especifique la frecuencia con la que desea que se envíe en el futuro. El sistema creará y enviará una nueva factura al cliente según su preferencia de tiempo.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">Ahorre tiempo con la facturación automática</li>
		<li class=\"hide-features-list-item\">Acceso rápido a todas las plantillas de facturación automática</li>
		<li class=\"hide-features-list-item\">Envíe la factura al correo electrónico del cliente <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Conozca más</a></li>
	</ul>		
	<strong>La facturación automática está disponible en los planes comerciales seleccionados.</strong>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "La facturación automática está disponible en los planes comerciales.";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
 		El plan gratuito asigna el mismo permiso de acceso a todos los empleados. Puede actualizar a uno de los planes comerciales para asignar diferentes roles, acciones y datos a varios usuarios.	
		Para obtener más información sobre los diferentes planes, vaya a la <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">página de comparación de planes</a>.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		En su plan actual, todos los empleados comparten permisos unificados. Actualice a uno de los planes principales para definir roles, asignar permisos y especificar datos a los que sus empleados pueden acceder.
		<a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">Obtenga más información sobre los planes y compare las funciones incluidas</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "Para asignar diferentes permisos de acceso a sus empleados, actualice a uno de los planes comerciales.";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "Agregue lo siguiente a su CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversión entre negociaciones, facturas y cotizaciones</li>
<li class=\"hide-features-list-item\">Ampliar búsqueda de duplicados</li>
<li class=\"hide-features-list-item\">Historial de cambios para la reversión y recuperación del CRM</li>
<li class=\"hide-features-list-item\">Registro de acceso del CRM</li>
<li class=\"hide-features-list-item\">Visualización de más de 5000 registros en CRM</li>
<li class=\"hide-features-list-item\">Llamadas guiadas por listas<sup class=\"hide-features-soon\">próximamente</sup></li>
<li class=\"hide-features-list-item\">Emails masivos a clientes<sup class=\"hide-features-soon\">próximamente</sup></li>
<li class=\"hide-features-list-item\">Soporte para la venta de servicios<sup class=\"hide-features-soon\">próximamente</sup>
<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Saber más</a>
</li>
</ul>
<strong>Telefonía Avanzada + CRM Avanzado y otras funciones útiles están disponibles en los planes comerciales seleccionados.</strong>
";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM avanzado en Bitrix24";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Ventas automatizadas del CRM de Bitrix24<br><br>
	Cree escenarios CRM para impulsar sus negociaciones: asigne tareas, planee reuniones, lance campañas publicitarias específicas y emita facturas. Las reglas de automatización guiarán a su gerente hacia los siguientes pasos en cada etapa de la negociación para completar el trabajo.<br><br>
	Los activadores reaccionan automáticamente a las acciones del cliente (cuando visita el sitio, llena el formulario, publica el comentario en las redes sociales o hace una llamada) y se lanza una regla de automatización para que los prospectos graduados lleguen a una negociación.<br><br>
	Las reglas de automatización y los activadores hacen todas las operaciones de rutina sin necesidad de entrenar a sus administradores: ¡Configúrelos una vez y siga trabajando!<br><br>
	¡Agregue reglas de automatización e impulse sus ventas!<br><br>
	Esta herramienta está disponible únicamente en las suscripciones comerciales de Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Reglas de automatización";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "La firma puede removerse solo en los planes comerciales.";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "Formularios CRM extendidos";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Su plan actual limita el número de formularios CRM. Con el fin de añadir nuevos formularios, actualice su plan.
<br><br>
CONSEJO: Bitrix24 Professional incluye formularios CRM ilimitados.
<br><br>
Pruebe Bitrix24 Professional gratis durante 30 días.";
