<?php
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_CREATE_RESTRICTED"] = "No puede crear una nueva Automatización Inteligente de Procesos debido a las restricciones de su plan actual";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_CREATE_RESTRICTED"] = "No puede crear un nuevo artículo debido a las restricciones de su plan actual";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_UPDATE_RESTRICTED"] = "No puede editar el artículo debido a las restricciones de su plan actual";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_UPDATE_RESTRICTED"] = "No puede editar la configuración de la Automatización Inteligente de Procesos debido a las restricciones de su plan actual";
