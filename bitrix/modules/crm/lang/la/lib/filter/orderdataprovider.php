<?php
$MESS["CRM_ORDER_FILTER_ACCOUNT_NUMBER"] = "Número";
$MESS["CRM_ORDER_FILTER_ACTIVITY_COUNTER"] = "Actividades";
$MESS["CRM_ORDER_FILTER_ALL"] = "(todo)";
$MESS["CRM_ORDER_FILTER_CANCELED"] = "Cancelado";
$MESS["CRM_ORDER_FILTER_CHECK_PRINTED"] = "Recibo impreso";
$MESS["CRM_ORDER_FILTER_COMPANY_ID"] = "Compañías";
$MESS["CRM_ORDER_FILTER_CONTACT_ID"] = "Contactos";
$MESS["CRM_ORDER_FILTER_COUPON"] = "Cupón usado";
$MESS["CRM_ORDER_FILTER_CREATED_BY"] = "Creado por";
$MESS["CRM_ORDER_FILTER_CURRENCY"] = "Moneda";
$MESS["CRM_ORDER_FILTER_DATE_INSERT"] = "Creado el";
$MESS["CRM_ORDER_FILTER_DATE_UPDATE"] = "Modificado el";
$MESS["CRM_ORDER_FILTER_DEDUCTED"] = "Enviado";
$MESS["CRM_ORDER_FILTER_DELIVERY_SERVICE"] = "Servicios de entrega";
$MESS["CRM_ORDER_FILTER_HAS_ASSOCIATED_DEAL"] = "Tiene una negociación";
$MESS["CRM_ORDER_FILTER_ORDER_TOPIC"] = "Tema del pedido";
$MESS["CRM_ORDER_FILTER_PAYED"] = "Pagado";
$MESS["CRM_ORDER_FILTER_PAY_SYSTEM"] = "Métodos de pago";
$MESS["CRM_ORDER_FILTER_PERSON_TYPE_ID"] = "Tipo de pagador";
$MESS["CRM_ORDER_FILTER_PRICE"] = "Importe";
$MESS["CRM_ORDER_FILTER_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_ORDER_FILTER_SHIPMENT_DELIVERY_DOC_DATE"] = "Fecha de envío del documento";
$MESS["CRM_ORDER_FILTER_SHIPMENT_TRACKING_NUMBER"] = "Número de seguimiento";
$MESS["CRM_ORDER_FILTER_SOURCE_ID"] = "Origen";
$MESS["CRM_ORDER_FILTER_STATUS_ID"] = "Estados";
$MESS["CRM_ORDER_FILTER_USER"] = "Cliente";
$MESS["CRM_ORDER_FILTER_XML_ID"] = "ID del pedido externo";
