<?php
$MESS["CRM_PRESET_ALL_COMPANIES"] = "Todas las empresas";
$MESS["CRM_PRESET_ALL_CONTACTS"] = "Todos los contactos";
$MESS["CRM_PRESET_CLOSED_DEALS"] = "Negociaciones cerradas";
$MESS["CRM_PRESET_CLOSED_LEADS"] = "Prospectos cerrados";
$MESS["CRM_PRESET_CLOSED_QUOTES"] = "Presupuestos cerrados";
$MESS["CRM_PRESET_CLOSED_SI"] = "Facturas cerradas";
$MESS["CRM_PRESET_DEALS_IN_ROBOT_DEBUGGER"] = "Negociaciones en depuración";
$MESS["CRM_PRESET_IN_WORK_DEALS"] = "Negociaciones en progreso";
$MESS["CRM_PRESET_IN_WORK_LEADS"] = "Prospectos en progreso";
$MESS["CRM_PRESET_IN_WORK_QUOTES"] = "En progreso";
$MESS["CRM_PRESET_IN_WORK_SI"] = "Facturas en progreso";
$MESS["CRM_PRESET_MY_COMPANIES"] = "Mis empresas";
$MESS["CRM_PRESET_MY_CONTACTS"] = "Mis contactos";
$MESS["CRM_PRESET_MY_DEALS"] = "Mis negociaciones";
$MESS["CRM_PRESET_MY_LEADS"] = "Mis prospectos";
$MESS["CRM_PRESET_MY_QUOTES"] = "Mis presupuestos";
$MESS["CRM_PRESET_MY_SI"] = "Mis facturas";
