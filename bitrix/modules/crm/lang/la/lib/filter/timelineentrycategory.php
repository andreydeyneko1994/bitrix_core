<?php
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_CALL"] = "Llamada";
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_EMAIL"] = "Email";
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_MEETING"] = "Reunión";
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_REQUEST"] = "Actividad (regla de automatización)";
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_TASK"] = "Tarea";
$MESS["CRM_TIMELINE_CATEGORY_ACTIVITY_VISIT"] = "Visitar";
$MESS["CRM_TIMELINE_CATEGORY_APPLICATION"] = "Aplicación";
$MESS["CRM_TIMELINE_CATEGORY_BIZ_PROCESS"] = "Flujo de trabajo";
$MESS["CRM_TIMELINE_CATEGORY_CALL_TRACKER"] = "Rastreador de llamadas";
$MESS["CRM_TIMELINE_CATEGORY_CHAT"] = "Chat";
$MESS["CRM_TIMELINE_CATEGORY_COMMENT"] = "Comentario";
$MESS["CRM_TIMELINE_CATEGORY_CONVERSION"] = "Entidad convertida";
$MESS["CRM_TIMELINE_CATEGORY_CREATION"] = "Entidad creada";
$MESS["CRM_TIMELINE_CATEGORY_DOCUMENT"] = "Documento";
$MESS["CRM_TIMELINE_CATEGORY_LINK"] = "Elemento del CRM vinculado";
$MESS["CRM_TIMELINE_CATEGORY_LOG_MESSAGE"] = "Evento del sistema";
$MESS["CRM_TIMELINE_CATEGORY_MODIFICATION"] = "Entidad actualizada";
$MESS["CRM_TIMELINE_CATEGORY_NOTIFICATION"] = "Enviar un mensaje utilizando la plantilla del Centro de notificaciones";
$MESS["CRM_TIMELINE_CATEGORY_NOTIFICATION_2"] = "Mensaje basado en una plantilla";
$MESS["CRM_TIMELINE_CATEGORY_ORDER"] = "Pedido";
$MESS["CRM_TIMELINE_CATEGORY_ORDER_CHECK"] = "Recibo";
$MESS["CRM_TIMELINE_CATEGORY_SMS"] = "SMS";
$MESS["CRM_TIMELINE_CATEGORY_UNLINK"] = "Elemento del CRM desvinculado";
$MESS["CRM_TIMELINE_CATEGORY_WAITING"] = "En espera";
$MESS["CRM_TIMELINE_CATEGORY_WEB_FORM"] = "Formulario del CRM";
$MESS["CRM_TIMELINE_CATEGORY_ZOOM"] = "Zoom";
