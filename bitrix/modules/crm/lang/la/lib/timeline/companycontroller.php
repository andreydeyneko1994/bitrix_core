<?
$MESS["CRM_COMPANY_BASE_CAPTION_LEAD"] = "Basado en el prospecto";
$MESS["CRM_COMPANY_CREATION"] = "Compañía agregada";
$MESS["CRM_COMPANY_MOVING_TO_RECYCLEBIN"] = "La compañía fue eliminada a la Papelera de reciclaje";
$MESS["CRM_COMPANY_RESTORATION"] = "La compañía fue recuperada de la Papelera de reciclaje.";
?>