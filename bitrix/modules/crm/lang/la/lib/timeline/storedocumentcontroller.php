<?php
$MESS["STORE_DOCUMENT_ARRIVAL_STATUS_CHANGE"] = "El estado de recepción de las existencias cambió";
$MESS["STORE_DOCUMENT_DEDUCT_STATUS_CHANGE"] = "El estado de la cancelación cambió";
$MESS["STORE_DOCUMENT_MOVING_STATUS_CHANGE"] = "El estado de la transferencia de las existencias cambió";
$MESS["STORE_DOCUMENT_STATUS_CANCELLED"] = "Cancelado";
$MESS["STORE_DOCUMENT_STATUS_CONDUCTED"] = "Procesado";
$MESS["STORE_DOCUMENT_STATUS_DRAFT"] = "Borrador";
$MESS["STORE_DOCUMENT_STORE_ADJUSTMENT_STATUS_CHANGE"] = "El estado del ajuste de las existencias cambió";
$MESS["STORE_DOCUMENT_TITLE"] = "#TITLE# del #DATE# por #PRICE_WITH_CURRENCY#";
