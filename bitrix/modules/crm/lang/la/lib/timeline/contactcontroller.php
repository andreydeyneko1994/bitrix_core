<?
$MESS["CRM_CONTACT_BASE_CAPTION_LEAD"] = "Basado en un prospecto";
$MESS["CRM_CONTACT_CREATION"] = "Contacto creado";
$MESS["CRM_CONTACT_MOVING_TO_RECYCLEBIN"] = "El contacto fue eliminado de la Papelera de reciclaje";
$MESS["CRM_CONTACT_RESTORATION"] = "El contacto fue recuperado de la Papelera de reciclaje.";
?>