<?php
$MESS["CRM_LEAD_CONVERSION"] = "Crear basado en";
$MESS["CRM_LEAD_CREATION"] = "Prospecto agregado";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "El modo de cálculo del importe total cambió";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Calcular automáticamente utilizando los precios de los productos";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manual";
$MESS["CRM_LEAD_MODIFICATION_STATUS"] = "Estado cambiado";
$MESS["CRM_LEAD_MOVING_TO_RECYCLEBIN"] = "El prospecto fue eliminado de la Papelera de reciclaje";
$MESS["CRM_LEAD_RESTORATION"] = "El prospecto fue recuperado de la Papelera de reciclaje.";
