<?php
$MESS["CRM_DEAL_BASE_CAPTION_DEAL_RECURRING"] = "Creado desde la plantilla";
$MESS["CRM_DEAL_BASE_CAPTION_LEAD"] = "Basado en un prospecto";
$MESS["CRM_DEAL_CHECK_TITLE"] = "Recibo #NAME# del #DATE_PRINT#";
$MESS["CRM_DEAL_CREATION"] = "Negociación agregada";
$MESS["CRM_DEAL_CREATION_BASED_ON"] = "Creado en base a";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "El modo de cálculo del importe total cambió";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Cálculo automático utilizando elementos de negociación";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manual";
$MESS["CRM_DEAL_MODIFICATION_STAGE"] = "Etapa cambiada";
$MESS["CRM_DEAL_MOVING_TO_RECYCLEBIN"] = "La negociación fue eliminada de la Papelera de reciclaje";
$MESS["CRM_DEAL_RESTORATION"] = "La negociación fue recuperada de la Papelera de reciclaje.";
$MESS["CRM_DEAL_SUMMARY_ORDER"] = "Pedido ##ORDER_ID# del #ORDER_DATE#";
