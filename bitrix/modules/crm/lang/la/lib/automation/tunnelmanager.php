<?php
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "No se puede agregar una regla de automatización al flujo de trabajo";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "No se puede crear un túnel dentro del embudo";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DEAL_TITLE"] = "Copiar negociación";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DYNAMIC_TITLE"] = "Copiar SPA";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DEAL_TITLE"] = "Mover negociación";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DYNAMIC_TITLE"] = "Mover SPA";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "La creación de túneles no está disponible en su plan";
