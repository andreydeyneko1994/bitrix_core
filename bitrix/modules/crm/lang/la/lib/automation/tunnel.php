<?
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "No se puede agregar una regla de automatización al flujo de trabajo";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "No se puede crear un túnel dentro del embudo";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_TITLE"] = "Copiar la negociación";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "La creación de túneles no está disponible en su plan";
?>