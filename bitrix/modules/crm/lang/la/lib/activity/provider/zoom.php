<?php
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_ENDED_TITLE"] = "La reunión de Zoom finalizó";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_JOINED_TITLE"] = "El cliente hizo clic en el enlace de la invitación de Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_START_TITLE"] = "Reunión de Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_ERROR_INCORRECT_DATETIME"] = "La reunión no se puede programar para la hora seleccionada.";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_MESSAGE_SENT_TITLE"] = "La notificación sobre la reunión de Zoom se envió al cliente";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_TITLE"] = "Reunión de Zoom";
