<?php
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTION"] = "#USER_NAME# escribir:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONF"] = "#USER_NAME# escribir:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONM"] = "#USER_NAME# escribir:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_COMMENT"] = "Ir al comentario";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_ENTRY"] = "Ir al mensaje";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_NAME"] = "Noticias";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_IN"] = "Comentario entrante al mensaje del noticias";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_OUT"] = "Comentario saliente al mensaje del noticias";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_ENTRY"] = "Mensaje del noticias";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT"] = "Comentario del mensaje del noticias";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_IN"] = "Comentario del mensaje del noticias (entrante)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_OUT"] = "Comentario del mensaje del noticias (saliente)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_ENTRY"] = "Mensaje del noticias";
