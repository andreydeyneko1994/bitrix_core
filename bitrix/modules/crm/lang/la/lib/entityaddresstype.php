<?php
$MESS["CRM_ADDRESS_TYPE_BENEFICIARY"] = "Dirección beneficiaria";
$MESS["CRM_ADDRESS_TYPE_DELIVERY"] = "Dirección de envío";
$MESS["CRM_ADDRESS_TYPE_HOME"] = "Dirección registrada";
$MESS["CRM_ADDRESS_TYPE_PRIMARY"] = "Dirección postal";
$MESS["CRM_ADDRESS_TYPE_REGISTERED"] = "Dirección legal";
