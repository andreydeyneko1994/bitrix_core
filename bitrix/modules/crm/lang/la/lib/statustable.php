<?php
$MESS["CRM_STATUS_ENTITY_ENTITY_ID_FIELD"] = "ID de la entidad";
$MESS["CRM_STATUS_ENTITY_NAME_FIELD"] = "Título";
$MESS["CRM_STATUS_ENTITY_STATUS_ID_FIELD"] = "ID del estado";
$MESS["CRM_STATUS_FIELD_UPDATE_ERROR"] = "No se puede actualizar el campo #STATUS_FIELD#";
$MESS["CRM_STATUS_MORE_THAN_ONE_SUCCESS_ERROR"] = "Solo puede existir un estado de éxito para cualquier tipo";
$MESS["CRM_STATUS_STAGE_WITH_ITEMS_ERROR"] = "No se puede eliminar una etapa que contiene elementos";
$MESS["CRM_STATUS_SUCCESS_SEMANTIC_UPDATE_ERROR"] = "No se puede cambiar la semántica del estado de éxito";
