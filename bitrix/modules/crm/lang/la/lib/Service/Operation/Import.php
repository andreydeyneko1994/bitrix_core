<?php
$MESS["CRM_FIELD_CRATED_TIME_VALUE_IN_FUTURE_ERROR"] = "El valor de \"#FIELD#\" no puede estar en el futuro";
$MESS["CRM_FIELD_CRATED_TIME_VALUE_NOT_MONOTONE_ERROR"] = "El valor de \"#FIELD#\" no puede ser menor que el de cualquier otro elemento";
$MESS["CRM_FIELD_VALUE_CAN_NOT_BE_GREATER_ERROR"] = "El valor de \"#FIELD1#\" no puede ser mayor que \"#FIELD2#\"";
$MESS["CRM_FIELD_VALUE_ONLY_ADMIN_CAN_SET_ERROR"] = "Solo el administrador puede cambiar el valor de \"#FIELD#\"";
