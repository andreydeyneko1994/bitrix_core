<?php
$MESS["CRM_TRACKED_OBJECT_DEPENDANT_UPDATE_TEXT"] = "El campo \"#FIELD_NAME#\" de la entidad vinculada \"#ENTITY_NAME#\" cambió";
$MESS["CRM_TRACKED_OBJECT_ENTITY_ADD_EVENT_NAME"] = "Se agregó un enlace a \"#FIELD_NAME#\"";
$MESS["CRM_TRACKED_OBJECT_ENTITY_DELETE_EVENT_NAME"] = "Se eliminó el enlace a \"#FIELD_NAME#\"";
$MESS["CRM_TRACKED_OBJECT_UPDATE_EVENT_NAME"] = "El campo \"#FIELD_NAME#\" cambió";
