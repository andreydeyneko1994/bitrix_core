<?
$MESS["CRM_AGENT_NOTICE_NOTIFY_AGENT_ABOUT_REPEAT_LEAD"] = "¡Los prospectos repetidos están habilitados en su Bitrix24! Siempre que un cliente existente se contacte con usted, el CRM lo reconocerá y creará un prospecto repetido para que una persona responsable pueda procesar y realizar una nueva venta. <a href=\"https://helpdesk.bitrix24.es/open/7347855/\">Detalles</a>";
?>