<?php
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_ALREADY_RUNNING"] = "No se puede iniciar la reindexación porque ya está en progreso.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_INDEX_TYPES"] = "Los tipos de índice no están definidos o tienen valores inválidos.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_RUNNING"] = "No se puede detener la reindexación porque no se ha iniciado.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_STOPPED"] = "No se puede reanudar la reindexación porque nunca se inició ni se detuvo.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_SCOPE"] = "El código de país no está especificado o tiene un valor inválido.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_TYPE_INDEX"] = "El tipo de índice actual es incorrecto.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_050"] = "Se escaneó el 50% de las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_090"] = "Se escaneó el 90% de las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_100"] = "Se escanearon las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_050"] = "Se escaneó el 50% de los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_090"] = "Se escaneó el 90% de los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_100"] = "Se escanearon los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_050"] = "Se escaneó el 50% de los prospectos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_090"] = "Se escaneó el 90% de los prospectos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_100"] = "Se escanearon los prospectos duplicados";
