<?php
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_ALREADY_RUNNING"] = "No se puede iniciar la reindexación porque ya está en progreso.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_RUNNING"] = "No se puede detener la reindexación porque nunca se inició.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_STOPPED"] = "No se puede reanudar la reindexación porque no está en progreso ni en pausa.";
