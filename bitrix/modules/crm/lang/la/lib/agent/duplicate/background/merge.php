<?php
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_ALREADY_RUNNING"] = "No se puede iniciar la fusión porque ya está en progreso.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_RUNNING"] = "No se puede detener la fusión porque no se ha iniciado.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_STOPPED"] = "No se puede reanudar la fusión porque nunca se inició ni se detuvo.";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_050"] = "Se fusionó el 50% de las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_090"] = "Se fusionó el 90% de las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_100"] = "Se fusionaron las empresas duplicadas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_050"] = "Se fusionó el 50% de los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_090"] = "Se fusionó el 90% de los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_100"] = "Se fusionaron los contactos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_050"] = "Se fusionó el 50% de los prospectos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_090"] = "Se fusionó el 90% de los prospectos duplicados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_100"] = "Se fusionaron los prospectos duplicados";
