<?php
$MESS["CRM_STATE_CATALOG_PRODUCT_PRICE_CHANGING_BLOCKED"] = "Actualmente no puede cambiar el precio del producto en una negociación, factura, etc. Aplique el descuento para vender un producto a un precio más bajo. Puede habilitar la edición de precios en la configuración del CRM.";
$MESS["CRM_STATE_ERR_CATALOG_PRODUCT_LIMIT"] = "Se excedió el número máximo de productos del catálogo del CRM. Actualmente hay #COUNT# productos (de un máximo de #LIMIT#).";
