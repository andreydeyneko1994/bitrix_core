<?php
$MESS["CRM_FIELD_NOT_UNIQUE_ERROR"] = "El valor de \"#FIELD#\" no es único";
$MESS["CRM_FIELD_NOT_VALID_ERROR"] = "El valor de \"#FIELD#\" es incorrecto";
$MESS["CRM_FIELD_VALUE_REQUIRED_ERROR"] = "El campo \"#FIELD#\" es requerido";
