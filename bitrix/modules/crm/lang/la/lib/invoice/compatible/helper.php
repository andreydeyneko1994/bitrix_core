<?
$MESS["CRM_INVOICE_COMPAT_HELPER_CANCEL_ERROR"] = "Error al cancelar la factura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DELETE_ERROR"] = "Error al eliminar la factura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_CANCEL"] = "El ID ##ID# de la factura ya fue cancelado";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_PAY"] = "El ID ##ID# de la factura ya fue pagado";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_STATUS"] = "El ID ##ID# de la factura ya está en el estado requerido";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_ACCOUNT_NUMBER"] = "La factura # no puede estar vacía";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_CURRENCY"] = "No se especifica la moneda";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_PERS_TYPE"] = "El tipo de pagador no está especificado";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_SITE"] = "La factura del sitio web no está especificada";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_USER_ID"] = "El ID del cliente no está especificado";
$MESS["CRM_INVOICE_COMPAT_HELPER_EXISTING_ACCOUNT_NUMBER"] = "El # de factura especificado ya está en uso";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE"] = "No se encontró el ID ##ID# de la factura";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE_ID"] = "Falta el ID de la factura";
$MESS["CRM_INVOICE_COMPAT_HELPER_PAY_ERROR"] = "Error al pagar la factura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_CURRENCY"] = "No se puede encontrar la moneda ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_DELIVERY"] = "No se puede encontrar el servicio de entrega ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PERSON_TYPE"] = "No se puede encontrar el tipo de pagador ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PS"] = "No se puede encontrar el sistema de pago ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_SITE"] = "No se puede encontrar el sitio web ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_STATUS"] = "No se puede encontrar el estado ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_USER"] = "No se puede encontrar al usuario ##ID#";
?>