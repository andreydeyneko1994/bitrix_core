<?
$MESS["CRM_ENTITY_PRESET_ACTIVE_FIELD"] = "Activo";
$MESS["CRM_ENTITY_PRESET_COUNTRY_ID_FIELD"] = "ID del país";
$MESS["CRM_ENTITY_PRESET_CREATED_BY_ID_FIELD"] = "Creado por";
$MESS["CRM_ENTITY_PRESET_DATE_CREATE_FIELD"] = "Creado el";
$MESS["CRM_ENTITY_PRESET_DATE_MODIFY_FIELD"] = "Modificado el";
$MESS["CRM_ENTITY_PRESET_ENTITY_TYPE_ID_FIELD"] = "ID del tipo de entidad";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "No puede eliminar esta plantilla porque es una plantilla de compañía predeterminada.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "No puede eliminar esta plantilla porque es una plantilla de contacto predeterminado.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "La plantilla no se puede eliminar porque existen detalles creados en él.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidad no válido para el uso con plantilla.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "No se ha encontrado la plantilla.";
$MESS["CRM_ENTITY_PRESET_FIELD_NAME_FIELD"] = "Nombre";
$MESS["CRM_ENTITY_PRESET_FIELD_TITLE_FIELD"] = "Nombre en la plantilla";
$MESS["CRM_ENTITY_PRESET_ID_FIELD"] = "ID";
$MESS["CRM_ENTITY_PRESET_IN_SHORT_LIST_FIELD"] = "Mostrar en el resumen";
$MESS["CRM_ENTITY_PRESET_MODIFY_BY_ID_FIELD"] = "Modificado por";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Plantilla sin título";
$MESS["CRM_ENTITY_PRESET_NAME_FIELD"] = "Nombre";
$MESS["CRM_ENTITY_PRESET_SORT_FIELD"] = "Clasificación";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_BOOLEAN"] = "Si/No";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DATETIME"] = "Fecha";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DOUBLE"] = "Número";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_STRING"] = "Cadena";
$MESS["CRM_ENTITY_PRESET_XML_ID_FIELD"] = "ID externo";
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Detalles";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Plantillas para el contacto o detalles de la compañía";
?>