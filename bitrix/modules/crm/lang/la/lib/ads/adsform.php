<?
$MESS["CRM_ADS_FORM_TYPE_ERR_DISABLED_FACEBOOK"] = "Debido a los cambios recientes en la política de privacidad de Facebook's, la integración entre los formularios Bitrix24 CRM y Facebook Lead Ads no está disponible temporalmente. Los técnicos de Facebook están trabajando en la nueva API y prometieron que esta característica estará disponible para los usuarios de Bitrix24 pronto otra vez.";
$MESS["CRM_ADS_FORM_TYPE_NAME_FACEBOOK"] = "Anuncios de Facebook";
$MESS["CRM_ADS_FORM_TYPE_NAME_VKONTAKTE"] = "Anuncios VK";
?>