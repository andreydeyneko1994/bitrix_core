<?php
$MESS["CRM_ZOOM_ACTIVITY_CONFERENCE_ENDED_TITLE"] = "Zoom finalizó";
$MESS["CRM_ZOOM_ACTIVITY_CONFERENCE_TITLE"] = "Reunión de Zoom";
$MESS["CRM_ZOOM_ACTIVITY_USER_JOINED_TITLE"] = "El cliente hizo clic en el enlace de la invitación de Zoom";
