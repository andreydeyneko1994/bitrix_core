<?php
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_ASSIGNED_TITLE"] = "Persona responsable";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_BANK_DETAIL_TITLE"] = "Datos bancarios";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CLIENT_EMAIL_TITLE"] = "Email";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CLIENT_NAME"] = "Cliente";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CLIENT_PHONE_TITLE"] = "Teléfono";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CLIENT_WEB_TITLE"] = "Sitio web";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_COMMENT"] = "Nuevo documento creado: <a class=\"document-title-link\">#TITLE#</a>";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_COMPANY_TITLE"] = "Compañía";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CONTACT_TITLE"] = "Contacto";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_CUSTOMER_REQUISITE_TITLE"] = "Detalles del cliente";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_EDIT_MY_COMPANY"] = "<a href=\"#URL#\" target='_blank'>Editar los detalles de mi compañía</a>";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_LEAD_TITLE"] = "Prospecto";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_MY_COMPANY_TITLE"] = "Mi compañía";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_PAYMENT_QR_CODE_TITLE"] = "Código QR de pago";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_STAMPS_DISABLED_EMPTY_FIELDS"] = "Se requieren sellos y firmas.";
$MESS["CRM_DOCGEN_CRMENTITYDATAPROVIDER_STAMPS_DISABLED_NO_TEMPLATE"] = "No hay sellos o firmas.";
$MESS["DOCGEN_TAX_INCLUDED"] = "Impuesto incluido";
$MESS["DOCGEN_TAX_INCLUDED_NOT_VAT"] = "Impuesto (sin IVA) incluido";
$MESS["DOCGEN_TAX_NOT_INCLUDED"] = "Impuesto no incluido";
$MESS["DOCGEN_TAX_NOT_INCLUDED_NOT_VAT"] = "Impuesto (sin IVA) no incluido";
$MESS["DOCGEN_UF_TYPE_BOOLEAN_NO"] = "No";
$MESS["DOCGEN_UF_TYPE_BOOLEAN_YES"] = "Si";
