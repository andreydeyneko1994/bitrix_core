<?php
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_ADDRESS"] = "Dirección del proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_COMPANY"] = "Dirección de la empresa proveedora";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_INN"] = "ID de contribuyente del proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_KPP"] = "KPP del proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_NAME"] = "Proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_PERSON_NAME"] = "Nombre completo del representante del proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_FLD_DOCUMENT_CONTRACTOR_PHONE"] = "Teléfono del proveedor";
$MESS["CRM_DOCGEN_DATAPROVIDER_SD_ARRIVAL_TITLE"] = "Recepción de existencias";
