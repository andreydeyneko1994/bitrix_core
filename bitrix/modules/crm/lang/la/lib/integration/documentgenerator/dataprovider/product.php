<?php
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DESCRIPTION_TITLE"] = "Descripci{on";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DETAIL_PICTURE_TITLE"] = "Imagen completa";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DISCOUNT_RATE_TITLE"] = "Porcentaje de descuento";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DISCOUNT_SUM_TITLE"] = "Valor de descuento";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DISCOUNT_TOTAL_TITLE"] = "Descuento total";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_DISCOUNT_TYPE_TITLE"] = "Tipo de descuento";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_ID_TITLE"] = "ID";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_MEASURE_CODE_TITLE"] = "Código de unidad de medida";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_MEASURE_NAME_TITLE"] = "Unidades de medida";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_NAME_TITLE"] = "Nombre";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PREVIEW_PICTURE_TITLE"] = "Vista previa de la imagen";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_BRUTTO_SUM_TITLE"] = "Precio total con impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_BRUTTO_TITLE"] = "Precio con descuento e impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_EXCLUSIVE_SUM_TITLE"] = "Precio total descontado sin impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_EXCLUSIVE_TITLE"] = "Precio con descuento sin impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_NETTO_SUM_TITLE"] = "Precio total completo sin impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_NETTO_TITLE"] = "Precio completo sin impuestos";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_RAW_NETTO_SUM_TITLE"] = "Precio original total sin descuento";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_RAW_NETTO_TITLE"] = "Precio original sin descuento";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_RAW_SUM_TITLE"] = "Precio total original";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_RAW_TITLE"] = "Precio original";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_SUM_TITLE"] = "Precio total";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_PRICE_TITLE"] = "Precio";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_QUANTITY_TITLE"] = "Cantidad";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_SECTION_TITLE"] = "Sección";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_SORT_TITLE"] = "Clasificar";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_TAX_INCLUDED_TITLE"] = "Incluir impuesto en el precio";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_TAX_RATE_TITLE"] = "Tasa de impuesto";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_TAX_VALUE_SUM_TITLE"] = "Importe del impuesto";
$MESS["CRM_DOCGEN_DATAPROVIDER_PRODUCT_TAX_VALUE_TITLE"] = "Importe del impuesto";
$MESS["CRM_DOCGEN_DATAPROVIDER_TITLE_FULL_TITLE"] = "Nombre del producto incluyendo propiedades importantes";
$MESS["CRM_DOCGEN_DATAPROVIDER_TITLE_TITLE"] = "Nombre del producto";
