<?php
$MESS["CRM_BP_DOCUMENT_ITEM_ENTITY_TYPE_ERROR"] = "No se encontró un SPA de este tipo";
$MESS["CRM_BP_DOCUMENT_ITEM_FIELD_IS_MANUAL_OPPORTUNITY"] = "Calcular la cantidad manualmente";
$MESS["CRM_ENTITY_EXISTENCE_ERROR"] = "El ID de la entidad #DOCUMENT_ID# no existe";
