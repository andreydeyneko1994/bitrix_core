<?php
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_CODE"] = "Venta automática de artículos sin enviar al completar la negociación";
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_DESCRIPTION"] = "Cuando la negociación se haya completado con éxito, el sistema revisará los artículos no enviados y creará un pedido de venta.";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE"] = "Modo reservación";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_MANUAL"] = "Manual";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_ADD_TO_DOCUMENT"] = "Al agregar producto a la negociación";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_PAYMENT"] = "Al efectuar el pago";
$MESS["CRM_SALE_RESERVATION_CONFIG_PERIOD"] = "Reservar durante, días";
$MESS["CRM_SALE_RESERVATION_ENTITY_DEAL"] = "Negociación";
$MESS["CRM_SALE_RESERVATION_ENTITY_INVOICE"] = "Factura";
$MESS["CRM_SALE_RESERVATION_ENTITY_OFFER"] = "Cotización";
$MESS["CRM_SALE_RESERVATION_ENTITY_SMARTPROCESS"] = "Automatización Inteligente de Procesos";
