<?php
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Hubo algún errores al eliminar los datos: entidad omitida (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Hubo algún errores al exportar los datos: entidad omitida (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Hubo algún errores al importar los datos: entidad omitida (#CODE#)";
