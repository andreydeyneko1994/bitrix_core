<?php
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Ya existe un campo con el código #CODE# de algún otro tipo. Elimine este campo y comience a importar de nuevo.";
