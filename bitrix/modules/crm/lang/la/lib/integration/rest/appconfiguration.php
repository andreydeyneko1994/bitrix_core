<?php
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "Haga clic en \"Exportar\" para guardar sus formularios CRM en un archivo";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "Haga clic en “Exportar” para guardar su preset de CRM al archivo";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "Exportar formularios CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "Exportar presets de soluciones de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "¡Prepárese para utilizar los formularios CRM para su negocio en cuestión de minutos!";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "¡Obtenga un CRM listo para usar para su negocio en solo unos minutos!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "Exportar formularios CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "Exportar presets de soluciones de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "Formularios del CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "Presets de soluciones de CRM";
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Hubo errores al eliminar los datos: entidad omitida (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Hubo errores al exportar los datos: entidad omitida (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Ya existe un campo con el código #CODE# de algún otro tipo. Elimine este campo y vuelva a comenzar la importación.";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Hubo errores al importar los datos: entidad omitida (#CODE#)";
