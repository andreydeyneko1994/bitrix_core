<?
$MESS["CRM_CALENDAR_HELP_LINK"] = "Leer más";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_DEAL"] = "Seleccione el modo de vista preferida de las negociaciones para usar en el calendario: por fecha de creación, por otra fecha o por disponibilidad de recursos.";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_LEAD"] = "Seleccione el modo de vista preferido para el prospecto a usar en el calendario: por fecha de creación, por otra fecha o por disponibilidad de recursos.";
$MESS["CRM_CALENDAR_VIEW_SPOTLIGHT"] = "Ahora puede ver los prospectos y negociaciones en el modo de calendario. Planifique las relaciones con sus clientes mientras vigila la disponibilidad de recursos.";
?>