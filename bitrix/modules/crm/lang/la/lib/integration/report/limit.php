<?
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK"] = "Para quitar la restricción, elimine las compañías que ya no necesita o actualice a uno de los planes superiores.";
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK_DELETE"] = "Elimine las compañías que ya no necesita para quitar la restricción. <br> <a href=\"#MORE_INFO_LINK#\">Detalles</a>";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK"] = "Para quitar la restricción, elimine los contactos que ya no necesita o actualice a uno de los planes superiores.";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK_DELETE"] = "Elimine los contactos que ya no necesita para quitar la restricción. <br> <a href=\"#MORE_INFO_LINK#\">Detalles</a>";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK"] = "Para quitar la restricción, elimine las negociaciones que ya no necesita o actualice a uno de los planes superiores.";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK_DELETE"] = "Elimine las negociaciones que ya no necesita para quitar la restricción. <br> <a href=\"#MORE_INFO_LINK#\">Detalles</a>";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK"] = "Para quitar la restricción, elimine los prospectos que ya no necesita o actualice a uno de los planes superiores.";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK_DELETE"] = "Elimine los prospectos que ya no necesita para quitar la restricción. <br> <a href=\"#MORE_INFO_LINK#\">Detalles</a>";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_ACTUAL_COUNT_MASK"] = "Cantidad actual de compañías: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_MAX_COUNT_MASK"] = "Hasta #MAX_COUNT# compañías se pueden incluir en un reporte.";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_ACTUAL_COUNT_MASK"] = "Cantidad actual de contactos: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_MAX_COUNT_MASK"] = "Hasta #MAX_COUNT# contactos se pueden incluir en un reporte.";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_ACTUAL_COUNT_MASK"] = "Cantidad actual de negociaciones: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_MAX_COUNT_MASK"] = "Hasta #MAX_COUNT# negociaciones se pueden incluir en un reporte.";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_ACTUAL_COUNT_MASK"] = "Cantidad actual de prospectos: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_MAX_COUNT_MASK"] = "Hasta #MAX_COUNT# prospectos se pueden incluir en un reporte.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT"] = "Solo se puede incluir un cierto número de entidades de CRM en un reporte analítico.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT_FOR_MORE_INFO"] = "Consulte la página de comparación de tarifas para obtener más detalles.";
?>