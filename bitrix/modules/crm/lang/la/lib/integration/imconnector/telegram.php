<?php
$MESS["CRM_IMCONNECTOR_TELEGRAM_ORDER"] = "El pedido ##ORDER_ID# del #DATE# por #SUM_WITH_CURRENCY# fue realizado";
$MESS["CRM_IMCONNECTOR_TELEGRAM_PAYMENT_PAID"] = "El pedido ##ORDER_ID# del #DATE# por #SUM_WITH_CURRENCY# fue pagado";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_DEDUCTED"] = "El pedido ##ORDER_ID# del #DATE# por #SUM_WITH_CURRENCY# fue enviado";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_READY"] = "El pedido ##ORDER_ID# del #DATE# por #SUM_WITH_CURRENCY# puede recibirse en el almacén que se encuentra en \"#STORE_ADDRESS#\"";
