<?
$MESS["CRM_DOCUMENTGENERATOR_ADD_NEW_TEMPLATE"] = "Agregar nueva plantilla";
$MESS["CRM_DOCUMENTGENERATOR_DOCUMENTS_LIST"] = "Documentos";
$MESS["CRM_DOCUMENTGENERATOR_SPOTLIGHT_TEXT"] = "Ahora puede trabajar con plantillas de documentos. Cree un documento, luego imprímalo o envíelo por correo electrónico.";
?>