<?
$MESS["CRM_RECYCLE_BIN_LEAD_ENTITY_NAME"] = "Prospecto";
$MESS["CRM_RECYCLE_BIN_LEAD_RECOVERY_CONFIRMATION"] = "¿Desea restaurar el prospecto seleccionado?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVAL_CONFIRMATION"] = "El prospecto se eliminará de forma irreversible. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVED"] = "Se eliminó el prospecto.";
$MESS["CRM_RECYCLE_BIN_LEAD_RESTORED"] = "El prospecto se restauró correctamente.";
?>