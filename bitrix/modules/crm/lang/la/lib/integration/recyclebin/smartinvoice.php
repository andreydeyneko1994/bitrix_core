<?php
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_ENTITY_NAME"] = "Factura";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RECOVERY_CONFIRMATION"] = "¿Desea restaurar la factura seleccionada?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVAL_CONFIRMATION"] = "La factura se eliminará de forma permanente. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVED"] = "La factura fue eliminada.";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RESTORED"] = "La factura se recuperó correctamente.";
