<?
$MESS["CRM_RECYCLE_BIN_CONTACT_ENTITY_NAME"] = "Contacto";
$MESS["CRM_RECYCLE_BIN_CONTACT_RECOVERY_CONFIRMATION"] = "¿Desea restaurar el contacto seleccionado?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVAL_CONFIRMATION"] = "El contacto se eliminará de forma irreversible. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVED"] = "Se eliminó el contacto.";
$MESS["CRM_RECYCLE_BIN_CONTACT_RESTORED"] = "El contacto se restauró correctamente.";
?>