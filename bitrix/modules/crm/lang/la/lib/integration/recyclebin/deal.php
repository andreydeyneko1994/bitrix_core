<?
$MESS["CRM_RECYCLE_BIN_DEAL_ENTITY_NAME"] = "Negociación";
$MESS["CRM_RECYCLE_BIN_DEAL_RECOVERY_CONFIRMATION"] = "¿Desea recuperar la negociación seleccionada?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVAL_CONFIRMATION"] = "La negociación se eliminará de forma irreversible. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVED"] = "Se eliminó la negociación.";
$MESS["CRM_RECYCLE_BIN_DEAL_RESTORED"] = "La negociación se restauró exitosamente.";
?>