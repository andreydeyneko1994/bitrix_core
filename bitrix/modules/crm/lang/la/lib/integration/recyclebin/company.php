<?
$MESS["CRM_RECYCLE_BIN_COMPANY_ENTITY_NAME"] = "Compañía";
$MESS["CRM_RECYCLE_BIN_COMPANY_RECOVERY_CONFIRMATION"] = "¿Desea restaurar la compañía seleccionada?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVAL_CONFIRMATION"] = "La compañía se eliminará de forma irreversible. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVED"] = "La compañía se eliminó.";
$MESS["CRM_RECYCLE_BIN_COMPANY_RESTORED"] = "La compañía se restauró correctamente.";
?>