<?php
$MESS["CRM_RECYCLE_BIN_DYNAMIC_ENTITY_NAME"] = "Elemento";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RECOVERY_CONFIRMATION"] = "¿Desea recuperar el elemento seleccionado?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVAL_CONFIRMATION"] = "El elemento se eliminará de forma permanente. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVED"] = "El elemento fue eliminado.";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RESTORED"] = "El elemento se recuperó exitosamente.";
