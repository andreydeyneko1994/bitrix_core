<?
$MESS["CRM_RECYCLE_BIN_ACTIVITY_ENTITY_NAME"] = "Actividad";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RECOVERY_CONFIRMATION"] = "¿Desea restaurar la actividad seleccionada?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVAL_CONFIRMATION"] = "La actividad se eliminará de forma irreversible. ¿Desea continuar?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVED"] = "La actividad se eliminó.";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RESTORED"] = "La actividad se restauró con éxito.";
?>