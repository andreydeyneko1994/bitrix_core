<?php
$MESS["CRM_TRACKING_SOURCE_BASE_ADS_DESC"] = "Fuente de anuncios %name%";
$MESS["CRM_TRACKING_SOURCE_BASE_DESC_OTHER"] = "Otro tráfico incluye a todos los clientes que no pertenecen a ninguna de las fuentes existentes en Sales Intelligence.";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_FB"] = "Facebook";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_GOOGLE"] = "Google Ads";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_INSTAGRAM"] = "Instagram";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_ORGANIC"] = "Tráfico orgánico";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_OTHER"] = "Otro tráfico";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_SENDER-MAIL"] = "E-Mail Marketing";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_VK"] = "VK";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_YANDEX"] = "Yandex.Direct";
$MESS["CRM_TRACKING_SOURCE_BASE_SHORT_NAME_ORGANIC"] = "Orgánico";
$MESS["CRM_TRACKING_SOURCE_BASE_TRAFFIC_DESC"] = "Fuente de tráfico %name%";
