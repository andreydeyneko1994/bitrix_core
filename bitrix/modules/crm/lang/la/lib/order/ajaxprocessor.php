<?
$MESS["CRM_ORDER_AJAX_ERROR"] = "Solicitud de error de procesamiento";
$MESS["CRM_ORDER_AJAX_ERROR_AD"] = "La solicitud no ha podido procesarse. Las razones más posibles incluyen: permisos de usuario insuficientes; Mal funcionamiento del almacenamiento de sesión PHP; Los datos en la solicitud POST fueron cortados por PHP o servidor web.";
?>