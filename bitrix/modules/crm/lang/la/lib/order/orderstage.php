<?php
$MESS["CRM_ORDER_STAGE_PAID"] = "Pagado";
$MESS["CRM_ORDER_STAGE_PAYMENT_CANCEL"] = "Cancelled";
$MESS["CRM_ORDER_STAGE_REFUND"] = "Refunded";
$MESS["CRM_ORDER_STAGE_SENT_NO_VIEWED"] = "Enviado, no visto";
$MESS["CRM_ORDER_STAGE_VIEWED_NO_PAID"] = "Visto, no pagado";
