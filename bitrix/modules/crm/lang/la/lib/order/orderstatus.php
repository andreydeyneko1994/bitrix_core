<?php
$MESS["CRM_ORDER_STATUS_FINISHED"] = "Cumplido";
$MESS["CRM_ORDER_STATUS_INITIAL"] = "Pedido aceptado, esperando pago";
$MESS["CRM_ORDER_STATUS_PAID"] = "Pagado, preparándose para el envío.";
$MESS["CRM_ORDER_STATUS_REFUSED"] = "Cancelado";
$MESS["CRM_ORDER_STATUS_SEND"] = "Enviado";
