<?php
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Los prospectos se convirtieron.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Ocurrió un error al convertir: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Se convirtieron correctamente: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "Sin nombre";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# de #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Convertir prospectos";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "El modo CRM simple convertirá prospectos en negocios. <br/><br/>
En este modo, los prospectos abiertos se convertirán en negociaciones y clientes. <br/><br/>
Todas las reglas de automatización y los procesos comerciales que se configuraron para la creación de prospectos y negociaciones se ejecutarán automática e inmediatamente para las entidades que se crearon recientemente.";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Convertir prospectos";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de prospectos";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Lista de prospectos";
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Análisis de prospectos";
$MESS["CRM_ROBOTS_TEXT"] = "En el modo CRM simple, Bitrix24 convierte automáticamente los nuevos prospectos a negociaciones usando reglas de automatización<br/><br/>
Ya tiene reglas de automatización de un prospecto que están activas actualmente. Habilitar este modo eliminará estas reglas.<br/><br/>
¿Seguro que quiere habilitar el modo CRM simple?";
$MESS["CRM_ROBOTS_TITLE"] = "Modo CRM simple";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_CONTINUE"] = "Continuar";
$MESS["CRM_TYPE_SAVE"] = "Guardar";
$MESS["CRM_TYPE_TITLE"] = "Elija la forma en que desea trabajar con su CRM";
$MESS["CRM_TYPE_TURN_ON"] = "Habilitar";
