<?
$MESS["CRM_DEAL_RECURRING_ENTITY_ACTIVE_FIELD"] = "Activo";
$MESS["CRM_DEAL_RECURRING_ENTITY_BASED_ID_FIELD"] = "Crear con base en la negociación";
$MESS["CRM_DEAL_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Nueva categoría de negociación";
$MESS["CRM_DEAL_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Número de instancias creadas";
$MESS["CRM_DEAL_RECURRING_ENTITY_DEAL_ID_FIELD"] = "ID de la negociación recurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_DEAL_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Método de restricción";
$MESS["CRM_DEAL_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Fecha en que se generó la última negociación recurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Por fecha";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Por número de instancias";
$MESS["CRM_DEAL_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Fecha en que se generará la próxima negociación recurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_PARAMS_FIELD"] = "Parámetros para calcular la próxima negociación recurrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_START_DATE_FIELD"] = "Fecha para comenzar a crear negociaciones recurrentes";
?>