<?php
$MESS["CRM_WEBFORM_FIELD_SYNCHRONIZER_ERR_RES_BOOK"] = "El campo de reservación de recursos \"%fieldCaption%\" no se puede crear automáticamente. Cree un campo de reservación de recursos en %entityCaption% y seleccione el campo en el formulario.";
