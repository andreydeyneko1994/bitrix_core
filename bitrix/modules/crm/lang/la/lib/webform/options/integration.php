<?php
$MESS["CRM_WEBFORM_OPTIONS_LINK_EMPTY_FIELD_MAPPING"] = "No se seleccionaron campos para la conversión.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_FORM_DUPLICATE_ERROR"] = "El formulario ya está conectado al CRM";
$MESS["CRM_WEBFORM_OPTIONS_LINK_REGISTER_FAILED"] = "No es posible suscribirse a los eventos del formulario.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_TYPE_DUPLICATE"] = "No se puede conectar de nuevo el formulario al mismo servicio";
$MESS["CRM_WEBFORM_OPTIONS_LINK_UNREGISTER_FAILED"] = "No se puede cancelar la suscripción a los eventos del formulario.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_WRONG_TYPE"] = "Servicio de enlace al formulario desconocido";
