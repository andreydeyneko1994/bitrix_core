<?
$MESS["CRM_TRACKING_EDITOR_END_CHECKING"] = "Finalizar";
$MESS["CRM_TRACKING_EDITOR_FOUND_ITEMS"] = "Números de teléfono - %phones%, correos electrónicos - %emails%";
$MESS["CRM_TRACKING_EDITOR_NOT_FOUND"] = "No se encontraron entradas.";
$MESS["CRM_TRACKING_EDITOR_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_TRACKING_EDITOR_REPLACEMENT"] = "Intercambio de números de teléfono y de correos electrónicos";
$MESS["CRM_TRACKING_EDITOR_SOURCES_NOT_CONFIGURED"] = "Fuentes no especificadas";
$MESS["CRM_TRACKING_EDITOR_VIEW_ITEMS"] = "Números de teléfono y correos electrónicos de origen";
?>