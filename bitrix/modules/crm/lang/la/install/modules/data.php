<?php
$MESS["CRM_CATALOG_NOT_INSTALLED"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_CATALOG_STORE_ADR"] = "944 5th Avenue";
$MESS["CRM_CATALOG_STORE_DESCR"] = "Aquí puede encontrar productos de los principales proveedores mundiales.";
$MESS["CRM_CATALOG_STORE_GPS_N"] = "40,7681635";
$MESS["CRM_CATALOG_STORE_GPS_S"] = "-73,95932";
$MESS["CRM_CATALOG_STORE_NAME"] = "Almacén";
$MESS["CRM_CATALOG_STORE_PHONE"] = "(212)123-4567";
$MESS["CRM_CATALOG_STORE_SCHEDULE"] = "Mon-Fri, 9AM to 6PM";
$MESS["CRM_DELIVERY_COURIER"] = "Servicio de entrega";
$MESS["CRM_DELIVERY_COURIER_DESCR"] = "Los pedidos se envían durante el día a la hora especificada.";
$MESS["CRM_DELIVERY_PICKUP"] = "Recoger en el local";
$MESS["CRM_DELIVERY_PICKUP_DESCR"] = "Puede recoger los pedidos en nuestra tienda.";
$MESS["CRM_ORDER_PAY_SYSTEM_CASH_DESC"] = "Pago contraentrega";
$MESS["CRM_ORDER_PAY_SYSTEM_CASH_NAME"] = "Efectivo";
$MESS["CRM_ORDER_PAY_SYSTEM_ORDERDOCUMENT_DESC"] = "Factura imprimible (debería abrirse en una nueva ventana).";
$MESS["CRM_ORDER_PAY_SYSTEM_ORDERDOCUMENT_NAME_V2"] = "Factura";
$MESS["CRM_ORD_PROP_2"] = "Ubicación";
$MESS["CRM_ORD_PROP_4"] = "Código postal";
$MESS["CRM_ORD_PROP_5"] = "Dirección de envío";
$MESS["CRM_ORD_PROP_6"] = "Nombre completo";
$MESS["CRM_ORD_PROP_6_2"] = "Nombre completo";
$MESS["CRM_ORD_PROP_7"] = "Dirección de facturación";
$MESS["CRM_ORD_PROP_8"] = "Nombre de la compañía";
$MESS["CRM_ORD_PROP_9"] = "Teléfono";
$MESS["CRM_ORD_PROP_10"] = "Persona de contacto";
$MESS["CRM_ORD_PROP_11"] = "Fax";
$MESS["CRM_ORD_PROP_12"] = "Dirección de envío";
$MESS["CRM_ORD_PROP_13"] = "ID DEL IMPUESTO";
$MESS["CRM_ORD_PROP_14"] = " ";
$MESS["CRM_ORD_PROP_21"] = "Ciudad";
$MESS["CRM_ORD_PROP_40"] = "Nombre de la compañía";
$MESS["CRM_ORD_PROP_41"] = "Persona de contacto";
$MESS["CRM_ORD_PROP_42"] = "Dirección de envío";
$MESS["CRM_ORD_PROP_43"] = "Ciudad";
$MESS["CRM_ORD_PROP_44"] = "Código postal";
$MESS["CRM_ORD_PROP_45"] = "Teléfono";
$MESS["CRM_ORD_PROP_46"] = " ";
$MESS["CRM_ORD_PROP_47"] = "Dirección de facturación";
$MESS["CRM_ORD_PROP_48"] = " ";
$MESS["CRM_ORD_PROP_49"] = "ID DEL IMPUESTO";
$MESS["CRM_ORD_PROP_BIC_SWIFT"] = "BIC/SWIFT";
$MESS["CRM_ORD_PROP_BLZ"] = " ";
$MESS["CRM_ORD_PROP_CRN"] = "Reg. de Compañía No.";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "Información personal";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "Datos de envio";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "Información de la compañía";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "Información del contacto";
$MESS["CRM_ORD_PROP_IBAN"] = "IBAN";
$MESS["CRM_ORD_PROP_SORT_CODE"] = "Código de clasificacion";
$MESS["CRM_ORD_PROP_STEU"] = " ";
$MESS["CRM_ORD_PROP_TRN"] = "Reg. de Impuestos No.";
$MESS["CRM_ORD_PROP_UST_IDNR"] = " ";
$MESS["CRM_SALE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupo de usuarios autorizado para editar las preferencias de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administradores de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupo de usuarios autorizado para utilizar las funciones de la tienda online.";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Personal de la tienda online.";
