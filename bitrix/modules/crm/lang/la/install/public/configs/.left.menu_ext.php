<?
$MESS["CRM_BP"] = "Procesos de negocios";
$MESS["CRM_CONFIG"] = "Configuraciones";
$MESS["CRM_CURRENCIES"] = "Moneda";
$MESS["CRM_EXTERNAL_SALE"] = "Tiendas online";
$MESS["CRM_FIELDS"] = "Campos del cliente";
$MESS["CRM_GUIDES"] = "Listas de Selección";
$MESS["CRM_PERMS"] = "Permisos de Acceso";
$MESS["CRM_SENDSAVE"] = "Integración de Enviar&Guardar";
?>