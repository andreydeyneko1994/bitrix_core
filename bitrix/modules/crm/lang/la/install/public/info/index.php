<?
$MESS["CRM_PAGE_CONTENT"] = "<h3>CRM En pocas palabras</h3>
 
<p><b>Customer relationship management (CRM)</b> es una estrategia de gestión de la interacción de una compañía con sus clientes y sus respectivas perspectivas de ventas. Se trata de utilizar la tecnología para organizar, automatizar, y sincronizar los procesos de negocio, principalmente las actividades de ventas, marketing y servicio al cliente. CRM almacena información de relaciones con clientes para un análisis posterior.</p>

<p>Bitrix CRM se basa en los siguientes conceptos.</p>

<ul>
  <li><b>Contacto</b> es un registro que contiene los datos de una persona con la que su compañía tendrá; o ya tiene una relación.</li>

   <li><b>Compañía</b> es un registro que contiene información acerca de una organización con la que su compañía tiene o puede tener alguna relación.</li>

  <li><b>Prospecto</b> es un registro que contiene datos de un posible contacto para el que hay disponible cierta información (correo electrónico, teléfono) pero con la que todavía no se ha producido ninguna interacción formal.  </li>

  <li><b>Evento</b> se refiere a cualquier suceso o al cambio del mismo,  referente a un contacto, clientes potenciales o compañías. Por ejemplo: añadir un nuevo número de teléfono.
</li>

  <li><b>Negociación</b> es el registro de una transacción financiera con un cliente o compañía.
</li>
 </ul>

<h3>¿Cómo funciona?</h3>

<p>El CRM se puede utilizar dependiendo de las necesidades de su negocio:</p>
 
<ol>
  <li>Como base de datos de contactos de una compañía;</li>

  <li>como un clásico <b>CRM</b> sistema.</li>
 </ol>

<h4>1.Uso de la CRM como una base de datos</h4>

<p>El CRM puede actuar como base de datos para el seguimiento de relaciones comerciales. En este caso, las entidades principales son los contactos, compañías, y los acontecimientos que se refieren a ellos, combinándolos para formar una descripción completa del historial relacionado a dicha compañía. En este modelo inclusive es posible crear eventos para clientes potenciales y aplicarlos de modo que se puedan convertir en negociaciones.</p>

<p><img height='432' border='0' width='900' src='/upload/crm/cim/01-en.png'  /></p>
 
<h4>2.Clásico sistema CRM</h4>

<p>Si desea utilizar el CRM como un verdadero sistema de gestión de relaciones con los clientes, el punto de partida sería crear un prospecto, el mismo que podría ser añadido manualmente o puede ser importado desde Bitrix Site Manager. Una vez creado el &quot;prospecto&quot;, este puede ser procesado y convertirse finalmente en un contacto, una compañía o quedarse como &quot;prospecto&quot;. Si un prospecto es convertido en compañía o contacto, es adicionado como registro de la base de datos de contactos o compañías, según sea el caso. Si los &quot;prospectos&quot; se convierten en una negociación como consecuencia serán tratados por el canal de ventas indicado. </p>
 
<p><img height='454' border='0' width='900' src='/upload/crm/cim/03-en.png'  /></p>";
$MESS["CRM_PAGE_TITLE"] = "Ayuda";
?>