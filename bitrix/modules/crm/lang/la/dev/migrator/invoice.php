<?php
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "Cancelado";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "Enviado";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "En espera de procesamiento";
$MESS["CRM_ORDER_STATUS_D"] = "Cancelado";
$MESS["CRM_ORDER_STATUS_F"] = "Cumplido";
$MESS["CRM_ORDER_STATUS_N"] = "Pedido aceptado, esperando pago";
$MESS["CRM_ORDER_STATUS_P"] = "Pagado, preparándose para el envío.";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_SUB_TITLE"] = "¡Bienvenido!";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TEXT"] = "No se puede imprimir el recibo ##CHECK_ID# por pedido ##ORDER_ACCOUNT_NUMBER# fechado #ORDER_DATE#.

Haga clic aquí para solucionar el problema:
#LINK_URL#";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TITLE"] = "Error al imprimir el recibo";
$MESS["SALE_CHECK_PRINT_ERROR_SUBJECT"] = "Error al imprimir el recibo";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_DESC"] = "#ORDER_ACCOUNT_NUMBER# - ID del pedido
#ORDER_DATE# - fecha del pedido
#ORDER_ID# - ID del pedido
#CHECK_ID# - ID del recibo";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_NAME"] = "Notificación de error de impresión de recibo";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupo de usuarios autorizado para editar las preferencias de la tienda en línea";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administradores de la tienda en línea";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "Grupo de clientes que incluye a todos los clientes de la tienda";
$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "Todos los clientes";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupo de usuarios autorizado para utilizar las funciones de la tienda en línea";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Gerentes de la tienda  en línea";
$MESS["SMAIL_FOOTER_BR"] = "Saludos cordiales,<br />equipo de soporte.";
$MESS["SMAIL_FOOTER_SHOP"] = "Tienda online";
$MESS["UP_TYPE_SUBJECT"] = "Volver a la notificación de stock";
$MESS["UP_TYPE_SUBJECT_DESC"] = "#USER_NAME# - nombre de usuario
#EMAIL# - e-mail de usuario
#NAME# - nombre del producto
#PAGE_URL# - página de detalles del producto";
