<?
$MESS["CRM_BP_CONTACT_TASK_NAME"] = "Processando contato: {=Document:NAME} {=Document:LAST_NAME} (Criado pelo processo comercial)";
$MESS["CRM_BP_CONTACT_TASK_TEXT"] = "Processar o contato: {=Document:NAME} {=Document:LAST_NAME}";
$MESS["CRM_BP_CONTACT_TITLE"] = "Processando contato";
$MESS["CRM_BP_CONTACT_TYPE"] = "Processo comercial sequencial";
?>