<?
$MESS["CRM_BP_SHOP_10000"] = "Um negócio foi criado na loja virtual. Você tem que contactar o cliente dentro de uma hora. A montante do Negócio está acima de 10000 {=Document:ACCOUNT_CURRENCY_ID}.";
$MESS["CRM_BP_SHOP_10000_1"] = "Um novo negócio foi criado na loja virtual.";
$MESS["CRM_BP_SHOP_PBP"] = "Processo de negócio sequencial";
$MESS["CRM_BP_SHOP_SS"] = "Mensagem de rede social";
$MESS["CRM_BP_SHOP_TITLE"] = "Negócio da loja virtual";
$MESS["CRM_BP_SHOP_US"] = "Condição";
$MESS["CRM_BP_SHOP_US_10000"] = "10000";
?>