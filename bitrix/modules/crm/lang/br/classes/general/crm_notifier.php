<?php
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Há novos e-mails";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Retorno de chamada solicitado";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_ASSIGNED_BY"] = "Você foi designado como pessoa responsável";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_STAGE"] = "Etapa alterada";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Você foi mencionado em um post";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Você foi especificado como destinatário de um post";
$MESS["CRM_NOTIFY_SCHEME_MERGE"] = "Notificações de controle de duplicados";
$MESS["CRM_NOTIFY_SCHEME_OTHER"] = "Outras notificações";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulário de CRM enviado";
