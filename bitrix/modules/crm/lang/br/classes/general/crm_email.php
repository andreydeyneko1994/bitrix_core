<?
$MESS["CRM_ADD_MESSAGE"] = "Nova mensagem CRM";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "Um funcionário dispensado existe na fila de distribuição de e-mail para a caixa de correio \"#EMAIL#\". Eles não receberão e-mails de novas fontes, mas todos os e-mails existentes ainda serão encaminhados para esse funcionário. <a href=\"#CONFIG_URL#\">Configurar parâmetros da fila</a>.";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Arquivos não disponíveis (tamanho máximo excedido: %MAX_SIZE% MB)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Adicionar ao corpo da mensagem";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "Não adicionar";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Adicionar ao assunto da mensagem";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(sem assunto)";
$MESS["CRM_EMAIL_EMAILS"] = "E-mail";
$MESS["CRM_EMAIL_FROM"] = "De";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Enviar e Salvar Mensagem";
$MESS["CRM_EMAIL_SUBJECT"] = "Título";
$MESS["CRM_EMAIL_TO"] = "Para";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Nome da empresa: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "criado a partir da mensagem de %SENDER% que não pôde ser relacionada a qualquer Lead, contato ou empresa existente.";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Lead da mensagem de %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Lead da mensagem encaminhada de %SENDER%";
?>