<?php
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "O campo '%FIELD_NAME%' está incorreto.";
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "O campo obrigatório \"%FIELD_NAME%\" está faltando.";
$MESS["CRM_FIELD_COMPARE_ADDRESS"] = "O campo \"Endereço\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_ASSIGNED_BY_ID"] = "O campo \"Pessoa Responsável\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_BIRTHDATE"] = "Campo da \"Data de nascimento\" alterado";
$MESS["CRM_FIELD_COMPARE_COMMENTS"] = "O campo \"Comentário\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_COMPANY_ID"] = "Сampo \"Empresa\" alterado";
$MESS["CRM_FIELD_COMPARE_COMPANY_TITLE"] = "O campo \"Nome da empresa\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_CONTACTS_ADDED"] = "Link para contatos criado";
$MESS["CRM_FIELD_COMPARE_CONTACTS_REMOVED"] = "Link para contatos excluído";
$MESS["CRM_FIELD_COMPARE_CONTACT_ID"] = "Campo \"Contato\" alterado";
$MESS["CRM_FIELD_COMPARE_CURRENCY"] = "Campo \"Moeda\" alterado";
$MESS["CRM_FIELD_COMPARE_EMPTY"] = "-vazio-";
$MESS["CRM_FIELD_COMPARE_GENERAL_CUSTOMER"] = "Lead alterado para lead primário";
$MESS["CRM_FIELD_COMPARE_LAST_NAME"] = "O campo \"Sobrenome\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_NAME"] = "O campo \"Primeiro Nome\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_OPPORTUNITY"] = "O campo \"Oportunidade\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_POST"] = "O campo \"Cargo\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_PRODUCT"] = "O campo \"Produto\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_PRODUCT_ID"] = "O campo \"Produto\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_RETURNING_CUSTOMER"] = "Lead alterado para lead repetido";
$MESS["CRM_FIELD_COMPARE_SECOND_NAME"] = "O campo \"Segundo Nome\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_SOURCE_DESCRIPTION"] = "O campo \"Descrição da Fonte\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_SOURCE_ID"] = "O campo \"Fonte\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_STATUS_DESCRIPTION"] = "O campo \"Descrição do Status\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_STATUS_ID"] = "O campo \"Status\" foi alterado.";
$MESS["CRM_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Valor da Oportunidade";
$MESS["CRM_FIELD_PHONE"] = "Telefone";
$MESS["CRM_FIELD_WEB"] = "Site";
$MESS["CRM_LEAD_CREATION_CANCELED"] = "Lead não foi criado porque a operação foi cancelada pelo manipulador de eventos: \"#NAME#\"";
$MESS["CRM_LEAD_DEFAULT_TITLE_TEMPLATE"] = "Lead #%NUMBER%";
$MESS["CRM_LEAD_DELETION_DEPENDENCIES_FOUND"] = "Não foi possível excluir os Leads convertidos \"#TITLE#\" porque existem empresas e contatos vinculados a ele. Por favor, apague esses itens e, em seguida, tente novamente.";
$MESS["CRM_LEAD_DELETION_NOT_FOUND"] = "O Lead não foi encontrado.";
$MESS["CRM_LEAD_EVENT_ADD"] = "Lead adicionado";
$MESS["CRM_LEAD_EVENT_UPDATE_ASSIGNED_BY"] = "Responsável mudou";
$MESS["CRM_LEAD_EVENT_UPDATE_STATUS"] = "Status atualizado";
$MESS["CRM_LEAD_EVENT_UPDATE_TITLE"] = "O nome foi alterado";
$MESS["CRM_LEAD_FIELD_ADDRESS"] = "Endereço";
$MESS["CRM_LEAD_FIELD_ADDRESS_2"] = "Endereço (linha 2)";
$MESS["CRM_LEAD_FIELD_ADDRESS_CITY"] = "Cidade";
$MESS["CRM_LEAD_FIELD_ADDRESS_COUNTRY"] = "País";
$MESS["CRM_LEAD_FIELD_ADDRESS_COUNTRY_CODE"] = "Código do País";
$MESS["CRM_LEAD_FIELD_ADDRESS_POSTAL_CODE"] = "CEP";
$MESS["CRM_LEAD_FIELD_ADDRESS_PROVINCE"] = "Estado / Província";
$MESS["CRM_LEAD_FIELD_ADDRESS_REGION"] = "Região";
$MESS["CRM_LEAD_FIELD_ASSIGNED_BY_ID"] = "Pessoa responsável";
$MESS["CRM_LEAD_FIELD_BIRTHDATE"] = "Data de nascimento";
$MESS["CRM_LEAD_FIELD_COMMENTS"] = "Observação";
$MESS["CRM_LEAD_FIELD_COMPANY_ID"] = "Empresa";
$MESS["CRM_LEAD_FIELD_COMPANY_TITLE"] = "Nome da empresa";
$MESS["CRM_LEAD_FIELD_COMPARE"] = " Campo \"#FIELD_NAME#\" alterado";
$MESS["CRM_LEAD_FIELD_COMPARE_IS_MANUAL_OPPORTUNITY"] = "Modo de cálculo do valor total alterado";
$MESS["CRM_LEAD_FIELD_COMPARE_IS_MANUAL_OPPORTUNITY_N"] = "Calcular automaticamente usando os preços dos produtos";
$MESS["CRM_LEAD_FIELD_COMPARE_IS_MANUAL_OPPORTUNITY_Y"] = "Manual";
$MESS["CRM_LEAD_FIELD_CONTACT_ID"] = "Contato";
$MESS["CRM_LEAD_FIELD_CREATED_BY_ID"] = "Criado por";
$MESS["CRM_LEAD_FIELD_CURRENCY_ID"] = "Moeda";
$MESS["CRM_LEAD_FIELD_DATE_CLOSED"] = "Concluído em";
$MESS["CRM_LEAD_FIELD_DATE_CREATE"] = "Criado em";
$MESS["CRM_LEAD_FIELD_DATE_MODIFY"] = "Modificado em";
$MESS["CRM_LEAD_FIELD_HAS_EMAIL"] = "E-mail especificado";
$MESS["CRM_LEAD_FIELD_HAS_IMOL"] = "Canal Aberto especificado";
$MESS["CRM_LEAD_FIELD_HAS_PHONE"] = "Telefone especificado";
$MESS["CRM_LEAD_FIELD_HONORIFIC"] = "Saudação";
$MESS["CRM_LEAD_FIELD_ID"] = "ID";
$MESS["CRM_LEAD_FIELD_IS_RETURN_CUSTOMER"] = "Lead repetido";
$MESS["CRM_LEAD_FIELD_LAST_NAME"] = "Sobrenome";
$MESS["CRM_LEAD_FIELD_MODIFY_BY_ID"] = "Modificado por";
$MESS["CRM_LEAD_FIELD_NAME"] = "Nome";
$MESS["CRM_LEAD_FIELD_OBSERVER"] = "Observadores";
$MESS["CRM_LEAD_FIELD_OBSERVER_IDS"] = "Observadores";
$MESS["CRM_LEAD_FIELD_OPENED"] = "Disponível para todos";
$MESS["CRM_LEAD_FIELD_OPPORTUNITY"] = "Total";
$MESS["CRM_LEAD_FIELD_OPPORTUNITY_INVALID"] = "O campo \"Total previsto para o Negócio\" contém caracteres inválidos.";
$MESS["CRM_LEAD_FIELD_ORIGINATOR_ID"] = "Fonte externa";
$MESS["CRM_LEAD_FIELD_ORIGIN_ID"] = "ID do item na fonte de dados";
$MESS["CRM_LEAD_FIELD_POST"] = "Posição";
$MESS["CRM_LEAD_FIELD_PRODUCT_ROWS"] = "Produtos";
$MESS["CRM_LEAD_FIELD_SECOND_NAME"] = "Segundo nome";
$MESS["CRM_LEAD_FIELD_SOURCE_DESCRIPTION"] = "Informações da fonte";
$MESS["CRM_LEAD_FIELD_SOURCE_ID"] = "Fonte";
$MESS["CRM_LEAD_FIELD_STATUS_DESCRIPTION"] = "Informações de status";
$MESS["CRM_LEAD_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_LEAD_FIELD_STATUS_SEMANTIC_ID"] = "Detalhes de status";
$MESS["CRM_LEAD_FIELD_TITLE"] = "Nome do Lead";
$MESS["CRM_LEAD_FIELD_UTM"] = "Parâmetros UTM";
$MESS["CRM_LEAD_FIELD_UTM_CAMPAIGN"] = "UTM da campanha publicitária";
$MESS["CRM_LEAD_FIELD_UTM_CONTENT"] = "Conteúdo da campanha";
$MESS["CRM_LEAD_FIELD_UTM_MEDIUM"] = "Mídia";
$MESS["CRM_LEAD_FIELD_UTM_SOURCE"] = "Sistema de anúncios";
$MESS["CRM_LEAD_FIELD_UTM_TERM"] = "Termo de pesquisa da campanha";
$MESS["CRM_LEAD_FIELD_WEBFORM_ID"] = "Criado pelo formulário de CRM";
$MESS["CRM_LEAD_NOT_RESPONSIBLE_IM_NOTIFY"] = "Você não é mais responsável pelo Lead: \"#title#\"";
$MESS["CRM_LEAD_PROGRESS_IM_NOTIFY"] = "Lead \"#title#\" mudou o status de \"#start_status_title#\" para \"#final_status_title#\"";
$MESS["CRM_LEAD_RESPONSIBLE_IM_NOTIFY"] = "Você agora é responsável pelo Lead \"#title#\"";
$MESS["CRM_LEAD_UPDATE_CANCELED"] = "Lead não foi atualizado porque a operação foi cancelada pelo manipulador de eventos: \"#NAME#\"";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PERMISSION_USER_NOT_DEFINED"] = "Não é possível verificar as permissões porque nenhum usuário foi especificado.";
