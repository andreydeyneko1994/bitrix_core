<?php
$MESS["CRM_ACTIVITY_TIME_NOT_SPECIFIED"] = "não configurado";
$MESS["CRM_CLIENT_SUMMARY_COMPANY_NOT_SPECIFIED"] = "Nenhuma empresa selecionada";
$MESS["CRM_CLIENT_SUMMARY_CONTACT_NOT_SPECIFIED"] = "Nenhum contato selecionado";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN"] = "Cliente oculto";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN_COMPANY"] = "Empresa oculta";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN_CONTACT"] = "Contato oculto";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN_DEAL"] = "Negócio oculto";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN_LEAD"] = "Lead oculto";
$MESS["CRM_CLIENT_SUMMARY_HIDDEN_QUOTE"] = "Orçamento oculto";
$MESS["CRM_CONFIRMATION_DLG_TTL"] = "Confirmar";
$MESS["CRM_DEAL_STAGE_MANAGER_APOLOGY_TTL"] = "Opções de fechamento do negócio";
$MESS["CRM_DEAL_STAGE_MANAGER_CHECK_ERROR_TTL"] = "Preencha todos os campos obrigatórios para alterar a etapa do negócio";
$MESS["CRM_DEAL_STAGE_MANAGER_DLG_TTL"] = "Selecione o resultado do negócio.";
$MESS["CRM_DEAL_STAGE_MANAGER_FAILURE_TTL"] = "Negócio perdido";
$MESS["CRM_DEAL_STAGE_MANAGER_SELECTOR_TTL"] = "Concluir negócio";
$MESS["CRM_DEAL_STAGE_MANAGER_WON_STEP_HINT"] = "Fechar negócio";
$MESS["CRM_DEAL_SUM_PAID_FIELD"] = "Pago";
$MESS["CRM_ENTITY_ACTIVITY_FOR_RESPONSIBLE"] = "para";
$MESS["CRM_ENTITY_ADD_ACTIVITY"] = "Adicionar atividade";
$MESS["CRM_ENTITY_ADD_ACTIVITY_HINT"] = "Nenhuma atividade";
$MESS["CRM_ENTITY_INFO_CONTACT"] = "Contato";
$MESS["CRM_ENTITY_INFO_DETAILS_BUTTON"] = "Detalhes";
$MESS["CRM_ENTITY_INFO_DETAILS_TITLE"] = "Detalhes";
$MESS["CRM_ENTITY_INFO_EMAIL"] = "E-mail";
$MESS["CRM_ENTITY_INFO_PHONE"] = "Telefone";
$MESS["CRM_ENTITY_INFO_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_ENTITY_INFO_RESPONSIBLE_CHANGE"] = "alterar";
$MESS["CRM_ENTITY_MULTI_FIELDS_MORE"] = "mais";
$MESS["CRM_ENTITY_WAITING"] = "Aguardando";
$MESS["CRM_GET_USER_INFO_GENERAL_ERROR"] = "Não é possível recuperar as informações do usuário.";
$MESS["CRM_INVOICE_FIELD_DATE_MARKED"] = "Data de cancelamento";
$MESS["CRM_INVOICE_FIELD_PAY_VOUCHER_DATE"] = "Data de pagamento";
$MESS["CRM_INVOICE_FIELD_PAY_VOUCHER_NUM"] = "Documento #";
$MESS["CRM_INVOICE_FIELD_REASON_MARKED"] = "Razão do cancelamento";
$MESS["CRM_INVOICE_FIELD_REASON_MARKED_SUCCESS"] = "Comentário do pagamento";
$MESS["CRM_INVOICE_STATUS_MANAGER_APOLOGY_TTL"] = "Opções de valor da fatura";
$MESS["CRM_INVOICE_STATUS_MANAGER_COMMENT_LABEL"] = "Comentário";
$MESS["CRM_INVOICE_STATUS_MANAGER_DATE_LABEL"] = "Data";
$MESS["CRM_INVOICE_STATUS_MANAGER_DLG_TTL"] = "Selecione o valor da fatura ";
$MESS["CRM_INVOICE_STATUS_MANAGER_FAILURE_TTL"] = "Recusado";
$MESS["CRM_INVOICE_STATUS_MANAGER_F_STEP_HINT"] = "Fechar fatura";
$MESS["CRM_INVOICE_STATUS_MANAGER_NOT_SPECIFIED"] = "não definido";
$MESS["CRM_INVOICE_STATUS_MANAGER_PAY_VOUCHER_NUM_LABEL_1"] = "Fatura #";
$MESS["CRM_INVOICE_STATUS_MANAGER_SELECTOR_TTL"] = "Fechar fatura";
$MESS["CRM_INVOICE_STATUS_MANAGER_SET_DATE"] = "Ajustar uma data";
$MESS["CRM_ITEM_STATUS_MANAGER_CHECK_ERROR_TTL"] = "Preencha os campos obrigatórios para alterar a etapa";
$MESS["CRM_ITEM_STATUS_MANAGER_DLG_TTL"] = "Selecionar resultado";
$MESS["CRM_ITEM_STATUS_MANAGER_FAILURE_TTL"] = "Recusado";
$MESS["CRM_ITEM_STATUS_MANAGER_SELECTOR_TTL"] = "Fechar SPA";
$MESS["CRM_LEAD_STAGE_MANAGER_CHECK_ERROR_TTL"] = "Preencha todos os campos obrigatórios para alterar o status do lead";
$MESS["CRM_LEAD_STATUS_MANAGER_APOLOGY_TTL"] = "Opções de gerenciamento do Lead";
$MESS["CRM_LEAD_STATUS_MANAGER_CONVERSION_CANCEL_CNT"] = "Isso excluirá links para entidades CRM criadas anteriormente. As próprias entidades permanecerão intactas no seu CRM. Tem certeza de que deseja cancelar a conversão do lead?";
$MESS["CRM_LEAD_STATUS_MANAGER_CONVERTED_STEP_HINT"] = "Lead concluído";
$MESS["CRM_LEAD_STATUS_MANAGER_CONVERTED_STEP_NAME"] = "Converter";
$MESS["CRM_LEAD_STATUS_MANAGER_DLG_TTL"] = "Selecione o resultado de gerenciamento do Lead";
$MESS["CRM_LEAD_STATUS_MANAGER_FAILURE_TTL"] = "Lead descartado";
$MESS["CRM_LEAD_STATUS_MANAGER_SELECTOR_TTL"] = "Lead concluído";
$MESS["CRM_ORDER_SHIPMENT_STATUS_MANAGER_APPROVED_STEP_HINT"] = "Concluir envio";
$MESS["CRM_ORDER_SHIPMENT_STATUS_MANAGER_DLG_TTL"] = "Selecionar resultado para envio concluído.";
$MESS["CRM_ORDER_SHIPMENT_STATUS_MANAGER_FAILURE_TTL"] = "Não enviado";
$MESS["CRM_ORDER_SHIPMENT_STATUS_MANAGER_SELECTOR_TTL"] = "Processando";
$MESS["CRM_ORDER_STATUS_MANAGER_APPROVED_STEP_HINT"] = "Concluir pedido";
$MESS["CRM_ORDER_STATUS_MANAGER_COMMENT_LABEL"] = "Comentário";
$MESS["CRM_ORDER_STATUS_MANAGER_DATE_LABEL"] = "Data";
$MESS["CRM_ORDER_STATUS_MANAGER_DLG_TTL"] = "Selecionar resultado para pedido concluído.";
$MESS["CRM_ORDER_STATUS_MANAGER_FAILURE_TTL"] = "Cancelado";
$MESS["CRM_ORDER_STATUS_MANAGER_F_STEP_HINT"] = "Concluir pedido";
$MESS["CRM_ORDER_STATUS_MANAGER_NOT_SPECIFIED"] = "não configurado";
$MESS["CRM_ORDER_STATUS_MANAGER_PAY_VOUCHER_NUM_LABEL"] = "Fatura #";
$MESS["CRM_ORDER_STATUS_MANAGER_SELECTOR_TTL"] = "Processando";
$MESS["CRM_ORDER_STATUS_MANAGER_SET_DATE"] = "Definir data";
$MESS["CRM_QUOTE_STAGE_MANAGER_CHECK_ERROR_TTL"] = "Preencha todos os campos obrigatórios para alterar o status da cotação";
$MESS["CRM_QUOTE_STATUS_MANAGER_APPROVED_STEP_HINT"] = "Fechar o orçamento";
$MESS["CRM_QUOTE_STATUS_MANAGER_CHECK_ERROR_TTL"] = "Preencha todos os campos obrigatórios para alterar o status da cotação";
$MESS["CRM_QUOTE_STATUS_MANAGER_DLG_TTL"] = "Selecione o resultado final para o orçamento.";
$MESS["CRM_QUOTE_STATUS_MANAGER_FAILURE_TTL"] = "Recusado";
$MESS["CRM_QUOTE_STATUS_MANAGER_SELECTOR_TTL"] = "Fechar o orçamento";
$MESS["CRM_SIP_NO_SUPPORTED"] = "Chamadas telefônicas não são compatíveis neste contexto.";
$MESS["CRM_STAGE_MANAGER_CHECK_ERROR_HELP"] = "Mais sobre campos obrigatórios";
