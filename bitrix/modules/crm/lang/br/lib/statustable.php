<?php
$MESS["CRM_STATUS_ENTITY_ENTITY_ID_FIELD"] = "ID da entidade";
$MESS["CRM_STATUS_ENTITY_NAME_FIELD"] = "Título";
$MESS["CRM_STATUS_ENTITY_STATUS_ID_FIELD"] = "ID do status";
$MESS["CRM_STATUS_FIELD_UPDATE_ERROR"] = "Não é possível atualizar o campo #STATUS_FIELD#";
$MESS["CRM_STATUS_MORE_THAN_ONE_SUCCESS_ERROR"] = "Apenas um status de sucesso pode existir para qualquer tipo";
$MESS["CRM_STATUS_STAGE_WITH_ITEMS_ERROR"] = "Não é possível excluir uma etapa contendo itens";
$MESS["CRM_STATUS_SUCCESS_SEMANTIC_UPDATE_ERROR"] = "Não é possível alterar a semântica do status de sucesso";
