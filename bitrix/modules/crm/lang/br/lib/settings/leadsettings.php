<?php
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Os leads foram convertidos.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Falha ao converter: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Convertidos com sucesso: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "Sem nome";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# de #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Converter Leads";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "O modo CRM Simples converterá leads em negócios. <br/><br/>
Leads abertos serão convertidos em negócios e clientes neste modo. <br/><br/>
Todas as regras de automação e processos de negócios configurados para criação de leads e negócios serão automática e imediatamente executados para as entidades recém-criadas.";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Converter Leads";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de Lead";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Lista de Leads";
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Análise de Leads";
$MESS["CRM_ROBOTS_TEXT"] = "No modo CRM Simples, o Bitrix24 converte automaticamente novos Leads em negócios usando regras de automação.<br/><br/>
Você já tem regras de automação de Negócios que estão ativas no momento. Ativar este modo irá excluir essas regras.<br/><br/>
Você tem certeza que deseja ativar o modo CRM simples?";
$MESS["CRM_ROBOTS_TITLE"] = "Modo simples de CRM";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_CONTINUE"] = "Continuar";
$MESS["CRM_TYPE_SAVE"] = "Salvar";
$MESS["CRM_TYPE_TITLE"] = "Escolha a maneira que você deseja trabalhar com o seu CRM";
$MESS["CRM_TYPE_TURN_ON"] = "Ativar";
