<?php
$MESS["CRM_ACTIVITY_BINDING_ALREADY_BOUND_ERROR"] = "A atividade já está vinculada a esta entidade";
$MESS["CRM_ACTIVITY_BINDING_LAST_BINDING_ERROR"] = "Não é possível excluir o único link entre atividade e entidade.";
$MESS["CRM_ACTIVITY_BINDING_NOT_BOUND_ERROR"] = "A atividade não está vinculada a esta entidade";
