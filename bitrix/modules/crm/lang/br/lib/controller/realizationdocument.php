<?php
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ACCESS_DENIED"] = "Permissões insuficientes para executar a operação.";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_DELETE_DEDUCTED_ERROR"] = "O \"Pedido de venda ##ID#\" não pode ser excluído porque já foi processado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_NOT_USED_INVENTORY_MANAGEMENT"] = "Você tem que ativar o gerenciamento do inventário para processar o objeto de inventário";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ORDER_NOT_FOUND_ERROR"] = "O pedido não foi encontrado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_PRODUCT_NOT_FOUND"] = "Insira pelo menos um produto";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIPMENT_NOT_FOUND_ERROR"] = "O envio ##ID# não foi encontrado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIP_DEDUCTED_ERROR"] = "Erro ao processar o \"Pedido de venda ##ID#\": ele já foi processado";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_UNSHIP_UNDEDUCTED_ERROR"] = "Erro ao cancelar o \"Pedido de venda ##ID#\": ele não foi processado.";
