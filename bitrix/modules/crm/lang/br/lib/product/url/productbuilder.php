<?php
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_CSV_IMPORT_NAME"] = "Importação de produto";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_CSV_IMPORT_TITLE"] = "Importação de produto";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_NAME_N"] = "Desativar gerenciamento do inventário";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_NAME_Y"] = "Ativar gerenciamento do inventário";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_TITLE_N"] = "Desativar gerenciamento do inventário";
$MESS["CRM_PRODUCT_BUILDER_CONTEXT_MENU_ITEM_WAREHOUSE_TITLE_Y"] = "Ativar gerenciamento do inventário";
