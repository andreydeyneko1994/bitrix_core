<?php
$MESS["CRM_DEAL_BASE_CAPTION_DEAL_RECURRING"] = "Criar a partir do modelo";
$MESS["CRM_DEAL_BASE_CAPTION_LEAD"] = "Com base em Lead";
$MESS["CRM_DEAL_CHECK_TITLE"] = "Recibo #NAME# de #DATE_PRINT#";
$MESS["CRM_DEAL_CREATION"] = "Negócio adicionado";
$MESS["CRM_DEAL_CREATION_BASED_ON"] = "Criado com base em";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Modo de cálculo do valor total alterado";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Cálculo automático usando itens de negócio";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manual";
$MESS["CRM_DEAL_MODIFICATION_STAGE"] = "Etapa alterada";
$MESS["CRM_DEAL_MOVING_TO_RECYCLEBIN"] = "O negócio foi excluído para a Lixeira";
$MESS["CRM_DEAL_RESTORATION"] = "O negócio foi recuperado da Lixeira.";
$MESS["CRM_DEAL_SUMMARY_ORDER"] = "Pedido ##ORDER_ID# de #ORDER_DATE#";
