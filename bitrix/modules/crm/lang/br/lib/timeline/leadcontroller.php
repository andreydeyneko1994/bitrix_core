<?php
$MESS["CRM_LEAD_CONVERSION"] = "Criar com base em";
$MESS["CRM_LEAD_CREATION"] = "Lead adicionado";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Modo de cálculo do valor total alterado";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Calcular automaticamente usando os preços dos produtos";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Manual";
$MESS["CRM_LEAD_MODIFICATION_STATUS"] = "Status alterado";
$MESS["CRM_LEAD_MOVING_TO_RECYCLEBIN"] = "O lead foi excluído para a Lixeira";
$MESS["CRM_LEAD_RESTORATION"] = "O lead foi recuperado da Lixeira.";
