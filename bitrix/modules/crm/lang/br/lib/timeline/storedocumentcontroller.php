<?php
$MESS["STORE_DOCUMENT_ARRIVAL_STATUS_CHANGE"] = "Status do recibo de estoque alterado";
$MESS["STORE_DOCUMENT_DEDUCT_STATUS_CHANGE"] = "Status de baixa alterado";
$MESS["STORE_DOCUMENT_MOVING_STATUS_CHANGE"] = "Status de transferência de estoque alterado";
$MESS["STORE_DOCUMENT_STATUS_CANCELLED"] = "Cancelado";
$MESS["STORE_DOCUMENT_STATUS_CONDUCTED"] = "Processado";
$MESS["STORE_DOCUMENT_STATUS_DRAFT"] = "Rascunho";
$MESS["STORE_DOCUMENT_STORE_ADJUSTMENT_STATUS_CHANGE"] = "Status de ajuste de estoque alterado";
$MESS["STORE_DOCUMENT_TITLE"] = "#TITLE# de #DATE# por #PRICE_WITH_CURRENCY#";
