<?php
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "Mencionou você no comentário para \"#ENTITY_TITLE#\", texto do comentário: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "Mencionou você no comentário para \"#ENTITY_TITLE#\", texto do comentário: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "Mencionou você no comentário para #ENTITY_TITLE#, texto do comentário: \"#COMMENT#\"";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "empresa #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "contato #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "negócio #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DYNAMIC"] = "item #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "fatura #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "lead #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_ORDER"] = "pedido #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_QUOTE"] = "citação #ENTITY_NAME#";
