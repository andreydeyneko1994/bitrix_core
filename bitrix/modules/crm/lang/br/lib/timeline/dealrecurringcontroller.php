<?
$MESS["CRM_DEAL_BASE_CAPTION_BASED_ON_DEAL"] = "Com base no negócio";
$MESS["CRM_DEAL_CREATION"] = "Negócio adicionado";
$MESS["CRM_DEAL_RECURRING_ACTIVE"] = "Negócio recorrente está ativo";
$MESS["CRM_DEAL_RECURRING_CREATION"] = "Modelo de negócio criado";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION"] = "Próxima data de criação";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION_CHANGED"] = "Próxima data de criação alterada";
$MESS["CRM_DEAL_RECURRING_NOT_ACTIVE"] = "Negócio recorrente não está ativo";
$MESS["CRM_RECURRING_CREATION_BASED_ON"] = "Criada com base em";
?>