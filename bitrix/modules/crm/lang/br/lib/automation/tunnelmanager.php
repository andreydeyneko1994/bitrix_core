<?php
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "Não é possível adicionar regra de automação ao fluxo de trabalho";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "Não é possível criar túnel dentro do funil";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DEAL_TITLE"] = "Copiar negócio";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_DYNAMIC_TITLE"] = "Copiar SPA";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DEAL_TITLE"] = "Mover negócio";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_MOVE_DYNAMIC_TITLE"] = "Mover SPA";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "A criação de túneis não está disponível no seu plano";
