<?
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_EXTERNAL_TEMPLATE"] = "Não é possível adicionar regra de automação ao fluxo de trabalho";
$MESS["CRM_AUTOMATION_TUNNEL_ADD_ERROR_SAME_CATEGORY"] = "Não é possível criar túnel dentro do funil";
$MESS["CRM_AUTOMATION_TUNNEL_ROBOT_TITLE"] = "Copiar negócio";
$MESS["CRM_AUTOMATION_TUNNEL_UNAVAILABLE"] = "A criação de túneis não está disponível no seu plano";
?>