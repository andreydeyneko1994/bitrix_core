<?php
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_ALREADY_RUNNING"] = "Não é possível iniciar a mesclagem porque ela já está em andamento.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_RUNNING"] = "Não é possível interromper a mesclagem porque ela não foi iniciada.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_STOPPED"] = "Não é possível retomar a mesclagem porque ela nunca foi iniciada ou interrompida.";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_050"] = "50% das empresas duplicadas mescladas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_090"] = "90% das empresas duplicadas mescladas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_100"] = "Empresas duplicadas foram mescladas";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_050"] = "50% dos contatos duplicados mesclados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_090"] = "90% dos contatos duplicados mesclados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_100"] = "Contatos duplicados foram mesclados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_050"] = "50% dos leads duplicados mesclados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_090"] = "90% dos leads duplicados mesclados";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_100"] = "Leads duplicados foram mesclados";
