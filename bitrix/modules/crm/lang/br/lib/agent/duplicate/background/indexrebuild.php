<?php
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_ALREADY_RUNNING"] = "Não é possível iniciar a reindexação porque ela já está em andamento.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_INDEX_TYPES"] = "Os tipos de índice são indefinidos ou têm valores inválidos.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_RUNNING"] = "Não é possível interromper a reindexação porque ela não foi iniciada.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_STOPPED"] = "Não é possível retomar a reindexação porque ela nunca foi iniciada ou interrompida.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_SCOPE"] = "O código do país não está especificado ou tem um valor inválido.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_TYPE_INDEX"] = "O tipo de índice atual está incorreto.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_050"] = "50% das empresas duplicadas verificadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_090"] = "90% das empresas duplicadas verificadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_100"] = "As empresas duplicadas foram verificadas";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_050"] = "50% dos contatos duplicados verificados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_090"] = "90% dos contatos duplicados verificados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_100"] = "Os contatos duplicados foram verificados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_050"] = "50% dos leads duplicados verificados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_090"] = "90% dos leads duplicados verificados";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_100"] = "Os leads duplicados foram verificados";
