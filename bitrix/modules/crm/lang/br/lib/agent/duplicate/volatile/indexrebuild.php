<?php
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_ALREADY_RUNNING"] = "Não é possível iniciar a reindexação porque ela já está em andamento.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_RUNNING"] = "Não é possível interromper a reindexação porque ela nunca foi iniciada.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_STOPPED"] = "Não é possível retomar a reindexação porque ela não está em andamento nem pausada.";
