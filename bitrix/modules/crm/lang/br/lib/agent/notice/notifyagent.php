<?
$MESS["CRM_AGENT_NOTICE_NOTIFY_AGENT_ABOUT_REPEAT_LEAD"] = "Leads repetidos estão ativados no seu Bitrix24! Sempre que um cliente existente contatar você, o CRM irá reconhecê-lo e criar um lead repetido para que uma pessoa responsável possa processá-lo e fazer uma nova venda. <a href=\"https://helpdesk.bitrix24.com/open/7348303/\">Informações</a>";
?>