<?
$MESS["CRM_DEAL_RECURRING_ENTITY_ACTIVE_FIELD"] = "Ativo";
$MESS["CRM_DEAL_RECURRING_ENTITY_BASED_ID_FIELD"] = "Criado com base no negócio";
$MESS["CRM_DEAL_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Nova categoria de negócios";
$MESS["CRM_DEAL_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Número de instâncias criadas";
$MESS["CRM_DEAL_RECURRING_ENTITY_DEAL_ID_FIELD"] = "ID do negócio recorrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_DEAL_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Método de restrição";
$MESS["CRM_DEAL_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Data em que o último negócio recorrente foi gerado";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Por data";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Por número de instâncias";
$MESS["CRM_DEAL_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Data em que o próximo negócio recorrente será gerado";
$MESS["CRM_DEAL_RECURRING_ENTITY_PARAMS_FIELD"] = "Parâmetros para calcular o próximo negócio recorrente";
$MESS["CRM_DEAL_RECURRING_ENTITY_START_DATE_FIELD"] = "Data para começar a criar negócios recorrentes a partir de";
?>