<?php
$MESS["CRM_PRESET_ALL_COMPANIES"] = "Todas as empresas";
$MESS["CRM_PRESET_ALL_CONTACTS"] = "Todos os contatos";
$MESS["CRM_PRESET_CLOSED_DEALS"] = "Negócios fechados";
$MESS["CRM_PRESET_CLOSED_LEADS"] = "Leads fechados";
$MESS["CRM_PRESET_CLOSED_QUOTES"] = "Cotações fechadas";
$MESS["CRM_PRESET_CLOSED_SI"] = "Faturas fechadas";
$MESS["CRM_PRESET_DEALS_IN_ROBOT_DEBUGGER"] = "Negócios em depuração";
$MESS["CRM_PRESET_IN_WORK_DEALS"] = "Negócios em andamento";
$MESS["CRM_PRESET_IN_WORK_LEADS"] = "Leads em andamento";
$MESS["CRM_PRESET_IN_WORK_QUOTES"] = "Em andamento";
$MESS["CRM_PRESET_IN_WORK_SI"] = "Faturas em andamento";
$MESS["CRM_PRESET_MY_COMPANIES"] = "Minhas empresas";
$MESS["CRM_PRESET_MY_CONTACTS"] = "Meus contatos";
$MESS["CRM_PRESET_MY_DEALS"] = "Meus negócios";
$MESS["CRM_PRESET_MY_LEADS"] = "Meus Leads";
$MESS["CRM_PRESET_MY_QUOTES"] = "Minhas cotações";
$MESS["CRM_PRESET_MY_SI"] = "Minhas faturas";
