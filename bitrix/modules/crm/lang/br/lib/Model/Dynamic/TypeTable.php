<?php
$MESS["CRM_TYPE_TABLE_DELETE_ERROR_ITEMS"] = "Não é possível excluir o tipo de entidade porque ainda existem itens desse tipo.";
$MESS["CRM_TYPE_TABLE_DISABLING_CATEGORIES_IF_MORE_THAN_ONE"] = "Você não pode desativar pipelines se houver mais de um deles.";
$MESS["CRM_TYPE_TABLE_DISABLING_RECYCLEBIN_WHILE_NOT_EMPTY"] = "Remova todos os itens deste tipo da Lixeira antes de desativar.";
$MESS["CRM_TYPE_TABLE_FIELD_NOT_CHANGEABLE_ERROR"] = "Não é possível alterar o valor de \"#FIELD#\".";
