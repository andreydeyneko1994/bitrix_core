<?php
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL"] = "#QTY# correspondências por valor #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_ENTITY_TOTAL_EXCEEDED"] = "Mais de #QTY# correspondências por valor #VALUE# de \"#FIELD#\"";
$MESS["CRM_DUP_CRITERION_VOL_SUMMARY"] = "Correspondência por valor \"#VALUE#\" de \"#FIELD#\"";
