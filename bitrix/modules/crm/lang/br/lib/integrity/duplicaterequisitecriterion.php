<?
$MESS["CRM_DUP_CRITERION_RQ_ENTITY_TOTAL"] = "#QTY# por informações da empresa \"#TITLE#\" com valor #DESCR#";
$MESS["CRM_DUP_CRITERION_RQ_ENTITY_TOTAL_EXCEEDED"] = "Mais do que #QTY# por informações da empresa \"#TITLE#\" com valor #DESCR#";
$MESS["CRM_DUP_CRITERION_RQ_SUMMARY"] = "Os dados \"#TITLE#\" correspondem a #DESCR#";
?>