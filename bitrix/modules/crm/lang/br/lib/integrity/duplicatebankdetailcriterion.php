<?
$MESS["CRM_DUP_CRITERION_BD_ENTITY_TOTAL"] = "#QTY# por dados bancários \"#TITLE#\" com valor #DESCR#";
$MESS["CRM_DUP_CRITERION_BD_ENTITY_TOTAL_EXCEEDED"] = "Mais do que #QTY# por dados bancários \"#TITLE#\" com valor #DESCR#";
$MESS["CRM_DUP_CRITERION_BD_SUMMARY"] = "Os dados bancários \"#TITLE#\" correspondem a #DESCR#";
?>