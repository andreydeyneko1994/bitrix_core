<?php
$MESS["CRM_DUP_INDEX_TYPE_COMM_BITRIX24"] = "Bitrix24.Network";
$MESS["CRM_DUP_INDEX_TYPE_COMM_EMAIL"] = "E-mail";
$MESS["CRM_DUP_INDEX_TYPE_COMM_FACEBOOK"] = "Facebook";
$MESS["CRM_DUP_INDEX_TYPE_COMM_OPENLINE"] = "Canal Aberto";
$MESS["CRM_DUP_INDEX_TYPE_COMM_PHONE"] = "Telefone";
$MESS["CRM_DUP_INDEX_TYPE_COMM_SKYPE"] = "Skype";
$MESS["CRM_DUP_INDEX_TYPE_COMM_SLUSER"] = "Usuário vinculado";
$MESS["CRM_DUP_INDEX_TYPE_COMM_TELEGRAM"] = "Telegram";
$MESS["CRM_DUP_INDEX_TYPE_COMM_VIBER"] = "Viber";
$MESS["CRM_DUP_INDEX_TYPE_COMM_VK"] = "VK";
$MESS["CRM_DUP_INDEX_TYPE_ORGANIZATION"] = "Nome da empresa";
$MESS["CRM_DUP_INDEX_TYPE_PERSON"] = "Nome completo";
