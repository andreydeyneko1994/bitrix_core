<?php
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_ENDED_TITLE"] = "Reunião do Zoom encerrada";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_JOINED_TITLE"] = "O cliente clicou no link de convite do Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_START_TITLE"] = "Reunião do Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_ERROR_INCORRECT_DATETIME"] = "A reunião não pode ser agendada para o horário selecionado.";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_MESSAGE_SENT_TITLE"] = "Notificação de reunião do Zoom enviada ao cliente";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_TITLE"] = "Reunião do Zoom";
