<?php
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_DURATION"] = "Duração: #DURATION#";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_DURATION_LONG"] = "#MIN# m. #SEC# s.";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_DURATION_SHORT"] = "#SEC# s.";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_STATUS_MISSED"] = "Chamada perdida";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_STATUS_SUCCESS"] = "Chamada bem sucedida";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_TITLE"] = "Rastreador de chamada";
$MESS["CRM_ACTIVITY_PROVIDER_CALL_TRACKER_TYPE_TITLE"] = "Rastreador de chamada";
