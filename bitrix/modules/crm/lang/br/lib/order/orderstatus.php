<?php
$MESS["CRM_ORDER_STATUS_FINISHED"] = "Concluído";
$MESS["CRM_ORDER_STATUS_INITIAL"] = "Pedido aceito, aguardando pagamento";
$MESS["CRM_ORDER_STATUS_PAID"] = "Pago, preparando para envio";
$MESS["CRM_ORDER_STATUS_REFUSED"] = "Cancelado";
$MESS["CRM_ORDER_STATUS_SEND"] = "Enviado";
