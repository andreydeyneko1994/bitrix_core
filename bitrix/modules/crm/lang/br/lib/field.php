<?php
$MESS["CRM_FIELD_NOT_UNIQUE_ERROR"] = "O valor de \"#FIELD#\" não é único";
$MESS["CRM_FIELD_NOT_VALID_ERROR"] = "O valor de \"#FIELD#\" está incorreto";
$MESS["CRM_FIELD_VALUE_REQUIRED_ERROR"] = "O campo \"#FIELD#\" é obrigatório";
