<?
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "Os relatórios exigem que você <a id=\"#ID#\" href=\"#URL#\">atualize as estatísticas de negócios</a> para executar corretamente.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Isso irá atualizar as estatísticas de negócios. Isso pode levar um longo tempo.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Atualizar estatísticas de Negócios";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Soma total";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "Negócios";
?>