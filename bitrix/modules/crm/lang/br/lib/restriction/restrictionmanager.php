<?php
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Canais Abertos extras estão disponíveis somente nos planos comerciais.";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class=\"crm-conditionally-required-field-tab-content\">
	<div class=\"crm-conditionally-required-field-tab-text\">
		Os campos obrigatórios específicos da etapa estão disponíveis apenas nos <a href=\"/settings/license_all.php\" target=\"_blank\">planos comerciais selecionados</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class=\"crm-deal-category-tab-content\">
	<div class=\"crm-deal-category-tab-text\">
		Seu plano atual restringe o número de pipelines de negócios que você pode usar. Por favor, faça o upgrade do seu plano para <a href=\"/settings/license_all.php\" target=\"_blank\">os planos comerciais selecionados</a> para criar pipelines envolvendo diversos produtos e estratégias de vendas para analisar vários caminhos de negócios. 
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Pipelines Múltiplos de CRM";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "Deixe o Bitrix24 criar negócios para você!
Negócios recorrentes vão economizar seu tempo e recursos em várias ocasiões. Por exemplo, se seu plano é assinar contratos semanais durante o próximo ano - negócios recorrentes serão sua libertação com certeza.
Crie negócios recorrentes, defina o cronograma e o pipeline. Novos negócios serão criados no horário especificado, e você não terá que levantar um dedo.
 <ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item\">Crie negócios automaticamente</li>
 <li class=\"hide-features-list-item\">Melhore o desempenho e a eficiência de seus funcionários</li>
 <li class=\"hide-features-list-item\">Acesso rápido aos atuais negócios recorrentes- <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Saiba mais</a></li>
 </ul>
 <strong>Negócios recorrentes estão disponíveis nos planos comerciais selecionados</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "Negócios recorrentes estão disponíveis nos planos comerciais";
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Pesquisa avançada de duplicados</h3>
<div class=\"crm-duplicate-tab-text\">
Quando um novo contato é criado (ou cliente potencial ou empresa), o Bitrix24 localiza e mostra possíveis duplicados &mdash; impedindo antecipadamente a criação de duplicados.
</div>
<div class=\"crm-duplicate-tab-text\">
Na pesquisa avançada de duplicados, o CRM também pode encontrar duplicados em dados importados e em dados já inseridos no banco de dados. Esses duplicados podem ser associados. 
</div>
<div class=\"crm-duplicate-tab-text\">
Adicione estes e outros excelentes recursos ao seu Bitrix24! Telefonia Avançada + CRM Avançado e outros recursos úteis estão disponíveis nos planos comerciais selecionados. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
</div>
<div class=\"ui-btn-container ui-btn-container-center\">
<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Obtenha um plano estendido</span>
<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_lICENSE_SCRIPT#\">Obtenha um teste gratuito de 30 dias</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class=\"crm-history-tab-content\">
	 <h3 class=\"crm-history-tab-title\">A mudança de histórico de CRM está disponível apenas no CRM avançado</h3>
	 <div class=\"crm-history-tab-text\">
		 O Bitrix24 mantém um registro detalhado de todas as mudanças de CRM. Quando você faz upgrade para o CRM avançado, você verá o histórico de mudanças (ou seja, quem acessou ou alterou a entrada de CRM) e poderá restabelecer valores anteriores, se necessário.
	</div>
	 <div class=\"crm-history-tab-text\">Este é o seu aspecto:</div>
	 <img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	 <div class=\"crm-history-tab-text\">
		 Adicione estes e outros excelentes recursos ao seu Bitrix24! Telefonia Avançada + CRM Avançado e outros recursos úteis estão disponíveis nos planos comerciais selecionados. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
	 </div>
	 <div class=\"ui-btn-container ui-btn-container-center\">
		 <span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Ir para o plano estendido</span>
		 <span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_lICENSE_SCRIPT#\">Grátis por 30 dias</span>
	 </div>
 </div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "Use o recurso de Faturamento Automático para cobrar seus clientes regularmente e economizar tempo. 
Crie uma fatura e especifique quantas vezes você deseja que ela seja enviada no futuro. O sistema irá criar e enviar uma nova fatura para o cliente de acordo com o período de sua preferência.
 <ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item\">Economize seu tempo com o Faturamento Automático</li>
 <li class=\"hide-features-list-item\">Acesso rápido a todos os modelos de faturamento automático</li>
 <li class=\"hide-features-list-item\">Envie a fatura para o e-mail do cliente <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Leia mais</a></li>
 </ul>
 <strong>O Faturamento Automático está disponível nos planos comerciais selecionados.</strong>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "O Faturamento Automático está disponível nos planos comerciais selecionados.";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
 		O plano free atribui as mesmas permissões de acesso a todos os funcionários. Considere fazer o upgrade para um dos planos comerciais para atribuir diferentes funções, ações e dados a vários usuários.	
		Para saber mais sobre os diferentes planos, vá para a <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">página de comparação de planos</a>.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		No seu plano atual, todos os funcionários compartilham permissões unificadas. Faça o upgrade para um dos principais planos para definir funções, atribuir permissões e especificar dados que seus funcionários podem acessar.
		<a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">Saiba mais sobre os planos e compare os recursos incluídos</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "Para atribuir diferentes permissões de acesso aos seus funcionários, faça o upgrade para um dos planos comerciais.";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "Adicione o seguinte ao seu CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversão entre negócios, faturas e orçamentos</li>
 <li class=\"hide-features-list-item\">Pesquisa expandida de duplicados</li>
 <li class=\"hide-features-list-item\">Mude o histórico para reversão e recuperação de CRM</li>
 <li class=\"hide-features-list-item\">Log de acesso de CRM</li>
 <li class=\"hide-features-list-item\">Visualização de mais de 5000 registros no CRM</li>
 <li class=\"hide-features-list-item\">Chamada acionada pela lista<sup class=\"hide-features-soon\">em breve</sup></li>
 <li class=\"hide-features-list-item\">Volume de e-mails para clientes<sup class=\"hide-features-soon\">em breve</sup></li>
 <li class=\"hide-features-list-item\">Suporte para vendas de serviços<sup class=\"hide-features-soon\">em breve</sup>
 <a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
 </li>
 </ul>
 <strong>Telefonia Avançada + CRM Avançado e outros recursos úteis estão disponíveis nos planos comerciais selecionados.</strong>
";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM Avançado no Bitrix24";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Vendas Automatizadas por CRM Bitrix24<br><br>
	Crie cenários para o CRM alavancar seus negócios: atribua tarefas, planeje reuniões, lance campanhas de anúncios direcionados e emita faturas. As regras de automação irão orientar o seu gerente nos próximos passos em cada etapa do negócio para concluir a tarefa.<br><br>
	Os gatilhos reagem automaticamente às ações do cliente (quando o site é visitado, o formulário é preenchido, um comentário é postado em rede social ou uma chamada é feita) e lança uma regra de automação ajudando a transformar o lead em um negócio.<br><br>
	As regras e gatilhos de automação fazem todas as operações de rotina sem nenhum treinamento dos seus gerentes: configure uma só vez e continue trabalhando!<br><br>
	Adicione regras de automação e aumente suas vendas!<br><br>
	Esta ferramenta está disponível apenas em assinaturas comerciais Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Regras de automação";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "A assinatura pode ser removida somente em planos comerciais.";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "Formulários expandidos de CRM";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Seu plano atual limita o número de formulários de CRM que você pode ter. Para adicionar novos formulários, faça um upgrade do seu plano. 
<br><br>
DICA: Alguns planos comerciais vem com formulários de CRM ilimitados.
<br><br>
Experimente o Bitrix24 Professional gratuitamente por 30 dias.";
