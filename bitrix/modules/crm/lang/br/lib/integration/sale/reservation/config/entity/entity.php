<?php
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_CODE"] = "Itens não enviados de venda automática ao concluir negócio";
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_DESCRIPTION"] = "Quando o negócio for concluído com sucesso, o sistema verificará itens não enviados e criará um pedido de venda.";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE"] = "Modo de reserva";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_MANUAL"] = "Manual";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_ADD_TO_DOCUMENT"] = "Ao adicionar produto ao negócio";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_PAYMENT"] = "No pagamento";
$MESS["CRM_SALE_RESERVATION_CONFIG_PERIOD"] = "Reservar por, dias";
$MESS["CRM_SALE_RESERVATION_ENTITY_DEAL"] = "Negócio";
$MESS["CRM_SALE_RESERVATION_ENTITY_INVOICE"] = "Fatura";
$MESS["CRM_SALE_RESERVATION_ENTITY_OFFER"] = "Orçamento";
$MESS["CRM_SALE_RESERVATION_ENTITY_SMARTPROCESS"] = "Automação Inteligente de Processos";
