<?php
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "Clique em \"Exportar\" para salvar seus formulários de CRM em um arquivo";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "Clique em \"Exportar\" para salvar sua predefinição de CRM em um arquivo";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "Exportar formulários de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "Exportar solução predefinida de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "Prepare-se para usar formulários de CRM na sua empresa em apenas alguns minutos!";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "Obtenha um CRM pronto para uso no seu negócio em apenas alguns minutos!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "Exportar formulários de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "Exportar solução predefinida de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "Formulários de CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "Solução predefinida de CRM";
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Ocorreram erros ao excluir os dados: entidade ignorada (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Ocorreram erros ao exportar os dados: entidade ignorada (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Já existe m campo com o código #CODE# de outro tipo. Exclua este campo e comece a importação novamente.";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Ocorreram erros ao importar os dados: entidade ignorada (#CODE#)";
