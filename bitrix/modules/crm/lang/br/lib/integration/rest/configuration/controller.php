<?php
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Ocorreram erros durante a exclusão de dados: entidade ignorada (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Ocorreram erros durante a exportação de dados: entidade ignorada (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Ocorreram erros durante a importação de dados: entidade ignorada (#CODE#)";
