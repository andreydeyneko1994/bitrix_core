<?
$MESS["CRM_DOCUMENTGENERATOR_ADD_NEW_TEMPLATE"] = "Adicionar novo modelo";
$MESS["CRM_DOCUMENTGENERATOR_DOCUMENTS_LIST"] = "Documentos";
$MESS["CRM_DOCUMENTGENERATOR_SPOTLIGHT_TEXT"] = "Agora você pode trabalhar com modelos de documentos. Crie um documento, depois imprima ou envie por e-mail.";
?>