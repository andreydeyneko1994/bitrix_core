<?
$MESS["CRM_CALENDAR_HELP_LINK"] = "Saiba mais";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_DEAL"] = "Selecione o modo preferido de exibição de negócio para uso no calendário: por data de criação, por outra data ou disponibilidade de recursos.";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_LEAD"] = "Selecione o modo preferido de exibição de lead para uso no calendário: por data criada, por outra data ou disponibilidade de recursos.";
$MESS["CRM_CALENDAR_VIEW_SPOTLIGHT"] = "Agora você pode visualizar leads e negócios no modo de calendário. Planeje seus relacionamentos com clientes, mantendo-se atento à disponibilidade de recursos.";
?>