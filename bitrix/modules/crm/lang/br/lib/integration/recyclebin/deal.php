<?
$MESS["CRM_RECYCLE_BIN_DEAL_ENTITY_NAME"] = "Negócio";
$MESS["CRM_RECYCLE_BIN_DEAL_RECOVERY_CONFIRMATION"] = "Você deseja recuperar o negócio selecionado?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVAL_CONFIRMATION"] = "O negócio será excluído irreversivelmente. Você deseja prosseguir?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVED"] = "O negócio foi excluído.";
$MESS["CRM_RECYCLE_BIN_DEAL_RESTORED"] = "O negócio foi recuperado com sucesso.";
?>