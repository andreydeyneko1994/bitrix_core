<?
$MESS["CRM_RECYCLE_BIN_ACTIVITY_ENTITY_NAME"] = "Atividade";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RECOVERY_CONFIRMATION"] = "Você deseja recuperar a atividade selecionada?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVAL_CONFIRMATION"] = "A atividade será excluída irreversivelmente. Você deseja prosseguir?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVED"] = "A atividade foi excluída.";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RESTORED"] = "A atividade foi recuperada com sucesso.";
?>