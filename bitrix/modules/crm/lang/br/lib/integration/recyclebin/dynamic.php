<?php
$MESS["CRM_RECYCLE_BIN_DYNAMIC_ENTITY_NAME"] = "Item";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RECOVERY_CONFIRMATION"] = "Você deseja recuperar o item selecionado?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVAL_CONFIRMATION"] = "O item será excluído permanentemente. Deseja continuar?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVED"] = "O item foi excluído.";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RESTORED"] = "O item foi recuperado com sucesso.";
