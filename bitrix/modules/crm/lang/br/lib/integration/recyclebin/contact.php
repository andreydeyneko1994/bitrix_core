<?
$MESS["CRM_RECYCLE_BIN_CONTACT_ENTITY_NAME"] = "Contato";
$MESS["CRM_RECYCLE_BIN_CONTACT_RECOVERY_CONFIRMATION"] = "Você deseja recuperar o contato selecionado?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVAL_CONFIRMATION"] = "O contato será excluído irreversivelmente. Você deseja prosseguir?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVED"] = "O contato foi excluído.";
$MESS["CRM_RECYCLE_BIN_CONTACT_RESTORED"] = "O contato foi recuperado com sucesso.";
?>