<?
$MESS["CRM_RECYCLE_BIN_COMPANY_ENTITY_NAME"] = "Empresa";
$MESS["CRM_RECYCLE_BIN_COMPANY_RECOVERY_CONFIRMATION"] = "Você deseja recuperar a empresa selecionada?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVAL_CONFIRMATION"] = "A empresa será excluída irreversivelmente. Você deseja prosseguir?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVED"] = "A empresa foi excluída.";
$MESS["CRM_RECYCLE_BIN_COMPANY_RESTORED"] = "A empresa foi recuperada com sucesso.";
?>