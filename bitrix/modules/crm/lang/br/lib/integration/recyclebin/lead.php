<?
$MESS["CRM_RECYCLE_BIN_LEAD_ENTITY_NAME"] = "Lead";
$MESS["CRM_RECYCLE_BIN_LEAD_RECOVERY_CONFIRMATION"] = "Você deseja recuperar o Lead selecionado?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVAL_CONFIRMATION"] = "O Lead será excluído irreversivelmente. Você deseja prosseguir?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVED"] = "O Lead foi excluído.";
$MESS["CRM_RECYCLE_BIN_LEAD_RESTORED"] = "O Lead foi recuperado com sucesso.";
?>