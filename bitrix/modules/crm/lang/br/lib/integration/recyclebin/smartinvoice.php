<?php
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_ENTITY_NAME"] = "Fatura";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RECOVERY_CONFIRMATION"] = "Deseja recuperar a fatura selecionada?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVAL_CONFIRMATION"] = "A fatura será excluída permanentemente. Deseja continuar?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVED"] = "A fatura foi excluída.";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RESTORED"] = "A fatura foi recuperada com sucesso.";
