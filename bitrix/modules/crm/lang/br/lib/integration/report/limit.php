<?
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK"] = "Para contornar a restrição, exclua as empresas desnecessárias ou faça upgrade para um dos planos superiores.";
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK_DELETE"] = "Exclua as empresas que não precisa mais para remover a restrição. <br> <a href=\"#MORE_INFO_LINK#\"> Detalhes </a>";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK"] = "Para contornar a restrição, exclua os contatos desnecessários ou faça upgrade para um dos planos superiores.";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK_DELETE"] = "Exclua os contatos que não precisa mais para remover a restrição. <br> <a href=\"#MORE_INFO_LINK#\"> Detalhes </a>";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK"] = "Para contornar a restrição, exclua os negócios desnecessários ou faça upgrade para um dos planos superiores.";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK_DELETE"] = "Exclua os negócios que não precisa mais para remover a restrição. <br> <a href=\"#MORE_INFO_LINK#\"> Detalhes </a>";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK"] = "Para contornar a restrição, exclua os leads desnecessários ou faça upgrade para um dos planos superiores.";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK_DELETE"] = "Exclua os leads que não precisa mais para remover a restrição. <br> <a href=\"#MORE_INFO_LINK#\"> Detalhes </a>";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_ACTUAL_COUNT_MASK"] = "Quantidade atual de empresas: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_MAX_COUNT_MASK"] = "Até #MAX_COUNT# empresas podem ser incluídas em um relatório.";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_ACTUAL_COUNT_MASK"] = "Quantidade atual de contatos: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_MAX_COUNT_MASK"] = "Até #MAX_COUNT# contatos podem ser incluídos em um relatório.";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_ACTUAL_COUNT_MASK"] = "Quantidade atual de negócios: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_MAX_COUNT_MASK"] = "Até #MAX_COUNT# negócios podem ser incluídos em um relatório.";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_ACTUAL_COUNT_MASK"] = "Quantidade atual de leads: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_MAX_COUNT_MASK"] = "Até #MAX_COUNT# leads podem ser incluídos em um relatório.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT"] = "Apenas um determinado número de entidades de CRM pode ser incluído em um relatório analítico.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT_FOR_MORE_INFO"] = "Consulte a página de comparação de planos para obter mais detalhes.";
?>