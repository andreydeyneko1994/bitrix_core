<?
$MESS["CRM_REPORT_FILTER_CURRENT_MONTH_PRESET_TITLE"] = "Mês atual";
$MESS["CRM_REPORT_FILTER_LAST_30_DAYS_PRESET_TITLE"] = "Últimos 30 dias";
$MESS["CRM_REPORT_FILTER_LAST_MONTH_PRESET_TITLE"] = "Mês anterior";
$MESS["CRM_REPORT_FILTER_LAST_QUARTER_PRESET_TITLE"] = "Trimestre anterior";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_FILTER_DEAL_FIELDS_POSTFIX"] = "(negócios)";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_FILTER_LEAD_FIELDS_POSTFIX"] = "(leads)";
?>