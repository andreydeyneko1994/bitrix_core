<?
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_AMOUNT_TITLE"] = "Total:";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_CONVERSION_TITLE"] = "Conversão";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_GROUPING_COLUMN_TITLE"] = "Funcionário";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_ORDER_COUNT_TITLE"] = "Pedidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_ORDER_LOSES_COUNT_TITLE"] = "Pedidos perdidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_ORDER_SUM_TITLE"] = "Total de pedidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_TITLE"] = "Desempenho do funcionário";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_WON_ORDER_COUNT_TITLE"] = "Pedidos bem-sucedidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_WON_ORDER_SUM_TITLE"] = "Total de negócios ganhos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_CONVERSION_TITLE"] = "Conversão";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_GOOD_ORDER_METRIC_TITLE"] = "Pedidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_SUCCESS_ORDER_TITLE"] = "Pedidos bem-sucedidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_TITLE"] = "Funil de vendas";
?>