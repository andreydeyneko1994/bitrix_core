<?
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_AMOUNT_TITLE"] = "Total:";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_CONVERSION_TITLE"] = "Conversão";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_DEAL_COUNT_TITLE"] = "Número de negócios";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_DEAL_LOSES_COUNT_TITLE"] = "Negócios perdidos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_DEAL_SUM_TITLE"] = "Total de negócios";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_GROUPING_COLUMN_TITLE"] = "Gerente";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_LEAD_COUNT_TITLE"] = "Número de leads";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_TITLE"] = "Desempenho do funcionário";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_WON_DEAL_COUNT_TITLE"] = "Negócios realizados";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_MANAGER_EFFICIENCY_GRID_WON_DEAL_SUM_TITLE"] = "Valor dos negócios realizados";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_CONVERSION_TITLE"] = "Conversão";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_GOOD_DEAL_METRIC_TITLE"] = "Negócios";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_GOOD_LEAD_COUNT_TITLE"] = "Leads";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_SUCCESS_DEAL_TITLE"] = "Negócios ganhos";
$MESS["CRM_REPORT_SALES_FUNNEL_BOARD_SALES_FUNNEL_TITLE"] = "Canal de vendas";
?>