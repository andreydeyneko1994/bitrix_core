<?php
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_DEALS_COUNT_EXACT_1"] = "#COUNT# negócio";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_DEALS_COUNT_EXACT_2"] = "#COUNT# negócios";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_DEALS_COUNT_EXACT_5"] = "#COUNT# negócios";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_DEAL_COUNT_BETWEEN"] = "#COUNT_FROM# a #COUNT_TO# negócios";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_DEAL_COUNT_MORE"] = "Mais de #COUNT# negócios";
