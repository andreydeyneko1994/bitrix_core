<?
$MESS["CRM_REPORT_SALES_DYNAMIC_FIRST_LEAD_TITLE"] = "Inicial";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_CONVERSION_TITLE"] = "Conversão";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_DEAL_SUM_TITLE"] = "Total de negócios";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_FIRST_DEAL_WON_SUM_TITLE"] = "Inicial";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_LOSES_SUM_TITLE"] = "Perdido";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_REPEAT_DEAL_WON_SUM_TITLE"] = "Repetição";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_TITLE"] = "Tendência de vendas (dinheiro)";
$MESS["CRM_REPORT_SALES_DYNAMIC_MANAGER_GRID_AMOUNT_TITLE"] = "Total";
$MESS["CRM_REPORT_SALES_DYNAMIC_MANAGER_GRID_GROUPING_COLUMN_TITLE"] = "Gerente";
$MESS["CRM_REPORT_SALES_DYNAMIC_REPEAT_LEAD_TITLE"] = "Repetição";
$MESS["CRM_REPORT_SALES_DYNAMIC_TITLE"] = "Tendências de vendas";
?>