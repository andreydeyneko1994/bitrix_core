<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Nenhuma entidade de dados bancários especificada";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidade de dados bancários incorreto";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Nenhum dado bancário encontrado para ser excluído";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Erro ao excluir dados bancários";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ACTIVE"] = "Ativo";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COMMENTS"] = "Comentário";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_ID"] = "ID do país";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_NAME"] = "País";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ID"] = "ID";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_NAME"] = "Nome";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_SORT"] = "Ordem de Classificação";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MAX"] = "O comprimento de \"#FIELD_TITLE#\" não deve exceder #MAX_LENGTH# caracteres.";
$MESS["CRM_BANK_DETAIL_FIELD_VALIDATOR_LENGTH_MIN"] = "O comprimento de \"#FIELD_TITLE#\" deve ser de pelo menos #MAX_LENGTH# caracteres.";
?>