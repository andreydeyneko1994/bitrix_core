<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Erro genérico.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Não é possível encontrar o tipo de pagador.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "O modelo selecionado não possui campos que possam ser preenchidos usando as informações existentes.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Não é possível transferir dados porque as informações da fatura existente foram excluídas.";
?>