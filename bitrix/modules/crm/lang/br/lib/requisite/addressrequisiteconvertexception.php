<?
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "O acesso à empresa foi negado";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "Não é possível criar um item de informações da empresa";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "O acesso ao contato foi negado";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "Não é possível criar um item de informações do contato";
?>