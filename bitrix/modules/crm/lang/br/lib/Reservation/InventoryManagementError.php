<?php
$MESS["CRM_RESERVATION_INVENTORY_MANAGEMENT_ERROR"] = "Não é possível fechar o negócio porque alguns dos produtos não estão disponíveis no depósito ou você selecionou um depósito errado. Isso faz com que o pedido de venda não seja registrado no Gerenciamento de Estoque. Remova do negócio os produtos que você não pode entregar ou selecione um depósito diferente com estoque suficiente.";
