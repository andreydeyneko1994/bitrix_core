<?
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_OCCURRED"] = "Não é possível mesclar automaticamente devido a conflito de dados.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_RESOLUTION_NOT_SUPPORTED"] = "O método de resolução de conflitos \"#RESOLUTION_CAPTION#\" não é suportado.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_DENIED"] = "Nenhum acesso excluído para\"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_FAILED"] = "Não foi possível excluir #TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_ERROR"] = "Ocorreu um erro ao mesclar. Mensagem de erro: #ERROR#";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_NOT_FOUND"] = "Não foi possível encontrar o item [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_READ_DENIED"] = "Nenhum acesso lido para \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_DENIED"] = "Nenhum acesso atualizado para\"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_FAILED"] = "Não foi possível salvar \"#TITLE#\" [#ID#].";
?>