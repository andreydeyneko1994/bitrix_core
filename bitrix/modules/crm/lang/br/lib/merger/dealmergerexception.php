<?
$MESS["CRM_DEAL_MERGER_EXCEPTION_CONFLICT_OCCURRED_CATEGORY"] = "Os negócios que você deseja mesclar precisam ter o mesmo pipeline.";
$MESS["CRM_DEAL_MERGER_EXCEPTION_CONFLICT_OCCURRED_RECURRENCE"] = "Os negócios que você deseja mesclar precisam ter as mesmas propriedades de repetição.";
?>