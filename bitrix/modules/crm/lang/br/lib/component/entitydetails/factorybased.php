<?php
$MESS["CRM_COMPONENT_FACTORYBASED_COPY_PAGE_URL"] = "Copiar link do item para a Área de Transferência";
$MESS["CRM_COMPONENT_FACTORYBASED_EDITOR_MAIN_SECTION_TITLE"] = "Sobre o item";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TEXT"] = "Por padrão, o valor do item é calculado como a soma dos preços dos produtos. No entanto, o item já especifica um valor diferente. Você pode deixar o valor atual inalterado ou confirmar o recálculo.";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TITLE"] = "Alterar o modo de cálculo do valor do item";
$MESS["CRM_COMPONENT_FACTORYBASED_NEW_ITEM_TITLE"] = "Criar #ENTITY_NAME#";
$MESS["CRM_COMPONENT_FACTORYBASED_PAGE_URL_COPIED"] = "O link do item foi copiado para a Área de Transferência";
$MESS["CRM_COMPONENT_FACTORYBASED_TIMELINE_HISTORY_STUB"] = "Você está criando um item...";
