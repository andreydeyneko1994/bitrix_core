<?php
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Nenhum dos seguintes campos foram encontrados: #FIELD_LIST#";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Um arquivo deve seguir estes requisitos para serem importados com sucesso: Condificação: UTF-16; linguagem: Inglês; separador de campo: vírgula.";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS_NEW"] = "Seu arquivo deve seguir estes requisitos para ser importado com sucesso: codificação: UTF-8; idioma do nome do campo: inglês; separador do campo: vírgula.";
