<?php
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BIRTHDAY"] = "Aniversário";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_CITY"] = "Cidade Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_FAX"] = "Fax Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE"] = "Telefone Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE_2"] = "Telefone Comercial 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_CODE"] = "Código Postal Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_COUNTRY"] = "País/Região Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STATE"] = "Estado Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STREET"] = "Rua Comercial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_CAR_PHONE"] = "Telefone do Carro";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY"] = "Empresa";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY_MAIN_PHONE"] = "Telefone Principal da Empresa";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_2_ADDRESS"] = "Endereço de E-mail 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_3_ADDRESS"] = "Endereço de E-mail 3";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_ADDRESS"] = "Endereço de E-mail";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_FIRST_NAME"] = "Nome";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_CITY"] = "Cidade Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_COUNTRY"] = "País/Região Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_FAX"] = "Fax Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE"] = "Telefone Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE_2"] = "Telefone Residencial 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_POSTAL_CODE"] = "Código Postal Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STATE"] = "Estado Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STREET"] = "Rua Residencial";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_JOB_TITLE"] = "Empresa";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_LAST_NAME"] = "Sobrenome";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MIDDLE_NAME"] = "Segundo nome";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MOBILE_PHONE"] = "Telefone Celular";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_NOTES"] = "Notas";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_CITY"] = "Outra Cidade";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_COUNTRY"] = "Outro País/Região";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_FAX"] = "Outro Fax";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_PHONE"] = "Outro Telefone";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_POSTAL_CODE"] = "Outro Código Postal";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STATE"] = "Outro Estado";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STREET"] = "Outra Rua";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PAGER"] = "Pager";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PRIMARY_PHONE"] = "Telefone Principal";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_RADIO_PHONE"] = "Telefone de Rádio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_WEB_PAGE"] = "Página Web";
$MESS["CRM_IMPORT_OUTLOOK_ERROR_FIELDS_NOT_FOUND"] = "Nenhum dos seguintes campos foram encontrados: #FIELD_LIST#";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS"] = "Um arquivo de importação deve atender aos seguintes requisitos para importar corretamente. Codificação de arquivo: #FILE_ENCODING#. Idioma dos títulos das colunas: #FILE_LANG#. Separador de campos: vírgula.";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS_NEW"] = "Seu arquivo deve seguir estes requisitos para ser importado com sucesso: codificação: UTF-8; idioma do nome do campo: #FILE_LANG#; separador do campo: vírgula.";
