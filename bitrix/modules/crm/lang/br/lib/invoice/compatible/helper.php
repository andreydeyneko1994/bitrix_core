<?
$MESS["CRM_INVOICE_COMPAT_HELPER_CANCEL_ERROR"] = "Erro ao cancelar a fatura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DELETE_ERROR"] = "Erro ao excluir a fatura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_CANCEL"] = "O ID da fatura ##ID# já foi cancelado";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_PAY"] = "O ID da fatura ##ID# já foi pago";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_STATUS"] = "O ID da fatura ##ID# já está no status obrigatório";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_ACCOUNT_NUMBER"] = "O número da fatura não pode estar vazio";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_CURRENCY"] = "Moeda não especificada";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_PERS_TYPE"] = "O tipo de pagador não está especificado";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_SITE"] = "O site da fatura não foi especificado";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_USER_ID"] = "O ID do cliente não está especificado";
$MESS["CRM_INVOICE_COMPAT_HELPER_EXISTING_ACCOUNT_NUMBER"] = "O número da fatura especificado já está em uso";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE"] = "O ID da fatura ##ID# não foi encontrado";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE_ID"] = "Está faltando o ID da fatura";
$MESS["CRM_INVOICE_COMPAT_HELPER_PAY_ERROR"] = "Erro ao pagar a fatura. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_CURRENCY"] = "Não é possível encontrar a moeda ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_DELIVERY"] = "Não é possível encontrar o serviço de entrega ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PERSON_TYPE"] = "Não é possível encontrar o tipo de pagador ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PS"] = "Não é possível encontrar o sistema de pagamento ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_SITE"] = "Não é possível encontrar o site ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_STATUS"] = "Não é possível encontrar o status ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_USER"] = "Não é possível encontrar o usuário ##ID#";
?>