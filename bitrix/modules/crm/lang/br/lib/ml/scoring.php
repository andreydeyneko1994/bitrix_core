<?
$MESS["CRM_SCORING_LICENSE_TEXT_P1"] = "Quais são os critérios usados pelo seu gerente de vendas ao lidar com os negócios? Fila? Intuição? Tudo o que você precisa é captar e levar seus clientes mais promissores ao topo da sua lista. A Previsão IA analisará os atuais negócios para mostrar sua probabilidade de sucesso.";
$MESS["CRM_SCORING_LICENSE_TEXT_P2"] = "O sistema ajudará seus funcionários a identificar áreas que exigem mais atenção.";
$MESS["CRM_SCORING_LICENSE_TEXT_P3"] = "A Previsão IA está disponível nos principais planos. Faça um upgrade agora mesmo para melhorar a eficiência do seu departamento de vendas e reduzir os custos com anúncios.";
$MESS["CRM_SCORING_LICENSE_TITLE"] = "Pontuação IA";
?>