<?php
$MESS["CRM_ADDRESS_TYPE_BENEFICIARY"] = "Endereço do beneficiário";
$MESS["CRM_ADDRESS_TYPE_DELIVERY"] = "Endereço de entrega";
$MESS["CRM_ADDRESS_TYPE_HOME"] = "Endereço registrado";
$MESS["CRM_ADDRESS_TYPE_PRIMARY"] = "Endereço rua";
$MESS["CRM_ADDRESS_TYPE_REGISTERED"] = "Endereço jurídico";
