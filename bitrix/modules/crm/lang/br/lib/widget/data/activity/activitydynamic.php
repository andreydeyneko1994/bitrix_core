<?
$MESS["CRM_ACTIVITY_DYNAMIC_CHANNELS_ARE_NOT_FOUND"] = "Não há canais disponíveis.";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECT"] = "Conectar: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED"] = "Conectado: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS"] = "Canais conectados";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_IN"] = "Recebidos";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_OUT"] = "Enviados";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE"] = "Messengers e serviços sociais";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE_1"] = "Primeiro Canal Aberto";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_LIVECHAT"] = "Bate-papo ao vivo";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_VKGROUP"] = "VK";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM"] = "Formulário de CRM";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_1"] = "Informações do Contato";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_2"] = "Formulário de Feedback";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_4"] = "Retorno de chamada do \"Número principal\"";
$MESS["CRM_ACTIVITY_DYNAMIC_TITLE"] = "Visão geral de comunicação";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_1"] = "e outros";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_3"] = "Conectar mais canais: ";
$MESS["CRM_CH_TRACKER_START_VIDEO"] = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GipsvUe1gB0?rel=0&autoplay=1#VOLUME#\" frameborder=\"0\" allowfullscreen></iframe>";
$MESS["CRM_CH_TRACKER_START_VIDEO_TITLE"] = "Você pode ver o vídeo a qualquer momento";
?>