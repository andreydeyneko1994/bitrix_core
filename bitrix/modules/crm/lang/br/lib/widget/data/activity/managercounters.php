<?
$MESS["CRM_MANAGER_CNTR_COMPANY"] = "Empresas";
$MESS["CRM_MANAGER_CNTR_CONTACT"] = "Contatos";
$MESS["CRM_MANAGER_CNTR_DEAL"] = "Negócios";
$MESS["CRM_MANAGER_CNTR_FAILED"] = "Restantes";
$MESS["CRM_MANAGER_CNTR_LEAD"] = "Leads";
$MESS["CRM_MANAGER_CNTR_SUCCEED"] = "Concluídos";
$MESS["CRM_MANAGER_CNTR_TITLE"] = "Contadores do gerente";
$MESS["CRM_MANAGER_CNTR_VIDEO"] = "<div style=\"width:400px;height:100px;font-size:15px;\">Use seu contador pessoal para avaliar seu desempenho: o CRM mantém controle sobre suas tarefas concluídas e restantes para hoje. Seu objetivo é não ter nenhuma tarefa inacabada no final do dia.</div>";
?>