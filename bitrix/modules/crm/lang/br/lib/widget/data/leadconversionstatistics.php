<?
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Leads convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Número de empresas obtidas a partir de Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Número de contatos obtidos a partir de Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Número de Negócios obtidos a partir de Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Número de Leads convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Número de Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Valor total de Leads convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Valor total de Leads";
?>