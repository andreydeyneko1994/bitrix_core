<?
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "Perdido";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "Conversão";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "Nova dinâmica de processamento de Leads ";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "Se você ainda não tiver Leads, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crie um</a> agora mesmo!";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "Esta é uma exibição de demonstração. Oculte-a para acessar a análise de seus Leads.";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "Eficiência no processamento de Leads dos empregados";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "Funil de Leads";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "Número de atividades";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "Número de chamadas";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "O número de Leads descartados";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "O número de Leads inativos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "Número de Leads ativos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "Número de novos Leads";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "Número de Leads convertidos";
$MESS["CRM_LEAD_WGT_RATING"] = "Classificação de Leads convertidos";
$MESS["CRM_LEAD_WGT_SOURCE"] = "Fontes de Leads";
?>