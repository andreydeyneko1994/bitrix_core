<?
$MESS["CRM_DEAL_WGT_DEAL_IN_WORK"] = "Negócios em andamento";
$MESS["CRM_DEAL_WGT_DEMO_CONTENT"] = "Se você não tiver qualquer negócio, adicione seu <a href=\"#URL#\" class=\"#CLASS_NAME#\">primeio</a> agora mesmo!";
$MESS["CRM_DEAL_WGT_DEMO_TITLE"] = "Este é um painel de demonstração, você pode ocultar os dados de demonstração e usar seus dados de Negócio.";
$MESS["CRM_DEAL_WGT_EMPLOYEE_DEAL_IN_WORK"] = "Negócios em andamento (por empregado)";
$MESS["CRM_DEAL_WGT_FUNNEL"] = "Funil de vendas para negócios";
$MESS["CRM_DEAL_WGT_PAYMENT_CONTROL"] = "Controle de pagamento para negócios ganhos";
$MESS["CRM_DEAL_WGT_QTY_ACTIVITY"] = "Contagem de atividade";
$MESS["CRM_DEAL_WGT_QTY_CALL"] = "Número de chamadas";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IDLE"] = "Número de negócios em espera";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IN_WORK"] = "Número de negócios em andamento";
$MESS["CRM_DEAL_WGT_QTY_DEAL_WON"] = "Número de negócios ganhos";
$MESS["CRM_DEAL_WGT_QTY_EMAIL"] = "Número de e-mails";
$MESS["CRM_DEAL_WGT_QTY_MEETING"] = "Número de reuniões";
$MESS["CRM_DEAL_WGT_RATING"] = "Classificação por negócios ganhos";
$MESS["CRM_DEAL_WGT_SUM_DEAL_IN_WORK"] = "Total de negócios em andamento";
$MESS["CRM_DEAL_WGT_SUM_DEAL_OVERALL"] = "Valor total de negócio";
$MESS["CRM_DEAL_WGT_SUM_DEAL_WON"] = "Total de negócios ganhos";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OVERALL"] = "Total de vendas faturadas";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OWED"] = "Total de vendas não faturadas";
?>