<?
$MESS["CRM_INVOICE_RECURRING_ENTITY_ACTIVE_FIELD"] = "Ativo";
$MESS["CRM_INVOICE_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Nova categoria de negócios";
$MESS["CRM_INVOICE_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Número de instâncias criadas";
$MESS["CRM_INVOICE_RECURRING_ENTITY_EMAIL_ID_FIELD"] = "ID do e-mail";
$MESS["CRM_INVOICE_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_INVOICE_RECURRING_ENTITY_INVOICE_ID_FIELD"] = "ID da fatura recorrente";
$MESS["CRM_INVOICE_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Método de restrição";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Data em que a última fatura recorrente foi gerada";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Por data";
$MESS["CRM_INVOICE_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Por número de instâncias";
$MESS["CRM_INVOICE_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Data em que a próxima fatura recorrente será gerada";
$MESS["CRM_INVOICE_RECURRING_ENTITY_PARAMS_FIELD"] = "Parâmetros para calcular a próxima fatura recorrente";
$MESS["CRM_INVOICE_RECURRING_ENTITY_SEND_BILL_FIELD"] = "Enviar fatura por e-mail";
$MESS["CRM_INVOICE_RECURRING_ENTITY_START_DATE_FIELD"] = "Data para começar a criar faturas recorrentes a partir de";
?>