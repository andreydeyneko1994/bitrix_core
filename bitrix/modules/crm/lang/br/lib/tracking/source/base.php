<?php
$MESS["CRM_TRACKING_SOURCE_BASE_ADS_DESC"] = "Fonte de anúncios %name%";
$MESS["CRM_TRACKING_SOURCE_BASE_DESC_OTHER"] = "Outro tráfego inclui todos os clientes que não pertencem a nenhuma das fontes existentes na Inteligência de Vendas.";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_FB"] = "Facebook";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_GOOGLE"] = "Google Ads";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_INSTAGRAM"] = "Instagram";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_ORGANIC"] = "Tráfego orgânico";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_OTHER"] = "Outro tráfego";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_SENDER-MAIL"] = "E-mail marketing";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_VK"] = "VK";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_YANDEX"] = "Yandex.Direct";
$MESS["CRM_TRACKING_SOURCE_BASE_SHORT_NAME_ORGANIC"] = "Orgânico";
$MESS["CRM_TRACKING_SOURCE_BASE_TRAFFIC_DESC"] = "Fonte de tráfego %name%";
