<?php
$MESS["CRM_STATE_CATALOG_PRODUCT_PRICE_CHANGING_BLOCKED"] = "Atualmente, você não pode alterar o preço do produto em um negócio, fatura etc. Aplique desconto para vender um produto por um preço mais baixo. Você pode ativar a edição de preços nas configurações de CRM.";
$MESS["CRM_STATE_ERR_CATALOG_PRODUCT_LIMIT"] = "Número máximo de produtos do catálogo de CRM excedido. Existem atualmente #COUNT# produtos (de máx. #LIMIT#).";
