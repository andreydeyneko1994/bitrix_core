<?
$MESS["CRM_ADS_FORM_TYPE_ERR_DISABLED_FACEBOOK"] = "Devido a mudanças recentes na política de privacidade do Facebook, a integração entre os formulários CRM do Bitrix24 e os Anúncios para Cliente Potenciais do Facebook está temporariamente indisponível. Os técnicos do Facebook estão trabalhando na nova API e prometeram disponibilizar esse recurso para os usuários do Bitrix24 em breve.";
$MESS["CRM_ADS_FORM_TYPE_NAME_FACEBOOK"] = "Anúncios do Facebook";
?>