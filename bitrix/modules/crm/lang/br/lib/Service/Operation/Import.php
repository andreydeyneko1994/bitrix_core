<?php
$MESS["CRM_FIELD_CRATED_TIME_VALUE_IN_FUTURE_ERROR"] = "O valor de \"#FIELD#\" não pode estar no futuro";
$MESS["CRM_FIELD_CRATED_TIME_VALUE_NOT_MONOTONE_ERROR"] = "O valor de \"#FIELD#\" não pode ser inferior ao de quaisquer outros itens";
$MESS["CRM_FIELD_VALUE_CAN_NOT_BE_GREATER_ERROR"] = "O valor de \"#FIELD#\" não pode ser superior a \"#FIELD2#\"";
$MESS["CRM_FIELD_VALUE_ONLY_ADMIN_CAN_SET_ERROR"] = "Apenas o administrador por alterar o valor de \"#FIELD#\"";
