<?php
$MESS["CRM_WEBFORM_FIELD_SYNCHRONIZER_ERR_RES_BOOK"] = "O campo de reserva de recurso \"%fieldCaption%\" não pode ser criado automaticamente. Por favor, crie um campo de reserva de recurso em %entityCaption% e selecione o campo no formulário.";
