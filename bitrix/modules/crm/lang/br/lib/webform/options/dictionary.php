<?php
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_HIDE"] = "Ocultar";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_SHOW"] = "Mostrar";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_EVENT_CHANGE"] = "Mudanças";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY"] = "Qualquer";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY1"] = "Não vazio";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_CONTAIN"] = "Contém";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY"] = "Vazio";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY1"] = "Vazio";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EQUAL"] = "Igual a";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATER"] = "Mais do que";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATEROREQUAL"] = "Mais ou igual a";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESS"] = "Menos do que";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESSOREQUAL"] = "Menos ou igual a";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTCONTAIN"] = "Não contém";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTEQUAL"] = "Não igual a";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_AUDIO"] = "Audio";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_IMAGE"] = "Imagens";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_VIDEO"] = "Vídeo";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_COMPANY_NAME"] = "Nome da empresa";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_LASTNAME"] = "Sobrenome";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_NAME"] = "Nome";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_PHONE"] = "Telefone";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_SECOND_NAME"] = "Nome do meio";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_BUTTON"] = "Botão";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_LINK"] = "Link";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_CLASSIC"] = "Clássico";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_ELEGANT"] = "Oblíquo";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_MODERN"] = "Moderno";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_CENTER"] = "Centro";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_INLINE"] = "Em linha";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_LEFT"] = "Esquerda";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_RIGHT"] = "Direita";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_PANEL"] = "Barra lateral";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_POPUP"] = "Pop-up";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_BOTTOM"] = "Inferior";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_TOP"] = "Superior";
