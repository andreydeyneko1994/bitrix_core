<?php
$MESS["CRM_WEBFORM_OPTIONS_LINK_EMPTY_FIELD_MAPPING"] = "Nenhum campo selecionado para conversão.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_FORM_DUPLICATE_ERROR"] = "O formulário já está conectado ao CRM";
$MESS["CRM_WEBFORM_OPTIONS_LINK_REGISTER_FAILED"] = "Não é possível cadastrar no eventos do formulário.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_TYPE_DUPLICATE"] = "Não é possível conectar o formulário ao mesmo serviço novamente";
$MESS["CRM_WEBFORM_OPTIONS_LINK_UNREGISTER_FAILED"] = "Não é possível cancelar o cadastro dos eventos do formulário.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_WRONG_TYPE"] = "Serviço de vinculação de formulário desconhecido";
