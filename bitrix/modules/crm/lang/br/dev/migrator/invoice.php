<?php
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "Cancelado";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "Enviado";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "Aguardando processamento";
$MESS["CRM_ORDER_STATUS_D"] = "Cancelado";
$MESS["CRM_ORDER_STATUS_F"] = "Concluído";
$MESS["CRM_ORDER_STATUS_N"] = "Pedido aceito, aguardando pagamento";
$MESS["CRM_ORDER_STATUS_P"] = "Pago, preparando para envio";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupo de usuários autorizado a editar as preferências da loja virtual";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administradores da loja virtual";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "Grupo de clientes contendo todos os clientes da loja";
$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "Todos os clientes";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupo de usuários autorizado a usar os recursos da loja virtual";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Parâmetros da loja virtual";
$MESS["SMAIL_FOOTER_BR"] = "Atenciosamente,<br />equipe de suporte.";
$MESS["SMAIL_FOOTER_SHOP"] = "Loja virtual";
$MESS["UP_TYPE_SUBJECT"] = "Notificação de retorno ao estoque";
$MESS["UP_TYPE_SUBJECT_DESC"] = "#USER_NAME# - nome do usuário
#EMAIL# - e-mail do usuário 
#NAME# - nome do produto
#PAGE_URL# - página de detalhes do produto";
