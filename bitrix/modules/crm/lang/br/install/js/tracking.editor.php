<?
$MESS["CRM_TRACKING_EDITOR_END_CHECKING"] = "Terminar";
$MESS["CRM_TRACKING_EDITOR_FOUND_ITEMS"] = "Números de telefone - %phones%, e-mails - %emails%";
$MESS["CRM_TRACKING_EDITOR_NOT_FOUND"] = "Não foram encontradas inserções.";
$MESS["CRM_TRACKING_EDITOR_NOT_SELECTED"] = "Não selecionado";
$MESS["CRM_TRACKING_EDITOR_REPLACEMENT"] = "Troca de número de telefone e e-mail";
$MESS["CRM_TRACKING_EDITOR_SOURCES_NOT_CONFIGURED"] = "Origens não especificadas";
$MESS["CRM_TRACKING_EDITOR_VIEW_ITEMS"] = "Números de telefone e e-mails de origem";
?>