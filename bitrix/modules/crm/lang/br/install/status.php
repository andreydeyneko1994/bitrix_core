<?
$MESS["CRM_STATUSN_D"] = "Não pago";
$MESS["CRM_STATUSN_D_DESCR"] = "Não pago";
$MESS["CRM_STATUSN_N"] = "Nova";
$MESS["CRM_STATUSN_N_DESCR"] = "Nova";
$MESS["CRM_STATUSN_P"] = "Pago";
$MESS["CRM_STATUSN_P_DESCR"] = "Pago";
$MESS["CRM_STATUSN_S"] = "Enviado ao cliente";
$MESS["CRM_STATUSN_S_DESCR"] = "Enviado ao cliente";
$MESS["CRM_STATUS_A"] = "Confirmado";
$MESS["CRM_STATUS_A_DESCR"] = "Confirmado";
$MESS["CRM_STATUS_D"] = "Recusado";
$MESS["CRM_STATUS_D_DESCR"] = "Recusado";
$MESS["CRM_STATUS_N"] = "Rascunho";
$MESS["CRM_STATUS_N_DESCR"] = "Rascunho";
$MESS["CRM_STATUS_P"] = "Concluído";
$MESS["CRM_STATUS_P_DESCR"] = "Concluído";
$MESS["CRM_STATUS_S"] = "Enviar para o cliente";
$MESS["CRM_STATUS_S_DESCR"] = "Enviar para o cliente";
?>