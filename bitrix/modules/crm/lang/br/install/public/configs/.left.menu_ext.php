<?
$MESS["CRM_BP"] = "Processo de Negócio";
$MESS["CRM_CONFIG"] = "Configurações";
$MESS["CRM_CURRENCIES"] = "Moedas";
$MESS["CRM_EXTERNAL_SALE"] = "Lojas virtuais";
$MESS["CRM_FIELDS"] = "Campos personalizados";
$MESS["CRM_GUIDES"] = "Listas de seleção";
$MESS["CRM_PERMS"] = "Permissões de Acesso";
$MESS["CRM_SENDSAVE"] = "Enviar e Salvar Integração";
?>