<?
$MESS["CRM_INVOICE_COMPAT_HELPER_CANCEL_ERROR"] = "Błąd anulowania faktury. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DELETE_ERROR"] = "Błąd usuwania faktury. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_CANCEL"] = "Faktura o ID ##ID# została już anulowana";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_PAY"] = "Faktura o ID ##ID# została już opłacona";
$MESS["CRM_INVOICE_COMPAT_HELPER_DUB_STATUS"] = "Faktura o ID ##ID# posiada już wymagany status";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_ACCOUNT_NUMBER"] = "Nr faktury nie może być pusty";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_CURRENCY"] = "Waluta nie jest określona";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_PERS_TYPE"] = "Nie określono typu płatnika";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_SITE"] = "Nie określono strony faktury";
$MESS["CRM_INVOICE_COMPAT_HELPER_EMPTY_USER_ID"] = "Nie określono ID klienta";
$MESS["CRM_INVOICE_COMPAT_HELPER_EXISTING_ACCOUNT_NUMBER"] = "Określony nr faktury jest już w użyciu";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE"] = "Nie znaleziono faktury o ID: ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_NO_INVOICE_ID"] = "Brak ID faktury";
$MESS["CRM_INVOICE_COMPAT_HELPER_PAY_ERROR"] = "Błąd płacenia faktury. #MESSAGE#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_CURRENCY"] = "Nie można odnaleźć waluty ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_DELIVERY"] = "Nie można znaleźć usługi dostawy ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PERSON_TYPE"] = "Nie można znaleźć typu płatnika ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_PS"] = "Nie można znaleźć systemu płatności ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_SITE"] = "Nie można znaleźć strony ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_STATUS"] = "Nie można znaleźć statusu ##ID#";
$MESS["CRM_INVOICE_COMPAT_HELPER_WRONG_USER"] = "Nie można znaleźć użytkownika ##ID#";
?>