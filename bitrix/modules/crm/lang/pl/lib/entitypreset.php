<?
$MESS["CRM_ENTITY_PRESET_ACTIVE_FIELD"] = "Aktywny";
$MESS["CRM_ENTITY_PRESET_COUNTRY_ID_FIELD"] = "ID kraju";
$MESS["CRM_ENTITY_PRESET_CREATED_BY_ID_FIELD"] = "Utworzone przez";
$MESS["CRM_ENTITY_PRESET_DATE_CREATE_FIELD"] = "Utworzono";
$MESS["CRM_ENTITY_PRESET_DATE_MODIFY_FIELD"] = "Zmodyfikowano";
$MESS["CRM_ENTITY_PRESET_ENTITY_TYPE_ID_FIELD"] = "ID typu elementu";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "Nie można usunąć tego szablonu, ponieważ jest to domyślny szablon firmy.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "Nie można usunąć tego szablonu, ponieważ jest to domyślny szablon kontaktu.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "Szablon nie może być usunięty ponieważ są dane stworzone na nim.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Niewłaściwy typ elementu do użycia z szablonem.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "Szablon nie został odnaleziony.";
$MESS["CRM_ENTITY_PRESET_FIELD_NAME_FIELD"] = "Nazwa";
$MESS["CRM_ENTITY_PRESET_FIELD_TITLE_FIELD"] = "Nazwa w szablonie";
$MESS["CRM_ENTITY_PRESET_ID_FIELD"] = "ID";
$MESS["CRM_ENTITY_PRESET_IN_SHORT_LIST_FIELD"] = "Pokaż w podsumowaniu";
$MESS["CRM_ENTITY_PRESET_MODIFY_BY_ID_FIELD"] = "Zmodyfikowane przez";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Szablon nienazwany";
$MESS["CRM_ENTITY_PRESET_NAME_FIELD"] = "Nazwa";
$MESS["CRM_ENTITY_PRESET_SORT_FIELD"] = "Sortowanie";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_BOOLEAN"] = "Tak/Nie";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DATETIME"] = "Data";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DOUBLE"] = "Numer";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_STRING"] = "String";
$MESS["CRM_ENTITY_PRESET_XML_ID_FIELD"] = "Zewnętrzne ID";
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Szczegóły";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Szablon dla danych kontaktu lub firmy";
?>