<?
$MESS["CRM_SCORING_LICENSE_TEXT_P1"] = "Jakie kryteria stosuje menedżer sprzedaży podczas obsługi deali? Kolejność? Intuicję? Przede wszystkim musisz pracować z najbardziej obiecującymi klientami. AI Ocenianie przeanalizuje istniejące deale, aby wyświetlić prawdopodobieństwo sukcesu.";
$MESS["CRM_SCORING_LICENSE_TEXT_P2"] = "System pomoże Twoim pracownikom zidentyfikować obszary wymagające największej uwagi.";
$MESS["CRM_SCORING_LICENSE_TEXT_P3"] = "Prognoza AI jest dostępna dla głównych planów. Rozszerz teraz, aby poprawić wydajność działu sprzedaży i zmniejszyć koszty reklam.";
$MESS["CRM_SCORING_LICENSE_TITLE"] = "Ocenianie AI";
?>