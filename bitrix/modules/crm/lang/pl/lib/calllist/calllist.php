<?
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Za pomocą listy połączeń zestaw listę klientów oczekujących na rozmowę i utwórz odpowiednie zadanie dla wyznaczonej osoby, która za to odpowiada.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Listy połączeń i inne przydatne funkcje są dostępne począwszy od planu Plus za jedyne 39 zł/mies.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER_2"] = "Listy połączeń i inne przydatne funkcje są dostępne w <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planach płatnych</a>.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Dostępne w planach płatnych";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Utwórz listę połączeń w kilka kliknięć";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Wykonuj połączenia do wszystkich klientów po kolei w jednym oknie";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Kontroluj postępy i wyniki rozmów";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "Dowiedz się więcej";
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Przekroczono maksymalną liczbę elementów. Ustaw filtr, aby wybrać nie więcej niż #LIMIT# elementów.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Lista połączeń";
$MESS["CRM_CALL_LIST_UPDATE"] = "Dodaj do listy połączeń";
?>