<?php
$MESS["CRM_TRACKING_SOURCE_BASE_ADS_DESC"] = "Źródło reklamy %name%";
$MESS["CRM_TRACKING_SOURCE_BASE_DESC_OTHER"] = "Pozostały ruch obejmuje wszystkich klientów, którzy nie należą do żadnego z istniejących źródeł w module Sales Intelligence.";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_FB"] = "Facebook";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_GOOGLE"] = "Google Ads";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_INSTAGRAM"] = "Instagram";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_ORGANIC"] = "Ruch organiczny";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_OTHER"] = "Pozostały ruch";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_SENDER-MAIL"] = "Marketing e-mailowy";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_VK"] = "VK";
$MESS["CRM_TRACKING_SOURCE_BASE_NAME_YANDEX"] = "Yandex.Direct";
$MESS["CRM_TRACKING_SOURCE_BASE_SHORT_NAME_ORGANIC"] = "Organiczny";
$MESS["CRM_TRACKING_SOURCE_BASE_TRAFFIC_DESC"] = "%name% źródło ruchu";
