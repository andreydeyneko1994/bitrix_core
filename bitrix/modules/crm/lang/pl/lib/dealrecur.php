<?
$MESS["CRM_DEAL_RECURRING_ENTITY_ACTIVE_FIELD"] = "Aktywny";
$MESS["CRM_DEAL_RECURRING_ENTITY_BASED_ID_FIELD"] = "Utworzone na podstawie deala";
$MESS["CRM_DEAL_RECURRING_ENTITY_CATEGORY_ID_FIELD"] = "Kategoria nowego deala";
$MESS["CRM_DEAL_RECURRING_ENTITY_COUNTER_REPEAT_FIELD"] = "Liczba utworzonych instancji";
$MESS["CRM_DEAL_RECURRING_ENTITY_DEAL_ID_FIELD"] = "ID deala cyklicznego";
$MESS["CRM_DEAL_RECURRING_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_DEAL_RECURRING_ENTITY_IS_LIMIT_FIELD"] = "Metoda ograniczania";
$MESS["CRM_DEAL_RECURRING_ENTITY_LAST_EXECUTION_FIELD"] = "Data wygenerowania ostatniego cyklicznego deala";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_DATE_FIELD"] = "Według daty";
$MESS["CRM_DEAL_RECURRING_ENTITY_LIMIT_REPEAT_FIELD"] = "Według liczby instancji";
$MESS["CRM_DEAL_RECURRING_ENTITY_NEXT_EXECUTION_FIELD"] = "Data wygenerowania następnego cyklicznego deala";
$MESS["CRM_DEAL_RECURRING_ENTITY_PARAMS_FIELD"] = "Parametry do obliczenia następnego deala cyklicznego";
$MESS["CRM_DEAL_RECURRING_ENTITY_START_DATE_FIELD"] = "Data rozpoczęcia tworzenia powtarzalnych deali od";
?>