<?php
$MESS["CRM_ADDRESS_TYPE_BENEFICIARY"] = "Adres beneficjenta";
$MESS["CRM_ADDRESS_TYPE_DELIVERY"] = "Adres dostawy";
$MESS["CRM_ADDRESS_TYPE_HOME"] = "Zarejestrowany adres";
$MESS["CRM_ADDRESS_TYPE_PRIMARY"] = "Adres rzeczewisty";
$MESS["CRM_ADDRESS_TYPE_REGISTERED"] = "Adres siedziby";
