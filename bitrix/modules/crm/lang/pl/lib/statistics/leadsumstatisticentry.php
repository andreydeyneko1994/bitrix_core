<?
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD"] = "Proszę <a id=\"#ID#\" href=\"#URL#\">aktualizuj statystyki leadów</a>, aby uzyskać prawidłowy raport.";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "To zaktualizuje statystyki leadów. Może to chwilę potrwać.";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Aktualizuj statystykę leadów";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Suma";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_TITLE"] = "Leady";
?>