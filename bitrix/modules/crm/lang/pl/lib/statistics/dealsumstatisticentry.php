<?
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "Raporty wymagają, abyś <a id=\"#ID#\" href=\"#URL#\">zaktualizował statystykę deali</a> do właściwego działania.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "To zaktualizuje statystyki deali. Może to chwilę potrwać.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Aktualizuj statystykę deali";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Suma";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "Deale";
?>