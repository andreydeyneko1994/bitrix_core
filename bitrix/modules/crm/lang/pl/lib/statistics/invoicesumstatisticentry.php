<?
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD"] = "Proszę <a id=\"#ID#\" href=\"#URL#\">aktualizuj statystyki faktur</a>, aby uzyskać prawidłowy raport.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "To zaktualizuje statystyki faktur. Może to chwilę potrwać.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Aktualizuj statystykę faktur";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Suma";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_TITLE"] = "Faktury";
?>