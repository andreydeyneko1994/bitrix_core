<?php
$MESS["CRM_FIELD_NOT_UNIQUE_ERROR"] = "Nieunikalna wartość \"#FIELD#\"";
$MESS["CRM_FIELD_NOT_VALID_ERROR"] = "Nieprawidłowa wartość \"#FIELD#\"";
$MESS["CRM_FIELD_VALUE_REQUIRED_ERROR"] = "Wymagane jest pole \"#FIELD#\"";
