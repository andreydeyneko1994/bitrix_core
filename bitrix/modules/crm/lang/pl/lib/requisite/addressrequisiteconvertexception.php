<?
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "Odmowa dostępu do firmy";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "Nie można utworzyć elementu danych firmy";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "Odmowa dostępu do kontaktu";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "Nie można utworzyć elementu szczegółów kontaktu";
?>