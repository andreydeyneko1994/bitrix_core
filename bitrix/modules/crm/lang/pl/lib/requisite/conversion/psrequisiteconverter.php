<?
$MESS["CRM_PS_RQ_CONV_ACC_NUM_DEF_VAL"] = "Numer konta";
$MESS["CRM_PS_RQ_CONV_INTRO"] = "Szczegóły dotyczące twojej firmy są ważne dla CRM w Bitrix24. Faktury, oferty, systemy płatności i inne części Bitrix24 wymagają dostarczenia szczegółowych danych o firmie.<br><br>Już nie musisz wprowadzać szczegółowych danych ręcznie za każdym razem. W prosty sposób wprowadzasz te informacje raz i wykorzystujesz je wszędzie. <br><br> W celu wykorzystania istniejących danych, skonwertuj System Płatności na nie. Będą one automatycznie wprowadzane w istniejących fakturach po zakończeniu konwersji.<br><br> <a id=\"#EXEC_ID#\" href=\"#EXEC_URL#\">Konwertuj</a> <a id=\"#SKIP_ID#\" href=\"#SKIP_URL#\">Pomiń</a>";
$MESS["CRM_PS_RQ_CONV_PRESET_NAME"] = "Sprzedawca";
?>