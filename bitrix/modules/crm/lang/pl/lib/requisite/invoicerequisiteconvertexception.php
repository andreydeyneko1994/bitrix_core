<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Błąd ogólny.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Nie można znaleźć typu płatnika.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "Wybrany szablon nie ma pól, które można by wypełnić istniejącymi danymi.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Nie można przenieść danych, ponieważ dane istniejącej faktury zostały usunięte.";
?>