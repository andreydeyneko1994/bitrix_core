<?php
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ACCESS_DENIED"] = "Niewystarczające uprawnienia do wykonania operacji.";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_DELETE_DEDUCTED_ERROR"] = "\"Zamówienie sprzedaży ##ID#\" nie może zostać usunięte, ponieważ zostało już przetworzone";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_NOT_USED_INVENTORY_MANAGEMENT"] = "Aby przetworzyć obiekt magazynowy, włącz zarządzanie asortymentem";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ORDER_NOT_FOUND_ERROR"] = "Nie znaleziono zamówienia";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_PRODUCT_NOT_FOUND"] = "Wprowadź co najmniej jeden produkt";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIPMENT_NOT_FOUND_ERROR"] = "Nie znaleziono wysyłki ##ID#";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIP_DEDUCTED_ERROR"] = "Błąd podczas przetwarzania \"Zamówienia sprzedaży ##ID#\": zostało już ono przetworzone";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_UNSHIP_UNDEDUCTED_ERROR"] = "Błąd podczas anulowania \"Zamówienia sprzedaży ##ID#\": nie zostało ono jeszcze przetworzone.";
