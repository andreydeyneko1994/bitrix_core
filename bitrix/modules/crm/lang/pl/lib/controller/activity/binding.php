<?php
$MESS["CRM_ACTIVITY_BINDING_ALREADY_BOUND_ERROR"] = "Aktywność jest już powiązana z obiektem";
$MESS["CRM_ACTIVITY_BINDING_LAST_BINDING_ERROR"] = "Nie można usunąć jedynego linku aktywność-obiekt.";
$MESS["CRM_ACTIVITY_BINDING_NOT_BOUND_ERROR"] = "Aktywność nie jest powiązana z obiektem";
