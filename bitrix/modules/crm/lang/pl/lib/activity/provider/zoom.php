<?php
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_ENDED_TITLE"] = "Spotkanie Zoom zakończone";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_JOINED_TITLE"] = "Klient kliknął link zaproszenia Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_CONF_START_TITLE"] = "Spotkanie Zoom";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_ERROR_INCORRECT_DATETIME"] = "Nie można zaplanować spotkania na wybrany czas.";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_MESSAGE_SENT_TITLE"] = "Powiadomienie o spotkaniu Zoom wysłane do klienta";
$MESS["CRM_ACTIVITY_PROVIDER_ZOOM_TITLE"] = "Spotkanie Zoom";
