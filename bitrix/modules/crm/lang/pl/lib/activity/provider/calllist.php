<?
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "Lista połączeń";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "Lista połączeń nie istnieje. Aby utworzyć listę, kliknij odnośnik w obszarze \"Listy połączeń\".";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "Teraz odpowiadasz za listę połączeń \"#title#\"";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "Nie określono przedmiotu działania";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "Zastosuj formularz";
?>