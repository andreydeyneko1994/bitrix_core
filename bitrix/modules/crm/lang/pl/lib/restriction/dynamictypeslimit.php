<?php
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_CREATE_RESTRICTED"] = "Z powodu ograniczeń bieżącego planu nie można utworzyć nowej Inteligentnej automatyzacji procesów";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_CREATE_RESTRICTED"] = "Z powodu ograniczeń bieżącego planu nie można utworzyć nowej pozycji";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_ITEM_UPDATE_RESTRICTED"] = "Z powodu ograniczeń bieżącego planu nie można edytować pozycji";
$MESS["CRM_RESTRICTION_DYNAMIC_TYPES_UPDATE_RESTRICTED"] = "Z powodu ograniczeń bieżącego planu nie można edytować ustawień Inteligentnej automatyzacji procesów";
