<?php
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Dodatkowe Otwarte Kanały są dostępne wyłącznie w planach płatnych.";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class=\"crm-conditionally-required-field-tab-content\">
	<div class=\"crm-conditionally-required-field-tab-text\">
		Pola wymagane dla danego etapu są dostępne tylko w <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planach płatnych</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class=\"crm-deal-category-tab-content\">
	<div class=\"crm-deal-category-tab-text\">
		Twój obecny plan ogranicza liczbę lejków, których możesz użyć. Przejdź na jeden z <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planów płatnych</a>, aby tworzyć lejki obejmujące wiele produktów i strategii sprzedaży w celu analizowania różnych możliwości biznesowych. 
	</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Wiele lejków CRM";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "Niech Bitrix24 tworzy deale za Ciebie! 
Powracające deale oszczędzą twój czas i zasoby przy wielu okazjach. Na przykład, jeśli planujesz w przyszłym roku podpisywać cotygodniowe umowy, deale powracające na pewno Ci się przydadzą.
Utwórz powracający deal i ustaw harmonogram oraz lejek. Nowe deale będą tworzone w określonym czasie, a Ty nie musisz nawet kiwnąć palcem.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">Automatyczne tworzenie deali</li>
		<li class=\"hide-features-list-item\">Zwiększenie wydajności i skuteczności pracowników</li>
		<li class=\"hide-features-list-item\">Szybki dostęp do bieżących deali powracających – <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Więcej informacji</a></li>
	</ul>		
	<strong>Deale powracające są dostępne w wybranych planach płatnych</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "Powracające deale są dostępne w planach płatnych";
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Zaawansowane wyszukiwanie duplikatów</h3>
<div class=\"crm-duplicate-tab-text\">
Kiedy jest tworzony nowy kontakt (lub lead albo firma), Bitrix24 znajduje i pokazuje potencjalne duplikaty &mdash; przeciwdziałając ich tworzeniu.
</div>
<div class=\"crm-duplicate-tab-text\">
W zaawansowanym wyszukiwaniu duplikatów, CRM może znaleźć duplikaty w importowanych danych oraz w danych, które już istnieją. Takie duplikaty mogą zostać scalone. 
</div>
<div class=\"crm-duplicate-tab-text\">
Dodaj te oraz inne świetne funkcjonalności do Bitrix24! Zaawansowana telefonia + Zaawansowany CRM i inne użyteczne funkcjonalności są dostępne w wybranych planach płatnych. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Dowiedz się więcej</a>
</div>
<div class=\"ui-btn-container ui-btn-container-center\">
<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Wybierz plan rozszerzony</span>
<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Wybierz darmową 30-dniową wersję</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">Historia zmian w CRM jest dostępna tylko w zaawansowanym CRM</h3>
	<div class=\"crm-history-tab-text\">
		Bitrix24 zachowuje szczegółowy log zmian w CRM. Kiedy rozszerzysz CRM do wersji zaawansowanej zobaczysz historię wszystkich zmian (np. kto używał lub zmieniał wpisy w CRM) i będziesz mógł przywrócić poprzednie wartości, jeśli będzie taka potrzeba.
	</div>
	<div class=\"crm-history-tab-text\">Tak to wygląda:</div>
	<img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	<div class=\"crm-history-tab-text\">
		Dodaj te oraz inne świetne funkcjonalności do Bitrix24! Zaawansowana telefonia + Zaawansowany CRM i inne użyteczne funkcjonalności są dostępne w wybranych planach płatnych. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Dowiedz się więcej</a>
	</div>
	<div class=\"ui-btn-container ui-btn-container-center\">
		<span class=\"ui-btn ui-btn-lg ui-btn-success\" onclick=\"#LICENSE_LIST_SCRIPT#\">Wybierz plan rozszerzony</span>
		<span class=\"ui-btn ui-btn-lg ui-btn-light-border\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Darmowa wersja 30-dniowa</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "Funkcja automatycznego fakturowania pozwala regularnie wystawiać klientom faktury i oszczędzić czas.
Utwórz fakturę i określ jak często chcesz ją w przyszłości wysyłać. System będzie wystawiał i wysyłał nową fakturę do klienta w ustalonych odstępach czasu.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">Oszczędzaj czas dzięki automatycznemu fakturowaniu</li>
		<li class=\"hide-features-list-item\">Szybki dostęp do wszystkich szablonów automatycznego fakturowania</li>
		<li class=\"hide-features-list-item\">Wysyłanie faktury na adres e-mail klienta <a href=\"https://www.bitrix24.com/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">Czytaj więcej</a></li>
	</ul>		
	<strong>Automatyczne fakturowanie dostępne jest w wybranych planach płatnych.</strong>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "Funkcja automatycznego fakturowania jest dostępna w wybranych planach płatnych.";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
 		Bezpłatny plan przypisuje jednakowe uprawnienia dostępu wszystkim pracownikom. Rozważ rozszerzenie do jednego z planów płatnych, aby przypisywać różne role, działania i dane różnym użytkownikom.	
		Aby dowiedzieć się więcej o różnych planach, przejdź na <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">stronę porównywania planów</a>.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		W Twoim bieżącym planie wszyscy pracownicy mają jednakowe uprawnienia. Rozszerz do jednego z głównych planów, aby definiować role, przypisywać uprawnienia i określać dane, do których mają mieć dostęp pracownicy.
		<a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">Dowiedz się więcej o planach i porównaj zawarte w nich funkcje</a>.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "Aby przypisać pracownikom różne uprawnienia dostępu, przejdź na jeden z planów płatnych.";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "Dodaj funkcjonalności do swojego CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Konwersje pomiędzy dealami, fakturami i ofertami</li>
<li class=\"hide-features-list-item\">Rozszerzona kontrola duplikatów</li>
<li class=\"hide-features-list-item\">Historia zmian i przywracanie poprzednich wersji CRM</li>
<li class=\"hide-features-list-item\">Dziennik używania CRM</li>
<li class=\"hide-features-list-item\">Przeglądanie ponad 5000 rekordów CRM</li>
<li class=\"hide-features-list-item\">Połączenia oparte na liście<sup class=\"hide-features-soon\">wkrótce</sup></li>
<li class=\"hide-features-list-item\">Masowe maile do klientów<sup class=\"hide-features-soon\">wkrótce</sup></li>
<li class=\"hide-features-list-item\">Wsparcie dla sprzedaży usług<sup class=\"hide-features-soon\">wkrótce</sup>
<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Dowiedz się więcej</a>
</li>
</ul>
<strong>Zaawansowana telefonia + Zaawansowany CRM i inne użyteczne funkcjonalności są dostępne w wybranych planach płatnych.</strong>

";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "Zaawansowany CRM w Bitrix24";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Automatyzacja sprzedaży CRM Bitrix24<br><br>
	Twórz scenariusze do systemu CRM, aby windować deale: przydzielaj zadania, planuj spotkania, uruchamiaj ukierunkowane kampanie reklamowe i wystawiaj faktury. Reguły automatyzacji przeprowadzą menedżera przez kolejne kroki na każdym etapie deala, aby zakończyć zadanie.<br><br>
	Wyzwalacze automatycznie reagują na działania klienta (odwiedzenie witryny, wypełnienie formularza, opublikowanie komentarza w sieci społecznościowej lub nawiązanie połączenia) i uruchamiają regułę automatyzacji, która pomaga w przemianie leada w deala.<br><br>
	Reguły automatyzacji i wyzwalacze wykonują wszystkie rutynowe operacje bez konieczności szkolenia menedżerów: skonfiguruj raz i kontynuuj pracę!<br><br>
	Dodaj reguły automatyzacji i zwiększ sprzedaż!<br><br>
	To narzędzie jest dostępne wyłącznie w płatnych subskrypcjach Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Reguły automatyzacji";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "Podpis można usunąć wyłącznie w planach płatnych.";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "Rozszerzone formularze CRM";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Twój bieżący plan ogranicza liczbę formularzy CRM. Aby dodać więcej formularzy rozszerz swój plan. 
<br><br>
Porada: Wybrane plany płatne obejmują nieograniczoną liczbę formularzy CRM.
<br><br>
Wypróbuj Bitrix24 Professional za darmo przez 30 dni.";
