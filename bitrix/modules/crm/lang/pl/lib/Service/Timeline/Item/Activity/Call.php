<?php
$MESS["CRM_TIMELINE_BLOCK_CALL_ADDITIONAL_INFO_1"] = "Numer";
$MESS["CRM_TIMELINE_BLOCK_CALL_ADDITIONAL_INFO_2"] = "Czas trwania";
$MESS["CRM_TIMELINE_BLOCK_CLIENT_MARK_TEXT"] = "Klient ocenił połączenie na #MARK#";
$MESS["CRM_TIMELINE_BLOCK_DESCRIPTION_EXCLUDE_1"] = "Połączenie zostało odebrane poza godzinami pracy";
$MESS["CRM_TIMELINE_BLOCK_DESCRIPTION_EXCLUDE_2"] = "Czas trwania połączenia:";
$MESS["CRM_TIMELINE_BLOCK_TITLE_CLIENT"] = "Klient";
$MESS["CRM_TIMELINE_BLOCK_TITLE_QUEUE"] = "Kolejka";
$MESS["CRM_TIMELINE_BLOCK_TITLE_RESPONSIBLE_USER"] = "Osoba odpowiedzialna";
$MESS["CRM_TIMELINE_BLOCK_TITLE_THEME"] = "Temat";
$MESS["CRM_TIMELINE_BUTTON_CALL"] = "Połącz";
$MESS["CRM_TIMELINE_BUTTON_CALL_COMPLETE"] = "Przetworzono";
$MESS["CRM_TIMELINE_BUTTON_CALL_SCHEDULE"] = "Harmonogram";
$MESS["CRM_TIMELINE_BUTTON_TIP_TRANSCRIPT"] = "Pokaż transkrypcję rozmowy";
$MESS["CRM_TIMELINE_TAG_CALL_COMPLETED"] = "Pomyślnie zakończone";
$MESS["CRM_TIMELINE_TAG_CALL_MISSED"] = "Nieodebrane";
$MESS["CRM_TIMELINE_TAG_TRANSCRIPT_PENDING"] = "Oczekiwanie na transkrypcję";
$MESS["CRM_TIMELINE_TITLE_CALL_INCOMING"] = "Zakończono połączenie przychodzące";
$MESS["CRM_TIMELINE_TITLE_CALL_INCOMING_DONE"] = "Przetworzono połączenie przychodzące";
$MESS["CRM_TIMELINE_TITLE_CALL_INCOMING_PLAN"] = "Zaplanowano połączenie przychodzące";
$MESS["CRM_TIMELINE_TITLE_CALL_MISSED"] = "Nie odebrano połączenia przychodzącego";
$MESS["CRM_TIMELINE_TITLE_CALL_OUTGOING"] = "Zakończono połączenie wychodzące";
$MESS["CRM_TIMELINE_TITLE_CALL_OUTGOING_PLAN"] = "Zaplanowano połączenie wychodzące";
