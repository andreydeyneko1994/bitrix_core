<?php
$MESS["CRM_TIMELINE_ACTIVITY_CREATION"] = "Utworzono aktywność: #TITLE#";
$MESS["CRM_TIMELINE_COMPANY_CREATION"] = "Utworzono firmę";
$MESS["CRM_TIMELINE_CONTACT_CREATION"] = "Utworzono kontakt";
$MESS["CRM_TIMELINE_DEAL_CREATION"] = "Utworzono deal";
$MESS["CRM_TIMELINE_DEAL_ORDER_TITLE"] = "Dla zamówienia: <a href='#HREF#'>##ORDER_ID#</a> z #DATE_TIME# (na #PRICE_WITH_CURRENCY#)";
$MESS["CRM_TIMELINE_INVOICE_CREATION"] = "Utworzono fakturę";
$MESS["CRM_TIMELINE_LEAD_CREATION"] = "Utworzono lead";
$MESS["CRM_TIMELINE_ORDER_CREATION"] = "Utworzono zamówienie";
$MESS["CRM_TIMELINE_ORDER_PAYMENT_CREATION"] = "Płatność utworzona";
$MESS["CRM_TIMELINE_ORDER_SHIPMENT_CREATION"] = "Wysyłka utworzona";
$MESS["CRM_TIMELINE_QUOTE_CREATION"] = "Utworzono ofertę";
$MESS["CRM_TIMELINE_RECURRING_DEAL_CREATION"] = "Utworzono szablon dealu";
$MESS["CRM_TIMELINE_TASK_CREATION"] = "Utworzono zadanie: #TITLE#";
