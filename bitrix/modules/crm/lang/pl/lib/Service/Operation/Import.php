<?php
$MESS["CRM_FIELD_CRATED_TIME_VALUE_IN_FUTURE_ERROR"] = "Wartość pola \"#FIELD#\" nie może dotyczyć przyszłości";
$MESS["CRM_FIELD_CRATED_TIME_VALUE_NOT_MONOTONE_ERROR"] = "Wartość pola \"#FIELD#\" nie może być mniejsza niż w przypadku innych pozycji";
$MESS["CRM_FIELD_VALUE_CAN_NOT_BE_GREATER_ERROR"] = "Wartość pola \"#FIELD1#\" nie może być większa niż \"#FIELD2#\"";
$MESS["CRM_FIELD_VALUE_ONLY_ADMIN_CAN_SET_ERROR"] = "Wyłącznie administrator może zmienić wartość pola \"#FIELD#\"";
