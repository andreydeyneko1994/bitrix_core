<?
$MESS["CRM_ADS_FORM_TYPE_ERR_DISABLED_FACEBOOK"] = "Ze względu na ostatnie zmiany w polityce prywatności Facebooka integracja formularzy CRM Bitrix24 z reklamami typu Lead Ads tego portalu jest tymczasowo niedostępna. Technicy z Facebooka pracują nad nowym API i obiecali, że wkrótce ta funkcja będzie dostępna dla użytkowników Bitrix24.";
$MESS["CRM_ADS_FORM_TYPE_NAME_FACEBOOK"] = "Reklamy na Facebooku";
?>