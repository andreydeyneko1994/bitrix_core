<?
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Wystąpił błąd podczas aktywowania zamówienia, ponieważ upłynęła data ostatniej cyklicznej faktury. Możesz chcieć zmienić datę.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Wystąpił błąd podczas aktywowania zamówienia ze względu na przekroczenie limitu powtórzeń. Możesz chcieć zwiększyć limit.";
$MESS["CRM_RECUR_WRONG_ID"] = "Błędne ID konta";
?>