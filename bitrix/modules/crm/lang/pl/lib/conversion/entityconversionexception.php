<?
$MESS["CRM_CONV_EX_COMPANY_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie firm wyłączonych.";
$MESS["CRM_CONV_EX_COMPANY_CREATE_DENIED"] = "Odmowa utworzenia firmy.";
$MESS["CRM_CONV_EX_COMPANY_CREATE_FAILED"] = "Nie można utworzyć firmy. Szczegóły: ";
$MESS["CRM_CONV_EX_COMPANY_EMPTY_FIELDS"] = "Nie można wypełnić pól firmy.";
$MESS["CRM_CONV_EX_COMPANY_HAS_WORKFLOWS"] = "Skonfigurowane są workflowy, które uruchamiają się za każdym utworzeniem firmy. Może być konieczne działanie ze strony użytkownika.";
$MESS["CRM_CONV_EX_COMPANY_INVALID_FIELDS"] = "Pola firmy mają nieprawidłowe wartości. Szczegóły: ";
$MESS["CRM_CONV_EX_COMPANY_NOT_FOUND"] = "Nie znaleziono firmy..";
$MESS["CRM_CONV_EX_COMPANY_READ_DENIED"] = "Odmowa dostępu do danych firmy w zakresie odczytu.";
$MESS["CRM_CONV_EX_COMPANY_UPDATE_DENIED"] = "Nie można zaktualizować firmy ze względu na odmowę dostępu.";
$MESS["CRM_CONV_EX_CONTACT_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie kontaktów wyłączone.";
$MESS["CRM_CONV_EX_CONTACT_CREATE_DENIED"] = "Odmowa utworzenia kontaktu.";
$MESS["CRM_CONV_EX_CONTACT_CREATE_FAILED"] = "Nie można utworzyć kontaktu. Szczegóły: ";
$MESS["CRM_CONV_EX_CONTACT_EMPTY_FIELDS"] = "Nie można wypełnić pól kontaktu.";
$MESS["CRM_CONV_EX_CONTACT_HAS_WORKFLOWS"] = "Skonfigurowane są workflowy, które uruchamiają się za każdym utworzeniem kontaktu. Może być konieczne działanie ze strony użytkownika.";
$MESS["CRM_CONV_EX_CONTACT_INVALID_FIELDS"] = "Pola firmy mają nieprawidłowe wartości. Szczegóły: ";
$MESS["CRM_CONV_EX_CONTACT_NOT_FOUND"] = "Nie znaleziono kontaktu.";
$MESS["CRM_CONV_EX_CONTACT_READ_DENIED"] = "Odmowa dostępu do danych kontaktu w zakresie odczytu.";
$MESS["CRM_CONV_EX_CONTACT_UPDATE_DENIED"] = "Nie można zaktualizować kontaktu ze względu na odmowę dostępu.";
$MESS["CRM_CONV_EX_DEAL_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie deali jest wyłączone.";
$MESS["CRM_CONV_EX_DEAL_CREATE_DENIED"] = "Odmowa utworzenia deala.";
$MESS["CRM_CONV_EX_DEAL_CREATE_FAILED"] = "Nie można utworzyć deala. Szczegóły: ";
$MESS["CRM_CONV_EX_DEAL_EMPTY_FIELDS"] = "Nie można wypełnić pól deala.";
$MESS["CRM_CONV_EX_DEAL_HAS_WORKFLOWS"] = "Skonfigurowane są workflowy, które uruchamiają się za każdym utworzeniem deala. Może być konieczne działanie ze strony użytkownika.";
$MESS["CRM_CONV_EX_DEAL_INVALID_FIELDS"] = "Pola deala mają nieprawidłowe wartości. Szczegóły: ";
$MESS["CRM_CONV_EX_DEAL_NOT_FOUND"] = "Deal nie został odnaleziony.";
$MESS["CRM_CONV_EX_DEAL_READ_DENIED"] = "Odmowa dostępu do danych deala w zakresie odczytu.";
$MESS["CRM_CONV_EX_DEAL_UPDATE_DENIED"] = "Nie można zaktualizować deala ze względu na odmowę dostępu.";
$MESS["CRM_CONV_EX_ENTITY_NOT_SUPPORTED"] = "Typ #ENTITY_TYPE_NAME# nie jest obsługiwany.";
$MESS["CRM_CONV_EX_INVALID_OPERATION"] = "Nieprawidłowa operacja.";
$MESS["CRM_CONV_EX_INVOICE_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie faktur wyłączone.";
$MESS["CRM_CONV_EX_INVOICE_CREATE_DENIED"] = "Nie można utworzyć faktury, ponieważ odmówiono dostępu.";
$MESS["CRM_CONV_EX_LEAD_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie leadów wyłączone.";
$MESS["CRM_CONV_EX_LEAD_HAS_WORKFLOWS"] = "Skonfigurowane są workflowy, które uruchamiają się za każdym utworzeniem leada. Może być konieczne działanie ze strony użytkownika.";
$MESS["CRM_CONV_EX_LEAD_NOT_FOUND"] = "Nie znaleziono leada.";
$MESS["CRM_CONV_EX_LEAD_READ_DENIED"] = "Odmowa dostępu do danych leada w zakresie odczytu.";
$MESS["CRM_CONV_EX_LEAD_UPDATE_DENIED"] = "Nie można zaktualizować leada ze względu na odmowę dostępu.";
$MESS["CRM_CONV_EX_NOT_SYNCHRONIZED"] = "Nie udało się przenieść wszystkich danych leada. Najpierw zakończ mapowanie pól niestandardowych.";
$MESS["CRM_CONV_EX_QUOTE_AUTOCREATION_DISABLED"] = "Automatyczne tworzenie ofert wyłączone.";
?>