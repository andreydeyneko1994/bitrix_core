<?
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_PERMISSION"] = "Użytkownik #USER_NAME# połączył kontakt \"#SEED_TITLE#\" [#SEED_ID#] z kontaktem \"#TARG_TITLE#\" [#TARG_ID#], którego nie można przeglądać z powodu preferencji uprawnień dostępu.";
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "Użytkownik #USER_NAME# połączył kontakt \"#SEED_TITLE#\" [#SEED_ID#] z kontaktem \"#TARG_TITLE#\" [#TARG_ID#], którego nie można przeglądać ani edytować z powodu preferencji uprawnień dostępu.";
$MESS["CRM_CONTACT_MERGER_COLLISION_UPDATE_PERMISSION"] = "Użytkownik #USER_NAME# połączył kontakt \"#SEED_TITLE#\" [#SEED_ID#] z kontaktem \"#TARG_TITLE#\" [#TARG_ID#], którego nie można edytować z powodu preferencji uprawnień dostępu.";
?>