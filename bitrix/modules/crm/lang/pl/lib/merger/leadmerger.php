<?
$MESS["CRM_LEAD_MERGER_COLLISION_READ_PERMISSION"] = "Użytkownik #USER_NAME# połączył lead \"#SEED_TITLE#\" [#SEED_ID#] z firmą \"#TARG_TITLE#\" [#TARG_ID#], której nie można przeglądać z powodu preferencji uprawnień dostępu.";
$MESS["CRM_LEAD_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "Użytkownik #USER_NAME# połączył lead \"#SEED_TITLE#\" [#SEED_ID#] z leadem \"#TARG_TITLE#\" [#TARG_ID#], którego nie można przeglądać ani edytować z powodu preferencji uprawnień dostępu.";
$MESS["CRM_LEAD_MERGER_COLLISION_UPDATE_PERMISSION"] = "Użytkownik #USER_NAME# połączył lead \"#SEED_TITLE#\" [#SEED_ID#] z firmą \"#TARG_TITLE#\" [#TARG_ID#], której nie można edytować z powodu preferencji uprawnień dostępu.";
?>