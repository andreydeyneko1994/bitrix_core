<?
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_OCCURRED"] = "Nie można automatycznie połączyć z powodu konfliktu danych.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_RESOLUTION_NOT_SUPPORTED"] = "Brak obsługi metody rozwiązywania konfliktów \"#RESOLUTION_CAPTION#\".";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_DENIED"] = "Brak uprawnień do usuwania \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_FAILED"] = "Nie można usunąć \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_ERROR"] = "Wystąpił błąd podczas łączenia. Komunikat błędu: #ERROR#";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_NOT_FOUND"] = "Nie można znaleźć elementu [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_READ_DENIED"] = "Brak uprawnień do odczytu \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_DENIED"] = "Brak uprawnień do aktualizacji \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_FAILED"] = "Nie można zapisać \"#TITLE#\" [#ID#].";
?>