<?php
$MESS["CRM_STATUS_ENTITY_ENTITY_ID_FIELD"] = "ID elementu";
$MESS["CRM_STATUS_ENTITY_NAME_FIELD"] = "Tytuł";
$MESS["CRM_STATUS_ENTITY_STATUS_ID_FIELD"] = "ID statusu";
$MESS["CRM_STATUS_FIELD_UPDATE_ERROR"] = "Nie można zaktualizować pola #STATUS_FIELD#";
$MESS["CRM_STATUS_MORE_THAN_ONE_SUCCESS_ERROR"] = "Dla danego typu może istnieć tylko jeden status sukcesu";
$MESS["CRM_STATUS_STAGE_WITH_ITEMS_ERROR"] = "Nie można usunąć etapu zawierającego pozycje";
$MESS["CRM_STATUS_SUCCESS_SEMANTIC_UPDATE_ERROR"] = "Nie można zmienić semantyki statusu sukcesu";
