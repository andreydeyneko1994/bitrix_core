<?php
$MESS["CRM_STATE_CATALOG_PRODUCT_PRICE_CHANGING_BLOCKED"] = "Obecnie nie możesz zmienić ceny produktu w ramach deala, faktury itp. Zastosuj rabat, aby sprzedać produkt po niższej cenie. Edycję ceny możesz włączyć w ustawieniach CRM.";
$MESS["CRM_STATE_ERR_CATALOG_PRODUCT_LIMIT"] = "Przekroczono maksymalną liczbę produktów z katalogu CRM. Obecna liczba produktów to #COUNT# (z maks. #LIMIT#).";
