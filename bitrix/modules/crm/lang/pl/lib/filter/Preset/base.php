<?php
$MESS["CRM_PRESET_ALL_COMPANIES"] = "Wszystkie firmy";
$MESS["CRM_PRESET_ALL_CONTACTS"] = "Wszystkie kontakty";
$MESS["CRM_PRESET_CLOSED_DEALS"] = "Zamknięte deale";
$MESS["CRM_PRESET_CLOSED_LEADS"] = "Zamknięte leady";
$MESS["CRM_PRESET_CLOSED_QUOTES"] = "Zamknięte oferty";
$MESS["CRM_PRESET_CLOSED_SI"] = "Zamknięte faktury";
$MESS["CRM_PRESET_DEALS_IN_ROBOT_DEBUGGER"] = "Debugowanie dealów";
$MESS["CRM_PRESET_IN_WORK_DEALS"] = "Deale w toku";
$MESS["CRM_PRESET_IN_WORK_LEADS"] = "Leady w toku";
$MESS["CRM_PRESET_IN_WORK_QUOTES"] = "W toku";
$MESS["CRM_PRESET_IN_WORK_SI"] = "Faktury w toku";
$MESS["CRM_PRESET_MY_COMPANIES"] = "Moje firmy";
$MESS["CRM_PRESET_MY_CONTACTS"] = "Moje kontakty";
$MESS["CRM_PRESET_MY_DEALS"] = "Moje deale";
$MESS["CRM_PRESET_MY_LEADS"] = "Moje leady";
$MESS["CRM_PRESET_MY_QUOTES"] = "Moje oferty";
$MESS["CRM_PRESET_MY_SI"] = "Moje faktury";
