<?php
$MESS["CRM_FILTER_ITEMDATAPROVIDER_COMPANY_TITLE"] = "Nazwa firmy";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_CONTACTS_FULL_NAME"] = "Nazwa kontaktu";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_MYCOMPANY_TITLE"] = "Nazwa Twojej firmy";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_OPPORTUNITY_WITH_CURRENCY"] = "Kwota/Waluta";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FAIL"] = "Niepowodzenie";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FILTER_NAME"] = "Grupa etapu";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_IN_WORK"] = "W toku";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_SUCCESS"] = "Powodzenie";
