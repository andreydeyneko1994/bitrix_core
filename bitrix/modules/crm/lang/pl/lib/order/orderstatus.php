<?php
$MESS["CRM_ORDER_STATUS_FINISHED"] = "Zrealizowano";
$MESS["CRM_ORDER_STATUS_INITIAL"] = "Zamówienie przyjęte, oczekiwanie na płatność";
$MESS["CRM_ORDER_STATUS_PAID"] = "Opłacono, przygotowanie do wysyłki";
$MESS["CRM_ORDER_STATUS_REFUSED"] = "Anulowano";
$MESS["CRM_ORDER_STATUS_SEND"] = "Wysłano";
