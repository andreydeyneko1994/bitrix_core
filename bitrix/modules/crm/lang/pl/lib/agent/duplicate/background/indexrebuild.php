<?php
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_ALREADY_RUNNING"] = "Nie można rozpocząć ponownego indeksowania, ponieważ jest już ono w toku.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_INDEX_TYPES"] = "Typy indeksów są nieokreślone lub mają nieprawidłowe wartości.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_RUNNING"] = "Nie można zatrzymać ponownego indeksowania, ponieważ nie zostało ono uruchomione.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_NOT_STOPPED"] = "Nie można wznowić ponownego indeksowania, ponieważ nie zostało ono rozpoczęte ani zatrzymane.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_SCOPE"] = "Kod kraju nie jest określony lub ma nieprawidłową wartość.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_ERR_TYPE_INDEX"] = "Bieżący typ indeksu jest nieprawidłowy.";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_050"] = "Zeskanowano 50% duplikatów firm";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_090"] = "Zeskanowano 90% duplikatów firm";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_COMPANY_100"] = "Duplikaty firm zostały zeskanowane";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_050"] = "Zeskanowano 50% duplikatów kontaktów";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_090"] = "Zeskanowano 90% duplikatów kontaktów";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_CONTACT_100"] = "Duplikaty kontaktów zostały zeskanowane";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_050"] = "Zeskanowano 50% duplikatów leadów";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_090"] = "Zeskanowano 90% duplikatów leadów";
$MESS["CRM_AGNT_DUP_BGRND_IDX_NOTIFY_LEAD_100"] = "Duplikaty leadów zostały zeskanowane";
