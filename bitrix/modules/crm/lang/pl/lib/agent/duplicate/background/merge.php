<?php
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_ALREADY_RUNNING"] = "Nie można rozpocząć już uruchomionego scalania.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_RUNNING"] = "Nie można przerwać nieuruchomionego scalania.";
$MESS["CRM_AGNT_DUP_BGRND_MERGE_ERR_NOT_STOPPED"] = "Nie można wznowić scalania, ponieważ nie zostało ono rozpoczęte ani zatrzymane.";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_050"] = "Scalono 50% zduplikowanych firm";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_090"] = "Scalono 90% zduplikowanych firm";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_COMPANY_100"] = "Zduplikowane firmy zostały scalone";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_050"] = "Scalono 50% zduplikowanych kontaktów";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_090"] = "Scalono 90% zduplikowanych kontaktów";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_CONTACT_100"] = "Zduplikowane kontakty zostały scalone";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_050"] = "Scalono 50% zduplikowanych leadów";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_090"] = "Scalono 90% zduplikowanych leadów";
$MESS["CRM_AGNT_DUP_BGRND_MRG_NOTIFY_LEAD_100"] = "Zduplikowane leady zostały scalone";
