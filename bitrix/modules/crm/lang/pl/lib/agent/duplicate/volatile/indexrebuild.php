<?php
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_ALREADY_RUNNING"] = "Nie można rozpocząć ponownego indeksowania, ponieważ jest już ono w toku.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_RUNNING"] = "Nie można zatrzymać ponownego indeksowania, ponieważ nie zostało ono uruchomione.";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_STOPPED"] = "Nie można wznowić ponownego indeksowania, ponieważ nie jest ono w toku ani wstrzymane.";
