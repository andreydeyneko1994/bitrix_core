<?
$MESS["CRM_AGENT_NOTICE_NOTIFY_AGENT_ABOUT_REPEAT_LEAD"] = "Na twoim Bitrix24 są włączone leady cykliczne! Za każdym razem, gdy skontaktuje się z tobą obecny klient, CRM rozpozna go i utworzy lead cykliczny, tak aby osoba odpowiedzialna mogła go przetworzyć i dokonać nowej sprzedaży. <a href=\"https://helpdesk.bitrix24.com/open/7348303/\">Szczegóły</a>";
?>