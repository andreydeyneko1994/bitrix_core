<?
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "Stracony";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "Konwersja";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "Dynamika przetwarzania nowych leadów";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "Jeśli nadal nie masz żadnych leadów, <a href=\"#URL#\" class=\"#CLASS_NAME#\">stwórz jeden</a> teraz!";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "To jest widok demo. Ukryj go aby otrzymać własne raporty leadów.";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "Efektywność pracowników w przetwarzaniu leadów";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "Lejek leadów";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "Liczba aktywności";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "Liczba rozmów";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "Liczba leadów śmieciowych";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "Liczba bezczynnych leadów";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "Liczba aktywnych leadów";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "Liczba nowych leadów";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "Liczba skonwertowanych leadów";
$MESS["CRM_LEAD_WGT_RATING"] = "Tablica wyników skonwertowanych leadów";
$MESS["CRM_LEAD_WGT_SOURCE"] = "Źródła leadów";
?>