<?
$MESS["CRM_DEAL_WGT_DEAL_IN_WORK"] = "Deale w toku";
$MESS["CRM_DEAL_WGT_DEMO_CONTENT"] = "Jeśli nie masz żadnych deali, dodaj swojego <a href=\"#URL#\" class=\"#CLASS_NAME#\">pierwszego deala</a> teraz!";
$MESS["CRM_DEAL_WGT_DEMO_TITLE"] = "To jest pulpit demonstracyjny, możesz schować dane demonstracyjne i użyć prawdziwych danych.";
$MESS["CRM_DEAL_WGT_EMPLOYEE_DEAL_IN_WORK"] = "Deale w toku (pracownik)";
$MESS["CRM_DEAL_WGT_FUNNEL"] = "Lejek sprzedażowy dla deali";
$MESS["CRM_DEAL_WGT_PAYMENT_CONTROL"] = "Kontrola płatności dla wygranych deali";
$MESS["CRM_DEAL_WGT_QTY_ACTIVITY"] = "Liczba aktywności";
$MESS["CRM_DEAL_WGT_QTY_CALL"] = "Liczba kontaktów";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IDLE"] = "Liczba wstrzymanych deali";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IN_WORK"] = "Liczba deali w toku";
$MESS["CRM_DEAL_WGT_QTY_DEAL_WON"] = "Liczba wygranych deali";
$MESS["CRM_DEAL_WGT_QTY_EMAIL"] = "Liczba e-maili";
$MESS["CRM_DEAL_WGT_QTY_MEETING"] = "Liczba spotkań";
$MESS["CRM_DEAL_WGT_RATING"] = "Ranking wygranych deali";
$MESS["CRM_DEAL_WGT_SUM_DEAL_IN_WORK"] = "Suma deali w toku";
$MESS["CRM_DEAL_WGT_SUM_DEAL_OVERALL"] = "Całkowita wartość deali";
$MESS["CRM_DEAL_WGT_SUM_DEAL_WON"] = "Suma wygranych deali";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OVERALL"] = "Suma faktur zapłaconych";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OWED"] = "Suma faktur niezapłaconych";
?>