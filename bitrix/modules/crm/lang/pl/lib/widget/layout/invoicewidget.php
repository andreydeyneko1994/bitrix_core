<?
$MESS["CRM_INVOICE_WGT_DEAL_PAYMENT_CONTROL"] = "Kontrola płatności wygranych deali";
$MESS["CRM_INVOICE_WGT_DEMO_CONTENT"] = "Jeśli nie masz faktur, <a href=\"#URL#\" class=\"#CLASS_NAME#\">stwórz jakąś</a> już teraz!";
$MESS["CRM_INVOICE_WGT_DEMO_TITLE"] = "To jest raport demonstracyjny. Ukryj go, aby uzyskać dostęp do danych analitycznych faktur.";
$MESS["CRM_INVOICE_WGT_FUNNEL"] = "Lejek faktur";
$MESS["CRM_INVOICE_WGT_INVOCE_MANAGER"] = "Efektywność pracowników w ściągalności faktur";
$MESS["CRM_INVOICE_WGT_INVOCE_PAYMENT"] = "Długi i płatności";
$MESS["CRM_INVOICE_WGT_QTY_INVOICE_OVERDUE"] = "Liczba przeterminowanych faktur";
$MESS["CRM_INVOICE_WGT_RATING"] = "Tablica wyników opłaconych faktur";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OVERALL"] = "Wartość utworzonych faktur";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OWED"] = "Wartość nieutworzonych faktur";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_IN_WORK"] = "Całkowita wartość aktywnych faktur";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OVERDUE"] = "Całkowita wartość przeterminowanych faktur";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OWED"] = "Całkowita wartość oczekujących faktur";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_SUCCESSFUL"] = "Całkowita wartość opłaconych faktur";
?>