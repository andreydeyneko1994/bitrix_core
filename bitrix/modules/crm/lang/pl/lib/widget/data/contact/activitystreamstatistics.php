<?
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_GROUP_BY_STREAM"] = "Żądanie tablicy";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_INCOMING_QTY"] = "Liczba połączeń przychodzących";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_MISSING_QTY"] = "Liczba połączeń nieodebranych";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_OUTGOING_QTY"] = "Liczba połączeń wychodzących";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_REVERSING_QTY"] = "Liczba odpowiedzi telefonicznych";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_TOTAL"] = "Liczba połączeń telefonicznych (według tablicy)";
?>