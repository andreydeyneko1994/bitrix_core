<?
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Przeterminowane faktury";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Liczba przeterminowanych faktur";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Liczba faktur";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Całkowita wartość przeterminowanych faktur";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Całkowita wartość faktur";
?>