<?
$MESS["CRM_MANAGER_CNTR_COMPANY"] = "Firmy";
$MESS["CRM_MANAGER_CNTR_CONTACT"] = "Kontakty";
$MESS["CRM_MANAGER_CNTR_DEAL"] = "Deale";
$MESS["CRM_MANAGER_CNTR_FAILED"] = "Pozostało";
$MESS["CRM_MANAGER_CNTR_LEAD"] = "Leady";
$MESS["CRM_MANAGER_CNTR_SUCCEED"] = "Zakończono";
$MESS["CRM_MANAGER_CNTR_TITLE"] = "Liczniki dla managerów";
$MESS["CRM_MANAGER_CNTR_VIDEO"] = "<div style=\"width:400px;height:100px;font-size:15px;\">Użyj swojego spersonalizowanego licznika, by mierzyć własne postępy: CRM sprawdza ilość ukończonych i pozostałych zadań w danym dniu. Twoim celem jest ukończenie wszystkich zadań przed zakończeniem dnia.</div>";
?>