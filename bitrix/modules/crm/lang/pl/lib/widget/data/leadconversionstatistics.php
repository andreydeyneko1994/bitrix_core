<?
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Skonwertowane leady";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Liczba firm uzyskanych z leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Liczba kontaktów uzyskanych z leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Liczba deali uzyskanych z leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Liczba skonwertowanych leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Liczba leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Całkowita wartość skonwertowanych leadów";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Całkowita wartość leadów";
?>