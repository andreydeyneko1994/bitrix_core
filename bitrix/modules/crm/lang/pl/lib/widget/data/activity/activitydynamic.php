<?
$MESS["CRM_ACTIVITY_DYNAMIC_CHANNELS_ARE_NOT_FOUND"] = "Brak dostępnych kanałów.";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECT"] = "Podłącz: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED"] = "Podłączone: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS"] = "Podłączone kanały";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_IN"] = "Przychodzące";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_OUT"] = "Wychodzące";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE"] = "Komunikatory i usługi społecznościowe";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE_1"] = "Pierwszy Otwarty Kanał";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_LIVECHAT"] = "Czat na żywo";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_VKGROUP"] = "VK";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM"] = "Formularz CRM";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_1"] = "Informacje kontaktowe";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_2"] = "Formularz zwrotny";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_4"] = "Callback z \"Głównego numeru wychodzącego\"";
$MESS["CRM_ACTIVITY_DYNAMIC_TITLE"] = "Podsumowanie komunikacji";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_1"] = "i inne";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_3"] = "Dodaj więcej kanałów: ";
$MESS["CRM_CH_TRACKER_START_VIDEO"] = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GipsvUe1gB0?rel=0&autoplay=1#VOLUME#\" frameborder=\"0\" allowfullscreen></iframe>";
$MESS["CRM_CH_TRACKER_START_VIDEO_TITLE"] = "Możesz obejrzeć film w każdej chwili";
?>