<?
$MESS["CRM_INVOICE_IN_WORK_CATEGORY"] = "Aktywne faktury";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT"] = "Liczba aktywnych faktur";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_OWED"] = "Liczba niezapłaconych faktur";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_SHORT"] = "Liczba faktur";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM"] = "Całkowita wartość aktywnych faktur";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_OWED"] = "Całkowita wartość niezapłaconych faktur";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_SHORT"] = "Całkowita wartość faktur";
$MESS["CRM_INVOICE_OWED_CATEGORY"] = "Niezapłacone faktury";
?>