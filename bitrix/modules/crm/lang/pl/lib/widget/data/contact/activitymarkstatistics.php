<?
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_GROUP_BY_MARK"] = "Ranking aktywności";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_GROUP_BY_SOURCE"] = "Źródło aktywności";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_NEGATIVE_QTY"] = "Liczba aktywności negatywnie ocenionych";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_NONE_QTY"] = "Liczba aktywności bez oceny";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_POSITIVE_QTY"] = "Liczba aktywności pozytywnie ocenionych";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_TOTAL"] = "Liczba aktywności (według ocen)";
?>