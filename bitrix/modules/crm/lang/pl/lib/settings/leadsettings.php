<?php
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Leady zostały skonwertowane.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Nie udało się skonwertować: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Skonwertowano: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "Bez nazwy";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# z #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Konwertuj leady";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "Prosty tryb CRM konwertuje leady w deale. <br/><br/>
W tym trybie otwarte leady zostaną przekonwertowane na deale i klientów. <br/><br/>
Wszystkie reguły automatyzacji i procesy biznesowe skonfigurowane do tworzenia leadów i dealów będą automatycznie i natychmiast realizowane dla nowo utworzonych podmiotów.";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Konwertuj leady";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Lead kanban";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Lista leadów";
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Analityka leadów";
$MESS["CRM_ROBOTS_TEXT"] = "W trybie Prostego CRM Bitrix24 automatycznie konweretuje nowe leady na deale zgodnie z zasadami automatyzacji.<br/><br/>Już posiadasz aktywne zasady automatyzacji. Uruchomienie tego trybu usunie te zasady. <br/><br/>Czy na pewno chcesz uruchomić tryb Prosty CRM?";
$MESS["CRM_ROBOTS_TITLE"] = "Prosty tryb CRM";
$MESS["CRM_TYPE_CANCEL"] = "Anuluj";
$MESS["CRM_TYPE_CONTINUE"] = "Kontynuuj";
$MESS["CRM_TYPE_SAVE"] = "Zapisz";
$MESS["CRM_TYPE_TITLE"] = "Wybierz jak chcesz pracować ze swoim CRM-em";
$MESS["CRM_TYPE_TURN_ON"] = "Włącz";
