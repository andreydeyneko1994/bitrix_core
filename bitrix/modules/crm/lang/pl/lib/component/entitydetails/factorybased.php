<?php
$MESS["CRM_COMPONENT_FACTORYBASED_COPY_PAGE_URL"] = "Kopiuj link pozycji do schowka";
$MESS["CRM_COMPONENT_FACTORYBASED_EDITOR_MAIN_SECTION_TITLE"] = "O pozycji";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TEXT"] = "Domyślnie kwota pozycji jest obliczana jako suma cen produktów. Jednak pozycja określa już inną kwotę. Możesz pozostawić bieżącą wartość niezmienioną lub potwierdzić ponowne obliczenie.";
$MESS["CRM_COMPONENT_FACTORYBASED_MANUAL_OPPORTUNITY_CHANGE_MODE_TITLE"] = "Zmień tryb obliczania kwoty pozycji";
$MESS["CRM_COMPONENT_FACTORYBASED_NEW_ITEM_TITLE"] = "Utwórz #ENTITY_NAME#";
$MESS["CRM_COMPONENT_FACTORYBASED_PAGE_URL_COPIED"] = "Link pozycji został skopiowany do schowka";
$MESS["CRM_COMPONENT_FACTORYBASED_TIMELINE_HISTORY_STUB"] = "Tworzysz pozycję...";
