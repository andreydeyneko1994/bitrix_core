<?
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_HIDE"] = "ukryj przycisk oddzwaniania";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT"] = "zapisz informacje o kliencie i wyświetl komunikat";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT_CALLBACK"] = "Niestety obecnie nie możemy oddzwonić. Skontaktujemy się z Państwem możliwie jak najszybciej w godzinach pracy.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_FR"] = "pt.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_MO"] = "pon.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SA"] = "sob.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SU"] = "niedz.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TH"] = "czw.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TU"] = "wt.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_WE"] = "śr.";
?>