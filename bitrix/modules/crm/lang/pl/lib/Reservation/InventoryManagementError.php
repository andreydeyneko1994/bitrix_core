<?php
$MESS["CRM_RESERVATION_INVENTORY_MANAGEMENT_ERROR"] = "Nie możesz zamknąć deala, ponieważ niektóre produkty nie są dostępne w magazynie lub wybrano nieprawidłowy magazyn. Powoduje to, że zamówienie sprzedaży nie jest rejestrowane w zarządzaniu asortymentem. Usuń z deala produkty, których nie możesz dostarczyć, lub wybierz inny magazyn z wystarczającą ilością zapasów.";
