<?
$MESS["CRM_IMPORT_YAHOO_ERROR_FIELDS_NOT_FOUND"] = "Nie znaleziono żadnego z następujących pól: #FIELD_LIST#";
$MESS["CRM_IMPORT_YAHOO_REQUIREMENTS"] = "Aby pomyślnie zaimportować plik, musi on spełniać następujące wymagania: kodowanie: UTF-8; nazwa pola językowego: angielski; separator pól: przecinek.";
?>