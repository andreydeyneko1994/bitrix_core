<?php
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BIRTHDAY"] = "Urodziny";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_CITY"] = "Firmowe Misto";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_FAX"] = "Firmowy Fax";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE"] = "Firmowy Telefon";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE_2"] = "Firmowy Telefon 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_CODE"] = "Firmowy Kod Pocztowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_COUNTRY"] = "Firmowy Kraj/Region";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STATE"] = "Firmowy Stan";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STREET"] = "Firmowa Ulica";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_CAR_PHONE"] = "Telefon Samochodowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY"] = "Firma";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY_MAIN_PHONE"] = "Główny Telefon Firmowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_2_ADDRESS"] = "Adres E-mail 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_3_ADDRESS"] = "Adres E-mail 3";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_ADDRESS"] = "adres E-mail";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_FIRST_NAME"] = "Imię";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_CITY"] = "Domowe Miasto";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_COUNTRY"] = "Domowy Kraj/Region";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_FAX"] = "Domowy Fax";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE"] = "Telefon Domowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE_2"] = "Domowy Telefon 2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_POSTAL_CODE"] = "Domowy Kod Pocztowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STATE"] = "Domowy Stan";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STREET"] = "Domowa Ulica";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_JOB_TITLE"] = "Firma";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_LAST_NAME"] = "Nazwisko";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MIDDLE_NAME"] = "Drugie Imię";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MOBILE_PHONE"] = "Telefon Komórkowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_NOTES"] = "Notatki";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_CITY"] = "Inne Miasto";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_COUNTRY"] = "Inne Kraj/Region";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_FAX"] = "Inny Fax";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_PHONE"] = "Inny Telefon";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_POSTAL_CODE"] = "Inny Kod Pocztowy";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STATE"] = "Inny Stan";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STREET"] = "Inna Ulica";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PAGER"] = "Pager";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PRIMARY_PHONE"] = "Pierwszy Telefon";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_RADIO_PHONE"] = "Radio Telefon";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_WEB_PAGE"] = "Strona Internetowa";
$MESS["CRM_IMPORT_OUTLOOK_ERROR_FIELDS_NOT_FOUND"] = "Nie znaleziono żadnego z następujących pól: #FIELD_LIST#";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS"] = "Importowany plik powinien spełniać następujące wymagania, aby import był poprawny. Kodowanie pliku: #FILE_ENCODING#. Język dla nagłówków kolumn: #FILE_LANG#. Separator pól: przecinek.";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS_NEW"] = "Aby pomyślnie zaimportować plik, musi on spełniać następujące wymagania: kodowanie: UTF-8; język nazwy pola: #FILE_LANG#; separator pól: przecinek.";
