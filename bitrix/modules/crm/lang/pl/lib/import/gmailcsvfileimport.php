<?php
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Nie znaleziono żadnego z następujących pól: #FIELD_LIST#";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Aby pomyślnie zaimportować plik, musi on spełniać następujące wymagania: kodowanie: UTF-16; nazwa pola językowego: angielski; separator pól: przecinek.";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS_NEW"] = "Aby pomyślnie zaimportować plik, musi on spełniać następujące wymagania: kodowanie: UTF-8; język nazwy pola: angielski; separator pól: przecinek.";
