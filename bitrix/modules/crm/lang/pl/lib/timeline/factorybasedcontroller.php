<?php
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_CATEGORY_CHANGE"] = "Zmieniono pipeline";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_CREATION"] = "Utworzono pozycję";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_MODIFICATION"] = "Zmieniono wartość \"#FIELD_NAME#\"";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_MOVE"] = "Zmieniono etap";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_RESTORATION"] = "Pozycję CRM przywrócono z kosza";
