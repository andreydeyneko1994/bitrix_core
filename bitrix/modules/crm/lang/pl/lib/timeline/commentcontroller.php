<?php
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "Wspomniał(a) o Tobie w komentarzu do \"#ENTITY_TITLE#\". Treść komentarza: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "Wspomniała o Tobie w komentarzu do \"#ENTITY_TITLE#\". Treść komentarza: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "Wspomniał o Tobie w komentarzu do \"#ENTITY_TITLE#\". Treść komentarza: \"#COMMENT#\"";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "firma #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "kontakt #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "deal #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_DYNAMIC"] = "element #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "faktura #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "lead #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_ORDER"] = "oferta #ENTITY_NAME#";
$MESS["CRM_ENTITY_TITLE_QUOTE"] = "oferta #ENTITY_NAME#";
