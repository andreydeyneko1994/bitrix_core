<?php
$MESS["CRM_LEAD_CONVERSION"] = "Utwórz bazując na";
$MESS["CRM_LEAD_CREATION"] = "Dodano lead";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Zmieniono tryb obliczania kwoty łącznej";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Automatycznie oblicz na podstawie cen produktów";
$MESS["CRM_LEAD_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Ręcznie";
$MESS["CRM_LEAD_MODIFICATION_STATUS"] = "Zmieniono status";
$MESS["CRM_LEAD_MOVING_TO_RECYCLEBIN"] = "Leada usunięto do kosza";
$MESS["CRM_LEAD_RESTORATION"] = "Leada przywrócono z kosza.";
