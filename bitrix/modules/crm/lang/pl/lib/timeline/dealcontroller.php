<?php
$MESS["CRM_DEAL_BASE_CAPTION_DEAL_RECURRING"] = "Utworzono z szablonu";
$MESS["CRM_DEAL_BASE_CAPTION_LEAD"] = "Oparte na leadzie";
$MESS["CRM_DEAL_CHECK_TITLE"] = "Pokwitowanie #NAME# z #DATE_PRINT#";
$MESS["CRM_DEAL_CREATION"] = "Dodano deal";
$MESS["CRM_DEAL_CREATION_BASED_ON"] = "Utworzono na podstawie";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY"] = "Zmieniono tryb obliczania kwoty łącznej";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_N"] = "Automatyczne obliczanie przy użyciu pozycji deala";
$MESS["CRM_DEAL_MODIFICATION_IS_MANUAL_OPPORTUNITY_Y"] = "Ręcznie";
$MESS["CRM_DEAL_MODIFICATION_STAGE"] = "Zmieniono etap";
$MESS["CRM_DEAL_MOVING_TO_RECYCLEBIN"] = "Deala usunięto do kosza";
$MESS["CRM_DEAL_RESTORATION"] = "Deala przywrócono z kosza.";
$MESS["CRM_DEAL_SUMMARY_ORDER"] = "Zamówienie ##ORDER_ID# z #ORDER_DATE#";
