<?
$MESS["CRM_DEAL_BASE_CAPTION_BASED_ON_DEAL"] = "Oparte na dealu";
$MESS["CRM_DEAL_CREATION"] = "Dodano deal";
$MESS["CRM_DEAL_RECURRING_ACTIVE"] = "Deal cykliczny jest aktywny";
$MESS["CRM_DEAL_RECURRING_CREATION"] = "Utworzono szablon deala";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION"] = "Kolejna data utworzenia";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION_CHANGED"] = "Zmieniono kolejną datę utworzenia";
$MESS["CRM_DEAL_RECURRING_NOT_ACTIVE"] = "Deal cykliczny nie jest aktywny";
$MESS["CRM_RECURRING_CREATION_BASED_ON"] = "Utworzono na podstawie";
?>