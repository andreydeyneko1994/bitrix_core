<?php
$MESS["CRM_DOCGEN_DATAPROVIDER_SHIPMENT_TITLE"] = "Wypełnienie";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_DATE_INSERT"] = "Utworzono";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_ALLOW_DELIVERY"] = "Zatwierdzono wysyłkę";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_CANCELED"] = "Anulowane";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DATE_ALLOW_DELIVERY"] = "Zatwierdzono wysyłkę ";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DATE_CANCELED"] = "Anulowane ";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DATE_DEDUCTED"] = "Odliczono ";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DATE_MARKED"] = "Oznaczone ";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DEDUCTED"] = "Odliczenia";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DELIVERY_DOC_DATE"] = "Data faktury";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DELIVERY_DOC_NUM"] = "Faktura #";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DELIVERY_ID"] = "ID dostawy";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_DELIVERY_NAME"] = "Nazwa dostawy";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_EMP_ALLOW_DELIVERY_ID"] = "Osoba odpowiedzialna za dostawę";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_EMP_CANCELED_ID"] = "Anulowane przez (ID)";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_EMP_DEDUCTED_ID"] = "Osoba odpowiedzialna za odliczenia";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_EMP_MARKED_ID"] = "Oznaczone przez";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_ID"] = "ID wysyłki";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_MARKED"] = "Oznaczone";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_PRICE_DELIVERY"] = "Cena dostawy";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_REASON_MARKED"] = "Powód oznaczenia";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_REASON_UNDO_DEDUCTED"] = "Powód anulowania odliczeń";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_STATUS_ID"] = "Status wysyłki";
$MESS["CRM_DOCUMENTGENERATOR_DATAPROVIDER_SHIPMENT_TRACKING_NUMBER"] = "Numer przesyłki";
