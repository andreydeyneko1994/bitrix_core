<?php
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_CODE"] = "Automatyczna sprzedaż niewysłanych pozycji podczas realizacji deala";
$MESS["CRM_SALE_RESERVATION_CONFIG_DEAL_AUTO_WRITE_OFF_ON_FINALIZE_DESCRIPTION"] = "Po pomyślnej realizacji deala system sprawdzi niewysłane pozycje i utworzy zamówienie sprzedaży.";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE"] = "Tryb rezerwacji";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_MANUAL"] = "Ręcznie";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_ADD_TO_DOCUMENT"] = "Podczas dodawania produktu do deala";
$MESS["CRM_SALE_RESERVATION_CONFIG_MODE_OPTION_ON_PAYMENT"] = "Po uiszczeniu zapłaty";
$MESS["CRM_SALE_RESERVATION_CONFIG_PERIOD"] = "Rezerwacja na, dni";
$MESS["CRM_SALE_RESERVATION_ENTITY_DEAL"] = "Deal";
$MESS["CRM_SALE_RESERVATION_ENTITY_INVOICE"] = "Faktura";
$MESS["CRM_SALE_RESERVATION_ENTITY_OFFER"] = "Oferta";
$MESS["CRM_SALE_RESERVATION_ENTITY_SMARTPROCESS"] = "Inteligentna automatyzacja procesów";
