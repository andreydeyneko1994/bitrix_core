<?
$MESS["CRM_CALENDAR_HELP_LINK"] = "Dowiedz się więcej";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_DEAL"] = "Wybierz preferowany tryb widoku deali do użytku w kalendarzu: według daty utworzenia, według innej daty lub według dostępności zasobów.";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_LEAD"] = "Wybierz preferowany tryb widoku leadów do użytku w kalendarzu: według daty utworzenia, według innej daty lub według dostępności zasobów.";
$MESS["CRM_CALENDAR_VIEW_SPOTLIGHT"] = "Możesz teraz przeglądać leady i deale w trybie kalendarza. Zaplanuj relacje z klientami, zwracając uwagę na dostępność zasobów.";
?>