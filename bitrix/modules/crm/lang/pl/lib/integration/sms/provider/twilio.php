<?
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_CAN_USE_ERROR"] = "Integracja Twilio nie jest skonfigurowana";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_ERROR_ACCOUNT_INACTIVE"] = "Wybrane konto jest nieaktywne";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_ACCEPTED"] = "Zaakceptowany";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_DELIVERED"] = "Dostarczono";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_FAILED"] = "Błąd";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_QUEUED"] = "W kolejce";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_SENDING"] = "Wysyłam";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_SENT"] = "Wysłana";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_UNDELIVERED"] = "Nie dostarczono";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_NAME"] = "Twilio.com";
?>