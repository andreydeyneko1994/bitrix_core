<?php
$MESS["CRM_IMCONNECTOR_TELEGRAM_ORDER"] = "Zamówienie ##ORDER_ID# z #DATE# na #SUM_WITH_CURRENCY# zostało złożone";
$MESS["CRM_IMCONNECTOR_TELEGRAM_PAYMENT_PAID"] = "Zamówienie ##ORDER_ID# z #DATE# na #SUM_WITH_CURRENCY# zostało opłacone";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_DEDUCTED"] = "Zamówienie ##ORDER_ID# z #DATE# na #SUM_WITH_CURRENCY# zostało wysłane";
$MESS["CRM_IMCONNECTOR_TELEGRAM_SHIPMENT_READY"] = "Zamówienie ##ORDER_ID# z #DATE# na #SUM_WITH_CURRENCY# zostało odebrane z magazynu pod adresem \"#STORE_ADDRESS#\"";
