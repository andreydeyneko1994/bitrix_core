<?
$MESS["WS_CRMINTEGRATION_APP_DESC"] = "Podczas pracy z programu CRM można skonfigurować wymianę danych z innymi programami. Wybierz jedną z opcji połączenia i kliknij przycisk \"Pobierz hasło\".";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_CAPTION"] = "Połącz CRM";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_TITLE_SALE"] = "Integracja z e-sklepem";
$MESS["WS_CRMINTEGRATION_APP_TITLE"] = "CRM";
?>