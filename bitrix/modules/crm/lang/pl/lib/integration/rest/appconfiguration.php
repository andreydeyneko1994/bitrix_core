<?php
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "Aby zapisać formularze CRM w pliku, kliknij \"Eksportuj\"";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "Kliknij \"Eksportuj\", aby zapisać szablony wstępne CRM jako plik";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "Eksportuj formularze CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "Eksport szablonów wstępnych CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "Przygotuj się do korzystania z formularzy CRM w firmie w zaledwie kilka minut!";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "Skonfiguruj CRM dla swojej firmy w zaledwie kilka minut!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "Eksportuj formularze CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "Eksport szablonów wstępnych CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "Formularze CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "Szablony wstępne CRM";
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Wystąpił błąd podczas usuwania danych: jednostka została pominięta (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Wystąpił błąd podczas eksportu danych: jednostka została pominięta (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS"] = "Pole z kodem #CODE# już istnieje i może mieć inny rodzaj. Usuń to pole i zacznij importować ponownie.";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Wystąpił błąd podczas importu danych: jednostka została pominięta (#CODE#)";
