<?php
$MESS["CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Podczas usuwania danych wystąpił błąd/błędy: pominięto element (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Podczas eksportowania danych wystąpił błąd/błędy: pominięto element (#CODE#)";
$MESS["CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Podczas importowania danych wystąpił błąd/błędy: pominięto element (#CODE#)";
