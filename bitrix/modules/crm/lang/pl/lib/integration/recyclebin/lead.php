<?
$MESS["CRM_RECYCLE_BIN_LEAD_ENTITY_NAME"] = "Lead";
$MESS["CRM_RECYCLE_BIN_LEAD_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybrany lead?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVAL_CONFIRMATION"] = "Lead zostanie bezpowrotnie usunięty. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_LEAD_REMOVED"] = "Lead został usunięty.";
$MESS["CRM_RECYCLE_BIN_LEAD_RESTORED"] = "Lead został przywrócony.";
?>