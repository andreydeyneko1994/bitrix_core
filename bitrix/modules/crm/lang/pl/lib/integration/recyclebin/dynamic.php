<?php
$MESS["CRM_RECYCLE_BIN_DYNAMIC_ENTITY_NAME"] = "Pozycja";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybraną pozycję?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVAL_CONFIRMATION"] = "Pozycja zostanie trwale usunięta. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_REMOVED"] = "Pozycja została usunięta.";
$MESS["CRM_RECYCLE_BIN_DYNAMIC_RESTORED"] = "Pozycja została odzyskana.";
