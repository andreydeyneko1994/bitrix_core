<?
$MESS["CRM_RECYCLE_BIN_DEAL_ENTITY_NAME"] = "Deal";
$MESS["CRM_RECYCLE_BIN_DEAL_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybrany deal?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVAL_CONFIRMATION"] = "Deal zostanie bezpowrotnie usunięty. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_DEAL_REMOVED"] = "Deal został usunięty.";
$MESS["CRM_RECYCLE_BIN_DEAL_RESTORED"] = "Deal został przywrócony.";
?>