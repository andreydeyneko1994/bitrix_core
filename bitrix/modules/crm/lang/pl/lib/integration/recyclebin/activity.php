<?
$MESS["CRM_RECYCLE_BIN_ACTIVITY_ENTITY_NAME"] = "Aktywność";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybraną aktywność?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVAL_CONFIRMATION"] = "Aktywność zostanie bezpowrotnie usunięta. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVED"] = "Aktywność została usunięta.";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RESTORED"] = "Aktywność została przywrócona.";
?>