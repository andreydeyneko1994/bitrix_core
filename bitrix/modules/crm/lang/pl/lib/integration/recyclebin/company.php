<?
$MESS["CRM_RECYCLE_BIN_COMPANY_ENTITY_NAME"] = "Firma";
$MESS["CRM_RECYCLE_BIN_COMPANY_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybraną firmę?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVAL_CONFIRMATION"] = "Firma zostanie bezpowrotnie usunięta. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_COMPANY_REMOVED"] = "Firma została usunięta.";
$MESS["CRM_RECYCLE_BIN_COMPANY_RESTORED"] = "Firma została przywrócona.";
?>