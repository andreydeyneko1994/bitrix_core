<?
$MESS["CRM_RECYCLE_BIN_CONTACT_ENTITY_NAME"] = "Kontakt";
$MESS["CRM_RECYCLE_BIN_CONTACT_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybrany kontakt?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVAL_CONFIRMATION"] = "Kontakt zostanie bezpowrotnie usunięty. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_CONTACT_REMOVED"] = "Kontakt został usunięty.";
$MESS["CRM_RECYCLE_BIN_CONTACT_RESTORED"] = "Kontakt został przywrócony.";
?>