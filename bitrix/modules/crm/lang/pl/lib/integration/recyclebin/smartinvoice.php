<?php
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_ENTITY_NAME"] = "Faktura";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RECOVERY_CONFIRMATION"] = "Czy chcesz przywrócić wybraną fakturę?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVAL_CONFIRMATION"] = "Faktura zostanie trwale usunięta. Czy chcesz kontynuować?";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVED"] = "Usunięto fakturę.";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RESTORED"] = "Przywrócono fakturę.";
