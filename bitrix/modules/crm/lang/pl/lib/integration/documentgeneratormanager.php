<?
$MESS["CRM_DOCUMENTGENERATOR_ADD_NEW_TEMPLATE"] = "Dodaj nowy szablon";
$MESS["CRM_DOCUMENTGENERATOR_DOCUMENTS_LIST"] = "Dokumenty";
$MESS["CRM_DOCUMENTGENERATOR_SPOTLIGHT_TEXT"] = "Teraz możesz pracować z szablonami dokumentów. Utwórz dokument, a następnie wydrukuj go lub wyślij pocztą elektroniczną.";
?>