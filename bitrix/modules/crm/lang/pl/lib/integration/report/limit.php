<?
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK"] = "Aby obejść to ograniczenie, usuń niepotrzebne firmy lub uaktualnij swój Bitrix24 do jednego z planów komercyjnych.";
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK_DELETE"] = "Usuń niepotrzebne firmy, aby obejść ograniczenie. <br> <a href=\"#MORE_INFO_LINK#\">Szczegóły</a>";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK"] = "Aby obejść to ograniczenie, usuń niepotrzebne kontakty lub uaktualnij swój Bitrix24 do jednego z planów komercyjnych.";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK_DELETE"] = "Usuń niepotrzebne kontakty, aby obejść ograniczenie. <br> <a href=\"#MORE_INFO_LINK#\">Szczegóły</a>";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK"] = "Aby obejść to ograniczenie, usuń niepotrzebne deale lub uaktualnij swój Bitrix24 do jednego z planów komercyjnych.";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK_DELETE"] = "Usuń niepotrzebne deale, aby obejść ograniczenie. <br> <a href=\"#MORE_INFO_LINK#\">Szczegóły</a>";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK"] = "Aby obejść to ograniczenie, usuń niepotrzebne leady lub uaktualnij swój Bitrix24 do jednego z planów komercyjnych.";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK_DELETE"] = "Usuń niepotrzebne leady, aby obejść ograniczenie. <br> <a href=\"#MORE_INFO_LINK#\">Szczegóły</a>";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_ACTUAL_COUNT_MASK"] = "Bieżąca liczba firm: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_MAX_COUNT_MASK"] = "Raport może zawierać maksymalnie #MAX_COUNT# firm.";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_ACTUAL_COUNT_MASK"] = "Bieżąca liczba kontaktów: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_MAX_COUNT_MASK"] = "Raport może zawierać maksymalnie #MAX_COUNT# kontaktów.";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_ACTUAL_COUNT_MASK"] = "Bieżąca liczba deali: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_MAX_COUNT_MASK"] = "Raport może zawierać do #MAX_COUNT# deali.";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_ACTUAL_COUNT_MASK"] = "Bieżąca liczba leadów: #ACTUAL_COUNT#";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_MAX_COUNT_MASK"] = "Raport może zawierać maksymalnie #MAX_COUNT# leadów.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT"] = "Tylko pewna liczba elementów CRM może być uwzględniona w raporcie analitycznym.";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT_FOR_MORE_INFO"] = "Aby uzyskać więcej informacji, zobacz tabelę porównawczą planów taryfowych.";
?>