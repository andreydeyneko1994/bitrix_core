<?php
$MESS["CRM_TYPE_TABLE_DELETE_ERROR_ITEMS"] = "Nie można usunąć typu obiektu, ponieważ nadal istnieją elementy tego typu.";
$MESS["CRM_TYPE_TABLE_DISABLING_CATEGORIES_IF_MORE_THAN_ONE"] = "Nie można wyłączyć pipelinów, jeśli istnieje więcej niż jeden.";
$MESS["CRM_TYPE_TABLE_DISABLING_RECYCLEBIN_WHILE_NOT_EMPTY"] = "Usuń wszystkie elementy tego typu z Kosza przed jego wyłączeniem.";
$MESS["CRM_TYPE_TABLE_FIELD_NOT_CHANGEABLE_ERROR"] = "Nie można zmienić wartości \"#FIELD#\".";
