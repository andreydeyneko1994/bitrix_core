<?php
$MESS["CRM_WEBFORM_OPTIONS_LINK_EMPTY_FIELD_MAPPING"] = "Nie wybrano pól do konwersji.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_FORM_DUPLICATE_ERROR"] = "Formularz jest już podłączony do CRM";
$MESS["CRM_WEBFORM_OPTIONS_LINK_REGISTER_FAILED"] = "Nie można zasubskrybować wydarzeń formularza.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_TYPE_DUPLICATE"] = "Nie można ponownie połączyć formularza z tą samą usługą";
$MESS["CRM_WEBFORM_OPTIONS_LINK_UNREGISTER_FAILED"] = "Nie można anulować subskrypcji wydarzeń formularza.";
$MESS["CRM_WEBFORM_OPTIONS_LINK_WRONG_TYPE"] = "Nieznana usługa powiązywania formularzy";
