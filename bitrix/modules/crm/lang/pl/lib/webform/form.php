<?
$MESS["CRM_WEBFORM_FORM_BUTTON_CAPTION_DEFAULT"] = "Wyślij";
$MESS["CRM_WEBFORM_FORM_COPY_NAME_PREFIX"] = "Kopiuj";
$MESS["CRM_WEBFORM_FORM_ERROR_CAPTCHA_KEY"] = "Klucz i tajny klucz muszą używać reCAPTCHA";
$MESS["CRM_WEBFORM_FORM_ERROR_SCHEME"] = "Nieprawidłowy schemat dokumentu.";
$MESS["CRM_WEBFORM_FORM_PRESET_FIELDS"] = "W polach domyślnych";
$MESS["CRM_WEBFORM_FORM_SCRIPT_BUTTON_TEXT"] = "Nazwa przycisku";
?>