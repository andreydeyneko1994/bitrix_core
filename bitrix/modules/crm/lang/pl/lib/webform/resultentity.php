<?
$MESS["CRM_WEBFORM_RESULT_ENTITY_ACTIVITY_RESPONSIBLE_IM_NOTIFY"] = "Aktywność \"#title#\" została przydzielona do ciebie";
$MESS["CRM_WEBFORM_RESULT_ENTITY_ACTIVITY_SUBJECT"] = "Wypełniony formularz CRM #";
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_MERGE"] = "Scal";
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_NONE"] = "Pozwól na duplikaty";
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_REPLACE"] = "Podmień";
$MESS["CRM_WEBFORM_RESULT_ENTITY_NOTIFY_SUBJECT"] = "Dodano formularz CRM \"%title%\"";
$MESS["CRM_WEBFORM_RESULT_ENTITY_NOTIFY_SUBJECT_CALL"] = "Prośba o oddzwonienie na telefon \"%phone%\"";
?>