<?php
$MESS["CRM_WEBFORM_FIELD_SYNCHRONIZER_ERR_RES_BOOK"] = "Pole rezerwacji zasobów „%fieldCaption%” nie może zostać utworzone automatycznie. Utwórz pole rezerwacji zasobów w %entityCaption% i wybierz to pole w formularzu.";
