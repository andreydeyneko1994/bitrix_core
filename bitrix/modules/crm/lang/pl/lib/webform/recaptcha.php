<?
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_RESPONSE"] = "Spam sprawdzanie nie powiodło się.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_SECRET"] = "reCAPTCHA: tajny klucz jest niepoprawny.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_RESPONSE"] = "reCAPTCHA: nie podano odpowiedzi.
";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_SECRET"] = "re CAPTCHA: tajny klucz nie jest określony.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_UNKNOWN"] = "Nieznany błąd.";
?>