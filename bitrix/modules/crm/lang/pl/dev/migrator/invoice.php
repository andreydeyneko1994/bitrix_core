<?php
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "Anulowano";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "Wysłano";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "Oczekiwanie na przetwarzanie";
$MESS["CRM_ORDER_STATUS_D"] = "Anulowano";
$MESS["CRM_ORDER_STATUS_F"] = "Zrealizowano";
$MESS["CRM_ORDER_STATUS_N"] = "Zamówienie przyjęte, oczekiwanie na płatność";
$MESS["CRM_ORDER_STATUS_P"] = "Opłacono, przygotowanie do wysyłki";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupa użytkowników, która może edytować preferencje sklepu internetowego";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administratorzy sklepu internetowego";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "Grupa klientów zawierająca wszystkich klientów sklepu";
$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "Wszyscy klienci";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupa użytkowników, która może korzystać z funkcji sklepu internetowego";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Kierownicy sklepu internetowego";
$MESS["SMAIL_FOOTER_BR"] = "Z poważaniem,<br />personel pomocy.";
$MESS["SMAIL_FOOTER_SHOP"] = "Sklep internetowy";
$MESS["UP_TYPE_SUBJECT"] = "Powiadomienie o dostępności w magazynie";
$MESS["UP_TYPE_SUBJECT_DESC"] = "#USER_NAME# – nazwa użytkownika
#EMAIL# – adres e-mail użytkownika 
#NAME# – nazwa produktu
#PAGE_URL# – strona szczegółów produktu";
