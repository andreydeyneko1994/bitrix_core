<?
$MESS["CRM_BP_DEAL_PROPERTIES_1"] = "Proces Biznesowy State-driven";
$MESS["CRM_BP_DEAL_PROPERTIES_10_TITLE"] = "Nieudane";
$MESS["CRM_BP_DEAL_PROPERTIES_11_TITLE"] = "Faktura";
$MESS["CRM_BP_DEAL_PROPERTIES_13_TITLE"] = "Status: nieudane";
$MESS["CRM_BP_DEAL_PROPERTIES_14_TITLE"] = "Oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_15_TITLE"] = "Status: Faktura";
$MESS["CRM_BP_DEAL_PROPERTIES_16_TITLE"] = "Sukces";
$MESS["CRM_BP_DEAL_PROPERTIES_17_TITLE"] = "Status: sukces";
$MESS["CRM_BP_DEAL_PROPERTIES_18_TITLE"] = "Kod PHP";
$MESS["CRM_BP_DEAL_PROPERTIES_19_TITLE"] = "Przedsprzedaż";
$MESS["CRM_BP_DEAL_PROPERTIES_1_TITLE"] = "Status: Nowy (analiza)";
$MESS["CRM_BP_DEAL_PROPERTIES_2"] = "Interfejs wydarzenia";
$MESS["CRM_BP_DEAL_PROPERTIES_20_TITLE"] = "Czas rozpoczęcia zadania";
$MESS["CRM_BP_DEAL_PROPERTIES_2_TITLE"] = "Utwórz ofertę";
$MESS["CRM_BP_DEAL_PROPERTIES_3_TITLE"] = "Ustaw status";
$MESS["CRM_BP_DEAL_PROPERTIES_4_TITLE"] = "Wprowadź status";
$MESS["CRM_BP_DEAL_PROPERTIES_5_TITLE"] = "Edytuj Dokument";
$MESS["CRM_BP_DEAL_PROPERTIES_6_TITLE"] = "Przetwarzanie";
$MESS["CRM_BP_DEAL_PROPERTIES_7_TITLE"] = "Status: nowa oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_8_TITLE"] = "Oferta";
$MESS["CRM_BP_DEAL_TASK_NAME"] = "Przedsprzedaż dla deala: {=Document:TITLE}  (ustawione z procesu biznesowego)";
$MESS["CRM_BP_DEAL_TASK_TEXT"] = "Przerpowadź przedsprzedaż dla deala: {=Document:TITLE}";
$MESS["CRM_BP_DEAL_TITLE"] = "Realizacja Transakcji według etapu";
?>