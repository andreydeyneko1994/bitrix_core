<?
$MESS["CRM_TRACKING_EDITOR_END_CHECKING"] = "Zakończ";
$MESS["CRM_TRACKING_EDITOR_FOUND_ITEMS"] = "Numery telefonów – %phones%, e-maile - %emails%";
$MESS["CRM_TRACKING_EDITOR_NOT_FOUND"] = "Nie znaleziono wpisów.";
$MESS["CRM_TRACKING_EDITOR_NOT_SELECTED"] = "Nie wybrano";
$MESS["CRM_TRACKING_EDITOR_REPLACEMENT"] = "Zamiana numerów telefonów i adresów e-mail";
$MESS["CRM_TRACKING_EDITOR_SOURCES_NOT_CONFIGURED"] = "Nie określono źródeł";
$MESS["CRM_TRACKING_EDITOR_VIEW_ITEMS"] = "Numery telefonów i e-maile źródła";
?>