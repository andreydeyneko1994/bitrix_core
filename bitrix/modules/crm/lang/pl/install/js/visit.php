<?php
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "Zamknij";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "Akceptuję Regulamin";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "WAŻNE: Rozmowa z osobą odwiedzającą zostanie nagrana.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "Postępuj zgodnie ze wszystkimi przepisami prawnymi obowiązującymi w Twoim kraju.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "Zamknij";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "Regulamin użytkowania";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "Domyślna kamera";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "Nie można uzyskać dostęp do mikrofonu. Opis błędu:";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
 Usługę FindFace  (dalej zwana \"Usługą FindFace\") zapewnia N-TECH.LAB LTD. 
Użytkownicy Usługi FindFace są zobowiązani do zawarcia umowy z Usługą FindFace i wyrażenia zgody na <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">następujący regulamin</a> poprzez wybór opcji \"Wyrażam zgodę\". 
 <br>
 Wybierając \"Wyrażam zgodę\", Użytkownik zgadza się na następujące wymagania i gwarantuje ich spełnienie: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Ściśle przestrzegać wszelkich przepisów prawa miejscowego dotyczących ochrony prywatności i wykorzystania danych osobowych obowiązujących w państwie zamieszkania Użytkownika. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Ściśle przestrzegać wszystkich przepisów prawa miejscowego. Wymóg ten obowiązuje przez cały czas korzystania z Usługi FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Uzyskać pisemne upoważnienie, lub inną zgodę wymaganą zgodnie z prawem miejscowym każdego państwa, z którego Użytkownik prowadzi działalność. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Użytkownik ma obowiązek sumiennie informować każdą osobę, której dane osobowe, w tym jej fotografie i inne wizerunki, Użytkownik zamierza przetwarzać poprzez korzystanie z Usługi FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
        Zwrócić się o poradę prawną PRZED skorzystaniem z Usługi FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Zagwarantować, że Użytkownik ma upoważnienie zainteresowanej osoby do korzystania ze wszelkich przesyłanych informacji i postępuje zgodnie z przepisami prawa jurysdykcji, której podlega Użytkownik.
   </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Przyjąć do wiadomości, że Użytkownik korzysta z Usługi FindFace na własne ryzyko i bez żadnej gwarancji. 
   </li>   
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Firma Bitrix24 zrzeka się odpowiedzialności za Usługę i nie składa żadnych zapewnień ani nie udziela gwarancji dotyczących bezbłędności czy dostępności, jak również nie udziela wsparcia technicznego ani konsultacji w sprawach związanych z Usługą. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
         Firma Bitrix24 nie ponosi żadnej odpowiedzialności za Usługę FindFace i nie składa żadnych zapewnień ani nie udziela żadnych gwarancji dotyczących Usługi FindFace.
 Akceptacja regulaminu niniejszej umowy o świadczenie usług jest jednoznaczne z uznaniem faktu, że Usługa FindFace Service ani firma Bitrix24 nie gromadzą ani nie przetwarzają danych przesyłanych przez Użytkownika.
Użytkownik potwierdza, że Usługa FindFace ani firma Bitrix24 nie mają wiedzy na temat okoliczności, w jakich dane, zdjęcia lub wizerunki zostały zgromadzone przez Użytkownika. 
 Użytkownik zgadza się nie wnosić roszczeń wobec Usługi FindFace Service ani firmy Bitrix w związku ze wszelkimi działaniami i zaniechaniami Użytkownika. 
 <br><br>
 Rozstrzyganie sporów; wiążący charakter postępowania arbitrażowego: wszelkie spory, różnice zdań, interpretacji lub roszczeń, w tym m.in. roszczeń z tytułu naruszenia warunków umowy, wszelkich zaniedbań, oszustw lub błędnych interpretacji wynikających z niniejszej umowy bądź z nią związanych, podlegają ostatecznemu i wiążącemu postępowaniu arbitrażowemu. 
   </div>
  </div>";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "Wyszukiwanie danych...";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "Szukaj profilu VK";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "Zmień";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "Zrób jeszcze raz";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "Wybierz";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "Zrób zdjęcie";
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "Odwiedziny";
