<?
$MESS["CRM_STATUSN_D"] = "Nieopłacone";
$MESS["CRM_STATUSN_D_DESCR"] = "Nieopłacone";
$MESS["CRM_STATUSN_N"] = "Nowy";
$MESS["CRM_STATUSN_N_DESCR"] = "Nowy";
$MESS["CRM_STATUSN_P"] = "Zapłacone";
$MESS["CRM_STATUSN_P_DESCR"] = "Zapłacone";
$MESS["CRM_STATUSN_S"] = "Wysłane do klienta";
$MESS["CRM_STATUSN_S_DESCR"] = "Wysłane do klienta";
$MESS["CRM_STATUS_A"] = "Potwierdzone";
$MESS["CRM_STATUS_A_DESCR"] = "Potwierdzone";
$MESS["CRM_STATUS_D"] = "Odmówione";
$MESS["CRM_STATUS_D_DESCR"] = "Odmówione";
$MESS["CRM_STATUS_N"] = "Wersja Robocza";
$MESS["CRM_STATUS_N_DESCR"] = "Wersja Robocza";
$MESS["CRM_STATUS_P"] = "Zakończone";
$MESS["CRM_STATUS_P_DESCR"] = "Zakończone";
$MESS["CRM_STATUS_S"] = "Wysłane do klienta";
$MESS["CRM_STATUS_S_DESCR"] = "Wysłane do klienta";
?>