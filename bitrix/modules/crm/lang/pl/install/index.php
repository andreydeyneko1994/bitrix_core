<?php
$MESS["CRM_COMPANY_TYPE_COMPETITOR"] = "Konkurencja";
$MESS["CRM_COMPANY_TYPE_CUSTOMER"] = "Klient";
$MESS["CRM_COMPANY_TYPE_INTEGRATOR"] = "Integrator";
$MESS["CRM_COMPANY_TYPE_INVESTOR"] = "Inwestor";
$MESS["CRM_COMPANY_TYPE_OTHER"] = "inne";
$MESS["CRM_COMPANY_TYPE_PARTNER"] = "Partner";
$MESS["CRM_COMPANY_TYPE_PRESS"] = "Media";
$MESS["CRM_COMPANY_TYPE_PROSPECT"] = "Perspektywa";
$MESS["CRM_COMPANY_TYPE_RESELLER"] = "Sprzedawca";
$MESS["CRM_CONTACT_TYPE_CLIENT"] = "Klienci";
$MESS["CRM_CONTACT_TYPE_JOURNALIST"] = "Dziennikarze";
$MESS["CRM_CONTACT_TYPE_PARTNER"] = "Partnerzy";
$MESS["CRM_CONTACT_TYPE_SHARE"] = "Ogólne kontakty";
$MESS["CRM_CONTACT_TYPE_SUPPLIER"] = "Dostawcy";
$MESS["CRM_DEAL_STATE_CANCELED"] = "Anuluowane";
$MESS["CRM_DEAL_STATE_COMPLETE"] = "Zakończone";
$MESS["CRM_DEAL_STATE_PLANNED"] = "Zaplanowany";
$MESS["CRM_DEAL_STATE_PROCESS"] = "W toku";
$MESS["CRM_DEAL_TYPE_COMPLEX"] = "Zintegrowana sprzedaż";
$MESS["CRM_DEAL_TYPE_GOODS"] = "Sprzedaż towarów";
$MESS["CRM_DEAL_TYPE_SALE"] = "Sprzedaż";
$MESS["CRM_DEAL_TYPE_SERVICE"] = "Usługi po sprzedaży";
$MESS["CRM_DEAL_TYPE_SERVICES"] = "Usługi";
$MESS["CRM_EMAIL_CONFIRM_EVENT_DESC"] = "<span style=\"font-size:16px;line-height:20px;\">
Wpisz niniejszy kod potwierdzający w swoim Bitrix24, aby potwierdzić adres e-mail.<br>

<span style=\"font-size:24px;line-height:70px;\"><b>#CONFIRM_CODE#</b></span><br>

<span style=\"color:#808080;\">
Po co mam weryfikować swój e-mail?<br><br>
<span style=\"font-size:14px;\">Wymagamy potwierdzenia adresu e-mail, aby zapobiec podszywaniu się (spoofing) i zapewnić, że posiadasz adres e-mail, z którego wysyłane są wiadomości.</span>
</span>";
$MESS["CRM_EMAIL_CONFIRM_EVENT_NAME"] = "Potwierdź adres e-mail";
$MESS["CRM_EMAIL_CONFIRM_TYPE_DESC"] = "#EMAIL# - adres e-mail do potwierdzenia
#CONFIRM_CODE# - kod potwierdzenia";
$MESS["CRM_EMAIL_CONFIRM_TYPE_NAME"] = "Potwierdź adres e-mail nadawcy";
$MESS["CRM_EVENT_TYPE_INFO"] = "Informacje";
$MESS["CRM_EVENT_TYPE_MESSAGE"] = "E-mail został wysłany";
$MESS["CRM_EVENT_TYPE_PHONE"] = "Rozmowa telefoniczna";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Zamknięte deale";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Ostatnie wydarzenia";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Moje leady";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nowe Firmy";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Nowe Kontakty";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Nowe leady";
$MESS["CRM_INDUSTRY_BANKING"] = "Usługi Bankowe";
$MESS["CRM_INDUSTRY_CONSULTING"] = "Doradztwo";
$MESS["CRM_INDUSTRY_DELIVERY"] = "Dostawa";
$MESS["CRM_INDUSTRY_ENTERTAINMENT"] = "Rozrywka";
$MESS["CRM_INDUSTRY_FINANCE"] = "Finanse";
$MESS["CRM_INDUSTRY_GOVERNMENT"] = "Rząd";
$MESS["CRM_INDUSTRY_IT"] = "Technologia Informacyjna";
$MESS["CRM_INDUSTRY_MANUFACTURING"] = "Produkcja";
$MESS["CRM_INDUSTRY_NOTPROFIT"] = "Non-profit";
$MESS["CRM_INDUSTRY_OTHER"] = "inne";
$MESS["CRM_INDUSTRY_TELECOM"] = "Telekomunikacja";
$MESS["CRM_INSTALL_DESCRIPTION"] = "Zapewnia wsparcie CRM.";
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_INSTALL_TITLE"] = "Instalacja Modułu CRM";
$MESS["CRM_ORD_PROP_2"] = "Zasób";
$MESS["CRM_ORD_PROP_4"] = "Kod pocztowy";
$MESS["CRM_ORD_PROP_5"] = "Adres dostawy";
$MESS["CRM_ORD_PROP_6"] = "Pełna nazwa";
$MESS["CRM_ORD_PROP_7"] = "Adres siedziby";
$MESS["CRM_ORD_PROP_8"] = "Nazwa firmy";
$MESS["CRM_ORD_PROP_9"] = "Telefon";
$MESS["CRM_ORD_PROP_10"] = "Osoba kontaktowa";
$MESS["CRM_ORD_PROP_11"] = "Fax";
$MESS["CRM_ORD_PROP_12"] = "Adres dostawy";
$MESS["CRM_ORD_PROP_13"] = "ID Podatnika";
$MESS["CRM_ORD_PROP_14"] = "Atrapa";
$MESS["CRM_ORD_PROP_21"] = "Miasto";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "Dane Osobowe";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "Informacje o dostawie";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "Informacje o firmie";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "Informacje kontaktowe";
$MESS["CRM_PAGE_FUNNEL"] = "Lejek sprzedażowy";
$MESS["CRM_PERM_D"] = "Odmowa dostępu";
$MESS["CRM_PERM_R"] = "Wyświetl strony";
$MESS["CRM_PERM_W"] = "Utwórz i edytuj strony";
$MESS["CRM_PERM_X"] = "Utwórz i edytuj strony w edytorze";
$MESS["CRM_PERM_Y"] = "Usuń strony i historię";
$MESS["CRM_PERM_Z"] = "Ustaw Zgodę";
$MESS["CRM_ROLE_ADMIN"] = "Administrator";
$MESS["CRM_ROLE_CHIF"] = "Kierownik działu";
$MESS["CRM_ROLE_DIRECTOR"] = "Dyrektor generalny";
$MESS["CRM_ROLE_MAN"] = "Manager";
$MESS["CRM_SALE_STATUS_A"] = "Potwierdzone";
$MESS["CRM_SALE_STATUS_D"] = "Odrzucony";
$MESS["CRM_SALE_STATUS_P"] = "Zakończone";
$MESS["CRM_SALE_STATUS_S"] = "Wysłane do klienta";
$MESS["CRM_STATUS_TYPE_SOURCE_CALL"] = "Połączenie";
$MESS["CRM_STATUS_TYPE_SOURCE_COMPANY"] = "Kampania";
$MESS["CRM_STATUS_TYPE_SOURCE_CONFERENCE"] = "Konferencja";
$MESS["CRM_STATUS_TYPE_SOURCE_EMAIL"] = "E-mail";
$MESS["CRM_STATUS_TYPE_SOURCE_EMPLOYEE"] = "Pracownik";
$MESS["CRM_STATUS_TYPE_SOURCE_HR"] = "Dział HR";
$MESS["CRM_STATUS_TYPE_SOURCE_MAIL"] = "Wiadomość e-mail";
$MESS["CRM_STATUS_TYPE_SOURCE_OTHER"] = "inne";
$MESS["CRM_STATUS_TYPE_SOURCE_PARTNER"] = "Istniejący Klient";
$MESS["CRM_STATUS_TYPE_SOURCE_SELF"] = "Kontakt Osobisty";
$MESS["CRM_STATUS_TYPE_SOURCE_TRADE_SHOW"] = "Pokaz/Ekspozycja";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB"] = "Strona internetowa";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB_FORM"] = "Formularz Sieciowy";
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_UF_NAME"] = "Elementy CRM";
$MESS["CRM_UF_NAME_CAL"] = "Elementy CRM";
$MESS["CRM_UF_NAME_LF_ID"] = "ID jednostki CRM dla Aktualności";
$MESS["CRM_UF_NAME_LF_TYPE"] = "Kod jednostki CRM dla Aktualności";
$MESS["CRM_UNINSTALL_TITLE"] = "Deinstalacja Modułu crm";
$MESS["CRM_VAT_1"] = "Bez podatku";
$MESS["CRM_VAT_2"] = "Stawka podatku: 18%";
