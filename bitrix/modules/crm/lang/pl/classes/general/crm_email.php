<?
$MESS["CRM_ADD_MESSAGE"] = "nowa wiadomość CRM";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "Zwolniony pracownik istnieje w kolejce rozsyłki wiadomości e-mail dla skrzynki pocztowej \"#EMAIL#\". Nie będzie otrzymywać wiadomości e-mail z nowych źródeł, ale wszystkie istniejące wiadomości e-mail nadal będą tej osobie przekazywane. <a href=\"#CONFIG_URL#\">Skonfiguruj parametry kolejki</a>.";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Pliki niedostępne (maksymalna wielkość przekroczyła: %MAX_SIZE% MB )";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Dodaj do treści wiadomości";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "Nie dodawaj";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Dodaj do tematu wiadomości";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(bez tematu)";
$MESS["CRM_EMAIL_EMAILS"] = "E-mail";
$MESS["CRM_EMAIL_FROM"] = "od";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Wyślij i zapisz wiadomość";
$MESS["CRM_EMAIL_SUBJECT"] = "Tytuł";
$MESS["CRM_EMAIL_TO"] = "do";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Nazwa Firmy: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "utworzony z wiadomości %SENDER%, która nie może być powiązana z którymkolwiek istniejącym leadem, kontaktem lub firmą.";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Lead z wiadomości %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Lead z wiadomości przekazanej od %SENDER%";
?>