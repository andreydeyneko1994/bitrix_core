<?
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "Serwer zwrócił pustą odpowiedź.";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "Błąd analizy składniowej odpowiedzi serwera.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "Niezidentyfikowana odpowiedź serwera. Pierwsze 100 znaków:";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "Błąd wymiany danych sklepu internetowego.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "Niewystarczające uprawnienia. Proszę sprawdzić czy użytkownik ma uprawnienia dostępu do zamawiania exportu.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "Niewystarczające uprawnienia. Proszę sprawdzić czy użytkownik ma uprawnienia dostępu do zamawiania exportu.";
$MESS["CRM_EXT_SALE_IM_GROUP"] = "Grupa";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "Błąd importu";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "Importuj wynik";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "Błąd synchronizacji:<br />próby połączeń: 10, ostatnia: #DATE#<br />połączenie zostało deaktywowane.<br /><br />Aby uzyskać więcej informacji, otwórz formularz połączenia (<a href='#URL#'>CRM &gt; Ustawienia &gt; Łączność sklepu internetowego</a>).";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "Synchronizacja z \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "Synchronizacja zakończona, otrzymano:<br />#TOTALDEALS# <a href=\"#DEAL_URL#\">deali</a> (#CREATEDDEALS# nowych)<br />#TOTALCONTACTS# <a href=\"#CONTACT_URL#\">kontaktów</a> (#CREATEDCONTACTS# nowych)<br />#TOTALCOMPANIES# <a href=\"#COMPANY_URL#\">firm</a> (#CREATEDCOMPANIES# nowych)";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "Synchronizacja z \"#NAME#\"";
?>