<?
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "Pole \"%FIELD_NAME%\" jest nieprawidłowe.";
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "Pole \"%FIELD_NAME%\" jest wymagane.";
$MESS["CRM_FIELD_ADDRESS"] = "Adres";
$MESS["CRM_FIELD_COMMENTS"] = "Komentarz";
$MESS["CRM_FIELD_COMPANY_ID"] = "Firma";
$MESS["CRM_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_FIELD_FIND"] = "Szukaj";
$MESS["CRM_FIELD_LAST_NAME"] = "Nazwisko";
$MESS["CRM_FIELD_MESSENGER"] = "Komunikator";
$MESS["CRM_FIELD_NAME"] = "Imię";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Wartość";
$MESS["CRM_FIELD_PHONE"] = "Telefon";
$MESS["CRM_FIELD_PHOTO"] = "Galeria";
$MESS["CRM_FIELD_POST"] = "Stanowisko";
$MESS["CRM_FIELD_SECOND_NAME"] = "Drugie Imię";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Opis";
$MESS["CRM_FIELD_SOURCE_ID"] = "Źródło";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Opis";
$MESS["CRM_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_FIELD_TYPE_ID"] = "Typ kontaktu";
$MESS["CRM_FIELD_WEB"] = "Strona";
?>