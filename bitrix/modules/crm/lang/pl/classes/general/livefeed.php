<?php
$MESS["CCRM_LF_SIP_MGR_MAKE_CALL"] = "Połączenie";
$MESS["CRM_LF_ACTIVITY_CALL_COMPANY_COMMENT_MENTION_TITLE"] = "do posta odnośnie rozmowy telefonicznej w firmie #title#";
$MESS["CRM_LF_ACTIVITY_CALL_COMPLETED"] = "(zakończone)";
$MESS["CRM_LF_ACTIVITY_CALL_CONTACT_COMMENT_MENTION_TITLE"] = "do posta odnośnie rozmowy telefonicznej w kontakcie #title#";
$MESS["CRM_LF_ACTIVITY_CALL_DEAL_COMMENT_MENTION_TITLE"] = "do posta odnośnie rozmowy telefonicznej w dealu #title#";
$MESS["CRM_LF_ACTIVITY_CALL_INCOMING_TITLE"] = "Przychodzące połączenie telefoniczne";
$MESS["CRM_LF_ACTIVITY_CALL_LEAD_COMMENT_MENTION_TITLE"] = "do posta odnośnie rozmowy telefonicznej w leadzie #title#";
$MESS["CRM_LF_ACTIVITY_CALL_OUTGOING_TITLE"] = "Połączenie telefoniczne wychodzące #COMPLETED#";
$MESS["CRM_LF_ACTIVITY_CALL_TITLE"] = "Spotkanie #COMPLETED#";
$MESS["CRM_LF_ACTIVITY_EMAIL_COMPANY_COMMENT_MENTION_TITLE"] = "do posta odnośnie e-maila w firmie #title#";
$MESS["CRM_LF_ACTIVITY_EMAIL_COMPLETED"] = "(zakończone)";
$MESS["CRM_LF_ACTIVITY_EMAIL_CONTACT_COMMENT_MENTION_TITLE"] = "do posta odnośnie e-maila w kontakcie #title#";
$MESS["CRM_LF_ACTIVITY_EMAIL_DEAL_COMMENT_MENTION_TITLE"] = "do posta odnośnie e-maila w dealu #title#";
$MESS["CRM_LF_ACTIVITY_EMAIL_INCOMING_TITLE"] = "Przysłany e-mail";
$MESS["CRM_LF_ACTIVITY_EMAIL_LEAD_COMMENT_MENTION_TITLE"] = "do posta odnośnie e-maila w leadzie #title#";
$MESS["CRM_LF_ACTIVITY_EMAIL_OUTGOING_TITLE"] = "Wysłany e-mail";
$MESS["CRM_LF_ACTIVITY_MEETING_COMPANY_COMMENT_MENTION_TITLE"] = "do posta odnośnie spotkania w firmie #title#";
$MESS["CRM_LF_ACTIVITY_MEETING_COMPLETED"] = "(zakończone)";
$MESS["CRM_LF_ACTIVITY_MEETING_CONTACT_COMMENT_MENTION_TITLE"] = "do posta odnośnie spotkania w kontakcie #title#";
$MESS["CRM_LF_ACTIVITY_MEETING_DEAL_COMMENT_MENTION_TITLE"] = "do posta odnośnie spotkania w dealu #title#";
$MESS["CRM_LF_ACTIVITY_MEETING_LEAD_COMMENT_MENTION_TITLE"] = "do posta odnośnie spotkania w leadzie #title#";
$MESS["CRM_LF_ACTIVITY_MEETING_NOT_COMPLETED"] = "(nierozpoczęte jeszcze)";
$MESS["CRM_LF_ACTIVITY_MEETING_TITLE"] = "Spotkanie #COMPLETED#";
$MESS["CRM_LF_COMMENT_IM_NOTIFY"] = "skomentowano #title#";
$MESS["CRM_LF_COMMENT_IM_NOTIFY_F"] = "skomentowano #title#";
$MESS["CRM_LF_COMMENT_IM_NOTIFY_M"] = "skomentowano #title#";
$MESS["CRM_LF_COMMENT_MENTION"] = "zostałeś wspomniany w komentarzu #title#";
$MESS["CRM_LF_COMMENT_MENTION_F"] = "zostałeś wspomniany w komentarzu #title#";
$MESS["CRM_LF_COMMENT_MENTION_M"] = "zostałeś wspomniany w komentarzu #title#";
$MESS["CRM_LF_COMMON_PRESET_ALL"] = "Wszystkie";
$MESS["CRM_LF_COMMON_PRESET_MY"] = "Moje";
$MESS["CRM_LF_COMPANY_ADD_COMMENT_MENTION_TITLE"] = "do posta odnośnie nowej firmy #title#";
$MESS["CRM_LF_COMPANY_MESSAGE_COMMENT_MENTION_TITLE"] = "do posta dodanego do firmy #title#";
$MESS["CRM_LF_COMPANY_PRESET_TOP"] = "Firma";
$MESS["CRM_LF_COMPANY_PRESET_TOP_EXTENDED"] = "Firma / Kontakty";
$MESS["CRM_LF_COMPANY_RESPONSIBLE_COMMENT_MENTION_TITLE"] = "do posta odnośnie zmiany osoby odpowiedzialnej: #title#";
$MESS["CRM_LF_CONTACT_ADD_COMMENT_MENTION_TITLE"] = "do posta odnośnie nowego kontaktu #title#";
$MESS["CRM_LF_CONTACT_MESSAGE_COMMENT_MENTION_TITLE"] = "do posta dodanego do kontaktu #title#";
$MESS["CRM_LF_CONTACT_RESPONSIBLE_COMMENT_MENTION_TITLE"] = "do posta odnośnie zmiany osoby odpowiedzialnej za kontakt: #title#";
$MESS["CRM_LF_CRMCOMPANY_DESTINATION_PREFIX"] = "Firma";
$MESS["CRM_LF_CRMCONTACT_DESTINATION_PREFIX"] = "Kontakt";
$MESS["CRM_LF_CRMDEAL_DESTINATION_PREFIX"] = "Deal";
$MESS["CRM_LF_CRMLEAD_DESTINATION_PREFIX"] = "Lead";
$MESS["CRM_LF_DEAL_ADD_COMMENT_MENTION_TITLE"] = "do posta odnośnie nowego deala #title#";
$MESS["CRM_LF_DEAL_MESSAGE_COMMENT_MENTION_TITLE"] = "do posta dodanego do deala #title#";
$MESS["CRM_LF_DEAL_PROGRESS_COMMENT_MENTION_TITLE"] = "do deala \"#title#\" powiadomienie o zmianie etapu";
$MESS["CRM_LF_DEAL_RESPONSIBLE_COMMENT_MENTION_TITLE"] = "do posta odnośnie zmiany osoby odpowiedzialnej za deal: #title#";
$MESS["CRM_LF_EVENT_ADD"] = "Utworzono pozycję \"#ENTITY_TYPE_CAPTION#\"";
$MESS["CRM_LF_EVENT_BECOME_RESPONSIBLE"] = "Ponosisz teraz odpowiedzialność za pozycję #ENTITY_TYPE_CAPTION# \"<a href=\"#URL#\" class=\"bx-notifier-item-action\">#TITLE#</a>\"";
$MESS["CRM_LF_EVENT_FIELD_CHANGED"] = "Zmieniono pole \"#FIELD_CAPTION#\"";
$MESS["CRM_LF_EVENT_IM_POST"] = "zaadresował post do ciebie \"#title#\"";
$MESS["CRM_LF_EVENT_IM_POST_F"] = "zaadresowała post do ciebie \"#title#\"";
$MESS["CRM_LF_EVENT_IM_POST_M"] = "zaadresował post do ciebie \"#title#\"";
$MESS["CRM_LF_EVENT_NO_LONGER_RESPONSIBLE"] = "Nie ponosisz już odpowiedzialności za pozycję #ENTITY_TYPE_CAPTION# \"<a href=\"#URL#\" class=\"bx-notifier-item-action\">#TITLE#</a>\"";
$MESS["CRM_LF_EVENT_STAGE_CHANGED"] = "Etap pozycji #ENTITY_TYPE_CAPTION# \"<a href=\"#URL#\" class=\"bx-notifier-item-action\">#TITLE#</a>\" zmieniono z \"#START_STAGE_CAPTION#\" na \"#FINAL_STAGE_CAPTION#\"";
$MESS["CRM_LF_IM_COMMENT_TITLE_ACTIVITY_CALL_ADD"] = "twój post do nowego połączenia telefonicznego #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_ACTIVITY_EMAIL_ADD"] = "twój post do nowego e-maila #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_ACTIVITY_MEETING_ADD"] = "twój post do nowego spotkania #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_ACTIVITY_TASK_ADD"] = "twój post o nowym zadaniu #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_COMPANY_ADD"] = "twój post do nowej firmy #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_COMPANY_MESSAGE"] = "twoja wiadomość \"#message_title#\" w firmie #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_CONTACT_ADD"] = "twój post do nowego kontaktu #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_CONTACT_MESSAGE"] = "twoja wiadomość \"#message_title#\" w kontakcie #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_DEAL_ADD"] = "twój post do nowego deala #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_DEAL_MESSAGE"] = "twoja wiadomość \"#message_title#\" w dealu #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_DEAL_PROGRESS"] = "twój post do zmiany statusu deala: #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_INVOICE_ADD"] = "twój post do nowej faktury #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_LEAD_ADD"] = "twój post do nowego leadu #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_LEAD_MESSAGE"] = "twoja wiadomość \"#message_title#\" w leadzie #title#";
$MESS["CRM_LF_IM_COMMENT_TITLE_LEAD_PROGRESS"] = "twój post do zmiany statusu leada: #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_ACTIVITY_CALL_ADD"] = "twój post do nowego połączenia telefonicznego #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_ACTIVITY_EMAIL_ADD"] = "twój post do nowego e-maila #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_ACTIVITY_MEETING_ADD"] = "twój post do nowego spotkania #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_ACTIVITY_TASK_ADD"] = "twój post o nowym zadaniu #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_COMPANY_ADD"] = "twój post do nowej firmy #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_COMPANY_MESSAGE"] = "twoja wiadomość \"#message_title#\" w firmie #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_CONTACT_ADD"] = "twój post do nowego kontaktu #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_CONTACT_MESSAGE"] = "twoja wiadomość \"#message_title#\" w kontakcie #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_DEAL_ADD"] = "twój post do nowego deala #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_DEAL_MESSAGE"] = "twoja wiadomość \"#message_title#\" w dealu #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_DEAL_PROGRESS"] = "twój post do zmiany statusu deala: #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_INVOICE_ADD"] = "twój post do nowej faktury #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_LEAD_ADD"] = "twój post do nowego leadu #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_LEAD_MESSAGE"] = "twoja wiadomość \"#message_title#\" w leadzie #title#";
$MESS["CRM_LF_IM_LIKE_TITLE_LEAD_PROGRESS"] = "twój post do zmiany statusu leada: #title#";
$MESS["CRM_LF_INVOICE_ADD_COMMENT_MENTION_TITLE"] = "do posta odnośnie faktury ##id#";
$MESS["CRM_LF_LEAD_ADD_COMMENT_MENTION_TITLE"] = "do posta odnośnie nowego leada #title#";
$MESS["CRM_LF_LEAD_MESSAGE_COMMENT_MENTION_TITLE"] = "do posta dodanego do leada #title#";
$MESS["CRM_LF_LEAD_PROGRESS_COMMENT_MENTION_TITLE"] = "do leada \"#title#\" powiadomienie o zmianie etapu";
$MESS["CRM_LF_LEAD_RESPONSIBLE_COMMENT_MENTION_TITLE"] = "do posta odnośnie zmiany osoby odpowiedzialnej za lead: #title#";
$MESS["CRM_LF_LIKE_IM_NOTIFY"] = "Lubię #title#";
$MESS["CRM_LF_MESSAGE_TITLE_24"] = "Wiadomość";
$MESS["CRM_LF_MSG_EMPTY"] = "Proszę wprowadzić wiadomość testową.";
$MESS["CRM_LF_MSG_ENTITY_TYPE_NOT_FOUND"] = "Nie znaleziono jednostki wiadomości";
$MESS["CRM_LF_NOTIFICATIONS_DISLIKE_COMMENT"] = "Nie podoba mi się już Twój komentarz w aktualnościach CRM \"#LINK#\"";
$MESS["CRM_LF_NOTIFICATIONS_REACT_COMMENT"] = "Zareagowano na Twój komentarz w aktualnościach CRM \"#LINK#\"
\"#REACTION#\"";
$MESS["CRM_LF_NOTIFICATIONS_REACT_COMMENT_F"] = "Zareagowała na Twój komentarz w aktualnościach CRM \"#LINK#\"
\"#REACTION#\"";
$MESS["CRM_LF_NOTIFICATIONS_REACT_COMMENT_M"] = "Zareagował na Twój komentarz w aktualnościach CRM \"#LINK#\"
\"#REACTION#\"";
$MESS["CRM_LF_PRESET_ACTIVITIES"] = "Działania";
$MESS["CRM_LF_PRESET_ALL"] = "Wszystkie";
$MESS["CRM_LF_PRESET_MESSAGES"] = "wiadomości";
$MESS["CRM_LF_SIP_MGR_ENABLE_CALL_RECORDING"] = "Zapis rozmowy";
$MESS["CRM_LF_SIP_MGR_UNKNOWN_RECIPIENT"] = "Nierozpozany numer";
$MESS["CRM_SL_DELETE_COMMENT_SOURCE_ERROR_FORUM_NOT_INSTALLED"] = "Moduł Forum nie jest zainstalowany";
$MESS["CRM_SL_EVENT_EDIT_EMPTY_MESSAGE"] = "Proszę wprowadzić treść wiadomości.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_NOT_DEFINED"] = "Proszę wybrać przynajmniej jeden lead, kontakt, firmę lub deal.";
$MESS["CRM_SL_EVENT_EDIT_PERMISSION_DENIED"] = "Nie masz dostępu do utworzenia wiadomości w '#TITLE#'.";
$MESS["CRM_SL_EVENT_IM_MENTION_POST"] = "wspomniał cię w poście \"#title#\"";
$MESS["CRM_SL_EVENT_IM_MENTION_POST_F"] = "wspomniała cię w poście \"#title#\"";
$MESS["CRM_SL_EVENT_IM_MENTION_POST_M"] = "wspomniał cię w poście \"#title#\"";
$MESS["CRM_SL_UPDATE_COMMENT_SOURCE_ERROR"] = "Nie można zaktualizować komentarza do źródła wydarzenia";
$MESS["C_CRM_LF_CLIENT_ID_TITLE"] = "Klient";
$MESS["C_CRM_LF_CLIENT_NAME_TITLE"] = "Klient";
$MESS["C_CRM_LF_COMBI_TITLE_ID_VALUE"] = "# #ID# #TITLE#";
$MESS["C_CRM_LF_COMPANY_TITLE"] = "Firma";
$MESS["C_CRM_LF_COMPANY_TITLE_TITLE"] = "Firma";
$MESS["C_CRM_LF_COMPANY_TYPE_TITLE"] = "Typ firmy";
$MESS["C_CRM_LF_CONTACT_NAME_TITLE"] = "Utworzono kontakt";
$MESS["C_CRM_LF_CONTACT_TITLE"] = "Kontakt";
$MESS["C_CRM_LF_CRMCOMPANY_ADD_TITLE"] = "Firma dodana";
$MESS["C_CRM_LF_CRMCONTACT_ADD_TITLE"] = "Utworzono kontakt";
$MESS["C_CRM_LF_CRMDEAL_ADD_TITLE"] = "Dodano deal";
$MESS["C_CRM_LF_CRMLEAD_ADD_TITLE"] = "Dodano lead";
$MESS["C_CRM_LF_DATE_TITLE"] = "Kiedy";
$MESS["C_CRM_LF_DEAL_FINAL_STATUS_ID_TITLE"] = "Etap został zmieniony na";
$MESS["C_CRM_LF_DEAL_START_STATUS_ID_TITLE"] = "Poprzedni etap";
$MESS["C_CRM_LF_DEAL_STATUS_TITLE"] = "Etap";
$MESS["C_CRM_LF_DEAL_TITLE"] = "Deal";
$MESS["C_CRM_LF_EMAILS_TITLE"] = "E-mail";
$MESS["C_CRM_LF_FINAL_CLIENT_ID_TITLE"] = "Nowy klient";
$MESS["C_CRM_LF_FINAL_OWNER_COMPANY_ID_TITLE"] = "Nowa Firma";
$MESS["C_CRM_LF_FINAL_RESPONSIBLE_ID_TITLE"] = "Nowa osoba odpowiedzialna";
$MESS["C_CRM_LF_FINAL_STATUS_ID_TITLE"] = "Status zmieniono na";
$MESS["C_CRM_LF_FINAL_TITLE_TITLE"] = "Nowa nazwa";
$MESS["C_CRM_LF_INVOICE_ADD_TITLE_TITLE"] = "Utworzona faktura";
$MESS["C_CRM_LF_LOCATION_TITLE"] = "Zasób";
$MESS["C_CRM_LF_LOGO_TITLE"] = "Logo";
$MESS["C_CRM_LF_OPPORTUNITY_TITLE"] = "Razem";
$MESS["C_CRM_LF_PHONES_TITLE"] = "Telefony";
$MESS["C_CRM_LF_PRICE_TITLE"] = "Kwota";
$MESS["C_CRM_LF_RESPONSIBLE_TITLE"] = "Osoba odpowiedzialna";
$MESS["C_CRM_LF_REVENUE_TITLE"] = "Roczne Przychody";
$MESS["C_CRM_LF_START_CLIENT_ID_TITLE"] = "Poprzedni klient";
$MESS["C_CRM_LF_START_OWNER_COMPANY_ID_TITLE"] = "Poprzednia firma";
$MESS["C_CRM_LF_START_RESPONSIBLE_ID_TITLE"] = "Poprzednia osoba odpowiedzialna";
$MESS["C_CRM_LF_START_STATUS_ID_TITLE"] = "Poprzedni status";
$MESS["C_CRM_LF_START_TITLE_TITLE"] = "Poprzednia nazwa";
$MESS["C_CRM_LF_STATUS_TITLE"] = "Status";
$MESS["C_CRM_LF_SUBJECT_TITLE"] = "Temat";
$MESS["C_CRM_LF_SUBJECT_TITLE_EMPTY"] = "Brak tematu";
