<?
$MESS["CRM_CURRERCY_ERR_DELETION_OF_ACCOUNTING_CURRENCY"] = "Waluta raportowania nie może być usunięta.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_BASE_CURRENCY"] = "Waluta podstawowa nie może być usunięta.";
$MESS["CRM_CURRERCY_MODULE_IS_NOT_INSTALLED"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_CURRERCY_MODULE_WARNING"] = "Uwaga! Moduł \"Waluty\" jest wymagany dla tej operacji.";
?>