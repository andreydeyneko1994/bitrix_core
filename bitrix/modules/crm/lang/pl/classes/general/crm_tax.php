<?php
$MESS["CRM_CATALOG_MODULE_IS_NOT_INSTALLED"] = "Nie znaleziono modułu Katalog Produktów. Proszę zainstalować ten moduł.";
$MESS["CRM_MODULE_IS_NOT_INSTALLED"] = "Nie znaleziono modułu CRM. Proszę zainstalować ten moduł.";
$MESS["CRM_SALE_MODULE_IS_NOT_INSTALLED"] = "Nie znaleziono modułu e-Sklepu. Proszę zainstalować ten moduł.";
$MESS["CRM_VAT_EMPTY_VALUE"] = "Bez podatku";
$MESS["CRM_VAT_NOT_SELECTED"] = "[nie wybrano]";
