<?php
$MESS["CRM_EVENT_PROD_ROW_ADD"] = "Produkt dodano";
$MESS["CRM_EVENT_PROD_ROW_DISCOUNT_UPD"] = "Zniżka dla \"#NAME#\" została zmieniona";
$MESS["CRM_EVENT_PROD_ROW_MEASURE_UPD"] = "Zmieniono jednostkę miary na \"#NAME#\"";
$MESS["CRM_EVENT_PROD_ROW_NAME_UPD"] = "Zmieniono nazwę produktu";
$MESS["CRM_EVENT_PROD_ROW_PRICE_UPD"] = "Cena '#NAME#' zaktualizowana";
$MESS["CRM_EVENT_PROD_ROW_QTY_UPD"] = "Ilość '#NAME#' zaktualizowana";
$MESS["CRM_EVENT_PROD_ROW_REM"] = "Usunięty produkt";
$MESS["CRM_EVENT_PROD_ROW_TAX_UPD"] = "Zmieniono podatek za \"#NAME#\"";
$MESS["CRM_EVENT_PROD_ROW_UPD"] = "Produkt zmodyfikowano";
$MESS["CRM_PRODUCT_ROW_FIELD_ID"] = "ID";
$MESS["CRM_PRODUCT_ROW_FIELD_MEASURE_NAME"] = "Jednostka miary";
$MESS["CRM_PRODUCT_ROW_FIELD_VAT_RATE"] = "Stawka podatku";
