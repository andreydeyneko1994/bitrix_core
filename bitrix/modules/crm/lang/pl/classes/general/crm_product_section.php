<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "Nie można usunąć sekcji, ponieważ zawiera ona produkty odnoszące się do dela, leada, oferty lub faktury.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "Nazwa sekcji nie jest określona.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "Nie znaleziono sekcji.";
$MESS["CRM_PRODUCT_SECTION_FIELD_ID"] = "ID";
?>