<?php
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Masz nowe e-maile";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Prośba o oddzwonienie";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_ASSIGNED_BY"] = "Wyznaczono Cię jako osobę odpowiedzialną";
$MESS["CRM_NOTIFY_SCHEME_ENTITY_STAGE"] = "Zmieniono etap";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Zostałeś wspomniany w poście";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Zostałeś podany jako odbiorca posta";
$MESS["CRM_NOTIFY_SCHEME_MERGE"] = "Zduplikowane powiadomienia kontrolne";
$MESS["CRM_NOTIFY_SCHEME_OTHER"] = "Inne powiadomienia";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Przesłano formularz CRM";
