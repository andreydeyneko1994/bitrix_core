<?
$MESS["CRM_VCARD_ERR_FORMAT"] = "Nieprawidłowy format pliku vCard";
$MESS["CRM_VCARD_ERR_READ"] = "Błąd odczytu vCard";
$MESS["CRM_VCARD_ERR_TMP_FILE"] = "Błąd podczas tworzenia pliku tymczasowego vCard";
$MESS["CRM_VCARD_ERR_VERSION"] = "Nie można określić wersji vCard";
?>