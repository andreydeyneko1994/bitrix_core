<?
$MESS["CRM_EVENT_ERR_ENTITY_DATE_NOT_VALID"] = "Data wydarzenia jest błędna.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "ID elementu jednostki nie jest określone.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "Typ wydarzenia nie jest określony.";
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "ID jednostki nie jest określone.";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Zmiany";
$MESS["CRM_EVENT_TYPE_DELETE"] = "Usuń";
$MESS["CRM_EVENT_TYPE_EXPORT"] = "Export";
$MESS["CRM_EVENT_TYPE_SNS"] = "E-mail przychodzący";
$MESS["CRM_EVENT_TYPE_USER"] = "Wydarzenia użytkownika";
$MESS["CRM_EVENT_TYPE_VIEW"] = "Widok";
?>