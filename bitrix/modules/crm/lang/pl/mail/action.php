<?
$MESS["CRM_ENTITY_REGEXP"] = "Rozwiąż Podmiot Używając Wyrażenia";
$MESS["CRM_ENTITY_REGEXP_NOTES"] = "(wyrażenie regularne; pierwsza grupa w nawiasie kwadratowym musi zawierać ID elementu)";
$MESS["CRM_MAIL_ENTITY"] = "Jednostka:";
$MESS["CRM_MAIL_ENTITY_ALL"] = "nie wybrany";
$MESS["CRM_MAIL_ENTITY_COMPANY"] = "Firma";
$MESS["CRM_MAIL_ENTITY_CONTACT"] = "Kontakt";
$MESS["CRM_MAIL_ENTITY_DEAL"] = "Deal";
$MESS["CRM_MAIL_ENTITY_LEAD"] = "Lead";
$MESS["CRM_MAIL_MAIL"] = "Adres e-mail CRM";
?>