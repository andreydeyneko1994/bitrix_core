<?
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_EXCH1C_AUTH_ERROR"] = "Błąd uwierzytelniania: login i/lub hasło są nieprawidłowe.";
$MESS["CRM_EXCH1C_NOT_ENABLED"] = "Wymiana danych z 1C wyłączona";
$MESS["CRM_EXCH1C_PERMISSION_DENIED"] = "Odmowa dostępu.";
$MESS["CRM_EXCH1C_UNKNOWN_COMMAND_TYPE"] = "Nieznana komenda.";
$MESS["CRM_EXCH1C_UNKNOWN_XML_ID"] = "ID katalogu jest nieprawidłowe.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>