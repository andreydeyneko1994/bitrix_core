<?php
$MESS["CRM_BP_DOCUMENT_ITEM_ENTITY_TYPE_ERROR"] = "Смарт-процес заданого типу не знайдено";
$MESS["CRM_BP_DOCUMENT_ITEM_FIELD_CONTACTS"] = "Контакти";
$MESS["CRM_BP_DOCUMENT_ITEM_FIELD_IS_MANUAL_OPPORTUNITY"] = "Ручний розрахунок суми";
$MESS["CRM_ENTITY_EXISTENCE_ERROR"] = "Документ з ID: \"#DOCUMENT_ID#\" не існує";
