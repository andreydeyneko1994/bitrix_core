<?
$MESS["SBLP_ACC_POS_SUPPLI"] = "Cargo de trabajo contador";
$MESS["SBLP_ACC_POS_SUPPLI_DESC"] = "Cargo de trabajo contador (ventas)";
$MESS["SBLP_ACC_SIGN_SUPPLI"] = "Firma del Contable";
$MESS["SBLP_ACC_SIGN_SUPPLI_DESC"] = "Firma electrónica del contador (tamaño recomendado: 200x50)";
$MESS["SBLP_ACC_SUPPLI"] = "Nombre completo del contador";
$MESS["SBLP_ACC_SUPPLI_DESC"] = "Nombre completo del contador (ventas9";
$MESS["SBLP_ADRESS_SUPPLI"] = "Dirección del vendedor";
$MESS["SBLP_ADRESS_SUPPLI_DESC"] = "Dirección física del vendedor";
$MESS["SBLP_BACKGROUND"] = "Fondo";
$MESS["SBLP_BACKGROUND_DESC"] = "Imagen de fondo para las facturas (tamaño recomendado: 800x1120)";
$MESS["SBLP_BACKGROUND_STYLE"] = "Estilo de la imagen de fondo";
$MESS["SBLP_BACKGROUND_STYLE_NONE"] = "Ninguna";
$MESS["SBLP_BACKGROUND_STYLE_STRETCH"] = "Pantalla completa";
$MESS["SBLP_BACKGROUND_STYLE_TILE"] = "Mosaico";
$MESS["SBLP_BANK_SUPPLI"] = "Nombre del banco del vendedor";
$MESS["SBLP_BANK_SUPPLI_DESC"] = "Nombre del banco donde se encuentra la cuenta del vendedor";
$MESS["SBLP_BCITY_SUPPLI"] = "Ciudad del banco";
$MESS["SBLP_BCITY_SUPPLI_DESC"] = "Proveedor (vendedor) banco de la ciudad";
$MESS["SBLP_COMMENT1"] = "Comentario 1";
$MESS["SBLP_COMMENT2"] = "Comentario 2";
$MESS["SBLP_CUSTOMER"] = "Comprador";
$MESS["SBLP_CUSTOMER_ADRES"] = "Dirección del comprador";
$MESS["SBLP_CUSTOMER_ADRES_DESC"] = "Dirección del beneficiario";
$MESS["SBLP_CUSTOMER_DESC"] = "Nombre de la compañía pagadora / Pagador";
$MESS["SBLP_CUSTOMER_EDRPOU"] = "Cliente IDN";
$MESS["SBLP_CUSTOMER_EDRPOU_DESC"] = "Cliente IDN";
$MESS["SBLP_CUSTOMER_EMAIL"] = "E-mail del Comprador";
$MESS["SBLP_CUSTOMER_EMAIL_DESC"] = "Dirección de e-mail del Comprador";
$MESS["SBLP_CUSTOMER_FAX"] = "Fax del Comprador";
$MESS["SBLP_CUSTOMER_FAX_DESC"] = "Número de fax del Comprador";
$MESS["SBLP_CUSTOMER_PERSON"] = "Persona de contacto";
$MESS["SBLP_CUSTOMER_PERSON_DESC"] = "Persona de contacto para el Comprador";
$MESS["SBLP_CUSTOMER_PHONE"] = "Teléfono del comprador";
$MESS["SBLP_CUSTOMER_PHONE_DESC"] = "Número de teléfono del comprador";
$MESS["SBLP_DATE"] = "Fecha de la cotización";
$MESS["SBLP_DATE_DESC"] = "La fecha en que se creó la cotización";
$MESS["SBLP_DDESCR"] = "Cotización para imprimir. Se abrirá en una nueva ventana.";
$MESS["SBLP_DIR_POS_SUPPLI"] = "Cargo de trabajo de supervisor";
$MESS["SBLP_DIR_POS_SUPPLI_DESC"] = "Cargo de trabajo de supervisor (ventas)";
$MESS["SBLP_DIR_SIGN_SUPPLI"] = "Firma del Director";
$MESS["SBLP_DIR_SIGN_SUPPLI_DESC"] = "Firma electrónica del director (tamaño recomendado: 200x50)";
$MESS["SBLP_DIR_SUPPLI"] = "Nombre completo del supervisor";
$MESS["SBLP_DIR_SUPPLI_DESC"] = "Nombre completo del supervisor (ventas)";
$MESS["SBLP_DTITLE"] = "Cotización (Ucraniano)";
$MESS["SBLP_EDRPOU_SUPPLI"] = "Proveedor USREOU";
$MESS["SBLP_EDRPOU_SUPPLI_DESC"] = "Proveedor USREOU";
$MESS["SBLP_EMAIL_SUPPLI"] = "Dirección de correo electrónico del proveedor";
$MESS["SBLP_EMAIL_SUPPLI_DESC"] = "Dirección de correo electrónico del proveedor (vendedor)";
$MESS["SBLP_LOGO"] = "Logotipo del proveedor";
$MESS["SBLP_LOGO_DESC"] = "Registro del proveedor (tamaño recomendado: 80x80)";
$MESS["SBLP_LOGO_DPI"] = "Escala de logotipo";
$MESS["SBLP_LOGO_DPI_150"] = "64% (150 dpi)";
$MESS["SBLP_LOGO_DPI_300"] = "32% (300 dpi)";
$MESS["SBLP_LOGO_DPI_600"] = "16% (600 dpi)";
$MESS["SBLP_LOGO_DPI_72"] = "133% (72 dpi)";
$MESS["SBLP_LOGO_DPI_96"] = "100% (96 dpi)";
$MESS["SBLP_MARGIN_BOTTOM"] = "Margen inferior";
$MESS["SBLP_MARGIN_LEFT"] = "Margen izquierdo";
$MESS["SBLP_MARGIN_RIGHT"] = "Margen derecho";
$MESS["SBLP_MARGIN_TOP"] = "Margen superior";
$MESS["SBLP_MFO_SUPPLI"] = "BIC";
$MESS["SBLP_MFO_SUPPLI_DESC"] = "Banco de proveedores BIC";
$MESS["SBLP_ORDER_SUBJECT"] = "Asunto";
$MESS["SBLP_ORDER_SUPPLI"] = "Cuenta de proveedor nº.";
$MESS["SBLP_ORDER_SUPPLI_DESC"] = "Proveedor (vendedor) cuenta nº.";
$MESS["SBLP_PAY_BEFORE"] = "Pagado por";
$MESS["SBLP_PAY_BEFORE_DESC"] = "Cotización válida hasta";
$MESS["SBLP_PHONE_SUPPLI"] = "Teléfono del vendedor";
$MESS["SBLP_PHONE_SUPPLI_DESC"] = "Número de teléfono del vendedor";
$MESS["SBLP_PRINT"] = "Sello";
$MESS["SBLP_PRINT_DESC"] = "Imagen del sello StampSupplier (tamaño de pantalla recomendado: 150x150)";
$MESS["SBLP_Q_UA_COLUMN_DISCOUNT_TITLE"] = "Título de la columna \"Descuento\"";
$MESS["SBLP_Q_UA_COLUMN_MEASURE_TITLE"] = "Título de la columna \"Unidad de medida\"";
$MESS["SBLP_Q_UA_COLUMN_NAME_TITLE"] = "Título de la columna \"Nombre del producto\"";
$MESS["SBLP_Q_UA_COLUMN_NUMBER_TITLE"] = "\"#\" título de la columna";
$MESS["SBLP_Q_UA_COLUMN_PRICE_TITLE"] = "Título de la columna \"Precio\"";
$MESS["SBLP_Q_UA_COLUMN_QUANTITY_TITLE"] = "Título de la columna \"Cantidad\"";
$MESS["SBLP_Q_UA_COLUMN_SHOW"] = "Activo";
$MESS["SBLP_Q_UA_COLUMN_SORT"] = "Clasificar";
$MESS["SBLP_Q_UA_COLUMN_SUM_TITLE"] = "Título de la columna \"Importe\"";
$MESS["SBLP_Q_UA_COLUMN_VAT_RATE_TITLE"] = "Título de la columna \"Tasa de impuesto\"";
$MESS["SBLP_Q_UA_HEADER_SHOW"] = "Mostrar encabezado de la cotización";
$MESS["SBLP_Q_UA_SIGN_SHOW"] = "Mostrar firmas";
$MESS["SBLP_Q_UA_TOTAL_SHOW"] = "Mostrar resumen";
$MESS["SBLP_SUPPLI"] = "Vendedor";
$MESS["SBLP_SUPPLI_DESC"] = "Nombre del beneficiario o vendedor";
$MESS["SBLP_USERFIELD1"] = "Campo personalizado 1";
$MESS["SBLP_USERFIELD2"] = "Campo personalizado 2";
$MESS["SBLP_USERFIELD3"] = "Campo personalizado 3";
$MESS["SBLP_USERFIELD4"] = "Campo personalizado 4";
$MESS["SBLP_USERFIELD5"] = "Campo personalizado 5";
?>