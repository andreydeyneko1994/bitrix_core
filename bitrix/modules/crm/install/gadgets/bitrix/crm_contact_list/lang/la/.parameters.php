<?
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Creado";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modificado";
$MESS["GD_CRM_COLUMN_TYPE"] = "Tipo";
$MESS["GD_CRM_CONTACT_LIST_CONTACT_COUNT"] = "Contactos por Página";
$MESS["GD_CRM_ONLY_MY"] = "Sólo Míos";
$MESS["GD_CRM_SORT"] = "Clasificación";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_BY"] = "Pedido";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
?>