<?
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Creado";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modificado";
$MESS["GD_CRM_COLUMN_DEAL_STAGE"] = "Etapa";
$MESS["GD_CRM_DEAL_LIST_DEAL_COUNT"] = "Negociaciones por Página";
$MESS["GD_CRM_ONLY_MY"] = "Sólo Míos";
$MESS["GD_CRM_SORT"] = "Clasificación";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_BY"] = "Pedido";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
?>