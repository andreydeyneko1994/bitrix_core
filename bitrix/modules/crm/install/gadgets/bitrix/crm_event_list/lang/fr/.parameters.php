<?
$MESS["GD_CRM_EVENT_LIST_EVENT_COUNT"] = "Nombre d'évènements sur la page";
$MESS["GD_CRM_EVENT_TYPE"] = "Entités dans la liste";
$MESS["GD_CRM_EVENT_TYPE_COMPANY"] = "Entreprise";
$MESS["GD_CRM_EVENT_TYPE_CONTACT"] = "Client";
$MESS["GD_CRM_EVENT_TYPE_DEAL"] = "Transaction";
$MESS["GD_CRM_EVENT_TYPE_LEAD"] = "Prospect";
?>