<?
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Anuluj";
$MESS["CRM_BUTTON_CREATE_TITLE"] = "Utwórz";
$MESS["CRM_CURRENCY_IS_NOT_FOUND"] = "Nie można znaleźć ID waluty = #CURRENCY_ID#.";
$MESS["CRM_FIELD_ACTIVE"] = "Aktywny";
$MESS["CRM_FIELD_CURRENCY"] = "Waluta";
$MESS["CRM_FIELD_DESCRIPTION"] = "Opis";
$MESS["CRM_FIELD_NAME"] = "Nazwa";
$MESS["CRM_FIELD_PRICE"] = "Cena";
$MESS["CRM_FIELD_SECTION"] = "Sekcja";
$MESS["CRM_FIELD_SORT"] = "Sortowanie";
$MESS["CRM_FIELD_VAT_ID"] = "Stawka VAT";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "z VAT";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu.";
$MESS["CRM_PRODUCT_CREATE"] = "Utwórz nowy produkt";
$MESS["CRM_PRODUCT_CREATE_AJAX_ERR"] = "Błąd przetwarzania żądania nowego produktu<br>";
$MESS["CRM_PRODUCT_CREATE_WAIT"] = "Utwórz produkt…";
$MESS["CRM_UNSUPPORTED_OWNER_TYPE"] = "Nieobsługiwany typ właściciela: \"#OWNER_TYPE#\".";
?>