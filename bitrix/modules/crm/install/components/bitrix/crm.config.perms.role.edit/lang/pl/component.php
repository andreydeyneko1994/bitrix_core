<?
$MESS["CRM_ENTITY_TYPE_BUTTON"] = "Widżet na stronę";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Firma";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Deal";
$MESS["CRM_ENTITY_TYPE_EXCLUSION"] = "wyjątki";
$MESS["CRM_ENTITY_TYPE_INVOICE"] = "Faktura";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_ORDER"] = "Zamówienie";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Oferta";
$MESS["CRM_ENTITY_TYPE_SALETARGET"] = "Cel sprzedażowy";
$MESS["CRM_ENTITY_TYPE_WEBFORM"] = "Formularz CRM";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PERMS_ENTITY_LIST"] = "Uprawnienia dostępu";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Zarządzaj funkcjami";
$MESS["CRM_PERMS_TYPE_"] = "Niedozwolone";
$MESS["CRM_PERMS_TYPE_A"] = "Własne";
$MESS["CRM_PERMS_TYPE_AUTOMATION_ALL"] = "Aktualizuj";
$MESS["CRM_PERMS_TYPE_AUTOMATION_NONE"] = "Czytaj";
$MESS["CRM_PERMS_TYPE_D"] = "Własne i działu";
$MESS["CRM_PERMS_TYPE_F"] = "Własne, działu, pododdziału";
$MESS["CRM_PERMS_TYPE_O"] = "Otwarte";
$MESS["CRM_PERMS_TYPE_X"] = "Wszystkie";
?>