<?
$MESS["CRM_COLUMN_CITY_NAME"] = "Miasto";
$MESS["CRM_COLUMN_COUNTRY_NAME"] = "Kraj";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_REGION_NAME"] = "Region";
$MESS["CRM_COLUMN_SORT"] = "Sortowanie";
$MESS["CRM_INTS_TASKS_NAV"] = "Wyniki";
$MESS["CRM_LOC_DELETION_GENERAL_ERROR"] = "Błąd usuwania lokalizacji.";
$MESS["CRM_LOC_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji lokalizacji.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>