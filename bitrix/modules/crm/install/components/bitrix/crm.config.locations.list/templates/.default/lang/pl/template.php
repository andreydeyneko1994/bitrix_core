<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_LOC_DELETE"] = "Usuń lokalizację";
$MESS["CRM_LOC_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_LOC_DELETE_TITLE"] = "Usuń tę lokalizację";
$MESS["CRM_LOC_EDIT"] = "Edytuj lokalizację";
$MESS["CRM_LOC_EDIT_TITLE"] = "Otwórz tę lokalizację do edycji";
?>