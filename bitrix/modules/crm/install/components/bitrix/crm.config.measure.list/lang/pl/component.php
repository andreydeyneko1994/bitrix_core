<?
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_COLUMN_CODE"] = "Kod";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_IS_DEFAULT"] = "domyślne";
$MESS["CRM_COLUMN_MEASURE_TITLE"] = "Nazwa jednostki";
$MESS["CRM_COLUMN_SYMBOL_INTL"] = "Symbol jednostki (międzynarodowy)";
$MESS["CRM_COLUMN_SYMBOL_LETTER_INTL"] = "Nazwa kodu (zagr.)";
$MESS["CRM_COLUMN_SYMBOL_RUS"] = "Symbol jednostki";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>