<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_MEASURE_DELETE"] = "Usuń jednostkę";
$MESS["CRM_MEASURE_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '#MEASURE_TITLE#'?";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Usuń tę jednostkę miary";
$MESS["CRM_MEASURE_EDIT"] = "Edytuj jednostkę";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Edytuj parametry dla tej jednostki miary";
$MESS["CRM_MEASURE_SHOW"] = "Wyświetl jednostkę";
$MESS["CRM_MEASURE_SHOW_TITLE"] = "Wyświetl szczegóły dotyczące tej jednostki miary";
?>