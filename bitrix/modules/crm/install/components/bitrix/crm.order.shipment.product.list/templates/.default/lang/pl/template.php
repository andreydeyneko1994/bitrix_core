<?
$MESS["CRM_ORDER_SPLT_ADD_PRODUCT"] = "Dodaj produkt";
$MESS["CRM_ORDER_SPLT_ADD_STORE"] = "Dodaj magazyn";
$MESS["CRM_ORDER_SPLT_ARE_YOU_SURE_YOU_WANT"] = "Na pewno usunąć ten element?";
$MESS["CRM_ORDER_SPLT_BARCODE"] = "Kod kreskowy";
$MESS["CRM_ORDER_SPLT_BARCODES"] = "Kody kreskowe";
$MESS["CRM_ORDER_SPLT_CHOOSE"] = "Wybierz";
$MESS["CRM_ORDER_SPLT_CHOOSE_STORE"] = "Wybierz magazyn";
$MESS["CRM_ORDER_SPLT_CLOSE"] = "Zamknij";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM"] = "Usuń element";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM_FROM_SHIPMENT"] = "Usuń pozycję z wysyłki";
$MESS["CRM_ORDER_SPLT_DELETE_STORE"] = "Usuń wysyłkę z magazynu";
$MESS["CRM_ORDER_SPLT_FIND_BY_BARCODE"] = "Znajdź według kodu kreskowego";
$MESS["CRM_ORDER_SPLT_ITEMS_HAVE_BARCODES"] = "Elementy zestawu mają kody kreskowe";
$MESS["CRM_ORDER_SPLT_MARKING_CODE"] = "Kod oznakowania";
$MESS["CRM_ORDER_SPLT_TO_REMOVE"] = "Usuń";
?>