<?
$MESS["CRM_ORDER_SPLT_ADD_PRODUCT"] = "Adicionar produto";
$MESS["CRM_ORDER_SPLT_ADD_STORE"] = "Adicionar armazém";
$MESS["CRM_ORDER_SPLT_ARE_YOU_SURE_YOU_WANT"] = "Você tem certeza que deseja excluir este item?";
$MESS["CRM_ORDER_SPLT_BARCODE"] = "Código de barras";
$MESS["CRM_ORDER_SPLT_BARCODES"] = "Códigos de barras";
$MESS["CRM_ORDER_SPLT_CHOOSE"] = "Selecionar";
$MESS["CRM_ORDER_SPLT_CHOOSE_STORE"] = "Selecionar armazém";
$MESS["CRM_ORDER_SPLT_CLOSE"] = "Fechar";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM"] = "Remover item";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM_FROM_SHIPMENT"] = "Remover item do envio";
$MESS["CRM_ORDER_SPLT_DELETE_STORE"] = "Remover envio do armazém";
$MESS["CRM_ORDER_SPLT_FIND_BY_BARCODE"] = "Localizar por código de barras";
$MESS["CRM_ORDER_SPLT_ITEMS_HAVE_BARCODES"] = "Os itens do pacote possuem códigos de barras";
$MESS["CRM_ORDER_SPLT_MARKING_CODE"] = "Código de marcação";
$MESS["CRM_ORDER_SPLT_TO_REMOVE"] = "Remover";
?>