<?
$MESS["CRM_CANCEL_BUTTON"] = "Anuluj";
$MESS["CRM_CLOSE_BUTTON"] = "Zamknij";
$MESS["CRM_IMPORT_BUTTON"] = "Importuj";
$MESS["CRM_LOC_IMP_CHOOSE_FILE"] = "Wybierz plik definiujący lokalizację";
$MESS["CRM_LOC_IMP_FILE_CNTR"] = "Świat (kraje)";
$MESS["CRM_LOC_IMP_FILE_FFILE"] = "Dodaj plik";
$MESS["CRM_LOC_IMP_FILE_NONE"] = "brak";
$MESS["CRM_LOC_IMP_FILE_RS"] = "Rosja i ex-ZSRR (miasta)";
$MESS["CRM_LOC_IMP_FILE_USA"] = "USA (miasta)";
$MESS["CRM_LOC_IMP_JS_ERROR"] = "Błąd";
$MESS["CRM_LOC_IMP_JS_FILE_PROCESS"] = "Ładowanie pliku...";
$MESS["CRM_LOC_IMP_JS_IMPORT_PROCESS"] = "Importowanie danych...";
$MESS["CRM_LOC_IMP_JS_IMPORT_SUCESS"] = "Import danych zakończył się powodzeniem.";
$MESS["CRM_LOC_IMP_LOAD_ZIP"] = "załaduj bazę danych kodów pocztowych";
$MESS["CRM_LOC_IMP_STEP_CHECK"] = "Długość kroku nie może być zerowa lub ujemna.";
$MESS["CRM_LOC_IMP_STEP_LENGTH"] = "Długość kroku (sek)";
$MESS["CRM_LOC_IMP_STEP_LENGTH_HINT"] = "Określ wymaganą długość kroku, w sekundach. Nie zmieniaj tego parametru, jeżeli nie jesteś absolutnie pewny, że wartość którą masz zamiar wprowadzić jest wystarczająca i skrypt nie zakończy się przedwcześnie.";
$MESS["CRM_LOC_IMP_SYNC"] = "Synchronizacja";
$MESS["CRM_LOC_IMP_SYNC_N"] = "Usuń stare dane";
$MESS["CRM_LOC_IMP_SYNC_NO_ZIP"] = "Tylko Rosyjskie kody POCZTOWE zostaną zsynchronizowane.";
$MESS["CRM_LOC_IMP_SYNC_Y"] = "spróbuj i zsynchronizuj istniejące i nowe dane";
$MESS["CRM_LOC_IMP_TITLE"] = "Import Lokalizacji";
?>