<?
$MESS["CRM_PRODUCT_FILE_CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PRODUCT_FILE_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informações não está instalado.";
$MESS["CRM_PRODUCT_FILE_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PRODUCT_FILE_UNKNOWN_ERROR"] = "Erro desconhecido.";
$MESS["CRM_PRODUCT_FILE_WRONG_FILE"] = "Arquivo não encontrado.";
?>