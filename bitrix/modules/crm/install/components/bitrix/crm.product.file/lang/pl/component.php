<?
$MESS["CRM_PRODUCT_FILE_CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PRODUCT_FILE_IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["CRM_PRODUCT_FILE_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRODUCT_FILE_UNKNOWN_ERROR"] = "Nieznany błąd.";
$MESS["CRM_PRODUCT_FILE_WRONG_FILE"] = "Nie znaleziono pliku.";
?>