<?
$MESS["CRM_PRODUCT_FILE_CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PRODUCT_FILE_IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Block de información no está instalado.";
$MESS["CRM_PRODUCT_FILE_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRODUCT_FILE_UNKNOWN_ERROR"] = "Error desconocido.";
$MESS["CRM_PRODUCT_FILE_WRONG_FILE"] = "Archivo no encontrado.";
?>