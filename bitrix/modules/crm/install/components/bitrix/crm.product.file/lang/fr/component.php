<?
$MESS["CRM_PRODUCT_FILE_CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PRODUCT_FILE_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module Blocs d'Information n'est pas installé.";
$MESS["CRM_PRODUCT_FILE_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_PRODUCT_FILE_UNKNOWN_ERROR"] = "Erreur inconnue.";
$MESS["CRM_PRODUCT_FILE_WRONG_FILE"] = "Fichier introuvable";
?>