<?php
$MESS["DEAL_ORDER_SCENARIO"] = "Modo de manejo de los pedidos";
$MESS["ORDER_ADD"] = "Nuevo pedido";
$MESS["ORDER_ADD_TITLE"] = "Nuevo pedido";
$MESS["ORDER_CANCEL"] = "Cancelar el pedido";
$MESS["ORDER_CANCEL_TITLE"] = "Cancelar el pedido";
$MESS["ORDER_COPY"] = "Copiar";
$MESS["ORDER_COPY_TITLE"] = "Copiar el pedido";
$MESS["ORDER_DELETE"] = "Eliminar pedido";
$MESS["ORDER_DELETE_TITLE"] = "Eliminar pedido";
$MESS["ORDER_DOCUMENT_BUTTON_TEXT"] = "Documento";
$MESS["ORDER_DOCUMENT_BUTTON_TITLE"] = "Crear un documento en el pedido";
$MESS["ORDER_EDIT_ORDER_FORM"] = "Configurar formulario de pago";
$MESS["ORDER_EDIT_ORDER_FORM_TITLE"] = "Configurar Formulario de Pago";
$MESS["ORDER_EXPORT_CSV"] = "Exportar pedidos a CSV";
$MESS["ORDER_EXPORT_CSV_TITLE"] = "Exportar pedidos a CSV";
$MESS["ORDER_EXPORT_EXCEL"] = "Exportar pedidos a Microsoft Excel";
$MESS["ORDER_EXPORT_EXCEL_TITLE"] = "Exportar pedidos a Microsoft Excel";
$MESS["ORDER_RESTORE"] = "Recuperar pedido";
$MESS["ORDER_RESTORE_TITLE"] = "Recuperar pedido";
$MESS["ORDER_STEXPORT_OPTION_EXPORT_ALL_FIELDS"] = "Exportar todos los campos del pedido";
$MESS["ORDER_STEXPORT_SUMMARY"] = "Esto creará un archivo de exportación de pedidos. Exportar una cantidad considerable de datos puede tomar algún tiempo.";
