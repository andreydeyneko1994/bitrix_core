<?
$MESS["CRM_ACCOUNT_NUMBER_DATE"] = "Okres:";
$MESS["CRM_ACCOUNT_NUMBER_DATE_1"] = "Codziennie";
$MESS["CRM_ACCOUNT_NUMBER_DATE_2"] = "Co miesiąc";
$MESS["CRM_ACCOUNT_NUMBER_DATE_3"] = "Co rok";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER"] = "Początkowy numer: ";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_DESC"] = "1 do 7 znaków. Nowa wartość musi być większa niż poprzednia.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX"] = "Prefiks:";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_DESC"] = "1 do 7 znaków (Litery łacińskie, cyfry, myślniki, podkreślenia). Na przykład: TEST_1234";
$MESS["CRM_ACCOUNT_NUMBER_RANDOM"] = "Liczba znaków:";
$MESS["CRM_ACCOUNT_NUMBER_TEMPL"] = "Szablon:";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_EXAMPLE"] = "Przykład: ";
$MESS["CRM_ACCOUNT_NUMBER_WARNING"] = "Prefiks:";
?>