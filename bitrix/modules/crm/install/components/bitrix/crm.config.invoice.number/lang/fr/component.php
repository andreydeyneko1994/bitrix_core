<?
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_0"] = "Non utilisé";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_1"] = "Numérotation à partir d'un nombre bien déterminé";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_2"] = "Préfixe devant le numéro";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_3"] = "Numéro unique aléatoire";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_4"] = "Identificateur et numéro de la commande d'utilisateur";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_5"] = "Numération au cours d'une période définie";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit.";
?>