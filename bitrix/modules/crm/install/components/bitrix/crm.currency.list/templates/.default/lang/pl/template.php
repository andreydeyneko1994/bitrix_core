<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_CURRENCY_DELETE"] = "Usuń walutę";
$MESS["CRM_CURRENCY_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Usuń tę walutę";
$MESS["CRM_CURRENCY_EDIT"] = "Edytuj walutę";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Edytuj tę walutę";
$MESS["CRM_CURRENCY_SET_AS_BASE"] = "Stwórz walutę podstawową";
$MESS["CRM_CURRENCY_SET_AS_BASE_TITLE"] = "Stwórz walutę podstawową";
$MESS["CRM_CURRENCY_SHOW"] = "Wyświetl walutę";
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Wyświetl tę walutę";
?>