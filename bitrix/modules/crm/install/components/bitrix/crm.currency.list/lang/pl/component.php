<?
$MESS["CRM_COLUMN_ACCOUNTING"] = "Waluta raportowania";
$MESS["CRM_COLUMN_AMOUNT_CNT"] = "Wartość nominalna";
$MESS["CRM_COLUMN_CURRENCY_LIST_BASE"] = "Waluta podstawowa";
$MESS["CRM_COLUMN_EXCH_RATE"] = "Kurs wymiany";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_INVOICE_DEF"] = "Domyślna waluta fakturowania";
$MESS["CRM_COLUMN_NAME"] = "Nazwa";
$MESS["CRM_COLUMN_SORT"] = "Sortuj";
$MESS["CRM_CURRENCY_DELETION_GENERAL_ERROR"] = "Błąd usuwania waluty.";
$MESS["CRM_CURRENCY_MARK_AS_BASE_GENERAL_ERROR"] = "Błąd ustawienia waluty podstawowej";
$MESS["CRM_CURRENCY_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji waluty.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>