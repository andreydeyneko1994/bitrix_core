<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Aktywny";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Sortowanie";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_VAT_FIELD_NAME"] = "Nazwa";
$MESS["CRM_VAT_FIELD_RATE"] = "Stawka";
$MESS["CRM_VAT_NO"] = "Nie";
$MESS["CRM_VAT_NOT_FOUND"] = "Nie znaleziono stawki VAT.";
$MESS["CRM_VAT_SECTION_MAIN"] = "Stawka VAT";
$MESS["CRM_VAT_YES"] = "Tak";
?>