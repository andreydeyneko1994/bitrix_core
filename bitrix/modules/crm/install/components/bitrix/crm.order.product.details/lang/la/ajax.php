<?
$MESS["CRM_ORDER_NOT_FOUND"] = "El pedido no fue encontrada";
$MESS["CRM_PRODUCT_CURRENCY_EMPTY"] = "No se especifica la moneda del producto.";
$MESS["CRM_PRODUCT_NAME_EMPTY"] = "Falta el nombre del producto";
$MESS["CRM_PRODUCT_PRICE_EMPTY"] = "Se requiere precio";
$MESS["CRM_PRODUCT_QUANTITY_EMPTY"] = "Falta la cantidad del producto";
?>