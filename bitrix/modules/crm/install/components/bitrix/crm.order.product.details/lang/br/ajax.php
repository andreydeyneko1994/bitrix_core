<?
$MESS["CRM_ORDER_NOT_FOUND"] = "O pedido não foi encontrado";
$MESS["CRM_PRODUCT_CURRENCY_EMPTY"] = "A moeda do produto não está especificada";
$MESS["CRM_PRODUCT_NAME_EMPTY"] = "Está faltando o nome do produto";
$MESS["CRM_PRODUCT_PRICE_EMPTY"] = "O preço é obrigatório";
$MESS["CRM_PRODUCT_QUANTITY_EMPTY"] = "Está faltando a quantidade do produto";
?>