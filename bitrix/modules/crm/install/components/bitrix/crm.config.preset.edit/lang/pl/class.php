<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRESET_EDIT_SELECT_FIELDS_NONE"] = "(Nie wybrano)";
$MESS["CRM_PRESET_EDIT_SELECT_OTHER_FIELDS"] = "(Pola nielinkowane)";
$MESS["CRM_PRESET_EDIT_TITLE"] = "Pola dla szablonu: #NAME# (#ENTITY_TYPE_NAME#)";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Niewłaściwy typ szablonu";
$MESS["CRM_PRESET_FIELD_FIELD_ETITLE"] = "Nazwa";
$MESS["CRM_PRESET_FIELD_FIELD_NAME"] = "Nazwa";
$MESS["CRM_PRESET_FIELD_FIELD_TITLE"] = "Nazwa w szablonie";
$MESS["CRM_PRESET_FIELD_ID"] = "ID";
$MESS["CRM_PRESET_FIELD_IN_SHORT_LIST"] = "Pokaż w podsumowaniu";
$MESS["CRM_PRESET_FIELD_SORT"] = "Sortowanie";
$MESS["CRM_PRESET_NOT_FOUND"] = "Szablon nie został znaleziony";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Szablon nienazwany";
?>