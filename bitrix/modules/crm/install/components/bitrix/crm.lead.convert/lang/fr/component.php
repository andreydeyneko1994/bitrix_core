<?
$MESS["CRM_COMPANY_ERROR"] = "Il faut choisir une entreprise.";
$MESS["CRM_CONTACT_ERROR"] = "Il faut choisir un contact.";
$MESS["CRM_DEAL_ERROR"] = "Il faut remplir des champs sur la transaction.";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "Conversion d'Entreprise";
$MESS["CRM_FIELD_LEAD_CONTACT"] = "Conversion du Contact";
$MESS["CRM_FIELD_LEAD_DEAL"] = "Conversion de la transaction";
$MESS["CRM_FIELD_TITLE"] = "Nom du prospect";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "Conversion du prospect : #NAME#";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "Prospects";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["UNKNOWN_ERROR"] = "Erreur inconnue.";
?>