<?
$MESS["CRM_COMPANY_ERROR"] = "Proszę wybrać firmę.";
$MESS["CRM_CONTACT_ERROR"] = "Proszę wybrać kontakt.";
$MESS["CRM_DEAL_ERROR"] = "Proszę wypełnić pola deala.";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "Konwertuj Firmę";
$MESS["CRM_FIELD_LEAD_CONTACT"] = "Konwertuj Kontakt";
$MESS["CRM_FIELD_LEAD_DEAL"] = "Konwertuj deala";
$MESS["CRM_FIELD_TITLE"] = "Nazwa leada";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "Konwertuj leada: #NAZWA#";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "Leady";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["UNKNOWN_ERROR"] = "Nieznany błąd.";
?>