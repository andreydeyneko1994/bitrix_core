<?
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_COMPLETED_SUMMARY"] = "Atrybuty dostępu zostały zaktualizowane. Elementy przetworzone: #PROCESSED_ITEMS#.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_NOT_REQUIRED_SUMMARY"] = "Atrybuty dostępu nie wymagają aktualizacji.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_PROGRESS_SUMMARY"] = "Przetworzonych elementów: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
?>