<?
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_CLOSE"] = "Zamknij";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_START"] = "Wykonaj";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_STOP"] = "zatrzymaj";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_REQUEST_ERR"] = "Błąd przetwarzania żądania.";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_WAIT"] = "Proszę czekać…";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTRS"] = "Proszę <a id=\"#ID#\" href=\"#URL#\">aktualizuj</a> atrybuty dostępu dla istniejących firm przed użyciem zaawansowanych uprawnień dostępu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu firmy są obecnie odtwarzane. Ta operacja może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_TITLE"] = "Aktualizuj atrybuty dostępu firmy";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTRS"] = "Zaktualizowane prawa dostępu do kontaktu wymagają <a id=\"#ID#\" href=\"#URL#\">aktualizacji</a> atrybutów praw dostępu dla poprzednio utworzonych kontaktów.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu zostaną teraz przebudowane dla wszystkich kontaktów. Ta procedura może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_TITLE"] = "Aktualizuj Atrybuty Dostępu Kontaktu";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTRS"] = "Zaktualizowane uprawnienia dostępu do deali wymagają <a id=\"#ID#\" href=\"#URL#\">aktualizacji</a> atrybutów uprawnień dostępu dla poprzednio utworzonych deali.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu zostaną teraz przebudowane dla wszystkich deali. Ta procedura może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_TITLE"] = "Aktualizuj atrybuty dostępu deala";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTRS"] = "Zaktualizowane prawa dostępu do faktury wymagają <a id=\"#ID#\" href=\"#URL#\">aktualizacji</a> atrybutów praw dostępu dla poprzednio utworzonych faktur.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu zostaną przebudowane dla wszystkich faktur. Ta procedura może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_TITLE"] = "Aktualizuj Atrybuty Dostępu Faktury";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTRS"] = "Zaktualizowane prawa dostępu do leada wymagają <a id=\"#ID#\" href=\"#URL#\">aktualizacji</a> atrybutów praw dostępu dla poprzednio utworzonych leadów.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu zostaną teraz przebudowane dla wszystkich leadów. Ta procedura może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_TITLE"] = "Aktualizuj atrybuty dostępu leada";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTRS"] = "Zaktualizowane prawa dostępu do oferty wymagają <a id=\"#ID#\" href=\"#URL#\">aktualizacji</a> atrybutów praw dostępu dla poprzednio utworzonych ofert.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_SUMMARY"] = "Atrybuty dostępu zostaną przebudowane dla wszystkich ofert. Ta procedura może zająć trochę czasu.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_TITLE"] = "Aktualizuj Atrybuty Dostępu Oferty";
?>