<?
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Dados estatísticos de processamento realizados para atividades. Atividades processadas: #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Nenhuma estatística de atividades precisa ser atualizada.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Atividades processadas: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "O índice de pesquisa de atividades foi recriado. Atividades processadas: #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "O índice de pesquisa de atividades não precisa ser recriado.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Atividades processadas: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
?>