<?
$MESS["CRM_ACTIVITY_CURRENT"] = "Aktywność";
$MESS["CRM_ACTIVITY_DLG_BTN_CANCEL"] = "Wyjdź bez Zapisywania";
$MESS["CRM_ACTIVITY_EXECUTE"] = "Wykonaj";
$MESS["CRM_ACTIVITY_HISTORY"] = "Dziennik";
$MESS["CRM_ACTIVITY_LIST_ADD_CALL"] = "Nowe połączenie telefoniczne";
$MESS["CRM_ACTIVITY_LIST_ADD_EMAIL"] = "Wyślij wiadomość";
$MESS["CRM_ACTIVITY_LIST_ADD_MEETING"] = "Nowe spotkanie";
$MESS["CRM_ACTIVITY_LIST_ADD_TASK"] = "Nowe zadanie";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_DEAD_LINE"] = "Termin ostateczny";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_PRIORITY"] = "Priorytet";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_SUBJECT"] = "Nazwa";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_TYPE"] = "Rodzaj";
$MESS["CRM_ACTIVITY_SHOW_ALL"] = "Pokaż wszystko (#COUNT#)";
$MESS["CRM_CALL_STATUS_COMPLETED"] = "Zakończone";
$MESS["CRM_CALL_STATUS_WAITING"] = "W toku";
$MESS["CRM_FF_CANCEL"] = "Anuluj";
$MESS["CRM_FF_CHANGE"] = "Edytuj";
$MESS["CRM_FF_CLOSE"] = "Zamknij";
$MESS["CRM_FF_LAST"] = "Ostatnie";
$MESS["CRM_FF_NO_RESULT"] = "Niestety wyszukiwanie twojego zapytania nie przyniosło rezultatów.";
$MESS["CRM_FF_OK"] = "Wybierz";
$MESS["CRM_FF_SEARCH"] = "Szukaj";
$MESS["CRM_MEETING_STATUS_COMPLETED"] = "Zakończone";
$MESS["CRM_MEETING_STATUS_WAITING"] = "W toku";
$MESS["CRM_UNDEFINED_VALUE"] = "[nie ustawiony]";
?>