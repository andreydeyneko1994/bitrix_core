<?
$MESS["CRM_ACTION_COMPLETED"] = "Zakończone";
$MESS["CRM_ACTION_CUSTOMER"] = "Klient";
$MESS["CRM_ACTION_END_TIME"] = "Termin ostateczny";
$MESS["CRM_ACTION_EXPIRED"] = "po terminie";
$MESS["CRM_ACTION_GO_TO_FULL_VIEW"] = "Wyświetl pełną listę";
$MESS["CRM_ACTION_IMPORTANT"] = "Ważne rzeczy";
$MESS["CRM_ACTION_REFERENCE_DEAL"] = "Deal";
$MESS["CRM_ACTION_REFERENCE_LEAD"] = "Lead";
$MESS["CRM_ACTION_TYPE_CALL_INCOMING"] = "Połączenie telefoniczne od klienta";
$MESS["CRM_ACTION_TYPE_CALL_OUTGOING"] = "Połączenie telefoniczne do klienta";
$MESS["CRM_ACTION_TYPE_EMAIL_INCOMING"] = "Przysłany e-mail";
$MESS["CRM_ACTION_TYPE_EMAIL_OUTGOING"] = "Wysłany e-mail";
$MESS["CRM_ACTION_TYPE_MEETING"] = "Spotkanie z klientem";
$MESS["CRM_ACTION_TYPE_TASK"] = "Zadanie";
?>