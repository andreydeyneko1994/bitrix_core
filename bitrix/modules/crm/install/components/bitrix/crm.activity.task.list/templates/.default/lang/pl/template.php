<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_TASK_DELETE"] = "Usuń Zadanie";
$MESS["CRM_TASK_DELETE_CONFIRM"] = "Na pewno chcesz to usunąć?";
$MESS["CRM_TASK_DELETE_TITLE"] = "Usuń Zadanie";
$MESS["CRM_TASK_EDIT"] = "Edytuj zadanie";
$MESS["CRM_TASK_EDIT_TITLE"] = "Edytuj zadanie";
$MESS["CRM_TASK_SHOW"] = "Wyświetl Zadanie";
$MESS["CRM_TASK_SHOW_TITLE"] = "Wyświetl Zadanie";
?>