<?php
$MESS["CRM_STORE_DOCUMENT_SD_CART_NOT_FOUND"] = "Nie znaleziono koszyka zamówienia";
$MESS["CRM_STORE_DOCUMENT_SD_FORM_DATA_MISSING"] = "Brak niektórych danych formularza";
$MESS["CRM_STORE_DOCUMENT_SD_INSUFFICIENT_RIGHTS"] = "Niewystarczające uprawnienia.";
$MESS["CRM_STORE_DOCUMENT_SD_ORDER_ID_NEGATIVE"] = "Aby utworzyć nowe zamówienie, wymagany jest prawidłowy identyfikator zamówienia lub dane wejściowe";
$MESS["CRM_STORE_DOCUMENT_SD_PRODUCT_NOT_FOUND"] = "Wprowadź co najmniej jeden produkt";
$MESS["CRM_STORE_DOCUMENT_SD_SHIPMENT_NOT_FOUND"] = "Nie znaleziono wysyłki";
