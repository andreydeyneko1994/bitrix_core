<?php
$MESS["CRM_STORE_DOCUMENT_SD_CART_NOT_FOUND"] = "No se encontró el carrito de compras";
$MESS["CRM_STORE_DOCUMENT_SD_FORM_DATA_MISSING"] = "Faltan algunos datos del formulario";
$MESS["CRM_STORE_DOCUMENT_SD_INSUFFICIENT_RIGHTS"] = "Permisos insuficientes.";
$MESS["CRM_STORE_DOCUMENT_SD_ORDER_ID_NEGATIVE"] = "Se requiere el ID de una orden válida o datos de entrada para crear una nueva orden";
$MESS["CRM_STORE_DOCUMENT_SD_PRODUCT_NOT_FOUND"] = "Introduzca por lo menos un producto";
$MESS["CRM_STORE_DOCUMENT_SD_SHIPMENT_NOT_FOUND"] = "No se encontró el envío";
