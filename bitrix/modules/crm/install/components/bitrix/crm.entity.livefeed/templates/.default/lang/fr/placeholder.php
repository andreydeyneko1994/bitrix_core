<?
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "Flux d'activités CRM";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "Ce flux affiche tout ce qui se passe dans votre CRM : appels récents ; mises à jour de transaction, prospect et facture. Une liste affiche à droite les tâches urgentes.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "Vous pouvez envoyer un e-mail directement à votre client depuis le flux CRM. Sa réponse sera affichée dans le flux dès qu'elle sera disponible.";
?>