<?php
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "Feed de CRM";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "Este feed mostra tudo o que está acontecendo no seu CRM: chamadas recentes; atualizações de vendas, clientes potenciais e faturas. Uma lista à direita exibe tarefas urgentes.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "Você pode enviar um e-mail para o seu cliente diretamente do feed do CRM. Você verá a resposta dele no feed, assim que ela estiver disponível.";
