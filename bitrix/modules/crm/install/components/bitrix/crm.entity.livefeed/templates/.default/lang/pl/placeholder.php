<?php
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "Aktualności CRM";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "Ten kanał pokazuje wszystko, co się dzieje w twoim CRM: ostatnie połączenia; aktualizacje umów, leadów i faktur. Lista po prawej wyświetla pilne zadania.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "Możesz wysłać wiadomość e-mail do klienta bezpośrednio z kanału CRM. Ich odpowiedź pojawi się w pliku danych, gdy tylko będzie dostępna.";
