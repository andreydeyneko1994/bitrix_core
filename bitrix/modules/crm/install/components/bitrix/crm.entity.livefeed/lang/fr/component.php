<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_ID_NOT_DEFINED"] = "Identifiant de l'entité CRM non renseigné.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_TYPE_NOT_DEFINED"] = "Type d'entité CRM non renseigné.";
$MESS["CRM_SL_EVENT_EDIT_HIDDEN_GROUP"] = "Groupe caché";
$MESS["CRM_SL_EVENT_NOT_AVAIBLE"] = "Les messages sont inaccessibles pour cet utilisateur.";
$MESS["SONET_MODULE_NOT_INSTALLED"] = "Le module Réseau social n'est pas installé.";
?>