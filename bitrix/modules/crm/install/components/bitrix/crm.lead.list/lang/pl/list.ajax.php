<?
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_AUTOCREATION_DISABLED"] = "Konwersja grupy wymaga włączenia opcji \"Automatyczne tworzenie kontaktów, firm, deali, faktur i ofert\" na stronie ustawień CRM (Ustawienia > Inne ustawienia > Ogólne > Ustawienia auto-generowania)";
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_CONFIG_IS_NOT_SUPPORTED"] = "Przy wybranych ustawieniach można konwertować leadów.";
$MESS["CRM_LEAD_LIST_DELETION_ACCESS_ERROR"] = "Operacja usuwania została przerwana z powodu niewystarczających uprawnień.";
$MESS["CRM_LEAD_LIST_DELETION_COMPLETED_SUMMARY"] = "Leady zostały usunięte. Przetworzone leady: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_NOT_FOUND_ERROR"] = "Operacja usuwania została przerwana, ponieważ nie znaleziono preferencji filtra. Odśwież stronę i spróbuj ponownie.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_OUTDATED_ERROR"] = "Operacja usuwania została przerwana, ponieważ preferencje filtra są nieaktualne. Odśwież stronę i spróbuj ponownie.";
$MESS["CRM_LEAD_LIST_DELETION_PARAM_ERROR"] = "Operacja usuwania została przerwana z powodu nieprawidłowych parametrów.";
$MESS["CRM_LEAD_LIST_DELETION_PROGRESS_SUMMARY"] = "Leady usunięte: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_COMPLETED_SUMMARY"] = "Duplikaty leadu zostały ponownie zindeksowane. Przetworzone firmy: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_NOT_REQUIRED_SUMMARY"] = "Indeks duplikatu leadu nie wymaga przetwarzania.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_PROGRESS_SUMMARY"] = "Leady przetworzone: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "Indeks wyszukiwania leadów został odtworzony. Przetworzone leady: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "Indeks wyszukiwania leadów nie wymaga odtworzenia.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Leady przetworzone: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Zakończono aktualizowanie pól obsługi leada. Przetworzone leady: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Pola obsługi leadu nie wymagają aktualizacji.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Leady przetworzone: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Ukończono przetwarzanie danych statystycznych dla leadów. Przetworzone firmy: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Dane statystyczne dla leadów są aktualne.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Leady przetworzone: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_ROW_COUNT"] = "Suma: #ROW_COUNT#";
?>