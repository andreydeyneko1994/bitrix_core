<?
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_AUTOCREATION_DISABLED"] = "La conversion de groupe nécessite l'activation de l'option \"créer automatiquement des contacts, sociétés, transactions, factures et devis\" dans les paramètres du CRM (Paramètres > Autres paramètres > Général > Paramètres de la génération automatique)";
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_CONFIG_IS_NOT_SUPPORTED"] = "Les prospects ne peuvent pas être convertis avec les paramètres sélectionnés.";
$MESS["CRM_LEAD_LIST_DELETION_ACCESS_ERROR"] = "L'opération de suppression a été abandonnée pour cause d'autorisations insuffisantes.";
$MESS["CRM_LEAD_LIST_DELETION_COMPLETED_SUMMARY"] = "Les prospects ont été supprimés. Prospects traités : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_NOT_FOUND_ERROR"] = "L'opération de suppression a été abandonnée pour cause de préférences de filtre introuvables. Veuillez actualiser la page et réessayer.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_OUTDATED_ERROR"] = "L'opération de suppression a été abandonnée pour cause de préférences de filtre dépassées. Veuillez actualiser la page et réessayer.";
$MESS["CRM_LEAD_LIST_DELETION_PARAM_ERROR"] = "L'opération de suppression a été abandonnée pour cause de paramètres non valides.";
$MESS["CRM_LEAD_LIST_DELETION_PROGRESS_SUMMARY"] = "Prospects supprimés : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_COMPLETED_SUMMARY"] = "La réorganisation de l'index des doubles pour les prospects est terminée. Prospects traités : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_NOT_REQUIRED_SUMMARY"] = "La restructuration de l'index des doubles des prospects n'est pas requise.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_PROGRESS_SUMMARY"] = "Prospects transformés : #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "L'index de recherche des prospects a été recréé. Prospects traités : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "L'index de recherche des prospects n'a pas besoin d'être recréé.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Prospects traités : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Mise à jour des champs du service de prospects terminée. Prospects traités : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Les champs du service de prospects ne nécessitent pas de mise à jour.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Prospects traités : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Le traitement des données statistiques des prospects est terminé. Prospects traitées : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Les données statistiques des prospects sont à jour.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Prospects traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_ROW_COUNT"] = "Total : #ROW_COUNT#";
?>