<?php
$MESS["CMR_CONFIG_CHECKER_TITLE"] = "Configurez les fonctionnalités dont vous avez besoin";
$MESS["CRM_BUTTON_APPLY"] = "Appliquer";
$MESS["CRM_BUTTON_CHECK"] = "Appliquer";
$MESS["CRM_CHANGE_CRM_FORM_NUMBER"] = "Numéro de téléphone à utiliser dans les formulaires de rappel :";
$MESS["CRM_CONFIG"] = "Configurer";
$MESS["CRM_CONFIG_CHECKER_DONE"] = "Prêt";
$MESS["CRM_CONFIG_CHECKER_INPROCESS"] = "En attente";
$MESS["CRM_CONFIG_CHECKER_NOT_ACTUAL"] = "Inutilisé";
$MESS["CRM_CONFIG_CHECKER_NOT_CHECKED"] = "Non vérifié";
$MESS["CRM_PICK_UP_THE_NUMBER_FOR_CRMFORM"] = "Sélectionnez un numéro";
$MESS["CRM_SEVERAL_NUMBERS_IS_IN_USE"] = "Les formulaires utilisent des numéros de téléphone différents";
