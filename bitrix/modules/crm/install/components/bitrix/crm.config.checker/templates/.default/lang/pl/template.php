<?php
$MESS["CMR_CONFIG_CHECKER_TITLE"] = "Skonfiguruj wymagane funkcje";
$MESS["CRM_BUTTON_APPLY"] = "Zastosuj";
$MESS["CRM_BUTTON_CHECK"] = "Zastosuj";
$MESS["CRM_CHANGE_CRM_FORM_NUMBER"] = "Numer telefonu używany w formularzach oddzwaniania:";
$MESS["CRM_CONFIG"] = "Konfiguruj";
$MESS["CRM_CONFIG_CHECKER_DONE"] = "Gotowe";
$MESS["CRM_CONFIG_CHECKER_INPROCESS"] = "W toku";
$MESS["CRM_CONFIG_CHECKER_NOT_ACTUAL"] = "Nieużywany";
$MESS["CRM_CONFIG_CHECKER_NOT_CHECKED"] = "Nie sprawdzono";
$MESS["CRM_PICK_UP_THE_NUMBER_FOR_CRMFORM"] = "Wybierz numer";
$MESS["CRM_SEVERAL_NUMBERS_IS_IN_USE"] = "Formularze używają różnych numerów telefonów";
