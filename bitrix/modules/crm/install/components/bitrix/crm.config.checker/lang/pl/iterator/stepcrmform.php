<?
$MESS["STEP_CRMFORM_DESCRIPTION"] = "Włącz integrację telefoniczną i skonfiguruj formularze oddzwaniania";
$MESS["STEP_CRMFORM_ERROR1"] = "Brak telefonów w module Telefonii.";
$MESS["STEP_CRMFORM_ERROR2"] = "Istnieją formularze oddzwaniania, nie zawierające aktywnego numeru telefonu. Wybierz numer telefonu do tych formularzy.";
$MESS["STEP_CRMFORM_TITLE"] = "Oddzwaniaj do klientów";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "Moduł Telefonia nie jest zainstalowany.";
?>