<?
$MESS["IMCONNECTOR_IS_NOT_INSTALLED"] = "El módulo \"Conectores IM externos\" no está instalado. Instale el módulo.";
$MESS["IMOPENLINES_IS_NOT_INSTALLED"] = "El módulo Canal abierto (imopenlines) no está instalado. Instale el módulo.";
$MESS["STEP_IMCONNECTOR_DESCRIPTION"] = "Conecte Facebook, Instagram y otras redes sociales a los canales abiertos de Bitrix24";
$MESS["STEP_IMCONNECTOR_ERROR1"] = "Nota: hay conectores en uso que no se configuraron.";
$MESS["STEP_IMCONNECTOR_TITLE"] = "Comuníquese y venda a través de las redes sociales";
?>