<?
$MESS["IMCONNECTOR_IS_NOT_INSTALLED"] = "Moduł „Zewnętrzne konektory IM” nie jest zainstalowany. Zainstaluj moduł.";
$MESS["IMOPENLINES_IS_NOT_INSTALLED"] = "Moduł Otwartych kanałów (imopenlines) nie jest zainstalowany. Zainstaluj ten moduł.";
$MESS["STEP_IMCONNECTOR_DESCRIPTION"] = "Połącz Facebooka, Instagram i inne media społecznościowe z Otwartymi kanałami Bitrix24";
$MESS["STEP_IMCONNECTOR_ERROR1"] = "Uwaga: używane są nieskonfigurowane konektory.";
$MESS["STEP_IMCONNECTOR_TITLE"] = "Komunikuj się i sprzedawaj za pośrednictwem mediów społecznościowych";
?>