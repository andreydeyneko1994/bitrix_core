<?
$MESS["STEP_CRMFORM_DESCRIPTION"] = "Activez l'intégration de la téléphonie et configurez les formulaires de rappel";
$MESS["STEP_CRMFORM_ERROR1"] = "Aucun téléphone n'existe dans la Téléphonie.";
$MESS["STEP_CRMFORM_ERROR2"] = "Certains formulaires de rappel ne répertorient pas de numéro de téléphone actif. Veuillez sélectionner un numéro de téléphone pour ces formulaires.";
$MESS["STEP_CRMFORM_TITLE"] = "Rappelez vos clients";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "Le module Téléphonie n'est pas installé.";
?>