<?
$MESS["STEP_TELEPHONY_DESCRIPTION"] = "Włącz integrację telefonii, aby odbierać i nawiązywać połączenia bezpośrednio w Bitrix24";
$MESS["STEP_TELEPHONY_ERROR1"] = "Nie znaleziono skonfigurowanych numerów telefonów.";
$MESS["STEP_TELEPHONY_TITLE"] = "Dzwoń do klientów i przetwarzaj połączenia przychodzące";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "Moduł Telefonii (voximplant) nie jest zainstalowany. Zainstaluj ten moduł.";
?>