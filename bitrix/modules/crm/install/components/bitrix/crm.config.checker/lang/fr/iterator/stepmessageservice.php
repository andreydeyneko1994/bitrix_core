<?
$MESS["STEP_MESSAGESERVICES_DESCRIPTION"] = "Connectez un fournisseur de SMS et envoyez des SMS groupés à vos clients";
$MESS["STEP_MESSAGESERVICES_IS_NOT_CONFIGURED"] = "Aucun service de SMS correctement configuré n'a été trouvé.";
$MESS["STEP_MESSAGESERVICES_TITLE"] = "Envoyez des SMS";
$MESS["STEP_MESSAGESERVICE_ERROR_NONEXISTENT_PROVIDER"] = "Un fournisseur de SMS est utilisé alors qu'il n'existe pas dans #provider#. ";
$MESS["STEP_MESSAGESERVICE_ERROR_NONWORKING_PROVIDER"] = "Le fournisseur #provider# est utilisé, mais pas configuré.";
$MESS["STEP_MESSAGESERVICE_IS_NOT_INSTALLED"] = "Le module Service de messagerie (messageservice) n'est pas installé. Veuillez l'installer.";
?>