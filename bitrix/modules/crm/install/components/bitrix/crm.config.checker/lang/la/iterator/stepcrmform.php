<?
$MESS["STEP_CRMFORM_DESCRIPTION"] = "Habilitar la integración de la telefonía y configurar los formularios de devolución de las llamadas";
$MESS["STEP_CRMFORM_ERROR1"] = "No hay teléfonos en Telefonía.";
$MESS["STEP_CRMFORM_ERROR2"] = "Hay formularios de devolución de llamadas que no incluyen un número de teléfono activo. Seleccione un número de teléfono para estos formularios.";
$MESS["STEP_CRMFORM_TITLE"] = "Devolver la llamada a sus clientes";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "El módulo Telefonía no está instalado.";
?>