<?
$MESS["IMCONNECTOR_IS_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado. Por favor, instale o módulo.";
$MESS["IMOPENLINES_IS_NOT_INSTALLED"] = "O módulo Canal Aberto (imopenlines) não está instalado. Por favor, instale o módulo.";
$MESS["STEP_IMCONNECTOR_DESCRIPTION"] = "Conecte o Facebook, Instagram e outras redes sociais aos Canais Abertos do Bitrix24";
$MESS["STEP_IMCONNECTOR_ERROR1"] = "Observação: há conectores em uso que não foram configurados.";
$MESS["STEP_IMCONNECTOR_TITLE"] = "Comunique-se e venda por meio das redes sociais";
?>