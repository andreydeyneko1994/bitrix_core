<?
$MESS["BPABL_PAGE_TITLE"] = "Conectividad de la tienda online";
$MESS["BPWC_WLC_WRONG_BP"] = "No se encontró el registro.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Se requiere el campo 'Iniciar sesión'.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Se requiere el campo 'URL'.";
$MESS["BPWC_WNC_EMPTY_SESSID"] = "Error de conexión de CRM a la tienda online. Es probable que el módulo de \"e-Store\" necesite ser actualizado.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Se requiere el campo 'URL'.";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Su edición no permite agregar nuevas tiendas online.";
$MESS["CRM_PERMISSION_DENIED"] = "No tiene permiso para acceder a este formulario.";
?>