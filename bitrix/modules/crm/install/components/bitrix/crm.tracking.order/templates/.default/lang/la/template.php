<?php
$MESS["CRM_TRACKING_ORDER_CONNECT"] = "Conecte el canal";
$MESS["CRM_TRACKING_ORDER_CONNECTED"] = "¡El canal ha sido conectado!";
$MESS["CRM_TRACKING_ORDER_DESC"] = "¿Quiere recibir los pedidos de su tienda en línea como negociaciones en el CRM? ¡Haga un seguimiento de las fuentes de los anuncios y los recorridos de los clientes para estas negociaciones!";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE"] = "Conecte el sitio web";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE_DESC"] = "Asegúrese de que el sitio web de su tienda online esté conectado en el canal \"Sitio web propio\".";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE"] = "Agregue el código a la página de pago";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC"] = "Coloque el código de seguimiento en la página de creación del pedido o confirmación de pago.";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC_LINK"] = "Más información";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE"] = "Ejemplo del código";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_ID"] = "ORDER_ID";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_SUM"] = "ORDER_TOTAL";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD"] = "Seleccione el campo de la negociación";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_DESC"] = "ID del pedido de su tienda online se puede guardar en la negociación. Especifique aquí el campo de la negociación para guardar el ID del pedido.";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NAME"] = "Campo de la negociación";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NONE"] = "No seleccionado";
