<?php
$MESS["CRM_TRACKING_ORDER_CONNECT"] = "Podłącz kanał";
$MESS["CRM_TRACKING_ORDER_CONNECTED"] = "Kanał został podłączony!";
$MESS["CRM_TRACKING_ORDER_DESC"] = "Otrzymujesz zamówienia ze swojego sklepu internetowego jako deale CRM? Śledź źródła reklam i podróże klientów tych deali!";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE"] = "Podłącz witrynę";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE_DESC"] = "Upewnij się, że witryna sklepu internetowego jest połączona z kanałem „Własna witryna”.";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE"] = "Dodaj kod do strony płatności";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC"] = "Umieść kod śledzenia na stronie tworzenia zamówienia lub potwierdzenia płatności.";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC_LINK"] = "Dowiedz się więcej";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE"] = "Przykład kodu";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_ID"] = "ORDER_ID";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_SUM"] = "ORDER_TOTAL";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD"] = "Wybierz pole deala";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_DESC"] = "Twój ID zamówienia w sklepie internetowym można zapisać w formularzu deala. Podaj tutaj pole deala, które będzie zawierać ID zamówienia.";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NAME"] = "Pole deala";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NONE"] = "Nie wybrano";
