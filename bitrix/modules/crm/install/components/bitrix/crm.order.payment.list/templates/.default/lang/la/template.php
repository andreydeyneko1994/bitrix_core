<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID"] = "Paga";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID_N"] = "Cancelar el pago";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_DLG_TITLE"] = "Preparar el historial de pagos";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_STATE"] = "#processed# de #total#";
$MESS["CRM_ORDER_PAYMENT_DELETE"] = "Eliminar";
$MESS["CRM_ORDER_PAYMENT_DELETE_CONFIRM"] = "¿Usted está seguro que quiere eliminar este elemento?";
$MESS["CRM_ORDER_PAYMENT_DELETE_TITLE"] = "Eliminar Pago";
$MESS["CRM_ORDER_PAYMENT_EDIT"] = "Editar";
$MESS["CRM_ORDER_PAYMENT_EDIT_TITLE"] = "Editar pago";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD"] = "Agregar pago";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD_SHORT"] = "Pago";
$MESS["CRM_ORDER_PAYMENT_LIST_CHOOSE_ACTION"] = "Seleccione la acción";
$MESS["CRM_ORDER_PAYMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_CLOSE"] = "Cerrar";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_START"] = "Ejecutar";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_STOP"] = "Stop";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_REQUEST_ERR"] = "Error al procesar la solicitud.";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_WAIT"] = "Por favor espera...";
$MESS["CRM_ORDER_PAYMENT_PAID"] = "Pagar";
$MESS["CRM_ORDER_PAYMENT_PAYMENT_SUMMARY"] = "Pago ##ACCOUNT_NUMBER# de#DATE_BILL#";
$MESS["CRM_ORDER_PAYMENT_REBUILD_ACCESS_ATTRS"] = "Los permisos de acceso actualizados requieren que actualice los atributos de acceso actuales utilizando la <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">página de gestión de permisos</a>.";
$MESS["CRM_ORDER_PAYMENT_SHOW"] = "Ver";
$MESS["CRM_ORDER_PAYMENT_SHOW_TITLE"] = "Ver Pago";
$MESS["CRM_ORDER_PAYMENT_UNPAID"] = "No pagar";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar cantidad";
?>