<?
$MESS["CRM_ALL"] = "Łącznie";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID"] = "Zapłać";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID_N"] = "Anuluj płatność";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_DLG_TITLE"] = "Przygotuj historię płatności";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_STATE"] = "#processed# z #total#";
$MESS["CRM_ORDER_PAYMENT_DELETE"] = "Usuń";
$MESS["CRM_ORDER_PAYMENT_DELETE_CONFIRM"] = "Na pewno usunąć ten element?";
$MESS["CRM_ORDER_PAYMENT_DELETE_TITLE"] = "Usuń płatność";
$MESS["CRM_ORDER_PAYMENT_EDIT"] = "Edytuj";
$MESS["CRM_ORDER_PAYMENT_EDIT_TITLE"] = "Edytuj płatność";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD"] = "Dodaj płatność";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD_SHORT"] = "Płatność";
$MESS["CRM_ORDER_PAYMENT_LIST_CHOOSE_ACTION"] = "Wybierz działanie";
$MESS["CRM_ORDER_PAYMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_CLOSE"] = "Zamknij";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_START"] = "Uruchom";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_STOP"] = "Stop";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_REQUEST_ERR"] = "Błąd przetwarzania żądania.";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_WAIT"] = "Proszę czekać...";
$MESS["CRM_ORDER_PAYMENT_PAID"] = "Zapłacono";
$MESS["CRM_ORDER_PAYMENT_PAYMENT_SUMMARY"] = "Płatność ##ACCOUNT_NUMBER# of #DATE_BILL#";
$MESS["CRM_ORDER_PAYMENT_REBUILD_ACCESS_ATTRS"] = "Zaktualizowane uprawnienia dostępu wymagają aktualizacji bieżących atrybutów dostępu przy użyciu <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">strony zarządzania uprawnieniami</a>.";
$MESS["CRM_ORDER_PAYMENT_SHOW"] = "Widok";
$MESS["CRM_ORDER_PAYMENT_SHOW_TITLE"] = "Wyświetl płatność";
$MESS["CRM_ORDER_PAYMENT_UNPAID"] = "Nie zapłacono";
$MESS["CRM_SHOW_ROW_COUNT"] = "Pokaż ilość";
?>