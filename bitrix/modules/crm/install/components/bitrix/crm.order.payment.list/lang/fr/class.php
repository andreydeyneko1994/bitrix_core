<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devises n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_PAYMENT_DELETE_ERROR"] = "Erreur lors de la suppression du paiement";
$MESS["CRM_PAYMENT_HEADER_ACCOUNT_NUMBER"] = "Paiement #";
$MESS["CRM_PAYMENT_HEADER_CURRENCY"] = "Devise";
$MESS["CRM_PAYMENT_HEADER_DATE_BILL"] = "Date de création";
$MESS["CRM_PAYMENT_HEADER_DATE_PAID"] = "Date de paiement";
$MESS["CRM_PAYMENT_HEADER_DATE_PAY_BEFORE"] = "Payer avant";
$MESS["CRM_PAYMENT_HEADER_ID"] = "ID du paiement";
$MESS["CRM_PAYMENT_HEADER_ORDER_ID"] = "ID de la commande";
$MESS["CRM_PAYMENT_HEADER_PAID"] = "Statut";
$MESS["CRM_PAYMENT_HEADER_PAYMENT_SUMMARY"] = "Paiement";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_FULL"] = "Mode de paiement";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_NAME"] = "Système de paiement";
$MESS["CRM_PAYMENT_HEADER_PAY_VOUCHER_NUM"] = "Numéro du document de paiement";
$MESS["CRM_PAYMENT_HEADER_RESPONSIBLE"] = "Responsable";
$MESS["CRM_PAYMENT_HEADER_SUM"] = "Montant";
$MESS["CRM_PAYMENT_HEADER_USER_ID"] = "Client";
$MESS["CRM_PAYMENT_NAME"] = "Livraison ##ACCOUNT_NUMBER# du #DATE_BILL#";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
?>