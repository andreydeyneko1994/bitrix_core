<?
$MESS["CRM_DEAL_SETTINGS"] = "Możesz skonfigurować #LINK_START# tryb tworzenia deala #LINK_END#";
$MESS["CRM_RIGTHS_INFO"] = "Skontaktuj się z administratorem, aby zmienić tryb CRM";
$MESS["CRM_TYPE_CANCEL"] = "Anuluj";
$MESS["CRM_TYPE_CHANGE"] = "Możesz zmienić swoje preferencje w dowolnym momencie w ustawieniach CRM";
$MESS["CRM_TYPE_CLASSIC"] = "Klasyczny CRM";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Leady &rarr; deale + kontakty";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Zalecane dla średnich i dużych firm. Po pierwsze, wszystkie zapytania przychodzące stają się potencjalnymi klientami - leadami. Następnie wasz zespół sprzedaży próbuje przekonwertować leady do deali i kontaktów.";
$MESS["CRM_TYPE_MORE"] = "Dowiedz się więcej";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Leadem jest każda jednostka lub osoba, która może potencjalnie zostać klientem.<br/>Istnieją dwa sposoby pracy z systemem CRM Bitrix24:";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "W tej chwili używasz trybu \"Prosty CRM\", w którym<br/>nowe zapytania klientów natychmiast stają się umowami.<br/>Możliwe są dwie opcje:";
$MESS["CRM_TYPE_SAVE"] = "Zapisz";
$MESS["CRM_TYPE_SIMPLE"] = "Prosty CRM";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Deale + kontakty (bez leadów)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Zalecane dla działów sprzedaży małych firm. Wszystkie przychodzące wiadomości e-mail, połączenia, żądania i czaty stają się natychmiast dealami i/lub kontaktami.";
$MESS["CRM_TYPE_TITLE"] = "Wybierz, jak chcesz pracować ze swoim CRM-em";
?>