<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CALENDAR_DELETE"] = "Supprimer l'appel/la rencontre";
$MESS["CRM_CALENDAR_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer ?";
$MESS["CRM_CALENDAR_DELETE_TITLE"] = "Supprimer l'appel/la rencontre";
$MESS["CRM_CALENDAR_EDIT"] = "Éditer l'appel/rendez-vous";
$MESS["CRM_CALENDAR_EDIT_TITLE"] = "Éditer l'appel/rendez-vous";
$MESS["CRM_CALENDAR_SHOW"] = "Voir appel/réunion";
$MESS["CRM_CALENDAR_SHOW_TITLE"] = "Voir appel/réunion";
?>