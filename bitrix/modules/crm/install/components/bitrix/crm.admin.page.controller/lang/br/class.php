<?php
$MESS["CRM_SHOP_MENU_ITEM_EXPERT_SETTINGS"] = "Configurações Avançadas";
$MESS["CRM_SHOP_MENU_ITEM_GOODS"] = "Produtos";
$MESS["SHOP_MENU_BUYER_GROUP_TITLE"] = "Grupos de clientes";
$MESS["SHOP_MENU_CATALOG_GOODS"] = "Catálogo de produtos";
$MESS["SHOP_MENU_CONTACT_CENTER"] = "Contact Center";
$MESS["SHOP_MENU_CRM_CLIENTS"] = "Clientes";
$MESS["SHOP_MENU_CRM_COMPANIES"] = "Empresas";
$MESS["SHOP_MENU_CRM_CONTACTS"] = "Contatos";
$MESS["SHOP_MENU_DEALS_TITLE"] = "Negócios";
$MESS["SHOP_MENU_GOODS_DOCUMENTS"] = "Inventário";
$MESS["SHOP_MENU_ORDER_FORM_SETTINGS_TITLE"] = "Configurar formulário de pagamento";
$MESS["SHOP_MENU_ORDER_TITLE"] = "Pedidos";
$MESS["SHOP_MENU_PRODUCT_MARKETING_COUPONS"] = "Cupons";
$MESS["SHOP_MENU_PRODUCT_MARKETING_DISCOUNT"] = "Regras do carrinho de compras";
$MESS["SHOP_MENU_PRODUCT_MARKETING_TITLE"] = "CRM Marketing";
$MESS["SHOP_MENU_QUICK_START_SETTINGS"] = "Configurações";
$MESS["SHOP_MENU_SETTINGS_CATALOG_SETTINGS"] = "Gerenciamento de inventário e mercadorias";
$MESS["SHOP_MENU_SETTINGS_SALE_SETTINGS"] = "Parâmetros da Loja On-line";
$MESS["SHOP_MENU_SETTINGS_STATUS"] = "Status";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER"] = "Status do Pedido";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER_SHIPMENT"] = "Status da Entrega";
$MESS["SHOP_MENU_SETTINGS_USER_FIELDS"] = "Campos Personalizados";
$MESS["SHOP_MENU_SHOP_MARKETING"] = "Marketing de loja online";
$MESS["SHOP_MENU_SHOP_TITLE"] = "Lojas On-line";
$MESS["SHOP_MENU_SITES"] = "Sites";
$MESS["SHOP_MENU_WEBFORMS"] = "Formulários de CRM";
