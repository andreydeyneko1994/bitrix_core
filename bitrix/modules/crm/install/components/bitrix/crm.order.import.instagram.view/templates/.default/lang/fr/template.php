<?
$MESS["CRM_OIIV_CANCEL"] = "Annuler";
$MESS["CRM_OIIV_CREATE_STORE"] = "Créer une boutique en ligne";
$MESS["CRM_OIIV_IMPORT"] = "Importer";
$MESS["CRM_OIIV_IMPORT_FEEDBACK"] = "Commentaire";
$MESS["CRM_OIIV_IMPORT_MORE"] = "Importer plus";
$MESS["CRM_OIIV_IMPORT_NETWORK_ERROR"] = "Erreur inconnue lors de l'importation des données. Veuillez réessayer.";
$MESS["CRM_OIIV_INSTAGRAM_CONNECTED"] = "connecté";
$MESS["CRM_OIIV_NO_MEDIA"] = "Il n'y a pas de publications dans le profil";
$MESS["CRM_OIIV_NO_PRICE"] = "spécifier le prix";
$MESS["CRM_OIIV_PRODUCTS_ADDED_SUCCESSFUL"] = "Les produits ont été importés dans le catalogue commercial CRM";
$MESS["CRM_OIIV_PRODUCT_CHANGE_PRICE_SUCCESSFUL"] = "Le prix du produit a été modifié";
$MESS["CRM_OIIV_PRODUCT_RENAME_SUCCESSFUL"] = "Le nom du produit a été changé";
$MESS["CRM_OIIV_SELECT_ALL"] = "Tout sélectionner";
$MESS["CRM_OIIV_SELECT_MEDIA"] = "Sélectionnez la publication à importer";
$MESS["CRM_OIIV_SELECT_NO_MEDIA"] = "Aucune nouvelle publication est disponible";
$MESS["CRM_OIIV_STEP_IMPORTED"] = "Publications importées";
$MESS["CRM_OIIV_STEP_PROCESS_TITLE"] = "Les publications sont importées";
$MESS["CRM_OIIV_STEP_READY_TITLE"] = "C'est fait !";
$MESS["CRM_OIIV_STEP_STOPPED_TITLE"] = "Importation interrompue";
$MESS["CRM_OIIV_STOP"] = "Arrêter";
$MESS["CRM_OIIV_TOTAL_IMPORTED"] = "Nombre total de publications importées du profil";
$MESS["CRM_OIIV_UNSELECT_ALL"] = "Tout déselectionner";
?>