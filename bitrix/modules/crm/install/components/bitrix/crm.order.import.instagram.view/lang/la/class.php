<?
$MESS["CRM_OIIV_COLUMN_CAPTION"] = "Leyenda";
$MESS["CRM_OIIV_COLUMN_IMPORTED"] = "Importado";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE"] = "Tipo de publicación";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_CAROUSEL_ALBUM"] = "Carrusel";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_PICTURE"] = "Imagen";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_VIDEO"] = "Video";
$MESS["CRM_OIIV_COLUMN_NEW"] = "Nuevo";
$MESS["CRM_OIIV_COLUMN_PERMALINK"] = "Enlace a la publicación";
$MESS["CRM_OIIV_COLUMN_TIMESTAMP"] = "Creado el";
$MESS["CRM_OIIV_DEFAULT_PRODUCT_NAME"] = "Sin título";
$MESS["CRM_OIIV_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo";
$MESS["CRM_OIIV_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "La importación de productos se configuró para su uso con un grupo al que actualmente no tiene acceso de administrador.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CRM"] = "El módulo CRM no está instalado.";
$MESS["CRM_OIIV_PRESET_IMPORTED"] = "Importado";
$MESS["CRM_OIIV_PRESET_NOT_IMPORTED"] = "No importado";
$MESS["CRM_OIIV_PRESET_RECENT"] = "Nuevo";
$MESS["CRM_OIIV_TITLE"] = "Importar productos a la tienda online de Bitrix24";
?>