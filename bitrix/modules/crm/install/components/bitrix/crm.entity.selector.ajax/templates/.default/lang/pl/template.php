<?
$MESS["REST_CRM_FF_CANCEL"] = "Anuluj";
$MESS["REST_CRM_FF_CHANGE"] = "Edytuj";
$MESS["REST_CRM_FF_CHOISE"] = "Wybierz";
$MESS["REST_CRM_FF_CLOSE"] = "Zamknij";
$MESS["REST_CRM_FF_COMPANY"] = "Firmy";
$MESS["REST_CRM_FF_CONTACT"] = "Kontakty";
$MESS["REST_CRM_FF_DEAL"] = "Deale";
$MESS["REST_CRM_FF_LAST"] = "Ostatnie";
$MESS["REST_CRM_FF_LEAD"] = "Leady";
$MESS["REST_CRM_FF_NO_RESULT"] = "Niestety wyszukiwanie twojego zapytania nie przyniosło rezultatów.";
$MESS["REST_CRM_FF_OK"] = "Wybierz";
$MESS["REST_CRM_FF_QUOTE"] = "Oferty";
$MESS["REST_CRM_FF_SEARCH"] = "Szukaj";
?>