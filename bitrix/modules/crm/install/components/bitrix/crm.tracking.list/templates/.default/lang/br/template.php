<?php
$MESS["CRM_TRACKING_LIST_CHANNELS"] = "Canais de comunicação";
$MESS["CRM_TRACKING_LIST_SOURCES"] = "Fontes de tráfego";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP"] = "Ajuda de configuração";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP_ORDER"] = "Solicitar ajuda de configuração de parceiros";
$MESS["CRM_TRACKING_START_CONFIGURATION_NEED_HELP"] = "Você precisa de ajuda para configurar o rastreador?";
$MESS["CRM_TRACKING_START_CONFIGURATION_ORDER"] = "Obter ajuda agora";
