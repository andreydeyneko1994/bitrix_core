<?php
$MESS["CRM_TRACKING_LIST_CHANNELS"] = "Canaux de communication";
$MESS["CRM_TRACKING_LIST_SOURCES"] = "Sources du trafic";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP"] = "Aide à la configuration";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP_ORDER"] = "Aide des partenaires dans la configuration";
$MESS["CRM_TRACKING_START_CONFIGURATION_NEED_HELP"] = "Avez-vous besoin d'aide pour configurer le suivi ?";
$MESS["CRM_TRACKING_START_CONFIGURATION_ORDER"] = "Demander de l'aide";
