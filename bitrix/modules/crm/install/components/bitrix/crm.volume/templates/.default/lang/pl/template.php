<?
$MESS["CRM_VOLUME_AGENT_FINISHED_COMMENT"] = "Zadanie czyszczenia zakończone. Należy ponownie zeskanować dysk.";
$MESS["CRM_VOLUME_CANCEL"] = "Anuluj";
$MESS["CRM_VOLUME_CANCEL_WORKERS"] = "Anuluj wszystkie zadania czyszczenia";
$MESS["CRM_VOLUME_CLEARING"] = "Oczyszczanie CRM";
$MESS["CRM_VOLUME_CLOSE_WARNING"] = "Strona portalu musi pozostać otwarta, aby zakończyć czyszczenie dysku.";
$MESS["CRM_VOLUME_ERROR"] = "Wystąpił błąd.";
$MESS["CRM_VOLUME_MEASURE_ACCEPT"] = "Tak, zeskanuj ponownie";
$MESS["CRM_VOLUME_MEASURE_CONFIRM"] = "Skanowanie dysku zatrzyma proces czyszczenia. Będzie można uruchomić go ponownie dopiero po zeskanowaniu dysku.";
$MESS["CRM_VOLUME_MEASURE_CONFIRM_QUESTION"] = "Czy na pewno chcesz anulować czyszczenie i ponownie zeskanować dysk?";
$MESS["CRM_VOLUME_MEASURE_DATA"] = "Rozpocznij skanowanie";
$MESS["CRM_VOLUME_MEASURE_DATA_REPEAT"] = "Powtórz skanowanie";
$MESS["CRM_VOLUME_MEASURE_PROCESS"] = "Skanowanie może trochę potrwać.";
$MESS["CRM_VOLUME_MEASURE_TITLE"] = "Analizowanie wykorzystywanej przestrzeni...";
$MESS["CRM_VOLUME_NEED_RELOAD_COMMENT"] = "Poprzednie wyniki skanowania mogą być nieaktualne.";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_MEASURE"] = "Anuluj skanowanie";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_WORKERS"] = "Anuluj zadania czyszczenia";
$MESS["CRM_VOLUME_PERFORMING_MEASURE_DATA"] = "Zbiór danych";
$MESS["CRM_VOLUME_PERFORMING_QUEUE"] = "Zbieranie danych. Krok #QUEUE_STEP# z #QUEUE_LENGTH#";
$MESS["CRM_VOLUME_SETUP_CLEANER"] = "Harmonogram czyszczenia";
$MESS["CRM_VOLUME_START_COMMENT"] = "Bitrix24 zeskanuje dokumenty i poprosi o wybranie elementów, które nie będą już potrzebne, aby zwolnić więcej miejsca.";
$MESS["CRM_VOLUME_START_TITLE"] = "Wolne miejsce";
$MESS["CRM_VOLUME_SUCCESS"] = "Sukces";
$MESS["CRM_VOLUME_TITLE"] = "Wykorzystanie dysku";
$MESS["CRM_VOLUME_TOTAL_FILES"] = "Wykorzystywane przez pliki: #FILE_SIZE#";
$MESS["CRM_VOLUME_TOTAL_USEAGE"] = "Łącznie wykorzystane miejsce: #FILE_SIZE#";
?>