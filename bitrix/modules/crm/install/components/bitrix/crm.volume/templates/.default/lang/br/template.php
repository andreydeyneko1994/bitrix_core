<?
$MESS["CRM_VOLUME_AGENT_FINISHED_COMMENT"] = "Tarefa de limpeza concluída. Você tem que verificar o drive novamente.";
$MESS["CRM_VOLUME_CANCEL"] = "Cancelar";
$MESS["CRM_VOLUME_CANCEL_WORKERS"] = "Cancelar todas as tarefas de limpeza";
$MESS["CRM_VOLUME_CLEARING"] = "Limpeza de CRM";
$MESS["CRM_VOLUME_CLOSE_WARNING"] = "A página do portal deve permanecer aberta para concluir a limpeza do drive.";
$MESS["CRM_VOLUME_ERROR"] = "Isso é um erro.";
$MESS["CRM_VOLUME_MEASURE_ACCEPT"] = "Sim, verificar novamente";
$MESS["CRM_VOLUME_MEASURE_CONFIRM"] = "A verificação do drive irá interromper o processo de limpeza. Você poderá iniciá-la novamente somente depois que o drive foi verificado.";
$MESS["CRM_VOLUME_MEASURE_CONFIRM_QUESTION"] = "Você tem certeza que deseja cancelar a limpeza e a verificação do drive novamente?";
$MESS["CRM_VOLUME_MEASURE_DATA"] = "Começar a Verificar";
$MESS["CRM_VOLUME_MEASURE_DATA_REPEAT"] = "Repetir Varredura";
$MESS["CRM_VOLUME_MEASURE_PROCESS"] = "A digitalização pode demorar um pouco.";
$MESS["CRM_VOLUME_MEASURE_TITLE"] = "Analisando o espaço usado...";
$MESS["CRM_VOLUME_NEED_RELOAD_COMMENT"] = "Os resultados da verificação anterior podem estar desatualizados.";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_MEASURE"] = "Cancelar verificação";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_WORKERS"] = "Cancelar tarefa de limpeza";
$MESS["CRM_VOLUME_PERFORMING_MEASURE_DATA"] = "Coleta de Dados";
$MESS["CRM_VOLUME_PERFORMING_QUEUE"] = "Coleta de dados. Etapa #QUEUE_STEP# de #QUEUE_LENGTH#";
$MESS["CRM_VOLUME_SETUP_CLEANER"] = "Agendamento de limpeza";
$MESS["CRM_VOLUME_START_COMMENT"] = "O Bitrix24 irá verificar seus documentos e solicitar que você selecione os itens que não precisa mais para liberar mais espaço.";
$MESS["CRM_VOLUME_START_TITLE"] = "Espaço Livre";
$MESS["CRM_VOLUME_SUCCESS"] = "Bem sucedido";
$MESS["CRM_VOLUME_TITLE"] = "Uso do Drive";
$MESS["CRM_VOLUME_TOTAL_FILES"] = "Usado por arquivos: #FILE_SIZE#";
$MESS["CRM_VOLUME_TOTAL_USEAGE"] = "Espaço total utilizado: #FILE_SIZE#";
?>