<?php
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS"] = "Analityka";
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS_CALLS"] = "Analityka połączeń";
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS_DIALOGS"] = "Analityka rozmów";
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS_MANAGERS"] = "Wyniki pracowników";
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS_SALES_FUNNEL"] = "Lejek sprzedażowy";
$MESS["CRM_CTRL_PANEL_ITEM_ANALYTICS_TITLE"] = "Analityka CRM";
$MESS["CRM_CTRL_PANEL_ITEM_BUTTON"] = "Widżet na stronę";
$MESS["CRM_CTRL_PANEL_ITEM_CATALOGUE"] = "Katalog";
$MESS["CRM_CTRL_PANEL_ITEM_CATALOGUE_2"] = "Produkty";
$MESS["CRM_CTRL_PANEL_ITEM_CATALOGUE_GOODS"] = "Katalog produktów";
$MESS["CRM_CTRL_PANEL_ITEM_CATALOGUE_STORE_DOCS"] = "Asortyment";
$MESS["CRM_CTRL_PANEL_ITEM_CLIENTS"] = "Klienci";
$MESS["CRM_CTRL_PANEL_ITEM_COMPANY"] = "Firmy";
$MESS["CRM_CTRL_PANEL_ITEM_COMPANY_TITLE"] = "Firmy";
$MESS["CRM_CTRL_PANEL_ITEM_CONFIGS"] = "Ustawienia CRM";
$MESS["CRM_CTRL_PANEL_ITEM_CONTACT"] = "Kontakty";
$MESS["CRM_CTRL_PANEL_ITEM_CONTACT_CENTER"] = "Centrum kontaktowe";
$MESS["CRM_CTRL_PANEL_ITEM_CONTACT_TITLE"] = "Kontakty";
$MESS["CRM_CTRL_PANEL_ITEM_CRMPLUS"] = "CRM+";
$MESS["CRM_CTRL_PANEL_ITEM_CRM_TRACKING"] = "Sales Intelligence";
$MESS["CRM_CTRL_PANEL_ITEM_DEAL"] = "Deale";
$MESS["CRM_CTRL_PANEL_ITEM_DEAL_TITLE"] = "Deale";
$MESS["CRM_CTRL_PANEL_ITEM_DEVOPS"] = "Rest API";
$MESS["CRM_CTRL_PANEL_ITEM_DYNAMIC_LIST"] = "Inteligentna automatyzacja procesów";
$MESS["CRM_CTRL_PANEL_ITEM_EVENT"] = "Wydarzenia";
$MESS["CRM_CTRL_PANEL_ITEM_EVENT_2"] = "Historia";
$MESS["CRM_CTRL_PANEL_ITEM_FUNNEL"] = "Lejek sprzedażowy";
$MESS["CRM_CTRL_PANEL_ITEM_FUNNEL_BRIEF"] = "Lejek";
$MESS["CRM_CTRL_PANEL_ITEM_INTEGRATIONS"] = "Dodatki";
$MESS["CRM_CTRL_PANEL_ITEM_INVOICE"] = "Faktury";
$MESS["CRM_CTRL_PANEL_ITEM_INVOICE_TITLE"] = "Lista faktur";
$MESS["CRM_CTRL_PANEL_ITEM_INVOICING"] = "Wyciąg z konta";
$MESS["CRM_CTRL_PANEL_ITEM_LEAD"] = "Leady";
$MESS["CRM_CTRL_PANEL_ITEM_LEAD_TITLE"] = "Leady";
$MESS["CRM_CTRL_PANEL_ITEM_MAIL"] = "Poczta internetowa";
$MESS["CRM_CTRL_PANEL_ITEM_MARKETPLACE"] = "Rynek aplikacji";
$MESS["CRM_CTRL_PANEL_ITEM_MARKETPLACE_2"] = "Market";
$MESS["CRM_CTRL_PANEL_ITEM_MARKETPLACE_CRM_MIGRATION"] = "Import z zewnętrznego CRM";
$MESS["CRM_CTRL_PANEL_ITEM_MARKETPLACE_CRM_SOLUTIONS"] = "Ustawienia wstępne rozwiązania CRM";
$MESS["CRM_CTRL_PANEL_ITEM_MESSENGERS"] = "Komunikatory";
$MESS["CRM_CTRL_PANEL_ITEM_MORE"] = "Więcej...";
$MESS["CRM_CTRL_PANEL_ITEM_MORE_TITLE"] = "więcej";
$MESS["CRM_CTRL_PANEL_ITEM_MY_ACTIVITY"] = "Działania";
$MESS["CRM_CTRL_PANEL_ITEM_MY_ACTIVITY_TITLE"] = "Moje działania";
$MESS["CRM_CTRL_PANEL_ITEM_MY_COMPANY"] = "Moje dane firmy";
$MESS["CRM_CTRL_PANEL_ITEM_ORDER"] = "Zamówienia";
$MESS["CRM_CTRL_PANEL_ITEM_PERMISSIONS"] = "Uprawnienia dostępu";
$MESS["CRM_CTRL_PANEL_ITEM_QUOTE"] = "Oferty";
$MESS["CRM_CTRL_PANEL_ITEM_QUOTE_TITLE"] = "Wyświetl wszystkie oferty";
$MESS["CRM_CTRL_PANEL_ITEM_RECYCLE_BIN"] = "Kosz";
$MESS["CRM_CTRL_PANEL_ITEM_RECYCLE_BIN_TITLE"] = "Przeglądaj i odzyskaj usunięte leady, kontakty, firmy i deale.";
$MESS["CRM_CTRL_PANEL_ITEM_REPORT"] = "Raporty";
$MESS["CRM_CTRL_PANEL_ITEM_REPORT_CONSTRUCTOR2"] = "Raporty";
$MESS["CRM_CTRL_PANEL_ITEM_REPORT_TITLE"] = "Raporty";
$MESS["CRM_CTRL_PANEL_ITEM_SALES"] = "Sprzedaż";
$MESS["CRM_CTRL_PANEL_ITEM_SALES_CENTER"] = "Centrum sprzedaży";
$MESS["CRM_CTRL_PANEL_ITEM_SALES_CENTER_DELIVERY"] = "CRM.Delivery";
$MESS["CRM_CTRL_PANEL_ITEM_SALES_CENTER_PAYMENT"] = "CRM.Payment";
$MESS["CRM_CTRL_PANEL_ITEM_SETTINGS"] = "Ustawienia";
$MESS["CRM_CTRL_PANEL_ITEM_SMART_ENTITY_LIST"] = "Inteligentne automatyzacje procesów";
$MESS["CRM_CTRL_PANEL_ITEM_START"] = "Start";
$MESS["CRM_CTRL_PANEL_ITEM_START_TITLE"] = "Strona główna ukazująca podsumowanie kanału";
$MESS["CRM_CTRL_PANEL_ITEM_STORE_DOCS"] = "Zarządzanie asortymentem";
$MESS["CRM_CTRL_PANEL_ITEM_STREAM"] = "Tablica";
$MESS["CRM_CTRL_PANEL_ITEM_STREAM_TITLE"] = "Aktualności CRM";
$MESS["CRM_CTRL_PANEL_ITEM_TELEPHONY"] = "Telefonia";
$MESS["CRM_CTRL_PANEL_ITEM_WEBFORM"] = "Formularze CRM";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
