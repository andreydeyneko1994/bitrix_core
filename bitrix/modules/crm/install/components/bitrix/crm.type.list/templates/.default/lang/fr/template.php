<?php
$MESS["CRM_TYPE_LIST_WELCOME_LINK"] = "Plus d'informations sur les Automatisations intelligentes des processus";
$MESS["CRM_TYPE_LIST_WELCOME_TEXT"] = "Allez au-delà des transactions, des prospects et des devis : créez et automatisez vos propres entités (listes d'entrepreneurs, entonnoirs d'achat, registre de documents, etc.).";
$MESS["CRM_TYPE_LIST_WELCOME_TITLE"] = "Créer une nouvelle Automatisation intelligente de processus";
