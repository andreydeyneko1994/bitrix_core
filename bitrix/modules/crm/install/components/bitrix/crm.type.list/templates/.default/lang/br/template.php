<?php
$MESS["CRM_TYPE_LIST_WELCOME_LINK"] = "Saiba mais sobre Automações Inteligentes de Processos";
$MESS["CRM_TYPE_LIST_WELCOME_TEXT"] = "Vá além de negócios, leads e cotações - crie e automatize suas próprias entidades (listas de contratados, funis de compra, registro de documentos, etc.).";
$MESS["CRM_TYPE_LIST_WELCOME_TITLE"] = "Criar uma nova Automação Inteligente de Processos";
