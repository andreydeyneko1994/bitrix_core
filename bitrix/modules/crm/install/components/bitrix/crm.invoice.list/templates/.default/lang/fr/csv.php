<?
$MESS["CRM_COLUMN_COMPANY"] = "Entreprise";
$MESS["CRM_COLUMN_CONTACT"] = "Contact";
$MESS["CRM_COLUMN_DEAL"] = "Transaction";
$MESS["CRM_COLUMN_PRODUCT_NAME"] = "Produit";
$MESS["CRM_COLUMN_PRODUCT_PRICE"] = "Prix";
$MESS["CRM_COLUMN_PRODUCT_QUANTITY"] = "Quantité";
$MESS["ERROR_INVOICE_IS_EMPTY_2"] = "Il n'y a aucune facture dans la liste.";
?>