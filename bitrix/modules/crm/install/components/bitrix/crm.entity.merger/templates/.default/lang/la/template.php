<?php
$MESS["CRM_ENTITY_MERGER_ENTITIES_NOT_FOUND"] = "Nada que fusionar aquí.";
$MESS["CRM_ENTITY_MERGER_GO_TO_DUPLICATE_LIST"] = "Volver a la lista";
$MESS["CRM_ENTITY_MERGER_MARK_AS_NON_DUPLICATE"] = "no es un duplicado";
$MESS["CRM_ENTITY_MERGER_MERGE_AND_EDIT"] = "Fusionar y editar";
$MESS["CRM_ENTITY_MERGER_OPEN_ENTITY"] = "abrir";
$MESS["CRM_ENTITY_MERGER_POSTPONE"] = "Más tarde";
$MESS["CRM_ENTITY_MERGER_PRIMARY_ENTITY_NOT_FOUND"] = "Seleccione el registro primario para fusionar los datos duplicados.";
$MESS["CRM_ENTITY_MERGER_PROCESS"] = "Fusionar";
$MESS["CRM_ENTITY_MERGER_PROCESSED_AMOUNT"] = "fusionado";
$MESS["CRM_ENTITY_MERGER_REMAINING_AMOUNT"] = "sin procesar";
$MESS["CRM_ENTITY_MERGER_RESULT_TITLE"] = "Resultado de la fusión";
$MESS["CRM_ENTITY_MERGER_UNRESOLVED_CONFLICTS_FOUND"] = "Hubo conflictos que no pudieron resolverse. Seleccione las opciones correctas.";
