<?php
$MESS["CRM_ENTITY_MERGER_ENTITIES_NOT_FOUND"] = "Nie ma tu nic do scalenia.";
$MESS["CRM_ENTITY_MERGER_GO_TO_DUPLICATE_LIST"] = "Powrót do listy";
$MESS["CRM_ENTITY_MERGER_MARK_AS_NON_DUPLICATE"] = "to nie jest duplikat";
$MESS["CRM_ENTITY_MERGER_MERGE_AND_EDIT"] = "Scal i edytuj";
$MESS["CRM_ENTITY_MERGER_OPEN_ENTITY"] = "otwórz";
$MESS["CRM_ENTITY_MERGER_POSTPONE"] = "Później";
$MESS["CRM_ENTITY_MERGER_PRIMARY_ENTITY_NOT_FOUND"] = "Wybierz rekord główny, aby scalić zduplikowane dane.";
$MESS["CRM_ENTITY_MERGER_PROCESS"] = "Scal";
$MESS["CRM_ENTITY_MERGER_PROCESSED_AMOUNT"] = "scalone";
$MESS["CRM_ENTITY_MERGER_REMAINING_AMOUNT"] = "nieprzetworzone";
$MESS["CRM_ENTITY_MERGER_RESULT_TITLE"] = "Scal wynik";
$MESS["CRM_ENTITY_MERGER_UNRESOLVED_CONFLICTS_FOUND"] = "Wystąpiły konflikty, których nie można było rozwiązać. Wybierz odpowiednie opcje.";
