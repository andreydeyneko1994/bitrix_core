<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_NOT_FOUND"] = "La société de la liste de mentions n'a pas été trouvée.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_READ_DENIED"] = "L'accès à la société a été refusé.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_NOT_FOUND"] = "Le contact de la liste de mentions n'a pas été trouvé.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_READ_DENIED"] = "L'accès au contact a été refusé.";
$MESS["CRM_REQUISITE_LIST_ERR_ENTITY_TYPE_ID"] = "Type d'entité incorrect pour la liste des mentions de contact ou de la société fournie.";
$MESS["CRM_REQUISITE_LIST_NO"] = "Non";
$MESS["CRM_REQUISITE_LIST_PRESET_NAME_FIELD"] = "Nom du modèle";
$MESS["CRM_REQUISITE_LIST_YES"] = "Oui";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Modèle sans titre";
?>