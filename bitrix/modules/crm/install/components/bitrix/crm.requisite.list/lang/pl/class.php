<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_NOT_FOUND"] = "Nie znaleziono firmy dla listy szczegółów.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_READ_DENIED"] = "Odmowa dostępu do firmy.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_NOT_FOUND"] = "Nie znaleziono kontaktu dla listy szczegółów.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_READ_DENIED"] = "Odmowa dostępu do kontaktu.";
$MESS["CRM_REQUISITE_LIST_ERR_ENTITY_TYPE_ID"] = "Podano nieprawidłowy typ jednostki dla (podanej) listy szczegółów kontaktu lub firmy.";
$MESS["CRM_REQUISITE_LIST_NO"] = "Nie";
$MESS["CRM_REQUISITE_LIST_PRESET_NAME_FIELD"] = "Nazwa szablonu";
$MESS["CRM_REQUISITE_LIST_YES"] = "Tak";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Szablon nienazwany";
?>