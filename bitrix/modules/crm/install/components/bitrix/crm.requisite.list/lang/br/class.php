<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_NOT_FOUND"] = "A empresa para lista de informações não foi encontrada.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_READ_DENIED"] = "O acesso à empresa foi negado.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_NOT_FOUND"] = "O contato para lista de informações não foi encontrado.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_READ_DENIED"] = "O acesso ao contato foi negado.";
$MESS["CRM_REQUISITE_LIST_ERR_ENTITY_TYPE_ID"] = "Tipo de entidade para contato ou lista de informações da empresa fornecida incorretos.";
$MESS["CRM_REQUISITE_LIST_NO"] = "Não";
$MESS["CRM_REQUISITE_LIST_PRESET_NAME_FIELD"] = "Nome do modelo";
$MESS["CRM_REQUISITE_LIST_YES"] = "Sim";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Modelo sem título";
?>