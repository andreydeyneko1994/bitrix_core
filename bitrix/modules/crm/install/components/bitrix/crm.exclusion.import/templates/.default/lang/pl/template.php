<?
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_LOAD"] = "Importuj";
$MESS["CRM_EXCLUSION_IMPORT_FORMAT_DESC"] = "Wpisz adresy e-mail i / lub numery telefonów, każdy wpis w nowej linii.<br>Dodatkowo możesz dodawać komentarze: wstaw średnik po e-mailu lub numerze i wpisz treść komentarza. <br>Przykład:<br><br> john@example.com<br>peter@example.com; Peter (dostawa) <br>+0112233445566<br>+01234567890; Ann";
$MESS["CRM_EXCLUSION_IMPORT_LOADING"] = "Wczytywanie";
$MESS["CRM_EXCLUSION_IMPORT_RECIPIENTS"] = "E-maile i/lub numery telefonów";
?>