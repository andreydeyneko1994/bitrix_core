<?
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_LOAD"] = "Importar";
$MESS["CRM_EXCLUSION_IMPORT_FORMAT_DESC"] = "Ingrese el email y/o números de teléfono, cada entrada en una nueva línea.<br>Además, puede agregar comentarios: agregue un punto y coma después de un correo electrónico o número e ingrese el texto del comentario. <br>Ejemplo:<br><br> john@example.com<br>peter@example.com;Peter (enviado)<br>+0112233445566<br>+01234567890;Ann";
$MESS["CRM_EXCLUSION_IMPORT_LOADING"] = "Cargando";
$MESS["CRM_EXCLUSION_IMPORT_RECIPIENTS"] = "Email y / o número de teléfono";
?>