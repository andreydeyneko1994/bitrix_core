<?
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE"] = "Supprimer";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE_CONF"] = "Êtes-vous sûr de vouloir supprimer ce champ ?";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_EDIT"] = "Éditer";
$MESS["CRM_FIELDS_LIST_TITLE_EDIT"] = "Liste des champs : #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>