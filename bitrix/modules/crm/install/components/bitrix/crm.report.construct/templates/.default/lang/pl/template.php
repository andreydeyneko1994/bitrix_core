<?
$MESS["CRM_FF_CANCEL"] = "Anuluj";
$MESS["CRM_FF_CHANGE"] = "Edytuj";
$MESS["CRM_FF_CHOISE"] = "Wybierz";
$MESS["CRM_FF_CLOSE"] = "Zamknij";
$MESS["CRM_FF_COMPANY"] = "Firmy";
$MESS["CRM_FF_CONTACT"] = "Kontakty";
$MESS["CRM_FF_DEAL"] = "Deale";
$MESS["CRM_FF_LAST"] = "Ostatnie";
$MESS["CRM_FF_LEAD"] = "Leady";
$MESS["CRM_FF_NO_RESULT"] = "Niestety wyszukiwanie twojego zapytania nie przyniosło rezultatów.";
$MESS["CRM_FF_OK"] = "Wybierz";
$MESS["CRM_FF_SEARCH"] = "Szukaj";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CONTINUE"] = "Następny ...";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Wszystkie";
$MESS["CRM_REPORT_SELECT_OWNER"] = "Wybierz pola dla nowego raportu";
$MESS["REPORT_CHOOSE"] = "Wybierz";
$MESS["REPORT_IGNORE_FILTER_VALUE"] = "Ignoruj";
$MESS["REPORT_POPUP_COLUMN_TITLE"] = "Deal";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM"] = "Deal";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM_PRODUCT_ROW"] = "Produkt";
?>