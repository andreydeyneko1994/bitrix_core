<?
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_CHOISE"] = "Seleccionado";
$MESS["CRM_FF_CLOSE"] = "Cerrar";
$MESS["CRM_FF_COMPANY"] = "Compañías";
$MESS["CRM_FF_CONTACT"] = "Contactos";
$MESS["CRM_FF_DEAL"] = "Negociaciones";
$MESS["CRM_FF_LAST"] = "Prospectos";
$MESS["CRM_FF_LEAD"] = "Prospectos";
$MESS["CRM_FF_NO_RESULT"] = "Lamentablemente, su solicitud de búsqueda no ha obtenido resultados.";
$MESS["CRM_FF_OK"] = "Seleccionado";
$MESS["CRM_FF_SEARCH"] = "Búsqueda";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CONTINUE"] = "Siguiente...";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Todos";
$MESS["CRM_REPORT_SELECT_OWNER"] = "Seleccione los campos para el nuevo informe";
$MESS["REPORT_CHOOSE"] = "Seleccionado";
$MESS["REPORT_IGNORE_FILTER_VALUE"] = "Ignorar";
$MESS["REPORT_POPUP_COLUMN_TITLE"] = "Negociaciones";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM"] = "Negociaciones";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM_PRODUCT_ROW"] = "Productos";
?>