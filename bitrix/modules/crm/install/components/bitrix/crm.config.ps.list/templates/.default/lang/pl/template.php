<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_PS_DELETE"] = "Usuń";
$MESS["CRM_PS_DELETE_CONFIRM_TITLE"] = "Jesteś pewien, że chcesz usunąć ten system płatności?";
$MESS["CRM_PS_DELETE_TITLE"] = "Usuń system płatności";
$MESS["CRM_PS_EDIT"] = "Edytuj";
$MESS["CRM_PS_EDIT_TITLE"] = "Otwórz system płatności dla edycji";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_OFF"] = "wyłączone";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_ON"] = "włączone";
$MESS["CRM_PS_LIST_PS_ACTIVE_OFF"] = "System płatności jest nieaktywny";
$MESS["CRM_PS_LIST_PS_ACTIVE_ON"] = "System płatności jest aktywny";
$MESS["CRM_PS_LIST_PS_CREATE"] = "Utwórz system płatności";
$MESS["CRM_PS_LIST_PS_CREATE_DESC"] = "Kliknij w okno aby utworzyć nowy system płatności";
$MESS["CRM_PS_LIST_TITLE"] = "System płatności";
?>