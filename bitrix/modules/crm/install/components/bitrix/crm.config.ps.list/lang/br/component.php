<?php
$MESS["CRM_COLUMN_ACTIVE"] = "Ativo";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Tipo de cliente";
$MESS["CRM_COLUMN_SORT"] = "Classificação";
$MESS["CRM_COMPANY_PT"] = "Empresa";
$MESS["CRM_CONTACT_PT"] = "Contato";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Erro ao deletar o sistema de pagamento.";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2"] = "Usado para pagamentos online";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_BILL"] = "Usado para formulário de impressão de fatura";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_QUOTE"] = "Usado para formulário de impressão de orçamento";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "Sem limites para valor do pagamento";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Limitações de retorno dependem do método de pagamento";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar o sistema de pagamento.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "O módulo e-Store não está instalado.";
