<?php
$MESS["CRM_COLUMN_ACTIVE"] = "Activo";
$MESS["CRM_COLUMN_NAME"] = "Nombre";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Tipo de cliente";
$MESS["CRM_COLUMN_SORT"] = "Clasificar";
$MESS["CRM_COMPANY_PT"] = "Compañía";
$MESS["CRM_CONTACT_PT"] = "Contacto";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Error al eliminar el sistema de pago.";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2"] = "Utilizado para pagos en línea";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_BILL"] = "Utilizado para el formulario de impresión de facturas";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_QUOTE"] = "Se utiliza para el formulario de impresión de cotización";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "No hay límites para la cantidad de pago";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Límite de devolución dependen del método de pago";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Error al actualizar el sistema de pago.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
