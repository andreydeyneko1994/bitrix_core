<?
$MESS["CRM_PS_REQUISITES_TRANSFER_COMPLETED_SUMMARY"] = "Fin de la migration des options de paiement aux entreprises.";
$MESS["CRM_PS_REQUISITES_TRANSFER_NOT_REQUIRED_SUMMARY"] = "Les mentions du vendeur ne doivent pas être migrées des options de paiement aux entreprises.";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY1"] = "Modèles de mentions ajoutés ...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY2"] = "Entreprises et mentions créées...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY3"] = "Factures traitées : #PROCESSED_ITEMS# de #TOTAL_ITEMS#...";
?>