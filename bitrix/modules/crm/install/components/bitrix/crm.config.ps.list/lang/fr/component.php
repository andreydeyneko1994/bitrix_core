<?php
$MESS["CRM_COLUMN_ACTIVE"] = "Actif(ve)";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Type du client";
$MESS["CRM_COLUMN_SORT"] = "Trier";
$MESS["CRM_COMPANY_PT"] = "Entreprise";
$MESS["CRM_CONTACT_PT"] = "Contact";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Erreur survenue au cours de la suppression du système de paiement.";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2"] = "Utilisé pour les paiements en ligne";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_BILL"] = "Utilisé pour les formulaires d'impression des factures";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_QUOTE"] = "Utilisé pour le formulaire d'impression du devis";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "Aucune limite de montant du paiement";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Les limitations de retour dépendent du mode de paiement";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Lors de mise à jour du système de paiement, une erreur s'est produite.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module Boutique en ligne n'est pas installé.";
