<?php
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_CSRF_ERROR"] = "Błędne ID sesji";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_MODULE_ERROR"] = "Nie wszystkie niezbędne moduły zostały znalezione";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_PROCESSED_NO_PDF_ERROR"] = "Błąd podczas konwertowania dokumentu do formatu PDF";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_SMS_TEXT"] = "Użyj odnośnika #LINK#, aby pobrać #TITLE#";
