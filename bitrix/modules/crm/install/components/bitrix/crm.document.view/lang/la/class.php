<?php
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_CSRF_ERROR"] = "ID de sesión inválido";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_MODULE_ERROR"] = "No se encontraron todos los módulos necesarios";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_PROCESSED_NO_PDF_ERROR"] = "Error al convertir el documento a PDF";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_SMS_TEXT"] = "Usar el enlace #LINK# descargar #TITLE#";
