<?php
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_CSRF_ERROR"] = "ID da sessão inválido";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_MODULE_ERROR"] = "Nem todos os módulos necessários foram encontrados";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_PROCESSED_NO_PDF_ERROR"] = "Erro ao converter o documento para PDF ";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_SMS_TEXT"] = "Use o link #LINK# para fazer o download de #TITLE#";
