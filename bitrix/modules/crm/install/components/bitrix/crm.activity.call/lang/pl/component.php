<?
$MESS["CRM_ACTIVITY_CALL_VI_CALLBACK_CALL"] = "Oddzwonienie";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_CALL"] = "Połączenie przychodzące";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_REDIRECT_CALL"] = "Połączenie przychodzące przekierowane";
$MESS["CRM_ACTIVITY_CALL_VI_OUTGOING_CALL"] = "Połączenie wychodzące";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "Moduł Telefonia nie jest zainstalowany.";
?>