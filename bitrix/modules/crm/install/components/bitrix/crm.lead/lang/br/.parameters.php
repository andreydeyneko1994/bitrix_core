<?
$MESS["CRM_ELEMENT_ID"] = "ID do Lead";
$MESS["CRM_LEAD_VAR"] = "Nome variável do ID do Lead";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
$MESS["CRM_SEF_PATH_TO_CONVERT"] = "Modelo de Caminho da Página de Conversão de Leads";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modelo de Caminho da Página do Editor de Leads";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modelo de Caminho da Página de Importação de Leads";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modelo de Caminho da Página de Índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modelo de Caminho da Página de Leads";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Modelo de Caminho da Página de Serviço Web";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modelo de Caminho da Página de Exibição do Lead";
?>