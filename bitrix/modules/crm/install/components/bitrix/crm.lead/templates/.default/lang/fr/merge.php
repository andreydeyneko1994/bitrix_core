<?
$MESS["CRM_LEAD_MERGE_HEADER_TEMPLATE"] = "Date de création : #DATE_CREATE#";
$MESS["CRM_LEAD_MERGE_PAGE_TITLE"] = "Fusionner les transactions";
$MESS["CRM_LEAD_MERGE_RESULT_LEGEND"] = "Sélectionnez le prospect prioritaire dans la liste. Il sera utilisé comme base pour le profil de prospect. Vous pouvez lui ajouter plus de données à partir d'autres prospects.";
?>