<?
$MESS["CRM_LEAD_MERGE_HEADER_TEMPLATE"] = "Criado em #DATE_CREATE#";
$MESS["CRM_LEAD_MERGE_PAGE_TITLE"] = "Mesclar leads";
$MESS["CRM_LEAD_MERGE_RESULT_LEGEND"] = "Selecione o lead prioritário na lista. Ele será usado como base para o perfil de lead. Você pode adicionar mais dados de outros leads.";
?>