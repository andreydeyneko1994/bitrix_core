<?
$MESS["CRM_QUOTE_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se a página atual ainda estiver sendo exibida, feche-a manualmente.";
$MESS["CRM_QUOTE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "O orçamento <a href='#URL#'>#TITLE#</a> foi criado. Redirecionando para a página anterior. Se esta página ainda estiver sendo exibida, feche-a manualmente.";
?>