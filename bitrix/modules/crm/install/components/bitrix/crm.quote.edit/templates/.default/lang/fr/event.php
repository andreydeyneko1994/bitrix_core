<?
$MESS["CRM_QUOTE_EDIT_EVENT_CANCELED"] = "Une action a été annulée. Vous êtes maintenant redirigé vers la page précédente. Si la page actuelle est toujours affichée, veuillez la fermer manuellement.";
$MESS["CRM_QUOTE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Le devis <a href='#URL#'>#TITLE#</a> a été créé. Vous allez maintenant être redirigé vers la page précédente. Si cette page est encore affichée, veuillez la fermer manuellement.";
?>