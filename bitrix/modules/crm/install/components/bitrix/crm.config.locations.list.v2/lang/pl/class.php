<?
$MESS["CRM_CLL2_CRM_MODULE_NOT_INSTALL"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_CLL2_IMPORT_ERROR"] = "Procedura importu lokalizacji się nie powiodła. Zaimportuj lokalizację ponownie.";
$MESS["CRM_CLL2_INTS_TASKS_NAV"] = "Wyniki";
$MESS["CRM_CLL2_LOC_DELETION_GENERAL_ERROR"] = "Błąd usuwania lokalizacji.";
$MESS["CRM_CLL2_LOC_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji lokalizacji.";
$MESS["CRM_CLL2_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_CLL2_SALE_MODULE_NOT_INSTALL"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>