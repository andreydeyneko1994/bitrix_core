<?
$MESS["CRM_CLL2_ALL"] = "Razem";
$MESS["CRM_CLL2_DELETE"] = "Usuń lokalizację";
$MESS["CRM_CLL2_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_CLL2_DELETE_TITLE"] = "Usuń tę lokalizację";
$MESS["CRM_CLL2_EDIT"] = "Edytuj lokalizację";
$MESS["CRM_CLL2_EDIT_TITLE"] = "Otwórz tę lokalizację do edycji";
$MESS["CRM_CLL2_NOT_SELECTED"] = "Nie wybrano";
$MESS["CRM_CLL2_VIEW_SUBTREE"] = "Wyświetl podrzędne lokalizacje";
$MESS["CRM_CLL2_VIEW_SUBTREE_TITLE"] = "Wyświetl podrzędne lokalizacje";
?>