<?
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_1"] = "Możesz odtworzyć film ponownie klikając na tę ikonę.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_2"] = "Podłącz więcej kanałów komunikacji do Twojego CRM Bitrix24: telefoniczny, E-mail, Otwarte Kanały, formularze CRM.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_EMPLOYEE"] = "Użyj swojego spersonalizowanego licznika, by mierzyć własne postępy: CRM sprawdza ilość ukończonych i pozostałych zadań w danym dniu. Twoim celem jest ukończenie wszystkich zadań przed zakończeniem dnia.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_SUPERVISOR"] = "Użyj liczników, by sprawdzić bieżące postępy Twoich pracowników: ukończone i pozostałe zadania na ten dzień. Twoi pracownicy pracują lepiej, jeśli na koniec dnia nie ma nieukończonych zadań.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_4"] = "Możesz w każdej chwili włączyć widok demo.";
$MESS["CRM_CH_TRACKER_WGT_DEMO_TITLE"] = "To jest widok demo. Ukryj go, aby analizować własne dane.";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET"] = "Cel sprzedażowy";
?>