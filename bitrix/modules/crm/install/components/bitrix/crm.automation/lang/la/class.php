<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Proceso de negocio no está instalado.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "Se denegó el acceso a la entidad.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "La automatización no está disponible";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "La automatización no está disponible en el modo CRM simple";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "El componente no admite esta entidad en el CRM.";
$MESS["CRM_AUTOMATION_WEBHOOK_CREATE_FAILURE"] = "No se puede crear un webhook entrante";
$MESS["CRM_AUTOMATION_WEBHOOK_NOT_AVAILABLE"] = "No se pueden crear webhooks entrantes en su plan actual";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
