<?
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "O módulo Calendário de Eventos não está instalado.";
$MESS["CRM_ACTIVITY_PLANNER_IMPORTANT_SLIDER"] = "Importante";
$MESS["CRM_ACTIVITY_PLANNER_NO_ACTIVITY"] = "A atividade não foi encontrada.";
$MESS["CRM_ACTIVITY_PLANNER_NO_PROVIDER"] = "O provedor de atividades não foi encontrado.";
$MESS["CRM_ACTIVITY_PLANNER_NO_READ_PERMISSION"] = "Permissão insuficiente para visualizar dados da atividade.";
$MESS["CRM_ACTIVITY_PLANNER_NO_UPDATE_PERMISSION"] = "Permissão insuficiente para editar dados da atividade.";
$MESS["CRM_ACT_EMAIL_REPLY_ADD_DOCS"] = "Alterar negócio";
$MESS["CRM_ACT_EMAIL_REPLY_SET_DOCS"] = "Selecionar negócio";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
?>