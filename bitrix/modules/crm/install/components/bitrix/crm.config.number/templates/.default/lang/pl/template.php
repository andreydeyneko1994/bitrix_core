<?
$MESS["CRM_NUMBER_DATE"] = "Okres:";
$MESS["CRM_NUMBER_DATE_1"] = "Codziennie";
$MESS["CRM_NUMBER_DATE_2"] = "Co miesiąc";
$MESS["CRM_NUMBER_DATE_3"] = "Co rok";
$MESS["CRM_NUMBER_NUMBER"] = "Początkowy numer: ";
$MESS["CRM_NUMBER_NUMBER_DESC"] = "1 do 7 znaków. Nowa wartość musi być większa niż poprzednia.";
$MESS["CRM_NUMBER_PREFIX"] = "Prefiks:";
$MESS["CRM_NUMBER_PREFIX_DESC"] = "1 do 7 znaków (użyj liter łacińskich, liczb, znaku minus i podkreśleń). Na przykład z prefiksem: TEST_1234";
$MESS["CRM_NUMBER_RANDOM"] = "Liczba znaków:";
$MESS["CRM_NUMBER_TEMPL"] = "Szablon:";
$MESS["CRM_NUMBER_TEMPLATE_EXAMPLE"] = "Przykład: ";
$MESS["CRM_NUMBER_WARNING"] = "Prefiks:";
?>