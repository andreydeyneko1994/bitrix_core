<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_NUMBER_TEMPLATE_0"] = "Non utilisé";
$MESS["CRM_NUMBER_TEMPLATE_1"] = "Numérotation à partir d'un nombre bien déterminé";
$MESS["CRM_NUMBER_TEMPLATE_2"] = "Préfixe devant le numéro";
$MESS["CRM_NUMBER_TEMPLATE_3"] = "Numéro unique aléatoire";
$MESS["CRM_NUMBER_TEMPLATE_4"] = "ID d'utilisateur et numéro";
$MESS["CRM_NUMBER_TEMPLATE_5"] = "Numération au cours d'une période définie";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit.";
?>