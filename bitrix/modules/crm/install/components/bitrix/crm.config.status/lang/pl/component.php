<?
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Typy";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD"] = "Nie można usunąć #field# pola, ponieważ jest używane w aktualnych elementach.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD_MANY"] = "Nie można usunąć pól #field# ponieważ są używane w aktualnych elementach.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_STATUS_ADD_DEAL_STAGE"] = "Dodaj etap";
$MESS["CRM_STATUS_ADD_INVOICE_STATUS"] = "Dodaj status";
$MESS["CRM_STATUS_ADD_QUOTE_STATUS"] = "Dodaj status";
$MESS["CRM_STATUS_ADD_STATUS"] = "Dodaj status";
$MESS["CRM_STATUS_DEFAULT_NAME_DEAL_STAGE"] = "Nowy etap";
$MESS["CRM_STATUS_DEFAULT_NAME_INVOICE_STATUS"] = "Nowy status";
$MESS["CRM_STATUS_DEFAULT_NAME_QUOTE_STATUS"] = "Nowy status";
$MESS["CRM_STATUS_DEFAULT_NAME_STATUS"] = "Nowy status";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_DEAL_STAGE"] = "Jesteś pewien, że chcesz usunąć etap? Może on być używany w aktywnych elementach.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_INVOICE_STATUS"] = "Jesteś pewien, że chcesz usunąć status? Może on być używany w aktywnych elementach.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_QUOTE_STATUS"] = "Jesteś pewien, że chcesz usunąć status? Może on być używany w aktywnych elementach.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_STATUS"] = "Jesteś pewien, że chcesz usunąć status? Może on być używany w aktywnych elementach.";
?>