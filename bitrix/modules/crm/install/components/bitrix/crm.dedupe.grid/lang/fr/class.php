<?php
$MESS["CRM_DEDUPE_GRID_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_GRID_COL_MATCH"] = "Correspondance";
$MESS["CRM_DEDUPE_GRID_COL_ORGANIZATION"] = "Nom de l'entreprise";
$MESS["CRM_DEDUPE_GRID_COL_PERSON"] = "Nom complet";
$MESS["CRM_DEDUPE_GRID_COL_PHONE"] = "Téléphone";
$MESS["CRM_DEDUPE_GRID_COL_RESPONSIBLE"] = "Responsable";
$MESS["CRM_DEDUPE_GRID_COMPANY_PAGE_TITLE"] = "Doublons d'entreprise";
$MESS["CRM_DEDUPE_GRID_CONTACT_PAGE_TITLE"] = "Doublons de contact";
$MESS["CRM_DEDUPE_GRID_ENTITY_TYPE_NOT_DEFINED"] = "Le type d'entité n'est pas spécifié.";
$MESS["CRM_DEDUPE_GRID_INVALID_ENTITY_TYPE"] = "Le type '#TYPE_NAME#' n'est pas pris en charge dans ce contexte.";
$MESS["CRM_DEDUPE_GRID_LEAD_PAGE_TITLE"] = "Doublons de transaction";
$MESS["CRM_DEDUPE_GRID_PAGE_TITLE"] = "Coïncidences retrouvées";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
