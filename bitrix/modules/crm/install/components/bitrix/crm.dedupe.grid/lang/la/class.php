<?
$MESS["CRM_DEDUPE_GRID_COL_EMAIL"] = "Correo electrónico";
$MESS["CRM_DEDUPE_GRID_COL_MATCH"] = "Coincidencia";
$MESS["CRM_DEDUPE_GRID_COL_ORGANIZATION"] = "Nombre de la compañía";
$MESS["CRM_DEDUPE_GRID_COL_PERSON"] = "Nombre completo";
$MESS["CRM_DEDUPE_GRID_COL_PHONE"] = "Teléfono";
$MESS["CRM_DEDUPE_GRID_COL_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_DEDUPE_GRID_COMPANY_PAGE_TITLE"] = "Compañías duplicadas";
$MESS["CRM_DEDUPE_GRID_CONTACT_PAGE_TITLE"] = "Contactos duplicados";
$MESS["CRM_DEDUPE_GRID_ENTITY_TYPE_NOT_DEFINED"] = "El tipo de entidad no está especificado.";
$MESS["CRM_DEDUPE_GRID_INVALID_ENTITY_TYPE"] = "El tipo '#TYPE_NAME#' no es compatible en este contexto.";
$MESS["CRM_DEDUPE_GRID_LEAD_PAGE_TITLE"] = "Prospectos duplicados";
$MESS["CRM_DEDUPE_GRID_PAGE_TITLE"] = "Duplicados encontrados";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
?>