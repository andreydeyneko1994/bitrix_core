<?
$MESS["CRM_DEDUPE_GRID_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_GRID_COL_MATCH"] = "Resultado";
$MESS["CRM_DEDUPE_GRID_COL_ORGANIZATION"] = "Nome da empresa";
$MESS["CRM_DEDUPE_GRID_COL_PERSON"] = "Nome completo";
$MESS["CRM_DEDUPE_GRID_COL_PHONE"] = "Telefone";
$MESS["CRM_DEDUPE_GRID_COL_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_DEDUPE_GRID_COMPANY_PAGE_TITLE"] = "Duplicados de empresa";
$MESS["CRM_DEDUPE_GRID_CONTACT_PAGE_TITLE"] = "Duplicados de contato";
$MESS["CRM_DEDUPE_GRID_ENTITY_TYPE_NOT_DEFINED"] = "O tipo de entidade não está especificado.";
$MESS["CRM_DEDUPE_GRID_INVALID_ENTITY_TYPE"] = "O tipo '#TYPE_NAME#' não está disponível neste contexto.";
$MESS["CRM_DEDUPE_GRID_LEAD_PAGE_TITLE"] = "Duplicados de leads";
$MESS["CRM_DEDUPE_GRID_PAGE_TITLE"] = "Duplicados encontrados";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
?>