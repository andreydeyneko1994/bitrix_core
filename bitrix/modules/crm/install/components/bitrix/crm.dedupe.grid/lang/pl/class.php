<?
$MESS["CRM_DEDUPE_GRID_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_GRID_COL_MATCH"] = "Dopasowanie";
$MESS["CRM_DEDUPE_GRID_COL_ORGANIZATION"] = "Nazwa firmy";
$MESS["CRM_DEDUPE_GRID_COL_PERSON"] = "Imię i nazwisko";
$MESS["CRM_DEDUPE_GRID_COL_PHONE"] = "Telefon";
$MESS["CRM_DEDUPE_GRID_COL_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_DEDUPE_GRID_COMPANY_PAGE_TITLE"] = "Duplikaty firmy";
$MESS["CRM_DEDUPE_GRID_CONTACT_PAGE_TITLE"] = "Duplikaty kontaktów";
$MESS["CRM_DEDUPE_GRID_ENTITY_TYPE_NOT_DEFINED"] = "Nie określono typu jednostki.";
$MESS["CRM_DEDUPE_GRID_INVALID_ENTITY_TYPE"] = "Typ '#TYPE_NAME#' nie jest obsługiwany w tym kontekście.";
$MESS["CRM_DEDUPE_GRID_LEAD_PAGE_TITLE"] = "Duplikaty leadów";
$MESS["CRM_DEDUPE_GRID_PAGE_TITLE"] = "Znaleziono dopasowania";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>