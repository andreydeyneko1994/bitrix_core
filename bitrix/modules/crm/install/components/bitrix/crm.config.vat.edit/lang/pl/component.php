<?
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_VAT_ADD_UNKNOWN_ERROR"] = "Błąd tworzenia stawki VAT.";
$MESS["CRM_VAT_DELETE_UNKNOWN_ERROR"] = "Błąd usuwania stawki VAT.";
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Aktywne";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Sortuj";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_VAT_FIELD_NAME"] = "Nazwa";
$MESS["CRM_VAT_FIELD_RATE"] = "Stawka";
$MESS["CRM_VAT_NOT_FOUND"] = "Nie znaleziono stawki VAT.";
$MESS["CRM_VAT_SECTION_MAIN"] = "Stawka VAT";
$MESS["CRM_VAT_UPDATE_UNKNOWN_ERROR"] = "Błąd aktualizacji stawki VAT.";
?>