<?
$MESS["CRM_IMPORT_AGAIN"] = "Importuj kolejny plik";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Kliknij, aby zaimportować kolejny plik";
$MESS["CRM_IMPORT_CANCEL"] = "Anuluj";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Anuluj i wróć do produktów";
$MESS["CRM_IMPORT_DONE"] = "Gotowe";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Przejdź do produktów";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Następna >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Przejdź do następnego kroku";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Powrót";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Przejdź do poprzedniego kroku";
$MESS["CRM_TAB_1"] = "Ustawienia Importu";
$MESS["CRM_TAB_1_TITLE"] = "Edytuj ustawienia importu";
$MESS["CRM_TAB_2"] = "Pola";
$MESS["CRM_TAB_2_TITLE"] = "Skonfiguruj mapowanie pól";
$MESS["CRM_TAB_3"] = "Importuj";
$MESS["CRM_TAB_3_TITLE"] = "Importuj wynik";
?>