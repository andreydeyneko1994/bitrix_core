<?
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócios Designer não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_BP_COMPANY"] = "Empresa";
$MESS["CRM_BP_CONTACT"] = "Contato";
$MESS["CRM_BP_DEAL"] = "Negócio";
$MESS["CRM_BP_ENTITY_LIST"] = "Tipos";
$MESS["CRM_BP_INVOICE"] = "Fatura";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Modelos: #NAME#";
$MESS["CRM_BP_ORDER"] = "Pedido";
$MESS["CRM_BP_WFEDIT_TITLE_ADD"] = "Novo modelo de fluxo de trabalho para #NAME#";
$MESS["CRM_BP_WFEDIT_TITLE_EDIT"] = "Editar o modelo \"#TEMPLATE#\" do fluxo de trabalho #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
?>