<?
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "Indeks wyszukiwania ofert został odtworzony. Przetworzone oferty: #PROCESSED_ITEMS#.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "Indeks wyszukiwania ofert nie wymaga odtworzenia.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Przetworzonych ofert: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_QUOTE_LIST_ROW_COUNT"] = "Suma: #ROW_COUNT#";
?>