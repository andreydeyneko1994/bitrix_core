<?
$MESS["CRM_CONTACT_INFO_EMAIL"] = "E-mail";
$MESS["CRM_CONTACT_INFO_IM"] = "Komunikator";
$MESS["CRM_CONTACT_INFO_PHONE"] = "Telefon";
$MESS["CRM_CONTACT_INFO_WEB"] = "Strona internetowa";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Osoba odpowiedzialna";
$MESS["CRM_FIELD_CONTACT_FULL_NAME"] = "Kontakt";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Okazja";
$MESS["CRM_FIELD_STATUS"] = "Status";
$MESS["CRM_TITLE_LEAD"] = "Lead";
?>