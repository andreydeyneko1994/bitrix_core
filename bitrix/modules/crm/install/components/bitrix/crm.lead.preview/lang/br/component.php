<?
$MESS["CRM_CONTACT_INFO_EMAIL"] = "E-mail";
$MESS["CRM_CONTACT_INFO_IM"] = "Messenger";
$MESS["CRM_CONTACT_INFO_PHONE"] = "Telefone";
$MESS["CRM_CONTACT_INFO_WEB"] = "Site";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Pessoa responsável";
$MESS["CRM_FIELD_CONTACT_FULL_NAME"] = "Contato";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Oportunidade";
$MESS["CRM_FIELD_STATUS"] = "Status";
$MESS["CRM_TITLE_LEAD"] = "Lead";
?>