<?
$MESS["CRM_CONTACT_INFO_EMAIL"] = "E-mail";
$MESS["CRM_CONTACT_INFO_IM"] = "Messenger";
$MESS["CRM_CONTACT_INFO_PHONE"] = "Teléfono";
$MESS["CRM_CONTACT_INFO_WEB"] = "Sitio Web";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Persona responsable";
$MESS["CRM_FIELD_CONTACT_FULL_NAME"] = "Contacto";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Presupuesto expectativo";
$MESS["CRM_FIELD_STATUS"] = "Estados";
$MESS["CRM_TITLE_LEAD"] = "Prospecto";
?>