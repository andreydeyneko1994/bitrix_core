<?
$MESS["CRM_DEAL_BIZPROC_LIST"] = "Proces Biznesowy";
$MESS["CRM_DEAL_CLIENT"] = "Klient";
$MESS["CRM_DEAL_CLIENT_EMAIL"] = "E-mail";
$MESS["CRM_DEAL_CLIENT_NOT_ASSIGNED"] = "[nie przypisane]";
$MESS["CRM_DEAL_CLIENT_PHONE"] = "Telefon";
$MESS["CRM_DEAL_COMMENT"] = "Komentarz";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Niewystarczające uprawnienia do stworzenia faktury i oferty.";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Kontynuuj";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wybierz obiekty, w których będą stworzone dodatkowe pola";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Następujące pola będą stworzone";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "Wybrane obiekty nie posiadają pól, do których można przekazać dane.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Utwórz element z deala";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Błąd ogólny.";
$MESS["CRM_DEAL_CREATOR"] = "Utworzony przez";
$MESS["CRM_DEAL_DATE_CREATE"] = "Utworzony";
$MESS["CRM_DEAL_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_DEAL_DURATION"] = "Data";
$MESS["CRM_DEAL_DURATION_FROM"] = "od";
$MESS["CRM_DEAL_DURATION_TO"] = "przez";
$MESS["CRM_DEAL_EVENT_LIST"] = "Ostatnie zmiany";
$MESS["CRM_DEAL_GET_USER_INFO_GENERAL_ERROR"] = "Nie można odzyskać informacji użytkownika.";
$MESS["CRM_DEAL_MODIFIER"] = "Edytowane przez";
$MESS["CRM_DEAL_NOT_OPENED"] = "Ten deal nie jest publiczny.";
$MESS["CRM_DEAL_OPENED"] = "Umowa ta ma charakter publiczny i może być oglądana przez innych.";
$MESS["CRM_DEAL_OPPORTUNITY"] = "Liczba możliwości";
$MESS["CRM_DEAL_OPPORTUNITY_SHORT"] = "Razem";
$MESS["CRM_DEAL_PROBABILITY"] = "Prawdopodobieństwo";
$MESS["CRM_DEAL_RECUR_SHOW_TITLE"] = "Szablon dealu ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_DEAL_RESPONSIBLE_CHANGE"] = "zmień";
$MESS["CRM_DEAL_SHOW_LEGEND"] = "deal ##ID#";
$MESS["CRM_DEAL_SHOW_LEGEND_EXTERNAL"] = "Zamówienie sklepu internetowego";
$MESS["CRM_DEAL_SHOW_NAVIGATION_NEXT"] = "Następny";
$MESS["CRM_DEAL_SHOW_NAVIGATION_PREV"] = "Poprzednie";
$MESS["CRM_DEAL_SHOW_TITLE"] = "Deal ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_SIDEBAR_STAGE"] = "Etap";
$MESS["CRM_DEAL_SIDEBAR_TITLE"] = "Informacje deala";
$MESS["CRM_DEAL_STAGE_UNDEF"] = "[nie ustawiony]";
$MESS["CRM_DEAL_TYPE"] = "Typ deala";
$MESS["CRM_DEAL_TYPE_UNDEF"] = "[nie przypisane]";
$MESS["CRM_EDIT_BTN_TTL"] = "Kliknij, aby edytować";
$MESS["CRM_LOCK_BTN_TTL"] = "Nie można edytować tego elementu";
$MESS["CRM_TAB_1"] = "Deal";
$MESS["CRM_TAB_1_TITLE"] = "Właściwości deala";
$MESS["CRM_TAB_2"] = "Kontakty";
$MESS["CRM_TAB_2_TITLE"] = "Kontakty deala";
$MESS["CRM_TAB_3"] = "Firmy";
$MESS["CRM_TAB_3_TITLE"] = "Firmy deala";
$MESS["CRM_TAB_4"] = "Leady";
$MESS["CRM_TAB_4_TITLE"] = "Leady deala";
$MESS["CRM_TAB_5"] = "Zapis";
$MESS["CRM_TAB_5_TITLE"] = "Dziennik Deala";
$MESS["CRM_TAB_6"] = "Aktywność";
$MESS["CRM_TAB_6_TITLE"] = "Aktywność deala";
$MESS["CRM_TAB_7"] = "Proces Biznesowy";
$MESS["CRM_TAB_7_TITLE"] = "Proces biznesowy deala";
$MESS["CRM_TAB_8"] = "Faktura";
$MESS["CRM_TAB_8_TITLE"] = "Faktura deala";
$MESS["CRM_TAB_8_TITLE_V2"] = "Faktury deali";
$MESS["CRM_TAB_8_V2"] = "Faktury";
$MESS["CRM_TAB_9"] = "Oferty";
$MESS["CRM_TAB_9_TITLE"] = "Oferty deala";
$MESS["CRM_TAB_AUTOMATION"] = "Automatyzacja";
$MESS["CRM_TAB_AUTOMATION_TITLE"] = "Automatyzacja. Reguły i wyzwalacze";
$MESS["CRM_TAB_DETAILS"] = "Szczegóły";
$MESS["CRM_TAB_DETAILS_TITLE"] = "Szczegóły";
$MESS["CRM_TAB_EVENT"] = "Dziennik Zmian";
$MESS["CRM_TAB_EVENT_TITLE"] = "Dziennik zmian deala";
$MESS["CRM_TAB_HISTORY"] = "Historia";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Dziennik zmian deala";
$MESS["CRM_TAB_LIVE_FEED"] = "Tablica";
$MESS["CRM_TAB_LIVE_FEED_TITLE"] = "Tablica";
$MESS["CRM_TAB_PRODUCT_ROWS"] = "Produkty";
$MESS["CRM_TAB_PRODUCT_ROWS_TITLE"] = "Produkty deala";
$MESS["CRM_TAB_TREE"] = "Zależności";
$MESS["CRM_TAB_TREE_TITLE"] = "Odnośniki do innych elementów i pozycji";
?>