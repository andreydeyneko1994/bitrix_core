<?
$MESS["CIPC_EMPTY_PAY_SYSTEM"] = "Nie wybrano systemu płatności";
$MESS["CIPC_ERROR_PAYMENT_EXECUTION"] = "Błąd realizacji płatności";
$MESS["CIPC_TITLE_COMPONENT"] = "Płatność za fakturę";
$MESS["CIPC_WRONG_ACCOUNT_NUMBER"] = "Nie znaleziono faktury o podanym numerze";
$MESS["CIPC_WRONG_LINK"] = "Nie znaleziono wpisów.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>