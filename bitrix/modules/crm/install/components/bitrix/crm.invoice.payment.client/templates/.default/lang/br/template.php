<?
$MESS["CIPC_TPL_BANK_PROPS"] = "Detalhes do pagamento";
$MESS["CIPC_TPL_BITRIX_SIGN"] = "Fornecido por #LOGO#, o CRM gratuito";
$MESS["CIPC_TPL_BUTTON_LOAD"] = "Baixar";
$MESS["CIPC_TPL_BUTTON_PRINT"] = "Imprimir";
$MESS["CIPC_TPL_PAID"] = "Pago";
$MESS["CIPC_TPL_PAID_DATE"] = "Data de pagamento";
$MESS["CIPC_TPL_PAID_SUM"] = "Valor pago";
$MESS["CIPC_TPL_PAID_SYSTEM"] = "Pago com";
$MESS["CIPC_TPL_PAID_TITLE"] = "Fatura ##INVOICE_ID# de #DATE_BILL#";
$MESS["CIPC_TPL_PAY_FOR"] = "Pagar usando";
$MESS["CIPC_TPL_PAY_SYSTEM_BLOCK_HEADER"] = "Exibir sistemas de pagamento";
$MESS["CIPC_TPL_RETURN_LIST"] = "Voltar às opções de pagamento";
$MESS["CIPC_TPL_SUM_PAYMENT"] = "Valor pagável";
?>