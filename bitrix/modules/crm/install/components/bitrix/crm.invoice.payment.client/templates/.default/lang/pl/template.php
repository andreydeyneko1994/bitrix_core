<?
$MESS["CIPC_TPL_BANK_PROPS"] = "Ustawienia płatności";
$MESS["CIPC_TPL_BITRIX_SIGN"] = "Obsługiwane przez #LOGO#, bezpłatny CRM";
$MESS["CIPC_TPL_BUTTON_LOAD"] = "Pobierz";
$MESS["CIPC_TPL_BUTTON_PRINT"] = "Drukuj";
$MESS["CIPC_TPL_PAID"] = "Opłacono";
$MESS["CIPC_TPL_PAID_DATE"] = "Data płatności";
$MESS["CIPC_TPL_PAID_SUM"] = "Zapłacone kwoty";
$MESS["CIPC_TPL_PAID_SYSTEM"] = "Opłacono z";
$MESS["CIPC_TPL_PAID_TITLE"] = "Faktura ##INVOICE_ID# z #DATE_BILL#";
$MESS["CIPC_TPL_PAY_FOR"] = "Płatność za pomocą";
$MESS["CIPC_TPL_PAY_SYSTEM_BLOCK_HEADER"] = "Pokaż systemy płatności";
$MESS["CIPC_TPL_RETURN_LIST"] = "Powrót do opcji płatności";
$MESS["CIPC_TPL_SUM_PAYMENT"] = "Kwota do zapłaty";
?>