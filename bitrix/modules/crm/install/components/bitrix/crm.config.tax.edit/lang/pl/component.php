<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_SALE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_TAX_ADD_UNKNOWN_ERROR"] = "Błąd tworzenia podatku.";
$MESS["CRM_TAX_DELETE_UNKNOWN_ERROR"] = "Błąd usuwania podatku.";
$MESS["CRM_TAX_FIELD_CODE"] = "Kod podatku";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Opis";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_LID"] = "Strona internetowa";
$MESS["CRM_TAX_FIELD_NAME"] = "Nazwa";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Zmodyfikowany";
$MESS["CRM_TAX_NOT_FOUND"] = "Nie znaleziono podateku.";
$MESS["CRM_TAX_RATE_ADD"] = "Dodaj stawkę podatku";
$MESS["CRM_TAX_RATE_ADD_TITLE"] = "Dodaj stawkę podatku";
$MESS["CRM_TAX_RATE_LIST"] = "Stawka podatku";
$MESS["CRM_TAX_SECTION_MAIN"] = "Podatek";
$MESS["CRM_TAX_UPDATE_UNKNOWN_ERROR"] = "Błąd aktualizacji podatku.";
?>