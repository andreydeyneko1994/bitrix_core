<?
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_LOAD_TITLE"] = "Obciążenie komunikacyjne wygenerowane przez firmę";
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_TITLE"] = "Profil firmy";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_LOAD_TITLE"] = "Obciążenie komunikacyjne wygenerowane przez kontakt";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_TITLE"] = "Profil kontaktu";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[niepodpisane]";
?>