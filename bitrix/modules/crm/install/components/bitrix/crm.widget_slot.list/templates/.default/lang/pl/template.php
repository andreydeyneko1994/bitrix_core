<?
$MESS["CRM_WGT_SLST_ADD"] = "Dodaj";
$MESS["CRM_WGT_SLST_ADD_PRODUCT_SUM"] = "Dodaj ilość produktu";
$MESS["CRM_WGT_SLST_BY_DEFALT"] = "Domyślne";
$MESS["CRM_WGT_SLST_CALCEL"] = "Anuluj";
$MESS["CRM_WGT_SLST_DLG_WAIT"] = "Proszę czekać …";
$MESS["CRM_WGT_SLST_EDIT"] = "Edytuj";
$MESS["CRM_WGT_SLST_ERR_FIELD_ALREADY_EXISTS"] = "Proszę wybrać inne pole. Pole \"#FIELD#\" jest już w użyciu.";
$MESS["CRM_WGT_SLST_ERR_FIELD_LIMT_EXCEEDED"] = "Nie można dodać nowego pola, ponieważ osiągnięto limit dla pól.";
$MESS["CRM_WGT_SLST_ERR_NO_FREE_SLOTS"] = "Nie można dodać nowego pola, ponieważ nie ma wolnych miejsc.";
$MESS["CRM_WGT_SLST_ERR_SELECT_FIELD"] = "Proszę wybierz pole, aby kontynuować.";
$MESS["CRM_WGT_SLST_GENERAL_INTRO"] = "Wybierz na tej stronie swoje pola, aby uwzględnić je w raportach analitycznych CRM.";
$MESS["CRM_WGT_SLST_LIMITS_INTRO"] = "Można wybrać do 10 pól; do 5 pól tego samego typu.";
$MESS["CRM_WGT_SLST_LIMITS_TOTALS"] = "Wybrano: #TOTAL# z #OVERALL#";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_CLOSE"] = "Zamknij";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_START"] = "Uruchom";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_STOP"] = "Stop";
$MESS["CRM_WGT_SLST_LRP_DLG_REQUEST_ERR"] = "Błąd przetwarzania żądania.";
$MESS["CRM_WGT_SLST_NODE_TOOLTIP"] = "Jeżeli używasz własnego pola do podsumowania wartości deala, wskaż je tutaj. Wartość produktów może być w tym polu sumowana. Domyślnie raporty korzystają do podsumowywania deali z danych w polach \"Suma\".";
$MESS["CRM_WGT_SLST_NOT_SELECTED"] = "Nie wybrano";
$MESS["CRM_WGT_SLST_REMOVE"] = "Usuń";
$MESS["CRM_WGT_SLST_RESET"] = "Wyzeruj";
$MESS["CRM_WGT_SLST_SAVE"] = "Zapisz";
$MESS["CRM_WGT_SLST_TOTAL_SUM"] = "Suma";
$MESS["CRM_WGT_SLST_USER_FIELDS"] = "Pola dodatkowe";
?>