<?
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Entidad";
$MESS["CRM_MAIL_ENTITY_TYPE2"] = "Unión";
$MESS["CRM_MAIL_ENTITY_TYPE_UNI"] = "No";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Error al crear la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Plantilla";
$MESS["CRM_MAIL_TEMPLATE_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_MAIL_TEMPLATE_DELETE_BTN"] = "Eliminar plantilla";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "¿Está seguro que quiere eliminar esta plantilla de correo electrónico?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Error al eliminar la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "De";
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Activo";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "No se ha encontrado la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_SAVE_BTN"] = "Guardar";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Disponible";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PUBLIC"] = "Disponible para todos";
$MESS["CRM_MAIL_TEMPLATE_SENDER_MENU"] = "Remitente";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Índice de clasificación";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "Asunto";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT_HINT"] = "Ingrese el asunto del mensaje";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Nombre";
$MESS["CRM_MAIL_TEMPLATE_TITLE_HINT"] = "Ingrese su nombre";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Error al actualizar la plantilla de correo electrónico.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
?>