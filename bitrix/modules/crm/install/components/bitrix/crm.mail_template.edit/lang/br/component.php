<?
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Entidade";
$MESS["CRM_MAIL_ENTITY_TYPE2"] = "Obrigatório";
$MESS["CRM_MAIL_ENTITY_TYPE_UNI"] = "Não";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Erro ao criar o Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Modelo";
$MESS["CRM_MAIL_TEMPLATE_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_MAIL_TEMPLATE_DELETE_BTN"] = "Excluir modelo";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Você tem certeza que deseja excluir este modelo de e-mail?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Erro ao criar o Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "De";
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Ativo";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "O modelo de Email não foi encontrado";
$MESS["CRM_MAIL_TEMPLATE_SAVE_BTN"] = "Salvar";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Disponível";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PUBLIC"] = "Disponível para todos";
$MESS["CRM_MAIL_TEMPLATE_SENDER_MENU"] = "Remetente";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Ordenar índice";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "Assunto";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT_HINT"] = "Digite o assunto da mensagem";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Nome";
$MESS["CRM_MAIL_TEMPLATE_TITLE_HINT"] = "Inserir nome";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Erro ao atualizar o Modelo de Email";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
?>