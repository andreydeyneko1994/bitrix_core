<?
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Bloc Highload";
$MESS["CRM_MAIL_ENTITY_TYPE2"] = "Objet cible";
$MESS["CRM_MAIL_ENTITY_TYPE_UNI"] = "Non";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Lors de la création du modèle de messagerie une erreur s'est produite.";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Modèle";
$MESS["CRM_MAIL_TEMPLATE_CANCEL_BTN"] = "Annuler";
$MESS["CRM_MAIL_TEMPLATE_DELETE_BTN"] = "Supprimer le modèle";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Voulez-vous vraiment supprimer ce modèle d'e-mail ?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Une erreur a eu lieu lors de la suppression du modèle postal.";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "De qui";
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Actif(ve)";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "Modèle d'e-mail introuvable.";
$MESS["CRM_MAIL_TEMPLATE_SAVE_BTN"] = "Enregistrer";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Accessible";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PUBLIC"] = "Disponible pour tout le monde";
$MESS["CRM_MAIL_TEMPLATE_SENDER_MENU"] = "Expéditeur";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Classification";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "En-tête";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT_HINT"] = "Saisissez l'objet du message";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Dénomination";
$MESS["CRM_MAIL_TEMPLATE_TITLE_HINT"] = "Saisissez un nom";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Une erreur est survenue lors de la mise à jour du modèle postal.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>