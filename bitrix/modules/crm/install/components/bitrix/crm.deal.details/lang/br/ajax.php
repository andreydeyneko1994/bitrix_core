<?php
$MESS["CRM_DEAL_CONVERSION_ID_NOT_DEFINED"] = "O ID do negócio não foi encontrado.";
$MESS["CRM_DEAL_CONVERSION_NOT_FOUND"] = "O negócio não foi encontrado.";
$MESS["CRM_DEAL_DELETION_ERROR"] = "Erro ao excluir negócio.";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_ERROR"] = "Não foi possível transferir o negócio para o novo pipeline. Um negócio só pode ser transferido depois que todos os fluxos de trabalho ativos tiverem terminado de ser executados. Você pode interromper ou concluir o fluxo de trabalho na guia Processos de Negócios. Além disso, certifique-se de que o negócio que você deseja transferir tenha uma pessoa responsável atribuída a ele e que o estágio do negócio esteja definido.";
$MESS["CRM_DEAL_PRODUCT_ROWS_SAVING_ERROR"] = "Ocorreu um erro ao salvar produtos.";
$MESS["CRM_DEAL_RECURRING_DATE_START_ERROR"] = "A próxima data de criação é anterior à data atual. Altere os parâmetros do negócio recorrente.";
