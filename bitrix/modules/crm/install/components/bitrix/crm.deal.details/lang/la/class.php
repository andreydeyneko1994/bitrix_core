<?php
$MESS["CRM_DEAL_COPY_PAGE_TITLE"] = "Copiar negociación";
$MESS["CRM_DEAL_CREATION_PAGE_TITLE"] = "Nueva negociación";
$MESS["CRM_DEAL_FIELD_ADD_OBSERVER"] = "Agregar observador";
$MESS["CRM_DEAL_FIELD_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_DEAL_FIELD_BEGINDATE"] = "Fecha de inicio";
$MESS["CRM_DEAL_FIELD_CATEGORY_RECURRING"] = "Negociación pipeline";
$MESS["CRM_DEAL_FIELD_CLIENT"] = "Cliente";
$MESS["CRM_DEAL_FIELD_CLOSEDATE"] = "Fecha final";
$MESS["CRM_DEAL_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_DEAL_FIELD_CONTACT_LEGEND"] = "Contactos vinculados a la negociación";
$MESS["CRM_DEAL_FIELD_CURRENCY_ID"] = "Moneda";
$MESS["CRM_DEAL_FIELD_DATE_CREATE"] = "Creado el";
$MESS["CRM_DEAL_FIELD_DATE_MODIFY"] = "Modificado el";
$MESS["CRM_DEAL_FIELD_ID"] = "ID";
$MESS["CRM_DEAL_FIELD_LOCATION_ID"] = "ubicación";
$MESS["CRM_DEAL_FIELD_NUM_SIGN"] = "##DEAL_ID#";
$MESS["CRM_DEAL_FIELD_OBSERVERS"] = "Observadores";
$MESS["CRM_DEAL_FIELD_OPENED"] = "Disponible para todos";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY"] = "Importe";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY_WITH_CURRENCY"] = "Monto y moneda";
$MESS["CRM_DEAL_FIELD_PAYMENT_DOCUMENTS"] = "Documentación de pago";
$MESS["CRM_DEAL_FIELD_PROBABILITY"] = "Probabilidad";
$MESS["CRM_DEAL_FIELD_PRODUCTS"] = "Productos";
$MESS["CRM_DEAL_FIELD_RECURRING"] = "Repetir";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_FROM_CURRENT"] = "La plantilla ha sido creada para una negociación recurrente ##RECURRING_ID#";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_MANY_FROM_CURRENT"] = "La plantilla ha sido creada para una negociación recurrente: #RECURRING_LIST#";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_DAY"] = "día(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL"] = "Periodo personalizado";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL_TITLE"] = "Repetir cada";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_MONTH"] = "mes(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_WEEK"] = "semana(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_YEAR"] = "año";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_BEGINDATE_OFFSET_TITLE"] = "Intervalo de la fecha de inicio";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_CLOSEDATE_OFFSET_TITLE"] = "Intervalo de la fecha de cierre";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_NEXT_EXECUTION"] = "Siguiente negociación #NEXT_DATE#";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERYDAY"] = "Diariamente";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_MONTH"] = "Mensual";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_WEEK"] = "Semanal";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_YEAR"] = "Anual";
$MESS["CRM_DEAL_FIELD_RECURRING_MANY_TIMES"] = "Regularmente";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_DATE"] = "Cuando llegó a la fecha final";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TIMES"] = "Máximo de iteraciones alcanzado";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TITLE"] = "Deja de repetir";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_NO_LIMIT"] = "Nunca";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_DATE_TITLE"] = "Finalizado el";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_TIMES_TITLE"] = "Máximo de iteraciones";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_START_DATE_TITLE"] = "Iniciado el";
$MESS["CRM_DEAL_FIELD_RECURRING_MUTLTIPLE_PERIOD_TITLE"] = "Intervalo de repetición";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_BEGINDATE_VALUE_TITLE"] = "Valor de la fecha de inicio para la nueva negociación";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_CLOSEDATE_VALUE_TITLE"] = "Valor de la fecha de cierre para la nueva negociación";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_CURRENT_FIELD"] = "valor del campo actual";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_DATE_CREATION_OFFSET"] = "como intervalo desde la fecha de creación";
$MESS["CRM_DEAL_FIELD_RECURRING_NOTHING_SELECTED"] = "Sin selección";
$MESS["CRM_DEAL_FIELD_RECURRING_NOT_REPEAT"] = "No repetir";
$MESS["CRM_DEAL_FIELD_RECURRING_ONCE_TIME"] = "Sólo una vez";
$MESS["CRM_DEAL_FIELD_RECURRING_RESTRICTED"] = "Derechos insuficientes";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TITLE"] = "Negociación creada el";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_DAY"] = "días(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_MONTH"] = "mes(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_WEEK"] = "semana(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_TITLE"] = "Plantilla de la negociación: #TITLE#";
$MESS["CRM_DEAL_FIELD_SOURCE_DESCRIPTION"] = "Origen de información";
$MESS["CRM_DEAL_FIELD_SOURCE_ID"] = "Origen";
$MESS["CRM_DEAL_FIELD_STAGE_ID"] = "Etapa";
$MESS["CRM_DEAL_FIELD_TITLE"] = "Nombre";
$MESS["CRM_DEAL_FIELD_TYPE_ID"] = "Tipo de negociación";
$MESS["CRM_DEAL_FIELD_UTM"] = "Parámetros UTM";
$MESS["CRM_DEAL_NOT_FOUND"] = "La negociación no fue encontrada.";
$MESS["CRM_DEAL_QUOTE_LINK"] = "La negociación fue creada en base a la cotización <a target='_blank' href='#URL#'>#TITLE#</a>";
$MESS["CRM_DEAL_REPEATED_APPROACH"] = "Repetir consulta";
$MESS["CRM_DEAL_RETURNING"] = "Negociación repetida";
$MESS["CRM_DEAL_SECTION_ADDITIONAL"] = "Más";
$MESS["CRM_DEAL_SECTION_MAIN"] = "Sobre la negociación";
$MESS["CRM_DEAL_SECTION_PRODUCTS"] = "Productos";
$MESS["CRM_DEAL_SECTION_RECURRING"] = "Negociación recurrente";
$MESS["CRM_DEAL_SOURCE_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_DEAL_TAB_AUTOMATION"] = "Automatización";
$MESS["CRM_DEAL_TAB_BIZPROC"] = "Procesos de negocio";
$MESS["CRM_DEAL_TAB_EVENT"] = "Historial";
$MESS["CRM_DEAL_TAB_INVOICES"] = "Facturas";
$MESS["CRM_DEAL_TAB_ORDERS"] = "Pedidos";
$MESS["CRM_DEAL_TAB_PRODUCTS"] = "Productos";
$MESS["CRM_DEAL_TAB_QUOTE"] = "Cotizaciones";
$MESS["CRM_DEAL_TAB_TREE"] = "Dependencias";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
