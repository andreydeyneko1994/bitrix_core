<?php
$MESS["CRM_DEAL_COPY_PAGE_TITLE"] = "Copier la transaction";
$MESS["CRM_DEAL_CREATION_PAGE_TITLE"] = "Nouvelle transaction";
$MESS["CRM_DEAL_FIELD_ADD_OBSERVER"] = "Ajouter un observateur";
$MESS["CRM_DEAL_FIELD_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_DEAL_FIELD_BEGINDATE"] = "Date de début";
$MESS["CRM_DEAL_FIELD_CATEGORY_RECURRING"] = "Pipeline de la transaction";
$MESS["CRM_DEAL_FIELD_CLIENT"] = "Client";
$MESS["CRM_DEAL_FIELD_CLOSEDATE"] = "Date de fin";
$MESS["CRM_DEAL_FIELD_COMMENTS"] = "Commentaire";
$MESS["CRM_DEAL_FIELD_CONTACT_LEGEND"] = "Contacts liés à la transaction";
$MESS["CRM_DEAL_FIELD_CURRENCY_ID"] = "Devise";
$MESS["CRM_DEAL_FIELD_DATE_CREATE"] = "Créé le";
$MESS["CRM_DEAL_FIELD_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_DEAL_FIELD_ID"] = "ID";
$MESS["CRM_DEAL_FIELD_LOCATION_ID"] = "Lieu";
$MESS["CRM_DEAL_FIELD_NUM_SIGN"] = "##DEAL_ID#";
$MESS["CRM_DEAL_FIELD_OBSERVERS"] = "Observateurs";
$MESS["CRM_DEAL_FIELD_OPENED"] = "Accessible à tout le monde";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY"] = "Montant";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY_WITH_CURRENCY"] = "Montant et devise";
$MESS["CRM_DEAL_FIELD_PAYMENT_DOCUMENTS"] = "Documentation sur les paiements";
$MESS["CRM_DEAL_FIELD_PROBABILITY"] = "Probabilité";
$MESS["CRM_DEAL_FIELD_PRODUCTS"] = "Produits";
$MESS["CRM_DEAL_FIELD_RECURRING"] = "Répéter";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_FROM_CURRENT"] = "Un modèle a été créé pour la transaction récurrente ##RECURRING_ID#";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_MANY_FROM_CURRENT"] = "Un modèle a été créé pour les transactions récurrentes : #RECURRING_LIST#";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_DAY"] = "jour(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL"] = "Période personnalisée";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL_TITLE"] = "Répéter tous les";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_MONTH"] = "mois";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_WEEK"] = "semaine(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_YEAR"] = "an";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_BEGINDATE_OFFSET_TITLE"] = "Décalage de la date de début";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_CLOSEDATE_OFFSET_TITLE"] = "Décalage de la date de fermeture";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_NEXT_EXECUTION"] = "La prochaine occurrence aura lieu le #NEXT_DATE#";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERYDAY"] = "Chaque jour";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_MONTH"] = "Chaque mois";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_WEEK"] = "Chaque semaine";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_YEAR"] = "Chaque année";
$MESS["CRM_DEAL_FIELD_RECURRING_MANY_TIMES"] = "Régulièrement";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_DATE"] = "Une fois la date de fin atteinte";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TIMES"] = "Une fois le maximum d'itérations atteint";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TITLE"] = "Arrêter la répétition";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_NO_LIMIT"] = "Jamais";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_DATE_TITLE"] = "Terminer le";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_TIMES_TITLE"] = "Maximum d'itérations";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_START_DATE_TITLE"] = "Démarrer le";
$MESS["CRM_DEAL_FIELD_RECURRING_MUTLTIPLE_PERIOD_TITLE"] = "Intervalle de répétition";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_BEGINDATE_VALUE_TITLE"] = "Valeur de date de début pour une nouvelle transaction";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_CLOSEDATE_VALUE_TITLE"] = "Valeur de date d'achèvement pour une nouvelle transaction";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_CURRENT_FIELD"] = "valeur actuelle du champ";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_DATE_CREATION_OFFSET"] = "en décalage par rapport à la date de création";
$MESS["CRM_DEAL_FIELD_RECURRING_NOTHING_SELECTED"] = "Aucune sélection";
$MESS["CRM_DEAL_FIELD_RECURRING_NOT_REPEAT"] = "Ne pas répéter";
$MESS["CRM_DEAL_FIELD_RECURRING_ONCE_TIME"] = "Une seule fois";
$MESS["CRM_DEAL_FIELD_RECURRING_RESTRICTED"] = "Permissions insuffisantes";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TITLE"] = "Créer la transaction dans";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_DAY"] = "jour(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_MONTH"] = "mois";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_WEEK"] = "semaine(s)";
$MESS["CRM_DEAL_FIELD_RECURRING_TITLE"] = "Modèle de transaction : #TITLE#";
$MESS["CRM_DEAL_FIELD_SOURCE_DESCRIPTION"] = "Informations sur la source";
$MESS["CRM_DEAL_FIELD_SOURCE_ID"] = "Source";
$MESS["CRM_DEAL_FIELD_STAGE_ID"] = "Étape";
$MESS["CRM_DEAL_FIELD_TITLE"] = "Référence";
$MESS["CRM_DEAL_FIELD_TYPE_ID"] = "Type de transaction";
$MESS["CRM_DEAL_FIELD_UTM"] = "Paramètres UTM";
$MESS["CRM_DEAL_NOT_FOUND"] = "Transaction introuvable.";
$MESS["CRM_DEAL_QUOTE_LINK"] = "La transaction créée est basée sur le devis <a target='_blank' href='#URL#'>#TITLE#</a>";
$MESS["CRM_DEAL_REPEATED_APPROACH"] = "Répéter la requête";
$MESS["CRM_DEAL_RETURNING"] = "Répéter la transaction";
$MESS["CRM_DEAL_SECTION_ADDITIONAL"] = "Plus";
$MESS["CRM_DEAL_SECTION_MAIN"] = "À propos de la transaction";
$MESS["CRM_DEAL_SECTION_PRODUCTS"] = "Produits";
$MESS["CRM_DEAL_SECTION_RECURRING"] = "Transaction récurrente";
$MESS["CRM_DEAL_SOURCE_NOT_SELECTED"] = "Non sélectionné";
$MESS["CRM_DEAL_TAB_AUTOMATION"] = "Automatisation";
$MESS["CRM_DEAL_TAB_BIZPROC"] = "Flux de travail";
$MESS["CRM_DEAL_TAB_EVENT"] = "Historique";
$MESS["CRM_DEAL_TAB_INVOICES"] = "Factures";
$MESS["CRM_DEAL_TAB_ORDERS"] = "Commandes";
$MESS["CRM_DEAL_TAB_PRODUCTS"] = "Produits";
$MESS["CRM_DEAL_TAB_QUOTE"] = "Devis";
$MESS["CRM_DEAL_TAB_TREE"] = "Dépendances";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
