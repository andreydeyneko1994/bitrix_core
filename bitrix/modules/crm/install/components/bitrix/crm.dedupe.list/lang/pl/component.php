<?
$MESS["CRM_DEDUPE_LIST_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_LIST_COL_ORGANIZATION"] = "Nazwa firmy";
$MESS["CRM_DEDUPE_LIST_COL_PERSON"] = "Pełna nazwa";
$MESS["CRM_DEDUPE_LIST_COL_PHONE"] = "Telefon";
$MESS["CRM_DEDUPE_LIST_COL_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_DEDUPE_LIST_COL_TITLE"] = "Nazwa";
$MESS["CRM_DEDUPE_LIST_DEFAUL_SCOPE_TITLE"] = "Nie wybrano";
$MESS["CRM_DEDUPE_LIST_INVALID_ENTITY_TYPE"] = "Typ '#TYPE_NAME#' nie jest obsługiwany w tym zakresie.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND"] = "Nie można usyskać listy duplikatów z pola '#NAME#'. Proszę przebudować listę.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG"] = "Nie znaleziono duplikatów \"#NAME#\".";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG_PLURAL"] = "Nie znaleziono duplikatów dla pól #NAMES#\"\"";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>