<?
$MESS["CRM_DEDUPE_LIST_JUNK"] = "Informacja jest nieaktualna. Proszę zaktualizować listę.";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_DENIED"] = "Brak uprawnień do usuwania \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_FAILED"] = "Nie można usunąć \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_NOT_FOUND"] = "Nie można znaleźć elementu [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_READ_DENIED"] = "Brak uprawnień do odczytu \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_DENIED"] = "Brak uprawnień do aktualizacji \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_FAILED"] = "Nie można zapisać \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_GENERAL_ERROR"] = "Wystąpił błąd podczas scalania";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_COMPLETED_SUMMARY"] = "Lista duplikatów została zbudowana. Znalezione dopasowania: #PROCESSED_ITEMS#.";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_PROGRESS_SUMMARY"] = "Znalezione dopasowania: #PROCESSED_ITEMS#.";
?>