<?
$MESS["CRM_INVOICE_CUSTOM_SAVE_BUTTON_TITLE"] = "Enregistrer et revenir";
$MESS["CRM_INVOICE_PS_PROPS_CONTENT"] = "Pour commencer à travailler avec les factures, fournissez les mentions de l'entreprise qui seront utilisées dans les factures.";
$MESS["CRM_INVOICE_PS_PROPS_GOTO"] = "Ajouter les informations détaillées sur l'entreprise";
$MESS["CRM_INVOICE_PS_PROPS_TITLE"] = "Mentions de la société";
$MESS["CRM_INVOICE_RECUR_SHOW_TITLE"] = "Facture récurrente ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_INVOICE_SHOW_LEGEND"] = "Facture ##ACCOUNT_NUMBER#";
$MESS["CRM_INVOICE_SHOW_NEW_TITLE"] = "Nouvelle facture";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Facture ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_TAB_1"] = "Facture";
$MESS["CRM_TAB_1_TITLE"] = "Propriétés de la facture";
?>