<?
$MESS["CRM_INVOICE_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se a página atual ainda estiver sendo exibida, feche-a manualmente.";
$MESS["CRM_INVOICE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "A fatura <a href='#URL#'>#TITLE#</a> foi criada. Agora você está sendo redirecionado para a página anterior. Se a página atual ainda estiver sendo exibida, feche-a manualmente.";
?>