<?
$MESS["CRM_INVOICE_EDIT_EVENT_CANCELED"] = "Działanie zostało anulowane. Trwa przekierowywanie z powrotem na poprzednią stronę. Jeśli bieżąca strona nadal się wyświetla, zamknij ją ręcznie.";
$MESS["CRM_INVOICE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Utworzono fakturę <a href='#URL#'>#TITLE#</a>. Trwa przekierowywanie z powrotem na poprzednią stronę. Jeśli bieżąca strona nadal się wyświetla, zamknij ją ręcznie.";
?>