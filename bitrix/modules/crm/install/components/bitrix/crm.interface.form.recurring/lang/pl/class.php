<?
$MESS["CRM_RECURRING_CREATE_NEW"] = "utwórz nowy";
$MESS["CRM_RECURRING_DEAL_HINT_BASE"] = "Deal zostanie utworzony #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_DEAL_SWITCHER_BLOCK"] = "Utwórz deal cykliczny";
$MESS["CRM_RECURRING_DEAL_TEMPLATE_WILL_BE_CREATED"] = "Uwaga! Nowy szablon o określonych parametrach będzie utworzony na podstawie bieżącego dealu.";
$MESS["CRM_RECURRING_EMAIL_LABEL"] = "wyślij fakturę na e-mail klienta";
$MESS["CRM_RECURRING_EMAIL_TEMPLATE"] = "szablon wiadomości";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL"] = "Pole \"e-mail firmowy\" w twoich danych firmy (CRM – Ustawienia – Dane firmy) jest puste.";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL1"] = "Brak adresu e-mail w Twoich danych firmy.";
$MESS["CRM_RECURRING_FILTER_BEFORE_DATE"] = "przed";
$MESS["CRM_RECURRING_FILTER_CREATE_DATE_PAYMENT"] = "Ustaw termin płatności";
$MESS["CRM_RECURRING_FILTER_DAILY"] = "dzień";
$MESS["CRM_RECURRING_FILTER_DAILY_ANY"] = "kalendarzowe";
$MESS["CRM_RECURRING_FILTER_DAILY_WORK"] = "dzień roboczy";
$MESS["CRM_RECURRING_FILTER_DATE_PAYMENT_AFTER_EXPOSING"] = "dni po dacie wystawienia faktury";
$MESS["CRM_RECURRING_FILTER_DATE_PAYMENT_IN_DATE"] = "za";
$MESS["CRM_RECURRING_FILTER_DAYS"] = "dni";
$MESS["CRM_RECURRING_FILTER_DAY_INTERVAL"] = "dzień";
$MESS["CRM_RECURRING_FILTER_DAY_OF_MONTH"] = "dzień";
$MESS["CRM_RECURRING_FILTER_DEAL_CATEGORISES"] = "Lejek";
$MESS["CRM_RECURRING_FILTER_DEAL_CREATE_BY"] = "utwórz deal";
$MESS["CRM_RECURRING_FILTER_DO_REPEAT"] = "powtórz";
$MESS["CRM_RECURRING_FILTER_EACH"] = "w każdą";
$MESS["CRM_RECURRING_FILTER_EACH_F"] = "co";
$MESS["CRM_RECURRING_FILTER_EACH_M"] = "co";
$MESS["CRM_RECURRING_FILTER_EACH_M_ALT"] = "co";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_0"] = "powtórzenia";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_1"] = "powtórzenia";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_2"] = "powtórzenia";
$MESS["CRM_RECURRING_FILTER_EVERY_DAY"] = "codziennie";
$MESS["CRM_RECURRING_FILTER_EVERY_MONTH"] = "co miesiąc";
$MESS["CRM_RECURRING_FILTER_EVERY_WEEK"] = "co tydzień";
$MESS["CRM_RECURRING_FILTER_EVERY_YEAR"] = "co rok";
$MESS["CRM_RECURRING_FILTER_MONTHLY"] = "mies.";
$MESS["CRM_RECURRING_FILTER_MONTHS"] = "miesiące";
$MESS["CRM_RECURRING_FILTER_MONTH_1"] = "Styczeń";
$MESS["CRM_RECURRING_FILTER_MONTH_10"] = "Październik";
$MESS["CRM_RECURRING_FILTER_MONTH_11"] = "Listopad";
$MESS["CRM_RECURRING_FILTER_MONTH_12"] = "grudzień";
$MESS["CRM_RECURRING_FILTER_MONTH_2"] = "luty";
$MESS["CRM_RECURRING_FILTER_MONTH_3"] = "marzec";
$MESS["CRM_RECURRING_FILTER_MONTH_4"] = "kwiecień";
$MESS["CRM_RECURRING_FILTER_MONTH_5"] = "maj";
$MESS["CRM_RECURRING_FILTER_MONTH_6"] = "czerwiec";
$MESS["CRM_RECURRING_FILTER_MONTH_7"] = "lipiec";
$MESS["CRM_RECURRING_FILTER_MONTH_8"] = "Sierpień";
$MESS["CRM_RECURRING_FILTER_MONTH_9"] = "Wrzesień";
$MESS["CRM_RECURRING_FILTER_MONTH_ALT"] = "miesiące";
$MESS["CRM_RECURRING_FILTER_MONTH_SHORT"] = "mies.";
$MESS["CRM_RECURRING_FILTER_NUMBER_0"] = "pierwsze";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_F"] = "pierwsze";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_M"] = "pierwsze";
$MESS["CRM_RECURRING_FILTER_NUMBER_1"] = "drugie";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_F"] = "drugie";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_M"] = "drugie";
$MESS["CRM_RECURRING_FILTER_NUMBER_2"] = "trzecie";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_F"] = "trzecie";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_M"] = "trzecie";
$MESS["CRM_RECURRING_FILTER_NUMBER_3"] = "czwarte";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_F"] = "czwarte";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_M"] = "czwarte";
$MESS["CRM_RECURRING_FILTER_NUMBER_4"] = "ostatnie";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_F"] = "ostatnie";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_M"] = "ostatnie";
$MESS["CRM_RECURRING_FILTER_NUMBER_OF_EACH_M_ALT"] = "dnia każdego";
$MESS["CRM_RECURRING_FILTER_REPEATS"] = "powtórzenia";
$MESS["CRM_RECURRING_FILTER_REPEAT_END"] = "Powtarzaj do";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_DATE"] = "data końcowa";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_NONE"] = "brak daty końcowej";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_TIMES"] = "zakończ po";
$MESS["CRM_RECURRING_FILTER_REPEAT_START"] = "Wykonaj powtórzenie po";
$MESS["CRM_RECURRING_FILTER_REPEAT_TYPE"] = "powtórz typ";
$MESS["CRM_RECURRING_FILTER_UNSET_DATE_PAYMENT"] = "nie ustawiaj";
$MESS["CRM_RECURRING_FILTER_WD_1"] = "Poniedziałek";
$MESS["CRM_RECURRING_FILTER_WD_2"] = "Wtorek";
$MESS["CRM_RECURRING_FILTER_WD_3"] = "Środa";
$MESS["CRM_RECURRING_FILTER_WD_4"] = "Czwartek";
$MESS["CRM_RECURRING_FILTER_WD_5"] = "Piątek";
$MESS["CRM_RECURRING_FILTER_WD_6"] = "Sobota";
$MESS["CRM_RECURRING_FILTER_WD_7"] = "Niedziela";
$MESS["CRM_RECURRING_FILTER_WD_SH_1"] = "pon.";
$MESS["CRM_RECURRING_FILTER_WD_SH_2"] = "wt.";
$MESS["CRM_RECURRING_FILTER_WD_SH_3"] = "śr.";
$MESS["CRM_RECURRING_FILTER_WD_SH_4"] = "czw.";
$MESS["CRM_RECURRING_FILTER_WD_SH_5"] = "pt.";
$MESS["CRM_RECURRING_FILTER_WD_SH_6"] = "sob.";
$MESS["CRM_RECURRING_FILTER_WD_SH_7"] = "niedz.";
$MESS["CRM_RECURRING_FILTER_WEEKLY"] = "tydzień";
$MESS["CRM_RECURRING_FILTER_WEEKS"] = "tygodnie";
$MESS["CRM_RECURRING_FILTER_WEEK_ALT"] = "tydzień";
$MESS["CRM_RECURRING_FILTER_YEARLY"] = "rok";
$MESS["CRM_RECURRING_HINT_1_PLURAL_0"] = "dzień";
$MESS["CRM_RECURRING_HINT_1_PLURAL_1"] = "dni";
$MESS["CRM_RECURRING_HINT_1_PLURAL_2"] = "dni";
$MESS["CRM_RECURRING_HINT_2_PLURAL_0"] = "tydzień";
$MESS["CRM_RECURRING_HINT_2_PLURAL_1"] = "tygodnie";
$MESS["CRM_RECURRING_HINT_2_PLURAL_2"] = "tygodnie";
$MESS["CRM_RECURRING_HINT_3_PLURAL_0"] = "mies.";
$MESS["CRM_RECURRING_HINT_3_PLURAL_1"] = "miesiące";
$MESS["CRM_RECURRING_HINT_3_PLURAL_2"] = "miesiące";
$MESS["CRM_RECURRING_HINT_A_FEW_DAYS_BEFORE_DATE"] = "dla #COUNT_ELEMENT# #TYPE_ELEMENT##BEFORE_DATE#";
$MESS["CRM_RECURRING_HINT_BASE"] = "Faktura zostanie utworzona #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_HINT_BEFORE_DATE"] = "do #DATE#";
$MESS["CRM_RECURRING_HINT_EACH"] = "co";
$MESS["CRM_RECURRING_HINT_EACH_F"] = "co";
$MESS["CRM_RECURRING_HINT_EACH_M"] = "co";
$MESS["CRM_RECURRING_HINT_EACH_M_ALT"] = "co";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK"] = "każdego #DAY_NUMBER#. dnia";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK_WORK"] = "każdego #DAY_NUMBER#. dnia roboczego";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1"] = "każdego #DAY_NUMBER#. dnia miesiąca co #MONTH_NUMBER# miesięcy";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1_WORK"] = "każdego #DAY_NUMBER#. dnia roboczego miesiąca co #MONTH_NUMBER# miesięcy";
$MESS["CRM_RECURRING_HINT_ELEMENT_WEEKDAY_MASK"] = "co #WEEK_NUMBER# tygodni (#LIST_WEEKDAY_NAMES#)";
$MESS["CRM_RECURRING_HINT_END"] = " do <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_0"] = "powtórzenia";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_1"] = "powtórzenia";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_2"] = "powtórzenia";
$MESS["CRM_RECURRING_HINT_END_NONE"] = ", brak daty końcowej";
$MESS["CRM_RECURRING_HINT_END_TIMES"] = ", zakończy się <nobr>#TIMES# #TIMES_PLURAL#</nobr>";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_1"] = "codziennie";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_2"] = "co tydzień";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_3"] = "co miesiąc";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_4"] = "co rok";
$MESS["CRM_RECURRING_HINT_MONTHLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER#. #WEEKDAY_NAME# co #MONTH_NUMBER# mies.";
$MESS["CRM_RECURRING_HINT_MONTH_1"] = "Styczeń";
$MESS["CRM_RECURRING_HINT_MONTH_10"] = "Październik";
$MESS["CRM_RECURRING_HINT_MONTH_11"] = "Listopad";
$MESS["CRM_RECURRING_HINT_MONTH_12"] = "Grudzień";
$MESS["CRM_RECURRING_HINT_MONTH_2"] = "Luty";
$MESS["CRM_RECURRING_HINT_MONTH_3"] = "Marzec";
$MESS["CRM_RECURRING_HINT_MONTH_4"] = "Kwiecień";
$MESS["CRM_RECURRING_HINT_MONTH_5"] = "Maj";
$MESS["CRM_RECURRING_HINT_MONTH_6"] = "Czerwiec";
$MESS["CRM_RECURRING_HINT_MONTH_7"] = "lipiec";
$MESS["CRM_RECURRING_HINT_MONTH_8"] = "Sierpień";
$MESS["CRM_RECURRING_HINT_MONTH_9"] = "Wrzesień";
$MESS["CRM_RECURRING_HINT_START_DATE"] = ", począwszy od <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_START_EMPTY"] = " po zapisaniu";
$MESS["CRM_RECURRING_HINT_TODAY"] = "dzisiaj";
$MESS["CRM_RECURRING_HINT_WEEKDAY_EVERY_DAY"] = "codziennie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0"] = "pierwsze";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_F"] = "pierwsze";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_M"] = "pierwsze";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1"] = "drugie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_F"] = "drugie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_M"] = "drugie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2"] = "trzecie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_F"] = "trzecie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_M"] = "trzecie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3"] = "czwarte";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_F"] = "czwarte";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_M"] = "czwarte";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4"] = "ostatnie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_F"] = "ostatnie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_M"] = "ostatnie";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_1"] = "Poniedziałek";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_2"] = "Wtorek";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3"] = "Środa";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3_ALT"] = "Środa";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_4"] = "Czwartek";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5"] = "Piątek";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5_ALT"] = "Piątek";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6"] = "Sobota";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6_ALT"] = "Sobota";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_7"] = "Niedziela";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1"] = "każdego #DAY_NUMBER#. dnia #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1_WORKDAY"] = "każdego #DAY_NUMBER#. dnia roboczego #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER#. #WEEKDAY_NAME# #MONTH_NAME#";
$MESS["CRM_RECURRING_INVOICE_HINT_BASE"] = "Faktura zostanie utworzona #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_INVOICE_SWITCHER_BLOCK"] = "Utwórz fakturę cykliczną";
$MESS["CRM_RECURRING_SWITCHER_BLOCK"] = "Utwórz fakturę cykliczną";
$MESS["CRM_RECURRING_TEMPLATE_WILL_BE_CREATED"] = "Uwaga! Nowy szablon zostanie utworzony na podstawie wybranej faktury z określonymi parametrami powtarzania.";
$MESS["NEXT_DEAL_EMPTY"] = "Deal jest nieaktywny";
$MESS["NEXT_EXECUTION_DEAL_HINT"] = "Następny deal zostanie utworzony #DATE_EXECUTION#";
$MESS["NEXT_EXECUTION_HINT"] = "Następna faktura zostanie wystawiona #DATE_EXECUTION#";
$MESS["NEXT_EXECUTION_INVOICE_HINT"] = "Data następnej faktury #DATE_EXECUTION#";
$MESS["NEXT_INVOICE_EMPTY"] = "Szablon faktury jest nieaktywny";
?>