<?
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MEASURE_ERR_ALREADY_EXISTS"] = "Jednostka miary z ID \"#CODE#\" już istnieje.";
$MESS["CRM_MEASURE_ERR_CODE_EMPTY"] = "Proszę określić kod dla jednostki miary. Kod powinien być dodatnią liczbą całkowitą.";
$MESS["CRM_MEASURE_ERR_CODE_INVALID"] = "ID jednostki miary może zawierać jedynie liczbę.";
$MESS["CRM_MEASURE_ERR_CREATE"] = "Nieznany błąd przy tworzeniu jednostki miary.";
$MESS["CRM_MEASURE_ERR_TITLE_EMPTY"] = "Proszę podać nazwę dla jednostki miary.";
$MESS["CRM_MEASURE_ERR_UPDATE"] = "Nieznany błąd przy aktualizacji jednostki miary.";
$MESS["CRM_MEASURE_FIELD_CODE"] = "Kod";
$MESS["CRM_MEASURE_FIELD_ID"] = "ID";
$MESS["CRM_MEASURE_FIELD_IS_DEFAULT"] = "domyślne";
$MESS["CRM_MEASURE_FIELD_MEASURE_TITLE"] = "Nazwa jednostki";
$MESS["CRM_MEASURE_FIELD_SYMBOL_INTL"] = "Symbol jednostki (międzynarodowy)";
$MESS["CRM_MEASURE_FIELD_SYMBOL_LETTER_INTL"] = "Nazwa kodu (zagr.)";
$MESS["CRM_MEASURE_FIELD_SYMBOL_RUS"] = "Symbol jednostki";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SECTION_MEASURE_INFO"] = "Jednostka miary";
?>