<?
$MESS["C_CRM_LFA_TASKS_CHANGED_MESSAGE_24_2"] = "Zmodyfikowane";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE"] = "#USER_NAME# ukończył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_F"] = "#USER_NAME# ukończył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_M"] = "#USER_NAME# ukończył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE"] = "#USER_NAME# utworzył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_F"] = "#USER_NAME# utworzył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_M"] = "#USER_NAME# utworzył/a zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY"] = "#USER_NAME# zmienił zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_F"] = "#USER_NAME# zmieniła zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_M"] = "#USER_NAME# zmienił zadanie #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS"] = "#USER_NAME# zmienił status of #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_F"] = "#USER_NAME# zmieniła status of #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_M"] = "#USER_NAME# zmienił status of #TITLE#";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_1_24"] = "Zadanie zostało wznowione";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_2_24"] = "Zadanie jest oczekujące";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_3_24"] = "Zadanie jest w toku";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24"] = "Zadanie zostało zamknięte";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_2"] = "Powód";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_CHANGES"] = "wymaga kontroli autora";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_5_24"] = "Zadanie zostało zamknięte";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_6_24"] = "Zadanie zostało odroczone";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24"] = "Zadanie zostało odrzucone";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24_2"] = "Powód";
?>