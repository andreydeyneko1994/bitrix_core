<?php
$MESS["CRM_CURRENCY_CLASSIFIER_ADD_UNKNOWN_ERROR"] = "Erro desconhecido ao criar uma moeda.";
$MESS["CRM_CURRENCY_CLASSIFIER_CURRENCY_NOT_FOUND"] = "A moeda #currency# não foi encontrada";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME_ERROR"] = "O nome da moeda não deve estar vazio ou ultrapassar 50 caracteres";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_INCORRECT_VALUE_ERROR"] = "O valor está incorreto ou muito grande";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_MAX_LENGTH_ERROR"] = "O valor não deve ultrapassar #max_length# caracteres";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_AFTER"] = "Depois do valor";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_BEFORE"] = "Antes do valor";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE_ERROR"] = "O código simbólico precisa ser uma abreviação de três letras latinas";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT_OWN"] = "Outro valor";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CRM"] = "O módulo CRM não está instalado.";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_CURRENCY_CLASSIFIER_UPDATE_UNKNOWN_ERROR"] = "Erro desconhecido ao atualizar uma moeda.";
