<?
$MESS["CRM_TRACKING_SITE_B24_AUTO_CONNECTED"] = "Podłączono kanał %name%!";
$MESS["CRM_TRACKING_SITE_B24_AUTO_DESC"] = "Ten kanał jest automatyczny. Nie wymaga żadnej specjalnej konfiguracji.";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_SITES"] = "Brak witryn";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_STORES"] = "Brak sklepów";
$MESS["CRM_TRACKING_SITE_B24_REPLACEMENT"] = "Zamiana numerów telefonu i adresów e-mail witryn Bitrix24";
$MESS["CRM_TRACKING_SITE_B24_VIEW"] = "Wyświetl witrynę";
?>