<?
$MESS["CRM_TRACKING_SITE_B24_AUTO_CONNECTED"] = "O canal %name% conectado!";
$MESS["CRM_TRACKING_SITE_B24_AUTO_DESC"] = "Este canal é automático. Não requer nenhuma configuração específica.";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_SITES"] = "Não há sites";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_STORES"] = "Não há lojas";
$MESS["CRM_TRACKING_SITE_B24_REPLACEMENT"] = "Troca de número de telefone e e-mail para Sites Bitrix24";
$MESS["CRM_TRACKING_SITE_B24_VIEW"] = "Visualizar site";
?>