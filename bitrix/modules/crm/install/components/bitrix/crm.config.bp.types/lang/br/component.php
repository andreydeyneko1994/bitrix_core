<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_BP_COMPANY"] = "Empresa";
$MESS["CRM_BP_COMPANY_DESC"] = "Modelos de processos de negócios para \"Empresas\"";
$MESS["CRM_BP_CONTACT"] = "Contato";
$MESS["CRM_BP_CONTACT_DESC"] = "Modelos de processos de negócios para \"Contatos\"";
$MESS["CRM_BP_DEAL"] = "Negócio";
$MESS["CRM_BP_DEAL_DESC"] = "Modelos de processos de negócios do negócio";
$MESS["CRM_BP_DYNAMIC_DESC"] = "Modelos de fluxo de trabalho #DYNAMIC_TYPE_NAME#";
$MESS["CRM_BP_ENTITY_LIST"] = "Tipos";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_BP_LEAD_DESC"] = "Modelos de processos de negócio do Lead";
$MESS["CRM_BP_QUOTE_DESC"] = "Modelos de fluxo de trabalho de cotação";
$MESS["CRM_BP_SMART_INVOICE_DESC"] = "Modelos de fluxo de trabalho de fatura";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
