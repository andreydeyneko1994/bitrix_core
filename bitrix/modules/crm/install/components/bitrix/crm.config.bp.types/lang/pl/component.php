<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_BP_COMPANY"] = "Firma";
$MESS["CRM_BP_COMPANY_DESC"] = "Szablony procesów biznesowych dla \"Firmy\"";
$MESS["CRM_BP_CONTACT"] = "Kontakt";
$MESS["CRM_BP_CONTACT_DESC"] = "Szablony procesów biznesowych dla \"Kontakty\"";
$MESS["CRM_BP_DEAL"] = "Deal";
$MESS["CRM_BP_DEAL_DESC"] = "Szablony procesów dla obiektów typu Deal";
$MESS["CRM_BP_DYNAMIC_DESC"] = "Szablony przepływów pracy #DYNAMIC_TYPE_NAME#";
$MESS["CRM_BP_ENTITY_LIST"] = "Typy";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_BP_LEAD_DESC"] = "Szablony procesów dla obiektów typu Lead";
$MESS["CRM_BP_QUOTE_DESC"] = "Szablony worflow ofert";
$MESS["CRM_BP_SMART_INVOICE_DESC"] = "Szablony worflowów faktur";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
