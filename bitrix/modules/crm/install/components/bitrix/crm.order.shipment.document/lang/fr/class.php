<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_SUBTITLE"] = "Informations sur la livraison";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_TITLE"] = "Documents de livraison";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_DATE"] = "Date du document de livraison";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_NUM"] = "Document de livraison #";
$MESS["CRM_ORDER_SHIPMENT_FIELD_TRACKING_NUMBER"] = "Numéro de suivi";
?>