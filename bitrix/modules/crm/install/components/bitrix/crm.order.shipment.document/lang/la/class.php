<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_SUBTITLE"] = "Información de envío";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_TITLE"] = "Documentos de envío";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_DATE"] = "Fecha de envío del documento";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_NUM"] = "Documento de despacho #";
$MESS["CRM_ORDER_SHIPMENT_FIELD_TRACKING_NUMBER"] = "El número de rastreo";
?>