<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Nieprawidłowe szablony danych dla kontaktu lub firmy";
$MESS["CRM_REQUISITE_ENTITY_TYPE_INVALID"] = "Nieprawidłowy typ rekordu danych dla kontaktu lub firmy";
$MESS["CRM_REQUISITE_FORM_ID_INVALID"] = "Nieprawidłowy ID formularza danych dla kontaktu lub firmy";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Szablon nienazwany";
?>