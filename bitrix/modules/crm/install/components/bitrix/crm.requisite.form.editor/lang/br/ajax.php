<?
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_EMPTY_ACTION"] = "Nenhuma ação selecionada.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUIRED_PARAMETER"] = "O parâmetro #PARAM# é obrigatório, mas está faltando.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_DELETE"] = "Não foi possível excluir contato ou informações da empresa (ID = #ID#)";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_NOT_FOUND"] = "Contato ou informações da empresa não encontrados (ID = #ID#)";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_UNKNOWN_ACTION"] = "Ação desconhecida: #ACTION#.";
?>