<?
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_EMPTY_ACTION"] = "Nie wybrano działania.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUIRED_PARAMETER"] = "Parametr #PARAM# jest wymagany, ale brakuje go.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_DELETE"] = "Nie udało się usunąć danych kontaktu lub firmy (ID = #ID#)";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_NOT_FOUND"] = "Nie znaleziono danych kontaktu lub firmy (ID = #ID#)";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_UNKNOWN_ACTION"] = "Nieznane działanie: #ACTION#.";
?>