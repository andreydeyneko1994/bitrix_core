<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_ACTIVE"] = "Activité";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_C_SORT"] = "Trier";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_DESCRIPTION"] = "Description";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_ID"] = "ID";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_NAME"] = "Nom";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_COLUMN_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_DELETE_LINK"] = "Supprimer";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_NO"] = "Non";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_OPEN_LINK"] = "Ouvrir";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_TITLE"] = "Groupes de clients";
$MESS["CRM_ORDER_BUYER_GROUP_LIST_YES"] = "Oui";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
