<?php
$MESS["CRM_REPORT_VC_W_C_CHART_NOT_AVAILABLE"] = "indisponível";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ACTIONS"] = "Ações";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_AVG_COST"] = "preço médio";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COMING_SOON"] = "Em breve!";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_CONV_FULL"] = "Conversão de vendas";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COST"] = "preço";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_COST"] = "A mudança percentual no custo em relação ao período anterior.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_QUANTITY"] = "A mudança percentual na quantidade em relação ao período anterior.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_MOVE_TO_STAGE"] = "movido para a etapa";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_QUANTITY_SMALL"] = "quantidade";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ROI"] = "ROI de publicidade";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP"] = "Configurar fontes";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP_BTN"] = "Configurar";
