<?php
$MESS["CRM_CONTACT_DETAIL_ATTR_GR_TYPE_GENERAL"] = "Todas as etapas e funis";
$MESS["CRM_CONTACT_DETAIL_ATTR_GR_TYPE_JUNK"] = "Perdido";
$MESS["CRM_CONTACT_DETAIL_ATTR_GR_TYPE_PIPELINE"] = "Em andamento + ganho";
$MESS["CRM_CONTACT_DETAIL_ATTR_REQUIRED_FULL"] = "Obrigatório começar com etapa";
$MESS["CRM_CONTACT_DETAIL_ATTR_REQUIRED_FULL_1"] = "Requerido na etapa";
$MESS["CRM_CONTACT_DETAIL_ATTR_REQUIRED_SHORT"] = "Obrigatório";
$MESS["CRM_CONTACT_DETAIL_HISTORY_STUB"] = "Você está adicionando um contato agora...";
