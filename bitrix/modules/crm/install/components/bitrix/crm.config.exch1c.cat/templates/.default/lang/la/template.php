<?
$MESS["CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Cancelar cambios actuales";
$MESS["CRM_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Guardar preferencias de integración";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TEXT"] = "1C Configuración de integración";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TITLE"] = "1C Configuración de integración";
$MESS["CRM_TAB_CATALOG_EXPORT"] = "Productos: exportación";
$MESS["CRM_TAB_CATALOG_EXPORT_TITLE"] = "Configurar 1C:Enterprise exportación de productos";
$MESS["CRM_TAB_CATALOG_IMPORT"] = "Productos: importación";
$MESS["CRM_TAB_CATALOG_IMPORT_TITLE"] = "Configurar 1C: Enterprise importación de productos";
?>