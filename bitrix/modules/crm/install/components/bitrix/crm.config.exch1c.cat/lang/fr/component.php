<?
$MESS["CAT_1CE_ELEMENTS_PER_STEP"] = "Éléments par étape (0 - importer tout à la fois)";
$MESS["CAT_1CE_INTERVAL"] = "Durée de l'étape d'importation en secondes. (0 - importer tout à la fois)";
$MESS["CAT_1C_DEACTIVATE"] = "Désactiver";
$MESS["CAT_1C_DELETE"] = "Supprimer";
$MESS["CAT_1C_DETAIL_HEIGHT"] = "Hauteur maximale admissible de l'image détaillée";
$MESS["CAT_1C_DETAIL_RESIZE"] = "Modifier l'image détaillée";
$MESS["CAT_1C_DETAIL_WIDTH"] = "Largeur admissible maximale de l'image détaillée";
$MESS["CAT_1C_ELEMENT_ACTION"] = "Éléments manquants dans le fichier d'importation";
$MESS["CAT_1C_FILE_SIZE_LIMIT"] = "Taille maximale du bloc de fichier d'importation (en bytes)";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "Autoriser l'importation pour les groupes d'utilisateurs";
$MESS["CAT_1C_INTERVAL"] = "Durée de l'étape d'importation en secondes. (0 - importer tout à la fois)";
$MESS["CAT_1C_NONE"] = "Rien";
$MESS["CAT_1C_PICTURE"] = "Image de l'annonce";
$MESS["CAT_1C_SECTION_ACTION"] = "Sections manquantes dans le fichier d'importation";
$MESS["CAT_1C_USE_CRC"] = "Utiliser des totaux de contrôle d'éléments pour l'optimisation de la mise à jour du catalogue";
$MESS["CRM_CATALOG_XML_ID"] = "Identificateur du catalogue pour l'échange";
$MESS["CRM_EXCH1C_LIST"] = "Intégration au programme '1C: Entreprise'";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devises n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Le module Blocs d'Information n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_SELECTED_CATALOG_GROUP_ID"] = "Type de prix utilisé dans CRM";
?>