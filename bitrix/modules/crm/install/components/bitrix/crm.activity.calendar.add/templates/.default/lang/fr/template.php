<?
$MESS["BX_CRM_CACA_PRIORITY"] = "Importance de l'évènement";
$MESS["BX_CRM_CACA_PRIORITY_HIGH"] = "Augmenté";
$MESS["BX_CRM_CACA_PRIORITY_LOW"] = "Bas";
$MESS["BX_CRM_CACA_PRIORITY_NORMAL"] = "Moyen";
$MESS["BX_CRM_CACA_REM_DAY"] = "jours";
$MESS["BX_CRM_CACA_REM_HOUR"] = "heures";
$MESS["BX_CRM_CACA_REM_MIN"] = "minutes";
$MESS["CRM_CALENDAR_ADD_BUTTON"] = "Ajouter";
$MESS["CRM_CALENDAR_ADD_TITLE"] = "Créer un appel/une rencontre au calendrier";
$MESS["CRM_CALENDAR_DATE"] = "Date et temps";
$MESS["CRM_CALENDAR_DESC"] = "Description";
$MESS["CRM_CALENDAR_DESC_TITLE"] = "Veuillez saisir votre commentaire";
$MESS["CRM_CALENDAR_REMIND"] = "Rappeler de l'évènement";
$MESS["CRM_CALENDAR_REMIND_FROM"] = "Dans";
$MESS["CRM_CALENDAR_TOPIC"] = "Objet";
?>