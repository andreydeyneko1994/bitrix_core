<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Estados";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Permisos insuficientes para configurar los túneles y embudos";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Los canales no están disponibles";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Canales #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Embudos y túneles de venta: Negociaciones";
