<?php
$MESS["CRM_SALES_STAGE_CREATE_ERROR"] = "Não é possível criar a etapa";
$MESS["CRM_SALES_STAGE_UPDATE_ERROR"] = "Não é possível atualizar a etapa";
$MESS["CRM_SALES_TUNNELS_ROBOTS_NOT_SUPPORTED"] = "Este tipo não suporta automação";
$MESS["CRM_SALES_TUNNELS_STAGE_HAS_DEALS"] = "Há itens ativos nesta etapa. Transfira os negócios antes de excluir a etapa.";
$MESS["CRM_SALES_TUNNELS_STAGE_IS_SYSTEM"] = "Não é possível excluir a etapa";
$MESS["CRM_SALES_TUNNELS_STAGE_NOT_FOUND"] = "Não é possível obter informações da etapa";
$MESS["CRM_ST_ACCESS_ERROR"] = "Acesso negado";
