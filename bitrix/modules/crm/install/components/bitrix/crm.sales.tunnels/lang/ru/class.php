<?php
$MESS['CRM_SALES_TUNNELS_ACCESS_DENIED'] = 'Недостаточно прав для настройки Воронок и туннелей';
$MESS['CRM_SALES_TUNNELS_TITLE'] = 'Воронки и туннели: #NAME#';
$MESS['CRM_SALES_TUNNELS_TITLE_DEAL'] = 'Воронки и туннели продаж: Сделки';
$MESS['CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED'] = 'Работа с воронками и туннелями недоступна';
$MESS['CRM_SALES_STATUSES_TITLE'] = 'Статусы';
