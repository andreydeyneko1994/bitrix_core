<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Status";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Permissão insuficiente para configurar túneis e funis";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Pipelines não estão disponíveis";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Pipelines #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Funis e túneis de vendas: negócios";
