<?php
$MESS["CRM_MENU_RIGHTS_CATEGORY_ALL_FOR_ALL"] = "Conceder acesso a todos os usuários";
$MESS["CRM_MENU_RIGHTS_CATEGORY_COPY_FROM_TUNNELS"] = "Copiar permissões de acesso do funil";
$MESS["CRM_MENU_RIGHTS_CATEGORY_CUSTOM"] = "Configurações de acesso personalizadas";
$MESS["CRM_MENU_RIGHTS_CATEGORY_NONE_FOR_ALL"] = "Fechar acesso para todos os usuários";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ALL"] = "Conceder acesso apenas ao meu negócio";
$MESS["CRM_ST_ACTION_COPY"] = "copiar";
$MESS["CRM_ST_ACTION_MOVE"] = "mover";
$MESS["CRM_ST_ADD_FUNNEL_BUTTON"] = "Adicionar funil";
$MESS["CRM_ST_ADD_NEW_CATEGORY_BUTTON_LABEL"] = "Adicionar funil de vendas";
$MESS["CRM_ST_CATEGORY_DRAG_BUTTON"] = "Arraste para classificar funis";
$MESS["CRM_ST_CONFIRM_STAGE_REMOVE_TEXT"] = "Você tem certeza que deseja excluir a etapa?";
$MESS["CRM_ST_DOT_TITLE"] = "Arraste para criar um túnel";
$MESS["CRM_ST_EDIT_CATEGORY_TITLE"] = "Editar nome";
$MESS["CRM_ST_EDIT_RIGHTS_CATEGORY"] = "Editar configurações";
$MESS["CRM_ST_ERROR_POPUP_CLOSE_BUTTON_LABEL"] = "Fechar";
$MESS["CRM_ST_ERROR_POPUP_TITLE"] = "Erro";
$MESS["CRM_ST_GENERATOR_HELP_BUTTON"] = "Leia sobre Incentivo de Vendas";
$MESS["CRM_ST_GENERATOR_SETTINGS_LINK_LABEL"] = "Incentivo de Vendas";
$MESS["CRM_ST_HELP_BUTTON"] = "Ajuda";
$MESS["CRM_ST_NOTIFICATION_CHANGES_SAVED"] = "As alterações foram salvas.";
$MESS["CRM_ST_REMOVE"] = "Excluir";
$MESS["CRM_ST_REMOVE_CATEGORY"] = "Excluir funil";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_BUTTON_LABEL"] = "excluir";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_CANCEL_BUTTON_LABEL"] = "Cancelar";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_DESCRIPTION"] = "Excluir um funil irá excluir todos os túneis configurados";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_TITLE"] = "Excluir funil \"#name#\"";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_REMOVE_BUTTON_LABEL"] = "excluir funil";
$MESS["CRM_ST_ROBOTS_HELP_BUTTON"] = "Leia sobre automação de CRM";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Vendas Automatizadas por CRM Bitrix24<br><br>
	Crie cenários para o CRM alavancar seus negócios: atribua tarefas, planeje reuniões, lance campanhas de anúncios direcionados e emita faturas. As regras de automação irão orientar o seu gerente nos próximos passos em cada etapa do negócio para concluir a tarefa.<br><br>
	Os gatilhos reagem automaticamente às ações do cliente (quando o site é visitado, o formulário é preenchido, um comentário é postado em rede social ou uma chamada é feita) e lança uma regra de automação ajudando a transformar o lead em um negócio.<br><br>
	As regras e gatilhos de automação fazem todas as operações de rotina sem nenhum treinamento dos seus gerentes: configure uma só vez e continue trabalhando!<br><br>
	Adicione regras de automação e aumente suas vendas!<br><br>
	Esta ferramenta está disponível apenas em assinaturas comerciais Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Regras de automação";
$MESS["CRM_ST_ROBOT_ACTION_COPY"] = "Copiar auxiliar";
$MESS["CRM_ST_ROBOT_ACTION_MOVE"] = "Mover auxiliar";
$MESS["CRM_ST_ROBOT_SETTINGS_LINK_LABEL"] = "Configurar regras de automação";
$MESS["CRM_ST_SAVE_ERROR"] = "Ocorreu um erro ao salvar o funil";
$MESS["CRM_ST_SETTINGS"] = "Configurar";
$MESS["CRM_ST_STAGES_DISABLED"] = "Etapas desativadas";
$MESS["CRM_ST_STAGES_GROUP_FAIL"] = "Falha";
$MESS["CRM_ST_STAGES_GROUP_IN_PROGRESS"] = "Em andamento";
$MESS["CRM_ST_STAGES_GROUP_SUCCESS"] = "Bem sucedido";
$MESS["CRM_ST_TITLE_EDITOR_PLACEHOLDER"] = "Novo funil";
$MESS["CRM_ST_TUNNEL_BUTTON_LABEL"] = "Túnel";
$MESS["CRM_ST_TUNNEL_BUTTON_TITLE"] = "Editar ou excluir túnel";
$MESS["CRM_ST_TUNNEL_EDIT_ACCESS_DENIED"] = "Permissão insuficiente para criar um túnel.";
