<?php
$MESS["CRM_MENU_RIGHTS_CATEGORY_ALL_FOR_ALL"] = "Conceder acceso a todos los usuarios";
$MESS["CRM_MENU_RIGHTS_CATEGORY_COPY_FROM_TUNNELS"] = "Copiar los permisos de acceso del embudo";
$MESS["CRM_MENU_RIGHTS_CATEGORY_CUSTOM"] = "Configuraciones de acceso personalizadas";
$MESS["CRM_MENU_RIGHTS_CATEGORY_NONE_FOR_ALL"] = "Bloquear el acceso a todos los usuarios";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ALL"] = "Conceder acceso solo a mi negociación";
$MESS["CRM_ST_ACTION_COPY"] = "copiar";
$MESS["CRM_ST_ACTION_MOVE"] = "mover";
$MESS["CRM_ST_ADD_FUNNEL_BUTTON"] = "Añadir un embudo";
$MESS["CRM_ST_ADD_NEW_CATEGORY_BUTTON_LABEL"] = "Añadir embudo de ventas";
$MESS["CRM_ST_CATEGORY_DRAG_BUTTON"] = "Arrastre para ordenar los embudos";
$MESS["CRM_ST_CONFIRM_STAGE_REMOVE_TEXT"] = "¿Seguro que desea eliminar la etapa?";
$MESS["CRM_ST_DOT_TITLE"] = "Arrastre para crear un túnel";
$MESS["CRM_ST_EDIT_CATEGORY_TITLE"] = "Editar el nombre";
$MESS["CRM_ST_EDIT_RIGHTS_CATEGORY"] = "Editar ajustes";
$MESS["CRM_ST_ERROR_POPUP_CLOSE_BUTTON_LABEL"] = "Cerrar";
$MESS["CRM_ST_ERROR_POPUP_TITLE"] = "Error";
$MESS["CRM_ST_GENERATOR_HELP_BUTTON"] = "Leer acerca del impulso a las ventas";
$MESS["CRM_ST_GENERATOR_SETTINGS_LINK_LABEL"] = "Impulso a las ventas";
$MESS["CRM_ST_HELP_BUTTON"] = "Ayuda";
$MESS["CRM_ST_NOTIFICATION_CHANGES_SAVED"] = "Los cambios fueron guardados.";
$MESS["CRM_ST_REMOVE"] = "Eliminar";
$MESS["CRM_ST_REMOVE_CATEGORY"] = "Eliminar el embudo";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_BUTTON_LABEL"] = "eliminar";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_CANCEL_BUTTON_LABEL"] = "Cancelar";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_DESCRIPTION"] = "Al eliminar un embudo se eliminarán todos los túneles configurados";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_TITLE"] = "Eliminar el embudo \"#name#\"";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_REMOVE_BUTTON_LABEL"] = "eliminar el embudo";
$MESS["CRM_ST_ROBOTS_HELP_BUTTON"] = "Leer acerca de la automatización CRM";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Ventas automatizadas de Bitrix24 CRM<br><br>
	Cree escenarios para CRM para impulsar sus negociaciones: asigne tareas, planee reuniones, lance campañas publicitarias específicas y emita facturas. Las reglas de automatización guiarán a su gerente hacia los siguientes pasos en cada etapa de la negociación para completar el trabajo.<br><br>
	Los triggers reaccionan automáticamente a las acciones del cliente (cuando visita el sitio, llena el formulario, publica el comentario en las redes sociales o hace una llamada) y se lanza una regla de automatización para que los prospectos se conviertan en una negociación.<br><br>
	Las reglas de automatización y los triggers hacen todas las operaciones de rutina sin necesidad de entrenar a sus administradores: ¡Configure una vez y siga trabajando!<br><br>
	¡Agregue reglas de automatización e impulse sus ventas!<br><br>
	Esta herramienta está disponible únicamente en las suscripciones comerciales de Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Reglas de automatización";
$MESS["CRM_ST_ROBOT_ACTION_COPY"] = "Copiar por regla";
$MESS["CRM_ST_ROBOT_ACTION_MOVE"] = "Mover por regla";
$MESS["CRM_ST_ROBOT_SETTINGS_LINK_LABEL"] = "Configurar las reglas de automatización";
$MESS["CRM_ST_SAVE_ERROR"] = "Se produjo un error al guardar el embudo";
$MESS["CRM_ST_SETTINGS"] = "Configurar";
$MESS["CRM_ST_STAGES_DISABLED"] = "Etapas deshabilitadas";
$MESS["CRM_ST_STAGES_GROUP_FAIL"] = "Se produjo un fallo";
$MESS["CRM_ST_STAGES_GROUP_IN_PROGRESS"] = "En progreso";
$MESS["CRM_ST_STAGES_GROUP_SUCCESS"] = "Éxito";
$MESS["CRM_ST_TITLE_EDITOR_PLACEHOLDER"] = "Nuevo embudo";
$MESS["CRM_ST_TUNNEL_BUTTON_LABEL"] = "Túnel";
$MESS["CRM_ST_TUNNEL_BUTTON_TITLE"] = "Editar o eliminar el túnel";
$MESS["CRM_ST_TUNNEL_EDIT_ACCESS_DENIED"] = "Permisos insuficientes para crear un túnel.";
