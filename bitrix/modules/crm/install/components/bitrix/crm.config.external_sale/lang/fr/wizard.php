<?
$MESS["BPWC_WLC_NEED_FIRST_SYNC1"] = "L'importation doit être exécutée manuellement pour la première fois.";
$MESS["BPWC_WLC_NEED_FIRST_SYNC2"] = "Fonctionne";
$MESS["BPWC_WLC_NEED_FIRST_SYNC3"] = "Erreur d'importation :";
$MESS["BPWC_WLC_NO_RECORD"] = "L'identifiant de l'enregistrement n'est pas indiqué.";
$MESS["BPWC_WLC_WRONG_BP"] = "Enregistrement n'est pas trouvé.";
$MESS["BPWC_WNCW_BACK"] = "Précédent";
$MESS["BPWC_WNCW_CANCEL"] = "Annuler";
$MESS["BPWC_WNCW_CLOSE"] = "Achever";
$MESS["BPWC_WNCW_NEXT"] = "Suivant";
$MESS["BPWC_WNCW_SELECT"] = "Choisir";
$MESS["BPWC_WNCW_SESS_ERR"] = "Erreur de sécurité. Démarrez l'assistant encore une fois.";
$MESS["BPWC_WNCW_SHAGENT"] = "Nombre de synchronisations (en minutes, 0 - désactiver)";
$MESS["BPWC_WNCW_SHDATE_CREATE"] = "Créé le";
$MESS["BPWC_WNCW_SHDATE_UPDATE"] = "Date de la dernière modification";
$MESS["BPWC_WNCW_SHGROUPS"] = "Adresser les avis au groupe";
$MESS["BPWC_WNCW_SHLOGIN"] = "Identifiant de l'utilisateur du boutique en ligne";
$MESS["BPWC_WNCW_SHNAME"] = "Le nom de boutique en ligne";
$MESS["BPWC_WNCW_SHPASSWORD"] = "Mot de passe de l'utilisateur de la boutique en ligne";
$MESS["BPWC_WNCW_SHPERIOD"] = "Importer des données pour les derniers (jours)";
$MESS["BPWC_WNCW_SHPREF"] = "Préfixe de la dénomination de la transaction";
$MESS["BPWC_WNCW_SHPROB"] = "Probabilité de transactions (%)";
$MESS["BPWC_WNCW_SHPUBLIC"] = "Transactions disponibles à tous";
$MESS["BPWC_WNCW_SHRESPONS"] = "Responsable pour les nouvelles transactions";
$MESS["BPWC_WNCW_SHSTATUS"] = "Statut";
$MESS["BPWC_WNCW_SHURL"] = "URL de boutiques en ligne";
$MESS["BPWC_WNCW_SYNC_ERROR"] = "Erreur de chargement des commandes";
$MESS["BPWC_WNCW_SYNC_LOAD"] = "Importation de base de données de localisation à partir de sources externes";
$MESS["BPWC_WNCW_SYNC_STAT"] = "Statistique par la première importation";
$MESS["BPWC_WNCW_SYNC_STAT_COMPS"] = "Entreprises importées";
$MESS["BPWC_WNCW_SYNC_STAT_CONTS"] = "Nombre de contacts chargés";
$MESS["BPWC_WNCW_SYNC_STAT_DEALS"] = "Transactions chargées";
$MESS["BPWC_WNCW_SYNC_STOPPING"] = "Suspension";
$MESS["BPWC_WNCW_SYNC_STOP_LOAD"] = "Arrêter le chargement";
$MESS["BPWC_WNCW_SYNC_SUCCESS"] = "L'importation initiale s'est terminée avec succès.";
$MESS["BPWC_WNCW_SYNC_TERMINATED"] = "Importation interrompue par un utilisateur. Pas toutes les données ont pu être importés.";
$MESS["BPWC_WNCW_TITLE"] = "tape #STEP# de 6";
$MESS["BPWC_WNCW_TITLE_1"] = "tape 2";
$MESS["BPWC_WNCW_TITLE_2"] = "Tape 2 sur 6 : Réglage des paramètres de connexion";
$MESS["BPWC_WNCW_TITLE_3"] = "tape 3 de 6 : Mise en place de la première importation";
$MESS["BPWC_WNCW_TITLE_4"] = "tape 4 de 6 : Première importation";
$MESS["BPWC_WNCW_TITLE_5"] = "tape 5 des 6 : Réglage des paramètres de la synchronisation régulière";
$MESS["BPWC_WNCW_TITLE_6"] = "tape 6 sur 6 : Réglage accompli";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Le nom de l'utilisateur de la boutique en ligne n'est pas indiqué.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Le mot de passe de l'utilisateur de la boutique en ligne n'est pas indiqué.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Aucune adresse de boutique en ligne indiquée";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Aux termes de votre licence, la création de nouvelles liaisons avec les e-commerce est inaccessible.";
$MESS["CRM_CES_HEAD_1_0"] = "Assistant à la modification de l'intégration au e-commerce sur la base de 'Bitrix gestion du site";
$MESS["CRM_CES_HEAD_1_1"] = "Assistant à la modification de l'intégration au e-commerce sur la base de 'Bitrix gestion du site";
$MESS["CRM_CES_HEAD_2_0"] = "Spécifiez les paramètres de connexion de votre boutique en ligne";
$MESS["CRM_CES_HEAD_2_1"] = "Spécifiez les paramètres de connexion de votre boutique en ligne";
$MESS["CRM_CES_HEAD_3_1"] = "Configurer l'importation initiale des données de la boutique en ligne";
$MESS["CRM_CES_HEAD_4_1"] = "Importation des données de la boutique en ligne. La durée de ce processus dépend du nombre des commandes à importer de la boutique en ligne. S'il vous plaît, ne fermez pas cette fenêtre, attendez la fin de l'importation.";
$MESS["CRM_CES_HEAD_5_1"] = "Réglage des paramètres de la synchronisation régulière du système CRM avec e-commerce";
$MESS["CRM_CES_HEAD_6_1"] = "Intégration avec e-boutique.";
$MESS["CRM_CES_STEP_1"] = "Cet assistant vous aidera à intégrer sans effort votre boutique en ligne et CRM dans le cadre Bitrix24 ou portail d'entreprise. L'assistant comprend les principales étapes suivantes : <ul><li>Spécifiez l'URL de la boutique en ligne et l'utilisateur au nom de laquelle la synchronisation de votre boutique en ligne et CRM aura lieu. Vous devrez fournir l'identifiant et le mot de passe pour cet utilisateur ; l'utilisateur doit disposer des autorisations d'accès appropriées.<br /> <br /> </li> <li>Configurez l'importation initiale des données depuis la boutique en ligne vers CRM. Il faut déterminer le temps pour lequel vous voulez importer les commandes et la façon de les répartir entre les managers.<br /> <br /> </li> <li>Importation de données principale. La réussite de cette étape est nécessaire pour mettre en place la synchronisation périodique des données entre la boutique en ligne et CRM.<br /> <br /> </li> <li>Réglage des paramètres de la synchronisation. Configurez les paramètres de synchronisation périodique entre la boutique en ligne et le CRM. Il vous sera demandé de spécifier la période de synchronisation et les règles pour créer de nouvelles transactions dans le CRM.</ul>";
$MESS["CRM_CES_STEP_6"] = "Félicitations ! Maintenant, l'intégration de votre boutique en ligne et CRM est configurée. Les nouvelles transactions, les contacts et les entreprises seront automatiquement ajoutés au fur et à mesure qu'arrivent de nouvelles données. Vous recevrez les notifications de toutes synchronisations réussies dans le 'Fil d'actualités', vous obtiendrez également une notification en cas de problèmes avec la synchronisation.<br /><br />Vous pouvez toujours modifier les paramètres de synchronisation à la page de gestion d'intégration avec la boutique en ligne (<a href='#URL#'>CRM &gt; Paramètres &gt; Connexion avec les boutiques en ligne</a>).";
$MESS["CRM_EXT_SALE_C1ERROR_CONNECT"] = "Erreur de connexion à la boutique en ligne. Peut-être le site spécifié n'est pas la boutique, établie sur la base de 'Bitrix : Commande du site.' Ou la boutique ne fonctionne pas.";
$MESS["CRM_EXT_SALE_C1NO_AUTH"] = "On n'a pas réussi à s'autoriser dans e-commerce.";
$MESS["CRM_EXT_SALE_C1NO_BITRIX"] = "CRM n'arrive pas à se connecter au site indiqué parce que ce n'est pas un magasin créé à la base de 'Bitrix : Gestion du site'.";
$MESS["CRM_EXT_SALE_C1NO_CONNECT"] = "Le lien pour aller à e-commerce n' pas été trouvé.";
$MESS["CRM_EXT_SALE_C1STATUS"] = "Il est rendu le statut %s %s. Il est possible que le site indiqué - ce n'est pas le magasin créé à la base du logiciel 'Bitrix : Gestion du site', ou il vous faut faire la mise à jour du magasin à l'aide d'un système des mises à jour.";
$MESS["CRM_EXT_SALE_C1SUCCESS"] = "Connexion établie.";
$MESS["CRM_EXT_SALE_ERROR_DETAILS"] = "Informations sur une erreur";
$MESS["CRM_PERMISSION_DENIED"] = "Vous n'avez pas de droits pour créer l'intégration avec le magasin en ligne.";
?>