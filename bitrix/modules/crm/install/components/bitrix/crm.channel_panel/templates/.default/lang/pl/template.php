<?
$MESS["CRM_CHANNEL_PANEL_CLOSE"] = "Ukryj";
$MESS["CRM_CHANNEL_PANEL_CLOSE_CONFIRM"] = "Jesteś pewien, że chcesz ukryć opis dostępnych kanałów komunikacji w swoim CRM?";
$MESS["CRM_CHANNEL_PANEL_CLOSE_TITLE"] = "Ukryj opis";
$MESS["CRM_CHANNEL_PANEL_CONNECT_BUTTON"] = "Połącz";
?>