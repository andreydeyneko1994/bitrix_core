<?
$MESS["CRM_CHANNEL_1C_CAPTION"] = "1C konektor";
$MESS["CRM_CHANNEL_1C_LEGEND"] = "Eksport historii sprzedaży i klientów do CRM. Łączy sprzedaż offline z CRM online.";
$MESS["CRM_CHANNEL_CALLBACK_CAPTION"] = "Oddzwoń";
$MESS["CRM_CHANNEL_CALLBACK_LEGEND"] = "Darmowy widżet oddzwaniania. Klient prosi o telefon, nowe połączenie telefoniczne tworzone jest z Bitrix24 i zapisane w CRM.";
$MESS["CRM_CHANNEL_CHAT_CAPTION"] = "Czat na żywo";
$MESS["CRM_CHANNEL_CHAT_LEGEND"] = "Darmowy widżet czatu. Wszystkie zapytania klientów są wysyłane do Bitrix24; wszystkie rozmowy są zapisane w CRM.";
$MESS["CRM_CHANNEL_EMAIL_CAPTION"] = "Konta Email CRM";
$MESS["CRM_CHANNEL_EMAIL_LEGEND"] = "Nowy email zakłada nowy lead. Wiadomość z emaila dodawana jest do opisu i zakładana jest nowa aktywność.";
$MESS["CRM_CHANNEL_OPEN_LINE_CAPTION"] = "Open Channels";
$MESS["CRM_CHANNEL_OPEN_LINE_LEGEND"] = "Migruj rozmowy z klientami z serwisów społecznościowych do czatu Bitrix24. Cała konwersacja jest zapisywana w CRM.";
$MESS["CRM_CHANNEL_TELEPHONY_CAPTION"] = "Telefonia";
$MESS["CRM_CHANNEL_TELEPHONY_LEGEND"] = "Wszystkie połączenia telefoniczne są zapisywane w CRM. Nowy lead jest tworzony nawet, jeżeli połączenie nie zostało odebrane.";
$MESS["CRM_CHANNEL_WEB_FORM_CAPTION"] = "formularze CRM";
$MESS["CRM_CHANNEL_WEB_FORM_LEGEND"] = "Aplikacje, formularze, formularze rejestracji są dodawane bezpośrednio do CRM. Formularz CRM jest łatwy do utworzenia, nawet jeżeli nie masz własnej strony internetowej.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
?>