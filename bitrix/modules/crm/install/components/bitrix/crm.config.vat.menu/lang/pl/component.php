<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_VAT_ADD"] = "Dodaj stawkę VAT";
$MESS["CRM_VAT_ADD_TITLE"] = "Utwórz nową stawkę VAT";
$MESS["CRM_VAT_DELETE"] = "Usuń stawkę podatku";
$MESS["CRM_VAT_DELETE_DLG_BTNTITLE"] = "Usuń stawkę VAT";
$MESS["CRM_VAT_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć tę stawkę?";
$MESS["CRM_VAT_DELETE_DLG_TITLE"] = "Usuń stawkę VAT";
$MESS["CRM_VAT_DELETE_TITLE"] = "Usuń stawkę VAT";
$MESS["CRM_VAT_EDIT"] = "Edytuj Stawkę Podatku";
$MESS["CRM_VAT_EDIT_TITLE"] = "Otwórz stawkę VAT do edycji";
$MESS["CRM_VAT_LIST"] = "Stawki VAT";
$MESS["CRM_VAT_LIST_TITLE"] = "Wyświetl wszystkie stawki VAT";
$MESS["CRM_VAT_SETTINGS"] = "Ustawienia";
$MESS["CRM_VAT_SETTINGS_TITLE"] = "Konfiguruj system podatkowy";
$MESS["CRM_VAT_SHOW"] = "Wyświetl stawkę";
$MESS["CRM_VAT_SHOW_TITLE"] = "Wyświetl stawkę VAT";
?>