<?php
$MESS["CRM_TRACKING_REPORT_SOURCE_AD_ACCOUNT"] = "Konto reklamowe";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_ACTIONS"] = "Działania";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_CODE"] = "ID";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_CPC"] = "CPC";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_CTR"] = "CTR";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_DEALS"] = "Deale";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_IMPRESSIONS"] = "Wyświetlenia";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_INCOME"] = "Zysk";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_LEADS"] = "Leady";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_ORDERS"] = "Zamówienia";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_OUTCOME"] = "Koszt";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_ROI"] = "ROI";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_STATUS"] = "Oczekiwanie na status leadu";
$MESS["CRM_TRACKING_REPORT_SOURCE_COLUMN_SUCCESS_DEALS"] = "Wygrane deale";
