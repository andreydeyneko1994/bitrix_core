<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ACTIVE"] = "Activité";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ANONYMOUS"] = "Anonyme";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_C_SORT"] = "Trier";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_DESCRIPTION"] = "Description";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ID"] = "ID";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_NAME"] = "Nom";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_CREATE_TITLE"] = "Créer un groupe de clients";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_SAVE_ERROR"] = "Une erreur est survenue lors de l'enregistrement du client.";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_TITLE"] = "Modifier un groupe de clients";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
