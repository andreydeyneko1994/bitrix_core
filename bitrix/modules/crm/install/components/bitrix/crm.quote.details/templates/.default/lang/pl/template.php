<?
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Potrzebujesz uprawnień do tworzenia deala i faktury, aby kontynuować.";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Kontynuuj";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wybierz obiekty, w których będą stworzone brakujące pola";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Zostaną utworzone następujące pola";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "Wybrane elementy nie mają pól, które mogą przechowywać dane oferty. Wybierz elementy, w których powstaną brakujące pola, mogące zebrać wszystkie dostępne informacje.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Utwórz element z oferty";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Błąd ogólny.";
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Trwa tworzenie oferty...";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Zapisz";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Lejek";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Ustawienia deala";
?>