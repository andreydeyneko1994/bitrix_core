<?
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Você precisa de permissão de criação de venda e fatura para prosseguir.";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Selecionar as entidades nas quais os campos faltantes serão criados";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Estes campos serão criados";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos que podem armazenar dados de orçamentos. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Crie entidade no orçamento";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Erro genérico.";
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Você está criando um orçamento...";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferências de venda";
?>