<?
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Necesita una negociación y una factura para crear un permiso para continuar.";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Seleccionar entidades en las que se crearán los campos faltantes";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Estos campos serán creados";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "Las entidades seleccionadas no tienen campos que puedan almacenar datos de cotización. Seleccione las entidades en las que se crearán los campos faltantes para acomodar toda la información disponible.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Crear entidad basada en la cotización";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Error generico.";
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Ahora estás creando una cotización...";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferencias de las negociaciones";
?>