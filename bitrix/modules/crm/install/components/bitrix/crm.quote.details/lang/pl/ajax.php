<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Nowa oferta";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "Błąd usuwania oferty.";
$MESS["CRM_QUOTE_NOT_FOUND"] = "Oferta nie została odnaleziona.";
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Wystąpił błąd zapisywania produktów.";
?>