<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Nueva Cotización";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "Error al eliminar la cotización.";
$MESS["CRM_QUOTE_NOT_FOUND"] = "La cotización no fue encontrada";
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Se produjo un error al guardar productos.";
?>