<?php
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_0"] = "Nunca";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_1"] = "Diariamente";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_7"] = "Una vez a la semana";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_14"] = "Una vez cada dos semanas";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_30"] = "Una vez al mes";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_182"] = "Una vez cada seis meses";
