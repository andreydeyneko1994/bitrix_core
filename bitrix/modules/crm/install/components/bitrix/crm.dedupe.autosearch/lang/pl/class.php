<?php
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_0"] = "Nigdy";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_1"] = "Codziennie";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_7"] = "Raz w tygodniu";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_14"] = "Raz na dwa tygodnie";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_30"] = "Raz w miesiącu";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_182"] = "Raz na sześć miesięcy";
