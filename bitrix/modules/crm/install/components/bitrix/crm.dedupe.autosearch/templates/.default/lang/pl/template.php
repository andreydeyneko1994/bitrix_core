<?php
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_CONFLICTED_TEXT"] = "Musisz przejrzeć i ręcznie scalić następującą liczbę duplikatów: #CONFLICTS_COUNT#.";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_BUTTON"] = "Sprawdź teraz";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_LIST_LINK"] = "Znalezione duplikaty";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT"] = "Liczba automatycznie scalonych duplikatów: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_COMPANY"] = "Liczba automatycznie scalonych zduplikowanych firm: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_CONTACT"] = "Liczba automatycznie scalonych zduplikowanych kontaktów: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_LEAD"] = "Liczba automatycznie scalonych zduplikowanych leadów: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_CONFLICTS_FOUND"] = "Nie scalono następującej liczby duplikatów: #CONFLICTS_COUNT#.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_INTERVAL_TITLE"] = "Uruchom wyszukiwarkę duplikatów";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_NOTE"] = "System spróbuje znaleźć i scalić duplikaty zgodnie z wybranymi opcjami.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_TITLE"] = "Automatyczne wyszukiwanie duplikatów";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_BUTTON"] = "Włącz";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT"] = "Czy chcesz włączyć funkcję automatycznego wyszukiwania i scalania duplikatów? Będzie działać z określoną przez Ciebie częstotliwością.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_COMPANY"] = "W Twoim CRM znaleziono następującą liczbę zduplikowanych firm: #TOTAL_ENTITIES_COUNT#.<br>Spośród nich #FOUND_ITEMS_COUNT# to dokładne dopasowania według wszystkich wybranych pól.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_CONTACT"] = "W Twoim CRM znaleziono następującą liczbę zduplikowanych kontaktów: #TOTAL_ENTITIES_COUNT#.<br>Spośród nich #FOUND_ITEMS_COUNT# to dokładne dopasowania według wszystkich wybranych pól.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_LEAD"] = "W Twoim CRM znaleziono następującą liczbę zduplikowanych leadów: #TOTAL_ENTITIES_COUNT#.<br>Spośród nich #FOUND_ITEMS_COUNT# to dokładne dopasowania według wszystkich wybranych pól.";
