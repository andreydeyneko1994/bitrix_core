<?php
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_CONFLICTED_TEXT"] = "Tendrá que revisar y fusionar manualmente #CONFLICTS_COUNT# duplicados.";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_BUTTON"] = "Revisar ahora";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_LIST_LINK"] = "Duplicados encontrados";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT"] = "Duplicados fusionados automáticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_COMPANY"] = "Empresas duplicadas fusionadas automáticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_CONTACT"] = "Contactos duplicados fusionados automáticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_LEAD"] = "Prospectos duplicados fusionados automáticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_CONFLICTS_FOUND"] = "#CONFLICTS_COUNT# duplicados no se fusionaron.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_INTERVAL_TITLE"] = "Ejecutar el buscador de duplicados";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_NOTE"] = "El sistema intentará buscar y fusionar los duplicados de acuerdo con las opciones seleccionadas.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_TITLE"] = "Búsqueda automática de duplicados";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_BUTTON"] = "Habilitar";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT"] = "¿Le gustaría habilitar la función de búsqueda automática y fusión de duplicados? Se ejecutará con la frecuencia que especifique.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_COMPANY"] = "Encontramos #TOTAL_ENTITIES_COUNT# empresas duplicadas en su CRM.<br>#FOUND_ITEMS_COUNT# de ellas son una coincidencia exacta en todos los campos que seleccionó.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_CONTACT"] = "Encontramos #TOTAL_ENTITIES_COUNT# contactos duplicados en su CRM.<br>#FOUND_ITEMS_COUNT# de ellos son una coincidencia exacta en todos los campos que seleccionó.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_LEAD"] = "Encontramos #TOTAL_ENTITIES_COUNT# prospectos duplicados en su CRM.<br>#FOUND_ITEMS_COUNT# de ellos son una coincidencia exacta en todos los campos que seleccionó.";
