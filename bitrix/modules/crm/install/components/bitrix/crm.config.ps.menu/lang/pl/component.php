<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PS_ADD"] = "Dodawanie";
$MESS["CRM_PS_ADD_TITLE"] = "Dodaj nowy system płatności";
$MESS["CRM_PS_DELETE"] = "Usuń";
$MESS["CRM_PS_DELETE_DLG_BTNTITLE"] = "Usuń";
$MESS["CRM_PS_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć ten system płatności?";
$MESS["CRM_PS_DELETE_DLG_TITLE"] = "Usuń system płatności";
$MESS["CRM_PS_DELETE_TITLE"] = "Usuń system płatności";
$MESS["CRM_PS_LIST"] = "Systemy płatności";
$MESS["CRM_PS_LIST_TITLE"] = "Wyświetl wszystkie systemy płatności";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>