<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_PS_ADD"] = "Ajouter";
$MESS["CRM_PS_ADD_TITLE"] = "Créer un nouveau système de paiement";
$MESS["CRM_PS_DELETE"] = "Supprimer";
$MESS["CRM_PS_DELETE_DLG_BTNTITLE"] = "Supprimer";
$MESS["CRM_PS_DELETE_DLG_MESSAGE"] = "Êtes-vous sûr d'éliminer le système de paiement ?";
$MESS["CRM_PS_DELETE_DLG_TITLE"] = "Elimination du système de paiement";
$MESS["CRM_PS_DELETE_TITLE"] = "Elimination du système de paiement";
$MESS["CRM_PS_LIST"] = "Liste";
$MESS["CRM_PS_LIST_TITLE"] = "Liste des systèmes de paiement";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module Boutique en ligne n'est pas installé.";
?>