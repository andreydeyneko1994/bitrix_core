<?
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Anuluj";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Zapisz";
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Wyświetl ustawienia CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Otwórz ustawienia CRM";
$MESS["CRM_TRACKER_SECTION_NAME"] = "Konfiguruj tracker";
$MESS["CRM_TRACKER_SECTION_TITLE"] = "Ustawienia wstępne";
$MESS["CRM_TRACKER_SETTINGS_COMPANY"] = "Dla firmy";
$MESS["CRM_TRACKER_SETTINGS_CONTACT"] = "Dla kontaktu";
?>