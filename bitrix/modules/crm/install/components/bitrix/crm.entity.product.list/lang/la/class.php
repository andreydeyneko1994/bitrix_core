<?php
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_AVAILABLE"] = "Disponible";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_AVAILABLE_HINT_2"] = "Existencias disponibles = Existencias en el almacén - Cantidad reservada. #HELPER_HTML_LINK#";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_BASE_PRICE"] = "Precio base";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_CURRENCY"] = "Moneda";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_CUSTOM_PRICE"] = "Precio personalizado";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_DEDUCTED"] = "Enviado";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_DIMENSIONS"] = "Dimensiones";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_DISCOUNT_PRICE"] = "Descuento";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_DISCOUNT_ROW"] = "Importe del descuento";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_HINT_ABOUT_TITLE"] = "Más información";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_MAIN_INFO"] = "Producto";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_MEASURE"] = "Unidad de medida";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_NOTES"] = "Tipo de precio";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PRICE"] = "Precio";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PRICE_TYPE_ID"] = "ID del tipo de precio";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PRODUCT_ID"] = "ID del producto";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PRODUCT_NAME"] = "Nombre";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PRODUCT_PRICE_ID"] = "ID del precio";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_PROPERTIES"] = "Propiedades";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_QUANTITY"] = "Cantidad";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_RESERVED"] = "Reservado";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_RESERVED_INTO_DEAL"] = "Reservado (en la negociación)";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_RESERVED_INTO_DEAL_HINT"] = "Cantidad del producto aún reservada después de un envío parcial";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_SORT"] = "Clasificación";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_STORE_FROM_INFO"] = "Almacén de abastecimiento";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_STORE_FROM_INFO_HINT"] = "Seleccione el almacén en el que desea reservar las existencias";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_STORE_FROM_RESERVED"] = "Reservado";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_STORE_FROM_RESERVED_HINT"] = "Cantidad del producto reservado para la negociación. #HELPER_HTML_LINK#";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_STORE_FROM_TITLE"] = "Almacén";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_SUMM"] = "Importe";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_TAX_INCLUDED"] = "Impuesto incluido en el precio";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_TAX_RATE"] = "Impuesto";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_TAX_SUM"] = "Impuesto total";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_VAT_INCLUDED"] = "Impuesto incluido";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_VAT_RATE"] = "Tasa de impuestos";
$MESS["CRM_ENTITY_PRODUCT_LIST_COLUMN_WEIGHT"] = "Peso";
$MESS["CRM_ENTITY_PRODUCT_LIST_EMPTY_STORE_TITLE"] = "Sin título";
$MESS["CRM_ENTITY_PRODUCT_LIST_ERR_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_ENTITY_PRODUCT_LIST_ERR_BAD_ENTITY_ID"] = "ID de la entidad propietaria inválido";
$MESS["CRM_ENTITY_PRODUCT_LIST_ERR_BAD_ENTITY_TYPE"] = "Tipo de entidad propietaria inválido";
$MESS["CRM_ENTITY_PRODUCT_LIST_SETTING_DISCOUNTS_DESC"] = "Descripción del descuento";
$MESS["CRM_ENTITY_PRODUCT_LIST_SETTING_DISCOUNTS_TITLE"] = "Mostrar el descuento";
$MESS["CRM_ENTITY_PRODUCT_LIST_SETTING_TAXES_DESC"] = "Descripción del impuesto";
$MESS["CRM_ENTITY_PRODUCT_LIST_SETTING_TAXES_TITLE"] = "Mostrar los impuestos";
$MESS["CRM_ENTITY_PRODUCT_LIST_SETTING_WAREHOUSE_TITLE"] = "Habilitar la administración del inventario";
