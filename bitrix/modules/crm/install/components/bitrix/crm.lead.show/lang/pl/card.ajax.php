<?
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_LAST_NAME"] = "Nazwisko";
$MESS["CRM_COLUMN_NAME"] = "Imię";
$MESS["CRM_COLUMN_PHONE"] = "Telefon";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produkty";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Drugie Imię";
$MESS["CRM_COLUMN_STATUS"] = "Status";
$MESS["CRM_OPER_EDIT"] = "Edytuj";
$MESS["CRM_OPER_SHOW"] = "Wyświetl";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Informacje kontaktowe";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Telefon";
?>