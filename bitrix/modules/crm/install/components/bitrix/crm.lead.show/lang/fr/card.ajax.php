<?
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_LAST_NAME"] = "Nom de famille";
$MESS["CRM_COLUMN_NAME"] = "Prénom";
$MESS["CRM_COLUMN_PHONE"] = "Téléphone";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produits";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Deuxième prénom";
$MESS["CRM_COLUMN_STATUS"] = "Statut";
$MESS["CRM_OPER_EDIT"] = "Éditer";
$MESS["CRM_OPER_SHOW"] = "Affichage";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Information de contact";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Appelant inconnu";
?>