<?php
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_FAIL"] = "O número não suporta rastreamento de chamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_SUCCESS"] = "Número suporta rastreamento de chamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_UNKNOWN"] = "Falha ao rastrear a chamada para este número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE"] = "Última data de verificação de rastreamento de chamada";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE_NO"] = "nenhum";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_CALL"] = "Ligar para \"%number%\"";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_END"] = "Aguarde os primeiros toques e desligue";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_WAIT"] = "Aguardando chamada";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER"] = "Verificação do número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_DESC"] = "Verifique se o seu número de telefone e provedor de telefonia oferecem suporte de rastreamento de chamadas. Para fazer isso, digite o número de telefone que você usará para fazer a chamada e faça a ligação.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_INPUT"] = "Digite o número de telefone que você usará para ligar";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_START"] = "Iniciar verificação";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_STOP"] = "Parar verificação";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT"] = "Suporte de rastreamento de chamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT_DESC"] = "Você recebeu chamadas neste número. O rastreamento de chamadas está disponível.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN"] = "Nenhuma chamada recebida neste número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN_DESC"] = "Você ainda não recebeu nenhuma chamada neste número. O rastreamento de chamadas pode estar indisponível. Você pode verificar o suporte de rastreamento de chamadas deste número agora ou aguardar chamadas dos seus clientes.";
