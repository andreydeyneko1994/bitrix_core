<?
$MESS["CRM_TRACKING_CHANNEL_POOL_AV"] = "Disponible";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_EMAIL"] = "Adresses e-mail disponibles";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_PHONE"] = "Numéros disponibles";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD"] = "Ajouter";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_EMAIL"] = "ajouter un e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_PHONE"] = "ajouter un numéro";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ALLOCATE"] = "Affecter automatiquement";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_CANCEL"] = "Annuler";
$MESS["CRM_TRACKING_CHANNEL_POOL_DELETE_CONFIRM"] = "Voulez-vous supprimer %code% ? Cette action est irréversible.";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_EMAIL"] = "Ajouter des adresses e-mail aux sources de publicités";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_PHONE"] = "Ajouter des numéros de téléphone aux sources de publicités";
$MESS["CRM_TRACKING_CHANNEL_POOL_FORMAT_PHONE"] = "Format des numéros de téléphone";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_EMAIL"] = "Affecter des adresses e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_PHONE"] = "Affecter des numéros de téléphone";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_EMAIL"] = "Adresses e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_PHONE"] = "Numéros de téléphone";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_EMAIL"] = "Saisissez l'adresse e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_PHONE"] = "Saisissez le numéro";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_SUCCESS"] = "Ce numéro prend en charge le suivi des appels";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_UNKNOWN"] = "Ce numéro peut ne pas prendre en charge le suivi des appels. Cliquez pour vérifier.";
?>