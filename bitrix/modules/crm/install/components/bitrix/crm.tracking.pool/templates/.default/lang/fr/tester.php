<?php
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_FAIL"] = "Le numéro ne prend pas en charge le suivi des appels";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_SUCCESS"] = "Le numéro prend en charge le suivi des appels";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_UNKNOWN"] = "Impossible de suivre l'appel vers ce numéro";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE"] = "Date de vérification du suivi du dernier appel";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE_NO"] = "aucune";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_CALL"] = "Appeler le \"%number%\"";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_END"] = "Veuillez attendre les premières sonneries et raccrocher";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_WAIT"] = "En attente d'un appel entrant";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER"] = "Vérification des numéros";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_DESC"] = "Vérifiez si votre numéro de téléphone et votre fournisseur de téléphonie prennent en charge le suivi des appels. Pour ce faire, saisissez le numéro de téléphone à partir duquel vous allez appeler et passez l'appel.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_INPUT"] = "Saisissez le numéro de téléphone d'où vous appellerez";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_START"] = "Démarrer la vérification";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_STOP"] = "Arrêter la vérification";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT"] = "Suivi des appels pris en charge";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT_DESC"] = "Vous avez reçu des appels à ce numéro. Le suivi des appels est disponible.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN"] = "Aucun appel reçu sur ce numéro";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN_DESC"] = "Vous n'avez pas encore reçu d'appels sur ce numéro. Le suivi des appels n'est peut-être pas disponible. Vous pouvez vérifier maintenant sa prise en charge pour ce numéro ou attendre des appels de vos clients.";
