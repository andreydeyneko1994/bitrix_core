<?
$MESS["CRM_TRACKING_CHANNEL_POOL_AV"] = "Disponible";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_EMAIL"] = "Direcciones de correo electrónico disponibles";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_PHONE"] = "Numeros disponibles";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD"] = "Agregar";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_EMAIL"] = "agregar correo electrónico";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_PHONE"] = "agregar número";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ALLOCATE"] = "Asignación automática";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_CANCEL"] = "Cancelar";
$MESS["CRM_TRACKING_CHANNEL_POOL_DELETE_CONFIRM"] = "¿Desea eliminar %code%? Esta acción no se puede deshacer.";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_EMAIL"] = "Asignar direcciones de correo electrónico a las fuentes de anuncios";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_PHONE"] = "Asignar números de teléfono a las fuentes de anuncios";
$MESS["CRM_TRACKING_CHANNEL_POOL_FORMAT_PHONE"] = "Formato del número de teléfono";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_EMAIL"] = "Asignar direcciones de correo electrónico";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_PHONE"] = "Asignar números de teléfono";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_EMAIL"] = "Direcciones de correo electrónico";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_PHONE"] = "Números de teléfono";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_EMAIL"] = "Introduzca la dirección de correo electrónico";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_PHONE"] = "Introduzca el número";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_SUCCESS"] = "Este número admite el seguimiento de llamadas.";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_UNKNOWN"] = "Es posible que este número no admita el seguimiento de llamadas. Haga clic para verificar.";
?>