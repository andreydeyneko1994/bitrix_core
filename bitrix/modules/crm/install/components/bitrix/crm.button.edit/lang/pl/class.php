<?
$MESS["CRM_BUTTON_EDIT_ERROR_FILE"] = "Nie można wygenerować kodu widżetu. Proszę zapisz ponownie formularz.";
$MESS["CRM_BUTTON_EDIT_TITLE_ADD"] = "Utwórz widżet";
$MESS["CRM_BUTTON_EDIT_TITLE_EDIT"] = "Edytuj widżet";
$MESS["CRM_BUTTON_EDIT_UNIT_MINUTE"] = "min";
$MESS["CRM_BUTTON_EDIT_UNIT_SECOND"] = "s";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>