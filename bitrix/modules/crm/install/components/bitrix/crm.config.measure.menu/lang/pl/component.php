<?
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MEASURE_ADD"] = "Dodawanie";
$MESS["CRM_MEASURE_ADD_TITLE"] = "Utwórz nową jendostkę miary";
$MESS["CRM_MEASURE_DELETE"] = "Usuń";
$MESS["CRM_MEASURE_DELETE_DLG_BTNTITLE"] = "Usuń jednostkę";
$MESS["CRM_MEASURE_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć tę jednostkę?";
$MESS["CRM_MEASURE_DELETE_DLG_TITLE"] = "Usuń jednostkę miary";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Usuń jednostkę miary";
$MESS["CRM_MEASURE_EDIT"] = "Edytuj";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Edytuj jednostkę  miary";
$MESS["CRM_MEASURE_LIST"] = "Wszystkie jendostki";
$MESS["CRM_MEASURE_LIST_TITLE"] = "Wyświetl wszystkie jednostki miary";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>