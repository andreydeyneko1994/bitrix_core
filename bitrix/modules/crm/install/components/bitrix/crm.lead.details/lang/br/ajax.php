<?php
$MESS["CRM_LEAD_DELETION_ERROR"] = "Erro ao excluir negócio.";
$MESS["CRM_LEAD_NOT_FOUND"] = "O lead não foi encontrado.";
$MESS["CRM_LEAD_PRODUCT_ROWS_SAVING_ERROR"] = "Ocorreu um erro ao salvar produtos.";
