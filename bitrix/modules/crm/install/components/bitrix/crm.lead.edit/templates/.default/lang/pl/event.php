<?
$MESS["CRM_LEAD_EDIT_EVENT_CANCELED"] = "Działanie zostało anulowane. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli nadal jesteś na tej stronie, zamknij ją ręcznie.";
$MESS["CRM_LEAD_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Utworzono lead <a href='#URL#'>#TITLE#</a>. Teraz nastąpi przekierowanie do strony początkowej. Jeśli bieżąca strona jest nadal wyświetlana, zamknij ją ręcznie.";
?>