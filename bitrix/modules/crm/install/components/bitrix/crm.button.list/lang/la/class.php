<?
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Proceso de negocio no está instalado.";
$MESS["CRM_BUTTON_LIST_TITLE"] = "Iconos";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Moneda no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
?>