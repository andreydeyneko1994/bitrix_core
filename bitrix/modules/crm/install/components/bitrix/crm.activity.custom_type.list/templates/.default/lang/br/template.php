<?
$MESS["CRM_ACT_CUST_TYPE_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_ACT_CUST_TYPE_DEFAULT_NAME"] = "Novo tipo de atividade";
$MESS["CRM_ACT_CUST_TYPE_DELETE"] = "Excluir";
$MESS["CRM_ACT_CUST_TYPE_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir \"#NAME#\"?";
$MESS["CRM_ACT_CUST_TYPE_DELETE_TITLE"] = "Excluir pipeline";
$MESS["CRM_ACT_CUST_TYPE_EDIT"] = "Editar";
$MESS["CRM_ACT_CUST_TYPE_EDIT_TITLE"] = "Editar tipo de atividade";
$MESS["CRM_ACT_CUST_TYPE_ERROR_TITLE"] = "Erro ao salvar";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME"] = "Nome";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "Está faltando o valor do campo \"Nome\".";
$MESS["CRM_ACT_CUST_TYPE_FIELD_SORT"] = "Classificação";
$MESS["CRM_ACT_CUST_TYPE_TITLE_CREATE"] = "Criar novo tipo de atividade";
$MESS["CRM_ACT_CUST_TYPE_TITLE_EDIT"] = "Editar tipo de atividade";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT"] = "Editar campos personalizados";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT_TITLE"] = "Editar campos personalizados para este tipo de atividade";
$MESS["CRM_ALL"] = "Total";
?>