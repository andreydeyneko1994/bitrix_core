<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_REQUISITE_BANK_DETAILS_EDITOR_FIELD"] = "Dane bankowe";
$MESS["CRM_REQUISITE_EDIT_CUSTOM_FORM_HTML"] = "Dodatkowe do danych";
$MESS["CRM_REQUISITE_EDIT_ENTITY_BINDING"] = "Powiązania";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_EDIT_DENIED"] = "Odmowa dostępu do edycji danych kontaktu lub firmy.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_NOT_FOUND"] = "Nie znaleziono firmy do edycji kontaktowych lub danych firmy.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_EDIT_DENIED"] = "Odmowa dostępu do edycji danych kontaktu lub firmy.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_NOT_FOUND"] = "Nie znaleziono kontaktu do edycji danych kontaktowych lub danych firmy.";
$MESS["CRM_REQUISITE_EDIT_ERR_ENTITY_TYPE_ID"] = "Do edycji podano nieprawidłowy typ jednostki.";
$MESS["CRM_REQUISITE_EDIT_ERR_INVALID_DATA"] = "Podano niepoprawne dane kontaktowe lub dane firmy.";
$MESS["CRM_REQUISITE_EDIT_ERR_NOT_FOUND"] = "Nie można uzyskać kontaktu ani danych firmy (ID = #ID#)";
$MESS["CRM_REQUISITE_EDIT_ERR_PRESET_NOT_SELECTED"] = "Nie wskazano szablony danych kontaktu ani firmy.";
$MESS["CRM_REQUISITE_EDIT_POPUP_RESPONSE"] = "Dane wewnętrzne";
$MESS["CRM_REQUISITE_EDIT_PRESET_NAME"] = "Szablon";
$MESS["CRM_REQUISITE_EDIT_PRESET_NOT_SELECTED"] = "(nie wybrane)";
$MESS["CRM_SECTION_COMPANY_REQUISITE_INFO"] = "Profil";
$MESS["CRM_SECTION_COMPANY_REQUISITE_VALUES"] = "Wartości danych firmy";
$MESS["CRM_SECTION_CONTACT_REQUISITE_INFO"] = "Profil";
$MESS["CRM_SECTION_CONTACT_REQUISITE_VALUES"] = "Wartości danych kontaktu";
?>