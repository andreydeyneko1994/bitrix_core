<?
$MESS["CRM_ML_MODEL_LIST_AVAILABLE_MODELS"] = "Modelos disponibles";
$MESS["CRM_ML_MODEL_LIST_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_ML_MODEL_LIST_BUTTON_DISABLE"] = "Desactivar";
$MESS["CRM_ML_MODEL_LIST_BUTTON_TRAIN_FREE_OF_CHARGE"] = "Entrenar gratuitamente";
$MESS["CRM_ML_MODEL_LIST_CONFIRMATION"] = "Confirmar la acción";
$MESS["CRM_ML_MODEL_LIST_DEAL_SCORING_DISABLE"] = "Desactivar el scoring de negociaciones para este pipeline";
$MESS["CRM_ML_MODEL_LIST_DISABLE_DEAL_SCORING"] = "¿Desactivar el scoring de negociaciones para este pipeline?";
$MESS["CRM_ML_MODEL_LIST_DISABLE_LEAD_SCORING"] = "¿Desactivar el scoring de prospectos?";
$MESS["CRM_ML_MODEL_LIST_HELP"] = "Ayuda";
$MESS["CRM_ML_MODEL_LIST_LEAD_SCORING_DISABLE"] = "Desactivar el scoring de prospectos";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P1"] = "¿Es intuición lo que su gerente de ventas toma en cuenta cuando administra los negocios? Traiga nuevos clientes a la cima de su lista.";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P2"] = "AI Scoring analizará las negociaciones existentes para mostrar su probabilidad de éxito. El sistema ayudará a sus empleados a identificar las áreas que requieren más atención. ¡No pierda su tiempo con aquellos que no tienen la intención de convertirse en sus clientes!";
$MESS["CRM_ML_MODEL_LIST_SCORING_ENOUGH_DATA"] = "Su CRM tiene datos suficientes para capacitar el modelo de AI Scoring.";
$MESS["CRM_ML_MODEL_LIST_SCORING_ERROR_TOO_SOON_2"] = "AI Scoring ha sido deshabilitado por su solicitud. Podrá volver a capacitar y habilitar el modelo de scoring después del #DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_QUALITY"] = "Calidad del modelo: #QUALITY#%.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_READY"] = "El modelo de IA ha sido entrenado y ahora está listo para hacer predicciones.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_TRAINING_DATE"] = "Entrenado el: #TRAINING_DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_NOT_ENOUGH_DATA"] = "Su CRM no tiene datos suficientes para capacitar el modelo de AI Scoring. Consulte este #LINK_START#artículo#LINK_END# para obtener más información sobre el tamaño y el contenido del conjunto de datos requerido para la capacitación.";
$MESS["CRM_ML_MODEL_LIST_SCORING_REENABLE_WARNING"] = "No podrá volver a activar el scoring hasta el #DATE#";
$MESS["CRM_ML_MODEL_LIST_SCORING_TITLE"] = "AI Scoring";
$MESS["CRM_ML_MODEL_LIST_SCORING_TRAINING_IN_PROCESS"] = "La capacitación del modelo de AI Scoring está en progreso. Completado: #PROGRESS#%";
?>