<?
$MESS["CRM_ML_MODEL_LIST_AVAILABLE_MODELS"] = "Modèles disponibles";
$MESS["CRM_ML_MODEL_LIST_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_ML_MODEL_LIST_BUTTON_DISABLE"] = "Désactiver";
$MESS["CRM_ML_MODEL_LIST_BUTTON_TRAIN_FREE_OF_CHARGE"] = "Configurer gratuitement";
$MESS["CRM_ML_MODEL_LIST_CONFIRMATION"] = "Confirmer l'action";
$MESS["CRM_ML_MODEL_LIST_DEAL_SCORING_DISABLE"] = "Désactiver le scoring de transactions pour ce pipeline";
$MESS["CRM_ML_MODEL_LIST_DISABLE_DEAL_SCORING"] = "Désactiver le scoring de transactions pour ce pipeline ?";
$MESS["CRM_ML_MODEL_LIST_DISABLE_LEAD_SCORING"] = "Désactiver le scoring de prospects ?";
$MESS["CRM_ML_MODEL_LIST_HELP"] = "Aide";
$MESS["CRM_ML_MODEL_LIST_LEAD_SCORING_DISABLE"] = "Désactiver le scoring de prospects";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P1"] = "Votre responsable commercial prend en compte son intuition lors du traitement des transactions ? Propulsez des clients prometteurs en haut de votre liste.";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P2"] = "AI Scoring analyse les transactions existantes pour présenter les probabilités de réussite. Le système aide vos employés à identifier les domaines qui nécessitent le plus d'attention. Ne perdez pas votre temps sur ceux qui n'ont aucunement l'intention de devenir clients !";
$MESS["CRM_ML_MODEL_LIST_SCORING_ENOUGH_DATA"] = "Votre CRM dispose d'assez de données pour configurer le modèle d'AI Scoring.";
$MESS["CRM_ML_MODEL_LIST_SCORING_ERROR_TOO_SOON_2"] = "AI Scoring a été désactivé comme vous l'avez demandé. Vous pourrez configurer le modèle d'IA et activer le scoring après le #DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_QUALITY"] = "Qualité du modèle : #QUALITY#%.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_READY"] = "Le modèle d'IA a été configuré et est maintenant prêt pour les prévisions.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_TRAINING_DATE"] = "Date de la configuration : #TRAINING_DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_NOT_ENOUGH_DATA"] = "Votre CRM ne dispose pas d'assez de données pour configurer le modèle d'AI Scoring. Veuillez consulter cet #LINK_START#article#LINK_END# pour obtenir plus d'informations sur les exigences en matière de taille et de contenu du jeu de données pour la configuration.";
$MESS["CRM_ML_MODEL_LIST_SCORING_REENABLE_WARNING"] = "Vous ne pourrez pas réactiver le scoring avant le #DATE#";
$MESS["CRM_ML_MODEL_LIST_SCORING_TITLE"] = "AI Scoring";
$MESS["CRM_ML_MODEL_LIST_SCORING_TRAINING_IN_PROCESS"] = "La configuration du modèle d'AI Scoring est en cours. Progression : #PROGRESS#%";
?>