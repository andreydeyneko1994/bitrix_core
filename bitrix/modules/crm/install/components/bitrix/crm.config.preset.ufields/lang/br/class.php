<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de modelo incorreto";
$MESS["CRM_PRESET_UFIELDS_TITLE"] = "Campos personalizados não utilizados";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Nome";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Nome";
$MESS["CRM_PRESET_UFIELD_FIELD_TYPE"] = "Tipo";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
?>