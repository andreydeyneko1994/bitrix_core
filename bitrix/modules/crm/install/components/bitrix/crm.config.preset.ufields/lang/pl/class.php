<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Niewłaściwy typ szablonu";
$MESS["CRM_PRESET_UFIELDS_TITLE"] = "Nieużywane pola dokumentu";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Nazwa";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Nazwa";
$MESS["CRM_PRESET_UFIELD_FIELD_TYPE"] = "Rodzaj";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
?>