<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE"] = "Supprimer";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE_CONF"] = "Supprimer l'enregistrement ?";
$MESS["CRM_PRESET_UFIELDS_NOTE"] = "La liste contient les champs personnalisés qui ne sont utilisés par aucun modèle. Il est possible que ces champs ne soient plus nécessaires. Une fois le champ supprimé, toutes les données qu'il contient seront définitivement supprimées.";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST"] = "Modèles";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST_TITLE"] = "Liste des modèles : #NAME#";
?>