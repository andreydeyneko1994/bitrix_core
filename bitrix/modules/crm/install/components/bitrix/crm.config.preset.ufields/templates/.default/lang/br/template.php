<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE"] = "Excluir";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE_CONF"] = "Excluir registros?";
$MESS["CRM_PRESET_UFIELDS_NOTE"] = "A lista contém campos personalizados que não estão em uso por nenhum dos modelos. É possível que esses campos não sejam mais necessários. Ao excluir um campo, você também excluirá irreversivelmente quaisquer dados associados a ele.";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST"] = "Modelos";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST_TITLE"] = "Modelos: #NAME#";
?>