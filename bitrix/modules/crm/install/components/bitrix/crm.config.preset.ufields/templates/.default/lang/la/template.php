<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE"] = "Eliminar";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE_CONF"] = "¿Eliminar el registro?";
$MESS["CRM_PRESET_UFIELDS_NOTE"] = "La lista contiene campos personalizados que no están en uso por ninguna de las plantillas. Es posible que estos campos ya no sean necesarios. Eliminar un campo también eliminará irreversiblemente cualquier dato asociado a él.";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST"] = "Plantillas";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST_TITLE"] = "Plantillas: #NAME#";
?>