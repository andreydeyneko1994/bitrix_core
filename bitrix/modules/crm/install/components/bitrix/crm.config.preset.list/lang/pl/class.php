<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRESET_COUNTRY_EMPTY"] = "(nie wybrano)";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_COMPANY"] = "Główny dla firm";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_CONTACT"] = "Główny dla kontaktów";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Niewłaściwy typ szablonu";
$MESS["CRM_PRESET_ERR_DELETE"] = "Błąd usuwania szablonu (ID = #ID#)";
$MESS["CRM_PRESET_LIST_TITLE_EDIT"] = "Szablony: #NAME#";
$MESS["CRM_PRESET_NOT_SELECTED"] = "(nie wybrano)";
?>