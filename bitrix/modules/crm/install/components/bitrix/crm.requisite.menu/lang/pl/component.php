<?
$MESS["REQUISITE_ADD"] = "Dodaj szczegóły";
$MESS["REQUISITE_ADD_TITLE"] = "Dodaj szczegóły";
$MESS["REQUISITE_COPY"] = "Kopiuj";
$MESS["REQUISITE_COPY_TITLE"] = "Skopiuj szczegóły";
$MESS["REQUISITE_DELETE"] = "Usuń szczegóły";
$MESS["REQUISITE_DELETE_DLG_BTNTITLE"] = "Usuń";
$MESS["REQUISITE_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć ten element?";
$MESS["REQUISITE_DELETE_DLG_TITLE"] = "Usuń szczegóły";
$MESS["REQUISITE_DELETE_TITLE"] = "Usuń szczegóły";
?>