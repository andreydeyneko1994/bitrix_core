<?
$MESS["REQUISITE_ADD"] = "Adicionar informações";
$MESS["REQUISITE_ADD_TITLE"] = "Adicionar informações";
$MESS["REQUISITE_COPY"] = "Copiar";
$MESS["REQUISITE_COPY_TITLE"] = "Copiar informações";
$MESS["REQUISITE_DELETE"] = "Excluir informações";
$MESS["REQUISITE_DELETE_DLG_BTNTITLE"] = "Excluir";
$MESS["REQUISITE_DELETE_DLG_MESSAGE"] = "Você tem certeza que deseja excluir o item?";
$MESS["REQUISITE_DELETE_DLG_TITLE"] = "Excluir informações";
$MESS["REQUISITE_DELETE_TITLE"] = "Excluir informações";
?>