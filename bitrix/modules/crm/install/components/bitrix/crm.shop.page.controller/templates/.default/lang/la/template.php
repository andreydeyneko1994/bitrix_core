<?
$MESS["SHOP_MENU_BUYER_GROUP_TITLE"] = "Grupos de clientes";
$MESS["SHOP_MENU_ORDER_FORM_SETTINGS_TITLE"] = "Configurar formulario de pago";
$MESS["SHOP_MENU_ORDER_TITLE"] = "Pedidos";
$MESS["SHOP_MENU_PRODUCT_MARKETING_COUPONS"] = "Cupones";
$MESS["SHOP_MENU_PRODUCT_MARKETING_DISCOUNT"] = "Reglas del carrito de compras";
$MESS["SHOP_MENU_PRODUCT_MARKETING_PRESET"] = "Negociaciones de marketing preinstaladas";
$MESS["SHOP_MENU_PRODUCT_MARKETING_TITLE"] = "Marketing del producto";
$MESS["SHOP_MENU_SETTINGS_SALE_SETTINGS"] = "Parámetros de la tienda online";
$MESS["SHOP_MENU_SETTINGS_STATUS"] = "Estados";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER"] = "Estados del pedido";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER_SHIPMENT"] = "Estados de entrega";
$MESS["SHOP_MENU_SETTINGS_USER_FIELDS"] = "Campos personalizados";
$MESS["SHOP_MENU_SHOP_LIST_TITLE"] = "Tiendas online";
$MESS["SHOP_MENU_SHOP_ROLES_TITLE"] = "Permisos de acceso";
$MESS["SHOP_MENU_SHOP_TITLE"] = "Tienda online";
?>