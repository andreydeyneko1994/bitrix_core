<?
$MESS["SHOP_MENU_BUYER_GROUP_TITLE"] = "Grupos de clientes";
$MESS["SHOP_MENU_ORDER_FORM_SETTINGS_TITLE"] = "Configurar Formulário de Pagamento";
$MESS["SHOP_MENU_ORDER_TITLE"] = "Pedidos";
$MESS["SHOP_MENU_PRODUCT_MARKETING_COUPONS"] = "Cupons";
$MESS["SHOP_MENU_PRODUCT_MARKETING_DISCOUNT"] = "Regras do Carrinho de Compras";
$MESS["SHOP_MENU_PRODUCT_MARKETING_PRESET"] = "Ofertas de Marketing Pré-instaladas";
$MESS["SHOP_MENU_PRODUCT_MARKETING_TITLE"] = "Marketing de Produto";
$MESS["SHOP_MENU_SETTINGS_SALE_SETTINGS"] = "Parâmetros da Loja On-line";
$MESS["SHOP_MENU_SETTINGS_STATUS"] = "Status";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER"] = "Status do Pedido";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER_SHIPMENT"] = "Status da Entrega";
$MESS["SHOP_MENU_SETTINGS_USER_FIELDS"] = "Campos Personalizados";
$MESS["SHOP_MENU_SHOP_LIST_TITLE"] = "Lojas Virtuais";
$MESS["SHOP_MENU_SHOP_ROLES_TITLE"] = "Permissões de Acesso";
$MESS["SHOP_MENU_SHOP_TITLE"] = "Lojas On-line";
?>