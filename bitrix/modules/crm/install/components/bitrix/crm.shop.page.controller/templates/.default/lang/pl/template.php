<?
$MESS["SHOP_MENU_BUYER_GROUP_TITLE"] = "Grupy klientów";
$MESS["SHOP_MENU_ORDER_FORM_SETTINGS_TITLE"] = "Skonfiguruj formularz finalizacji zamówienia";
$MESS["SHOP_MENU_ORDER_TITLE"] = "Zamówienia";
$MESS["SHOP_MENU_PRODUCT_MARKETING_COUPONS"] = "Kupony";
$MESS["SHOP_MENU_PRODUCT_MARKETING_DISCOUNT"] = "Reguły koszyka";
$MESS["SHOP_MENU_PRODUCT_MARKETING_PRESET"] = "Wstępnie zainstalowane oferty marketingowe";
$MESS["SHOP_MENU_PRODUCT_MARKETING_TITLE"] = "Marketing produktu";
$MESS["SHOP_MENU_SETTINGS_SALE_SETTINGS"] = "Parametry sklepu internetowego";
$MESS["SHOP_MENU_SETTINGS_STATUS"] = "Statusy";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER"] = "Statusy zamówień";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER_SHIPMENT"] = "Statusy dostaw";
$MESS["SHOP_MENU_SETTINGS_USER_FIELDS"] = "Własne pola";
$MESS["SHOP_MENU_SHOP_LIST_TITLE"] = "Sklepy internetowe";
$MESS["SHOP_MENU_SHOP_ROLES_TITLE"] = "Uprawnienia dostępu";
$MESS["SHOP_MENU_SHOP_TITLE"] = "Sklepy internetowe";
?>