<?php
$MESS["CRM_ITEM_AUTOMATION_INVALID_ENTITY_TYPE"] = "Tipo de entidade inválido";
$MESS["CRM_ITEM_AUTOMATION_TITLE"] = "Regras de automação #ENTITY#";
$MESS["CRM_ITEM_AUTOMATION_TITLE_CATEGORY"] = "Regras de automação #ENTITY# (#CATEGORY#)";
$MESS["CRM_ITEM_AUTOMATION_WRONG_CATEGORY"] = "A categoria é inválida";
