<?
$MESS["CRM_TRACKING_ENTITY_DETAILS_BTN_HIDE"] = "Masquer";
$MESS["CRM_TRACKING_ENTITY_DETAILS_BTN_SETUP"] = "Configurer";
$MESS["CRM_TRACKING_ENTITY_DETAILS_DESC"] = "Évaluer la qualité du trafic publicitaire.";
$MESS["CRM_TRACKING_ENTITY_DETAILS_HEAD"] = "Connecter %tagStart% la Sales Intelligence %tagEnd%";
$MESS["CRM_TRACKING_ENTITY_DETAILS_TITLE"] = "Sales Intelligence";
?>