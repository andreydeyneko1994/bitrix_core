<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_TAX_FIELD_CODE"] = "Kod podatku";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Opis";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_LID"] = "Strona internetowa";
$MESS["CRM_TAX_FIELD_NAME"] = "Nazwa";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Zmodyfikowany";
$MESS["CRM_TAX_NOT_FOUND"] = "Nie znaleziono podateku.";
$MESS["CRM_TAX_RATE_LIST"] = "Stawka podatku";
$MESS["CRM_TAX_SECTION_MAIN"] = "Podatek";
?>