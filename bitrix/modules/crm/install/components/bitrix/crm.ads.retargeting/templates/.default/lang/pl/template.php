<?
$MESS["CRM_ADS_RTG_ADD_AUDIENCE"] = "Stwórz grupę odbiorców";
$MESS["CRM_ADS_RTG_APPLY"] = "Wykonaj";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_DAYS"] = "dni";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE"] = "usunąć z grupy odbiorców za";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE_YANDEX"] = "usunąć z segmentów za";
$MESS["CRM_ADS_RTG_CABINET_FACEBOOK"] = "Konto reklamowe na Facebooku";
$MESS["CRM_ADS_RTG_CABINET_GOOGLE"] = "Konto reklamowe na Google AdWords";
$MESS["CRM_ADS_RTG_CABINET_VKONTAKTE"] = "Konto reklamowe na VK";
$MESS["CRM_ADS_RTG_CABINET_YANDEX"] = "Konto reklamowe na Yandex.Audience";
$MESS["CRM_ADS_RTG_CANCEL"] = "Anuluj";
$MESS["CRM_ADS_RTG_CLOSE"] = "Zamknij";
$MESS["CRM_ADS_RTG_ERROR_ACTION"] = "Akcja została wstrzymana z powodu błędu.";
$MESS["CRM_ADS_RTG_ERROR_NO_AUDIENCES"] = "Nie znaleziono odbiorców. Przejdź do %name%, aby stworzyć grupę odbiorców.";
$MESS["CRM_ADS_RTG_LOGIN"] = "Połącz";
$MESS["CRM_ADS_RTG_LOGOUT"] = "Rozłącz";
$MESS["CRM_ADS_RTG_REFRESH"] = "Aktualizuj";
$MESS["CRM_ADS_RTG_REFRESH_TEXT"] = "Zaktualizuj grupę docelową.";
$MESS["CRM_ADS_RTG_REFRESH_TEXT_YANDEX"] = "Zaktualizuj segmenty docelowe.";
$MESS["CRM_ADS_RTG_SELECT_ACCOUNT"] = "Wybierz konto reklamowe";
$MESS["CRM_ADS_RTG_SELECT_AUDIENCE"] = "Dodaj do grona odbiorców";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA"] = "Dodaj telefon i e-mail do segmentów";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_EMAIL"] = "E-mail";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_PHONE"] = "Telefon";
$MESS["CRM_ADS_RTG_TITLE"] = "Skonfiguruj grupę docelową";
?>