<?php
$MESS["CRM_EMPTY_PERSON_TYPE_LIST"] = "Debe crear un tipo de pagador adecuado antes de editar el formulario de pago.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_ORDERFORM_EMPTY_LIST_LABEL"] = "La lista de propiedades está vacía";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT"] = "Cliente";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC"] = "contacto o compañía";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC2"] = "Cliente desvinculado";
$MESS["CRM_ORDERFORM_RELATION_DELIVERY"] = "Servicio de entrega";
$MESS["CRM_ORDERFORM_RELATION_PAY_SYSTEM"] = "Sistema de pago";
$MESS["CRM_ORDERFORM_REQUIERD_FIELDS_ERROR"] = "Tienes que agregar uno o más campos obligatorios.";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_MERGE"] = "Unir";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_NONE"] = "Permitir duplicados";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_REPLACE"] = "Reemplazar";
$MESS["CRM_ORDERFORM_TITLE"] = "Configurar formulario de pago";
$MESS["CRM_ORDERFORM_TRADING_PLATFORM_GENERAL"] = "General";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
