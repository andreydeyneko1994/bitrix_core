<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_MODULE_ERROR_NOT_FOUNT"] = "Nie znaleziono formularza";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "Moduł Rest nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_WEBFORM_EDIT_"] = "Szukaj";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN"] = "Nazwa domeny";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN_DESC"] = "Nazwa domeny, z której wysłano formularz";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID"] = "ID formularza";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID_DESC"] = "ID formularza";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME"] = "Nazwa formularza";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME_DESC"] = "Nazwa formularza";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM"] = "Parametr";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM_DESC"] = "Nazwa parametru URL, która posłuży do rozpoznania formularza";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID"] = "ID rezultatu";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID_DESC"] = "Określa numer przypisany do rezultatu formularza.";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL"] = "Adres strony";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL_DESC"] = "Nazwa strony, z której przesłano formularz";
$MESS["CRM_WEBFORM_EDIT_SECOND_SHORT"] = "sek.";
$MESS["CRM_WEBFORM_EDIT_TITLE"] = "Edytuj formularz CRM";
$MESS["CRM_WEBFORM_EDIT_TITLE_ADD"] = "Utwórz formularz CRM";
$MESS["CRM_WEBFORM_EDIT_TITLE_VIEW"] = "Widok formularza CRM";
$MESS["CRM_WEBFORM_SCENARIO_NAME_TEMPLATE"] = "#NAME# z #DATE#";
