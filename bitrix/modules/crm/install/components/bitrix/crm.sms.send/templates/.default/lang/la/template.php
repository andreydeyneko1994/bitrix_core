<?php
$MESS["CRM_SMS_CANCEL"] = "Cancelar";
$MESS["CRM_SMS_ERROR_NO_COMMUNICATIONS"] = "Proporcione el número de teléfono del cliente en los detalles para enviar mensajes SMS.";
$MESS["CRM_SMS_FROM"] = "desde el número";
$MESS["CRM_SMS_MANAGE_TEXT_1"] = "Explore las posibilidades de comercialización de SMS";
$MESS["CRM_SMS_MANAGE_TEXT_2"] = "Bitrix24 proporciona mensajes SMS fáciles y sin problemas";
$MESS["CRM_SMS_MANAGE_TEXT_3"] = "Envía mensajes directamente desde el prospecto, negociación, contacto o cotización.";
$MESS["CRM_SMS_MANAGE_URL"] = "configurar";
$MESS["CRM_SMS_REST_MARKETPLACE"] = "Seleccione otro servicio";
$MESS["CRM_SMS_SEND"] = "Enviar";
$MESS["CRM_SMS_SENDER"] = "usando";
$MESS["CRM_SMS_SEND_MESSAGE"] = "Enviar mensaje SMS";
$MESS["CRM_SMS_SEND_SENDER_SUBTITLE"] = "Utilizando #SENDER#";
$MESS["CRM_SMS_SYMBOLS"] = "Caracteres";
$MESS["CRM_SMS_SYMBOLS_FROM"] = "apagado";
$MESS["CRM_SMS_TO"] = "para";
