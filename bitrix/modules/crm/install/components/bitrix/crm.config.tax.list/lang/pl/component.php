<?
$MESS["CRM_COLUMN_CODE"] = "Kod podatku";
$MESS["CRM_COLUMN_DATE"] = "Zmodyfikowany";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Nazwa / opis";
$MESS["CRM_COLUMN_RATES"] = "Stawka podatku";
$MESS["CRM_COLUMN_SITE"] = "Strona internetowa";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_TAX_DELETION_GENERAL_ERROR"] = "Błąd usuwania podatku.";
$MESS["CRM_TAX_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji podatku.";
?>