<?php
$MESS["CRM_AUTOMATION_TITLE"] = "Automatyzacja";
$MESS["CRM_AUTOMATION_TITLE_1"] = "Automatyzacja sprzedaży";
$MESS["CRM_BP_DEAL"] = "Deal";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
