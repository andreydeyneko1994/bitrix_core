<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">sin actividad</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">con actividades atrasadas</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">con actividades hoy</span></span>";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Actividades";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "No hay actividades";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Entrante";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Atrasado";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "Para hoy";
