<?php
$MESS["CRM_ENTITY_ED_PROG_CLOSE"] = "Concluir";
$MESS["CRM_ENTITY_ED_PROG_DEAL_CLOSE"] = "Fechar negócio";
$MESS["CRM_ENTITY_ED_PROG_HAS_INACCESSIBLE_FIELDS"] = "O acesso a esta função foi negado porque há campos indisponíveis para você. Por favor, entre em contato com o administrador para obter acesso e resolver o problema.";
$MESS["CRM_ENTITY_ED_PROG_LEAD_CLOSE"] = "Concluir lead";
$MESS["CRM_ENTITY_ED_PROG_ORDER_CLOSE"] = "Concluir pedido";
$MESS["CRM_ENTITY_ED_PROG_ORDER_SHIPMENT_CLOSE"] = "Concluir envio";
$MESS["CRM_ENTITY_ED_PROG_QUOTE_CLOSE"] = "Concluir orçamento";
