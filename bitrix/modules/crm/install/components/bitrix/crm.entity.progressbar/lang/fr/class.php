<?php
$MESS["CRM_ENTITY_ED_PROG_CLOSE"] = "Terminer";
$MESS["CRM_ENTITY_ED_PROG_DEAL_CLOSE"] = "Fermer la transaction";
$MESS["CRM_ENTITY_ED_PROG_HAS_INACCESSIBLE_FIELDS"] = "L'accès à cette fonction a été refusé parce que vous n'avez pas accès à certains champs. Veuillez contacter l'administrateur pour obtenir l'accès et résoudre le problème.";
$MESS["CRM_ENTITY_ED_PROG_LEAD_CLOSE"] = "Finaliser le prospect";
$MESS["CRM_ENTITY_ED_PROG_ORDER_CLOSE"] = "Finaliser la commande";
$MESS["CRM_ENTITY_ED_PROG_ORDER_SHIPMENT_CLOSE"] = "Finaliser l'expédition";
$MESS["CRM_ENTITY_ED_PROG_QUOTE_CLOSE"] = "Devis complet";
