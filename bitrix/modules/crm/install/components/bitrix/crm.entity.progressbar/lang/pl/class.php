<?php
$MESS["CRM_ENTITY_ED_PROG_CLOSE"] = "Zakończ";
$MESS["CRM_ENTITY_ED_PROG_DEAL_CLOSE"] = "Zamknij deal";
$MESS["CRM_ENTITY_ED_PROG_HAS_INACCESSIBLE_FIELDS"] = "Odmówiono dostępu do tej funkcji, ponieważ obejmuje ona pola niedostępne dla Ciebie. Aby uzyskać dostęp i rozwiązać problem, skontaktuj się z administratorem.";
$MESS["CRM_ENTITY_ED_PROG_LEAD_CLOSE"] = "Zakończony lead";
$MESS["CRM_ENTITY_ED_PROG_ORDER_CLOSE"] = "Zakończ zamówienie";
$MESS["CRM_ENTITY_ED_PROG_ORDER_SHIPMENT_CLOSE"] = "Zakończ dostawę";
$MESS["CRM_ENTITY_ED_PROG_QUOTE_CLOSE"] = "Zakończ ofertę";
