<?
$MESS["CRM_TRACKING_SOURCE_UTM_ADDITIONAL_GOOGLE"] = "Dodatkowe parametry UTM dla Google Adwords";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN"] = "Nazwa kampanii reklamowej";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN_DESC_GOOGLE"] = "AdWords zastąpi {network} parametrem \"g\" (wyszukiwarka google), \"s\" (partner wyszukiwania) lub \"d\" (narzędzie Planer kampanii w sieci reklamowej). Przykład: ubranka_dzieci, tanie_buty itd.";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_DESC_GOOGLE"] = "Możliwe wartości: e (dopasowanie ścisłe), p (dopasowanie wyrażenia) lub b (zmodyfikowane dopasowanie przybliżone)";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_GOOGLE"] = "Typ dopasowania słowa kluczowego";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM"] = "Typ kampanii reklamowej";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM_DESC"] = "Typ ruchu. Przykład: cpc, e-mail, wyświetlenie, cena, retargetowanie, partnerskie, społecznościowe, specjalne itd.";
$MESS["CRM_TRACKING_SOURCE_UTM_NAME"] = "Nazwa zestawu parametrów UTM";
$MESS["CRM_TRACKING_SOURCE_UTM_OBLIGATORY_FIELDS"] = "Parametry wymagane";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT"] = "Miejsce";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT_DESC_GOOGLE"] = "Tylko narzędzie Planer kampanii w sieci reklamowej: adres miejsca";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION"] = "Położenie reklamy";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION_DESC_GOOGLE"] = "Możliwe wartości: 1t2 (strona 1, u góry strony, miejsce 2), 1s3 (strona 1, po prawej stronie, miejsce 3) lub brak (Planer kampanii w sieci reklamowej)";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME"] = "Wprowadź adres URL witryny";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME_DESC"] = "Strona docelowa, do której zostanie przekierowany użytkownik";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE"] = "Źródło kampanii reklamowej";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE_DESC"] = "Kliknij źródło. Przykład: instagram, google, e-mail, biuletyn";
?>