<?
$MESS["CRM_TRACKING_SOURCE_UTM_ADDITIONAL_GOOGLE"] = "Parâmetros UTM extras para Google Adwords";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN"] = "Nome da campanha de publicidade";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN_DESC_GOOGLE"] = "O AdWords substituirá {network} por \"g\" (google search), \"s\" (search partner) ou \"d\" (Display Planner). Exemplo: children_clothes, cheap_shoes etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_DESC_GOOGLE"] = "Valores possíveis: e (correspondência exata), p (correspondência de frase) ou b (correspondência ampla modificada)";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_GOOGLE"] = "Tipo de correspondência de palavras-chave";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM"] = "Tipo de campanha publicitária";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM_DESC"] = "Tipo de tráfego. Exemplo: cpc, e-mail, display, preço, retargeting, afiliado, social, especial etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_NAME"] = "Nome do conjunto de parâmetros do UTM";
$MESS["CRM_TRACKING_SOURCE_UTM_OBLIGATORY_FIELDS"] = "Parâmetros obrigatórios";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT"] = "Veiculação";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT_DESC_GOOGLE"] = "Somente Display Planner: endereço de veiculação";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION"] = "Posição do anúncio";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION_DESC_GOOGLE"] = "Valores possíveis: 1t2 (página 1, no topo da página, posicionamento 2), 1s3 (página 1, à direita da página, posicionamento 3) ou nenhum (Display Planner)";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME"] = "Digite o URL do site";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME_DESC"] = "Página de destino um visitante será redirecionado para";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE"] = "Fonte da campanha publicitária";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE_DESC"] = "Clique na fonte. Exemplo: instagram, google, e-mail, boletim informativo";
?>