<?
$MESS["CRM_TRACKING_SOURCE_UTM_ADDITIONAL_GOOGLE"] = "Paramètres UTM supplémentaires pour Google Adwords";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN"] = "Nom de la campagne publicitaire";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN_DESC_GOOGLE"] = "Adwords remplace {network} par \"g\" (recherche google), \"s\" (partenaire de recherche) ou \"d\" (Display Planner). Exemple : vêtements_enfants, chaussurs_pas_chères, etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_DESC_GOOGLE"] = "Valeurs possibles : e (correspondance exacte), p (correspondance partielle) ou b (correspondance vague modifiée)";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_GOOGLE"] = "Type de correspondance de mot-clé";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM"] = "Type de campagne publicitaire";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM_DESC"] = "Type de trafic. Exemple : cpc, e-mail, affichage, prix, reciblage, affiliation, social, spécial, etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_NAME"] = "Nom du jeu de paramètres UTM";
$MESS["CRM_TRACKING_SOURCE_UTM_OBLIGATORY_FIELDS"] = "Paramètres obligatoires";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT"] = "Emplacement";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT_DESC_GOOGLE"] = "Display Planner uniquement : adresse de l'emplacement";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION"] = "Position de la publicité";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION_DESC_GOOGLE"] = "Valeurs possibles : 1t2 (page 1, page du haut, emplacement 2), 1s3 (page 1, page de droite, emplacement 3) ou aucune (Display Planner)";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME"] = "Saisissez l'URL du site";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME_DESC"] = "Page de destination vers laquelle sera redirigé un visiteur";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE"] = "Source de la campagne publicitaire";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE_DESC"] = "Source du clic. Exemple : Instagram, Google, e-mail, lettre d'information";
?>