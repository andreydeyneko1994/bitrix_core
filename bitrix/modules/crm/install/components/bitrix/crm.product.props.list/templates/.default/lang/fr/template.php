<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRODUCTPROP_DELETE"] = "Supprimer";
$MESS["CRM_PRODUCTPROP_DELETE_CONFIRM"] = "Êtes-vous sûr de vouloir supprimer '%s' ?";
$MESS["CRM_PRODUCTPROP_DELETE_TITLE"] = "Supprimer cette propriété";
$MESS["CRM_PRODUCTPROP_EDIT"] = "Éditer";
$MESS["CRM_PRODUCTPROP_EDIT_TITLE"] = "Ouvrez cette propriété pour l'édition";
?>