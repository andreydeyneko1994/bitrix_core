<?
$MESS["CRM_INTS_TASKS_NAV"] = "Wyniki";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRODUCT_PROP_IBLOCK_ALL"] = "(jakikolwiek)";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_E"] = "Wiąż z obiektami CRM";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_F"] = "Plik";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_G"] = "Powiąż z Sekcjami";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_L"] = "Lista";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_N"] = "Liczba";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_S"] = "String";
$MESS["CRM_PRODUCT_PROP_PL_ACTIVE"] = "Aktywne";
$MESS["CRM_PRODUCT_PROP_PL_CODE"] = "Kod symboliczny";
$MESS["CRM_PRODUCT_PROP_PL_DELETE_ERROR"] = "Wystąpił błąd podczas usuwania obiektu.";
$MESS["CRM_PRODUCT_PROP_PL_FILTRABLE"] = "Pokaż we filtrze";
$MESS["CRM_PRODUCT_PROP_PL_HINT"] = "Wskazówka";
$MESS["CRM_PRODUCT_PROP_PL_ID"] = "ID";
$MESS["CRM_PRODUCT_PROP_PL_IS_REQUIRED"] = "Wymagany";
$MESS["CRM_PRODUCT_PROP_PL_MULTIPLE"] = "Wielokrotne";
$MESS["CRM_PRODUCT_PROP_PL_NAME"] = "Nazwa";
$MESS["CRM_PRODUCT_PROP_PL_PROPERTY_TYPE"] = "Rodzaj";
$MESS["CRM_PRODUCT_PROP_PL_SAVE_ERROR"] = "Błąd zapisu rekordu ##ID#: #ERROR_TEXT#";
$MESS["CRM_PRODUCT_PROP_PL_SEARCHABLE"] = "Szukaj";
$MESS["CRM_PRODUCT_PROP_PL_SORT"] = "Sortuj";
$MESS["CRM_PRODUCT_PROP_PL_WITH_DESCRIPTION"] = "Ma opis";
$MESS["CRM_PRODUCT_PROP_PL_XML_ID"] = "Zewnętrzne ID";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
?>