<?php
$MESS["CRM_KANBAN_ED_CANCEL"] = "Anuluj";
$MESS["CRM_KANBAN_ED_CHANGE"] = "zmień";
$MESS["CRM_KANBAN_ED_CHANGE_USER"] = "Zmień";
$MESS["CRM_KANBAN_HAS_INACCESSIBLE_FIELDS"] = "Odmówiono dostępu do tej funkcji, ponieważ obejmuje ona pola niedostępne dla Ciebie. Aby uzyskać dostęp i rozwiązać problem, skontaktuj się z administratorem.";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Komentarz";
$MESS["CRM_KANBAN_POPUP_CONFIRM"] = "Potwierdź działanie";
$MESS["CRM_KANBAN_POPUP_CONFIRM_DELETE"] = "Czy na pewno chcesz usunąć jednostkę? Nie można cofnąć tego działania.";
$MESS["CRM_KANBAN_POPUP_DATE"] = "Data";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Faktura #";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Parametry zamknięcia faktury:";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Utwórz w oparciu o lead:";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Wybierz";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Anuluj";
$MESS["CRM_KANBAN_POPUP_PARAMS_DELETE"] = "Usuń";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Zapisz";
