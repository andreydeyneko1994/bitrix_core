<?php
$MESS["CRM_KANBAN_ED_CANCEL"] = "Annuler";
$MESS["CRM_KANBAN_ED_CHANGE"] = "modifier";
$MESS["CRM_KANBAN_ED_CHANGE_USER"] = "Changer";
$MESS["CRM_KANBAN_HAS_INACCESSIBLE_FIELDS"] = "L'accès à cette fonction a été refusé parce que vous n'avez pas accès à certains champs. Veuillez contacter l'administrateur pour obtenir l'accès et résoudre le problème.";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Commentaire";
$MESS["CRM_KANBAN_POPUP_CONFIRM"] = "Confirmer l'action";
$MESS["CRM_KANBAN_POPUP_CONFIRM_DELETE"] = "Voulez-vous supprimer l'entité ? Cette action ne peut pas être annulée.";
$MESS["CRM_KANBAN_POPUP_DATE"] = "Date";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Facture #";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Paramètres finaux de facture : ";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Créer à partir d'un prospect :";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Sélectionner";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Annuler";
$MESS["CRM_KANBAN_POPUP_PARAMS_DELETE"] = "Supprimer";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Enregistrer";
