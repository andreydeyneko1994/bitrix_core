<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module De processus d'entreprise n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devises n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "Le module REST n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "Le formulaire est inactif.";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "Le formulaire est introuvable.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "L'URL du formulaire a peut-être changé.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_SUBTITLE"] = "Quelqu'un vous a envoyé un lien d'aperçu de formulaire. Veuillez vous connecter à Bitrix24 ou demander au propriétaire un lien public.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_TITLE"] = "Vous n'êtes pas identifié";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_SUBTITLE"] = "Quelqu'un vous a envoyé un lien d'aperçu du formulaire. Vous devez disposer du droit d'accès respectif ; sinon, demandez au propriétaire un lien public.";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_TITLE"] = "Accès refusé";
