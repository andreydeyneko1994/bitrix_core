<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "O módulo REST não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "O formulário está inativo.";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "O formulário não foi encontrado.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "O formulário URL foi possivelmente alterado.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_SUBTITLE"] = "Alguém enviou um link de pré-visualização de formulário para você. Entre no Bitrix24 ou peça para o proprietário um link público.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_TITLE"] = "Você não está conectado";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_SUBTITLE"] = "Alguém enviou para você um link de pré-visualização de formulário. Você precisa ter a respectiva permissão de acesso; caso contrário, peça ao proprietário um link público.";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_TITLE"] = "Acesso negado";
