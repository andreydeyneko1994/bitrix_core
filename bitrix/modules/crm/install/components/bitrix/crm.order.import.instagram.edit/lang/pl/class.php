<?
$MESS["CRM_OIIE_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "Nie można połączyć strony";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_PAGE"] = "Nie można odłączyć strony";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_USER"] = "Nie można odłączyć konta użytkownika";
$MESS["CRM_OIIE_FACEBOOK_NO_IMPORT_AVAILABLE"] = "Import produktów jest wyłączony";
$MESS["CRM_OIIE_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "Strona została połączona";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_PAGE"] = "Strona została odłączona";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_USER"] = "Twoje konto użytkownika zostało odłączone";
$MESS["CRM_OIIE_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "Import produktu został skonfigurowany do pracy w grupie, do której obecnie nie masz uprawnień administratora.";
$MESS["CRM_OIIE_FACEBOOK_REPEATING_ERROR"] = "Jeśli problem będzie się powtarzał, odłącz kanał i skonfiguruj go ponownie.";
$MESS["CRM_OIIE_FACEBOOK_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Prześlij formularz ponownie.";
$MESS["CRM_OIIE_GO_TO_IMPORT"] = "Strona została połączona. <a href=\"#LINK#\">Przejdź do importu produktu</a>";
$MESS["CRM_OIIE_IMPORT_CONNECTED"] = "Import produktów został włączony";
$MESS["CRM_OIIE_IMPORT_DISCONNECTED"] = "Import produktów został wyłączony";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CRM"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_OIIE_SETTINGS_NO_DISABLE"] = "Błąd podczas wyłączania importu produktu";
$MESS["CRM_OIIE_TITLE"] = "Połącz Instagram ze swoim sklepem internetowym";
?>