<?
$MESS["CRM_OIIE_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "No se puede conectar la página";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_PAGE"] = "No se puede desvincular la página";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_USER"] = "No se puede desvincular su cuenta de usuario";
$MESS["CRM_OIIE_FACEBOOK_NO_IMPORT_AVAILABLE"] = "Importación de productos deshabilitada";
$MESS["CRM_OIIE_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "La página ha sido conectada";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_PAGE"] = "La página ha sido desvinculada";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_USER"] = "Su cuenta de usuario ha sido desvinculada";
$MESS["CRM_OIIE_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "La importación de productos se configuró para su uso con un grupo al que actualmente no tiene acceso de administrador.";
$MESS["CRM_OIIE_FACEBOOK_REPEATING_ERROR"] = "Si el problema persiste, es posible que deba desconectar el canal y volver a configurarlo.";
$MESS["CRM_OIIE_FACEBOOK_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor vuelva a enviar el formulario.";
$MESS["CRM_OIIE_GO_TO_IMPORT"] = "La página ha sido conectada. <a href=\"#LINK#\">Continuar con la importación de productos</a>";
$MESS["CRM_OIIE_IMPORT_CONNECTED"] = "La importación de productos ha sido habilitada";
$MESS["CRM_OIIE_IMPORT_DISCONNECTED"] = "La importación de productos ha sido deshabilitada";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CRM"] = "El módulo CRM no está instalado.";
$MESS["CRM_OIIE_SETTINGS_NO_DISABLE"] = "Error al deshabilitar la importación del producto";
$MESS["CRM_OIIE_TITLE"] = "Conectar Instagram a su tienda online";
?>