<?
$MESS["CRM_ELEMENT_ID"] = "ID del pedido";
$MESS["CRM_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["CRM_ORDER_VAR"] = "Nombre de variable de ID del pedido";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Ruta a la página de edición del pedido";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Ruta a la página de importación";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Ruta a la página índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Ruta a la página de vista de pedidos";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Ruta a la página de vista del pedido";
?>