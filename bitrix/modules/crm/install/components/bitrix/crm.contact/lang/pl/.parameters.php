<?
$MESS["CRM_CONTACT_VAR"] = "Nazwa Zmiennej ID Kontaktu";
$MESS["CRM_ELEMENT_ID"] = "ID Kontaktu";
$MESS["CRM_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Szablon Ścieżki Strony Edytora Kontaktu";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "Szablon Ścieżki Strony Exportu";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Importuj Szablony Ścieżek Stron";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Szablon Ścieżki do Strony Indeksu";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Szablon Ścieżki Strony Kontaktów";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Szablon Ścieżki Strony Usług Sieciowych";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Szablon Ścieżki Strony Widoku Kontaktu";
?>