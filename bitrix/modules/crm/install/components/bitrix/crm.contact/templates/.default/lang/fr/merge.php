<?
$MESS["CRM_CONTACT_MERGE_HEADER_TEMPLATE"] = "Date de création : #DATE_CREATE#";
$MESS["CRM_CONTACT_MERGE_PAGE_TITLE"] = "Fusionner les contacts";
$MESS["CRM_CONTACT_MERGE_RESULT_LEGEND"] = "Sélectionnez le contact prioritaire dans la liste. Il sera utilisé comme base pour le profil de contact. Vous pouvez lui ajouter plus de données à partir d'autres contacts.";
?>