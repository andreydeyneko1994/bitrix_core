<?
$MESS["CRM_CONTACT_WGT_DEMO_CONTENT"] = "Jeżeli nie masz jeszcze kontaktów, <a href=\"#URL#\" class=\"#CLASS_NAME#\">utwórz nowy</a> teraz!";
$MESS["CRM_CONTACT_WGT_DEMO_TITLE"] = "To jest widok demonstracyjny. Zamknij go, aby zobaczyć dane analityczne Twoich kontaktów.";
$MESS["CRM_CONTACT_WGT_PAGE_TITLE"] = "Kontakty: Podsumowanie";
?>