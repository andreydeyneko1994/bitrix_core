<?
$MESS["CRM_CONTACT_MERGE_HEADER_TEMPLATE"] = "Utworzono #DATE_CREATE#";
$MESS["CRM_CONTACT_MERGE_PAGE_TITLE"] = "Scal kontakty";
$MESS["CRM_CONTACT_MERGE_RESULT_LEGEND"] = "Wybierz z listy kontakt priorytetowy. Będzie on służył jako podstawa profilu kontaktu. Możesz dodać do niego więcej danych z innych kontaktów.";
?>