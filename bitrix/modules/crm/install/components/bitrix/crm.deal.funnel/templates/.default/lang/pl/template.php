<?
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "Lejek";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "Pokaż/Ukryj filtr";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "Filtr";
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "Typ lejka";
$MESS["DEAL_STAGES_LOSE"] = "Nieaktywne";
$MESS["DEAL_STAGES_WON"] = "Aktywne";
$MESS["FUNNEL_CHART_HIDE"] = "Ukryj grafikę";
$MESS["FUNNEL_CHART_NO_DATA"] = "Brak danych do utworzenia grafiki";
$MESS["FUNNEL_CHART_SHOW"] = "Pokaż grafikę";
?>