<?
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Empresa";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Selecione os campos para o novo relatório";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_PHONE"] = "Telefone";
$MESS["CRM_COLUMN_TYPE"] = "pesquisar";
$MESS["CRM_OPER_EDIT"] = "Editar";
$MESS["CRM_OPER_SHOW"] = "Ver todos os filtros";
$MESS["CRM_SECTION_CONTACT_INFO"] = "configurações";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Ligar";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Autor da chamada desconhecido";
?>