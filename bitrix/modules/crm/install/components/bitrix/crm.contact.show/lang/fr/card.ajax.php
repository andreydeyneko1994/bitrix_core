<?
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Entreprise";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_PHONE"] = "Téléphone";
$MESS["CRM_COLUMN_TYPE"] = "Type de contact";
$MESS["CRM_OPER_EDIT"] = "Éditer";
$MESS["CRM_OPER_SHOW"] = "Affichage";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Information de contact";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Appelant inconnu";
?>