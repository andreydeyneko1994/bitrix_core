<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_COLUMN_NUMERATOR_LIST_ID"] = "ID";
$MESS["CRM_COLUMN_NUMERATOR_LIST_NAME"] = "Nom du modèle d'auto numérotation";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE"] = "Modèle de numéro";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE_NAME"] = "Les modèles utilisent de modèle de numérotation";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TYPE"] = "Type";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_NUMERATOR_LIST_CREATE_NUMERATOR"] = "Créer un modèle d'auto numérotation";
$MESS["CRM_NUMERATOR_LIST_DELETE"] = "Supprimer le modèle d'auto numérotation";
$MESS["CRM_NUMERATOR_LIST_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer \"%s\" ?";
$MESS["CRM_NUMERATOR_LIST_DELETE_ERROR"] = "Impossible de supprimer le modèle d'auto numérotation \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_DELETE_TITLE"] = "Supprimer ce modèle d'auto numérotation";
$MESS["CRM_NUMERATOR_LIST_EDIT"] = "Éditer";
$MESS["CRM_NUMERATOR_LIST_EDIT_ERROR"] = "Impossible d'enregistrer le modèle d'auto numérotation \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_EDIT_TITLE"] = "Éditer ce modèle d'auto numérotation";
$MESS["CRM_NUMERATOR_LIST_FILTER_ENTITIES"] = "Lien vers la section CRM";
$MESS["CRM_NUMERATOR_LIST_INTERNAL_ERROR_TITLE"] = "Une erreur inconnue est survenue.";
$MESS["CRM_NUMERATOR_LIST_PAGE_TITLE"] = "Modèles d'auto numérotation des documents";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["DOCUMENTGENERATOR_MODULE_NOT_INSTALLED"] = "Le module Document Generator n'est pas installé.";
?>