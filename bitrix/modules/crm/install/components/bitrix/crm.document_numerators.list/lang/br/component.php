<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_COLUMN_NUMERATOR_LIST_ID"] = "ID";
$MESS["CRM_COLUMN_NUMERATOR_LIST_NAME"] = "Nome do modelo de numeração automática";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE"] = "Modelo de número";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE_NAME"] = "Modelos usando este modelo de numeração";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TYPE"] = "Tipo";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_NUMERATOR_LIST_CREATE_NUMERATOR"] = "Criar modelo de numeração automática";
$MESS["CRM_NUMERATOR_LIST_DELETE"] = "Excluir modelo de numeração automática";
$MESS["CRM_NUMERATOR_LIST_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir \"%s\"?";
$MESS["CRM_NUMERATOR_LIST_DELETE_ERROR"] = "Não é possível excluir o modelo de numeração automática \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_DELETE_TITLE"] = "Excluir este modelo de numeração automática";
$MESS["CRM_NUMERATOR_LIST_EDIT"] = "Editar";
$MESS["CRM_NUMERATOR_LIST_EDIT_ERROR"] = "Não é possível salvar o modelo de numeração automática \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_EDIT_TITLE"] = "Editar este modelo de numeração automática";
$MESS["CRM_NUMERATOR_LIST_FILTER_ENTITIES"] = "Link para a seção do CRM";
$MESS["CRM_NUMERATOR_LIST_INTERNAL_ERROR_TITLE"] = "Ocorreu um erro desconhecido.";
$MESS["CRM_NUMERATOR_LIST_PAGE_TITLE"] = "Modelos de Numeração Automática de Documentos";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["DOCUMENTGENERATOR_MODULE_NOT_INSTALLED"] = "O módulo Documentgenerator não está instalado.";
?>