<?
$MESS["CRM_ALL"] = "Wszystkie";
$MESS["CRM_COLUMN_NUMERATOR_LIST_ID"] = "ID";
$MESS["CRM_COLUMN_NUMERATOR_LIST_NAME"] = "Nazwa szablonu numerowania automatycznego";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE"] = "Szablon numeru";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE_NAME"] = "Szablony korzystające z tego szablonu numerowania";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TYPE"] = "Rodzaj";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_NUMERATOR_LIST_CREATE_NUMERATOR"] = "Utwórz szablon numerowania automatycznego";
$MESS["CRM_NUMERATOR_LIST_DELETE"] = "Usuń szablon numerowania automatycznego";
$MESS["CRM_NUMERATOR_LIST_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć \"%s\"?";
$MESS["CRM_NUMERATOR_LIST_DELETE_ERROR"] = "Nie można usunąć szablonu automatycznego numerowania \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_DELETE_TITLE"] = "Usuń wybrany szablon numerowania automatycznego";
$MESS["CRM_NUMERATOR_LIST_EDIT"] = "Edytuj";
$MESS["CRM_NUMERATOR_LIST_EDIT_ERROR"] = "Nie można zapisać szablonu automatycznego numerowania \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_EDIT_TITLE"] = "Edytuj wybrany szablon numerowania automatycznego";
$MESS["CRM_NUMERATOR_LIST_FILTER_ENTITIES"] = "Link do sekcji CRM";
$MESS["CRM_NUMERATOR_LIST_INTERNAL_ERROR_TITLE"] = "Wystąpił nieznany błąd.";
$MESS["CRM_NUMERATOR_LIST_PAGE_TITLE"] = "Szablony automatycznego numerowania dokumentów";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["DOCUMENTGENERATOR_MODULE_NOT_INSTALLED"] = "Moduł Generator dokumentów nie jest zainstalowany.";
?>