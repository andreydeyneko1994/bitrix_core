<?
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_COLUMN_ACTIVE"] = "Actif(ve)";
$MESS["CRM_COLUMN_C_SORT"] = "Trier";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_RATE"] = "Coefficient";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_VAT_DELETION_GENERAL_ERROR"] = "Erreur au cours de la suppression d'un impôt";
$MESS["CRM_VAT_UPDATE_GENERAL_ERROR"] = "Lors de la mise à jour du montant de l'impôt une erreur s'est produite.";
?>