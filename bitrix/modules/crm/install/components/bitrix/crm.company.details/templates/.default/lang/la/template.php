<?php
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_GENERAL"] = "Todas las etapas y embudos.";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_JUNK"] = "Perdido";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_PIPELINE"] = "En progreso + ganado";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL"] = "Iniciar por la etapa requerida";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL_1"] = "Requerido en la etapa";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_SHORT"] = "Requerido";
$MESS["CRM_COMPANY_DETAIL_HISTORY_STUB"] = "Ahora está agregando una compañía...";
