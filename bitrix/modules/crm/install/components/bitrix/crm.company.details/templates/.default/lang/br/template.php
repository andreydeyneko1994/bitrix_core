<?php
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_GENERAL"] = "Todas as etapas e funis";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_JUNK"] = "Perdido";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_PIPELINE"] = "Em andamento + ganho";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL"] = "Obrigatório começar com etapa";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL_1"] = "Requerido na etapa";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_SHORT"] = "Obrigatório";
$MESS["CRM_COMPANY_DETAIL_HISTORY_STUB"] = "Você está adicionando uma empresa agora...";
