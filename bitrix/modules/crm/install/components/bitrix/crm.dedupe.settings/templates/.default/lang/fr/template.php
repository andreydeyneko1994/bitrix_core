<?php
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_CRITERION"] = "Sélectionnez les champs à comparer";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_SCOPE"] = "Pays";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_TITLE"] = "Dupliquer les paramètres d'analyse";
$MESS["CRM_DEDUPE_WIZARD_SELECT_ALL"] = "Tout sélectionner";
$MESS["CRM_DEDUPE_WIZARD_UNSELECT_ALL"] = "Tout désélectionner";
