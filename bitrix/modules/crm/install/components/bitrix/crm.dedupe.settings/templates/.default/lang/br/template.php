<?php
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_CRITERION"] = "Selecionar campos a comparar";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_SCOPE"] = "País";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_TITLE"] = "Configurações de verificação de duplicados";
$MESS["CRM_DEDUPE_WIZARD_SELECT_ALL"] = "Selecionar todos";
$MESS["CRM_DEDUPE_WIZARD_UNSELECT_ALL"] = "Desmarcar todos";
