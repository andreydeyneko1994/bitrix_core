<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_DEDUCTED"] = "Enviado";
$MESS["CRM_ORDER_NOT_DEDUCTED"] = "No enviado";
$MESS["CRM_ORDER_SHIPMENT_DELETE"] = "Eliminar";
$MESS["CRM_ORDER_SHIPMENT_DELETE_CONFIRM"] = "¿Usted está seguro que quiere eliminar este elemento?";
$MESS["CRM_ORDER_SHIPMENT_DELETE_TITLE"] = "Eliminar envío";
$MESS["CRM_ORDER_SHIPMENT_EDIT"] = "Editar";
$MESS["CRM_ORDER_SHIPMENT_EDIT_TITLE"] = "Editar envío";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD"] = "Agregar el envío";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD_SHORT"] = "Envío";
$MESS["CRM_ORDER_SHIPMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_SHIPMENT_REBUILD_ACCESS_ATTRS"] = "Los permisos de acceso actualizados requieren que actualice los atributos de acceso actuales utilizando la <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">página de gestión de permisos</a>.";
$MESS["CRM_ORDER_SHIPMENT_SHOW"] = "Ver";
$MESS["CRM_ORDER_SHIPMENT_SHOW_TITLE"] = "Ver envío";
$MESS["CRM_ORDER_SHIPMENT_SUMMARY"] = "Envío ##NUMBER# - #DATE#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar cantidad";
?>