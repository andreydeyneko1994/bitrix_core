<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_DEDUCTED"] = "Enviado";
$MESS["CRM_ORDER_NOT_DEDUCTED"] = "Não enviado";
$MESS["CRM_ORDER_SHIPMENT_DELETE"] = "Excluir";
$MESS["CRM_ORDER_SHIPMENT_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir este item?";
$MESS["CRM_ORDER_SHIPMENT_DELETE_TITLE"] = "Excluir envio";
$MESS["CRM_ORDER_SHIPMENT_EDIT"] = "Editar";
$MESS["CRM_ORDER_SHIPMENT_EDIT_TITLE"] = "Editar envio";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD"] = "Adicionar envio";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD_SHORT"] = "Envio";
$MESS["CRM_ORDER_SHIPMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_SHIPMENT_REBUILD_ACCESS_ATTRS"] = "As permissões de acesso atualizadas exigem que você atualize os atuais atributos de acesso usando a <a id=\"#ID#\" target=\"_blank\" href= \"#URL#\">página de gerenciamento de permissões</a>.";
$MESS["CRM_ORDER_SHIPMENT_SHOW"] = "Visualizar";
$MESS["CRM_ORDER_SHIPMENT_SHOW_TITLE"] = "Visualizar envio";
$MESS["CRM_ORDER_SHIPMENT_SUMMARY"] = "Envio ##NUMBER# - #DATE#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
?>