<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_TAX_ADD"] = "Dodaj podatek";
$MESS["CRM_TAX_ADD_TITLE"] = "Utwórz nowy podatek";
$MESS["CRM_TAX_DELETE"] = "Usuń podatek";
$MESS["CRM_TAX_DELETE_DLG_BTNTITLE"] = "Usuń podatek";
$MESS["CRM_TAX_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć ten podatek?";
$MESS["CRM_TAX_DELETE_DLG_TITLE"] = "Usuń podatek";
$MESS["CRM_TAX_DELETE_TITLE"] = "Usuń podatek";
$MESS["CRM_TAX_EDIT"] = "Edytuj";
$MESS["CRM_TAX_EDIT_TITLE"] = "Edytuj podatek";
$MESS["CRM_TAX_LIST"] = "Podatki";
$MESS["CRM_TAX_LIST_TITLE"] = "Wyświetl wszystkie podatki";
$MESS["CRM_TAX_SETTINGS"] = "Ustawienia";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Konfiguruj system podatkowy";
$MESS["CRM_TAX_SHOW"] = "Wyświetl";
$MESS["CRM_TAX_SHOW_TITLE"] = "Wyświetl podatek";
?>