<?
$MESS["CRM_COMPANY_MERGE_HEADER_TEMPLATE"] = "Creado el #DATE_CREATE#";
$MESS["CRM_COMPANY_MERGE_PAGE_TITLE"] = "Fusionar compañías";
$MESS["CRM_COMPANY_MERGE_RESULT_LEGEND"] = "Seleccione la prioridad de los prospectos de la lista. Se utilizará como base para el perfil de los prospectos. Puede agregar más datos de los otros prospectos.";
?>