<?
$MESS["CRM_COMPANY_MERGE_HEADER_TEMPLATE"] = "Criado em #DATE_CREATE#";
$MESS["CRM_COMPANY_MERGE_PAGE_TITLE"] = "Mesclar empresas";
$MESS["CRM_COMPANY_MERGE_RESULT_LEGEND"] = "Selecione o lead prioritário na lista. Ele será usado como base para o perfil de lead. Você pode adicionar mais dados de outros leads.";
?>