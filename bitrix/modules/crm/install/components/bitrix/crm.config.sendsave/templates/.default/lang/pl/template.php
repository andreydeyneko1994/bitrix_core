<?
$MESS["CRM_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Nie zapisuj bieżących parametrów";
$MESS["CRM_BUTTON_DELETE"] = "Usuń";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "Usuń bieżące parametry integracji";
$MESS["CRM_BUTTON_SAVE"] = "Zapisz";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Zapisz parametry integracji";
$MESS["CRM_TAB_CONFIG"] = "Integracja poczty z CRM";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Wyślij i zapisz parametry integracji";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "Integracja poczty z CRM";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "Wyślij i zapisz Parametry Integracji E-mail";
?>