<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Błąd podczas przetwarzania wniosku.";
$MESS["INTR_MAIL_AUTH"] = "Błąd uwierzytelnienia";
$MESS["INTR_MAIL_CHECK_INTERVAL_10M"] = "10 minut";
$MESS["INTR_MAIL_CHECK_INTERVAL_12H"] = "12 godziny";
$MESS["INTR_MAIL_CHECK_INTERVAL_1H"] = "1 godzina";
$MESS["INTR_MAIL_CHECK_INTERVAL_2M"] = "2 minuty";
$MESS["INTR_MAIL_CHECK_INTERVAL_3H"] = "3 godziny";
$MESS["INTR_MAIL_CHECK_INTERVAL_5M"] = "5 minut";
$MESS["INTR_MAIL_CRM_ALREADY"] = "Już skonfigurowano konta e-mail programu CRM";
$MESS["INTR_MAIL_CSRF"] = "Błąd zabezpieczeń podczas składania formularza.";
$MESS["INTR_MAIL_FORM_ERROR"] = "Błąd przetwarzania formularza.";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Błąd uwierzytelniania. Sprawdź, czy login i hasło są poprawne. <br>Jeśli używasz haseł do aplikacji, a włączone jest uwierzytelnianie dwustopniowe, należy użyć specjalnego hasła integracji.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Wybierz foldery do synchronizacji";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Błąd pobierania danych skrzynki pocztowej";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Nieprawidłowy adres e-mail";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Proszę określić interwał synchronizacji.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Błąd zapisywania danych.";
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "Moduł Mail nie jest zainstalowany.";
?>