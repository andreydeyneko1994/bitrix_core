<?
$MESS["CRM_ACTIVITY_WGT_DEMO_CONTENT"] = "Jeżeli nie masz jeszcze żadnej aktywności, <a href=\"#URL#\" class=\"#CLASS_NAME#\">utwórz nową</a> teraz!";
$MESS["CRM_ACTIVITY_WGT_DEMO_TITLE"] = "To jest widok demonstracyjny. Zamknij go, aby przeglądać dane Twoich aktywności.";
$MESS["CRM_ACTIVITY_WGT_PAGE_TITLE"] = "Raport podsumowujący aktywności";
?>