<?php
$MESS["interface_filter_days"] = "d";
$MESS["interface_filter_note"] = "Los resultados se mostrarán según los criterios del filtro.";
$MESS["interface_filter_note_clear"] = "Borrar filtro";
$MESS["interface_grid_additional"] = "Más filtros";
$MESS["interface_grid_find"] = "Buscar";
$MESS["interface_grid_find_title"] = "Buscar registros que coincidan con los criterios de búsqueda";
$MESS["interface_grid_flt_cancel"] = "Cancelar";
$MESS["interface_grid_flt_cancel_title"] = "Mostrar todos los registros";
$MESS["interface_grid_hide_all"] = "Ocultar todos los filtros";
$MESS["interface_grid_no_no_no"] = "(no)";
$MESS["interface_grid_search"] = "Buscar";
$MESS["interface_grid_show_all"] = "Mostrar todos los filtros";
$MESS["main_interface_filter_save"] = "Guardar como...";
$MESS["main_interface_filter_save_title"] = "Guardar el filtro actual";
$MESS["main_interface_filter_saved"] = "Filtros guardados";
$MESS["main_interface_filter_saved_apply"] = "Aplicar el filtro guardado";
