<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_NEW_ENT_COUNTER_COMPANY_CAPTION"] = "Nowe firmy";
$MESS["CRM_NEW_ENT_COUNTER_CONTACT_CAPTION"] = "Nowe kontakty";
$MESS["CRM_NEW_ENT_COUNTER_DEAL_CAPTION"] = "Nowe deale";
$MESS["CRM_NEW_ENT_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "Typ jednostki nie jest określony.";
$MESS["CRM_NEW_ENT_COUNTER_LEAD_CAPTION"] = "Nowe leady";
?>