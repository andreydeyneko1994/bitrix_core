<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_NEW_ENT_COUNTER_COMPANY_CAPTION"] = "Nouvelles sociétés";
$MESS["CRM_NEW_ENT_COUNTER_CONTACT_CAPTION"] = "Nouveaux contacts";
$MESS["CRM_NEW_ENT_COUNTER_DEAL_CAPTION"] = "Nouvelles transactions";
$MESS["CRM_NEW_ENT_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "Le type d'entité n'est pas spécifié.";
$MESS["CRM_NEW_ENT_COUNTER_LEAD_CAPTION"] = "Nouveaux prospects";
?>