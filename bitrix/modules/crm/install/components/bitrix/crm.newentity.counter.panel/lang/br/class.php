<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_NEW_ENT_COUNTER_COMPANY_CAPTION"] = "Novas empresas";
$MESS["CRM_NEW_ENT_COUNTER_CONTACT_CAPTION"] = "Novos contatos";
$MESS["CRM_NEW_ENT_COUNTER_DEAL_CAPTION"] = "Novos negócios";
$MESS["CRM_NEW_ENT_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "O tipo de entidade não está especificado.";
$MESS["CRM_NEW_ENT_COUNTER_LEAD_CAPTION"] = "Novos clientes potenciais";
?>