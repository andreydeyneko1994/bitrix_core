<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_TAXRATE_DELETE"] = "Eliminer le taux";
$MESS["CRM_TAXRATE_DELETE_CONFIRM"] = "Êtes-vous sûr de vouloir supprimer '%s' ?";
$MESS["CRM_TAXRATE_DELETE_TITLE"] = "Supprimer ce pari";
$MESS["CRM_TAXRATE_EDIT"] = "Éditer le tarif";
$MESS["CRM_TAXRATE_EDIT_TITLE"] = "Aller à l'édition de ce taux";
?>