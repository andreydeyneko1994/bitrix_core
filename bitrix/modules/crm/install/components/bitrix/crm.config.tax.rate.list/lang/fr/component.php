<?php
$MESS["CRM_COLUMN_ACTIVE"] = "Activité";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "Modalités d'application";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "Inclure la TVA au prix";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "Type du client";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_COLUMN_VALUE"] = "Coefficient";
$MESS["CRM_COMPANY_PT"] = "Entreprise";
$MESS["CRM_CONTACT_PT"] = "Contact";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "Erreur survenue au cours de la suppression du taux de l'impôt.";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "Erreur au cours du rafraîchissement du taux de l'impôt.";
