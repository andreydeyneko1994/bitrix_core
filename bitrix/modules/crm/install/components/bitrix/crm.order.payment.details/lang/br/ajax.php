<?
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "O pagamento não foi encontrado";
$MESS["CRM_ORDER_P_ACCESS_DENIED"] = "Permissões insuficientes.";
$MESS["CRM_ORDER_P_FORM_DATA_MISSING"] = "Faltam alguns dados do formulário";
$MESS["CRM_ORDER_P_NOT_FOUND"] = "O pagamento não foi encontrado";
?>