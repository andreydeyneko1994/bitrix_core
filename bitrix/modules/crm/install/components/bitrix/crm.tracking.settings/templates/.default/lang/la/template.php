<?
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW"] = "Ventana de atribución";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_DAYS"] = "días";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE"] = "Usar la ventana de atribución para prospectos y negociaciones";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE_HINT"] = "Los prospectos y las negociaciones creadas fuera del tráfico del sitio web recibirán la misma fuente que el contacto o la compañía asociada, si la fecha de registro no precede a la ventana de atribución.";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE"] = "Ventana de atribución";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE_HINT"] = "El número de días entre el momento en que una persona interactúa por primera vez con su campaña publicitaria y luego realiza una acción rastreada por Bitrix24 se denomina ventana de atribución. Puede cambiar la duración del período de atribución según sea necesario para aumentar la precisión del reporte.";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN"] = "Dominios referentes";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN_USE"] = "Agregar fuente para el tráfico de redes sociales sin la etiqueta utm_source";
?>