<?
$MESS["CRM_ACTIVITY_CREATE_ERROR"] = "Erro ao criar atividade";
$MESS["CRM_ACTIVITY_FILE_ERROR"] = "Erro de gravação de arquivo";
$MESS["CRM_ACTIVITY_NO_RECORD_ERROR"] = "Erro ao salvar a gravação.";
$MESS["CRM_ACTIVITY_VISIT_DESCRIPTION"] = "A gravação do evento ocorreu em #DATE#";
$MESS["CRM_ACTIVITY_VISIT_FACE_NOTHING_FOUND"] = "Não foram encontradas inserções.";
$MESS["CRM_ACTIVITY_VISIT_FACE_SERVER_ERROR"] = "Erro de reconhecimento";
$MESS["CRM_ACTIVITY_VISIT_FILE_TOO_LARGE"] = "Não é possível enviar a imagem porque o tamanho do arquivo excede o limite.";
$MESS["CRM_ACTIVITY_VISIT_LEAD_CREATE_ERROR"] = "Erro ao criar novo cliente potencial";
$MESS["CRM_ACTIVITY_VISIT_LEAD_TITLE"] = "Novo visitante (#DATE#)";
$MESS["CRM_ACTIVITY_VISIT_NO_FACEID"] = "O módulo Reconhecimento facial não está instalado.";
$MESS["CRM_ACTIVITY_VISIT_NO_PICTURE"] = "Erro ao enviar a imagem";
$MESS["CRM_ACTIVITY_VISIT_SUBJECT"] = "Gravação de evento";
?>