<?
$MESS["CRM_ACTIVITY_CREATE_ERROR"] = "Błąd tworzenia działania";
$MESS["CRM_ACTIVITY_FILE_ERROR"] = "Błąd zapisywania pliku";
$MESS["CRM_ACTIVITY_NO_RECORD_ERROR"] = "Błąd zapisywania nagrania.";
$MESS["CRM_ACTIVITY_VISIT_DESCRIPTION"] = "Zdarzenie nagrdnia #DATE#";
$MESS["CRM_ACTIVITY_VISIT_FACE_NOTHING_FOUND"] = "Nie znaleziono wpisów.";
$MESS["CRM_ACTIVITY_VISIT_FACE_SERVER_ERROR"] = "Błąd rozpoznawania";
$MESS["CRM_ACTIVITY_VISIT_FILE_TOO_LARGE"] = "Nie można wysłać obrazu, ponieważ rozmiar pliku jest większy niż dozwolony.";
$MESS["CRM_ACTIVITY_VISIT_LEAD_CREATE_ERROR"] = "Błąd tworzenia nowego leadu";
$MESS["CRM_ACTIVITY_VISIT_LEAD_TITLE"] = "Nowy gość (#DATE#)";
$MESS["CRM_ACTIVITY_VISIT_NO_FACEID"] = "Moduł rozpoznawania twarzy nie jest zainstalowany.";
$MESS["CRM_ACTIVITY_VISIT_NO_PICTURE"] = "Błąd wysyłania obrazu";
$MESS["CRM_ACTIVITY_VISIT_SUBJECT"] = "Rejestracja zdarzeń";
?>