<?
$MESS["CRM_COMPANY_EDIT_EVENT_CANCELED"] = "Działanie zostało anulowane. Zostaniesz przekierowany na poprzednią stronę. Jeśli nadal jesteś na tej stronie, zamknij ją ręcznie.";
$MESS["CRM_COMPANY_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Firma <a href='#URL#'>#TITLE#</a> została utworzona. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli nadal jesteś na tej stronie, zamknij ją ręcznie.";
?>