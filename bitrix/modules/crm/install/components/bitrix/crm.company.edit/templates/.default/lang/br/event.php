<?
$MESS["CRM_COMPANY_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se você ainda estiver nesta página, encerre-a manualmente.";
$MESS["CRM_COMPANY_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "A empresa <a href='#URL#'>#TITLE#</a> foi criada com sucesso. Agora você está sendo redirecionado para a página anterior. Se você ainda estiver nesta página, feche-a manualmente.";
?>