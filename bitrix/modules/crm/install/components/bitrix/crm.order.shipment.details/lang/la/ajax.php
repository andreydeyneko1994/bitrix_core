<?
$MESS["CRM_ORDER_SD_ERROR_SHIPPING_DEDUCTED"] = "No se puede agregar el producto porque el envío ya se completó.";
$MESS["CRM_ORDER_SD_FORM_DATA_MISSING"] = "Faltan algunos datos del formulario";
$MESS["CRM_ORDER_SD_INSUFFICIENT_RIGHTS"] = "Permisos insuficientes.";
$MESS["CRM_ORDER_SD_SHIPMENT_NOT_FOUND"] = "No se encontró el envío";
?>