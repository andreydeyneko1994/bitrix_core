<?
$MESS["CRM_ORDER_SD_ERROR_SHIPPING_DEDUCTED"] = "Nie można dodać produktu, ponieważ wysyłka została już zrealizowana.";
$MESS["CRM_ORDER_SD_FORM_DATA_MISSING"] = "Brak niektórych danych formularza";
$MESS["CRM_ORDER_SD_INSUFFICIENT_RIGHTS"] = "Niewystarczające uprawnienia.";
$MESS["CRM_ORDER_SD_SHIPMENT_NOT_FOUND"] = "Nie znaleziono wysyłki";
?>