<?
$MESS["CRM_ODCA_COMPANY"] = "Compañía";
$MESS["CRM_ODCA_CONTACT"] = "Contacto";
$MESS["CRM_ODCA_DATE_UPDATED"] = "Modificado el";
$MESS["CRM_ODCA_GO_ORDER"] = "Ver";
$MESS["CRM_ODCA_ORDER_NUM"] = "##ORDER_NUMBER#";
$MESS["CRM_ODCA_PRICE"] = "Precio";
$MESS["CRM_ODCA_PRODUCTS"] = "Productos";
$MESS["CRM_ODCA_STATUS"] = "Estado";
?>