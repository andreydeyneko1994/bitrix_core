<?
$MESS["CRM_ORDER_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ORDER_DA_ADD_COUPON_ERROR"] = "Erro ao adicionar o cupom";
$MESS["CRM_ORDER_DA_BASKET_CODE_ABSENT"] = "Está faltando o ID do item do carrinho de compras";
$MESS["CRM_ORDER_DA_BASKET_ID_BY_CODE_ERROR"] = "Não é possível obter o ID do item do carrinho de compras";
$MESS["CRM_ORDER_DA_CART_NOT_FOUND"] = "O carrinho do pedido não foi encontrado";
$MESS["CRM_ORDER_DA_COUPONS_ABSENT"] = "Estão faltando os cupons";
$MESS["CRM_ORDER_DA_CURRENCY_CHANGED"] = "Moeda do pedido atualizada. Verifique se todos os preços foram convertidos corretamente.";
$MESS["CRM_ORDER_DA_DELETE_COUPON_ERROR"] = "Erro ao excluir o cupom";
$MESS["CRM_ORDER_DA_DELIVERY_INDEX_ERROR"] = "Código postal de envio indefinido";
$MESS["CRM_ORDER_DA_GROUP_ACTION_ABSENT"] = "Não foi especificada uma ação de grupo";
$MESS["CRM_ORDER_DA_INSUFFICIENT_RIGHTS"] = "Permissões insuficientes.";
$MESS["CRM_ORDER_DA_ORDER_ID_NEGATIVE"] = "É necessário um ID de pedido válido ou inserção de dados para criar um novo pedido";
$MESS["CRM_ORDER_DA_PRODUCT_ID_NOT_DEFINED"] = "ID do produto indefinido";
$MESS["CRM_ORDER_DA_PROPERTIES_ABSENT"] = "Está faltando o SKU do pedido de propriedade";
$MESS["CRM_ORDER_DA_PROPERTIES_ORDER_ABSENT"] = "A ordem de propriedades SKU está faltando";
$MESS["CRM_ORDER_DA_QUANTITY_ABSENT"] = "A quantidade do produto é indefinida";
$MESS["CRM_ORDER_NOT_FOUND"] = "O pedido não foi encontrado";
$MESS["CRM_ORDER_NOT_SELECTED"] = "o campo está vazio";
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "O pagamento não foi encontrado";
$MESS["CRM_ORDER_SHIPMENT_NOT_FOUND"] = "O envio não foi encontrado";
$MESS["CRM_ORDER_WRONG_FIELD_NAME"] = "Nome de campo invalido";
$MESS["CRM_ORDER_WRONG_FIELD_VALUE"] = "Valor de campo inválido";
$MESS["CRM_ORDER_WRONG_PROPERTY_NAME"] = "Nome de propriedade inválido";
$MESS["CRM_ORDER_WRONG_PROPERTY_TYPE"] = "Tipo de propriedade inválido";
?>