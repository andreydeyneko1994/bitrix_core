<?
$MESS["CRM_ORDER_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["CRM_ORDER_DA_ADD_COUPON_ERROR"] = "Błąd dodawania kuponu";
$MESS["CRM_ORDER_DA_BASKET_CODE_ABSENT"] = "Brak ID pozycji z koszyka";
$MESS["CRM_ORDER_DA_BASKET_ID_BY_CODE_ERROR"] = "Nie można uzyskać ID pozycji z koszyka";
$MESS["CRM_ORDER_DA_CART_NOT_FOUND"] = "Nie znaleziono koszyka zamówienia";
$MESS["CRM_ORDER_DA_COUPONS_ABSENT"] = "Brak kuponów";
$MESS["CRM_ORDER_DA_CURRENCY_CHANGED"] = "Zaktualizowano walutę zamówienia. Sprawdź, czy wszystkie ceny zostały poprawnie przeliczone.";
$MESS["CRM_ORDER_DA_DELETE_COUPON_ERROR"] = "Błąd usuwania kuponu";
$MESS["CRM_ORDER_DA_DELIVERY_INDEX_ERROR"] = "Niezdefiniowany kod pocztowy wysyłki";
$MESS["CRM_ORDER_DA_GROUP_ACTION_ABSENT"] = "Nie określono działania grupowego";
$MESS["CRM_ORDER_DA_INSUFFICIENT_RIGHTS"] = "Niewystarczające uprawnienia.";
$MESS["CRM_ORDER_DA_ORDER_ID_NEGATIVE"] = "Aby utworzyć nowe zamówienie, wymagane jest prawidłowe ID zamówienia lub dane wejściowe";
$MESS["CRM_ORDER_DA_PRODUCT_ID_NOT_DEFINED"] = "Nie zdefiniowano ID produktu";
$MESS["CRM_ORDER_DA_PROPERTIES_ABSENT"] = "Brak właściwości SKU zamówienia";
$MESS["CRM_ORDER_DA_PROPERTIES_ORDER_ABSENT"] = "Brak kolejności właściwości SKU";
$MESS["CRM_ORDER_DA_QUANTITY_ABSENT"] = "Nieokreślona liczba produktów";
$MESS["CRM_ORDER_NOT_FOUND"] = "Nie znaleziono zamówienia";
$MESS["CRM_ORDER_NOT_SELECTED"] = "pole jest puste";
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "Nie znaleziono płatności";
$MESS["CRM_ORDER_SHIPMENT_NOT_FOUND"] = "Nie znaleziono wysyłki";
$MESS["CRM_ORDER_WRONG_FIELD_NAME"] = "Nieprawidłowa nazwa pola";
$MESS["CRM_ORDER_WRONG_FIELD_VALUE"] = "Nieprawidłowa wartość pola";
$MESS["CRM_ORDER_WRONG_PROPERTY_NAME"] = "Nieprawidłowa nazwa właściwości";
$MESS["CRM_ORDER_WRONG_PROPERTY_TYPE"] = "Nieprawidłowy typ właściwości";
?>