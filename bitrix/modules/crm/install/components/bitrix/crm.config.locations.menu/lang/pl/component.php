<?
$MESS["CRM_LOCATIONS_IMPORT"] = "Importuj";
$MESS["CRM_LOCATIONS_IMPORT_TITLE"] = "Import Lokalizacji";
$MESS["CRM_LOC_ADD"] = "Dodawanie";
$MESS["CRM_LOC_ADD_TITLE"] = "Utwórz nową lokalizację";
$MESS["CRM_LOC_DELETE"] = "Usuń";
$MESS["CRM_LOC_DELETE_DLG_BTNTITLE"] = "Usuń lokalizację";
$MESS["CRM_LOC_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć tę lokalizację?";
$MESS["CRM_LOC_DELETE_DLG_TITLE"] = "Usuń lokalizację";
$MESS["CRM_LOC_DELETE_TITLE"] = "Usuń lokalizację";
$MESS["CRM_LOC_EDIT"] = "Edytuj";
$MESS["CRM_LOC_EDIT_TITLE"] = "Otwórz lokalizację do edycji";
$MESS["CRM_LOC_LIST"] = "Lokalizacje";
$MESS["CRM_LOC_LIST_TITLE"] = "Wyświetl wszystkie lokalizacje";
$MESS["CRM_LOC_STEP_UP"] = "W górę";
$MESS["CRM_LOC_STEP_UP_TITLE"] = "Jeden poziom wyżej";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
?>