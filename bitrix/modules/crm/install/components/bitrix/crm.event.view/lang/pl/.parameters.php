<?
$MESS["CRM_ENTITY_ID"] = "ID Jednostki";
$MESS["CRM_ENTITY_TYPE"] = "Typ jednostki";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Firma";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Deal";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Oferta";
$MESS["CRM_EVENT_COUNT"] = "Wydarzeń na stronę";
$MESS["CRM_EVENT_ENTITY_LINK"] = "Pokaż tytuł jednostki";
$MESS["CRM_NAME_TEMPLATE"] = "Nazwa Formatu";
?>