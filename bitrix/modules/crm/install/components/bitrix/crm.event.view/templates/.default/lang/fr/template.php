<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_EVENT_DELETE"] = "Supprimer";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer ?";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Suppression de l'Evénement";
$MESS["CRM_EVENT_TABLE_EMPTY"] = "- Pas de données -";
$MESS["CRM_EVENT_TABLE_FILES"] = "Fichiers";
$MESS["CRM_EVENT_VIEW_ADD"] = "Ajouter un évènement";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Evènement";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Voir la section de menu / masquer";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Filtre";
$MESS["CRM_IMPORT_EVENT"] = "Si email du contractant est mentionné dans la carte CRM, vous pouvez automatiquement enregistrer votre correspondance par Adresse emailavec lui comme l'Evènement. Pour le faire, transférez l'email reçu à l'adresse <b>% EMAIL%</b> et le système ajoutera automatiquement le texte, ainsi que les pièces jointes comme l'Evènement à ce contractant.";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
?>