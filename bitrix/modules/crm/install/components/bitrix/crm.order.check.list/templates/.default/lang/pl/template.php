<?
$MESS["CRM_ALL"] = "Ogółem";
$MESS["CRM_CHECK_LIST_ADD"] = "Dodaj pokwitowanie";
$MESS["CRM_CHECK_LIST_ADD_SHORT"] = "Pokwitowanie";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS"] = "Zaktualizuj status";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS_TITLE"] = "Zaktualizuj status pokwitowania";
$MESS["CRM_ORDER_CHECK_DELETE"] = "Usuń";
$MESS["CRM_ORDER_CHECK_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć ten element?";
$MESS["CRM_ORDER_CHECK_DELETE_TITLE"] = "Usuń pokwitowanie";
$MESS["CRM_ORDER_CHECK_SHOW"] = "Wyświetl";
$MESS["CRM_ORDER_CHECK_SHOW_TITLE"] = "Wyświetl pokwitowanie";
$MESS["CRM_ORDER_CHECK_TITLE"] = "Pokwitowanie ##ID# od #DATE_CREATE#";
$MESS["CRM_ORDER_CHECK_URL"] = "Link do pokwitowania";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "Płatność ##ACCOUNT_NUMBER# od #DATE_BILL#";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "Wysyłka ##ACCOUNT_NUMBER# od #DATE_INSERT#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Pokaż ilość";
?>