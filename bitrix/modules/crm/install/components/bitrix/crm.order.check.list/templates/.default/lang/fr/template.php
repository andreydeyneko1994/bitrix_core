<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CHECK_LIST_ADD"] = "Ajouter un reçu";
$MESS["CRM_CHECK_LIST_ADD_SHORT"] = "Reçu";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS"] = "Mettre à jour le statut";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS_TITLE"] = "Mettre à jour le statut du reçu";
$MESS["CRM_ORDER_CHECK_DELETE"] = "Supprimer";
$MESS["CRM_ORDER_CHECK_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cet élément ?";
$MESS["CRM_ORDER_CHECK_DELETE_TITLE"] = "Supprimer le reçu";
$MESS["CRM_ORDER_CHECK_SHOW"] = "Afficher";
$MESS["CRM_ORDER_CHECK_SHOW_TITLE"] = "Afficher le reçu";
$MESS["CRM_ORDER_CHECK_TITLE"] = "Reçu ##ID# de #DATE_CREATE#";
$MESS["CRM_ORDER_CHECK_URL"] = "Lien du reçu";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "Paiement ##ACCOUNT_NUMBER# de #DATE_BILL#";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "Livraison ##ACCOUNT_NUMBER# de #DATE_INSERT#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
?>