<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CHECK_LIST_ADD"] = "Adicionar recibo";
$MESS["CRM_CHECK_LIST_ADD_SHORT"] = "Recibo";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS"] = "Atualizar status";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS_TITLE"] = "Atualizar o status do recibo";
$MESS["CRM_ORDER_CHECK_DELETE"] = "Excluir";
$MESS["CRM_ORDER_CHECK_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir este item?";
$MESS["CRM_ORDER_CHECK_DELETE_TITLE"] = "Excluir recibo";
$MESS["CRM_ORDER_CHECK_SHOW"] = "Visualizar";
$MESS["CRM_ORDER_CHECK_SHOW_TITLE"] = "Visualizar recibo";
$MESS["CRM_ORDER_CHECK_TITLE"] = "Recibo ##ID# de #DATE_CREATE#";
$MESS["CRM_ORDER_CHECK_URL"] = "Link do recibo";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "Pagamento ##ACCOUNT_NUMBER# de #DATE_BILL#";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "Entrega ##ACCOUNT_NUMBER# de #DATE_INSERT#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
?>