<?
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_DESC"] = "Utwórz dokument CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_ID"] = "ID dokumentu CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_NAME"] = "Utwórz dokument CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_NUMBER"] = "Nr dokumentu";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_URL"] = "Publiczny link do dokumentu";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCX_FILE"] = "Plik DOCX";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_PDF_FILE"] = "Plik PDF";
?>