<?
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_DESC"] = "Crear Documento del CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_ID"] = "ID del Documento del CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_NAME"] = "Crear Documento del CRM";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_NUMBER"] = "Documento #";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCUMENT_URL"] = "Documento de enlace público";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_DOCX_FILE"] = "Archivo DOCX";
$MESS["CRM_ACTIVITY_GENERATE_ENTITY_PDF_FILE"] = "Archivo PDF";
?>