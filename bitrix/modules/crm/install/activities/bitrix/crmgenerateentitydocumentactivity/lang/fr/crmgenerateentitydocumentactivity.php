<?php
$MESS["CRM_GEDA_EMPTY_TEMPLATE_ID"] = "Le modèle du document n'est pas spécifié";
$MESS["CRM_GEDA_MODULE_DOCGEN_ERROR"] = "Le module Document Generator n'est pas installé.";
$MESS["CRM_GEDA_NAME_CREATE_ACTIVITY"] = "Ajouter l'activité";
$MESS["CRM_GEDA_NAME_DELETE"] = "Supprimer";
$MESS["CRM_GEDA_NAME_MY_COMPANY_BANK_DETAIL_ID"] = "Coordonnées bancaires de mon entreprise";
$MESS["CRM_GEDA_NAME_MY_COMPANY_ID"] = "Mon entreprise";
$MESS["CRM_GEDA_NAME_MY_COMPANY_REQUISITE_ID"] = "Informations sur mon entreprise";
$MESS["CRM_GEDA_NAME_PUBLIC_URL"] = "Créer un lien public";
$MESS["CRM_GEDA_NAME_TEMPLATE_ID"] = "Modèle";
$MESS["CRM_GEDA_NAME_USE_SUBSCRIPTION"] = "Attendre la fin de la conversion PDF";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG"] = "Le flux de travail attend la fin de la conversion du document";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG_COMPLETE"] = "Document converti";
$MESS["CRM_GEDA_NAME_WITH_STAMPS"] = "Avec signature et cachet";
$MESS["CRM_GEDA_PROVIDER_NOT_FOUND"] = "Impossible de trouver un gestionnaire pour ce type d'entité";
$MESS["CRM_GEDA_TRANSFORMATION_ERROR"] = "Erreur lors de la conversion du document : ";
