<?php
$MESS["CRM_CHANGE_NEW_RESPONSIBLE_ID"] = "Responsable changé. Nouveau :";
$MESS["CRM_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "La personne responsable n'est pas spécifiée.";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE"] = "Sélectionner un nouveau responsable";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_F"] = "première disponibilité";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_R"] = "dans un ordre aléatoire";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_S"] = "dans un ordre spécifique";
$MESS["CRM_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Modifier de la part de";
$MESS["CRM_CHANGE_RESPONSIBLE_NEW"] = "Nouvelle personne responsable";
$MESS["CRM_CHANGE_RESPONSIBLE_SKIP_ABSENT"] = "Ignorer les absents";
