<?php
$MESS["CRM_CHANGE_NEW_RESPONSIBLE_ID"] = "Osobę odpowiedzialną zmieniono na";
$MESS["CRM_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "Osoba odpowiedzialna nie jest określona.";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE"] = "Wybierz nową osobę odpowiedzialną";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_F"] = "pierwszą dostępną";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_R"] = "w losowej kolejności";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_S"] = "w określonej kolejności";
$MESS["CRM_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Zmień w imieniu";
$MESS["CRM_CHANGE_RESPONSIBLE_NEW"] = "Nowa osoba odpowiedzialna";
$MESS["CRM_CHANGE_RESPONSIBLE_SKIP_ABSENT"] = "Pomiń osoby nieobecne";
