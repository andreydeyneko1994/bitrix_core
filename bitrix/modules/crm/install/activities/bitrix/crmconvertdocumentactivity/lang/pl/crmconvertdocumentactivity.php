<?php
$MESS["CRM_CVTDA_COMPANY"] = "Firma";
$MESS["CRM_CVTDA_CONTACT"] = "Kontakt";
$MESS["CRM_CVTDA_DEAL"] = "Deal";
$MESS["CRM_CVTDA_DEAL_CATEGORY_ID"] = "Lejek";
$MESS["CRM_CVTDA_DEFAULT_CONTACT_NAME"] = "Brak nazwy";
$MESS["CRM_CVTDA_DISABLE_ACTIVITY_COMPLETION"] = "Nie zamykaj działania po konwersji";
$MESS["CRM_CVTDA_EMPTY_PROP"] = "Nie określono jednostek do utworzenia";
$MESS["CRM_CVTDA_INCORRECT_DOCUMENT"] = "Nie można konwertować elementów tego typu";
$MESS["CRM_CVTDA_INVOICE"] = "Faktura";
$MESS["CRM_CVTDA_ITEMS"] = "Utwórz ze źródła";
$MESS["CRM_CVTDA_QUOTE"] = "Oferta";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_DEAL"] = "Następujące jednostki muszą być tworzone za pomocą Dealu: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_LEAD"] = "Następujące jednostki muszą być tworzone za pomocą Leadu: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_DEAL"] = "Deal musi zostać przekonwertowany";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_LEAD"] = "Lead musi zostać przekonwertowany";
$MESS["CRM_CVTDA_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_CVTDA_WIZARD_NOT_FOUND"] = "Nie można zainicjować Kreatora konwersji.";
