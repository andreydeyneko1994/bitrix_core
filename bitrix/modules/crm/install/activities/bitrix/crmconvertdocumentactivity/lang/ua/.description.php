<?php
$MESS["CRM_CVTDA_DESC"] = "Створити на підставі";
$MESS["CRM_CVTDA_NAME"] = "Створити на підставі";
$MESS["CRM_CVTDA_RETURN_COMPANY_ID"] = "ID компанії";
$MESS["CRM_CVTDA_RETURN_CONTACT_ID"] = "ID контакту";
$MESS["CRM_CVTDA_RETURN_DEAL_ID"] = "ID угоди";
$MESS["CRM_CVTDA_RETURN_INVOICE_ID"] = "ID рахунку";
$MESS["CRM_CVTDA_RETURN_QUOTE_ID"] = "ID пропозиції";
