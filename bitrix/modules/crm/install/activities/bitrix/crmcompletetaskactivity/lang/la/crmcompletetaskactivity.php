<?php
$MESS["CRM_CTA_COMPLETED_BY"] = "Finalizar la tarea como usuario";
$MESS["CRM_CTA_COMPLETED_TASKS"] = "Tareas completadas";
$MESS["CRM_CTA_COMPLETE_TASK"] = "Finalizar las tareas que se crearon durante la etapa";
$MESS["CRM_CTA_INCORRECT_STAGE"] = "Estado o etapa de finalización incorrecta";
