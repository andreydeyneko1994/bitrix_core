<?php
$MESS["CRM_CTA_COMPLETED_BY"] = "Finalizar tarefa como usuário";
$MESS["CRM_CTA_COMPLETED_TASKS"] = "Tarefas concluídas";
$MESS["CRM_CTA_COMPLETE_TASK"] = "Finalizar tarefas criadas enquanto na etapa";
$MESS["CRM_CTA_INCORRECT_STAGE"] = "Etapa ou status de conclusão incorreto";
