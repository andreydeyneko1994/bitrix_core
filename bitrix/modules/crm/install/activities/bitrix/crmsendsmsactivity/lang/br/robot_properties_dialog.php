<?
$MESS["CRM_SSMSA_RPD_CHOOSE_PROVIDER"] = "selecionar o provedor de SMS...";
$MESS["CRM_SSMSA_RPD_MARKETPLACE"] = "Selecionar outro provedor";
$MESS["CRM_SSMSA_RPD_PROVIDER_CANT_USE"] = "O provedor não está disponível porque não foi configurado";
$MESS["CRM_SSMSA_RPD_PROVIDER_IS_DEMO"] = "O provedor está em modo de demonstração";
$MESS["CRM_SSMSA_RPD_PROVIDER_MANAGE_URL"] = "Configurar o Provedor";
$MESS["CRM_SSMSA_SMS_SYMBOLS"] = "Caracteres";
$MESS["CRM_SSMSA_SMS_SYMBOLS_FROM"] = "de";
?>