<?
$MESS["CRM_SSMSA_RPD_CHOOSE_PROVIDER"] = "wybierz dostawcę usługi SMS...";
$MESS["CRM_SSMSA_RPD_MARKETPLACE"] = "Wybierz innego dostawcę usługi";
$MESS["CRM_SSMSA_RPD_PROVIDER_CANT_USE"] = "Usługodawca jest niedostępny, ponieważ nie został skonfigurowany";
$MESS["CRM_SSMSA_RPD_PROVIDER_IS_DEMO"] = "Usługodawca jest w trybie demo";
$MESS["CRM_SSMSA_RPD_PROVIDER_MANAGE_URL"] = "Konfiguruj usługodawcę";
$MESS["CRM_SSMSA_SMS_SYMBOLS"] = "Znaki";
$MESS["CRM_SSMSA_SMS_SYMBOLS_FROM"] = "z";
?>