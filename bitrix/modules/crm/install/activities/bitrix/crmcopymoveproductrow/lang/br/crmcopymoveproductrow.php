<?php
$MESS["CRM_CMPR_COPIED_ROWS"] = "Produtos copiados";
$MESS["CRM_CMPR_COPY_PRODUCTS_ERROR"] = "Não é possível copiar itens de produto";
$MESS["CRM_CMPR_DST_ENTITY_ID"] = "ID do destino";
$MESS["CRM_CMPR_DST_ENTITY_TYPE"] = "Destino";
$MESS["CRM_CMPR_MOVED_ROWS"] = "Produtos movidos";
$MESS["CRM_CMPR_MOVE_PRODUCTS_ERROR"] = "Não é possível mover itens de produto";
$MESS["CRM_CMPR_NO_DST_ENTITY"] = "Não é possível encontrar entidade de destino para copiar ou mover itens de produto";
$MESS["CRM_CMPR_NO_SOURCE_PRODUCTS"] = "Não é possível obter itens de produto da entidade de origem";
$MESS["CRM_CMPR_OPERATION"] = "Ação";
$MESS["CRM_CMPR_OPERATION_CP"] = "Copiar";
$MESS["CRM_CMPR_OPERATION_MV"] = "Mover";
$MESS["CRM_CMPR_SAME_ENTITY_ERROR"] = "As entidades atuais e de destino não podem ser iguais";
