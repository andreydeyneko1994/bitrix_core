<?php
$MESS["CRM_CMPR_COPIED_ROWS"] = "Skopiowano produkty";
$MESS["CRM_CMPR_COPY_PRODUCTS_ERROR"] = "Nie można skopiować pozycji produktów";
$MESS["CRM_CMPR_DST_ENTITY_ID"] = "ID celu";
$MESS["CRM_CMPR_DST_ENTITY_TYPE"] = "Cel";
$MESS["CRM_CMPR_MOVED_ROWS"] = "Przeniesiono produkty";
$MESS["CRM_CMPR_MOVE_PRODUCTS_ERROR"] = "Nie można przenieść pozycji produktów";
$MESS["CRM_CMPR_NO_DST_ENTITY"] = "Nie można znaleźć jednostki docelowej, do której mają zostać skopiowane lub przeniesione pozycje produktów";
$MESS["CRM_CMPR_NO_SOURCE_PRODUCTS"] = "Nie można pobrać pozycji produktów z jednostki źródłowej";
$MESS["CRM_CMPR_OPERATION"] = "Działanie";
$MESS["CRM_CMPR_OPERATION_CP"] = "Kopiuj";
$MESS["CRM_CMPR_OPERATION_MV"] = "Przenieś";
$MESS["CRM_CMPR_SAME_ENTITY_ERROR"] = "Jednostka bieżąca nie może być jednostką docelową";
