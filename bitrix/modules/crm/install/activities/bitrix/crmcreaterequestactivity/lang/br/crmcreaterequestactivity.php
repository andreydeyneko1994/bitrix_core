<?
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE_ON_ENTITY_ST_CHG"] = "Concluir automaticamente a atividade quando o status for atualizado";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Descrição";
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "O parâmetro obrigatório está vazio: #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Pessoa responsável";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Assunto";
?>