<?
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE_ON_ENTITY_ST_CHG"] = "Terminer automatiquement l'activité lorsque le statut est actualisé";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Description";
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "Le paramètre requis est vide : #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Important";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Objet";
?>