<?
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Automatycznie uruchamiane po ukończeniu workflowu";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Opis";
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "Wymagany parametr jest pusty: #PROPERTY#";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Data końcowa";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Ważne";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Okres przypominania";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Przypomnij za";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Data początkowa";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Temat";
?>