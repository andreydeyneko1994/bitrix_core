<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "No se especifica el estado de destino";
$MESS["CRM_CHANGE_STATUS_INCORRECT_STAGE"] = "Etapa o estado de destino incorrecto";
$MESS["CRM_CHANGE_STATUS_MODIFIED_BY"] = "Cambiar en nombre de";
$MESS["CRM_CHANGE_STATUS_RECURSION"] = "Error de ejecución: posible recursión infinita en el cambio de estado";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Nueva fase";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Nuevo estado";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Completado porque el estado ha cambiado";
?>