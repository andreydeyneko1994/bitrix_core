<?php
$MESS["CRM_CHANGE_DEAL_STAGE_NAME"] = "Changer d'étape";
$MESS["CRM_CHANGE_STATUS_DESC"] = "Modifier le statut du document et terminer le flux de travail.";
$MESS["CRM_CHANGE_STATUS_NAME"] = "Modifier le statut";
