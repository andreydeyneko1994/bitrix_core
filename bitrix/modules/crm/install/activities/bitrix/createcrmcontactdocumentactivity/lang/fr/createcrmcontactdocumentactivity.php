<?
$MESS["BPCDA_FIELD_NOT_FOUND"] = "La propriété '#NAME#' n'est pas spécifiée.";
$MESS["BPCDA_FIELD_REQUIED"] = "Le champ '#FIELD#' doit être rempli.";
$MESS["BPCDA_MODULE_NOT_LOADED"] = "Échec de chargement du module 'CRM'.";
$MESS["BPCDA_WRONG_TYPE"] = "Le type du paramètre '#PARAM#' n'est pas défini.";
?>