<?php
$MESS["CRM_CREATE_ADS_ACCOUNT_ID"] = "Seleccionar cuenta de publicidad";
$MESS["CRM_CREATE_ADS_AUDIENCE_ID"] = "Agregar a audiencia";
$MESS["CRM_CREATE_ADS_CLIENT_ID"] = "Configurar Público Objetivo";
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "Falta el campo requerido";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Este valor debe ser un número";
