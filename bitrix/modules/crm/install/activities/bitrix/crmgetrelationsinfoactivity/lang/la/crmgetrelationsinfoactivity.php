<?php
$MESS["CRM_GRI_BINDING"] = "Vinculado a";
$MESS["CRM_GRI_ENTITY_EXISTENCE_ERROR"] = "No hay entidades #ELEMENT_TYPE#  vinculadas a este elemento";
$MESS["CRM_GRI_PARENT_TYPE_ERROR"] = "Se seleccionó un tipo de entidad incorrecto";
$MESS["CRM_GRI_PARENT_TYPE_EXISTENCE_ERROR"] = "El tipo que seleccionó no tiene visualización en los flujos de trabajo";
$MESS["CRM_GRI_RELATION_EXISTENCE_ERROR"] = "El enlace que seleccionó no existe";
