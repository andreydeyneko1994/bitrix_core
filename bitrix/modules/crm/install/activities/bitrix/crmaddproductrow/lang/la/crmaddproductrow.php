<?php
$MESS["CRM_APR_ADD_ERROR"] = "No se puede agregar el producto";
$MESS["CRM_APR_DOCUMENT_ERROR"] = "La acción no está disponible para la entidad actual";
$MESS["CRM_APR_GET_PRODUCT_ERROR"] = "No se puede obtener la información del producto";
$MESS["CRM_APR_PRODUCT_ID"] = "ID del producto";
$MESS["CRM_APR_ROW_DISCOUNT_RATE"] = "Descuento, %";
$MESS["CRM_APR_ROW_PRICE_ACCOUNT"] = "Precio";
$MESS["CRM_APR_ROW_QUANTITY"] = "Cantidad";
