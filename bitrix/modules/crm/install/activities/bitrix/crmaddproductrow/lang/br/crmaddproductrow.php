<?php
$MESS["CRM_APR_ADD_ERROR"] = "Não é possível adicionar produto";
$MESS["CRM_APR_DOCUMENT_ERROR"] = "A ação não está disponível para a entidade atual";
$MESS["CRM_APR_GET_PRODUCT_ERROR"] = "Não é possível obter informações do produto";
$MESS["CRM_APR_PRODUCT_ID"] = "ID do produto";
$MESS["CRM_APR_ROW_DISCOUNT_RATE"] = "Desconto, %";
$MESS["CRM_APR_ROW_PRICE_ACCOUNT"] = "Preço";
$MESS["CRM_APR_ROW_QUANTITY"] = "Quantidade";
