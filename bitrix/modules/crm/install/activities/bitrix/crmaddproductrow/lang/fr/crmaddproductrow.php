<?php
$MESS["CRM_APR_ADD_ERROR"] = "Impossible d'ajouter un produit";
$MESS["CRM_APR_DOCUMENT_ERROR"] = "L'action est indisponible pour l'entité actuelle";
$MESS["CRM_APR_GET_PRODUCT_ERROR"] = "Échec de la récupération des informations du produit";
$MESS["CRM_APR_PRODUCT_ID"] = "ID du produit";
$MESS["CRM_APR_ROW_DISCOUNT_RATE"] = "Réduction, %";
$MESS["CRM_APR_ROW_PRICE_ACCOUNT"] = "Prix";
$MESS["CRM_APR_ROW_QUANTITY"] = "Quantité";
