<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Etap początkowy";
$MESS["CRM_CDA_CYCLING_ERROR"] = "Nie można skopiować Inteligentnej automatyzacji procesów ponieważ istnieje możliwość wystąpienia w działaniach nieskończonej pętli";
$MESS["CRM_CDA_EMPTY_PROP"] = "Wymagany parametr jest pusty: #PROPERTY#";
$MESS["CRM_CDA_ITEM_TITLE"] = "Nazwa SPA";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Przenieś do pipelina";
$MESS["CRM_CDA_NEW_ITEM_TITLE"] = "#SOURCE_TITLE# (kopia)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Nie można uzyskać danych źródłowego SPA";
$MESS["CRM_CDA_STAGE_EXISTENCE_ERROR"] = "Wybrany etap nie istnieje";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "Wybrany etap początkowy należy do innego pipelina";
