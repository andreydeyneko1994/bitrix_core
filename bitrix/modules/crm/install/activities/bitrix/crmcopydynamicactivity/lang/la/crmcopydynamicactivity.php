<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Etapa inicial";
$MESS["CRM_CDA_CYCLING_ERROR"] = "No se puede copiar la Automatización Inteligente de Procesos porque hay un posible bucle infinito en las acciones";
$MESS["CRM_CDA_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_CDA_ITEM_TITLE"] = "Nombre del SPA";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Mover al canal";
$MESS["CRM_CDA_NEW_ITEM_TITLE"] = "#SOURCE_TITLE# (copiar)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "No se pueden obtener los datos del SPA de origen";
$MESS["CRM_CDA_STAGE_EXISTENCE_ERROR"] = "La etapa seleccionada no existe";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "La etapa inicial seleccionada pertenece a algún otro canal";
