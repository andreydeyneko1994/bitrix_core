<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Responsable";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Étape initiale";
$MESS["CRM_CDA_CYCLING_ERROR"] = "Impossible de copier la Smart Process Automation car une possible boucle infinie se trouve dans la ou les actions";
$MESS["CRM_CDA_EMPTY_PROP"] = "Le paramètre requis est vide : #PROPERTY#";
$MESS["CRM_CDA_ITEM_TITLE"] = "Nom de la SPA";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Déplacer dans le pipeline";
$MESS["CRM_CDA_NEW_ITEM_TITLE"] = "#SOURCE_TITLE# (Copier)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Impossible de récupérer les données de la SPA source";
$MESS["CRM_CDA_STAGE_EXISTENCE_ERROR"] = "L'étape sélectionnée n'existe pas";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "L'étape initiale sélectionnée appartient à un autre pipeline";
