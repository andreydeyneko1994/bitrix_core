<?php
$MESS["CRM_DDA_DELETE_ERROR"] = "Błąd podczas usuwania pozycji";
$MESS["CRM_DDA_DYNAMIC_ID"] = "ID SPA";
$MESS["CRM_DDA_DYNAMIC_TYPE"] = "Typ SPA";
$MESS["CRM_DDA_ELEMENT_ID"] = "Identyfikator pozycji CRM";
$MESS["CRM_DDA_ELEMENT_TYPE"] = "Typ pozycji CRM";
$MESS["CRM_DDA_ENTITY_ERROR"] = "Jednostka o tym ID nie istnieje";
$MESS["CRM_DDA_TYPE_ID_ERROR"] = "Nieprawidłowy typ obiektu";
