<?php
$MESS["CRM_DDA_DELETE_ERROR"] = "Erro ao excluir item";
$MESS["CRM_DDA_DYNAMIC_ID"] = "ID da SPA";
$MESS["CRM_DDA_DYNAMIC_TYPE"] = "Tipo de SPA";
$MESS["CRM_DDA_ELEMENT_ID"] = "ID do item de CRM";
$MESS["CRM_DDA_ELEMENT_TYPE"] = "Tipo de item de CRM";
$MESS["CRM_DDA_ENTITY_ERROR"] = "Não existe entidade com este ID";
$MESS["CRM_DDA_TYPE_ID_ERROR"] = "Tipo de entidade inválido";
