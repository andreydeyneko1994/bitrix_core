<?php
$MESS["CRM_CDCA_CATEGORY"] = "Pipeline";
$MESS["CRM_CDCA_CYCLING_ERROR"] = "Não é possível mover o negócio devido a um possível loop infinito.";
$MESS["CRM_CDCA_CYCLING_EXCEPTION_MESSAGE"] = "Regra de automação não iniciada";
$MESS["CRM_CDCA_EMPTY_CATEGORY"] = "O pipeline não está especificado";
$MESS["CRM_CDCA_MOVE_ERROR"] = "Não é possível alterar o pipeline (Código de erro: #ERROR_CODE#)";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_CHANGED"] = "O negócio já pertence a este pipeline";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_FOUND"] = "O pipeline não foi encontrado";
$MESS["CRM_CDCA_MOVE_ERROR_RESPONSIBLE_NOT_FOUND"] = "Nenhuma pessoa responsável especificada para o negócio";
$MESS["CRM_CDCA_MOVE_ERROR_STAGE_NOT_FOUND"] = "Não é possível determinar o estágio do novo negócio";
$MESS["CRM_CDCA_MOVE_TERMINATION_TITLE"] = "Concluído automaticamente quando o pipeline mudou";
$MESS["CRM_CDCA_NO_SOURCE_FIELDS"] = "Não é possível obter dados do negócio de fonte";
$MESS["CRM_CDCA_STAGE"] = "Etapa";
