<?php
$MESS["CRM_CDCA_CATEGORY"] = "Pipeline";
$MESS["CRM_CDCA_CYCLING_ERROR"] = "No se puede mover la negociación debido a un posible bucle infinito.";
$MESS["CRM_CDCA_CYCLING_EXCEPTION_MESSAGE"] = "Regla de automatización no ejecutada";
$MESS["CRM_CDCA_EMPTY_CATEGORY"] = "No se especifica el pipeline";
$MESS["CRM_CDCA_MOVE_ERROR"] = "No se puede cambiar el pipeline (Error code: #ERROR_CODE#)";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_CHANGED"] = "Le negociación ya pertenece a este pipeline";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_FOUND"] = "No se encontró el pipeline";
$MESS["CRM_CDCA_MOVE_ERROR_RESPONSIBLE_NOT_FOUND"] = "No hay persona responsable especificado en esta negociación";
$MESS["CRM_CDCA_MOVE_ERROR_STAGE_NOT_FOUND"] = "No se puede resolver la nueva etapa de la negociación";
$MESS["CRM_CDCA_MOVE_TERMINATION_TITLE"] = "Auto completado cuando el pipeline cambió";
$MESS["CRM_CDCA_NO_SOURCE_FIELDS"] = "No se pueden obtener datos de la negociación de origen";
$MESS["CRM_CDCA_STAGE"] = "Etapa";
