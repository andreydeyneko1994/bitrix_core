<?php
$MESS["CRM_CDCA_CATEGORY"] = "Pipeline";
$MESS["CRM_CDCA_CYCLING_ERROR"] = "Impossible de déplacer la transaction en raison d'une possible boucle infinie.";
$MESS["CRM_CDCA_CYCLING_EXCEPTION_MESSAGE"] = "La règle d'automatisation n'est pas lancée";
$MESS["CRM_CDCA_EMPTY_CATEGORY"] = "Le pipeline n'est pas spécifié";
$MESS["CRM_CDCA_MOVE_ERROR"] = "Impossible de modifier le pipeline (code d'erreur : #ERROR_CODE#)";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_CHANGED"] = "La transaction appartient déjà à ce pipeline";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_FOUND"] = "Pipeline introuvable";
$MESS["CRM_CDCA_MOVE_ERROR_RESPONSIBLE_NOT_FOUND"] = "Aucun responsable désigné pour cette transaction";
$MESS["CRM_CDCA_MOVE_ERROR_STAGE_NOT_FOUND"] = "Impossible de résoudre l'étape de la nouvelle transaction";
$MESS["CRM_CDCA_MOVE_TERMINATION_TITLE"] = "Terminé automatiquement quand le pipeline est modifié";
$MESS["CRM_CDCA_NO_SOURCE_FIELDS"] = "Impossible de récupérer les données de la transaction source";
$MESS["CRM_CDCA_STAGE"] = "Étape";
