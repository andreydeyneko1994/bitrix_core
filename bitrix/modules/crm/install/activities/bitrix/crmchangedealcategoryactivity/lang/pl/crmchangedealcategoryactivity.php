<?php
$MESS["CRM_CDCA_CATEGORY"] = "Lejek";
$MESS["CRM_CDCA_CYCLING_ERROR"] = "Nie można przenieść dealu z powodu możliwej nieskończonej pętli.";
$MESS["CRM_CDCA_CYCLING_EXCEPTION_MESSAGE"] = "Nie uruchomiono reguły automatyzacji";
$MESS["CRM_CDCA_EMPTY_CATEGORY"] = "Lejek nie został określony";
$MESS["CRM_CDCA_MOVE_ERROR"] = "Nie można zmienić lejka (kod błędu: #ERROR_CODE#)";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_CHANGED"] = "Deal już należy do tego lejka";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_FOUND"] = "Nie znaleziono lejka";
$MESS["CRM_CDCA_MOVE_ERROR_RESPONSIBLE_NOT_FOUND"] = "Nie określono osoby odpowiedzialnej za deal";
$MESS["CRM_CDCA_MOVE_ERROR_STAGE_NOT_FOUND"] = "Nie można ustalić nowego etapu deala";
$MESS["CRM_CDCA_MOVE_TERMINATION_TITLE"] = "Automatyczne uzupełnianie po zmianie lejka";
$MESS["CRM_CDCA_NO_SOURCE_FIELDS"] = "Nie można uzyskać danych o umowie źródłowej";
$MESS["CRM_CDCA_STAGE"] = "Etap";
