<?php
$MESS["CRM_SOF_ACTION_ADD_OBSERVERS"] = "adicionar";
$MESS["CRM_SOF_ACTION_ON_OBSERVERS"] = "Aplicar ação aos observadores";
$MESS["CRM_SOF_ACTION_REMOVE_OBSERVERS"] = "excluir";
$MESS["CRM_SOF_ACTION_REPLACE_OBSERVERS"] = "mudar";
$MESS["CRM_SOF_EMPTY_PROP"] = "O parâmetro obrigatório está vazio: #PROPERTY#";
$MESS["CRM_SOF_OBSERVERS"] = "Observadores";
