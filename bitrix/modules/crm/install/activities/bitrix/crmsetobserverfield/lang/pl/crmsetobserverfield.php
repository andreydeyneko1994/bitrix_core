<?php
$MESS["CRM_SOF_ACTION_ADD_OBSERVERS"] = "dodaj";
$MESS["CRM_SOF_ACTION_ON_OBSERVERS"] = "Zastosuj działanie do obserwatorów";
$MESS["CRM_SOF_ACTION_REMOVE_OBSERVERS"] = "usuń";
$MESS["CRM_SOF_ACTION_REPLACE_OBSERVERS"] = "zmień";
$MESS["CRM_SOF_EMPTY_PROP"] = "Wymagany parametr jest pusty: #PROPERTY#";
$MESS["CRM_SOF_OBSERVERS"] = "Obserwatorzy";
