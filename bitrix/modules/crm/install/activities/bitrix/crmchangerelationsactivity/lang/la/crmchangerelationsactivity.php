<?php
$MESS["CRM_CRA_ACTION_ADD"] = "Agregar";
$MESS["CRM_CRA_ACTION_REMOVE"] = "Eliminar";
$MESS["CRM_CRA_ACTION_REPLACE"] = "Reemplazar";
$MESS["CRM_CRA_ACTION_TYPE"] = "Tipo de acción";
$MESS["CRM_CRA_ELEMENT_EXISTENCE_ERROR"] = "El elemento que seleccionó no existe";
$MESS["CRM_CRA_ELEMENT_NOT_CHOSEN_ERROR"] = "Elemento vinculado no seleccionado";
$MESS["CRM_CRA_PARENT_ID"] = "ID del elemento";
$MESS["CRM_CRA_PARENT_TYPE"] = "Vinculado a";
