<?php
$MESS["CRM_CRA_ACTION_ADD"] = "Dodaj";
$MESS["CRM_CRA_ACTION_REMOVE"] = "Usuń";
$MESS["CRM_CRA_ACTION_REPLACE"] = "Zastąp";
$MESS["CRM_CRA_ACTION_TYPE"] = "Typ działania";
$MESS["CRM_CRA_ELEMENT_EXISTENCE_ERROR"] = "Wybrany element nie istnieje";
$MESS["CRM_CRA_ELEMENT_NOT_CHOSEN_ERROR"] = "Nie wybrano powiązanego elementu";
$MESS["CRM_CRA_PARENT_ID"] = "ID elementu";
$MESS["CRM_CRA_PARENT_TYPE"] = "Powiązane z";
