<?php
$MESS["CRM_UDA_ADD_CONDITION"] = "Agregar condición";
$MESS["CRM_UDA_DELETE_CONDITION"] = "Eliminar";
$MESS["CRM_UDA_DYNAMIC_TYPE"] = "Tipo de elemento SPA";
$MESS["CRM_UDA_ENTITY_EXISTENCE_ERROR"] = "El ID #ENTITY_ID# de la entidad #TYPE_NAME# no existe";
$MESS["CRM_UDA_ENTITY_ID_ERROR"] = "El ID de la entidad debe ser mayor que cero";
$MESS["CRM_UDA_ENTITY_TYPE_ERROR"] = "Se seleccionó un tipo de entidad incorrecto";
