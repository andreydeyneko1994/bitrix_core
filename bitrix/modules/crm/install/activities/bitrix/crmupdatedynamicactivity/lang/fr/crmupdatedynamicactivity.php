<?php
$MESS["CRM_UDA_ADD_CONDITION"] = "Ajouter une condition";
$MESS["CRM_UDA_DELETE_CONDITION"] = "Supprimer";
$MESS["CRM_UDA_DYNAMIC_TYPE"] = "Type d'article SPA";
$MESS["CRM_UDA_ENTITY_EXISTENCE_ERROR"] = "L'ID #ENTITY_ID# d'entité #TYPE_NAME# n'existe pas";
$MESS["CRM_UDA_ENTITY_ID_ERROR"] = "L'ID de l'entité doit être supérieur à zéro";
$MESS["CRM_UDA_ENTITY_TYPE_ERROR"] = "Le type d'entité sélectionné est incorrect";
