<?php
$MESS["CRM_UDA_ADD_CONDITION"] = "Dodaj warunek";
$MESS["CRM_UDA_DELETE_CONDITION"] = "Usuń";
$MESS["CRM_UDA_DYNAMIC_TYPE"] = "Typ pozycji SPA";
$MESS["CRM_UDA_ENTITY_EXISTENCE_ERROR"] = "Jednostka #TYPE_NAME# o ID #ENTITY_ID# nie istnieje";
$MESS["CRM_UDA_ENTITY_ID_ERROR"] = "ID jednostki musi mieć wartość większą niż zero";
$MESS["CRM_UDA_ENTITY_TYPE_ERROR"] = "Wybrano nieprawidłowy typ jednostki";
