<?php
$MESS["CRM_CDA_DYNAMIC_TYPE_ID"] = "Tipo de SPA";
$MESS["CRM_CDA_DYNAMIC_TYPE_ID_ERROR"] = "Se seleccionó un tipo de entidad incorrecto";
$MESS["CRM_CDA_ITEM_CREATION_ERROR"] = "Error al crear el elemento del CRM";
$MESS["CRM_CDA_TYPE_ID"] = "Tipo de elemento del CRM";
