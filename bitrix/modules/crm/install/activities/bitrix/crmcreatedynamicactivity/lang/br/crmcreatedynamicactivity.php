<?php
$MESS["CRM_CDA_DYNAMIC_TYPE_ID"] = "Tipo de SPA";
$MESS["CRM_CDA_DYNAMIC_TYPE_ID_ERROR"] = "Tipo de entidade incorreto selecionado";
$MESS["CRM_CDA_ITEM_CREATION_ERROR"] = "Erro ao criar item de CRM";
$MESS["CRM_CDA_TYPE_ID"] = "Tipo de item de CRM";
