<?php

$MESS['CRMBPGQR_DESCR_DESCR'] = "Позволяет создать QR-код на специальную страницу";
$MESS['CRMBPGQR_DESCR_NAME'] = "Создать QR-код";
$MESS['CRMBPGQR_RETURN_PAGE_LINK'] = "Короткая ссылка на страницу";
$MESS['CRMBPGQR_RETURN_PAGE_LINK_BB'] = "Ссылка на страницу (BBcode)";
$MESS['CRMBPGQR_RETURN_PAGE_LINK_HTML'] = "Ссылка на страницу для вставки в письмо (HTML)";
$MESS['CRMBPGQR_RETURN_QR_LINK'] = "Короткая ссылка на QR-код";
$MESS['CRMBPGQR_RETURN_QR_LINK_BB'] = "Ссылка на QR-код (BBcode)";
$MESS['CRMBPGQR_RETURN_QR_LINK_HTML'] = "Ссылка на QR-код для вставки в письмо (HTML)";
$MESS['CRMBPGQR_RETURN_QR_IMG'] = "Вставка QR-кода (HTML картинка)";
