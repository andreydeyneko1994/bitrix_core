<?php
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "Falta el campo requerido";
$MESS["CRM_CREATE_ADS_FB_RESTRICTED"] = "La regla de automatización no está disponible en su área.";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Este valor debe ser un número";
