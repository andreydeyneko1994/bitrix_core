<?php
$MESS["CRM_RMPR_DELETED_IDS"] = "Produtos excluídos";
$MESS["CRM_RMPR_DESCRIPTION"] = "Exclui todos os produtos nesta entidade";
$MESS["CRM_RMPR_REMOVE_PRODUCTS_ERROR"] = "Não é possível excluir produtos";
