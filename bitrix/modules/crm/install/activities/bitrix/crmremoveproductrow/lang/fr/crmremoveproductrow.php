<?php
$MESS["CRM_RMPR_DELETED_IDS"] = "Produits supprimés";
$MESS["CRM_RMPR_DESCRIPTION"] = "Supprime tous les produits faisant partie de l'entité";
$MESS["CRM_RMPR_REMOVE_PRODUCTS_ERROR"] = "Impossible de supprimer les produits";
