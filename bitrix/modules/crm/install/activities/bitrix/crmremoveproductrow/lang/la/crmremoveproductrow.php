<?php
$MESS["CRM_RMPR_DELETED_IDS"] = "Productos eliminados";
$MESS["CRM_RMPR_DESCRIPTION"] = "Elimina todos los productos de esta entidad";
$MESS["CRM_RMPR_REMOVE_PRODUCTS_ERROR"] = "No se pueden eliminar los productos";
