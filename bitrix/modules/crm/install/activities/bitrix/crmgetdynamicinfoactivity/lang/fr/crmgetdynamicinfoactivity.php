<?php
$MESS["CRM_GDIA_DYNAMIC_TYPE_ID"] = "Type d'élément SPA";
$MESS["CRM_GDIA_ENTITY_EXISTENCE_ERROR"] = "Les filtres que vous avez sélectionnés n'ont produit aucun résultat de recherche";
$MESS["CRM_GDIA_ENTITY_TYPE_ERROR"] = "Le type d'entité sélectionné est incorrect";
$MESS["CRM_GDIA_FILTERING_FIELDS_PROPERTY"] = "Filtre de champ";
$MESS["CRM_GDIA_RETURN_FIELDS_SELECTION"] = "Sélectionner les champs";
