<?
$MESS["CRM_SOCCL_COMMENT_NAME"] = "Powód anulowania";
$MESS["CRM_SOCCL_ORDER_ERROR"] = "Bieżąca jednostka nie jest jednostką typu „Zamówienie”";
$MESS["CRM_SOCCL_ORDER_IS_CANCELED"] = "Zamówienie zostało już anulowane";
$MESS["CRM_SOCCL_ORDER_NOT_FOUND"] = "Nie można uzyskać danych zamówienia";
$MESS["CRM_SOCCL_STATUS_ERROR"] = "Nie określono statusu anulowania";
$MESS["CRM_SOCCL_STATUS_NAME"] = "Status anulowania";
$MESS["CRM_SOCCL_TERMINATE"] = "Ukończono z powodu zmiany statusu";
?>