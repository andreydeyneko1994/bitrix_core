<?
$MESS["CRM_SOCCL_COMMENT_NAME"] = "Motif d'annulation";
$MESS["CRM_SOCCL_ORDER_ERROR"] = "L'entité actuelle n'est pas une entité de type \"Commande\"";
$MESS["CRM_SOCCL_ORDER_IS_CANCELED"] = "La commande a été annulée";
$MESS["CRM_SOCCL_ORDER_NOT_FOUND"] = "Récupération des données de la commande impossible";
$MESS["CRM_SOCCL_STATUS_ERROR"] = "Statut de l'annulation non spécifié";
$MESS["CRM_SOCCL_STATUS_NAME"] = "Statut de l'annulation";
$MESS["CRM_SOCCL_TERMINATE"] = "Terminée parce que le statut a été modifié";
?>