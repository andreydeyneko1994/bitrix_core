<?
$MESS["CRM_SOCCL_COMMENT_NAME"] = "Motivo do cancelamento";
$MESS["CRM_SOCCL_ORDER_ERROR"] = "A entidade atual não é entidade do tipo \"Pedido\"";
$MESS["CRM_SOCCL_ORDER_IS_CANCELED"] = "O pedido já foi cancelado";
$MESS["CRM_SOCCL_ORDER_NOT_FOUND"] = "Não é possível obter dados do pedido";
$MESS["CRM_SOCCL_STATUS_ERROR"] = "Status de cancelamento não especificado";
$MESS["CRM_SOCCL_STATUS_NAME"] = "Status de cancelamento";
$MESS["CRM_SOCCL_TERMINATE"] = "Concluído porque o status foi alterado";
?>