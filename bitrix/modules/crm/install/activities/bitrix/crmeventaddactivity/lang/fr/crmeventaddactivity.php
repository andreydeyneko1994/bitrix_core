<?
$MESS["BPEAA_EMPTY_MESSAGE"] = "Propriété 'Texte du message' n'est pas indiqué.";
$MESS["BPEAA_EMPTY_TYPE"] = "Le paramètre 'Type d'évènement' n'est pas spécifié.";
$MESS["BPEAA_EVENT_TEXT"] = "Texte du message";
$MESS["BPEAA_EVENT_TYPE"] = "Type d'événement";
$MESS["BPEAA_EVENT_USER"] = "Créé par";
?>