<?
$MESS["CRM_CRLC_CONTACT_NAME_DEFAULT"] = "Sin título";
$MESS["CRM_CRLC_LEAD_HAS_CONTACT"] = "El prospecto ya pertenece a un contacto";
$MESS["CRM_CRLC_LEAD_NOT_EXISTS"] = "No se pueden obtener datos del prospecto";
$MESS["CRM_CRLC_LEAD_WRONG_STATUS"] = "No se puede crear un contacto desde el estado de éxito de un prospecto. Utilice la regla de automatización de la conversión.";
$MESS["CRM_CRLC_RESPONSIBLE"] = "Persona responsable";
?>