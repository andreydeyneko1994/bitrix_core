<?php
$MESS["BPCTLCA_COMMENT_TEXT"] = "Komentarz";
$MESS["BPCTLCA_COMMENT_USER"] = "Utworzony przez";
$MESS["BPCTLCA_CREATION_ERROR"] = "Nie można dodać komentarza";
$MESS["BPCTLCA_EMPTY_COMMENT_TEXT"] = "Komentarz „brakuje właściwości”.";
$MESS["BPCTLCA_NO_COMMENT"] = "Nie określono tekstu komentarza.";
