<?php
$MESS["BPCTLCA_COMMENT_TEXT"] = "Commentaire";
$MESS["BPCTLCA_COMMENT_USER"] = "Créé par";
$MESS["BPCTLCA_CREATION_ERROR"] = "Ajout de commentaire impossible";
$MESS["BPCTLCA_EMPTY_COMMENT_TEXT"] = "Commentaire \"la propriété est manquante\".";
$MESS["BPCTLCA_NO_COMMENT"] = "Le texte du commentaire n'a pas été spécifié.";
