<?
$MESS["CRM_SSS_ORDER_ERROR"] = "A entidade atual não é entidade do tipo \"Pedido\"";
$MESS["CRM_SSS_ORDER_NOT_FOUND"] = "Não é possível obter dados do pedido";
$MESS["CRM_SSS_TARGET_STATUS_EMPTY"] = "O status final não está especificado";
$MESS["CRM_SSS_TARGET_STATUS_NAME"] = "Novo status";
?>