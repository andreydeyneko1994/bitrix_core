<?
$MESS["CRM_ML_SCORING_BUTTON_TITLE"] = "Ocenianie AI24";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_DEAL"] = "Twój CRM ma ponad 2000 deali!<br> Teraz mamy wystarczająco danych, aby wygrenować model prognozy. Kliknij \"AI Ocenianie\", aby wyświetlić prawdopodobieństwo dotyczące aktualnych deali.";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_LEAD"] = "Twój CRM ma ponad 2000 leadów!<br> Teraz mamy wystarczająco danych, aby wytrenować model prognozy. Kliknij „Prognoza AI”, aby wyświetlić prawdopodobieństwo dotyczące aktualnych leadów.";
?>