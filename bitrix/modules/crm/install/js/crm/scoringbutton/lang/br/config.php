<?
$MESS["CRM_ML_SCORING_BUTTON_TITLE"] = "Pontuação AI24";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_DEAL"] = "Seu CRM agora tem mais de 2000 negócios!<br> Agora, temos dados suficientes para treinar o modelo de previsão. Clique em \"Previsão IA\" para visualizar a probabilidade dos negócios atuais.";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_LEAD"] = "Seu CRM agora tem mais de 2000 leads!<br> Agora, temos dados suficientes para treinar o modelo de previsão. Clique em \"Previsão IA\" para visualizar a probabilidade dos leads atuais.";
?>