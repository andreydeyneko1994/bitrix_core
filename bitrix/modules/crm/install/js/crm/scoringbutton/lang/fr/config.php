<?
$MESS["CRM_ML_SCORING_BUTTON_TITLE"] = "AI24 Scoring";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_DEAL"] = "Votre CRM dépasse à présent les 2000 transactions !<br> Nous avons maintenant suffisamment de données pour configurer le modèle de scoring. Cliquez sur « IA Prévisions » pour afficher les probabilités actuelles des transactions.";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_LEAD"] = "Votre CRM dépasse à présent les 2000 prospects !<br> Nous avons maintenant suffisamment de données pour configurer le modèle de scoring. Cliquez sur « IA Prévisions » pour afficher les probabilités actuelles des prospects.";
?>