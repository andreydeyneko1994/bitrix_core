<?php

$MESS['CRM_TIMELINE_ITEM_LINK_IS_COPIED'] = "Ссылка скопирована в буфер обмена";
$MESS['CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR'] = "Произошла неизвестная ошибка при создании публичной ссылки на документ";
$MESS['CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR'] = "Не удалось скопировать ссылку в буфер обмена. Попробуйте еще раз, пожалуйста";
$MESS['CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY'] = "Печать документа невозможна, пока не будет сформирован PDF файл. Пожалуйста, повторите попытку позднее";
$MESS['CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY'] = "PDF файл пока не сформирован. Пожалуйста, повторите попытку позднее или скачайте DOCX файл прямо сейчас.";
$MESS['CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR'] = "Произошла неизвестная ошибка при редактировании документа";
$MESS['CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS'] = "Отправлено";
