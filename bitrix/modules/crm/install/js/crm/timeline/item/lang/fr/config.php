<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "Impossible de copier le lien dans le Presse-papiers. Veuillez réessayer.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Erreur lors de la création du lien public vers le document";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "Le fichier PDF est en cours de création. Veuillez réessayer plus tard. Vous pouvez télécharger le fichier DOCX correspondant dès maintenant.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "Impossible d'imprimer le document car le fichier PDF est en cours de création. Veuillez réessayer plus tard.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Erreur lors de la mise à jour du document";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "Annuler";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "Choses à faire";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "Enregistrer";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Lien copié dans le Presse-papiers";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Envoyé";
