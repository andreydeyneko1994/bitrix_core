<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "Não é possível copiar o link para a área de transferência. Tente novamente.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Erro ao criar link público do documento";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "O arquivo PDF ainda está sendo criado. Tente novamente mais tarde. Você pode baixar o respectivo arquivo DOCX agora mesmo.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "Não é possível imprimir o documento porque o arquivo PDF ainda está sendo criado. Tente novamente mais tarde.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Erro ao atualizar o documento";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "Cancelar";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "Coisas a fazer";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "Salvar";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Link copiado para a Área de Transferência";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Enviado";
