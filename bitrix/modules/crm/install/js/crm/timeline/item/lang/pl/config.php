<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "Nie można skopiować linku do schowka. Spróbuj ponownie.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Błąd podczas tworzenia publicznego linku do dokumentu";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "Plik PDF jest nadal tworzony. Spróbuj ponownie później. Teraz możesz pobrać odpowiadający plik DOCX.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "Nie można wydrukować dokumentu, ponieważ plik PDF jest nadal tworzony. Spróbuj ponownie później.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Błąd podczas aktualizowania dokumentu";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "Anuluj";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "Rzeczy do zrobienia";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "Zapisz";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Link skopiowano do schowka";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Wysłano";
