<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "Не вдалося скопіювати посилання в буфер обміну. Спробуйте ще раз.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Помилка під час створення публічного посилання на документ";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "PDF-файл поки не сформований. Повторіть спробу пізніше або завантажте файл DOCX саме зараз.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "Друк документа неможливий, доки не буде сформований файл PDF. Повторіть спробу пізніше.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Помилка оновлення документа";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "Скасувати";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "Що потрібно зробити";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "Зберегти";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Посилання скопійованo в буфер";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Відправлено";
