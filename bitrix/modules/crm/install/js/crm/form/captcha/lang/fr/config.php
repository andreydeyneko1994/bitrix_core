<?php
$MESS["CRM_FORM_CAPTCHA_JS_ACCESS_DENIED"] = "Accès refusé. Veuillez contacter votre administrateur Bitrix24.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_HOWTO"] = "Comment obtenir les clés ?";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TEXT"] = "Vous pouvez choisir d'utiliser vos propres clés de protection contre le spam. Elles seront utilisées avec tous les formulaires CRM.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TITLE"] = "Utiliser des clés reCAPTCHA v2 personnalisées";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TEXT"] = "Configuration terminée. Vous pouvez maintenant utiliser la protection anti-spam.";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TITLE"] = "Configuration automatique";
$MESS["CRM_FORM_CAPTCHA_JS_TITLE"] = "Paramètres de protection contre le spam";
