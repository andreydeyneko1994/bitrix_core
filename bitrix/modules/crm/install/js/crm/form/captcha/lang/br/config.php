<?php
$MESS["CRM_FORM_CAPTCHA_JS_ACCESS_DENIED"] = "Acesso negado. Por favor, entre em contato com o seu administrador do Bitrix24.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_HOWTO"] = "Como obter as chaves?";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TEXT"] = "Você pode optar por usar suas próprias chaves de proteção contra spam. Elas serão usadas em todos os formulários de CRM.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TITLE"] = "Usar chaves reCAPTCHA v2 personalizadas";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TEXT"] = "Configuração concluída. Agora você pode usar proteção contra spam.";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TITLE"] = "Configurar automaticamente";
$MESS["CRM_FORM_CAPTCHA_JS_TITLE"] = "Configurações de proteção contra spam";
