<?php
$MESS["CRM_FORM_CAPTCHA_JS_ACCESS_DENIED"] = "Acceso denegado. Comuníquese con su administrador de Bitrix24.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_HOWTO"] = "¿Cómo obtener las claves?";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TEXT"] = "Puede utilizar sus propias claves de protección contra el correo no deseado. Se utilizarán en todos los formularios del CRM.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TITLE"] = "Utilice claves de reCAPTCHA v2 personalizadas";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TEXT"] = "Configuración finalizada. Ahora puede utilizar la protección contra correo no deseado.";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TITLE"] = "Configuración automática";
$MESS["CRM_FORM_CAPTCHA_JS_TITLE"] = "Configuración de protección contra el correo no deseado";
