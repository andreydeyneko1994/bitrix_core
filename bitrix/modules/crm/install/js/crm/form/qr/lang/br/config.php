<?php
$MESS["CRM_WEBFORM_QR_DESC"] = "Faça a leitura do código QR com a câmera do seu celular para abrir o link";
$MESS["CRM_WEBFORM_QR_OPEN"] = "Abrir";
$MESS["CRM_WEBFORM_QR_TAKE_CODE"] = "Receba o código";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK"] = "Copiar link";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK_COMPLETE"] = "Copiado";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_OPEN_SITE"] = "Abrir numa nova guia";
$MESS["CRM_WEBFORM_QR_TITLE"] = "Abrir no dispositivo móvel";
