<?php
$MESS["CRM_WEBFORM_QR_DESC"] = "Scannez le code QR avec l'appareil photo de votre téléphone portable pour ouvrir le lien";
$MESS["CRM_WEBFORM_QR_OPEN"] = "Ouvrir";
$MESS["CRM_WEBFORM_QR_TAKE_CODE"] = "Récupérer le code";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK"] = "Copier le lien";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK_COMPLETE"] = "Copié";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_OPEN_SITE"] = "Ouvrir dans un nouvel onglet";
$MESS["CRM_WEBFORM_QR_TITLE"] = "Ouvrir sur un appareil mobile";
