<?php
$MESS["CRM_WEBFORM_QR_DESC"] = "Escanee el código QR con la cámara de su móvil para abrir el enlace";
$MESS["CRM_WEBFORM_QR_OPEN"] = "Abrir";
$MESS["CRM_WEBFORM_QR_TAKE_CODE"] = "Obtener el código";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK"] = "Copiar link";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK_COMPLETE"] = "Copiado";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_OPEN_SITE"] = "Abrir en una pestaña nueva";
$MESS["CRM_WEBFORM_QR_TITLE"] = "Abrir en un dispositivo móvil";
