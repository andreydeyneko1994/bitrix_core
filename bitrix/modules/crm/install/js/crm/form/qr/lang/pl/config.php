<?php
$MESS["CRM_WEBFORM_QR_DESC"] = "Aby otworzyć link, zeskanuj kod QR za pomocą aparatu smartfona";
$MESS["CRM_WEBFORM_QR_OPEN"] = "Otwórz";
$MESS["CRM_WEBFORM_QR_TAKE_CODE"] = "Uzyskaj kod";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK"] = "Kopiuj link";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_COPY_LINK_COMPLETE"] = "Skopiowano";
$MESS["CRM_WEBFORM_QR_TILE_POPUP_OPEN_SITE"] = "Otwórz w nowej karcie";
$MESS["CRM_WEBFORM_QR_TITLE"] = "Otwórz na urządzeniu mobilnym";
