<?php
$MESS["DOCUMENT_GRID_CANCEL"] = "Cancelar";
$MESS["DOCUMENT_GRID_CONTINUE"] = "Continuar";
$MESS["DOCUMENT_GRID_DOCUMENT_CANCEL_CONTENT"] = "¿Seguro que desea cancelar el procesamiento de esta entidad?";
$MESS["DOCUMENT_GRID_DOCUMENT_CANCEL_TITLE"] = "Cancelar el procesamiento";
$MESS["DOCUMENT_GRID_DOCUMENT_CONDUCT_CONTENT"] = "¿Estás seguro que desea procesar esta entidad?";
$MESS["DOCUMENT_GRID_DOCUMENT_CONDUCT_TITLE"] = "Proceso";
$MESS["DOCUMENT_GRID_DOCUMENT_DELETE_CONTENT"] = "¿Seguro que desea eliminar esta entidad?";
$MESS["DOCUMENT_GRID_DOCUMENT_DELETE_TITLE"] = "Eliminar entidad";
