<?php
$MESS["CRM_REPORT_TRACKING_AD_REPORT_BUILD"] = "Création d'un rapport";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_CLOSE"] = "Fermer";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_ERROR_TITLE"] = "C’est une erreur.";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_SETTINGS_HINT"] = "Vous devez configurer les paramètres publicitaires pour obtenir un rapport correct";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_ENABLED"] = "Activer";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_PAUSE"] = "arrêter";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_0"] = "Performance de la campagne";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_1"] = "Performance du groupe d'annonces";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_100"] = "Performance du mot-clé";
