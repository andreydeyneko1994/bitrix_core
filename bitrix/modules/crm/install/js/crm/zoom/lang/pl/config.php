<?php
$MESS["CRM_ZOOM_CREATE_MEETING_SERVER_RETURNS_ERROR"] = "Nie można utworzyć spotkania z powodu błędu serwera:";
$MESS["CRM_ZOOM_ERROR_EMPTY_TITLE"] = "Nazwa spotkania nie może być pusta.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DATETIME"] = "Godzina rozpoczęcia nie może być wcześniejsza niż bieżąca.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DURATION"] = "Nieprawidłowy czas trwania spotkania.";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DATE_CAPTION"] = "Data i godzina rozpoczęcia";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_CAPTION"] = "Czas trwania";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_HOURS"] = "godz.";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_MINUTES"] = "min";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE"] = "Nazwa";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE_PLACEHOLDER"] = "Spotkanie Zoom";
