<?php
$MESS["CLIENT_REQUISITES_ADDRESS_SHOW_ADDRESS"] = "Mostrar dirección";
$MESS["REQUISITE_LABEL_DETAILS_SELECT"] = "seleccionar de la lista";
$MESS["REQUISITE_LABEL_DETAILS_TEXT"] = "detalles";
$MESS["REQUISITE_LIST_EMPTY_PRESET_LIST"] = "No se encontró ninguna plantilla de datos";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_CONTENT"] = "¿Está seguro de que desea eliminar los datos y todas las direcciones asociadas?";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_TITLE"] = "Eliminar datos";
$MESS["REQUISITE_LIST_ITEM_ERROR_CAPTION"] = "Error";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_CONTENT"] = "¿Seguro que desea borrar los campos de datos?";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_TITLE"] = "Borrar los campos de datos";
$MESS["REQUISITE_TOOLTIP_ADD"] = "Agregar datos";
$MESS["REQUISITE_TOOLTIP_ADD_BANK_DETAILS"] = "agregar datos bancarios";
$MESS["REQUISITE_TOOLTIP_BANK_DETAILS_TITLE"] = "Datos bancarios";
$MESS["REQUISITE_TOOLTIP_DELETE"] = "eliminar";
$MESS["REQUISITE_TOOLTIP_EDIT"] = "cambiar";
$MESS["REQUISITE_TOOLTIP_SHOW_DETAILS"] = "detalles";
