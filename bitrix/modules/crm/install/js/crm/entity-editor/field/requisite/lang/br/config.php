<?php
$MESS["CLIENT_REQUISITES_ADDRESS_SHOW_ADDRESS"] = "Mostrar endereço";
$MESS["REQUISITE_LABEL_DETAILS_SELECT"] = "selecionar da lista";
$MESS["REQUISITE_LABEL_DETAILS_TEXT"] = "informações";
$MESS["REQUISITE_LIST_EMPTY_PRESET_LIST"] = "Nenhum modelo de informações foi encontrado";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_CONTENT"] = "Você tem certeza que deseja excluir as informações e todos os endereços associados?";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_TITLE"] = "Excluir informações";
$MESS["REQUISITE_LIST_ITEM_ERROR_CAPTION"] = "Erro";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_CONTENT"] = "Você tem certeza que deseja limpar os campos de informações?";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_TITLE"] = "Limpar campos de informações";
$MESS["REQUISITE_TOOLTIP_ADD"] = "Adicionar informações";
$MESS["REQUISITE_TOOLTIP_ADD_BANK_DETAILS"] = "adicionar dados bancários";
$MESS["REQUISITE_TOOLTIP_BANK_DETAILS_TITLE"] = "Dados bancários";
$MESS["REQUISITE_TOOLTIP_DELETE"] = "excluir";
$MESS["REQUISITE_TOOLTIP_EDIT"] = "mudar";
$MESS["REQUISITE_TOOLTIP_SHOW_DETAILS"] = "informações";
