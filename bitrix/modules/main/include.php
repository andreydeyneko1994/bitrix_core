<?php

/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2022 Bitrix
 */

use Bitrix\Main;
use Bitrix\Main\Session\Legacy\HealerEarlySessionStart;

require_once(__DIR__."/bx_root.php");
require_once(__DIR__."/start.php");

$application = Main\HttpApplication::getInstance();
$application->initializeExtendedKernel([
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
]);

if (defined('SITE_ID'))
{
	define('LANG', SITE_ID);
}

$context = $application->getContext();
$context->initializeCulture(defined('LANG') ? LANG : null, defined('LANGUAGE_ID') ? LANGUAGE_ID : null);

// needs to be after culture initialization
$application->start();

// constants for compatibility
$culture = $context->getCulture();
define('SITE_CHARSET', $culture->getCharset());
define('FORMAT_DATE', $culture->getFormatDate());
define('FORMAT_DATETIME', $culture->getFormatDatetime());
define('LANG_CHARSET', SITE_CHARSET);

$site = $context->getSiteObject();
if (!defined('LANG'))
{
	define('LANG', ($site ? $site->getLid() : $context->getLanguage()));
}
define('SITE_DIR', ($site ? $site->getDir() : ''));
define('SITE_SERVER_NAME', ($site ? $site->getServerName() : ''));
define('LANG_DIR', SITE_DIR);

if (!defined('LANGUAGE_ID'))
{
	define('LANGUAGE_ID', $context->getLanguage());
}
define('LANG_ADMIN_LID', LANGUAGE_ID);

if (!defined('SITE_ID'))
{
	define('SITE_ID', LANG);
}

/** @global $lang */
$lang = $context->getLanguage();

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = [];
$GLOBALS["ALL_LANG_FILES"] = [];
IncludeModuleLangFile(__DIR__."/tools.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR | E_PARSE) & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING & ~E_NOTICE);

if(!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

// global functions
require_once(__DIR__."/filter_tools.php");

define('BX_AJAX_PARAM_ID', 'bxajaxid');

/*ZDUyZmZMDljNDY4YjUzMWQ2ODUwZDNmNWM1YmM3YjBiNWVjMGM=*/$GLOBALS['_____2110909281']= array(base64_decode('R2'.'V'.'0TW9k'.'d'.'WxlRXZl'.'bnRz'),base64_decode(''.'RXhlY3V0ZU'.'1vZHV'.'sZ'.'UV'.'2'.'ZW'.'50RXg'.'='));$GLOBALS['____29772847']= array(base64_decode(''.'Z'.'GVma'.'W5l'),base64_decode(''.'c3RybGV'.'u'),base64_decode('Y'.'mFzZTY'.'0'.'X'.'2R'.'lY29'.'kZQ=='),base64_decode('dW5z'.'Z'.'XJp'.'YW'.'xpemU='),base64_decode('a'.'XNfYXJyYXk'.'='),base64_decode(''.'Y'.'2'.'91bnQ='),base64_decode('aW5'.'fYXJy'.'YXk'.'='),base64_decode('c'.'2VyaWFsaXpl'),base64_decode('YmF'.'zZTY0X2Vu'.'Y'.'2'.'9k'.'ZQ=='),base64_decode('c3Ryb'.'GV'.'u'),base64_decode(''.'YXJyYXlfa2V'.'5'.'X2V4aXN0cw=='),base64_decode('YXJy'.'YX'.'lf'.'a2V5X2V'.'4'.'aXN0'.'cw=='),base64_decode('bWt'.'0'.'aW1l'),base64_decode('ZGF'.'0'.'ZQ'.'=='),base64_decode(''.'ZGF0ZQ=='),base64_decode('Y'.'XJyYXl'.'f'.'a2V5X2V'.'4aXN0cw='.'='),base64_decode(''.'c3RybG'.'Vu'),base64_decode('Y'.'XJyY'.'Xlfa2'.'V'.'5X2V4'.'aXN0cw=='),base64_decode('c3Ryb'.'GV'.'u'),base64_decode('YXJ'.'yY'.'Xl'.'fa'.'2V5X2'.'V4aXN0'.'cw'.'=='),base64_decode('Y'.'XJyY'.'X'.'lfa2V'.'5X2V4aX'.'N0cw=='),base64_decode('bWt'.'0aW1l'),base64_decode(''.'ZGF0ZQ='.'='),base64_decode('ZGF0'.'Z'.'Q=='),base64_decode('bWV0aG9k'.'X2V4aXN'.'0cw=='),base64_decode('Y'.'2FsbF'.'9'.'1c2Vy'.'X2Z1b'.'m'.'NfYXJy'.'Y'.'Xk='),base64_decode('c'.'3'.'R'.'ybGVu'),base64_decode('YXJyYX'.'lfa2V5'.'X2V4aXN'.'0c'.'w'.'=='),base64_decode('Y'.'XJyYXl'.'fa2V'.'5X2V4aXN0cw='.'='),base64_decode('c2VyaWFsaXpl'),base64_decode('YmFzZTY0X2VuY29k'.'ZQ=='),base64_decode('c3RybGVu'),base64_decode('YXJy'.'YXlfa2'.'V5X2V4aXN0'.'cw=='),base64_decode(''.'YXJyYXlfa2'.'V5'.'X2V'.'4aXN0cw=='),base64_decode('YXJyYXlfa2V'.'5X'.'2V4a'.'XN0cw=='),base64_decode('aX'.'NfYXJyYXk='),base64_decode(''.'YXJ'.'y'.'YXlfa2V5X2V4aX'.'N'.'0cw=='),base64_decode('c2V'.'ya'.'W'.'F'.'saXpl'),base64_decode('Y'.'m'.'FzZTY0X'.'2Vu'.'Y29kZQ=='),base64_decode('YXJyYXlf'.'a'.'2V'.'5'.'X2V4'.'aXN'.'0cw=='),base64_decode(''.'YXJyYXlfa2V5X2'.'V4aXN'.'0cw=='),base64_decode('c2VyaWFsaXpl'),base64_decode('YmF'.'zZTY0'.'X2VuY29kZQ=='),base64_decode('aXN'.'f'.'Y'.'XJ'.'y'.'Y'.'Xk='),base64_decode('a'.'XNfYXJyYXk='),base64_decode('aW5f'.'YX'.'JyYXk='),base64_decode('YX'.'J'.'y'.'YXl'.'fa'.'2V5X2V4a'.'XN0cw=='),base64_decode('aW'.'5f'.'YXJyYXk'.'='),base64_decode('bWt0aW1l'),base64_decode('Z'.'GF0ZQ'.'='.'='),base64_decode('ZGF0ZQ'.'=='),base64_decode('ZG'.'F0'.'ZQ='.'='),base64_decode('bWt0aW1l'),base64_decode('ZGF0ZQ'.'='.'='),base64_decode('Z'.'GF0ZQ=='),base64_decode('a'.'W5f'.'YX'.'JyYX'.'k'.'='),base64_decode('YXJyYXl'.'fa2V5X2V'.'4'.'a'.'XN0c'.'w=='),base64_decode('YXJyYXl'.'f'.'a2V'.'5'.'X'.'2'.'V4aXN'.'0cw=='),base64_decode('c'.'2Vy'.'aW'.'Fs'.'aX'.'pl'),base64_decode('YmFzZTY'.'0X'.'2Vu'.'Y29kZQ=='),base64_decode('YXJ'.'yYXlfa2'.'V'.'5X2'.'V4a'.'XN0'.'cw=='),base64_decode('aW5'.'0'.'dm'.'F'.'s'),base64_decode('d'.'GltZQ=='),base64_decode(''.'YX'.'Jy'.'YXlfa2V5'.'X2V4aXN'.'0'.'cw='.'='),base64_decode(''.'Z'.'mls'.'Z'.'V9leGlzdH'.'M='),base64_decode('c3RyX3'.'JlcG'.'x'.'hY2U='),base64_decode('Y2'.'xh'.'c'.'3NfZXh'.'pc3'.'R'.'z'),base64_decode('ZG'.'VmaW5l'));if(!function_exists(__NAMESPACE__.'\\___879442563')){function ___879442563($_2143743224){static $_1060869535= false; if($_1060869535 == false) $_1060869535=array('SU5UUkFORVRfRURJV'.'ElPTg='.'=','WQ'.'='.'=','bWFpbg='.'=','fmNw'.'Z'.'l9tYXBfdmFsdWU=','','ZQ='.'=','Zg==','Z'.'Q='.'=',''.'Rg'.'='.'=','WA==','Zg='.'=','bWFpbg==','fmNw'.'Z'.'l9tYXBfd'.'mFsd'.'WU=','UG'.'9yd'.'GFs','Rg'.'==','Z'.'Q==','ZQ==',''.'WA='.'=','Rg==','RA==','RA==','bQ==','Z'.'A='.'=','W'.'Q='.'=','Z'.'g==','Zg==','Zg'.'==','Zg==','UG9yd'.'GF'.'s','Rg'.'==','ZQ==','ZQ==','WA==','Rg==','R'.'A'.'='.'=',''.'R'.'A==',''.'bQ==','ZA'.'==','WQ==','bWFpbg==',''.'T'.'24'.'=','U'.'2V0d'.'G'.'luZ3NDaGFuZ2U=',''.'Zg==',''.'Zg==','Z'.'g==','Zg==','bWFpbg='.'=','f'.'m'.'NwZl'.'9tYXBfdmFsdW'.'U'.'=','ZQ='.'=','ZQ='.'=','ZQ==',''.'RA==',''.'Z'.'Q==','ZQ==',''.'Zg==','Zg'.'==','Zg'.'='.'=','ZQ'.'==',''.'b'.'WFpb'.'g'.'==','fm'.'NwZl9'.'tYXBf'.'dmFsdWU=','ZQ==','Zg'.'==','Zg==','Zg='.'=','Zg==','bWFpbg==','fmNwZl9tYXB'.'fd'.'mFsdW'.'U=','ZQ'.'==',''.'Zg==',''.'UG9'.'ydGFs','UG9yd'.'GFs','ZQ==',''.'Z'.'Q'.'='.'=',''.'UG9yd'.'GFs','Rg==','WA==','Rg==','RA==','Z'.'Q==','Z'.'Q==','RA'.'==','bQ==','ZA==',''.'WQ'.'==','ZQ'.'==','WA'.'='.'=','ZQ==','R'.'g==','Z'.'Q==',''.'RA==','Zg==','ZQ==','RA==','Z'.'Q==','b'.'Q==','ZA==','WQ==','Zg==','Zg==',''.'Zg==','Zg==','Zg==','Zg==','Zg==','Zg'.'==','bWFpbg==','fmNwZl9t'.'YXBf'.'dmFs'.'dW'.'U=','Z'.'Q==','ZQ'.'==','UG9ydGFs','Rg='.'=','WA='.'=','VFlQ'.'RQ==','RE'.'FUR'.'Q==','RkV'.'BVFVSRV'.'M'.'=','RV'.'hQSVJFR'.'A='.'=','V'.'FlQ'.'R'.'Q==','RA'.'==',''.'VFJZX0'.'R'.'B'.'WVNfQ09V'.'Tl'.'Q=','REFURQ'.'==','VFJZX0R'.'BWVNfQ'.'09VTlQ'.'=','RV'.'hQSVJF'.'R'.'A==','RkVBVF'.'VSRV'.'M=','Z'.'g==','Zg==','RE9'.'DV'.'U1FTl'.'RfUk9'.'PVA==','L2JpdH'.'JpeC9tb2R1bG'.'Vz'.'Lw'.'==','L2luc3RhbGwvaW5'.'kZXg'.'ucG'.'hw','Lg==','Xw'.'==','c2Vh'.'cmNo','T'.'g='.'=','','','Q'.'UNUSVZF','WQ==','c29jaW'.'Fs'.'bmV0d29y'.'a'.'w'.'==','YWxsb'.'3dfZn'.'Jp'.'ZW'.'xkcw==','WQ==',''.'SUQ'.'=','c29'.'jaWFsbmV0d'.'29yaw==',''.'YWx'.'s'.'b3d'.'fZnJp'.'ZW'.'xkcw==','SUQ=','c2'.'9j'.'aW'.'Fsbm'.'V0d29ya'.'w'.'==','YW'.'xsb3d'.'fZnJpZWxk'.'cw==','Tg==','','',''.'QUNU'.'SVZF',''.'WQ==','c2'.'9jaWFsbmV0d'.'29yaw==','YWxsb'.'3dfbWljcm'.'9ibG9'.'nX3'.'VzZX'.'I=','WQ'.'='.'=','SUQ=','c29j'.'aWFs'.'bmV0d29y'.'aw==','YWxsb3d'.'fbWljcm9i'.'bG9'.'nX3VzZ'.'XI'.'=','SU'.'Q=','c29j'.'aWF'.'sbmV0d29ya'.'w==','Y'.'Wxsb'.'3dfb'.'Wlj'.'cm9ibG9nX3'.'VzZXI'.'=',''.'c29jaWFs'.'bmV'.'0d'.'29y'.'aw==','YW'.'xsb3dfb'.'Wljcm9i'.'bG9nX'.'2'.'dyb'.'3Vw','WQ==','SU'.'Q=',''.'c'.'29jaWFs'.'bmV0d29'.'ya'.'w='.'=',''.'Y'.'Wxsb3'.'dfbWljcm9ib'.'G9nX'.'2'.'dyb3Vw',''.'SUQ=','c29jaWFs'.'bmV0'.'d29yaw='.'=','YWx'.'s'.'b'.'3df'.'bW'.'ljcm'.'9ibG9nX2dyb'.'3V'.'w','Tg==','','','Q'.'UNUSVZF',''.'W'.'Q='.'=','c29j'.'a'.'WFsbm'.'V'.'0'.'d2'.'9yaw==',''.'YWxs'.'b3'.'df'.'Z'.'mlsZXNfd'.'XNlcg'.'==','WQ==',''.'SUQ=','c29jaWFsbmV0d29yaw==','YWxs'.'b3dfZ'.'mlsZXNfdXN'.'lcg==','SU'.'Q=',''.'c29jaWF'.'sb'.'mV0d29yaw==',''.'Y'.'Wx'.'sb'.'3d'.'fZm'.'l'.'sZXNf'.'dXNl'.'cg='.'=','T'.'g'.'==','','',''.'Q'.'UNUS'.'V'.'ZF','WQ==',''.'c29jaWFsbmV0d2'.'9y'.'aw==','Y'.'W'.'x'.'sb3dfYmx'.'vZ191c'.'2'.'Vy',''.'W'.'Q='.'=','SUQ=','c2'.'9jaWFsbm'.'V0d'.'29yaw'.'='.'=','Y'.'W'.'xsb'.'3dfYmxvZ'.'191c2Vy','SU'.'Q=','c'.'29j'.'aWFsb'.'mV'.'0d29yaw'.'='.'=','Y'.'Wxsb'.'3d'.'fYmxvZ191c2Vy',''.'Tg'.'='.'=','','','QUNUSVZF','WQ==','c29ja'.'W'.'Fsb'.'mV0'.'d29y'.'aw==','YWxsb'.'3dfc'.'GhvdG9'.'fdXNlc'.'g==','WQ==','S'.'UQ=','c29'.'jaWF'.'sb'.'mV'.'0d29yaw==','Y'.'Wxsb3dfcGh'.'vd'.'G9fdXN'.'lcg==','SUQ=','c29jaWFsbmV0d29yaw==','YWx'.'sb3dfcGhvdG9'.'f'.'dXN'.'l'.'c'.'g'.'='.'=','Tg==','','','QU'.'NUSVZF',''.'WQ==','c29jaWFsbmV0d'.'29yaw==','YW'.'xsb'.'3df'.'Zm9ydW1fdX'.'Nlcg==','W'.'Q==',''.'SUQ=',''.'c29'.'jaWFsbmV0'.'d29yaw==','YWxsb'.'3dfZ'.'m9'.'ydW1fdXNlcg='.'=','SUQ=','c2'.'9jaWFs'.'bmV0d2'.'9yaw==','YWxsb3dfZ'.'m9ydW'.'1fd'.'XNlcg'.'==','Tg'.'==','','','QU'.'NUSVZ'.'F','WQ'.'==',''.'c29'.'j'.'aWF'.'sb'.'mV0'.'d29'.'yaw==','YWxsb3dfdGFza3Nfd'.'XNlcg==','WQ='.'=','SUQ=','c29'.'jaWFsbmV0'.'d'.'29yaw'.'==',''.'YW'.'x'.'sb'.'3dfdGFza3'.'Nfd'.'XNlcg='.'=',''.'S'.'UQ=',''.'c29jaWFsbmV0'.'d29y'.'a'.'w==',''.'Y'.'Wxsb3df'.'dG'.'Fza3N'.'fdXN'.'lcg==','c29j'.'aWF'.'sbmV0d29yaw==',''.'Y'.'W'.'x'.'sb3df'.'dGFza3'.'NfZ3'.'JvdX'.'A=','WQ'.'==','SUQ=',''.'c29j'.'aWFsbm'.'V0d29y'.'aw'.'==','YWx'.'sb3dfdGFza3NfZ3JvdXA=','SU'.'Q=','c29ja'.'WFsb'.'m'.'V0'.'d29yaw==','Y'.'W'.'xsb3'.'d'.'fdGFza3Nf'.'Z3Jv'.'d'.'XA'.'=',''.'dGF'.'za3'.'M=','T'.'g==','','','QUN'.'USVZF','WQ==','c'.'29jaWFsb'.'m'.'V0d2'.'9yaw==','Y'.'Wx'.'sb3dfY2'.'FsZW5'.'k'.'YXJ'.'fd'.'XNl'.'cg='.'=','W'.'Q'.'==',''.'SUQ=','c29ja'.'WFs'.'bmV'.'0d29yaw==','YWxs'.'b3dfY'.'2Fs'.'ZW5'.'kYX'.'JfdXNlcg==','SUQ=','c'.'29jaWF'.'s'.'bmV0d'.'29yaw'.'==','YWxsb3d'.'fY'.'2FsZW5kYXJfdXNl'.'cg==','c'.'29ja'.'W'.'FsbmV0d29yaw==','YWxs'.'b3dfY2Fs'.'ZW5kYXJ'.'f'.'Z3J'.'vdX'.'A'.'=','WQ==','SUQ=','c'.'29'.'jaW'.'F'.'sbmV0'.'d2'.'9yaw'.'==','YWxs'.'b3'.'dfY2Fs'.'ZW5kY'.'XJfZ3'.'JvdXA=','SUQ=','c29jaW'.'Fs'.'bmV0d29yaw='.'=',''.'YW'.'xsb3dfY2Fs'.'ZW5kYXJfZ3J'.'vdXA=','QUNUSVZF','WQ==','T'.'g==','ZXh0cmFuZXQ=',''.'a'.'WJs'.'b2Nr','T2'.'5B'.'Zn'.'Rl'.'cklCb'.'G'.'9ja0'.'VsZW1lbn'.'RVcGRhdGU=','aW50cmF'.'uZXQ'.'=','Q'.'0'.'ludHJ'.'hbmV0RXZlbnRIYW5kb'.'GVycw==','U1BSZWdpc3Rl'.'clVwZGF0ZW'.'RJdGV'.'t','Q0l'.'udHJhbmV0U'.'2'.'h'.'hc'.'mVwb2l'.'udDo'.'6QWdlbn'.'RMa'.'XN0cygpOw'.'='.'=','aW50'.'cmFuZXQ=','Tg'.'==',''.'Q0lud'.'HJhbmV'.'0'.'U2hh'.'cm'.'Vwb2lu'.'dDo6QWdlbnR'.'RdWV1Z'.'SgpOw==',''.'a'.'W50cmFuZX'.'Q'.'=','Tg==','Q0lud'.'HJhb'.'m'.'V0U2hhcmVwb2'.'lu'.'d'.'Do'.'6QW'.'dl'.'b'.'nR'.'VcGRhd'.'GUoKTs=',''.'aW50cmFuZXQ=','Tg==','aWJ'.'sb2Nr','T25BZnRlcklCbG9'.'ja0'.'VsZW1'.'lbn'.'RBZG'.'Q=','a'.'W50'.'cm'.'F'.'uZXQ=',''.'Q0ludH'.'JhbmV0RXZlbnRI'.'Y'.'W5kbGVycw==','U1BSZWdpc3RlclV'.'wZG'.'F'.'0ZWRJdGVt','aWJsb2'.'Nr','T25B'.'Z'.'nRlcklCbG'.'9ja0V'.'sZW1lbn'.'RVcGRh'.'dGU=','aW50'.'cmFu'.'Z'.'XQ=','Q0lud'.'HJhbmV0R'.'X'.'Zlbn'.'RIY'.'W'.'5'.'kbG'.'Vycw==',''.'U1B'.'SZWd'.'p'.'c3'.'Rl'.'cl'.'V'.'w'.'ZGF0'.'ZWRJdGVt','Q0'.'ludHJ'.'hbmV0'.'U2'.'hhcm'.'Vwb2'.'ludD'.'o6QWdlbn'.'RMaXN0cygpOw==','aW50'.'c'.'mFuZXQ=','Q0lud'.'H'.'JhbmV0U2hhcmVwb2ludDo6QWdlbnRRdW'.'V'.'1ZS'.'gp'.'Ow==','aW5'.'0c'.'mF'.'uZXQ=','Q0ludHJhbm'.'V0'.'U'.'2hhcmVwb2'.'l'.'ud'.'D'.'o'.'6QWdlbnRVcGRhdGU'.'o'.'KTs=','aW50cmF'.'uZXQ'.'=','Y3Jt',''.'bWFpbg'.'==',''.'T25C'.'ZW'.'Z'.'vcmVQ'.'cm9'.'s'.'b2c=',''.'bWFpbg==','Q1dp'.'em'.'FyZFNvb'.'FBh'.'bmV'.'sSW50'.'cmFuZXQ'.'=',''.'U2hvd1BhbmV'.'s',''.'L21vZH'.'VsZ'.'XMva'.'W50cmFuZXQ'.'vcGFuZWxfYnV0dG9uLn'.'BocA==','RU5DT0RF','WQ==');return base64_decode($_1060869535[$_2143743224]);}};$GLOBALS['____29772847'][0](___879442563(0), ___879442563(1));class CBXFeatures{ private static $_10045780= 30; private static $_277558867= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller", "LdapUnlimitedUsers",), "Holding" => array( "Cluster", "MultiSites",),); private static $_1546593339= false; private static $_693840745= false; private static function __1370576700(){ if(self::$_1546593339 == false){ self::$_1546593339= array(); foreach(self::$_277558867 as $_1839862404 => $_2057055557){ foreach($_2057055557 as $_1145895983) self::$_1546593339[$_1145895983]= $_1839862404;}} if(self::$_693840745 == false){ self::$_693840745= array(); $_937726425= COption::GetOptionString(___879442563(2), ___879442563(3), ___879442563(4)); if($GLOBALS['____29772847'][1]($_937726425)> min(202,0,67.333333333333)){ $_937726425= $GLOBALS['____29772847'][2]($_937726425); self::$_693840745= $GLOBALS['____29772847'][3]($_937726425); if(!$GLOBALS['____29772847'][4](self::$_693840745)) self::$_693840745= array();} if($GLOBALS['____29772847'][5](self::$_693840745) <= min(182,0,60.666666666667)) self::$_693840745= array(___879442563(5) => array(), ___879442563(6) => array());}} public static function InitiateEditionsSettings($_831661298){ self::__1370576700(); $_178850927= array(); foreach(self::$_277558867 as $_1839862404 => $_2057055557){ $_512088747= $GLOBALS['____29772847'][6]($_1839862404, $_831661298); self::$_693840745[___879442563(7)][$_1839862404]=($_512088747? array(___879442563(8)): array(___879442563(9))); foreach($_2057055557 as $_1145895983){ self::$_693840745[___879442563(10)][$_1145895983]= $_512088747; if(!$_512088747) $_178850927[]= array($_1145895983, false);}} $_878699415= $GLOBALS['____29772847'][7](self::$_693840745); $_878699415= $GLOBALS['____29772847'][8]($_878699415); COption::SetOptionString(___879442563(11), ___879442563(12), $_878699415); foreach($_178850927 as $_716602235) self::__475659820($_716602235[min(32,0,10.666666666667)], $_716602235[round(0+0.5+0.5)]);} public static function IsFeatureEnabled($_1145895983){ if($GLOBALS['____29772847'][9]($_1145895983) <= 0) return true; self::__1370576700(); if(!$GLOBALS['____29772847'][10]($_1145895983, self::$_1546593339)) return true; if(self::$_1546593339[$_1145895983] == ___879442563(13)) $_1581663671= array(___879442563(14)); elseif($GLOBALS['____29772847'][11](self::$_1546593339[$_1145895983], self::$_693840745[___879442563(15)])) $_1581663671= self::$_693840745[___879442563(16)][self::$_1546593339[$_1145895983]]; else $_1581663671= array(___879442563(17)); if($_1581663671[(192*2-384)] != ___879442563(18) && $_1581663671[(146*2-292)] != ___879442563(19)){ return false;} elseif($_1581663671[min(56,0,18.666666666667)] == ___879442563(20)){ if($_1581663671[round(0+0.2+0.2+0.2+0.2+0.2)]< $GLOBALS['____29772847'][12]((982-2*491),(902-2*451),(1336/2-668), Date(___879442563(21)), $GLOBALS['____29772847'][13](___879442563(22))- self::$_10045780, $GLOBALS['____29772847'][14](___879442563(23)))){ if(!isset($_1581663671[round(0+1+1)]) ||!$_1581663671[round(0+0.66666666666667+0.66666666666667+0.66666666666667)]) self::__1865142460(self::$_1546593339[$_1145895983]); return false;}} return!$GLOBALS['____29772847'][15]($_1145895983, self::$_693840745[___879442563(24)]) || self::$_693840745[___879442563(25)][$_1145895983];} public static function IsFeatureInstalled($_1145895983){ if($GLOBALS['____29772847'][16]($_1145895983) <= 0) return true; self::__1370576700(); return($GLOBALS['____29772847'][17]($_1145895983, self::$_693840745[___879442563(26)]) && self::$_693840745[___879442563(27)][$_1145895983]);} public static function IsFeatureEditable($_1145895983){ if($GLOBALS['____29772847'][18]($_1145895983) <= 0) return true; self::__1370576700(); if(!$GLOBALS['____29772847'][19]($_1145895983, self::$_1546593339)) return true; if(self::$_1546593339[$_1145895983] == ___879442563(28)) $_1581663671= array(___879442563(29)); elseif($GLOBALS['____29772847'][20](self::$_1546593339[$_1145895983], self::$_693840745[___879442563(30)])) $_1581663671= self::$_693840745[___879442563(31)][self::$_1546593339[$_1145895983]]; else $_1581663671= array(___879442563(32)); if($_1581663671[(194*2-388)] != ___879442563(33) && $_1581663671[(134*2-268)] != ___879442563(34)){ return false;} elseif($_1581663671[(1448/2-724)] == ___879442563(35)){ if($_1581663671[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]< $GLOBALS['____29772847'][21]((1152/2-576), min(170,0,56.666666666667),(1356/2-678), Date(___879442563(36)), $GLOBALS['____29772847'][22](___879442563(37))- self::$_10045780, $GLOBALS['____29772847'][23](___879442563(38)))){ if(!isset($_1581663671[round(0+1+1)]) ||!$_1581663671[round(0+2)]) self::__1865142460(self::$_1546593339[$_1145895983]); return false;}} return true;} private static function __475659820($_1145895983, $_1405960180){ if($GLOBALS['____29772847'][24]("CBXFeatures", "On".$_1145895983."SettingsChange")) $GLOBALS['____29772847'][25](array("CBXFeatures", "On".$_1145895983."SettingsChange"), array($_1145895983, $_1405960180)); $_453328843= $GLOBALS['_____2110909281'][0](___879442563(39), ___879442563(40).$_1145895983.___879442563(41)); while($_409264351= $_453328843->Fetch()) $GLOBALS['_____2110909281'][1]($_409264351, array($_1145895983, $_1405960180));} public static function SetFeatureEnabled($_1145895983, $_1405960180= true, $_536069501= true){ if($GLOBALS['____29772847'][26]($_1145895983) <= 0) return; if(!self::IsFeatureEditable($_1145895983)) $_1405960180= false; $_1405960180=($_1405960180? true: false); self::__1370576700(); $_1118924567=(!$GLOBALS['____29772847'][27]($_1145895983, self::$_693840745[___879442563(42)]) && $_1405960180 || $GLOBALS['____29772847'][28]($_1145895983, self::$_693840745[___879442563(43)]) && $_1405960180 != self::$_693840745[___879442563(44)][$_1145895983]); self::$_693840745[___879442563(45)][$_1145895983]= $_1405960180; $_878699415= $GLOBALS['____29772847'][29](self::$_693840745); $_878699415= $GLOBALS['____29772847'][30]($_878699415); COption::SetOptionString(___879442563(46), ___879442563(47), $_878699415); if($_1118924567 && $_536069501) self::__475659820($_1145895983, $_1405960180);} private static function __1865142460($_1839862404){ if($GLOBALS['____29772847'][31]($_1839862404) <= 0 || $_1839862404 == "Portal") return; self::__1370576700(); if(!$GLOBALS['____29772847'][32]($_1839862404, self::$_693840745[___879442563(48)]) || $GLOBALS['____29772847'][33]($_1839862404, self::$_693840745[___879442563(49)]) && self::$_693840745[___879442563(50)][$_1839862404][(1424/2-712)] != ___879442563(51)) return; if(isset(self::$_693840745[___879442563(52)][$_1839862404][round(0+1+1)]) && self::$_693840745[___879442563(53)][$_1839862404][round(0+0.66666666666667+0.66666666666667+0.66666666666667)]) return; $_178850927= array(); if($GLOBALS['____29772847'][34]($_1839862404, self::$_277558867) && $GLOBALS['____29772847'][35](self::$_277558867[$_1839862404])){ foreach(self::$_277558867[$_1839862404] as $_1145895983){ if($GLOBALS['____29772847'][36]($_1145895983, self::$_693840745[___879442563(54)]) && self::$_693840745[___879442563(55)][$_1145895983]){ self::$_693840745[___879442563(56)][$_1145895983]= false; $_178850927[]= array($_1145895983, false);}} self::$_693840745[___879442563(57)][$_1839862404][round(0+0.66666666666667+0.66666666666667+0.66666666666667)]= true;} $_878699415= $GLOBALS['____29772847'][37](self::$_693840745); $_878699415= $GLOBALS['____29772847'][38]($_878699415); COption::SetOptionString(___879442563(58), ___879442563(59), $_878699415); foreach($_178850927 as $_716602235) self::__475659820($_716602235[(900-2*450)], $_716602235[round(0+0.5+0.5)]);} public static function ModifyFeaturesSettings($_831661298, $_2057055557){ self::__1370576700(); foreach($_831661298 as $_1839862404 => $_740373560) self::$_693840745[___879442563(60)][$_1839862404]= $_740373560; $_178850927= array(); foreach($_2057055557 as $_1145895983 => $_1405960180){ if(!$GLOBALS['____29772847'][39]($_1145895983, self::$_693840745[___879442563(61)]) && $_1405960180 || $GLOBALS['____29772847'][40]($_1145895983, self::$_693840745[___879442563(62)]) && $_1405960180 != self::$_693840745[___879442563(63)][$_1145895983]) $_178850927[]= array($_1145895983, $_1405960180); self::$_693840745[___879442563(64)][$_1145895983]= $_1405960180;} $_878699415= $GLOBALS['____29772847'][41](self::$_693840745); $_878699415= $GLOBALS['____29772847'][42]($_878699415); COption::SetOptionString(___879442563(65), ___879442563(66), $_878699415); self::$_693840745= false; foreach($_178850927 as $_716602235) self::__475659820($_716602235[(127*2-254)], $_716602235[round(0+0.5+0.5)]);} public static function SaveFeaturesSettings($_1929078729, $_1998705412){ self::__1370576700(); $_408672913= array(___879442563(67) => array(), ___879442563(68) => array()); if(!$GLOBALS['____29772847'][43]($_1929078729)) $_1929078729= array(); if(!$GLOBALS['____29772847'][44]($_1998705412)) $_1998705412= array(); if(!$GLOBALS['____29772847'][45](___879442563(69), $_1929078729)) $_1929078729[]= ___879442563(70); foreach(self::$_277558867 as $_1839862404 => $_2057055557){ if($GLOBALS['____29772847'][46]($_1839862404, self::$_693840745[___879442563(71)])) $_1122693200= self::$_693840745[___879442563(72)][$_1839862404]; else $_1122693200=($_1839862404 == ___879442563(73))? array(___879442563(74)): array(___879442563(75)); if($_1122693200[(212*2-424)] == ___879442563(76) || $_1122693200[(194*2-388)] == ___879442563(77)){ $_408672913[___879442563(78)][$_1839862404]= $_1122693200;} else{ if($GLOBALS['____29772847'][47]($_1839862404, $_1929078729)) $_408672913[___879442563(79)][$_1839862404]= array(___879442563(80), $GLOBALS['____29772847'][48](min(212,0,70.666666666667),(958-2*479),(1240/2-620), $GLOBALS['____29772847'][49](___879442563(81)), $GLOBALS['____29772847'][50](___879442563(82)), $GLOBALS['____29772847'][51](___879442563(83)))); else $_408672913[___879442563(84)][$_1839862404]= array(___879442563(85));}} $_178850927= array(); foreach(self::$_1546593339 as $_1145895983 => $_1839862404){ if($_408672913[___879442563(86)][$_1839862404][(206*2-412)] != ___879442563(87) && $_408672913[___879442563(88)][$_1839862404][(1116/2-558)] != ___879442563(89)){ $_408672913[___879442563(90)][$_1145895983]= false;} else{ if($_408672913[___879442563(91)][$_1839862404][(127*2-254)] == ___879442563(92) && $_408672913[___879442563(93)][$_1839862404][round(0+0.25+0.25+0.25+0.25)]< $GLOBALS['____29772847'][52](min(220,0,73.333333333333),(798-2*399),(818-2*409), Date(___879442563(94)), $GLOBALS['____29772847'][53](___879442563(95))- self::$_10045780, $GLOBALS['____29772847'][54](___879442563(96)))) $_408672913[___879442563(97)][$_1145895983]= false; else $_408672913[___879442563(98)][$_1145895983]= $GLOBALS['____29772847'][55]($_1145895983, $_1998705412); if(!$GLOBALS['____29772847'][56]($_1145895983, self::$_693840745[___879442563(99)]) && $_408672913[___879442563(100)][$_1145895983] || $GLOBALS['____29772847'][57]($_1145895983, self::$_693840745[___879442563(101)]) && $_408672913[___879442563(102)][$_1145895983] != self::$_693840745[___879442563(103)][$_1145895983]) $_178850927[]= array($_1145895983, $_408672913[___879442563(104)][$_1145895983]);}} $_878699415= $GLOBALS['____29772847'][58]($_408672913); $_878699415= $GLOBALS['____29772847'][59]($_878699415); COption::SetOptionString(___879442563(105), ___879442563(106), $_878699415); self::$_693840745= false; foreach($_178850927 as $_716602235) self::__475659820($_716602235[(1124/2-562)], $_716602235[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function GetFeaturesList(){ self::__1370576700(); $_1211712219= array(); foreach(self::$_277558867 as $_1839862404 => $_2057055557){ if($GLOBALS['____29772847'][60]($_1839862404, self::$_693840745[___879442563(107)])) $_1122693200= self::$_693840745[___879442563(108)][$_1839862404]; else $_1122693200=($_1839862404 == ___879442563(109))? array(___879442563(110)): array(___879442563(111)); $_1211712219[$_1839862404]= array( ___879442563(112) => $_1122693200[min(56,0,18.666666666667)], ___879442563(113) => $_1122693200[round(0+0.33333333333333+0.33333333333333+0.33333333333333)], ___879442563(114) => array(),); $_1211712219[$_1839862404][___879442563(115)]= false; if($_1211712219[$_1839862404][___879442563(116)] == ___879442563(117)){ $_1211712219[$_1839862404][___879442563(118)]= $GLOBALS['____29772847'][61](($GLOBALS['____29772847'][62]()- $_1211712219[$_1839862404][___879442563(119)])/ round(0+28800+28800+28800)); if($_1211712219[$_1839862404][___879442563(120)]> self::$_10045780) $_1211712219[$_1839862404][___879442563(121)]= true;} foreach($_2057055557 as $_1145895983) $_1211712219[$_1839862404][___879442563(122)][$_1145895983]=(!$GLOBALS['____29772847'][63]($_1145895983, self::$_693840745[___879442563(123)]) || self::$_693840745[___879442563(124)][$_1145895983]);} return $_1211712219;} private static function __1696621896($_900104059, $_842591663){ if(IsModuleInstalled($_900104059) == $_842591663) return true; $_1379758741= $_SERVER[___879442563(125)].___879442563(126).$_900104059.___879442563(127); if(!$GLOBALS['____29772847'][64]($_1379758741)) return false; include_once($_1379758741); $_721446038= $GLOBALS['____29772847'][65](___879442563(128), ___879442563(129), $_900104059); if(!$GLOBALS['____29772847'][66]($_721446038)) return false; $_2133058989= new $_721446038; if($_842591663){ if(!$_2133058989->InstallDB()) return false; $_2133058989->InstallEvents(); if(!$_2133058989->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___879442563(130))) CSearch::DeleteIndex($_900104059); UnRegisterModule($_900104059);} return true;} protected static function OnRequestsSettingsChange($_1145895983, $_1405960180){ self::__1696621896("form", $_1405960180);} protected static function OnLearningSettingsChange($_1145895983, $_1405960180){ self::__1696621896("learning", $_1405960180);} protected static function OnJabberSettingsChange($_1145895983, $_1405960180){ self::__1696621896("xmpp", $_1405960180);} protected static function OnVideoConferenceSettingsChange($_1145895983, $_1405960180){ self::__1696621896("video", $_1405960180);} protected static function OnBizProcSettingsChange($_1145895983, $_1405960180){ self::__1696621896("bizprocdesigner", $_1405960180);} protected static function OnListsSettingsChange($_1145895983, $_1405960180){ self::__1696621896("lists", $_1405960180);} protected static function OnWikiSettingsChange($_1145895983, $_1405960180){ self::__1696621896("wiki", $_1405960180);} protected static function OnSupportSettingsChange($_1145895983, $_1405960180){ self::__1696621896("support", $_1405960180);} protected static function OnControllerSettingsChange($_1145895983, $_1405960180){ self::__1696621896("controller", $_1405960180);} protected static function OnAnalyticsSettingsChange($_1145895983, $_1405960180){ self::__1696621896("statistic", $_1405960180);} protected static function OnVoteSettingsChange($_1145895983, $_1405960180){ self::__1696621896("vote", $_1405960180);} protected static function OnFriendsSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(131); $_213023458= CSite::GetList(($_512088747= ___879442563(132)),($_1177638507= ___879442563(133)), array(___879442563(134) => ___879442563(135))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(136), ___879442563(137), ___879442563(138), $_922123110[___879442563(139)]) != $_1727208188){ COption::SetOptionString(___879442563(140), ___879442563(141), $_1727208188, false, $_922123110[___879442563(142)]); COption::SetOptionString(___879442563(143), ___879442563(144), $_1727208188);}}} protected static function OnMicroBlogSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(145); $_213023458= CSite::GetList(($_512088747= ___879442563(146)),($_1177638507= ___879442563(147)), array(___879442563(148) => ___879442563(149))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(150), ___879442563(151), ___879442563(152), $_922123110[___879442563(153)]) != $_1727208188){ COption::SetOptionString(___879442563(154), ___879442563(155), $_1727208188, false, $_922123110[___879442563(156)]); COption::SetOptionString(___879442563(157), ___879442563(158), $_1727208188);} if(COption::GetOptionString(___879442563(159), ___879442563(160), ___879442563(161), $_922123110[___879442563(162)]) != $_1727208188){ COption::SetOptionString(___879442563(163), ___879442563(164), $_1727208188, false, $_922123110[___879442563(165)]); COption::SetOptionString(___879442563(166), ___879442563(167), $_1727208188);}}} protected static function OnPersonalFilesSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(168); $_213023458= CSite::GetList(($_512088747= ___879442563(169)),($_1177638507= ___879442563(170)), array(___879442563(171) => ___879442563(172))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(173), ___879442563(174), ___879442563(175), $_922123110[___879442563(176)]) != $_1727208188){ COption::SetOptionString(___879442563(177), ___879442563(178), $_1727208188, false, $_922123110[___879442563(179)]); COption::SetOptionString(___879442563(180), ___879442563(181), $_1727208188);}}} protected static function OnPersonalBlogSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(182); $_213023458= CSite::GetList(($_512088747= ___879442563(183)),($_1177638507= ___879442563(184)), array(___879442563(185) => ___879442563(186))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(187), ___879442563(188), ___879442563(189), $_922123110[___879442563(190)]) != $_1727208188){ COption::SetOptionString(___879442563(191), ___879442563(192), $_1727208188, false, $_922123110[___879442563(193)]); COption::SetOptionString(___879442563(194), ___879442563(195), $_1727208188);}}} protected static function OnPersonalPhotoSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(196); $_213023458= CSite::GetList(($_512088747= ___879442563(197)),($_1177638507= ___879442563(198)), array(___879442563(199) => ___879442563(200))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(201), ___879442563(202), ___879442563(203), $_922123110[___879442563(204)]) != $_1727208188){ COption::SetOptionString(___879442563(205), ___879442563(206), $_1727208188, false, $_922123110[___879442563(207)]); COption::SetOptionString(___879442563(208), ___879442563(209), $_1727208188);}}} protected static function OnPersonalForumSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(210); $_213023458= CSite::GetList(($_512088747= ___879442563(211)),($_1177638507= ___879442563(212)), array(___879442563(213) => ___879442563(214))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(215), ___879442563(216), ___879442563(217), $_922123110[___879442563(218)]) != $_1727208188){ COption::SetOptionString(___879442563(219), ___879442563(220), $_1727208188, false, $_922123110[___879442563(221)]); COption::SetOptionString(___879442563(222), ___879442563(223), $_1727208188);}}} protected static function OnTasksSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(224); $_213023458= CSite::GetList(($_512088747= ___879442563(225)),($_1177638507= ___879442563(226)), array(___879442563(227) => ___879442563(228))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(229), ___879442563(230), ___879442563(231), $_922123110[___879442563(232)]) != $_1727208188){ COption::SetOptionString(___879442563(233), ___879442563(234), $_1727208188, false, $_922123110[___879442563(235)]); COption::SetOptionString(___879442563(236), ___879442563(237), $_1727208188);} if(COption::GetOptionString(___879442563(238), ___879442563(239), ___879442563(240), $_922123110[___879442563(241)]) != $_1727208188){ COption::SetOptionString(___879442563(242), ___879442563(243), $_1727208188, false, $_922123110[___879442563(244)]); COption::SetOptionString(___879442563(245), ___879442563(246), $_1727208188);}} self::__1696621896(___879442563(247), $_1405960180);} protected static function OnCalendarSettingsChange($_1145895983, $_1405960180){ if($_1405960180) $_1727208188= "Y"; else $_1727208188= ___879442563(248); $_213023458= CSite::GetList(($_512088747= ___879442563(249)),($_1177638507= ___879442563(250)), array(___879442563(251) => ___879442563(252))); while($_922123110= $_213023458->Fetch()){ if(COption::GetOptionString(___879442563(253), ___879442563(254), ___879442563(255), $_922123110[___879442563(256)]) != $_1727208188){ COption::SetOptionString(___879442563(257), ___879442563(258), $_1727208188, false, $_922123110[___879442563(259)]); COption::SetOptionString(___879442563(260), ___879442563(261), $_1727208188);} if(COption::GetOptionString(___879442563(262), ___879442563(263), ___879442563(264), $_922123110[___879442563(265)]) != $_1727208188){ COption::SetOptionString(___879442563(266), ___879442563(267), $_1727208188, false, $_922123110[___879442563(268)]); COption::SetOptionString(___879442563(269), ___879442563(270), $_1727208188);}}} protected static function OnSMTPSettingsChange($_1145895983, $_1405960180){ self::__1696621896("mail", $_1405960180);} protected static function OnExtranetSettingsChange($_1145895983, $_1405960180){ $_1485005567= COption::GetOptionString("extranet", "extranet_site", ""); if($_1485005567){ $_1235377537= new CSite; $_1235377537->Update($_1485005567, array(___879442563(271) =>($_1405960180? ___879442563(272): ___879442563(273))));} self::__1696621896(___879442563(274), $_1405960180);} protected static function OnDAVSettingsChange($_1145895983, $_1405960180){ self::__1696621896("dav", $_1405960180);} protected static function OntimemanSettingsChange($_1145895983, $_1405960180){ self::__1696621896("timeman", $_1405960180);} protected static function Onintranet_sharepointSettingsChange($_1145895983, $_1405960180){ if($_1405960180){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___879442563(275), ___879442563(276), ___879442563(277), ___879442563(278), ___879442563(279)); CAgent::AddAgent(___879442563(280), ___879442563(281), ___879442563(282), round(0+166.66666666667+166.66666666667+166.66666666667)); CAgent::AddAgent(___879442563(283), ___879442563(284), ___879442563(285), round(0+300)); CAgent::AddAgent(___879442563(286), ___879442563(287), ___879442563(288), round(0+1800+1800));} else{ UnRegisterModuleDependences(___879442563(289), ___879442563(290), ___879442563(291), ___879442563(292), ___879442563(293)); UnRegisterModuleDependences(___879442563(294), ___879442563(295), ___879442563(296), ___879442563(297), ___879442563(298)); CAgent::RemoveAgent(___879442563(299), ___879442563(300)); CAgent::RemoveAgent(___879442563(301), ___879442563(302)); CAgent::RemoveAgent(___879442563(303), ___879442563(304));}} protected static function OncrmSettingsChange($_1145895983, $_1405960180){ if($_1405960180) COption::SetOptionString("crm", "form_features", "Y"); self::__1696621896(___879442563(305), $_1405960180);} protected static function OnClusterSettingsChange($_1145895983, $_1405960180){ self::__1696621896("cluster", $_1405960180);} protected static function OnMultiSitesSettingsChange($_1145895983, $_1405960180){ if($_1405960180) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___879442563(306), ___879442563(307), ___879442563(308), ___879442563(309), ___879442563(310), ___879442563(311));} protected static function OnIdeaSettingsChange($_1145895983, $_1405960180){ self::__1696621896("idea", $_1405960180);} protected static function OnMeetingSettingsChange($_1145895983, $_1405960180){ self::__1696621896("meeting", $_1405960180);} protected static function OnXDImportSettingsChange($_1145895983, $_1405960180){ self::__1696621896("xdimport", $_1405960180);}} $GLOBALS['____29772847'][67](___879442563(312), ___879442563(313));/**/			//Do not remove this

//component 2.0 template engines
$GLOBALS["arCustomTemplateEngines"] = [];

require_once(__DIR__."/autoload.php");
require_once(__DIR__."/classes/general/menu.php");
require_once(__DIR__."/classes/mysql/usertype.php");

if(file_exists(($_fname = __DIR__."/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = False;
	include($_fname);
}

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
	include_once($_fname);

if(($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(!defined("BX_FILE_PERMISSIONS"))
	define("BX_FILE_PERMISSIONS", 0644);
if(!defined("BX_DIR_PERMISSIONS"))
	define("BX_DIR_PERMISSIONS", 0755);

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && mb_substr($GLOBALS["APPLICATION"]->GetCurPage(), 0, mb_strlen(BX_ROOT."/admin/")) != BX_ROOT."/admin/")) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && LANG_CHARSET <> '')
	header("Content-Type: text/html; charset=".LANG_CHARSET);

if(COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");

header("X-Powered-CMS: Bitrix Site Manager (".(LICENSE_KEY == "DEMO"? "DEMO" : md5("BITRIX".LICENSE_KEY."LICENCE")).")");
if (COption::GetOptionString("main", "update_devsrv", "") == "Y")
	header("X-DevSrv-CMS: Bitrix");

if (!defined("BX_CRONTAB_SUPPORT"))
{
	define("BX_CRONTAB_SUPPORT", defined("BX_CRONTAB"));
}

//agents
if(COption::GetOptionString("main", "check_agents", "Y") == "Y")
{
	$application->addBackgroundJob(["CAgent", "CheckAgents"], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW);
}

//send email events
if(COption::GetOptionString("main", "check_events", "Y") !== "N")
{
	$application->addBackgroundJob(['\Bitrix\Main\Mail\EventManager', 'checkEvents'], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW-1);
}

$healerOfEarlySessionStart = new HealerEarlySessionStart();
$healerOfEarlySessionStart->process($application->getKernelSession());

$kernelSession = $application->getKernelSession();
$kernelSession->start();
$application->getSessionLocalStorageManager()->setUniqueId($kernelSession->getId());

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if(
	(
		//IP address changed
		$kernelSession['SESS_IP']
		&& $arPolicy["SESSION_IP_MASK"] <> ''
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($kernelSession['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $kernelSession['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $kernelSession['SESS_TIME']
	)
	||
	(
		//signed session
		isset($kernelSession["BX_SESSION_SIGN"])
		&& $kernelSession["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$compositeSessionManager = $application->getCompositeSessionManager();
	$compositeSessionManager->destroy();

	$application->getSession()->setId(Main\Security\Random::getString(32));
	$compositeSessionManager->start();

	$GLOBALS["USER"] = new CUser;
}
$kernelSession['SESS_IP'] = $_SERVER['REMOTE_ADDR'];
if (empty($kernelSession['SESS_TIME']))
{
	$kernelSession['SESS_TIME'] = $currTime;
}
elseif (($currTime - $kernelSession['SESS_TIME']) > 60)
{
	$kernelSession['SESS_TIME'] = $currTime;
}
if(!isset($kernelSession["BX_SESSION_SIGN"]))
{
	$kernelSession["BX_SESSION_SIGN"] = bitrix_sess_sign();
}

//session control from security module
if(
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if(!isset($kernelSession['SESS_ID_TIME']))
	{
		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
	elseif(($kernelSession['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $kernelSession['SESS_TIME'])
	{
		$compositeSessionManager = $application->getCompositeSessionManager();
		$compositeSessionManager->regenerateId();

		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
}

define("BX_STARTED", true);

if (isset($kernelSession['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($kernelSession['BX_ADMIN_LOAD_AUTH']);
}

$bRsaError = false;
$USER_LID = false;

if(!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$doLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if($doLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$secureLogout = (\Bitrix\Main\Config\Option::get("main", "secure_logout", "N") == "Y");

		if(!$secureLogout || check_bitrix_sessid())
		{
			$GLOBALS["USER"]->Logout();
			LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout', 'sessid')));
		}
	}

	// authorize by cookies
	if(!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if(($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	//Only POST is accepted
	if(isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] <> '')
	{
		if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if(($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(['USER_PASSWORD', 'USER_CONFIRM_PASSWORD', 'USER_CURRENT_PASSWORD']);
				if($errno == CRsaSecurity::ERROR_SESS_CHECK)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				elseif($errno < 0)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");

				if($errno < 0)
					$bRsaError = true;
			}
		}

		if (!$bRsaError)
		{
			if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
			{
				$USER_LID = SITE_ID;
			}

			if($_POST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login($_POST["USER_LOGIN"], $_POST["USER_PASSWORD"], $_POST["USER_REMEMBER"]);
			}
			elseif($_POST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp($_POST["USER_OTP"], $_POST["OTP_REMEMBER"], $_POST["captcha_word"], $_POST["captcha_sid"]);
			}
			elseif($_POST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword($_POST["USER_LOGIN"], $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], $_POST["USER_PHONE_NUMBER"]);
			}
			elseif($_POST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword($_POST["USER_LOGIN"], $_POST["USER_CHECKWORD"], $_POST["USER_PASSWORD"], $_POST["USER_CONFIRM_PASSWORD"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], true, $_POST["USER_PHONE_NUMBER"], $_POST["USER_CURRENT_PASSWORD"]);
			}

			if($_POST["TYPE"] == "AUTH" || $_POST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$kernelSession['BX_ADMIN_LOAD_AUTH'] = true;

					// die() follows
					CMain::FinalActions('<script type="text/javascript">window.onload=function(){(window.BX || window.parent.BX).AUTHAGENT.setAuthResult(false);};</script>');
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif(!$GLOBALS["USER"]->IsAuthorized() && isset($_REQUEST['bx_hit_hash']))
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash($_REQUEST['bx_hit_hash']);
	}
}

//logout or re-authorize the user if something importand has changed
$GLOBALS["USER"]->CheckAuthActions();

//magic short URI
if(defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

//application password scope control
if(($applicationID = $GLOBALS["USER"]->getContext()->getApplicationId()) !== null)
{
	$appManager = Main\Authentication\ApplicationManager::getInstance();
	if($appManager->checkScope($applicationID) !== true)
	{
		$event = new Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		$context->getResponse()->setStatus("403 Forbidden");
		$application->end();
	}
}

//define the site template
if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if(isset($_REQUEST["bitrix_preview_site_template"]) && is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if(isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}
	define("SITE_TEMPLATE_ID", $siteTemplate);
	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}
else
{
	// prevents undefined constants
	define('SITE_TEMPLATE_ID', '.default');
	define('SITE_TEMPLATE_PATH', '/bitrix/templates/.default');
}

//magic parameters: show page creation time
if(isset($_GET["show_page_exec_time"]))
{
	if($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
		$kernelSession["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
}

//magic parameters: show included file processing time
if(isset($_GET["show_include_exec_time"]))
{
	if($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
		$kernelSession["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
}

//magic parameters: show include areas
if(isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");

//magic sound
if($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if(!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
}

//magic cache
\Bitrix\Main\Composite\Engine::shouldBeEnabled();

// should be before proactive filter on OnBeforeProlog
$userPassword = $_POST["USER_PASSWORD"] ?? null;
$userConfirmPassword = $_POST["USER_CONFIRM_PASSWORD"] ?? null;

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
{
	ExecuteModuleEventEx($arEvent);
}

if (!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS !== true)
{
	//Register user from authorization html form
	//Only POST is accepted
	if (isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] != '' && $_POST["TYPE"] == "REGISTRATION")
	{
		if (!$bRsaError)
		{
			if(COption::GetOptionString("main", "new_user_registration", "N") == "Y" && (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register($_POST["USER_LOGIN"], $_POST["USER_NAME"], $_POST["USER_LAST_NAME"], $userPassword, $userConfirmPassword, $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], false, $_POST["USER_PHONE_NUMBER"]);
				$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
			}
		}
	}
}

if((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $context->getRequest()->getScriptFile();

	if(!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		/** @noinspection PhpUndefinedVariableInspection */
		if($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
		{
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

			if(COption::GetOptionString("main", "event_log_permissions_fail", "N") === "Y")
			{
				CEventLog::Log("SECURITY", "USER_PERMISSIONS_FAIL", "main", $GLOBALS["USER"]->GetID(), $real_path);
			}
		}

		if(defined("ADMIN_SECTION") && ADMIN_SECTION==true)
		{
			if ($_REQUEST["mode"]=="list" || $_REQUEST["mode"]=="settings")
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif ($_REQUEST["mode"]=="frame")
			{
				echo "<script type=\"text/javascript\">
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif(defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN==true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

/*ZDUyZmZMWQxY2QxZmYyZTU2ZGMyYjY5MTFiZWMzZjFjOGJmMTE=*/$GLOBALS['____911673207']= array(base64_decode('b'.'X'.'Rfcm'.'F'.'uZA=='),base64_decode('ZXhwbG9kZ'.'Q=='),base64_decode('c'.'GFjaw='.'='),base64_decode('bW'.'Q1'),base64_decode(''.'Y29u'.'c'.'3RhbnQ='),base64_decode('aGFzaF'.'9ob'.'W'.'Fj'),base64_decode('c'.'3R'.'yY2'.'1w'),base64_decode('a'.'XNfb'.'2'.'Jq'.'ZWN0'),base64_decode(''.'Y2FsbF'.'91c2VyX2Z1bmM='),base64_decode('Y2Fsb'.'F91c2'.'V'.'yX2Z1bmM='),base64_decode('Y2Fsb'.'F91c2VyX2Z1'.'bmM='),base64_decode('Y'.'2Fsb'.'F9'.'1'.'c2V'.'yX2Z1bmM='),base64_decode('Y2Fs'.'b'.'F91c2VyX2Z1'.'bmM='));if(!function_exists(__NAMESPACE__.'\\___1679805027')){function ___1679805027($_294784693){static $_1640169703= false; if($_1640169703 == false) $_1640169703=array(''.'RE'.'I'.'=','U0VMR'.'UNUIFZBTFV'.'FIEZ'.'ST00gYl9v'.'cHR'.'pb24gV0'.'hFUkUgT'.'kFNRT0nflBBUkFN'.'X0'.'1BWF9VU0V'.'S'.'Uy'.'cg'.'QU5EIE'.'1'.'P'.'RFVMRV9'.'JR'.'D0nbWF'.'p'.'b'.'icg'.'QU5'.'EI'.'FN'.'JVEVf'.'SUQgSVMgT'.'l'.'VMTA==','V'.'kFM'.'V'.'UU=','Lg='.'=','SCo=','Yml'.'0'.'cm'.'l4','TE'.'lD'.'RU'.'5TRV'.'9L'.'R'.'V'.'k=','c2hhMj'.'U2',''.'VVN'.'FUg'.'==','VVN'.'FUg==','VV'.'NFUg'.'==','SXNBdXR'.'o'.'b3JpemVk','VV'.'NF'.'Ug='.'=',''.'SX'.'N'.'BZG1pbg==',''.'QVBQTElDQ'.'V'.'RJT04=','UmV'.'zdGFyd'.'E'.'J1ZmZlcg==','TG9jYWxSZWR'.'pcmVjdA==','L2xpY2Vuc2Vf'.'cmVzd'.'HJpY3'.'R'.'pb24'.'uc'.'Ghw','XE'.'J'.'pdHJpeF'.'xNYWluX'.'EN'.'vbmZpZ1x'.'P'.'cHRpb24'.'6OnNldA'.'==',''.'bWFpbg==',''.'UEFSQU1fTUFYX1VT'.'RVJT');return base64_decode($_1640169703[$_294784693]);}};if($GLOBALS['____911673207'][0](round(0+0.2+0.2+0.2+0.2+0.2), round(0+5+5+5+5)) == round(0+7)){ $_90234393= $GLOBALS[___1679805027(0)]->Query(___1679805027(1), true); if($_1110833702= $_90234393->Fetch()){ $_1132516397= $_1110833702[___1679805027(2)]; list($_1755585925, $_1849849834)= $GLOBALS['____911673207'][1](___1679805027(3), $_1132516397); $_1458221994= $GLOBALS['____911673207'][2](___1679805027(4), $_1755585925); $_1471230484= ___1679805027(5).$GLOBALS['____911673207'][3]($GLOBALS['____911673207'][4](___1679805027(6))); $_475462920= $GLOBALS['____911673207'][5](___1679805027(7), $_1849849834, $_1471230484, true); if($GLOBALS['____911673207'][6]($_475462920, $_1458221994) !== min(36,0,12)){ if(isset($GLOBALS[___1679805027(8)]) && $GLOBALS['____911673207'][7]($GLOBALS[___1679805027(9)]) && $GLOBALS['____911673207'][8](array($GLOBALS[___1679805027(10)], ___1679805027(11))) &&!$GLOBALS['____911673207'][9](array($GLOBALS[___1679805027(12)], ___1679805027(13)))){ $GLOBALS['____911673207'][10](array($GLOBALS[___1679805027(14)], ___1679805027(15))); $GLOBALS['____911673207'][11](___1679805027(16), ___1679805027(17), true);}}} else{ $GLOBALS['____911673207'][12](___1679805027(18), ___1679805027(19), ___1679805027(20), round(0+12));}}/**/       //Do not remove this

