<?
$MESS["LEARNING_ATTEMPT_CREATE_ERROR"] = "Wystąpił błąd w trakcie próby utworzenia zadania.";
$MESS["LEARNING_ATTEMPT_FAILED"] = "Test nie zdany";
$MESS["LEARNING_ATTEMPT_NOT_FOUND_ERROR"] = "Nie znaleziono zadania.";
$MESS["LEARNING_COURSES_QUESTION_EDIT"] = "Edytuj pytanie";
$MESS["LEARNING_COURSES_TEST_DELETE"] = "Usuń Test";
$MESS["LEARNING_COURSES_TEST_DELETE_CONF"] = "Spowoduje to usunięcie wszystkich informacji związanych z tym testem! Kontynuować?";
$MESS["LEARNING_COURSES_TEST_EDIT"] = "Edytuj test";
$MESS["LEARNING_LIMIT_ERROR"] = "Nie ma więcej podejść.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_NEW_TEXT_ANSWER"] = "Nowa Odpowiedź Tekstowa";
$MESS["LEARNING_NO_AUTHORIZE"] = "Autoryzacja wymaga podglądu tej strony.";
$MESS["LEARNING_RESPONSE_SAVE_ERROR"] = "Wystąpił błąd w trakcie próby zapisu odpowiedzi.";
$MESS["LEARNING_TEST_DENIED"] = "Nie znaleziono testu lub odmowa dostępu.";
$MESS["LEARNING_TEST_DENIED_PREVIOUS"] = "Musisz przejść pomyślnie test #TEST_LINK#, aby osiągnąć dostęp do tego testu.";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR"] = "Możesz przystąpić do testu jeszcze raz za";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_D"] = "d.";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_H"] = "godz.";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_M"] = "min.";
$MESS["LEARNING_TIME_LIMIT"] = "Czas zdania testu minął.";
?>