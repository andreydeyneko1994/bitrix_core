<?
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Sprawdź możliwe uprawnienia";
$MESS["LEARNING_COURSE_ID"] = "ID Kursu";
$MESS["LEARNING_DESC_NO"] = "Nie";
$MESS["LEARNING_DESC_YES"] = "Tak";
$MESS["LEARNING_GRADEBOOK_TEMPLATE_NAME"] = "URL strony wyników testu";
$MESS["LEARNING_PAGE_WINDOW_NAME"] = "Numer pytania w nawigacji";
$MESS["LEARNING_SHOW_TIME_LIMIT"] = "Pokaż licznik czasu";
$MESS["T_LEARNING_DETAIL_ID"] = "ID testu";
$MESS["T_LEARNING_PAGE_NUMBER_VARIABLE"] = "ID pytania";
?>