<?
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Sprawdź możliwe uprawnienia";
$MESS["LEARNING_COURSES_PER_PAGE"] = "Liczba kursów na stronę";
$MESS["LEARNING_COURSE_URL_NAME"] = "URL strony szczegółów kursu";
$MESS["LEARNING_DESC_ASC"] = "Rosnąco";
$MESS["LEARNING_DESC_DESC"] = "Malejąco";
$MESS["LEARNING_DESC_FACT"] = "Data aktywacji";
$MESS["LEARNING_DESC_FID"] = "ID Kursu";
$MESS["LEARNING_DESC_FNAME"] = "Nazwa";
$MESS["LEARNING_DESC_FSORT"] = "Indeks sortowania";
$MESS["LEARNING_DESC_FTSAMP"] = "Ostatnia aktualizacja";
$MESS["LEARNING_DESC_NO"] = "Nie";
$MESS["LEARNING_DESC_SORTBY"] = "Pole sortowania";
$MESS["LEARNING_DESC_SORTORDER"] = "Porządek sortowania";
$MESS["LEARNING_DESC_YES"] = "Tak";
?>