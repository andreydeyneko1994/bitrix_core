<?
$MESS["COURSES_PER_PAGE_TIP"] = "Maksymalna liczba kursów, które można wyświetlić na stronie. Inne kursy będą dostępne przez linki nawigacji breadcrumb.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony szczegółów kursu.";
$MESS["SET_TITLE_TIP"] = "Zaznacz tę opcję, aby ustawić tytuł strony na <b>Kursy</b>.";
$MESS["SORBY_TIP"] = "Określa pole według którego kursy będą sortowane.";
$MESS["SORORDER_TIP"] = "Określa porządek według którego kursy będą sortowane.";
?>