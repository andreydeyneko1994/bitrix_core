<?
$MESS["LEARNING_BAD_TEST_LIST"] = "Ten kurs nie kończy się certyfikatem.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_TESTS_LIST"] = "Lista testów";
$MESS["LEARNING_TESTS_NAV"] = "Testy";
$MESS["LEARNING_TEST_DENIED_PREVIOUS"] = "Musisz przejść pomyślnie test #TEST_LINK#, aby osiągnąć dostęp do tego testu.";
?>