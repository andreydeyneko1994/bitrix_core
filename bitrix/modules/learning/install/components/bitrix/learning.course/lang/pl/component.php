<?
$MESS["INCORRECT_QUESTION_MESSAGE"] = "Błąd";
$MESS["LEARNING_COURSES_CHAPTER_ADD"] = "Dodaj nowy rozdział";
$MESS["LEARNING_COURSES_CHAPTER_EDIT"] = "Edytuj rozdział";
$MESS["LEARNING_COURSES_COURSE_ADD"] = "Dodaj nowy kurs";
$MESS["LEARNING_COURSES_COURSE_EDIT"] = "Edytuj kurs";
$MESS["LEARNING_COURSES_LESSON_ADD"] = "Dodaj nową lekcję";
$MESS["LEARNING_COURSES_LESSON_EDIT"] = "Edytuj lekcję";
$MESS["LEARNING_COURSES_QUEST_M_ADD"] = "Dodaj nowe pytanie (wielokrotnego wyboru)";
$MESS["LEARNING_COURSES_QUEST_R_ADD"] = "Dodaj nowe pytanie (rodzaj)";
$MESS["LEARNING_COURSES_QUEST_S_ADD"] = "Dodaj nowe pytanie (jednokrotnego wyboru)";
$MESS["LEARNING_COURSES_QUEST_T_ADD"] = "Dodaj nowe pytanie (odpowiedź tekstowa)";
$MESS["LEARNING_COURSES_TEST_ADD"] = "Dodaj nowy test";
$MESS["LEARNING_COURSES_TEST_EDIT"] = "Edytuj test";
$MESS["LEARNING_COURSE_DENIED"] = "Nie znaleziono kursu lub odmowa dostępu.";
$MESS["LEARNING_COURSE_DETAIL"] = "Szczegóły kursu";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_PANEL_CONTROL_PANEL"] = "Panel Sterowania";
$MESS["LEARNING_PANEL_CONTROL_PANEL_ALT"] = "Wykonaj działanie w panelu kontrolnym";
?>