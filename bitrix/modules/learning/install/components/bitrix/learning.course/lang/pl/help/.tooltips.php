<?
$MESS["CACHE_TIME_TIP"] = "W tym miejscu określ okres czasu, w trakcie którego pamięć podręczna jest aktualna.";
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["PAGE_NUMBER_VARIABLE_TIP"] = "Nazwa zmiennej dla ID pytań testowych.";
$MESS["PAGE_WINDOW_TIP"] = "Liczba pytań do wyświetlenia w łańcuchu nawigacji.";
$MESS["SEF_FOLDER_TIP"] = "Określa komponent folderu roboczego.";
$MESS["SEF_MODE_TIP"] = "Zaznaczając opcję umożliwiasz tryb SEF i URL polom konfiguracji.";
$MESS["SEF_URL_TEMPLATES_gradebook_TIP"] = "Ścieżka do strony dzienniczka ocen z kursu.";
$MESS["SEF_URL_TEMPLATES_test_TIP"] = "Ścieżka do strony testowej.";
$MESS["SET_TITLE_TIP"] = "Sprawdzenie tej opcji włącza tytuł strony w zależności od jej zawartości.";
$MESS["SHOW_TIME_LIMIT_TIP"] = "Jeżeli test nakłada ograniczenia czasowe, włączenie tej opcji spowoduje wyświetlenie licznika czasu.";
$MESS["TESTS_PER_PAGE_TIP"] = "Maksymalna liczba testów, które mogą być wyświetlone na stronie. Inne testy będą dostępne przy użyciu nawigacji breadcrumb.";
$MESS["VARIABLE_ALIASES_CHAPTER_ID_TIP"] = "Nazwa zmiennej, dla której ID rozdziału zostanie przekazane.";
$MESS["VARIABLE_ALIASES_COURSE_ID_TIP"] = "Nazwa zmiennej dla której ID kursu zostaną przekazane.";
$MESS["VARIABLE_ALIASES_FOR_TEST_ID_TIP"] = "Nazwa zmiennej, dla której ID testu dzienniczka ocen zostanie przekazane.";
$MESS["VARIABLE_ALIASES_GRADEBOOK_TIP"] = "Nazwa zmiennej, dla której ID dzienniczka ocen zostanie przekazane.";
$MESS["VARIABLE_ALIASES_INDEX_TIP"] = "Nazwa zmiennej dla której ID strony głównej zostanie przekazane.";
$MESS["VARIABLE_ALIASES_LESSON_ID_TIP"] = "Nazwa zmiennej, dla której ID lekcji zostanie przekazane.";
$MESS["VARIABLE_ALIASES_SELF_TEST_ID_TIP"] = "Nazwa zmiennej, dla której ID samodzielnie sprawdzanego testu zostanie przekazane.";
$MESS["VARIABLE_ALIASES_TEST_ID_TIP"] = "Nazwa zmiennej, dla której ID testu zostanie przekazane.";
$MESS["VARIABLE_ALIASES_TEST_LIST_TIP"] = "Nazwa zmiennej, dla którejID listy testu zostanie przekazane.";
$MESS["VARIABLE_ALIASES_TYPE_TIP"] = "Nazwa zmiennej, dla której zebrane ID danych kursu zostanie przekazane.";
?>