<?
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_TRANSCRIPT_ERROR"] = "Błąd! Proszę skontaktować się z Administratorem.";
$MESS["LEARNING_TRANSCRIPT_NOT_FOUND"] = "Nie znaleziono użytkownika.";
$MESS["LEARNING_TRANSCRIPT_PERMISSION_DENIED"] = "Nie masz uprawnień do przeglądania tego podsumowania.";
?>