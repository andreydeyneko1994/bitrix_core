<?
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony szczegółów kursu.";
$MESS["SET_TITLE_TIP"] = "Zaznaczenie tej opcji ustawi tytuł strony na <b>Wyniki testu</b>.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Ścieżka do głównej strony testów.";
$MESS["TEST_ID_VARIABLE_TIP"] = "Nazwa zmiennej, dla której ID testu zostanie przekazane.";
?>