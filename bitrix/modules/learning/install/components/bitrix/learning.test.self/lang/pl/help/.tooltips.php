<?
$MESS["CACHE_TIME_TIP"] = "W tym miejscu określ okres czasu, w trakcie którego pamięć podręczna jest aktualna.";
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["LESSON_ID_TIP"] = "Wyrażenie, które odnosi się do ID lekcji.";
$MESS["SET_TITLE_TIP"] = "Sprawdzenie tej opcji wstawi tytuł strony do nazwy lekcji.";
?>