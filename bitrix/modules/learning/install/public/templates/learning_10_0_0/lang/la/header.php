<?
$MESS["LEARNING_ALL_COURSE_CONTENTS"] = "Todos el contenido del curso";
$MESS["LEARNING_ALL_LESSONS"] = "Total de lecciones ";
$MESS["LEARNING_BACK"] = "Volver";
$MESS["LEARNING_COLLAPSE"] = "Ocultar el capítulo";
$MESS["LEARNING_COURSE_DESCRIPTION"] = "Descripción del curso";
$MESS["LEARNING_CURRENT_LESSON"] = "Lecciones completadas";
$MESS["LEARNING_EXPAND"] = "Expandir el capítulo";
$MESS["LEARNING_FORWARD"] = "Avanzar";
$MESS["LEARNING_GRADEBOOK"] = "Libro de calificaciones";
$MESS["LEARNING_LOGO_TEXT"] = "e-Learning";
$MESS["LEARNING_PASS_TEST"] = "Realizar un test";
$MESS["LEARNING_PRINT_PAGE"] = "Imprimir esta página";
?>