<?
$MESS["LEARNING_ACCESS_D"] = "Denegar";
$MESS["LEARNING_ACCESS_D_FOR_EDIT_CONTENT"] = "Usted no tiene permisos para editar el contenido.";
$MESS["LEARNING_ACCESS_D_FOR_MANAGE_RIGHTS"] = "Usted no tiene permisos para administrar otros permisos.";
$MESS["LEARNING_ACCESS_R"] = "Leer";
$MESS["LEARNING_ACCESS_W"] = "Modificar";
$MESS["LEARNING_ACCESS_X"] = "Acceso completo";
$MESS["LEARNING_ACTIVE"] = "Registro activo";
$MESS["LEARNING_ACTIVE_PERIOD"] = "Periodo activo";
$MESS["LEARNING_ADD"] = "Agregar";
$MESS["LEARNING_ADD_ALT"] = "Agregar";
$MESS["LEARNING_ADD_COURSE"] = "Agregar curso";
$MESS["LEARNING_ADD_COURSE_ALT"] = "Agregar nuevo curso";
$MESS["LEARNING_ADD_ELEMENT"] = "Agregar elementos";
$MESS["LEARNING_ALL"] = "(cualquiera)";
$MESS["LEARNING_ALL2"] = "(cualquiera)";
$MESS["LEARNING_ALL_CHAPTERS"] = "Todos los capítulos";
$MESS["LEARNING_ALL_LESSONS"] = "Todas la lecciones";
$MESS["LEARNING_ANSWER"] = "Respuesta";
$MESS["LEARNING_ANSWERS"] = "Respuestas";
$MESS["LEARNING_ANSWER_CORRECT"] = "Respuesta correcta";
$MESS["LEARNING_APPLY"] = "Aplicar";
$MESS["LEARNING_ATTEMPT_STATUS_B"] = "Comenzado ";
$MESS["LEARNING_ATTEMPT_STATUS_D"] = "Retrasado";
$MESS["LEARNING_ATTEMPT_STATUS_F"] = "Terminado";
$MESS["LEARNING_ATTEMPT_STATUS_N"] = "Nuevo";
$MESS["LEARNING_AUTHOR"] = "Creado por";
$MESS["LEARNING_A_UP"] = "Un nivel más arriba";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Regresar a la lista de cursos";
$MESS["LEARNING_BAD_ACTIVE_FROM"] = "Formato de fecha de incio incorrecto.";
$MESS["LEARNING_BAD_ACTIVE_TO"] = "Formato de fecha de expiración incorrecta.";
$MESS["LEARNING_BAD_ATTEMPT_ID"] = "El ID de la alternativa no se especifica.";
$MESS["LEARNING_BAD_ATTEMPT_ID_EX"] = "Alternativa no encontrada o no se puede acceder.";
$MESS["LEARNING_BAD_BLOCK_SECTION_ID_PARENT"] = "El identificador del curso no coincide con el capítulo superior!";
$MESS["LEARNING_BAD_BLOCK_SECTION_PARENT"] = "¡Capítulo superior incorrecto! ";
$MESS["LEARNING_BAD_CERTIFICATE_DUPLICATE"] = "El certificado ya existe.";
$MESS["LEARNING_BAD_COMPLETED_SCORE"] = "El porcentaje del escore debe tener un rango de 1 a 100.";
$MESS["LEARNING_BAD_COURSE"] = "Curso no encontrado o acceso denegado.";
$MESS["LEARNING_BAD_COURSE_ID"] = "Código incorrecto del curso.";
$MESS["LEARNING_BAD_COURSE_ID_EX"] = "ID incorrecto del curso";
$MESS["LEARNING_BAD_DATE_CREATE"] = "Fecha de creación incorrecta.";
$MESS["LEARNING_BAD_DATE_END"] = "El formato de la fecha final de la prueba es incorrecta.";
$MESS["LEARNING_BAD_DATE_ENROLL"] = "La fecha de la inscripción es incorrecta.";
$MESS["LEARNING_BAD_DATE_START"] = "La fecha de inicio de la prueba no se especifica.";
$MESS["LEARNING_BAD_FILENAME"] = "El nombre del archivo tiene caracteres no válidos que se han eliminado.";
$MESS["LEARNING_BAD_GRADEBOOK_DUPLICATE"] = "Registro ya existe en el libro de calificaciones.";
$MESS["LEARNING_BAD_LESSON"] = "Lección no encontrada o acceso denegado.";
$MESS["LEARNING_BAD_LESSON_ID"] = "El código de la lección no coincide.";
$MESS["LEARNING_BAD_LESSON_ID_EX"] = "Código incorrecto de la lección.";
$MESS["LEARNING_BAD_MARK"] = "No se ha marcado.";
$MESS["LEARNING_BAD_MARK_SCORE"] = "El porcentaje de respuestas correctas debe estar en el rango de 1 a 100.";
$MESS["LEARNING_BAD_NAME"] = "Nombre no especificado.";
$MESS["LEARNING_BAD_NO_ANSWERS"] = "Respuesta vacía.";
$MESS["LEARNING_BAD_PACKAGE"] = "Ruta incorrecta del directorio";
$MESS["LEARNING_BAD_PREVIOUS_TEST"] = "ID de la prueba es incorrecto";
$MESS["LEARNING_BAD_QUESTION_ID"] = "El código de la pregunta no coincide.";
$MESS["LEARNING_BAD_QUESTION_TYPE"] = "Código incorrecto de la pregunta.";
$MESS["LEARNING_BAD_SITE_ID"] = "Sitio web no especificado.";
$MESS["LEARNING_BAD_SITE_ID_EX"] = "Sitio web incorrecto.";
$MESS["LEARNING_BAD_TEST_ID"] = "El ID de la prueba no se especifica.";
$MESS["LEARNING_BAD_TEST_ID_EX"] = "La prueba no se ha encontrado o no se puede acceder.";
$MESS["LEARNING_BAD_TEST_IS_EMPTY"] = "Prueba vacía.";
$MESS["LEARNING_BAD_USER_ID"] = "El ID del usuario no está especificado.";
$MESS["LEARNING_BAD_USER_ID_EX"] = "El ID del usuario es incorrecto.";
$MESS["LEARNING_BAD_USER_ID_EXISTS"] = "El ID de usuario ya existe.";
$MESS["LEARNING_CHANGE"] = "Modificar";
$MESS["LEARNING_CHANGE_ALT"] = "Edtar configuración";
$MESS["LEARNING_CHANGE_USER_PROFILE"] = "Editar perfil de usuario";
$MESS["LEARNING_CHAPTERS"] = "Capítulos";
$MESS["LEARNING_CHAPTER_ADD"] = "Nuevo capítulo";
$MESS["LEARNING_CODE"] = "Código mnemotécnico";
$MESS["LEARNING_CONFIRM_DEL_LESSON_WITH_PARENT_PATHES"] = "El artículo que usted está a punto de eliminar forma parte de uno o más (#CNT#) elementos. El artículo será borrado de todas partes. Desea continuar?";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Toda información relacionada con este registro será eliminada! Desea continuar?";
$MESS["LEARNING_CONTENT"] = "Nivel superior ";
$MESS["LEARNING_CONTENT_SOURCE"] = "Fuente del contenido de la lección";
$MESS["LEARNING_CONTENT_SOURCE_FIELD"] = "campo del texto";
$MESS["LEARNING_CONTENT_SOURCE_FILE"] = "archivo";
$MESS["LEARNING_COURSES"] = "Cursos";
$MESS["LEARNING_COURSE_ADM_ACT"] = "Activo";
$MESS["LEARNING_COURSE_ADM_ACTIONS"] = "Acciones";
$MESS["LEARNING_COURSE_ADM_CREATED"] = "Por";
$MESS["LEARNING_COURSE_ADM_CREATED2"] = "Creado por";
$MESS["LEARNING_COURSE_ADM_DATECH"] = "Modificado";
$MESS["LEARNING_COURSE_ADM_DCREATE"] = "Crear";
$MESS["LEARNING_COURSE_ADM_DELETE"] = "Del.";
$MESS["LEARNING_COURSE_ADM_FILT_ACT"] = "Activo:";
$MESS["LEARNING_COURSE_ADM_LISTEMPTY"] = "Lista vacía";
$MESS["LEARNING_COURSE_ADM_LISTTOTAL"] = "Listado total:";
$MESS["LEARNING_COURSE_ADM_QUESTIONS_COUNT"] = "Preguntas";
$MESS["LEARNING_COURSE_ADM_SORT"] = "Clasificar";
$MESS["LEARNING_COURSE_CONTENT"] = "Contenidos";
$MESS["LEARNING_COURSE_DELETE_ERROR"] = "Error al eliminar.";
$MESS["LEARNING_COURSE_UNREMOVABLE_CAUSE_OF_CERTIFICATES"] = "El curso no se puede eliminar porque hay certificados emitidos en él.";
$MESS["LEARNING_DATA_IN_DB_NEEDS_TO_BE_CONVERTED"] = "Usted ha actualizado el módulo de e-Learning. Por favor <a href=\"/bitrix/admin/learning_convert.php?lang=#LANG#\">convert the module data</a> now.";
$MESS["LEARNING_DAYS"] = "d.";
$MESS["LEARNING_DELETE"] = "Eliminar";
$MESS["LEARNING_DELETE_ALT"] = "Eliminar registro";
$MESS["LEARNING_DELETE_ERROR"] = "Error al eliminar.";
$MESS["LEARNING_DESCRIPTION"] = "Descripción";
$MESS["LEARNING_DESC_TYPE"] = "Tipo de descripción";
$MESS["LEARNING_DESC_TYPE_FILE"] = "Archivo";
$MESS["LEARNING_DESC_TYPE_HTML"] = "Html";
$MESS["LEARNING_DESC_TYPE_TEXT"] = "Texto";
$MESS["LEARNING_EDIT_PARAM_SECTION"] = "Parámetros";
$MESS["LEARNING_EDIT_TITLE"] = "Editar";
$MESS["LEARNING_ELEMENT_DETAIL"] = "Descripción completa";
$MESS["LEARNING_ELEMENT_PREVIEW"] = "Pequeña descripción";
$MESS["LEARNING_ERROR"] = "Error!";
$MESS["LEARNING_F_ACTIVE"] = "Activo";
$MESS["LEARNING_F_ACTIVE2"] = "Actividad";
$MESS["LEARNING_F_CHAPTER"] = "Capítulo";
$MESS["LEARNING_F_DEL"] = "Remover filtro";
$MESS["LEARNING_F_SELF"] = "Auto examen";
$MESS["LEARNING_F_SUBMIT"] = "Fijar filtro";
$MESS["LEARNING_HOURS"] = "h.";
$MESS["LEARNING_KEYWORDS"] = "Palabras claves";
$MESS["LEARNING_LAST_UPDATE"] = "Última actualización";
$MESS["LEARNING_LESSONS"] = "Lecciones";
$MESS["LEARNING_LESSON_ADD"] = "Nueva lección";
$MESS["LEARNING_LESSON_IS_PUBLISH_PROHIBITED"] = "no publicables";
$MESS["LEARNING_LESSON_LIST"] = "Lista de cursos";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_CHAPTER"] = "Este capítulo forma parte de:";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_CHAPTER_IS_EMPTY"] = "Este capítulo no es parte de ninguna otras lecciones o capítulos.";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_COURSE"] = "Este curso forma parte de:";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_COURSE_IS_EMPTY"] = "Este curso no es parte de otros cursos.";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_LESSON"] = "Esta lección es parte de:";
$MESS["LEARNING_LIST_OF_ALL_PARENT_PATHES_FOR_LESSON_IS_EMPTY"] = "Esta lección no es parte de ninguna otras lecciones o cursos.";
$MESS["LEARNING_LIST_OF_DESCENDANTS"] = "Lecciones anidadas inmediatas";
$MESS["LEARNING_LIST_OF_PARENTS"] = "Lecciones Superiores Inmediatas";
$MESS["LEARNING_MANIFEST_NOT_FOUND"] = "El archivo imsmanifest.xml no se encontró";
$MESS["LEARNING_MINUTES"] = "m.";
$MESS["LEARNING_MULTIPLE_CHOICE"] = "Múltiples opciones";
$MESS["LEARNING_NAME"] = "Nombre";
$MESS["LEARNING_NEW_TITLE"] = "Nuevo";
$MESS["LEARNING_NO"] = "No";
$MESS["LEARNING_NO_AUTHORIZE"] = "Requiere autorización para ver esta página.";
$MESS["LEARNING_PARENT_CHAPTER_ID"] = "Capítulo superior";
$MESS["LEARNING_PATH_TO_FILE"] = "Ruta al archivo";
$MESS["LEARNING_PERMISSIONS"] = "Permisos de Acceso";
$MESS["LEARNING_PICTURE"] = "Imagen";
$MESS["LEARNING_POINT"] = "Marca";
$MESS["LEARNING_PREVENT_LANG_REMOVE"] = "Hay cursos vinculados con el sitio web que está a punto de eliminar. Por favor, eliminarlos o enlazarlos a otro sitio web primero.";
$MESS["LEARNING_QUESTION"] = "Preguntas";
$MESS["LEARNING_QUESTION_ADD"] = "Nueva pregunta";
$MESS["LEARNING_QUESTION_ADM_CORRECT"] = "Corr.";
$MESS["LEARNING_QUESTION_ADM_SELF"] = "Uno mismo.";
$MESS["LEARNING_QUESTION_ADM_TYPE"] = "Tipo";
$MESS["LEARNING_QUESTION_ALT"] = "Lista de preguntas";
$MESS["LEARNING_QUESTION_TYPE"] = "Tipo de preguntas";
$MESS["LEARNING_RESET"] = "Resetear";
$MESS["LEARNING_RIGHTS_ADD"] = "Agregar";
$MESS["LEARNING_RIGHTS_FOR_ADMINISTRATION"] = "Permisos de administración ";
$MESS["LEARNING_RIGHTS_NOTE"] = "* Los visitantes del Sitio Web son controlados por permisos únicos. Si un usuario tiene un permiso de visualización para un curso, él o ella tendrá acceso a todos los capítulos y lecciones en este curso independientemente de sus permisos.";
$MESS["LEARNING_SAVE"] = "Guardar";
$MESS["LEARNING_SAVE_ERROR"] = "Error al intentar cambiar o llamar un elemento.";
$MESS["LEARNING_SECONDS"] = "s.";
$MESS["LEARNING_SELECT"] = "Seleccionar";
$MESS["LEARNING_SINGLE_CHOICE"] = "Unica opción";
$MESS["LEARNING_SITE_ID"] = "Sitios web";
$MESS["LEARNING_SITE_ID2"] = "Sitio web";
$MESS["LEARNING_SORT"] = "Índice de Ordamiento";
$MESS["LEARNING_SORTING"] = "Clasificación";
$MESS["LEARNING_TEST"] = "Prueba";
$MESS["LEARNING_TESTS"] = "Pruebas";
$MESS["LEARNING_TESTS_LIST"] = "Lista de pruebas";
$MESS["LEARNING_TEXT_ANSWER"] = "Texto de la respuesta";
$MESS["LEARNING_TRAVERSE"] = "Examinar los elementos anidados";
$MESS["LEARNING_UNILESSON_ADD"] = "Nuevo capítulo/lección";
$MESS["LEARNING_YES"] = "Si";
?>