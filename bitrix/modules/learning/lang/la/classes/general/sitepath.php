<?
$MESS["LRN_GSP_EMPTY_PATH"] = "La ruta al curso no está especificada";
$MESS["LRN_GSP_EMPTY_SITE_ID"] = "El sitio web no está especificado";
$MESS["LRN_GSP_EMPTY_TYPE"] = "El tipo de ruta no está especificada";
$MESS["LRN_GSP_ERROR_NO_SITE"] = "El sitio web con el ID &quot;#ID#&quot; no fue encontrado";
?>