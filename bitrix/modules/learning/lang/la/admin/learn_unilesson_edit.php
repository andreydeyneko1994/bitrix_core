<?
$MESS["LEARNING_ADMIN_TAB1"] = "Lección";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Configuración de la lección";
$MESS["LEARNING_ADMIN_TAB2"] = "Anuncio";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Breve descripción";
$MESS["LEARNING_ADMIN_TAB3"] = "Descripción";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Descripción de lección";
$MESS["LEARNING_ADMIN_TAB4"] = "Dependencias";
$MESS["LEARNING_ADMIN_TAB4_EX"] = "Dependencias de la lección";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "¿Está seguro que desea eliminar esta lección?";
$MESS["LEARNING_CONFIRM_UNLINK_LESSON_FROM_PARENT"] = "¿Está seguro que desea desvincular esta lección del principal?";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED"] = "No publicar";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED_CONTEXT"] = "cuando se ve como parte de#COURSE_NAME#";
$MESS["LEARNING_EDIT_FORM_WILL_BE_AVAILABLE_AFTER_LESSON_CREATION"] = "Esta ficha sólo está disponible para los capítulos y lecciones existentes.";
$MESS["LEARNING_ERROR"] = "Error al guardar la lección";
$MESS["LEARNING_HEIGHT"] = "Altura";
$MESS["LEARNING_PATH_TO_FILE"] = "Ruta de acceso al archivo";
$MESS["LEARNING_PREVIEW_TEXT"] = "Vista previa";
$MESS["LEARNING_SAVE"] = "Guardar";
$MESS["LEARNING_UNLINK_LESSON_FROM_PARENT"] = "Desvinculación del principal";
$MESS["LEARNING_VIDEO_AUDIO"] = "audio/video";
$MESS["LEARNING_WIDTH"] = "Ancho";
?>