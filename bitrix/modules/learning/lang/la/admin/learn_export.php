<?
$MESS["LEARNING_2_1_STEP"] = "Regresar al primer paso";
$MESS["LEARNING_ADMIN_TAB1"] = "Curso";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Seleccione un curso para exportar";
$MESS["LEARNING_ADMIN_TAB2"] = "Configuración de exportación";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Editar parámetros de exportación";
$MESS["LEARNING_ADMIN_TAB3"] = "Resutlados";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Exportar resultados";
$MESS["LEARNING_BACK"] = "Atrás";
$MESS["LEARNING_DATA_FILE_NAME"] = "Guardar archivos de datos como...";
$MESS["LEARNING_DATA_FILE_NAME1"] = "Nombre del archivo de datos";
$MESS["LEARNING_DATA_FILE_NAME1_DESC"] = "Si un archivo existe, este será sobreescrito";
$MESS["LEARNING_DATA_FILE_NAME1_T"] = "(relativo a la raíz del sitio web)";
$MESS["LEARNING_NEXT_STEP"] = "Siguiente";
$MESS["LEARNING_NEXT_STEP_F"] = "Inicio";
$MESS["LEARNING_NO_DATA_FILE"] = "Seleccione el archivo de salida por favor.";
$MESS["LEARNING_PAGE_TITLE"] = "Exportar curso: paso";
$MESS["LEARNING_SUCCESS"] = "Exportación completa";
$MESS["LEARNING_SU_ALL"] = "Total de lineas exportadas:";
$MESS["LEARNING_SU_ALL1"] = "Descargar archivo %DATA_URL%";
?>