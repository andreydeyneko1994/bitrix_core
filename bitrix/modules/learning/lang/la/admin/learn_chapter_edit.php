<?
$MESS["LEARNING_ADMIN_TAB1"] = "Capítulo ";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Configuración de capítulo";
$MESS["LEARNING_ADMIN_TAB2"] = "Aviso";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Descripción del brief";
$MESS["LEARNING_ADMIN_TAB3"] = "Decripción";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Descripción completa";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "¿Está seguro que desea eliminar este capítulo?";
$MESS["LEARNING_ERROR"] = "Error guardando capítulo";
$MESS["LEARNING_PREVIEW_TEXT"] = "Anterior";
?>