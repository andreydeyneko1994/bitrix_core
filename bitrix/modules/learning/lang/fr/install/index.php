<?
$MESS["COPY_PUBLIC_FILES"] = "Copier les fichiers réservés au public et le modèle du site";
$MESS["LEARNING_ADMIN_NOTIFY_CONVERT_DB"] = "Vous avez mise à jour le module 'Éducation', pour que ce dernier fonctionne correctement, vous devez faire <a href='/bitrix/admin/learning_convert.php?lang=#LANG#'>la conversion des données</a>.";
$MESS["LEARNING_INSTALL_BACK"] = "Revenir à la commande des modules";
$MESS["LEARNING_INSTALL_COMPLETE_ERROR"] = "L'installation est terminée";
$MESS["LEARNING_INSTALL_COMPLETE_OK"] = "L'installation est terminée.";
$MESS["LEARNING_INSTALL_COPY_PUBLIC"] = "Copier les scripts de la section publique";
$MESS["LEARNING_INSTALL_PUBLIC_SETUP"] = "Installation";
$MESS["LEARNING_INSTALL_TEMPLATE_NAME"] = "Identifiant du modèle";
$MESS["LEARNING_INSTALL_TITLE"] = "installation du module e-Education";
$MESS["LEARNING_MODULE_DESC"] = "e-Education système";
$MESS["LEARNING_MODULE_NAME"] = "apprentissage";
$MESS["LEARNING_PERM_ADMIN_D"] = "pas d'accès aux paramètres";
$MESS["LEARNING_PERM_ADMIN_W"] = "accès complet aux réglages";
$MESS["LEARNING_PERM_D"] = "lecture de cours";
$MESS["LEARNING_PERM_W"] = "accès complet";
$MESS["LEARNING_UNINSTALL_COMPLETE"] = "La suppression est terminée";
$MESS["LEARNING_UNINSTALL_DEL"] = "Supprimer";
$MESS["LEARNING_UNINSTALL_ERROR"] = "Erreurs lors de la suppression : ";
$MESS["LEARNING_UNINSTALL_SAVEDATA"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Enregistrer les tables&quot;.";
$MESS["LEARNING_UNINSTALL_SAVETABLE"] = "Enregistrer les tables";
$MESS["LEARNING_UNINSTALL_TITLE"] = "le module e-Education désinstallation";
$MESS["LEARNING_UNINSTALL_WARNING"] = "Attention ! Le module sera supprimé du système.";
?>