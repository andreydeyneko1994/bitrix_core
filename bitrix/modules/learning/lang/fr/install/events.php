<?
$MESS["NEW_LEARNING_TEXT_ANSWER_DESC"] = "#ID# - ID résultat
#ATTEMPT_ID# - ID essai
#TEST_NAME# - Nom du test
#USER# - Utilisateur ayant répondu à la question
#DATE# - Date et heure
#QUESTION_TEXT# - Question
#ANSWER_TEXT# - Réponse
#EMAIL_FROM# - Adresse emailde l'expéditeur du message
#EMAIL_TO# - Email du destinataire de message
#MESSAGE_TITLE# - Intitulé du message";
$MESS["NEW_LEARNING_TEXT_ANSWER_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Cours:#COURSE_NAME#
Test:#TEST_NAME#

Utilisateur : #USER#
Date : #DATE#

Question:
------------------------------------------
#QUESTION_TEXT#
------------------------------------------

Réponse:
------------------------------------------
#ANSWER_TEXT#
------------------------------------------

Pour afficher et éditer la réponse utilisez ce lien:
http://#SERVER_NAME#/bitrix/admin/learn_test_result_edit.php?lang=fr&ID=#ID#&ATTEMPT_ID=#ATTEMPT_ID#

Ce message a été généré automatiquement.";
$MESS["NEW_LEARNING_TEXT_ANSWER_NAME"] = "Nouvelle réponse textuelle";
$MESS["NEW_LEARNING_TEXT_ANSWER_SUBJECT"] = "#SITE_NAME# : #COURSE_NAME# : #MESSAGE_TITLE#";
?>