<?
$MESS["LEARNING_ADMIN_ANSWER"] = "Répondre";
$MESS["LEARNING_ADMIN_ANSWERED"] = "Répondu";
$MESS["LEARNING_ADMIN_CORRECT"] = "La réponse est correcte";
$MESS["LEARNING_ADMIN_EDIT_QUESTION"] = "Modifier la question";
$MESS["LEARNING_ADMIN_POINT"] = "Evaluation";
$MESS["LEARNING_ADMIN_QUESTION"] = "Question";
$MESS["LEARNING_ADMIN_RESULTS"] = "Résultats";
$MESS["LEARNING_ADMIN_STUDENT"] = "tudiant";
$MESS["LEARNING_ADMIN_TAB1"] = "Répondre";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "La réponse à la question de l'étudiant";
$MESS["LEARNING_ADMIN_TITLE"] = "Modifier résultat";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "tes-vous sûr de vouloir supprimer cette question ?";
$MESS["LEARNING_ERROR"] = "Enregistrer le résultat Erreur";
?>