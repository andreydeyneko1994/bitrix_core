<?
$MESS["LEARNING_ADD"] = "Ajouter un cours";
$MESS["LEARNING_ADD_ALT"] = "Ajouter un nouveau cours";
$MESS["LEARNING_ADMIN_TITLE"] = "Cours";
$MESS["LEARNING_CHAPTER_TITLE"] = "Chapitres";
$MESS["LEARNING_ERROR"] = "Erreur bien sûr d'économiser";
$MESS["LEARNING_LESSON_TITLE"] = "Liste de leçons";
$MESS["LEARNING_TEST_ADD"] = "Créer un nouveau test";
$MESS["LEARNING_TEST_TITLE"] = "Liste des tests";
$MESS["SAVE_ERROR"] = "Impossible de changer le cours #";
?>