<?
$MESS["LEARNING_ACTION_DESELF"] = "Les messages cachés ne sont pas trouvés.";
$MESS["LEARNING_ACTION_SELF"] = "pour les tests de l'auto";
$MESS["LEARNING_ADD"] = "Ajouter une question";
$MESS["LEARNING_ADD_ALT"] = "Ajouter une question";
$MESS["LEARNING_ADMIN_TITLE"] = "Nombre de questions";
$MESS["LEARNING_CHAPTER"] = "Chapitre";
$MESS["LEARNING_ERROR"] = "Erreur question d'économie";
$MESS["LEARNING_F_CORRECT_REQUIRED"] = "Réponse correcte exigée";
$MESS["LEARNING_LESSON"] = "Leçon";
$MESS["LEARNING_QUESTION_ADM_POINT"] = "Evaluation";
$MESS["LEARNING_QUESTION_ADM_REQUIRED"] = "Corriger <br />réponse<br />requise";
$MESS["LEARNING_QUESTION_ADM_STATS"] = "Nombre de réponses (correctes / total)";
$MESS["LEARNING_QUESTION_OF_TEXT_TYPE_IGNORED"] = "La question du type texte n'est pas prévue pour l'autocontrôle, donc ignorée.";
$MESS["LEARNING_QUESTION_TYPE_M"] = "Choix pluriel";
$MESS["LEARNING_QUESTION_TYPE_R"] = "Trier";
$MESS["LEARNING_QUESTION_TYPE_S"] = "Sélection isolée";
$MESS["LEARNING_QUESTION_TYPE_T"] = "Réponse par texte";
$MESS["MAIN_ADMIN_LIST_NOT_REQUIRED"] = "la réponse correcte non obligatoire";
$MESS["MAIN_ADMIN_LIST_REQUIRED"] = "une réponse correcte est de rigueur";
$MESS["SAVE_ERROR"] = "Erreur mise à jour la question #";
?>