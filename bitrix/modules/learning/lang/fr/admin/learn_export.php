<?
$MESS["LEARNING_2_1_STEP"] = "Retour à la première étape";
$MESS["LEARNING_ADMIN_TAB1"] = "Taux";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Sélectionnez sûr pour l'exportation";
$MESS["LEARNING_ADMIN_TAB2"] = "Options d'exportation";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Réglage des paramètres de l'exportation";
$MESS["LEARNING_ADMIN_TAB3"] = "Résultat";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Résultat de l'exportation";
$MESS["LEARNING_BACK"] = "Précédent";
$MESS["LEARNING_DATA_FILE_NAME"] = "Enregistrer le fichier de données sous...";
$MESS["LEARNING_DATA_FILE_NAME1"] = "Nom de fichier de données";
$MESS["LEARNING_DATA_FILE_NAME1_DESC"] = "Si ce fichier existe déjà, il sera remplacé";
$MESS["LEARNING_DATA_FILE_NAME1_T"] = "(relativement à la racine du site)";
$MESS["LEARNING_NEXT_STEP"] = "Suivant";
$MESS["LEARNING_NEXT_STEP_F"] = "Commencer";
$MESS["LEARNING_NO_DATA_FILE"] = "Déterminez le fichier pour enregistrer le résultat.";
$MESS["LEARNING_PAGE_TITLE"] = "L'exportation du cours : étape";
$MESS["LEARNING_SUCCESS"] = "Le déchargement est terminé";
$MESS["LEARNING_SU_ALL"] = "Nombre de lignes exportées : ";
$MESS["LEARNING_SU_ALL1"] = "Télécharger le fichier %DATA_URL%";
?>