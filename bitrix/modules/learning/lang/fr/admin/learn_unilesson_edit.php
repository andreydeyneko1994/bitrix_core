<?
$MESS["LEARNING_ADMIN_TAB1"] = "Leçon";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Paramètres de la leçon";
$MESS["LEARNING_ADMIN_TAB2"] = "Annonce";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Description courte";
$MESS["LEARNING_ADMIN_TAB3"] = "Description";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Contenu de la leçon";
$MESS["LEARNING_ADMIN_TAB4"] = "Liens";
$MESS["LEARNING_ADMIN_TAB4_EX"] = "Leçon de relations";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "tes-vous sûr d'éliminer cette leçon ?";
$MESS["LEARNING_CONFIRM_UNLINK_LESSON_FROM_PARENT"] = "tes-vous sûr de vouloir détacher cette leçon de celle de parents ?";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED"] = "Ne pas publier";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED_CONTEXT"] = "dans le cadre du cours : #COURSE_NAME#";
$MESS["LEARNING_EDIT_FORM_WILL_BE_AVAILABLE_AFTER_LESSON_CREATION"] = "Cet onglet est disponible uniquement pour les leçons/chapitres existants.";
$MESS["LEARNING_ERROR"] = "Erreur de sauvegarde de la leçon";
$MESS["LEARNING_HEIGHT"] = "Hauteur";
$MESS["LEARNING_PATH_TO_FILE"] = "Chemin vers le fichier";
$MESS["LEARNING_PREVIEW_TEXT"] = "Affichage";
$MESS["LEARNING_SAVE"] = "Enregistrer";
$MESS["LEARNING_UNLINK_LESSON_FROM_PARENT"] = "Décrocher de parents";
$MESS["LEARNING_VIDEO_AUDIO"] = "Vidéo/audio";
$MESS["LEARNING_WIDTH"] = "Largeur";
?>