<?
$MESS["TASK_BINDING_LESSON"] = "Leçon";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_DENIED"] = "Accès interdit";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_ANY"] = "Attachement/détachement des leçons et des cours, tant en qualité de parent qu'en qualité de descendant";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_AS_CHILD"] = "Attacher/détacher les leçons et les cours comme un descendant";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_AS_PARENT"] = "Rattachement / détachement des leçons et des cours en tant que parent";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_AS_CHILD"] = "Modifier, supprimer et créer des leçons et des cours; attacher et détacher des leçons de descendants et des cours";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_AS_PARENT"] = "Modifier, supprimer et créer des leçons et des cours; attacher et détacher les leçons des parents et des cours";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_BASIC"] = "Modifier, supprimer et créer des leçons et des cours";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_DUAL"] = "Modification / suppression / création / rattachement / détachement des leçons et des cours à la fois en tant que parent et en tant qu'enfant";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_FULL"] = "Gérer types de menus";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_READ"] = "Examen des données de la leçon ou du cours (y compris les tests)";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_DENIED"] = "Accès interdit";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_ANY"] = "Attachement/détachement";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_AS_CHILD"] = "Attacher / détacher comme un descendant";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_AS_PARENT"] = "Attachement/détachement en qualité de parent";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_AS_CHILD"] = "Modifier / supprimer / créer; accrocher / détacher comme un nud descendant";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_AS_PARENT"] = "Edition / élimination / création; rattachement / détachement comme parent";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_BASIC"] = "Modification / suppression / création";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_DUAL"] = "Modification / suppression / création / rattachement/détachement";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_FULL"] = "La gestion des droits; / suppression / création/ rattachement/ détachement";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_READ"] = "Affichage";
?>