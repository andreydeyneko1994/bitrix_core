<?
$MESS["LEARNING_CONVERT_COMPLETE"] = "La conversion de données est terminée.";
$MESS["LEARNING_CONVERT_FAILED"] = "Une erreur s'est produite lors de l'essai de convertir les données du module d'apprentissage.";
$MESS["LEARNING_CONVERT_IN_PROGRESS"] = "La totalité des données n'a pas été convertie";
$MESS["LEARNING_CONVERT_START_BUTTON"] = "Commencer la conversion";
$MESS["LEARNING_CONVERT_STOP_BUTTON"] = "Suspendre la conversion";
$MESS["LEARNING_CONVERT_TAB"] = "Conversion des données";
$MESS["LEARNING_CONVERT_TAB_TITLE"] = "Conversion des données";
$MESS["LEARNING_CONVERT_TITLE"] = "conversion des données du module d'apprentissage";
$MESS["LEARNING_PROCESSED_SUMMARY"] = "Eléments convertis (cumul) : ";
?>