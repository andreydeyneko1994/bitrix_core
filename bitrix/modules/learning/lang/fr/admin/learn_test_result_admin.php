<?
$MESS["LEARNING_ADMIN_ANSWERED"] = "Répondu";
$MESS["LEARNING_ADMIN_ANSWER_NAME"] = "Répondre";
$MESS["LEARNING_ADMIN_CORRECT"] = "La réponse est correcte";
$MESS["LEARNING_ADMIN_POINT"] = "Evaluation";
$MESS["LEARNING_ADMIN_QUESTION_NAME"] = "Question";
$MESS["LEARNING_ADMIN_RESULTS"] = "Résultats";
$MESS["LEARNING_ADMIN_TITLE"] = "Résultat du test";
$MESS["LEARNING_ADMIN_USER_RESPONSE_TEXT"] = "Réponse d'utilisateur";
$MESS["LEARNING_ERROR"] = "Essai d'économie d'erreur";
$MESS["SAVE_ERROR"] = "Erreur test de la mise à jour #";
?>