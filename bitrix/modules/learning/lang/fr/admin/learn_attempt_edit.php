<?
$MESS["LEARNING_ADMIN_COMPLETED"] = "Test réussi";
$MESS["LEARNING_ADMIN_DATE_END"] = "Date de la fin";
$MESS["LEARNING_ADMIN_DATE_START"] = "Date de début";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "Maximum de points";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Nombre de questions";
$MESS["LEARNING_ADMIN_SCORE"] = "Evaluation";
$MESS["LEARNING_ADMIN_STATUS"] = "Statut";
$MESS["LEARNING_ADMIN_TAB1"] = "Essai";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Les paramètres de la tentative";
$MESS["LEARNING_ADMIN_TEST"] = "Test";
$MESS["LEARNING_ADMIN_TITLE"] = "Edition de la Tentative";
$MESS["LEARNING_ADMIN_USER"] = "tudiant";
$MESS["LEARNING_ADMIN_USER_FIELDS"] = "Propriétés supplémentaires";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Revenir sur la liste des tentatives";
$MESS["LEARNING_BAD_ATTEMPT_ID_EX"] = "La tentative n'a pas été trouvée ou l'accès à celle-ci est interdite.";
$MESS["LEARNING_CHANGE_USER_PROFILE"] = "Éditer le profil de l'utilisateur";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Etes-vous sûr de vouloir supprimer cet essai ?";
$MESS["LEARNING_ERROR"] = "Erreur lors de la sauvegarde de la tentative.";
?>