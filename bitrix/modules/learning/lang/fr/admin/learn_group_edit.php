<?
$MESS["LEARNING_ACCESS_DENIED_TO_UF_MANAGE"] = "Il n'y a pas de droits suffisants pour la création des champs utilisateurs.";
$MESS["LEARNING_ACCESS_DENIED_TO_USERS"] = "Autorisations insuffisantes pour voir la liste des utilisateurs.";
$MESS["LEARNING_ACTIVATION_SCHEDULE"] = "Schéma d'activation";
$MESS["LEARNING_ACTIVATION_SCHEDULE_TITLE"] = "Retardement avant l'ouverture (en jours)";
$MESS["LEARNING_ACTIVE_PERIOD"] = "Période d'activité";
$MESS["LEARNING_ADMIN_ACTIVE"] = "Actif(ve)";
$MESS["LEARNING_ADMIN_ATTACHED_COURSE"] = "Taux";
$MESS["LEARNING_ADMIN_CHANGE_ATTACHED_COURSE"] = "sélectionner";
$MESS["LEARNING_ADMIN_CODE"] = "Nom symbolique";
$MESS["LEARNING_ADMIN_SORT"] = "Classification";
$MESS["LEARNING_ADMIN_TAB1"] = "Groupe de formation";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Groupe de formation";
$MESS["LEARNING_ADMIN_TITLE"] = "Dénomination";
$MESS["LEARNING_AVAILABLE_AFTER_ELEMENT_CREATION"] = "S'il vous plaît créer au moins un groupe avant de régler les paramètres.";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Retour à la liste de groupes d'étude";
$MESS["LEARNING_BACK_TO_LEARNING_GROUPS"] = "Retour à la liste de groupes d'étude";
$MESS["LEARNING_BAD_LEARNING_GROUP_ID_EX"] = "Le groupe de formation est introuvable ou bien vous n'êtes pas autorisés à le voir.";
$MESS["LEARNING_EDIT_TITLE"] = "diter";
$MESS["LEARNING_GROUPS_LIST"] = "Groupes de formation";
$MESS["LEARNING_GROUP_MEMBERSHIP"] = "Appartenance à un groupe";
$MESS["LEARNING_GROUP_MEMBERS_LIST"] = "Appartenance à un groupe";
$MESS["LEARNING_NEW_TITLE"] = "Création d'un nouveau Groupe d'étude";
?>