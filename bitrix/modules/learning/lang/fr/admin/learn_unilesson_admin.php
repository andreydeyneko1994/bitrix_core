<?
$MESS["LEARNING_ADMIN_MENU_DELETE_RECURSIVE"] = "Supprimer";
$MESS["LEARNING_ADMIN_MENU_DISBAND"] = "Licencier";
$MESS["LEARNING_ADMIN_MENU_DISBAND_QUESTION"] = "La dislocation de l'élément entraînera sa suppression, mais tous les cours/chapitres/leçons qui y sont inclus demeureront et seront disponibles dans la section 'Liste de toutes les leçons'. Continuer ?";
$MESS["LEARNING_ADMIN_MENU_DISBAND_TITLE"] = "Supprimer l'élément sélectionné en gardant ses descendants";
$MESS["LEARNING_ADMIN_TITLE"] = "Liste de leçons";
$MESS["LEARNING_CONFIRM_DISBAND_LESSON_WITH_PARENT_PATHES"] = "Cet élément est inclus dans l'un ou plusieurs (#CNT#) autres éléments. Il sera supprimé partout. Continuer ?";
$MESS["LEARNING_CONSIST_FROM"] = "Comprend";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_CHAPTERS"] = "Nombre de chapitres";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_DEPTH"] = "Annexes";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_LESSONS"] = "De leçons au total";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_QUESTIONS"] = "Nombre de questions";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_TESTS"] = "De tests";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED"] = "Interdiction de la publication";
$MESS["LEARNING_FILTER_TYPE_COURSE"] = "Taux";
$MESS["LEARNING_FILTER_TYPE_LESSON_WITH_CHILDS"] = "Chapitre";
$MESS["LEARNING_FILTER_TYPE_LESSON_WO_CHILDS"] = "Leçon";
$MESS["LEARNING_FILTER_TYPE_OF_UNILESSON"] = "Type de l'élément";
$MESS["LEARNING_INCLUDED_IN"] = "Inclus dans";
$MESS["LEARNING_QUESTION_ADD"] = "Ajouter une question";
?>