<?
$MESS["LEARNING_ADMIN_TAB1"] = "Fichier de données";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Sélectionnez un fichier de données";
$MESS["LEARNING_ADMIN_TAB2"] = "Résultat";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Résultats d'importation";
$MESS["LEARNING_BACK"] = "Précédent";
$MESS["LEARNING_DATA_FILE"] = "Fichier de données";
$MESS["LEARNING_DATA_FILE_NOT_FOUND"] = "Le fichier de données n'est pas téléchargé, ni sélectionné. Impossible de le télécharger.";
$MESS["LEARNING_IF_SCORM"] = "SCORM";
$MESS["LEARNING_NEXT_STEP_F"] = "Charger les données";
$MESS["LEARNING_NOT_TAR_GZ"] = "Le fichier de données ne contient pas de données. Téléchargement impossible.";
$MESS["LEARNING_OPEN"] = "Aller";
$MESS["LEARNING_PAGE_TITLE"] = "Chargement de cours";
$MESS["LEARNING_SUCCESS"] = "Importation terminée";
?>