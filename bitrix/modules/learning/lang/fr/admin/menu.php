<?
$MESS["LEARNING_CHAPTERS_LIST"] = "Liste des chapitres";
$MESS["LEARNING_LESSONS_LIST"] = "Liste de toutes les leçons";
$MESS["LEARNING_MENU_ATTEMPT"] = "Essais";
$MESS["LEARNING_MENU_ATTEMPT_ALT"] = "Essais";
$MESS["LEARNING_MENU_CERTIFICATION"] = "Certificats";
$MESS["LEARNING_MENU_CERTIFICATION_ALT"] = "Certificats";
$MESS["LEARNING_MENU_COURSES"] = "Cours";
$MESS["LEARNING_MENU_COURSES_ALT"] = "Répertoire des cours";
$MESS["LEARNING_MENU_COURSES_OTHER"] = "Autre";
$MESS["LEARNING_MENU_EXPORT"] = "Décharger";
$MESS["LEARNING_MENU_EXPORT_ALT"] = "L'exportation de cours";
$MESS["LEARNING_MENU_GRADEBOOK"] = "Historique de formation";
$MESS["LEARNING_MENU_GRADEBOOK_ALT"] = "Historique de formation";
$MESS["LEARNING_MENU_GROUPS"] = "Groupes de formation";
$MESS["LEARNING_MENU_GROUPS_ALT"] = "Gestion des groupes de formation";
$MESS["LEARNING_MENU_IMPORT"] = "Importation";
$MESS["LEARNING_MENU_IMPORT_ALT"] = "L'importation de cours";
$MESS["LEARNING_MENU_LEARNING"] = "e-Education";
$MESS["LEARNING_MENU_LEARNING_TITLE"] = "e-Education panneau de contrôle";
$MESS["LEARNING_MENU_STUDENTS"] = "tudiants";
$MESS["LEARNING_MENU_STUDENTS_ALT"] = "Liste des étudiants";
$MESS["LEARNING_QUESTION_LIST"] = "Toutes liste de questions";
?>