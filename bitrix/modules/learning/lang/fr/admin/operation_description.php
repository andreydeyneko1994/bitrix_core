<?
$MESS["OP_NAME_LESSON_CREATE"] = "Création d'une nouvelle leçon";
$MESS["OP_NAME_LESSON_LINK_DESCENDANTS"] = "Rattachement à la leçon d'autres leçons";
$MESS["OP_NAME_LESSON_LINK_TO_PARENTS"] = "Lier un leçon à l'autre comme un enfant";
$MESS["OP_NAME_LESSON_MANAGE_RIGHTS"] = "Gérer les marges";
$MESS["OP_NAME_LESSON_READ"] = "Afficher la leçon";
$MESS["OP_NAME_LESSON_REMOVE"] = "Eliminer la leçon";
$MESS["OP_NAME_LESSON_UNLINK_DESCENDANTS"] = "Détachement des descendants de la leçon";
$MESS["OP_NAME_LESSON_UNLINK_FROM_PARENTS"] = "Détachement de la leçon du parent";
$MESS["OP_NAME_LESSON_WRITE"] = "Changement de leçon";
?>