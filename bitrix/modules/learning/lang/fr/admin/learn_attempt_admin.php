<?
$MESS["LEARNING_ADMIN_ATTEMPT_SPEED"] = "Temps de réponse moyen (sec.)";
$MESS["LEARNING_ADMIN_COMPLETED"] = "Test réussi";
$MESS["LEARNING_ADMIN_DATE_END"] = "Date de la fin";
$MESS["LEARNING_ADMIN_DATE_START"] = "Date de début";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "Max. But";
$MESS["LEARNING_ADMIN_MENU_RESULTS"] = "Résultats";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Nombre de questions";
$MESS["LEARNING_ADMIN_RESULTS"] = "Essais";
$MESS["LEARNING_ADMIN_SCORE"] = "Evaluation";
$MESS["LEARNING_ADMIN_STATUS"] = "Statut";
$MESS["LEARNING_ADMIN_STUDENT"] = "tudiant";
$MESS["LEARNING_ADMIN_TEST"] = "Test";
$MESS["LEARNING_ADMIN_TITLE"] = "Essais";
$MESS["LEARNING_ERROR"] = "Erreur lors de la sauvegarde de la tentative";
$MESS["SAVE_ERROR"] = "Une erreur s'est produite lors de réessayage #";
?>