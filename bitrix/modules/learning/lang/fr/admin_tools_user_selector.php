<?
$MESS["LEARNING_USER_SELECTOR_ADD"] = "Ajouter";
$MESS["LEARNING_USER_SELECTOR_CURRENT"] = "Actuel";
$MESS["LEARNING_USER_SELECTOR_NONE"] = "Absent";
$MESS["LEARNING_USER_SELECTOR_NOT_FOUND"] = "Désolé, votre recherche n'a donné aucun résultat.";
$MESS["LEARNING_USER_SELECTOR_OTHER"] = "Choisir";
$MESS["LEARNING_USER_SELECTOR_USER_PROFILE"] = "Evènements du profil utilisateur";
$MESS["LEARNING_USER_SELECTOR_WAIT"] = "Attendez...";
?>