<?
$MESS["LEARNING_OPTIONS_MENU_MAX_COURSES"] = "Nombre de cours dans les menus";
$MESS["LEARNING_OPTIONS_USE_HTMLEDIT"] = "Utiliser l'éditeur visuel";
$MESS["LEARNING_PATH_EXAMPLE"] = "Exemples des modèles du chemin";
$MESS["LEARNING_SITE_PATH"] = "Versions des fichiers publics";
$MESS["LEARNING_SITE_PATH_SITE"] = "Modèles des Chemins pour le site &quot;#SITE#&quot;";
$MESS["LEARNING_SITE_PATH_SITE_CHAPTER"] = "Modèle de chemin d'accès au chapitre";
$MESS["LEARNING_SITE_PATH_SITE_COURSE"] = "Modèle de chemin d'accès au cours";
$MESS["LEARNING_SITE_PATH_SITE_LESSON"] = "Leçons et chapitres";
$MESS["LEARNING_TAB_RIGHTS"] = "Accès";
$MESS["LEARNING_TAB_RIGHTS_ALT"] = "Configuration des droits d'accès au module de formation";
$MESS["LEARNING_TAB_SET"] = "Paramètres";
$MESS["LEARNING_TAB_SET_ALT"] = "Paramètres communs";
?>