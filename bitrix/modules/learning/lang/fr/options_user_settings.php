<?
$MESS["learning_CERTIFICATION"] = "Certificats";
$MESS["learning_GRADEBOOK"] = "Carnet de notes";
$MESS["learning_PUBLIC_PROFILE"] = "Autoriser l'accès du public au profil de l'étudiant";
$MESS["learning_RESUME"] = "Curriculum vitae";
$MESS["learning_TAB"] = "Apprentissage";
$MESS["learning_TAB_TITLE"] = "Enquête du spécialiste";
$MESS["learning_TRANSCRIPT"] = "Transcription #";
?>