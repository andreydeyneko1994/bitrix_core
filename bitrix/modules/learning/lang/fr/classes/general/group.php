<?
$MESS["LEARNING_GROUP_ADD_UNKNOWN_ERROR"] = "Impossible de créer un groupe d'étude";
$MESS["LEARNING_GROUP_DELETE_UNKNOWN_ERROR"] = "Impossible de supprimer le groupe d'instruction.";
$MESS["LEARNING_GROUP_UPDATE_UNKNOWN_ERROR"] = "Impossible de changer de groupe d'étude.";
?>