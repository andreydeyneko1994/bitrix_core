<?
$MESS["LEARNING_ACTION_DESELF"] = "nie dla samodzielnego sprawdzenia";
$MESS["LEARNING_ACTION_SELF"] = "dla samodzielnego sprawdzenia";
$MESS["LEARNING_ADD"] = "Nowe pytanie";
$MESS["LEARNING_ADD_ALT"] = "Nowe pytanie";
$MESS["LEARNING_ADMIN_TITLE"] = "Pytania";
$MESS["LEARNING_CHAPTER"] = "Rozdział";
$MESS["LEARNING_ERROR"] = "Błąd zapisu pytania";
$MESS["LEARNING_F_CORRECT_REQUIRED"] = "Wymagana jest poprawna odpowiedź";
$MESS["LEARNING_LESSON"] = "Lekcja";
$MESS["LEARNING_QUESTION_ADM_POINT"] = "Ocena";
$MESS["LEARNING_QUESTION_ADM_REQUIRED"] = "Poprawna<br />odpowiedź<br />wymagana";
$MESS["LEARNING_QUESTION_ADM_STATS"] = "Odpowiedzi (poprawne/wszystkie)";
$MESS["LEARNING_QUESTION_OF_TEXT_TYPE_IGNORED"] = "Pytanie tekstowe zostało pominięte, ponieważ takie pytania nie są wykorzystywane w samosprawdzalnych testach.";
$MESS["LEARNING_QUESTION_TYPE_M"] = "Wybór wielokrotny";
$MESS["LEARNING_QUESTION_TYPE_R"] = "Sortuj";
$MESS["LEARNING_QUESTION_TYPE_S"] = "Jednokrotnego wyboru";
$MESS["LEARNING_QUESTION_TYPE_T"] = "Odpowiedź tekstowa";
$MESS["MAIN_ADMIN_LIST_NOT_REQUIRED"] = "poprawna odpowiedź nie jest wymagana";
$MESS["MAIN_ADMIN_LIST_REQUIRED"] = "Wymagana jest poprawna odpowiedź";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji pytania #";
?>