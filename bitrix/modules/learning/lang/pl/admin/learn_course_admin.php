<?
$MESS["LEARNING_ADD"] = "Dodaj kurs";
$MESS["LEARNING_ADD_ALT"] = "Dodaj nowy kurs";
$MESS["LEARNING_ADMIN_TITLE"] = "Kursy";
$MESS["LEARNING_CHAPTER_TITLE"] = "Rozdziały";
$MESS["LEARNING_ERROR"] = "Błąd zapisu kursu";
$MESS["LEARNING_LESSON_TITLE"] = "Lekcje";
$MESS["LEARNING_TEST_ADD"] = "Dodaj nowy test";
$MESS["LEARNING_TEST_TITLE"] = "Lista testów";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji kursu #";
?>