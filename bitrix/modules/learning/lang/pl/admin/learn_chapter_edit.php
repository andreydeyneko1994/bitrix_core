<?
$MESS["LEARNING_ADMIN_TAB1"] = "Rozdział";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Ustawienia rozdziału";
$MESS["LEARNING_ADMIN_TAB2"] = "Ogłoszenie";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Krótki opis";
$MESS["LEARNING_ADMIN_TAB3"] = "Opis";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Pełny Opis";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Na pewno chcesz usunąć ten rozdział?";
$MESS["LEARNING_ERROR"] = "Błąd zapisu rozdziału";
$MESS["LEARNING_PREVIEW_TEXT"] = "Podgląd";
?>