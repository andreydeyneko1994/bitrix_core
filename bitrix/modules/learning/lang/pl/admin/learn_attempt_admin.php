<?
$MESS["LEARNING_ADMIN_COMPLETED"] = "Test został zakończony";
$MESS["LEARNING_ADMIN_DATE_END"] = "Data zakończenia";
$MESS["LEARNING_ADMIN_DATE_START"] = "Data rozpoczęcia";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "Max. Punktów";
$MESS["LEARNING_ADMIN_MENU_RESULTS"] = "Wyniki";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Pytania";
$MESS["LEARNING_ADMIN_RESULTS"] = "Podejścia";
$MESS["LEARNING_ADMIN_SCORE"] = "Punkty";
$MESS["LEARNING_ADMIN_STATUS"] = "Status";
$MESS["LEARNING_ADMIN_STUDENT"] = "Uczeń";
$MESS["LEARNING_ADMIN_TEST"] = "Test";
$MESS["LEARNING_ADMIN_TITLE"] = "Podejścia";
$MESS["LEARNING_ERROR"] = "Błąd zapisu próby";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji próby #";
?>