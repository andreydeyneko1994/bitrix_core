<?
$MESS["LEARNING_ADMIN_MENU_DELETE_RECURSIVE"] = "Usuń";
$MESS["LEARNING_ADMIN_MENU_DISBAND"] = "Rozwiąż i usuń";
$MESS["LEARNING_ADMIN_MENU_DISBAND_QUESTION"] = "Działanie to usunie pozycję, ale wszystkie kursy, lekcje i rozdziały w nim zawarte pozostaną i będą dostępne w sumarycznym widoku lekcji w Panelu Kontrolnym. Kontynuować?";
$MESS["LEARNING_ADMIN_MENU_DISBAND_TITLE"] = "Usuń wybraną pozycję, ale zostaw jego pochodne nietknięte";
$MESS["LEARNING_ADMIN_TITLE"] = "Lista lekcji";
$MESS["LEARNING_CONFIRM_DISBAND_LESSON_WITH_PARENT_PATHES"] = "Pozycja, którą chcesz usunąć jest częścią jednej lub więcej (#CNT#) innych pozycji. Pozycja ta zostanie usunięta wszędzie. Kontynuować?";
$MESS["LEARNING_CONSIST_FROM"] = "Zawierający";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_CHAPTERS"] = "Wszystkie rozdziały";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_DEPTH"] = "Grupuj";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_LESSONS"] = "Wszystkie lekcje";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_QUESTIONS"] = "Pytania";
$MESS["LEARNING_COURSE_ADM_CARDINALITY_TESTS"] = "Testy";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED"] = "Odmów publikacji";
$MESS["LEARNING_FILTER_TYPE_COURSE"] = "Kurs";
$MESS["LEARNING_FILTER_TYPE_LESSON_WITH_CHILDS"] = "Rozdział";
$MESS["LEARNING_FILTER_TYPE_LESSON_WO_CHILDS"] = "Lekcja";
$MESS["LEARNING_FILTER_TYPE_OF_UNILESSON"] = "Rodzaj elementu";
$MESS["LEARNING_INCLUDED_IN"] = "Zawarty w";
$MESS["LEARNING_QUESTION_ADD"] = "Dodaj pytanie";
?>