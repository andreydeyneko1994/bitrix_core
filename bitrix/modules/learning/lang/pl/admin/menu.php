<?
$MESS["LEARNING_CHAPTERS_LIST"] = "Lista rozdziałów";
$MESS["LEARNING_LESSONS_LIST"] = "Lista wszystkich lekcji";
$MESS["LEARNING_MENU_ATTEMPT"] = "Podejścia";
$MESS["LEARNING_MENU_ATTEMPT_ALT"] = "Podejścia";
$MESS["LEARNING_MENU_CERTIFICATION"] = "Certyfikowanie";
$MESS["LEARNING_MENU_CERTIFICATION_ALT"] = "Certyfikowanie";
$MESS["LEARNING_MENU_COURSES"] = "Kursy";
$MESS["LEARNING_MENU_COURSES_ALT"] = "Lista kursów";
$MESS["LEARNING_MENU_COURSES_OTHER"] = "inne";
$MESS["LEARNING_MENU_EXPORT"] = "Eksportuj";
$MESS["LEARNING_MENU_EXPORT_ALT"] = "Eksport kursu";
$MESS["LEARNING_MENU_GRADEBOOK"] = "Dzienniczek ocen";
$MESS["LEARNING_MENU_GRADEBOOK_ALT"] = "Dzienniczek ocen";
$MESS["LEARNING_MENU_GROUPS"] = "Lekcje";
$MESS["LEARNING_MENU_GROUPS_ALT"] = "Zarządzaj lekcjamo";
$MESS["LEARNING_MENU_IMPORT"] = "Importuj";
$MESS["LEARNING_MENU_IMPORT_ALT"] = "Import kursu";
$MESS["LEARNING_MENU_LEARNING"] = "e-Nauka";
$MESS["LEARNING_MENU_LEARNING_TITLE"] = "panel kontrolny e-Nauki";
$MESS["LEARNING_MENU_STUDENTS"] = "Uczniowie";
$MESS["LEARNING_MENU_STUDENTS_ALT"] = "Lista uczniów";
$MESS["LEARNING_QUESTION_LIST"] = "Lista wszystkich pytań";
?>