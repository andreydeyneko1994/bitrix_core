<?
$MESS["LEARNING_ADD_MARK"] = "Dodaj ocenę";
$MESS["LEARNING_APPROVED"] = "Automatyczne sprawdzenie wyników";
$MESS["LEARNING_ATTEMPT_LIMIT"] = "Dozwolonych prób";
$MESS["LEARNING_ATTEMPT_LIMIT_HINT"] = "(0 lub pozostaw puste dla nieograniczonej liczby)";
$MESS["LEARNING_COMPLETED_SCORE"] = "Minimalna liczba punktów do zaliczenia testu";
$MESS["LEARNING_COMPLETED_SCORE2"] = "% wszystkich ocen";
$MESS["LEARNING_CURRENT_INDICATION"] = "Pokaż Bieżące Wyniki";
$MESS["LEARNING_CURRENT_INDICATION_MARK"] = "pokaż jako ocenę";
$MESS["LEARNING_CURRENT_INDICATION_PERCENT"] = "pokaż jako procent";
$MESS["LEARNING_DESC"] = "Opis";
$MESS["LEARNING_DESC_TITLE"] = "Opis testu";
$MESS["LEARNING_EDIT_TITLE1"] = "Utwórz nowy test";
$MESS["LEARNING_EDIT_TITLE2"] = "Edytuj test ##ID#";
$MESS["LEARNING_FINAL_INDICATION"] = "Pokaż Końcowy Wynik";
$MESS["LEARNING_FINAL_INDICATION_CORRECT_COUNT"] = "pokaż wyliczenie poprawnych odpowiedzi";
$MESS["LEARNING_FINAL_INDICATION_MARK"] = "pokaż końcową ocenę";
$MESS["LEARNING_FINAL_INDICATION_MESSAGE"] = "pokaż wiadomość według ocen";
$MESS["LEARNING_FINAL_INDICATION_SCORE"] = "pokaż otrzymane oceny";
$MESS["LEARNING_INCLUDE_SELF_TEST"] = "Włączając pytania samodzielnego testu";
$MESS["LEARNING_INCORRECT_CONTROL"] = "Kontrola Niepoprawnych Odpowiedzi";
$MESS["LEARNING_MARKS"] = "Oceny";
$MESS["LEARNING_MARKS_TITLE"] = "Oceny";
$MESS["LEARNING_MAX_MARK_ERROR"] = "Maksymalna ocena nie jest określona (100% poprawnych odpowiedzi)";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS"] = "Minimalna Przerwa Pomiędzy Podejściami";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_D"] = "dni";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_H"] = "godziny";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_M"] = "minut(y)";
$MESS["LEARNING_NEXT_QUESTION_ON_ERROR"] = "przejdź do następnego pytania";
$MESS["LEARNING_ON_ERROR"] = "W Niepoprawnej Odpowiedzi";
$MESS["LEARNING_PASSAGE_TYPE"] = "Typ zdawanego testu";
$MESS["LEARNING_PASSAGE_TYPE_0"] = "Nie zezwalaj na przejście do następnego pytania bez udzielenia odpowiedzi na bieżące pytanie. Użytkownicy <b>nie mogą</b> modyfikować swojej odpowiedzi.";
$MESS["LEARNING_PASSAGE_TYPE_1"] = "Zezwól na przejście do następnego pytania bez udzielnia odpowiedzi na bieżące pytanie. Użytkownicy <b>nie mogą</b> modyfikować swojej odpowiedzi.";
$MESS["LEARNING_PASSAGE_TYPE_2"] = "Zezwól na przejście do następnego pytania bez udzielnia odpowiedzi na bieżące pytanie. Użytkownicy <b>mogą</b> modyfikować swoje odpowiedzi.";
$MESS["LEARNING_PREVIOUS_TEST_ID"] = "Odmów dostępu do tego testu bez zaliczonego testu";
$MESS["LEARNING_PREVIOUS_TEST_SCORE"] = "nie mniej niż";
$MESS["LEARNING_PREVIOUS_TEST_SCORE2"] = "% wszystkich ocen";
$MESS["LEARNING_PREV_QUESTION_ON_ERROR"] = "pozostań przy tym pytaniu";
$MESS["LEARNING_QUESTIONS_FROM"] = "Załącz w teście";
$MESS["LEARNING_QUESTIONS_FROM_ALL"] = "wszystkie pytania kursu";
$MESS["LEARNING_QUESTIONS_FROM_ALL_CHAPTER"] = "wszystkie pytania rozdziału";
$MESS["LEARNING_QUESTIONS_FROM_ALL_LESSON"] = "wszystkie pytania lekcji";
$MESS["LEARNING_QUESTIONS_FROM_ALL_LESSON_WITH_SUBLESSONS"] = "wszystkie pytania lekcji + podrzędnych lekcji";
$MESS["LEARNING_QUESTIONS_FROM_CHAPTERS"] = "pytania każdego rozdziału";
$MESS["LEARNING_QUESTIONS_FROM_COURSE"] = "pytania kursu";
$MESS["LEARNING_QUESTIONS_FROM_LESSONS"] = "pytania każdej lekcji";
$MESS["LEARNING_RANDOM_ANSWERS"] = "Tasuj odpowiedzi";
$MESS["LEARNING_RANDOM_QUESTIONS"] = "Tasuj pytania";
$MESS["LEARNING_SCORE_EXISTS_ERROR"] = "Kopia zapisu istnieje dla ##SCORE##% odpowiedzi.";
$MESS["LEARNING_SHOW_ERRORS"] = "Pokaż błędy";
$MESS["LEARNING_TEST"] = "Ustawienia";
$MESS["LEARNING_TEST_MARK"] = "Ocena";
$MESS["LEARNING_TEST_MARK_DELETE"] = "Usuń";
$MESS["LEARNING_TEST_MARK_MESSAGE"] = "Wiadomość";
$MESS["LEARNING_TEST_MARK_SCORE"] = "Poprawne odpowiedzi";
$MESS["LEARNING_TEST_NO_DEPENDS"] = "Nie ograniczaj dostępu";
$MESS["LEARNING_TEST_SCORE_TILL"] = "do";
$MESS["LEARNING_TEST_TEST"] = "Test";
$MESS["LEARNING_TEST_TITLE"] = "Ustawienia testu";
$MESS["LEARNING_TIME_LIMIT"] = "Limit czasu";
$MESS["LEARNING_TIME_LIMIT_HINT"] = "minut (0 lub puste - bez limitu)";
?>