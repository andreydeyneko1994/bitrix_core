<?
$MESS["LEARNING_CONVERT_COMPLETE"] = "Dane zostały przetworzone.";
$MESS["LEARNING_CONVERT_FAILED"] = "Błąd próby przetwarzania modułowych danych e-Nauki.";
$MESS["LEARNING_CONVERT_IN_PROGRESS"] = "W trakcie przetwarzania";
$MESS["LEARNING_CONVERT_START_BUTTON"] = "Konwertuj";
$MESS["LEARNING_CONVERT_STOP_BUTTON"] = "zatrzymaj";
$MESS["LEARNING_CONVERT_TAB"] = "Przetworzenie danych";
$MESS["LEARNING_CONVERT_TAB_TITLE"] = "Przetworzenie danych";
$MESS["LEARNING_CONVERT_TITLE"] = "Przetworzenie Danych e-Nauki";
$MESS["LEARNING_PROCESSED_SUMMARY"] = "Pozycje przetworzone (łącznie):";
?>