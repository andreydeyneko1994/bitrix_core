<?
$MESS["LEARNING_ADMIN_TAB1"] = "Lekcja";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Ustawienia lekcji";
$MESS["LEARNING_ADMIN_TAB2"] = "Ogłoszenie";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Krótki opis";
$MESS["LEARNING_ADMIN_TAB3"] = "Opis";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Opis lekcji";
$MESS["LEARNING_ADMIN_TAB4"] = "Zależności";
$MESS["LEARNING_ADMIN_TAB4_EX"] = "Zależności lekcji";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Na pewno chcesz usunąć tę lekcję?";
$MESS["LEARNING_CONFIRM_UNLINK_LESSON_FROM_PARENT"] = "Na pewno chcesz odczepić tę lekcję od nadrzędnej?";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED"] = "Nie publikuj";
$MESS["LEARNING_COURSE_ADM_PUBLISH_PROHIBITED_CONTEXT"] = "kiedy jest oglądana jako część #COURSE_NAME#";
$MESS["LEARNING_EDIT_FORM_WILL_BE_AVAILABLE_AFTER_LESSON_CREATION"] = "Ta zakładka jest tylko dostępna dla istniejących lekcji i rozdziałów.";
$MESS["LEARNING_ERROR"] = "Błąd zapisu lekcji";
$MESS["LEARNING_HEIGHT"] = "Wysokość";
$MESS["LEARNING_PATH_TO_FILE"] = "Ścieżka do pliku";
$MESS["LEARNING_PREVIEW_TEXT"] = "Podgląd";
$MESS["LEARNING_SAVE"] = "Zapisz";
$MESS["LEARNING_UNLINK_LESSON_FROM_PARENT"] = "Odczep od nadrzędnej";
$MESS["LEARNING_VIDEO_AUDIO"] = "Wideo/audio";
$MESS["LEARNING_WIDTH"] = "Szerokość";
?>