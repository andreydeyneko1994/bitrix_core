<?
$MESS["LEARNING_ADMIN_ANSWERED"] = "Odpowiedzianych";
$MESS["LEARNING_ADMIN_ANSWER_NAME"] = "Odpowiedź";
$MESS["LEARNING_ADMIN_CORRECT"] = "Odpowiedź jest prawidłowa";
$MESS["LEARNING_ADMIN_POINT"] = "Punkty";
$MESS["LEARNING_ADMIN_QUESTION_NAME"] = "Pytanie";
$MESS["LEARNING_ADMIN_RESULTS"] = "Wyniki";
$MESS["LEARNING_ADMIN_TITLE"] = "Wyniki testu";
$MESS["LEARNING_ADMIN_USER_RESPONSE_TEXT"] = "Odpowiedź użytkownika";
$MESS["LEARNING_ERROR"] = "Błąd zapisu testu";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji testu #";
?>