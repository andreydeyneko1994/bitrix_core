<?
$MESS["LEARNING_ADMIN_ANSWER"] = "Odpowiedź";
$MESS["LEARNING_ADMIN_ANSWERED"] = "Odpowiedzianych";
$MESS["LEARNING_ADMIN_CORRECT"] = "Odpowiedź jest prawidłowa";
$MESS["LEARNING_ADMIN_EDIT_QUESTION"] = "Edytuj pytanie";
$MESS["LEARNING_ADMIN_POINT"] = "Punkty";
$MESS["LEARNING_ADMIN_QUESTION"] = "Pytanie";
$MESS["LEARNING_ADMIN_RESULTS"] = "Wyniki";
$MESS["LEARNING_ADMIN_STUDENT"] = "Uczeń";
$MESS["LEARNING_ADMIN_TAB1"] = "Odpowiedź";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Odpowiedź ucznia na pytanie";
$MESS["LEARNING_ADMIN_TITLE"] = "Edytuj wyniki";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Na pewno chcesz usunąć te pytanie?";
$MESS["LEARNING_ERROR"] = "Błąd zapisania rezultatu";
?>