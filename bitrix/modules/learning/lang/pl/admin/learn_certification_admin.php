<?
$MESS["LEARNING_ADMIN_APPROVED"] = "Zdane";
$MESS["LEARNING_ADMIN_ATTEMPTS"] = "Podejścia";
$MESS["LEARNING_ADMIN_COURSE_ID"] = "Kurs";
$MESS["LEARNING_ADMIN_FROM_ONLINE"] = "Certyfikowany On-line";
$MESS["LEARNING_ADMIN_MAX_SUMMARY"] = "Max. Punktów";
$MESS["LEARNING_ADMIN_ONLINE"] = "Certyfikowany online";
$MESS["LEARNING_ADMIN_PUBLIC"] = "Publikowany w profilu ucznia";
$MESS["LEARNING_ADMIN_RESULTS"] = "Certyfikaty";
$MESS["LEARNING_ADMIN_STUDENT"] = "Uczeń";
$MESS["LEARNING_ADMIN_SUMMARY"] = "Punkty";
$MESS["LEARNING_ADMIN_TITLE"] = "Certyfikowanie";
$MESS["LEARNING_ERROR"] = "Błąd zapisu certyfikatu";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji certyfikatu #";
?>