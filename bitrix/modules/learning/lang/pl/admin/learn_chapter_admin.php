<?
$MESS["LEARNING_ADD"] = "Dodaj rozdział";
$MESS["LEARNING_ADD_ALT"] = "Dodaj nowy rozdział";
$MESS["LEARNING_ADMIN_TITLE"] = "Lista rozdziałów";
$MESS["LEARNING_CHAPTER_TITLE"] = "Lista rozdziałów";
$MESS["LEARNING_ERROR"] = "Błąd zapisu rozdziału";
$MESS["LEARNING_LESSON_TITLE"] = "Lista lekcji";
$MESS["SAVE_ERROR"] = "Błąd aktualizacji rozdziału #";
?>