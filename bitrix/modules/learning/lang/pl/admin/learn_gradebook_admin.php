<?
$MESS["LEARNING_ADMIN_APPROVED"] = "Zdane";
$MESS["LEARNING_ADMIN_ATTEMPTS"] = "Podejścia";
$MESS["LEARNING_ADMIN_EXTRA_ATTEMPTS"] = "Extra Attempts";
$MESS["LEARNING_ADMIN_MAX_RESULT"] = "Max. Punktów";
$MESS["LEARNING_ADMIN_RESULT"] = "Punkty";
$MESS["LEARNING_ADMIN_RESULTS"] = "Wyniki";
$MESS["LEARNING_ADMIN_STUDENT"] = "Uczeń";
$MESS["LEARNING_ADMIN_TEST"] = "Test";
$MESS["LEARNING_ADMIN_TITLE"] = "Training results";
$MESS["LEARNING_ERROR"] = "Error saving gradebook";
$MESS["MAIN_ADMIN_LIST_COMPLETED"] = "confirm result";
$MESS["MAIN_ADMIN_LIST_UNCOMPLETED"] = "reject result";
$MESS["SAVE_ERROR"] = "Error updating gradebook #";
?>