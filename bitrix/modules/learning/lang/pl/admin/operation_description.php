<?
$MESS["OP_NAME_LESSON_CREATE"] = "Utwórz nową lekcję";
$MESS["OP_NAME_LESSON_LINK_DESCENDANTS"] = "Dodaj podrzędne lekcje";
$MESS["OP_NAME_LESSON_LINK_TO_PARENTS"] = "Dodaj lekcję do innej jako podrzędną";
$MESS["OP_NAME_LESSON_MANAGE_RIGHTS"] = "Zarządzaj uprawnieniami kursów i lekcji";
$MESS["OP_NAME_LESSON_READ"] = "Podgląd lekcji";
$MESS["OP_NAME_LESSON_REMOVE"] = "Usuń lekcję";
$MESS["OP_NAME_LESSON_UNLINK_DESCENDANTS"] = "Odłącz podrzędne lekcje";
$MESS["OP_NAME_LESSON_UNLINK_FROM_PARENTS"] = "Odłącz podrzędną lekcję";
$MESS["OP_NAME_LESSON_WRITE"] = "Edytuj lekcję";
?>