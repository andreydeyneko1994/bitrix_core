<?
$MESS["TASK_BINDING_LESSON"] = "Lekcja";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_ANY"] = "Dołącz/odłącz podrzędne i nadrzędne lekcje i kursy";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_AS_CHILD"] = "Dołącz i odłącz podrzędne lekcje i kursy";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_LINKAGE_AS_PARENT"] = "Dołącz/odłącz nadrzędne lekcje i kursy";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_BASIC"] = "Edytuj, usuń i utwórz lekcje i kursy";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_MANAGE_DUAL"] = "Edytuj / usuń / utwórz / dołącz / odłącz nadrzędne i pochodne lekcje i kursy";
$MESS["TASK_DESC_LEARNING_LESSON_ACCESS_READ"] = "Podgląd lekcji i kursu włącznie z testami";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_ANY"] = "Dołącz/odłącz każdy";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_AS_CHILD"] = "Dołącz/odłącz podrzędne pozycje";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_LINKAGE_AS_PARENT"] = "Dołącz/odłącz nadrzędne pozycje";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_BASIC"] = "Edytuj / usuń / utwórz";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_MANAGE_DUAL"] = "Edytuj / usuń / utwórz / dołącz / odłącz każdy";
$MESS["TASK_NAME_LEARNING_LESSON_ACCESS_READ"] = "Wyświetl";
?>