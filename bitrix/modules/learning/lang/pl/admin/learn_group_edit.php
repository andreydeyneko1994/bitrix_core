<?
$MESS["LEARNING_ACCESS_DENIED_TO_UF_MANAGE"] = "Niewystarczające uprawnienia do tworzenia pól niestandardowych.";
$MESS["LEARNING_ACCESS_DENIED_TO_USERS"] = "Niewystarczające uprawnienia do wyświetlania listy użytkownika.";
$MESS["LEARNING_ACTIVATION_SCHEDULE"] = "Plan aktywacji";
$MESS["LEARNING_ACTIVATION_SCHEDULE_TITLE"] = "Opóźnij aktywację (dni)";
$MESS["LEARNING_ACTIVE_PERIOD"] = "Okres aktywności";
$MESS["LEARNING_ADMIN_ACTIVE"] = "Aktywne";
$MESS["LEARNING_ADMIN_ATTACHED_COURSE"] = "Kurs";
$MESS["LEARNING_ADMIN_CHANGE_ATTACHED_COURSE"] = "wybierz";
$MESS["LEARNING_ADMIN_CODE"] = "Nazwa";
$MESS["LEARNING_ADMIN_SORT"] = "Indeks sortowania";
$MESS["LEARNING_ADMIN_TAB1"] = "Klasa";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Klasa";
$MESS["LEARNING_ADMIN_TITLE"] = "Nazwa";
$MESS["LEARNING_AVAILABLE_AFTER_ELEMENT_CREATION"] = "Proszę utworzyć przynajmniej jedną grupę przed ustawieniem parametrów.\"";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Powrót do grup szkoleniowych";
$MESS["LEARNING_BACK_TO_LEARNING_GROUPS"] = "Powrót do lekcji";
$MESS["LEARNING_BAD_LEARNING_GROUP_ID_EX"] = "Klasa nie została znaleziona lub nie masz wystarczających uprawnień.";
$MESS["LEARNING_EDIT_TITLE"] = "Edytuj";
$MESS["LEARNING_GROUPS_LIST"] = "Lekcje";
$MESS["LEARNING_GROUP_MEMBERSHIP"] = "Uczestnicy lekcji";
$MESS["LEARNING_GROUP_MEMBERS_LIST"] = "Uczestnicy lekcji";
$MESS["LEARNING_NEW_TITLE"] = "Nowa lekcja";
?>