<?
$MESS["LEARNING_ADMIN_TAB1"] = "Plik danych";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Wybierz plik danych";
$MESS["LEARNING_ADMIN_TAB2"] = "Wynik";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Wyniki importu";
$MESS["LEARNING_BACK"] = "Wstecz";
$MESS["LEARNING_DATA_FILE"] = "Plik danych";
$MESS["LEARNING_DATA_FILE_NOT_FOUND"] = "Brak danych w pliku, który został załadowany lub wybrany. Nie można zaimportować.";
$MESS["LEARNING_IF_SCORM"] = "SCORM";
$MESS["LEARNING_NEXT_STEP_F"] = "Ładuj dane";
$MESS["LEARNING_NOT_TAR_GZ"] = "Plik danych nie zawiera danych. Nie można zaimportować.";
$MESS["LEARNING_OPEN"] = "Otwórz";
$MESS["LEARNING_PAGE_TITLE"] = "Ładuję Kurs";
$MESS["LEARNING_SUCCESS"] = "Import zakończony";
?>