<?
$MESS["LEARNING_2_1_STEP"] = "Wstecz do pierwszego kroku";
$MESS["LEARNING_ADMIN_TAB1"] = "Kurs";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Wybierz kurs do eksportowania";
$MESS["LEARNING_ADMIN_TAB2"] = "Ustawienia Eksportu";
$MESS["LEARNING_ADMIN_TAB2_EX"] = "Edytuj parametry eksportu";
$MESS["LEARNING_ADMIN_TAB3"] = "Wynik";
$MESS["LEARNING_ADMIN_TAB3_EX"] = "Wynik eksportu";
$MESS["LEARNING_BACK"] = "Wstecz";
$MESS["LEARNING_DATA_FILE_NAME"] = "Zapisz plik danych jako...";
$MESS["LEARNING_DATA_FILE_NAME1"] = "Nazwa pliku danych";
$MESS["LEARNING_DATA_FILE_NAME1_DESC"] = "Jeżeli plik istnieje, zostanie nadpisany";
$MESS["LEARNING_DATA_FILE_NAME1_T"] = "(w stosunku do strony źródła)";
$MESS["LEARNING_NEXT_STEP"] = "Następny";
$MESS["LEARNING_NEXT_STEP_F"] = "Uruchom";
$MESS["LEARNING_NO_DATA_FILE"] = "Proszę wybierz plik wyjściowy.";
$MESS["LEARNING_PAGE_TITLE"] = "Eksport kursu: krok";
$MESS["LEARNING_SUCCESS"] = "Eksport zakończony";
$MESS["LEARNING_SU_ALL"] = "Łącznie eksportowanych wierszy:";
$MESS["LEARNING_SU_ALL1"] = "Pobierz plik %DATA_URL%";
?>