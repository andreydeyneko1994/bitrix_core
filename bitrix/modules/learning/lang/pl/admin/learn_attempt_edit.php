<?
$MESS["LEARNING_ADMIN_COMPLETED"] = "Test został zakończony";
$MESS["LEARNING_ADMIN_DATE_END"] = "Data zakończenia";
$MESS["LEARNING_ADMIN_DATE_START"] = "Data rozpoczęcia";
$MESS["LEARNING_ADMIN_MAX_SCORE"] = "Maksimum Punktów";
$MESS["LEARNING_ADMIN_QUESTIONS"] = "Pytania";
$MESS["LEARNING_ADMIN_SCORE"] = "Punkty";
$MESS["LEARNING_ADMIN_STATUS"] = "Status";
$MESS["LEARNING_ADMIN_TAB1"] = "Próba";
$MESS["LEARNING_ADMIN_TAB1_EX"] = "Parametry Próby";
$MESS["LEARNING_ADMIN_TEST"] = "Test";
$MESS["LEARNING_ADMIN_TITLE"] = "Edytuj Próby";
$MESS["LEARNING_ADMIN_USER"] = "Uczeń";
$MESS["LEARNING_ADMIN_USER_FIELDS"] = "Dodatkowe Właściwości";
$MESS["LEARNING_BACK_TO_ADMIN"] = "Powrót do próby";
$MESS["LEARNING_BAD_ATTEMPT_ID_EX"] = "Nie znaleziono podejścia lub odmowa dostępu.";
$MESS["LEARNING_CHANGE_USER_PROFILE"] = "Edytuj Profil Użytkownika";
$MESS["LEARNING_CONFIRM_DEL_MESSAGE"] = "Na pewno chcesz usunąć tę próbę?";
$MESS["LEARNING_ERROR"] = "Błąd zapisu próby.";
?>