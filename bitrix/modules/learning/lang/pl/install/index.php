<?
$MESS["COPY_PUBLIC_FILES"] = "Kopiuj publiczne pliki i szablony dla strony";
$MESS["LEARNING_INSTALL_BACK"] = "Powrót do sekcji zarządzania modułem";
$MESS["LEARNING_INSTALL_COMPLETE_ERROR"] = "Instalacja zakończona z błędami";
$MESS["LEARNING_INSTALL_COMPLETE_OK"] = "Instalacja zakończona.";
$MESS["LEARNING_INSTALL_COPY_PUBLIC"] = "Kopiuj skrypty sekcji publicznej";
$MESS["LEARNING_INSTALL_PUBLIC_SETUP"] = "Instaluj";
$MESS["LEARNING_INSTALL_TEMPLATE_NAME"] = "ID Szablonu";
$MESS["LEARNING_INSTALL_TITLE"] = "Instalacja modułu e-Nauki";
$MESS["LEARNING_MODULE_DESC"] = "System e-Nauki";
$MESS["LEARNING_MODULE_NAME"] = "e-Nauka";
$MESS["LEARNING_PERM_ADMIN_D"] = "odmów dostępu do ustawień";
$MESS["LEARNING_PERM_ADMIN_W"] = "pełny dostęp do ustawień";
$MESS["LEARNING_PERM_D"] = "Czytaj kursy";
$MESS["LEARNING_PERM_W"] = "Pełny dostęp";
$MESS["LEARNING_UNINSTALL_COMPLETE"] = "Odinstalowywanie zakończone.";
$MESS["LEARNING_UNINSTALL_DEL"] = "Dezinstalacja";
$MESS["LEARNING_UNINSTALL_ERROR"] = "Błędy deinstalacji:";
$MESS["LEARNING_UNINSTALL_SAVETABLE"] = "Zabisz Tabele";
$MESS["LEARNING_UNINSTALL_TITLE"] = "Odinstalowanie modułu e-Nauki";
$MESS["LEARNING_UNINSTALL_WARNING"] = "Ostrzeżenie! Moduł zostanie usunięty z systemu.";
?>