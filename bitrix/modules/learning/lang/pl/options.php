<?
$MESS["LEARNING_OPTIONS_MENU_MAX_COURSES"] = "Liczba kursów w menu";
$MESS["LEARNING_OPTIONS_USE_HTMLEDIT"] = "Użyj edytora wizualnego";
$MESS["LEARNING_PATH_EXAMPLE"] = "Przykład szablonu ścieżki";
$MESS["LEARNING_SITE_PATH"] = "Szablon Ścieżki Strefy Publicznej (określona strona internetowa)";
$MESS["LEARNING_SITE_PATH_SITE_CHAPTER"] = "Szablon ścieżki rozdziału";
$MESS["LEARNING_SITE_PATH_SITE_COURSE"] = "Szablon ścieżki strony";
$MESS["LEARNING_SITE_PATH_SITE_LESSON"] = "Szablon ścieżki lekcji";
$MESS["LEARNING_TAB_RIGHTS"] = "Uprawnienia";
$MESS["LEARNING_TAB_RIGHTS_ALT"] = "Moduł Uprawnień Dostępu";
$MESS["LEARNING_TAB_SET"] = "Ustawienia";
$MESS["LEARNING_TAB_SET_ALT"] = "Ogólne Parametry";
?>