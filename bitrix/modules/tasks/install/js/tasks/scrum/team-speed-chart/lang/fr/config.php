<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_DONE_COLUMN"] = "Terminée";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_PLAN_COLUMN"] = "Planifiée";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_TITLE"] = "Tableau de vélocité de l'équipe";
