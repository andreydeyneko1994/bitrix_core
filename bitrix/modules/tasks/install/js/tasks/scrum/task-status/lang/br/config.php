<?php
$MESS["TST_ERROR_POPUP_TITLE"] = "Ocorreu um erro";
$MESS["TST_PARENT_COMPLETE_CANCEL_CAPTION"] = "Não terminado";
$MESS["TST_PARENT_COMPLETE_MESSAGE"] = "Todas as subtarefas de\"<b>#name#</b>\" foram concluídas.<br><br>Deseja fechar a tarefa\"<b>#name#</b>\" ou continuar trabalhando nela?";
$MESS["TST_PARENT_COMPLETE_NOTIFY"] = "A tarefa principal foi concluída com sucesso.";
$MESS["TST_PARENT_COMPLETE_OK_CAPTION"] = "Concluir";
$MESS["TST_PARENT_PROCEED_CAPTION"] = "Continuar";
$MESS["TST_PARENT_PROCEED_NOTIFY"] = "Você pode continuar trabalhando na tarefa principal no kanban do sprint.";
$MESS["TST_PARENT_RENEW_CANCEL_CAPTION"] = "Não retomar";
$MESS["TST_PARENT_RENEW_MESSAGE"] = "A subtarefa \"#sub-name#\" foi retomada.<br><br>Deseja retomar a tarefa\"<b>#name#</b>\"?";
$MESS["TST_PARENT_RENEW_NOTIFY"] = "A tarefa principal foi retomada.";
$MESS["TST_PARENT_RENEW_OK_CAPTION"] = "Retomar";
