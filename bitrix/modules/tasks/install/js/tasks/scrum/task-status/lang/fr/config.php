<?php
$MESS["TST_ERROR_POPUP_TITLE"] = "Une erreur est survenue";
$MESS["TST_PARENT_COMPLETE_CANCEL_CAPTION"] = "Ne pas terminer";
$MESS["TST_PARENT_COMPLETE_MESSAGE"] = "Toutes les sous-tâches de \"<b>#name#</b>\" ont été terminées.<br><br>Voulez-vous fermer la tâche \"<b>#name#</b>\" ou continuer à travailler dessus ?";
$MESS["TST_PARENT_COMPLETE_NOTIFY"] = "La tâche parente a bien été accomplie.";
$MESS["TST_PARENT_COMPLETE_OK_CAPTION"] = "Terminer";
$MESS["TST_PARENT_PROCEED_CAPTION"] = "Continuer";
$MESS["TST_PARENT_PROCEED_NOTIFY"] = "Vous pouvez continuer à travailler sur la tâche parente sur le kanban du sprint.";
$MESS["TST_PARENT_RENEW_CANCEL_CAPTION"] = "Ne pas reprendre";
$MESS["TST_PARENT_RENEW_MESSAGE"] = "La sous-tâche \"#sub-name#\" a été reprise. <br><br>Voulez-vous reprendre la tâche \"<b>#name#</b>\" ?";
$MESS["TST_PARENT_RENEW_NOTIFY"] = "La tâche parente a été reprise.";
$MESS["TST_PARENT_RENEW_OK_CAPTION"] = "Reprendre";
