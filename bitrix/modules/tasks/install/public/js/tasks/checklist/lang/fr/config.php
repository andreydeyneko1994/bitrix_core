<?php
$MESS["TASKS_CHECKLIST_ACCOMPLICE_ICON_HINT"] = "Co-traitant";
$MESS["TASKS_CHECKLIST_ADD_NEW_ITEM"] = "Ajouter un élément";
$MESS["TASKS_CHECKLIST_AUDITOR_ICON_HINT"] = "Superviseur";
$MESS["TASKS_CHECKLIST_COMPLETE_ALL"] = "Sélectionner tout";
$MESS["TASKS_CHECKLIST_DEFAULT_DISPLAY_TITLE_2"] = "Liste de contrôle";
$MESS["TASKS_CHECKLIST_DEFAULT_DISPLAY_TITLE_WITH_NUMBER"] = "Liste de contrôle ##ITEM_NUMBER#";
$MESS["TASKS_CHECKLIST_DELETE_CHECKLIST"] = "Supprimer la liste de contrôle";
$MESS["TASKS_CHECKLIST_FILES_LOADER_POPUP_FROM_B24"] = "Trouver sur Bitrix24";
$MESS["TASKS_CHECKLIST_FILES_LOADER_POPUP_FROM_CLOUD"] = "Télécharger à partir d'un lecteur externe";
$MESS["TASKS_CHECKLIST_FILES_LOADER_POPUP_FROM_COMPUTER"] = "Télécharger à partir de l'ordinateur";
$MESS["TASKS_CHECKLIST_GROUP_ACTIONS"] = "Actions de groupe";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_ADD_ACCOMPLICE"] = "Ajouter un collaborateur";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_ADD_AUDITOR"] = "Ajouter un observateur";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_ADD_FILE"] = "Ajouter un fichier";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_IMPORTANT"] = "Important";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_REMOVE"] = "Supprimer";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_RENAME"] = "Renommer";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_TAB_IN"] = "Déplacer à droite";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_TAB_OUT"] = "Déplacer à gauche";
$MESS["TASKS_CHECKLIST_MOBILE_POPUP_MENU_TO_ANOTHER_CHECKLIST"] = "Déplacer dans une autre liste de contrôle";
$MESS["TASKS_CHECKLIST_NEW_CHECKLIST_TITLE"] = "Liste de contrôle ##ITEM_NUMBER#";
$MESS["TASKS_CHECKLIST_NEW_ITEM_PLACEHOLDER"] = "Texte d'élément";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_ACTION_ACCOMPLICE_ADDED"] = "Participant ajouté";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_ACTION_AUDITOR_ADDED"] = "Observateur ajouté";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_ACTION_DELETE_CHECKLIST"] = "Liste de contrôle supprimée";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_ACTION_DELETE_ITEM"] = "Élément supprimé";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_ACTION_DELETE_SELECTED_ITEMS"] = "Éléments sélectionnés supprimés";
$MESS["TASKS_CHECKLIST_NOTIFICATION_BALLOON_CANCEL"] = "Annuler";
$MESS["TASKS_CHECKLIST_PANEL_ACCOMPLICE"] = "Participant";
$MESS["TASKS_CHECKLIST_PANEL_AUDITOR"] = "Observateur";
$MESS["TASKS_CHECKLIST_PANEL_IMPORTANT"] = "Important";
$MESS["TASKS_CHECKLIST_PANEL_TO_ANOTHER_CHECKLIST"] = "Déplacer dans une autre liste de contrôle";
$MESS["TASKS_CHECKLIST_PANEL_TO_ANOTHER_CHECKLIST_POPUP_NEW_CHECKLIST"] = "Créer une nouvelle";
$MESS["TASKS_CHECKLIST_PROGRESS_BAR_PROGRESS_TEXT"] = "terminées : #completed# sur #total#";
