<?
$MESS["TASKS_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "La persona responsable no está especificada.";
$MESS["TASKS_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Cambiar a nombre de";
$MESS["TASKS_CHANGE_RESPONSIBLE_NEW"] = "Nueva persona responsable";
$MESS["TASKS_CHANGE_RESPONSIBLE_NO_PERMISSIONS"] = "Permiso insuficiente para cambiar de persona responsable.";
?>