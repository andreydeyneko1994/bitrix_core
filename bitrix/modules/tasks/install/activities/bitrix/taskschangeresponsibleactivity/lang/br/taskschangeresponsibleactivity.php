<?
$MESS["TASKS_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "A pessoa responsável não está especificada.";
$MESS["TASKS_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Mudança em nome de";
$MESS["TASKS_CHANGE_RESPONSIBLE_NEW"] = "Nova pessoa responsável";
$MESS["TASKS_CHANGE_RESPONSIBLE_NO_PERMISSIONS"] = "Permissão insuficiente para alterar a pessoa responsável.";
?>