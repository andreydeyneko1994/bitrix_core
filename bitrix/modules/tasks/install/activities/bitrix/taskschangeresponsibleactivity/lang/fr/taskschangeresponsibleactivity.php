<?
$MESS["TASKS_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "Le responsable n'est pas précisé.";
$MESS["TASKS_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Modifier de la part de";
$MESS["TASKS_CHANGE_RESPONSIBLE_NEW"] = "Nouveau responsable";
$MESS["TASKS_CHANGE_RESPONSIBLE_NO_PERMISSIONS"] = "Permissions insuffisantes pour changer le responsable.";
?>