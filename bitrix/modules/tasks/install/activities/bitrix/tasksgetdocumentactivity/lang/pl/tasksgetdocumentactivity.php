<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Dostęp do właściwości działań mają jedynie administratorzy Bitrix24.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "ID Zadania";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "Identyfikator zadania jest wymagany";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "Nie można uzyskać informacji o dokumencie";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "Nie wybrano pól dokumentu";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Wybierz pola";
