<?
$MESS["TASKS_CHANGE_STAGE_EMPTY_PROP"] = "Etapa final no especificada";
$MESS["TASKS_CHANGE_STAGE_RECURSION"] = "Error de ejecución: posible recursión infinita en el cambio de estado";
$MESS["TASKS_CHANGE_STAGE_STAGE"] = "Nueva fase";
$MESS["TASKS_CHANGE_STAGE_STAGE_ERROR"] = "Etapa de destino incorrecto";
$MESS["TASKS_CHANGE_STAGE_TERMINATED"] = "Completado porque la etapa cambió.";
?>