<?
$MESS["TASKS_CHANGE_STAGE_EMPTY_PROP"] = "Dernière étape non spécifiée";
$MESS["TASKS_CHANGE_STAGE_RECURSION"] = "Erreur lors de l'exécution : récursion infinie possible dans le changement de statut";
$MESS["TASKS_CHANGE_STAGE_STAGE"] = "Nouvelle étape";
$MESS["TASKS_CHANGE_STAGE_STAGE_ERROR"] = "Étape de la cible incorrecte";
$MESS["TASKS_CHANGE_STAGE_TERMINATED"] = "Terminé parce que l'étape a été changée";
?>