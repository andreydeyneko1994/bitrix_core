<?
$MESS["TASKS_CHANGE_STAGE_EMPTY_PROP"] = "Etap końcowy jest nieokreślony";
$MESS["TASKS_CHANGE_STAGE_RECURSION"] = "Błąd wykonania: możliwa nieskończona rekursja zmiany statusu";
$MESS["TASKS_CHANGE_STAGE_STAGE"] = "Nowy etap";
$MESS["TASKS_CHANGE_STAGE_STAGE_ERROR"] = "Nieprawidłowy etap docelowy";
$MESS["TASKS_CHANGE_STAGE_TERMINATED"] = "Ukończono z powodu zmiany statusu";
?>