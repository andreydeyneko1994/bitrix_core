<?
$MESS["TASKS_CHANGE_STATUS_COMPLETED"] = "Ukończono";
$MESS["TASKS_CHANGE_STATUS_DEFERRED"] = "Odroczono";
$MESS["TASKS_CHANGE_STATUS_EMPTY_PROP"] = "Status końcowy jest nieokreślony";
$MESS["TASKS_CHANGE_STATUS_IN_PROGRESS"] = "W toku";
$MESS["TASKS_CHANGE_STATUS_MODIFIED_BY"] = "Zmień w imieniu";
$MESS["TASKS_CHANGE_STATUS_NO_PERMISSIONS"] = "Niewystarczające uprawnienia do zmiany statusu zadania";
$MESS["TASKS_CHANGE_STATUS_PENDING"] = "Oczekiwanie";
$MESS["TASKS_CHANGE_STATUS_STATUS"] = "Nowy status";
?>