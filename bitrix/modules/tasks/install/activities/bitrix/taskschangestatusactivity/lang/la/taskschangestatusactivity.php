<?
$MESS["TASKS_CHANGE_STATUS_COMPLETED"] = "Completado";
$MESS["TASKS_CHANGE_STATUS_DEFERRED"] = "Diferido";
$MESS["TASKS_CHANGE_STATUS_EMPTY_PROP"] = "El estado final no fue especificado";
$MESS["TASKS_CHANGE_STATUS_IN_PROGRESS"] = "En progreso";
$MESS["TASKS_CHANGE_STATUS_MODIFIED_BY"] = "Cambiar a nombre de";
$MESS["TASKS_CHANGE_STATUS_NO_PERMISSIONS"] = "Permisos insuficientes para cambiar el estado de la tarea";
$MESS["TASKS_CHANGE_STATUS_PENDING"] = "Pendiente";
$MESS["TASKS_CHANGE_STATUS_STATUS"] = "Nuevo estado";
?>