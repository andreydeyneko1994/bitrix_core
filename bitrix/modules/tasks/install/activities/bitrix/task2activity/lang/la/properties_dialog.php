<?php
$MESS["BPCGHLP_HOLD_TO_CLOSE"] = "Proceso de pausa mientras se ejecuta la tarea";
$MESS["BPCGHLP_NO"] = "No";
$MESS["BPCGHLP_YES"] = "Si";
$MESS["TASKS_BP_AUTO_LINK_TO_CRM_ENTITY"] = "Enlazar a la actual entidad CRM";
$MESS["TASKS_BP_PD_AS_CHILD_TASK"] = "Crear como subtarea de esta tarea";
