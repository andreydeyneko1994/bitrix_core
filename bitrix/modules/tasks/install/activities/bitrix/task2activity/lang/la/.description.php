<?php
$MESS["BPTA2_DESCR_CLOSEDBY"] = "Tarea cerrada por";
$MESS["BPTA2_DESCR_CLOSEDDATE"] = "Tarea cerrada";
$MESS["BPTA2_DESCR_DESCR"] = "Agregar una nueva tarea";
$MESS["BPTA2_DESCR_IS_DELETED"] = "La tarea ha sido eliminada";
$MESS["BPTA2_DESCR_NAME"] = "Tarea";
$MESS["BPTA2_DESCR_TASKID"] = "Tarea ID";
