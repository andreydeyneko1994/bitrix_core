<?php
$MESS["BPCGHLP_HOLD_TO_CLOSE"] = "Interrompre le processus pendant l'exécution de la tâche";
$MESS["BPCGHLP_NO"] = "Non";
$MESS["BPCGHLP_YES"] = "Oui";
$MESS["TASKS_BP_AUTO_LINK_TO_CRM_ENTITY"] = "Lier à l'entité actuelle CRM";
$MESS["TASKS_BP_PD_AS_CHILD_TASK"] = "Créer en tant que sous-tâche de cette tâche";
