<?php
$MESS["TASKS_BP_RPD_ADDITIONAL"] = "plus";
$MESS["TASKS_BP_RPD_AS_CHILD_TASK"] = "Créer en tant que sous-tâche de cette tâche";
$MESS["TASKS_BP_RPD_HOLD_TO_CLOSE"] = "Interrompre le processus pendant l'exécution de la tâche";
$MESS["TASKS_BP_RPD_PRIORITY"] = "Priorité élevée";
