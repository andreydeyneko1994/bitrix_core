<?
$MESS["BPTA2_DESCR_CLOSEDBY"] = "L'utilisateur qui a fermé la tâche";
$MESS["BPTA2_DESCR_CLOSEDDATE"] = "Date d'achèvement de la tâche";
$MESS["BPTA2_DESCR_DESCR"] = "Ajouter une tâche";
$MESS["BPTA2_DESCR_IS_DELETED"] = "La tâche a été supprimée";
$MESS["BPTA2_DESCR_NAME"] = "Tâche";
$MESS["BPTA2_DESCR_TASKID"] = "ID de la tâche";
?>