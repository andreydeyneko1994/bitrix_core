<?
$MESS["TASKS_PROJECTS_HEAD"] = "Supervisor";
$MESS["TASKS_PROJECTS_HEADS"] = "Supervisores";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_0"] = "e #SPAN#mais #COUNT# membro#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_1"] = "e #SPAN#mais #COUNT# membros#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_2"] = "e #SPAN#mais #COUNT# membros#/SPAN#";
$MESS["TASKS_PROJECTS_OVERVIEW_NO_DATA"] = "Atualmente não existem tarefas em projetos";
$MESS["TASKS_PROJECTS_SUMMARY"] = "Total de tarefas em projetos";
$MESS["TASKS_PROJECTS_TASK_ALL"] = "Total";
$MESS["TASKS_PROJECTS_TASK_COMPLETE"] = "Concluído";
$MESS["TASKS_PROJECTS_TASK_IN_WORK"] = "Tarefas em andamento";
$MESS["TASKS_PROJECTS_WITH_MY_MEMBERSHIP"] = "Projetos em que eu estou";
?>