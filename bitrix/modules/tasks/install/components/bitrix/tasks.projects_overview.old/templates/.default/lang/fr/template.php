<?
$MESS["TASKS_PROJECTS_HEAD"] = "Superviseur";
$MESS["TASKS_PROJECTS_HEADS"] = "Superviseurs";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_0"] = "et #SPAN##COUNT# autres participants#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_1"] = "et #SPAN##COUNT# autres membres#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_2"] = "et #SPAN##COUNT# autres membres#/SPAN#";
$MESS["TASKS_PROJECTS_OVERVIEW_NO_DATA"] = "Les projets ne comptent aucune tâche pour le moment";
$MESS["TASKS_PROJECTS_SUMMARY"] = "Total de tâches des projets";
$MESS["TASKS_PROJECTS_TASK_ALL"] = "Total";
$MESS["TASKS_PROJECTS_TASK_COMPLETE"] = "Terminées";
$MESS["TASKS_PROJECTS_TASK_IN_WORK"] = "Tâches en cours";
$MESS["TASKS_PROJECTS_WITH_MY_MEMBERSHIP"] = "Projets avec ma participation";
?>