<?
$MESS["TASKS_TUP_TEMPLATE_CLOSE_NOTIFICATION"] = "Nie konwertuj";
$MESS["TASKS_TUP_TEMPLATE_PROMPT"] = "Istnieją zadania w Twoim Bitrix24, które zawierają pliki dodane przy wykorzystaniu starej wersji modułu. Teraz zadania mogą korzystać jedynie z nowej wersji Drive; musisz dokonać konwersji plików, aby je zachować.";
$MESS["TASKS_TUP_TEMPLATE_START"] = "Przeprowadź konwersję";
$MESS["TASKS_TUP_TEMPLATE_SUCCESS"] = "Pliki zostały przekonwertowane";
?>