<?
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "godz.";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY"] = "Zadania podwładnych";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY_HINT"] = "Pokazuje zadania utworzone przez użytkownika lub przypisane do podwładnych";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY"] = "Grupa zadań";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY_HINT"] = "Pokazuje wszystkie zadania przydzielone do twojej grupy roboczej";
$MESS["TASKS_REPORT_MY_TASKS_HINT"] = "Pokazuje wszystkie zadania, dla których jesteś wykonawcą lub uczestnikiem";
$MESS["TASKS_REPORT_MY_TASKS_ONLY"] = "Moje zadania";
?>