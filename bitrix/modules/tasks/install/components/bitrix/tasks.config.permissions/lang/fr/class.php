<?php
$MESS["TASKS_CONFIG_PERMISSIONS"] = "Droits d'accès";
$MESS["TASKS_CONFIG_PERMISSIONS_SECTION_ACCESS"] = "Droits d'accès";
$MESS["TASKS_CONFIG_PERMISSIONS_SECTION_ROBOTS"] = "Règles d'automatisation";
$MESS["TASKS_CONFIG_PERMISSIONS_SECTION_TASKS"] = "Tâches";
$MESS["TASKS_CONFIG_PERMISSIONS_SECTION_TEMPLATES"] = "Modèles";
