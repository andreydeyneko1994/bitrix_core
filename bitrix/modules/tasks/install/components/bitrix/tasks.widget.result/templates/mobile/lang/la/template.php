<?php
$MESS["TASKS_RESULT_TUTORIAL_MESSAGE"] = "Puede utilizar cualquiera de sus comentarios como informe de tareas, final o intermedio. Haga clic en el comentario y seleccione la opción\"Marcar como resumen del estado de la tarea\".";
$MESS["TASKS_RESULT_TUTORIAL_TITLE"] = "El creador de tareas requiere que proporcione un informe de tareas.";
