<?
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_0"] = "Superviseur";
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_1"] = "Superviseurs";
$MESS["TASKS_PROJECT_OVERVIEW_ADD_PROJECT"] = "Ajouter un projet";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_0"] = "et <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# participants supplémentaires</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_1"] = "et <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# participants supplémentaires</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_2"] = "et <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# participants supplémentaires</a>";
$MESS["TASKS_TITLE_PROJECTS_OVERVIEW"] = "Projets";
?>