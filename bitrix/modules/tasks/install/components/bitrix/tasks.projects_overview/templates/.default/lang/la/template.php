<?
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_0"] = "Supervisor";
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_1"] = "Supervisores";
$MESS["TASKS_PROJECT_OVERVIEW_ADD_PROJECT"] = "Agregar un proyecto";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_0"] = "y <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# más participantes</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_1"] = "and <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# más participantes</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_2"] = "and <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# más participantes</a>";
$MESS["TASKS_TITLE_PROJECTS_OVERVIEW"] = "Proyectos";
?>