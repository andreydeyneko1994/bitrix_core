<?
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "No se pueden vincular estas tareas";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "Los cambios que ha realizado pueden perderse.";
$MESS["TASKS_DELETE_SUCCESS"] = "La tarea ha sido eliminada";
$MESS["TASKS_GANTT_MONTH_APR"] = "Abril";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Agosto";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Diciembre";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Febrero";
$MESS["TASKS_GANTT_MONTH_JAN"] = "Enero";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Julio";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Junio";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Marzo";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Mayo";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Noviembre";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Octubre";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Septiembre";
$MESS["TASKS_GANTT_PRINT_SPOTLIGHT_TEXT"] = "Ahora puede imprimir el gráfico y establecer el cronograma de horarios.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "El plan gratuito solo admite cinco dependencias de tareas. Habilitar tareas extendidas para usar
dependencias ilimitadas.<br>
<br>
Se admiten cuatro tipos de dependencia:<br>
Dependencias clásicas (Finalizar> Inicio) <br>
Inicio simultáneo de tareas (Inicio> Inicio) <br>
Fecha límite de tarea simultánea (Finalizar> Finalizar) <br>
Comenzar al finalizar (Inicio> Finalizar) <br>
<br>
<a href=\"https://bitrix24.com/pro/tasks.php\" target=\"_blank\">Más información</a><br>
<br>
Tareas prolongadas, CRM extendido y Telefonía extendidas están disponibles en los planes comerciales seleccionados.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "Disponible solo en tareas extendidas";
$MESS["TASKS_TITLE"] = "Tareas";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tareas del grupo de trabajo";
$MESS["TASKS_TITLE_MY"] = "Mis tareas";
?>