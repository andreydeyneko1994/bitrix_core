<?
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Nie można połączyć tych zadań";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "Wprowadzone zmiany mogą zostać utracone.";
$MESS["TASKS_GANTT_MONTH_APR"] = "Kwiecień";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Sierpień";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Grudzień";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Luty";
$MESS["TASKS_GANTT_MONTH_JAN"] = "Styczeń";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Lipiec";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Czerwiec";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Marzec";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Maj";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Listopad";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Październik";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Wrzesień";
$MESS["TASKS_GANTT_PRINT_SPOTLIGHT_TEXT"] = "Teraz możesz wydrukować wykres i ustawić godzinowe ramy czasowe.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "Plan Free obsługuje tylko pięć zależności zadań. Włącz rozszerzone zadania, aby używać nieograniczonych zależności.<br>
<br>
Obsługiwane są cztery typy zależności:<br>
Klasyczne zależności (Zakończenie > Rozpoczęcie) <br>
Jednoczesne uruchomienie zadania (Rozpoczęcie > Rozpoczęcie)<br>
Jednoczesne zakończenie zadania (Zakończenie > Zakończenie) <br>
Rozpoczęcie po zakończeniu (Rozpoczęcie > Zakończenie)<br>
<br>
<a href=\"https://bitrix24.com/pro/tasks.php\" target=\"_blank\">Dowiedz się więcej</a><br>
<br>
Rozszerzone zadania, Rozszerzony CRM i Rozszerzona telefonia są dostępne w wybranych planach płatnych.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "Dostępne tylko w przypadku planów rozszerzonych";
$MESS["TASKS_TITLE"] = "Zadania";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Zadania grupy roboczej";
$MESS["TASKS_TITLE_MY"] = "Moje zadania";
?>