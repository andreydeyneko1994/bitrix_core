<?php
$MESS["TASKS_PANEL_TAB_ALL"] = "Tudo";
$MESS["TASKS_PANEL_TAB_APPLICATIONS_2"] = "Market";
$MESS["TASKS_PANEL_TAB_CONFIG_PERMISSIONS"] = "Permissões de Acesso";
$MESS["TASKS_PANEL_TAB_EFFECTIVE"] = "Eficiência";
$MESS["TASKS_PANEL_TAB_EMPLOYEE_PLAN"] = "Participação";
$MESS["TASKS_PANEL_TAB_KANBAN"] = "Kanban";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Supervisão";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Projetos";
$MESS["TASKS_PANEL_TAB_RECYCLEBIN"] = "Lixeira";
$MESS["TASKS_PANEL_TAB_REPORTBOARD"] = "Quadro de Relatórios";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Relatórios";
$MESS["TASKS_PANEL_TAB_SCRUM"] = "Scrum";
$MESS["TASKS_PANEL_TAB_TASKS"] = "Tarefas";
$MESS["TASKS_PANEL_TAB_TEMPLATES"] = "Modelos";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE"] = "<strong>Eficiência</strong> é um indicador de desempenho que mostra quão bem as pessoas trabalham com suas tarefas.";
