<?php
$MESS["TASKS_PANEL_TAB_ALL"] = "Tous";
$MESS["TASKS_PANEL_TAB_APPLICATIONS_2"] = "Market";
$MESS["TASKS_PANEL_TAB_CONFIG_PERMISSIONS"] = "Droits d'accès";
$MESS["TASKS_PANEL_TAB_EFFECTIVE"] = "Efficacité";
$MESS["TASKS_PANEL_TAB_EMPLOYEE_PLAN"] = "Participation";
$MESS["TASKS_PANEL_TAB_KANBAN"] = "Kanban";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Supervision";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Projets";
$MESS["TASKS_PANEL_TAB_RECYCLEBIN"] = "Corbeille";
$MESS["TASKS_PANEL_TAB_REPORTBOARD"] = "Tableau de rapport";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Rapports";
$MESS["TASKS_PANEL_TAB_SCRUM"] = "Scrum";
$MESS["TASKS_PANEL_TAB_TASKS"] = "Tâches";
$MESS["TASKS_PANEL_TAB_TEMPLATES"] = "Modèles";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE"] = "<strong>L'Efficacité</strong> est un indicateur de performance qui affiche à quel point les employés s'en sortent avec leurs tâches.";
