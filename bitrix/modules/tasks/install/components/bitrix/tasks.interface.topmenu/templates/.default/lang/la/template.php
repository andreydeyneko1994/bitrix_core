<?php
$MESS["TASKS_PANEL_TAB_ALL"] = "Todas";
$MESS["TASKS_PANEL_TAB_APPLICATIONS_2"] = "Market";
$MESS["TASKS_PANEL_TAB_CONFIG_PERMISSIONS"] = "Permisos de acceso";
$MESS["TASKS_PANEL_TAB_EFFECTIVE"] = "Eficiencia";
$MESS["TASKS_PANEL_TAB_EMPLOYEE_PLAN"] = "Implicados";
$MESS["TASKS_PANEL_TAB_KANBAN"] = "Kanban";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Supervisando";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Proyectos";
$MESS["TASKS_PANEL_TAB_RECYCLEBIN"] = "Papelera de reciclaje";
$MESS["TASKS_PANEL_TAB_REPORTBOARD"] = "Junta de informes";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Reportes";
$MESS["TASKS_PANEL_TAB_SCRUM"] = "Scrum";
$MESS["TASKS_PANEL_TAB_TASKS"] = "Tareas";
$MESS["TASKS_PANEL_TAB_TEMPLATES"] = "Plantillas";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE"] = "<strong>Eficiencia</strong> es un indicador de rendimiento que muestra qué tan bien las personas trabajan con sus tareas.";
