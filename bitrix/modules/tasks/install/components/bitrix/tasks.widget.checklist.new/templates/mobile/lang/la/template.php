<?
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_1"] = "Espere mientras se actualiza la lista de verificación.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_2"] = "En breve se agregará una nueva lista de verificación a la tarea.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_3"] = "Todos sus datos permanecerán intactos.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_NEW_CHECKLIST_TITLE"] = "Lista de verificación ##ITEM_NUMBER#";
$MESS["TASKS_CHECKLIST_MOBILE_TEMPLATE_ADD_CHECKLIST"] = "Agregar una lista de verificación";
?>