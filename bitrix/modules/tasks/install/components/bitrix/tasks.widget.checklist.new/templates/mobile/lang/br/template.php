<?
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_1"] = "Por favor, aguarde enquanto a lista de verificação está sendo atualizada.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_2"] = "Nova lista de verificação será adicionada à tarefa em breve.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_3"] = "Todos os seus dados permanecerão intactos.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_NEW_CHECKLIST_TITLE"] = "Lista de verificação ##ITEM_NUMBER#";
$MESS["TASKS_CHECKLIST_MOBILE_TEMPLATE_ADD_CHECKLIST"] = "Adicionar uma lista de verificação";
?>