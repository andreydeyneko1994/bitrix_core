<?
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_1"] = "Veuillez patienter pendant que la liste de contrôle est mise à jour.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_2"] = "Une nouvelle liste de contrôle sera bientôt ajoutée à la tâche.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_CHECKLIST_NOT_CONVERTED_MESSAGE_PART_3"] = "Toutes vos données resteront intactes.";
$MESS["TASKS_CHECKLIST_MOBILE_COMPONENT_JS_NEW_CHECKLIST_TITLE"] = "Liste de contrôle ##ITEM_NUMBER#";
$MESS["TASKS_CHECKLIST_MOBILE_TEMPLATE_ADD_CHECKLIST"] = "Ajouter une liste de contrôle";
?>