<?php
$MESS["TASKS_TUFP_DRAG_N_DROP_OFF"] = "Desactivar campo arrastrando";
$MESS["TASKS_TUFP_DRAG_N_DROP_ON"] = "Activar campo arrastrando";
$MESS["TASKS_TUFP_FIELD_HIDE_CONFIRM"] = "¿Quieres ocultar el campo? Usted puede volver a mostrarlo haciendo clic en \"Mostrar el campo\" en el formulario inferior.";
$MESS["TASKS_TUFP_FIELD_HIDE_DELETE_CONFIRM"] = "¿Quieres eliminar el campo, o simplemente ocultarlo para que no se muestre en el formulario de edición de tareas?";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "requerido";
$MESS["TASKS_TUFP_LICENSE_BODY"] = "Los campos personalizados están disponibles en los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a>. <br />
<br />
Complete sus tareas con más campos (por ejemplo, \"Presupuesto del proyecto\" o \"Cantidad pagada por adelantado\"). Estarán disponibles para todos los usuarios.";
$MESS["TASKS_TUFP_LICENSE_TITLE"] = "Agregar los campos de Tareas";
$MESS["TASKS_TUFP_NEW_FIELD_BOOLEAN"] = "Nuevo campo booleano";
$MESS["TASKS_TUFP_NEW_FIELD_DATETIME"] = "Nuevo campo de fecha";
$MESS["TASKS_TUFP_NEW_FIELD_DOUBLE"] = "Nuevo campo de número";
$MESS["TASKS_TUFP_NEW_FIELD_STRING"] = "Nuevo campo de cadena";
$MESS["TASKS_TUFP_SAVE_SCHEME_TO_EVERYONE"] = "Configurar el campo establecido para todos los usuarios";
$MESS["TASKS_TUFP_SAVE_TO_ALL_CONFIRM"] = "La configuración del campo seleccionado será asignado a todos los usuarios del portal. ¿Desea continuar?";
