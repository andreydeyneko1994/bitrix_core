<?
$MESS["TASKS_TUFP_DRAG_N_DROP_OFF"] = "Wyłącz przeciąganie pól";
$MESS["TASKS_TUFP_DRAG_N_DROP_ON"] = "Włącz przeciąganie pól";
$MESS["TASKS_TUFP_FIELD_HIDE_CONFIRM"] = "Czy chcesz ukryć pole? Możesz pokazać je ponownie, klikając w \"Pokaż pole\" na dole formularza.";
$MESS["TASKS_TUFP_FIELD_HIDE_DELETE_CONFIRM"] = "Czy chcesz usunąć pole, czy tylko je ukryć, aby nie było widoczne w formularzu edycji zadania?";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "wymagane";
$MESS["TASKS_TUFP_LICENSE_BODY"] = "Własne pola są dostępne w <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planach płatnych</a>.<br />
<br />
Wzbogać swoje zadania o dodatkowe pola (np. „Budżet projektu” i „Przedpłacona suma”). Staną się dostępne dla wszystkich użytkowników.";
$MESS["TASKS_TUFP_LICENSE_TITLE"] = "Dodaj swoje pola do zadań";
$MESS["TASKS_TUFP_NEW_FIELD_BOOLEAN"] = "Nowe pole znacznikowe";
$MESS["TASKS_TUFP_NEW_FIELD_DATETIME"] = "Nowe pole daty";
$MESS["TASKS_TUFP_NEW_FIELD_DOUBLE"] = "Nowe pole liczby";
$MESS["TASKS_TUFP_NEW_FIELD_STRING"] = "Nowe pole tekstowe";
$MESS["TASKS_TUFP_SAVE_SCHEME_TO_EVERYONE"] = "Ustaw pole dla wszystkich użytkowników";
$MESS["TASKS_TUFP_SAVE_TO_ALL_CONFIRM"] = "Wybrana konfiguracja pól zostanie zapisana dla wszystkich użytkowników portalu. Czy kontynuować?";
?>