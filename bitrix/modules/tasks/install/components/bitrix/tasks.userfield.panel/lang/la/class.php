<?php
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "No se especifica el nombre del campo";
$MESS["TASKS_TUFE_UF_ADMIN_RESTRICTED"] = "El administrador solo puede acceder a los campos personalizados";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "No se puede administrar campos personalizados en su plan.";
$MESS["TASKS_TUFE_UF_NAME_GENERATION_FAILED"] = "No se puede crear un nombre de campo personalizado";
$MESS["TASKS_TUFE_UF_NOT_FOUND"] = "No se encontró el campo personalizado";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_CREATING_ERROR"] = "No se pueden crear algunos de los campos vinculados.";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_UPDATING_ERROR"] = "No se pueden actualizar algunos de los campos vinculados.";
$MESS["TASKS_TUFE_UF_UNEXPECTED_ERROR"] = "No se puede guardar el campo debido a un error desconocido.";
$MESS["TASKS_TUFE_UF_UNKNOWN_ENTITY_CODE"] = "Código de entidad desconocido";
$MESS["TASKS_TUFE_UF_UNKNOWN_ID"] = "ID de campo personalizado desconocido";
$MESS["TASKS_TUFE_UF_UNKNOWN_TYPE"] = "Tipo de campo personalizado desconocido";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "No se puede utilizar campos personalizados en su plan.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "No se puede crear un campo obligatorio.";
