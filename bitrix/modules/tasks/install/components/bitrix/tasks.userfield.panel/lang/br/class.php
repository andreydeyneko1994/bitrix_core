<?php
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "O nome de campo não está especificado";
$MESS["TASKS_TUFE_UF_ADMIN_RESTRICTED"] = "Campos personalizados podem ser acessados ​​apenas pelo administrador";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "Você não pode gerenciar campos personalizados no seu plano.";
$MESS["TASKS_TUFE_UF_NAME_GENERATION_FAILED"] = "Não é possível criar nome do campo personalizado";
$MESS["TASKS_TUFE_UF_NOT_FOUND"] = "O campo personalizado não foi encontrado";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_CREATING_ERROR"] = "Não é possível criar alguns dos campos vinculados";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_UPDATING_ERROR"] = "Não é possível atualizar alguns dos campos vinculados";
$MESS["TASKS_TUFE_UF_UNEXPECTED_ERROR"] = "Não é possível salvar o campo devido a um erro desconhecido.";
$MESS["TASKS_TUFE_UF_UNKNOWN_ENTITY_CODE"] = "Código de entidade desconhecido";
$MESS["TASKS_TUFE_UF_UNKNOWN_ID"] = "ID do campo personalizado desconhecido";
$MESS["TASKS_TUFE_UF_UNKNOWN_TYPE"] = "Tipo de campo personalizado desconhecido";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "Você não pode usar campos personalizados no seu plano.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "Não é possível criar um campo obrigatório.";
