<?
$MESS["TASKS_NOTIFY_TASK_DELETED"] = "Le modèle a été supprimé";
$MESS["TASKS_TEMPLATE_POPUP_MENU_CHECKLIST_SECTION"] = "Liste de contrôle";
$MESS["TASKS_TEMPLATE_POPUP_MENU_SHOW_COMPLETED"] = "Afficher les éléments de liste de contrôle terminés";
$MESS["TASKS_TEMPLATE_POPUP_MENU_SHOW_ONLY_MINE"] = "Afficher les éléments de liste de contrôle qui me concernent uniquement";
$MESS["TASKS_TTTV_TEMPLATE_LCF"] = "modèle de tâche";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_BUTTON_CANCEL"] = "Annuler";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_BUTTON_CLOSE"] = "Fermer";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_CONTENT"] = "Les modifications n'ont pas été enregistrées. Voulez-vous vraiment fermer l'éditeur ?";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_HEADER"] = "Confirmer l'action";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_BUTTON_NO"] = "Non";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_BUTTON_YES"] = "Oui";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_CONTENT"] = "Voulez-vous vraiment supprimer les modifications ?";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_HEADER"] = "Confirmer l'action";
?>