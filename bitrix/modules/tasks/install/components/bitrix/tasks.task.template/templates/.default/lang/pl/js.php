<?
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_MULTIPLE_RESPONSIBLE_NOTICE"] = "Oddzielne zadanie zostanie utworzone dla każdej osoby odpowiedzialnej";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_NO_PARENT_TEMPLATE_NOTICE"] = "Szablon zadania nie może mieć szablonu nadrzędnego, jeśli zaznaczona jest opcja \"#TPARAM_FOR_NEW_USER#\".";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_CHECKLIST_SECTION"] = "Lista kontrolna";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_SHOW_COMPLETED"] = "Pokaż zakończone pozycje listy kontrolnej";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_SHOW_ONLY_MINE"] = "Pokaż pozycje listy kontrolnej dotyczące wyłącznie mnie";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_CHECKLIST_ADD_NEW_CHECKLIST"] = "Utwórz nowy";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_CHECKLIST_HINT"] = "Wybierz tekst w opisie szablonu, który chcesz dodać do listy kontrolnej";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TPARAM_TYPE"] = "Szablon zadania dla nowego pracownika";
?>