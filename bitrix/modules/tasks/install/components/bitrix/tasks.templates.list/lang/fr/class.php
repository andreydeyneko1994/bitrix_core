<?
$MESS["TASKS_PRESET_ACTIVE_REGULAR"] = "Récurrent";
$MESS["TASKS_PRESET_NEW_USER"] = "Pour les nouveaux utilisateurs";
$MESS["TASKS_TEMPLATES_NO"] = "Non";
$MESS["TASKS_TEMPLATES_YES"] = "Oui";
$MESS["TASKS_TEMPLATE_CREATED_BY"] = "Créé par";
$MESS["TASKS_TEMPLATE_DEADLINE_AFTER"] = "Date limite";
$MESS["TASKS_TEMPLATE_FOR_NEW_USER"] = "Pour les nouveaux utilisateurs";
$MESS["TASKS_TEMPLATE_GROUP_ID"] = "Groupe";
$MESS["TASKS_TEMPLATE_ID"] = "ID";
$MESS["TASKS_TEMPLATE_PRIORITY"] = "PrioritéPriorité";
$MESS["TASKS_TEMPLATE_REGULAR"] = "Récurrent";
$MESS["TASKS_TEMPLATE_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["TASKS_TEMPLATE_TAGS"] = "Mots-clés";
$MESS["TASKS_TEMPLATE_TITLE"] = "Prénom";
?>