<?
$MESS["TASKS_COLUMN_CREATED_DATE"] = "Data zgłoszenia zastrzeżenia";
$MESS["TASKS_COLUMN_DINAMIC"] = "Dynamika";
$MESS["TASKS_COLUMN_EFFECTIVE"] = "Wydajność";
$MESS["TASKS_COLUMN_ID"] = "ID";
$MESS["TASKS_COLUMN_MORE"] = "więcej";
$MESS["TASKS_COLUMN_USER"] = "Pracownik";
$MESS["TASKS_EFFECTIVE_DATE_FORMAT"] = "#DAY# #MONTH_NAME# #YEAR_IF_DIFF#";
$MESS["TASKS_FILTER_COLUMN_DATE"] = "Okres";
$MESS["TASKS_FILTER_COLUMN_GROUP_ID"] = "Projekt/Grupa";
$MESS["TASKS_FILTER_COLUMN_KPI"] = "Wydajność";
$MESS["TASKS_MONTH_1"] = "styczeń";
$MESS["TASKS_MONTH_10"] = "Październik";
$MESS["TASKS_MONTH_11"] = "Listopad";
$MESS["TASKS_MONTH_12"] = "Grudzień";
$MESS["TASKS_MONTH_2"] = "luty";
$MESS["TASKS_MONTH_3"] = "Marzec";
$MESS["TASKS_MONTH_4"] = "kwiecień";
$MESS["TASKS_MONTH_5"] = "Maj";
$MESS["TASKS_MONTH_6"] = "czerwiec";
$MESS["TASKS_MONTH_7"] = "lipiec";
$MESS["TASKS_MONTH_8"] = "Sierpień";
$MESS["TASKS_MONTH_9"] = "Wrzesień";
$MESS["TASKS_PRESET_CURRENT_DAY"] = "Bieżący dzień";
$MESS["TASKS_PRESET_CURRENT_MONTH"] = "Bieżący miesiąc";
$MESS["TASKS_PRESET_CURRENT_QUARTER"] = "Bieżący kwartał";
$MESS["TASKS_PRESET_CURRENT_YEAR"] = "Bieżący rok";
?>