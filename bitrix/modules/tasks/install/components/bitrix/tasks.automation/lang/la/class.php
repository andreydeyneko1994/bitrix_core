<?php
$MESS["TASKS_AUTOMATION_ACCESS_DENIED"] = "Se denegó el acceso a la entidad.";
$MESS["TASKS_AUTOMATION_GROUPS_CAPTION"] = "Proyectos";
$MESS["TASKS_AUTOMATION_NOT_AVAILABLE"] = "Las reglas de automatización no están disponibles.";
$MESS["TASKS_AUTOMATION_TITLE"] = "Automatización de tareas";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "El módulo Tarea no está instalado.";
