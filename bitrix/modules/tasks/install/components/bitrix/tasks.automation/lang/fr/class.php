<?php
$MESS["TASKS_AUTOMATION_ACCESS_DENIED"] = "L'accès à l'entité a été refusé.";
$MESS["TASKS_AUTOMATION_GROUPS_CAPTION"] = "Projets";
$MESS["TASKS_AUTOMATION_NOT_AVAILABLE"] = "Les règles d'automatisation sont indisponibles";
$MESS["TASKS_AUTOMATION_TITLE"] = "Automatisation des tâches";
