<?
$MESS["TASKS_AUTOMATION_CMP_CHOOSE_GROUP"] = "Seleccionar...";
$MESS["TASKS_AUTOMATION_CMP_ROBOT_HELP_TIP"] = "La regla de automatización es una secuencia de operaciones que se ejecutarán automáticamente cuando una tarea se mueve al estado especificado.
Las reglas mejoran el rendimiento empresarial, aumentan la productividad de los empleados y racionalizan proyectos a gran escala.";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PERSONAL"] = "Estados";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PLAN_1"] = "Planificador";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PROJECTS"] = "Proyectos";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT"] = "Editar reglas de automatización para todas las tareas del proyecto.";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_PLAN_1"] = "Configurar reglas de automatización para todas las tareas del planificador";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_STATUSES"] = "Configure reglas de automatización para todas las tareas en las que está involucrado";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_VIEW"] = "Estado actual de la tarea: \"#TITLE#\"";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW"] = "Reglas de automatización actuales utilizadas por las tareas del proyecto.";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_PLAN_1"] = "Reglas de automatización actuales para todas las tareas del planificador";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_STATUSES"] = "Reglas de automatización actuales para todas las tareas en las que estás involucrado";
$MESS["TASKS_AUTOMATION_CMP_TRIGGER_HELP_TIP_2"] = "El activador es cualquier acción o situación que hace que una tarea se mueva al estado especificado. Cuando se produce ese movimiento, si se especifica, se ejecutará una regla de automatización.";
?>