<?
$MESS["TASKS_AUTOMATION_CMP_CHOOSE_GROUP"] = "Selecionar...";
$MESS["TASKS_AUTOMATION_CMP_ROBOT_HELP_TIP"] = "A regra de automação é uma sequência de operações a serem executadas automaticamente quando uma tarefa é movida para o status especificado.
As regras melhoram o desempenho dos negócios, aumentam a produtividade dos funcionários e agilizam projetos em larga escala.";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PERSONAL"] = "Status";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PLAN_1"] = "Planejador";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PROJECTS"] = "Projetos";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT"] = "Editar regras de automação para todas as tarefas do projeto";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_PLAN_1"] = "Configurar regras de automação para todas as tarefas do Planejador";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_STATUSES"] = "Configurar regras de automação para todas as tarefas nas quais você está envolvido";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_VIEW"] = "Status atual da tarefa: \"#TITLE#\"";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW"] = "Atuais regras de automação usadas pelas tarefas do projeto";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_PLAN_1"] = "Atuais regras de automação para todas as tarefas do Planejador";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_STATUSES"] = "Atuais regras de automação para todas as tarefas nas quais você está envolvido";
$MESS["TASKS_AUTOMATION_CMP_TRIGGER_HELP_TIP_2"] = "Gatilho é qualquer ação ou situação que fará com que uma tarefa seja movida para o status especificado. Quando essa mudança ocorrer, uma regra de automação, se especificada, será executada.";
?>