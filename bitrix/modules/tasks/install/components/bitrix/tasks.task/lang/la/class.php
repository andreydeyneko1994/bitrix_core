<?php
$MESS["TASKS_ACTION_RESULT_REQUIRED"] = "El creador de tareas requiere que proporcione un informe de la tarea.<br> Deje un comentario en la tarea y márquelo como un resumen del estado de la tarea.";
$MESS["TASKS_REST_BUTTON_TITLE_2"] = "Bitrix24.Market";
$MESS["TASKS_TEMPLATE_CREATE_FORBIDDEN"] = "No puede crearse una plantilla porque el acceso fue denegado";
$MESS["TASKS_TT_AUTO_CHANGE_GROUP"] = "La tarea se adjuntó al proyecto actual";
$MESS["TASKS_TT_AUTO_CHANGE_ORIGINATOR"] = "El creador de tareas ha sido cambiado automáticamente a usuario actual";
$MESS["TASKS_TT_AUTO_CHANGE_PARENT"] = "La tarea se adjunto a la tarea principal";
$MESS["TASKS_TT_AUTO_CHANGE_PARENT_GROUP"] = "La tarea se adjunto al proyecto principal de la tarea";
$MESS["TASKS_TT_AUTO_CHANGE_RESPONSIBLE"] = "Se cambió a la persona responsable de esta tarea";
$MESS["TASKS_TT_COPY_READ_ERROR"] = "Error al leer el objeto que se desea copiar";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "El módulo Forum no está instalado.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE"] = "La tarea no se ha encontrado, o se ha denegado el acceso.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE_COPY"] = "Tarea a copiar no se ha encontrado o se ha denegado el acceso.";
$MESS["TASKS_TT_SAVE_AS_TEMPLATE_ERROR_MESSAGE_PREFIX"] = "No se ha creado la plantilla";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "El módulo Red social no está instalado.";
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "El módulo Task no está instalado.";
