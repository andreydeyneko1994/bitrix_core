<?
$MESS["TASKS_QUICK_CANCEL"] = "Anuluj";
$MESS["TASKS_QUICK_DEADLINE"] = "Deadline";
$MESS["TASKS_QUICK_DESCRIPTION"] = "opis";
$MESS["TASKS_QUICK_FORM_AFTER_SAVE_MESSAGE"] = "Zadanie &quot;#TASK_NAME#&quot; zostało zapisane.";
$MESS["TASKS_QUICK_FORM_DESC_PLACEHOLDER"] = "Opis Zadania";
$MESS["TASKS_QUICK_FORM_HIGHLIGHT_TASK"] = "Pokaż na liście";
$MESS["TASKS_QUICK_FORM_OPEN_TASK"] = "Otwórz zadanie";
$MESS["TASKS_QUICK_FORM_TITLE_PLACEHOLDER"] = "Nowe zadanie";
$MESS["TASKS_QUICK_IN_GROUP"] = "zadanie w projekcie";
$MESS["TASKS_QUICK_RESPONSIBLE"] = "Odpowiedzialny";
$MESS["TASKS_QUICK_SAVE"] = "Zapisz";
$MESS["TASKS_QUICK_TITLE"] = "Nazwa zadania";
?>