<?php
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_MUTE"] = "Wycisz dźwięk. Liczniki zadań będą wyświetlane na szaro. Nie będą dodawane do licznika sumarycznego.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_UNMUTE"] = "Wyłącz wyciszenie dźwięku. Liczniki zadań będą wyświetlane na zielono. Będą dodawane do licznika sumarycznego.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_CHECKLIST_SECTION"] = "Lista kontrolna";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_COMPLETED"] = "Pokaż zakończone pozycje listy kontrolnej";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_ONLY_MINE"] = "Pokaż pozycje listy kontrolnej dotyczące wyłącznie mnie";
