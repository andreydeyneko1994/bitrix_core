<?php
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_MUTE"] = "Desactivar sonido. Los contadores de tareas se mostrarán en gris, no se sumarán al contador total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_UNMUTE"] = "Activar sonido. Los contadores de tareas se mostrarán en gris, no se sumarán al contador total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_CHECKLIST_SECTION"] = "Lista de verificación";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_COMPLETED"] = "Mostrar los elementos completados de la lista de verificación";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_ONLY_MINE"] = "Mostrar los elementos de la lista de verificación que sean relevantes solo para mí";
