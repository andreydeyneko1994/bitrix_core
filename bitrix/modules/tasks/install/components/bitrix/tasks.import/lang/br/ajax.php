<?php
$MESS["TASKS_IMPORT_DEFAULT_CHECKLIST_NAME"] = "Lista de verificação #1";
$MESS["TASKS_IMPORT_ERRORS_UNKNOWN_DELETE_ERROR"] = "Erro ao excluir a tarefa inválida";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_HASH"] = "Não é possível acessar o arquivo carregado";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_PATH"] = "Caminho de upload do arquivo inválido";
