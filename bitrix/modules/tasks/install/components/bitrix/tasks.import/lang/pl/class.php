<?php
$MESS["TASKS_IMPORT_CUSTOM_HEADER"] = "Kolumna";
$MESS["TASKS_IMPORT_DEMO_VALUES_ACCOMPLICES"] = "Jan Kowalski,Jan Kowalski";
$MESS["TASKS_IMPORT_DEMO_VALUES_AUDITORS"] = "Jan Kowalski,Jan Kowalski";
$MESS["TASKS_IMPORT_DEMO_VALUES_CHECKLIST"] = "[*]pozycja 1[*]pozycja 2[*]pozycja 3";
$MESS["TASKS_IMPORT_DEMO_VALUES_CHECKLIST_NEW"] = "[**]Lista kontrolna nr 1[*]pozycja 1[*]pozycja 2[**]Lista kontrolna nr 2[*]pozycja 1[*]pozycja 2";
$MESS["TASKS_IMPORT_DEMO_VALUES_DESCRIPTION"] = "Opis Zadania";
$MESS["TASKS_IMPORT_DEMO_VALUES_ORIGINATOR"] = "Jan Kowalski";
$MESS["TASKS_IMPORT_DEMO_VALUES_PROJECT"] = "Nazwa projektu";
$MESS["TASKS_IMPORT_DEMO_VALUES_RESPONSIBLE"] = "Jan Kowalski";
$MESS["TASKS_IMPORT_DEMO_VALUES_TAGS"] = "[*]tag 1[*]tag 2";
$MESS["TASKS_IMPORT_DEMO_VALUES_TITLE"] = "Nazwa zadania";
$MESS["TASKS_IMPORT_ERRORS_FILE_EMPTY"] = "Plik jest pusty lub nie znaleziono nagłówków kolumn";
$MESS["TASKS_IMPORT_ERRORS_FILE_ERRORS"] = "Plik zawiera błędy";
$MESS["TASKS_IMPORT_ERRORS_FILE_NOT_FOUND"] = "Nie wybrano pliku";
$MESS["TASKS_IMPORT_ERRORS_FILE_PATH"] = "Błąd ścieżki przesyłania pliku";
$MESS["TASKS_IMPORT_ERRORS_REQUIRED_FIELDS"] = "Wymagane pole nie jest zaznaczone";
$MESS["TASKS_IMPORT_FIELDS_FILE_ENCODING_AUTO_DETECT"] = "Automatyczne wykrywanie";
$MESS["TASKS_IMPORT_FIELDS_FILE_NAME"] = "Nie wybrano pliku";
$MESS["TASKS_IMPORT_FIELDS_FILE_SEPARATOR_COMMA"] = "przecinek";
$MESS["TASKS_IMPORT_FIELDS_FILE_SEPARATOR_SEMICOLON"] = "średnik";
$MESS["TASKS_IMPORT_FIELDS_FILE_SEPARATOR_SPACE"] = "spacja";
$MESS["TASKS_IMPORT_FIELDS_FILE_SEPARATOR_TAB"] = "tabulator";
$MESS["TASKS_IMPORT_HEADERS_ACCOMPLICES"] = "Uczestnicy";
$MESS["TASKS_IMPORT_HEADERS_ALLOW_CHANGE_DEADLINE"] = "Osoba odpowiedzialna może zmienić termin ostateczny";
$MESS["TASKS_IMPORT_HEADERS_ALLOW_TIME_TRACKING"] = "Zadanie ma ograniczenia czasowe";
$MESS["TASKS_IMPORT_HEADERS_AUDITORS"] = "Obserwatorzy";
$MESS["TASKS_IMPORT_HEADERS_CHECKLIST"] = "Lista kontrolna";
$MESS["TASKS_IMPORT_HEADERS_DEADLINE"] = "Termin";
$MESS["TASKS_IMPORT_HEADERS_DESCRIPTION"] = "Opis";
$MESS["TASKS_IMPORT_HEADERS_END_DATE_PLAN"] = "Ukończ zadanie do";
$MESS["TASKS_IMPORT_HEADERS_END_DATE_PLAN_V2"] = "Ukończ zadanie w dniu";
$MESS["TASKS_IMPORT_HEADERS_MATCH_WORK_TIME"] = "Pomiń weekendy i dni wolne";
$MESS["TASKS_IMPORT_HEADERS_ORIGINATOR"] = "Utworzone przez";
$MESS["TASKS_IMPORT_HEADERS_PARAM_1"] = "Pozyskaj daty zadań z dat podzadań";
$MESS["TASKS_IMPORT_HEADERS_PARAM_2"] = "Automatycznie ukończ zadanie po ukończeniu podzadań";
$MESS["TASKS_IMPORT_HEADERS_PRIORITY"] = "Ważne zadanie";
$MESS["TASKS_IMPORT_HEADERS_PROJECT"] = "Projekt";
$MESS["TASKS_IMPORT_HEADERS_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["TASKS_IMPORT_HEADERS_START_DATE_PLAN"] = "Rozpocznij zadanie";
$MESS["TASKS_IMPORT_HEADERS_TAGS"] = "Tagi";
$MESS["TASKS_IMPORT_HEADERS_TASK_CONTROL"] = "Zatwierdź zadanie po ukończeniu";
$MESS["TASKS_IMPORT_HEADERS_TASK_CONTROL_V2"] = "Sprawdź zadanie po zakończeniu";
$MESS["TASKS_IMPORT_HEADERS_TIME_ESTIMATE"] = "Czas ukończenia zadania, w sekundach";
$MESS["TASKS_IMPORT_HEADERS_TITLE"] = "Nazwa";
