<?php
$MESS["TASKS_IMPORT_DONE"] = "La importación se ha completado";
$MESS["TASKS_IMPORT_ERROR"] = "Error de importación";
$MESS["TASKS_IMPORT_ERROR_FILE_NOT_SELECTED"] = "Ningún archivo seleccionado";
$MESS["TASKS_IMPORT_ERROR_FILE_WRONG_EXTENSION"] = "Formato de archivo inválido";
$MESS["TASKS_IMPORT_FILE_NOT_SELECTED"] = "Ningún archivo seleccionado";
$MESS["TASKS_IMPORT_LINE"] = "Línea ";
$MESS["TASKS_IMPORT_POPUP_WINDOW_TITLE"] = "Seleccione la codificación correcta";
$MESS["TASKS_IMPORT_STOPPED"] = "Importación abortado";
