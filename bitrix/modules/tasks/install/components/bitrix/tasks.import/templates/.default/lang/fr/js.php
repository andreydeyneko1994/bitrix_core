<?
$MESS["TASKS_IMPORT_DONE"] = "Importation est terminée";
$MESS["TASKS_IMPORT_ERROR"] = "Erreur d'importation";
$MESS["TASKS_IMPORT_ERROR_FILE_NOT_SELECTED"] = "Aucun fichier sélectionné";
$MESS["TASKS_IMPORT_ERROR_FILE_WRONG_EXTENSION"] = "Format de fichier invalide";
$MESS["TASKS_IMPORT_FILE_NOT_SELECTED"] = "Aucun fichier sélectionné";
$MESS["TASKS_IMPORT_LINE"] = "Ligne ";
$MESS["TASKS_IMPORT_POPUP_WINDOW_TITLE"] = "Sélectionner le bon encodage";
$MESS["TASKS_IMPORT_STOPPED"] = "importation interrompue";
?>