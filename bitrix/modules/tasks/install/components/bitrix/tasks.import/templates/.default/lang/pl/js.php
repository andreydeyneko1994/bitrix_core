<?php
$MESS["TASKS_IMPORT_DONE"] = "Import kompletny";
$MESS["TASKS_IMPORT_ERROR"] = "Błąd importu";
$MESS["TASKS_IMPORT_ERROR_FILE_NOT_SELECTED"] = "Nie wybrano pliku";
$MESS["TASKS_IMPORT_ERROR_FILE_WRONG_EXTENSION"] = "Błędny format pliku";
$MESS["TASKS_IMPORT_FILE_NOT_SELECTED"] = "Nie wybrano pliku";
$MESS["TASKS_IMPORT_LINE"] = "Linia ";
$MESS["TASKS_IMPORT_POPUP_WINDOW_TITLE"] = "Wybierz prawidłowe kodowanie";
$MESS["TASKS_IMPORT_STOPPED"] = "Import został przerwany";
