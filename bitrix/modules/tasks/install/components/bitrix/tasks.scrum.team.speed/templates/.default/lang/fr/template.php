<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_DONE_COLUMN"] = "Terminée";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_NOT_DATA_LABEL"] = "Il n'y a pas d'éléments qui se sont produits au cours de la période sélectionnée.";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_PLAN_COLUMN"] = "Planifiée";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_AVERAGE_LABEL"] = "Moyenne des points de story réalisés :";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MAX_LABEL"] = "Maximum de points de story réalisés :";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MIN_LABEL"] = "Minimum de points de story réalisés :";
