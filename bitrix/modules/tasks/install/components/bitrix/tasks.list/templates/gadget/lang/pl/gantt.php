<?
$MESS["INTASK_FROM_DATE_TLP"] = "od #DATA#";
$MESS["INTASK_LIST_EMPTY"] = "Nie przydzielono zadań.";
$MESS["INTASK_NO_DATE_TLP"] = "nie przypisano z powodu daty";
$MESS["INTASK_TASKASSIGNEDTO"] = "Przypisano do";
$MESS["INTASK_TASKPRIORITY"] = "Priorytet";
$MESS["INTASK_TASKSTATUS"] = "Status";
$MESS["INTASK_TO_DATE_TLP"] = "do #DATA#";
$MESS["TASKS_DEADLINE"] = "Termin ostateczny";
$MESS["TASKS_PRIORITY_0"] = "Niski";
$MESS["TASKS_PRIORITY_1"] = "Normalny";
$MESS["TASKS_PRIORITY_2"] = "Wysoki";
$MESS["TASKS_STATUS_1"] = "Nowe";
$MESS["TASKS_STATUS_2"] = "W toku";
$MESS["TASKS_STATUS_3"] = "W toku";
$MESS["TASKS_STATUS_4"] = "Rzekomo zakończone";
$MESS["TASKS_STATUS_5"] = "Zakończone";
$MESS["TASKS_STATUS_6"] = "Wstrzymane";
$MESS["TASKS_STATUS_7"] = "Odrzucone";
?>