<?
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Nie można łączyć tych zadań";
$MESS["TASKS_GANTT_MONTH_APR"] = "kwiecień";
$MESS["TASKS_GANTT_MONTH_AUG"] = "sierpień";
$MESS["TASKS_GANTT_MONTH_DEC"] = "grudzień";
$MESS["TASKS_GANTT_MONTH_FEB"] = "luty";
$MESS["TASKS_GANTT_MONTH_JAN"] = "styczeń";
$MESS["TASKS_GANTT_MONTH_JUL"] = "lipiec";
$MESS["TASKS_GANTT_MONTH_JUN"] = "czerwiec";
$MESS["TASKS_GANTT_MONTH_MAR"] = "marzec";
$MESS["TASKS_GANTT_MONTH_MAY"] = "maj";
$MESS["TASKS_GANTT_MONTH_NOV"] = "listopad";
$MESS["TASKS_GANTT_MONTH_OCT"] = "październik";
$MESS["TASKS_GANTT_MONTH_SEP"] = "wrzesień";
?>