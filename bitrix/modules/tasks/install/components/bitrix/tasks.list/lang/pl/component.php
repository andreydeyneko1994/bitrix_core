<?
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Moduł Forum nie jest zainstalowany.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Moduł \"Sieci społecznościowej\" nie jest zainstalowany.";
$MESS["TASKS_ACCESS_DENIED"] = "Nie masz uprawnień wyświetlanie tej listy zadań";
$MESS["TASKS_ACCESS_TO_GROUP_DENIED"] = "Nie można wyświetlić listy zadań dla tej grupy";
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Moduł Zadań nie jest dostępny w tej edycji.";
$MESS["TASKS_MODULE_NOT_FOUND"] = "Moduł zadań nie jest zainstalowany.";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Grupa zadań";
$MESS["TASKS_TITLE_MY_TASKS"] = "Moje zadania";
$MESS["TASKS_TITLE_TASKS"] = "Zadania";
$MESS["TASKS_UNEXPECTED_ERROR"] = "Niezdefiniowany błąd";
?>