<?php
$MESS["TASKS_COUNTER_EXPIRED"] = "Atrasada";
$MESS["TASKS_COUNTER_MORE"] = "Mais";
$MESS["TASKS_COUNTER_MY"] = "Meus itens:";
$MESS["TASKS_COUNTER_MY_TASKS"] = "Minhas tarefas:";
$MESS["TASKS_COUNTER_NEW_COMMENTS"] = "Comentários";
$MESS["TASKS_COUNTER_OTHER"] = "Outro:";
$MESS["TASKS_COUNTER_OTHER_TASKS"] = "Outras tarefas: #TITLE#";
$MESS["TASKS_COUNTER_READ_ALL"] = "Marcar tudo como lido";
