<?
$MESS["SCHEDULER_PRINT_ALERT_BUTTON_TITLE"] = "Sim";
$MESS["SCHEDULER_PRINT_ALERT_TEXT"] = "Você terminou de imprimir?";
$MESS["SCHEDULER_PRINT_BORDER_TITLE"] = "Adicionar borda";
$MESS["SCHEDULER_PRINT_BUTTON_TITLE"] = "Imprimir";
$MESS["SCHEDULER_PRINT_CANCEL_BUTTON_TITLE"] = "Cancelar";
$MESS["SCHEDULER_PRINT_DATE_FROM_TITLE"] = "Data de início";
$MESS["SCHEDULER_PRINT_DATE_TO_TITLE"] = "Data de término";
$MESS["SCHEDULER_PRINT_FIT_TO_PAGE"] = "Ajustar à página";
$MESS["SCHEDULER_PRINT_FORMAT_TITLE"] = "Formato";
$MESS["SCHEDULER_PRINT_LANDSCAPE_TITLE"] = "Panorama";
$MESS["SCHEDULER_PRINT_ORIENTATION_TITLE"] = "Orientação";
$MESS["SCHEDULER_PRINT_PORTRAIT_TITLE"] = "Retrato";
$MESS["SCHEDULER_PRINT_SETTINGS_TITLE"] = "Configuração de Impressão";
$MESS["SCHEDULER_PRINT_TOO_MANY_PAGES"] = "Muitas páginas para imprimir (#NUMBER#). Por favor, reduza o intervalo de datas ou diminua a escala do gráfico.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_FROM"] = "A data de início está fora do intervalo do gráfico.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_RANGE"] = "Intervalo de datas inválido.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_TO"] = "A data de término está fora do intervalo do gráfico.";
?>