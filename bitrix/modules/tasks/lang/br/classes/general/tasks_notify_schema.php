<?php
$MESS["TASKS_NS_COMMENT"] = "Novo comentário adicionado a uma tarefa";
$MESS["TASKS_NS_MANAGE"] = "Tarefa criada ou editada";
$MESS["TASKS_NS_REMINDER"] = "Lembrete de tarefa";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "Tarefa atribuída a você";
$MESS["TASKS_NS_TASK_EXPIRED_SOON"] = "Tarefa quase atrasada";
