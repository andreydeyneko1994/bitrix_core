<?
$MESS["TASKS_COUNTERS_PROCESSOR_ADMIN_IS_NOT_AN_ADMIN"] = "O usuário do sistema não é administrador. O sistema pode não funcionar corretamente. Certifique-se de que o usuário com ID=1 está ativo e tem privilégios de administrador.";
?>