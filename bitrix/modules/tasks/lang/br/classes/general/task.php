<?
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "A data do prazo da tarefa está fora do intervalo de datas do projeto";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "Participantes";
$MESS["TASKS_MESSAGE_AUDITORS"] = "Observadores";
$MESS["TASKS_MESSAGE_DEADLINE"] = "Prazo de entrega";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "Descrição";
$MESS["TASKS_MESSAGE_NO"] = "não";
$MESS["TASKS_MESSAGE_PRIORITY"] = "Prioridade";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "Pessoa Responsável";
$MESS["TASKS_MESSAGE_TITLE"] = "Nome";
$MESS["TASKS_NEW_TASK"] = "Nova tarefa";
$MESS["TASKS_NEW_TASK_MESSAGE"] = "A nova tarefa foi adicionada

Nome da tarefa: #TASK_TITLE#
Criado por: #TASK_AUTHOR#
Responsável: #TASK_RESPONSIBLE#
#TASK_EXTRA#
Ver a tarefa:
#PATH_TO_TASK#";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "A data planejada de término da tarefa está fora do intervalo de datas do projeto";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "A data planejada de início da tarefa está fora do intervalo de datas do projeto";
?>