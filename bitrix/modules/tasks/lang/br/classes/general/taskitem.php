<?php
$MESS["TASKS_ACCESS_DENIED_TO_CREATOR_UPDATE"] = "Não é possível alterar o criador da tarefa devido a permissões insuficientes";
$MESS["TASKS_ACCESS_DENIED_TO_DEADLINE_UPDATE"] = "Não é possível alterar o prazo devido a permissões insuficientes";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_DELETE"] = "Não é possível excluir a tarefa porque o acesso foi negado";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_UPDATE"] = "Não é possível editar a tarefa devido a permissões insuficientes";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "A data do prazo da tarefa está fora do intervalo de datas do projeto";
$MESS["TASKS_ERROR_TASK_ADD"] = "Não é possível criar a tarefa";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Não é possível vincular uma tarefa à sua tarefa primária";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "A data planejada de término da tarefa está fora do intervalo de datas do projeto";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "A data planejada de início da tarefa está fora do intervalo de datas do projeto";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Seu período de teste expirou";
$MESS["TASK_CANT_ADD_LINK"] = "Não é possível vincular tarefas";
$MESS["TASK_CANT_DELETE_LINK"] = "Não é possível desvincular tarefas";
$MESS["TASK_CANT_UPDATE_LINK"] = "Não é possível atualizar o link da tarefa";
