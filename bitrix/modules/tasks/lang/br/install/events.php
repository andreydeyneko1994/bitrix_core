<?php
$MESS["TASKS_UPDATE_TASK_NAME"] = "Status da tarefa alterado";
$MESS["TASK_REMINDER_DESC"] = "#TASK_TITLE# - nome da tarefa
#PATH_TO_TASK# - URL da tarefa";
$MESS["TASK_REMINDER_MESSAGE"] = "Mensagem de #SITE_NAME#
------------------------------------------
Clique no link abaixo para visualizar a tarefa:

#PATH_TO_TASK#

Essa mensagem foi gerada automaticamente.";
$MESS["TASK_REMINDER_NAME"] = "Lembrete de tarefa";
$MESS["TASK_REMINDER_SUBJECT"] = "#SITE_NAME#: Esse é um lembrete de \"#TASK_TITLE#\".";
