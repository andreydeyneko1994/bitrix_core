<?php
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION"] = "Você é um verdadeiro profissional! Agora você tem #COUNT# tarefas no seu Bitrix24. Observação: você só poderá pesquisar tarefas nos planos comerciais quando o número exceder #LIMIT#. Pesquisar tantas tarefas exigirá recursos consideráveis, portanto, apenas os filtros predefinidos podem ser usados no plano Free. #HELPDESK_LINK#";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION_HELPDESK_LINK"] = "Informações";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT"] = "
	<p>Estamos nos esforçando para melhorar a qualidade e o desempenho do serviço para atender à sua escala de negócios. A quantidade de dados aumenta à medida que sua empresa cresce: novas tarefas, negócios e outras entidades são criadas todos os dias.</p>
	<p>Quanto maior a escala da sua empresa, mais dados a pesquisar você cria. Escolha um plano que se adapte aos seus dados para pesquisar mais rapidamente.</p>";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT_V2"] = "<p>Limite de pesquisa de tarefa (#LIMIT#) atingido. Estamos nos esforçando para melhorar a qualidade e o desempenho do serviço para atender à sua escala de negócios. A quantidade de dados aumenta à medida que sua empresa cresce: novas tarefas, negócios e outras entidades são criadas todos os dias.</p>
<p>Quanto maior a escala da sua empresa, mais dados a pesquisar você cria. Escolha um plano que se adapte aos seus dados para pesquisar mais rapidamente.</br>#HELPDESK_LINK#</p>
";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_HELPDESK_LINK"] = "Informações";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE"] = "Limite de (#LIMIT#) tarefas atingido";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE_V2"] = "Limite de pesquisa atingido";
