<?
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TEXT"] = "
	<p>Estamos nos esforçando para melhorar a qualidade e o desempenho do serviço para atender à sua escala de negócios. A quantidade de dados aumenta à medida que sua empresa cresce: novas tarefas, negócios e outras entidades são criadas todos os dias.</p>
	<p>Quanto maior a escala da sua empresa, mais dados a pesquisar você cria. Escolha um plano que se adapte aos seus dados para pesquisar mais rapidamente.</p>";
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TITLE"] = "Limite de (#LIMIT#) tarefas atingido";
?>