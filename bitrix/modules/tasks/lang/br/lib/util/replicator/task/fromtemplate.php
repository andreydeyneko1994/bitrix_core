<?php
$MESS["TASKS_REPLICATOR_CANT_IDENTIFY_USER"] = "Não é possível definir um usuário para executar a tarefa como. Por favor, verifique se a conta de administrador está ativa.";
$MESS["TASKS_REPLICATOR_CREATOR_INACTIVE"] = "A conta do criador da tarefa não existe ou está inativa.";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "Data de término atingida";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "Não foi possível calcular o tempo da próxima execução";
$MESS["TASKS_REPLICATOR_INTERNAL_ERROR"] = "Erro interno. Por favor, entre em contato com a Assistência Técnica.";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Máximo de iterações atingido";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Próxima execução programada em: #TIME# (em #PERIOD# segundos)";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Erro ao calcular o tempo da próxima execução";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "Iteração da tarefa interrompida";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_0"] = "segundo";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_1"] = "segundos";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_2"] = "segundos";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "É possível que alguns dos submodelos sejam autorreferenciados. As subtarefas não foram criadas.";
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Tarefa programada criada";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Tarefa programada criada, houve erros";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "É possível que a tarefa programada não tenha sido criada";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "A tarefa programada não foi criada";
