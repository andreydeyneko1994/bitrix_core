<?php
$MESS["TASKS_REPLICATOR_ACCESS_DENIED"] = "Não é possível criar tarefas usando este modelo porque o criador da tarefa não tem permissões suficientes para atribuir tarefas ao responsável selecionado ou no projeto selecionado. #LINK#";
