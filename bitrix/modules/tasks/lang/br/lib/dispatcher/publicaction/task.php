<?php
$MESS["TASKS_ACTION_RESULT_REQUIRED"] = "O criador da tarefa requer que você forneça um relatório de tarefa.<br> Deixe um comentário sobre a tarefa e marque-o como um resumo do status da tarefa.";
