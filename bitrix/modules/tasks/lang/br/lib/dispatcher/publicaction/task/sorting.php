<?
$MESS["TASKS_SORTING_AUTH_REQUIRED"] = "Você tem que estar autorizado para realizar esta operação.";
$MESS["TASKS_SORTING_WRONG_GROUP_PERMISSIONS"] = "Você não pode classificar tarefas neste grupo";
$MESS["TASKS_SORTING_WRONG_SOURCE_TASK"] = "Tarefa de origem não encontrada";
$MESS["TASKS_SORTING_WRONG_TARGET_TASK"] = "Tarefa adjacente não encontrada";
?>