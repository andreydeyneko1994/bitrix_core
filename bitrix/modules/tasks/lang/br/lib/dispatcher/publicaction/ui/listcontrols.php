<?php
$MESS["TASKS_LISTCONTROLS_ACCESS_DENIED"] = "Você não tem permissão para criar tarefas.";
$MESS["TASKS_LISTCONTROLS_AUTH_REQUIRED"] = "Você precisa estar logado para realizar esta ação.";
