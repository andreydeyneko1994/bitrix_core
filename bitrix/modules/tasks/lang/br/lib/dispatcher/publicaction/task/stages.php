<?
$MESS["STAGES_ERROR_ACCESS_DENIED_GROUP_TASK"] = "Você não pode visualizar tarefas neste grupo";
$MESS["STAGES_ERROR_ACCESS_DENIED_MOVE"] = "Você não pode mover esta tarefa";
$MESS["STAGES_ERROR_ACCESS_DENIED_STAGES"] = "Você não pode gerenciar estágios";
$MESS["STAGES_ERROR_DIFFERENT_STAGES"] = "Estágio e tarefa pertencem a diferentes grupos";
$MESS["STAGES_ERROR_EMPTY_DATA"] = "Nenhum dado de atualização foi fornecido";
$MESS["STAGES_ERROR_EMPTY_TITLE"] = "O título do estágio está faltando";
$MESS["STAGES_ERROR_IS_SYSTEM"] = "O estágio padrão não pode ser excluído";
$MESS["STAGES_ERROR_NOT_FOUND"] = "O estágio não foi encontrado";
$MESS["STAGES_ERROR_NO_EMPTY"] = "Há tarefas neste estágio";
$MESS["STAGES_ERROR_SOCIALNETWORK_IS_NOT_INSTALLED"] = "o módulo rede social não está instalado.";
$MESS["STAGES_ERROR_TASK_NOT_FOUND"] = "A tarefa não foi encontrada ou o acesso é negado.";
?>