<?
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Uma etapa inicial não pode ser excluída. Transfira a etapa antes de excluí-la.";
$MESS["TASKS_STAGE_FINISH"] = "Concluída";
$MESS["TASKS_STAGE_MP_1"] = "Não planejada";
$MESS["TASKS_STAGE_MP_2"] = "Para ser feita esta semana";
$MESS["TASKS_STAGE_NEW"] = "Nova";
$MESS["TASKS_STAGE_PERIOD1"] = "Vencido";
$MESS["TASKS_STAGE_PERIOD2"] = "Vencimento hoje";
$MESS["TASKS_STAGE_PERIOD3"] = "Vencimento esta semana";
$MESS["TASKS_STAGE_PERIOD4"] = "Vencimento na próxima semana";
$MESS["TASKS_STAGE_PERIOD5"] = "Sem prazo";
$MESS["TASKS_STAGE_PERIOD6"] = "Vencimento em duas semanas";
$MESS["TASKS_STAGE_WORK"] = "Em andamento";
?>