<?php
$MESS["TASKS_SPRINT_ERROR_CANT_ADD_TO_SPRINT"] = "Não é possível adicionar uma tarefa ao sprint.";
$MESS["TASKS_SPRINT_ERROR_CANT_CREATE"] = "Você não pode iniciar um novo sprint.";
$MESS["TASKS_SPRINT_ERROR_END_DATE_WRONG"] = "A data de término do sprint deve ser posterior a hoje.";
$MESS["TASKS_STAGE_TL1"] = "Hoje";
$MESS["TASKS_STAGE_TL2"] = "Amanhã";
$MESS["TASKS_STAGE_TL3"] = "Esta semana";
$MESS["TASKS_STAGE_TL4"] = "Este mês";
$MESS["TASKS_STAGE_TL5"] = "Sem prazo";
