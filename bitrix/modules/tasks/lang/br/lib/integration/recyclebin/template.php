<?
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_CONFIRM"] = "O modelo será excluído permanentemente. Você tem certeza de que deseja continuar? ";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_MESSAGE"] = "O modelo foi excluído";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_CONFIRM"] = "Você deseja recuperar o modelo selecionado e movê-lo de volta para a lista?";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_MESSAGE"] = "O modelo foi recuperado";
?>