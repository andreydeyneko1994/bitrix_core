<?
$MESS["TASKS_RECYCLEBIN_REMOVE_CONFIRM"] = "A tarefa será excluída permanentemente. Você tem certeza de que deseja continuar?";
$MESS["TASKS_RECYCLEBIN_REMOVE_MESSAGE"] = "A tarefa foi excluída";
$MESS["TASKS_RECYCLEBIN_RESTORE_CONFIRM"] = "Você deseja recuperar a tarefa selecionada e movê-la de volta para a lista?";
$MESS["TASKS_RECYCLEBIN_RESTORE_MESSAGE"] = "A tarefa foi recuperada";
?>