<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Você já tem um sprint em execução.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Não é possível executar sprint sem tarefas.";
