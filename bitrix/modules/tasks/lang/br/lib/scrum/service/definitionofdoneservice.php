<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_0"] = "Desenvolvimento";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_1"] = "Código-fonte concluído e verificado no repositório VCS";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_2"] = "O código-fonte segue as diretrizes de estilo de código";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_3"] = "Código-fonte testado para erros; erros encontrados corrigidos";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_4"] = "Código-fonte testado para falhas de segurança; problemas encontrados corrigidos";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_5"] = "Funcionalidades e recursos necessários testados para erros; erros encontrados corrigidos";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_6"] = "API testada e compatível com versões anteriores; problemas encontrados de compatibilidade com versões anteriores corrigidos";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_7"] = "Testes automáticos de funcionalidade aprovados";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_8"] = "Código-fonte revisado (inspeção visual rápida de más práticas de codificação)";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_9"] = "Item da lista de pendências aceito pelo proprietário do produto. Se aceitar um item para Demo exigir muito tempo ou esforço, ele deve ser aceito antes da Demo.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_10"] = "Mensagens de texto aprovadas e estáveis enviadas para localização.";
