<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_READ_REQUIRED_OPTION_ERROR"] = "Erro ao ler a opção \"item obrigatório\"";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_SAVE_ITEM_ERROR"] = "Erro ao salvar lista";
$MESS["TASKS_SDC_ERROR_ACCESS_DENIED"] = "Acesso negado.";
$MESS["TASKS_SDC_ERROR_INCLUDE_MODULE_ERROR"] = "Não é possível conectar os módulos necessários.";
$MESS["TASKS_SDC_ERROR_TYPE_NOT_FOUND"] = "Não é possível encontrar o tipo.";
