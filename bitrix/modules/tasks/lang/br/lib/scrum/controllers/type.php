<?php
$MESS["TASKS_STC_ERROR_ACCESS_DENIED"] = "Acesso negado.";
$MESS["TASKS_STC_ERROR_COULD_NOT_CREATE_TYPE"] = "Não é possível criar um novo tipo.";
$MESS["TASKS_STC_ERROR_COULD_NOT_EMPTY_TYPE"] = "Digite o nome";
$MESS["TASKS_STC_ERROR_COULD_NOT_FOUND_TYPE"] = "Não é possível encontrar o tipo.";
$MESS["TASKS_STC_ERROR_INCLUDE_MODULE_ERROR"] = "Não é possível conectar os módulos necessários.";
