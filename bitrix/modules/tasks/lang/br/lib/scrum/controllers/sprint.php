<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Você já tem um sprint em execução.";
$MESS["TASKS_SCRUM_SPRINT_START_ERROR"] = "Erro ao iniciar sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Não é possível executar sprint sem tarefas.";
$MESS["TSSC_ERROR_ACCESS_DENIED"] = "Acesso negado.";
$MESS["TSSC_ERROR_COULD_NOT_READ_ACTIVE_SPRINT"] = "Não é possível concluir o sprint ativo. Pode já ter sido concluído.";
$MESS["TSSC_ERROR_COULD_NOT_READ_SPRINT"] = "Não é possível obter dados para o formulário.";
$MESS["TSSC_ERROR_INCLUDE_MODULE_ERROR"] = "Não é possível conectar os módulos necessários.";
