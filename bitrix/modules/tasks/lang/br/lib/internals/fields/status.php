<?
$MESS["TASKS_STATUS_1"] = "Nova";
$MESS["TASKS_STATUS_2"] = "Pendente";
$MESS["TASKS_STATUS_3"] = "Em andamento";
$MESS["TASKS_STATUS_4"] = "Revisão pendente";
$MESS["TASKS_STATUS_5"] = "Concluída";
$MESS["TASKS_STATUS_6"] = "Adiada";
$MESS["TASKS_STATUS_7"] = "Recusada";
?>