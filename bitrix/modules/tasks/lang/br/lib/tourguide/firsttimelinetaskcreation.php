<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TEXT"] = "O prazo será definido automaticamente. Você sempre pode alterá-lo mais tarde.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TITLE"] = "Clique para criar uma tarefa";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TEXT"] = "O prazo é atribuído automaticamente quando uma tarefa é solta na coluna. O prazo padrão é o final da semana.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TITLE"] = "Mova a tarefa para alterar o prazo";
