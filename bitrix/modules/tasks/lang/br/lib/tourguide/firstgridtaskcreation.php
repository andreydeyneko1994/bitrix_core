<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TEXT"] = "Nenhum prazo será definido para a nova tarefa. Você sempre pode alterá-lo mais tarde.";
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TITLE"] = "Clique para criar uma tarefa";
