<?php
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TEXT"] = "Os contadores vermelhos mostram o número de tarefas atrasadas. Clique para ver.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TITLE"] = "O tempo está quase acabando!";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TEXT"] = "Clique e mova o prazo. O contador será zerado. Os contadores são pequenos ajudantes para alertar você quando o prazo se aproxima.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TITLE"] = "Mover prazo";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_BUTTON"] = "Entendi, obrigado";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TEXT"] = "Observe os contadores para ficar em dia com suas tarefas.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TITLE"] = "Você está indo muito bem, sem contador agora!";
