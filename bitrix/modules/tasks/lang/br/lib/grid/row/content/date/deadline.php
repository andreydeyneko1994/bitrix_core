<?php
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_DEFERRED"] = "Adiado";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_DAY_PLURAL_0"] = "&minus; #TIME# dia";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_DAY_PLURAL_1"] = "&minus; #TIME# dias";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_DAY_PLURAL_2"] = "&minus; #TIME# dias";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_HOUR_PLURAL_0"] = "&minus; #TIME# hora";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_HOUR_PLURAL_1"] = "&minus; #TIME# horas";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_HOUR_PLURAL_2"] = "&minus; #TIME# horas";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MINUTE_PLURAL_0"] = "&minus; #TIME# minuto";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MINUTE_PLURAL_1"] = "&minus; #TIME# minutos";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MINUTE_PLURAL_2"] = "&minus; #TIME# minutos";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MONTH_PLURAL_0"] = "&minus; #TIME# mês";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MONTH_PLURAL_1"] = "&minus; #TIME# meses";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_MONTH_PLURAL_2"] = "&minus; #TIME# meses";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_WEEK_PLURAL_0"] = "&minus; #TIME# semana";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_WEEK_PLURAL_1"] = "&minus; #TIME# semanas";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_WEEK_PLURAL_2"] = "&minus; #TIME# semanas";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_YEAR_PLURAL_0"] = "&minus; #TIME# ano";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_YEAR_PLURAL_1"] = "&minus; #TIME# anos";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_EXPIRED_YEAR_PLURAL_2"] = "&minus; #TIME# anos";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_NO_DEADLINE"] = "Sem prazo";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_SUPPOSEDLY_COMPLETED"] = "Revisão pendente";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_TODAY"] = "Hoje#TIME#";
$MESS["TASKS_GRID_ROW_CONTENT_DEADLINE_STATE_TOMORROW"] = "Amanhã#TIME#";
