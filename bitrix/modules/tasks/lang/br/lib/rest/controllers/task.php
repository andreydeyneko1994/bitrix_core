<?php
$MESS["TASKS_FAILED_RESULT_REQUIRED"] = "O criador da tarefa requer que você forneça um relatório de tarefa.<br> Deixe um comentário sobre a tarefa e marque-o como um resumo do status da tarefa.";
$MESS["TASKS_FAILED_START_TASK_TIMER"] = "Não é possível iniciar o cronômetro de tarefa";
$MESS["TASKS_FAILED_STOP_TASK_TIMER"] = "Não é possível parar o cronômetro de tarefa";
$MESS["TASKS_FAILED_WRONG_ORDER_FIELD"] = "Campo de classificação incorreto";
$MESS["TASKS_OTHER_TASK_ON_TIMER"] = "Outra tarefa (##ID#) está ativa agora. Interrompa antes de continuar.";
