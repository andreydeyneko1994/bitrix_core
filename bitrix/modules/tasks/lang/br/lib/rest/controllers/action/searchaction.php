<?
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED"] = "Para pesquisar todas as tarefas, escolha um plano que se ajuste à quantidade de dados acumulados e à escala de seus negócios. <a onclick=\"top.BX.Helper.show('redirect=detail&code=9745327')\" style=\"cursor: pointer\">Detalhes</a>";
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED_TITLE"] = "Limite de #LIMIT# tarefas atingido";
?>