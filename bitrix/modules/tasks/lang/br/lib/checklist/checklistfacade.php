<?
$MESS["TASKS_CHECKLIST_FACADE_ACTION_ADD"] = "Adicionar item";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_MODIFY"] = "Editar item";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_NOT_ALLOWED"] = "#ACTION_NAME#: ação indisponível";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REMOVE"] = "Excluir item";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REORDER"] = "Mover item";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_TOGGLE"] = "Editar status do item";
$MESS["TASKS_CHECKLIST_FACADE_ATTACHMENT_ADDING_FAILED"] = "Erro ao adicionar anexo";
$MESS["TASKS_CHECKLIST_FACADE_CHECKLIST_DELETE_FAILED"] = "Erro ao excluir item";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_FIELDS"] = "Os campos editáveis não estão especificados";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_TITLE"] = "O nome do item está faltando";
$MESS["TASKS_CHECKLIST_FACADE_MEMBER_DELETE_FAILED"] = "Erro ao excluir participante";
$MESS["TASKS_CHECKLIST_FACADE_NOT_ALLOWED_FIELD"] = "Campo desconhecido passado [\"FIELD_NAME#]";
$MESS["TASKS_CHECKLIST_FACADE_NO_LOOPS_AVAILABLE"] = "O item principal não pode ser um subitem dele mesmo";
$MESS["TASKS_CHECKLIST_FACADE_USER_FIELD_DELETE_FAILED"] = "Erro ao excluir campo personalizado";
$MESS["TASKS_CHECKLIST_FACADE_WRONG_MEMBER_TYPE"] = "Tipo de usuário desconhecido passado [#TYPE#]";
?>