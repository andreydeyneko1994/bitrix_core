<?
$MESS["TASKS_CHECKLIST_TREE_CHILD_ATTACH"] = "Não é possível criar referência circular [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_EXISTING_NODE_ADDING"] = "Não é possível adicionar o nó porque ele já existe [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_ILLEGAL_NODE"] = "ID do nó incorreto [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_NODE_NOT_FOUND"] = "O nó especificado não foi encontrado [#ID#]";
$MESS["TASKS_CHECKLIST_TREE_PATH_EXISTS"] = "Nós já vinculados [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_SELF_ATTACH"] = "Não é possível anexar o nó a ele mesmo [#ID#, #PARENT_ID#]";
?>