<?php
$MESS["MAIN_KANBAN_NOTIFY_CHANGE_DEADLINE"] = "La date limite de la tâche a été modifiée : #date#";
$MESS["MAIN_KANBAN_NOTIFY_REMOVE_DEADLINE"] = "La date limite de la tâche a été supprimée";
$MESS["MAIN_KANBAN_TITLE_PLACEHOLDER"] = "Intitulé #tag";
$MESS["TASKS_KANBAN_DIABLE_SORT_TOOLTIP"] = "Vous n'avez pas les autorisations pour modifier les préférences du projet";
$MESS["TASKS_KANBAN_ITEM_STORY_POINTS_TITLE"] = "Points d'histoire";
$MESS["TASKS_KANBAN_ME_DISABLE_COMPLETE"] = "La tâche a été fermée. Reprenez-la pour la déplacer vers une autre colonne.";
$MESS["TASKS_KANBAN_ME_DISABLE_DEADLINE"] = "Les dates de la tâche ne peuvent pas être modifiées sans changer la date limite";
$MESS["TASKS_KANBAN_ME_DISABLE_DEADLINE_PART"] = "La date limite de la tâche restreint la plage de dates possible";
$MESS["TASKS_KANBAN_ME_DISABLE_DEADLINE_PART2"] = "Impossible de déplacer cet élément à cause de la date limite de la tâche";
$MESS["TASKS_KANBAN_ME_DISABLE_FROM_OVERDUE"] = "Modifier la date limite de la tâche ou la fermer avant de la déplacer";
$MESS["TASKS_KANBAN_MORE_USERS"] = "plus";
$MESS["TASKS_KANBAN_NOTIFY_BUTTON"] = "Envoyer un message";
$MESS["TASKS_KANBAN_NOTIFY_HEADER"] = "Autorisations insuffisantes pour modifier les étapes.";
$MESS["TASKS_KANBAN_NOTIFY_TEXT"] = "Seul l'administrateur de votre Bitrix24 ou le chef de projet peut créer une nouvelle étape. Veuillez lui envoyer un message ou lui parler afin de pouvoir ajouter les étapes de la tâche dont vous avez besoin.";
$MESS["TASKS_KANBAN_NOTIFY_TITLE"] = "Autorisations insuffisantes";
$MESS["TASKS_KANBAN_NO_DATE"] = "Pas de date limite";
$MESS["TASKS_KANBAN_PANEL_ADDACCOMPLICETASK"] = "Participant affecté aux tâches : #entityName#";
$MESS["TASKS_KANBAN_PANEL_ADDAUDITORTASK"] = "Observateur affecté aux tâches : #entityName#";
$MESS["TASKS_KANBAN_PANEL_ADDFAVORITETASK"] = "Tâches ajoutées aux favoris";
$MESS["TASKS_KANBAN_PANEL_CHANGEAUTHORTASK"] = "Créateur affecté aux tâches : #entityName#";
$MESS["TASKS_KANBAN_PANEL_CHANGEGROUPTASK"] = "Groupe de travail affecté aux tâches : #entityName#";
$MESS["TASKS_KANBAN_PANEL_COMPLETE"] = "Terminer";
$MESS["TASKS_KANBAN_PANEL_COMPLETETASK"] = "Tâches accomplies";
$MESS["TASKS_KANBAN_PANEL_CONFIRM_MESS_COMPLETE"] = "Voulez-vous vraiment terminer les tâches sélectionnées ?";
$MESS["TASKS_KANBAN_PANEL_CONFIRM_MESS_DELETE"] = "Voulez-vous vraiment supprimer les tâches sélectionnées ?";
$MESS["TASKS_KANBAN_PANEL_CONFIRM_TITLE"] = "Confirmer l'action";
$MESS["TASKS_KANBAN_PANEL_DEADLINE"] = "Date limite";
$MESS["TASKS_KANBAN_PANEL_DEADLINETASK"] = "Date limite définie pour les tâches : #deadline#";
$MESS["TASKS_KANBAN_PANEL_DELEGATETASK"] = "Responsable affecté aux tâches : #entityName#";
$MESS["TASKS_KANBAN_PANEL_DELETE"] = "Supprimer";
$MESS["TASKS_KANBAN_PANEL_DELETEFAVORITETASK"] = "Tâches supprimées des favoris";
$MESS["TASKS_KANBAN_PANEL_DELETETASK"] = "Tâches supprimées";
$MESS["TASKS_KANBAN_PANEL_FAVORITE"] = "Favoris";
$MESS["TASKS_KANBAN_PANEL_FAVORITE_ADD"] = "Ajouter aux favoris";
$MESS["TASKS_KANBAN_PANEL_FAVORITE_REMOVE"] = "Supprimer des favoris";
$MESS["TASKS_KANBAN_PANEL_GROUP"] = "Configurer le groupe de travail";
$MESS["TASKS_KANBAN_PANEL_MEMBERS"] = "Participants";
$MESS["TASKS_KANBAN_PANEL_MEMBERS_AUDITOR"] = "Ajouter un observateur";
$MESS["TASKS_KANBAN_PANEL_MEMBERS_CORESPONSE"] = "Ajouter un collaborateur";
$MESS["TASKS_KANBAN_PANEL_MEMBERS_CREATED"] = "Changer le créateur";
$MESS["TASKS_KANBAN_PANEL_MEMBERS_RESPONSE"] = "Changer le responsable";
$MESS["TASKS_KANBAN_PANEL_MOVETASK"] = "Tâches déplacées dans la colonne #columnName#";
$MESS["TASKS_KANBAN_PANEL_STAGE"] = "Étape";
$MESS["TASKS_KANBAN_STATUS_COMPLETED"] = "Terminée";
$MESS["TASKS_KANBAN_STATUS_COMPLETED_SUPPOSEDLY"] = "En attente de passage en revue";
$MESS["TASKS_KANBAN_STATUS_DEFERRED"] = "Reportée";
$MESS["TASKS_KANBAN_STATUS_NEW"] = "Nouveau";
$MESS["TASKS_KANBAN_STATUS_OVERDUE"] = "Dépassé";
$MESS["TASKS_KANBAN_STATUS_PAUSE"] = "Pause";
$MESS["TASKS_KANBAN_STATUS_PROGRESS"] = "En cours";
$MESS["TASKS_KANBAN_TITLE_CHECKLIST"] = "Liste de vérification (#complete# terminés de #all#)";
$MESS["TASKS_KANBAN_TITLE_COMMENTS"] = "Commentaires (#count#)";
$MESS["TASKS_KANBAN_TITLE_COMPLETE"] = "Terminer la tâche";
$MESS["TASKS_KANBAN_TITLE_DEADLINE"] = "Date limite";
$MESS["TASKS_KANBAN_TITLE_DEADLINE_SET"] = "Définir la date limite";
$MESS["TASKS_KANBAN_TITLE_FILES"] = "Fichiers (#count#)";
$MESS["TASKS_KANBAN_TITLE_MUTE"] = "Sourdine";
$MESS["TASKS_KANBAN_TITLE_PAUSE"] = "Suspendre la tâche";
$MESS["TASKS_KANBAN_TITLE_START"] = "Démarrer la tâche";
$MESS["TASKS_KANBAN_TITLE_UNMUTE"] = "Désactiver la sourdine";
$MESS["TASKS_KANBAN_WO_GROUP_LABEL"] = "Groupe (projet)";
$MESS["TASKS_KANBAN_WO_GROUP_VALUE"] = "aucun";
$MESS["TASKS_SCRUM_KANBAN_PARENT_COMPLETE_CANCEL_CAPTION"] = "Ne pas terminer";
$MESS["TASKS_SCRUM_KANBAN_PARENT_COMPLETE_MESSAGE"] = "Toutes les sous-tâches de \"<b>#name#</b>\" ont été accomplies. <br><br>Voulez-vous terminer la tâche \"<b>#name#</b>\" ?";
$MESS["TASKS_SCRUM_KANBAN_PARENT_COMPLETE_MESSAGE_1"] = "Toutes les sous-tâches de \"<b>#name#</b>\" ont été terminées.<br><br>Voulez-vous fermer la tâche \"<b>#name#</b>\" ou continuer à travailler dessus ?";
$MESS["TASKS_SCRUM_KANBAN_PARENT_COMPLETE_OK_CAPTION"] = "Terminer";
$MESS["TASKS_SCRUM_KANBAN_PARENT_COMPLETE_PROCEED_CAPTION"] = "Continuer";
$MESS["TASKS_SCRUM_KANBAN_PARENT_RENEW_CANCEL_CAPTION"] = "Ne pas reprendre";
$MESS["TASKS_SCRUM_KANBAN_PARENT_RENEW_MESSAGE"] = "La sous-tâche \"#sub-name#\" a été reprise. <br><br>Voulez-vous reprendre la tâche \"<b>#name#</b>\" ?";
$MESS["TASKS_SCRUM_KANBAN_PARENT_RENEW_OK_CAPTION"] = "Reprendre";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Vous utilisez déjà le tracker de temps pour \"{{TITLE}}\". Cette tâche va être suspendue. Continuer ?";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Le tracker de temps est pour l'instant utilisé avec une autre tâche.";
