<?
$MESS["SCHEDULER_PRINT_ALERT_BUTTON_TITLE"] = "Oui";
$MESS["SCHEDULER_PRINT_ALERT_TEXT"] = "Avez-vous fini d'imprimer ?";
$MESS["SCHEDULER_PRINT_BORDER_TITLE"] = "Ajouter une bordure";
$MESS["SCHEDULER_PRINT_BUTTON_TITLE"] = "Imprimer";
$MESS["SCHEDULER_PRINT_CANCEL_BUTTON_TITLE"] = "Annuler";
$MESS["SCHEDULER_PRINT_DATE_FROM_TITLE"] = "Date de début";
$MESS["SCHEDULER_PRINT_DATE_TO_TITLE"] = "Date de fin";
$MESS["SCHEDULER_PRINT_FIT_TO_PAGE"] = "Ajuster à la page";
$MESS["SCHEDULER_PRINT_FORMAT_TITLE"] = "Affichage";
$MESS["SCHEDULER_PRINT_LANDSCAPE_TITLE"] = "Paysage";
$MESS["SCHEDULER_PRINT_ORIENTATION_TITLE"] = "Orientation";
$MESS["SCHEDULER_PRINT_PORTRAIT_TITLE"] = "Portrait";
$MESS["SCHEDULER_PRINT_SETTINGS_TITLE"] = "Configuration de l'impression";
$MESS["SCHEDULER_PRINT_TOO_MANY_PAGES"] = "Trop de pages à imprimer (#NUMBER#). Veuillez réduire la plage de dates ou l'échelle du graphique.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_FROM"] = "La date de début est en dehors de la plage de dates.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_RANGE"] = "Plage de dates incorrecte.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_TO"] = "La date de fin est en dehors de la plage de dates.";
?>