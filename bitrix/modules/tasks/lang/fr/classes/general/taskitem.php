<?php
$MESS["TASKS_ACCESS_DENIED_TO_CREATOR_UPDATE"] = "Impossible de modifier le créateur de la tâche à cause de permissions insuffisantes";
$MESS["TASKS_ACCESS_DENIED_TO_DEADLINE_UPDATE"] = "Impossible de modifier la date limite à cause de permissions insuffisantes";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_DELETE"] = "Impossible de supprimer la tâche parce que son accès est refusé";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_UPDATE"] = "Impossible d'éditer la tâche à cause de permissions insuffisantes";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "La date limite de la tâche est en dehors de la plage de dates du projet";
$MESS["TASKS_ERROR_TASK_ADD"] = "Impossible de créer la tâche";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Impossible de lier une tâche à sa tâche parente";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "La date limite de la tâche prévue est en dehors de la plage de dates du projet";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "La date de début de la tâche prévue est en dehors de la plage de dates du projet";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Votre période d'essai a expiré";
$MESS["TASK_CANT_ADD_LINK"] = "Impossible de lier les tâches";
$MESS["TASK_CANT_DELETE_LINK"] = "Impossible de délier les tâches";
$MESS["TASK_CANT_UPDATE_LINK"] = "Impossible de mettre à jour le lien d'une tâche";
