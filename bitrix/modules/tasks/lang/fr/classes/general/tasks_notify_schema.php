<?php
$MESS["TASKS_NS_COMMENT"] = "Nouveau commentaire de la tâche";
$MESS["TASKS_NS_MANAGE"] = "Créer/modifier une tâche";
$MESS["TASKS_NS_REMINDER"] = "Rappel de tâche";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "Notification d'attribution de tâche";
$MESS["TASKS_NS_TASK_EXPIRED_SOON"] = "Tâche presque en retard";
