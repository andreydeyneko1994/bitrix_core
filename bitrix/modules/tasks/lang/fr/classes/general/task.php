<?
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "La date limite de la tâche est en dehors de la plage de dates du projet";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "Co-exécuteurs";
$MESS["TASKS_MESSAGE_AUDITORS"] = "Auditeurs";
$MESS["TASKS_MESSAGE_DEADLINE"] = "Date limite";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "Description";
$MESS["TASKS_MESSAGE_NO"] = "non";
$MESS["TASKS_MESSAGE_PRIORITY"] = "Criticité";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_MESSAGE_TITLE"] = "Dénomination";
$MESS["TASKS_NEW_TASK"] = "Ajouter une tâche";
$MESS["TASKS_NEW_TASK_MESSAGE"] = "Une nouvelle tâche a été ajoutée

Nom : #TASK_TITLE#
Auteur : #TASK_AUTHOR#
Responsable : #TASK_RESPONSIBLE#
#TASK_EXTRA#
Affichage de tâche :
#PATH_TO_TASK#";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "La date limite de la tâche prévue est en dehors de la plage de dates du projet";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "La date de début de la tâche prévue est en dehors de la plage de dates du projet";
?>