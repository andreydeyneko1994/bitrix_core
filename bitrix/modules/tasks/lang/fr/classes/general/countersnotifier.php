<?
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_0"] = "[B]#TASKS_COUNT# votre tâche[/B] contient une remarques ou exige une réaction.
[URL=#HREF#]Accéder aux tâches pour prendre connaissance[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_1"] = "Dans [B]#TASKS_COUNT# vos tâches [/B] contiennent des remarques ou exigent une réaction.
[URL=#HREF#] Accéder aux tâches pour prendre connaissance [/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_2"] = "[B]#TASKS_COUNT# de vos tâches [/B] contiennent des remarques ou exigent une modification.
[URL=#HREF#]Accéder aux tâches pour prendre connaissance [/URL].";
$MESS["TASKS_COUNTERS_NOTICE_TITLE"] = "Assistant du portail";
?>