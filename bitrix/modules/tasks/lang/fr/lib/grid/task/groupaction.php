<?php
$MESS["TASKS_LIST_CHOOSE_ACTION"] = "Sélectionner une action";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_ACCOMPLICE"] = "Ajouter un participant";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_AUDITOR"] = "Ajouter un observateur";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_FAVORITE"] = "Ajouter aux favoris";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_ORIGINATOR"] = "Changer le créateur";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_RESPONSIBLE"] = "Changer le responsable";
$MESS["TASKS_LIST_GROUP_ACTION_COMPLETE"] = "Terminer";
$MESS["TASKS_LIST_GROUP_ACTION_DELETE_FAVORITE"] = "Supprimer des favoris";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_DAY"] = "jour";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_MONTH"] = "mois";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_WEEK"] = "semaine";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_LEFT"] = "Reculer l'échéance";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_RIGHT"] = "Avancer l'échéance";
$MESS["TASKS_LIST_GROUP_ACTION_MUTE"] = "Sourdine";
$MESS["TASKS_LIST_GROUP_ACTION_PING"] = "Ping";
$MESS["TASKS_LIST_GROUP_ACTION_REMOVE"] = "Supprimer";
$MESS["TASKS_LIST_GROUP_ACTION_SET_DEADLINE"] = "Définir une échéance";
$MESS["TASKS_LIST_GROUP_ACTION_SET_GROUP"] = "Définir un groupe (projet)";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_NO"] = "Non";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_V2"] = "Désactiver le contrôle à la fin du processus";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_YES"] = "Oui";
$MESS["TASKS_LIST_GROUP_ACTION_UNMUTE"] = "Désactiver la sourdine";
