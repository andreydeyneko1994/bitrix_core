<?
$MESS["TASKS_STATUS_1"] = "Nouvelle";
$MESS["TASKS_STATUS_2"] = "En attente";
$MESS["TASKS_STATUS_3"] = "En cours";
$MESS["TASKS_STATUS_4"] = "En attente de contrôle";
$MESS["TASKS_STATUS_5"] = "Terminée";
$MESS["TASKS_STATUS_6"] = "Repoussée";
$MESS["TASKS_STATUS_7"] = "Refusée";
?>