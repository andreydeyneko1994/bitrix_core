<?php
$MESS["HINT_TASK_CLOSED_DEPARTMENT_EDIT"] = "Modifier les tâches terminées de son propre service";
$MESS["HINT_TASK_CLOSED_NON_DEPARTMENT_EDIT"] = "Un utilisateur peut modifier toutes les tâches des autres services, tant qu'il peut les voir, même une fois la tâche terminée. Un superviseur hérite de la permission de modifier les tâches de ses employés. Un superviseur peut modifier une tâche si elle lui a été assignée par des employés.";
$MESS["HINT_TASK_DEPARTMENT_DELETE"] = "Un utilisateur peut modifier toutes les tâches de son service, tant qu'il peut les voir, même une fois la tâche terminée.";
$MESS["HINT_TASK_DEPARTMENT_DIRECT"] = "Employés de son propre service et sous-département";
$MESS["HINT_TASK_DEPARTMENT_EDIT"] = "Un utilisateur peut modifier toutes les tâches de son service, tant qu'il peut les voir.";
$MESS["HINT_TASK_DEPARTMENT_VIEW"] = "Les tâches d'un participant sont toujours disponible pour lui, quel que soit le statut de l'option.";
$MESS["HINT_TASK_RESPONSE_ASSIGN"] = "Ajouter un participant nécessaire la permission d'assigner une tâche à une personne en particulier.";
$MESS["HINT_TASK_RESPONSE_CHANGE_RESPONSIBLE"] = "Par défaut, un responsable peut réassigner des tâches à des employés subordonnés. Les tâches peuvent être réassignées aux superviseurs ou aux employés d'autres services si la permission d'assigner des tâches existe pour une personne en particulier.";
$MESS["HINT_TASK_RESPONSE_CHECKLIST_ADD"] = "Un utilisateur peut ajouter ou modifier les éléments d'une liste de contrôle. Cette permission affecte les éléments ajoutés par d'autres utilisateurs.";
$MESS["HINT_TASK_RESPONSE_CHECKLIST_EDIT"] = "Modifier tous les éléments d'une liste de contrôle. Le responsable peut ouvrir et fermer les éléments d'une liste de contrôle, quel que soit le statut de cette option.";
$MESS["HINT_TASK_RESPONSE_DELEGATE"] = "Par défaut, les tâches peuvent être déléguées à des employés subordonnés. Les tâches peuvent être déléguées aux superviseurs ou aux employés d'autres services si la permission d'assigner des tâches existe pour une personne en particulier.";
$MESS["HINT_TASK_RESPONSE_EDIT"] = "Un superviseur hérite de la permission de modifier les tâches de ses employés. Un superviseur peut modifier une tâche si elle lui a été assignée par des employés.";
$MESS["TASK_ACCESS_MANAGE"] = "Gérer les droits d'accès";
$MESS["TASK_CLOSED_DEPARTMENT_EDIT"] = "Modifier les tâches terminées de son propre service";
$MESS["TASK_CLOSED_DIRECTOR_EDIT"] = "Créateur : modifier une tâche terminée";
$MESS["TASK_CLOSED_NON_DEPARTMENT_EDIT"] = "Modifier les tâches terminées des autres services";
$MESS["TASK_DEPARTMENT_DELETE"] = "Supprimer les tâches disponibles de son propre service";
$MESS["TASK_DEPARTMENT_DIRECT"] = "Assigner une tâche aux employés de son propre service";
$MESS["TASK_DEPARTMENT_EDIT"] = "Modifier les tâches disponibles de son propre service";
$MESS["TASK_DEPARTMENT_MANAGER_DIRECT"] = "Assigner une tâche au superviseur de son propre service";
$MESS["TASK_DEPARTMENT_VIEW"] = "Afficher les tâches de son propre service";
$MESS["TASK_DIRECTOR_DELETE"] = "Créateur : supprimer la tâche";
$MESS["TASK_EXPORT"] = "Exporter les tâches accessibles";
$MESS["TASK_IMPORT"] = "Importer les tâches";
$MESS["TASK_NON_DEPARTMENT_DELETE"] = "Supprimer les tâches disponibles des autres services";
$MESS["TASK_NON_DEPARTMENT_DIRECT"] = "Assigner une tâche aux employés des autres services";
$MESS["TASK_NON_DEPARTMENT_EDIT"] = "Modifier les tâches disponibles des autres services";
$MESS["TASK_NON_DEPARTMENT_MANAGER_DIRECT"] = "Assigner une tâche au superviseur des autres services";
$MESS["TASK_NON_DEPARTMENT_VIEW"] = "Afficher les tâches des autres services";
$MESS["TASK_RESPONSE_ASSIGN"] = "Responsable : ajouter des participants";
$MESS["TASK_RESPONSE_CHANGE_RESPONSIBLE"] = "Responsable : changer le responsable";
$MESS["TASK_RESPONSE_CHECKLIST_ADD"] = "Responsable : ajouter de nouveaux éléments à la liste de contrôle";
$MESS["TASK_RESPONSE_CHECKLIST_EDIT"] = "Responsable : modifier la liste de contrôle";
$MESS["TASK_RESPONSE_DELEGATE"] = "Responsable : déléguer la tâche";
$MESS["TASK_RESPONSE_EDIT"] = "Responsable : modifier la tâche";
$MESS["TASK_ROBOT_EDIT"] = "Créer des règles d'automatisation pour ses propres tâches";
$MESS["TEMPLATE_CREATE"] = "Créer des modèles de tâche";
$MESS["TEMPLATE_DEPARTMENT_EDIT"] = "Modifier les modèles de son propre service";
$MESS["TEMPLATE_DEPARTMENT_VIEW"] = "Afficher les modèles de son propre service";
$MESS["TEMPLATE_FULL"] = "Accès complet";
$MESS["TEMPLATE_NON_DEPARTMENT_EDIT"] = "Modifier les modèles des autres services";
$MESS["TEMPLATE_NON_DEPARTMENT_VIEW"] = "Afficher les modèles des autres services";
$MESS["TEMPLATE_REMOVE"] = "Supprimer les modèles accessibles";
$MESS["TEMPLATE_VIEW"] = "Lecture seule";
