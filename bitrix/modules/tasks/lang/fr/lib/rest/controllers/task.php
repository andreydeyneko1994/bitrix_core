<?php
$MESS["TASKS_FAILED_RESULT_REQUIRED"] = "Le créateur de la tâche vous demande de fournir un rapport de tâche.<br>Laissez un commentaire sur la tâche et marquez-le comme résumé de l'état de la tâche.";
$MESS["TASKS_FAILED_START_TASK_TIMER"] = "Impossible de démarrer le minuteur des tâches";
$MESS["TASKS_FAILED_STOP_TASK_TIMER"] = "Impossible d'arrêter le minuteur des tâches";
$MESS["TASKS_FAILED_WRONG_ORDER_FIELD"] = "Champ de tri incorrect";
$MESS["TASKS_OTHER_TASK_ON_TIMER"] = "Une autre tâche (##ID#) est actuellement active. Veuillez l'arrêter avant de poursuivre.";
