<?
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED"] = "Pour rechercher toutes les tâches, choisissez une offre qui correspond au montant de données accumulées et l'échelle de votre entreprise. <a onclick=\"top.BX.Helper.show('redirect=detail&code=9745327')\" style=\"cursor: pointer\">Détails</a>";
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED_TITLE"] = "Limite de #LIMIT# tâches atteinte";
?>