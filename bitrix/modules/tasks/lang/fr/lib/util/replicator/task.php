<?php
$MESS["TASKS_REPLICATOR_ACCESS_DENIED"] = "Impossible de créer une tâche à l'aide de ce modèle car le créateur de la tâche n'a pas les autorisations suffisantes pour attribuer des tâches au responsable sélectionné ou dans le projet sélectionné. #LINK#";
