<?
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TEXT"] = "
	<p>Nous nous efforçons d'améliorer la qualité et les performances du service pour l'adapter à votre échelle commerciale. La quantité de données augmente à mesure que votre entreprise se développe : de nouvelles tâches, offres et autres entités sont créées chaque jour.</p>
	<p>Plus l'échelle de votre entreprise est grande, plus vous créez de données à rechercher. Choisissez une offre qui correspond à vos données pour rechercher plus rapidement.</p>";
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TITLE"] = "Limite de tâches (#LIMIT#) atteinte";
?>