<?php
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION"] = "Vous êtes un vrai pro ! Vous avez maintenant #COUNT# tâches dans votre Bitrix24. Attention : vous ne pourrez rechercher des tâches que sur les offres commerciales lorsque leur nombre dépasse #LIMIT#. La recherche d'un si grand nombre de tâches nécessitera des ressources considérables, c'est pourquoi seuls les filtres prédéfinis peuvent être utilisés avec l'offre gratuite. #HELPDESK_LINK#";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION_HELPDESK_LINK"] = "Détails";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT"] = "
	<p>Nous nous efforçons d'améliorer la qualité et les performances du service pour l'adapter à votre échelle commerciale. La quantité de données augmente à mesure que votre entreprise se développe : de nouvelles tâches, offres et autres entités sont créées chaque jour.</p>
	<p>Plus l'échelle de votre entreprise est grande, plus vous créez de données à rechercher. Choisissez une offre qui correspond à vos données pour rechercher plus rapidement.</p>";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT_V2"] = "<p>La limite de recherche de tâches (#LIMIT#) a été atteinte. Nous nous efforçons d'améliorer la qualité et les performances du service pour l'adapter à votre échelle commerciale. La quantité de données augmente à mesure que votre entreprise se développe : de nouvelles tâches, offres et autres entités sont créées chaque jour.</p>
<p>Plus votre entreprise est grande, plus vous créez de données à rechercher. Choisissez une offre qui correspond à vos données pour rechercher plus rapidement.</br>#HELPDESK_LINK#</p>
";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_HELPDESK_LINK"] = "Détails";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE"] = "Limite de tâches (#LIMIT#) atteinte";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE_V2"] = "Limite de recherche atteinte";
