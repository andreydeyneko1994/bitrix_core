<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Vous avez déjà un sprint actif.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Impossible d'exécuter le sprint sans tâche.";
