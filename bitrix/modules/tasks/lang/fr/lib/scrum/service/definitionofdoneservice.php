<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_0"] = "Développement";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_1"] = "Code source terminé et enregistré dans le dépôt VCS";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_2"] = "Le code source respecte les directives de style de code";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_3"] = "Une recherche d'erreurs a été effectuée sur le code source ; les erreurs trouvées ont été corrigées";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_4"] = "Une recherche de failles de sécurité a été effectuée sur le code source ; les problèmes trouvés ont été corrigés";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_5"] = "Une recherche d'erreurs a été effectuée sur les caractéristiques et fonctionnalités requises ; les erreurs trouvées ont été corrigées";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_6"] = "L'API a été testée et jugée rétrocompatible ; les problèmes de rétrocompatibilité trouvés ont été corrigés";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_7"] = "Tests automatiques de fonctionnalité réussis";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_8"] = "Code source examiné (inspection visuelle rapide pour détecter les mauvaises pratiques de codage)";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_9"] = "Élément du backlog accepté par le propriétaire du produit. Si l'acceptation d'un élément pour la démo demande beaucoup de temps ou d'efforts, il doit être accepté avant la démo.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_10"] = "Messages texte approuvés et stables envoyés en localisation.";
