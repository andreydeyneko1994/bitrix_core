<?php
$MESS["TASKS_CC_ERROR_ACCESS_DENIED"] = "Accès refusé.";
$MESS["TASKS_CC_ERROR_INCLUDE_MODULE_ERROR"] = "Impossible de connecter les modules requis.";
$MESS["TSC_CHAT_MESSAGE"] = "Chat \"#EVENT_TITLE#\"";
