<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_READ_REQUIRED_OPTION_ERROR"] = "Erreur lors de la lecture de l'option \"item-required\"";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_SAVE_ITEM_ERROR"] = "Erreur lors de l'enregistrement de la liste";
$MESS["TASKS_SDC_ERROR_ACCESS_DENIED"] = "Accès refusé.";
$MESS["TASKS_SDC_ERROR_INCLUDE_MODULE_ERROR"] = "Impossible de connecter les modules requis.";
$MESS["TASKS_SDC_ERROR_TYPE_NOT_FOUND"] = "Type introuvable.";
