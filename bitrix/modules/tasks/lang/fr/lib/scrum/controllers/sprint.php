<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Vous avez déjà un sprint actif.";
$MESS["TASKS_SCRUM_SPRINT_START_ERROR"] = "Erreur lors du lancement du sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Impossible d'exécuter le sprint sans tâche.";
$MESS["TSSC_ERROR_ACCESS_DENIED"] = "Accès refusé.";
$MESS["TSSC_ERROR_COULD_NOT_READ_ACTIVE_SPRINT"] = "Impossible de terminer le sprint actif. Il a peut-être déjà été terminé.";
$MESS["TSSC_ERROR_COULD_NOT_READ_SPRINT"] = "Impossible de récupérer les données pour le formulaire.";
$MESS["TSSC_ERROR_INCLUDE_MODULE_ERROR"] = "Impossible de connecter les modules requis.";
