<?php
$MESS["TASKS_STC_ERROR_ACCESS_DENIED"] = "Accès refusé.";
$MESS["TASKS_STC_ERROR_COULD_NOT_CREATE_TYPE"] = "Impossible de créer un nouveau type.";
$MESS["TASKS_STC_ERROR_COULD_NOT_EMPTY_TYPE"] = "Saisissez un nom";
$MESS["TASKS_STC_ERROR_COULD_NOT_FOUND_TYPE"] = "Type introuvable.";
$MESS["TASKS_STC_ERROR_INCLUDE_MODULE_ERROR"] = "Impossible de connecter les modules requis.";
