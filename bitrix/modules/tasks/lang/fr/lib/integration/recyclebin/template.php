<?
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_CONFIRM"] = "Le modèle sera définitivement supprimé. Voulez-vous vraiment poursuivre ? ";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_MESSAGE"] = "Le modèle a été supprimé";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_CONFIRM"] = "Voulez-vous récupérer le modèle sélectionné et le renvoyer dans la liste ?";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_MESSAGE"] = "Le modèle a été récupéré";
?>