<?
$MESS["TASKS_RECYCLEBIN_REMOVE_CONFIRM"] = "La tâche sera définitivement supprimée. Voulez-vous vraiment poursuivre ?";
$MESS["TASKS_RECYCLEBIN_REMOVE_MESSAGE"] = "La tâche a été supprimée";
$MESS["TASKS_RECYCLEBIN_RESTORE_CONFIRM"] = "Voulez-vous récupérer la tâche sélectionnée et la renvoyer dans la liste ?";
$MESS["TASKS_RECYCLEBIN_RESTORE_MESSAGE"] = "La tâche a été récupérée";
?>