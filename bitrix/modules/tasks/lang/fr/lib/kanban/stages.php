<?
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Une étape initiale ne peut être supprimée. Déplacez l'étape avant de la supprimer.";
$MESS["TASKS_STAGE_FINISH"] = "Terminé";
$MESS["TASKS_STAGE_MP_1"] = "Non planifié";
$MESS["TASKS_STAGE_MP_2"] = "À faire cette semaine";
$MESS["TASKS_STAGE_NEW"] = "Nouveau";
$MESS["TASKS_STAGE_PERIOD1"] = "Retard";
$MESS["TASKS_STAGE_PERIOD2"] = "À terminer aujourd'hui";
$MESS["TASKS_STAGE_PERIOD3"] = "À terminer cette semaine";
$MESS["TASKS_STAGE_PERIOD4"] = "À terminer la semaine prochaine";
$MESS["TASKS_STAGE_PERIOD5"] = "Pas de date limite";
$MESS["TASKS_STAGE_PERIOD6"] = "À terminer dans deux semaines";
$MESS["TASKS_STAGE_WORK"] = "En cours";
?>