<?php
$MESS["TASKS_SPRINT_ERROR_CANT_ADD_TO_SPRINT"] = "Impossible d'ajouter une tâche au sprint.";
$MESS["TASKS_SPRINT_ERROR_CANT_CREATE"] = "Vous ne pouvez pas commencer un nouveau sprint.";
$MESS["TASKS_SPRINT_ERROR_END_DATE_WRONG"] = "La date de fin du sprint doit être postérieure à aujourd'hui.";
$MESS["TASKS_STAGE_TL1"] = "Aujourd'hui";
$MESS["TASKS_STAGE_TL2"] = "Demain";
$MESS["TASKS_STAGE_TL3"] = "Cette semaine";
$MESS["TASKS_STAGE_TL4"] = "Ce mois-ci";
$MESS["TASKS_STAGE_TL5"] = "Pas de date limite";
