<?php
$MESS["TASKS_ACTION_RESULT_REQUIRED"] = "Le créateur de la tâche vous demande de fournir un rapport de tâche.<br>Laissez un commentaire sur la tâche et marquez-le comme résumé de l'état de la tâche.";
