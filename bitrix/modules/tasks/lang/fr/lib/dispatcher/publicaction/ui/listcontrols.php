<?php
$MESS["TASKS_LISTCONTROLS_ACCESS_DENIED"] = "Vous n'avez pas de droits de créer des tâches.";
$MESS["TASKS_LISTCONTROLS_AUTH_REQUIRED"] = "Vous devez être connecté(e) pour effectuer cette action.";
