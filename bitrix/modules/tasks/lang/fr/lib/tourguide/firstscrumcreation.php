<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_SCRUM_CREATION_POPUP_0_TEXT"] = "Vous pouvez commencer à travailler avec votre équipe scrum une fois le scrum créé.";
$MESS["TASKS_TOUR_GUIDE_FIRST_SCRUM_CREATION_POPUP_0_TITLE"] = "Cliquez pour créer un scrum";
