<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TEXT"] = "L'échéance sera automatiquement définie. Vous pouvez toujours la modifier ultérieurement.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TITLE"] = "Cliquez ici pour créer une tâche";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TEXT"] = "L'échéance est attribuée automatiquement lorsqu'une tâche est déposée sur la colonne. L'échéance par défaut est la fin de la semaine.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TITLE"] = "Déplacez la tâche pour modifier l'échéance";
