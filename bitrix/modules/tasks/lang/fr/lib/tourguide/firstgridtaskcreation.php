<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TEXT"] = "Lors de la création, vous pourrez définir une échéance. Vous pourrez toujours la modifier ultérieurement.";
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TITLE"] = "Cliquez ici pour créer une tâche";
