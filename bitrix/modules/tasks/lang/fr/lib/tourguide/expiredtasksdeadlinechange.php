<?php
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TEXT"] = "Les compteurs rouges indiquent le nombre de tâches en retard. Cliquez pour les visualiser.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TITLE"] = "Le temps est presque écoulé !";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TEXT"] = "Cliquez et déplacez l'échéance. Le compteur se remet à zéro. Les compteurs sont vos petits assistants qui vous avertissent de l'approche de l'échéance.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TITLE"] = "Déplacer l'échéance";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_BUTTON"] = "Compris, merci";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TEXT"] = "Surveillez vos compteurs pour être dans les temps au niveau de vos tâches.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TITLE"] = "Vous vous en sortez bien, pas de compteur pour le maintenant !";
