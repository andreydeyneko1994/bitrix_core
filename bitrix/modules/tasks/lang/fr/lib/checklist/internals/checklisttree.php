<?
$MESS["TASKS_CHECKLIST_TREE_CHILD_ATTACH"] = "Impossible de créer une référence circulaire [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_EXISTING_NODE_ADDING"] = "Impossible d'ajouter un nœud car il existe déjà [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_ILLEGAL_NODE"] = "ID de nœud incorrect [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_NODE_NOT_FOUND"] = "Le nœud spécifié est introuvable [#ID#]";
$MESS["TASKS_CHECKLIST_TREE_PATH_EXISTS"] = "Nœuds déjà associés[#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_SELF_ATTACH"] = "Impossible d'associer le nœud à lui-même [#ID#, #PARENT_ID#]";
?>