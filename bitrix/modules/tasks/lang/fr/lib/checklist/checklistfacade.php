<?
$MESS["TASKS_CHECKLIST_FACADE_ACTION_ADD"] = "Ajouter un élément";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_MODIFY"] = "Modifier l'élément";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_NOT_ALLOWED"] = "#ACTION_NAME# : action indisponible";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REMOVE"] = "Supprimer l'élément";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REORDER"] = "Déplacer l'élément";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_TOGGLE"] = "Modifier le statut de l'élément";
$MESS["TASKS_CHECKLIST_FACADE_ATTACHMENT_ADDING_FAILED"] = "Erreur lors de l'ajout de pièce jointe";
$MESS["TASKS_CHECKLIST_FACADE_CHECKLIST_DELETE_FAILED"] = "Erreur lors de la suppression de l'article";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_FIELDS"] = "Les champs modifiables ne sont pas spécifiés";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_TITLE"] = "Le nom de l'article manque";
$MESS["TASKS_CHECKLIST_FACADE_MEMBER_DELETE_FAILED"] = "Erreur lors de la suppression du participant";
$MESS["TASKS_CHECKLIST_FACADE_NOT_ALLOWED_FIELD"] = "Champ inconnu transmis [#FIELD_NAME#]";
$MESS["TASKS_CHECKLIST_FACADE_NO_LOOPS_AVAILABLE"] = "L'éléent parent ne peut être un sous-élément de lui-même";
$MESS["TASKS_CHECKLIST_FACADE_USER_FIELD_DELETE_FAILED"] = "Erreur lors de la suppression du champ personnalisé";
$MESS["TASKS_CHECKLIST_FACADE_WRONG_MEMBER_TYPE"] = "Type d'utilisateur inconnu transmis [#TYPE#]";
?>