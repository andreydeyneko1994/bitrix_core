<?php
$MESS["TASKS_NS_COMMENT"] = "Powiadomienie o skomentowaniu zadania";
$MESS["TASKS_NS_MANAGE"] = "Powiadomienie o utworzeniu lub modyfikacji zadania";
$MESS["TASKS_NS_REMINDER"] = "Powiadomienie o zadaniu";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "Przypomnienie o wyznaczeniu zadania";
$MESS["TASKS_NS_TASK_EXPIRED_SOON"] = "Zadanie blisko terminu";
