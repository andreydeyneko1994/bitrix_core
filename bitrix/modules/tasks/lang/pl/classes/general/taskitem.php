<?php
$MESS["TASKS_ACCESS_DENIED_TO_CREATOR_UPDATE"] = "Nie można zmienić twórcy zadań z powodu niewystarczających uprawnień";
$MESS["TASKS_ACCESS_DENIED_TO_DEADLINE_UPDATE"] = "Nie można zmienić terminu wykonania z powodu niewystarczających uprawnień";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_UPDATE"] = "Nie można edytować zadania z powodu niewystarczających uprawnień";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "Termin wykonania zadania wykracza poza zakres dat projektu";
$MESS["TASKS_ERROR_TASK_ADD"] = "Nie można utworzyć zadania";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Nie można połączyć zadania do jego nadrzędnego zadania";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "Planowana data zakończenia zadania wykracza poza zakres dat projektu";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "Planowana data rozpoczęcia zadania wykracza poza zakres dat projektu";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Twój okres próbny skończył się";
$MESS["TASK_CANT_ADD_LINK"] = "Nie można utworzyć linka do zadania";
$MESS["TASK_CANT_DELETE_LINK"] = "Nie można usunąć linka do zadania";
$MESS["TASK_CANT_UPDATE_LINK"] = "Nie można zaktualizować linka do zadania";
