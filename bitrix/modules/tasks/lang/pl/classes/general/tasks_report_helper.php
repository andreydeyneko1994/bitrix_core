<?
$MESS["REPORT_TASKS_COLUMN_TREE_STATUS_SUB"] = "W statusie";
$MESS["REPORT_TASKS_DURATION_FOR_PERIOD"] = "Czas spędzony (okres raportowania)";
$MESS["REPORT_TASKS_Member:TASK_COWORKED.USER"] = "Uczestnicy";
$MESS["REPORT_TASKS_TAGS"] = "Tagi";
$MESS["TASKS_REPORT_DEFAULT_2"] = "Zaangażowanie w projekty";
$MESS["TASKS_REPORT_DEFAULT_3"] = "Zadania w tym miesiącu";
$MESS["TASKS_REPORT_DEFAULT_4"] = "Raport wydajności";
$MESS["TASKS_REPORT_DEFAULT_5"] = "Zadania w zeszłym miesiącu";
$MESS["TASKS_REPORT_DEFAULT_6"] = "Śledzenie zasobów zadań";
$MESS["TASKS_REPORT_DEFAULT_6_DESCR"] = "Ten raport pokazuje zadania wykonane według określonego projektu, działu lub całej firmy dla okresu raportowania i spędzonego czasu.";
$MESS["TASKS_REPORT_DEFAULT_7"] = "Śledzenie zasobów pracowników";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_10"] = "Planowane koszty pracy";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_4"] = "Liczba zadań";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_6"] = "Łącznie spędzony";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_8"] = "Spędzony na okresie raportowania";
$MESS["TASKS_REPORT_DEFAULT_7_DESCR"] = "Pokaż jak długo zajęło każdemu pracownikowi wykonanie aktywnego zadania śledzenia zasobów. Raport może być podzielony na projekty.";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "godz.";
$MESS["TASKS_REPORT_EFF_CLOSED"] = "Zamknięte";
$MESS["TASKS_REPORT_EFF_EFFICIENCY"] = "Wydajność";
$MESS["TASKS_REPORT_EFF_EMPLOYEE"] = "Pracownik";
$MESS["TASKS_REPORT_EFF_MARKED"] = "Ocena";
$MESS["TASKS_REPORT_EFF_NEW"] = "Nowe";
$MESS["TASKS_REPORT_EFF_OPEN"] = "Otwórz";
$MESS["TASKS_REPORT_EFF_OVERDUE"] = "po terminie";
$MESS["TASKS_REPORT_UF_TASK_WEBDAV_FILES"] = "Dokumenty";
?>