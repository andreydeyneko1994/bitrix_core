<?
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "Termin wykonania zadania wykracza poza zakres dat projektu";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "Uczestnicy";
$MESS["TASKS_MESSAGE_AUDITORS"] = "Obserwatorzy";
$MESS["TASKS_MESSAGE_DEADLINE"] = "Termin ostateczny";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "Opis";
$MESS["TASKS_MESSAGE_NO"] = "Nie";
$MESS["TASKS_MESSAGE_PRIORITY"] = "Priorytet";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["TASKS_MESSAGE_TITLE"] = "Nazwa";
$MESS["TASKS_NEW_TASK"] = "Nowe zadanie";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "Planowana data zakończenia zadania wykracza poza zakres dat projektu";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "Planowana data rozpoczęcia zadania wykracza poza zakres dat projektu";
?>