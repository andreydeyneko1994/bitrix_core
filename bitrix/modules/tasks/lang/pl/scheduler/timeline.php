<?
$MESS["SCHEDULER_PRINT_ALERT_BUTTON_TITLE"] = "Tak";
$MESS["SCHEDULER_PRINT_ALERT_TEXT"] = "Czy zakończono drukowanie?";
$MESS["SCHEDULER_PRINT_BORDER_TITLE"] = "Dodaj ramkę";
$MESS["SCHEDULER_PRINT_BUTTON_TITLE"] = "Drukuj";
$MESS["SCHEDULER_PRINT_CANCEL_BUTTON_TITLE"] = "Anuluj";
$MESS["SCHEDULER_PRINT_DATE_FROM_TITLE"] = "Data rozpoczęcia";
$MESS["SCHEDULER_PRINT_DATE_TO_TITLE"] = "Data końcowa";
$MESS["SCHEDULER_PRINT_FIT_TO_PAGE"] = "Dopasuj do strony";
$MESS["SCHEDULER_PRINT_FORMAT_TITLE"] = "Format";
$MESS["SCHEDULER_PRINT_LANDSCAPE_TITLE"] = "Pozioma";
$MESS["SCHEDULER_PRINT_ORIENTATION_TITLE"] = "Orientacja";
$MESS["SCHEDULER_PRINT_PORTRAIT_TITLE"] = "Portret";
$MESS["SCHEDULER_PRINT_SETTINGS_TITLE"] = "Ustawienia drukowania";
$MESS["SCHEDULER_PRINT_TOO_MANY_PAGES"] = "Zbyt wiele stron do wydrukowania (#NUMBER#). Skróć zakres dat lub zmniejsz skalę wykresu.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_FROM"] = "Data rozpoczęcia wykracza poza zakres wykresu.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_RANGE"] = "Nieprawidłowy zakres dat.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_TO"] = "Data zakończenia wykracza poza zakres wykresu.";
?>