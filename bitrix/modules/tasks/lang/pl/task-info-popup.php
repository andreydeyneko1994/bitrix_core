<?
$MESS["TASKS_DATE_COMPLETED"] = "Zakończone";
$MESS["TASKS_DATE_CREATED"] = "Utworzone";
$MESS["TASKS_DATE_DEADLINE"] = "Termin ostateczny";
$MESS["TASKS_DATE_END"] = "Data zakończenia";
$MESS["TASKS_DATE_START"] = "Data rozpoczęcia";
$MESS["TASKS_DATE_STARTED"] = "Rozpoczęte";
$MESS["TASKS_DIRECTOR"] = "Twórca";
$MESS["TASKS_FILES"] = "Pliki";
$MESS["TASKS_PRIORITY_V2"] = "Ważne zadanie";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Szczegóły";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "brak";
$MESS["TASKS_RESPONSIBLE"] = "Odpowiedzialny";
$MESS["TASKS_STATUS"] = "Status";
$MESS["TASKS_STATUS_ACCEPTED"] = "Oczekujące";
$MESS["TASKS_STATUS_COMPLETED"] = "Zakończone";
$MESS["TASKS_STATUS_DECLINED"] = "Odrzucone";
$MESS["TASKS_STATUS_DELAYED"] = "Odroczone";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "W toku";
$MESS["TASKS_STATUS_NEW"] = "Nowe";
$MESS["TASKS_STATUS_OVERDUE"] = "Przeterminowane";
$MESS["TASKS_STATUS_WAITING"] = "Oczekujące zatwierdzenia";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Zadanie nr";
?>