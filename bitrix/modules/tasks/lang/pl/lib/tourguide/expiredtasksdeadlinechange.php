<?php
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TEXT"] = "Czerwone liczniki pokazują liczbę zadań po terminie. Kliknij, aby je zobaczyć.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TITLE"] = "Czas się kończy!";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TEXT"] = "Kliknij i przesuń termin. Licznik zostanie zresetowany. Liczniki są małymi pomocnikami, którzy informują, gdy zbliża się termin.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TITLE"] = "Przesuń termin";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_BUTTON"] = "Rozumiem, dzięki";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TEXT"] = "Pamiętaj, aby liczniki były zgodne z harmonogramem zadań.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TITLE"] = "Świetnie Ci idzie, więc teraz nie ma licznika!";
