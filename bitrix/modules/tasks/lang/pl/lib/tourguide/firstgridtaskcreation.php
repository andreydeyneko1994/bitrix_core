<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TEXT"] = "Dla nowego zadania nie zostanie wyznaczony żaden termin. Zawsze możesz to zmienić później.";
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TITLE"] = "Kliknij, aby utworzyć zadanie";
