<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TEXT"] = "Termin zostanie ustalony automatycznie. Zawsze możesz to zmienić później.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TITLE"] = "Kliknij, aby utworzyć zadanie";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TEXT"] = "Termin jest przydzielany automatycznie po upuszczeniu zadania do kolumny. Domyślny termin to koniec tygodnia.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TITLE"] = "Aby zmienić termin, przenieś zadanie";
