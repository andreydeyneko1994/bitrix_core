<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_READ_REQUIRED_OPTION_ERROR"] = "Błąd podczas odczytu opcji \"item-required\"";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_SAVE_ITEM_ERROR"] = "Błąd podczas zapisywania listy";
$MESS["TASKS_SDC_ERROR_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["TASKS_SDC_ERROR_INCLUDE_MODULE_ERROR"] = "Nie można podłączyć wymaganych modułów.";
$MESS["TASKS_SDC_ERROR_TYPE_NOT_FOUND"] = "Nie można znaleźć typu.";
