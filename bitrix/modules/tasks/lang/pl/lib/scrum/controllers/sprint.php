<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Masz już uruchomiony sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_ERROR"] = "Błąd podczas uruchamiania sprintu.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Nie można uruchomić sprintu bez zadań.";
$MESS["TSSC_ERROR_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["TSSC_ERROR_COULD_NOT_READ_ACTIVE_SPRINT"] = "Nie można ukończyć aktywnego sprintu. Być może został już ukończony.";
$MESS["TSSC_ERROR_COULD_NOT_READ_SPRINT"] = "Nie można pobrać danych dla formularza.";
$MESS["TSSC_ERROR_INCLUDE_MODULE_ERROR"] = "Nie można podłączyć wymaganych modułów.";
