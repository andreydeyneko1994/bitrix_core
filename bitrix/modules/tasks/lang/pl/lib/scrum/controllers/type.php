<?php
$MESS["TASKS_STC_ERROR_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["TASKS_STC_ERROR_COULD_NOT_CREATE_TYPE"] = "Nie można utworzyć nowego typu.";
$MESS["TASKS_STC_ERROR_COULD_NOT_EMPTY_TYPE"] = "Wprowadź nazwę";
$MESS["TASKS_STC_ERROR_COULD_NOT_FOUND_TYPE"] = "Nie można znaleźć typu.";
$MESS["TASKS_STC_ERROR_INCLUDE_MODULE_ERROR"] = "Nie można podłączyć wymaganych modułów.";
