<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_0"] = "Rozwój";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_1"] = "Kod źródłowy został zakończony i zapisany w repozytorium VCS";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_2"] = "Kod źródłowy jest zgodny z wytycznymi dotyczącymi stylu kodu";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_3"] = "Kod źródłowy został przetestowany pod kątem błędów. Znalezione błędy zostały naprawione.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_4"] = "Kod źródłowy został przetestowany pod kątem luk w zabezpieczeniach. Znalezione problemy zostały rozwiązane.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_5"] = "Wymagane funkcje i funkcjonalności zostały przetestowane pod kątem błędów. Znalezione błędy zostały naprawione.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_6"] = "Przetestowane API uznano za zgodne wstecz. Znalezione problemy ze zgodnością wsteczną zostały naprawione.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_7"] = "Powodzenie automatycznych testów funkcjonalności";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_8"] = "Sprawdzono kod źródłowy (szybkie oględziny pod kątem złych praktyk kodowania)";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_9"] = "Pozycja zaległa zaakceptowana przez właściciela produktu. Jeśli przyjęcie pozycji do Demo wymaga dużo czasu lub wysiłku to musi ono zostać wcześniej zaakceptowane.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_10"] = "Zatwierdzone i stabilne wiadomości tekstowe zostały przesłane do tłumaczenia.";
