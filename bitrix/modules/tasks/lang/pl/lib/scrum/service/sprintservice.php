<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Masz już uruchomiony sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "Nie można uruchomić sprintu bez zadań.";
