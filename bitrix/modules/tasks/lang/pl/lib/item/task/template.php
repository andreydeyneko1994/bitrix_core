<?
$MESS["TASKS_ITEM_TASK_TEMPLATE_BAD_RESPONSIBLE_ERROR"] = "Nie znaleziono użytkownika określonego w polu „Osoba odpowiedzialna”.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "Nie można zmienić typu istniejącego szablonu";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "Ogólny szablon";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "Uprawnienia dostępu";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "Lista kontrolna";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "Tagi";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Nie można przypisać szablonu podstawowego, jeśli określono wiele osób odpowiedzialnych.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Nie można przypisać szablonu podstawowego, ponieważ włączone jest wykonanie cykliczne.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Nie można przypisać szablonu podstawowego do szablonu nowego użytkownika.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Nie można utworzyć kilku szablonów osób odpowiedzialnych dla nowego użytkownika.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Nie można ustawić parametrów wykonywania cyklicznego podczas przypisywania szablonu podstawowego.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Nie można ustawić parametrów wykonywania cyklicznego podczas tworzenia szablonu nowego użytkownika.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Nie można utworzyć szablonu nowego użytkownika podczas przypisywania szablonu bazowego.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Nie można utworzyć nowego szablonu użytkownika, ponieważ włączone jest wykonanie cykliczne.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Nie można utworzyć nowego szablonu użytkownika, ponieważ włączone jest wykonanie cykliczne.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "Nie można ustawić jednocześnie ogólnego szablonu i podstawowego zadania";
?>