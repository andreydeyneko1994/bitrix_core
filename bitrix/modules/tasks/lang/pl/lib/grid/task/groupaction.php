<?php
$MESS["TASKS_LIST_CHOOSE_ACTION"] = "Wybierz działanie";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_ACCOMPLICE"] = "Dodaj uczestnika";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_AUDITOR"] = "Dodaj obserwatora";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_FAVORITE"] = "Dodaj do ulubionych";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_ORIGINATOR"] = "Zmień twórcę";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_RESPONSIBLE"] = "Zmień osobę odpowiedzialną";
$MESS["TASKS_LIST_GROUP_ACTION_COMPLETE"] = "Zakończ";
$MESS["TASKS_LIST_GROUP_ACTION_DELETE_FAVORITE"] = "Usuń z ulubionych";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_DAY"] = "dzień";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_MONTH"] = "mies.";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_WEEK"] = "tydzień";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_LEFT"] = "Przenieś termin do tyłu";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_RIGHT"] = "Przenieś termin do przodu";
$MESS["TASKS_LIST_GROUP_ACTION_MUTE"] = "Wycisz";
$MESS["TASKS_LIST_GROUP_ACTION_PING"] = "Ping";
$MESS["TASKS_LIST_GROUP_ACTION_REMOVE"] = "Usuń";
$MESS["TASKS_LIST_GROUP_ACTION_SET_DEADLINE"] = "Ustal termin";
$MESS["TASKS_LIST_GROUP_ACTION_SET_GROUP"] = "Ustal grupę (projekt)";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_NO"] = "Nie";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_V2"] = "Wyłącz sprawdzanie po zakończeniu";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_YES"] = "Tak";
$MESS["TASKS_LIST_GROUP_ACTION_UNMUTE"] = "Wyłącz wyciszenie";
