<?php
$MESS["TASKS_GRID_PROJECT_GRID_GROUP_ACTION_ADD_TO_ARCHIVE"] = "Oznacz jako zarchiwizowane";
$MESS["TASKS_GRID_PROJECT_GRID_GROUP_ACTION_DELETE"] = "Usuń";
$MESS["TASKS_GRID_PROJECT_GRID_GROUP_ACTION_REMOVE_FROM_ARCHIVE"] = "Przywróć z archiwum";
$MESS["TASKS_GRID_PROJECT_GRID_GROUP_ACTION_REQUEST"] = "Dołącz";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_ACTIVITY_DATE"] = "Działanie";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_DATE_FINISH"] = "Data końcowa";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_DATE_START"] = "Data rozpoczęcia";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_EFFICIENCY"] = "Wydajność";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_ID"] = "ID";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_MEMBERS"] = "Członkowie";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_PROJECT"] = "Projekt";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_ROLE"] = "Moja rola";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_TAGS"] = "Tagi";
$MESS["TASKS_GRID_PROJECT_GRID_HEADER_TYPE"] = "Typ";
