<?
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TEXT"] = "
	<p>Staramy się poprawiać jakość usług i wydajność, aby pasowały do wielkości Twojej firmy. Ilość danych rośnie wraz z rozwojem firmy: każdego dnia tworzone są nowe zadania, deale i inne obiekty.</p>
	<p>Im większa firma, tym więcej danych do przeszukiwania. Wybierz plan, który pasuje do Twoich danych, aby szybciej wyszukiwać.</p>";
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TITLE"] = "Osiągnięto limit (#LIMIT#) zadań";
?>