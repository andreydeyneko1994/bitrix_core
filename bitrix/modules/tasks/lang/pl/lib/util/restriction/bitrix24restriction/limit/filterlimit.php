<?php
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION"] = "Jesteś prawdziwym profesjonalistą! Aktualnie masz #COUNT# zadań w swoim Bitrix24. Uwaga: po przekroczeniu limitu #LIMIT#, zadania można wyszukiwać wyłącznie w planach płatnych. Przeszukiwanie tak wielu zadań wymaga znacznych zasobów, dlatego w planie bezpłatnym można używać wyłącznie wstępnie ustawionych filtrów. #HELPDESK_LINK#";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION_HELPDESK_LINK"] = "Szczegóły";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT"] = "
	<p>Staramy się poprawiać jakość usług i wydajność, aby pasowały do wielkości Twojej firmy. Ilość danych rośnie wraz z rozwojem firmy: każdego dnia tworzone są nowe zadania, deale i inne obiekty.</p>
	<p>Im większa firma, tym więcej danych do przeszukiwania. Wybierz plan, który pasuje do Twoich danych, aby szybciej wyszukiwać.</p>";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT_V2"] = "<p>Osiągnięto limit wyszukiwania zadań (#LIMIT#). Staramy się poprawiać jakość usług i wydajność, aby pasowały do wielkości danej firmy. Ilość danych rośnie wraz z rozwojem biznesu: każdego dnia tworzone są nowe zadania, deale i inne obiekty.</p>
<p>Im większa firma, tym więcej danych do przeszukiwania. Wybierz plan, który pasuje do Twoich danych, aby szybciej wyszukiwać.</br>#HELPDESK_LINK#</p>
";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_HELPDESK_LINK"] = "Szczegóły";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE"] = "Osiągnięto limit (#LIMIT#) zadań";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE_V2"] = "Osiągnięto limit wyszukiwania";
