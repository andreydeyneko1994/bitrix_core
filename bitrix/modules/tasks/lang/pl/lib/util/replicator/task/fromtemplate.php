<?php
$MESS["TASKS_REPLICATOR_CANT_IDENTIFY_USER"] = "Nie można zdefiniować użytkownika, który wykona zadanie. Sprawdź, czy konto administratora jest aktywne.";
$MESS["TASKS_REPLICATOR_CREATOR_INACTIVE"] = "Żądanie tworzenia konta, nie istnieje lub jest nieaktywne.";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "Data zakończenia została osiągnięta";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "Nie można obliczyć czasu następnego uruchomienia";
$MESS["TASKS_REPLICATOR_INTERNAL_ERROR"] = "Błąd wewnętrzny. Skontaktuj się z działem pomocy technicznej.";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Osiągnięto maksimum iteracji";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Następny bieg zaplanowany na: #TIME# (w #PERIOD# sekund)";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Błąd podczas obliczania czasu następnego uruchomienia";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "Zatrzymano zadanie";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_0"] = "sekunda";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_1"] = "sekundy";
$MESS["TASKS_REPLICATOR_SECOND_PLURAL_2"] = "sekundy";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "Możliwe wystąpienie wewnętrznych odwołań w podszablonch. Podzadania nie zostały utworzone.";
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Utworzono zaplanowane zadanie";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Utworzono zaplanowane zadanie, wystąpiły błędy";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "Możliwe, że zaplanowane zadanie nie zostało utworzone";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "Zaplanowane zadanie nie zostało utworzone";
