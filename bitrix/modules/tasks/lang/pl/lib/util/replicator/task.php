<?php
$MESS["TASKS_REPLICATOR_ACCESS_DENIED"] = "Nie można utworzyć zadania przy użyciu tego szablonu, ponieważ twórca zadania nie ma wystarczających uprawnień do przypisywania zadań wybranej osobie odpowiedzialnej lub w wybranym projekcie. #LINK#";
