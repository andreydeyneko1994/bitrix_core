<?
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "Argument#ARG_NAME#musi być zbiorem.";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "Argument#ARG_NAME#musi być nie pustym zbiorem.";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "Argument#ARG_NAME#nie jest zbiorem liczb dodatnich.";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "Argument#ARG_NAME#nie jest zbiorem nie pustych ciągów.";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "Pusty argument zaliczony.";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "An empty enum passed to the check method.";
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "Argument#ARG_NAME#musi być liczbą całkowitą.";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "Argument#ARG_NAME#musi być nieujemną liczbą całkowitą.";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "Argument#ARG_NAME#musi być liczbą dodatnią.";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "Argument#ARG_NAME#nie jest wartością enum.";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "Argument#ARG_NAME#musi być nie pustym ciągiem.";
?>