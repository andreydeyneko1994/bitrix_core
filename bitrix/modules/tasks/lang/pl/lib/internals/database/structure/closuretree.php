<?
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_CHILD"] = "Zadania nie mogą używać odwołań cyklicznych.";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_SELF"] = "Nie można powiązać z węzła do niego samego";
$MESS["TASKS_CLOSURE_TREE_ILLEGAL_NODE"] = "Niewłaściwe ID węzła";
$MESS["TASKS_CLOSURE_TREE_LINK_EXISTS"] = "Węzły już powiązane";
$MESS["TASKS_CLOSURE_TREE_NODE_EXISTS_BUT_DECLARED_NEW"] = "Wskazany węzeł już istnieje, ale zostanie dodany jako nowy";
$MESS["TASKS_CLOSURE_TREE_NODE_NOT_FOUND"] = "Nie znaleziono określonego węzła.";
?>