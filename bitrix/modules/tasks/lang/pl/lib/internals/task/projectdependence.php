<?
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET"] = "Powiązane zadanie nie ma daty utworzenia";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET_PARENT_TASK"] = "Zadanie nadrzędne nie ma daty utworzenia";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET"] = "Powiązane zadanie nie ma zaplanowanej daty zakończenia";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET_PARENT_TASK"] = "Zadanie nadrzędne nie ma zaplanowanej daty zakończenia";
$MESS["DEPENDENCE_ENTITY_DEPENDS_ON_ID_FIELD"] = "Zadanie nadrzędne";
$MESS["DEPENDENCE_ENTITY_TASK_ID_FIELD"] = "Zadanie";
$MESS["DEPENDENCE_ENTITY_TYPE_FIELD"] = "Rodzaj powiązania";
?>