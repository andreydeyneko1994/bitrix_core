<?
$MESS["TASKS_CHECKLIST_FACADE_ACTION_ADD"] = "Dodaj element";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_MODIFY"] = "Edytuj element";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_NOT_ALLOWED"] = "#ACTION_NAME#: działanie niedostępne";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REMOVE"] = "Usuń element";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REORDER"] = "Przenieś element";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_TOGGLE"] = "Edytuj status elementu";
$MESS["TASKS_CHECKLIST_FACADE_ATTACHMENT_ADDING_FAILED"] = "Błąd dodawania załącznika";
$MESS["TASKS_CHECKLIST_FACADE_CHECKLIST_DELETE_FAILED"] = "Błąd usuwania elementu";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_FIELDS"] = "Pola edytowalne nie są określone";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_TITLE"] = "Brak nazwy elementu";
$MESS["TASKS_CHECKLIST_FACADE_MEMBER_DELETE_FAILED"] = "Błąd usuwania uczestnika";
$MESS["TASKS_CHECKLIST_FACADE_NOT_ALLOWED_FIELD"] = "Przekazano nieznane pole [#FIELD_NAME#]";
$MESS["TASKS_CHECKLIST_FACADE_NO_LOOPS_AVAILABLE"] = "Element nadrzędny nie może być podelementem samym w sobie";
$MESS["TASKS_CHECKLIST_FACADE_USER_FIELD_DELETE_FAILED"] = "Błąd usuwania pola niestandardowego";
$MESS["TASKS_CHECKLIST_FACADE_WRONG_MEMBER_TYPE"] = "Przekazano nieznany typ użytkownika [#TYPE#]";
?>