<?
$MESS["TASKS_CHECKLIST_TREE_CHILD_ATTACH"] = "Nie można utworzyć odwołania cyklicznego [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_EXISTING_NODE_ADDING"] = "Nie można dodać już istniejącego węzła [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_ILLEGAL_NODE"] = "Nieprawidłowe ID węzła [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_NODE_NOT_FOUND"] = "Nie znaleziono określonego węzła [#ID#]";
$MESS["TASKS_CHECKLIST_TREE_PATH_EXISTS"] = "Powiązane węzły [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_SELF_ATTACH"] = "Nie można dołączyć węzła do jego samego [#ID#, #PARENT_ID#]";
?>