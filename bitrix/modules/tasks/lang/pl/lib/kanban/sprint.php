<?php
$MESS["TASKS_SPRINT_ERROR_CANT_ADD_TO_SPRINT"] = "Nie można dodać zadania do sprintu.";
$MESS["TASKS_SPRINT_ERROR_CANT_CREATE"] = "Nie możesz rozpocząć nowego sprintu.";
$MESS["TASKS_SPRINT_ERROR_END_DATE_WRONG"] = "Data końcowa sprintu musi być późniejsza niż dzisiaj.";
$MESS["TASKS_STAGE_TL1"] = "Dzisiaj";
$MESS["TASKS_STAGE_TL2"] = "Jutro";
$MESS["TASKS_STAGE_TL3"] = "Bieżący tydzień";
$MESS["TASKS_STAGE_TL4"] = "Bieżący miesiąc";
$MESS["TASKS_STAGE_TL5"] = "Brak terminu";
