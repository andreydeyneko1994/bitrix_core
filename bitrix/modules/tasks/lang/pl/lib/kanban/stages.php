<?
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Początkowego etapu nie można usunąć. Przenieś etap przed usunięciem.";
$MESS["TASKS_STAGE_FINISH"] = "Zakończone";
$MESS["TASKS_STAGE_MP_1"] = "Niezaplanowane";
$MESS["TASKS_STAGE_MP_2"] = "Do zrobienia w tym tygodniu";
$MESS["TASKS_STAGE_NEW"] = "Nowe";
$MESS["TASKS_STAGE_PERIOD1"] = "Po terminie";
$MESS["TASKS_STAGE_PERIOD2"] = "Na dziś";
$MESS["TASKS_STAGE_PERIOD3"] = "Na ten tydzień";
$MESS["TASKS_STAGE_PERIOD4"] = "Na przyszły tydzień";
$MESS["TASKS_STAGE_PERIOD5"] = "Brak terminu";
$MESS["TASKS_STAGE_PERIOD6"] = "Na za dwa tygodnie";
$MESS["TASKS_STAGE_WORK"] = "W toku";
?>