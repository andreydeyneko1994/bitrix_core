<?php
$MESS["TASKS_FAILED_RESULT_REQUIRED"] = "Twórca zadania wymaga dostarczenia raportu z zadania.<br> Zostaw komentarz do zadania i oznacz go jako podsumowanie statusu zadania.";
$MESS["TASKS_FAILED_START_TASK_TIMER"] = "Nie można uruchomić licznika czasu zadania";
$MESS["TASKS_FAILED_STOP_TASK_TIMER"] = "Nie można zatrzymać licznika czasu zadania";
$MESS["TASKS_FAILED_WRONG_ORDER_FIELD"] = "Nieprawidłowe pole sortowania";
$MESS["TASKS_OTHER_TASK_ON_TIMER"] = "Inne zadanie (##ID#) jest teraz aktywne. Przed kontynuacją zatrzymaj je.";
