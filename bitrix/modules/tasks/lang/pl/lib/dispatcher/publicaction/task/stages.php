<?
$MESS["STAGES_ERROR_ACCESS_DENIED_GROUP_TASK"] = "Nie można wyświetlić zadań w tej grupie";
$MESS["STAGES_ERROR_ACCESS_DENIED_MOVE"] = "Nie możesz przenieść tego zadania";
$MESS["STAGES_ERROR_ACCESS_DENIED_STAGES"] = "Nie możesz zarządzać etapami";
$MESS["STAGES_ERROR_DIFFERENT_STAGES"] = "Etap i zadanie należą do różnych grup";
$MESS["STAGES_ERROR_EMPTY_DATA"] = "Nie podano żadnych danych aktualizacji";
$MESS["STAGES_ERROR_EMPTY_TITLE"] = "Brakuje tytułu etapu";
$MESS["STAGES_ERROR_IS_SYSTEM"] = "Ostatni etap nie może zostać usunięty";
$MESS["STAGES_ERROR_NOT_FOUND"] = "Nie znaleziono etapu";
$MESS["STAGES_ERROR_NO_EMPTY"] = "Na tym etapie są zadania";
$MESS["STAGES_ERROR_SOCIALNETWORK_IS_NOT_INSTALLED"] = "moduł sieci społecznościowej nie jest zainstalowany.";
$MESS["STAGES_ERROR_TASK_NOT_FOUND"] = "Nie znaleziono zadania lub odmowa dostępu.";
?>