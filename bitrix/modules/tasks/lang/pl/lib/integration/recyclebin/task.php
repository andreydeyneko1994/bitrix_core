<?
$MESS["TASKS_RECYCLEBIN_REMOVE_CONFIRM"] = "Zadanie zostanie trwale usunięte. Na pewno chcesz kontynuować?";
$MESS["TASKS_RECYCLEBIN_REMOVE_MESSAGE"] = "Zadanie zostało usunięte";
$MESS["TASKS_RECYCLEBIN_RESTORE_CONFIRM"] = "Czy chcesz odzyskać wybrane zadanie i przenieść je z powrotem na listę?";
$MESS["TASKS_RECYCLEBIN_RESTORE_MESSAGE"] = "Zadanie zostało odzyskane";
?>