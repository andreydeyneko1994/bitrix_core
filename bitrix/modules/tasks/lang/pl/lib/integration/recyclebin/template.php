<?
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_CONFIRM"] = "Szablon zostanie trwale usunięty. Na pewno chcesz kontynuować? ";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_MESSAGE"] = "Szablon został usunięty";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_CONFIRM"] = "Czy chcesz odzyskać wybrany szablon i przenieść go z powrotem na listę?";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_MESSAGE"] = "Szablon został odzyskany";
?>