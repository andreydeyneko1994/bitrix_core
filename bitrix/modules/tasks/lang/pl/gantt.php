<?
$MESS["TASKS_GANTT_CHART_TITLE"] = "Zadania";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "Połączenia zapętlone nie są dozwolone";
$MESS["TASKS_GANTT_DATE_END"] = "Data końcowa";
$MESS["TASKS_GANTT_DATE_START"] = "Data początkowa";
$MESS["TASKS_GANTT_DEADLINE"] = "Termin ostateczny";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "Usuń";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "Formularz";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "Do";
$MESS["TASKS_GANTT_EMPTY_DATE"] = "brak";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "Data końcowa jest wymagana do ustalenia powiązania";
$MESS["TASKS_GANTT_END"] = "koniec";
$MESS["TASKS_GANTT_INDENT_TASK"] = "Wcięcie Zadania";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "Wysunięcie Zadania";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "Nie masz uprawnień do edycji dat w tym zadaniu";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "Nie można łączyć zadania z jego zadaniem nadrzędnym";
$MESS["TASKS_GANTT_START"] = "start";
?>