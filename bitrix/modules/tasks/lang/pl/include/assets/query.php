<?
$MESS["TASKS_ASSET_QUERY_EMPTY_RESPONSE"] = "Usuń odpowiedź serwera.";
$MESS["TASKS_ASSET_QUERY_ILLEGAL_RESPONSE"] = "Błąd analizy składniowej odpowiedzi serwera.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED"] = "Podczas przetwarzania wniosku wystąpił błąd.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_EXCEPTION"] = "Błąd analizy składniowej odpowiedzi serwera.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_STATUS"] = "Podczas przetwarzania żądania wystąpił błąd... (HTTP #HTTP_STATUS#)";
?>