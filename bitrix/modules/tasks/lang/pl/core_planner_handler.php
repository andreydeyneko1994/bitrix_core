<?
$MESS["JS_CORE_PL_TASKS"] = "Dzisiejsze zadania";
$MESS["JS_CORE_PL_TASKS_ADD"] = "wprowadź nowe zadanie";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "Wybierz z listy";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "Nowe zadanie";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "Zakończ zadanie i zatrzymaj śledzenie czasu";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "Usuń z dziennego planu";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "Rozpocznij śledzenie czasu";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "Wstrzymaj moj czasomierz";
?>