<?
$MESS["COPY_PUBLIC_FILES"] = "Kopiuj publiczne pliki i szablon strony";
$MESS["TASKS_CONVERT_AND_USE_NEW"] = "Convert tasks and start using Tasks 2.0";
$MESS["TASKS_INSTALL_BACK"] = "Powród do zarządzania modułem";
$MESS["TASKS_INSTALL_COMPLETE_ERROR"] = "Installation has been completed with errors.";
$MESS["TASKS_INSTALL_COMPLETE_OK"] = "Installation has been completed.";
$MESS["TASKS_INSTALL_COPY_PUBLIC"] = "Kopiuj skrypty sekcji publicznej";
$MESS["TASKS_INSTALL_PUBLIC_SETUP"] = "Instaluj";
$MESS["TASKS_INSTALL_TITLE"] = "Install The Task Management Module";
$MESS["TASKS_MODULE_DESC"] = "Task management module";
$MESS["TASKS_MODULE_NAME"] = "Zadania";
$MESS["TASKS_UNINSTALL_COMPLETE"] = "Uninstallation has been completed.";
$MESS["TASKS_UNINSTALL_DEL"] = "Usuń";
$MESS["TASKS_UNINSTALL_ERROR"] = "Błędy odinstalowania:";
$MESS["TASKS_UNINSTALL_SAVEDATA"] = "Aby zapisać dane przechowywane w tabeli danych, zaznacz okienko &quot;Zapisz tabele&quot;";
$MESS["TASKS_UNINSTALL_SAVETABLE"] = "Zabisz Tabele";
$MESS["TASKS_UNINSTALL_TITLE"] = "Uninstall The Task Management Module";
$MESS["TASKS_UNINSTALL_WARNING"] = "Ostrzeżenie! Moduł zostanie usunięty z systemu.";
?>