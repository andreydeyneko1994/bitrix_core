<?
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "La fecha límite de la tarea está fuera del rango de fechas del proyecto";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "Participantes";
$MESS["TASKS_MESSAGE_AUDITORS"] = "Observadores";
$MESS["TASKS_MESSAGE_DEADLINE"] = "Fecha límite";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "Descripción";
$MESS["TASKS_MESSAGE_NO"] = "no";
$MESS["TASKS_MESSAGE_PRIORITY"] = "Prioridad";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "Persona responsable";
$MESS["TASKS_MESSAGE_TITLE"] = "Nombre";
$MESS["TASKS_NEW_TASK"] = "Nueva tarea";
$MESS["TASKS_NEW_TASK_MESSAGE"] = "Una nueva tarea ha sido agregada

Nombre de la tarea: #TASK_TITLE#
Creada por: #TASK_AUTHOR#
Persona responsable: #TASK_RESPONSIBLE#
#TASK_EXTRA#
Ver la tarea:
#PATH_TO_TASK#";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "La fecha de finalización de la tarea planificada, está fuera del rango de fechas del proyecto";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "La fecha de inicio de la tarea planificada, está fuera del rango de fechas del proyecto";
?>