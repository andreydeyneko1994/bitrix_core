<?php
$MESS["TASKS_NS_COMMENT"] = "Nuevo comentario agregado a una tarea";
$MESS["TASKS_NS_MANAGE"] = "Tarea creada o editada";
$MESS["TASKS_NS_REMINDER"] = "Recordatorio de tarea";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "Tarea asignada a usted";
$MESS["TASKS_NS_TASK_EXPIRED_SOON"] = "Tarea casi vencida";
