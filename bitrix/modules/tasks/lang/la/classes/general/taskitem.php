<?php
$MESS["TASKS_ACCESS_DENIED_TO_CREATOR_UPDATE"] = "No se puede cambiar el creador de la tarea debido a permisos insuficientes";
$MESS["TASKS_ACCESS_DENIED_TO_DEADLINE_UPDATE"] = "No se puede cambiar la fecha límite debido a permisos insuficientes";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_DELETE"] = "No se puede eliminar la tarea porque se denegó el acceso";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_UPDATE"] = "No se puede editar la tarea debido a permisos insuficientes";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "La fecha límite de la tarea, está fuera del rango de fechas del proyecto";
$MESS["TASKS_ERROR_TASK_ADD"] = "No se puede crear la tarea";
$MESS["TASKS_HAS_PARENT_RELATION"] = "No se puede vincular una tarea a una tarea principal";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "La fecha de finalización de la tarea planificada, está fuera del rango de fechas del proyecto";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "La fecha de inicio de la tarea planificada, está fuera del rango de fechas del proyecto";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Su período de prueba ha caducado";
$MESS["TASK_CANT_ADD_LINK"] = "No se puede vincular tareas";
$MESS["TASK_CANT_DELETE_LINK"] = "No se puede desvincular las tareas";
$MESS["TASK_CANT_UPDATE_LINK"] = "No puede actualizar vínculo de tarea";
