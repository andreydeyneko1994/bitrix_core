<?
$MESS["SCHEDULER_PRINT_ALERT_BUTTON_TITLE"] = "Si";
$MESS["SCHEDULER_PRINT_ALERT_TEXT"] = "¿Terminaste de imprimir?";
$MESS["SCHEDULER_PRINT_BORDER_TITLE"] = "Agregar borde";
$MESS["SCHEDULER_PRINT_BUTTON_TITLE"] = "Imprimir";
$MESS["SCHEDULER_PRINT_CANCEL_BUTTON_TITLE"] = "Cancelar";
$MESS["SCHEDULER_PRINT_DATE_FROM_TITLE"] = "Fecha de inicio";
$MESS["SCHEDULER_PRINT_DATE_TO_TITLE"] = "Fecha final";
$MESS["SCHEDULER_PRINT_FIT_TO_PAGE"] = "Ajustar a la página";
$MESS["SCHEDULER_PRINT_FORMAT_TITLE"] = "Formato";
$MESS["SCHEDULER_PRINT_LANDSCAPE_TITLE"] = "Paisaje";
$MESS["SCHEDULER_PRINT_ORIENTATION_TITLE"] = "Orientación";
$MESS["SCHEDULER_PRINT_PORTRAIT_TITLE"] = "Retrato";
$MESS["SCHEDULER_PRINT_SETTINGS_TITLE"] = "Configuración de impresión";
$MESS["SCHEDULER_PRINT_TOO_MANY_PAGES"] = "Demasiadas páginas para imprimir (#NUMBER#). Por favor reduzca el rango de fechas o disminuya la escala del gráfico.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_FROM"] = "La fecha de inicio está fuera del rango del gráfico.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_RANGE"] = "Rango de fecha no válido.";
$MESS["SCHEDULER_PRINT_WRONG_DATE_TO"] = "La fecha de finalización está fuera del rango del gráfico.";
?>