<?
$MESS["TASKS_BP_AUTOMATION_PERSONAL_STATUS_COMPLETED"] = "Completado";
$MESS["TASKS_BP_AUTOMATION_PERSONAL_STATUS_DEFERRED"] = "Diferido";
$MESS["TASKS_BP_AUTOMATION_PERSONAL_STATUS_IN_PROGRESS"] = "En Progreso";
$MESS["TASKS_BP_AUTOMATION_PERSONAL_STATUS_PENDING"] = "Pendiente";
$MESS["TASKS_BP_AUTOMATION_PERSONAL_STATUS_SUPPOSEDLY_COMPLETED"] = "Revisión pendiente";
?>