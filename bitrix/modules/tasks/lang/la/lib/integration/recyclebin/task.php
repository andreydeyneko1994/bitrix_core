<?
$MESS["TASKS_RECYCLEBIN_REMOVE_CONFIRM"] = "La tarea será eliminada permanentemente. ¿Esta seguro que desea continuar?";
$MESS["TASKS_RECYCLEBIN_REMOVE_MESSAGE"] = "La tarea ha sido eliminada";
$MESS["TASKS_RECYCLEBIN_RESTORE_CONFIRM"] = "¿Desea recuperar la tarea seleccionada y volver a la lista?";
$MESS["TASKS_RECYCLEBIN_RESTORE_MESSAGE"] = "La tarea ha sido recuperada";
?>