<?
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_CONFIRM"] = "La plantilla será eliminada permanentemente. ¿Esta seguro que deseas continuar?";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_MESSAGE"] = "Se ha eliminado la plantilla.";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_CONFIRM"] = "¿Desea recuperar la plantilla seleccionada y volver a moverla a la lista?";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_MESSAGE"] = "Se ha recuperado la plantilla.";
?>