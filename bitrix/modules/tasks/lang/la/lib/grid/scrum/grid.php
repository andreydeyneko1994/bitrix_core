<?php
$MESS["TASKS_GRID_SCRUM_GRID_GROUP_ACTION_ADD_TO_ARCHIVE"] = "Agregar al archivo";
$MESS["TASKS_GRID_SCRUM_GRID_GROUP_ACTION_DELETE"] = "Eliminar";
$MESS["TASKS_GRID_SCRUM_GRID_GROUP_ACTION_LEAVE"] = "Salir";
$MESS["TASKS_GRID_SCRUM_GRID_GROUP_ACTION_REMOVE_FROM_ARCHIVE"] = "Recuperar del archivo";
$MESS["TASKS_GRID_SCRUM_GRID_GROUP_ACTION_REQUEST"] = "Unirse";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_ACTIVITY_DATE"] = "Activo";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_DATE_FINISH"] = "Fecha de finalización";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_DATE_START"] = "Fecha de inicio";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_EFFICIENCY"] = "Desempeño";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_ID"] = "ID";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_MEMBERS"] = "Miembros";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_PROJECT"] = "Nombre";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_ROLE"] = "Rol";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_TAGS"] = "Etiquetas";
$MESS["TASKS_GRID_SCRUM_GRID_HEADER_TYPE"] = "Tipo";
