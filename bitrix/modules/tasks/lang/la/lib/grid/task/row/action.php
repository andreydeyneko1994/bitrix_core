<?php
$MESS["TASKS_GRID_TASK_ROW_ACTION_ACCEPT"] = "Aceptar Tareas";
$MESS["TASKS_GRID_TASK_ROW_ACTION_ADD_SUB_TASK"] = "Crear subtarea";
$MESS["TASKS_GRID_TASK_ROW_ACTION_ADD_TO_FAVORITES"] = "Agregar a los favoritos";
$MESS["TASKS_GRID_TASK_ROW_ACTION_ADD_TO_TIMEMAN"] = "Agregar al plan de día de trabajo";
$MESS["TASKS_GRID_TASK_ROW_ACTION_APPROVE"] = "Aceptar como completado";
$MESS["TASKS_GRID_TASK_ROW_ACTION_COMPLETE"] = "Completar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_COPY"] = "Copiar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_COPY_LINK"] = "Copiar el enlace de la tarea";
$MESS["TASKS_GRID_TASK_ROW_ACTION_DEFER"] = "Posponer";
$MESS["TASKS_GRID_TASK_ROW_ACTION_EDIT"] = "Editar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_MUTE"] = "Desactivar audio";
$MESS["TASKS_GRID_TASK_ROW_ACTION_PAUSE"] = "Pausar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_PIN"] = "Fijar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_PING"] = "Ping";
$MESS["TASKS_GRID_TASK_ROW_ACTION_REMOVE"] = "Eliminar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_REMOVE_FROM_FAVORITES"] = "Eliminar de favoritos";
$MESS["TASKS_GRID_TASK_ROW_ACTION_RENEW"] = "Reanudar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_START"] = "Iniciar tarea";
$MESS["TASKS_GRID_TASK_ROW_ACTION_UNMUTE"] = "Reactivar audio";
$MESS["TASKS_GRID_TASK_ROW_ACTION_UNPIN"] = "No fijar";
$MESS["TASKS_GRID_TASK_ROW_ACTION_VIEW"] = "Ver";
