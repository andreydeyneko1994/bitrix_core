<?php
$MESS["TASKS_ACTION_RESULT_REQUIRED"] = "El creador de tareas requiere que proporcione un informe de la tarea.<br> Deje un comentario en la tarea y márquelo como un resumen del estado de la tarea.";
