<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TEXT"] = "No se establecerá una fecha límite para la nueva tarea. Puede cambiar esta opción en cualquier momento.";
$MESS["TASKS_TOUR_GUIDE_FIRST_GRID_TASK_CREATION_POPUP_0_TITLE"] = "Haga clic para crear una tarea";
