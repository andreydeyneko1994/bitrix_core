<?php
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TEXT"] = "Los contadores rojos muestran el número de tareas atrasadas. Haga clic para verlos.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TITLE"] = "¡El tiempo está a punto de agotarse!";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TEXT"] = "Haga clic y cambie la fecha límite. El contador se reiniciará. Los contadores serán sus pequeños ayudantes y le avisaran cuando se acerque la fecha límite.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TITLE"] = "Cambiar la fecha límite";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_BUTTON"] = "Entendido, gracias";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TEXT"] = "Vea sus contadores para mantenerse al día con sus tareas.";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TITLE"] = "¡Su trabajo es excelente, no hay ningún contador por el momento!";
