<?php
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TEXT"] = "La fecha límite se fijará automáticamente. Puede cambiar esta opción en cualquier momento.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_0_TITLE"] = "Haga clic para crear una tarea";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TEXT"] = "La fecha límite se asigna automáticamente cuando se coloca una tarea en la columna. La fecha límite predeterminada es al final de la semana.";
$MESS["TASKS_TOUR_GUIDE_FIRST_TIMELINE_TASK_CREATION_POPUP_1_TITLE"] = "Modifique la tarea para cambiar la fecha límite";
