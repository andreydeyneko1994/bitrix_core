<?
$MESS["TASKS_CHECKLIST_FACADE_ACTION_ADD"] = "Añadir elemento";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_MODIFY"] = "Editar elemento";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_NOT_ALLOWED"] = "#ACTION_NAME#: acción no disponible";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REMOVE"] = "Eliminar elemento";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REORDER"] = "Mover elemento";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_TOGGLE"] = "Editar estado del elemento";
$MESS["TASKS_CHECKLIST_FACADE_ATTACHMENT_ADDING_FAILED"] = "Error al agregar el archivo adjunto";
$MESS["TASKS_CHECKLIST_FACADE_CHECKLIST_DELETE_FAILED"] = "Error al eliminar el elemento";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_FIELDS"] = "Los campos editables no están especificados";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_TITLE"] = "Falta el nombre del elemento";
$MESS["TASKS_CHECKLIST_FACADE_MEMBER_DELETE_FAILED"] = "Error al eliminar el participante";
$MESS["TASKS_CHECKLIST_FACADE_NOT_ALLOWED_FIELD"] = "Campo pasado desconocido [#FIELD_NAME#]";
$MESS["TASKS_CHECKLIST_FACADE_NO_LOOPS_AVAILABLE"] = "El elemento principal no puede ser un subelemento de sí mismo";
$MESS["TASKS_CHECKLIST_FACADE_USER_FIELD_DELETE_FAILED"] = "Error al eliminar el campo personalizado";
$MESS["TASKS_CHECKLIST_FACADE_WRONG_MEMBER_TYPE"] = "Tipo de usuario pasado desconocido [#TYPE#]";
?>