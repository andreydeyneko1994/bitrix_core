<?
$MESS["TASKS_CHECKLIST_TREE_CHILD_ATTACH"] = "No se puede crear una referencia circular [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_EXISTING_NODE_ADDING"] = "No se puede agregar el nodo porque ya existe [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_ILLEGAL_NODE"] = "ID de nodo incorrecto [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_NODE_NOT_FOUND"] = "No se encontró el nodo especificado [#ID#]";
$MESS["TASKS_CHECKLIST_TREE_PATH_EXISTS"] = "Los nodos ya están vinculados [#ID#, #PARENT_ID#]";
$MESS["TASKS_CHECKLIST_TREE_SELF_ATTACH"] = "No se puede adjuntar el nodo a sí mismo [#ID#, #PARENT_ID#]";
?>