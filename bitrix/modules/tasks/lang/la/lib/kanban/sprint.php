<?php
$MESS["TASKS_SPRINT_ERROR_CANT_ADD_TO_SPRINT"] = "No se puede agregar una tarea al sprint.";
$MESS["TASKS_SPRINT_ERROR_CANT_CREATE"] = "No puede iniciar un nuevo sprint.";
$MESS["TASKS_SPRINT_ERROR_END_DATE_WRONG"] = "La fecha de finalización del Sprint debe ser posterior a hoy.";
$MESS["TASKS_STAGE_TL1"] = "Hoy";
$MESS["TASKS_STAGE_TL2"] = "Mañana";
$MESS["TASKS_STAGE_TL3"] = "Esta semana";
$MESS["TASKS_STAGE_TL4"] = "Este mes";
$MESS["TASKS_STAGE_TL5"] = "Sin fecha límite";
