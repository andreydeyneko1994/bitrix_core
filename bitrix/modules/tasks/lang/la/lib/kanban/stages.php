<?
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Una etapa inicial no puede ser eliminada. Mueva la etapa antes de eliminarlo.";
$MESS["TASKS_STAGE_FINISH"] = "Terminado";
$MESS["TASKS_STAGE_MP_1"] = "No planificado";
$MESS["TASKS_STAGE_MP_2"] = "Para hacer esta semana";
$MESS["TASKS_STAGE_NEW"] = "Nuevo";
$MESS["TASKS_STAGE_PERIOD1"] = "Atrasado";
$MESS["TASKS_STAGE_PERIOD2"] = "Para hoy";
$MESS["TASKS_STAGE_PERIOD3"] = "Para esta semana";
$MESS["TASKS_STAGE_PERIOD4"] = "Para la próxima semana";
$MESS["TASKS_STAGE_PERIOD5"] = "Sin fecha límite";
$MESS["TASKS_STAGE_PERIOD6"] = "Para dentro de dos semanas";
$MESS["TASKS_STAGE_WORK"] = "En progreso";
?>