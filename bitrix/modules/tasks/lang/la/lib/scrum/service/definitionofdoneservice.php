<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_0"] = "Desarrollo";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_1"] = "Código fuente completado y revisado en el repositorio de VCS";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_2"] = "El código fuente sigue las normas de estilo del código";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_3"] = "El código fuente fue probado para detectar errores. Se arreglaron los errores encontrados";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_4"] = "El código fuente fue probado para detectar fallas de seguridad. Se solucionaron los problemas encontrados";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_5"] = "Características y funciones necesarias probadas para detectar errores. Se corrigieron los errores encontrados";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_6"] = "Se probó la API y es compatible con versiones anteriores. Se corrigieron los problemas de compatibilidad que se encontraron en las versiones anteriores ";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_7"] = "Se aprobaron las pruebas automáticas de funcionalidad";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_8"] = "Código fuente revisado (inspección visual rápida para detectar malas prácticas de codificación)";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_9"] = "Artículo del registro aceptado por el propietario del producto. Si aceptar un artículo de demostración toma mucho tiempo o esfuerzo, debe aceptarlo antes de la demostración.";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_10"] = "Mensajes de texto estables y aprobados enviados para localización.";
