<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Ya está ejecutando un sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "No se puede ejecutar el sprint sin tareas.";
