<?php
$MESS["TASKS_STC_ERROR_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["TASKS_STC_ERROR_COULD_NOT_CREATE_TYPE"] = "No se puede crear un nuevo tipo.";
$MESS["TASKS_STC_ERROR_COULD_NOT_EMPTY_TYPE"] = "Introduzca un nombre";
$MESS["TASKS_STC_ERROR_COULD_NOT_FOUND_TYPE"] = "No se puede encontrar el tipo.";
$MESS["TASKS_STC_ERROR_INCLUDE_MODULE_ERROR"] = "No se pueden conectar los módulos necesarios.";
