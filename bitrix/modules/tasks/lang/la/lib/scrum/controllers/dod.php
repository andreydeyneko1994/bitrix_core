<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_READ_REQUIRED_OPTION_ERROR"] = "Error al leer la opción \"elemento obligatorio\"";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_SAVE_ITEM_ERROR"] = "Error al guardar la lista";
$MESS["TASKS_SDC_ERROR_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["TASKS_SDC_ERROR_INCLUDE_MODULE_ERROR"] = "No se pueden conectar los módulos necesarios.";
$MESS["TASKS_SDC_ERROR_TYPE_NOT_FOUND"] = "No se puede encontrar el tipo.";
