<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "Sprint %s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "Ya está ejecutando un sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_ERROR"] = "Error al iniciar el sprint.";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "No se puede ejecutar el sprint sin tareas.";
$MESS["TSSC_ERROR_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["TSSC_ERROR_COULD_NOT_READ_ACTIVE_SPRINT"] = "No se puede completar el sprint activo. Es posible que ya se haya completado.";
$MESS["TSSC_ERROR_COULD_NOT_READ_SPRINT"] = "No se pueden obtener datos para el formulario.";
$MESS["TSSC_ERROR_INCLUDE_MODULE_ERROR"] = "No se pueden conectar los módulos necesarios.";
