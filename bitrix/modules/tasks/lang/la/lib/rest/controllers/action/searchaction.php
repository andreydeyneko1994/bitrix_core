<?
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED"] = "Para buscar todas las tareas, elija un plan que se ajuste a la cantidad de datos acumulados y a la escala de su empresa. <a onclick=\"top.BX.Helper.show('redirect=detail&code=9745327')\" style=\"cursor: pointer\">Detalles</a>";
$MESS["TASKS_CONTROLLER_SEARCH_ACTION_TASKS_LIMIT_EXCEEDED_TITLE"] = "#LIMIT# límite de tareas alcanzado";
?>