<?php
$MESS["TASKS_FAILED_RESULT_REQUIRED"] = "El creador de tareas requiere que proporcione un informe de la tarea.<br> Deje un comentario en la tarea y márquelo como un resumen del estado de la tarea.";
$MESS["TASKS_FAILED_START_TASK_TIMER"] = "No se puede iniciar el temporizador de tareas";
$MESS["TASKS_FAILED_STOP_TASK_TIMER"] = "No se puede detener el temporizador de tareas";
$MESS["TASKS_FAILED_WRONG_ORDER_FIELD"] = "Campo de clasificación incorrecto";
$MESS["TASKS_OTHER_TASK_ON_TIMER"] = "Actualmente, hay otra tarea (##ID#) activa. Deténgala antes de continuar.";
