<?
$MESS["TASKS_STATUS_1"] = "Nuevo";
$MESS["TASKS_STATUS_2"] = "Pendiente";
$MESS["TASKS_STATUS_3"] = "En progreso";
$MESS["TASKS_STATUS_4"] = "Revisión pendiente";
$MESS["TASKS_STATUS_5"] = "Terminado";
$MESS["TASKS_STATUS_6"] = "Diferido";
$MESS["TASKS_STATUS_7"] = "Declinado";
?>