<?php
$MESS["TASKS_REPLICATOR_ACCESS_DENIED"] = "No se puede crear una tarea con esta plantilla porque el creador de la tarea no tiene permisos suficientes para asignar tareas a la persona responsable seleccionada o en el proyecto seleccionado. #LINK#";
