<?
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TEXT"] = "
	<p>Nos esforzamos por mejorar la calidad y el rendimiento del servicio para que se adapte a su escala comercial. La cantidad de datos aumenta a medida que su negocio crece: cada día se crean nuevas tareas, negociaciones y otras entidades.</p>
	<p>Entre mayor sea la escala de su empresa, creará más datos para buscar. Elija un plan que se ajuste a sus datos para buscar más rápido.</p>";
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TITLE"] = "Alcanzó el límite de tareas (#LIMIT#)";
?>