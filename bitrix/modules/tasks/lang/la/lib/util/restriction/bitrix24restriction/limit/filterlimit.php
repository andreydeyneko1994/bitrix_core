<?php
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION"] = "¡Es un verdadero profesional! Ahora tiene #COUNT# tareas en su Bitrix24. Tenga en cuenta que solo podrá buscar tareas en los planes comerciales cuando su número supere #LIMIT#. La búsqueda de muchas tareas requiere recursos considerables, así que solo pueden usarse los filtros predeterminados en el plan gratuito. #HELPDESK_LINK#";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION_HELPDESK_LINK"] = "Detalles";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT"] = "
	<p>Nos esforzamos por mejorar la calidad y el rendimiento del servicio para que se adapte a su escala comercial. La cantidad de datos aumenta a medida que su negocio crece: cada día se crean nuevas tareas, negociaciones y otras entidades.</p>
	<p>Entre mayor sea la escala de su empresa, creará más datos para buscar. Elija un plan que se ajuste a sus datos para buscar más rápido.</p>";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT_V2"] = "<p>Ya alcanzó el límite de búsqueda (#LIMIT#). Nos esforzamos por mejorar la calidad y el rendimiento del servicio para que se adapte a su escala comercial. La cantidad de datos aumenta a medida que su empresa crece: cada día se crean nuevas tareas, negociaciones y otras entidades.</p>
<p>Entre mayor sea la escala de su empresa, más búsqueda de datos creará. Elija un plan que se ajuste a sus datos para buscar más rápidamente.</br>#HELPDESK_LINK#</p>
";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_HELPDESK_LINK"] = "Detalles";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE"] = "Alcanzó el límite de tareas (#LIMIT#)";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE_V2"] = "Ya alcanzó el límite de búsqueda";
