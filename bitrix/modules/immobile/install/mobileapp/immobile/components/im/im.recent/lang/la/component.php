<?php
$MESS["IM_B24DISK"] = "Bitrix24.Drive";
$MESS["IM_CAMERA_ROLL"] = "Tomar una foto";
$MESS["IM_CHOOSE_FILE_TITLE"] = "Archivos";
$MESS["IM_CHOOSE_PHOTO"] = "Seleccionar de la galería";
$MESS["IM_DIALOG_UNNAMED"] = "Cargando...";
$MESS["IM_EMPTY_BUTTON"] = "Iniciar conversación";
$MESS["IM_EMPTY_OL_TEXT_1"] = "¿Está buscando conversaciones con sus clientes? Están aquí.";
$MESS["IM_EMPTY_OL_TEXT_2"] = "Se te notificará tan pronto como un cliente publique un mensaje en un canal abierto.";
$MESS["IM_EMPTY_TEXT_1"] = "¡Comienza la conversación ahora!";
$MESS["IM_EMPTY_TEXT_CREATE"] = "Haga clic en el botón ade abajo para crear un chat, o seleccione un empleado.";
$MESS["IM_EMPTY_TEXT_INVITE"] = "¡Haga clic en el siguiente botón para invitar a sus colegas a su Bitrix24!";
$MESS["IM_LIST_ACTION_ERROR"] = "No pudimos conectarnos a su Bitrix24, probablemente debido a una mala conexión a Internet. Intente de nuevo más tarde.";
$MESS["IM_LIST_EMPTY"] = "No hay conversaciones actuales";
$MESS["IM_LIST_LOADING"] = "Cargando...";
$MESS["IM_LIST_NOTIFICATIONS"] = "Notificaciones";
$MESS["IM_M_MESSAGE_SEND"] = "Enviar";
$MESS["IM_M_TEXTAREA"] = "Redactar un mensaje...";
$MESS["IM_PROMO_VIDEO_01042020_MOBILE"] = "¡Las videollamadas mejoraron exponencialmente!#BR##BR#¡Pruébelo ahora!#BR##BR# Use las llamadas de voz y video para ahorrar tiempo.";
$MESS["IM_READ_ALL"] = "Leer todo";
$MESS["IM_REFRESH_ERROR"] = "No es posible conectarse a su Bitrix24. Quizás su servidor tenga problemas de conectividad a Internet. Intentaremos de nuevo en algunos segundos.";
$MESS["IM_REFRESH_TITLE"] = "Actualizando...";
$MESS["IM_SCOPE_CHATS"] = "Chats";
$MESS["IM_SCOPE_DEPARTMENTS"] = "Departamentos";
$MESS["IM_SCOPE_USERS"] = "Empleados";
$MESS["INVITE_RESEND_DONE"] = "La invitación fue enviada con éxito.";
$MESS["OL_LIST_EMPTY"] = "Sin conversaciones";
$MESS["OL_SECTION_ANSWERED"] = "Contestada";
$MESS["OL_SECTION_NEW"] = "Sin respuesta";
$MESS["OL_SECTION_PIN"] = "Fijado";
$MESS["OL_SECTION_WORK_2"] = "En progreso";
$MESS["SEARCH_RECENT"] = "Búsqueda reciente";
$MESS["U_STATUS_OFFLINE"] = "Desconectado";
$MESS["U_STATUS_ONLINE"] = "En línea";
$MESS["WIDGET_CHAT_CREATE_TITLE"] = "Nueva conversación";
