<?php
$MESS["IM_DEPARTMENT_START"] = "Aby rozpocząć wyszukiwanie, wprowadź nazwę działu.";
$MESS["IM_USER_SELECTOR_API_ERROR"] = "Błąd podczas podłączania nowych uczestników. Spróbuj ponownie później.";
$MESS["IM_USER_SELECTOR_CONNECTION_ERROR"] = "Błąd podczas łączenia z Bitrix24. Sprawdź połączenie sieciowe.";
