<?php
$MESS["SEARCH"] = "Recherche...";
$MESS["SEARCH_CHATS"] = "CHATS";
$MESS["SEARCH_EMPLOYEES"] = "EMPLOYÉS";
$MESS["SEARCH_EMPTY"] = "Aucun résultat pour \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Charger plus...";
$MESS["SEARCH_MORE_READY"] = "Affichage des éléments trouvés. Charger plus...";
