/**
 * @module im/messenger/lib/counters
 */
jn.define('im/messenger/lib/counters', (require, exports, module) => {

	const { Counters } = jn.require('im/messenger/lib/counters/counters');

	module.exports = {
		Counters,
	};
});
