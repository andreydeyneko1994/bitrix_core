/**
 * @module im/messenger/controller/dialog
 */
jn.define('im/messenger/controller/dialog', (require, exports, module) => {

	const { Dialog } = jn.require('im/messenger/controller/dialog/dialog');

	module.exports = {
		Dialog,
	};
});
