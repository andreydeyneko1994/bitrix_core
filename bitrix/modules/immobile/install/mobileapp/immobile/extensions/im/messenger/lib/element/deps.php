<?php

return [
	'extensions' => [
		'type',
		'loc',
		'im:messenger/lib/helper',
		'im:messenger/lib/params',
	],
	'bundle' => [
		'./src/chat-avatar/chat-avatar',
		'./src/chat-title/chat-title',
	],
];