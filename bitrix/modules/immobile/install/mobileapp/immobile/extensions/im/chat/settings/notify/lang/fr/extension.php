<?php
$MESS["SE_NOTIFY_CATEGORY_IM_TITLE"] = "Chat et notifications";
$MESS["SE_NOTIFY_DEFAULT_TITLE"] = "Paramètres";
$MESS["SE_NOTIFY_DESC"] = "Notifications push et compteur";
$MESS["SE_NOTIFY_EMPTY"] = "Impossible de configurer cette catégorie.";
$MESS["SE_NOTIFY_LOADING"] = "Chargement...";
$MESS["SE_NOTIFY_MAIN_COUNTER_DETAIL_DESC"] = "Sélectionnez les outils dont vous voulez voir les compteurs dans le résumé, sur l'icône de l'application";
$MESS["SE_NOTIFY_MAIN_COUNTER_TITLE"] = "Compteur d'application";
$MESS["SE_NOTIFY_NOTIFY_TYPES_TITLE"] = "Notifications";
$MESS["SE_NOTIFY_PUSH_TITLE"] = "Autoriser les notifications";
$MESS["SE_NOTIFY_SMART_FILTER_DESC"] = "Le filtre intelligent rend les notifications moins intrusives. Vous n'en recevrez pas quand vous naviguerez sur internet ou quand vous utiliserez l'application de bureau sans pause notable.";
$MESS["SE_NOTIFY_SMART_FILTER_TITLE"] = "Utiliser le filtre intelligent";
$MESS["SE_NOTIFY_TITLE"] = "Notifications";
