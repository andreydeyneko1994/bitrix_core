/**
 * @module im/messenger/controller/chat-creator
 */
jn.define('im/messenger/controller/chat-creator', (require, exports, module) => {

	const { ChatCreator } = jn.require('im/messenger/controller/chat-creator/chat-creator');

	module.exports = {
		ChatCreator,
	};
});
