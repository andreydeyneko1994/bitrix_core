<?php
$MESS["MOBILE_EXT_CHAT_SELECTOR_BUTTON_MORE"] = "Más";
$MESS["MOBILE_EXT_CHAT_SELECTOR_BUTTON_SEARCH_NETWORK"] = "Búsqueda de Bitrix24.Network";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_COMMON_CHAT_USER_TITLE"] = "Chats del usuario";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_COMMON_TITLE"] = "Chats en vivo";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_CUSTOM_TITLE"] = "Empleados";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_NETWORK_TITLE"] = "Canales abiertos externos";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_RECENT_TITLE"] = "Búsqueda reciente";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE"] = "Buscar chat";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE_BUTTON_LESS"] = "Mostrar menos";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE_BUTTON_MORE"] = "Mostrar más";
