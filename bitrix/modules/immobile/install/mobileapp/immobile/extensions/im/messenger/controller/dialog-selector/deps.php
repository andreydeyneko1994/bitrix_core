<?php

return [
	'extensions' => [
		'im:chat/selector/chat',
		'im:messenger/const',
		'im:messenger/controller/base',
		'im:messenger/controller/dialog-list',
		'im:messenger/lib/logger',
		'im:messenger/lib/converter',
		'im:messenger/lib/params',
		'im:chat/utils',
	],
];