<?php
$MESS["SEARCH"] = "Buscar...";
$MESS["SEARCH_BACK"] = "Volver a la lista";
$MESS["SEARCH_CATEGORY_CHAT"] = "CHATS";
$MESS["SEARCH_CATEGORY_DEPARTMENT_USER"] = "Empleados";
$MESS["SEARCH_CATEGORY_LINE"] = "CANALES ABIERTOS";
$MESS["SEARCH_CATEGORY_USER"] = "EMPLEADOS";
$MESS["SEARCH_EMPTY"] = "No hay resultados para \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Cargar más...";
$MESS["SEARCH_MORE_READY"] = "Mostrando elementos encontrados. Carga más...";
