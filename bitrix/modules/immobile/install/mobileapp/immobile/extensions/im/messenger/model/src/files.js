/* eslint-disable flowtype/require-return-type */

/**
 * @module im/messenger/model/files
 */
jn.define('im/messenger/model/files', (require, exports, module) => {

	const filesModel = {
		namespaced: true,
		state: () => ({

		}),
		getters: {

		},
		actions: {

		},
		mutations: {

		}
	};

	module.exports = { filesModel };
});
