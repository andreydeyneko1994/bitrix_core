<?php

return [
	'extensions' => [
		'type',
		'loc',
		'im:messenger/controller/base',
		'im:messenger/lib/params',
		'im:chat/dataconverter',
		'im:chat/searchscopes',
		'im:chat/utils',
	],
	'bundle' => [
		'./src/chat-creator',
	],
];