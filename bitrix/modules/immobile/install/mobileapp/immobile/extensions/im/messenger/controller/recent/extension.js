/**
 * @module im/messenger/controller/recent
 */
jn.define('im/messenger/controller/recent', (require, exports, module) => {

	const { Recent } = jn.require('im/messenger/controller/recent/recent');

	module.exports = {
		Recent,
	};
});
