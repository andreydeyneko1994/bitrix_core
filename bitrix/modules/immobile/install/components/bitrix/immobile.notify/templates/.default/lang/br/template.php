<?php
$MESS["NM_DOWNTEXT"] = "Solte para atualizar...";
$MESS["NM_EMPTY"] = "Não há novas notificações";
$MESS["NM_FOLD"] = "Recolher";
$MESS["NM_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g: i a";
$MESS["NM_LOADTEXT"] = "descarregando...";
$MESS["NM_MORE"] = "Detalhes";
$MESS["NM_MORE_USERS"] = "mais #COUNT# pessoas";
$MESS["NM_PULLTEXT"] = "Puxe para baixo para atualizar...";
$MESS["NM_SYSTEM_USER"] = "Mensagem do sistema";
$MESS["NM_TITLE"] = "Notificações";
$MESS["NM_TITLE_2"] = "Atualizando...";
$MESS["NM_UNFOLD"] = "Expandir";
