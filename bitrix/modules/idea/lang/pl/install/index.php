<?
$MESS["ERR_BLOG_MODULE_NOT_INSTALLED"] = "Moduł Blogi nie jest zainstalowany.";
$MESS["ERR_IDEA_INSTALL_TITLE"] = "Błąd instalowania modułu Managera Pomysłów.";
$MESS["ERR_SESSION_EXPIRED"] = "Twoja sesja wygasła. Proszę spróbować zainstalować moduł Managera Pomysłów ponownie.";
$MESS["IDEA_INSTALL_TITLE"] = "Ideas Module Installation";
$MESS["IDEA_MODULE_DESCRIPTION"] = "Zarządzanie pomysłami";
$MESS["IDEA_MODULE_NAME"] = "Zarządzanie pomysłami";
$MESS["IDEA_UF_ANSWER_ID_DESCRIPTION"] = "Official reply ID";
$MESS["IDEA_UF_CATEGORY_CODE_DESCRIPTION"] = "Kategoria";
$MESS["IDEA_UF_ORIGINAL_ID_DESCRIPTION"] = "Kopiuj";
$MESS["IDEA_UF_STATUS_COMPLETED_TITLE"] = "Zaimplementowane";
$MESS["IDEA_UF_STATUS_DESCRIPTION"] = "Status";
$MESS["IDEA_UF_STATUS_NEW_TITLE"] = "Nowe";
$MESS["IDEA_UF_STATUS_PROCESSING_TITLE"] = "W toku";
?>