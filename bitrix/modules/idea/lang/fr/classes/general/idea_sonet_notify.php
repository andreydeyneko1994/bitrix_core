<?
$MESS["IDEA_SONET_GROUP_SETTINGS"] = "Toutes modifications des idées de ce groupe";
$MESS["IDEA_SONET_GROUP_SETTINGS_1"] = "Idées du groupe #TITLE#";
$MESS["IDEA_SONET_GROUP_SETTINGS_2"] = "Messages sur les idées du groupe #TITLE#";
$MESS["IDEA_SONET_NOTIFY_TITLE"] = "Avez-vous une Idée ?";
$MESS["IDEA_SONET_NOTIFY_TITLE_24"] = "a créé une idée";
?>