<?
$MESS["ADD_IDEA_COMMENT_TEMPLATE"] = "Message d'information du site web #SITE_NAME#
------------------------------------------

Un nouveau commentaire sur l'idée sur le site web #SERVER_NAME# a été rajouté.

Thème de l'idée:
#IDEA_TITLE#

Auteur du commentaire: #AUTHOR#
Date du commentaire: #DATE_CREATE#
Texte du commentaire:

#POST_TEXT#

Adresse du message:
#FULL_PATH#

Ce message a été généré automatiquement.";
$MESS["ADD_IDEA_TEMPLATE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Une nouvelle idée a été ajoutée dans la catégorie #CATEGORY# sur le site #SERVER_NAME#.

Thème: #TITLE#

Auteur: #AUTHOR#

Ajouté: #DATE_PUBLISH#

Description:
#IDEA_TEXT#

Adresse de l'idée:
#FULL_PATH#

Ce message a été généré automatiquement.";
$MESS["IDEA_EVENT_ADD_IDEA"] = "Une nouvelle idée est ajoutée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT"] = "Un nouveau commentaire est ajouté à l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_AUTHOR"] = "Nom de l'auteur du commentaire pour l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_DATE_CREATE"] = "Date et l'heure de création d'un commentaire pour l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_EMAIL_TO"] = "Adresse emaild'abonnement";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_FULL_PATH"] = "Chemin d'accès au commentaire de l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_IDEA_COMMENT_TEXT"] = "Le texte du commentaire sur l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_IDEA_TITLE"] = "Titre de l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_AUTHOR"] = "Nom de l'auteur de l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_CATEGORY"] = "Catégorie";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_DATE_PUBLISH"] = "La date et le temps de la création de l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_EMAIL_TO"] = "Adresse emaild'abonnement";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_FULL_PATH"] = "Chemin vers l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_IDEA_TEXT"] = "Texte de l'idée";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_TITLE"] = "Titre de l'idée";
?>