<?
$MESS["ERR_BLOG_MODULE_NOT_INSTALLED"] = "Le module 'Blogs' n'a pas été installé.";
$MESS["ERR_IBLOCK_MODULE_NOT_INSTALLED"] = "Module des blocs d'information non installé..";
$MESS["ERR_IDEA_INSTALL_TITLE"] = "Erreur d'installer le module Gestionnaire de Idea.";
$MESS["ERR_SESSION_EXPIRED"] = "Votre session a expiré. S'il vous plaît essayez d'installer à nouveau le module Gestionnaire de Idée.";
$MESS["IDEA_INSTALL_TITLE"] = "Installation du module 'Gestionnaire des idées'";
$MESS["IDEA_MODULE_DESCRIPTION"] = "Manager des idées";
$MESS["IDEA_MODULE_NAME"] = "Manager des idées";
$MESS["IDEA_UF_ANSWER_ID_DESCRIPTION"] = "ID de la réponse officielle";
$MESS["IDEA_UF_CATEGORY_CODE_DESCRIPTION"] = "Catégorie";
$MESS["IDEA_UF_ORIGINAL_ID_DESCRIPTION"] = "Copie";
$MESS["IDEA_UF_STATUS_COMPLETED_TITLE"] = "Mis en uvre";
$MESS["IDEA_UF_STATUS_DESCRIPTION"] = "Statut";
$MESS["IDEA_UF_STATUS_NEW_TITLE"] = "Créer";
$MESS["IDEA_UF_STATUS_PROCESSING_TITLE"] = "En cours d'exécution";
?>