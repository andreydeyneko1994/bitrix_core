<?php
$MESS["IMBOT_SUPPORT24_QUESTION_BUTTON_ASK_TITLE"] = "Haz una pregunta";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_DESCRIPTION"] = "Haga clic en el siguiente botón para hacer otra pregunta. En esta ventana se mostrarán todas las preguntas que hizo.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_TITLE"] = "¿Tiene más dudas?";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_RESTRICTION_NOT_ADMIN"] = "Permisos insuficientes. Solo un administrador puede hacer más preguntas.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH"] = "Búsqueda...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCHING"] = "Buscando...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH_NOT_FOUND"] = "Lo sentimos, pero no pudimos encontrar nada";
