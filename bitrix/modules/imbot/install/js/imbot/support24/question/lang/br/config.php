<?php
$MESS["IMBOT_SUPPORT24_QUESTION_BUTTON_ASK_TITLE"] = "Fazer uma pergunta";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_DESCRIPTION"] = "Clique no botão abaixo para fazer outra pergunta. Esta janela mostrará todas as perguntas que você fez aqui.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_TITLE"] = "Mais alguma dúvida?";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_RESTRICTION_NOT_ADMIN"] = "Permissões insuficientes. Apenas um administrador pode fazer mais perguntas.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH"] = "Pesquisa...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCHING"] = "Pesquisando...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH_NOT_FOUND"] = "Desculpe, mas não conseguimos encontrar nada";
