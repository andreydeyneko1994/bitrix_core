<?php
$MESS["IMBOT_SUPPORT24_QUESTION_BUTTON_ASK_TITLE"] = "Poser une question";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_DESCRIPTION"] = "Cliquez sur le bouton ci-dessous pour poser une autre question. Cette fenêtre affichera celles que vous avez posées ici.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_TITLE"] = "Vous avez d'autres questions ?";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_RESTRICTION_NOT_ADMIN"] = "Droits insuffisants. Seul un administrateur peut poser d'autres questions.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH"] = "Recherche...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCHING"] = "Recherche...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH_NOT_FOUND"] = "Désolé, mais nous n'avons rien trouvé";
