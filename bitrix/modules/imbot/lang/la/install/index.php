<?php
$MESS["IMBOT_CHECK_IM"] = "El módulo de \"Mensajería Instantánea\" no está instalado.";
$MESS["IMBOT_CHECK_IM_VERSION"] = "Por favor, actualice el módulo de \"Mensajería Instantánea\" a la versión 16.1.0";
$MESS["IMBOT_CHECK_PUBLIC_PATH"] = "Dirección pública especificada es incorrecta.";
$MESS["IMBOT_CHECK_PULL"] = "El módulo \"Push and Pull\" no está instalado o servidor de espera no está configurado.";
$MESS["IMBOT_INSTALL_TITLE"] = "Instalación del módulo \"Bitrix24 Chat Bots\"";
$MESS["IMBOT_MODULE_DESCRIPTION"] = "El módulo Chat Bots para su uso con Bitrix24.";
$MESS["IMBOT_MODULE_NAME"] = "Chatbots de Bitrix24";
$MESS["IMBOT_UNINSTALL_HAS_BOTS"] = "Todos los bots de chat registrados deben desinstalarse antes de desinstalar el módulo.";
$MESS["IMBOT_UNINSTALL_QUESTION"] = "¿Seguro que quieres eliminar el módulo? <br> Esto desactivará todos los robots de chat y borrar su historial de mensajes.";
$MESS["IMBOT_UNINSTALL_TITLE"] = "Desinstalación del módulo \"Bitrix24 Chat Bots\"";
$MESS["IMBOT_USERS_LINK"] = "Lista de usuarios";
