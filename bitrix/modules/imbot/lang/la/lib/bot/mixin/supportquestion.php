<?php
$MESS["CHAT_QUESTION_DESC"] = "Envíe aquí una nueva pregunta a nuestros agentes de asistencia técnica.";
$MESS["CHAT_QUESTION_DISALLOWED"] = "No puede publicar preguntas adicionales en el servicio de asistencia de su plan actual. Siempre podrá enviar su pregunta al chat general del servicio de asistencia.";
$MESS["CHAT_QUESTION_GREETING"] = "Utilice este chat para hacer más preguntas a nuestros agentes de asistencia técnica.[br] Puede cambiar el nombre de este chat por otro más significativo que le permita encontrarlo más rápidamente.";
$MESS["CHAT_QUESTION_LIMITED"] = "Excedió su límite de #NUMBER# preguntas adicionales para el servicio de asistencia. Espere la respuesta o elimine alguna de las preguntas que hizo para enviar una nueva.";
$MESS["CHAT_QUESTION_TITLE"] = "Solicitud de asistencia técnica - #NUMBER#";
$MESS["SUPPORT_QUESTION_DESCRIPTION"] = "Envíe aquí una nueva pregunta a nuestros agentes de asistencia técnica.";
$MESS["SUPPORT_QUESTION_TITLE"] = "Nueva pregunta";
