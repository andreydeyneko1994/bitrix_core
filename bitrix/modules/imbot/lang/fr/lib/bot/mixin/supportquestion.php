<?php
$MESS["CHAT_QUESTION_DESC"] = "Envoyez ici une nouvelle question à nos agents du service d'assistance.";
$MESS["CHAT_QUESTION_DISALLOWED"] = "Votre offre actuelle ne vous permet pas d'envoyer d'autres questions à l'assistance technique. Vous pouvez toujours envoyer votre question au chat général de l'assistance technique.";
$MESS["CHAT_QUESTION_GREETING"] = "Utilisez ce chat pour poser des questions à nos agents du service d'assistance.[br] Vous pouvez renommer ce chat en quelque chose de plus significatif pour vous aider à le retrouver plus rapidement.";
$MESS["CHAT_QUESTION_LIMITED"] = "Vous avez dépassé votre limite de #NUMBER# questions supplémentaires à l'assistance technique. Veuillez attendre la réponse ou retirer l'une des questions que vous avez posées pour en envoyer une nouvelle.";
$MESS["CHAT_QUESTION_TITLE"] = "Demande d'assistance technique - #NUMBER#";
$MESS["SUPPORT_QUESTION_DESCRIPTION"] = "Envoyez ici une nouvelle question à nos agents du service d'assistance.";
$MESS["SUPPORT_QUESTION_TITLE"] = "Nouvelle question";
