<?php
$MESS["SUPPORT_BOX_QUEUE_NUMBER"] = "Vous avez actuellement le numéro #QUEUE_NUMBER# dans la file d'attente";
$MESS["SUPPORT_BOX_QUEUE_NUMBER_REFRESH"] = "Actualiser";
$MESS["SUPPORT_BOX_START_DIALOG_F"] = "Conversation démarrée par #USER_FULL_NAME#";
$MESS["SUPPORT_BOX_START_DIALOG_M"] = "Conversation démarrée par #USER_FULL_NAME#";
