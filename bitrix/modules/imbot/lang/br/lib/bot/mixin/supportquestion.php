<?php
$MESS["CHAT_QUESTION_DESC"] = "Envie aqui uma nova pergunta para nossos agentes de suporte técnico.";
$MESS["CHAT_QUESTION_DISALLOWED"] = "Você não pode postar perguntas extras ao Suporte Técnico no seu plano atual. Você sempre pode enviar sua pergunta para o bate-papo geral do Suporte Técnico.";
$MESS["CHAT_QUESTION_GREETING"] = "Use este bate-papo para fazer mais perguntas aos nossos agentes de suporte técnico.[br] Você pode renomear este bate-papo para algo mais significativo para ajudar a encontrá-lo mais rapidamente.";
$MESS["CHAT_QUESTION_LIMITED"] = "Você excedeu seu limite de #NUMBER# perguntas extras para o Suporte Técnico. Aguarde a resposta ou retire uma das perguntas que você fez para enviar uma nova.";
$MESS["CHAT_QUESTION_TITLE"] = "Solicitação de suporte técnico - #NUMBER#";
$MESS["SUPPORT_QUESTION_DESCRIPTION"] = "Envie aqui uma nova pergunta para nossos agentes de suporte técnico.";
$MESS["SUPPORT_QUESTION_TITLE"] = "Nova pergunta";
