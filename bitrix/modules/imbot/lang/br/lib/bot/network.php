<?php
$MESS["IMBOT_NETWORK_BOT_WORK_POSITION"] = "Canal Aberto";
$MESS["IMBOT_NETWORK_ERROR_CONVERTING_PUNYCODE"] = "Erro ao converter o nome do host #HOST# para Punycode: #ERROR#";
$MESS["IMBOT_NETWORK_ERROR_LINE_DISABLED"] = "Mensagem não enviada![br] Este canal aberto está bloqueado para novas mensagens.";
$MESS["IMBOT_NETWORK_ERROR_NOT_FOUND"] = "Mensagem não enviada![br] Este canal aberto não está disponível no momento";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1"] = "Olá, #USER_NAME#! Bem-vindo ao Bitrix24. Pelos primeiros 30 dias, estarei com você para ajudar a entender o Bitrix24. Por favor, me conte mais sobre a sua empresa e o que você deseja do Bitrix24? Quais ferramentas você gostaria de utilizar?";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1_2"] = "Você já teve a chance de visitar o nosso [url=https://www.youtube.com/user/Bitrix24/videos]canal no YouTube[/url]? Aqui você vai encontrar vários webinars e vídeos de treinamento sobre o Bitrix24 em geral, bem como de ferramentas específicas, como CRM, telefonia, tarefas e gerenciamento de projetos. É uma boa ideia compartilhar esses vídeos com os demais usuários do seu Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_2"] = "Obrigado por escolher o Bitrix24! Certifique-se de visitar a nossa [url=https://helpdesk.bitrix24.com/]Seção de Serviço de Atendimento[/url] para aprender como tudo funciona dentro do Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_7"] = "Faz uma semana que nos conhecemos e isso é legal :) A propósito, você já visitou a nossa [url=https://www.bitrix24.com/partners/]Seção Parceiro[/url], para ver se há um revendedor Bitrix24 no seu país? Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_15"] = "Obrigado por estar com a gente nas ultimas duas semanas:)[br]
Avise-nos se pudermos ajudar em algo. Estamos aqui para ajudá-lo a entender melhor o Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_23"] = "Gostaria de avisar que você tem somente mais 7 dias para bater papo com o seu Assistente Online :) Estamos aqui para esclarecer suas dúvidas.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_30"] = "Seu acesso ao Assistente Online acabou agora. Esperamos que ele tenha ajudado você a aprender algo novo. Sempre que tiver dúvidas, esses recursos ainda estão disponíveis para você:

- #LINK_START_4#Bate-papo do Serviço de Atendimento#LINK_END_4#: disponível nos planos comerciais. Você irá desfrutar da conversa direta com um representante do Serviço de Atendimento.
- #LINK_START_2#Support24#LINK_END_2#: tudo sobre o Bitrix24, um extenso guia dos recursos do sistema.
- #LINK_START_3#Webinars Grátis#LINK_END_3#: estamos no ar todas as semanas com uma nova receita para ajudar você a enfrentar seus problemas ao usar o Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Parceiros locais[/URL] - procure um revendedor Bitrix24 em seu país. Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração.

Obrigado por escolher Bitrix24!";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_1"] = "O Assistente de Integração está disponível durante as primeiras 24 horas para ajudar você a aprender mais sobre o Bitrix24. Se você tiver mais perguntas, mais tarde, você pode querer usar: 

- #LINK_START_2#Support24#LINK_END_2# - acessa a seção Support24 de perguntas frequentes, manuais de treinamento on-line e tutoriais em vídeo.
- #LINK_START_3#Free Webinars#LINK_END_3# - webinars semanais e gravações webinar abrangem todos os aspectos do Bitrix24.
- [URL = https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - procure um revendedor Bitrix24 em seu país. Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração. 

Obrigado por escolher Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_7"] = "O Assistente de Integração está disponível durante os primeiros 7 dias para ajudar você a aprender mais sobre o Bitrix24. Se você tiver mais perguntas, mais tarde, você pode querer usar: 

- #LINK_START_2#Support24#LINK_END_2# - acessa a seção Support24 de perguntas frequentes, manuais de treinamento on-line e tutoriais em vídeo.
- #LINK_START_3#Free Webinars#LINK_END_3# - webinars semanais e gravações webinar abrangem todos os aspectos do Bitrix24.
- [URL = https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - procure um revendedor Bitrix24 em seu país. Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração. 

Obrigado por escolher Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_1"] = "Olá, #USER_NAME#! 

A sessão de bate-papo com o Assistente de Integração terminou. Se você tiver mais perguntas, mais tarde, você pode querer usar: 

- #LINK_START_2#Support24#LINK_END_2# - acessa a seção Support24 de perguntas frequentes, manuais de treinamento on-line e tutoriais em vídeo.
- #LINK_START_3#Free Webinars#LINK_END_3# - webinars semanais e gravações webinar abrangem todos os aspectos do Bitrix24.
- [URL = https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - procure um revendedor Bitrix24 em seu país. Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração. 

Obrigado por escolher Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_7"] = "Olá, #USER_NAME#! 

A sessão de bate-papo com o Assistente de Integração terminou. Se tiver mais perguntas, mais tarde, você pode querer usar: 

- #LINK_START_2#Support24#LINK_END_2# - acessa a seção Support24 de perguntas frequentes, manuais de treinamento on-line e tutoriais em vídeo.
- #LINK_START_3#Free Webinars#LINK_END_3# - webinars semanais e gravações webinar abrangem todos os aspectos do Bitrix24.
- [URL = https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - procure um revendedor Bitrix24 em seu país. Parceiros locais podem fornecer serviços de treinamento, configuração, customização e integração. 

Obrigado por escolher Bitrix24 :)";
