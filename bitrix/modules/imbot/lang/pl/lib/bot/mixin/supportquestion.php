<?php
$MESS["CHAT_QUESTION_DESC"] = "Tu prześlesz nowe pytanie do konsultantów naszego działu pomocy.";
$MESS["CHAT_QUESTION_DISALLOWED"] = "W ramach bieżącego planu nie możesz wysyłać dodatkowych pytań do działu pomocy. Zawsze możesz przesłać swoje pytanie na ogólny czat działu pomocy.";
$MESS["CHAT_QUESTION_GREETING"] = "Użyj tego czatu, aby zadać więcej pytań konsultantom naszego działu pomocy.[br] Możesz zmienić nazwę tego czatu na swoją, aby móc szybciej go znaleźć.";
$MESS["CHAT_QUESTION_LIMITED"] = "Przekroczono limit #NUMBER# dodatkowych pytań do działu pomocy. Poczekaj na odpowiedź lub wycofaj jedno z zadanych pytań, aby przesłać nowe.";
$MESS["CHAT_QUESTION_TITLE"] = "Zgłoszenie do działu pomocy – #NUMBER#";
$MESS["SUPPORT_QUESTION_DESCRIPTION"] = "Tu prześlesz nowe pytanie do konsultantów naszego działu pomocy.";
$MESS["SUPPORT_QUESTION_TITLE"] = "Nowe pytanie";
