<?php
$MESS["IMBOT_NETWORK_BOT_WORK_POSITION"] = "Otwarty Kanał";
$MESS["IMBOT_NETWORK_ERROR_CONVERTING_PUNYCODE"] = "Błąd podczas konwersji nazwy hosta #HOST# na Punycode: #ERROR#";
$MESS["IMBOT_NETWORK_ERROR_LINE_DISABLED"] = "Wiadomość nie została wysłana![br] Wybrany Otwarty Kanał jest zablokowany dla nowych wiadomości.";
$MESS["IMBOT_NETWORK_ERROR_NOT_FOUND"] = "Wiadomość nie została wysłana![br] Wybrany Otwarty Kanał jest obecnie niedostępny";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1"] = "Cześć #USER_NAME#! Witaj w Bitrix24. Przez pierwsze 30 dni będę z tobą i pomogę ci w nauce Bitrix24. Opowiedz mi więcej o swojej firmie i oczekiwaniach co do Bitrix24? Z jakich narzędzi chcesz korzystać?";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1_2"] = "Czy miałeś/aś okazję odwiedzić nasz [url=https://www.youtube.com/user/Bitrix24/videos]kanał YouTube[/url]? Znajdziesz na nim liczne webinaria i filmy treningowe dotyczące ogólnych funkcji Bitrix24, jak i konkretnych narzędzi, jak na przykład CRM, telefonii, zadań i zarządzania projketami. Warto podzielić się tymi filmami z innymi użytkownikami Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_2"] = "Dziękujemy za wybranie Bitrix24! Odwiedź [url=https://helpdesk.bitrix24.com/]sekcję Helpdesk [/url] na naszej stronie, by dowiedzieć się wszystkiego o tym, jak działa Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_7"] = "Minął tydzień odkąd się poznaliśmy i jest nam bardzo miło :) Tak przy okazji, czy już odwiedziłeś/aś naszą [url=https://www.bitrix24.com/partners/]Sekcję partnerów[/url], aby sprawdzić, czy w Twoim kraju są resellerzy Bitrix24? Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_15"] = "Hej, nie wierzę, że już minęły dwa tygodnie :)[br]
Daj nam znać, jeśli możemy jakoś pomóc.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_23"] = "Cześć, nie chcemy być namolni, ale zostało Ci tylko 7 dni czatowania z Twoim Asystentem Online :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_30"] = "Twój okres dostępu do Asystenta Online właśnie się zakończył. Mamy nadzieję, że pomógł Ci nauczyć się wielu nowych rzeczy. Jeśli będziesz mieć jakiekolwiek pytania, możesz skorzystać z poniższych zasobów:

- #LINK_START_4#Chat z Helpdeskiem#LINK_END_4#: dostępny tylko w planach komercyjnych. Będziesz mógł korzystać z bezpośrednich rozmów z konsultantami z naszego Helpdeska.
- #LINK_START_2#Support24#LINK_END_2#: wszystko o Bitrix24, szczegółowy przewodnik po funkcjonalnościach systemu.
- #LINK_START_3#Darmowe Webinaria#LINK_END_3#: jesteśmy na żywo co tydzień z nowymi poradami, które pomogą Ci rozwiązać wszystkie problemy, jakie możesz napotkać korzystając z Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Lokalni partnerzy[/URL] - sprawdź proszę lokalnych resellerów Bitrix24 w Twoim kraju. Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.
Dziękujemy za wybranie Bitrix24!";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_1"] = "Asystent Online jest dostępny dla Ciebie przez pierwsze 24 godziny, aby pomóc Ci dowiedzieć się wszystkiego o Bitrix24. Jeśli będziesz mieć jakiekolwiek dalsze pytania, możesz chcieć skorzystać z:

- #LINK_START_2#Support24#LINK_END_2# - odwiedź sekcję Support24, aby zobaczyć najczęściej zadawane pytania, instrukcje obsługi online oraz tutoriale video.
- #LINK_START_3#Free Webinars#LINK_END_3# - cotygodniowe webinaria i zapisy webinariów, które opisują wszystkie aspekty korzystania z Bitrix24.
- [URL=https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - zobacz informacje o lokalnych resellerach Bitrix24 w Twoim kraju. Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.

Dziękujemy za wybranie Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_7"] = "Asystent Online jest dostępny dla Ciebie przez pierwsze 7 dni, aby pomóc Ci dowiedzieć się wszystkiego o Bitrix24. Jeśli będziesz mieć jakiekolwiek dalsze pytania, możesz chcieć skorzystać z:

- #LINK_START_2#Support24#LINK_END_2# - odwiedź sekcję Support24, aby zobaczyć najczęściej zadawane pytania, instrukcje obsługi online oraz tutoriale video.
- #LINK_START_3#Free Webinars#LINK_END_3# - cotygodniowe webinaria i zapisy webinariów, które opisują wszystkie aspekty korzystania z Bitrix24.
- [URL=https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - zobacz informacje o lokalnych resellerach Bitrix24 w Twoim kraju. Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.

Dziękujemy za wybranie Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_1"] = "Witaj, #USER_NAME#!

Zakończono czas z Asystentem Wprowadzającym. Jeśli masz dalsze pytania, może chcesz sprawdzić:

- #LINK_START_2#Support24#LINK_END_2# - odwiedź sekcję Support24, aby zobaczyć najczęściej zadawane pytania, instrukcje obsługi online oraz tutoriale video.
- #LINK_START_3#Free Webinars#LINK_END_3# - cotygodniowe webinaria i zapisy webinariów, które opisują wszystkie aspekty korzystania z Bitrix24.
- [URL=https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - zobacz informacje o lokalnych resellerach Bitrix24 w Twoim kraju. Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.

Dziękujemy za wybranie Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_7"] = "Witaj, #USER_NAME#!

Zakończono czas z Asystentem Online. Jeśli masz dalsze pytania, może chcesz sprawdzić:

- #LINK_START_2#Support24#LINK_END_2# - odwiedź sekcję Support24, aby zobaczyć najczęściej zadawane pytania, instrukcje obsługi online oraz tutoriale video.
- #LINK_START_3#Free Webinars#LINK_END_3# - cotygodniowe webinaria i zapisy webinariów, które opisują wszystkie aspekty korzystania z Bitrix24.
- [URL=https://www.bitrix24.com/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Local partners[/URL] - zobacz informacje o lokalnych resellerach Bitrix24 w Twoim kraju. Lokalni partnerzy mogą zapewnić szkolenia, pomóc rozpocząć korzystać z naszych usług, dopasować je do Twoich potrzeb i zintegrować je z istniejącymi systemami.

Dziękujemy za wybranie Bitrix24 :)";
