<?
$MESS["IMBOT_GIPHY_BOT_EMAIL"] = "giphy@bitrix24.com";
$MESS["IMBOT_GIPHY_BOT_NAME"] = "Giphy";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "Ten bot pomoże odnaleźć obraz pasujący do zapytania";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "temat obrazka";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "Więcej!";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "Wyślij obrazek pasujący do Twojego zapytania";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "Nie mogę odnaleźć niczego pasującego do Twojego zapytania :( [br] Jednak mam coś podobnego:";
$MESS["IMBOT_GIPHY_ICON_BROWSE_DESCRIPTION"] = "Przeglądaj i wybierz odpowiednie obrazy w formacie GIF";
$MESS["IMBOT_GIPHY_ICON_BROWSE_TITLE"] = "Przeglądaj obrazy w formacie GIF";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "Niczego nie znaleziono :(";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "Mogę pomóc w znalezieniu obrazów pasuących do Twojego zapytania! [br] Wpisz coś, a ja wybiorę jeden dla Ciebie! :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "Mogę pomóc w znalezieniu obrazów pasuących do Twojego zapytania! [br] Wspomnij mnie w swojej wiadomości, albo kliknij w mój avatar:)";
?>