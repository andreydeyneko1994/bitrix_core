<?
$MESS["BPDT_DATE_CURRENT_TZ"] = "Meu horário";
$MESS["BPDT_DATE_INVALID"] = "O valor do campo não é uma data válida.";
$MESS["BPDT_DATE_MOBILE_SELECT"] = "Selecionar";
$MESS["BPDT_DATE_SERVER_TZ"] = "Horário do servidor";
?>