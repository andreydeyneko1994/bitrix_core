<?php
$MESS["BIZPROC_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Ocorreram erros durante a limpeza: entidade ignorada (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Ocorreram erros durante a exportação de dados: entidade ignorada (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Ocorreram erros durante a importação de dados: entidade ignorada (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION_BP"] = "Ocorreram erros durante a importação de dados: fluxo de trabalho ignorado";
