<?php
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT"] = "Não é possível excluir constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT_RIGHT"] = "Permissões insuficientes para excluir constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE"] = "Não é possível excluir variável.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE_RIGHT"] = "Permissão insuficiente para excluir variável.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT"] = "Não é possível adicionar constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT_RIGHT"] = "Permissão insuficiente para adicionar ou editar constantes.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE"] = "Não é possível adicionar variável.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE_RIGHT"] = "Permissão insuficiente para adicionar ou editar variável.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_MODE_NOT_DEFINED"] = "Modo incorreto.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_CONSTANT"] = "A constante solicitada não existe.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_VARIABLE"] = "A variável solicitada não existe.";
