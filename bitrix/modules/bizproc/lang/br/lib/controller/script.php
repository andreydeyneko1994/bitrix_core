<?php
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_QUEUE"] = "Permissão insuficiente para remover script inteligente da programação";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_RUNNING_SCRIPT"] = "Não é possível excluir o script porque há fluxos de trabalho sendo executados por este script";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_SCRIPT"] = "Permissão insuficiente para excluir script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START"] = "Permissão insuficiente para executar script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START_INACTIVE"] = "Não é possível executar o script inteligente porque ele está desativado";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE"] = "Permissão insuficiente para interromper script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE_FINISHED"] = "Não é possível interromper o script inteligente porque ele já foi concluído";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_UPDATE_SCRIPT"] = "Permissão insuficiente para editar script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_DOCUMENT_ID_LIMIT"] = "Excedeu o máximo permitido de itens para executar um script inteligente (selecionado: #SELECTED#, máx: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_QUEUES_LIMIT"] = "Excedeu o máximo de instâncias de script inteligente (executando: #CNT#, máx: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_NOT_EXISTS"] = "O script inteligente que você solicitou não foi encontrado";
