<?php
$MESS["BIZPROC_JS_BP_FIELD_TYPE_ADD"] = "adicionar";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_CHOOSE_FILE"] = "Selecionar um arquivo";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NO"] = "Não";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NOT_SELECTED"] = "[não definido]";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS1"] = "Especificar cada variante em uma nova linha. Se o valor da variante e o nome da variante são diferentes, preceda o nome com o valor entre colchetes. Por exemplo: [v1]Variante 1";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS2"] = "Quando terminar, clique em \"Definir\"";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS3"] = "Estabelecer";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_YES"] = "Sim";
