<?php
$MESS["BIZPROC_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Hubo errores durante la limpieza: entidad omitida (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Hubo errores al exportar los datos: entidad omitida (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Hubo algunos errores al importar los datos: entidad omitida (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION_BP"] = "Hubo errores al importar los datos: flujo de trabajo omitido";
