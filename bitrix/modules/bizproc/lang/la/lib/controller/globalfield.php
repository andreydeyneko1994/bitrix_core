<?php
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT"] = "No se puede eliminar la constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT_RIGHT"] = "Permisos insuficientes para eliminar la constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE"] = "No se puede eliminar la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE_RIGHT"] = "Permisos insuficientes para eliminar la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT"] = "No se puede agregar la constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT_RIGHT"] = "Permisos insuficientes para agregar o editar constantes.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE"] = "No se puede agregar la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE_RIGHT"] = "Permisos insuficientes para agregar o editar la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_MODE_NOT_DEFINED"] = "Modo incorrecto.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_CONSTANT"] = "La constante solicitada no existe.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_VARIABLE"] = "La variable solicitada no existe.";
