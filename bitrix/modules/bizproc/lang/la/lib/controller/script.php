<?php
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_QUEUE"] = "Permisos insuficientes para eliminar el script inteligente de la programación";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_RUNNING_SCRIPT"] = "No se puede eliminar el script porque se están ejecutando flujos de trabajo en este momento";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_SCRIPT"] = "Permisos insuficientes para eliminar el script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START"] = "Permisos insuficientes para ejecutar un script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START_INACTIVE"] = "No se puede ejecutar el script inteligente porque está desactivado";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE"] = "Permisos insuficientes para detener el script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE_FINISHED"] = "No se puede detener el script inteligente porque ya finalizó";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_UPDATE_SCRIPT"] = "Permisos insuficientes para editar el script inteligente";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_DOCUMENT_ID_LIMIT"] = "Excedió el número máximo permitido de elementos para ejecutar un script inteligente (seleccionado: #SELECTED#, máx: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_QUEUES_LIMIT"] = "Excedió el número máximo de instancias de los scripts inteligentes (en ejecución: #CNT#, máx: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_NOT_EXISTS"] = "No se encontró el script inteligente que solicitó";
