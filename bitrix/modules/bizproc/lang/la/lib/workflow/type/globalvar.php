<?php
$MESS["BIZPROC_LIB_WF_TYPE_GLOBAL_VAR_VISIBILITY_FULL_DOCUMENT_PROCESS"] = "Variables del flujo de trabajo \"#PROCESS#\"";
$MESS["BIZPROC_LIB_WF_TYPE_GLOBAL_VAR_VISIBILITY_FULL_DOCUMENT_SECTION"] = "Variables de la sección \"#SECTION#\"";
$MESS["BIZPROC_LIB_WF_TYPE_GLOBAL_VAR_VISIBILITY_FULL_GLOBAL"] = "Constantes globales";
$MESS["BIZPROC_LIB_WF_TYPE_GLOBAL_VAR_VISIBILITY_FULL_MODULE"] = "Variables del módulo #MODULE#";
