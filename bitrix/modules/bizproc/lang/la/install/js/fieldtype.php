<?php
$MESS["BIZPROC_JS_BP_FIELD_TYPE_ADD"] = "agregar";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_CHOOSE_FILE"] = "Seleccionar un archivo";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NO"] = "No";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NOT_SELECTED"] = "[no establecido]";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS1"] = "Especifique cada variante en una nueva línea. Si el valor de la variante y el nombre de la variante son diferentes, el prefijo del nombre con el valor entre corchetes. Por ejemplo: [v1] Variante 1";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS2"] = "Cuando haya terminado, haga clic en \"Establecer\"";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS3"] = "Establecer";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_YES"] = "Sí";
