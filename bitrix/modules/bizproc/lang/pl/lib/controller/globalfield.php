<?php
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT"] = "Nie można usunąć stałej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT_RIGHT"] = "Niewystarczające uprawnienia do usunięcia stałej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE"] = "Nie można usunąć zmiennej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE_RIGHT"] = "Niewystarczające uprawnienia do usunięcia zmiennej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT"] = "Nie można dodać stałej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT_RIGHT"] = "Niewystarczające uprawnienia do dodawania lub edytowania stałej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE"] = "Nie można dodać zmiennej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE_RIGHT"] = "Niewystarczające uprawnienia do dodawania lub edytowania zmiennej.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_MODE_NOT_DEFINED"] = "Nieprawidłowy tryb.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_CONSTANT"] = "Żądana stała nie istnieje.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_VARIABLE"] = "Żądana zmienna nie istnieje.";
