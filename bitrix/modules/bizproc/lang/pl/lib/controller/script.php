<?php
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_QUEUE"] = "Niewystarczające uprawnienia do usunięcia inteligentnego skryptu z harmonogramu";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_RUNNING_SCRIPT"] = "Nie można usunąć skryptu, ponieważ ten skrypt aktualnie uruchamia przepływ pracy";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_SCRIPT"] = "Niewystarczające uprawnienia do usunięcia inteligentnego skryptu";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START"] = "Niewystarczające uprawnienia do uruchomienia inteligentnego skryptu";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START_INACTIVE"] = "Nie można uruchomić inteligentnego skryptu, ponieważ jest dezaktywowany";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE"] = "Niewystarczające uprawnienia do zatrzymania inteligentnego skryptu";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE_FINISHED"] = "Nie można zatrzymać inteligentnego skryptu, ponieważ został już zakończony";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_UPDATE_SCRIPT"] = "Niewystarczające uprawnienia do edycji inteligentnego skryptu";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_DOCUMENT_ID_LIMIT"] = "Przekroczono maksymalną dozwoloną liczbę pozycji, w których można uruchomić inteligentny skrypt (wybrano: #SELECTED#, maks.: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_QUEUES_LIMIT"] = "Przekroczono maksymalną liczbę wystąpień inteligentnego skryptu (uruchomiono: #CNT#, maks.: #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_NOT_EXISTS"] = "Żądany inteligentny skrypt nie został znaleziony";
