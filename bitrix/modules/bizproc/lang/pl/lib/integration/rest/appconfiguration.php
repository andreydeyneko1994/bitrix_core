<?php
$MESS["BIZPROC_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Podczas czyszczenia wystąpiły błędy: pominięto jednostkę (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Podczas eksportowania danych wystąpiły błędy: pominięto jednostkę (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Podczas importowania danych wystąpiły błędy: pominięto jednostkę (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION_BP"] = "Podczas importowania danych wystąpiły błędy: pominięto przepływ pracy";
