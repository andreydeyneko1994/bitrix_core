<?php
$MESS["BPDT_SELECT_INVALID"] = "Wybrany element nie jest elementem listy.";
$MESS["BPDT_SELECT_NOT_SET"] = "nie ustawiaj";
$MESS["BPDT_SELECT_OPTIONS1"] = "Wpisz każdą zmienną w nowej linii. Jeżeli wartość zmiennej i nazwa zmiennej są różne, dodaj prefiks do nazwy z wartością w nawiasie kwadratowym. Np.: [v1]Zmienna 1";
$MESS["BPDT_SELECT_OPTIONS2"] = "Wciśnij \"Ustaw\" gdy skończysz.";
$MESS["BPDT_SELECT_OPTIONS3"] = "Ustaw";
