<?
$MESS["BIZPROC_LIMIT_SIMULTANEOUS_PROCESSES"] = "Maksymalna liczba jednoczesnych procesów dla dokumentu";
$MESS["BIZPROC_LOG_CLEANUP_DAYS"] = "Liczba dni do utrzymania dziennika procesu biznesowego";
$MESS["BIZPROC_LOG_SKIP_TYPES"] = "Nie rejestruj wydarzeń";
$MESS["BIZPROC_LOG_SKIP_TYPES_1"] = "rozpoczęcie działania";
$MESS["BIZPROC_LOG_SKIP_TYPES_2"] = "zakończenie działania";
$MESS["BIZPROC_NAME_TEMPLATE"] = "Format wyświetlania nazwy";
$MESS["BIZPROC_OPTIONS_NAME_IN_SITE_FORMAT"] = "Format strony internetowej";
$MESS["BIZPROC_OPT_TIME_LIMIT_D"] = "dni";
$MESS["BIZPROC_OPT_TIME_LIMIT_H"] = "godziny";
$MESS["BIZPROC_OPT_TIME_LIMIT_M"] = "minuty";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION"] = "Kompresuj archiwalne dane";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_EMPTY"] = "domyślne";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_N"] = "Nie";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_Y"] = "Tak";
$MESS["BIZPROC_TAB_SET"] = "Ustawienia";
$MESS["BIZPROC_TAB_SET_ALT"] = "Ustawienia Modułu";
?>