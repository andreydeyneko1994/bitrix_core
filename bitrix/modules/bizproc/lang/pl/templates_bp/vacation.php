<?
$MESS["BPT1_BTF_P_APP"] = "Zatwierdzenie";
$MESS["BPT1_BT_AA11_NAME"] = "Zatwierdź {=Template:TargetUser_printable} urlop od {=Template:date_start} do {=Template:date_end}";
$MESS["BPT1_BT_AA11_STATUS_MESSAGE"] = "Do zatwierdzenia przez kierownictwo wyższego szczebla";
$MESS["BPT1_BT_AA11_TITLE"] = "Zatwierdź Urlop";
$MESS["BPT1_BT_CYCLE"] = "Cykl Zatwierdzania";
$MESS["BPT1_BT_IEBA15_V1"] = "Urlop został zatwierdzony.";
$MESS["BPT1_BT_IEBA15_V2"] = "Urlop nie został zatwierdzony.";
$MESS["BPT1_BT_IEBA1_V1"] = "Wymaga Dalszego Zatwierdzenia";
$MESS["BPT1_BT_IEBA2_V2"] = "Nie Wymaga Dalszego Zatwierdzenia";
$MESS["BPT1_BT_IF11_N"] = "Warunek";
$MESS["BPT1_BT_PARAM_BOOK"] = "Księgowość";
$MESS["BPT1_BT_PARAM_BOSS"] = "Osoby Upoważnione do Zatwierdzenia Urlopu";
$MESS["BPT1_BT_PARAM_OP_ADMIN"] = "Pracownicy Upoważnieni do Zarządzania Procesami";
$MESS["BPT1_BT_PARAM_OP_CREATE"] = "Pracownicy Upoważnieni do Tworzenia Nowych Procesów";
$MESS["BPT1_BT_PARAM_OP_READ"] = "Pracownicy Upoważnieni do Przeglądania Wszystkich p\\Procesów";
$MESS["BPT1_BT_P_TARGET"] = "Pracownik";
$MESS["BPT1_BT_RA17_NAME"] = "Rejestrowanie urlopu pracownika {=Template:TargetUser_printable}, {=Template:date_start} - {=Template:date_end}";
$MESS["BPT1_BT_RA17_STATUS_MESSAGE"] = "Przetwarzany w Księgowości";
$MESS["BPT1_BT_RA17_TBM"] = "Urlop został zatwierdzony i zarejestrowany.";
$MESS["BPT1_BT_RA17_TITLE"] = "Przetwarzany w Księgowości";
$MESS["BPT1_BT_RIA11_NAME"] = "Czy urlop {=Template:TargetUser_printable} od {=Template:date_start} do {=Template:date_end} wymaga dalszego zatwierdzenia?";
$MESS["BPT1_BT_RIA11_P1"] = "Wymaga Dalszego Zatwierdzenia";
$MESS["BPT1_BT_RIA11_P2"] = "Do zatwierdzenia przez";
$MESS["BPT1_BT_RIA11_TITLE"] = "Dodatkowe Zatwierdzenie";
$MESS["BPT1_BT_SA1_TITLE"] = "Sekwencja Działań";
$MESS["BPT1_BT_SFA12_TITLE"] = "Edytuj Dokument";
$MESS["BPT1_BT_SFA1_TITLE"] = "Zapisz Parametry Urlopu";
$MESS["BPT1_BT_SFTA12_ST"] = "Urlop został zatwierdzony przez kierownictwo wyższego szczebla.";
$MESS["BPT1_BT_SFTA12_T"] = "Ustaw Tekst Statusu";
$MESS["BPT1_BT_SNMA16_TEXT"] = "Twój urlop został zatwierdzony przez kierownictwo wyższego szczebla.";
$MESS["BPT1_BT_SNMA16_TITLE"] = "Wiadomość Sieci Społecznościowej";
$MESS["BPT1_BT_SNMA18_TEXT"] = "Twój urlop nie został zatwierdzony. {=A94751_67978_49922_99999:Comments}";
$MESS["BPT1_BT_SNMA18_TITLE"] = "Wiadomość Sieci Społecznościowej";
$MESS["BPT1_BT_SSTA14_ST"] = "Odrzucony";
$MESS["BPT1_BT_SSTA14_T"] = "Ustaw Tekst Statusu";
$MESS["BPT1_BT_SSTA18_ST"] = "Urlop został zarejestrowany.";
$MESS["BPT1_BT_SSTA18_T"] = "Ustaw Tekst Statusu";
$MESS["BPT1_BT_STA1_STATE_TITLE"] = "Zatwierdzenie";
$MESS["BPT1_BT_STA1_TITLE"] = "Ustaw Tekst Statusu";
$MESS["BPT1_BT_SWA"] = "Sekwencyjny proces biznesowy";
$MESS["BPT1_BT_T_DATE_END"] = "Urlop Kończy się";
$MESS["BPT1_BT_T_DATE_START"] = "Urlop Zaczyna się Od";
$MESS["BPT1_TTITLE"] = "Urlop";
$MESS["BPT_BT_AA7_DESCR"] = "Roczny Urlop";
$MESS["BPT_BT_AA7_FSTATE"] = "Działa";
$MESS["BPT_BT_AA7_NAME"] = "Urlop";
$MESS["BPT_BT_AA7_STATE"] = "Urlop";
$MESS["BPT_BT_AA7_TITLE"] = "Grafik Nieobecności";
?>