<?
$MESS["BPCGDOC_ADD"] = "Dodawanie";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Utwórz";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Usuń";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Aktualizacja";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "Nie";
$MESS["BPCGDOC_DELEGATE_LOG"] = "Użytkownik #FROM# delegował zadanie \"#NAME#\" na #TO#";
$MESS["BPCGDOC_DELEGATE_LOG_TITLE"] = "Delegowane";
$MESS["BPCGDOC_DELEGATE_NOTIFY_TEXT"] = "Workflow został przekazany Tobie: [URL=#TASK_URL#]#TASK_NAME#[/URL]";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "Brakuje ID szablonu procesu biznesowego.";
$MESS["BPCGDOC_ERROR_ACTION"] = "Zadanie \"#NAME#\": #ERROR#";
$MESS["BPCGDOC_ERROR_DELEGATE"] = "Zadanie \"#NAME#\" nie może być delegowane, ponieważ wybrany pracownik jest aktualnie przypisany do tego zadania.";
$MESS["BPCGDOC_ERROR_DELEGATE_0"] = "Zadanie \"#NAME#\" nie może być delegowane, ponieważ wybrany pracownik nie jest Twoim podwładnym.";
$MESS["BPCGDOC_ERROR_DELEGATE_1"] = "Zadanie \"#NAME#\" nie może być delegowane, ponieważ wybrany użytkownik nie pracuje dla Twojej firmy.";
$MESS["BPCGDOC_ERROR_DELEGATE_2"] = "Zadanie \"#NAME#\" nie może być delegowane, ponieważ delegacja została zabroniona w ustawieniach zadania.";
$MESS["BPCGDOC_ERROR_TASK_IS_NOT_INLINE"] = "Zadanie \"#NAME#\" nie może być uruchomione w ten sposób.";
$MESS["BPCGDOC_INVALID_TYPE"] = "Typ parametru jest niezdefiniowany.";
$MESS["BPCGDOC_INVALID_WF"] = "Nie znaleziono działającego procesu biznesowego dla tego dokumentu.";
$MESS["BPCGDOC_INVALID_WF_ID"] = "Nie można zaleźć szablonu dla procesu biznesowego #ID#.";
$MESS["BPCGDOC_NO"] = "Nie";
$MESS["BPCGDOC_WAIT"] = "czekaj...";
$MESS["BPCGDOC_WI_B24_LIMITS_MESSAGE"] = "Niektóre elementy na portalu użytkownika mają uruchomione dwa workflowy jednocześnie. Wkrótce zostanie wprowadzone ograniczenie do dwóch jednoczesnych workflowów na jednostkę. Zaleca się zmianę logiki swoich workflowów. [URL=https://helpdesk.bitrix24.com/open/4838471/]Więcej informacji[/URL]";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_MESSAGE"] = "Niektóre z procesów, które uruchomiłeś (#CNT#) wywołały błędy lub nie zostały skończone prawidłowo.
[URL=#PATH#]Zobacz te procesy[/URL].";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_TITLE"] = "Asystent portalu";
$MESS["BPCGDOC_YES"] = "Tak";
?>