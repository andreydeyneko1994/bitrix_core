<?
$MESS["BPCGERR_INVALID_ARG"] = "Argument '#PARAM#' ma nieprawidłową wartość.";
$MESS["BPCGERR_INVALID_ARG1"] = "Argument '#PARAM#' ma nieprawidłową wartość '#VALUE#'";
$MESS["BPCGERR_INVALID_TYPE"] = "Typ argumentu '#PARAM#'  jest nieprawidłowy.";
$MESS["BPCGERR_INVALID_TYPE1"] = "Argument '#PARAM#' musi być typu '#VALUE#'.";
$MESS["BPCGERR_NULL_ARG"] = "Argument '#PARAM#' nie jest zdefiniowany.";
?>