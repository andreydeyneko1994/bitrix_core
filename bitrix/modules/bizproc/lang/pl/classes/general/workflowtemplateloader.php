<?
$MESS["BPCGWTL_CANT_DELETE"] = "Nie można usunąć szablonu procesu biznesowego, ponieważ jeden lub więcej aktywnych procesów biznesowych wykorzystuje ten szablon.";
$MESS["BPCGWTL_EMPTY_TEMPLATE"] = "Szablon procesu biznesowego '#ID#' jest pusty.";
$MESS["BPCGWTL_INVALID1"] = "Wartość '#NAME#' nie jest liczbą całkowitą.";
$MESS["BPCGWTL_INVALID2"] = "Wartość '#NAME#' nie jest liczbą rzeczywistą.";
$MESS["BPCGWTL_INVALID3"] = "Wartość '#NAME#' jest nieprawidłowa.";
$MESS["BPCGWTL_INVALID4"] = "Wartości '#NAME#' nie można określić poprzez 'tak' lub 'nie'.";
$MESS["BPCGWTL_INVALID5"] = "Format daty użyty w '#NAME#' nie jest zgodny z formatem '#FORMAT#'.";
$MESS["BPCGWTL_INVALID6"] = "Parametr '#NAME#' typu 'Użytkownik' wymaga modułu Sieci Społecznościowej do instalacji.";
$MESS["BPCGWTL_INVALID7"] = "Parametr typu '#NAME#' jest niezdefiniowany.";
$MESS["BPCGWTL_INVALID8"] = "Pole '#NAME#' jest wymagane.";
$MESS["BPCGWTL_INVALID_WF_ID"] = "Nie znaleziono szablonu procesu biznesowego '#ID#'.";
$MESS["BPCGWTL_UNKNOWN_ERROR"] = "Nieznany błąd.";
$MESS["BPCGWTL_WRONG_TEMPLATE"] = "Szablon procesu biznesowego jest nieprawidłowy";
$MESS["BPWTL_ERROR_MESSAGE_PREFIX"] = "Działanie '#TITLE#':";
?>