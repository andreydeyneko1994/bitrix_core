<?
$MESS["BPATL_DESCR"] = "Opis";
$MESS["BPATL_DESCR_FULL"] = "Pełny Opis";
$MESS["BPATL_FILTER_STATUS_ALL"] = "Wszystko";
$MESS["BPATL_FILTER_STATUS_COMPLETE"] = "Zakończone";
$MESS["BPATL_FILTER_STATUS_RUNNING"] = "W toku";
$MESS["BPATL_F_MODIFIED"] = "Zmodyfikowany";
$MESS["BPATL_F_NAME"] = "Nazwa";
$MESS["BPATL_GROUP_ACTION_DELEGATE"] = "Oddeleguj";
$MESS["BPATL_GROUP_ACTION_OK"] = "Uruchom";
$MESS["BPATL_MODIFIED"] = "Zmodyfikowany";
$MESS["BPATL_NAME"] = "Nazwa";
$MESS["BPATL_NAV"] = "Zadania";
$MESS["BPATL_STARTED_BY"] = "Rozpoczęte przez";
$MESS["BPATL_TITLE"] = "Zadania";
$MESS["BPATL_USER"] = "Użytkownik";
$MESS["BPATL_USER_ID"] = "ID Użytkownika";
$MESS["BPATL_USER_NOT_FOUND"] = "Nie znaleziono użytkownika ##USER_ID#.";
$MESS["BPATL_VIEW"] = "Wyświetl Zadanie";
$MESS["BPATL_WORKFLOW_NAME"] = "Proces Biznesowy";
$MESS["BPATL_WORKFLOW_STATE"] = "Status";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
?>