<?
$MESS["BPAT_ACTION_DELEGATE"] = "Oddeleguj";
$MESS["BPAT_BACK"] = "Wstecz";
$MESS["BPAT_DESCR"] = "Opis Zadania";
$MESS["BPAT_GOTO_DOC"] = "Przejdź Do Dokumentu";
$MESS["BPAT_NAME"] = "Nazwa Zadania";
$MESS["BPAT_NO_TASK"] = "Nie znaleziono zadania.";
$MESS["BPAT_TAB"] = "Zadanie";
$MESS["BPAT_TAB_TITLE"] = "Zadanie";
$MESS["BPAT_TITLE"] = "Zadanie #ID#";
$MESS["BPAT_USER"] = "Użytkownik";
$MESS["BPAT_USER_NOT_FOUND"] = "(Nie znaleziono)";
?>