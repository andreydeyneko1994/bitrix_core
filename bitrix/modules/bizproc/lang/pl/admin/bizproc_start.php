<?
$MESS["BPABS_ADD"] = "Dodawanie";
$MESS["BPABS_BACK"] = "Wstecz";
$MESS["BPABS_DESCRIPTION"] = "Opis szablonu procesu biznesowego";
$MESS["BPABS_DO_CANCEL"] = "Anuluj";
$MESS["BPABS_DO_START"] = "Uruchom";
$MESS["BPABS_EMPTY_DOC_ID"] = "Nie określonego ID dokumentu, dla którego ma zostać utworzony proces biznesowy.";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Wymagany jest typ dokumentu.";
$MESS["BPABS_EMPTY_ENTITY"] = "Nie określono podmiotu, dla którego proces biznesowy ma zostać utworzony.";
$MESS["BPABS_ERROR"] = "Błąd";
$MESS["BPABS_INVALID_TYPE"] = "Typ parametru jest niezdefiniowany.";
$MESS["BPABS_MESSAGE_ERROR"] = "Nie można uruchomić procesu biznesowego typu '#TEMPLATE#'.";
$MESS["BPABS_MESSAGE_SUCCESS"] = "'#TEMPLATE#'-typ procesu biznesowego został pomyślnie uruchomiony.";
$MESS["BPABS_NAME"] = "Nazwa szablonu procesu biznesowego";
$MESS["BPABS_NO"] = "Nie";
$MESS["BPABS_NO_PERMS"] = "Nie masz uprawnień do przeprowadzenia procesu biznesowego dla tego dokumentu.";
$MESS["BPABS_NO_TEMPLATES"] = "Nie znaleziono procesu biznesowego dla tego typu dokumentu.";
$MESS["BPABS_TAB"] = "Proces Biznesowy";
$MESS["BPABS_TAB1"] = "Proces Biznesowy";
$MESS["BPABS_TAB1_TITLE"] = "Wybierz szablon procesu biznesowego do uruchomienia.";
$MESS["BPABS_TAB_TITLE"] = "Parametry uruchamiania procesu biznesowego";
$MESS["BPABS_TITLE"] = "Uruchom Proces Biznesowy";
$MESS["BPABS_WAIT"] = "proszę czekać...";
$MESS["BPABS_YES"] = "Tak";
?>