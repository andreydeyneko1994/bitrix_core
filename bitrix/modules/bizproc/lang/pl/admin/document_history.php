<?
$MESS["BPADH_AUTHOR"] = "Autor";
$MESS["BPADH_DELETE_DOC"] = "Usuń Zapis";
$MESS["BPADH_DELETE_DOC_CONFIRM"] = "Na pewno chcesz usunąć ten zapis?";
$MESS["BPADH_ERROR"] = "Błąd";
$MESS["BPADH_F_AUTHOR"] = "Autor";
$MESS["BPADH_F_AUTHOR_ANY"] = "nie wybrany";
$MESS["BPADH_F_MODIFIED"] = "Zmodyfikowany";
$MESS["BPADH_MODIFIED"] = "Zmodyfikowany";
$MESS["BPADH_NAME"] = "Nazwa";
$MESS["BPADH_NO_DOC_ID"] = "Brakuje ID dokumentu.";
$MESS["BPADH_NO_ENTITY"] = "Brakuje jednostki dokumentu.";
$MESS["BPADH_NO_PERMS"] = "Nie masz uprawnień dostępu do historii dokumentu.";
$MESS["BPADH_RECOVERY_DOC"] = "Odzyskaj Dokument";
$MESS["BPADH_RECOVERY_DOC_CONFIRM"] = "Na pewno chcesz odzyskać ten dokument z tego zapisu?";
$MESS["BPADH_RECOVERY_ERROR"] = "Nie można odzyskać zapisu z historii.";
$MESS["BPADH_RECOVERY_SUCCESS"] = "Zapis został pomyślnie przywrócony z historii.";
$MESS["BPADH_TITLE"] = "Historia Dokumentu";
$MESS["BPADH_USER_PROFILE"] = "Profil Użytkownika";
$MESS["BPADH_VIEW_DOC"] = "Wyświetl dokument";
?>