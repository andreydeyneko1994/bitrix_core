<?
$MESS["BPT_ST_APPROVERS"] = "Osoby do Zatwierdzenia Dokumentu";
$MESS["BPT_ST_APPROVE_TITLE"] = "Zatwierdzenie Dokumentu";
$MESS["BPT_ST_AUTHOR"] = "Autor";
$MESS["BPT_ST_BP_NAME"] = "Proces Biznesowy State-driven";
$MESS["BPT_ST_CMD_APP"] = "Zatwierdzenie";
$MESS["BPT_ST_CMD_APPR"] = "Wyślij Do Zatwierdzenia";
$MESS["BPT_ST_CMD_PUBLISH"] = "Publikuj";
$MESS["BPT_ST_CREATORS"] = "Osoby do Edycji Dokumentu";
$MESS["BPT_ST_DESC"] = "Zatwierdzanie dokumentu SSD przez wiele osób.";
$MESS["BPT_ST_F"] = "Interfejs wydarzenia";
$MESS["BPT_ST_INIT"] = "Inicjuj Stan";
$MESS["BPT_ST_INS"] = "Status inicjalizacji";
$MESS["BPT_ST_MAIL_TITLE"] = "Wiadomość email";
$MESS["BPT_ST_NAME"] = "Zatwierdź Dokument ze Stanami";
$MESS["BPT_ST_PUBDC"] = "Publikowanie Dokumentu";
$MESS["BPT_ST_SAVEH"] = "Zapisywanie Historii Operacji";
$MESS["BPT_ST_SEQ"] = "Sekwencja działań";
$MESS["BPT_ST_SETSTATE"] = "Ustaw Stan";
$MESS["BPT_ST_SET_DRAFT"] = "Ustaw status Wersji Roboczej";
$MESS["BPT_ST_SET_PUB"] = "Ustaw status Publikowania";
$MESS["BPT_ST_ST_DRAFT"] = "Wersja Robocza";
$MESS["BPT_ST_TP"] = "Publikowanie";
?>