<?
$MESS["BP_V1ST_APPR"] = "Zatwierdzone";
$MESS["BP_V1ST_APPR_S"] = "Status: Zatwierdzone";
$MESS["BP_V1ST_DESC"] = "Zalecane kiedy wystarcza zatwierdzenie przez jednego respondenta. Twórz listę osób sugerowanych do wzięcia udziału w głosowaniu. Głosowanie jest zakończone, gdy zostanie oddany pierwszy głos.";
$MESS["BP_V1ST_MAIL2_NA"] = "Głosowanie na {=Document:NAME}: Dokument został odrzucony.";
$MESS["BP_V1ST_MAIL_NAME"] = "Wiadomość email";
$MESS["BP_V1ST_MAIL_SUBJ"] = "Głosowanie na {=Document:NAME}: Dokument został zatwierdzony.";
$MESS["BP_V1ST_NAME"] = "Pierwsze Zatwierdzenie";
$MESS["BP_V1ST_PARAM1"] = "Osoby Głosujące";
$MESS["BP_V1ST_PARAM1_DESC"] = "Użytkownicy biorący udział w procesie zatwierdzania.";
$MESS["BP_V1ST_S2"] = "Sekwencja Działań";
$MESS["BP_V1ST_SEQ"] = "Sekwencyjny proces biznesowy";
$MESS["BP_V1ST_STAT_NA"] = "Odrzucony";
$MESS["BP_V1ST_STAT_NA_T"] = "Status: Odrzucony";
$MESS["BP_V1ST_T3"] = "Publikuj Dokument";
$MESS["BP_V1ST_TASK_T"] = "Proszę zatwierdzić lub odrzucić dokument.";
$MESS["BP_V1ST_TNA"] = "Dokument został odrzucony.";
$MESS["BP_V1ST_VNAME"] = "Odpowiedz w Sprwie Dokumentu";
?>