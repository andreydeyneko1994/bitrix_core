<?
$MESS["BP_EXPR_DESC"] = "Zalecane w sytuacji, gdy osoba zatwierdzająca lub odrzucająca dokument potrzebuje uzyskać do niego komentarz od eksperta. Proces tworzy grupę ekspertów, z których każdy wyraża swoją opinię na temat dokumentu. Następnie opinie są przekazywane osobie, która podejmuje ostateczną decyzję.";
$MESS["BP_EXPR_M"] = "Wiadomość email";
$MESS["BP_EXPR_NA"] = "Odrzucony";
$MESS["BP_EXPR_NAME"] = "Opinia Eksperta";
$MESS["BP_EXPR_NA_ST"] = "Status: Odrzucony";
$MESS["BP_EXPR_PARAM1"] = "Osoba Zatwierdzająca";
$MESS["BP_EXPR_PARAM2"] = "Eksperci";
$MESS["BP_EXPR_PARAM2_DESC"] = "Grupa ekspertów, której członkowie mogą wyrażać swoją opinię na temat dokumentu.";
$MESS["BP_EXPR_PUB"] = "Publikuj Dokument";
$MESS["BP_EXPR_S"] = "Sekwencyjny proces biznesowy";
$MESS["BP_EXPR_ST"] = "Sekwencja Działań";
$MESS["BP_EXPR_ST3_T"] = "Zatwierdzone";
$MESS["BP_EXPR_ST3_TIT"] = "Status: Zatwierdzone";
$MESS["BP_EXPR_TAPP"] = "Zatwierdź Dokument";
?>