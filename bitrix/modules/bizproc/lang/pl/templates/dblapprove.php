<?
$MESS["BP_DBLA_APP"] = "Zatwierdzone";
$MESS["BP_DBLA_APPROVE"] = "Proszę zatwierdzić lub odrzucić dokument.";
$MESS["BP_DBLA_APPROVE2"] = "Proszę zatwierdzić lub odrzucić dokument.";
$MESS["BP_DBLA_APPROVE2_TITLE"] = "Zatwierdzenie Dokumentu: Etap 2";
$MESS["BP_DBLA_APPROVE_TITLR"] = "Zatwierdzenie Dokumentu: Etap 1";
$MESS["BP_DBLA_APP_S"] = "Status: Zatwierdzone";
$MESS["BP_DBLA_DESC"] = "Zalecane kiedy dokument wymaga wstępnej oceny eksperta przed zatwierdzeniem. W trakcie pierwszego etapu procesu dokument jest atestowany przez eksperta. Jeżeli ekspert odrzuci dokument, wówczas jest on odsyłany do twórcy celem dokonania poprawek. Jeżeli doument zostaje zatwierdzony, to jest on przekazywany do ostatecznego zatwierdzenia przez wybraną grupę pracowników w oparciu o zwykłą większość. Jeżeli końcowe głosowanie da wynik negatywny, dokument zostanie zwrócony celem dokonana poprawek, a procedura zatwierdzania zacznie się od początku.";
$MESS["BP_DBLA_M"] = "Wiadomość email";
$MESS["BP_DBLA_MAIL4_SUBJ"] = "Głosowanie na {=Document:NAME}: Dokument został odrzucony.";
$MESS["BP_DBLA_MAIL_SUBJ"] = "Dokument przeszedł Etap 1";
$MESS["BP_DBLA_NAME"] = "Dwuetapowe Zatwierdzanie";
$MESS["BP_DBLA_NAPP_DRAFT"] = "Wysłany do weryfikacji";
$MESS["BP_DBLA_NAPP_DRAFT_S"] = "Status: Wysłane do weryfikacji";
$MESS["BP_DBLA_PARAM1"] = "Etap 1 Osoby Głosujące";
$MESS["BP_DBLA_PARAM2"] = "Etap 2 Osoby Głosujące";
$MESS["BP_DBLA_PUB_TITLE"] = "Publikuj Dokument";
$MESS["BP_DBLA_S"] = "Sekwencja Działań";
$MESS["BP_DBLA_T"] = "Sekwencyjny proces biznesowy";
?>