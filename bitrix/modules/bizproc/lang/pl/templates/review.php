<?
$MESS["BP_REVW_DESC"] = "Ten proces jest idealny do upewnienia się, że wszystkie osoby, które powinny zapoznać się z pewnymi informacjami przeczytają je. Użytkownicy mogą umieszczać komentarze potwierdzające, że zapoznali się z treścią.";
$MESS["BP_REVW_MAIL"] = "Wiadomość email";
$MESS["BP_REVW_PARAM1"] = "Lista Czytających";
$MESS["BP_REVW_REVIEW"] = "Czytaj dokument {=Document:NAME}";
$MESS["BP_REVW_T"] = "Sekwencyjny proces biznesowy";
$MESS["BP_REVW_TITLE"] = "Czytaj dokument";
?>