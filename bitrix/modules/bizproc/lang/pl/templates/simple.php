<?
$MESS["BPT_SM_ACT_NAME"] = "Sekwencja Działań";
$MESS["BPT_SM_ACT_TITLE"] = "Wiadomość email";
$MESS["BPT_SM_APPROVE_NAME"] = "Proszę zatwierdzić lub odrzucić dokument.";
$MESS["BPT_SM_APPROVE_TITLE"] = "Odpowiedz w Sprwie Dokumentu";
$MESS["BPT_SM_DESC"] = "Zalecany, gdy decyzja ma być podjęta przez osiągnięcie zwykłej większości w głosowaniu. Można wyznaczyć osoby biorące udział w głosowaniu i zezwolić im na komentowanie. Po zakończeniu głosowania wszystkie osoby biorące w nim udział zostaną poinformowane o wyniku.";
$MESS["BPT_SM_MAIL1_TITLE"] = "Dokument został zatwierdzony";
$MESS["BPT_SM_MAIL2_STATUS"] = "Odrzucony";
$MESS["BPT_SM_MAIL2_STATUS2"] = "Status: Odrzucony";
$MESS["BPT_SM_MAIL2_TITLE"] = "Dokument został odrzucony.";
$MESS["BPT_SM_NAME"] = "Proste Zatwierdzanie/Głosuj";
$MESS["BPT_SM_PARAM_DESC"] = "Użytkownicy biorący udział w głosowaniu.";
$MESS["BPT_SM_PARAM_NAME"] = "Osoby Głosujące";
$MESS["BPT_SM_PUB"] = "Publikuj Dokument";
$MESS["BPT_SM_STATUS"] = "Zatwierdzone";
$MESS["BPT_SM_STATUS2"] = "Status: Zatwierdzone";
$MESS["BPT_SM_TITLE1"] = "Sekwencyjny proces biznesowy";
?>