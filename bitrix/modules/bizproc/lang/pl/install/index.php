<?
$MESS["BIZPROC_BIZPROCDESIGNER_INSTALLED"] = "Moduł projektowania procesów biznesowych jest zainstalowany. Proszę odinstalować go przed podjęciem dalszych kroków.";
$MESS["BIZPROC_INSTALL_DESCRIPTION"] = "Moduł do tworzenia, zarządzania i przeprowadzzania procesów biznesowych.";
$MESS["BIZPROC_INSTALL_NAME"] = "Proces Biznesowy";
$MESS["BIZPROC_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["BIZPROC_PERM_D"] = "Odmowa dostępu";
$MESS["BIZPROC_PERM_R"] = "Odczyt";
$MESS["BIZPROC_PERM_W"] = "Napisz";
$MESS["BIZPROC_PHP_L439"] = "Używasz wersji #VERS# PHP, a moduł wymaga wersji 5.0.0 lub nowszej. Proszę zaktualizować swoją wersję instalacyjną PHP lub skontaktować się z pomocą techniczną.";
?>