<?php
$MESS["BIZPROC_JS_BP_FIELD_TYPE_ADD"] = "dodaj";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_CHOOSE_FILE"] = "Wybierz plik";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NO"] = "Nie";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NOT_SELECTED"] = "[nieustawiony]";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS1"] = "Wprowadź każdą zmienną w nowej linii. Jeżeli wartość zmiennej i nazwa zmiennej są różne, do nazwy z wartością dodaj prefiks w nawiasie kwadratowym, np.: [v1]Zmienna 1";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS2"] = "Po zakończeniu, kliknij \"Ustaw\"";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS3"] = "Ustaw";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_YES"] = "Tak";
