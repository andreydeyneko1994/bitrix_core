<?php
$MESS["BIZPROC_JS_BP_FIELD_TYPE_ADD"] = "ajouter";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_CHOOSE_FILE"] = "Sélectionner un fichier";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NO"] = "Non";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_NOT_SELECTED"] = "[non défini]";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS1"] = "Précisez chaque variante sur une nouvelle ligne. Si la valeur et le nom de la variante sont différents, ajoutez la valeur entre crochets avant le nom. Par exemple : [v1]Variante 1";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS2"] = "Une fois terminé, cliquez sur \"Définir\"";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_SELECT_OPTIONS3"] = "Définir";
$MESS["BIZPROC_JS_BP_FIELD_TYPE_YES"] = "Oui";
