<?php
$MESS["BIZPROC_ERROR_CONFIGURATION_CLEAR_EXCEPTION"] = "Une ou plusieurs erreurs sont survenues lors du nettoyage : entité ignorée (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_EXPORT_EXCEPTION"] = "Une ou plusieurs erreurs sont survenues lors de l'exportation des données : entité ignorée (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION"] = "Une ou plusieurs erreurs sont survenues lors de l'importation des données : entité ignorée (#CODE#)";
$MESS["BIZPROC_ERROR_CONFIGURATION_IMPORT_EXCEPTION_BP"] = "Une ou plusieurs erreurs sont survenues lors de l'importation des données : flux de travail ignoré";
