<?php
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT"] = "Impossible de supprimer la constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_CONSTANT_RIGHT"] = "Droits insuffisants pour supprimer une constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE"] = "Impossible de supprimer la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_DELETE_VARIABLE_RIGHT"] = "Droits insuffisants pour supprimer une variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT"] = "Impossible d'ajouter une constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_CONSTANT_RIGHT"] = "Droits insuffisants pour ajouter ou modifier une constante.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE"] = "Impossible d'ajouter la variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_CANT_UPSERT_VARIABLE_RIGHT"] = "Droits insuffisants pour ajouter ou modifier une variable.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_MODE_NOT_DEFINED"] = "Mode incorrect.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_CONSTANT"] = "La constante demandée n'existe pas.";
$MESS["BIZPROC_CONTROLLER_GLOBALFIELD_NOT_EXISTS_VARIABLE"] = "La variable demandée n'existe pas.";
