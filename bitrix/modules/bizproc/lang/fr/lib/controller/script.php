<?php
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_QUEUE"] = "Droits insuffisants pour supprimer un script intelligent de la programmation";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_RUNNING_SCRIPT"] = "Impossible de supprimer le script car des flux de travail sont actuellement exécutés par ce script";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_DELETE_SCRIPT"] = "Droits insuffisants pour supprimer un script intelligent";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START"] = "Droits insuffisants pour exécuter un script intelligent";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_START_INACTIVE"] = "Impossible d'exécuter le script intelligent car il est désactivé";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE"] = "Droits insuffisants pour arrêter le script intelligent";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_TERMINATE_FINISHED"] = "Impossible d'arrêter le script intelligent car il est déjà terminé";
$MESS["BIZPROC_CONTROLLER_SCRIPT_CANT_UPDATE_SCRIPT"] = "Droits insuffisants pour modifier un script intelligent";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_DOCUMENT_ID_LIMIT"] = "Maximum d'éléments autorisés pour l'exécution d'un script intelligent dépassé (sélectionnés : #SELECTED#, max : #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_ERROR_QUEUES_LIMIT"] = "Maximum d'instances de script intelligent dépassé (exécutées : #CNT#, max : #LIMIT#)";
$MESS["BIZPROC_CONTROLLER_SCRIPT_NOT_EXISTS"] = "Le script intelligent que vous avez demandé est introuvable";
