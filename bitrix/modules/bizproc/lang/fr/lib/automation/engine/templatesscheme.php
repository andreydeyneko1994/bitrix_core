<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_MISSING_TEMPLATE_ERROR"] = "Le modèle sélectionné n'existe pas";
$MESS["BIZPROC_AUTOMATION_SCHEME_TEMPLATE_ERROR"] = "Impossible de sélectionner un des modèles";
