<?
$MESS["BPDT_DATE_CURRENT_TZ"] = "Mon heure";
$MESS["BPDT_DATE_INVALID"] = "La valeur du champ n'est pas une date valide.";
$MESS["BPDT_DATE_MOBILE_SELECT"] = "Sélectionner";
$MESS["BPDT_DATE_SERVER_TZ"] = "Heure du serveur";
?>