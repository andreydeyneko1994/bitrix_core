<?
$MESS["BPDT_FILE_CHOOSE_FILE"] = "Sélectionner un fichier";
$MESS["BPDT_FILE_INVALID"] = "Erreur lors de l'enregistrement du fichier.";
$MESS["BPDT_FILE_SECURITY_ERROR"] = "Les paramètres de sécurité actuels refuse l'accès au fichier.";
?>