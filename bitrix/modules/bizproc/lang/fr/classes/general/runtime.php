<?
$MESS["BPCGDOC_LIMIT_SIMULTANEOUS_PROCESSES"] = "Pour chaque document, peut être lancée simultanément pas plus de #NUM# processus.";
$MESS["BPCGDOC_WORKFLOW_RECURSION_LOCK"] = "Le modèle ne peut être exécuté de manière récursive";
$MESS["BPRA_IS_TIMEOUT"] = "Délai d'attente de l'opération dépassé";
?>