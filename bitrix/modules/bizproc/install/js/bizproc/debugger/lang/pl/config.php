<?php
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_1"] = "Filtr [b]Tylko deale testowe[/b] jest aktywny. Widok ten pokazuje deale używane przez system do testowania reguł automatyzacji.";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_2"] = "Aby wyświetlić swoje deale, zmień filtr na [b]Deale w toku[/b] lub [b]Moje deale[/b].";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TITLE"] = "Nie martw się, Twoje deale są bezpieczne";
