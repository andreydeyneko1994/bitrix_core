<?php
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_1"] = "El filtro [b]Solo negociaciones de prueba[/b] está actualmente activo. Esta vista muestra las negociaciones que utiliza el sistema para probar sus reglas de automatización.";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_2"] = "Para ver sus negociaciones, cambie el filtro a [b]Negociaciones en curso[/b] o [b]Mis negociaciones[/b].";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TITLE"] = "No se preocupe, sus negociaciones están a salvo.";
