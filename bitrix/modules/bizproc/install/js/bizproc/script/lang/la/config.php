<?php
$MESS["BIZPROC_SCRIPT_MANAGER_START_BUTTON_SEND_PARAMS"] = "Enviar";
$MESS["BIZPROC_SCRIPT_MANAGER_START_BUTTON_START"] = "Iniciar";
$MESS["BIZPROC_SCRIPT_MANAGER_START_FINISHED"] = "Script finalizado";
$MESS["BIZPROC_SCRIPT_MANAGER_START_NOTHING_SELECTED"] = "Nada seleccionado";
$MESS["BIZPROC_SCRIPT_MANAGER_START_PARAMS_POPUP_TITLE"] = "Parámetros del script";
$MESS["BIZPROC_SCRIPT_MANAGER_START_QUEUED"] = "El script se inició correctamente";
$MESS["BIZPROC_SCRIPT_MANAGER_START_TEXT_START"] = "El script se iniciará para todos los elementos seleccionados. Seleccionado: #CNT#.";
