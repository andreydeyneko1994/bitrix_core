<?php
$MESS["BPSA_ERROR_MOVE"] = "Una acción no puede ser hecha anidada a una acción anidada.";
$MESS["BPSA_MARKETPLACE_ADD_DESCR"] = "Agregar acciones desde las aplicaciones Bitrix24";
$MESS["BPSA_MARKETPLACE_ADD_DESCR_2"] = "Agregar acciones de Bitrix24.Market";
$MESS["BPSA_MARKETPLACE_ADD_TITLE"] = "Agregar más...";
$MESS["BPSA_MARKETPLACE_ADD_TITLE_2"] = "Instalar desde Bitrix24.Market";
$MESS["BPSA_MY_ACTIVITIES"] = "Mis acciones";
