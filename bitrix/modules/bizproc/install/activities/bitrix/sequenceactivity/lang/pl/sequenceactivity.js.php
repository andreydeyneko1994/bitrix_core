<?php
$MESS["BPSA_ERROR_MOVE"] = "Działanie nie może zostać przeprowadzone jako samo sobie podrzędne.";
$MESS["BPSA_MARKETPLACE_ADD_DESCR"] = "Dodaj działania z aplikacji Bitrix24";
$MESS["BPSA_MARKETPLACE_ADD_DESCR_2"] = "Dodaj działania z Bitrix24.Market";
$MESS["BPSA_MARKETPLACE_ADD_TITLE"] = "Dodaj więcej...";
$MESS["BPSA_MARKETPLACE_ADD_TITLE_2"] = "Zainstaluj z Bitrix24.Market";
$MESS["BPSA_MY_ACTIVITIES"] = "Moje Działania";
