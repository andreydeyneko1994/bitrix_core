<?php
$MESS["BPSA_ERROR_MOVE"] = "Une action ne peut être faite d'un enfant de son enfant.";
$MESS["BPSA_MARKETPLACE_ADD_DESCR"] = "Ajouter des actions issues des applications Bitrix24";
$MESS["BPSA_MARKETPLACE_ADD_DESCR_2"] = "Ajouter des actions à partir de Bitrix24.Market";
$MESS["BPSA_MARKETPLACE_ADD_TITLE"] = "Ajouter plus...";
$MESS["BPSA_MARKETPLACE_ADD_TITLE_2"] = "Installer à partir de Bitrix24.Market";
$MESS["BPSA_MY_ACTIVITIES"] = "Mes actions";
