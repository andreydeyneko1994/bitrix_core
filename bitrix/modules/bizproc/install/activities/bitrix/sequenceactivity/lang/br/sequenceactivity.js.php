<?php
$MESS["BPSA_ERROR_MOVE"] = "Uma ação não pode ser derivada de sua secundária.";
$MESS["BPSA_MARKETPLACE_ADD_DESCR"] = "Adicionar ações dos Aplicativos Bitrix24";
$MESS["BPSA_MARKETPLACE_ADD_DESCR_2"] = "Adicionar ações do Bitrix24.Market";
$MESS["BPSA_MARKETPLACE_ADD_TITLE"] = "Adicionar mais...";
$MESS["BPSA_MARKETPLACE_ADD_TITLE_2"] = "Instalar do Bitrix24 Marketplace";
$MESS["BPSA_MY_ACTIVITIES"] = "Minhas Ações";
