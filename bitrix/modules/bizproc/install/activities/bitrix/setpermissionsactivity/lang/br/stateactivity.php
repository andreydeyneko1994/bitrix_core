<?php
$MESS["BPSA_EMPTY_PERMS"] = "A permissão para elemento neste status não está especificada.";
$MESS["BPSA_INVALID_CHILD"] = "O 'Status de Atividade' pode conter somente 'StatusAtividadeInicializada', 'StatusAtividadeFinalizada' ou 'AtividadeConduzidaPorEvento'.";
$MESS["BPSA_TRACK1"] = "Permissão para elemento neste status #VAL#";
