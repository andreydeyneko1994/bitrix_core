<?php
$MESS["BPSA_PD_PERM"] = "A permissão \"#OP#\" é concedida a";
$MESS["BPSA_PD_PERM_CLEAR"] = "Desmarcar";
$MESS["BPSA_PD_PERM_CURRENT_LABEL"] = "Permissões atuais do elemento";
$MESS["BPSA_PD_PERM_HOLD"] = "Manter";
$MESS["BPSA_PD_PERM_REWRITE"] = "Sobrescrever";
$MESS["BPSA_PD_PERM_SCOPE_DOCUMENT"] = "Todas as permissões do elemento";
$MESS["BPSA_PD_PERM_SCOPE_LABEL"] = "Possibilidade de apagar e substituir";
$MESS["BPSA_PD_PERM_SCOPE_WORFLOW"] = "Conjunto de permissões pelo processo de negócio atual";
