<?php
$MESS["BPSA_PD_PERM"] = "Personnes ayant droit pour l'opération '#OP#'";
$MESS["BPSA_PD_PERM_CLEAR"] = "Annuler";
$MESS["BPSA_PD_PERM_CURRENT_LABEL"] = "Droits actuels de l'élément";
$MESS["BPSA_PD_PERM_HOLD"] = "Maintenir";
$MESS["BPSA_PD_PERM_REWRITE"] = "Réenregistrer";
$MESS["BPSA_PD_PERM_SCOPE_DOCUMENT"] = "Toutes les autorisations d'éléments";
$MESS["BPSA_PD_PERM_SCOPE_LABEL"] = "Portée pour la compensation et écraser";
$MESS["BPSA_PD_PERM_SCOPE_WORFLOW"] = "Permission définie par le processus d'affaires actuel";
