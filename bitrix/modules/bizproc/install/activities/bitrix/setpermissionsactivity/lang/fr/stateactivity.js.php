<?
$MESS["STATEACT_ADD"] = "Ajouter";
$MESS["STATEACT_BACK"] = "<< Retour à la configuration des statuts";
$MESS["STATEACT_DEL"] = "Supprimer le gestionnaire";
$MESS["STATEACT_EDITBP"] = "Éditer ce processus d'affaires";
$MESS["STATEACT_MENU_COMMAND"] = "Commande";
$MESS["STATEACT_MENU_DELAY"] = "Exécution dans un délai imparti";
$MESS["STATEACT_MENU_FIN"] = "Gestionnaire de la sortie du statut donné";
$MESS["STATEACT_MENU_INIT"] = "Gestionnaire d'entrée dans ce statut";
$MESS["STATEACT_SETT"] = "Réglages du gestionnaire";
?>