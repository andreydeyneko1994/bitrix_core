<?php
$MESS["BPSA_PD_PERM"] = "\"#OP#\" uprawnienie jest nadane do";
$MESS["BPSA_PD_PERM_CLEAR"] = "Wyczyść";
$MESS["BPSA_PD_PERM_CURRENT_LABEL"] = "Aktualne uprawnienia elementu";
$MESS["BPSA_PD_PERM_HOLD"] = "Utrzymanie";
$MESS["BPSA_PD_PERM_REWRITE"] = "Nadpisz istniejące uprawnienia";
$MESS["BPSA_PD_PERM_SCOPE_DOCUMENT"] = "Wszystkie uprawnienia elementu";
$MESS["BPSA_PD_PERM_SCOPE_LABEL"] = "Zakres czyszczenia i nadpisywania";
$MESS["BPSA_PD_PERM_SCOPE_WORFLOW"] = "Uprawnienia ustawione przez bieżący proces biznesowy";
