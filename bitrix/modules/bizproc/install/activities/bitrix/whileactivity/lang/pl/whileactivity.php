<?
$MESS["BPWA_CONDITION_NOT_SET"] = "Nie określono warunków zapętlenia";
$MESS["BPWA_CYCLE_LIMIT"] = "Wykonanie w pętli przekroczyło maksymalny limit.";
$MESS["BPWA_INVALID_CONDITION_TYPE"] = "Nie znaleziono typu warunku.";
$MESS["BPWA_NO_CONDITION"] = "Brakuje warunku.";
?>