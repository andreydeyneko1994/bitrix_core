<?
$MESS["BPWA_CONDITION_NOT_SET"] = "Condição de loop não especificada";
$MESS["BPWA_CYCLE_LIMIT"] = "Máximo de iteração do loop excedido";
$MESS["BPWA_INVALID_CONDITION_TYPE"] = "O tipo de condição não foi encontrado.";
$MESS["BPWA_NO_CONDITION"] = "A condição está faltando.";
?>