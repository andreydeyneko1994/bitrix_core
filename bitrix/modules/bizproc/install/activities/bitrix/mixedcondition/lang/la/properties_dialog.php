<?php
$MESS["BPMC_PD_ACTIVITY_RESULTS"] = "Resultados adicionales";
$MESS["BPMC_PD_ADD"] = "Agregar condición";
$MESS["BPMC_PD_AND"] = "Y";
$MESS["BPMC_PD_CONDITION"] = "Condición";
$MESS["BPMC_PD_CONSTANTS"] = "Constantes";
$MESS["BPMC_PD_DELETE"] = "Eliminar";
$MESS["BPMC_PD_DOCUMENT_FIELDS"] = "Campos del elemento";
$MESS["BPMC_PD_FIELD"] = "Origen";
$MESS["BPMC_PD_FIELD_CHOOSE"] = "seleccionar";
$MESS["BPMC_PD_OR"] = "O";
$MESS["BPMC_PD_PARAMS"] = "Parámetros";
$MESS["BPMC_PD_VALUE"] = "Valor";
$MESS["BPMC_PD_VARS"] = "Variables";
