<?
$MESS["BPDA_EMPTY_PROP"] = "Wartość 'Okres' nie jest określona.";
$MESS["BPDA_TRACK"] = "Wstrzymane na #PERIOD#";
$MESS["BPDA_TRACK1"] = "Odroczony do #PERIOD#";
$MESS["BPDA_TRACK2"] = "Okres odroczenia nie jest określony.";
$MESS["BPDA_TRACK3"] = "Wstrzymanie pominięto. Skonfigurowana data wstrzymania była wcześniejsza niż data bieżąca.";
$MESS["BPDA_TRACK4"] = "Przełożono na #PERIOD1#, do #PERIOD2#";
?>