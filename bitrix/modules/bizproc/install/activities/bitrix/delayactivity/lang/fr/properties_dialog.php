<?php
$MESS["CPAD_DP_TIME"] = "Autonomie en veille";
$MESS["CPAD_DP_TIME1"] = "Date";
$MESS["CPAD_DP_TIME_D"] = "jours";
$MESS["CPAD_DP_TIME_H"] = "heures";
$MESS["CPAD_DP_TIME_LOCAL"] = "Heure locale";
$MESS["CPAD_DP_TIME_M"] = "minutes";
$MESS["CPAD_DP_TIME_S"] = "secondes";
$MESS["CPAD_DP_TIME_SELECT"] = "Mode";
$MESS["CPAD_DP_TIME_SELECT_DELAY"] = "Délais";
$MESS["CPAD_DP_TIME_SELECT_TIME"] = "Temps";
$MESS["CPAD_DP_TIME_SERVER"] = "Heure du serveur";
$MESS["CPAD_DP_WRITE_TO_LOG"] = "Enregistrer les informations de pause dans le journal du flux de travail";
$MESS["CPAD_PD_TIMEOUT_LIMIT"] = "Temps d'attente minimum";
