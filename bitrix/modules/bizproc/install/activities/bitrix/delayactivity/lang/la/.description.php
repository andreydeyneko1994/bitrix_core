<?
$MESS["BPDA_DESCR_DESCR"] = "La acción \"Detener ejecución\" suspende el proceso por el tiempo especificado, retrasando la acción siguiente.";
$MESS["BPDA_DESCR_NAME"] = "Detener ejecución";
?>