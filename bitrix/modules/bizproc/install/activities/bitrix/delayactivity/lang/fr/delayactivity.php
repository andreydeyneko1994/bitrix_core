<?
$MESS["BPDA_EMPTY_PROP"] = "Propriété 'Période' n'est pas indiquée.";
$MESS["BPDA_TRACK"] = "Payez du compte";
$MESS["BPDA_TRACK1"] = "Reporté à #PERIOD#";
$MESS["BPDA_TRACK2"] = "Période de report non renseignée.";
$MESS["BPDA_TRACK3"] = "Pause ignorée. La date de pause configurée est antérieure à la date actuelle.";
$MESS["BPDA_TRACK4"] = "Reporté de #PERIOD1#, jusqu'au #PERIOD2#";
?>