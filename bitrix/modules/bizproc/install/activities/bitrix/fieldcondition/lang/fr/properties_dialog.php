<?php
$MESS["BPFC_PD_ADD"] = "Ajouter une condition supplémentaire";
$MESS["BPFC_PD_AND"] = "ET";
$MESS["BPFC_PD_CALENDAR"] = "Calendrier";
$MESS["BPFC_PD_CONDITION"] = "Condition";
$MESS["BPFC_PD_CONTAIN"] = "contient";
$MESS["BPFC_PD_DELETE"] = "Supprimer";
$MESS["BPFC_PD_EMPTY"] = "le champ est vide";
$MESS["BPFC_PD_EQ"] = "égal";
$MESS["BPFC_PD_FIELD"] = "Champ de l'élément";
$MESS["BPFC_PD_GE"] = "au moins";
$MESS["BPFC_PD_GT"] = "plus";
$MESS["BPFC_PD_IN"] = "se trouve dans";
$MESS["BPFC_PD_LE"] = "pas plus";
$MESS["BPFC_PD_LT"] = "moins";
$MESS["BPFC_PD_MODIFIED"] = "modifié";
$MESS["BPFC_PD_NE"] = "pas égal";
$MESS["BPFC_PD_NO"] = "Non";
$MESS["BPFC_PD_NOT_EMPTY"] = "le champ n'est pas vide";
$MESS["BPFC_PD_OR"] = "OU";
$MESS["BPFC_PD_VALUE"] = "Valeur";
$MESS["BPFC_PD_YES"] = "Oui";
