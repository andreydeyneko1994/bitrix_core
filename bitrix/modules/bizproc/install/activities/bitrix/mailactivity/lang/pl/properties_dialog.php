<?php
$MESS["BPMA_PD_BODY"] = "Wiadomość Tekstowa";
$MESS["BPMA_PD_CP"] = "Kodowanie";
$MESS["BPMA_PD_DIRRECT_MAIL"] = "Tryb wysyłania wiadomości";
$MESS["BPMA_PD_DIRRECT_MAIL_N"] = "Wyślij używając podsystemu poczty (sprzężone z wydarzeniami e-mail i szablonami)";
$MESS["BPMA_PD_DIRRECT_MAIL_Y"] = "Bezpośrednie wysyłanie";
$MESS["BPMA_PD_FILE"] = "Załączniki";
$MESS["BPMA_PD_FILE_SELECT"] = "wybierz";
$MESS["BPMA_PD_FROM"] = "Nadawca";
$MESS["BPMA_PD_FROM_LIMITATION"] = "Adresy e-mail, które nie zostały potwierdzone lub połączone z Bitrix24, są niedozwolone.";
$MESS["BPMA_PD_MAIL_SEPARATOR"] = "Separator adresu email";
$MESS["BPMA_PD_MAIL_SITE"] = "Strona szablonu wiadomości";
$MESS["BPMA_PD_MAIL_SITE_OTHER"] = "inne";
$MESS["BPMA_PD_MESS_TYPE"] = "Typ Wiadomości";
$MESS["BPMA_PD_SUBJECT"] = "Temat";
$MESS["BPMA_PD_TEXT"] = "Tekst";
$MESS["BPMA_PD_TO"] = "Odbiorca";
