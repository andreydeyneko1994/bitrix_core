<?
$MESS["BPMA_ATTACHMENT"] = "Załączniki";
$MESS["BPMA_ATTACHMENT_DISK"] = "Pliki na Dysku";
$MESS["BPMA_ATTACHMENT_FILE"] = "Pliki";
$MESS["BPMA_ATTACHMENT_TYPE"] = "Typ załącznika";
$MESS["BPMA_EMPTY_PROP1"] = "Brakuje parametru 'Nadawca'.";
$MESS["BPMA_EMPTY_PROP2"] = "Brakuje parametru 'Odbiorca'.";
$MESS["BPMA_EMPTY_PROP3"] = "Brakuje parametru 'Temat'.";
$MESS["BPMA_EMPTY_PROP4"] = "Brakuje parametru 'Kodowanie'.";
$MESS["BPMA_EMPTY_PROP5"] = "Brakuje parametru 'Typ Wiadomości'.";
$MESS["BPMA_EMPTY_PROP6"] = "Parametr 'Typ Wiadomości' jest nieprawidłowy.";
$MESS["BPMA_EMPTY_PROP7"] = "Brakuje parametru 'Wiadomość Tekstowa'.";
$MESS["BPMA_MAIL_SUBJECT"] = "Temat";
$MESS["BPMA_MAIL_USER_FROM"] = "Od";
$MESS["BPMA_MAIL_USER_TO"] = "Do";
?>