<?php
$MESS["SEQWF_BEG"] = "Début";
$MESS["SEQWF_CONF"] = "L'action choisie sera enlevée de la liste de vos actions. Continuer ?";
$MESS["SEQWF_END"] = "Fin";
$MESS["SEQWF_MARKETPLACE_ADD"] = "Ajouter plus...";
$MESS["SEQWF_SNIP"] = "Mes actions";
$MESS["SEQWF_SNIP_DD"] = "Faites glisser ici cette action pour l'enregistrer";
$MESS["SEQWF_SNIP_DEL"] = "Supprimer cette action";
$MESS["SEQWF_SNIP_NAME"] = "Dénomination : ";
$MESS["SEQWF_SNIP_TITLE"] = "Propriétés de l'action sélectionnée";
