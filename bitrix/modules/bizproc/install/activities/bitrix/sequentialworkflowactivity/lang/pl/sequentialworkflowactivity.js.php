<?
$MESS["SEQWF_BEG"] = "Uruchom";
$MESS["SEQWF_CONF"] = "Wybrane działanie zostanie usunięte z twojej listy działań. Kontynuować?";
$MESS["SEQWF_END"] = "Koniec";
$MESS["SEQWF_MARKETPLACE_ADD"] = "Dodaj więcej...";
$MESS["SEQWF_SNIP"] = "Moje Działania";
$MESS["SEQWF_SNIP_DD"] = "Upuść tutaj działania, aby zachować";
$MESS["SEQWF_SNIP_DEL"] = "Usuń to działanie";
$MESS["SEQWF_SNIP_NAME"] = "Nazwa:";
$MESS["SEQWF_SNIP_TITLE"] = "Atrybuty działania";
?>