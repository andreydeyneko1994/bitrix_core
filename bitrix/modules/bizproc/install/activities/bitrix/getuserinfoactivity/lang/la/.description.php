<?
$MESS["BPGUIA_DESCR_DESCR"] = "Información del empleado";
$MESS["BPGUIA_DESCR_NAME"] = "Información del empleado";
$MESS["BPGUIA_IS_ABSENT"] = "Fuera de la oficina (según el horario de ausencia)";
$MESS["BPGUIA_TIMEMAN_STATUS"] = "Estado de la jornada laboral";
$MESS["BPGUIA_USER_ACTIVE"] = "Activo";
$MESS["BPGUIA_USER_EMAIL"] = "Correo electrónico";
$MESS["BPGUIA_USER_LAST_NAME"] = "Apellido";
$MESS["BPGUIA_USER_LOGIN"] = "Iniciar sesión";
$MESS["BPGUIA_USER_NAME"] = "Nombre";
$MESS["BPGUIA_USER_PERSONAL_BIRTHDAY"] = "Fecha de nacimiento";
$MESS["BPGUIA_USER_PERSONAL_CITY"] = "Ciudad";
$MESS["BPGUIA_USER_PERSONAL_MOBILE"] = "Teléfono móvil";
$MESS["BPGUIA_USER_PERSONAL_WWW"] = "Sitio web";
$MESS["BPGUIA_USER_SECOND_NAME"] = "Segundo nombre";
$MESS["BPGUIA_USER_UF_DEPARTMENT"] = "Departamento (lista de identificación)";
$MESS["BPGUIA_USER_UF_FACEBOOK"] = "Facebook";
$MESS["BPGUIA_USER_UF_LINKEDIN"] = "LinkedIn";
$MESS["BPGUIA_USER_UF_PHONE_INNER"] = "Extensión telefónica";
$MESS["BPGUIA_USER_UF_SKYPE"] = "Skype";
$MESS["BPGUIA_USER_UF_TWITTER"] = "Twitter";
$MESS["BPGUIA_USER_UF_WEB_SITES"] = "Otros sitios web";
$MESS["BPGUIA_USER_UF_XING"] = "Xing";
$MESS["BPGUIA_USER_WORK_PHONE"] = "Teléfono del trabajo";
$MESS["BPGUIA_USER_WORK_POSITION"] = "Nombre del puesto";
?>