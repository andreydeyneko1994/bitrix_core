<?php
$MESS["BPTA1_KILL_WF_NAME"] = "Supprimer les données du flux de travail";
$MESS["BPTA1_STATE_TITLE"] = "Exécution interrompue.";
$MESS["BPTA1_STATE_TITLE_NAME"] = "Texte du statut";
$MESS["BPTA1_TERMINATE"] = "Abandonner le flux de travail";
$MESS["BPTA1_TERMINATE_ALL"] = "Tous les flux de travail de modèles";
$MESS["BPTA1_TERMINATE_ALL_EXCEPT_CURRENT"] = "Tous les flux de travail de modèles, sauf l'actuel";
$MESS["BPTA1_TERMINATE_CURRENT"] = "Actuel";
