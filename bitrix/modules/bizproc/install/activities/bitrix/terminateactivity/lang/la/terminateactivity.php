<?php
$MESS["BPTA1_KILL_WF_NAME"] = "Eliminar los datos del flujo de trabajo";
$MESS["BPTA1_STATE_TITLE"] = "Ejecución interrumpida.";
$MESS["BPTA1_STATE_TITLE_NAME"] = "Texto del estado";
$MESS["BPTA1_TERMINATE"] = "Abortar el flujo de trabajo";
$MESS["BPTA1_TERMINATE_ALL"] = "Todos los flujos de trabajo de la plantilla";
$MESS["BPTA1_TERMINATE_ALL_EXCEPT_CURRENT"] = "Todos los flujos de trabajo de la plantilla excepto el actual";
$MESS["BPTA1_TERMINATE_CURRENT"] = "Actual";
