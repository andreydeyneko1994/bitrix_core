<?
$MESS["STATEACT_ADD"] = "Dodawanie";
$MESS["STATEACT_BACK"] = "<< Powrót Do Stanów";
$MESS["STATEACT_DEL"] = "Usuń Procedurę Obsługi";
$MESS["STATEACT_EDITBP"] = "Edytuj ten proces biznesowy";
$MESS["STATEACT_MENU_COMMAND"] = "Komenda";
$MESS["STATEACT_MENU_DELAY"] = "Opóźnij Wykonanie";
$MESS["STATEACT_MENU_FIN"] = "Procedura obsługi Wyjścia z Stanu";
$MESS["STATEACT_MENU_INIT"] = "Procedura obsługi Wejścia do Stanu";
$MESS["STATEACT_SETT"] = "Ustawienia Procedury Obsługi";
?>