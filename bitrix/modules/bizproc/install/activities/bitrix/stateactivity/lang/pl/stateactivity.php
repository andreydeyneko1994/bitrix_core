<?php
$MESS["BPSA_EMPTY_PERMS"] = "Uprawnienia do elementu w tym stanie nie są określone.";
$MESS["BPSA_INVALID_CHILD"] = "Działanie 'StateActivity' może zawierać tylko działania 'StateInitializationActivity', 'StateFinalizationActivity' lub  'EventDrivenActivity'.";
$MESS["BPSA_INVALID_CHILD_1"] = "Aktywność „StateActivity” może zawierać tylko aktywności „StateInitializationActivity”, „StateFinalizationActivity” lub „EventDrivenActivity”.";
$MESS["BPSA_TRACK1"] = "Uprawnienia elementu w tym stanie #VAL#";
