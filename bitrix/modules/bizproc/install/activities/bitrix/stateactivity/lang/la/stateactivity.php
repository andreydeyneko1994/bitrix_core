<?php
$MESS["BPSA_EMPTY_PERMS"] = "No se especificó el permiso para el elemento en este estado.";
$MESS["BPSA_INVALID_CHILD"] = "Una actividad 'StateActivity' puede contener 'StateInitializationActivity', 'StateFinalizationActivity' o solo acciones 'EventDrivenActivity'";
$MESS["BPSA_INVALID_CHILD_1"] = "Una actividad 'StateActivity' puede contener actividades 'StateInitializationActivity', 'StateFinalizationActivity' o solo actividades 'EventDrivenActivity'.";
$MESS["BPSA_TRACK1"] = "Permiso para el elemento en este estado #VAL#";
