<?php
$MESS["BPSA_EMPTY_PERMS"] = "Les droits pour l'élément dans ce statut ne sont pas spécifiés.";
$MESS["BPSA_INVALID_CHILD"] = "L'action de type 'StateActivity' ne peut contenir que les actions de type 'StateInitializationActivity', 'StateFinalizationActivity' ou 'EventDrivenActivity'.";
$MESS["BPSA_INVALID_CHILD_1"] = "Une action de type 'StateActivity' ne peut contenir que les activités de type 'StateInitializationActivity', 'StateFinalizationActivity' ou 'EventDrivenActivity'.";
$MESS["BPSA_TRACK1"] = "Droits pour l'élément dans ce statut #VAL#";
