<?php
$MESS["BPAA_ACT_APPROVERS_NONE"] = "aucun";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "Le champ '#COMMENT_LABEL#'  est obligatoire.";
$MESS["BPAA_ACT_NO_ACTION"] = "Action non valide sélectionné.";
$MESS["BPAR_ACT_BUTTON2"] = "A pris connaissance";
$MESS["BPAR_ACT_COMMENT"] = "Commentaire";
$MESS["BPAR_ACT_INFO"] = "Achevé(e)s #PERCENT#% (#REVIEWED# de #TOTAL#)";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "La propriété 'Utilisateurs' n'est pas indiquée.";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "La propriété 'Nom' n'est pas spécifiée.";
$MESS["BPAR_ACT_REVIEWED"] = "La lecture de l'élément est terminée.";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "L'utilisateur #PERSON# a lu l'élément #COMMENT#";
$MESS["BPAR_ACT_TRACK2"] = "L'élément a été lu par les utilisateurs #VAL#";
