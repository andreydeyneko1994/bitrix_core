<?php
$MESS["BPAA_DESCR_CM"] = "Komentarz";
$MESS["BPAR_DESCR_DESCR"] = "Czytaj element i umieszczaj komentarze";
$MESS["BPAR_DESCR_LR"] = "Ostatnio czytany przez";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Komentarz ostatniego czytelnika";
$MESS["BPAR_DESCR_NAME"] = "Czytaj element";
$MESS["BPAR_DESCR_RC"] = "Osoby czytające";
$MESS["BPAR_DESCR_TA1"] = "Automatyczna Analiza";
$MESS["BPAR_DESCR_TASKS"] = "Zadania";
$MESS["BPAR_DESCR_TC"] = "Osoby, które mają przeczytać";
