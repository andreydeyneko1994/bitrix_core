<?php
$MESS["BPAA_ACT_APPROVERS_NONE"] = "ninguno";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "El campo '#COMMENT_LABEL#' es requerido.";
$MESS["BPAA_ACT_NO_ACTION"] = "Acción seleccionada inválida.";
$MESS["BPAR_ACT_BUTTON2"] = "Listo";
$MESS["BPAR_ACT_COMMENT"] = "Comentarios";
$MESS["BPAR_ACT_INFO"] = "Completado el #PERC#% (#REV# de #TOT#)";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "La propiedad 'Usuarios' no esta especificada.";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "La propiedad 'Nombre' esta desaparecida.";
$MESS["BPAR_ACT_REVIEWED"] = "La lectura del elemento ha finalizado.";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "El usuario #PERSON# leyó el elemento #COMMENT#";
$MESS["BPAR_ACT_TRACK2"] = "El elemento fue leído por #VAL# usuarios";
