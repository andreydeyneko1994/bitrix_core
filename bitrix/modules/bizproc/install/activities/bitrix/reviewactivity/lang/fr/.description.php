<?php
$MESS["BPAA_DESCR_CM"] = "Commentaire";
$MESS["BPAR_DESCR_DESCR"] = "Étude de l'élément avec possibilité de laisser un commentaire dessus";
$MESS["BPAR_DESCR_LR"] = "Dernière lu par";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Commentaire du dernier lecteur";
$MESS["BPAR_DESCR_NAME"] = "Lecture de l'élément";
$MESS["BPAR_DESCR_RC"] = "Combien de personnes en ont pris connaissance";
$MESS["BPAR_DESCR_TA1"] = "Introduction automatique";
$MESS["BPAR_DESCR_TASKS"] = "Tâches";
$MESS["BPAR_DESCR_TC"] = "Nombre de ceux qui doivent être informés";
