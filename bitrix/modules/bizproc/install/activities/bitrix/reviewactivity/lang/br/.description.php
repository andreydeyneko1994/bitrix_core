<?php
$MESS["BPAA_DESCR_CM"] = "Comentar";
$MESS["BPAR_DESCR_DESCR"] = "Ler o elemento e postar comentários";
$MESS["BPAR_DESCR_LR"] = "Última leitura por";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Último comentário do leitor";
$MESS["BPAR_DESCR_NAME"] = "Ler elemento";
$MESS["BPAR_DESCR_RC"] = "Pessoas Lendo";
$MESS["BPAR_DESCR_TA1"] = "Auto Exame";
$MESS["BPAR_DESCR_TASKS"] = "Tarefas";
$MESS["BPAR_DESCR_TC"] = "Pessoas que ainda não leram";
