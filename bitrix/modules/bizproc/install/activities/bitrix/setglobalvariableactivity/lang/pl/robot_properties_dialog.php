<?php
$MESS["BIZPROC_AUTOMATION_SGVA_ACCESS_DENIED"] = "Wyłącznie administratorzy Bitrix24 mają dostęp do właściwości reguł automatyzacji.";
$MESS["BIZPROC_AUTOMATION_SGVA_DELETE"] = "Usuń";
$MESS["BIZPROC_AUTOMATION_SGVA_VARIABLE_LIST"] = "Wybierz zmienną globalną";
