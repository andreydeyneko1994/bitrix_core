<?
$MESS["BPCRU_PD_MAX_LEVEL"] = "Niveau du responsable (plus la valeur est importante, plus haut il se situe dans la hiérarchie)";
$MESS["BPCRU_PD_MAX_LEVEL_1"] = "1 (supérieur immédiat)";
$MESS["BPCRU_PD_NO"] = "Non";
$MESS["BPCRU_PD_SKIP_ABSENT"] = "Ignorer les absents";
$MESS["BPCRU_PD_SKIP_RESERVE"] = "Appliquer aux utilisateurs de réserve";
$MESS["BPCRU_PD_SKIP_TIMEMAN"] = "Ignorer les employés avec une journée de travail terminée";
$MESS["BPCRU_PD_TYPE"] = "Type";
$MESS["BPCRU_PD_TYPE_BOSS"] = "superviseur";
$MESS["BPCRU_PD_TYPE_ORDER"] = "séquentiel";
$MESS["BPCRU_PD_TYPE_RANDOM"] = "aléatoire";
$MESS["BPCRU_PD_USER2"] = "Utilisateurs de réserve";
$MESS["BPCRU_PD_USER_BOSS"] = "Pour l'utilisateur";
$MESS["BPCRU_PD_USER_RANDOM"] = "Des utilisateurs";
$MESS["BPCRU_PD_YES"] = "Oui";
?>