<?php
$MESS["BPSWFA_ACCESS_DENIED"] = "Jedynie administrator portalu ma dostęp do właściwości akcji";
$MESS["BPSWFA_DOCTYPE_ERROR"] = "Typ dokumentu nie jest zgodny z określonym w szablonie workflowu";
$MESS["BPSWFA_DOCTYPE_NOT_FOUND_ERROR"] = "Nie można znaleźć elementu docelowego (nieprawidłowe ID?)";
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "ID elementu nie zostało określone";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "Nie wybrano szablonu";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID elementu";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Typ pozycji";
$MESS["BPSWFA_SELFSTART_ERROR"] = "Szablon nie może być uruchamiany rekurencyjnie";
$MESS["BPSWFA_START_ERROR"] = "Błąd startu: #MESSAGE#";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Parametry workflow";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "Wymagany parametr jest pusty: #NAME#";
