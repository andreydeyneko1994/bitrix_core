<?php
$MESS["BPSWFA_PD_ACCESS_DENIED"] = "Jedynie administrator portalu ma dostęp do właściwości akcji";
$MESS["BPSWFA_PD_DOCUMENT_ID"] = "ID elementu";
$MESS["BPSWFA_PD_DOCUMENT_TYPE"] = "Typ elementu";
$MESS["BPSWFA_PD_ENTITY"] = "Jednostka";
$MESS["BPSWFA_PD_TEMPLATE"] = "Szablon";
$MESS["BPSWFA_PD_USE_SUBSCRIPTION"] = "Czekaj na zakończenie workflow";
