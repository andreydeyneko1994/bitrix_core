<?php
$MESS["BPSWFA_RPD_ACCESS_DENIED"] = "Apenas os administradores do Bitrix24 podem acessar propriedades de ação.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID da entidade";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Tipo de documento";
$MESS["BPSWFA_RPD_ENTITY"] = "Entidade";
$MESS["BPSWFA_RPD_TEMPLATE"] = "Modelo";
$MESS["BPSWFA_RPD_USE_SUBSCRIPTION"] = "Aguarde o fluxo de trabalho terminar";
