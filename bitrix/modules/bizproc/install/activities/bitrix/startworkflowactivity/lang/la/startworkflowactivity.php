<?php
$MESS["BPSWFA_ACCESS_DENIED"] = "Sólo el administrador del portar puede acceder a las acciones de las propiedades.";
$MESS["BPSWFA_DOCTYPE_ERROR"] = "El tipo de documento no coincide con el que se especificó en la plantilla del procesos de negocio";
$MESS["BPSWFA_DOCTYPE_NOT_FOUND_ERROR"] = "No se puede encontrar la entidad de destino (¿ID incorrecta?)";
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "No se especificó el ID del elemento";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "No se seleccionó plantilla.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID de la entidad";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Tipo de elemento";
$MESS["BPSWFA_SELFSTART_ERROR"] = "La plantilla no puede ejecutarse recursivamente";
$MESS["BPSWFA_START_ERROR"] = "Error de inicio: #MESSAGE#";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Parámetros del workflow";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "El parámetro requerido #NAME# está vacío";
