<?php
$MESS["BPSWFA_RPD_ACCESS_DENIED"] = "Solo los administradores de Bitrix24 pueden acceder a las propiedades de la acción.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID de la entidad";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Tipo de documento";
$MESS["BPSWFA_RPD_ENTITY"] = "Entidad";
$MESS["BPSWFA_RPD_TEMPLATE"] = "Plantilla";
$MESS["BPSWFA_RPD_USE_SUBSCRIPTION"] = "Espere a que finalice el procesos de negocio";
