<?php
$MESS["BPSWFA_DESCR_DESCR"] = "Exécute un flux de travail sur l'élément en cours";
$MESS["BPSWFA_DESCR_NAME"] = "Démarrer un flux de travail";
$MESS["BPSWFA_DESCR_WORKFLOW_ID"] = "ID du flux de travail démarré";
