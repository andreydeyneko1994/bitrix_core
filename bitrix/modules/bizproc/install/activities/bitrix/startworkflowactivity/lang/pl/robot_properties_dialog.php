<?php
$MESS["BPSWFA_RPD_ACCESS_DENIED"] = "Wyłącznie administratorzy Bitrix24 mają dostęp do właściwości działań.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID elementu";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Typ dokumentu";
$MESS["BPSWFA_RPD_ENTITY"] = "Element";
$MESS["BPSWFA_RPD_TEMPLATE"] = "Szablon";
$MESS["BPSWFA_RPD_USE_SUBSCRIPTION"] = "Czekaj na zakończenie workflowu";
