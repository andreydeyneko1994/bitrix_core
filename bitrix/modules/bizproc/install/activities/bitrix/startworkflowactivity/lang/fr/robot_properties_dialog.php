<?php
$MESS["BPSWFA_RPD_ACCESS_DENIED"] = "Seuls les administrateurs Bitrix24 peuvent accéder aux propriétés des actions.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID de l'entité";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Type du document";
$MESS["BPSWFA_RPD_ENTITY"] = "Entité";
$MESS["BPSWFA_RPD_TEMPLATE"] = "Modèle";
$MESS["BPSWFA_RPD_USE_SUBSCRIPTION"] = "En attente de la fin du flux de travail";
