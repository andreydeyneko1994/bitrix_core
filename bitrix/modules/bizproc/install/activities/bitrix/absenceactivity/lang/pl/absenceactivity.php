<?
$MESS["BPAA2_NO_PERMS"] = "Nie posiadasz prawa zapisu w grafiku nieobecności.";
$MESS["BPSNMA_EMPTY_ABSENCEFROM"] = "Brakuje atrybutu 'Data rozpoczęcia'.";
$MESS["BPSNMA_EMPTY_ABSENCENAME"] = "Brakuje atrybutu 'Nazwa Wydarzenia'.";
$MESS["BPSNMA_EMPTY_ABSENCETO"] = "Brakuje atrybutu 'Data zakończenia'.";
$MESS["BPSNMA_EMPTY_ABSENCEUSER"] = "Brakuje atrybutu 'Użytkownik'.";
?>