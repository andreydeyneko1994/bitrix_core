<?
$MESS["BPRNDSA_ALPHABET_ALPHALOWER"] = "Caracteres minúsculos";
$MESS["BPRNDSA_ALPHABET_ALPHAUPPER"] = "Caracteres maiúsculos";
$MESS["BPRNDSA_ALPHABET_NAME"] = "Alfabeto";
$MESS["BPRNDSA_ALPHABET_NUM"] = "Números";
$MESS["BPRNDSA_ALPHABET_SPECIAL"] = "Caracteres especiais";
$MESS["BPRNDSA_EMPTY_ALPHABET"] = "O alfabeto não está especificado";
$MESS["BPRNDSA_EMPTY_SIZE"] = "O comprimento da cadeia não está especificado";
$MESS["BPRNDSA_SIZE_NAME"] = "Comprimento da cadeia";
?>