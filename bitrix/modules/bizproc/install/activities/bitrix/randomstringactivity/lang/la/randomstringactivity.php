<?
$MESS["BPRNDSA_ALPHABET_ALPHALOWER"] = "Caracteres en minúscula";
$MESS["BPRNDSA_ALPHABET_ALPHAUPPER"] = "Caracteres en mayúscula";
$MESS["BPRNDSA_ALPHABET_NAME"] = "Alfabeto";
$MESS["BPRNDSA_ALPHABET_NUM"] = "Números";
$MESS["BPRNDSA_ALPHABET_SPECIAL"] = "Caracteres especiales";
$MESS["BPRNDSA_EMPTY_ALPHABET"] = "El alfabeto no está especificado";
$MESS["BPRNDSA_EMPTY_SIZE"] = "La longitud de la cadena no está definida";
$MESS["BPRNDSA_SIZE_NAME"] = "Longitud de la cadena";
?>