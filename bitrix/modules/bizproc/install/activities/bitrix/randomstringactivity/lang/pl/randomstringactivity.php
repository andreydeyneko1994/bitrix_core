<?
$MESS["BPRNDSA_ALPHABET_ALPHALOWER"] = "Małe litery";
$MESS["BPRNDSA_ALPHABET_ALPHAUPPER"] = "Wielkie litery";
$MESS["BPRNDSA_ALPHABET_NAME"] = "Alfabet";
$MESS["BPRNDSA_ALPHABET_NUM"] = "Cyfry";
$MESS["BPRNDSA_ALPHABET_SPECIAL"] = "Znaki specjalne";
$MESS["BPRNDSA_EMPTY_ALPHABET"] = "Nie określono alfabetu";
$MESS["BPRNDSA_EMPTY_SIZE"] = "Nie określono długości ciągu";
$MESS["BPRNDSA_SIZE_NAME"] = "Długość ciągu";
?>