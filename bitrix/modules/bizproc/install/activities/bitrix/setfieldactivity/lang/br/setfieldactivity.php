<?php
$MESS["BPSFA_ARGUMENT_NULL"] = "Está faltando o valor '#PARAM#' obrigatório.";
$MESS["BPSFA_EMPTY_FIELDS"] = "Todos os campos estão vazios";
$MESS["BPSFA_MERGE_MULTIPLE"] = "Não substituir, incluir em vários campos ao invés";
