<?php
$MESS["BPSFA_ARGUMENT_NULL"] = "Valeur obligatoire '#PARAM#' non renseignée.";
$MESS["BPSFA_EMPTY_FIELDS"] = "Aucune valeur de paramètres.";
$MESS["BPSFA_MERGE_MULTIPLE"] = "Remplir les champs multiples au lieu de les écraser :";
