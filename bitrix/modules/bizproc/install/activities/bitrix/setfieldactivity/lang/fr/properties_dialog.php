<?
$MESS["BPSFA_PD_ADD"] = "Ajouter une condition supplémentaire";
$MESS["BPSFA_PD_CALENDAR"] = "Calendrier";
$MESS["BPSFA_PD_CANCEL"] = "Annuler";
$MESS["BPSFA_PD_CANCEL_HINT"] = "Annuler";
$MESS["BPSFA_PD_CREATE"] = "Ajouter un champ";
$MESS["BPSFA_PD_DELETE"] = "Supprimer";
$MESS["BPSFA_PD_EMPTY_CODE"] = "Le code du champ n'est pas indiqué";
$MESS["BPSFA_PD_EMPTY_NAME"] = "Le nom du champ n'est pas indiqué";
$MESS["BPSFA_PD_FIELD"] = "Champ";
$MESS["BPSFA_PD_F_CODE"] = "ID";
$MESS["BPSFA_PD_F_LIST"] = "Liste de valeurs";
$MESS["BPSFA_PD_F_MULT"] = "Multiple";
$MESS["BPSFA_PD_F_NAME"] = "Dénomination";
$MESS["BPSFA_PD_F_REQ"] = "Oblig.";
$MESS["BPSFA_PD_F_TYPE"] = "Entité";
$MESS["BPSFA_PD_MODIFIED_BY"] = "Modifier de la part de";
$MESS["BPSFA_PD_NO"] = "Non";
$MESS["BPSFA_PD_SAVE"] = "Enregistrer";
$MESS["BPSFA_PD_SAVE_HINT"] = "Créer un champ";
$MESS["BPSFA_PD_WRONG_CODE"] = "Le code du champ peut contenir uniquement les lettres latines et les chiffres.";
$MESS["BPSFA_PD_YES"] = "Oui";
?>