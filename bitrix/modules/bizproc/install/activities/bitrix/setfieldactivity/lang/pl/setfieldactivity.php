<?php
$MESS["BPSFA_ARGUMENT_NULL"] = "Wymagana wartość '#PARAM#' jest brakująca.";
$MESS["BPSFA_EMPTY_FIELDS"] = "Wszystkie pola są puste.";
$MESS["BPSFA_MERGE_MULTIPLE"] = "Nie nadpisuj, ale dołącz do pól wielokrotnych";
