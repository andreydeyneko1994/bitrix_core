<?php
$MESS["BPSFA_ARGUMENT_NULL"] = "El valor requerido '#PARAM#' no fue encontrado.";
$MESS["BPSFA_EMPTY_FIELDS"] = "Todo los campos están vacíos.";
$MESS["BPSFA_MERGE_MULTIPLE"] = "No sobrescribir, en vez de ello, agregar a múltiples campos";
