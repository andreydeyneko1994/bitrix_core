<?
$MESS["BPFC_PD_ADD"] = "Dodaj Warunek";
$MESS["BPFC_PD_AND"] = "I";
$MESS["BPFC_PD_CALENDAR"] = "Kalendarz";
$MESS["BPFC_PD_CONDITION"] = "Warunek";
$MESS["BPFC_PD_CONTAIN"] = "zawiera";
$MESS["BPFC_PD_DELETE"] = "Usuń";
$MESS["BPFC_PD_EMPTY"] = "pole jest puste";
$MESS["BPFC_PD_EQ"] = "równe";
$MESS["BPFC_PD_FIELD"] = "Właściwość lub Pole";
$MESS["BPFC_PD_GE"] = "nie mniejsze niż";
$MESS["BPFC_PD_GT"] = "więcej niż";
$MESS["BPFC_PD_IN"] = "jest w";
$MESS["BPFC_PD_LE"] = "nie więcej niż";
$MESS["BPFC_PD_LT"] = "mniejsze niż";
$MESS["BPFC_PD_NE"] = "nierówne";
$MESS["BPFC_PD_NO"] = "Nie";
$MESS["BPFC_PD_NOT_EMPTY"] = "pole nie jest puste";
$MESS["BPFC_PD_OR"] = "LUB";
$MESS["BPFC_PD_PARAMS"] = "Parametry";
$MESS["BPFC_PD_VALUE"] = "Wartość";
$MESS["BPFC_PD_VARS"] = "Zmienne";
$MESS["BPFC_PD_YES"] = "Tak";
?>