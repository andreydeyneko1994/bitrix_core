<?php
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_DELETE"] = "Excluir";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_TERMINATE"] = "Parar";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_DELETE"] = "Tem certeza que deseja excluir o script em execução?";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_TERMINATE"] = "Tem certeza que deseja interromper o script?";
