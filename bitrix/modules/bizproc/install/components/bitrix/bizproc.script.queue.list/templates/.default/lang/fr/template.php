<?php
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_DELETE"] = "Supprimer";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_TERMINATE"] = "Stop";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_DELETE"] = "Voulez-vous vraiment supprimer le script exécuté ?";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_TERMINATE"] = "Voulez-vous vraiment arrêter le script ?";
