<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_RIGHTS_ERROR"] = "Droits insuffisants.";
$MESS["BIZPROC_AUTOMATION_SCHEME_SCHEME_ERROR"] = "La fonction de copie des règles d'automatisation n'est pas disponible pour ce type de document.";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_ACTION"] = "Action inconnue sélectionnée";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_DOCUMENT"] = "Type de document inconnu";
