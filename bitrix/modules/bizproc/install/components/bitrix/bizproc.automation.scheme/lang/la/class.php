<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_RIGHTS_ERROR"] = "Permisos insuficientes.";
$MESS["BIZPROC_AUTOMATION_SCHEME_SCHEME_ERROR"] = "La función para copiar las reglas de automatización no está disponible para este tipo de documento.";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_ACTION"] = "Se seleccionó una acción desconocida";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_DOCUMENT"] = "Tipo de documento desconocido";
