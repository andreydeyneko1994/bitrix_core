<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_RIGHTS_ERROR"] = "Permissões insuficientes.";
$MESS["BIZPROC_AUTOMATION_SCHEME_SCHEME_ERROR"] = "A função de cópia da regra de automação não está disponível para este tipo de documento.";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_ACTION"] = "Ação selecionada desconhecida";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_DOCUMENT"] = "Tipo de documento desconhecido";
