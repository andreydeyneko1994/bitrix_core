<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_CATEGORIES_NOT_EXISTS"] = "L'option que vous avez sélectionnée n'a pas de pipelines. Vous pouvez ignorer cette étape.";
$MESS["BIZPROC_AUTOMATION_SCHEME_DESTINATION_SCOPE_ERROR_ACTION_COPY"] = "Sélectionner la destination de la copie";
$MESS["BIZPROC_AUTOMATION_SCHEME_DESTINATION_SCOPE_ERROR_ACTION_MOVE"] = "Sélectionner la destination du déplacement";
$MESS["BIZPROC_AUTOMATION_SCHEME_DROPDOWN_PLACEHOLDER"] = "Sélectionner";
$MESS["BIZPROC_AUTOMATION_SCHEME_DST_CATEGORY"] = "Pipeline de destination";
$MESS["BIZPROC_AUTOMATION_SCHEME_DST_STAGE"] = "Étape de destination";
$MESS["BIZPROC_AUTOMATION_SCHEME_DST_TYPE_ACTION_COPY"] = "Copier les règles d'automatisation dans";
$MESS["BIZPROC_AUTOMATION_SCHEME_DST_TYPE_ACTION_MOVE"] = "Déplacer les règles d'automatisation dans";
$MESS["BIZPROC_AUTOMATION_SCHEME_EXECUTE_BUTTON"] = "Exécuter";
$MESS["BIZPROC_AUTOMATION_SCHEME_HELP_BUTTON"] = "aide";
$MESS["BIZPROC_AUTOMATION_SCHEME_TITLE_ACTION_COPY"] = "Copier les règles d'automatisation";
$MESS["BIZPROC_AUTOMATION_SCHEME_TITLE_ACTION_MOVE"] = "Déplacer les règles d'automatisation";
