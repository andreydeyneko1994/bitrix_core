<?
$MESS["BPATT_ALL"] = "Total";
$MESS["BPATT_AUTO_EXECUTE"] = "Autochargement";
$MESS["BPATT_HELP1_TEXT"] = "Le processus d'affaires avec les statuts est une longue procédure d'entreprise avec le partage des droits d'accès pour gérer des documents de statut différent.";
$MESS["BPATT_HELP2_TEXT"] = "Le processus d'affaires séquentiel est un processus d'affaires simple qui effectue une série d'actions consécutives sur un élément.";
$MESS["BPATT_MODIFIED"] = "Modifié";
$MESS["BPATT_NAME"] = "Dénomination";
$MESS["BPATT_USER"] = "Modifié par";
$MESS["WD_EMPTY"] = "Il n'y a pas de modèle de procédure d'entreprise. Créez un <a href=#HREF#>Business Processes</a> standard.";
$MESS["WD_EMPTY_DEFAULT"] = "Il n'y a pas de modèles de processus d'affaires.";
?>