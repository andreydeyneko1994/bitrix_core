<?php
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_DESCRIPTION"] = "Descripción";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_EMPTY"] = "Vacío";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_EMPTY_NAME"] = "El nombre no está especificado";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_HEPL"] = "Ayuda";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_MULTIPLE"] = "Múltiple";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_NAME"] = "Nombre";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_NO"] = "No";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_TYPE"] = "Tipo";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_VALUE"] = "Valor";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_VISIBILITY"] = "Visibilidad";
$MESS["BIZPROC_GLOBALFIELD_EDIT_TMP_YES"] = "Sí";
