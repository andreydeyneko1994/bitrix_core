<?php
$MESS["BIZPROC_GLOBALFIELDS_LIST_BTN_DELETE"] = "Excluir";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_CONSTANT_DELETE"] = "Tem certeza que deseja excluir a constante?";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_VARIABLE_DELETE"] = "Tem certeza que deseja excluir a variável?";
