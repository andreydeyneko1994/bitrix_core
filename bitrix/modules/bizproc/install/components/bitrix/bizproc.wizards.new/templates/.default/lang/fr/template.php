<?
$MESS["BPWC_WNCT_2LIST"] = "De processus d'entreprise";
$MESS["BPWC_WNCT_CANCEL"] = "Annuler";
$MESS["BPWC_WNCT_COMP_LIST_TPL"] = "Modèle de composant de liste des procédures d'entreprise";
$MESS["BPWC_WNCT_COMP_START_TPL"] = "Modèle de composant de lancement de procédure d'entreprise";
$MESS["BPWC_WNCT_COMP_TPL_DEF"] = "(par défaut)";
$MESS["BPWC_WNCT_COMP_VIEW_TPL"] = "Modèle de composant de la procédure d'entreprise";
$MESS["BPWC_WNCT_DESCR"] = "Description du processus métier";
$MESS["BPWC_WNCT_EADD"] = "Ajouter un élément d'intitulé";
$MESS["BPWC_WNCT_FILTERABLEFIELDS"] = "Les champs sont accessibles pour la filtration";
$MESS["BPWC_WNCT_ICON"] = "Icône de processus d'affaires";
$MESS["BPWC_WNCT_NAME"] = "Nom du processus d'entreprise";
$MESS["BPWC_WNCT_NEW_TMPL"] = "Nouveau Modèle (conséquent)";
$MESS["BPWC_WNCT_NEW_TMPL1"] = "Nouveau Modèle (avec les statuts)";
$MESS["BPWC_WNCT_P"] = "Nom unique des éléments du processus d'affaires";
$MESS["BPWC_WNCT_PERMS"] = "Processus d'affaires est disponible pour";
$MESS["BPWC_WNCT_PS"] = "Noms multiples pour les éléments de processus d'affaires";
$MESS["BPWC_WNCT_SAVE0"] = "Éditer";
$MESS["BPWC_WNCT_SAVE1"] = "Créer un Processus d'affaires";
$MESS["BPWC_WNCT_SAVE2"] = "Suivant";
$MESS["BPWC_WNCT_SORT"] = "Index de tri du processus d'affaires";
$MESS["BPWC_WNCT_SUBTITLE1"] = "Nouveau Processus d'affaires";
$MESS["BPWC_WNCT_SUBTITLE11"] = "Modification du processus d'affaires";
$MESS["BPWC_WNCT_SUBTITLE2"] = "Nouvelle configuration du processus d'affaires";
$MESS["BPWC_WNCT_TMPL"] = "Modèle de procédure d'entreprise";
$MESS["BPWC_WNCT_VISIBLEFIELDS"] = "Les champs visibles";
?>