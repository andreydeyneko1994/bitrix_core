<?php
$MESS["BIZPROC_SCRIPT_LIST_BTN_DELETE"] = "Supprimer";
$MESS["BIZPROC_SCRIPT_LIST_CONFIRM_DELETE"] = "Voulez-vous vraiment supprimer le script ?";
$MESS["BIZPROC_SCRIPT_LIST_TITLE_BUTTON_ADD_SCENARIO"] = "nouveau script";
$MESS["BIZPROC_SCRIPT_LIST_TITLE_MARKETPLACE"] = "Bitrix24.Market";
$MESS["BIZPROC_SCRIPT_LIST_TITLE_MARKETPLACE_EXPORT"] = "Exporter";
$MESS["BIZPROC_SCRIPT_LIST_TITLE_MARKETPLACE_IMPORT"] = "Importer";
