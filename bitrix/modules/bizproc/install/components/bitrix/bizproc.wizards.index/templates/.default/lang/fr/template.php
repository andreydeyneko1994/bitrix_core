<?
$MESS["BPWC_WICT_CREATE"] = "Ajouter";
$MESS["BPWC_WICT_DELETE"] = "Supprimer";
$MESS["BPWC_WICT_DELETE_PROMT"] = "Cela permettra d'éliminer toutes les informations associées à ce processus ! Continuer ?";
$MESS["BPWC_WICT_EDIT"] = "Éditer";
$MESS["BPWC_WICT_EMPTY"] = "Aucun type de processus d'affaires n'est disponible.";
$MESS["BPWC_WICT_NEW_BP"] = "Nouveau Processus d'affaires";
?>