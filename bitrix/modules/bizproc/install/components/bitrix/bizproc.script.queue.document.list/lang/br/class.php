<?php
$MESS["BIZPROC_SCRIPT_QDL_ACTION_DELETE"] = "Excluir";
$MESS["BIZPROC_SCRIPT_QDL_COLUMN_DOCUMENT_ID"] = "Entidade";
$MESS["BIZPROC_SCRIPT_QDL_COLUMN_STATUS"] = "Status";
$MESS["BIZPROC_SCRIPT_QDL_COLUMN_WORKFLOW_ID"] = "Fluxo de trabalho";
$MESS["BIZPROC_SCRIPT_QDL_PAGE_TITLE"] = "Log de script";
$MESS["BIZPROC_SCRIPT_QDL_SCRIPT_NOT_FOUND"] = "O script não foi encontrado";
