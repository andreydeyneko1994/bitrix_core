<?php
$MESS["BIZPROC_AS_ACT_TITLE"] = "Título:";
$MESS["BIZPROC_AS_DESC"] = "Por favor configure os parâmetros de ação.";
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Selecionar Campos";
$MESS["BIZPROC_AS_TITLE"] = "Parâmetros de ação";
$MESS["BP_ACT_SET_BROKEN_LINK_MESSAGE_ERROR"] = "A regra de automação refere-se a campos, variáveis ou constantes ausentes ou indisponíveis.";
$MESS["BP_ACT_SET_COMMENT_ROW"] = "Comentário:";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_DUP"] = "A ação ID #ID# já está em uso por este modelo.";
$MESS["BP_ACT_SET_ID_EMPTY"] = "O ID de ação não pode ser vazio.";
$MESS["BP_ACT_SET_ID_ROW"] = "ID da Ação:";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Mostrar/ocultar ID da ação";
