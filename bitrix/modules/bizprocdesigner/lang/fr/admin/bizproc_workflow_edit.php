<?
$MESS["BIZPROC_EMPTY_EXPORT"] = "S'il vous plaît enregistrer le modèle avant de l'exporter.";
$MESS["BIZPROC_IMPORT_BUTTON"] = "Charger";
$MESS["BIZPROC_IMPORT_FILE"] = "Fichier";
$MESS["BIZPROC_IMPORT_TITLE"] = "Importation d'un modèle";
$MESS["BIZPROC_USER_PARAMS_SAVE_ERROR"] = "La taille des actions dans le panel 'Mes actions' est trop importante. Les modifications du panel ne seront pas sauvegardées.";
$MESS["BIZPROC_WFEDIT_APPLY_BUTTON"] = "Appliquer";
$MESS["BIZPROC_WFEDIT_BEFOREUNLOAD"] = "Attention ! Les données non enregistrées seront perdues. Voulez-vous vraiment quitter cette page ?";
$MESS["BIZPROC_WFEDIT_CANCEL_BUTTON"] = "Annuler";
$MESS["BIZPROC_WFEDIT_CATEGORY_CONSTR"] = "Constructions";
$MESS["BIZPROC_WFEDIT_CATEGORY_DOC"] = "Traitement des documents";
$MESS["BIZPROC_WFEDIT_CATEGORY_INTER"] = "Notifications";
$MESS["BIZPROC_WFEDIT_CATEGORY_MAIN"] = "Essentiel";
$MESS["BIZPROC_WFEDIT_CATEGORY_OTHER"] = "Autre";
$MESS["BIZPROC_WFEDIT_CATEGORY_REST"] = "Actions d'application";
$MESS["BIZPROC_WFEDIT_CATEGORY_TASKS"] = "Tâches";
$MESS["BIZPROC_WFEDIT_CHECK_ERROR"] = "Erreur lors de l'ouverture du modèle : toutes les actions présentes dans le modèle ne sont pas installées sur le portail.";
$MESS["BIZPROC_WFEDIT_DEFAULT_TITLE"] = "Modèle de procédure d'entreprise";
$MESS["BIZPROC_WFEDIT_ERROR_TYPE"] = "Le type de document n'a pas été indiqué.";
$MESS["BIZPROC_WFEDIT_IMPORT_ERROR"] = "Erreur dans l'importation de modèle de procédure d'entreprise.";
$MESS["BIZPROC_WFEDIT_MENU_ADD"] = "Créer un Modèle";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ"] = "Processus d'affaires consécutif";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ_TITLE"] = "Le processus d'affaires séquentiel est un processus d'affaires simple qui effectue une série d'actions consécutives sur un document.";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE"] = "Processus d'entreprise avec des statuts";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE_TITLE"] = "Le processus d'affaires avec les statuts est une longue procédure d'entreprise avec le partage des droits d'accès pour gérer des documents de statut différent.";
$MESS["BIZPROC_WFEDIT_MENU_ADD_TITLE"] = "Ajouter un nouveau modèle";
$MESS["BIZPROC_WFEDIT_MENU_ADD_WARN"] = "Si vous avez modifié le modèle et n'avez pas sauvegardé les modifications, elles seront perdues. Continuer ?";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT"] = "Décharger";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT_TITLE"] = "Exportation d'un modèle de procédure d'entreprise";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT"] = "Charger";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_PROMT"] = "Le modèle actuel du processus d'affaires sera remplacé par celui qui est importé. Continuer ?";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_TITLE"] = "Importation du modèle du processus d'affaires";
$MESS["BIZPROC_WFEDIT_MENU_LIST"] = "Liste de modèles";
$MESS["BIZPROC_WFEDIT_MENU_LIST_TITLE"] = "Accéder à la liste de modèles";
$MESS["BIZPROC_WFEDIT_MENU_PARAMS"] = "Paramètres de modèle";
$MESS["BIZPROC_WFEDIT_MENU_PARAMS_TITLE"] = "Paramètres des modèles; variables; autorun";
$MESS["BIZPROC_WFEDIT_SAVE_BUTTON"] = "Enregistrer";
$MESS["BIZPROC_WFEDIT_SAVE_ERROR"] = "Erreur de sauvegarde : ";
$MESS["BIZPROC_WFEDIT_TITLE_ADD"] = "Nouveau modèle de processus d'affaires";
$MESS["BIZPROC_WFEDIT_TITLE_EDIT"] = "Édition du modèle du processus d'entreprise";
?>