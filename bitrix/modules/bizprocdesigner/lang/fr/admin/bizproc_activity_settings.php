<?php
$MESS["BIZPROC_AS_ACT_TITLE"] = "Dénomination:";
$MESS["BIZPROC_AS_DESC"] = "Configurez, s'il vous plaît, les paramètres de l'action.";
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Sélectionner des champs";
$MESS["BIZPROC_AS_TITLE"] = "Configuration des paramètres de l'action";
$MESS["BP_ACT_SET_BROKEN_LINK_MESSAGE_ERROR"] = "La règle d'automatisation fait référence à des champs, variables ou constantes manquants ou indisponibles.";
$MESS["BP_ACT_SET_COMMENT_ROW"] = "Texte du commentaire :";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_DUP"] = "L'identificateur de l'action #ID# est déjà utilisé dans ce modèle.";
$MESS["BP_ACT_SET_ID_EMPTY"] = "L'identificateur de l'action ne peut pas être vide.";
$MESS["BP_ACT_SET_ID_ROW"] = "ID de l'action : ";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Montrer/cacher l'identifiant de l'action";
