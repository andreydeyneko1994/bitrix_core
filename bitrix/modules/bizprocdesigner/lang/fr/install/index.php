<?
$MESS["BIZPROCDESIGNER_INSTALL_DESCRIPTION"] = "Module pour développer, créer et modifier des workflows.";
$MESS["BIZPROCDESIGNER_INSTALL_NAME"] = "Créateur visuel de procédure d'entreprise";
$MESS["BIZPROC_ERROR_BPM"] = "Module de processus d'affaires est requis.";
$MESS["BIZPROC_ERROR_EDITABLE"] = "Le module n'est ipas disponible dans votre édition.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Installation de la solution";
$MESS["BIZPROC_PHP_L439"] = "Vous utilisez la version PHP #VERS#, pour fonctionner le module a besoin d'une version 5.0.0 ou plus récente. Veuillez mettre à jour PHP ou contactez le service d'appui technique de votre hébergement.";
?>