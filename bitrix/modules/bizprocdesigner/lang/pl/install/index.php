<?
$MESS["BIZPROCDESIGNER_INSTALL_DESCRIPTION"] = "Moduł służący do projektowania, tworzenia i edytowania procesów biznesowych.";
$MESS["BIZPROCDESIGNER_INSTALL_NAME"] = "Konstruktor procesu biznesowego";
$MESS["BIZPROC_ERROR_BPM"] = "Moduł procesów biznesowych jest wymagany.";
$MESS["BIZPROC_ERROR_EDITABLE"] = "Twoja wersja nie zapewnia tego modułu.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["BIZPROC_PHP_L439"] = "Używasz wersji #VERS# PHP, a moduł wymaga wersji 5.0.0 lub nowszej. Proszę zaktualizować swoją wersję instalacyjną PHP lub skontaktować się z pomocą techniczną.";
?>