<?php
$MESS["BIZPROC_AS_ACT_TITLE"] = "Tytuł:";
$MESS["BIZPROC_AS_DESC"] = "Proszę skonfigurować parametry działania.";
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Wstaw parametr z pola";
$MESS["BIZPROC_AS_TITLE"] = "Parametry Działania";
$MESS["BP_ACT_SET_BROKEN_LINK_MESSAGE_ERROR"] = "Reguła automatyzacji odnosi się do brakujących lub niedostępnych pól, zmiennych lub stałych.";
$MESS["BP_ACT_SET_COMMENT_ROW"] = "Komentarz:";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_DUP"] = "ID działania #ID# jest już używane przez ten szablon.";
$MESS["BP_ACT_SET_ID_EMPTY"] = "ID działania nie może być puste.";
$MESS["BP_ACT_SET_ID_ROW"] = "ID działania:";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Pokaż/ukryj ID działania";
