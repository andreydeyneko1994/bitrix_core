<?php
$MESS["BIZPROC_AS_ACT_TITLE"] = "Titulo:";
$MESS["BIZPROC_AS_DESC"] = "Por favor configure los parámetros de la acción.";
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Seleccionar campos";
$MESS["BIZPROC_AS_TITLE"] = "Parámetros de la acción";
$MESS["BP_ACT_SET_BROKEN_LINK_MESSAGE_ERROR"] = "La regla de automatización se refiere a campos, variables o constantes faltantes o no disponibles.";
$MESS["BP_ACT_SET_COMMENT_ROW"] = "Comentario:";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_DUP"] = "El ID de la acción #ID# ya esta en uso por esta plantilla.";
$MESS["BP_ACT_SET_ID_EMPTY"] = "El ID de la acción no puede estar vació.";
$MESS["BP_ACT_SET_ID_ROW"] = "ID de la acción:";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Mostrar/Ocultar ID de la acción";
