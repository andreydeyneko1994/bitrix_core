<?php
$MESS["CALENDAR_ICAL_INVENT_DESC"] = "
#DATE_START# – godzina rozpoczęcia
#DATE_END# – godzina zakończenia
#MESSAGE# – wiadomość tekstowa
";
$MESS["CALENDAR_ICAL_INVENT_NAME"] = "Wyślij zaproszenie do użytkownika  ";
$MESS["CALENDAR_INVENT_MESSAGE_BODY"] = "
<p>Zapraszam na wydarzenie #NAME# organizowane przez #ORGANIZER#.</p>

<p>Odbędzie się ono od #DATE_START# do #DATE_END# w #LOCATION#.</p>

<p>Uczestnicy: #ATTENDEES_LIST#.</p>

<p>Opis: #DESCRIPTION#.</p> 

<p>Pliki: #FILES#</p>
";
$MESS["CALENDAR_INVITATION_NAME"] = "Zaproszenie";
$MESS["CALENDAR_REPLY_MESSAGE_BODY"] = "
<p>#NAME# odpowiedział/-a #ANSWER#</p>
";
