<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Uczestnicy wydarzenia zmienili się";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "Godzina wydarzenia została zmieniona";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "Miejsce wydarzenia zostało zmienione";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "Temat spotkania został zmieniony";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Cykliczne zasady wydarzenia uległy zmianie";
$MESS["EC_CALENDAR_ICAL_MAIL_HIDE_GUESTS_INFORMATION"] = "Twórca wydarzenia zdecydował się ukryć uczestników";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "Wydarzenie zostało zmienione.";
