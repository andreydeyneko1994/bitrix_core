<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Uczestnicy wydarzenia zmienili się";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "Czas wydarzenia został zmieniony";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "Miejsce wydarzenia zostało zmienione";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "Temat wydarzenia został zmieniony";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Cykliczne zasady wydarzenia uległy zmianie";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_CANCEL"] = "Wydarzenie odwołane";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "Wydarzenie zostało zmienione";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_ACCEPTED"] = "Zaproszenie przyjęte";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_DECLINED"] = "Zaproszenie odrzucone";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REQUEST"] = "Zaproszenie";
