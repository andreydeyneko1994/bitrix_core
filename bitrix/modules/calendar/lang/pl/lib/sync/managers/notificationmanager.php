<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "Kalendarz został zsynchronizowany z kalendarzem Google.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "Kalendarz został zsynchronizowany z kalendarzem iCloud.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "Kalendarz został zsynchronizowany z kalendarzem Office365.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Błąd podczas włączania synchronizacji kalendarza Google.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Błąd podczas włączania synchronizacji kalendarza iCloud.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Błąd podczas włączania synchronizacji kalendarza Office365.";
