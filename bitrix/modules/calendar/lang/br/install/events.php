<?php
$MESS["CALENDAR_ICAL_INVENT_DESC"] = "
#DATE_START# - horário de início
#DATE_END# - horário de término
#MESSAGE# - mensagem de texto
";
$MESS["CALENDAR_ICAL_INVENT_NAME"] = "Enviar convite ao usuário  ";
$MESS["CALENDAR_INVENT_MESSAGE_BODY"] = "
<p>Eu gostaria de convidar você para o evento #NAME# realizado por #ORGANIZER#</p>

<p>Acontecerá de #DATE_START# a #DATE_END# em #LOCATION#.</p>

<p>Participantes: #ATTENDEES_LIST#.</p>

<p>Descrição: #DESCRIPTION#.</p> 

<p>Arquivos: #FILES#</p>
";
$MESS["CALENDAR_INVITATION_AUTO_GENERATED"] = "
---------------------------------------------------------------------

Esta mensagm foi gerada automaticamente.";
$MESS["CALENDAR_INVITATION_DESC"] = "
#EMAIL_TO# - e-mail da pessoa convidada
#TITLE# - assunto
#MESSAGE# - descrio completa do evento
";
$MESS["CALENDAR_INVITATION_NAME"] = "Convite";
$MESS["CALENDAR_REPLY_MESSAGE_BODY"] = "
<p>#NAME# respondeu #ANSWER#</p>
";
