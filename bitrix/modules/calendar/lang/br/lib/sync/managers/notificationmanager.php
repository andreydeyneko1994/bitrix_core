<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "O calendário foi sincronizado com o Calendário do Google com sucesso.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "O calendário foi sincronizado com o Calendário do iCloud com sucesso.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "O calendário foi sincronizado com o Calendário do Office365 com sucesso.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Erro ao ativar a sincronização do calendário Google.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Erro ao ativar a sincronização do calendário iCloud.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Erro ao ativar a sincronização do calendário Office365.";
