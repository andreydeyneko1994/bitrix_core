<?php
$MESS["EC_NS_CHANGE"] = "Evento editado";
$MESS["EC_NS_DELETE_LOCATION"] = "Excluir sala de reunião";
$MESS["EC_NS_EVENT_COMMENT"] = "Novo comentário sobre um evento";
$MESS["EC_NS_INFO"] = "Confirmar ou recusar participação no evento";
$MESS["EC_NS_INVITE"] = "Convite para evento";
$MESS["EC_NS_REMINDER"] = "Lembrete de evento";
