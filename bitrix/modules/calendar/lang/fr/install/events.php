<?php
$MESS["CALENDAR_ICAL_INVENT_DESC"] = "
#DATE_START# - heure de début
#DATE_END# - heure de fin
#MESSAGE# - message texte
";
$MESS["CALENDAR_ICAL_INVENT_NAME"] = "Envoyer l'invitation à l'utilisateur  ";
$MESS["CALENDAR_INVENT_MESSAGE_BODY"] = "
<p>Permettez-moi vous inviter à l'évènement #NAME# organisé par #ORGANIZER#.</p>

<p>Il aura lieu du #DATE_START# au #DATE_END# à #LOCATION#.</p>

<p>Participants : #ATTENDEES_LIST#.</p>

<p>Description : #DESCRIPTION#.</p> 

<p>Fichiers : #FILES#</p>
";
$MESS["CALENDAR_INVITATION_DESC"] = "
#EMAIL_TO# - adresse email (adresse électronique, adresse email ou adresse courriel) de la personne invitée
#TITLE# - sujet
#MESSAGE# - description détaillée de l'entretien";
$MESS["CALENDAR_INVITATION_NAME"] = "Invitation";
$MESS["CALENDAR_REPLY_MESSAGE_BODY"] = "
<p>#NAME# à répondu #ANSWER#</p>
";
