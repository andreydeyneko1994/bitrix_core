<?php
$MESS["CALENDAR_TIP_TEMPLATE_LINK_COPIED"] = "Le lien de l'évènement a été copié dans le Presse-papiers.";
$MESS["DEL_CON_CALENDARS"] = "Supprimer les Calendriers";
$MESS["EC_ACCESS_DENIED"] = "Accès refusé";
$MESS["EC_ACCESS_GROUP_ADMIN"] = "Propriétaire du groupe de travail";
$MESS["EC_ACCESS_GROUP_MEMBERS"] = "Tous les membres du groupe de travail";
$MESS["EC_ACCESS_GROUP_MODERATORS"] = "Modérateurs du groupe de travail";
$MESS["EC_ACTION_EXPORT"] = "Exporter (iCal)";
$MESS["EC_ACTION_EXTERNAL_ADJUST"] = "Éditer les paramètres";
$MESS["EC_ACTION_HIDE"] = "Désactiver et masquer";
$MESS["EC_ADD"] = "Ajouter";
$MESS["EC_ADD_APPLICATION"] = "Ajouter une application";
$MESS["EC_ADD_CALDAV"] = "Ajouter un calendrier externe";
$MESS["EC_ADD_CALDAV_LINK"] = "Adresse du serveur CalDav";
$MESS["EC_ADD_CALDAV_NAME"] = "Nom";
$MESS["EC_ADD_CALDAV_PASS"] = "Mot de passe";
$MESS["EC_ADD_CALDAV_SECTIONS"] = "Calendriers disponibles";
$MESS["EC_ADD_CALDAV_USER_NAME"] = "Nom d'utilisateur";
$MESS["EC_ADD_EVENT"] = "Ajouter un évènement";
$MESS["EC_ADD_LOCATION"] = "Ajouter une salle de réunion";
$MESS["EC_ADD_TASK"] = "Nouvelle tâche";
$MESS["EC_ALLOW_INVITE_LABEL"] = "Les participants peuvent inviter d'autres personnes";
$MESS["EC_ALL_DAY"] = "toute la journée";
$MESS["EC_ASK_TZ"] = "Sélectionnez votre fuseau horaire : ";
$MESS["EC_ATTENDEES_EDIT"] = "modifier";
$MESS["EC_ATTENDEES_LABEL"] = "Participants";
$MESS["EC_ATTENDEES_MORE"] = "#COUNT# en plus";
$MESS["EC_ATTENDEES_TOTAL_COUNT"] = "a invité #COUNT#";
$MESS["EC_B24_LOCATION_LIMITATION"] = "<span>Seules les <a href=\"/settings/license_all.php\" target=\"_blank\">offres commerciales sélectionnées</a> vous permettent de créer et de sélectionner des lieux d'événement. Mettez maintenant à niveau et profitez de plus de fonctionnalités utiles :
<ol>
<li>Gestion du temps de travail</li>
<li>Création et gestion de réunions et d'événements</li>
<li>Intégration d'utilisateurs externes et de tierces parties à des groupe de travail et des projets</li>
</ol>
</span>";
$MESS["EC_B24_LOCATION_LIMITATION_TITLE"] = "Créer et sélectionner un lieu";
$MESS["EC_BUSY_ALERT"] = "Un ou plusieurs participants ne sont pas disponibles pendant les heures indiquées et ne peuvent pas être invités.";
$MESS["EC_CALDAV_COLLAPSE"] = "cacher";
$MESS["EC_CALDAV_CONNECTION_ERROR"] = "Le serveur CalDav a renvoyé l'erreur \"#ERROR_STR#\" lors de la vérification de la connexion \"#CONNECTION_NAME#\". Veuillez vérifier les paramètres de connexion et réessayer.";
$MESS["EC_CALDAV_DEL"] = "supprimer";
$MESS["EC_CALDAV_EDIT"] = "éditer";
$MESS["EC_CALDAV_NOTICE"] = "Les calendriers externes sont automatiquement synchronisés.";
$MESS["EC_CALDAV_NO_CHANGE"] = "- ne pas modifier - ";
$MESS["EC_CALDAV_RESTORE"] = "restaurer";
$MESS["EC_CALDAV_SYNC_DATE"] = "Date de la synchronisation";
$MESS["EC_CALDAV_SYNC_ERROR"] = "La dernière synchronisation a échoué.";
$MESS["EC_CALDAV_SYNC_OK"] = "Les calendriers ont bien été synchronisés.";
$MESS["EC_CALDAV_TITLE"] = "Calendriers externes connectés";
$MESS["EC_CALDAV_URL_ERROR"] = "Les paramètres de connexion CalDAV sont incorrects";
$MESS["EC_CALENDAR_REC_EVENT"] = "Événement récurrent";
$MESS["EC_CAL_CONNECT_MORE"] = "connecter #COUNT# de plus";
$MESS["EC_CAL_DAV_CON_WAIT"] = "Veuillez patienter pendant que la demande précédente est traitée.";
$MESS["EC_CAL_GOOGLE_HIDE_CONFIRM"] = "Voulez-vous vraiment désactiver la synchronisation de ce calendrier et le masquer ?";
$MESS["EC_CAL_LAST_SYNC_DATE"] = "Dernière synchronisation le";
$MESS["EC_CAL_REMOVE_GOOGLE_SYNC_CONFIRM"] = "Voulez-vous vraiment désactiver la synchronisation de Google Calendar et supprimer ces calendriers ?";
$MESS["EC_CAL_SYNC_ANDROID"] = "Android";
$MESS["EC_CAL_SYNC_CONNECT"] = "connexion";
$MESS["EC_CAL_SYNC_DISCONNECT"] = "déconnexion";
$MESS["EC_CAL_SYNC_EXCHANGE"] = "MS Exchange";
$MESS["EC_CAL_SYNC_GOOGLE"] = "Google Calendar";
$MESS["EC_CAL_SYNC_IPHONE"] = "iPhone";
$MESS["EC_CAL_SYNC_MAC"] = "Mac OSX";
$MESS["EC_CAL_SYNC_OFFICE_365"] = "Office 365";
$MESS["EC_CAL_SYNC_OK"] = "Synchronisé";
$MESS["EC_CAL_SYNC_OUTLOOK"] = "MS Outlook";
$MESS["EC_CAL_SYNC_REFRESH"] = "mettre à jour";
$MESS["EC_CAL_SYNC_TITLE"] = "Synchroniser";
$MESS["EC_COLLAPSED_MESSAGE"] = "Évènements";
$MESS["EC_COLOR"] = "Autre couleur";
$MESS["EC_COUNTER_INVITATION_PLURAL_0"] = "invitation";
$MESS["EC_COUNTER_INVITATION_PLURAL_1"] = "invitations";
$MESS["EC_COUNTER_INVITATION_PLURAL_2"] = "invitations";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_0"] = "avec nouveaux commentaires";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_1"] = "avec nouveaux commentaires";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_2"] = "avec nouveaux commentaires";
$MESS["EC_COUNTER_TOTAL"] = "Évènements";
$MESS["EC_CREATE_EVENT"] = "Créer un évènement";
$MESS["EC_DATE_FORMAT_1_MAY"] = "F j,";
$MESS["EC_DATE_WEEK_NUMBER"] = "semaine #WEEK_NUMBER#";
$MESS["EC_DAY_LENGTH"] = "#COUNT# jours";
$MESS["EC_DECLINE_ALL"] = "Tous les évènements";
$MESS["EC_DECLINE_NEXT"] = "Cet évènement et tous les évènements suivants";
$MESS["EC_DECLINE_ONLY_THIS"] = "Seulement cet évènement";
$MESS["EC_DECLINE_REC_EVENT"] = "Voulez-vous refuser de participer à l'évènement périodique ?";
$MESS["EC_DEFAULT_ENTRY_NAME"] = "Nouvel évènement";
$MESS["EC_DELETE_MEETING_CONFIRM"] = "Vous êtes sur le point de supprimer un évènement contenant d'autres participants. Voulez-vous vraiment supprimer l'évènement ?";
$MESS["EC_DELETE_MEETING_GUEST_CONFIRM"] = "La suppression d'un évènement annulera automatiquement votre participation. Voulez-vous vraiment supprimer l'évènement ?";
$MESS["EC_DESCRIPTION"] = "Description";
$MESS["EC_DESTINATION_ADD_MORE"] = "Ajouter plus";
$MESS["EC_DESTINATION_ADD_USERS"] = "Ajouter des personnes, des groupes ou des services";
$MESS["EC_EDEV_EXP_WARN"] = "Remarque ! La vérification des données exportées a échoué. Veuillez vous assurer que vos droits d'accès soient valides.";
$MESS["EC_ENTRIES_EVENTS"] = "Évènements";
$MESS["EC_ENTRIES_TASKS"] = "Tâches";
$MESS["EC_ENTRY_NAME"] = "Nom de l'évènement";
$MESS["EC_EVENT"] = "Evènement";
$MESS["EC_EVENT_IS_MINE"] = "Mon évènement";
$MESS["EC_EVENT_TZ_DEF_HINT"] = "Sélectionnez votre fuseau horaire. Vos préférences seront enregistrées.";
$MESS["EC_EVENT_TZ_HINT"] = "Si l'évènement a lieu dans un fuseau horaire différent, sélectionnez-le dans la liste. Vous pouvez attribuer des fuseaux horaires différents aux heures de début et de fin.";
$MESS["EC_EXP_TEXT"] = "Exporter le calendrier vers des applications tierces (au format iCal) : ";
$MESS["EC_FULL_FORM_LABEL"] = "Formulaire complet";
$MESS["EC_HOST"] = "Organisateur";
$MESS["EC_JS_DEL_MEETING_GUEST_CONFIRM"] = "Voulez-vous vraiment refuser cet évènement ?";
$MESS["EC_JS_EXPORT_TILE"] = "Exporter au format iCal";
$MESS["EC_JS_ICAL_COPY_ICAL_SYNC_LINK"] = "Copier le lien";
$MESS["EC_JS_ICAL_ERROR_WITH_PATHES"] = "Attention ! Les données de synchronisation du calendrier sont incorrectes. Veuillez contacter votre administrateur Bitrix24 ou l'assistance.";
$MESS["EC_LOCATION"] = "Lieu";
$MESS["EC_LOCATION_404"] = "aucune donnée";
$MESS["EC_LOCATION_CHOOSE"] = "Sélectionner une salle de réunion";
$MESS["EC_LOCATION_LABEL"] = "Lieu";
$MESS["EC_LOCATION_MEETING_ROOM_SET"] = "Configurer la liste";
$MESS["EC_LOCATION_RESERVE_ERROR"] = "Le lieu est indisponible à l'heure indiquée.";
$MESS["EC_LOCATION_SETTINGS_MESSAGE_DESCRIPTION"] = "Sélectionnez les utilisateurs que vous voulez autoriser à créer, supprimer et modifier les salles de réunion de votre entreprise";
$MESS["EC_LOCATION_SETTINGS_MORE_INFO"] = "Détails";
$MESS["EC_LOCATION_VIEW_LOCKED"] = "Vous ne pouvez pas créer ou réserver de salle de réunion avec votre offre actuelle";
$MESS["EC_LOCATION_VIEW_UNLOCK_FEATURE"] = "Changez d'offre maintenant";
$MESS["EC_MANAGE_CALDAV"] = "Configurer les calendriers externes (CalDAV)";
$MESS["EC_MANAGE_CALDAV_TITLE"] = "Gérer les connexions aux calendriers externes.";
$MESS["EC_MAP_LOCATION_LABEL"] = "Préciser le lieu";
$MESS["EC_MEETING_ROOM_ADD"] = "Ajouter";
$MESS["EC_MEETING_ROOM_LIST_TITLE"] = "Salles de réunion";
$MESS["EC_MEETING_ROOM_PLACEHOLDER"] = "Nom";
$MESS["EC_MOBILE_HELP_ANDROID"] = "<p>Pour connecter vos calendriers, reproduisez les manipulations suivantes sur votre téléphone portable.
<ol>
<li>Ouvrez GooglePlay et installez l'application Bitrix24.</li>
<li>Sélectionnez le menu Paramètres > Comptes > \"Bitrix24\"
Si aucun compte n'est présent, créez-en un nouveau et sélectionnez Bitrix24.
<ul>
	<li>Indiquez votre adresse Bitrix24 (par ex. : <i>company.bitrix24.com</i>)</li>
	<li>Saisissez votre identifiant (e-mail)</li>
	<li>Saisissez votre mot de passe</li>
</ul>
<li>Si vous utilisez l'authentification en deux étapes, utilisez le mot de passe indiqué dans le profil d'utilisateur (\"Mots de passe des applications\").</li>
</li>
<li>Cliquez sur le compte et sélectionnez un calendrier et/ou des contacts à synchroniser et enregistrer sur votre téléphone.</li>

<li>Cliquez sur Synchroniser.</li>
</ol>
Votre calendrier Bitrix24 est maintenant disponible sur le calendrier de votre téléphone portable, et tous les évènements sont synchronisés.</p>";
$MESS["EC_MOBILE_HELP_IPHONE"] = "<p>Pour configurer votre appareil Apple afin qu'il prenne en charge CalDAV : </p>
<ol>
	<li>Sur votre appareil Apple, ouvrez le menu <b>Paramètres</b> &gt; <b>Comptes et mots de passe</b>.</li>
	<li>Sélectionnez <b>Ajouter un compte</b> dans la liste des comptes.</li>
	<li>Sélectionnez CalDAV comme type de compte (<b>Autre</b> &gt; <b>Comptes CalDAV</b>).</li>
	<li>Indiquez l'adresse du site en tant que serveur (<span class=\"bxec-link\">#CALENDAR_LINK#</span>). Utilisez vos nom d'utilisateur et mot de passe.</li>
	<li>Si vous utilisez l'authentification en deux étapes, utilisez le mot de passe indiqué dans le profil d'utilisateur : <b>Mots de passe des applications</b> &gt; <b>Calendrier</b>.</li>
	<li>Pour indiquer le numéro du port, enregistrez le compte, puis rouvrez-le en édition et allez dans l'espace <b>Plus</b>.</li>
</ol>
<p>Vos calendriers apparaîtront dans l'application Calendar.</p>";
$MESS["EC_MOBILE_HELP_MAC"] = "<p>Pour connecter des calendriers dans iCal : </p>
<ol>
		<li>Ouvrez Calendrier > Paramètres et tapotez sur l'onglet Comptes.</li>
		<li>Sélectionnez Ajouter un compte.</li>
		<li>Sélectionnez CalDAV.</li>
		<li>Réglez le type de compte sur manuel.</li>
		<li>Indiquez l'adresse de ce site en tant que serveur (<span class=\"bxec-link\">#CALENDAR_LINK#</span>). Utilisez vos nom d'utilisateur et mot de passe.</li>
		<li>Si vous utilisez l'authentification en deux étapes, utilisez le mot de passe indiqué dans le profil d'utilisateur (\"Mots de passe des applications\").</li>
		<li>Pour indiquer le numéro du port, enregistrez le compte, puis rouvrez-le en édition.</li>
</ol>
<p>Vos calendriers apparaîtront dans l'application iCal.</p>";
$MESS["EC_MOBILE_SYNC_TITLE_ANDROID"] = "Paramètres de synchronisation Android";
$MESS["EC_MOBILE_SYNC_TITLE_IPHONE"] = "Paramètres de synchronisation Apple iPhone/iPad";
$MESS["EC_MOBILE_SYNC_TITLE_MACOSX"] = "Paramètres de synchronisation Mac OSX";
$MESS["EC_MONTH_SHORT"] = "<small>#MONTH#</small> #DATE#";
$MESS["EC_MONTH_WEEK_TITLE"] = "<span>#DAY_OF_WEEK#</span>";
$MESS["EC_NEW_CONNECTION_NAME"] = "Nouvelle connexion CalDav";
$MESS["EC_NOTIFY_STATUS_LABEL"] = "Notifier quand des participants confirment ou refusent une invitation";
$MESS["EC_NO_COUNTERS"] = "Il n'y a aucun évènement urgent.";
$MESS["EC_NO_EVENTS"] = "Aucun évènement";
$MESS["EC_REINVITE_LABEL"] = "Demander une nouvelle confirmation de participation";
$MESS["EC_REMIND"] = "Rappeler";
$MESS["EC_REMIND1_0"] = "Quand l'évènement commence";
$MESS["EC_REMIND1_5"] = "5 minutes avant l'évènement";
$MESS["EC_REMIND1_10"] = "10 minutes avant l'évènement";
$MESS["EC_REMIND1_15"] = "15 minutes avant l'évènement";
$MESS["EC_REMIND1_20"] = "20 minutes avant l'évènement";
$MESS["EC_REMIND1_30"] = "30 minutes avant l'évènement";
$MESS["EC_REMIND1_60"] = "1 heure avant l'évènement";
$MESS["EC_REMIND1_120"] = "2 heures avant l'évènement";
$MESS["EC_REMIND1_1440"] = "Un jour avant l'évènement";
$MESS["EC_REMIND1_2880"] = "Deux jours avant l'évènement";
$MESS["EC_REMIND1_CUSTOM"] = "Personnalisé";
$MESS["EC_REMIND1_DAY_0"] = "Le jour de l'évènement (#TIME#)";
$MESS["EC_REMIND1_DAY_1"] = "Le jour avant l'évènement (#TIME#)";
$MESS["EC_REMIND1_DAY_2"] = "Deux jours avant l'évènement (#TIME#)";
$MESS["EC_REMIND1_DAY_COUNT"] = "#COUNT# jour(s) avant";
$MESS["EC_REMIND1_HOUR_COUNT"] = "#COUNT# heure(s) avant";
$MESS["EC_REMIND1_MIN_COUNT"] = "#COUNT# minute(s) avant";
$MESS["EC_REMIND1_NO"] = "Aucun";
$MESS["EC_REMIND1_SHORT_0"] = "Quand l'évènement commence";
$MESS["EC_REMIND1_SHORT_5"] = "5 minutes avant";
$MESS["EC_REMIND1_SHORT_10"] = "10 minutes avant";
$MESS["EC_REMIND1_SHORT_15"] = "15 minutes avant";
$MESS["EC_REMIND1_SHORT_20"] = "20 minutes avant";
$MESS["EC_REMIND1_SHORT_30"] = "30 minutes avant";
$MESS["EC_REMIND1_SHORT_60"] = "1 heure avant";
$MESS["EC_REMIND1_SHORT_120"] = "2 heures avant";
$MESS["EC_REMIND1_SHORT_1440"] = "Un jour avant";
$MESS["EC_REMIND1_SHORT_2880"] = "Deux jours avant";
$MESS["EC_REMIND_LABEL"] = "Rappel";
$MESS["EC_REPEAT"] = "Répéter";
$MESS["EC_REQUEST_APP_FAILURE"] = "Erreur lors du chargement ou de l'exécution de l'application <strong>\"#APPNAME#\"</strong>";
$MESS["EC_REQUEST_APP_NONAME_TAB"] = "- sans titre -";
$MESS["EC_RESERVE"] = "Réserver";
$MESS["EC_RESERVE_PERIOD_WARN"] = "Les évènements récurrents ne peuvent réserver de salle de réunion.";
$MESS["EC_ROOM_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer la salle de réunion ?";
$MESS["EC_SAVE_ENTRY_CONFIRM"] = "Les données non enregistrées seront perdues.";
$MESS["EC_SEARCH_RESET_RESULT"] = "réinitialiser";
$MESS["EC_SEARCH_RESULT"] = "Résultats de la recherche";
$MESS["EC_SECTION_BUTTON"] = "Calendriers";
$MESS["EC_SECTION_ROOMS"] = "Salles de réunion";
$MESS["EC_SECTION_ROOMS_LIST"] = "Salles de réunion";
$MESS["EC_SEC_CONNECT_TO_OUTLOOK"] = "Connexion à Microsoft Outlook";
$MESS["EC_SEC_DELETE"] = "Supprimer";
$MESS["EC_SEC_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le calendrier et tous les évènements ?";
$MESS["EC_SEC_EDIT"] = "Éditer";
$MESS["EC_SEC_EDIT_CONFIRM"] = "Les modifications n'ont pas été enregistrées. Voulez-vous vraiment continuer ?";
$MESS["EC_SEC_GROUP_TASK_DEFAULT"] = "Tâches de groupe de travail";
$MESS["EC_SEC_HIDE"] = "Supprimer de la liste";
$MESS["EC_SEC_LEAVE_ONE"] = "Afficher uniquement cet article";
$MESS["EC_SEC_LEAVE_ONE_ROOM"] = "Afficher uniquement cette pièce";
$MESS["EC_SEC_MY_TASK_DEFAULT"] = "Mes tâches";
$MESS["EC_SEC_OPEN_LINK"] = "Ouvrir le calendrier";
$MESS["EC_SEC_SLIDER_ACCESS"] = "droits d'accès";
$MESS["EC_SEC_SLIDER_ACCESS_ADD"] = "ajouter";
$MESS["EC_SEC_SLIDER_ADJUST_SYNC"] = "Configurer les paramètres de synchronisation";
$MESS["EC_SEC_SLIDER_CANCEL"] = "Annuler";
$MESS["EC_SEC_SLIDER_CHANGE"] = "modifier";
$MESS["EC_SEC_SLIDER_CLOSE"] = "Fermer";
$MESS["EC_SEC_SLIDER_COLOR"] = "Couleur";
$MESS["EC_SEC_SLIDER_CREATE_CHAT_LINK"] = "Chat avec les participants";
$MESS["EC_SEC_SLIDER_EDIT_SECTION"] = "Éditer le calendrier";
$MESS["EC_SEC_SLIDER_EDIT_SECTION_PERSONAL"] = "Personnalisation personnelle";
$MESS["EC_SEC_SLIDER_EDIT_SECTION_ROOM"] = "Modifier la salle de réunion";
$MESS["EC_SEC_SLIDER_GROUP_CALENDARS_LIST"] = "Calendriers de groupe de travail";
$MESS["EC_SEC_SLIDER_MY_CALENDARS_LIST"] = "Mes calendriers";
$MESS["EC_SEC_SLIDER_NEW_ROOM"] = "Nouvelle salle de réunion";
$MESS["EC_SEC_SLIDER_NEW_SECTION"] = "Nouveau calendrier";
$MESS["EC_SEC_SLIDER_NO_SECTIONS"] = "Aucun calendrier disponible";
$MESS["EC_SEC_SLIDER_POPUP_EXIST_TITLE"] = "Calendriers disponibles";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_COMP"] = "Calendriers de société";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_GROUP"] = "Calendriers de groupe de travail";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_USER"] = "Calendriers d'utilisateur";
$MESS["EC_SEC_SLIDER_POPUP_MENU_NO_ROOMS"] = "Il n'y a pas de salle de réunion";
$MESS["EC_SEC_SLIDER_POPUP_NEW_MENU"] = "Créer un calendrier";
$MESS["EC_SEC_SLIDER_POPUP_NEW_TITLE"] = "Nouveau";
$MESS["EC_SEC_SLIDER_SAVE"] = "Enregistrer";
$MESS["EC_SEC_SLIDER_SECTION_CAPACITY"] = "Nombre de participants";
$MESS["EC_SEC_SLIDER_SECTION_NECESSITY"] = "Respecter la capacité de la salle de réunion";
$MESS["EC_SEC_SLIDER_SECTION_TITLE"] = "Nom";
$MESS["EC_SEC_SLIDER_SELECT_GROUPS"] = "Ajouter des groupes de travail et des projets";
$MESS["EC_SEC_SLIDER_SELECT_USERS"] = "Ajouter des utilisateurs";
$MESS["EC_SEC_SLIDER_SYNC_DISABLED"] = "Désactivé";
$MESS["EC_SEC_SLIDER_TITLE_COMP_CAL"] = "Société";
$MESS["EC_SEC_SLIDER_TITLE_GROUP_CAL"] = "Groupes de travail et projets";
$MESS["EC_SEC_SLIDER_TITLE_LOCATION_CAL"] = "Emplacements";
$MESS["EC_SEC_SLIDER_TITLE_RESOURCE_CAL"] = "Ressources";
$MESS["EC_SEC_SLIDER_TYPE_ARCHIVE"] = "Calendriers de Google archive";
$MESS["EC_SEC_SLIDER_TYPE_ARCHIVE_HELPER"] = "Calendriers Google précédemment déconnectés. Ces calendriers ne sont pas synchronisés avec votre compte Google.";
$MESS["EC_SEC_SLIDER_TYPE_CALDAV"] = "Calendriers #CONNECTION_NAME# CalDav";
$MESS["EC_SEC_SLIDER_TYPE_CALENDARS_LIST"] = "Calendriers de société";
$MESS["EC_SEC_SLIDER_TYPE_DEFAULT"] = "Calendriers externes";
$MESS["EC_SEC_SLIDER_TYPE_GOOGLE"] = "Google : #CONNECTION_NAME#";
$MESS["EC_SEC_SLIDER_TYPE_GOOGLE_DIS"] = "Calendriers Google";
$MESS["EC_SEC_SLIDER_TYPE_ICLOUD"] = "iCloud : #CONNECTION_NAME#";
$MESS["EC_SEC_SLIDER_TYPE_ICLOUD_DIS"] = "Calendriers iCloud";
$MESS["EC_SEC_SLIDER_TYPE_LOCATION_LIST"] = "Calendriers des localisations";
$MESS["EC_SEC_SLIDER_TYPE_OFFICE365"] = "Office365 : #CONNECTION_NAME#";
$MESS["EC_SEC_SLIDER_TYPE_OFFICE365_DIS"] = "Calendriers Office365";
$MESS["EC_SEC_SLIDER_TYPE_RESOURCE_LIST"] = "Calendriers des ressources";
$MESS["EC_SEC_SLIDER_TYPE_ROOM_LIST"] = "Salles de réunion";
$MESS["EC_SEC_TASK_HIDE"] = "Masquer le calendrier de la tâche";
$MESS["EC_SEC_USER_TASK_DEFAULT"] = "Tâches d'utilisateur";
$MESS["EC_SET_SLIDER_PESONAL_SETTINGS_TITLE"] = "Préférences personnelles";
$MESS["EC_SET_SLIDER_SETTINGS_TITLE"] = "Paramètres";
$MESS["EC_SHOW_ALL"] = "Tout afficher";
$MESS["EC_SIMPLE_FORM_ADD"] = "ajouter";
$MESS["EC_SIMPLE_FORM_BACK"] = "retour";
$MESS["EC_SOCNET_DESTINATION_3"] = "À tous les employés";
$MESS["EC_SOCNET_DESTINATION_4"] = "À tous les utilisateurs";
$MESS["EC_TASK"] = "Tâche";
$MESS["EC_TILL_TIME"] = "jusqu'à #TIME#";
$MESS["EC_TIME_FROM_PLACEHOLDER"] = "Heure de début";
$MESS["EC_TIME_TO_PLACEHOLDER"] = "Heure de fin";
$MESS["EC_TODAY"] = "Aujourd'hui";
$MESS["EC_VIEW"] = "Ouvrir";
$MESS["EC_VIEW_DAY"] = "Jour";
$MESS["EC_VIEW_DESIDE_BUT_I"] = "Peut-être";
$MESS["EC_VIEW_DESIDE_BUT_N"] = "Refuser";
$MESS["EC_VIEW_DESIDE_BUT_OWNER_N"] = "Ne pas participer";
$MESS["EC_VIEW_DESIDE_BUT_Y"] = "Participer";
$MESS["EC_VIEW_LIST"] = "Agenda";
$MESS["EC_VIEW_MODE_SHOW_BY"] = "Trier par";
$MESS["EC_VIEW_MONTH"] = "Mois";
$MESS["EC_VIEW_STATUS_BUT_H"] = "Participer (organisateur)";
$MESS["EC_VIEW_STATUS_BUT_I"] = "Indécis";
$MESS["EC_VIEW_STATUS_BUT_N"] = "Ne participera pas";
$MESS["EC_VIEW_STATUS_BUT_Y"] = "Participe";
$MESS["EC_VIEW_WEEK"] = "Semaine";
$MESS["EC_VIEW_YEAR"] = "Année";
$MESS["EC_WEEK_TITLE"] = "<span class='calendar-day-of-week'>#DAY_OF_WEEK#</span><span class='calendar-num-day'>#DATE#</span>";
