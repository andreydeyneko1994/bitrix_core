<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "Le calendrier a bien été synchronisé avec Google Calendar.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "Le calendrier a bien été synchronisé avec iCloud Calendar.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "Le calendrier a bien été synchronisé avec Office365 Calendar.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Erreur lors de l'activation de la synchronisation du calendrier Google.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Erreur lors de l'activation de la synchronisation du calendrier iCloud.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Erreur lors de l'activation de la synchronisation du calendrier Office365.";
