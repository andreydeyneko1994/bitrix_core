<?php
$MESS["EC_SYNCAJAX_DAV_REQUIRED"] = "Le module DAV n'est pas disponible";
$MESS["EC_SYNCAJAX_GOOGLE_API_REQUIRED"] = "L'API Google n'est pas disponible";
$MESS["EC_SYNCALAX_ICLOUD_WRONG_AUTH"] = "Informations d'authentification fournies incorrectes";
