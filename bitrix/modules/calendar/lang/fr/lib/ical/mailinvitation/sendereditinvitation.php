<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Les participants à l'évènement ont changé";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "L'heure de l'évènement a été modifiée";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "Le lieu de l'évènement a été modifié";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "Le sujet de la réunion a été modifié";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Les règles de récurrence de l'évènement ont été modifiées";
$MESS["EC_CALENDAR_ICAL_MAIL_HIDE_GUESTS_INFORMATION"] = "Le créateur de l'évènement a choisi de masquer les participants";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "L'évènement a été modifié.";
