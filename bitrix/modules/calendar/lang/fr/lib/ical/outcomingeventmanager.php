<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Les participants à l'évènement ont changé";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "L'heure de l'évènement a été modifiée";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "Le lieu de l'évènement a été modifié";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "Le sujet de l'évènement a été modifié";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Les règles de récurrence de l'évènement ont été modifiée";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_CANCEL"] = "Évènement annulé";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "Évènement modifié";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_ACCEPTED"] = "Invitation acceptée";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_DECLINED"] = "Invitation rejetée";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REQUEST"] = "Invitation";
