<?php
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "Синхронизация с календарем iCloud успешно выполнена";
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "Синхронизация с Google календарем успешно выполнена";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "Синхронизация с календарем Office365 успешно выполнена";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "При подключении синхронизации с календарем iCloud произошла ошибка";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "При подключении синхронизации с календарем Google произошла ошибка";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "При подключении синхронизации с календарем Office365 произошла ошибка";