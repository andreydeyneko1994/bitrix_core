<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Los asistentes al evento cambiaron";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "La hora del evento cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "La ubicación del evento cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "El tema de la reunión cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Las reglas recurrentes del evento cambiaron";
$MESS["EC_CALENDAR_ICAL_MAIL_HIDE_GUESTS_INFORMATION"] = "El creador del evento eligió ocultar a los asistentes";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "El evento cambió";
