<?php
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_ATTENDEES"] = "Los asistentes al evento cambiaron";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_DATE"] = "La hora del evento cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_LOCATION"] = "La ubicación del evento cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_NAME"] = "El tema del evento cambió";
$MESS["EC_CALENDAR_ICAL_MAIL_CHANGE_FIELD_TITLE_RRULE"] = "Las reglas recurrentes del evento cambiaron";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_CANCEL"] = "Evento cancelado";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_EDIT"] = "Evento cambiado";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_ACCEPTED"] = "Invitación aceptada";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REPLY_DECLINED"] = "Invitación declinada";
$MESS["EC_CALENDAR_ICAL_MAIL_METHOD_REQUEST"] = "Invitación";
