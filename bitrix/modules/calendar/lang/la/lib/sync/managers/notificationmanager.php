<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "El calendario se sincronizó correctamente con el calendario de Google.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "El calendario se sincronizó correctamente con el calendario de iCloud.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "El calendario se sincronizó correctamente con el calendario de Office365.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Error al habilitar la sincronización del calendario de Google.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Error al habilitar la sincronización del calendario de iCloud.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Error al habilitar la sincronización del calendario de Office365.";
