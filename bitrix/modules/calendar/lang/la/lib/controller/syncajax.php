<?php
$MESS["EC_SYNCAJAX_DAV_REQUIRED"] = "El módulo DAV no está disponible";
$MESS["EC_SYNCAJAX_GOOGLE_API_REQUIRED"] = "La API de Google no está disponible";
$MESS["EC_SYNCALAX_ICLOUD_WRONG_AUTH"] = "La información de autenticación que se proporcionó es incorrecta";
