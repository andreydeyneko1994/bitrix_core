<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "Calendar has been synchronized with Google Calendar successfully.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "Calendar has been synchronized with iCloud Calendar successfully.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "Calendar has been synchronized with Office365 Calendar successfully.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Error enabling the Google calendar synchronization.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Error enabling the iCloud calendar synchronization.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Error enabling the Office365 calendar synchronization.";
