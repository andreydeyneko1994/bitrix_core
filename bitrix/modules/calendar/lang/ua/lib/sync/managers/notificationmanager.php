<?php
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "Синхронізація з Google Календарем успішно виконана.";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "Синхронізація з календарем iCloud успішно виконана.";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "Синхронізація з календарем Office365 успішно виконана.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "Під час увімкнення синхронізації з Google Календарем сталася помилка.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "Під час увімкнення синхронізації з календарем iCloud сталася помилка.";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "Під час увімкнення синхронізації з календарем Office365 сталася помилка.";
