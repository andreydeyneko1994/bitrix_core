<?php
$MESS["CALENDAR_UPDATE_EVENT_WITH_LOCATION"] = "Konwersja wydarzeń";
$MESS["CALENDAR_UPDATE_STRUCTURE_TITLE"] = "Optymalizacja struktury";
$MESS["EC_CALENDAR_INDEX"] = "Indeksuj kalendarz wydarzeń";
$MESS["EC_CALENDAR_NOT_PERMISSIONS_TO_VIEW_GRID_CONTENT"] = "Aby uzyskać pomoc, skontaktuj się z administratorem Bitrix24";
$MESS["EC_CALENDAR_NOT_PERMISSIONS_TO_VIEW_GRID_TITLE"] = "Dostęp do Kalendarza został ograniczony przez administratora Bitrix24";
$MESS["EC_CALENDAR_SPOTLIGHT_LIST"] = "Wygodnie przełączaj między różnymi widokami kalendarza. Wypróbuj nasz nowy widok Harmonogram stworzony dla zapracowanych profesjonalistów, którzy potrzebują widoku listy wszystkich umówionych spotkań.";
$MESS["EC_CALENDAR_SPOTLIGHT_SYNC"] = "Synchronizuj swoje kalendarze automatycznie z innymi usługami i urządzeniami mobilnymi. Synchronizacja działa w obie strony.";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "Nie można wyświetlić kalendarza grupy, poniważ ID grupy nie jest określone.";
$MESS["EC_GROUP_NOT_FOUND"] = "Nie znaleziono grupy";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł \"Bloki Informacji\" nie został zainstalowany.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Moduł \"Intranet Portal\" nie został zainstalowany.";
$MESS["EC_USER_ID_NOT_FOUND"] = "Nie można wyświetlić kalendarza użytkownika, ponieważ ID użytkownika nie jest określone.";
$MESS["EC_USER_NOT_FOUND"] = "Nie znaleziono użytkownika.";
