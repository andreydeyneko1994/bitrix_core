<?php
$MESS["EC_ACCESSIBILITY"] = "Disponibilidade do usuário";
$MESS["EC_ACCESSIBILITY_A"] = "Ausente";
$MESS["EC_ACCESSIBILITY_ABSENT"] = "Ausente";
$MESS["EC_ACCESSIBILITY_B"] = "Ocupado";
$MESS["EC_ACCESSIBILITY_BUSY"] = "Ocupado";
$MESS["EC_ACCESSIBILITY_F"] = "Disponível";
$MESS["EC_ACCESSIBILITY_FREE"] = "Disponível";
$MESS["EC_ACCESSIBILITY_Q"] = "Incerto";
$MESS["EC_ACCESSIBILITY_QUEST"] = "Incerto";
$MESS["EC_ACCESSIBILITY_S"] = "Disponibilidade";
$MESS["EC_ACCESSIBILITY_TITLE"] = "Minha disponibilidade para o evento";
$MESS["EC_CRM_TITLE"] = "itens de CRM";
$MESS["EC_EDDIV_SPECIAL_NOTES"] = "Notas especiais";
$MESS["EC_PRIVATE_EVENT"] = "Evento Privado";
$MESS["EC_VIEW_ATTENDEES_ADD"] = "convidar outras pessoas";
$MESS["EC_VIEW_ATTENDEES_TITLE"] = "Participantes";
$MESS["EC_VIEW_CREATED_BY"] = "Criado por";
$MESS["EC_VIEW_HOST"] = "Realizado por";
$MESS["EC_VIEW_REMINDERS"] = "Lembrete";
$MESS["EC_VIEW_REMINDER_ADD"] = "Lembrar";
$MESS["EC_VIEW_SECTION"] = "Calendário";
$MESS["EC_VIEW_SLIDER_ATTENDEES"] = "Participantes";
$MESS["EC_VIEW_SLIDER_CANCEL_BUTTON"] = "Cancelar";
$MESS["EC_VIEW_SLIDER_COMMENTS"] = "Comentários";
$MESS["EC_VIEW_SLIDER_COPY_LINK"] = "Copiar link do evento na Área de Transferência";
$MESS["EC_VIEW_SLIDER_DEL"] = "Excluir";
$MESS["EC_VIEW_SLIDER_EDIT"] = "Editar";
$MESS["EC_VIEW_SLIDER_EVENT_NOT_FOUND"] = "O evento não foi encontrado ou não pode ser exibido.";
$MESS["EC_VIEW_SLIDER_IMPORTANT_EVENT"] = "Este evento é importante";
$MESS["EC_VIEW_SLIDER_LOCATION"] = "Localização";
$MESS["EC_VIEW_SLIDER_SAVE_EVENT_BUTTON"] = "Salvar";
$MESS["EC_VIEW_STATUS_TITLE_I"] = "incerto";
$MESS["EC_VIEW_STATUS_TITLE_N"] = "declinado";
$MESS["EC_VIEW_STATUS_TITLE_Q"] = "Convidado";
$MESS["EC_VIEW_STATUS_TITLE_Y"] = "participante";
