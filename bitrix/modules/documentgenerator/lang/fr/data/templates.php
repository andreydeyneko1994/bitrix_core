<?php
$MESS["DOCGEN_TEMPLATE_ESTIMATE_DE_NAME"] = "Devis (Allemagne)";
$MESS["DOCGEN_TEMPLATE_ESTIMATE_UK_NAME"] = "Devis (Royaume-Uni)";
$MESS["DOCGEN_TEMPLATE_INVOICE_BR_NAME"] = "Facture (Brésil)";
$MESS["DOCGEN_TEMPLATE_INVOICE_DE_NAME"] = "Facture (Allemagne)";
$MESS["DOCGEN_TEMPLATE_INVOICE_FR_NAME"] = "Facture (France)";
$MESS["DOCGEN_TEMPLATE_INVOICE_PL_NAME"] = "Facture (Pologne)";
$MESS["DOCGEN_TEMPLATE_INVOICE_SP_NAME"] = "Facture (Espagne)";
$MESS["DOCGEN_TEMPLATE_INVOICE_UK_NAME"] = "Facture (Royaume-Uni)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_DE_NAME"] = "Feuille d'emballage (Allemagne)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_UK_NAME"] = "Feuille d'emballage (Royaume-Uni)";
$MESS["DOCGEN_TEMPLATE_QUOTE_BR_NAME"] = "Devis (Brésil)";
$MESS["DOCGEN_TEMPLATE_QUOTE_FR_NAME"] = "Devis (France)";
$MESS["DOCGEN_TEMPLATE_QUOTE_MX_NAME"] = "Devis (Mexique)";
$MESS["DOCGEN_TEMPLATE_QUOTE_SP_NAME"] = "Devis (Espagne)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_DE_NAME"] = "Confirmation de commande (Allemagne)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_PL_NAME"] = "Confirmation de commande (Pologne)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_UK_NAME"] = "Confirmation de commande (Royaume-Uni)";
