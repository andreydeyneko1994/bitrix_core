<?php
$MESS["DOCGEN_POPUP_CLOSE_BUTTON"] = "Fermer";
$MESS["DOCGEN_POPUP_CONTINUE_BUTTON"] = "Continuer";
$MESS["DOCGEN_POPUP_DO_USE_OLD_NUMBER"] = "Ce modèle a déjà été utilisé pour créer un de vos documents. Voulez-vous affecter un nouveau numéro à ce document ou utiliser celui du document créé ?";
$MESS["DOCGEN_POPUP_NEW_BUTTON"] = "Créer nouveau";
$MESS["DOCGEN_POPUP_NUMBER_TITLE"] = "Sélectionnez le numéro";
$MESS["DOCGEN_POPUP_OLD_BUTTON"] = "Utiliser précédent";
$MESS["DOCGEN_POPUP_PRINT_IS_NOT_AVAILABLE"] = "Impossible d'imprimer le document";
$MESS["DOCGEN_POPUP_PRINT_TITLE"] = "Imprimer un document";
