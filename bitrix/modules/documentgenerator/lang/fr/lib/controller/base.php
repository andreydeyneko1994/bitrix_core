<?php
$MESS["DOCGEN_CONTROLLER_DOCUMENT_NOT_FOUND_ERROR"] = "Le document est introuvable.";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_NOT_FOUND_ERROR"] = "Le modèle est introuvable.";
