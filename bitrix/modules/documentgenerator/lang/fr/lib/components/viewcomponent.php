<?
$MESS["DOCGEN_DOCUMENT_MODIFY_PERMISSION_ERROR"] = "Vous n'êtes pas autorisé à éditer les documents";
$MESS["DOCGEN_DOCUMENT_NOT_FOUND_ERROR"] = "Le document est introuvable.";
$MESS["DOCGEN_DOCUMENT_VIEW_ACCESS_ERROR"] = "Vous n'êtes pas autorisé à accéder à ce document";
$MESS["DOCGEN_DOCUMENT_VIEW_PERMISSION_ERROR"] = "Vous n'êtes pas autorisé à afficher les documents";
$MESS["DOCGEN_TEMPLATE_NOT_FOUND_ERROR"] = "Le template est introuvable";
$MESS["DOCGEN_UNKNOWN_ERROR"] = "Erreur inconnue";
?>