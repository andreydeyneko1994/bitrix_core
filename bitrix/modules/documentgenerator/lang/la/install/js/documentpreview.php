<?php
$MESS["DOCGEN_POPUP_CLOSE_BUTTON"] = "Cerrar";
$MESS["DOCGEN_POPUP_CONTINUE_BUTTON"] = "Continuar";
$MESS["DOCGEN_POPUP_DO_USE_OLD_NUMBER"] = "Ya tiene un documento creado con esta plantilla. ¿Desea asignar un nuevo número a este documento o utilizar el número del documento ya creado?";
$MESS["DOCGEN_POPUP_NEW_BUTTON"] = "Crear Nuevo";
$MESS["DOCGEN_POPUP_NUMBER_TITLE"] = "Seleccionar Número";
$MESS["DOCGEN_POPUP_OLD_BUTTON"] = "Usar Anterior";
$MESS["DOCGEN_POPUP_PRINT_IS_NOT_AVAILABLE"] = "No se puede imprimir el documento";
$MESS["DOCGEN_POPUP_PRINT_TITLE"] = "Imprimir documento";
