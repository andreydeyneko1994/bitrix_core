<?
$MESS["DOCUMENTGENERATOR_INSTALL_TITLE"] = "Instalación del módulo \"Document Generator\"";
$MESS["DOCUMENTGENERATOR_MODULE_DESCRIPTION"] = "Este módulo creará automáticamente documentos basados en las plantillas.";
$MESS["DOCUMENTGENERATOR_MODULE_NAME"] = "Generador de documentos";
$MESS["DOCUMENTGENERATOR_UNINSTALL_QUESTION"] = "¿usted está seguro que quiere eliminar el módulo?";
$MESS["DOCUMENTGENERATOR_UNINSTALL_TITLE"] = "Desinstalación del módulo \"Document Generator\"";
?>