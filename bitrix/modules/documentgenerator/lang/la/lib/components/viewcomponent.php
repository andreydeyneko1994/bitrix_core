<?
$MESS["DOCGEN_DOCUMENT_MODIFY_PERMISSION_ERROR"] = "No tiene permiso para editar documentos";
$MESS["DOCGEN_DOCUMENT_NOT_FOUND_ERROR"] = "No se encontró el documento.";
$MESS["DOCGEN_DOCUMENT_VIEW_ACCESS_ERROR"] = "No tiene permiso para acceder a este documento";
$MESS["DOCGEN_DOCUMENT_VIEW_PERMISSION_ERROR"] = "No tiene permiso para ver los documentos";
$MESS["DOCGEN_TEMPLATE_NOT_FOUND_ERROR"] = "No se encontró la plantilla";
$MESS["DOCGEN_UNKNOWN_ERROR"] = "Error desconocido";
?>