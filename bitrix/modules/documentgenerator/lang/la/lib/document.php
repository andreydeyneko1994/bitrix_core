<?
$MESS["DOCUMENT_GROUP_NAME"] = "Documento";
$MESS["DOCUMENT_TITLE_FIELD_NAME"] = "Nombre del documento";
$MESS["DOCUMENT_TRANSOFMATION_ERROR"] = "La imagen y el PDF no están disponibles debido a un error de conversión. Por favor, inténtelo de nuevo más tarde.";
$MESS["DOCUMENT_TRANSOFMER_MODULE_ERROR"] = "No se pueden convertir archivos porque el módulo no está instalado.";
?>