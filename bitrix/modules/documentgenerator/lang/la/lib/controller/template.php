<?
$MESS["DOCGEN_CONTROLLER_MODULE_INVALID"] = "No se puede utilizar el módulo #MODULE#";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_DOWNLOAD_ERROR"] = "No se puede descargar el archivo de la plantilla";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_FILE_PREFIX"] = "Plantilla";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_NOT_FOUND"] = "La plantilla no fue encontrada";
?>