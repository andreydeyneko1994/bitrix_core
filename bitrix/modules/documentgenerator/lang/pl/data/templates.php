<?php
$MESS["DOCGEN_TEMPLATE_ESTIMATE_DE_NAME"] = "Oferta (Niemcy)";
$MESS["DOCGEN_TEMPLATE_ESTIMATE_UK_NAME"] = "Oferta (Wielka Brytania)";
$MESS["DOCGEN_TEMPLATE_INVOICE_BR_NAME"] = "Faktura (Brazylia)";
$MESS["DOCGEN_TEMPLATE_INVOICE_DE_NAME"] = "Faktura (Niemcy)";
$MESS["DOCGEN_TEMPLATE_INVOICE_FR_NAME"] = "Faktura (Francja)";
$MESS["DOCGEN_TEMPLATE_INVOICE_PL_NAME"] = "Faktura (Polska)";
$MESS["DOCGEN_TEMPLATE_INVOICE_SP_NAME"] = "Faktura (Hiszpania)";
$MESS["DOCGEN_TEMPLATE_INVOICE_UK_NAME"] = "Faktura (Wielka Brytania)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_DE_NAME"] = "Arkusz pakunkowy (Niemcy)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_UK_NAME"] = "Arkusz pakunkowy (Wielka Brytania)";
$MESS["DOCGEN_TEMPLATE_QUOTE_BR_NAME"] = "Oferta (Brazylia)";
$MESS["DOCGEN_TEMPLATE_QUOTE_FR_NAME"] = "Oferta (Francja)";
$MESS["DOCGEN_TEMPLATE_QUOTE_MX_NAME"] = "Oferta (Meksyk)";
$MESS["DOCGEN_TEMPLATE_QUOTE_SP_NAME"] = "Oferta (Hiszpania)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_DE_NAME"] = "Potwierdzenie zamówienia (Niemcy)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_PL_NAME"] = "Potwierdzenie zamówienia (Polska)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_UK_NAME"] = "Potwierdzenie zamówienia (Wielka Brytania)";
