<?php
$MESS["DOCGEN_POPUP_CLOSE_BUTTON"] = "Zamknij";
$MESS["DOCGEN_POPUP_CONTINUE_BUTTON"] = "Kontynuuj";
$MESS["DOCGEN_POPUP_DO_USE_OLD_NUMBER"] = "Masz już dokument utworzony za pomocą tego szablonu. Czy chcesz przypisać nowy numer do tego dokumentu, czy też użyć numeru już utworzonego dokumentu?";
$MESS["DOCGEN_POPUP_NEW_BUTTON"] = "Utwórz nowy";
$MESS["DOCGEN_POPUP_NUMBER_TITLE"] = "Wybierz numer";
$MESS["DOCGEN_POPUP_OLD_BUTTON"] = "Użyj poprzedniego";
$MESS["DOCGEN_POPUP_PRINT_IS_NOT_AVAILABLE"] = "Nie można wydrukować dokumentu";
$MESS["DOCGEN_POPUP_PRINT_TITLE"] = "Drukuj dokument";
