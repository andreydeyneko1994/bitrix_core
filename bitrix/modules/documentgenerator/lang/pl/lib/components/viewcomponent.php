<?
$MESS["DOCGEN_DOCUMENT_MODIFY_PERMISSION_ERROR"] = "Nie masz uprawnienia do edycji dokumentów";
$MESS["DOCGEN_DOCUMENT_NOT_FOUND_ERROR"] = "Nie znaleziono dokumentu.";
$MESS["DOCGEN_DOCUMENT_VIEW_ACCESS_ERROR"] = "Nie masz uprawnienia dostępu do tego dokumentu";
$MESS["DOCGEN_DOCUMENT_VIEW_PERMISSION_ERROR"] = "Nie masz uprawnienia do wyświetlania dokumentów";
$MESS["DOCGEN_TEMPLATE_NOT_FOUND_ERROR"] = "Nie znaleziono szablonu";
$MESS["DOCGEN_UNKNOWN_ERROR"] = "Nieznany błąd";
?>