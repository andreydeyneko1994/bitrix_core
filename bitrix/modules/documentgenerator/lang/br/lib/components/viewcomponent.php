<?
$MESS["DOCGEN_DOCUMENT_MODIFY_PERMISSION_ERROR"] = "Você não tem permissão para editar documentos";
$MESS["DOCGEN_DOCUMENT_NOT_FOUND_ERROR"] = "O documento não foi encontrado.";
$MESS["DOCGEN_DOCUMENT_VIEW_ACCESS_ERROR"] = "Você não tem permissão para acessar este documento";
$MESS["DOCGEN_DOCUMENT_VIEW_PERMISSION_ERROR"] = "Você não tem permissão para visualizar documentos";
$MESS["DOCGEN_TEMPLATE_NOT_FOUND_ERROR"] = "O modelo não foi encontrado";
$MESS["DOCGEN_UNKNOWN_ERROR"] = "Erro desconhecido";
?>