<?php
$MESS["DOCGEN_POPUP_CLOSE_BUTTON"] = "Fechar";
$MESS["DOCGEN_POPUP_CONTINUE_BUTTON"] = "Continuar";
$MESS["DOCGEN_POPUP_DO_USE_OLD_NUMBER"] = "Você já tem um documento criado com este modelo. Deseja atribuir um novo número a este documento ou usar o número do documento já criado?";
$MESS["DOCGEN_POPUP_NEW_BUTTON"] = "Criar Novo";
$MESS["DOCGEN_POPUP_NUMBER_TITLE"] = "Selecionar Número";
$MESS["DOCGEN_POPUP_OLD_BUTTON"] = "Usar Anterior";
$MESS["DOCGEN_POPUP_PRINT_IS_NOT_AVAILABLE"] = "Não é possível imprimir o documento";
$MESS["DOCGEN_POPUP_PRINT_TITLE"] = "Imprimir documento";
