<?php
$MESS["DOCGEN_TEMPLATE_ESTIMATE_DE_NAME"] = "Cotação (Alemanha)";
$MESS["DOCGEN_TEMPLATE_ESTIMATE_UK_NAME"] = "Cotação (Reino Unido)";
$MESS["DOCGEN_TEMPLATE_INVOICE_BR_NAME"] = "Fatura (Brasil)";
$MESS["DOCGEN_TEMPLATE_INVOICE_DE_NAME"] = "Fatura (Alemanha)";
$MESS["DOCGEN_TEMPLATE_INVOICE_FR_NAME"] = "Fatura (França)";
$MESS["DOCGEN_TEMPLATE_INVOICE_PL_NAME"] = "Fatura (Polônia)";
$MESS["DOCGEN_TEMPLATE_INVOICE_SP_NAME"] = "Fatura (Espanha)";
$MESS["DOCGEN_TEMPLATE_INVOICE_UK_NAME"] = "Fatura (Reino Unido)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_DE_NAME"] = "Folha de embalagem (Alemanha)";
$MESS["DOCGEN_TEMPLATE_PACKING_SLIP_UK_NAME"] = "Folha de embalagem (Reino Unido)";
$MESS["DOCGEN_TEMPLATE_QUOTE_BR_NAME"] = "Cotação (Brasil)";
$MESS["DOCGEN_TEMPLATE_QUOTE_FR_NAME"] = "Cotação (França)";
$MESS["DOCGEN_TEMPLATE_QUOTE_MX_NAME"] = "Cotação (México)";
$MESS["DOCGEN_TEMPLATE_QUOTE_SP_NAME"] = "Cotação (Espanha)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_DE_NAME"] = "Confirmação do pedido (Alemanha)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_PL_NAME"] = "Confirmação do pedido (Polônia)";
$MESS["DOCGEN_TEMPLATE_SALES_ORDER_UK_NAME"] = "Confirmação do pedido (Reino Unido)";
