<?
$MESS["DOCGEN_PLACEHOLDERS_COPY_PLACEHOLDER"] = "O código foi copiado para a Área de Transferência";
$MESS["DOCGEN_PLACEHOLDERS_COPY_PLACEHOLDER_FAIL"] = "Não é possível copiar o código. Por favor, copie-o manualmente.";
$MESS["DOCGEN_PLACEHOLDERS_FIELD_EMPTY"] = "Não selecionado";
?>