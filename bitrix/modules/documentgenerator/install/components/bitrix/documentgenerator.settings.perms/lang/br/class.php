<?
$MESS["DOCGEN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Criar uma nova função";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "A função não foi encontrada.";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Editar função #ROLE#";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_PANEL"] = "Permissões de funcionário para entidades de CRM estão disponíveis nos planos comerciais.";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TEXT"] = "<div class=\"docgen-permissions-feature-popup\">Todos os funcionários compartilham as mesmas permissões no seu plano atual. Considere fazer um upgrade para um dos planos comerciais para atribuir diferentes funções, ações e dados a vários usuários.<br /><br /> Para saber mais sobre os diferentes planos, por favor, acesse a <a target=\"_blank\" href=\"/settings/license_all.php\">página de comparação de planos</a>.</div>";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TITLE"] = "Atribuir permissões de acesso";
$MESS["DOCGEN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "Você não tem permissão para editar permissões de acesso";
$MESS["DOCGEN_SETTINGS_PERMS_TITLE"] = "Configurar permissões de acesso";
$MESS["DOCGEN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(ID de acesso desconhecido)";
?>