<?
$MESS["DOCGEN_REGION_ADD_REGION_TITLE"] = "Ajouter un pays";
$MESS["DOCGEN_REGION_EDIT_REGION_TITLE"] = "Éditer le pays #TITLE#";
$MESS["DOCGEN_REGION_PERMISSIONS_ERROR"] = "Vous ne disposez pas des autorisations pour gérer ces pays";
?>