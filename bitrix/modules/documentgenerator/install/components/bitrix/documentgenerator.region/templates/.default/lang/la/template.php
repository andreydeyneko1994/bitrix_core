<?
$MESS["DOCGEN_REGION_EDIT_DELETE_CONFIRM"] = "¿Desea eliminar este país?";
$MESS["DOCGEN_REGION_EDIT_EMPTY"] = "No seleccionado";
$MESS["DOCGEN_REGION_EDIT_ERROR_TITLE_EMPTY"] = "Falta el campo \"Nombre\"";
$MESS["DOCGEN_REGION_EDIT_FORMAT_DATE"] = "Formato de fecha";
$MESS["DOCGEN_REGION_EDIT_FORMAT_NAME"] = "Formato de nombre";
$MESS["DOCGEN_REGION_EDIT_FORMAT_TIME"] = "Formato de hora";
$MESS["DOCGEN_REGION_EDIT_LANGUAGE"] = "Utilizar la configuración del país";
$MESS["DOCGEN_REGION_EDIT_PHRASES_TITLE"] = "Mensajes dependientes del idioma";
$MESS["DOCGEN_REGION_EDIT_TITLE"] = "Nombre";
?>