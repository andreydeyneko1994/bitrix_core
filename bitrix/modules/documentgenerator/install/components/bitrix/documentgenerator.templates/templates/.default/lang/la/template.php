<?
$MESS["DOCGEN_TEMPLATE_LIST_DELETE_CONFIRM"] = "¿Usted está seguro que quiere eliminar esta plantilla?";
$MESS["DOCGEN_TEMPLATE_LIST_FEEDBACK"] = "Feedback";
$MESS["DOCGEN_TEMPLATE_LIST_MORE"] = "más";
$MESS["DOCGEN_TEMPLATE_LIST_MORE_INFO"] = "Aprenda a configurar y cargar plantillas de documentos personalizados";
$MESS["DOCGEN_TEMPLATE_LIST_UPLOAD"] = "Cargar";
?>