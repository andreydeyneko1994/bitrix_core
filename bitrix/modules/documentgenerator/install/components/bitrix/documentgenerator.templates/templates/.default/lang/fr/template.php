<?
$MESS["DOCGEN_TEMPLATE_LIST_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer ce modèle ?";
$MESS["DOCGEN_TEMPLATE_LIST_FEEDBACK"] = "Commentaire";
$MESS["DOCGEN_TEMPLATE_LIST_MORE"] = "plus";
$MESS["DOCGEN_TEMPLATE_LIST_MORE_INFO"] = "Découvrez comment configurer et télécharger des modèles de document personnalisés";
$MESS["DOCGEN_TEMPLATE_LIST_UPLOAD"] = "Télécharger";
?>