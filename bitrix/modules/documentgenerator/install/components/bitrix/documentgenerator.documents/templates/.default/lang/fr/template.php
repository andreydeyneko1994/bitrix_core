<?
$MESS["DOCGEN_DOCUMENTS_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer ce document ?";
$MESS["DOCGEN_DOCUMENTS_DELETE_ERROR"] = "Erreur lors de la suppression du document : ";
$MESS["DOCGEN_DOCUMENTS_DELETE_SUCCESS"] = "Le document a été supprimé";
?>