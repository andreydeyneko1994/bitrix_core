<?
$MESS["DOCGEN_DOCUMENTS_DELETE_CONFIRM"] = "¿Estás seguro de que quieres eliminar este documento?";
$MESS["DOCGEN_DOCUMENTS_DELETE_ERROR"] = "Error al eliminar el documento:";
$MESS["DOCGEN_DOCUMENTS_DELETE_SUCCESS"] = "El documento ha sido eliminado";
?>