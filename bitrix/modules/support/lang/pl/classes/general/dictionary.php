<?
$MESS["SUP_CATEGORY"] = "Kategoria";
$MESS["SUP_CRITICALITY"] = "Priorytet";
$MESS["SUP_DIFFICULTY"] = "Poziom trudności";
$MESS["SUP_ERROR_ADD_DICTONARY"] = "Błąd zachowania zapisu";
$MESS["SUP_ERROR_UPDATE_DICTONARY"] = "Błąd aktualizacji zapisu";
$MESS["SUP_FUA"] = "Najczęściej wykorzytywane odpowiedzi";
$MESS["SUP_MARK"] = "Ocena odpowiedzi";
$MESS["SUP_SOURCE"] = "Źródło";
$MESS["SUP_STATUS"] = "Status";
$MESS["SUP_UNKNOWN_ID"] = "Zapis #ID# nie istnieje.";
?>