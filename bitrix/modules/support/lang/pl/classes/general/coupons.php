<?
$MESS["SUP_ST_ERROR_NO_NEW_COUPON"] = "Nie można utworzyć kuponu.";
$MESS["SUP_ST_ERROR_NO_UPDATES_ROWS"] = "Nie zaktualizowano żadnych danych.";
$MESS["SUP_ST_ERROR_NO_UPDATE_DATA"] = "Brak danych do aktualizacji.";
$MESS["SUP_ST_ERR_COUNT_TICKETS"] = "Kupon musi zostać użyty przynajmniej raz.";
$MESS["SUP_ST_ERR_DATE_INTERVAL"] = "Kupon musi być aktywny przynajmniej jeden dzień.";
?>