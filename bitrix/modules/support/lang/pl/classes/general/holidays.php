<?
$MESS["SUP_DATE_FROM"] = "Aktywne od";
$MESS["SUP_DATE_TILL"] = "przez";
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["SUP_ERROR_DB_ERROR"] = "Błąd bazy danych.";
$MESS["SUP_ERROR_EMPTY_DATE"] = "Data wyjątku jest wymagana.";
$MESS["SUP_ERROR_EMPTY_NAME"] = "Nazwa wyjątku jest wymagana.";
$MESS["SUP_ERROR_EMPTY_OPEN_TIME"] = "Działanie wyjątku jest wymagane.";
?>