<?
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "ID grupy jest puste";
$MESS["SUP_ERROR_NO_GROUP"] = "Nie ma takiej grupy";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "Ten użytkownik nie jest członkiem zespołu wsparcia technicznego.";
$MESS["SUP_ERROR_NO_USER"] = "Nie ma takiego użytkownika";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "Ten użytkownik należy już do tej grupy";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "ID użytkownika jest puste";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "Tylko klienci wsparcia technicznego mogą być dodani do tej grupy";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "Tylko członkowie zespołu wsparcia technicznego mogą być dodani do tej grupy.";
?>