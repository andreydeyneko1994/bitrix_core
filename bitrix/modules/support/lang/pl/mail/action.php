<?
$MESS["SUPPORT_MAIL_ADD_TO_CATEGORY"] = "Dodaj nowe zgłoszenie kłopotu do kategorii:";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_TICKET"] = "Dodaj nową wiadomość w już otwartym zgłoszeniu kłopotu:";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_ANY"] = "od każdego adresu (tylko temat jest sprawdzany)";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_DOMAIN"] = "od każdego adresu e-mail właściciela zgłoszenia kłopotu domeny (*@domain.com)";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_EMAIL"] = "tylko od adresu e-mail właściciela zgłoszenia kłopotu user@domain.com";
$MESS["SUPPORT_MAIL_ADD_WITH_CRITICALITY"] = "Ustaw krytykę nowego zgłoszenia kłopotu:";
$MESS["SUPPORT_MAIL_CONNECT_TICKET_WITH_SITE"] = "Połącz nowe zgłoszenie kłopotu do strony:";
$MESS["SUPPORT_MAIL_DEF_REGISTERED"] = "Identyfikuj zarejestrowanego użytkownika po e-mail:";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_N"] = "nie, zawsze twórz anonimowe zgłoszenie kłopotu";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_Y"] = "tak, spróbuj powiązać zgłoszenie kłopotu z użytkownikiem";
$MESS["SUPPORT_MAIL_HIDDEN"] = "jako ukrytą wiadomość (tylko dla członków wsparcia technicznego)";
$MESS["SUPPORT_MAIL_MAILBOX"] = "< ze skrzynki mailowej >";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE"] = "Szablony tematów dla określonych odpowiedzi na zgłoszenie kłopotu:";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE_NOTES"] = "(standardowe wyrażenie, pierwszy nawias zawiera numer zgłoszenia kłopotu)";
?>