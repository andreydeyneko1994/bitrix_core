<?
$MESS["SUPPORT_ERROR_EDITABLE"] = "Twoja wersja nie zapewnia tego modułu.";
$MESS["SUP_ATTENTION"] = "Ostrzeżenie! Moduł zostanie odinstalowany.";
$MESS["SUP_BACK"] = "Powrót";
$MESS["SUP_COMPLETE"] = "Instalacja zakończona.";
$MESS["SUP_DELETE_COMLETE"] = "Dezinstalacja zakończona.";
$MESS["SUP_DEMO_ACCESS"] = "dostęp demo";
$MESS["SUP_DENIED"] = "Odmowa dostępu";
$MESS["SUP_ERRORS"] = "Błędy:";
$MESS["SUP_INSTALL"] = "Instaluj";
$MESS["SUP_INSTALL_TITLE"] = "Instalacj modułu Hepldesk";
$MESS["SUP_LINK"] = "Link";
$MESS["SUP_MODULE_DESCRIPTION"] = "Ten moduł zaopatruje stronę w system Hepldesk.";
$MESS["SUP_MODULE_NAME"] = "Hepldesk";
$MESS["SUP_NO"] = "Nie";
$MESS["SUP_RESET"] = "Wyczyść";
$MESS["SUP_SAVE_TABLES"] = "Zabisz Tabele";
$MESS["SUP_SELECT_INITITAL"] = "Proszę wybrać folder dla plików modułu Hepldesk:";
$MESS["SUP_SITE"] = "Strona";
$MESS["SUP_UNINSTALL_TITLE"] = "Deinstalacja modułu Hepldesk";
$MESS["SUP_URL_PUBLIC"] = "Folder modułu Hepldesk (#SITE_DIR# - site root folder):";
?>