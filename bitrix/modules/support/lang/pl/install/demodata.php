<?
$MESS["SUP_DEF_ANSWER_DOES_NOT_SUIT"] = "Odpowiedź nie pasuje";
$MESS["SUP_DEF_ANSWER_IS_NOT_COMPLETE"] = "Odpowiedź nie jest kompletna";
$MESS["SUP_DEF_ANSWER_SUITS_THE_NEEDS"] = "Odpowiedź odpowiada potrzebom";
$MESS["SUP_DEF_BUGS"] = "Błędy";
$MESS["SUP_DEF_COULD_NOT_BE_SOLVED"] = "Rozwiązanie niemożliwe";
$MESS["SUP_DEF_EASY"] = "Łatwe";
$MESS["SUP_DEF_E_MAIL"] = "E-mail";
$MESS["SUP_DEF_FORUM"] = "Forum";
$MESS["SUP_DEF_HARD"] = "Trudne";
$MESS["SUP_DEF_HIGH"] = "Wysoki";
$MESS["SUP_DEF_LOW"] = "Niski";
$MESS["SUP_DEF_MEDIUM"] = "Średnie";
$MESS["SUP_DEF_MIDDLE"] = "środek";
$MESS["SUP_DEF_ORDER_PAYMENT"] = "Płatność za zamówienie";
$MESS["SUP_DEF_ORDER_SHIPPING"] = "Zgłoszenie wysłane";
$MESS["SUP_DEF_PHONE"] = "Telefon";
$MESS["SUP_DEF_PROBLEM_SOLVING_IN_PROGRESS"] = "Rozwiązanie problemu w trakcie";
$MESS["SUP_DEF_REQUEST_ACCEPTED"] = "Zgłoszenie przyjęte";
$MESS["SUP_DEF_SLA_NAME"] = "domyślne";
$MESS["SUP_DEF_SUCCESSFULLY_SOLVED"] = "Pomyślnie rozwiązane";
?>