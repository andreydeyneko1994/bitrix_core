<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
$MESS["MAIN_ALL"] = "(Wszystko)";
$MESS["SUP_GL_ADD"] = "Dodaj grupę";
$MESS["SUP_GL_DELETE"] = "Usuń";
$MESS["SUP_GL_DELETE_CONFIRMATION"] = "Na pewno chcesz to usunąć?";
$MESS["SUP_GL_EDIT"] = "Edytuj";
$MESS["SUP_GL_EXACT_MATCH"] = "dokładne wyszukiwanie";
$MESS["SUP_GL_FLT_CLIENT"] = "Grupa Klienta";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP"] = "Kategoria";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP_CN"] = "Kategoria:";
$MESS["SUP_GL_FLT_NAME"] = "Nazwa";
$MESS["SUP_GL_FLT_SUPPORT"] = "Grupa zespołu wsparcia";
$MESS["SUP_GL_IS_TEAM_GROUP"] = "Grupa zespołu Wsparcia technicznego";
$MESS["SUP_GL_NAME"] = "Nazwa";
$MESS["SUP_GL_PAGES"] = "Grupy";
$MESS["SUP_GL_SORT"] = "Sortowanie";
$MESS["SUP_GL_TITLE"] = "Grupa klienta wsparcia technicznego";
$MESS["SUP_GL_USERADD"] = "Dodaj użytkownika do grupy";
$MESS["SUP_GL_USERLIST"] = "Użytkownicy w grupie";
$MESS["SUP_GL_XML_ID"] = "XML_ID";
?>