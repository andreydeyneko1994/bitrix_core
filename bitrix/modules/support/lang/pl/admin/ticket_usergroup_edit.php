<?
$MESS["SUP_GE_ERROR"] = "Błąd";
$MESS["SUP_UGE_ADD_MORE_USERS"] = "Dodawanie";
$MESS["SUP_UGE_GROUP"] = "Grupa";
$MESS["SUP_UGE_NO_GROUP"] = "Nie znaleziono grupy";
$MESS["SUP_UGE_TAB1"] = "Użytkownik";
$MESS["SUP_UGE_TAB1_TITLE"] = "Użytkownik w grupie";
$MESS["SUP_UGE_TITLE_ADD"] = "Dodaj użytkownika do grupy";
$MESS["SUP_UGE_USER"] = "Użytkownik";
$MESS["SUP_UGE_USER_CAN_MAIL_GROUP_MESS"] = "Użytkownik może otrzymywać powiadomienia grupy";
$MESS["SUP_UGE_USER_CAN_READ_GROUP_MESS"] = "Użytkownik może przeczytać zgłoszenia tej grupy";
?>