<?
$MESS["SUP_CL_COUPON"] = "Kupon";
$MESS["SUP_CL_COUPON_ID"] = "ID Kuponu";
$MESS["SUP_CL_FIRST_NAME"] = "Nazwa";
$MESS["SUP_CL_FLT_COUPON"] = "Kupon";
$MESS["SUP_CL_FLT_COUPON_ID"] = "ID Kuponu";
$MESS["SUP_CL_GUEST_ID"] = "ID Gościa";
$MESS["SUP_CL_LAST_NAME"] = "Nazwisko";
$MESS["SUP_CL_LOGIN"] = "Login";
$MESS["SUP_CL_PAGES"] = "Wyniki";
$MESS["SUP_CL_SESSION_ID"] = "ID Sesji";
$MESS["SUP_CL_TIMESTAMP_X"] = "Data wykorzytsania";
$MESS["SUP_CL_TITLE"] = "Zapis wykorzystania kuponu";
$MESS["SUP_CL_USER_ID"] = "ID Użytkownika";
?>