<?
$MESS["SUP_UGL_ADD"] = "Dodawanie";
$MESS["SUP_UGL_ADD_TITLE"] = "Dodawanie";
$MESS["SUP_UGL_CAN_VIEW_GROUP_MESSAGES"] = "Użytkownik może wyświetlać zgłoszenia tej grupy";
$MESS["SUP_UGL_DELETE"] = "Usuń";
$MESS["SUP_UGL_DELETE_CONFIRMATION"] = "Na pewno chcesz to usunąć?";
$MESS["SUP_UGL_EDIT"] = "Edytuj";
$MESS["SUP_UGL_EXACT_MATCH"] = "dokładne wyszukiwanie";
$MESS["SUP_UGL_FIRST_NAME"] = "Nazwa";
$MESS["SUP_UGL_FLT_ALL_GROUPS"] = "Wszystkie grupy";
$MESS["SUP_UGL_FLT_CAN_VIEW_GROUP_MESSAGES"] = "Użytkownik może wyświetlać zgłoszenia tej grupy";
$MESS["SUP_UGL_FLT_GROUP"] = "Grupa";
$MESS["SUP_UGL_FLT_IS_TEAM_GROUP"] = "Grupa zespołu Wsparcia technicznego";
$MESS["SUP_UGL_FLT_LOGIN"] = "Login";
$MESS["SUP_UGL_FLT_USER"] = "Użytkownik";
$MESS["SUP_UGL_GROUP_ID"] = "ID grupy";
$MESS["SUP_UGL_GROUP_NAME"] = "Nazwa grupy";
$MESS["SUP_UGL_IS_TEAM_GROUP"] = "Grupa zespołu Wsparcia technicznego";
$MESS["SUP_UGL_LAST_NAME"] = "Nazwisko";
$MESS["SUP_UGL_LOGIN"] = "Login";
$MESS["SUP_UGL_PAGES"] = "Strony";
$MESS["SUP_UGL_TITLE"] = "Grupy i Użytkownicy";
$MESS["SUP_UGL_USER_ID"] = "ID Użytkownika";
?>