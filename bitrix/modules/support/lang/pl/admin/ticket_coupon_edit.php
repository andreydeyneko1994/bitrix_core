<?
$MESS["SUP_CE_COUPON"] = "Kupon";
$MESS["SUP_CE_COUPONS_LIST"] = "Kupony";
$MESS["SUP_CE_COUPON_TITLE"] = "Kupon";
$MESS["SUP_CE_ERROR"] = "Błąd";
$MESS["SUP_CE_F_ACTIVE"] = "Aktywny:";
$MESS["SUP_CE_F_ACTIVE_FROM"] = "Aktywny od:";
$MESS["SUP_CE_F_ACTIVE_TO"] = "Aktywny do:";
$MESS["SUP_CE_F_COUNT"] = "Pozostało do wykorzystania:";
$MESS["SUP_CE_F_COUPON"] = "Kupon:";
$MESS["SUP_CE_F_SLA"] = "SLA";
$MESS["SUP_CE_TITLE_EDIT"] = "Edytuj kupon %COUPON%";
$MESS["SUP_CE_TITLE_NEW"] = "Utwórz nowy kupon";
?>