<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "Edytuj";
$MESS["SUP_ADD"] = "Dodawanie";
$MESS["SUP_ALL"] = "(Wszystko)";
$MESS["SUP_DAY_1"] = "d.";
$MESS["SUP_DAY_3"] = "d.";
$MESS["SUP_DAY_5"] = "d.";
$MESS["SUP_DELETE"] = "Usuń";
$MESS["SUP_DELETE_CONFIRMATION"] = "Na pewno chcessz usunąć to SLA?";
$MESS["SUP_DESCRIPTION"] = "Opis";
$MESS["SUP_EDIT"] = "Modyfikuj";
$MESS["SUP_EXACT_MATCH"] = "Użyj dokładnego dopasowania";
$MESS["SUP_HOUR_1"] = "g.";
$MESS["SUP_HOUR_3"] = "g.";
$MESS["SUP_HOUR_5"] = "g.";
$MESS["SUP_MINUTE_1"] = "min.";
$MESS["SUP_MINUTE_3"] = "min.";
$MESS["SUP_MINUTE_5"] = "min.";
$MESS["SUP_NAME"] = "Nazwa";
$MESS["SUP_NO_LIMITS"] = "< bez ograniczeń >";
$MESS["SUP_PAGES"] = "Poziomy";
$MESS["SUP_PAGE_TITLE"] = "Poziomy wsparcia (SLA)";
$MESS["SUP_PRIORITY"] = "Waga";
$MESS["SUP_RESPONSE_TIME"] = "Czas odpowiedzi";
$MESS["SUP_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["SUP_SHEDULE_S"] = "Harmonogram";
$MESS["SUP_SITE"] = "Strona";
$MESS["SUP_USER_GROUPS"] = "Grupy użytkowników";
?>