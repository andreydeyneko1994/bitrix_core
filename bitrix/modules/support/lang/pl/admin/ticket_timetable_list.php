<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
$MESS["SUP_ADD_NEW"] = "Dodawanie";
$MESS["SUP_ADD_NEW_ALT"] = "Dodawanie";
$MESS["SUP_DELETE_ALT"] = "Usuń";
$MESS["SUP_DELETE_CONF"] = "Na pewno chcesz usunąc rekord?";
$MESS["SUP_DESCRIPTION"] = "Opis";
$MESS["SUP_FILTER_NAME"] = "Nazwa";
$MESS["SUP_GROUP_NAV"] = "Grafiki";
$MESS["SUP_NAME"] = "Nazwa";
$MESS["SUP_TITLE"] = "Grafiki";
$MESS["SUP_UPDATE_ALT"] = "Edytuj";
?>