<?
$MESS["MAIN_USER_PROFILE"] = "Edytuj Profil";
$MESS["MAIN_WAIT"] = "czekaj...";
$MESS["REQUIRED_FIELDS"] = "Wymagane pola.";
$MESS["SUP_GE_ADD_MORE_USERS"] = "Dodaj użytkownika";
$MESS["SUP_GE_CAN_MAIL"] = "Można otrzymywać grupowe powiadomienia";
$MESS["SUP_GE_CAN_MAIL_UPDATE"] = "Powiadomienia o aktualizacji otrzymanych zgłoszeń";
$MESS["SUP_GE_CAN_VIEW"] = "Można edytować zgłoszenia grupowe";
$MESS["SUP_GE_ERROR"] = "Błąd";
$MESS["SUP_GE_GROUP"] = "Grupa";
$MESS["SUP_GE_GROUPS_LIST"] = "Grupy";
$MESS["SUP_GE_GROUP_TITLE"] = "Parametry Grupy";
$MESS["SUP_GE_GROUP_USERS"] = "Użytkownicy";
$MESS["SUP_GE_GROUP_USERS_TITLE"] = "Użytkownicy w grupie";
$MESS["SUP_GE_IS_TEAM_GROUP"] = "Grupa zespołu Wsparcia technicznego";
$MESS["SUP_GE_NAME"] = "Nazwa grupy";
$MESS["SUP_GE_SORT"] = "Sortowanie";
$MESS["SUP_GE_TITLE_NEW"] = "Nowa grupa";
$MESS["SUP_GE_USER"] = "Użytkownik";
$MESS["SUP_GE_USERGROUPS_ADD"] = "Dodaj użytkownika do grupy";
$MESS["SUP_GE_USERGROUPS_LIST"] = "Użytkownicy w grupie";
$MESS["SUP_GE_XML_ID"] = "XML_ID";
?>