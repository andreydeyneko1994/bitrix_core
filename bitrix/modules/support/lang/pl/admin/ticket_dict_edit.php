<?
$MESS["MAIN_ADMIN_MENU_EDIT"] = "Edytuj";
$MESS["SUP_BY_DEFAULT"] = "Ustaw jako domyślne w rozwijanej liście przy tworzeniu nowego zgłoszenia kłopotu na stronie:";
$MESS["SUP_CREATE_NEW_RECORD"] = "Utwórz nowy zapis";
$MESS["SUP_DELETE_RECORD"] = "Usuń Zapis";
$MESS["SUP_DELETE_RECORD_CONFIRM"] = "Na pewno chcesz usunąć ten zapis słownika?";
$MESS["SUP_DESCRIPTION"] = "Opis";
$MESS["SUP_EDIT_RECORD"] = "Edytuj zapis # #ID#";
$MESS["SUP_ERROR"] = "Błąd zachowania zapisu";
$MESS["SUP_EVENT12"] = "wydarzenie1, wydarzenie2: identyfikatory typu wydarzeń";
$MESS["SUP_EVENT3"] = "wydarzenie3: parametr pomocniczy";
$MESS["SUP_EVENT_PARAMS"] = "Metoda rejestrowania nowych zapytań w module Statystyk";
$MESS["SUP_NAME"] = "Tytuł";
$MESS["SUP_NEW_RECORD"] = "Nowy zapis w słowniku";
$MESS["SUP_NO"] = "Nie";
$MESS["SUP_RECORD"] = "Zapis";
$MESS["SUP_RECORDS_LIST"] = "Słownik";
$MESS["SUP_RECORD_TITLE"] = "Ustawienia zapisu w informatorze";
$MESS["SUP_RESPONSIBLE"] = "Odpowiedzialny";
$MESS["SUP_SID"] = "Kod podatku";
$MESS["SUP_SITE"] = "Strony";
$MESS["SUP_SORT"] = "Indeks sortowania";
$MESS["SUP_STAT"] = "Statystyki";
$MESS["SUP_STAT_TITLE"] = "Rejestracja statystyk\"";
$MESS["SUP_TYPE"] = "Rodzaj";
?>