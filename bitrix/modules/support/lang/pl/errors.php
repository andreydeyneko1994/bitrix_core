<?
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["SUP_ERROR_ATTACH_NOT_FOUND"] = "Nie znaleziono pliku.";
$MESS["SUP_ERROR_DELETE"] = "Błąd usuwania.";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE"] = "Niepoprawna data utorzenia.";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY"] = "Niepoprawna data modyfikacji.";
$MESS["SUP_ERROR_INCORRECT_EMAIL"] = "Niepoprawny E-mail.";
$MESS["SUP_ERROR_REQUIRED_TIMETABLE_ID"] = "Nie dostarczono grafiku.";
$MESS["SUP_ERROR_SAVE"] = "Błąd aktualizacji zapisu ##ID#";
$MESS["SUP_ERROR_SLA_1"] = "SLA # 1 nie może zostać usunięte.";
$MESS["SUP_ERROR_SLA_HAS_TICKET"] = "Przed usunięciem SLA  # #ID# upewnij się, że nie ma zgłoszeń z tym SLA.";
$MESS["SUP_FILTER_ERROR"] = "Błąd filtra.";
?>