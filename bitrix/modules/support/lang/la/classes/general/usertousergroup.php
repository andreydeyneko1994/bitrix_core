<?
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "El ID del grupo está vacío";
$MESS["SUP_ERROR_NO_GROUP"] = "No hay tal grupo";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "Este usuario no es miembro del equipo de soporte técnico.";
$MESS["SUP_ERROR_NO_USER"] = "Ningún usuario";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "Este usuario ya es miembro de este grupo";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "El ID de usuario está vacío";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "Sólo clientes de soporte técnico pueden agregarse a este grupo";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "Sólo los miembros del equipo de soporte técnico pueden agregarse a este grupo";
?>