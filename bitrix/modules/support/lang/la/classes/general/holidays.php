<?
$MESS["SUP_DATE_FROM"] = "Activo desde";
$MESS["SUP_DATE_TILL"] = "A través de";
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["SUP_ERROR_DB_ERROR"] = "Error de base de datos.";
$MESS["SUP_ERROR_EMPTY_DATE"] = "Se requiere la fecha de la excepción.";
$MESS["SUP_ERROR_EMPTY_NAME"] = "Se requiere el nombre de la excepción.";
$MESS["SUP_ERROR_EMPTY_OPEN_TIME"] = "Se requiere la acción de la excepción.";
?>