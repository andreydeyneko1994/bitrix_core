<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Comprobado:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "eliminar";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Seleccionado:";
$MESS["SUP_ADD_NEW"] = "Agregar";
$MESS["SUP_ADD_NEW_ALT"] = "Agregar";
$MESS["SUP_DATE_FROM"] = "De";
$MESS["SUP_DATE_TILL"] = "A través de";
$MESS["SUP_DELETE_ALT"] = "Eliminar";
$MESS["SUP_DELETE_CONF"] = "Está seguro que desea eliminar el registro?";
$MESS["SUP_FILTER_NAME"] = "Nombre";
$MESS["SUP_FILTER_OPEN_TIME"] = "Acción";
$MESS["SUP_FILTER_SLA"] = "SLA";
$MESS["SUP_GROUP_NAV"] = "Excepciones";
$MESS["SUP_NAME"] = "Nombre";
$MESS["SUP_OPEN_TIME"] = "Acción";
$MESS["SUP_OPEN_TIME_HOLIDAY"] = "Día genérico off";
$MESS["SUP_OPEN_TIME_HOLIDAY_G"] = "R&R";
$MESS["SUP_OPEN_TIME_HOLIDAY_H"] = "Descanso en función del tiempo";
$MESS["SUP_OPEN_TIME_WORKDAY_0"] = "Día laboral en lugar del Lunes
";
$MESS["SUP_OPEN_TIME_WORKDAY_1"] = "Día laboral en lugar del Martes
";
$MESS["SUP_OPEN_TIME_WORKDAY_2"] = "Día laboral en lugar del Miercoles
";
$MESS["SUP_OPEN_TIME_WORKDAY_3"] = "Día laboral en lugar del Jueves
";
$MESS["SUP_OPEN_TIME_WORKDAY_4"] = "Día laboral en lugar del Viernes
";
$MESS["SUP_OPEN_TIME_WORKDAY_5"] = "Día laboral en lugar del Sábado
";
$MESS["SUP_OPEN_TIME_WORKDAY_6"] = "Día laboral en lugar del Domingo
";
$MESS["SUP_OPEN_TIME_WORKDAY_G"] = "Trabajar";
$MESS["SUP_OPEN_TIME_WORKDAY_H"] = "En función del tiempo de horas especiales de trabajo";
$MESS["SUP_SLA"] = "SLA";
$MESS["SUP_TITLE"] = "Excepciones";
$MESS["SUP_UPDATE_ALT"] = "Editar";
?>