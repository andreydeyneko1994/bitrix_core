<?
$MESS["SUP_CL_COUPON"] = "Cupón ";
$MESS["SUP_CL_COUPON_ID"] = "ID del Cupón ";
$MESS["SUP_CL_FIRST_NAME"] = "Nombre";
$MESS["SUP_CL_FLT_COUPON"] = "Cupón ";
$MESS["SUP_CL_FLT_COUPON_ID"] = "ID del Cupón ";
$MESS["SUP_CL_GUEST_ID"] = "ID del Invitado";
$MESS["SUP_CL_LAST_NAME"] = "Apellido";
$MESS["SUP_CL_LOGIN"] = "Inicio de sesión";
$MESS["SUP_CL_PAGES"] = "Registros";
$MESS["SUP_CL_SESSION_ID"] = "ID de la sesión";
$MESS["SUP_CL_TIMESTAMP_X"] = "Uso de la fecha";
$MESS["SUP_CL_TITLE"] = "Cupón de uso diario";
$MESS["SUP_CL_USER_ID"] = "ID del usuario";
?>