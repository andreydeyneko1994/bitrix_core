<?
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Accès interdit.";
$MESS["SUP_ERROR_ATTACH_NOT_FOUND"] = "Fichier introuvable";
$MESS["SUP_ERROR_DELETE"] = "Échec de suppression.";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE"] = "Date incorrecte de la création.";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_1"] = "Date incorrecte de la création 'de'.";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_2"] = "Date erronée de création de 'jusqu'à'.";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY"] = "Date de modification incorrecte.";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_1"] = "Date de modification est incorrecte 'de'.";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_2"] = "Date de modification du est incorrecte 'jusqu'à'.";
$MESS["SUP_ERROR_INCORRECT_EMAIL"] = "Adresse Email incorrecte.";
$MESS["SUP_ERROR_REQUIRED_NAME"] = "Le champ 'Nom' est pas rempli.";
$MESS["SUP_ERROR_REQUIRED_TIMETABLE_ID"] = "L'horaire n'est pas indiqué.";
$MESS["SUP_ERROR_SAVE"] = "Erreur dossier de mise à jour ##ID#";
$MESS["SUP_ERROR_SLA_1"] = "Il est impossible de supprimer SLA # 1 pour des raisons d'ordre technique.";
$MESS["SUP_ERROR_SLA_HAS_TICKET"] = "Avant SLASLA # #ID# enlever vous devez vous assurer qu'il n'y a pas de billets avec ce SLA.";
$MESS["SUP_FILTER_ERROR"] = "Erreur de filtre.";
?>