<?
$MESS["SUPPORT_MAIL_ADD_TO_CATEGORY"] = "Ajouter un nouveau ticket incident à la catégotie : ";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_TICKET"] = "Créer un nouveau message dans un ticket incident déjà ouvert : ";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_ANY"] = "de toute adresse (seul sujet est cochée)";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_DOMAIN"] = "de toute qdresse email de trouble propriétaire de billets domaine (*@domain.com)";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_EMAIL"] = "qu'à partir Adresse email propriétaire de ticket d'incident user@domain.com";
$MESS["SUPPORT_MAIL_ADD_WITH_CRITICALITY"] = "Ensemble pour une nouvelle criticité de ticket d'incident : ";
$MESS["SUPPORT_MAIL_CONNECT_TICKET_WITH_SITE"] = "Lier un nouveau ticket d'incident sur le site : ";
$MESS["SUPPORT_MAIL_DEF_REGISTERED"] = "Identification de l'utilisateur enregistré par e-mail : ";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_N"] = "non, toujours créer des tickets d'incidents anonymes";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_Y"] = "oui, essayez d'associer ticket d'incident avec l'utilisateur";
$MESS["SUPPORT_MAIL_HIDDEN"] = "comme un message caché (seulement pour les membres de TechSupport)";
$MESS["SUPPORT_MAIL_MAILBOX"] = "< de la boîte aux lettres >";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE"] = "Modèles, sous réserve de détermination réponse à la billetterie du mal : ";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE_NOTES"] = "(expressions régulières, entre premières parenthèses doit être le numéro de demande)";
?>