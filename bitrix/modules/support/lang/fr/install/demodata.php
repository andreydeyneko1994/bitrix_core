<?
$MESS["SUP_DEF_ANSWER_DOES_NOT_SUIT"] = "Réponse ne convient pas";
$MESS["SUP_DEF_ANSWER_IS_NOT_COMPLETE"] = "Réponse est incomplète";
$MESS["SUP_DEF_ANSWER_SUITS_THE_NEEDS"] = "Réponse répond aux besoins";
$MESS["SUP_DEF_BUGS"] = "Erreurs";
$MESS["SUP_DEF_COULD_NOT_BE_SOLVED"] = "Je ne pouvais pas être résolu";
$MESS["SUP_DEF_EASY"] = "Facile";
$MESS["SUP_DEF_E_MAIL"] = "E-mail";
$MESS["SUP_DEF_FORUM"] = "Forum";
$MESS["SUP_DEF_HARD"] = "Dur";
$MESS["SUP_DEF_HIGH"] = "Augmenté";
$MESS["SUP_DEF_LOW"] = "Bas";
$MESS["SUP_DEF_MEDIUM"] = "Moyen";
$MESS["SUP_DEF_MIDDLE"] = "au milieu";
$MESS["SUP_DEF_ORDER_PAYMENT"] = "Paiement de la commande";
$MESS["SUP_DEF_ORDER_SHIPPING"] = "Livraison de commande";
$MESS["SUP_DEF_PHONE"] = "Numéro de téléphone";
$MESS["SUP_DEF_PROBLEM_SOLVING_IN_PROGRESS"] = "Résoudre les problèmes en cours";
$MESS["SUP_DEF_REQUEST_ACCEPTED"] = "Accepté à l'examen";
$MESS["SUP_DEF_SLA_NAME"] = "ordinaire";
$MESS["SUP_DEF_SUCCESSFULLY_SOLVED"] = "Résolu avec succès";
?>