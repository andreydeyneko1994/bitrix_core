<?
$MESS["SUPPORT_ERROR_EDITABLE"] = "Le module n'est ipas disponible dans votre édition.";
$MESS["SUP_ATTENTION"] = "Attention ! Le module sera supprimé du système.";
$MESS["SUP_BACK"] = "Retourner";
$MESS["SUP_COMPLETE"] = "L'installation est terminée.";
$MESS["SUP_CREATE_TICKET"] = "client techsupport";
$MESS["SUP_DELETE_COMLETE"] = "La suppression est terminée.";
$MESS["SUP_DEMO_ACCESS"] = "accès démo";
$MESS["SUP_DEMO_DIR"] = "Suivez ce lien pour afficher le soutien dans l'action : ";
$MESS["SUP_DENIED"] = "Accès refusé";
$MESS["SUP_ERRORS"] = "Erreurs : ";
$MESS["SUP_INSTALL"] = "Installation";
$MESS["SUP_INSTALL_TITLE"] = "Installation du module de Helpdesk";
$MESS["SUP_LINK"] = "Lien au commentaire";
$MESS["SUP_MODULE_DESCRIPTION"] = "Ce module équipe le site avec le système de helpdesk.";
$MESS["SUP_MODULE_NAME"] = "Assistance technique";
$MESS["SUP_NO"] = "aucun";
$MESS["SUP_RESET"] = "Annuler";
$MESS["SUP_SAVE_TABLES"] = "Enregistrer les tables";
$MESS["SUP_SELECT_INITITAL"] = "S'il vous plaît sélectionner le dossier pour les fichiers de module de helpdesk : ";
$MESS["SUP_SITE"] = "Site";
$MESS["SUP_SUPPORT_ADMIN"] = "administrateur techsupport";
$MESS["SUP_SUPPORT_STAFF_MEMBER"] = "membre du personnel de techsupport";
$MESS["SUP_UNINSTALL_TITLE"] = "Helpdesk module de désinstallation";
$MESS["SUP_URL_PUBLIC"] = "dossier de module de Helpdesk (#SITE_DIR# - de dossier racine du site) : ";
$MESS["SUP_WRONG_MAIN_VERSION"] = "Pour installer ce module, vous devez mettre à jour le noyau du système à la version#VER#";
$MESS["SUP_YOU_CAN_SAVE_TABLES"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Enregistrer les tables&quot;";
?>