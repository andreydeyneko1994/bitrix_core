<?
$MESS["CHANGE_STATUS"] = "Ne pas modifier le statut de billets";
$MESS["SUP_ATTACHED_FILES"] = "Les fichiers attachés : ";
$MESS["SUP_ATTACH_NEW_FILES"] = "Fixer de nouveaux fichiers : ";
$MESS["SUP_CERTAIN_SPAM"] = "spam";
$MESS["SUP_CREATE"] = "Créé : ";
$MESS["SUP_DELETE_MESSAGE"] = "Supprimer le message";
$MESS["SUP_DELETE_MESSAGE_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce message ?";
$MESS["SUP_DOWNLOAD"] = "télécharger";
$MESS["SUP_EDIT_ALT"] = "Modifier les paramètres de message";
$MESS["SUP_EDIT_RECORD"] = "Message # #ID# (billet # #TID#)";
$MESS["SUP_ERROR"] = "Erreur message salvifique";
$MESS["SUP_EXTERNAL_FIELD_1"] = "Champ supplémentaire de source externe initiale (des têtes de courriel, etc.) : ";
$MESS["SUP_EXTERNAL_ID"] = "ID de source externe initial (email etc.) : ";
$MESS["SUP_FROM"] = "De la part de : ";
$MESS["SUP_GUEST_ID"] = "Profil des clients";
$MESS["SUP_IS_HIDDEN"] = "Invisible : ";
$MESS["SUP_IS_LOG"] = "Connectez-vous : ";
$MESS["SUP_IS_OVERDUE"] = "Message était en retard : ";
$MESS["SUP_MAX_FILE_SIZE_EXCEEDING"] = "Erreur ! Taille de fichier maximum de '#FILE_NAME#' est dépassée.";
$MESS["SUP_MESSAGE"] = "Message : ";
$MESS["SUP_MESSAGE_NOT_FOUND"] = "Message introuvable";
$MESS["SUP_MORE"] = "Plus";
$MESS["SUP_NUMBER"] = "Nombre : ";
$MESS["SUP_POSSIBLE_SPAM"] = "possible de spam";
$MESS["SUP_RESET"] = "Annuler";
$MESS["SUP_SAVE"] = "Enregistrer";
$MESS["SUP_SPAM_MARK"] = "Spam marque : ";
$MESS["SUP_TASK_TIME"] = "Tâche temps (min.) : ";
$MESS["SUP_TICKETS_LIST"] = "Liste de consultations";
$MESS["SUP_TICKET_EDIT"] = "Modifier ticket # #TID#";
$MESS["SUP_TIMESTAMP"] = "Changé : ";
$MESS["SUP_VIEW_ALT"] = "Accéder au fichier";
?>