<?
$MESS["MAIN_USER_PROFILE"] = "Passer à l'édition du profil";
$MESS["MAIN_WAIT"] = "attendez...";
$MESS["REQUIRED_FIELDS"] = "Champs obligatoires à remplir.";
$MESS["SUP_GE_ADD_MORE_USERS"] = "Ajouter un utilisateur";
$MESS["SUP_GE_CAN_MAIL"] = "Vous pouvez recevoir des notifications de groupe";
$MESS["SUP_GE_CAN_MAIL_UPDATE"] = "Reçoit les notifications au sujet de renouvellement de requêtes dans le groupe";
$MESS["SUP_GE_CAN_VIEW"] = "Peut voir les messages du groupe";
$MESS["SUP_GE_ERROR"] = "Erreur";
$MESS["SUP_GE_GROUP"] = "Groupe";
$MESS["SUP_GE_GROUPS_LIST"] = "Liste des groupes";
$MESS["SUP_GE_GROUP_TITLE"] = "Groupe De Paramètres";
$MESS["SUP_GE_GROUP_USERS"] = "Utilisateurs";
$MESS["SUP_GE_GROUP_USERS_TITLE"] = "Les utilisateurs du ce groupe";
$MESS["SUP_GE_IS_TEAM_GROUP"] = "Groupe d'employés de soutien technique";
$MESS["SUP_GE_NAME"] = "Nom du groupe";
$MESS["SUP_GE_SORT"] = "Classification";
$MESS["SUP_GE_TITLE_EDIT"] = "Modification du groupe '%GROUP_NAME%'";
$MESS["SUP_GE_TITLE_NEW"] = "Ajout d'un nouveau groupe";
$MESS["SUP_GE_USER"] = "Utilisateur";
$MESS["SUP_GE_USERGROUPS_ADD"] = "Ajouter un utilisateur dans le groupe";
$MESS["SUP_GE_USERGROUPS_LIST"] = "Les utilisateurs du ce groupe";
$MESS["SUP_GE_XML_ID"] = "XML_ID";
?>