<?
$MESS["SUP_CL_COUPON"] = "Code promo";
$MESS["SUP_CL_COUPON_ID"] = "ID du coupon";
$MESS["SUP_CL_FIRST_NAME"] = "Dénomination";
$MESS["SUP_CL_FLT_COUPON"] = "Code promo";
$MESS["SUP_CL_FLT_COUPON_ID"] = "ID du coupon";
$MESS["SUP_CL_GUEST_ID"] = "ID invité";
$MESS["SUP_CL_LAST_NAME"] = "Nom";
$MESS["SUP_CL_LOGIN"] = "Connexion";
$MESS["SUP_CL_PAGES"] = "Inscriptions";
$MESS["SUP_CL_SESSION_ID"] = "ID de la session";
$MESS["SUP_CL_TIMESTAMP_X"] = "Date d'utilisation";
$MESS["SUP_CL_TITLE"] = "Journal d'utilisation de coupons";
$MESS["SUP_CL_USER_ID"] = "Identifiant de l'utilisateur";
?>