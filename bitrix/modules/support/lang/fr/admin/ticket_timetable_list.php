<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Coché : ";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "supprimer";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné : ";
$MESS["SUP_ADD_NEW"] = "Ajouter";
$MESS["SUP_ADD_NEW_ALT"] = "Ajouter";
$MESS["SUP_DELETE_ALT"] = "Supprimer";
$MESS["SUP_DELETE_CONF"] = "tes-vous sûr de vouloir supprimer cette entrée ?";
$MESS["SUP_DESCRIPTION"] = "Description";
$MESS["SUP_FILTER_NAME"] = "Dénomination";
$MESS["SUP_GROUP_NAV"] = "Plannings";
$MESS["SUP_NAME"] = "Dénomination";
$MESS["SUP_TITLE"] = "Plannings";
$MESS["SUP_UPDATE_ALT"] = "Éditer";
?>