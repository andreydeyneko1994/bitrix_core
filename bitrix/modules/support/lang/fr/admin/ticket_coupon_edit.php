<?
$MESS["SUP_CE_COUPON"] = "Code promo";
$MESS["SUP_CE_COUPONS_LIST"] = "Coupons des rabais";
$MESS["SUP_CE_COUPON_TITLE"] = "Code promo";
$MESS["SUP_CE_ERROR"] = "Erreur";
$MESS["SUP_CE_F_ACTIVE"] = "Actif(ve) : ";
$MESS["SUP_CE_F_ACTIVE_FROM"] = "Le coupon est actif à partir de la date de : ";
$MESS["SUP_CE_F_ACTIVE_TO"] = "Coupon reste active jusqu'au : ";
$MESS["SUP_CE_F_COUNT"] = "Nombre d'utilisations qui restent : ";
$MESS["SUP_CE_F_COUPON"] = "Coupon : ";
$MESS["SUP_CE_F_SLA"] = "Niveau d'assistance : ";
$MESS["SUP_CE_TITLE_EDIT"] = "Modification du coupon %COUPON%";
$MESS["SUP_CE_TITLE_NEW"] = "Création d'un nouveau coupon";
?>