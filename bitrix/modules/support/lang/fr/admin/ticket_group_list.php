<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Coché : ";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "supprimer";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné : ";
$MESS["MAIN_ALL"] = "(partout)";
$MESS["SUP_GL_ADD"] = "Ajouter le droit d'accès";
$MESS["SUP_GL_DELETE"] = "Supprimer";
$MESS["SUP_GL_DELETE_CONFIRMATION"] = "Supprimer le groupe ?";
$MESS["SUP_GL_EDIT"] = "Éditer";
$MESS["SUP_GL_EXACT_MATCH"] = "Rechercher une coïncidence exacte";
$MESS["SUP_GL_FLT_CLIENT"] = "Les clients du support technique";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP"] = "Catégorie";
$MESS["SUP_GL_FLT_IS_TEAM_GROUP_CN"] = "Catégorie : ";
$MESS["SUP_GL_FLT_NAME"] = "Dénomination";
$MESS["SUP_GL_FLT_SUPPORT"] = "Les membres du service d'assistance technique";
$MESS["SUP_GL_IS_TEAM_GROUP"] = "Groupe d'employés de soutien technique";
$MESS["SUP_GL_NAME"] = "Dénomination";
$MESS["SUP_GL_PAGES"] = "Groupes";
$MESS["SUP_GL_SORT"] = "Classification";
$MESS["SUP_GL_TITLE"] = "Groupe des clients de l'appui technique";
$MESS["SUP_GL_USERADD"] = "Ajouter un utilisateur dans le groupe";
$MESS["SUP_GL_USERLIST"] = "Les utilisateurs du ce groupe";
$MESS["SUP_GL_XML_ID"] = "XML_ID";
?>