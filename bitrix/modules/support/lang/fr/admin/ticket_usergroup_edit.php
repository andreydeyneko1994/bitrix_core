<?
$MESS["SUP_GE_ERROR"] = "Erreur";
$MESS["SUP_UGE_ADD_MORE_USERS"] = "Ajouter";
$MESS["SUP_UGE_GROUP"] = "Groupe";
$MESS["SUP_UGE_NO_GROUP"] = "Groupe introuvable";
$MESS["SUP_UGE_TAB1"] = "Utilisateur";
$MESS["SUP_UGE_TAB1_TITLE"] = "Utilisateur dans le groupe";
$MESS["SUP_UGE_TITLE_ADD"] = "L'ajout d'un utilisateur à un groupe";
$MESS["SUP_UGE_USER"] = "Utilisateur";
$MESS["SUP_UGE_USER_CAN_MAIL_GROUP_MESS"] = "L'utilisateur peut recevoir des messages relatifs au groupe";
$MESS["SUP_UGE_USER_CAN_READ_GROUP_MESS"] = "L'utilisateur peut lire les tickets du groupe";
?>