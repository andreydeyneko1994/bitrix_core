<?
$MESS["SUP_UGL_ADD"] = "Ajouter";
$MESS["SUP_UGL_ADD_TITLE"] = "Ajouter";
$MESS["SUP_UGL_CAN_VIEW_GROUP_MESSAGES"] = "Utilisateur peut voir des billets de ce groupe";
$MESS["SUP_UGL_DELETE"] = "Supprimer";
$MESS["SUP_UGL_DELETE_CONFIRMATION"] = "Supprimer le groupe ?";
$MESS["SUP_UGL_EDIT"] = "Éditer";
$MESS["SUP_UGL_EXACT_MATCH"] = "Rechercher une coïncidence exacte";
$MESS["SUP_UGL_FIRST_NAME"] = "Dénomination";
$MESS["SUP_UGL_FLT_ALL_GROUPS"] = "tous les groupes";
$MESS["SUP_UGL_FLT_CAN_VIEW_GROUP_MESSAGES"] = "Utilisateur peut voir des billets de ce groupe";
$MESS["SUP_UGL_FLT_GROUP"] = "Groupe";
$MESS["SUP_UGL_FLT_IS_TEAM_GROUP"] = "Groupe d'employés de soutien technique";
$MESS["SUP_UGL_FLT_LOGIN"] = "Connexion";
$MESS["SUP_UGL_FLT_USER"] = "Utilisateur";
$MESS["SUP_UGL_GROUP_ID"] = "Identifiant du groupe";
$MESS["SUP_UGL_GROUP_NAME"] = "Nom du groupe";
$MESS["SUP_UGL_IS_TEAM_GROUP"] = "Groupe d'employés de soutien technique";
$MESS["SUP_UGL_LAST_NAME"] = "Nom";
$MESS["SUP_UGL_LOGIN"] = "Connexion";
$MESS["SUP_UGL_PAGES"] = "Au page";
$MESS["SUP_UGL_TITLE"] = "Les groupes d'utilisateurs dont les profils peuvent être modifiés";
$MESS["SUP_UGL_USER_ID"] = "Identifiant de l'utilisateur";
?>