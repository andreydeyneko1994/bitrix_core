<?
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Accès interdit.";
$MESS["SUP_ERROR_DB_ERROR"] = "Erreur de la base de données.";
$MESS["SUP_ERROR_EMPTY_NAME"] = "Nom de l'horaire non spécifié!";
$MESS["SUP_ERROR_TIMETABLE_HAS_SLA"] = "Les niveaux de soutien comprennent les référencés à cet horaire !";
?>