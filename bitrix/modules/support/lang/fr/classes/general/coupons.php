<?
$MESS["SUP_ST_ERROR_NO_NEW_COUPON"] = "Impossible de créer le coupon.";
$MESS["SUP_ST_ERROR_NO_UPDATES_ROWS"] = "Rien n'a été mise à jour.";
$MESS["SUP_ST_ERROR_NO_UPDATE_DATA"] = "Il n'y a pas de données pour la mise à jour.";
$MESS["SUP_ST_ERR_ACTIVE"] = "Le champ 'Activité' n'est pas rempli correctement.";
$MESS["SUP_ST_ERR_COUNT_TICKETS"] = "Le coupon doit être utilisé au moins une fois avant ce moment.";
$MESS["SUP_ST_ERR_DATE_INTERVAL"] = "La durée d'activité du coupon ne doit pas être inférieure à 24 heures.";
$MESS["SUP_ST_ERR_SLA_ID"] = "Le champ 'Niveau du support' est rempli incorrectement.";
?>