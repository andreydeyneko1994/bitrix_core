<?
$MESS["SUP_ERROR_EMPTY_MESSAGE"] = "Vous avez oublié de saisir le champ 'Message'";
$MESS["SUP_ERROR_EMPTY_TITLE"] = "S'il vous plaît remplir le contenu du modèle.";
$MESS["SUP_ERROR_INVALID_COUPON"] = "Coupon inconnu ou expiré";
$MESS["SUP_UNKNOWN_GUEST"] = "Utilisateur non enregistré";
$MESS["SUP_UNKNOWN_USER"] = "Utilisateur inconnu";
?>