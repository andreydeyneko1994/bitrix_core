<?
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "ID vide du groupe.";
$MESS["SUP_ERROR_NO_GROUP"] = "Il n'y a pas un tel groupe.";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "Cet utilisateur n'a aucun lien avec le support technique.";
$MESS["SUP_ERROR_NO_USER"] = "Un tel utilisateur n'existe pas.";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "Cet utilisateur fait déjà partie de ce groupe";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "ID d'utilisateur vide";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "Ce groupe est destiné uniquement aux clients d'assistance technique";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "Seul le personnel de soutien peut consister à ce groupe";
?>