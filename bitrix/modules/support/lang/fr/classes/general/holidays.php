<?
$MESS["SUP_DATE_FROM"] = "Actif(ve) du";
$MESS["SUP_DATE_TILL"] = "Par";
$MESS["SUP_ERROR_ACCESS_DENIED"] = "Accès interdit.";
$MESS["SUP_ERROR_DB_ERROR"] = "Erreur de la base de données.";
$MESS["SUP_ERROR_EMPTY_DATE"] = "La période de validité de l'Exception n'est pas remplie.";
$MESS["SUP_ERROR_EMPTY_NAME"] = "Le nom de l'Exception n'a pas été rempli.";
$MESS["SUP_ERROR_EMPTY_OPEN_TIME"] = "L'action Exceptions n'est pas remplie.";
?>