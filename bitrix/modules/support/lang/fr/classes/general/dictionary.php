<?
$MESS["SUP_CATEGORY"] = "Catégorie";
$MESS["SUP_CRITICALITY"] = "criticité";
$MESS["SUP_DIFFICULTY"] = "Niveau";
$MESS["SUP_ERROR_ADD_DICTONARY"] = "Erreur d'ajout de note";
$MESS["SUP_ERROR_UPDATE_DICTONARY"] = "Erreur de mise à jour de l'entrée";
$MESS["SUP_FORGOT_NAME"] = "Vous avez oublié de saisir le champ 'Nom'";
$MESS["SUP_FUA"] = "Réponses fréquemment utilisés";
$MESS["SUP_INCORRECT_SID"] = "Code symbole est incorrect (seulement des lettres latines / chiffres ou un symbole '_' peuvent être utilisés)";
$MESS["SUP_MARK"] = "Evaluation des réponses";
$MESS["SUP_SID_ALREADY_IN_USE"] = "Pour le type de répertoire '#TYPE#' et de langue '#LANG#' ce code mnémonique est déjà utilisé dans l'enregistrement # #RECORD_ID#.";
$MESS["SUP_SOURCE"] = "Chemin";
$MESS["SUP_STATUS"] = "Statut";
$MESS["SUP_UNKNOWN_ID"] = "Aucune note avec le code #ID#";
?>