<?
$MESS["FACEID_CHECK_PUBLIC_PATH"] = "Określono nieprawidłowy adres.";
$MESS["FACEID_INSTALL_TITLE"] = "Instalacja modułu \"Rozpoznawanie twarzy\"";
$MESS["FACEID_MODULE_DESCRIPTION"] = "Moduł rozpoznawania twarzy do użytku z Bitrix24.";
$MESS["FACEID_MODULE_NAME"] = "Rozpoznawanie twarzy";
$MESS["FACEID_UNINSTALL_QUESTION"] = "Czy na pewno usunąć ten moduł?";
$MESS["FACEID_UNINSTALL_TITLE"] = "Deinstalacja modułu \"Rozpoznawanie twarzy\"";
?>