<?php
$MESS["FACEID_LEAD_SOURCE_DEFAULT"] = "Face Tracker";
$MESS["FACEID_LICENSE_AGREEMENT_HTML"] = "Usługa FindFace (dalej: \"Usługa FindFace\") jest świadczona przez N-TECH.LAB LTD. 
Użytkownicy usługi FindFace zobowiązani są podpisać umowę z Usługą FindFace i muszą zgodzić się na <a href='https://saas.findface.pro/files/docs/license_us.pdf'>następujące warunki</a> klikając \"Zgadzam się\".

Klikając \"Zgadzam się\" użytkownik zgadza się i gwarantuje, że będzie przestrzegał poniższe warunki:

*Będzie ściśle przestrzegał przepisów prawa dotyczących prywatności i danych osobowych obowiązujących w kraju zamieszkania użytkownika.

*Będzie ściśle przestrzegał obowiązujące przepisy prawa. To zobowiązania będzie obowiązywało przez cały okres użytkowania Usługi FindFace.

*Otrzyma pisemną zgodę, lub dokona innych niezbędnych kroków zgodnych z przepisami prawa obowiązującymi w kraju, w którym użytkownik prowadzi działalność.

*Użytkownik jest zobowiązany poinformować z należytą starannością każdą osobę, której dane osobowe, w tym zdjęcia i obrazy, Użytkownik zamierza przetwarzać w trakcie korzystania z Usługi FindFace.

*Będzie szukał porady prawnej PRZED skorzystaniem z usług FindFace.

*Zagwarantuje, że wszystkie informacje zostały przesłane przez Użytkownika za zgodą osoby, której one dotyczą, i pozostają w zgodzie z przepisami prawa obowiązującymi w miejscu korzystania z usług.

*Potwierdzi, że Użytkownik korzysta z Usługi FindFace na własne ryzyko, bez jakiejkolwiek gwarancji.

Bitrix24 nie ponosi żadnej odpowiedzialności za Usługę i nie daje żadnej gwarancji za Usługę FindFace.
Akceptując warunki poniższej umowy, Użytkownik zgadza się i potwierdza, że Usługa FindFace oraz Bitrix24 nie zbiera oraz nie przetwarza żadnych danych przesłanych przez Użytkownika.
Użytkownik potwierdza, że zarówno Usługa FindFace, jak i Bitrix24 nie posiadają wiedzy odnośnie tego, jak Użytkownik zebrał dane, fotografie czy obrazy.
Użytkownik zgadza się, że Usługa FindFace oraz Bitrix nie ponosi odpowiedzialności za jakiekolwiek działania lub ich brak ze strony Użytkownika.

Rozstrzyganie sporów; Wiążący arbitraż: Jakikolwiek spór, kontrowersja, interpretacja lub roszczenie, w tym roszczenia związane ze złamaniem warunków umowy, jakąkolwiek formą zaniechania, oszustwa lub podania błędnych informacji wynikające lub związane z tą umową, będą rozstrzygnięte przez ostateczny i wiążący arbitraż.";
$MESS["FACEID_LICENSE_AGREEMENT_HTML_RICH"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
Usługa FindFace (dalej: \"Usługa FindFace\") jest świadczona przez N-TECH.LAB LTD. 
Użytkownicy usługi FindFace zobowiązani są podpisać umowę z Usługą FindFace i muszą zgodzić się na <a href='https://saas.findface.pro/files/docs/license_us.pdf'>następujące warunki</a> klikając \"Zgadzam się\". 
<br>
Klikając \"Zgadzam się\" użytkownik zgadza się i gwarantuje, że będzie przestrzegał poniższe warunki:

     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Będzie ściśle przestrzegał przepisów prawa dotyczących prywatności i danych osobowych obowiązujących w kraju zamieszkania użytkownika.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Będzie ściśle przestrzegał obowiązujące przepisy prawa. To zobowiązania będzie obowiązywało przez cały okres użytkowania Usługi FindFace.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Otrzyma pisemną zgodę, lub dokona innych niezbędnych kroków zgodnych z przepisami prawa obowiązującymi w kraju, w którym użytkownik prowadzi działalność.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
*Użytkownik jest zobowiązany poinformować z należytą starannością każdą osobę, której dane osobowe, w tym zdjęcia i obrazy, Użytkownik zamierza przetwarzać w trakcie korzystania z Usługi FindFace.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Będzie szukał porady prawnej PRZED skorzystaniem z usług FindFace.

</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Zagwarantuje, że wszystkie informacje zostały przesłane przez Użytkownika za zgodą osoby, której one dotyczą, i pozostają w zgodzie z przepisami prawa obowiązującymi w miejscu korzystania z usług.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Potwierdzi, że Użytkownik korzysta z Usługi FindFace na własne ryzyko, bez jakiejkolwiek gwarancji.
</li>
</ul>
</li>
<li class=\"tracker-agreement-popup-list-item\">
Bitrix24 nie ponosi żadnej odpowiedzialności za Usługę i nie daje żadnej gwarancji za Usługę FindFace.
</li>
</ol>
<div class=\"tracker-agreement-popup-description\">
Bitrix24 nie ponosi żadnej odpowiedzialności za Usługę i nie daje żadnej gwarancji za Usługę FindFace.
Bitrix24 nie ponosi żadnej odpowiedzialności za Usługę i nie daje żadnej gwarancji za Usługę FindFace.
Akceptując warunki poniższej umowy, Użytkownik zgadza się i potwierdza, że Usługa FindFace oraz Bitrix24 nie zbiera oraz nie przetwarza żadnych danych przesłanych przez Użytkownika.
Użytkownik potwierdza, że zarówno Usługa FindFace, jak i Bitrix24 nie posiadają wiedzy odnośnie tego, jak Użytkownik zebrał dane, fotografie czy obrazy.
Użytkownik zgadza się, że Usługa FindFace oraz Bitrix nie ponosi odpowiedzialności za jakiekolwiek działania lub ich brak ze strony Użytkownika.
<br><br>
Rozstrzyganie sporów; Wiążący arbitraż: Jakikolwiek spór, kontrowersja, interpretacja lub roszczenie, w tym roszczenia związane ze złamaniem warunków umowy, jakąkolwiek formą zaniechania, oszustwa lub podania błędnych informacji wynikające lub związane z tą umową, będą rozstrzygnięte przez ostateczny i wiążący arbitraż.
</div>
</div>";
