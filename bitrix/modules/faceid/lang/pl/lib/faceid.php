<?
$MESS["FACEID_CLOUD_ERR_FAIL"] = "Wystąpił błąd podczas pobierania danych z chmury.";
$MESS["FACEID_CLOUD_ERR_FAIL_CONNECT"] = "Wystąpił błąd podczas łączenia z serwerem.";
$MESS["FACEID_CLOUD_ERR_FAIL_DETECT_FACE"] = "Funkcja śledzenia twarzy nie zdołała rozpoznać twarzy.";
$MESS["FACEID_CLOUD_ERR_FAIL_GALLERY_CREATE"] = "Wystąpił błąd podczas tworzenia galerii portalu w chmurze.";
$MESS["FACEID_CLOUD_ERR_FAIL_NO_CREDITS"] = "Niewystarczająca ilość kredytów na Twoim saldzie na portalu.";
$MESS["FACEID_CLOUD_ERR_FAIL_PORTAL_REGISTER"] = "Wystąpił błąd podczas rejestracji portalu w chmurze.";
$MESS["FACEID_CLOUD_ERR_FAIL_PROVIDER_RESPONSE"] = "Funkcja rozpoznawania twarzy jest chwilowo niedostępna.";
$MESS["FACEID_CLOUD_ERR_FAIL_RESPONSE"] = "Wystąpił błąd podczas uzyskiwania odpowiedzi z chmury.";
$MESS["FACEID_CLOUD_ERR_FAIL_SAVE_LOCAL_PHOTO"] = "Wystąpił błąd podczas zapisywania zdjęcia na portalu. Prosimy spróbować ponownie.";
$MESS["FACEID_CLOUD_ERR_FAIL_TEMPORARY_UNAVAILABLE"] = "Usługi w chmurze są tymczasowo niedostępne, prosimy spróbować później.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_COMMAND"] = "Wystąpił błąd podczas wysyłania żądania do chmury.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_SERVICE"] = "Nie określono usługi wykonującej połączenie.";
$MESS["FACEID_CLOUD_ERR_OK_NOT_FOUND"] = "Twarz nie została znaleziona w chmurze.";
$MESS["FACEID_CLOUD_ERR_OK_UNKNOWN_PERSON"] = "Twarz nie została rozpoznana.";
$MESS["FACEID_TRACKER"] = "Face Tracker";
?>