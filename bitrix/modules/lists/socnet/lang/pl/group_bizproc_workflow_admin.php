<?
$MESS["CT_BL_ELEMENTS_TITLE"] = "Lista otwarta";
$MESS["CT_BL_SEQ_BIZPROC"] = "Sekwencyjny proces biznesowy";
$MESS["CT_BL_SEQ_BIZPROC_TITLE"] = "Sekwencyjny proces biznesowy jest prostym procesem biznesowym do przeprowadzania serii następujących po sobie działań na dokumencie.";
$MESS["CT_BL_STATE_BIZPROC"] = "Proces Biznesowy State-driven";
$MESS["CT_BL_STATE_BIZPROC_TITLE"] = "Proces biznesowy state-driven jest ciągłym procesem biznesowym z dystrybucją uprawnień dostępu do obsługi dokumentów o różnym statusie.";
?>