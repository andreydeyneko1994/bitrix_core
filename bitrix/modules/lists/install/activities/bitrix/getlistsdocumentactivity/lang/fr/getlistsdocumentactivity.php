<?
$MESS["BPGLDA_DOC_TYPE"] = "Type d'entité";
$MESS["BPGLDA_DT_LISTS"] = "Listes";
$MESS["BPGLDA_DT_LISTS_SOCNET"] = "Listes des flux de travail et des projets";
$MESS["BPGLDA_DT_PROCESSES"] = "Processus";
$MESS["BPGLDA_ELEMENT_ID"] = "ID de l'élément";
$MESS["BPGLDA_ERROR_DT"] = "Type d'entité incorrect.";
$MESS["BPGLDA_ERROR_ELEMENT_ID"] = "L'ID de l'élément est manquant";
$MESS["BPGLDA_ERROR_EMPTY_DOCUMENT"] = "Récupération des informations de l'entité impossible";
$MESS["BPGLDA_ERROR_FIELDS"] = "Aucun champ d'entité sélectionné";
$MESS["BPGLDA_FIELDS_LABEL"] = "Sélectionner les champs";
$MESS["BPGLDA_WRONG_TYPE"] = "Le type du paramètre '#PARAM#' n'est pas défini.";
?>