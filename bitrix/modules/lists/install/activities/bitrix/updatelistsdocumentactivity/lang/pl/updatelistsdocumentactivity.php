<?
$MESS["BPULDA_DOC_TYPE"] = "Typ jednostki";
$MESS["BPULDA_DT_LISTS"] = "Listy";
$MESS["BPULDA_DT_LISTS_SOCNET"] = "Listy grup roboczych i projektów";
$MESS["BPULDA_DT_PROCESSES"] = "Procesy";
$MESS["BPULDA_ELEMENT_ID"] = "ID elementu";
$MESS["BPULDA_ERROR_DT"] = "Nieprawidłowy typ jednostki.";
$MESS["BPULDA_ERROR_ELEMENT_ID"] = "Brakuje ID elementu";
$MESS["BPULDA_ERROR_FIELDS"] = "Pola elementu nie są określone";
$MESS["BPULDA_FIELD_REQUIED"] = "Pole „#FIELD#” jest wymagane.";
$MESS["BPULDA_WRONG_TYPE"] = "Typ parametru „#PARAM#” jest niezdefiniowany.";
?>