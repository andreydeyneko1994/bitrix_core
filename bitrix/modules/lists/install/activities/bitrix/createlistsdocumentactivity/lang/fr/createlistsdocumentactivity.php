<?
$MESS["BPCLDA_DOC_TYPE_1"] = "Type d'entité";
$MESS["BPCLDA_DT_LISTS"] = "Listes";
$MESS["BPCLDA_DT_LISTS_SOCNET_1"] = "Listes des flux de travail et des projets";
$MESS["BPCLDA_DT_PROCESSES"] = "Flux de travail";
$MESS["BPCLDA_ERROR_DT_1"] = "Type d'entité incorrect.";
$MESS["BPCLDA_FIELD_REQUIED"] = "Le champ '#FIELD#' est requis.";
$MESS["BPCLDA_WRONG_TYPE"] = "Le type de paramètre '#PARAM#' n'est pas défini.";
?>