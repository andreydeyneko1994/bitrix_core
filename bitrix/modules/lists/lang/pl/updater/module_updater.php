<?
$MESS["ELEMENT_NAME"] = "Elementy";
$MESS["MU_IBLOCK_ELEMENTS_NAME"] = "Elementy";
$MESS["MU_IBLOCK_ELEMENT_ADD"] = "Dodaj element";
$MESS["MU_IBLOCK_ELEMENT_DELETE"] = "Usuń element";
$MESS["MU_IBLOCK_ELEMENT_EDIT"] = "Edytuj element";
$MESS["MU_IBLOCK_ELEMENT_NAME"] = "Element";
$MESS["MU_IBLOCK_FIELD_LIST_NO"] = "Nie";
$MESS["MU_IBLOCK_FIELD_LIST_YES"] = "Tak";
$MESS["MU_IBLOCK_FIELD_NAME"] = "Nazwa";
$MESS["MU_IBLOCK_NAME_FIELD"] = "Nazwa";
$MESS["MU_IBLOCK_NAME_MISSION"] = "Podanie o wyjazd służbowy";
$MESS["MU_IBLOCK_SECTIONS_NAME"] = "Sekcje";
$MESS["MU_IBLOCK_SECTION_ADD"] = "Dodaj sekcję";
$MESS["MU_IBLOCK_SECTION_DELETE"] = "Usuń sekcję";
$MESS["MU_IBLOCK_SECTION_EDIT"] = "Edytuj sekcje";
$MESS["MU_IBLOCK_SECTION_NAME"] = "Sekcja";
$MESS["MU_MENU_TITLE_MY_PROCESSES"] = "Moje zgłoszenia";
$MESS["MU_MENU_TITLE_PROCESS"] = "Workflow";
$MESS["NAME"] = "Workflow";
$MESS["SECTION_NAME"] = "Sekcje";
?>