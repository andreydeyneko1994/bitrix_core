<?
$MESS["LISTS_DEL_SOCNET_LOG_GROUP"] = "Listy (usuwanie)";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE"] = "Usunięto listę \"#TITLE#\"";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE_MAIL"] = "Usunięto listę \"#TITLE#\" w grupie \"#ENTITY#\"";
$MESS["LISTS_SOCNET_LOG_GROUP"] = "Listy";
$MESS["LISTS_SOCNET_LOG_GROUP_SETTINGS"] = "Wszystkie zmiany w listach tej grupy";
$MESS["LISTS_SOCNET_LOG_TITLE"] = "Dodana lub zaktualizowana lista \"#TITLE#\"";
$MESS["LISTS_SOCNET_LOG_TITLE_MAIL"] = "Dodana lub zaktualizowana lista \"#TITLE#\" w grupie \"#ENTITY#\" ";
$MESS["LISTS_SOCNET_TAB"] = "Listy";
?>