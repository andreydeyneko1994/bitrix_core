<?php
$MESS["LISTS_LF_ADD_COMMENT_SOURCE_ERROR"] = "Nie powiodło się dodanie komentarza do źródła wydarzenia..";
$MESS["LISTS_LF_COMMENT_MENTION"] = "wspomniano cię w komentarzu #title#";
$MESS["LISTS_LF_COMMENT_MENTION_F"] = "wspomniała cię w komentarzu #title#";
$MESS["LISTS_LF_COMMENT_MENTION_M"] = "wspomniał cię w komentarzu #title#";
$MESS["LISTS_LF_COMMENT_MENTION_TITLE"] = "\"#PROCESS#\"";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD"] = "Dodał komentarz do twojego workflow '#PROCESS#'";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_F"] = "Dodał komentarz do twojego workflow '#PROCESS#'";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_M"] = "Dodał komentarz do twojego workflow '#PROCESS#'";
$MESS["LISTS_LF_MOBILE_DESTINATION"] = "do";
