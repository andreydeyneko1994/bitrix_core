<?
$MESS["LISTS_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["LISTS_COPY_IBLOCK_ERROR_ADD_FIELD"] = "Błąd kopiowania pola \"#field#\".";
$MESS["LISTS_COPY_IBLOCK_ERROR_GET_DATA"] = "Nie udało się pozyskać danych do skopiowania.";
$MESS["LISTS_COPY_IBLOCK_ERROR_SET_RIGHT"] = "Błąd przypisywania uprawnień do zduplikowania bloku informacji.";
$MESS["LISTS_COPY_IBLOCK_NAME_TITLE"] = "(Kopia)";
$MESS["LISTS_MODULE_BIZPROC_NOT_INSTALLED"] = "Moduł \"Workflow\" nie jest zainstalowany.";
$MESS["LISTS_REQUIRED_PARAMETER"] = "Brakuje wymaganego parametru \"#parameter#\".";
?>