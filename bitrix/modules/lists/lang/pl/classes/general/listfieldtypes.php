<?
$MESS["LISTS_LIST_FIELD_ACTIVE_FROM"] = "Aktywne od";
$MESS["LISTS_LIST_FIELD_ACTIVE_TO"] = "Aktywny do";
$MESS["LISTS_LIST_FIELD_CREATED_BY"] = "Utworzony przez";
$MESS["LISTS_LIST_FIELD_DATE_CREATE"] = "Utwórz datę";
$MESS["LISTS_LIST_FIELD_DETAIL_PICTURE"] = "Szczegółowy obraz";
$MESS["LISTS_LIST_FIELD_DETAIL_TEXT"] = "Szczegółowy tekst";
$MESS["LISTS_LIST_FIELD_E"] = "Wiąż z obiektami CRM";
$MESS["LISTS_LIST_FIELD_F"] = "Plik";
$MESS["LISTS_LIST_FIELD_G"] = "Powiąż z Sekcjami";
$MESS["LISTS_LIST_FIELD_L"] = "Lista";
$MESS["LISTS_LIST_FIELD_MODIFIED_BY"] = "Zmodyfikowane przez";
$MESS["LISTS_LIST_FIELD_N"] = "Liczba";
$MESS["LISTS_LIST_FIELD_NAME"] = "Nazwa";
$MESS["LISTS_LIST_FIELD_PREVIEW_PICTURE"] = "Miniatura";
$MESS["LISTS_LIST_FIELD_PREVIEW_TEXT"] = "Podgląd Tekstu";
$MESS["LISTS_LIST_FIELD_S"] = "String";
$MESS["LISTS_LIST_FIELD_SORT"] = "Sortuj";
$MESS["LISTS_LIST_FIELD_TIMESTAMP_X"] = "Data modyfikacji";
?>