<?php
$MESS["BITRIX_PROCESSES_ELEMENT_NAME"] = "Elementy";
$MESS["BITRIX_PROCESSES_SECTION_NAME"] = "Sekcje";
$MESS["BITRIX_PROCESSES_TYPE_NAME"] = "Procesy";
$MESS["LISTS_ELEMENT_NAME"] = "Elementy";
$MESS["LISTS_INSTALL_TITLE"] = "Instalacja modułu wspólnych list";
$MESS["LISTS_MODULE_DESCRIPTION"] = "Moduł ten ułatwia pracę z informacjami w sekcji publicznej.";
$MESS["LISTS_MODULE_NAME"] = "Listy ogólne";
$MESS["LISTS_SECTION_NAME"] = "Sekcje";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Elementy";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Sekcje";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listy sieci społecznych";
$MESS["LISTS_TYPE_NAME"] = "Listy";
$MESS["LISTS_UNINSTALL_TITLE"] = "Deinstalacja modułu wspólnych list";
