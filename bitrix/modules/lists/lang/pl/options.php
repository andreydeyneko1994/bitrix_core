<?php
$MESS["LISTS_OPTIONS_ADD_RIGHT"] = "Dodaj uprawnienia dostępu";
$MESS["LISTS_OPTIONS_CHOOSE_GROUP"] = "(Wybierz grupę)";
$MESS["LISTS_OPTIONS_CHOOSE_TYPE"] = "(Wybierz typ bloku informacji)";
$MESS["LISTS_OPTIONS_IBLOCK_TYPES"] = "Typ Bloku Informacji";
$MESS["LISTS_OPTIONS_LIVE_FEED_CHECK"] = "Włącz integrację z Tablicą";
$MESS["LISTS_OPTIONS_LIVE_FEED_IBLOCK_TYPE"] = "Typ bloku informacji do użycia na Aktualności";
$MESS["LISTS_OPTIONS_SOCNET_ENABLE"] = "Użyj wspólnych list w sieci społecznościowej";
$MESS["LISTS_OPTIONS_SOCNET_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["LISTS_OPTIONS_TAB_LIVE_FEED"] = "Aktualności";
$MESS["LISTS_OPTIONS_TAB_PERMISSIONS"] = "Uprawnienia dostępu";
$MESS["LISTS_OPTIONS_TAB_SOCNET"] = "Sieć Społecznościowa";
$MESS["LISTS_OPTIONS_TAB_TITLE_LIVE_FEED"] = "Konfiguruj listę dla użycia z Tablicą";
$MESS["LISTS_OPTIONS_TAB_TITLE_PERMISSIONS"] = "Wybierz grupę użytkownika mogącą zarządzać listą poszczególnych typów bloku informacji";
$MESS["LISTS_OPTIONS_TAB_TITLE_SOCNET"] = "Wspólne listy w sieci społecznościowej";
$MESS["LISTS_OPTIONS_USER_GROUPS"] = "Grupa użytkownika";
