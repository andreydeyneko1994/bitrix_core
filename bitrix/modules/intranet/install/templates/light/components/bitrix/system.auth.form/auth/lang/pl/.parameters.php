<?
$MESS["SAF_TP_PATH_TO_BLOG"] = "Szablon ścieżki do strony bloga użytkownika";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Szablon ścieżki do strony Kalendarza użytkownika";
$MESS["SAF_TP_PATH_TO_FILES"] = "Szablon ścieżki do Plików uzytkownika";
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Szablon ścieżki do strony pulpitu użytkownika";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Szablon ścieżki do strony galeria zdjęć użytkownika";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Ścieżka do strony procesów biznesowych";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Szablon ścieżki do strony grupy Grupy Roboczej";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Szablon ścieżki do strony grupy użytkownika";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Szablon ścieżki do strony aktualizacji użytkownika";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Szablon ścieżki do strony wiadomości użytkownika";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Szablon ścieżki do strony Czatu";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Szablon ścieżki do formularza postu wiadomości";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Szablon ścieżki do formularza odpowiedzi";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Szablom ścieżki do strony profilu użytkownika";
$MESS["SAF_TP_PATH_TO_TASKS"] = "Szablon URL strony Zadań użytkownika";
?>