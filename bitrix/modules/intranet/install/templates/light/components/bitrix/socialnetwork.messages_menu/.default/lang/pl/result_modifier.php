<?
$MESS["SONET_UM_ABSENT"] = "(poza biurem)";
$MESS["SONET_UM_BLOG"] = "Blog";
$MESS["SONET_UM_CALENDAR"] = "Kalendarz";
$MESS["SONET_UM_FILES"] = "Pliki";
$MESS["SONET_UM_FORUM"] = "Forum";
$MESS["SONET_UM_FRIENDS"] = "Znajomi";
$MESS["SONET_UM_GENERAL"] = "Główne";
$MESS["SONET_UM_GROUPS"] = "Grupy";
$MESS["SONET_UM_MICROBLOG"] = "Microblog";
$MESS["SONET_UM_ONLINE"] = "online";
$MESS["SONET_UM_PHOTO"] = "Galeria";
$MESS["SONET_UM_SEARCH"] = "Szukaj";
$MESS["SONET_UM_TASKS"] = "Zadania";
?>