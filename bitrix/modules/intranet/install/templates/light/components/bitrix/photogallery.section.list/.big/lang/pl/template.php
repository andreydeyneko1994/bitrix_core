<?
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "Album jest ukryty.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "Album jest ukryty i chroniony hasłem.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Chronione hasłem.";
$MESS["P_EDIT_ICON"] = "Wybierz okładkę";
$MESS["P_EDIT_ICON_TITLE"] = "Wybierz okładkę albumu";
$MESS["P_EMPTY_DATA"] = "Nie dodano jeszcze żadnego albumu";
$MESS["P_PHOTOS"] = "Zdjęcia";
$MESS["P_SECTION_DELETE"] = "Usuń";
$MESS["P_SECTION_DELETE_ASK"] = "Na pewno chcesz usunąć nieodwracalnie album?";
$MESS["P_SECTION_DELETE_TITLE"] = "Usuń album";
$MESS["P_SECTION_EDIT"] = "Edytuj";
$MESS["P_SECTION_EDIT_TITLE"] = "Edytuj album";
?>