<?
$MESS["SONET_UM_ABSENT"] = "(poza biurem)";
$MESS["SONET_UM_BIRTHDAY"] = "Dzisiaj są urodziny użytkownika";
$MESS["SONET_UM_BIZPROC"] = "Proces Biznesowy";
$MESS["SONET_UM_EDIT_FEATURES"] = "Edytuj ustawienia";
$MESS["SONET_UM_EDIT_PROFILE"] = "Edytuj Profil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Edytuj ustawienia prywatności";
$MESS["SONET_UM_HONOUR"] = "Użytkownik jest na Tablicy Honorowej";
$MESS["SONET_UM_INPUT"] = "Przychodzące";
$MESS["SONET_UM_LOG"] = "Tablica Aktywności";
$MESS["SONET_UM_MESSAGES"] = "wiadomości";
$MESS["SONET_UM_MUSERS"] = "wiadomości";
$MESS["SONET_UM_MY_MESSAGES"] = "Moje wiadomości";
$MESS["SONET_UM_ONLINE"] = "online";
$MESS["SONET_UM_OUTPUT"] = "Wychodzący";
$MESS["SONET_UM_SUBSCRIBE"] = "Subskrypcja";
$MESS["SONET_UM_TASKS"] = "Subskrypcja";
$MESS["SONET_UM_USER"] = "Mój profil";
$MESS["SONET_UM_USER_BAN"] = "Lista banów";
?>