<?
$MESS["INTR_COMP_IS_TPL_MY_OFFICE"] = "mon office";
$MESS["INTR_COMP_IS_TPL_SECH"] = "Recherche";
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Annuler";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Rechercher";
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Département";
$MESS["INTR_ISS_PARAM_EMAIL"] = "E-mail";
$MESS["INTR_ISS_PARAM_FIO"] = "Dénomination";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "Mots-clés";
$MESS["INTR_ISS_PARAM_PHONE"] = "Numéro de téléphone";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Interphone";
?>