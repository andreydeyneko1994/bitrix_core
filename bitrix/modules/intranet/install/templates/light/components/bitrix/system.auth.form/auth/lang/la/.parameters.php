<?
$MESS["SAF_TP_PATH_TO_BLOG"] = "Ruta a la plantilla de la página del blog de usuario";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Ruta a la plantilla de la página de calendario del usuario";
$MESS["SAF_TP_PATH_TO_FILES"] = "Ruta a la plantilla de la página de archivos del usuario";
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Ruta a la plantilla de la página de escritorio del usuario";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Ruta a la plantilla de la página galería de fotos de usuario";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Ruta a la plantilla de la página procesos de negocios";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Ruta a la plantilla de la página del grupo de trabajo";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Ruta a la plantilla de la página de grupo de usuarios";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Ruta a la plantilla de la página de actualizaciones de usuario";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Ruta a la plantilla de la página de mensajes del usuario";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Ruta a la plantilla de la página de chat";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Ruta a la plantilla del formulario de mensaje de entrada";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Ruta a la plantilla del formulario de respuesta";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Ruta a la plantilla de la página perfil del usuario";
$MESS["SAF_TP_PATH_TO_TASKS"] = "URL a la plantilla de la página de tareas del usuario";
?>