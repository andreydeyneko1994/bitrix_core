<?
$MESS["INTASK_T2456_UPDATES"] = "Aktualizacje";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "Archiwum grupy";
$MESS["SONET_C39_SEND_MESSAGE_GROUP"] = "Napisz wiadomość";
$MESS["SONET_C39_SEND_MESSAGE_GROUP_TITLE"] = "Napisz wiadomość do członków grupy";
$MESS["SONET_C6_ACT_BAN"] = "Lista banów";
$MESS["SONET_C6_ACT_DELETE"] = "Usuń grupę";
$MESS["SONET_C6_ACT_EDIT"] = "Edytuj grupę";
$MESS["SONET_C6_ACT_EXIT"] = "Opuść grupę";
$MESS["SONET_C6_ACT_FEAT"] = "Edytuj ustawienia";
$MESS["SONET_C6_ACT_JOIN"] = "Dołącz do grupy";
$MESS["SONET_C6_ACT_JOIN_REQUEST_SENT"] = "Już wysłałeś podanie o dołączenie do grupy roboczej.";
$MESS["SONET_C6_ACT_JOIN_REQUEST_SENT_BY_GROUP"] = "Zaproszenie o dołączenie do tej grupy roboczej zostało już do ciebie wysłane. Możesz potwierdzić swoje członkostwo na <a href='#LINK#'>stronie zaproszenia</a>.";
$MESS["SONET_C6_ACT_MOD"] = "Edytuj moderatorów";
$MESS["SONET_C6_ACT_MOD1"] = "Moderatorzy";
$MESS["SONET_C6_ACT_REQU"] = "Zaproś do grupy";
$MESS["SONET_C6_ACT_SUBSCRIBE"] = "Subskrybuj";
$MESS["SONET_C6_ACT_USER"] = "Edytuj członków";
$MESS["SONET_C6_ACT_USER1"] = "Członkowie";
$MESS["SONET_C6_ACT_VREQU"] = "Wyświetl podania o przyjęcie";
$MESS["SONET_C6_ACT_VREQU_OUT"] = "Zaproszenia do grupy";
$MESS["SONET_C6_AL_USERS"] = "Wszyscy Członkowie";
$MESS["SONET_C6_BLOG_T"] = "Blog";
$MESS["SONET_C6_CREATED"] = "Utworzony";
$MESS["SONET_C6_DESCR"] = "Opis";
$MESS["SONET_C6_FORUM_T"] = "Forum";
$MESS["SONET_C6_HIDE_DESCRIPTION"] = "Ukryj opis";
$MESS["SONET_C6_LIVEFEED"] = "Ostatnie zmiany";
$MESS["SONET_C6_MEMBERS_REST"] = "Inni członkowie";
$MESS["SONET_C6_MODERATORS_REST"] = "Inni moderatorzy";
$MESS["SONET_C6_MODS_TITLE"] = "Moderatorzy";
$MESS["SONET_C6_NMEM"] = "Członkowie";
$MESS["SONET_C6_NO_MODS"] = "Brak moderatorów";
$MESS["SONET_C6_NO_USERS"] = "Brak członków";
$MESS["SONET_C6_OWNER"] = "Właściciel";
$MESS["SONET_C6_PHOTO_T"] = "Zdjęcia";
$MESS["SONET_C6_SHOW_DESCRIPTION"] = "Pokaż opis";
$MESS["SONET_C6_TAGS"] = "Tagi";
$MESS["SONET_C6_TOPIC"] = "Temat";
$MESS["SONET_C6_TYPE"] = "Typ grupy";
$MESS["SONET_C6_TYPE_O1"] = "To jest grupa publiczna. Każdy może do niej dołączyć";
$MESS["SONET_C6_TYPE_O1_V1"] = "Grupa publiczna widoczna";
$MESS["SONET_C6_TYPE_O1_V2"] = "Grupa publiczna niewidoczna";
$MESS["SONET_C6_TYPE_O2"] = "To jest grupa prywatna. Członkostwo zależy od akceptacji przez administratora.";
$MESS["SONET_C6_TYPE_O2_V1"] = "Grupa prywatna widoczna";
$MESS["SONET_C6_TYPE_O2_V2"] = "Grupa prywatna niewidoczna";
$MESS["SONET_C6_TYPE_V1"] = "Ta grupa jest widoczna dla każdego.";
$MESS["SONET_C6_TYPE_V2"] = "Ta grupa jest niewidoczna. Tylko członkowie grupy mogą ją widzieć.";
$MESS["SONET_C6_USERS_T"] = "Członkowie";
$MESS["SONET_C6_USER_IS_EXTRANET"] = "(ekstranet)";
?>