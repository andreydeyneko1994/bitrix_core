<?
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Możesz eksportować pracowników jako kontakty programu Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Eksportuj";
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "Szukanie zaawansowane";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "Szukanie alfabetyczne";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Szukaj";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "Synchronizuj za pomocą CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Synchronizuj rejest pracowników z oprogramowaniem i sprzętem współpracującym z CardDAV (iPhone, iPad etc.)";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Ekaportuj wyniki wyszukiwania do MS Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "List";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Eksportuj listę pracowników jako kontakty Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Wyświetl";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "Lista";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "Szczegóły";
?>