<?
$MESS["FINISH_STEP_CONTENT"] = "<b>Gratulacje!</b><br /><br />Konfiguracja została zakończona. Możesz teraz otworzyć swój Bitrix24 i rozpocząć pracę.<br />";
$MESS["FINISH_STEP_TITLE"] = "Kończenie Ustawień";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "Konfiguracja jest kompletna";
$MESS["INST_ERROR_NOTICE"] = "Proszę powtórzyć obecny krok. Jeżeli błąd się powtórzy, pomiń ten krok.";
$MESS["INST_ERROR_OCCURED"] = "Uwaga! Wystąpił błąd na tym etapie instalacji.";
$MESS["INST_JAVASCRIPT_DISABLED"] = "Wizard wymaga włączonego JavaScript w twoim systemie. JavaScript jest wyłączone lub nie wspierane przez twoją przeglądarkę. Proszę zmienić ustawienia przeglądarki i <a href=\"\">spróbować ponownie</a>.";
$MESS["INST_RETRY_BUTTON"] = "Ponownie";
$MESS["INST_SKIP_BUTTON"] = "Pomiń";
$MESS["INST_TEXT_ERROR"] = "Wiadomość Błędu";
$MESS["NEXT_BUTTON"] = "Dalej";
$MESS["PREVIOUS_BUTTON"] = "Wstecz";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Instalowanie danych...";
$MESS["wiz_company_name"] = "Nazwa Firmy:";
$MESS["wiz_go"] = "Uruchom Bitrix24";
$MESS["wiz_install_data"] = "Instalacja Danych";
$MESS["wiz_install_data_extranet"] = "Instalacja ekstranetu";
$MESS["wiz_ldap_warn"] = "Uwaga!<br />jeżeli chcesz, aby Twój intranet wspierał Active Directory, upewnij się, że moduł LDAP został zainstalowany w PHP.";
$MESS["wiz_slogan"] = "Bitrix";
?>