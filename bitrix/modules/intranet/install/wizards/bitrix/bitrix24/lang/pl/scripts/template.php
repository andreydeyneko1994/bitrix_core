<?php
$MESS["COPYRIGHT"] = "Prawa autorskie; 2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "Wizard wymaga włączonego JavaScript w twoim systemie. JavaScript jest wyłączone lub nie wspierane przez twoją przeglądarkę. Proszę zmienić ustawienia przeglądarki i <a href=\"\">spróbować ponownie</a>.";
$MESS["WIZARD_TITLE"] = "Konfiguracja Bitrix24.CRM";
