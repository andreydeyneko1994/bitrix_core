<?
$MESS["FINISH_STEP_CONTENT"] = "<b>Parabéns!</b><br /><br />A configuração está concluída. Agora, você pode abrir seu Bitrix24 e começar a trabalhar.<br />";
$MESS["FINISH_STEP_TITLE"] = "Finalizando a Configuração";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "A configuração está concluída";
$MESS["INST_ERROR_NOTICE"] = "Repita a etapa atual. Se o erro persistir, ignore a etapa.";
$MESS["INST_ERROR_OCCURED"] = "Atenção! Ocorreu um erro nesta etapa da instalação.";
$MESS["INST_JAVASCRIPT_DISABLED"] = "O assistente requer que o JavaScript esteja ativado no seu sistema. O JavaScript está desativado ou não é suportado pelo seu navegador. Altere as configurações do navegador e <a href=\"\">tente novamente</a>.";
$MESS["INST_RETRY_BUTTON"] = "Repetir";
$MESS["INST_SKIP_BUTTON"] = "Pular";
$MESS["INST_TEXT_ERROR"] = "Mensagem de Erro";
$MESS["NEXT_BUTTON"] = "Próximo";
$MESS["PREVIOUS_BUTTON"] = "Voltar";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Instalando dados...";
$MESS["wiz_company_name"] = "Nome da Empresa:";
$MESS["wiz_go"] = "Iniciar o Bitrix24";
$MESS["wiz_install_data"] = "Instalação de Dados";
$MESS["wiz_install_data_extranet"] = "Instalação da Extranet";
$MESS["wiz_ldap_warn"] = "Atenção!<br />Se você deseja que o portal da intranet suporte o Active Directory, verifique se o módulo LDAP está instalado em PHP.";
$MESS["wiz_slogan"] = "Bitrix";
?>