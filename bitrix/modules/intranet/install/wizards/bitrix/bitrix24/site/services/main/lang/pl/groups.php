<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Członkowie tej grupy mają dostęp do Panelu Sterowania.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Użytkownicy Panelu Sterowania";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Członkowie tej grupy mogą tworzyć nowe grupy robocze.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Administratorzy Grup Roboczych";
$MESS["DIRECTION_GROUP_DESC"] = "Zarząd Firmy.";
$MESS["DIRECTION_GROUP_NAME"] = "Pulpit Zarządzania";
$MESS["EMPLOYEES_GROUP_DESC"] = "Wszyscy pracownicy zarejestrowani w Bitrix24.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Pracownicy";
$MESS["INTEGRATOR_GROUP_NAME"] = "Partner Bitrix24";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Pracownicy działu Sprzedaży i Marketingu.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Sprzedaż i Marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Personel Działu HR.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Dział HR";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Administratorzy Portalu mają dostęp do wszystkich usług portalu.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Administratorzy Portalu";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupa użytkowników, która może edytować preferencje sklepu internetowego";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administratorzy sklepów internetowych";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupa użytkowników, która może korzystać z funkcji sklepu internetowego";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Personel sklepu internetowego";
$MESS["SUPPORT_GROUP_DESC"] = "Specjalista Helpdesk.";
$MESS["SUPPORT_GROUP_NAME"] = "Hepldesk";
?>