<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Members of this group can access the Control Panel.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Control Panel users";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Members of this group can create new workgroups.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Workgroup Administrators";
$MESS["DIRECTION_GROUP_DESC"] = "Company Management Board.";
$MESS["DIRECTION_GROUP_NAME"] = "Management Board";
$MESS["EMPLOYEES_GROUP_DESC"] = "All company employees, registered in portal.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Employees";
$MESS["INTEGRATOR_GROUP_NAME"] = "Partenaire Bitrix24";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Sales and Marketing department personnel.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Sales and Marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "HR department personnel.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "HR department";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Portal Administrators have access to all portal services.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Portal Administrators";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Groupe d'utilisateurs autorisés à éditer les préférences de la boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administrateurs de boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Groupe d'utilisateurs autorisés à utiliser les fonctionnalités de la boutique en ligne";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Personnel de boutique en ligne";
$MESS["SUPPORT_GROUP_DESC"] = "Helpdesk specialist.";
$MESS["SUPPORT_GROUP_NAME"] = "Helpdesk";
?>