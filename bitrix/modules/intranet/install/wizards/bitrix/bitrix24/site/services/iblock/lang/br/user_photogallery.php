<?
$MESS["SONET_LOG_GUEST"] = "Convidado";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# adicionou uma nova foto \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# adicionou uma foto";
$MESS["SONET_PHOTO_LOG_2"] = "fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Novas fotos carregadas para \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Novas fotos: <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Ver álbum</a>.";
$MESS["W_IB_USER_PHOTOG_TAB1"] = "edit1--#--Foto --,--ACTIVE--#-- Ativo--,--NAME--#--*Título--,--IBLOCK_ELEMENT_PROP_VALUE--#----Valores de Propriedade--,--PREVIEW_PICTURE--#--Visualizar imagem--,--DETAIL_PICTURE--#-- Imagem detalhada--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB2"] = "--#--  Original--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB3"] = "--#--  Avaliação--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB4"] = "--#--  Votos--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB5"] = "--#--  Total de votos--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB6"] = "--#--  O item foi aprovado--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB7"] = "--#-- Item publicado--;--edit6--#--Descrição--,--PREVIEW_TEXT--#-- Visualizar Texto--,--DETAIL_TEXT--#-- Descrição detalhada--;--edit2--#--Seções--,--SECTIONS--#--Seções--;--edit3--#--Mais informações--, --SORT-- #--Índice de Classificação--,--CODE--#--  Código Simbólico--,--TAGS--#--  Marcadores--;--";
?>