<?php
$MESS["CAL_TYPE_GROUP_NAME"] = "Calendários do grupo";
$MESS["CAL_TYPE_LOCATION_NAME"] = "Disponibilidade da sala de reunião";
$MESS["CAL_TYPE_USER_NAME"] = "Calendários do usuário";
$MESS["CAL_YEAR_HOLIDAYS"] = "01.01,04.07,01.11,25.12";
$MESS["EC_COMPANY_CALENDAR_"] = "Calendário da empresa";
$MESS["EC_COMPANY_CALENDAR_GOV_ORGANIZATION"] = "Calendário da organização";
$MESS["EC_COMPANY_CALENDAR_PUBLIC_ORGANIZATION"] = "Calendário da organização";
$MESS["W_IB_CALENDAR_EMP_ABS"] = "Reunião com um cliente no escritório dele";
