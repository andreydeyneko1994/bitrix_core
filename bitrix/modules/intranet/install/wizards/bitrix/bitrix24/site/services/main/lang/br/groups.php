<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Os membros deste grupo podem acessar o Painel de Controle.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Usuários do Painel de Controle";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Os membros deste grupo podem criar novos grupos de trabalho.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Administradores do Grupo de Trabalho";
$MESS["DIRECTION_GROUP_DESC"] = "Conselho de Administração da Empresa.";
$MESS["DIRECTION_GROUP_NAME"] = "Conselho de Administração";
$MESS["EMPLOYEES_GROUP_DESC"] = "Todos os funcionários da empresa, registrados no portal.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Funcionários";
$MESS["INTEGRATOR_GROUP_NAME"] = "Parceiro Bitrix24";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Pessoal do departamento de Vendas e Marketing.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Vendas e Marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Pessoal do departamento de RH.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Departamento de RH";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Os Administradores do Portal têm acesso a todos os serviços do portal.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Administradores do Portal";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "Grupo de usuários autorizado a editar as preferências da loja on-line";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "Administradores da loja on-line";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "Grupo de usuários autorizado a usar os recursos da loja on-line";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "Equipe da loja on-line";
$MESS["SUPPORT_GROUP_DESC"] = "Especialista em assistência técnica.";
$MESS["SUPPORT_GROUP_NAME"] = "Assistência Técnica";
?>