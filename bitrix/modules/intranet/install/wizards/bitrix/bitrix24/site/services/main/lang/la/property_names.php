<?
$MESS["UF_1C"] = "Usuario desde 1C";
$MESS["UF_CRM_CAL_EVENT"] = "Elemento CRM";
$MESS["UF_CRM_TASK"] = "Elemento CRM";
$MESS["UF_DEPARTMENT"] = "Departamentos";
$MESS["UF_DISTRICT"] = "Distrito";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "NIF";
$MESS["UF_INTERESTS"] = "Intereses";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Teléfono interno";
$MESS["UF_SKILLS"] = "Experiencia";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_STATE_HISTORY"] = "Historia del estado";
$MESS["UF_STATE_LAST"] = "Último estado";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Otros sitios web";
$MESS["UF_XING"] = "Xing";
?>