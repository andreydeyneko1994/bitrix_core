<?php
$MESS["CAL_TYPE_GROUP_NAME"] = "Calendario de grupos";
$MESS["CAL_TYPE_LOCATION_NAME"] = "Disponibilidad de la sala de reuniones";
$MESS["CAL_TYPE_USER_NAME"] = "Calendario de usuarios";
$MESS["CAL_YEAR_HOLIDAYS"] = "01.01,04.07,01.11,25.12";
$MESS["EC_COMPANY_CALENDAR_"] = "Calendario de la compañía";
$MESS["EC_COMPANY_CALENDAR_GOV_ORGANIZATION"] = "Calendario de organización";
$MESS["EC_COMPANY_CALENDAR_PUBLIC_ORGANIZATION"] = "Calendario de organización";
$MESS["W_IB_CALENDAR_EMP_ABS"] = "Reunión con un cliente en su oficina";
