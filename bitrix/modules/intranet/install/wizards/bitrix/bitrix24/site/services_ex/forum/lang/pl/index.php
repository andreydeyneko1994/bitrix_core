<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Komentarze: Ekstranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Komentarze Do Plików Użytkownika: Ekstranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Komentarze Do Użytkowników i Grupowych Foto Galerii: Ekstranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Komentarze Do Zadań Użytkowników i Grup: Ekstranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Ukryte Fora: Ekstranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Fora poszczególnych użytkowników i grup strony ekstranetu.";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Forum Użytkowników i Grup: Ekstranet";
?>