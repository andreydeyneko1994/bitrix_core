<?
$MESS["EXTRANET_NEW_MESSAGE_MESSAGE"] = "Powitanie z #SITE_NAME#!
------------------------------------------

Witaj #USER_NAME#!

Otrzymałeś nową wiadomość od #SENDER_NAME# #SENDER_LAST_NAME#:

------------------------------------------
#MESSAGE#
------------------------------------------

Link do wiadomości:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

To powiadomienie zostało wysłane automatycznie.
";
$MESS["EXTRANET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Masz nową wiadomość";
?>