<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Administratorzy posiadają pełen dostęp do zarządzania i edycji stron ekstranetowych.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Ekstranetowi Administratorzy Strony";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Wszyscy użytkownicy, którzy mają uprawnienia do tworzenia grup użytkowników na stronie ekstranetu.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Zezwolono na tworzenie ekstranetowych grup użytkownika";
$MESS["EXTRANET_GROUP_DESC"] = "Wszyscy użytkownicy, którzy mają dostęp do strony ekstranetu.";
$MESS["EXTRANET_GROUP_NAME"] = "Użytkownicy Ekstranetu";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
$MESS["EXTRANET_SUPPORT_GROUP_DESC"] = "Osoby upoważnione do wsparcia na stronie ekstranetowej.";
$MESS["EXTRANET_SUPPORT_GROUP_NAME"] = "Wsparcie Ekstranetowe";
?>