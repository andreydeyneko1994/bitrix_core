<?
$MESS["UF_1C"] = "Usuário de 1C";
$MESS["UF_CRM_CAL_EVENT"] = "Itens de CRM";
$MESS["UF_CRM_TASK"] = "Itens de CRM";
$MESS["UF_DEPARTMENT"] = "Departamentos";
$MESS["UF_DISTRICT"] = "Distrito";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "INN";
$MESS["UF_INTERESTS"] = "Interesses";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Número de ramal";
$MESS["UF_SKILLS"] = "Habilidades";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_STATE_HISTORY"] = "Histórico de status";
$MESS["UF_STATE_LAST"] = "Último status";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Outros sites";
$MESS["UF_XING"] = "Xing";
?>