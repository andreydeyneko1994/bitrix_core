<?
$MESS["MEETING_DESCRIPTION"] = "Vamos ter que fazer uma reunião para discutir a implantação do portal de intranet em nossa organização.";
$MESS["MEETING_ITEM_TITLE_1"] = "Revisão das soluções disponíveis";
$MESS["MEETING_ITEM_TITLE_2"] = "Definir etapas de implementação; nomear pessoas responsáveis";
$MESS["MEETING_ITEM_TITLE_3"] = "Planos para o Natal";
$MESS["MEETING_PLACE"] = "Escritório do CEO";
$MESS["MEETING_TITLE"] = "Implantação do Portal de Intranet";
?>