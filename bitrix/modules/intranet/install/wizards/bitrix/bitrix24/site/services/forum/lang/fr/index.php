<?
$MESS["COMMENTS_GROUP_NAME"] = "Forum for comments";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Comments for Common Files library";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Public forum for the company employees. Discuss your business and exchange opinions here.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Attention ! The company employees can now maintain their file storage in their private area.

You will find the detailed information about file storage management and the method of mapping the storage to a network drive in the help section : \"My Profile - Files\".

Should you have any questions regarding file storage configuration, please send your requests to the techsupport engineers using the support request form.";
$MESS["GENERAL_FORUM_NAME"] = "General forum";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Portal News";
$MESS["GENERAL_GROUP_NAME"] = "General forums";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Comments for Files";
$MESS["HIDDEN_GROUP_NAME"] = "Hidden forums";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Photo gallery discussion";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Personal and group forums";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Users and groups ";
?>