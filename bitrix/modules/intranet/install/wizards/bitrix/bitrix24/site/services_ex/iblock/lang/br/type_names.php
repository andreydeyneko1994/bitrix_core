<?
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["EVENTS_SECTION_NAME"] = "Calendários";
$MESS["EVENTS_TYPE_NAME"] = "Calendário";
$MESS["LIBRARY_ELEMENT_NAME"] = "Arquivos";
$MESS["LIBRARY_SECTION_NAME"] = "Pastas";
$MESS["LIBRARY_TYPE_NAME"] = "Documentos";
$MESS["NEWS_ELEMENT_NAME"] = "Notícias";
$MESS["NEWS_TYPE_NAME"] = "Notícias";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["PHOTOS_SECTION_NAME"] = "Álbuns";
$MESS["PHOTOS_TYPE_NAME"] = "Galeria de Fotos";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_SECTION_NAME"] = "Seções";
$MESS["SERVICES_TYPE_NAME"] = "Serviços";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["STRUCTURE_SECTION_NAME"] = "Seções";
$MESS["STRUCTURE_TYPE_NAME"] = "Estrutura da empresa";
?>