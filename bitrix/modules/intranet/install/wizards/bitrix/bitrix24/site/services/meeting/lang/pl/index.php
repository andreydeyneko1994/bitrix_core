<?
$MESS["MEETING_DESCRIPTION"] = "Musimy zorganizować spotkanie aby przedyskutować utworzenie portalu intranetowego dla naszej firmy.";
$MESS["MEETING_ITEM_TITLE_1"] = "Przeglądnij istniejące rozwiązania";
$MESS["MEETING_ITEM_TITLE_2"] = "Zdefiniuj kroki implementacji; przypisz osoby odpowiedzialne";
$MESS["MEETING_ITEM_TITLE_3"] = "Plany na Święta";
$MESS["MEETING_PLACE"] = "Biuro Dyrektora Zarządzającego";
$MESS["MEETING_TITLE"] = "Utworzenie Portalu Intranetowego";
?>