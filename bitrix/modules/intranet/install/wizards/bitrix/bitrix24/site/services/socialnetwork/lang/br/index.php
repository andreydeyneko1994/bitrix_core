<?
$MESS["COMMENTS_GROUP_NAME"] = "Fórum para comentários";
$MESS["GENERAL_GROUP_NAME"] = "Fóruns gerais";
$MESS["HIDDEN_GROUP_NAME"] = "Fóruns ocultos";
$MESS["SONET_GROUP_DESCRIPTION_NEW_1"] = "Use este grupo para discutir projetos atuais. Novos membros precisam ser confirmados pelo proprietário ou administrador do grupo para obter acesso. Este grupo é visível para todos.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_2"] = "Use para discussões que exigem compartimentação (por exemplo, tópicos financeiros). Somente pessoas convidadas podem ter acesso. Este grupo é visível apenas para seus membros.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_3"] = "Este grupo é para todos. Discuta o que quiser: esporte, música, viagens, feriados. Este grupo é visível para todos; qualquer um pode participar dele, não é necessário nenhum convite.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_41"] = "Este grupo visível privado é para usuários externos. O grupo Extranet hospeda subcontratados, parceiros, clientes e outros usuários que não são funcionários da empresa. É necessário um convite para ingressar no grupo. Este grupo é o único visível para os usuários externos. Eles não podem visualizar ou acessar qualquer outra informação em seu Bitrix24.";
$MESS["SONET_GROUP_KEYWORDS_NEW_1"] = "projetos, rotina diária, pesquisa";
$MESS["SONET_GROUP_KEYWORDS_NEW_2"] = "mercado, mercadoria, dinheiro, finanças, concurso";
$MESS["SONET_GROUP_KEYWORDS_NEW_3"] = "esporte, viagens, diversão, futebol, música";
$MESS["SONET_GROUP_KEYWORDS_NEW_4"] = "projetos, autônomo, parceiros";
$MESS["SONET_GROUP_NAME_NEW_1"] = "Grupo privado visível";
$MESS["SONET_GROUP_NAME_NEW_2"] = "Grupo oculto privado ";
$MESS["SONET_GROUP_NAME_NEW_3"] = "Grupo público aberto";
$MESS["SONET_GROUP_NAME_NEW_4"] = "Extranet: grupo para usuários externos";
$MESS["SONET_GROUP_SUBJECT_0"] = "Grupos de trabalho";
$MESS["SONET_TASK_DESCRIPTION_1"] = "Preencha seus dados de perfil e carregue sua foto.";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Convide novos funcionários para participar do portal da intranet";
$MESS["SONET_TASK_TITLE_1"] = "Preencha os dados de perfil";
$MESS["SONET_TASK_TITLE_2"] = "Convide novos funcionários";
?>