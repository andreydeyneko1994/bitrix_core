<?
$MESS["REQUEST_DESCRIPTION_QUESTION"] = "Tekst zamówienia";
$MESS["REQUEST_NAME_QUESTION"] = "Tytuł zamówienia";
$MESS["REQUEST_TYPE_ANSWER1"] = "Dostawa kurierska";
$MESS["REQUEST_TYPE_ANSWER2"] = "Przepustki dla gości";
$MESS["REQUEST_TYPE_ANSWER3"] = "Wizytówki";
$MESS["REQUEST_TYPE_ANSWER4"] = "Materiały biurowe";
$MESS["REQUEST_TYPE_ANSWER5"] = "Akcesoria i dostawy";
$MESS["REQUEST_TYPE_QUESTION"] = "Typ zamówienia";
$MESS["SERVICE_ORDER_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_ORDER_FORM_NAME"] = "Dostawy i Usługi";
$MESS["URGENCY_QUESTION"] = "Priorytet lub termin";
?>