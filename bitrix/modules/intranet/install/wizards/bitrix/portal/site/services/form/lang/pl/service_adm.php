<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["ADM_TROUBLESHOOTING_MENU_NAME"] = "Wniosek działu konserwacji";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["REQUEST_NAME_QUESTION"] = "Tytuł zamówienia";
$MESS["REQUEST_TYPE_ANSWER1"] = "Telefon jest poza zasięgiem";
$MESS["REQUEST_TYPE_ANSWER2"] = "Naprawa mebli";
$MESS["REQUEST_TYPE_ANSWER3"] = "Serwis sprzątający";
$MESS["REQUEST_TYPE_ANSWER4"] = "inne";
$MESS["REQUEST_TYPE_QUESTION"] = "Typ zamówienia";
$MESS["SERVICE_ADM_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_ADM_FORM_NAME"] = "Wniosek działu konserwacji";
$MESS["TROUBLE_DESCRIPTION_QUESTION"] = "Opis problemu";
$MESS["URGENCY_QUESTION"] = "Priorytet lub termin";
?>