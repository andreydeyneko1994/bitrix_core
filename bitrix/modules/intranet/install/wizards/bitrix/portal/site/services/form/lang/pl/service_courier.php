<?
$MESS["ADDRESS_QUESTION"] = "Adres dostawy";
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["COURIER_DELIVERY_MENU_NAME"] = "Dostawa kurierska";
$MESS["DATE_QUESTION"] = "Data pobrania";
$MESS["DESCRIPTION_QUESTION"] = "Opis";
$MESS["SERVICE_COURIER_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_COURIER_FORM_NAME"] = "Dostawa kurierska";
$MESS["WEIGHT_QUESTION"] = "Waga (kg)";
?>