<?
$MESS["BIZPROC_DEMO1_ACCTS"] = "Fakturowanie";
$MESS["BIZPROC_DEMO1_ELEMENTS_NAME"] = "Fakturowanie";
$MESS["BIZPROC_DEMO1_ELEMENT_ADD"] = "Dodaj fakturę";
$MESS["BIZPROC_DEMO1_ELEMENT_DELETE"] = "Usuń fakturę";
$MESS["BIZPROC_DEMO1_ELEMENT_EDIT"] = "Edytuj fakturę";
$MESS["BIZPROC_DEMO1_ELEMENT_NAME"] = "Faktura";
$MESS["BIZPROC_DEMO_REST"] = "Wyświetl lub dodaj wniosek o urlop";
$MESS["BIZPROC_DEMO_REST_ADD"] = "Wniosek o urlop";
$MESS["BIZPROC_DEMO_TRIP"] = "Wyświetl lub dodaj wniosek o delegację";
$MESS["BIZPROC_DEMO_TRIP_ADD"] = "Utwórz wyjazd służbowy";
$MESS["BIZPROC_DEMO_TYPE_TITLE"] = "Proces Biznesowy";
?>