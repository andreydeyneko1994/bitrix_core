<?
$MESS["DIRECTION_FORM_NAME"] = "Usługi administracyjne";
$MESS["DIRECTION_ORDER_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["REQUEST_DESCRIPTION_QUESTION"] = "Tekst zamówienia";
$MESS["REQUEST_NAME_QUESTION"] = "Tytuł zamówienia";
$MESS["REQUEST_TYPE_ANSWER1"] = "Kierowcy";
$MESS["REQUEST_TYPE_ANSWER2"] = "Logistyka";
$MESS["REQUEST_TYPE_ANSWER3"] = "Rekrutacja";
$MESS["REQUEST_TYPE_ANSWER4"] = "Aranżacja miejsca pracy";
$MESS["REQUEST_TYPE_QUESTION"] = "Typ zamówienia";
$MESS["URGENCY_QUESTION"] = "Priorytet lub termin";
?>