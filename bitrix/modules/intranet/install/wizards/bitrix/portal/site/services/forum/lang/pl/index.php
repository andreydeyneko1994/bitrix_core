<?
$MESS["COMMENTS_GROUP_NAME"] = "Forum dla komentarzy";
$MESS["DOCS_DIRECTORS_COMMENTS_NAME"] = "Komentarze do biblioteki plików Kierownictwa";
$MESS["DOCS_SALES_COMMENTS_NAME"] = "Komentarze do biblioteki Sprzedaży i Marketingu";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Komentarze do biblioteki Wspólnych Plików";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Forum publiczne dla pracowników firmy. Przedyskutuj tutaj sprawy businessowe i wymieniaj opienie.";
$MESS["GENERAL_FORUM_NAME"] = "Forum główne";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Aktualności Portalu";
$MESS["GENERAL_GROUP_NAME"] = "Główne fora";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Komentarze do plików";
$MESS["HIDDEN_GROUP_NAME"] = "Ukryte forum";
$MESS["NEWS_COMMENTS_FORUM_NAME"] = "Aktualności dyskusji";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Dyskusja do galerii zdjęć";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Osobiste i grupowe fora";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Użytkownicy i grupy";
?>