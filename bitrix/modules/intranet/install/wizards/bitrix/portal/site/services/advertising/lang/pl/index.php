<?
$MESS["PORTAL_ADV_01_absence"] = "System Zarządzania Nieobscnościami";
$MESS["PORTAL_ADV_01_absence_url"] = "/help/absence_help.php";
$MESS["PORTAL_ADV_03_outlook"] = "Integracja z Microsoft Outlook";
$MESS["PORTAL_ADV_03_outlook_url"] = "/help/outlook.php";
$MESS["PORTAL_ADV_05_xmpp"] = "Komunikator XMPP";
$MESS["PORTAL_ADV_05_xmpp_url"] = "/help/xmpp.php";
$MESS["PORTAL_ADV_100_100_ONE"] = "Pierwszorzędny / Pierwszej strony baner 100x100";
$MESS["PORTAL_ADV_100_100_ONE_NAME"] = "Po raz pierwszy tutaj?";
$MESS["PORTAL_ADV_100_100_TWO"] = "Drugorzędny / Drugiej strony baner 100x100";
$MESS["PORTAL_ADV_468_60_BOTTOM"] = "Dolny baner 468x60";
$MESS["PORTAL_ADV_468_60_BOTTOM_NAME"] = "Po raz pierwszy tutaj?";
$MESS["PORTAL_ADV_468_60_TOP"] = "Górny baner 468x60";
$MESS["PORTAL_ADV_INFO"] = "Informacje";
$MESS["PORTAL_ADV_dashboard"] = "Spersonalizuj swój panel w kilka minut!";
?>