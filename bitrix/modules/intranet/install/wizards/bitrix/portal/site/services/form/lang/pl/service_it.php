<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["IT_TROUBLESHOOTING_MENU_NAME"] = "Wniosek działu IT";
$MESS["REQUEST_NAME_QUESTION"] = "Tytuł zamówienia";
$MESS["REQUEST_TYPE_ANSWER1"] = "Problemy z komputerem";
$MESS["REQUEST_TYPE_ANSWER2"] = "Problemy z urządzeniami peryferyjnymi";
$MESS["REQUEST_TYPE_ANSWER3"] = "Zapisz na dysku";
$MESS["REQUEST_TYPE_ANSWER4"] = "Doradztwo ogólne";
$MESS["REQUEST_TYPE_QUESTION"] = "Typ zamówienia";
$MESS["SERVICE_IT_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_IT_FORM_NAME"] = "Wniosek działu IT";
$MESS["TROUBLE_DESCRIPTION_QUESTION"] = "Opis problemu";
$MESS["URGENCY_QUESTION"] = "Priorytet lub termin";
?>