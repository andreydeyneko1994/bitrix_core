<?
$MESS["UF_1C"] = "Użytkownik z 1C";
$MESS["UF_DEPARTMENT"] = "Działy";
$MESS["UF_DISTRICT"] = "Dzielnica";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "INN";
$MESS["UF_INTERESTS"] = "Zainteresowania";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Numer wewnętrzny";
$MESS["UF_SKILLS"] = "Umiejętności";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_STATE_HISTORY"] = "State History";
$MESS["UF_STATE_LAST"] = "Last State";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Inne strony internetowe";
$MESS["UF_XING"] = "Xing";
?>