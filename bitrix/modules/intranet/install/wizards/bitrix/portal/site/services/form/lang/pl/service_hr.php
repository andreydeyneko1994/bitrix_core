<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["DATE_ACTUAL_QUESTION"] = "Ważne do";
$MESS["DEPARTMENT_QUESTION"] = "Dział";
$MESS["FUNCTIONS_QUESTION"] = "Zakres pracy";
$MESS["HR_REQUEST_MENU_NAME"] = "Rekrutacja";
$MESS["POSITION_QUESTION"] = "Stanowisko";
$MESS["REQUIREMENTS_QUESTION"] = "Wymagania wnioskodawcy";
$MESS["SALARY_QUESTION"] = "Pensja z";
$MESS["SERVICE_HR_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_HR_FORM_NAME"] = "Rekrutacja";
?>