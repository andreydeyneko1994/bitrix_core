<?php
$MESS["INTRANET_JS_CONTROL_BUTTON_CHAT"] = "Ouvrir le chat";
$MESS["INTRANET_JS_CONTROL_BUTTON_MEETING"] = "Créer un évènement";
$MESS["INTRANET_JS_CONTROL_BUTTON_NAME"] = "Appel vidéo HD";
$MESS["INTRANET_JS_CONTROL_BUTTON_POST"] = "Publier sur les Actualités";
$MESS["INTRANET_JS_CONTROL_BUTTON_TASK"] = "Créer une tâche";
$MESS["INTRANET_JS_CONTROL_BUTTON_VIDEOCALL"] = "Démarrer un appel vidéo";
