<?
$MESS["ISV_ERROR_dpt_not_empty"] = "Musisz przenieść wszystkich pracowników działu przed usunięciem działu.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> został przeniesiony do  <b>#DEPARTMENT#</b>.";
$MESS["ISV_move_department"] = "The <b>#DEPARTMENT#</b> wydział jest podległy w stosunku do <b>#DEPARTMENT_TO#</b>.";
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> został powołany na stanowisko szefa <b>#DEPARTMENT#</b> działu.";
?>