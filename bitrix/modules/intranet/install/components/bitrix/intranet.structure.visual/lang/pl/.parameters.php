<?
$MESS["INTR_ISV_PARAM_DETAIL_URL"] = "Strona struktury firmy";
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISV_PARAM_PATH_TO_VIDEO_CALL"] = "Strona rozmowy wideo";
$MESS["INTR_ISV_PARAM_PM_URL"] = "Okno adresowe natychmiastowych wiadomości";
$MESS["INTR_ISV_PARAM_PROFILE_URL"] = "Strona profilowa osobista";
$MESS["INTR_ISV_PARAM_SHOW_LOGIN"] = "Pokaż Login, jeżeli nie są dostępne wymagane pola nazwy użytkownika";
$MESS["INTR_ISV_PARAM_USE_USER_LINK"] = "Wyświetl karty informacyjne użytkowników";
?>