<?php
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE"] = "Installer le profil";
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE_IN_SETTINGS"] = "Dans les paramètres du téléphone, installez le profil que vous avez téléchargé";
$MESS["CAL_IOS_GUIDE_IS_INSTALLED_ON_DEVICE"] = "Le calendrier Bitrix24 est maintenant synchronisé avec le calendrier de votre téléphone !";
$MESS["CAL_IOS_GUIDE_IS_SUCCESS"] = "Réussi !";
$MESS["CAL_IOS_GUIDE_PREVIEW_INFO"] = "Pour synchroniser le calendrier Bitrix24 sur un appareil IOS, téléchargez un profil bitrix24 et installez-le sur votre appareil. Le profil comprend tous les paramètres nécessaires à la synchronisation d'un calendrier mobile.";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD"] = "Télécharger le profil";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD_BITRIX"] = "Télécharger le profil Bitrix24";
$MESS["CAL_IOS_GUIDE_READ_HOW_IT_WORK"] = "Comment ça fonctionne";
$MESS["EC_CALENDAR_IOS_GUIDE_IMAGEFILE"] = "ios_guide_en.gif";
