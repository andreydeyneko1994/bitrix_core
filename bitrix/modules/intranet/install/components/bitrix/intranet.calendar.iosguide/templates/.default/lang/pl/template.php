<?php
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE"] = "Zainstaluj profil";
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE_IN_SETTINGS"] = "Zainstaluj pobrany profil w ustawieniach telefonu";
$MESS["CAL_IOS_GUIDE_IS_INSTALLED_ON_DEVICE"] = "Kalendarz Bitrix24 jest teraz zsynchronizowany z kalendarzem telefonu!";
$MESS["CAL_IOS_GUIDE_IS_SUCCESS"] = "Sukces!";
$MESS["CAL_IOS_GUIDE_PREVIEW_INFO"] = "Aby zsynchronizować kalendarz Bitrix24 na urządzeniu z systemem iOS, pobierz i zainstaluj profil bitrix24 na urządzeniu. Profil zawiera wszystkie parametry wymagane do synchronizacji kalendarza mobilnego.";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD"] = "Pobierz profil";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD_BITRIX"] = "Pobierz profil Bitrix24";
$MESS["CAL_IOS_GUIDE_READ_HOW_IT_WORK"] = "Jak to działa";
$MESS["EC_CALENDAR_IOS_GUIDE_IMAGEFILE"] = "ios_guide_en.gif";
