<?php
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE"] = "Instalar perfil";
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE_IN_SETTINGS"] = "Nas configurações do telefone, instale o perfil que você baixou";
$MESS["CAL_IOS_GUIDE_IS_INSTALLED_ON_DEVICE"] = "Agora o calendário Bitrix24 está sincronizado com o calendário do seu telefone!";
$MESS["CAL_IOS_GUIDE_IS_SUCCESS"] = "Bem sucedido!";
$MESS["CAL_IOS_GUIDE_PREVIEW_INFO"] = "Para sincronizar o calendário Bitrix24 em um dispositivo IOS, baixe e instale um perfil bitrix24 em seu dispositivo. O perfil inclui todos os parâmetros necessários para sincronizar um calendário móvel.";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD"] = "Baixar perfil";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD_BITRIX"] = "Baixe o perfil Bitrix24";
$MESS["CAL_IOS_GUIDE_READ_HOW_IT_WORK"] = "Como funciona";
$MESS["EC_CALENDAR_IOS_GUIDE_IMAGEFILE"] = "ios_guide_en.gif";
