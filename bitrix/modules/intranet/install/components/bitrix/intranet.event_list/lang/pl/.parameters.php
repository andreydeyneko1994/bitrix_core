<?
$MESS["ECL_GROUP_BASE_SETTINGS"] = "Główne Ustawienia";
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "Uwzględnij ulubione kalendarze";
$MESS["ECL_P_CACHE_TIME"] = "Żywotność pamięci podręcznej (sek.)";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "Pokaż wydarzenia bieżącego użytkownika";
$MESS["ECL_P_DETAIL_URL"] = "URL Szczegółowego widoku strony";
$MESS["ECL_P_EVENTS_COUNT"] = "Wydarzenia na liście";
$MESS["ECL_P_EVENT_LIST_MODE"] = "Pokaż tylko listę wydarzeń";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "Pokaż najwcześniejsze wydarzenia dla (miesięcy)";
$MESS["ECL_P_IBLOCK"] = "Blok informacji";
$MESS["ECL_P_IBLOCK_SECTION_ID"] = "ID sekcji bloku infromacji";
$MESS["ECL_P_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["ECL_P_INIT_DATE"] = "Zainicjowany";
$MESS["ECL_P_LOAD_MODE"] = "Załaduj wydarzenia";
$MESS["ECL_P_SHOW_CUR_DATE"] = "Bieżąca Data";
$MESS["ECL_P_USER_IBLOCK_ID"] = "Blok informacji dla kalendarzy użytkowników";
?>