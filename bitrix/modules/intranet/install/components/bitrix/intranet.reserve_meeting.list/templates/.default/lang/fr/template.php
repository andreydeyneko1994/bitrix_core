<?
$MESS["INTASK_C23T_LOAD"] = "En cours de chargement...";
$MESS["INTDT_ACTIONS"] = "Actions";
$MESS["INTDT_GRAPH"] = "Graphique";
$MESS["INTDT_NO_TASKS"] = "Il n'y a pas de salles de réunion";
$MESS["INTST_CANCEL"] = "Annuler";
$MESS["INTST_CLOSE"] = "Fermer";
$MESS["INTST_DELETE"] = "Supprimer";
$MESS["INTST_FOLDER_NAME"] = "Dénomination du dossier";
$MESS["INTST_SAVE"] = "Enregistrer";
?>