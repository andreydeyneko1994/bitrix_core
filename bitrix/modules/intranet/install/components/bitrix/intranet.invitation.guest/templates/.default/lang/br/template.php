<?php
$MESS["INTRANET_INVITATION_GUEST_ADD_MORE"] = "Adicionar mais";
$MESS["INTRANET_INVITATION_GUEST_EMPTY_DATA"] = "Campos do formulário estão vazios.";
$MESS["INTRANET_INVITATION_GUEST_ENTER_EMAIL"] = "inserir e-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_EMAIL"] = "E-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_LAST_NAME"] = "Sobrenome";
$MESS["INTRANET_INVITATION_GUEST_FIELD_NAME"] = "Nome";
$MESS["INTRANET_INVITATION_GUEST_HINT"] = "Você também pode adicionar seu parceiro ou cliente por e-mail.";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON"] = "Convidar";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON_MORE"] = "Convidar mais usuários";
$MESS["INTRANET_INVITATION_GUEST_TITLE"] = "Enviar convite por e-mail";
$MESS["INTRANET_INVITATION_GUEST_WRONG_DATA"] = "Alguns ou todos os campos do formulário estão incorretos.";
