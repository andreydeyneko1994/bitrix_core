<?php
$MESS["INTRANET_INVITATION_GUEST_ADD_MORE"] = "Dodaj więcej";
$MESS["INTRANET_INVITATION_GUEST_EMPTY_DATA"] = "Pola formularza są puste.";
$MESS["INTRANET_INVITATION_GUEST_ENTER_EMAIL"] = "wprowadź adres e-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_EMAIL"] = "E-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_LAST_NAME"] = "Nazwisko";
$MESS["INTRANET_INVITATION_GUEST_FIELD_NAME"] = "Imię";
$MESS["INTRANET_INVITATION_GUEST_HINT"] = "Możesz również dodać partnera lub klienta za pośrednictwem poczty e-mail.";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON"] = "Zaproś";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON_MORE"] = "Zaproś więcej użytkowników";
$MESS["INTRANET_INVITATION_GUEST_TITLE"] = "Wyślij zaproszenie na adres e-mail";
$MESS["INTRANET_INVITATION_GUEST_WRONG_DATA"] = "Niektóre lub wszystkie pola formularza są nieprawidłowe.";
