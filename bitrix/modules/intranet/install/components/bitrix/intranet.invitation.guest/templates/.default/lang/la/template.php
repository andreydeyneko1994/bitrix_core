<?php
$MESS["INTRANET_INVITATION_GUEST_ADD_MORE"] = "Agregar más";
$MESS["INTRANET_INVITATION_GUEST_EMPTY_DATA"] = "Los campos del formulario están vacíos.";
$MESS["INTRANET_INVITATION_GUEST_ENTER_EMAIL"] = "introducir correo electrónico";
$MESS["INTRANET_INVITATION_GUEST_FIELD_EMAIL"] = "Correo electrónico";
$MESS["INTRANET_INVITATION_GUEST_FIELD_LAST_NAME"] = "Apellido";
$MESS["INTRANET_INVITATION_GUEST_FIELD_NAME"] = "Nombre";
$MESS["INTRANET_INVITATION_GUEST_HINT"] = "También puede agregar a su socio o cliente mediante un correo electrónico.";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON"] = "Invitar";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON_MORE"] = "Invitar a más usuarios";
$MESS["INTRANET_INVITATION_GUEST_TITLE"] = "Enviar invitación por correo electrónico";
$MESS["INTRANET_INVITATION_GUEST_WRONG_DATA"] = "Algunos o todos los campos del formulario son incorrectos.";
