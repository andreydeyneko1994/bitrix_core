<?php
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Viernes";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Lunes";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Sábado";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Domingo";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Jueves";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Martes";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Miércoles";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Cargar Logo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Eliminar Logo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "¿Estás seguro que quiere eliminar el logotipo?";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permitir a todos invitar a nuevos usuarios a este cuenta de Bitrix24";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Notificar acerca de las nuevas contrataciones en el noticias";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Permitir registro rápido";
$MESS["CONFIG_ALLOW_TOALL"] = "Permitir \"Todos los usuarios\" como opción en el noticias";
$MESS["CONFIG_BUY_TARIFF_BY_ALL"] = "Permitir a todos los usuarios actualizar el plan actual de Bitrix24";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de la compañía";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Su logotipo debe ser un <b>PNG</b> file.<br/>Las dimensiones máximas son 222px x 55px.";
$MESS["CONFIG_CLIENT_LOGO_DESC_RETINA"] = "Su logotipo debe ser un <b>PNG</b> file.<br/>Las dimensiones máximas son 444px x 110px.";
$MESS["CONFIG_CLIENT_LOGO_RETINA"] = "Logotipo de DPI alto (retina)";
$MESS["CONFIG_COLLECT_GEO_DATA"] = "Habilitar la recopilación de los datos de geolocalización";
$MESS["CONFIG_COLLECT_GEO_DATA_CONFIRM"] = "Tenga en cuenta que en algunas jurisdicciones se necesita el consentimiento explícito del usuario para procesar los datos de geolocalización del dispositivo";
$MESS["CONFIG_COLLECT_GEO_DATA_OK"] = "Aceptar";
$MESS["CONFIG_COMPANY_NAME"] = "Nombre de la compañía";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nombre de la compañía para mostrar en el encabezado";
$MESS["CONFIG_CREATE_OVERDUE_CHATS"] = "Crear chat en tareas vencidas con todos los participantes de la tarea";
$MESS["CONFIG_CULTURE_OTHER"] = "Otros";
$MESS["CONFIG_DATE_FORMAT"] = "Formato de fecha";
$MESS["CONFIG_DEFAULT_TOALL"] = "Utilizar \"Todos los usuarios\" como destinatario predeterminado";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Genera automáticamente archivos PDF y JPG para documentos";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Permitir a los usuarios involucrados editar documentos adjuntos a publicaciones, <br>tareas, comentarios, etc. (el acceso se puede configurar manualmente en <br>Mi Drive - Archivos cargados en cualquier momento)";
$MESS["CONFIG_DISK_ALLOW_USE_EXTENDED_FULLTEXT"] = "Buscar en documentos de Drive";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Permitir enlaces públicos";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Genera automáticamente archivos MP4 y JPG para medios de video";
$MESS["CONFIG_DISK_EXTENDED_FULLTEXT_INFO"] = "La búsqueda no está disponible temporalmente. Póngase en contacto con el servicio de asistencia para habilitar la búsqueda.";
$MESS["CONFIG_DISK_LIMIT_HISTORY_LOCK_POPUP_TEXT"] = "El historial de documentos ilimitado está disponible en los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a>.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Disfrute aún más de las funciones útiles de Bitrix24 con Advanced Drive:<br/><br/>
+ Historial de actualización de documentos (modificado cuando y por quién)<br/>
+ Recuperar cualquier versión del documento anterior en el historial<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Leer más</a><br/><br/>
Advanced Drive está disponible en el plan \"Standard\" y superior.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponible sólo en Bitrix24.Drive extendido";
$MESS["CONFIG_DISK_LOCK_EXTENDED_FULLTEXT_POPUP_TEXT"] = "Disponible en planes comerciales seleccionados";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Características Avanzadas del Drive:

<br/><br/>
- Bloquee los archivos para evitar que otros puedan editar los documentos que está trabajando actualmente.
<br/><br/>
- Deshabilitar vínculos públicos para evitar que otros usuarios puedan compartir archivos públicamente con usuarios fuera de su cuenta de Bitrix24
<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Learn More</a>
<br/><br/>
Las funciones avanzadas de Bitrix24.Drive están disponibles en suscripciones comerciales que comienzan con el plan Standard de Bitrix24.
";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponible sólo en avanzadas Bitrix24 Drive";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Permitir bloqueo de documentos";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Convertir archivo tan pronto como se abra";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Entradas máx. en el historial del documento";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Ver documentos utilizando";
$MESS["CONFIG_EMAIL_FROM"] = "Correo electrónico del administrador del sitio web<br>(dirección de remitente predeterminada)";
$MESS["CONFIG_EXAMPLE"] = "Ejemplo";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_LISTS"] = "Lista";
$MESS["CONFIG_FEATURES_MEETING"] = "Reuniones y sesiones informativas";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Flujos de trabajo administrativos";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gestión del tiempo y Reportes de trabajo";
$MESS["CONFIG_FEATURES_TITLE"] = "Servicios";
$MESS["CONFIG_GDRP_APP1"] = "GDPR para empleados";
$MESS["CONFIG_GDRP_APP2"] = "GDPR para CRM";
$MESS["CONFIG_GDRP_LABEL1"] = "Actualizaciones de productos, ofertas especiales, invitaciones a webinarios, <br>resúmenes de boletines";
$MESS["CONFIG_GDRP_LABEL2"] = "Materiales de entrenamiento";
$MESS["CONFIG_GDRP_LABEL3"] = "Acepto el Acuerdo de Procesamiento de Datos";
$MESS["CONFIG_GDRP_LABEL4"] = "Nombre legal del cliente*";
$MESS["CONFIG_GDRP_LABEL5"] = "Nombre del contacto principal*";
$MESS["CONFIG_GDRP_LABEL6"] = "Título*";
$MESS["CONFIG_GDRP_LABEL7"] = "Fecha*";
$MESS["CONFIG_GDRP_LABEL8"] = "Dirección de correo electrónico de notificación*";
$MESS["CONFIG_GDRP_LABEL11"] = "(según el <a href=\"https://www.bitrix24.ru/about/advertising.php\" target=\"_blank\">Acuerdo</a>)";
$MESS["CONFIG_GDRP_TITLE1"] = "Doy mi consentimiento para recibir correos electrónicos de Bitrix24 que contengan:";
$MESS["CONFIG_GDRP_TITLE2"] = "Si acepta el Acuerdo de procesamiento de datos, también debe proporcionar la siguiente información:";
$MESS["CONFIG_GDRP_TITLE3"] = "Aplicaciones de procesamiento de datos personales:";
$MESS["CONFIG_HEADER_GDRP"] = "Conformidad GDPR";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Configuraciones de seguridad";
$MESS["CONFIG_HEADER_SETTINGS"] = "Configuraciones";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permitir a los usuarios enviar mensajes en el chat general";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_ADMIN_RIGHTS"] = "Habilitar notificaciones de asignación y cambio de administrador al chat general";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Notificar sobre nuevos empleados en el chat general";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Notificar sobre empleados/usuarios desactivado en el chat general";
$MESS["CONFIG_IP_HELP_TEXT2"] = "Los usuarios sin permisos de administrador no podrán acceder a este Bitrix24 desde ninguna dirección IP que no coincida con el rango especificado.
Una vez que haya iniciado sesión, el usuario verá una página de error que le informa que se denegó el acceso desde su IP.
Puede ver los intentos de iniciar sesión en Bitrix24 desde cualquier dirección IP en el registro de eventos.";
$MESS["CONFIG_IP_TITLE"] = "Restricciones de IP (permiten el acceso sólo desde direcciones IP especificadas o rangos de direcciones. Ejemplo: 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_LIMIT_MAX_TIME_IN_DOCUMENT_HISTORY"] = "Las versiones del documento se guardan durante #NUM# días.";
$MESS["CONFIG_LOCATION_ADDRESS_FORMAT"] = "Formato de la dirección";
$MESS["CONFIG_LOCATION_DEFAULT_SOURCE"] = "Fuente predeterminada";
$MESS["CONFIG_LOCATION_SOURCES_SETTINGS"] = "Configuración de las fuentes de dirección y ubicación";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_BACKEND"] = "API de Google Places y clave del servidor de API Geocoding";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "API JavaScript de Google Maps, API de Places y clave de navegador de API Geocoding";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_NOTE"] = "Se requiere una clave API de Google para usar los mapas. Para obtener su clave, <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utilice este formulario</a>.";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "Muestra fotos de Google para la vista de mapa de Tus lugares (Google puede cobrar una tarifa adicional por esta opción)";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "Utilice el servicio Geocoding para direcciones desconocidas (Google puede cobrar una tarifa adicional por esta opción)";
$MESS["CONFIG_LOCATION_SOURCE_OSM_SERVICE_URL"] = "URL del servicio";
$MESS["CONFIG_LOGO_24"] = "Agregar \"24\" al logotipo de la compañía";
$MESS["CONFIG_MARKETPLACE_MORE"] = "Leer más";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrar datos a Bitrix24 de otros sistemas";
$MESS["CONFIG_MORE"] = "Leer más";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL"] = "Permitir a los usuarios instalar aplicaciones desde Marketplace24";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL1"] = "Permitir que los usuarios instalen aplicaciones";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Cambiar";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Nota: sólo puede cambiar la dirección de Bitrix24 una vez.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Cambiar la dirección de Bitrix24";
$MESS["CONFIG_NAME_CURRENT_MAP_PROVIDER"] = "Proveedor de mapas actual";
$MESS["CONFIG_NAME_FILEMAN_GOOGLE_API_KEY"] = "Clave de Google Maps especificada en la configuración del módulo Site Explorer";
$MESS["CONFIG_NAME_FORMAT"] = "Formato de nombre";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "La clave se obtuvo para el dominio <b>#domain#</b>. Si no puede hacer que Google Maps funcione, cambie la configuración de la clave u <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">obtenga una nueva</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Preferencias de integración de Google API";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API de Google Maps para clave de integración Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD2"] = "API JavaScript de Google Maps, API de Places y clave de navegador de API Geocoding";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "Se requiere una clave de API de Google para usar mapas. Para obtener tu clave, por favor <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">usa este formulario</a>.";
$MESS["CONFIG_NAME_MAP_PROVIDER_SETTINGS"] = "Configuración del proveedor #PROVIDER#";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Permitir a los usuarios de mi Bitrix24 comunicarse en la red global de Bitrix24 Network";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Esta función estará disponible una vez que el administrador haya confirmado la cuenta.";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "La comunicación en Bitrix24.Network está disponible solo para los usuarios comerciales de Bitrix24.<br/><br/>
Estas son las ventajas de conectarse en Bitrix24.Network:<br/>
<ul>
<li>Todos sus contactos y socios comerciales estarán conectados a una red</li>
<li>Comunicaciones rápidas y convenientes con clientes y usuarios externos</li>
<li>Comunicaciones fluidas entre usuarios en diferentes cuentas de Bitrix24</li>
</ul>
<b>Todas las herramientas de Bitrix24.Network están disponibles a partir de Bitrix24 Plus a solo #PRICE# al mes.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponible solo en planes comerciales de Bitrix24";
$MESS["CONFIG_ORGANIZATION"] = "Tipo de organización";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Compañía y empleados";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Organización y empleados";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organización y usuarios";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Antes de migrar a sus empleados a un sistema de autenticación de dos pasos, configúrelo primero para su cuenta.<br/><br/>Por favor, proceda activándolo en la página Seguridad de su perfil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Hoy, utiliza su nombre de usuario y contraseña para iniciar sesión en Bitrix24. Los datos comerciales y personales están protegidos por la tecnología de cifrado de datos. Sin embargo, hay herramientas que una persona maliciosa puede emplear para entrar en su computadora y robar sus credenciales de inicio de sesión.

Buscando protección contra posibles amenazas, hemos habilitado una función de seguridad adicional para Bitrix24: la autenticación en dos pasos.

La autenticación en dos pasos es un método especial para protegerse contra el software de hackers, especialmente el robo de contraseñas. Cada vez que inicie sesión en el sistema, tendrá que pasar dos niveles de verificación. En primer lugar, ingresará su correo electrónico y contraseña, luego ingresará un código de seguridad único obtenido de su dispositivo móvil.

Esto hará que nuestros datos comerciales sean seguros incluso si el usuario y la contraseña de cualquier empleado son robados por un atacante.

Tienes 5 días para habilitar esta función.

Para configurar el nuevo procedimiento de autenticación, vaya a su perfil y seleccione la página \"Configuración de seguridad\".

Si no tiene un dispositivo móvil adecuado para ejecutar la aplicación, o si experimenta algún tipo de problema, háganoslo saber comentando esta publicación.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Autenticación en dos pasos";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "No, gracias";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Compartir";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Aquí está el texto que puede publicar en el noticias<br/>para que sus empleados lo lean.<br/><br/>
Deje que sus colegas conozcan la autenticación en dos pasos,<br/>el procedimiento de configuración y el nuevo método de autenticación<br/>que tendrán que usar para iniciar sesión en su Bitrix24.";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "¿Qué hay de publicar en el noticias?";
$MESS["CONFIG_OTP_SECURITY"] = "Habilitar la autenticación de dos pasos para todos los usuarios";
$MESS["CONFIG_OTP_SECURITY2"] = "Hacer obligatoria la autorización de dos pasos para todos los usuarios";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Especifique el período de tiempo dentro del cual todos los empleados<br/>tendrán que habilitar la autenticación de dos pasos";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Hemos desarrollado un procedimiento de habilitación de autenticación de dos pasos, fácil de usar, que cualquier empleado puede tomar sin la asistencia de un experto.<br/><br/>
Se enviará un mensaje a cada empleado notificándoles que tendrán que habilitar la autenticación de dos pasos dentro del período de tiempo ingresado. Los usuarios que no lo hagan
Por lo que no se le permitirá iniciar sesión más.";
$MESS["CONFIG_OTP_SECURITY_INFO_1"] = "<br/>Para habilitar la autenticación en dos pasos, el usuario debe instalar una aplicación Bitrix24 OTP en su teléfono móvil. Esta aplicación se puede descargar desde AppStore o GooglePlay.<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_2"] = "Los empleados que no tienen dispositivo móvil adecuado deben estar equipados con hardware especial - eToken Pass. Hay una gama de vendedores de los cuales puedes obtenerlo, por ejemplo:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>, 
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Encuentre otros proveedores en <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_3"] = "Como alternativa, puede deshabilitar la autenticación de dos pasos para algunos de los empleados. Sin embargo, esto aumentará el riesgo de acceso no autorizado a su Bitrix24 si el nombre de usuario y la contraseña de uno de esos empleados son robados. Como administrador, puede deshabilitar la autenticación de dos pasos para un empleado en el perfil de usuario.";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Tenga en cuenta que al desmarcar esta opción no se detendrá el uso de contraseñas de una sola vez por los usuarios que ya tienen habilitada la autenticación de dos pasos.
<br/><br/>Todavía tendrán que usar OTP al iniciar sesión en Bitrix24.
<br/><br/>Puede deshabilitar la autenticación de dos pasos en su perfil de usuario respectivo.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Formato del número de teléfono: país predeterminado";
$MESS["CONFIG_SAVE"] = "Guardar";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "Los ajustes se han actualizado correctamente";
$MESS["CONFIG_SEND_OTP_PUSH"] = "Enviar notificaciones de chat y push sobre el intento de autenticación, <br>incluido el código de inicio de sesión (solo para OTP basado en el tiempo)";
$MESS["CONFIG_SHOW_FIRED_EMPLOYEES"] = "Mostrar empleados desactivados";
$MESS["CONFIG_SHOW_YEAR"] = "Mostrar el año de nacimiento en el perfil del usuario";
$MESS["CONFIG_SHOW_YEAR_FOR_FEMALE"] = "Mostrar fecha de nacimiento en perfiles femeninos";
$MESS["CONFIG_STRESSLEVEL_AVAILABLE"] = "Permite medir y muestra el nivel de estrés en el perfil del usuario";
$MESS["CONFIG_TIME_FORMAT"] = "Formato de hora";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 horas";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 horas";
$MESS["CONFIG_TOALL_DEL"] = "eliminar";
$MESS["CONFIG_TOALL_RIGHTS"] = "Configuración de la opción \"Todos los empleados\"";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Agregar";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK"] = "Rastrear clics de enlaces en correos electrónicos salientes";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK_HINT"] = "Los disparadores de CRM pueden rastrear los clics de enlaces de usuario.
Con estos disparadores, puede crear reglas de automatización para enviar una notificación cuando un cliente o un empleado abre un enlace.";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ"] = "Rastrear el estado de lectura de los correos electrónicos salientes";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ_HINT"] = "El sistema rastreará si los mensajes salientes fueron realmente abiertos por sus respectivos destinatarios y marcará estos mensajes como leídos.
Esta opción está disponible para correos electrónicos enviados desde CRM, CRM Marketing, Bitrix24 Webmail y reglas de automatización. También habilita el disparador \"Ver correo electrónico\".";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Habilitar enlaces multimedia";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Conectar automáticamente Group Drive cuando<br/>el usuario se une al grupo";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Activar la edición de documentos por vía externa<br/>servicios (Google Docs, MS Office Online y otros)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Permitir que usuarios individuales y grupos activen la edición de documentos a través de servicios externos";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Fin de semana";
$MESS["CONFIG_WEEK_START"] = "Primer día de la semana";
$MESS["CONFIG_WORK_TIME"] = "Parámetros del tiempo de trabajo";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Fines de semana y feriados";
$MESS["DAY_OF_WEEK_0"] = "Domingo";
$MESS["DAY_OF_WEEK_1"] = "Lunes";
$MESS["DAY_OF_WEEK_2"] = "Martes";
$MESS["DAY_OF_WEEK_3"] = "Miércoles";
$MESS["DAY_OF_WEEK_4"] = "Jueves";
$MESS["DAY_OF_WEEK_5"] = "Viernes";
$MESS["DAY_OF_WEEK_6"] = "Sábado";
$MESS["config_rating_label_likeN"] = "Texto de botón \"Like\" votado";
$MESS["config_rating_label_likeY"] = "Texto de botón \"Like\" no votado";
