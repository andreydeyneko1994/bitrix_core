<?php
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Vendredi";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Lundi";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Samedi";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Dimanche";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Jeudi";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Mardi";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Mercredi";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Télécharger un logo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Supprimer le logo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer le logo ?";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permettre à tout le monde d'inviter de nouveaux utilisateurs à ce compte Bitrix24";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Poster les nouveaux évènements de recrutement d'employés dans le Flux d'activités";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Autoriser les inscriptions rapides";
$MESS["CONFIG_ALLOW_TOALL"] = "Autoriser l'envoi de messages à « Tous les employés » dans le flux d'actualités";
$MESS["CONFIG_BUY_TARIFF_BY_ALL"] = "Permettre à tous les utilisateurs de mettre à niveau le plan Bitrix24 actuel";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de l'entreprise";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Votre logo doit être au format <b>PNG</b> fichier.<br/>Les dimensions ne doivent pas excéder 222px x 55px.";
$MESS["CONFIG_CLIENT_LOGO_DESC_RETINA"] = "Votre logo doit être au format <b>PNG</b> fichier.<br/>Les dimensions ne doivent pas excéder 444px x 110px.";
$MESS["CONFIG_CLIENT_LOGO_RETINA"] = "Logo DPI élevé (retina)";
$MESS["CONFIG_COLLECT_GEO_DATA"] = "Activer la récupération des données de géolocalisation";
$MESS["CONFIG_COLLECT_GEO_DATA_CONFIRM"] = "Veuillez noter que, dans certaines juridictions, il vous faut le consentement explicite de l'utilisateur pour traiter les données de géolocalisation d'un appareil";
$MESS["CONFIG_COLLECT_GEO_DATA_OK"] = "Accepter";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de l'entreprise";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nom de l'entreprise à afficher en en-tête";
$MESS["CONFIG_CREATE_OVERDUE_CHATS"] = "Créer un chat sur la tâche en retard avec tous les participants";
$MESS["CONFIG_CULTURE_OTHER"] = "Autre";
$MESS["CONFIG_DATE_FORMAT"] = "Format de la date";
$MESS["CONFIG_DEFAULT_TOALL"] = "Choisir « Tous les employés » comme destinataire par défaut";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Générer automatiquement des fichiers PDF et JPG pour documents";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Tous les utilisateurs impliqués peuvent éditer les documents attachés aux discussions, <br>tâches, commentaires et autres évènements. Ce comportement par défaut peut être <br/>changé individuellement dans n'importe quel évènement";
$MESS["CONFIG_DISK_ALLOW_USE_EXTENDED_FULLTEXT"] = "Rechercher des documents de lecteur";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Autoriser les liens publics";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Générer automatiquement des fichiers MP4 et JPG pour supports vidéo";
$MESS["CONFIG_DISK_EXTENDED_FULLTEXT_INFO"] = "La recherche est temporairement indisponible. Veuillez contacter le service d'assistance pour activer la recherche.";
$MESS["CONFIG_DISK_LIMIT_HISTORY_LOCK_POPUP_TEXT"] = "L'historique de documents illimité est disponible avec les <a href=\"/settings/license_all.php\" target=\"_blank\">offres commerciales sélectionnées</a>.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Profitez d'encore plus de fonctionnalités utiles Bitrix24 avec Advanced Drive : <br/><br/>
+ Historique de mise à jour des documents (modifié quand et par qui)<br/>
+ Récupération de toute version précédente d'un document depuis l'historique<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>En savoir plus</a><br/><br/>
Advanced Drive est disponible dans l'abonnement \"Standard\" et dans les abonnements supérieurs.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponible uniquement dans la version étendue de Bitrix24.Drive";
$MESS["CONFIG_DISK_LOCK_EXTENDED_FULLTEXT_POPUP_TEXT"] = "Disponible sur certains plans commerciaux";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Fonctionnalités avancées de Drive:

<br/><br/>
- Verrouillage des fichiers pour empêcher d'autres personnes d'éditer les documents sur lesquels vous travailler.
<br/><br/>
- Désactivation des liens publics pour empêcher d'autres personnes de partager publiquement les fichiers avec des utilisateurs extérieurs à votre compte Bitrix24
<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>En savoir plus</a>
<br/><br/>
Les fonctionnalités avancées de Bitrix24.Drive sont disponibles pour les abonnements commerciaux à partir de l'offre Bitrix24 Standard.
";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponible uniquement dans la version avancée de Bitrix24 Drive";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Autoriser le verrouillage de document";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Convertir le fichier dès qu'il est ouvert";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Entrées max. dans l'historique de document";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Afficher les documents avec";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail de l'administrateur du site <br>(adresse de l'expéditeur par défaut)";
$MESS["CONFIG_EXAMPLE"] = "Exemple";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_LISTS"] = "Listes";
$MESS["CONFIG_FEATURES_MEETING"] = "Réunions et briefings";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Flux de travail administratifs";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gestion du temps et rapport d'activité";
$MESS["CONFIG_FEATURES_TITLE"] = "Services";
$MESS["CONFIG_GDRP_APP1"] = "RGPD pour les employés";
$MESS["CONFIG_GDRP_APP2"] = "RGPD pour le CRM";
$MESS["CONFIG_GDRP_LABEL1"] = "Mises à jour de produits, offres spéciales, invitations à des webinaires, <br>bulletins d'information";
$MESS["CONFIG_GDRP_LABEL2"] = "Matériels de formation";
$MESS["CONFIG_GDRP_LABEL3"] = "J'accepte le Contrat de traitement des données";
$MESS["CONFIG_GDRP_LABEL4"] = "Nom légal du client*";
$MESS["CONFIG_GDRP_LABEL5"] = "Nom du contact principal*";
$MESS["CONFIG_GDRP_LABEL6"] = "Titre*";
$MESS["CONFIG_GDRP_LABEL7"] = "Date*";
$MESS["CONFIG_GDRP_LABEL8"] = "Adresse e-mail de notification*";
$MESS["CONFIG_GDRP_LABEL11"] = "(selon <a href=\"https://www.bitrix24.ru/about/advertising.php\" target=\"_blank\">Accord</a>)";
$MESS["CONFIG_GDRP_TITLE1"] = "Je consens à recevoir des e-mails de Bitrix24 contenant : ";
$MESS["CONFIG_GDRP_TITLE2"] = "Si vous acceptez le Contrat de traitement des données, vous devez également fournir les informations suivantes : ";
$MESS["CONFIG_GDRP_TITLE3"] = "Applications de traitement de données personnelles : ";
$MESS["CONFIG_HEADER_GDRP"] = "Conformité RGPD";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Paramètres de sécurité";
$MESS["CONFIG_HEADER_SETTINGS"] = "Paramètres";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permet aux utilisateurs d'envoyer des messages dans Chat général";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_ADMIN_RIGHTS"] = "Activer le message automatique d'assignation et de changement d'administrateur<br>dans Chat général";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Informer des nouvelles recrues dans Chat général";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Informer des renvois d'employés/utilisateurs dans Chat général";
$MESS["CONFIG_IP_HELP_TEXT2"] = "Les utilisateurs sans autorisations d'administrateur ne pourront pas accéder à ce compte Bitrix24 à partir d'une adresse IP qui ne correspond pas à la plage spécifiée. Une fois connecté, l'utilisateur verra une page d'erreur l'informant que l'accès à partir de son IP a été refusé. Vous pourrez suivre les tentatives de connexion à Bitrix24 à partir d'autres adresses IP dans Log des événements.";
$MESS["CONFIG_IP_TITLE"] = "Restrictions IP (permettre l'accès uniquement à partir d'adresses IP spécifiées ou de domaines d'accès. Exemple : 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_LIMIT_MAX_TIME_IN_DOCUMENT_HISTORY"] = "Les versions des documents sont conservées pendant #NUM# jours.";
$MESS["CONFIG_LOCATION_ADDRESS_FORMAT"] = "Format d'adresse";
$MESS["CONFIG_LOCATION_DEFAULT_SOURCE"] = "Source par défaut";
$MESS["CONFIG_LOCATION_SOURCES_SETTINGS"] = "Paramètres des sources de localisation et d'adresse";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_BACKEND"] = "Clé serveur Google Places API et Geocoding API";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "Clé navigateur Google Maps JavaScript API, Places API et Geocoding API";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_NOTE"] = "Une clé API Google est nécessaire pour utiliser les cartes. Pour obtenir votre clé, veuillez <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utiliser ce formulaire</a>.";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "Afficher les photos Google pour l'affichage de la carte de vos lieux (Google peut facturer des frais supplémentaires pour cette option)";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "Utiliser le service de géocodage pour les adresses inconnues (Google peut facturer des frais supplémentaires pour cette option)";
$MESS["CONFIG_LOCATION_SOURCE_OSM_SERVICE_URL"] = "URL du service";
$MESS["CONFIG_LOGO_24"] = "Ajouter « 24 » au logo de l'entreprise";
$MESS["CONFIG_MARKETPLACE_MORE"] = "En savoir plus";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrer les données d'autres systèmes vers Bitrix24";
$MESS["CONFIG_MORE"] = "Lire la suite";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL"] = "Autoriser les utilisateurs à installer des applications à partir de Marketplace24";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL1"] = "Permettre aux utilisateurs d'installer des applications";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Modifier";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Remarque : vous ne pouvez modifier l'adresse de votre Bitrix24 qu'une seule fois.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Modifier l'adresse de votre Bitrix24";
$MESS["CONFIG_NAME_CURRENT_MAP_PROVIDER"] = "Fournisseur de cartes actuel";
$MESS["CONFIG_NAME_FILEMAN_GOOGLE_API_KEY"] = "Clé Google Maps indiquée dans les paramètres du module Site Explorer";
$MESS["CONFIG_NAME_FORMAT"] = "Format du nom";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "La clé a été obtenue pour le domaine <b>#domain#</b>. Si vous n'arrivez pas à faire fonctionner Google Maps, changez les paramètres de la clé ou <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">obtenez une nouvelle clé</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Préférences d'intégration de l'API Google";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API Google Maps pour clé d'intégration Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD2"] = "Clé navigateur Google Maps JavaScript API, Places API et Geocoding API";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "Une clé API Google est nécessaire pour utiliser les cartes. Pour obtenir votre clé, veuillez <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utiliser ce formulaire</a>.";
$MESS["CONFIG_NAME_MAP_PROVIDER_SETTINGS"] = "Paramètres du fournisseur #PROVIDER#";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Autoriser les utilisateurs de mon Bitrix24 à communiquer dans le réseau Bitrix24.Network";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Cette fonctionnalité sera disponible une fois que l'administrateur aura confirmé le compte.";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "La communication dans Bitrix24.Network est réservée aux utilisateurs commerciaux de Bitrix24.<br/><br/>
Voici les avantages de connecter Bitrix24.Network : <br/>
<ul>
<li>Tous vos contacts et partenaires professionnels sont connectés dans un réseau.</li>
<li>Communications rapides et pratiques avec les clients et les utilisateurs externes.</li>
<li>Communications continues entre les utilisateurs de différents comptes Bitrix24</li>
</ul>
<b>Tous les outils Bitrix24.Network sont disponibles à partir de Bitrix24 Plus pour #PRICE# par mois.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponible uniquement sur les plans commerciaux de Bitrix24";
$MESS["CONFIG_ORGANIZATION"] = "Type d'entreprise";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Entreprise et employés";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Entreprise et employés";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organisation et participants";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Avant de faire passer vos utilisateurs au système de validation en deux étapes, vous devrez préalablement configurer la connexion à votre propre compte. <br/><br/>L'activation se fait à partir de la page 'sécurité' de votre profil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Aujourd'hui, pour vous connecter à Bitrix24, vous utilisez votre identifiant et votre mot de passe. Les données sont, en outre, protégées par la technologie du cryptage. Cependant il existe des menaces telles que les programmes espions, tout à fait capables de s'introduire dans votre système et de voler vos identifiants et mots de passe et de les utiliser pour accéder au Bitrix24 de votre entreprise.                                                                                                                                              C'est pour cette raison qu'il est fortement recommandé d'activer la protection supplémentaire proposée par Bitrix24 : la validation en deux étapes.                                                                                                                                                                                                               La validation en deux étapes est une méthode de protection contre les logiciels espions. Lors de chaque connexion, vous devrez saisir votre email de référence (votre identifiant), votre mot de passe, ainsi qu'un code à usage unique qui vous sera envoyé sur votre téléphone mobile via une application.[IMG]/images/otp/ru/info.png[/IMG]                                                                                                                                      Cela créé une protection supplémentaire qui interdit l'accès à vos données, quand bien même des malfaiteurs seraient en possession du mot de passe et de l'identifiant de l'un de vos collaborateurs.                                                                                                                                                            Vous disposez de 5 jours pour activer cette fonction.                                                                                                                                                                                       Pour configurer la nouvelle procédure de validation, il suffit de vous rendre sur la page 'paramètres de sécurité' de votre profil.  Si vous n'avez pas la possibilité de télécharger l'application sur votre mobile, veuillez nous l'indiquer en commentaire à ce message. N'hésitez pas, également à nous faire part de difficultés rencontrées lors de l'activation de la validation en deux étapes.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Validation en 2 étapes";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "Non, merci";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Partager";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Vous pouvez publier le message suivant dans le flux d'activités<br/> pour communiquer à l'ensemble des utilisateurs.<br/><br/> Informez vos collègues au sujet de la validation en 2 étapes,<br/>afin qu'ils sachent comment paramétrer leur accès<br/> et qu'ils se familiarisent avec le nouveau mode de connexion à Bitrix24";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "Pourquoi ne pas publier ce message dans le flux d'activité ?";
$MESS["CONFIG_OTP_SECURITY"] = "Activer la validation en deux étapes pour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY2"] = "Faire basculer tous les utilisateurs vers la validation en deux étapes";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Veuillez indiquer la période durant laquelle tous les employés<br>devront activer la validation en deux étapes";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Une procédure d'accès en deux étapes de validation a été mise au point de manière à ce qu'elle soit accessible pour chaque utilisateur.<br/><br/> Chaque collaborateur reçoit un message lui indiquant qu'il dispose d'une période de temps limitée pour activer la validation en 2 étapes. Si au cours de cette période il ne configure pas la validation, il ne pourra plus s'identifier ensuite.";
$MESS["CONFIG_OTP_SECURITY_INFO_1"] = "<br/>Pour activer la validation en 2 étapes, l'utilisateur doit installer l'application Bitrix24 OTP sur son téléphone mobile. Cette application est téléchargeable sur Appstore ou GooglePlay.<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_2"] = "Les utilisateurs dont les téléphones ne peuvent accéder aux applications peuvent se procurer un dispositif eToken Pass. Ces authentificateurs sont disponibles sur internet, par exemple : 
<a target=_blank href='http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/'>www.safenet-inc.com</a>, 
<a target=_blank href='http://www.authguard.com/eToken-PASS.asp'>www.authguard.com</a>. Ou pour en savoir plus : <a target=_blank href='https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS'>Google</a>.
<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_3"] = "Il reste possible de désactiver l'authentification en 2 étapes pour certains utilisateurs, cependant il existera alors une probabilité d'accès frauduleux au Bitrix24 de l'entreprise si le mot de passe et l'identifiant de ces utilisateurs devaient être interceptés par une personne extérieure. La validation en deux étapes pour un utilisateur peut être désactivée par l'administrateur à partir de son profil.";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Attention ! Le fait de décocher cette option ne désactivera pas l'utilisation des codes à usage unique pour les utilisateurs ayant déjà activé la validation en deux étapes.
<br/><br/>Ils devront continuer à saisir le code pour se connecter à Bitrix24.
<br/><br/>La validation en deux étapes peut être désactivée à partir du profil de l'utilisateur concerné.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Format du numéro de téléphone : pays par défaut";
$MESS["CONFIG_SAVE"] = "Enregistrer";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "La mise à jour des paramètres a été effectuée avec succès.";
$MESS["CONFIG_SEND_OTP_PUSH"] = "Envoyer des notifications de chat et push sur la tentative d'authentification, <br>y compris un code de connexion (uniquement pour OTP basé sur le temps)";
$MESS["CONFIG_SHOW_FIRED_EMPLOYEES"] = "Afficher les employés licenciés";
$MESS["CONFIG_SHOW_YEAR"] = "Afficher la date de naissance dans le profil d'utilisateur";
$MESS["CONFIG_SHOW_YEAR_FOR_FEMALE"] = "Afficher la date de naissance dans les profils féminins";
$MESS["CONFIG_STRESSLEVEL_AVAILABLE"] = "Permettre de mesurer et d'afficher le niveau de stress dans le profil d'utilisateur";
$MESS["CONFIG_TIME_FORMAT"] = "Format de l'heure";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 heures";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 heures";
$MESS["CONFIG_TOALL_DEL"] = "Supprimer";
$MESS["CONFIG_TOALL_RIGHTS"] = "Paramètres de l'option 'Tous les employés'";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Ajouter";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK"] = "Suivre les clics de lien dans les e-mails sortants";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK_HINT"] = "Les déclencheurs CRM peuvent suivre les clics sur les liens des clients.
Avec ces déclencheurs, vous pouvez créer des règles d'automatisation pour envoyer une notification lorsqu'un client ou un employé ouvre un lien.";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ"] = "Suivre l'état de lecture des e-mails sortants";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ_HINT"] = "Le système suivra si les messages sortants ont été réellement ouverts par leurs destinataires respectifs, et marquera ces messages comme lus. Cette option est disponible pour les e-mails envoyés depuis CRM, Marketing CRM, Messagerie web de Bitrix24 et les règles d'automatisation. Cette option permet également utiliser le déclencheur \"E-mail consulté\".";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Activer les liens enrichis";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Connecter automatiquement le disque du groupe lorsque<br/>l'utilisateur rejoint le groupe";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Activer l'édition des documents via les services<br/>externes (Google Docs, MS Office Online et autres)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Autoriser les utilisateurs individuels ou les groupes à activer<br/>l'édition des documents via des services externes";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Weekend";
$MESS["CONFIG_WEEK_START"] = "Premier jour de la semaine";
$MESS["CONFIG_WORK_TIME"] = "Paramètres de temps de travail";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Weekends et jours fériés";
$MESS["DAY_OF_WEEK_0"] = "Dimanche";
$MESS["DAY_OF_WEEK_1"] = "Lundi";
$MESS["DAY_OF_WEEK_2"] = "Mardi";
$MESS["DAY_OF_WEEK_3"] = "Mercredi";
$MESS["DAY_OF_WEEK_4"] = "Jeudi";
$MESS["DAY_OF_WEEK_5"] = "Vendredi";
$MESS["DAY_OF_WEEK_6"] = "Samedi";
$MESS["config_rating_label_likeN"] = "Texte du bouton 'j'aime' après le vote";
$MESS["config_rating_label_likeY"] = "Texte du bouton « J'aime » avant le vote";
