<?php
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Sexta-feira";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Segunda-feira";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Sábado";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Domingo";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Quinta-feira";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Terça-feira";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Quarta-feira";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Carregar Logotipo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Excluir Logotipo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "Você tem certeza de que deseja excluir o logotipo?";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permitir a todos convidar novos usuários para esta conta Bitrix24";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Postar eventos de contratação de novo empregado em Feed";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Permitir cadastro rápido";
$MESS["CONFIG_ALLOW_TOALL"] = "Permitir \"Todos os funcionários\" como opção no Feed";
$MESS["CONFIG_BUY_TARIFF_BY_ALL"] = "Permitir que todos os usuários atualizem o plano atual Bitrix24";
$MESS["CONFIG_CLIENT_LOGO"] = "Logotipo da empresa";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Seu logotipo deve ser umarquivo <b>PNG</b>.<br/>As dimensões máximas são 222px x 55px.";
$MESS["CONFIG_CLIENT_LOGO_DESC_RETINA"] = "Seu logotipo deve ser umarquivo <b>PNG</b>.<br/>As dimensões máximas são 444px x 110px.";
$MESS["CONFIG_CLIENT_LOGO_RETINA"] = "Logotipo de alto DPI (retina)";
$MESS["CONFIG_COLLECT_GEO_DATA"] = "Ativar coleta de dados de geolocalização";
$MESS["CONFIG_COLLECT_GEO_DATA_CONFIRM"] = "Observe que em certas jurisdições você precisa da autorização explícita do usuário para processar dados de localização geográfica do dispositivo";
$MESS["CONFIG_COLLECT_GEO_DATA_OK"] = "Aceitar";
$MESS["CONFIG_COMPANY_NAME"] = "Nome da empresa";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nome da empresa para exibir no cabeçalho";
$MESS["CONFIG_CREATE_OVERDUE_CHATS"] = "Criar bate-papo em tarefas atrasadas com todos os participantes da tarefa";
$MESS["CONFIG_CULTURE_OTHER"] = "Outros";
$MESS["CONFIG_DATE_FORMAT"] = "Formato da data";
$MESS["CONFIG_DEFAULT_TOALL"] = "Usar \"Todos os funcionários\" como destinatário padrão";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Gerar automaticamente arquivos PDF e JPG para documentos";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Todos os usuários envolvidos podem editar documentos anexos a discussões, <br>tarefas, comentários ou outros eventos. Este comportamento padrão pode ser <br>alterado em quaisquer dos eventos individualmente.";
$MESS["CONFIG_DISK_ALLOW_USE_EXTENDED_FULLTEXT"] = "Pesquisar documentos do Drive";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Permitir links públicos";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Gerar automaticamente arquivos MP4 e JPG para mídia de vídeo";
$MESS["CONFIG_DISK_EXTENDED_FULLTEXT_INFO"] = "A pesquisa está temporariamente indisponível. Entre em contato com o suporte para ativar a pesquisa.";
$MESS["CONFIG_DISK_LIMIT_HISTORY_LOCK_POPUP_TEXT"] = "Histórico ilimitado de documentos está disponível em <a href=\"/settings/license_all.php\" target=\"_blank\">planos comerciais selecionados</a>.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Desfrute de recursos Bitrix24 ainda mais úteis com Unidade Avançada:<br/><br/>
 + Histórico de atualização de documentos (modificado quando e por quem)<br/>
 + Recuperação de qualquer versão de documento anterior do histórico<br/><br/>
 <a href=\"https://www.bitrix24.com/pro/drive.php\" target=\"_blank\">Saiba mais</a><br/><br/>Unidade Avançada está disponível no plano \"Standard\" e acima.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponível apenas em Bitrix24.Drive prolongado";
$MESS["CONFIG_DISK_LOCK_EXTENDED_FULLTEXT_POPUP_TEXT"] = "Disponível em planos comerciais selecionados";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Recursos do Drive avançado:

<br/><br/>
- Bloquear arquivos para impedir que outros possam editar documentos que você está trabalhando no momento.
<br/><br/>
- Desabilitar links públicos para evitar que outros possam compartilhar arquivos publicamente com usuários fora da sua conta Bitrix24
<br/><br/>
<a href=\"Https://www.bitrix24.com/pro/drive.php\" target=\"_blank\">Saiba mais</a>
<br/><br/>
Recursos avançados Bitrix24.Drive estão disponíveis em assinaturas comerciais a partir do plano padrão Bitrix24.
";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponível apenas no Bitrix24 Drive avançado";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Permitir bloqueio de documento";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Converter arquivo assim que aberto";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Máx. de entradas no histórico do documento";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Exibir documentos usando";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail do site do administrador<br>(endereço padrão do remetente)";
$MESS["CONFIG_EXAMPLE"] = "Exemplo";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_LISTS"] = "Listas";
$MESS["CONFIG_FEATURES_MEETING"] = "Reuniões e informações";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Fluxos de Trabalho Administrativos";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gerenciamento do tempo e relatórios de trabalho";
$MESS["CONFIG_FEATURES_TITLE"] = "Serviços";
$MESS["CONFIG_GDRP_APP1"] = "GDPR para funcionários";
$MESS["CONFIG_GDRP_APP2"] = "GDPR para CRM";
$MESS["CONFIG_GDRP_LABEL1"] = "Atualizações de produtos, ofertas especiais, convites para seminários online, <br>compilação de boletins informativos";
$MESS["CONFIG_GDRP_LABEL2"] = "Materiais de treinamento";
$MESS["CONFIG_GDRP_LABEL3"] = "Eu aceito o Contrato de Processamento de Dados";
$MESS["CONFIG_GDRP_LABEL4"] = "Denominação legal do cliente*";
$MESS["CONFIG_GDRP_LABEL5"] = "Nome do contato principal*";
$MESS["CONFIG_GDRP_LABEL6"] = "Título*";
$MESS["CONFIG_GDRP_LABEL7"] = "Data*";
$MESS["CONFIG_GDRP_LABEL8"] = "Endereço de e-mail de notificação*";
$MESS["CONFIG_GDRP_LABEL11"] = "(segundo o <a href=\"https://www.bitrix24.ru/about/advertising.php\" target=\"_blank\">Acordo</a>)";
$MESS["CONFIG_GDRP_TITLE1"] = "Concordo em receber e-mails da Bitrix24 contendo:";
$MESS["CONFIG_GDRP_TITLE2"] = "Se aceitar o Contrato de Processamento de Dados, você também deve fornecer as seguintes informações:";
$MESS["CONFIG_GDRP_TITLE3"] = "Aplicativos de processamento de dados pessoais:";
$MESS["CONFIG_HEADER_GDRP"] = "Conformidade com o GDPR";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Configurações de segurança";
$MESS["CONFIG_HEADER_SETTINGS"] = "Configurações";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permitir que os usuários enviem mensagens para o bate-papo Geral";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_ADMIN_RIGHTS"] = "Enviar notificações de atribuição e alteração de administrador no bate-papo geral";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Notificar novas contratações no bate-papo geral";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Notificar dispensa de empregado/usuário no bate-papo geral";
$MESS["CONFIG_IP_HELP_TEXT2"] = "Usuários sem acesso administrativo não poderão acessar este Bitrix24 a partir de qualquer endereço IP que não corresponda ao intervalo especificado. Uma vez logado, o usuário verá uma página de erro informando que o acesso através desse endereço IP foi negado. Você pode assistir a tentativas de fazer login no Bitrix24 a partir de qualquer endereço IP no log de eventos.";
$MESS["CONFIG_IP_TITLE"] = "Restrições de IP (permitir acesso apenas de determinados endereços IP ou intervalos de endereços. Exemplo: 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_LIMIT_MAX_TIME_IN_DOCUMENT_HISTORY"] = "Versões do documento são mantidas por #NUM# dias.";
$MESS["CONFIG_LOCATION_ADDRESS_FORMAT"] = "Formato do endereço";
$MESS["CONFIG_LOCATION_DEFAULT_SOURCE"] = "Fonte padrão";
$MESS["CONFIG_LOCATION_SOURCES_SETTINGS"] = "Configurações de endereço e fonte de localização";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_BACKEND"] = "Chave do servidor API Google Places e API de geocodificação";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "Chave do navegador API JavaScript Google Maps, API Places e API de geocodificação";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_NOTE"] = "É necessária uma chave API Google para usar mapas. Para obter a sua chave, <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utilize este formulário</a>.";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "Mostrar fotos do Google para a visualização do mapa dos Seus lugares (o Google pode cobrar uma taxa adicional por esta opção)";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "Usar o serviço de geocodificação para endereços desconhecidos (o Google pode cobrar uma taxa adicional por esta opção)";
$MESS["CONFIG_LOCATION_SOURCE_OSM_SERVICE_URL"] = "URL do serviço";
$MESS["CONFIG_LOGO_24"] = "Adicionar \"24\" ao logotipo da empresa";
$MESS["CONFIG_MARKETPLACE_MORE"] = "Saiba mais";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrar dados para Bitrix24 a partir de outros sistemas";
$MESS["CONFIG_MORE"] = "Leia mais";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL"] = "Permitir que os usuários instalem aplicativos do Marketplace24";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL1"] = "Permitir que os usuários instalem aplicativos";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Alterar";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Nota: você pode alterar seu endereço Bitrix24 apenas uma vez.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Altere seu endereço Bitrix24";
$MESS["CONFIG_NAME_CURRENT_MAP_PROVIDER"] = "Mapa atual do provedor";
$MESS["CONFIG_NAME_FILEMAN_GOOGLE_API_KEY"] = "Chave do Google Maps especificada nas configurações do módulo Site Explorer";
$MESS["CONFIG_NAME_FORMAT"] = "Formato do nome";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "A chave foi obtida para o domínio <b>#domain#</b>. Se você não conseguir fazer o Google Maps funcionar, altere as configurações da chave ou <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">pegue uma nova</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Preferências de integração API Google";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API Google Maps para chave de integração Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD2"] = "Chave do navegador API JavaScript Google Maps, API Places e API de geocodificação";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "É necessária uma chave API Google para usar mapas. Para obter a sua chave, <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utilize este formulário</a>.";
$MESS["CONFIG_NAME_MAP_PROVIDER_SETTINGS"] = "Configurações do provedor #PROVIDER#";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Permitir que os usuários do meu Bitrix24 se comuniquem na Rede global Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Esse recurso estará disponível assim que o administrador confirmar a conta.";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "A comunicação na Bitrix24.Network está disponível apenas para usuários comerciais Bitrix24.<br/><br/>
Confira as vantagens de conectar a Bitrix24.Network:<br/>
<ul>
<li>Todos os seus contatos e parceiros de negócios estão conectados em uma única rede</li>
<li>Comunicação rápida e conveniente com clientes e usuários externos</li>
<li>Comunicações contínuas entre usuários em diferentes contas Bitrix24</li>
</ul>
<b>Todas as ferramentas Bitrix24.Network estão disponíveis a partir do Bitrix24 Plus por #PRICE# ao mês.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponível apenas nos planos comerciais Bitrix24";
$MESS["CONFIG_ORGANIZATION"] = "Tipo da sua organização";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Empresa e empregados";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Organização e empregados";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organização e usuários";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Antes de você migrar seus funcionários para o sistema de autenticação de dois passos, configure-o para sua conta primeiro.<br/><br/>Prossiga, ativando-o na Página de segurança do seu perfil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Hoje, você utiliza seu login e senha para entrar no seu Bitrix24. Os dados pessoais e de negócios estão protegidos por tecnologia de criptografia de dados. No entanto, existem ferramentas que uma pessoa mal-intencionada pode empregar para entrar no seu computador e roubar suas credenciais de login. Procurando proteger contra possíveis ameaças, nós ativamos uma função extra de segurança para o Bitrix24: autenticação em dois passos";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Autenticação de dois passos";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "Não, obrigado";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Compartilhar";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Aqui está o texto que você pode postar num Feed<br/>para seus funcionários lerem.<br/><br/>Informe aos seus colegas sobre a autenticação de dois passos,<br/>procedimento de configuração e o novo método de autenticação<br/>que eles terão que usar para fazer login";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "Que tal postar este aqui num Feed?";
$MESS["CONFIG_OTP_SECURITY"] = "Ativar autenticação de dois passos para todos os usuários";
$MESS["CONFIG_OTP_SECURITY2"] = "Torne a autorização de dois passos obrigatória para todos os usuários";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Especificar o período de tempo dentro do qual todos os funcionários<br/>terão que ativar a autenticação de dois passos";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Desenvolvemos um procedimento de ativação da autenticação de dois passos fácil de usar que qualquer funcionário pode fazer sem o auxílio de especialistas.<br/><br/>Será enviada uma mensagem para cada funcionário, informando-os que eles terão que ativar a autenticação de dois passos dentro do período de tempo inserido. Os usuários que não fizerem não poderão mais assinar.";
$MESS["CONFIG_OTP_SECURITY_INFO_1"] = "<br/>Para ativar a autenticação de duas etapas, o usuário deverá instalar um aplicativo de OTP Bitrix24 para seu celular. Este aplicativo pode ser baixado da AppStore ou GooglePlay.<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_2"] = "Os empregados que não tenham um dispositivo móvel adequado devem ser equipados com um hardware especial - eToken Pass. Existe uma gama de fornecedores dos quais você pode obtê-lo, por exemplo:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>, 
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Encontrar outros fornecedores em <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_3"] = "Como opção, você pode desabilitar a autenticação em duas etapas para alguns dos empregados. No entanto, isso irá aumentar o risco de acesso não autorizado ao seu Bitrix24 se o login e a senha de um desses empregados forem roubados. Como administrador, você pode desabilitar a autenticação em dois passos para um empregado no perfil do usuário.";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Observe que desmarcar esta opção não irá interromper o uso de senhas únicas pelos usuários que já possuem autenticação de dois passos ativada. 
<br/><br/>Eles ainda terão que usar OTP ao fazer login no Bitrix24.
<br/><br/>Você pode desativar a autenticação de dois passos num respectivo perfil de usuário.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Formato de número de telefone: país padrão";
$MESS["CONFIG_SAVE"] = "Salvar";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "As configurações foram atualizadas com sucesso";
$MESS["CONFIG_SEND_OTP_PUSH"] = "Enviar notificações push e de bate-papo sobre tentativa de autenticação, <br>também incluir o código de login (apenas para OTP baseado em tempo)";
$MESS["CONFIG_SHOW_FIRED_EMPLOYEES"] = "Mostrar funcionários demitidos";
$MESS["CONFIG_SHOW_YEAR"] = "Mostrar ano de nascimento no perfil do usuário";
$MESS["CONFIG_SHOW_YEAR_FOR_FEMALE"] = "Mostrar data de nascimento em perfis femininos";
$MESS["CONFIG_STRESSLEVEL_AVAILABLE"] = "Permitir medir e mostrar o nível de estresse no perfil do usuário";
$MESS["CONFIG_TIME_FORMAT"] = "Formato da hora";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 horas";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 horas";
$MESS["CONFIG_TOALL_DEL"] = "excluir";
$MESS["CONFIG_TOALL_RIGHTS"] = "Opção de configurações para \"Todos os funcionários\"";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Adicionar";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK"] = "Rastrear cliques em links em e-mails enviados";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK_HINT"] = "Os gatilhos do CRM podem rastrear cliques no link de usuário.
Com esses gatilhos, você pode criar regras de automação para enviar uma notificação quando um cliente ou funcionário seguir um link .";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ"] = "Acompanhar o status de visualização de e-mails enviados";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ_HINT"] = "O sistema rastreará se as mensagens enviadas foram realmente visualizadas pelos respectivos destinatários e marcará essas mensagens como lidas. Esta opção está disponível para e-mails enviados do CRM, CRM Marketing, Bitrix24 E-mail e regras de automação. A opção também habilita o gatilho \"E-mail visualizado\" .";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Permitir links rich media";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Conectar automaticamente o disco do grupo quando<br/>o usuário ingressa no grupo";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Ativar a edição de documentos por via externa<br/>serviços (Google Docs, MS Office Online e outros)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Permitir aos usuários individuais e grupos ativarem<br/>a edição de documentos via serviços externos";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Dias de fim de semana";
$MESS["CONFIG_WEEK_START"] = "Primeiro dia da semana";
$MESS["CONFIG_WORK_TIME"] = "Parâmetros de tempo de trabalho";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Fins de semana e feriados";
$MESS["DAY_OF_WEEK_0"] = "Domingo";
$MESS["DAY_OF_WEEK_1"] = "Segunda-feira";
$MESS["DAY_OF_WEEK_2"] = "Terça-feira";
$MESS["DAY_OF_WEEK_3"] = "Quarta-feira";
$MESS["DAY_OF_WEEK_4"] = "Quinta-feira";
$MESS["DAY_OF_WEEK_5"] = "Sexta-feira";
$MESS["DAY_OF_WEEK_6"] = "Sábado";
$MESS["config_rating_label_likeN"] = "Votado texto do botão \"Curtir\"";
$MESS["config_rating_label_likeY"] = "Não votado texto do botão \"Curtir\"";
