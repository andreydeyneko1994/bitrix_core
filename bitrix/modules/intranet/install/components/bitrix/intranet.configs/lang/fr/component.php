<?
$MESS["CONFIG_ACCESS_DENIED"] = "Accès interdit";
$MESS["CONFIG_EMAIL_ERROR"] = "L'adresse mail de l'administrateur du site est incorrecte.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "Le format du nom est incorrect. Les macros suivantes sont possibles : #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["CONFIG_GDRP_EMAIL_ERROR"] = "Mauvaise adresse e-mail spécifiée.";
$MESS["CONFIG_GDRP_EMPTY_ERROR"] = "Veuillez compléter tous les champs obligatoires.";
$MESS["CONFIG_IP_ERROR"] = "Les adresses IP sont incorrectes.";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "Les mots de passe du serveur SMTP indiqués ne correspondent pas.";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Illimité";
?>