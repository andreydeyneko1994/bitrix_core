<?
$MESS["CONFIG_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CONFIG_EMAIL_ERROR"] = "El e-mail del administrador del sitio web es incorrecto.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "El formato del nombre es incorrecto. Las macros siguientes son posibles: #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["CONFIG_GDRP_EMAIL_ERROR"] = "Se especificó una dirección de correo electrónico incorrecta.";
$MESS["CONFIG_GDRP_EMPTY_ERROR"] = "Por favor complete todos los campos requeridos.";
$MESS["CONFIG_IP_ERROR"] = "La dirección IP es incorrecta.";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "Las contraseñas de servidor SMTP suministradas no coinciden.";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Ilimitado";
?>