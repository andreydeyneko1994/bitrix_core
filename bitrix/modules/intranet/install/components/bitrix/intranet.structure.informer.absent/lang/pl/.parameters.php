<?
$MESS["INTR_ABSC_PARAM_CALENDAR_IBLOCK_ID"] = "Blok informacji dla osobistych kalendarzy";
$MESS["INTR_ABSC_PARAM_CALENDAR_IBLOCK_TYPE"] = "Typ bloków informacji dla osbistych kalendarzy";
$MESS["INTR_ISIA_PARAM_DATE_FORMAT"] = "Format daty";
$MESS["INTR_ISIA_PARAM_DATE_TIME_FORMAT"] = "Format daty i godziny";
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "Strona widoku szczegółów";
$MESS["INTR_ISIA_PARAM_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["INTR_ISIA_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISIA_PARAM_NUM_USERS"] = "Pokaż wpisy";
$MESS["INTR_ISIA_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Szablon Ścieżki do Strony Działu";
$MESS["INTR_ISIA_PARAM_PM_URL"] = "Strona wiadomości osobistych";
$MESS["INTR_ISIA_PARAM_SHOW_LOGIN"] = "Pokaż Login, jeżeli nie są dostępne wymagane pola nazwy użytkownika";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR"] = "Pokaż rok urodzenia";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_M"] = "tylko mężczyźni";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_N"] = "Nikt";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_Y"] = "Wszystkie";
$MESS["INTR_PREDEF_DEPARTMENT"] = "Dział/Biuro";
?>