<?
$MESS["INTL_IBLOCK"] = "Blok informacji";
$MESS["INTL_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["INTL_ITEM_VAR"] = "Zmienne nazwy rezerwacji";
$MESS["INTL_MEETING_ID"] = "ID zasobu";
$MESS["INTL_MEETING_VAR"] = "Zmienne dla ID zasobu";
$MESS["INTL_PAGE_VAR"] = "Zmienna strony";
$MESS["INTL_PATH_TO_MEETING"] = "Strona harmonogramu zasobu";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Strona główna rezerwacji zasobu";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Strona edycji parametrów zasobu";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Strona rezerwacji zasobów";
$MESS["INTL_PATH_TO_VIEW_ITEM"] = "Widok strony rezerwacji zasobu";
$MESS["INTL_P_FRI_F"] = "Piątek";
$MESS["INTL_P_MON_F"] = "Poniedziałek";
$MESS["INTL_P_SAN_F"] = "Niedziela";
$MESS["INTL_P_SAT_F"] = "Sobota";
$MESS["INTL_P_THU_F"] = "Czwartek";
$MESS["INTL_P_TUE_F"] = "Wtorek";
$MESS["INTL_P_WEEK_HOLIDAYS"] = "Dni tygodnia";
$MESS["INTL_P_WEN_F"] = "Środa";
$MESS["INTL_SET_NAVCHAIN"] = "Ustaw Ścieżkę Nawigacji";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupy użytkowników mogące anulować rezerwację zasobu";
$MESS["INTL_VARIABLE_ALIASES"] = "Zmienne aliasów";
?>