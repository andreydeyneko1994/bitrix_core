<?
$MESS["INTL_IBLOCK"] = "Blok informacji";
$MESS["INTL_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["INTL_MEETING_ID"] = "ID zasobu";
$MESS["INTL_MEETING_VAR"] = "Zmienne dla ID zasobu";
$MESS["INTL_PAGE_VAR"] = "Zmienna strony";
$MESS["INTL_PATH_TO_MEETING"] = "Strona harmonogramu zasobu";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Strona główna rezerwacji zasobu";
$MESS["INTL_SET_NAVCHAIN"] = "Ustaw Ścieżkę Nawigacji";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupy użytkowników mogące edytować grafik zasobu";
$MESS["INTL_VARIABLE_ALIASES"] = "Zmienne aliasów";
?>