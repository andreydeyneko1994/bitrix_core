<?
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo Intranet não está instalado.";
$MESS["INAF_F_DESCRIPTION"] = "Descrição";
$MESS["INAF_F_FLOOR"] = "Andar";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Título";
$MESS["INAF_F_PHONE"] = "Telefone";
$MESS["INAF_F_PLACE"] = "Lugares";
$MESS["INAF_MEETING_NOT_FOUND"] = "A sala de reunião não foi encontrada.";
$MESS["INTASK_C36_EMPTY_NAME"] = "O título da sala de reunião está vazio.";
$MESS["INTASK_C36_NO_PERMS2CREATE"] = "Você não tem permissão para criar uma nova sala de reuniões.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Criar Sala de Reserva";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Reserva da Sala de Reunião";
$MESS["INTASK_C36_PAGE_TITLE2"] = "Editar Sala de Reunião";
$MESS["INTASK_C36_SHOULD_AUTH"] = "Por favor autorize antes de criar uma nova sala de reunião.";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
?>