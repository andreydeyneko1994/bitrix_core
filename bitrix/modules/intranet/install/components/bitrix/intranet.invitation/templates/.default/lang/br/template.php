<?php
$MESS["BX24_INVITE_DIALOG_ACTION_ADD"] = "Adicionar";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE"] = "Convidar";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE_MORE"] = "Convidar mais usúarios";
$MESS["BX24_INVITE_DIALOG_ACTION_SAVE"] = "Salvar";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Parabéns!</b><br>Um aviso de adesão à intranet foi enviado para este usuário.<br><br>Reveja os novos usuários que você convidou no <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">diretório de funcionários</a>.";
$MESS["BX24_INVITE_DIALOG_ADD_DEPARTMENT_PATTERN"] = "Adicionar #TITLE# ao departamento #DEPARTMENT# e ao #SONETGROUP#";
$MESS["BX24_INVITE_DIALOG_ADD_EMAIL_TITLE"] = "E-Mail";
$MESS["BX24_INVITE_DIALOG_ADD_GROUP_PATTERN"] = "Adicionar #TITLE# ao grupo";
$MESS["BX24_INVITE_DIALOG_ADD_LAST_NAME_TITLE"] = "Sobrenome";
$MESS["BX24_INVITE_DIALOG_ADD_NAME_TITLE"] = "Nome";
$MESS["BX24_INVITE_DIALOG_ADD_POSITION_TITLE"] = "Cargo";
$MESS["BX24_INVITE_DIALOG_ADD_SEND_PASSWORD_TITLE"] = "Enviar dados de login para o e-mail especificado";
$MESS["BX24_INVITE_DIALOG_ADD_WO_CONFIRMATION_TITLE"] = "Sem confirmação";
$MESS["BX24_INVITE_DIALOG_BUTTON_ADD"] = "Adicionar";
$MESS["BX24_INVITE_DIALOG_BUTTON_CLOSE"] = "Fechar";
$MESS["BX24_INVITE_DIALOG_BUTTON_INVITE"] = "Convidar";
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Confirmação de cadastro";
$MESS["BX24_INVITE_DIALOG_CONTINUE_ADD_BUTTON"] = "Adicionar mais";
$MESS["BX24_INVITE_DIALOG_CONTINUE_INVITE_BUTTON"] = "Convidar Mais ";
$MESS["BX24_INVITE_DIALOG_COPY_LINK"] = "Copiar link";
$MESS["BX24_INVITE_DIALOG_COPY_URL"] = "Link copiado para a Área de Transferência";
$MESS["BX24_INVITE_DIALOG_DEPARTMENT_CLOSE"] = "fechar";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_1"] = "Adicionar grupos";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_2"] = "Adicionar mais";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Digite os endereços de e-mail das pessoas que você deseja convidar. Separe várias entradas com uma vírgula ou espaço.";
$MESS["BX24_INVITE_DIALOG_EMAILS_DESCR"] = "Separe vários e-mails com uma vírgula ou espaço";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Formato de e-mail incorreto.";
$MESS["BX24_INVITE_DIALOG_EMAIL_SHORT"] = "E-Mail";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "funcionário";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Por favor, selecione um grupo para adicionar usuários externos";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Por favor, selecione um grupo para convidar usuários externos";
$MESS["BX24_INVITE_DIALOG_EXTRANET"] = "usuário externo";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>Parabéns!</b><br><br>Um convite para se juntar ao seu Bitrix24 foi enviado para o parceiro Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "Não há parceiro Bitrix24 com o e-mail especificado";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "Estou convidando você para participar do nosso Bitrix24! Você receberá permissões completas de parceiro Bitrix24 para nos ajudar a organizar e configurar a conta Bitrix24. Essas são permissões de acesso completo, exceto que você não pode adicionar ou excluir administradores do sistema.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_MORE"] = "Saiba mais";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_TEXT"] = "Parceiros certificados Bitrix24 podem ajudá-lo a configurar ou ajustar o seu Bitrix24 aos fluxos de trabalho da sua empresa: o CRM, Canais Abertos, documentação, telefonia, relatórios e outras ferramentas de negócios.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Sucesso!</b><br>Os convites foram enviados para os endereços especificados.<br><br>Para visualizar os usuários que você acabou de convidar, <a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">clique aqui</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_DEPARTMENT_PATTERN"] = "Convidar #TITLE# para o departamento #DEPARTMENT# e para #SONETGROUP#";
$MESS["BX24_INVITE_DIALOG_INVITE_GROUP_PATTERN"] = "Convidar #TITLE# para o grupo";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Você não pode convidar mais funcionários, porque vai exceder os termos da licença.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Junte-se a nós na nossa conta Bitrix24. Trata-se de um local onde todos podem se comunicar, colaborar em tarefas e projetos, gerenciar clientes e fazer muito mais.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_PHONE"] = "Por favor, junte-se a nós na nossa conta Bitrix24 - #PORTAL#. Aceite o convite aqui: #URL#";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Texto da mensagem";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Convidar mais usúarios";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>Parabéns!</b><br><br>Convites Bitrix24 foram enviados para os números selecionados.<br><br>Para ver quem foi convidado, por favor, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra a lista de funcionários</a>.";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CONNECT"] = "vincular caixa de e-mail existente";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CREATE"] = "criar caixa de e-mail";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_OR"] = "ou";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_DOMAIN"] = "Domínio";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_NAME"] = "Nome";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD"] = "Senha";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD_CONFIRM"] = "Confirmar senha";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ROLLUP"] = "ocultar";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_SELECT"] = "Selecionar";
$MESS["BX24_INVITE_DIALOG_PHONES_DESCR"] = "Digite números de telefone, separe várias inserções com um espaço ou vírgula";
$MESS["BX24_INVITE_DIALOG_PHONE_ADD"] = "adicionar";
$MESS["BX24_INVITE_DIALOG_PHONE_SHORT"] = "Telefones";
$MESS["BX24_INVITE_DIALOG_REGISTER_ALLOW_N"] = "Permitir cadastro rápido";
$MESS["BX24_INVITE_DIALOG_REGISTER_EXTENDED_SETTINGS"] = "Configurações adicionais";
$MESS["BX24_INVITE_DIALOG_REGISTER_LINK"] = "Link de cadastro rápido";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK"] = "atualizar link";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK_HELP"] = "Depois que um novo link for criado, o existente será invalidado, portanto os usuários não poderão utilizá-lo para cadastro.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TEXT_PLACEHOLDER_N_1"] = "Junte-se a nós na nossa conta Bitrix24. Trata-se de um local onde todos podem se comunicar, colaborar em tarefas e projetos, gerenciar clientes e fazer muito mais.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_HELP"] = "Novos usuários poderão se cadastrar na sua conta Bitrix24 e acessá-la após aprovação do administrador. ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_N"] = "Moderado";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS"] = "Não precisa confirmação para domínios:";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_HELP"] = "Novos usuários poderão se cadastrar na sua conta Bitrix24 e acessá-la sem aprovação do administrador se o endereço de e-mail deles corresponder ao domínio de e-mail especificado.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_PLACEHOLDER"] = "suaempresa.com; empresa.com";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_N"] = "Tipo de cadastro ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_HELP"] = "Novos usuários poderão se cadastrar na sua conta Bitrix24 e acessá-la sem a aprovação do administrador. As notificações do novo usuário serão exibidas no Fluxo de atividade e Bate-papo geral. ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_N"] = "Público";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>Parabéns!</b><br>As preferências foram atualizadas.";
$MESS["BX24_INVITE_DIALOG_SONETGROUP"] = "grupo";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE"] = "Adicionar";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE_NEW"] = "Cadastro";
$MESS["BX24_INVITE_DIALOG_TAB_INTEGRATOR_TITLE"] = "Convidar parceiro Bitrix24";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE"] = "Convidar";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_NEW"] = "Convidar usando e-mail";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_PHONE"] = "Convidar usando SMS";
$MESS["BX24_INVITE_DIALOG_TAB_SELF_TITLE"] = "Cadastro rápido";
$MESS["BX24_INVITE_DIALOG_TITLE"] = "Convidar um usuário";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TEXT"] = "Seu plano atual limita o número máximo de usuários Bitrix24 a #NUM#<br/><br/>
Por favor, faça um upgrade para um dos planos comerciais para adicionar mais usuários e desfrutar de muitas ferramentas de negócios essenciais.";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TITLE"] = "Limite de usuário";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Erro ao criar nova caixa de correio:";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "Sua senha e a senha de confirmação não correspondem.";
$MESS["INTRANET_INVITE_DIALOG_ACCESS_ERROR"] = "Acesso negado";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC"] = "O Bitrix24 tem três opções para usar o AD";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC1"] = "Exportar funcionários";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC2"] = "Criar estrutura da empresa";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC3"] = "Acesso único";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC4"] = "Você deve baixar e instalar um aplicativo para usar o Active Directory.";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_MORE"] = "Saiba mais sobre como usar o Diretório Ativo?";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_TITLE"] = "Diretório Ativo";
$MESS["INTRANET_INVITE_DIALOG_ADD_MASSIVE"] = "Convidar em massa";
$MESS["INTRANET_INVITE_DIALOG_ADD_MORE"] = "Adicionar mais";
$MESS["INTRANET_INVITE_DIALOG_ADD_TITLE"] = "Adicionar novo usuário";
$MESS["INTRANET_INVITE_DIALOG_CONFIRM_CREATOR_EMAIL_ERROR"] = "Você deve confirmar o endereço de e-mail do proprietário da conta Bitrix24 pelo link de confirmação no e-mail para convidar usuários.";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL"] = "Está faltando o endereço de e-mail.";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL_AND_PHONE"] = "Está faltando o endereço de e-mail ou número de telefone.";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_GROUP"] = "Convidar para o grupo de trabalho";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_TITLE"] = "Convidar usuário da extranet";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_DOMAINS"] = "Não precisa confirmação para domínios";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TITLE"] = "Convidar por link";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TYPE"] = "Solicitar aprovação do administrador para participar";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_INPUT"] = "Selecionar departamento ou grupo de trabalho";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_TITLE"] = "Convidar para departamento ou grupo de trabalho";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL"] = "Inserir e-mail";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL_AND_PHONE"] = "E-mail ou número de telefone";
$MESS["INTRANET_INVITE_DIALOG_INTEGRATOR_EMAIL"] = "E-mail do parceiro Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC"] = "Você pode inserir vários e-mails ou números de telefone";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL"] = "Insira um ou mais endereços de e-mail";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL_AND_PHONE"] = "Você pode inserir vários e-mails ou números de telefone";
$MESS["INTRANET_INVITE_DIALOG_MASS_INVITE_HINT"] = "O convite em massa permite apenas números de telefone do país atual";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL"] = "Digite os endereços de e-mail ou números de telefone das pessoas que você deseja convidar. Separe várias inserções com uma vírgula ou espaço.";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL"] = "Separe vários e-mails com um espaço ou vírgula";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL_AND_PHONE"] = "Digite os endereços de e-mail ou números de telefone das pessoas que você deseja convidar. Separe várias inserções com uma vírgula ou espaço.";
$MESS["INTRANET_INVITE_DIALOG_MASS_TITLE"] = "Convite em massa";
$MESS["INTRANET_INVITE_DIALOG_OR"] = "ou";
$MESS["INTRANET_INVITE_DIALOG_PICTURE_TITLE"] = "Convidar usuários";
$MESS["INTRANET_INVITE_DIALOG_REG_OFF"] = " ";
$MESS["INTRANET_INVITE_DIALOG_REG_ON"] = " ";
$MESS["INTRANET_INVITE_DIALOG_SELF_OFF_HINT"] = "O cadastro por link será desativado. Os links de cadastro enviados anteriormente ficarão inativos. Você sempre pode ativar esse recurso novamente.";
$MESS["INTRANET_INVITE_DIALOG_SUCCESS_SEND"] = "Os convites foram enviados!";
$MESS["INTRANET_INVITE_DIALOG_TITLE"] = "Convite";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL"] = "Convidar por e-mail";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL_AND_PHONE"] = "Convidar por e-mail ou número de telefone";
$MESS["INTRANET_INVITE_DIALOG_UNLIMITED"] = "ilimitado";
$MESS["INTRANET_INVITE_DIALOG_USER_CURRENT_COUNT"] = "Usuários atualmente registrados no seu Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_USER_MAX_COUNT"] = "Máximo de usuários permitidos no seu plano atual";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL"] = "O endereço de e-mail está incorreto.";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL_AND_PHONE"] = "E-mail ou número de telefone incorreto";
