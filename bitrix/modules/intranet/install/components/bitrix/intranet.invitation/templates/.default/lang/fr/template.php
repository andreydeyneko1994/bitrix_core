<?php
$MESS["BX24_INVITE_DIALOG_ACTION_ADD"] = "Ajouter";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE"] = "Inviter";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE_MORE"] = "Inviter d'autres utilisateurs";
$MESS["BX24_INVITE_DIALOG_ACTION_SAVE"] = "Enregistrer";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Félicitations !</b><br>Une notification d'adhésion à l'intranet a été envoyée à cet utilisateur.<br><br>Passez en revue les nouveaux utilisateurs que vous avez invités depuis le <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">répertoire des employés</a>.";
$MESS["BX24_INVITE_DIALOG_ADD_DEPARTMENT_PATTERN"] = "Ajouter #TITLE# au service #DEPARTMENT# et à #SONETGROUP#";
$MESS["BX24_INVITE_DIALOG_ADD_EMAIL_TITLE"] = "E-mail";
$MESS["BX24_INVITE_DIALOG_ADD_GROUP_PATTERN"] = "Ajouter #TITLE# au groupe";
$MESS["BX24_INVITE_DIALOG_ADD_LAST_NAME_TITLE"] = "Nom de famille";
$MESS["BX24_INVITE_DIALOG_ADD_NAME_TITLE"] = "Prénom";
$MESS["BX24_INVITE_DIALOG_ADD_POSITION_TITLE"] = "Fonction";
$MESS["BX24_INVITE_DIALOG_ADD_SEND_PASSWORD_TITLE"] = "Envoyer les données pour accéder à l'adresse e-mail indiquée";
$MESS["BX24_INVITE_DIALOG_ADD_WO_CONFIRMATION_TITLE"] = "Sans confirmation";
$MESS["BX24_INVITE_DIALOG_BUTTON_ADD"] = "Ajouter";
$MESS["BX24_INVITE_DIALOG_BUTTON_CLOSE"] = "Fermer";
$MESS["BX24_INVITE_DIALOG_BUTTON_INVITE"] = "Inviter";
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Confirmation d'inscription";
$MESS["BX24_INVITE_DIALOG_CONTINUE_ADD_BUTTON"] = "Ajouter plus";
$MESS["BX24_INVITE_DIALOG_CONTINUE_INVITE_BUTTON"] = "Inviter plus ";
$MESS["BX24_INVITE_DIALOG_COPY_LINK"] = "Copier le lien";
$MESS["BX24_INVITE_DIALOG_COPY_URL"] = "Lien copié dans le Presse-papiers";
$MESS["BX24_INVITE_DIALOG_DEPARTMENT_CLOSE"] = "fermer";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_1"] = "Ajouter des groupes";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_2"] = "Ajouter plus";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Saisissez les adresses e-mail des personnes que vous voulez inviter. En cas d'entrées multiples, veuillez les séparer avec une virgule ou un espace.";
$MESS["BX24_INVITE_DIALOG_EMAILS_DESCR"] = "Saisissez les adresses séparées par des virgules ou espaces";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Format d'e-mail incorrect.";
$MESS["BX24_INVITE_DIALOG_EMAIL_SHORT"] = "E-mail";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "employé";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Veuillez sélectionner un groupe pour ajouter des utilisateurs externes";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Veuillez sélectionner un groupe pour inviter des utilisateurs externes";
$MESS["BX24_INVITE_DIALOG_EXTRANET"] = "utilisateur externe";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>Félicitations !</b><br><br>Une invitation à rejoindre votre Bitrix24 a été envoyée au partenaire Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "Aucun partenaire Bitrix24 ne correspond à l'adresse e-mail indiquée";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "Je vous invite à rejoindre notre Bitrix24 ! Vous profiterez de l'intégralité des permissions de partenaire Bitrix24 pour nous aider à installer et configurer le compte Bitrix24. Ce sont des permissions d'accès intégrales, à part que vous ne pouvez pas ajouter ou supprimer d'administrateurs système.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_MORE"] = "Plus d'informations";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_TEXT"] = "Les partenaires Bitrix24 certifiés peuvent vous aider à configurer votre Bitrix24 et à l'ajuster aux flux de travail de votre société : CRM, canaux ouverts, documentation, téléphonie, rapports et autres outils professionnels.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Bravo !</b><br>Les invitations ont été envoyées aux adresses indiquées.<br><br>Pour voir les utilisateurs que vous venez d'inviter, <a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">cliquez ici</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_DEPARTMENT_PATTERN"] = "Inviter #TITLE# dans le service #DEPARTMENT# et dans le groupe #SONETGROUP#";
$MESS["BX24_INVITE_DIALOG_INVITE_GROUP_PATTERN"] = "Inviter #TITLE# dans le groupe";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Vous ne pouvez pas inviter plus d'employés à cause des conditions de votre licence.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Veuillez nous rejoindre sur notre compte Bitrix24. C'est un endroit où tout le monde peut communiquer, collaborer sur des tâches et des projets, gérer des clients, et bien plus encore.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_PHONE"] = "Veuillez nous rejoindre sur notre compte Bitrix24 - #PORTAL#. Acceptez l'invitation ici : #URL#";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Texte du message";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Inviter d'autres utilisateurs";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>Félicitations !</b><br><br>Les invitations Bitrix24 ont été envoyées aux personnes sélectionnées.<br><br>Pour voir qui a été invité(e), veuillez <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">ouvrir la liste des employés</a>.";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CONNECT"] = "associer une messagerie existante";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CREATE"] = "créer une messagerie";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_OR"] = "ou";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_DOMAIN"] = "Domaine";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_NAME"] = "Nom";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD"] = "Mot de passe";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD_CONFIRM"] = "Confirmer le mot de passe";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ROLLUP"] = "cacher";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_SELECT"] = "Sélectionner";
$MESS["BX24_INVITE_DIALOG_PHONES_DESCR"] = "Saisissez les numéros de téléphone, séparés par un espace ou une virgule s'il y en a plusieurs";
$MESS["BX24_INVITE_DIALOG_PHONE_ADD"] = "ajouter";
$MESS["BX24_INVITE_DIALOG_PHONE_SHORT"] = "Téléphones";
$MESS["BX24_INVITE_DIALOG_REGISTER_ALLOW_N"] = "Autoriser les inscriptions rapides";
$MESS["BX24_INVITE_DIALOG_REGISTER_EXTENDED_SETTINGS"] = "Paramètres avancés";
$MESS["BX24_INVITE_DIALOG_REGISTER_LINK"] = "Lien d'inscription rapide";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK"] = "obtenir un nouveau lien";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK_HELP"] = "Après avoir généré un nouveau lien, l'ancien ne permettra plus aux utilisateurs de s'inscrire.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TEXT_PLACEHOLDER_N_1"] = "Veuillez nous rejoindre sur notre compte Bitrix24. C'est un endroit où tout le monde peut communiquer, collaborer sur des tâches et des projets, gérer des clients, et bien plus encore.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_HELP"] = "Les nouveaux utilisateurs seront en mesure de s'inscrire sur votre compte Bitrix24 et d'y accéder après validation par un administrateur. ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_N"] = "Modérée";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS"] = "Ne nécessite pas de confirmation pour les domaines :";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_HELP"] = "Les nouveaux utilisateurs seront en mesure de s'inscrire sur votre compte Bitrix24 et d'y accéder sans validation par un administrateur si leur adresse e-mail correspond à un des domaine d'e-mail indiqué.";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_PLACEHOLDER"] = "yourcompany.com; company.com";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_N"] = "Type d'inscription ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_HELP"] = "Les nouveaux utilisateurs seront en mesure de s'inscrire sur votre compte Bitrix24 et d'y accéder sans validation par un administrateur. Les notifications de nouvel utilisateur seront affichées dans le Flux d'activités et dans le chat général. ";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_N"] = "Publique";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>Félicitations !</b><br>Les préférences ont été mises à jour.";
$MESS["BX24_INVITE_DIALOG_SONETGROUP"] = "groupe";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE"] = "Ajouter";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE_NEW"] = "S'inscrire";
$MESS["BX24_INVITE_DIALOG_TAB_INTEGRATOR_TITLE"] = "Inviter un partenaire Bitrix24";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE"] = "Inviter";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_NEW"] = "Inviter par e-mail";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_PHONE"] = "Inviter via SMS";
$MESS["BX24_INVITE_DIALOG_TAB_SELF_TITLE"] = "Inscription rapide";
$MESS["BX24_INVITE_DIALOG_TITLE"] = "Inviter l'employé";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TEXT"] = "Votre offre actuelle limite le nombre maximum d'utilisateurs Bitrix24 à #NUM#<br/><br/>
Veuillez passer sur une offre commerciale pour ajouter plus d'utilisateurs et bénéficier de nombreux outils commerciaux essentiels.";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TITLE"] = "Limite d'utilisateurs";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Erreur lors de la création d'une nouvelle messagerie :";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "La confirmation du mot de passe ne correspond pas au mot de passe.";
$MESS["INTRANET_INVITE_DIALOG_ACCESS_ERROR"] = "Accès refusé";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC"] = "Bitrix24 a accès à trois manières d'utiliser les AD";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC1"] = "Exporter les employés";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC2"] = "Créer la structure de l'entreprise";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC3"] = "Connexion simple";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC4"] = "Vous devez télécharger et installer une application pour utiliser Active Directory.";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_MORE"] = "Vous voulez en savoir plus sur Active Directory ?";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_TITLE"] = "Active Directory";
$MESS["INTRANET_INVITE_DIALOG_ADD_MASSIVE"] = "Inviter en masse";
$MESS["INTRANET_INVITE_DIALOG_ADD_MORE"] = "Ajouter plus";
$MESS["INTRANET_INVITE_DIALOG_ADD_TITLE"] = "Ajouter un nouvel utilisateur";
$MESS["INTRANET_INVITE_DIALOG_CONFIRM_CREATOR_EMAIL_ERROR"] = "Avant de pouvoir inviter des utilisateurs, vous devez confirmer l'adresse e-mail du propriétaire du compte Bitrix24 via le lien de confirmation contenu dans l'email.";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL"] = "L'adresse e-mail manque.";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL_AND_PHONE"] = "L'adresse e-mail ou le numéro de téléphone manquent.";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_GROUP"] = "Inviter à rejoindre le groupe de travail";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_TITLE"] = "Inviter un utilisateur externe";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_DOMAINS"] = "Ne pas demander de confirmation pour les domaines";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TITLE"] = "Inviter via lien";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TYPE"] = "Demander la validation de l'administrateur pour rejoindre";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_INPUT"] = "Sélectionnez le service ou groupe de travail";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_TITLE"] = "Inviter à rejoindre un service ou un groupe de travail";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL"] = "Saisissez une adresse e-mail";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL_AND_PHONE"] = "E-mail ou numéro de téléphone";
$MESS["INTRANET_INVITE_DIALOG_INTEGRATOR_EMAIL"] = "Adresse e-mail de partenaire Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC"] = "Vous pouvez saisir plusieurs adresses e-mail ou numéros de téléphone";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL"] = "Saisissez une ou plusieurs adresses e-mail";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL_AND_PHONE"] = "Vous pouvez saisir plusieurs adresses e-mail ou numéros de téléphone";
$MESS["INTRANET_INVITE_DIALOG_MASS_INVITE_HINT"] = "L'invitation en masse n'autorise que les numéros de téléphone du pays actuel";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL"] = "Saisissez les adresses e-mail ou numéros de téléphone des personnes que vous voulez inviter. En cas d'entrées multiples, veuillez les séparer avec une virgule ou un espace.";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL"] = "Séparez les multiples e-mails par un espace ou une virgule";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL_AND_PHONE"] = "Saisissez les adresses e-mail ou numéros de téléphone des personnes que vous voulez inviter. En cas d'entrées multiples, veuillez les séparer avec une virgule ou un espace.";
$MESS["INTRANET_INVITE_DIALOG_MASS_TITLE"] = "Invitation en masse";
$MESS["INTRANET_INVITE_DIALOG_OR"] = "ou";
$MESS["INTRANET_INVITE_DIALOG_PICTURE_TITLE"] = "Inviter des utilisateurs";
$MESS["INTRANET_INVITE_DIALOG_REG_OFF"] = " ";
$MESS["INTRANET_INVITE_DIALOG_REG_ON"] = " ";
$MESS["INTRANET_INVITE_DIALOG_SELF_OFF_HINT"] = "Les inscriptions via lien seront désactivées. Les liens d'enregistrement déjà envoyés deviendront inactifs. Vous pouvez toujours réactiver cette fonction.";
$MESS["INTRANET_INVITE_DIALOG_SUCCESS_SEND"] = "L'invitation a été envoyée !";
$MESS["INTRANET_INVITE_DIALOG_TITLE"] = "Invitation";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL"] = "Inviter via e-mail";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL_AND_PHONE"] = "Inviter via e-mail ou numéro de téléphone";
$MESS["INTRANET_INVITE_DIALOG_UNLIMITED"] = "illimité";
$MESS["INTRANET_INVITE_DIALOG_USER_CURRENT_COUNT"] = "Utilisateurs actuellement inscrits sur votre Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_USER_MAX_COUNT"] = "Nombre maximum d'utilisateurs autorisés avec votre offre actuelle";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL"] = "L'adresse e-mail est incorrecte.";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL_AND_PHONE"] = "Adresse e-mail ou numéro de téléphone incorrects";
