<?php
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Gratulacje!</b><br><br>Powiadomienie o dołączeniu zostało wysłane do określonego użytkownika.<br><br>Aby zobaczyć zaproszonych, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">otwórz listę pracowników</a>.";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Nieprawidłowy format adresu e-mail";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL"] = "Nieprawidłowy format adresu e-mail";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL_OR_PHONE"] = "Nieprawidłowy format adresu e-mail lub numeru telefonu";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Wybierz grupę do dodania użytkowników zewnętrznych do";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Wybierz grupę do zaproszenia użytkowników zewnętrznych do";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>Gratulacje!</b><br><br>Zaproszenie do dołączenia do Twojego Bitrix24 zostało wysłane na adres e-mail partnera Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_COUNT_ERROR"] = "Przekroczono maksymalną liczbę partnerów Bitrix24. Możesz odrzucić jednego z istniejących #LINK_START#partnerów Bitrix24#LINK_END# i zaprosić nowego.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "Nie ma partnera Bitrix24 o podanym adresie e-mail";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "Zapraszam do dołączenia do naszego Bitrix24! Otrzymasz pełne uprawnienia partnera Bitrix24, aby pomóc nam utworzyć i skonfigurować konto Bitrix24. Są to pełne uprawnienia dostępu, nie można jedynie dodawać ani usuwać administratorów systemu.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Gratulacje!</b><br><br>Zaproszenia Bitrix24 zostały wysłane do wybranych adresów e-mail.<br><br>Aby zobaczyć zaproszonych, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">otwórz listę pracowników</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>Gratulacje!</b><br><br>Zaproszenia Bitrix24 zostały wysłane do wybranych numerów.<br><br>Aby zobaczyć zaproszonych, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">otwórz listę pracowników</a>.";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>Gratulacje!</b><br><br>Zaktualizowano ustawienia szybkiej rejestracji.";
$MESS["BX24_INVITE_DIALOG_SELF_SUCCESS"] = "Zaktualizowano ustawienia szybkiej rejestracji.";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Błąd podczas tworzenia nowej skrzynki pocztowej:";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "Twoje hasło nie pasuje do potwierdzenia hasła";
$MESS["INTRANET_INVITE_DIALOG_ERROR_LENGTH"] = "Przekroczono maksymalną długość 50 znaków";
$MESS["INTRANET_INVITE_DIALOG_USER_COUNT_ERROR"] = "Przekroczono maksymalną dzienną liczbę zaproszonych użytkowników";
