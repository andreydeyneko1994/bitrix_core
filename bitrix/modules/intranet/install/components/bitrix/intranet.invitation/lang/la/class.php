<?php
$MESS["INTRANET_INVITE_DIALOG_MENU_ACTIVE_DIRECTORY"] = "Active Directory";
$MESS["INTRANET_INVITE_DIALOG_MENU_ADD"] = "Agregar un nuevo usuario";
$MESS["INTRANET_INVITE_DIALOG_MENU_EXTRANET"] = "Invitar a un usuario de extranet";
$MESS["INTRANET_INVITE_DIALOG_MENU_INTEGRATOR"] = "Invitar a un socio de Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL"] = "Invitar mediante correo electrónico";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL_AND_PHONE"] = "Invitar mediante correo electrónico o número de teléfono";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_WITH_GROUP_DP"] = "Invitar al departamento o al grupo de trabajo";
$MESS["INTRANET_INVITE_DIALOG_MENU_MASS_INVITE"] = "Invitación masiva";
$MESS["INTRANET_INVITE_DIALOG_MENU_PHONE"] = "Invitar mediante SMS";
$MESS["INTRANET_INVITE_DIALOG_MENU_SELF"] = "Invitar mediante enlace";
$MESS["INTRANET_INVITE_DIALOG_MENU_SOON"] = "pronto";
