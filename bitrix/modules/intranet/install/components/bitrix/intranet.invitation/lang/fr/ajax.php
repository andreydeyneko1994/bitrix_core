<?php
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Félicitations !</b><br><br>Une notification d'inscription a été envoyée à l'utilisateur spécifié.<br><br>Pour voir qui a été invité(e), veuillez <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">ouvrir la liste des employés</a>.";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Format d'e-mail incorrect";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL"] = "Format d'e-mail incorrect";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL_OR_PHONE"] = "Format de l'e-mail ou du numéro de téléphone incorrect";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Veuillez sélectionner un groupe pour ajouter des utilisateurs externes au";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Veuillez sélectionner un groupe pour inviter des utilisateurs externes à rejoindre le/la";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>Félicitations !</b><br><br>Une invitation à rejoindre votre Bitrix24 a été envoyée à l'adresse e-mail du partenaire Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_COUNT_ERROR"] = "Le nombre maximum de partenaires Bitrix24 a été dépassé. Vous pouvez renvoyer un des #LINK_START#partenaires Bitrix24#LINK_END# existants et en inviter un nouveau.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "Aucun partenaire Bitrix24 ne correspond à l'adresse e-mail indiquée";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "Je vous invite à rejoindre notre Bitrix24 ! Vous profiterez de l'intégralité des permissions de partenaire Bitrix24 pour nous aider à installer et configurer le compte Bitrix24. Ce sont des permissions d'accès intégrales, à part que vous ne pouvez pas ajouter ou supprimer d'administrateurs système.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Félicitations !</b><br><br>Les invitations Bitrix24 ont été envoyées aux adresses e-mail sélectionnées.<br><br>Pour voir qui a été invité(e), veuillez <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">ouvrir la liste des employés</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>Félicitations !</b><br><br>Les invitations Bitrix24 ont été envoyées aux personnes sélectionnées.<br><br>Pour voir qui a été invité(e), veuillez <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">ouvrir la liste des employés</a>.";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>Félicitations !</b><br><br>Les paramètres d'inscription rapide ont été mis à jour.";
$MESS["BX24_INVITE_DIALOG_SELF_SUCCESS"] = "Les paramètres d'inscription rapide ont été mis à jour.";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Erreur lors de la création d'une nouvelle messagerie :";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "La confirmation du mot de passe ne correspond pas au mot de passe";
$MESS["INTRANET_INVITE_DIALOG_ERROR_LENGTH"] = "Longueur maximale de 50 caractères dépassée";
$MESS["INTRANET_INVITE_DIALOG_USER_COUNT_ERROR"] = "Nombre maximum d'utilisateurs invités par jour dépassé";
