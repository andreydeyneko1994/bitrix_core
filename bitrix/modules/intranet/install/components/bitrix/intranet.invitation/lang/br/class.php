<?php
$MESS["INTRANET_INVITE_DIALOG_MENU_ACTIVE_DIRECTORY"] = "Diretório ativo";
$MESS["INTRANET_INVITE_DIALOG_MENU_ADD"] = "Adicionar novo usuário";
$MESS["INTRANET_INVITE_DIALOG_MENU_EXTRANET"] = "Convidar usuário da extranet";
$MESS["INTRANET_INVITE_DIALOG_MENU_INTEGRATOR"] = "Convidar parceiro Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL"] = "Convidar por e-mail";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL_AND_PHONE"] = "Convidar por e-mail ou número de telefone";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_WITH_GROUP_DP"] = "Convidar para departamento ou grupo de trabalho";
$MESS["INTRANET_INVITE_DIALOG_MENU_MASS_INVITE"] = "Convite em massa";
$MESS["INTRANET_INVITE_DIALOG_MENU_PHONE"] = "Convidar por SMS";
$MESS["INTRANET_INVITE_DIALOG_MENU_SELF"] = "Convidar por link";
$MESS["INTRANET_INVITE_DIALOG_MENU_SOON"] = "em breve";
