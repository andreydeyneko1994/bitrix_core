<?
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Moduł Sieci Społecznościowej nie jest zainstalowany.";
$MESS["INTV_CREATE_TITLE"] = "Nowy widok";
$MESS["INTV_INTERNAL_ERROR"] = "Wystąpił błąd systemu.";
$MESS["INTV_NO_IBLOCK_PERMS"] = "Nie masz uprawnień do wyświetlania zadań bloku informacji.";
$MESS["INTV_NO_SONET_PERMS"] = "Nie masz uprawnien do podglądu zadań.";
$MESS["INTV_TASKS_OFF"] = "Funkcja zadań jest wyłączona.";
?>