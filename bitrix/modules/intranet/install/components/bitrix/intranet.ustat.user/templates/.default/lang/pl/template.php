<?
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Firma<br>średnia";
$MESS["INTRANET_USTAT_USER_ACTIVITY_DEPT_TITLE"] = "Średnia<br>działu";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE"] = "Mój<br>wskaźnik aktywności";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE_OTHER"] = "Wskaźnik aktywności <br>";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Średnia firmy";
$MESS["INTRANET_USTAT_USER_GRAPH_DEPT"] = "%DEPT% dep. Średnia";
$MESS["INTRANET_USTAT_USER_GRAPH_ME"] = "Ja";
$MESS["INTRANET_USTAT_USER_GRAPH_TITLE"] = "Puls firmy";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY"] = "Łączna wartość działań wykonanych w Bitrix24 z użyciem jednego z siedmiu dostępnych narzędzi dla określonego okresu czasu.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Średnia wartość działań, które wykonał twój dział w Bitrix24 z użyciem jednego z siedmiu dostępnych narzędzi dla określonego okresu czasu.<br><br> <b>Użyj wartości średniej działu, aby zobaczyć o ile twoja działalność jest lepsza niż całej firmy.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Średnia wartość działań, które wykonał twój dział w Bitrix24 z użyciem jednego z siedmiu dostępnych narzędzi dla określonego okresu czasu.<br><br> <b>Użyj wartości średniej działu, aby zobaczyć o ile twoja działalność jest lepsza niż innych.";
$MESS["INTRANET_USTAT_USER_HELP_GENERAL"] = "<b>Puls Firmy</b> uwidacznia twoją działalność w Bitrix24 w określonym okresie czasu.";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Twoja pozycja w <b>ocenie aktywności</b> podsumowaniu wszystkich pracowników, którzy używali Bitrix24 przynajmniej raz w ciągu określonego okresu czasu.";
$MESS["INTRANET_USTAT_USER_RATING_TITLE"] = "Moja<br>ocena";
$MESS["INTRANET_USTAT_USER_RATING_TITLE_OTHER"] = "Ocena";
$MESS["INTRANET_USTAT_USER_SECTION_ACTIVITY"] = "Aktywne";
$MESS["INTRANET_USTAT_USER_SECTION_MAX_ACTIVITY"] = "Maks.";
$MESS["INTRANET_USTAT_USER_SECTION_TELL_ABOUT"] = "Informuj";
?>