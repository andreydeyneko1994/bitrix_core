<?php
$MESS["EC_IBLOCK_ID_EMPTY"] = "Le bloc d'information n'a pas été sélectionné.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Le module 'Intranet' n'a pas été installé.";
$MESS["INAF_F_DESCRIPTION"] = "Description";
$MESS["INAF_F_FLOOR"] = "Etage";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Titre";
$MESS["INAF_F_PHONE"] = "Numéro de téléphone";
$MESS["INAF_F_PLACE"] = "Nombre de places";
$MESS["INTASK_C36_PAGE_TITLE"] = "Recherche des salles de réunion";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Vous n'avez pas la permission d'afficher le bloc d'information de la salle de réunion.";
