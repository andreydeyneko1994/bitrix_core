<?php
$MESS["EC_CALENDAR_ANDROID_POINT_ADD_EVENT_TEXT"] = "Adicione eventos ao calendário Bitrix24 a partir do seu dispositivo";
$MESS["EC_CALENDAR_ANDROID_POINT_CALENDAR_SYNC_GOOD_TEXT"] = "Acompanhe novos eventos em qualquer lugar, a qualquer hora";
$MESS["EC_CALENDAR_ANDROID_POINT_DOCS_DETAIL_LINK"] = "Saiba mais sobre a sincronização";
$MESS["EC_CALENDAR_ANDROID_POINT_PERMISSION_TEXT"] = "Certifique-se de que o aplicativo Bitrix24 tem permissão para acessar o calendário do seu dispositivo";
$MESS["EC_CALENDAR_ANDROID_PREVIEW_TEXT"] = "Seu calendário Bitrix24 foi sincronizado";
$MESS["EC_CALENDAR_ANDROID_START_WORK"] = "Início";
