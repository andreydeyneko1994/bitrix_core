<?
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TITLE"] = "CRM: praca z klientami";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TITLE"] = "Edytowanie dokumentu…w 60 sekund";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TITLE"] = "Współpracuj z przyjemnością!";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TEXT"] = "Moi przyjaciele! Przestańmy być zbyt nieśmiali i zacznijmy mówić o wszystkich fajnych rzeczach, które dzieją się w firmie. Oczywiście nasz intranet nie jest całkiem siecią społecznościową, ale ma \"polubienia\" i jest świetnym sposobem, aby upewnić się, że wiesz, co się dzieje. 

Sprawdź posty, komentarze, zdjęcia i pliki oraz \"polub\" niektóre z nich. 

[IMG WIDTH = 500 HEIGHT = 341] https://www.bitrix24.com/images/ustat/en/likes1.png[/IMG]

[B]Jak pokazać, że coś ci się podoba[/B]

Jeśli post lub wiadomość od kolegi jest w strumieniu aktywności możesz \"polubić\" go za pomocą przycisku polubienia w tym poście. Polubienia zajmują tylko chwilę, ale pozwalają osobie, która ją pisze, wiedzieć, że ten wkład był dla kogoś wartościowy, a inni wiedzą, że może im to pomóc.

[B]Kto lubi[/B]

Możesz zobaczyć listę użytkowników, którzy \"polubili\" post lub komentarz, umieszczając kursor na gwiazdce obok tego słowa. Takie polubienia są ważne, ponieważ funkcja wyszukiwania nadaje priorytet ulubionym treściom, które są istotne dla danego wyszukiwania. 

[SZEROKOŚĆ IMG = 500 WYSOKOŚCI = 255] https://www.bitrix24.com/images/ustat/en/likes2.png[/IMG]

[B]Dlaczego istnieje system ocen z upodobaniami?[/B]

Wyszukiwanie społecznościowe - czyli wyniki wyszukiwania, na które wpływają polubienia, pomagają użytkownikom znaleźć dokumenty i dyskusje, które okazały się szczególnie przydatne. 

[SZEROKOŚĆ IMG = 500 WYSOKOŚCI = 426] https://www.bitrix24.com/images/ustat/pl/likes3.png[/IMG]

Wiadomości z największą liczbą \"polubień\" są wyświetlane w sekcji \"Popularne posty\" na przedniej stronie strona.  

[B]Co jeszcze możesz polubić?[/B]

Wiadomości i komentarze to nie jedyne rzeczy, które można polubić. Możesz je lubić, gdy są gotowe, aby zmotywować współpracowników do ciężkiej pracy. Pliki i zdjęcia również mogą być lubiane.

[SZEROKOŚĆ IMG = 500 WYSOKOŚCI = 322] https://www.bitrix24.com/images/ustat/en/likes4.png[/IMG]

[B]Ocena Twojej aktywności lubisz[/B]

Aby zobaczyć, jak aktywnie używasz \"polubienia\" użyj funkcji Puls firmy i porównaj swój wynik z wynikiem twojego działu lub całej firmy. 

[IMG WIDTH = 500 HEIGHT = 271] https://www.bitrix24.com/images/ustat/en/likes5.png[/IMG]

[B]Zacznij polubić![/B]

Możesz zacząć używać przycisku \"Lubię to\" teraz w tym właśnie poście. Nie zapomnij o dodawaniu polubień do plików i zadań, które uważasz za ważne lub które wykorzystałeś jako odniesienie. Zachęcaj swoich kolegów i pracuj trochę bardziej jako zespół - przycisk \"Podobny\" sprawia, że ​​jest to proste!
Nie zapomnij polubić tej wiadomości :)";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TITLE"] = "Lubię to! Lubię to! Lubię to!";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TITLE"] = "Natychmiastowa mobilizacja!";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Jedność w sile roboczej";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TITLE"] = "Zadania i zadaniowe!";
?>