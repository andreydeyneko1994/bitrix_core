<?php
$MESS["INTRANET_BITRIX24"] = "Bitrix24";
$MESS["INTRANET_CHANGE_NOTIFY_SETTINGS"] = "Edytuj ustawienia powiadomień";
$MESS["INTRANET_INVITE_ACCEPT"] = "Zaakceptuj zaproszenie";
$MESS["INTRANET_INVITE_FOOTER"] = "Powiadomienie zostało wygenerowane automatycznie. Otrzymujesz tę wiadomość, ponieważ masz zaproszenie do konta Bitrix24. Ustawienia powiadomień można będzie edytować zaraz po zaakceptowaniu zaproszenia.";
$MESS["INTRANET_INVITE_IMG_LINK"] = "http://www.bitrix24.com/images/mail/tutorials/tools_pl.png";
$MESS["INTRANET_INVITE_INFO_TEXT"] = "#SPAN_START# Bitrix24 to wiodąca platforma do współpracy, #SPAN_END# używana przez 10 000 000 firm na całym świecie.";
$MESS["INTRANET_INVITE_INFO_TEXT3"] = "Czym jest Bitrix24?";
$MESS["INTRANET_INVITE_MORE"] = "Dowiedz się więcej";
$MESS["INTRANET_INVITE_TEXT"] = "#NAME# zaprasza cię do #BLOCK_START#Bitrix#BLOCK_MIDDLE#24#BLOCK_END#";
$MESS["INTRANET_MAIL_EVENTS_UNSUBSCRIBE"] = "Anuluj subskrypcję";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE"] = "Nowa wiadomość od #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE_GROUP"] = "Nowe wiadomości od #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_NOTIFY"] = "Nowe powiadomienie od #NAME#";
$MESS["INTRANET_OPEN"] = "Wyświetl";
$MESS["INTRANET_OPEN_NOTIFY"] = "Wyświetl powiadomienia";
$MESS["INTRANET_SITE_LINK"] = "https://www.bitrix24.pl/";
