<?php
$MESS["INTRANET_BITRIX24"] = "Bitrix24";
$MESS["INTRANET_CHANGE_NOTIFY_SETTINGS"] = "Editar configurações de aviso";
$MESS["INTRANET_INVITE_ACCEPT"] = "Aceitar convite";
$MESS["INTRANET_INVITE_FOOTER"] = "Este é um aviso gerado automaticamente. Você recebeu este e-mail porque foi convidado a participar da conta Bitrix24. Você pode editar as configurações de aviso assim que aceitar o convite.";
$MESS["INTRANET_INVITE_IMG_LINK"] = "http://www.bitrix24.com/images/mail/tutorials/tools_br.png";
$MESS["INTRANET_INVITE_INFO_TEXT"] = "#SPAN_START# Bitrix24 é a principal plataforma de colaboração,#SPAN_END# utilizada por mais de 10.000.000 empresas em todo o mundo.";
$MESS["INTRANET_INVITE_INFO_TEXT3"] = "O que é o Bitrix24?";
$MESS["INTRANET_INVITE_MORE"] = "Saiba mais";
$MESS["INTRANET_INVITE_TEXT"] = "#NAME# convida você para o #BLOCK_START#Bitrix#BLOCK_MIDDLE#24#BLOCK_END#";
$MESS["INTRANET_MAIL_EVENTS_UNSUBSCRIBE"] = "Cancelar inscrição";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE"] = "Nova mensagem de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE_GROUP"] = "Novas mensagens de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_NOTIFY"] = "Novo aviso de #NAME#";
$MESS["INTRANET_OPEN"] = "Visualizar";
$MESS["INTRANET_OPEN_NOTIFY"] = "Ver notificações";
$MESS["INTRANET_SITE_LINK"] = "https://www.bitrix24.com.br/";
