<?
$MESS["INTRANET_OTP_CLOSE"] = "Przypomnij mi później";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "Nie pokazuj więcej";
$MESS["INTRANET_OTP_CODE"] = "Druga linia obrony: kod weryfikacyjny";
$MESS["INTRANET_OTP_GOTO"] = "Zaczynaj";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>Masz #NUM#. Nie będziesz w stanie zalogować się do swojego Bitrix 24, jeżeli nie włączysz dwustopniowego uwierzytelnienia.</p>";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Dodatkowe zabezpieczenie dla twoich danych biznesowych";
$MESS["INTRANET_OTP_PASS"] = "Pierwsza linia obrony: twoje hasło";
?>