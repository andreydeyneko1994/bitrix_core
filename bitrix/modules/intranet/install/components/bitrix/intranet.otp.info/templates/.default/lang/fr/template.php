<?
$MESS["INTRANET_OTP_CLOSE"] = "Rappele moi plus tard";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "Ne pas afficher de nouveau";
$MESS["INTRANET_OTP_CODE"] = "La deuxième ligne de défense : le code de vérification";
$MESS["INTRANET_OTP_GOTO"] = "Commencer";
$MESS["INTRANET_OTP_MANDATORY_DESCR"] = "<p>Aujourd'hui, votre Bitrix24 est protégé par la technologie de cryptage de données et une paire de l'identifiant et mots de passe pour chaque utilisateur. Cependant, ils y
sont des outils qu'un utilisateur malveillant peut utiliser pour entrer dans votre ordinateur et de voler ces données.</p>
<p>Votre administrateur système a activé l'option de sécurité supplémentaire pour activer l'authentification en deux étapes.</p>
<p>L'authentification en deux étapes signifie que vous aurez à passer deux niveaux de
vérification lors de la connexion. Tout d'abord, vous entrez votre mot de passe. Ensuite, vous aurez à
entrer un code de sécurité envoyé à votre appareil mobile.</p>
<p>Cela rendra vos données professionnelles plus sécurisées.</p>";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>Vous avez #NUM#. Vous ne serez pas en mesure de vous connecter à votre Bitrix24 si vous ne pas activer l'authentification en deux étapes jusque-là.</p>";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Une sécurité supplémentaire pour les données de votre entreprise";
$MESS["INTRANET_OTP_PASS"] = "La première ligne de défense : votre mot de passe";
?>