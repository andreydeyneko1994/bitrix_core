<?
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_PAGETITLE"] = "Medir o nível de estresse";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1"] = "Instale ou abra o aplicativo móvel Bitrix24";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE"] = "App Store";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE_URL"] = "https://itunes.apple.com/app/bitrix24/id561683423";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE"] = "Google Play";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_BUTTON"] = "enviar";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_HINT"] = "Enviar link para o meu celular";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP2"] = "Clique em &laquo;Medir seu nível de estresse&raquo; abaixo ou selecione o item de menu &laquo;Nível de estresse&raquo;";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP3"] = "Clique em &laquo;Medir agora&raquo; e siga as instruções";
?>