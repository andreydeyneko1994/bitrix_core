<?
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_PAGETITLE"] = "Zmierz poziom stresu";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1"] = "Zainstaluj lub otwórz aplikację mobilną Bitrix24";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE"] = "App Store";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE_URL"] = "https://itunes.apple.com/app/bitrix24/id561683423";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE"] = "Google Play";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_BUTTON"] = "wyślij";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_HINT"] = "Wyślij link na mój telefon komórkowy";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP2"] = "Kliknij &laquo;Zmierz swój poziom stresu&raquo; poniżej lub wybierz pozycję menu &laquo;Poziom stresu&raquo;";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP3"] = "Kliknij &laquo;Zmierz teraz&raquo; i postępuj zgodnie z instrukcjami";
?>