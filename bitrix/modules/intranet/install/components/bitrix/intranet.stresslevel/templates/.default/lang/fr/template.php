<?
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_PAGETITLE"] = "Mesurer le niveau de stress";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1"] = "Installez ou ouvrez l'application mobile Bitrix24";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE"] = "App Store";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE_URL"] = "https://itunes.apple.com/app/bitrix24/id561683423";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE"] = "Google Play";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_BUTTON"] = "envoyer";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_HINT"] = "Envoyer le lien sur mon mobile";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP2"] = "Cliquez sur &laquo;Mesurer votre niveau de stress&raquo; ci-dessous et choisissez &laquo;Niveau de stress&raquo; dans le menu";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP3"] = "Cliquez sur &laquo;Mesurer maintenant&raquo; et suivez les instructions";
?>