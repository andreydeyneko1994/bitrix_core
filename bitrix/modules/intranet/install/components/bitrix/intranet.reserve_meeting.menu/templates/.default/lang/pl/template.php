<?
$MESS["INTASK_C27T_CRAETE_TITLE"] = "Utwórz zasób";
$MESS["INTASK_C27T_CREATE"] = "Utwórz zasób";
$MESS["INTASK_C27T_EDIT"] = "Edytuj zasób";
$MESS["INTASK_C27T_EDIT_TITLE"] = "Edytuj parametry zasobu";
$MESS["INTASK_C27T_GRAPH"] = "Harmonogram";
$MESS["INTASK_C27T_GRAPH_TITLE"] = "Grafik rezerwacji zasobów";
$MESS["INTASK_C27T_RESERVE"] = "Rezerwuj";
$MESS["INTASK_C27T_RESERVE_TITLE"] = "Zarezerwuj zasób";
$MESS["ITSRM1_MEETING_LIST"] = "Zasoby";
$MESS["ITSRM1_MEETING_LIST_DESCR"] = "Wyświetl wszystkie zasoby";
$MESS["ITSRM1_MEETING_SEARCH"] = "Przeszukaj zasoby";
$MESS["ITSRM1_MEETING_SEARCH_DESCR"] = "Przeszukaj zasoby";
?>