<?
$MESS["WD_GROUP_FILE_PATH"] = "Strona dokumentów pracy grupowej";
$MESS["WD_IBLOCK_GROUP_ID"] = "Blok informacji dokumentów pracy grupowej";
$MESS["WD_IBLOCK_OTHER_ID"] = "Blok informacji dokumentów";
$MESS["WD_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["WD_IBLOCK_USER_ID"] = "Blok informacji dokumentów użytkownika";
$MESS["WD_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_USER_FILE_PATH"] = "Strona dokumentów użytkownika";
?>