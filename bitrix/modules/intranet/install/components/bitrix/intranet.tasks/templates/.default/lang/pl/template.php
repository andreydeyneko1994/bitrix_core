<?
$MESS["INTST_CANCEL"] = "Anuluj";
$MESS["INTST_CLOSE"] = "Zamknij";
$MESS["INTST_DELETE"] = "Usuń";
$MESS["INTST_FOLDER_NAME"] = "Nazwa folderu";
$MESS["INTST_OUTLOOK_WARNING"] = "<small><b>Uwaga!</b> Zalecane jest, aby zsynchronizować kontakty przed rozpoczęciem zadania synchronizacji z Microsoft Outlook.</small>";
$MESS["INTST_SAVE"] = "Zapisz";
?>