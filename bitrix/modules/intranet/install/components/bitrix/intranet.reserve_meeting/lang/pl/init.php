<?
$MESS["INAF_F_DESCRIPTION"] = "Opis";
$MESS["INAF_F_FLOOR"] = "Piętro";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Nazwa";
$MESS["INAF_F_PHONE"] = "Telefon";
$MESS["INAF_F_PLACE"] = "Pojemność";
$MESS["INTASK_C29_EVENT_LENGTH"] = "Czas trwania";
$MESS["INTASK_C29_PERIOD_ADDITIONAL"] = "Dodatkowe";
$MESS["INTASK_C29_PERIOD_COUNT"] = "Cykliczność";
$MESS["INTASK_C29_PERIOD_TYPE"] = "Typ Okresu";
$MESS["INTASK_C29_UF_PERSONS"] = "Osoby";
$MESS["INTASK_C29_UF_PREPARE_ROOM"] = "Przygotowanie zasobu";
$MESS["INTASK_C29_UF_RES_TYPE"] = "Typ spotkania";
$MESS["INTASK_C29_UF_RES_TYPEA"] = "Konsultacja";
$MESS["INTASK_C29_UF_RES_TYPEB"] = "Prezentacja";
$MESS["INTASK_C29_UF_RES_TYPEC"] = "Negocjacje";
$MESS["INTASK_C29_UF_RES_TYPED"] = "inne";
?>