<?php
$MESS["INTRANET_INVITATION_WIDGET_DESC"] = "¿por qué invitarlos?";
$MESS["INTRANET_INVITATION_WIDGET_DISABLED_TEXT"] = "Solo su administrador de Bitrix24 puede invitar nuevos usuarios a su Bitrix24. Comuníquese con él para invitar a una persona u obtener un permiso de invitación para que pueda hacerlo por su cuenta.";
$MESS["INTRANET_INVITATION_WIDGET_EDIT"] = "Editar";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES"] = "Usuarios";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_LIMIT"] = "Límite alcanzado";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_NO_LIMIT"] = "Usuarios ilimitados";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET"] = "Extranet";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET_DESC"] = "¿Qué es Extranet?";
$MESS["INTRANET_INVITATION_WIDGET_INVITE"] = "Invitar";
$MESS["INTRANET_INVITATION_WIDGET_INVITE_EMPLOYEE"] = "Invitar usuarios";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ADMIN_INVITE"] = "Solo los administradores pueden hacer invitaciones";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ALL_INVITE"] = "Todos pueden hacer invitaciones";
$MESS["INTRANET_INVITATION_WIDGET_STRUCTURE"] = "Estructura";
