<?
$MESS["INTR_MAIL_AJAX_ERROR"] = "Wystąpił błąd podczas przetwarzania żądania.";
$MESS["INTR_MAIL_AUTH"] = "Błąd uwierzytelnienia";
$MESS["INTR_MAIL_B24_PAGE_TITLE"] = "Bitrix24.Mail";
$MESS["INTR_MAIL_CONTROLLER_INVALID"] = "Usługa niedostępna.";
$MESS["INTR_MAIL_CSRF"] = "Błąd zabezpieczeń podczas składania formularza.";
$MESS["INTR_MAIL_DOMAIN_ICON"] = "b24mail-en.png";
$MESS["INTR_MAIL_DOMAIN_PAGE_TITLE"] = "Dodaj domenę e-mail";
$MESS["INTR_MAIL_FORM_ERROR"] = "Błąd przetwarzania formularza.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Wybierz foldery do synchronizacji";
$MESS["INTR_MAIL_INP_DOMAINTOKEN_BAD"] = "Dostarczony token nie pasuje do domeny.";
$MESS["INTR_MAIL_INP_DOMAIN_EMPTY"] = "Wymagana jest nazwa domeny.";
$MESS["INTR_MAIL_INP_DOMAIN_REMOVE"] = "Domena została odłączona pomyślnie.";
$MESS["INTR_MAIL_INP_DOMAIN_WAIT"] = "Oczekiwanie na zakończenie dołączania przez domenę e-maila Yandex";
$MESS["INTR_MAIL_INP_PASSWORD2_BAD"] = "Twoje hasło nie pasuje do potwierdzenia hasła.";
$MESS["INTR_MAIL_INP_TOKEN_EMPTY"] = "Wymagany jest token.";
$MESS["INTR_MAIL_MAILBOX_OCCUPIED"] = "Ta skrzynka pocztowa jest używana przez innego użytkownika.";
$MESS["INTR_MAIL_MANAGE_CHANGE"] = "Edytuj";
$MESS["INTR_MAIL_MANAGE_CREATE"] = "Utwórz";
$MESS["INTR_MAIL_MANAGE_DELETE"] = "Usuń skrzynkę pocztową";
$MESS["INTR_MAIL_MANAGE_PASSWORD"] = "Zmień hasło";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Proszę określić interwał synchronizacji.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Błąd zapisywania danych.";
$MESS["INTR_MAIL_UNAVAILABLE"] = "Usługi e-mail nie są dostępne. Proszę skontaktować się ze swoim administratorem intranetu.";
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "Moduł Mail nie jest zainstalowany.";
$MESS["NTR_MAIL_PAGE_TITLE"] = "Integracja skrzynki odbiorczej";
?>