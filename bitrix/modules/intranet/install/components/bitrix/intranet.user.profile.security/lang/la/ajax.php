<?php
$MESS["INTRANET_USER_PROFILE_AUTH_TITLE"] = "Contraseña de Bitrix24";
$MESS["INTRANET_USER_PROFILE_AUTH_TITLE_2"] = "Autenticación";
$MESS["INTRANET_USER_PROFILE_MAILING_TITLE"] = "Suscripciones al boletín de Bitrix24";
$MESS["INTRANET_USER_PROFILE_PASSWORD_TITLE"] = "Cambiar la contraseña";
$MESS["INTRANET_USER_PROFILE_RECOVERY_CODES_TITLE"] = "Códigos de recuperación";
$MESS["INTRANET_USER_PROFILE_SECURITY_OTP_TITLE"] = "Opciones de seguridad adicional";
$MESS["INTRANET_USER_PROFILE_SECURITY_PASSWORDS_TITLE"] = "Contraseñas de la aplicación";
$MESS["INTRANET_USER_PROFILE_SOCNET_EMAIL_TITLE"] = "Direcciones de reenvío del correo electrónico";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE_TITLE"] = "Sincronizar";
