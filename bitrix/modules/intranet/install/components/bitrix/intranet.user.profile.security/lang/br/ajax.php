<?php
$MESS["INTRANET_USER_PROFILE_AUTH_TITLE"] = "Senha Bitrix24";
$MESS["INTRANET_USER_PROFILE_AUTH_TITLE_2"] = "Autenticação";
$MESS["INTRANET_USER_PROFILE_MAILING_TITLE"] = "Assinaturas do boletim informativo Bitrix24";
$MESS["INTRANET_USER_PROFILE_PASSWORD_TITLE"] = "Alterar senha";
$MESS["INTRANET_USER_PROFILE_RECOVERY_CODES_TITLE"] = "Códigos de recuperação";
$MESS["INTRANET_USER_PROFILE_SECURITY_OTP_TITLE"] = "Opções de segurança adicionais";
$MESS["INTRANET_USER_PROFILE_SECURITY_PASSWORDS_TITLE"] = "Senhas do aplicativo";
$MESS["INTRANET_USER_PROFILE_SOCNET_EMAIL_TITLE"] = "Endereços de encaminhamento de e-mail";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE_TITLE"] = "Sincronizar";
