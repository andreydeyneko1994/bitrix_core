<?php
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CANCEL"] = "Cancelar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CONTINUE"] = "Continuar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_CLOSE"] = "Fechar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT"] = "Desconectar em todos os dispositivos";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_SUCCESS"] = "Você foi desconectado com sucesso em todos os dispositivos.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TEXT"] = "Isso irá desconectar você de todos os Bitrix24 em todos os navegadores e aplicativos, exceto neste que você está vendo esta mensagem.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TITLE"] = "Sair";
$MESS["INTRANET_USER_PROFILE_SECURITY_MENU_TITLE"] = "Configurar";
