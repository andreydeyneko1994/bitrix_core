<?php
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CANCEL"] = "Anuluj";
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CONTINUE"] = "Kontynuuj";
$MESS["INTRANET_USER_PROFILE_PASSWORD_CLOSE"] = "Zamknij";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT"] = "Wyloguj się na wszystkich urządzeniach";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_SUCCESS"] = "Wylogowano z powodzeniem na wszystkich urządzeniach.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TEXT"] = "Nastąpi wylogowanie ze wszystkich kont Bitrix24 we wszystkich przeglądarkach i aplikacjach oprócz tej, w której widzisz ten komunikat.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TITLE"] = "Wyloguj";
$MESS["INTRANET_USER_PROFILE_SECURITY_MENU_TITLE"] = "Konfiguruj";
