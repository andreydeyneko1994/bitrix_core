<?php
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CANCEL"] = "Cancelar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CONTINUE"] = "Continuar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_CLOSE"] = "Cerrar";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT"] = "Cerrar sesión en todos los dispositivos";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_SUCCESS"] = "Se cerró la sesión correctamente en todos los dispositivos.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TEXT"] = "Esto cerrará la sesión en todos los Bitrix24 que estén abiertos en todos los navegadores y aplicaciones, excepto en el que está viendo este mensaje.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TITLE"] = "Cerrar sesión";
$MESS["INTRANET_USER_PROFILE_SECURITY_MENU_TITLE"] = "Configurar";
