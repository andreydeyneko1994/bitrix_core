<?
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Bieżący poziom aktywności firmy (bierze pod uwagę wszystkich użytkowników w ciągu ostatniej godziny)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Bieżące zaangażowanie użytkowników. Pokazuje procentowo wszystkich użytkowników, którzy dzisiaj użyli przynajmniej czterech różnych narzędzi w intranecie.";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "Ładowanie…";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Puls firmy";
?>