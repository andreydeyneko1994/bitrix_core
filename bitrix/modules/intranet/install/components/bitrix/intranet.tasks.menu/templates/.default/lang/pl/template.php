<?
$MESS["INTMT_BACK2LIST"] = "Zadania";
$MESS["INTMT_BACK2LIST_DESCR"] = "Powrót do listy zadań";
$MESS["INTMT_CREATE_FOLDER"] = "Utwórz folder";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Utwórz nowy folder";
$MESS["INTMT_CREATE_TASK"] = "Utwórz zadanie";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Utwórz nowe zadanie";
$MESS["INTMT_CREATE_VIEW"] = "Utwórz widok";
$MESS["INTMT_DEFAULT"] = "domyślne";
$MESS["INTMT_DELETE_VIEW"] = "Usuń widok";
$MESS["INTMT_DELETE_VIEW_CONF"] = "Czy na pewno chcesz usunąć ten widok?";
$MESS["INTMT_EDIT_TASK"] = "Edytuj zadanie";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Edytuj bieżące zadanie";
$MESS["INTMT_EDIT_VIEW"] = "Edytuj widok";
$MESS["INTMT_OUTLOOK"] = "Połącz z Outlook";
$MESS["INTMT_OUTLOOK_TITLE"] = "Outlook synchronizator zadań";
$MESS["INTMT_VIEW"] = "Wyświetl";
$MESS["INTMT_VIEW_TASK"] = "Wyświetl Zadanie";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Widok bieżącego zadania";
?>