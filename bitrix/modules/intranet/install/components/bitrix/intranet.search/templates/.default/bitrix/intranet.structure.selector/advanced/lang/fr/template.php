<?
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Annuler";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Rechercher";
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Département";
$MESS["INTR_ISS_PARAM_DEPARTMENT_MINE"] = "Uniquement mon office";
$MESS["INTR_ISS_PARAM_EMAIL"] = "E-mail";
$MESS["INTR_ISS_PARAM_FIO"] = "Dénomination";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "Mots-clés";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Numéro interne";
$MESS["INTR_ISS_PARAM_POST"] = "Fonction";
$MESS["INTR_ISS_PARAM_WORK_COMPANY"] = "Entreprise";
?>