<?
$MESS["INTRANET_USER_PROFILE_EMPTY_DEPARTMENT_ERROR"] = "Nie wybrano działu";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_NOTIFY"] = "Przeniesiono cię z ekstranetu do intranetu. Aby zmiany weszły w życie, wyloguj się, a następnie ponownie się zaloguj.";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_SUCCESS"] = "Pracownik został przeniesiony. Ma teraz dostęp do zawartości twojego intranetu, zależny od nadanych uprawnień.";
?>