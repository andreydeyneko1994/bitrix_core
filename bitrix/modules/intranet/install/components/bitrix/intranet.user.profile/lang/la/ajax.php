<?
$MESS["INTRANET_USER_PROFILE_EMPTY_DEPARTMENT_ERROR"] = "El departamento no esta seleccionado";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_NOTIFY"] = "Ha sido transferido de la Extranet a la Intranet. Cierre la sesión y vuelva a iniciarla para que los cambios surtan efecto.";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_SUCCESS"] = "El empleado fue transferido con éxito. Ahora tendrá acceso al contenido interno de su Intranet, de acuerdo con sus permisos.";
?>