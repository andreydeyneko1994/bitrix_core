<?php
$MESS["INTRANET_USER_PROFILE_ADDITIONAL_INFO"] = "Informations supplémentaires";
$MESS["INTRANET_USER_PROFILE_ADD_PHOTO"] = "Changer de photo";
$MESS["INTRANET_USER_PROFILE_ADMIN_MODE"] = "Mode administrateur";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL"] = "Installation de l'application";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL_TEXT"] = "Vous recevrez un SMS contenant un lien pour télécharger et installer l'application mobile de Bitrix24.";
$MESS["INTRANET_USER_PROFILE_APP_PHONE"] = "Téléphone";
$MESS["INTRANET_USER_PROFILE_APP_SEND"] = "Envoyer";
$MESS["INTRANET_USER_PROFILE_AVATAR_CAMERA"] = "Prendre une photo";
$MESS["INTRANET_USER_PROFILE_AVATAR_LOAD"] = "Télécharger une photo";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_ADD"] = "Envoyer une appréciation";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_TITLE"] = "Appréciations";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_MODIFY"] = "éditer";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_BUTTON"] = "Parlez-nous de vous";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_TEXT"] = "Partagez des histoires de vie intéressantes ou parlez à d'autres utilisateurs de vous-même, téléchargez des photos de moments mémorables.";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_TITLE"] = "À propos de moi";
$MESS["INTRANET_USER_PROFILE_CHAT"] = "Chat";
$MESS["INTRANET_USER_PROFILE_CHAT_HISTORY"] = "Journal des messages";
$MESS["INTRANET_USER_PROFILE_CLOSE"] = "Fermer";
$MESS["INTRANET_USER_PROFILE_CONTACT_INFO"] = "Informations de contact";
$MESS["INTRANET_USER_PROFILE_DELETE"] = "Supprimer";
$MESS["INTRANET_USER_PROFILE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer l'utilisateur invité ?";
$MESS["INTRANET_USER_PROFILE_DESKTOP_APP"] = "Application de bureau";
$MESS["INTRANET_USER_PROFILE_DISK_INSTALLED"] = "Bitrix24.Drive installé";
$MESS["INTRANET_USER_PROFILE_DISK_SPACE"] = "espace disque : #VALUE#";
$MESS["INTRANET_USER_PROFILE_FIELD_LAST_NAME"] = "Nom de famille";
$MESS["INTRANET_USER_PROFILE_FIELD_MANAGERS"] = "Superviseur";
$MESS["INTRANET_USER_PROFILE_FIELD_NAME"] = "Prénom";
$MESS["INTRANET_USER_PROFILE_FIELD_SECOND_NAME"] = "Deuxième prénom";
$MESS["INTRANET_USER_PROFILE_FIELD_SUBORDINATE"] = "Subordonnés";
$MESS["INTRANET_USER_PROFILE_FIRE"] = "Renvoyer";
$MESS["INTRANET_USER_PROFILE_FIRE_CONFIRM"] = "L'employé ne pourra plus se connecter à Bitrix24 et n'apparaîtra plus dans la structure de l'entreprise. Toutes ses données (fichiers, messages, tâches, etc.) resteront intactes.
<br/><br/>
Voulez-vous vraiment renvoyer l'employé ?";
$MESS["INTRANET_USER_PROFILE_HIRE"] = "Engager";
$MESS["INTRANET_USER_PROFILE_HIRE_CONFIRM"] = "L'employé pourra se connecter à Bitrix24 et apparaîtra dans la structure de l'entreprise.
<br/><br/>
Voulez-vous vraiment accorder l'accès d'employé ?";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON"] = "Parlez de vos centres d'intérêt";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON_2"] = "Sélectionnez les centres d'intérêt";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_TEXT"] = "Créez ou rejoignez des centres d'intérêt. Découvrez quels amis ont les mêmes.";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_F"] = "vu(e) pour la dernière fois #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_F"] = "inactif(ve) depuis #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_M"] = "inactif(ve) depuis #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_M"] = "vu(e) pour la dernière fois #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_MOBILE_APP"] = "Application mobile";
$MESS["INTRANET_USER_PROFILE_MORE"] = "Afficher plus #NUM#";
$MESS["INTRANET_USER_PROFILE_MOVE"] = "Transférer";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM"] = "Votre Bitrix24 a atteint le maximum d'administrateurs possible.<br/>Vos privilèges d'administrateur seront révoqués si vous définissez cet utilisateur comme administrateur. Voulez-vous vraiment transférer vos permissions d'administrateurs à cet utilisateur ?";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM_PROMO"] = "#LINK_START#Profitez mieux de votre Bitrix24#LINK_END#";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET"] = "Transférer dans l'intranet";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_TITLE"] = "Transférer dans l'intranet";
$MESS["INTRANET_USER_PROFILE_NO"] = "Non";
$MESS["INTRANET_USER_PROFILE_PASSWORDS"] = "Mots de passe";
$MESS["INTRANET_USER_PROFILE_PHOTO_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cette photo ?";
$MESS["INTRANET_USER_PROFILE_QUIT_ADMIN_MODE"] = "Quitter le mode administrateur";
$MESS["INTRANET_USER_PROFILE_REINVITE"] = "Réinviter";
$MESS["INTRANET_USER_PROFILE_REINVITE_SUCCESS"] = "L'invitation a été envoyée";
$MESS["INTRANET_USER_PROFILE_REMOVE_ADMIN_RIGHTS"] = "Révoquer les permissions d'administrateur";
$MESS["INTRANET_USER_PROFILE_RIGHTS"] = "Permissions";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_MORE"] = "Plus d'informations";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT1"] = "Profitez mieux de votre Bitrix24";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT2"] = "Désignez plusieurs administrateurs pour assurer que votre Bitrix24 soit bien géré à tout moment ! Les administrateurs disposent d'un accès complet à toutes les fonctionnalités, dont la téléphonie, les canaux ouverts et les paramètres ! Vous pouvez désigner autant d'administrateurs que le permet votre offre Bitrix24 :";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TITLE"] = "Limite d'administrateurs";
$MESS["INTRANET_USER_PROFILE_SECURITY"] = "Sécurité";
$MESS["INTRANET_USER_PROFILE_SET_ADMIN_RIGHTS"] = "Assigner des permissions d'administrateur";
$MESS["INTRANET_USER_PROFILE_SET_INEGRATOR_RIGHTS"] = "Accorder les droits de partenaire Bitrix24";
$MESS["INTRANET_USER_PROFILE_SET_INTEGRATOR_RIGHTS_CONFIRM"] = "Les partenaires Bitrix24 certifiés peuvent vous aider à configurer et ajuster votre Bitrix24 (CRM, canaux ouverts, documentation, téléphonie, rapports et autres outils commerciaux) pour qu'il fonctionne avec les flux de travail de votre entreprise. 
#LINK_START#Plus d'informations#LINK_END#
<br/><br/>
#NAME# recevra toutes les permissions d'accès, sauf les droits d'inviter et supprimer des administrateurs. 
Voulez-vous vraiment accorder les droits de partenaire Bitrix24 à cet utilisateur ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_BUTTON"] = "Comment mesurer ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_INDICATOR_TEXT"] = "stress";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_TITLE"] = "Mesurez votre niveau de stress";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_INVISIBLE"] = "Les autres utilisateurs ne peuvent pas voir votre niveau de stress.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_VISIBLE"] = "Les autres utilisateurs peuvent voir votre niveau de stress.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_SHARE_LINK"] = "copier dans le presse-papiers";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE"] = "Synchroniser";
$MESS["INTRANET_USER_PROFILE_TAGS_MODIFY"] = "éditer";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_ADD"] = "Ajouter un élément";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_2"] = "Vous ne pouvez rejoindre aucun centre d'intérêt pour le moment. Créez les vôtres !";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_3"] = "Pour ajouter des centres d'intérêt, saisissez-les et appuyez sur Entrée. Ajoutez-en plusieurs en les séparant d'un espace.";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_SEARCH_TITLE"] = "Résultat de la recherche :";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_TITLE"] = "Tags populaires :";
$MESS["INTRANET_USER_PROFILE_TAGS_TITLE"] = "Centres d’intérêt";
$MESS["INTRANET_USER_PROFILE_TARIFF_COMPANY"] = "Professional";
$MESS["INTRANET_USER_PROFILE_TARIFF_PROJECT"] = "Gratuit";
$MESS["INTRANET_USER_PROFILE_TARIFF_TEAM"] = "Standard";
$MESS["INTRANET_USER_PROFILE_TARIFF_TF"] = "Plus";
$MESS["INTRANET_USER_PROFILE_THEME"] = "Modifier le fond";
$MESS["INTRANET_USER_PROFILE_UNLIM"] = "illimité";
$MESS["INTRANET_USER_PROFILE_VIDEOCALL"] = "Appel vidéo";
$MESS["INTRANET_USER_PROFILE_VIEW_ACCESS_DENIED"] = "L'accès au profil d'utilisateur est refusé.";
$MESS["INTRANET_USER_PROFILE_YES"] = "Oui";
$MESS["INTRANET_USER_PROFILE_admin"] = "Administrateur";
$MESS["INTRANET_USER_PROFILE_email"] = "Utilisateur d'e-mail";
$MESS["INTRANET_USER_PROFILE_employee"] = "Actions";
$MESS["INTRANET_USER_PROFILE_extranet"] = "Extranet";
$MESS["INTRANET_USER_PROFILE_fired"] = "Renvoyé";
$MESS["INTRANET_USER_PROFILE_integrator"] = "Partenaire";
$MESS["INTRANET_USER_PROFILE_invited"] = "Invité";
$MESS["INTRANET_USER_PROFILE_shop"] = "Utilisateur de la boutique";
$MESS["INTRANET_USER_PROFILE_visitor"] = "Guest";
$MESS["NTRANET_USER_PROFILE_MOBILE_GOOGLE_PLAY_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
