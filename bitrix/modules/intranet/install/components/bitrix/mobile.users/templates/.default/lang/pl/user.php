<?
$MESS["BM_BIRTHDAY"] = "Data urodzin";
$MESS["BM_DEPARTMENT"] = "Dział";
$MESS["BM_DIRECTOR"] = "Podległe";
$MESS["BM_DIRECTOR_OF"] = "Nadzoruje";
$MESS["BM_NO_USERS"] = "Brak użytkowników";
$MESS["BM_PHONE"] = "Telefon";
$MESS["BM_PHONE_INT"] = "Wewnętrzne";
$MESS["BM_PHONE_MOB"] = "Telefon Komórkowy";
$MESS["BM_TO_USER_LIST"] = "Powrót do listy";
$MESS["BM_USR_CNT"] = "Brak pracowników";
$MESS["BM_WRITE"] = "Napisz";
?>