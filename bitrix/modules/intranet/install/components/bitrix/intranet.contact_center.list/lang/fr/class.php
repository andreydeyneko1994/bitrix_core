<?php
$MESS["CONTACT_CENTER_CALL"] = "Rappel";
$MESS["CONTACT_CENTER_CRM_FORMS_CREATE"] = "Créer un formulaire";
$MESS["CONTACT_CENTER_CRM_FORMS_HELP"] = "Comment cela fonctionne ?";
$MESS["CONTACT_CENTER_CRM_FORMS_VIEW_ALL"] = "Formulaires";
$MESS["CONTACT_CENTER_FORM"] = "Formulaire du site";
$MESS["CONTACT_CENTER_FORM_ADD"] = "Ajouter un nouveau formulaire";
$MESS["CONTACT_CENTER_MAIL"] = "Courrier";
$MESS["CONTACT_CENTER_TELEPHONY"] = "Téléphonie";
$MESS["CONTACT_CENTER_WIDGET"] = "Widget du site";
$MESS["CONTACT_CENTER_WIDGET_ADD"] = "Ajouter un nouveau widget";
