<?php
$MESS["CONTACT_CENTER_CALL"] = "Retorno de chamada";
$MESS["CONTACT_CENTER_CRM_FORMS_CREATE"] = "Criar formulário";
$MESS["CONTACT_CENTER_CRM_FORMS_HELP"] = "Como funciona?";
$MESS["CONTACT_CENTER_CRM_FORMS_VIEW_ALL"] = "Formulários";
$MESS["CONTACT_CENTER_FORM"] = "Formulário do site";
$MESS["CONTACT_CENTER_FORM_ADD"] = "Adicionar novo formulário";
$MESS["CONTACT_CENTER_MAIL"] = "E-mail";
$MESS["CONTACT_CENTER_TELEPHONY"] = "Telefonia";
$MESS["CONTACT_CENTER_WIDGET"] = "Widgets do site";
$MESS["CONTACT_CENTER_WIDGET_ADD"] = "Adicionar novo widget";
