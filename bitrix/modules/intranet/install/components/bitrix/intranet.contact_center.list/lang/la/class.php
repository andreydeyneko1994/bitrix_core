<?php
$MESS["CONTACT_CENTER_ADS_FORM_FACEBOOK"] = "Prospectos de Anuncio de Facebook";
$MESS["CONTACT_CENTER_ADS_FORM_SETTINGS_FORM"] = "Editar formulario";
$MESS["CONTACT_CENTER_ADS_FORM_SETTINGS_LINK"] = "Editar #NAME# parámetros de conexión";
$MESS["CONTACT_CENTER_ADS_FORM_SHORTNAME_FACEBOOK"] = "Facebook";
$MESS["CONTACT_CENTER_ADS_FORM_SHORTNAME_VKONTAKTE"] = "VK";
$MESS["CONTACT_CENTER_ADS_FORM_VKONTAKTE"] = "Formularios VK";
$MESS["CONTACT_CENTER_CALL"] = "Volver a llamar";
$MESS["CONTACT_CENTER_CRM_FORMS_CREATE"] = "Crear un formulario";
$MESS["CONTACT_CENTER_CRM_FORMS_HELP"] = "¿Cómo funciona?";
$MESS["CONTACT_CENTER_CRM_FORMS_VIEW_ALL"] = "Formularios";
$MESS["CONTACT_CENTER_FORM"] = "Formulario de sitio web";
$MESS["CONTACT_CENTER_FORM_ADD"] = "Agregar nuevo formulario";
$MESS["CONTACT_CENTER_FORM_CREATE"] = "Agregar nuevo formulario";
$MESS["CONTACT_CENTER_FORM_LINK"] = "Formulario de enlace con #NAME#";
$MESS["CONTACT_CENTER_MAIL"] = "Correo electrónico";
$MESS["CONTACT_CENTER_TELEPHONY"] = "Telefonía";
$MESS["CONTACT_CENTER_WIDGET"] = "Widget de sitio web";
$MESS["CONTACT_CENTER_WIDGET_ADD"] = "Agregar nuevo widget";
