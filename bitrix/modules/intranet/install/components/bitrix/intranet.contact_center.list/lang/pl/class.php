<?php
$MESS["CONTACT_CENTER_CALL"] = "Oddzwaniać";
$MESS["CONTACT_CENTER_CRM_FORMS_CREATE"] = "Utwórz formularz";
$MESS["CONTACT_CENTER_CRM_FORMS_HELP"] = "Jak to działa?";
$MESS["CONTACT_CENTER_CRM_FORMS_VIEW_ALL"] = "Formularze";
$MESS["CONTACT_CENTER_FORM"] = "Formularz kontaktowy";
$MESS["CONTACT_CENTER_FORM_ADD"] = "Dodaj nowy formularz";
$MESS["CONTACT_CENTER_MAIL"] = "Poczta";
$MESS["CONTACT_CENTER_TELEPHONY"] = "Telefonia";
$MESS["CONTACT_CENTER_WIDGET"] = "Widżet na stronę";
$MESS["CONTACT_CENTER_WIDGET_ADD"] = "Dodaj nowy widżet";
