<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE"] = "Renvoyer";
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_TITLE"] = "Renvoyer l'employé";
$MESS["INTRANET_USER_LIST_ACTION_DELETE"] = "Supprimer";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_TITLE"] = "Supprimer l'invitation";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE"] = "Envoyer un message";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_HISTORY"] = "Journal des messages";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_HISTORY_TITLE"] = "Afficher le journal des messages de l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_TITLE"] = "Envoyer un message à l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE"] = "Réinviter";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_TITLE"] = "Réinviter l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE"] = "Engager";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_TITLE"] = "Rengager l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_TASK"] = "Assigner une tâche";
$MESS["INTRANET_USER_LIST_ACTION_TASK_TITLE"] = "Assigner une tâche à l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_VIDEOCALL"] = "Appel vidéo";
$MESS["INTRANET_USER_LIST_ACTION_VIDEOCALL_TITLE"] = "Démarrer un appel vidéo avec l'utilisateur";
$MESS["INTRANET_USER_LIST_ACTION_VIEW"] = "Afficher le profil";
$MESS["INTRANET_USER_LIST_ACTION_VIEW_TITLE"] = "Afficher le profil de l'utilisateur";
$MESS["INTRANET_USER_LIST_BUTTON_INVITE_TITLE"] = "Inviter des employés";
$MESS["INTRANET_USER_LIST_GENDER_F"] = "femme";
$MESS["INTRANET_USER_LIST_GENDER_M"] = "homme";
$MESS["INTRANET_USER_LIST_MENU_EXPORT_EXCEL_TITLE"] = "Exporter vers Microsoft Excel";
$MESS["INTRANET_USER_LIST_MENU_SYNC_CARDDAV_TITLE"] = "Synchroniser avec CardDAV";
$MESS["INTRANET_USER_LIST_MENU_SYNC_OUTLOOK_TITLE"] = "Synchroniser avec Microsoft Outlook";
?>