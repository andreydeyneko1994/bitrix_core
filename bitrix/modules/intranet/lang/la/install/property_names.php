<?
$MESS["UF_1C"] = "Usuario desde 1C";
$MESS["UF_DEPARTMENT"] = "Departamentos";
$MESS["UF_DISTRICT"] = "Distrito";
$MESS["UF_EMPLOYMENT_DATE"] = "Fecha de contratación";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "NIF";
$MESS["UF_INTERESTS"] = "Intereses";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Número de extensión";
$MESS["UF_SKILLS"] = "Experiencias";
$MESS["UF_SKYPE"] = "Nombre de usuario de Skype";
$MESS["UF_SKYPE_LINK"] = "Enlace de chat de Skype";
$MESS["UF_STATE_HISTORY"] = "Historia del estado";
$MESS["UF_STATE_LAST"] = "Último estado";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Otros sitios web";
$MESS["UF_XING"] = "Xing";
$MESS["UF_ZOOM"] = "Zoom";
?>