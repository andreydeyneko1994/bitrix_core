<?php
$MESS["TOP_MENU_ABSENCE"] = "Gráfico de ausencias";
$MESS["TOP_MENU_MEETING"] = "Reuniones y sesiones";
$MESS["TOP_MENU_MONITOR_REPORT"] = "Asistente personal del horario laboral";
$MESS["TOP_MENU_SETTINGS_PERMISSIONS"] = "Permisos de acceso";
$MESS["TOP_MENU_TIMEMAN"] = "Tiempo de trabajo";
$MESS["TOP_MENU_WORK_REPORT"] = "Reportes de trabajo";
$MESS["TOP_MENU_WORK_SCHEDULES"] = "Horarios de trabajo";
