<?
$MESS["ABOUT_MENU_ABOUT"] = "Acerca de la compañía";
$MESS["ABOUT_MENU_CALENDAR"] = "Calendario de eventos";
$MESS["ABOUT_MENU_CAREER"] = "Profesión";
$MESS["ABOUT_MENU_LIFE"] = "Su compañía";
$MESS["ABOUT_MENU_NEWS"] = "Noticias de la compañía (RSS)";
$MESS["ABOUT_MENU_OFFICIAL"] = "Información oficial";
$MESS["ABOUT_MENU_PHOTO"] = "Galería de fotos";
$MESS["ABOUT_MENU_VIDEO"] = "Vídeo";
?>