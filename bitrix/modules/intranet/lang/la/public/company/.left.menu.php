<?php
$MESS["COMPANY_MENU_ABSENCE"] = "Gráfico de ausencias";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Cumpleaños";
$MESS["COMPANY_MENU_CONFERENCES"] = "Reuniones en línea";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Buscar empleado";
$MESS["COMPANY_MENU_EMPLOYEE_LIST"] = "Empleados";
$MESS["COMPANY_MENU_EVENTS"] = "Cambios en el personal";
$MESS["COMPANY_MENU_GALLERY"] = "Fotos compartidas";
$MESS["COMPANY_MENU_KNOWLEDGE_BASE"] = "Base de conocimientos";
$MESS["COMPANY_MENU_LEADERS"] = "Empleados honorables";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Mis solicitudes";
$MESS["COMPANY_MENU_REPORT"] = "Reporte de eficiencia";
$MESS["COMPANY_MENU_STRUCTURE"] = "Estructura de la compañía";
$MESS["COMPANY_MENU_TELEPHONES"] = "Directorio telefónico";
$MESS["COMPANY_MENU_TIMEMAN"] = "Control de tiempo";
$MESS["COMPANY_MENU_TIMEMAN_SECTION"] = "Tiempo y reportes";
$MESS["COMPANY_MENU_WORKREPORT"] = "Reportes de trabajo";
