<?
$MESS["SERVICES_MENU_BOARD"] = "Clasificados";
$MESS["SERVICES_MENU_BP"] = "Procesos de negocios";
$MESS["SERVICES_MENU_CONTACT_CENTER"] = "Contact center";
$MESS["SERVICES_MENU_EVENTLIST"] = "Registro de cambios";
$MESS["SERVICES_MENU_FAQ"] = "FAQ";
$MESS["SERVICES_MENU_IDEA"] = "Ideas";
$MESS["SERVICES_MENU_LEARNING"] = "Educación";
$MESS["SERVICES_MENU_LINKS"] = "Directorio de links";
$MESS["SERVICES_MENU_LISTS"] = "Listas";
$MESS["SERVICES_MENU_MEETING"] = "Reuniones y sesiones";
$MESS["SERVICES_MENU_MEETING_ROOM"] = "Reserva de sala de reuniones";
$MESS["SERVICES_MENU_OPENLINES"] = "Canales abiertos";
$MESS["SERVICES_MENU_PROCESSES"] = "Procesos";
$MESS["SERVICES_MENU_REQUESTS"] = "e-Orders";
$MESS["SERVICES_MENU_SALARY"] = "Planilla y vacaciones";
$MESS["SERVICES_MENU_SUBSCR"] = "Suscripciones";
$MESS["SERVICES_MENU_SUPPORT"] = "Soporte técnico";
$MESS["SERVICES_MENU_TELEPHONY"] = "Telefonía";
$MESS["SERVICES_MENU_VOTE"] = "Encuestas";
$MESS["SERVICES_MENU_WIKI"] = "Wiki";
?>