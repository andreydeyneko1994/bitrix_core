<?
$MESS["SERVICES_MENU_ABSENCE_HELP"] = "Planificación de ausencia";
$MESS["SERVICES_MENU_BP"] = "Procesos de negocios";
$MESS["SERVICES_MENU_EXTRANET"] = "Extranet";
$MESS["SERVICES_MENU_NOVICE"] = "Para los nuevos empleados";
$MESS["SERVICES_MENU_OUTLOOK"] = "Integración MS Outlook";
$MESS["SERVICES_MENU_XMPP"] = "Mensajería instantánea";
$MESS["SERVICES_TITLE"] = "Ayuda";
?>