<?
$MESS["I_NEW_USER_EXTERNAL_TITLE"] = "Nuevo usuario externo agregado";
$MESS["I_NEW_USER_MENTION"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#";
$MESS["I_NEW_USER_MENTION_M"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#";
$MESS["I_NEW_USER_TITLE"] = "Un nuevo empleado ha sido agregado";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nuevos empleados";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nuevos empleados";
?>