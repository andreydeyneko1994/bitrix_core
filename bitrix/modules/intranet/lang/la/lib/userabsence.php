<?
$MESS["INTR_USER_ABSENCE_TYPE_ASSIGNMENT"] = "Viaje de negocios";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVEMATERINITY"] = "Licencia por maternidad";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVESICK"] = "Licencia por enfermedad";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVEUNPAYED"] = "Licencia sin goce de sueldo";
$MESS["INTR_USER_ABSENCE_TYPE_OTHER"] = "Otros";
$MESS["INTR_USER_ABSENCE_TYPE_PERSONAL"] = "Calendarios personales";
$MESS["INTR_USER_ABSENCE_TYPE_UNKNOWN"] = "Ausente sin permiso";
$MESS["INTR_USER_ABSENCE_TYPE_VACATION"] = "Vacaciones anuales";
$MESS["USER_ABSENCE_STATUS_VACATION"] = "De vacaciones";
?>