<?php
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_COMPONENT_NOT_FOUND"] = "No se puede mostrar la página de la sección actual";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_INVALID_URL"] = "El formato del enlace es incorrecto";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_AVAILABLE"] = "La sección no tiene páginas que se puedan visualizar";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_FOUND"] = "No se encontró la sección";
