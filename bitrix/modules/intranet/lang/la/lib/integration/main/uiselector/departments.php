<?php
$MESS["MAIN_UI_SELECTOR_DEPARTMENT_FLAT_PATTERN"] = "#NAME# (sin subdepartamentos)";
$MESS["MAIN_UI_SELECTOR_EXTRANET"] = "Extranet";
$MESS["MAIN_UI_SELECTOR_SELECT_FLAT_TEXT_DEPARTMENTS"] = "Todos los empleados del departamento";
$MESS["MAIN_UI_SELECTOR_SELECT_TEXT_DEPARTMENTS"] = "Todos los empleados del departamento y subdepartamento";
$MESS["MAIN_UI_SELECTOR_TAB_DEPARTMENTS"] = "Los empleados y departamentos";
$MESS["MAIN_UI_SELECTOR_TITLE_DEPARTMENTS"] = "Departamentos:";
