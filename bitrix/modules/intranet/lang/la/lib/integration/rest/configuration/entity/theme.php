<?php
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_NOT_FOUND"] = "El tema que seleccionó no se encontró en su Bitrix24.";
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_SET_DEFAULT"] = "No se pudo convertir el tema importado en un tema predeterminado.";
