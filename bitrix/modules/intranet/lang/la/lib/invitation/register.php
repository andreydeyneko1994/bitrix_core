<?php
$MESS["INTRANET_INVITATION_EMAIL_ERROR"] = "Las direcciones de correo electrónico son incorrectas:";
$MESS["INTRANET_INVITATION_EMAIL_LIMIT_EXCEEDED"] = "Se superó el número máximo de direcciones de correo electrónico permitidas en la invitación.";
$MESS["INTRANET_INVITATION_ERROR_USER_TRANSFER"] = "No se puede convertir el usuario del correo electrónico.";
$MESS["INTRANET_INVITATION_INVITE_MESSAGE_TEXT"] = "¡Únase a nuestro Bitrix24 empresarial! Aquí es donde colaboramos, administramos proyectos, tareas y documentos, planificamos eventos o reuniones, nos comunicamos y participamos en otras actividades.";
$MESS["INTRANET_INVITATION_MAX_COUNT_ERROR"] = "El número de personas invitadas excede los términos de la licencia.";
$MESS["INTRANET_INVITATION_PHONE_ERROR"] = "Los números de teléfono son incorrectos:";
$MESS["INTRANET_INVITATION_PHONE_LIMIT_EXCEEDED"] = "Se agregaron demasiados números de teléfono a la invitación.";
$MESS["INTRANET_INVITATION_USER_EXIST_ERROR"] = "Ya existen usuarios con estas direcciones de correo electrónico: #EMAIL_LIST#";
$MESS["INTRANET_INVITATION_USER_PHONE_EXIST_ERROR"] = "Ya existen usuarios con estos números de teléfono: #PHONE_LIST#";
