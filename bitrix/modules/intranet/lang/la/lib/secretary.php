<?php
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_EMPTY_SUBJECT"] = "Mensaje de correo electrónico #MESSAGE_ID#";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_FIRST_MESSAGE"] = "Chat creado a partir del mensaje de correo electrónico \"#MAIL_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_TITLE"] = "Chat creado a partir del mensaje de correo electrónico \"#MAIL_TITLE#\"";
