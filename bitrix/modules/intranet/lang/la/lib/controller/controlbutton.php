<?php
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_FIRST_MESSAGE"] = "Chat creado basado en el evento \"#EVENT_TITLE#\" del #DATETIME_FROM#";
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_TITLE"] = "Chat \"#EVENT_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_CREATE_CHAT_LOCK_ERROR"] = "Error al crear el chat. Intente de nuevo.";
$MESS["INTRANET_CONTROL_BUTTON_DELETE_TASK_FILE_ERROR"] = "Error al eliminar el archivo";
$MESS["INTRANET_CONTROL_BUTTON_ENTITY_ERROR"] = "No se encontró el elemento.";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_CALENDAR"] = "Creado con base en el [url=#LINK#]evento del calendario[/url]";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_TASK"] = "Creado con base en la [url=#LINK#]tarea[/url]";
$MESS["INTRANET_CONTROL_BUTTON_TASK_CHAT_FIRST_MESSAGE"] = "Chat creado basado en la tarea \"#TASK_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_LIMIT"] = "Se superó el número máximo de usuarios de las videollamadas";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_SELF_ERROR"] = "Una videollamada requiere dos o más participantes. Agregue más usuarios y vuelva a llamar.";
$MESS["INTRANET_CONTROL_MEETING_CREATED"] = "Evento creado basado en la tarea";
