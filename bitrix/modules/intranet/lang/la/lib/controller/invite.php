<?php
$MESS["INTRANET_CONTROLLER_INVITE_DELETE_FAILED"] = "Error al eliminar la invitación";
$MESS["INTRANET_CONTROLLER_INVITE_FAILED"] = "Error al aceptar la invitación";
$MESS["INTRANET_CONTROLLER_INVITE_NO_PERMISSIONS"] = "No tiene permiso para invitar usuarios.";
$MESS["INTRANET_CONTROLLER_INVITE_NO_USER_ID"] = "Falta el parámetro userId";
$MESS["INTRANET_CONTROLLER_INVITE_USER_NOT_FOUND"] = "No se encontró el usuario con el ID especificado.";
