<?php
$MESS["MENU_BLOG"] = "Conversaciones";
$MESS["MENU_CONTACT"] = "Contactos";
$MESS["MENU_EMPLOYEE"] = "Empleados";
$MESS["MENU_FILES"] = "Archivos";
$MESS["MENU_GROUPS"] = "Grupos de trabajo";
$MESS["MENU_GROUPS_EXTRANET_ALL"] = "Todos los grupos de trabajo";
$MESS["MENU_LIVE_FEED"] = "Noticias";
$MESS["MENU_LIVE_FEED2"] = "Noticias";
$MESS["MENU_TASKS"] = "Tareas";
