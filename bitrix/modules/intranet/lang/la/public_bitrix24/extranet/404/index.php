<?php
$MESS["EXTRANET_404_TEXT"] = "<p>No se puede acceder a la Extranet.</p>
<p>- Haga clic en \"Atrás\" en su navegador para llegar a la página anterior</p>
<p>- O abrir <a href=\"/\">Noticias</a></p>";
$MESS["EXTRANET_404_TITLE"] = "No se han encontrado entradas";
