<?php
$MESS["MENU_ABSENCE"] = "Gráfico de ausencias";
$MESS["MENU_MEETING"] = "Reuniones y sesiones";
$MESS["MENU_MONITOR_REPORT"] = "Asistente personal del horario laboral";
$MESS["MENU_SCHEDULES"] = "Horarios de trabajo";
$MESS["MENU_TIMEMAN"] = "Tiempo de trabajo";
$MESS["MENU_WORKTIME_SETTINGS_PERMISSIONS"] = "Permisos de acceso";
$MESS["MENU_WORKTIME_STATS"] = "Tiempo de trabajo";
$MESS["MENU_WORK_REPORT"] = "Reportes de trabajo";
