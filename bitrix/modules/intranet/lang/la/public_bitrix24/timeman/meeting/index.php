<?
$MESS["TARIFF_RESTRICTION_TEXT"] = "Bitrix24 puede ayudarlo a planificar y organizar eventos y reuniones con un planificador especial de reuniones y briefings. Las tareas serán asignadas a las personas designadas, los debates y los procedimientos registrados, las personas responsables se les pedirá que creen un informe para sus respectivos temas de la agenda. <a href=\"javascript:void(0)\" onclick='top.BX.Helper.show(\"redirect=detail&code=1436032\");'>Detalles</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Las reuniones y reuniones informativas solo están disponibles en los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a>.";
$MESS["TITLE"] = "Reuniones y sesiones informativas";
?>