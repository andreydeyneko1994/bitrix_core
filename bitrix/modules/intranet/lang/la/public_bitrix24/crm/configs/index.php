<?
$MESS["BP"] = "Business processes";
$MESS["CONFIG"] = "Otros ajustes";
$MESS["CURRENCY"] = "Monedas";
$MESS["EXCH1C"] = "1C integración";
$MESS["EXTERNAL_SALE"] = "Tiendas online";
$MESS["FIELDS"] = "Campos personalizados";
$MESS["LOCATIONS"] = "Ubicaciones";
$MESS["MAIL_TEMPLATES"] = "Plantillas de correo electrónico";
$MESS["MEASURE"] = "Unidades de medida";
$MESS["PERMS"] = "Permisos de acceso";
$MESS["PRODUCT_PROPS"] = "Propiedades del producto";
$MESS["PS"] = "Pago y facturas";
$MESS["SENDSAVE"] = "Integración de correo electrónico";
$MESS["SLOT"] = "Reportes Analíticos";
$MESS["STATUS"] = "Listas de selección";
$MESS["TAX"] = "Impuestos";
$MESS["TITLE"] = "Ajustes";
?>