<?php
$MESS["MARKETPLACE_BLOCK1_INFO"] = "<p>- crear una aplicación</p>
<p>- agregar a su cuenta</p>";
$MESS["MARKETPLACE_BLOCK1_TITLE"] = "Sólo mi cuenta";
$MESS["MARKETPLACE_BLOCK2_INFO"] = "<p>- hagase socio</p>
<p>- crear una aplicación</p>
<p>- cargue la aplicación en su perfil de socio</p>
<p>- publicar su aplicación</p>";
$MESS["MARKETPLACE_BLOCK2_LINK"] = "https://www.bitrix24.com/apps/dev.php";
$MESS["MARKETPLACE_BLOCK2_TITLE"] = "Lista en el Marketplace";
$MESS["MARKETPLACE_BUTTON"] = "Continuar";
$MESS["MARKETPLACE_BUTTON_ADD"] = "Agregar";
$MESS["MARKETPLACE_OR"] = "o";
$MESS["MARKETPLACE_PAGE_TITLE"] = "¿Cómo puede agregar su aplicación a Bitrix24?";
$MESS["MARKETPLACE_PAGE_TITLE2"] = "Eine Anwendung kann von Ihren Entwicklern erstellt werden, oder Sie können sie <a target='_blank' href=\"https://www.bitrix24.de/partners/\">bei einem unserer Partner</a> bestellen.";
$MESS["MARKETPLACE_TITLE"] = "Agregar aplicación";
