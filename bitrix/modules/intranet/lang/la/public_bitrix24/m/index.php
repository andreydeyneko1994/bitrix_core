<?php
$MESS["COMPANY"] = "Empleados";
$MESS["DOCS"] = "Documentos";
$MESS["FULL_VERSION"] = "Intranet (versión web)";
$MESS["MESSAGES"] = "Mensajes";
$MESS["TASKS"] = "Tareas";
$MESS["TITLE"] = "Intranet de la Compañía";
$MESS["UPDATES"] = "Noticias";
