<?php
$MESS["MENU_ADMIN_PANEL"] = "Panel de control de la tienda online";
$MESS["MENU_CONFIGS"] = "Configuraciones de la cuenta";
$MESS["MENU_EVENT_LOG"] = "Registro de eventos";
$MESS["MENU_MAIL_BLACKLIST"] = "Lista negra";
$MESS["MENU_MAIL_MANAGE"] = "Administrar cuentas de e-mail";
$MESS["MENU_UPDATE_DESC"] = "Historial de versiones";
$MESS["MENU_VM"] = "Dispositivo virtual";
