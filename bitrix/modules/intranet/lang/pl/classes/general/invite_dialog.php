<?
$MESS["BX24_CLOSE_BUTTON"] = "Zamknij";
$MESS["BX24_INVITE_BUTTON"] = "Zaproś";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Adresy e-mail są niepoprawne:<br/>";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Przekroczono maksymalną liczbę adresów e-mail w zaproszeniu.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "Adres e-mail nie został określony.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "Adresy e-mail nie zostały określone.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "Nazwisko jest wymagane.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "Hasło nie jest określone.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "Nie można przenieść e-maila użytkownika.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "Hasło nie pasuje do potwierdzenia hasła.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Określono nieprawidłowy e-mail użytkownika.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Dołącz do nas na naszym koncie Bitrix24. To miejsce, w którym każdy może komunikować się, współpracować przy zadaniach i projektach, zarządzać klientami i robić wiele więcej.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Liczba zaproszonych przekracza twoje warunki licencyjne";
$MESS["BX24_INVITE_DIALOG_PHONE_ERROR"] = "Te numery telefonów są nieprawidłowe:<br/>";
$MESS["BX24_INVITE_DIALOG_PHONE_LIMIT_EXCEEDED"] = "Do zaproszenia dodano zbyt wiele numerów telefonów.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR1"] = "Użytkownik z adresem e-mail #EMAIL# już istnieje.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR2"] = "Użytkownik z adresami e-mail #EMAIL# już istnieje.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "ID użytkownika jest nieprawidłowe.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR1"] = "Użytkownik o numerze telefonu #PHONE# już istnieje.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR2"] = "Użytkownik o numerach telefonu #PHONE_LIST# już istnieje.";
$MESS["BX24_INVITE_TITLE_ADD"] = "Dodaj pracownika";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Zaproś pracowników";
$MESS["BX24_LOADING"] = "Ładowanie…";
?>