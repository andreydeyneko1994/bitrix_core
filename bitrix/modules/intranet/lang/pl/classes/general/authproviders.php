<?
$MESS["authprov_check_d"] = "Pracownicy wszystkich działów";
$MESS["authprov_check_dr"] = "Pracownicy wszystkich działów i poddziałów";
$MESS["authprov_group"] = "Dział";
$MESS["authprov_group_extranet"] = "Extranet";
$MESS["authprov_name"] = "Działy";
$MESS["authprov_name_out_group"] = "Dział";
$MESS["authprov_name_out_user1"] = "Pracownik";
$MESS["authprov_panel_group"] = "Wybierz ze struktury";
$MESS["authprov_panel_last"] = "Ostatnie";
$MESS["authprov_panel_search"] = "Szukaj";
$MESS["authprov_panel_search_text"] = "Wpisz identyfikator logowania użytkownika, imię lub nazwisko lub nazwę działu.";
?>