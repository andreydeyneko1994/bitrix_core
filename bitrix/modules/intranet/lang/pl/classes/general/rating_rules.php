<?
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Struktura Firmy";
$MESS["PP_USER_CONDITION_SUBORDINATE_T0"] = "Maksymalna liczba głosów na przełożonego";
$MESS["PP_USER_CONDITION_SUBORDINATE_T1"] = "Głosy dla podwładnych";
$MESS["PP_USER_CONDITION_SUBORDINATE_T2"] = "50% z maksimum przełożonego";
$MESS["PP_USER_CONDITION_SUBORDINATE_T3"] = "75% z maksimum przełożonego";
$MESS["PP_USER_CONDITION_SUBORDINATE_T4"] = "100% z maksimum przełożonego";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Obliczenia dodatkowych głosów dla autorytetu użytkownika na podstawie struktury firmy.";
?>