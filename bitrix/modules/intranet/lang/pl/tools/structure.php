<?
$MESS["INTR_ADD_TITLE"] = "Dodaj dział";
$MESS["INTR_EDIT_TITLE"] = "Edytuj dział";
$MESS["INTR_STRUCTURE_ADD"] = "Dodaj";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Dodaj więcej";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Dział nadrzędny";
$MESS["INTR_STRUCTURE_EDIT"] = "Zapisz";
$MESS["INTR_STRUCTURE_HEAD"] = "Przełożony";
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["INTR_STRUCTURE_NAME"] = "Nazwa działu";
$MESS["INTR_STRUCTURE_SUCCESS"] = "Dział został dodany.";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Wybierz ze struktury";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Niewystarczaj?ce uprawnienia do wykonywania zmian.";
?>