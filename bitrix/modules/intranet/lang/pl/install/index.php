<?
$MESS["INTR_INSTALL_RATING_RULE"] = "Calculate extra votes for authority based on company structure";
$MESS["INTR_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["INTR_MODULE_DESCRIPTION"] = "Portal Intranetowy";
$MESS["INTR_MODULE_NAME"] = "Intranet";
$MESS["INTR_PHP_L439"] = "Używasz wersji #VERS# PHP, a moduł wymaga wersji 5.0.0 lub nowszej. Proszę zaktualizować swoją wersję instalacyjną PHP lub skontaktować się z pomocą techniczną.";
$MESS["INTR_UNINSTALL_TITLE"] = "Intranet Module Uninstallation";
?>