<?
$MESS["INTRANET_USER_ADD_NAME"] = "Dodaj pracowników";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "Zostałeś dodany do strony intranetowej";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "#USER_TEXT#

#LINK#

Użyj swojego adresu e-mail jako nazwy użytkownika. Zostaniesz poproszony o wybranie hasła, gdy będziesz się logował pierwszy raz.";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Zaproś ludzi";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "Zaproszono Cię do Bitrix24";
?>