<?php
$MESS["COMPANY_MENU_ABSENCE"] = "Grafik Nieobecności";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Urodziny";
$MESS["COMPANY_MENU_CONFERENCES"] = "Spotkania online";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Znajdź pracownika";
$MESS["COMPANY_MENU_EMPLOYEE_LIST"] = "Pracownicy";
$MESS["COMPANY_MENU_EVENTS"] = "Zmiany personelu";
$MESS["COMPANY_MENU_GALLERY"] = "Współdzielone zdjęcia";
$MESS["COMPANY_MENU_KNOWLEDGE_BASE"] = "Baza wiedzy";
$MESS["COMPANY_MENU_LEADERS"] = "Nagrodzeni pracownicy";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Moje zapytania";
$MESS["COMPANY_MENU_REPORT"] = "Raport wydajności";
$MESS["COMPANY_MENU_STRUCTURE"] = "Struktura Firmy";
$MESS["COMPANY_MENU_TELEPHONES"] = "Książka telefoniczna";
$MESS["COMPANY_MENU_TIMEMAN"] = "Śledzenie czasu";
$MESS["COMPANY_MENU_TIMEMAN_SECTION"] = "Czas i raporty";
$MESS["COMPANY_MENU_WORKREPORT"] = "Raporty Pracy";
