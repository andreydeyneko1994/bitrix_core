<?
$MESS["SERVICES_MENU_BOARD"] = "Ogłoszenia";
$MESS["SERVICES_MENU_BP"] = "Proces Biznesowy";
$MESS["SERVICES_MENU_CONTACT_CENTER"] = "Centrum kontaktowe";
$MESS["SERVICES_MENU_EVENTLIST"] = "Dziennik Zmian";
$MESS["SERVICES_MENU_FAQ"] = "FAQ";
$MESS["SERVICES_MENU_IDEA"] = "Pomysły";
$MESS["SERVICES_MENU_LEARNING"] = "Szkolenia";
$MESS["SERVICES_MENU_LINKS"] = "Link Directory";
$MESS["SERVICES_MENU_LISTS"] = "Listy";
$MESS["SERVICES_MENU_MEETING"] = "Spotkania i Odprawy";
$MESS["SERVICES_MENU_MEETING_ROOM"] = "Rezerwacja zasobu";
$MESS["SERVICES_MENU_OPENLINES"] = "Otwarte Kanały";
$MESS["SERVICES_MENU_PROCESSES"] = "Procesy";
$MESS["SERVICES_MENU_REQUESTS"] = "e-Zamówienia";
$MESS["SERVICES_MENU_SUBSCR"] = "Subskrypcja";
$MESS["SERVICES_MENU_SUPPORT"] = "Wsparcie Techniczne";
$MESS["SERVICES_MENU_TELEPHONY"] = "Telefonia";
$MESS["SERVICES_MENU_VOTE"] = "Ankiety";
$MESS["SERVICES_MENU_WIKI"] = "Wiki";
?>