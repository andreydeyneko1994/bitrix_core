<?
$MESS["CRM_MENU_BP"] = "Proces Biznesowy";
$MESS["CRM_MENU_CONFIG"] = "Inne ustawienia";
$MESS["CRM_MENU_CURRENCY"] = "Waluty";
$MESS["CRM_MENU_FIELDS"] = "Pola dodatkowe formularzy";
$MESS["CRM_MENU_INFO"] = "Pomoc";
$MESS["CRM_MENU_LOCATIONS"] = "Lokalizacje";
$MESS["CRM_MENU_MAILTEMPLATE"] = "Szablony e-mail";
$MESS["CRM_MENU_MEASURE"] = "Jednostki miary";
$MESS["CRM_MENU_PERMS"] = "Uprawnienia dostępu";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Właściwości produktu";
$MESS["CRM_MENU_PS"] = "Sposoby płatności";
$MESS["CRM_MENU_SALE"] = "e-Sklep";
$MESS["CRM_MENU_SENDSAVE"] = "Integracja E-mail";
$MESS["CRM_MENU_SLOT"] = "Raporty analityczne";
$MESS["CRM_MENU_STATUS"] = "Listy wyboru";
$MESS["CRM_MENU_TAX"] = "Podatki";
$MESS["CRM_TITLE"] = "Ustawienia";
?>