<?php
$MESS["MENU_CRM_ACTIVITY"] = "Moje działania";
$MESS["MENU_CRM_BUTTON"] = "Widżet na stronę";
$MESS["MENU_CRM_COMPANY"] = "Firmy";
$MESS["MENU_CRM_CONTACT"] = "Kontakty";
$MESS["MENU_CRM_DEAL"] = "Deale";
$MESS["MENU_CRM_DESKTOP"] = "CRM";
$MESS["MENU_CRM_FUNNEL"] = "Lejek sprzedażowy";
$MESS["MENU_CRM_HISTORY"] = "Wydarzenia";
$MESS["MENU_CRM_INVOICE"] = "Faktury";
$MESS["MENU_CRM_LEAD"] = "Leady";
$MESS["MENU_CRM_PRODUCT"] = "Katalog";
$MESS["MENU_CRM_QUOTE"] = "Oferty";
$MESS["MENU_CRM_REPORT"] = "Raporty";
$MESS["MENU_CRM_SETTINGS"] = "Ustawienia";
$MESS["MENU_CRM_STREAM"] = "Aktualności";
$MESS["MENU_CRM_WEBFORM"] = "Formularze CRM";
