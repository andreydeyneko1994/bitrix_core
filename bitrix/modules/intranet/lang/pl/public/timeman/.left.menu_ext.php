<?php
$MESS["TOP_MENU_ABSENCE"] = "Grafik Nieobecności";
$MESS["TOP_MENU_MEETING"] = "Spotkania i Odprawy";
$MESS["TOP_MENU_MONITOR_REPORT"] = "Osobisty asystent czasu pracy";
$MESS["TOP_MENU_SETTINGS_PERMISSIONS"] = "Uprawnienia dostępu";
$MESS["TOP_MENU_TIMEMAN"] = "Czas pracy";
$MESS["TOP_MENU_WORK_REPORT"] = "Raporty Pracy";
$MESS["TOP_MENU_WORK_SCHEDULES"] = "Harmonogramy pracy";
