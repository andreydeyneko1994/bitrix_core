<?php
$MESS["ABSENCE_FORM_2"] = "Wybierz nieobecnego użytkownika";
$MESS["APP_RIGHTS"] = "Uprawnienia dostępu użytkownika";
$MESS["AUTH_OTP_HELP_TEXT"] = "Aby lepiej zabezpieczyć twoje dane w Bitrix24, twój administrator włączył dodatkowe opcje zabezpieczeń: dwustopniowe uwierzytelnienie.<br/><br/>
Dwustopniowe uwierzytelnienie zakłada dwa następujące po sobie etapy weryfikacji. Pierwszy wymaga twojego głównego hasła. Drugi etap zawiera jednorazowy kod wysyłany na twój telefon komórkowy (lub zakłada użycie klucza sprzętowego).<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img1.png\"></div>
<br/>
Po włączeniu dwustopniowego uwierzytelnienia musisz przejść dwa ekrany uwierzytelniające:<br/><br/>
- wprowadź swój e-mail i hasło;<br/>
- następnie wprowadź jednorazowy kod pozyskany z aplikacji mobilnej Bitrix OTP, która została zainstalowana w trakcie włączania dwustopniowego uwierzytelniania.<br/><br/>
Uruchom aplikację na swoim telefonie komórkowym:<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img2.png\"></div>
<br/>
Wprowadź kod, który widzisz na ekranie:<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img3.png\"></div>
<br/>
Jeżeli korzystasz z kilku Bitrix24, upewnij się że jest to właściwy kod przed wprowadzeniem go.<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img4.png\"></div>
<br/>
W przypadku nie przeprowadzenia procesu włączania i konfiguracji dwustopniowego uwierzytelnienia lub utraty telefonu komórkowego, proszę skontaktować się ze swoim administratorem, aby odzyskać dostęp do swojego Bitrix24.<br/><br/>
Jeżeli jesteś administratorem, proszę skontaktuj się z <a href=\"http://www.bitrix24.com/support/helpdesk/\">Bitrix Helpdesk</a>, aby odzyskać dostęp.";
$MESS["B24_NEW_USER_MENTION"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["B24_NEW_USER_MENTION_F"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["B24_NEW_USER_MENTION_M"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["B24_NEW_USER_TITLE"] = "Nowy użytkownik został dodany";
$MESS["B24_NEW_USER_TITLE_LIST"] = "Nowy użytkownik";
$MESS["B24_NEW_USER_TITLE_SETTINGS"] = "Nowi użytkownicy";
$MESS["BITRIX24_HELP_VIDEO_TITLE_13"] = "Struktura organizacyjna";
$MESS["BITRIX24_HELP_VIDEO_TITLE_FULL_13"] = "Struktura organizacyjna";
$MESS["BITRIX24_INVITE"] = "Zaproś użytkowników";
$MESS["BITRIX24_INVITE_ACTION"] = "Zaproś użytkowników";
$MESS["BITRIX24_SEARCH_EMPLOYEE"] = "Użytkownicy";
$MESS["BITRIX24_USER_INVITATION_DESC"] = "#EMAIL_FROM# - e-mail zapraszającego użytkownika
#EMAIL_TO# - e-mail zapraszanego użytkownika
#LINK# - link aktywacji nowego użytkownika";
$MESS["BITRIX24_USER_INVITATION_NAME"] = "Zaproś ludzi";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Drodzy Koledzy, dzisiaj oficjalnie ruszamy z Intranetowym Portalem.

Faktycznie oznacza to, że każdy użytkownik ma uprawnienia, aby przeglądać informacje publiczne i posiada hasło dostępu do swojej prywatnej strefy.

Konfigurowalny dostęp do stref portalu dostarcza nam możliwości pracy zespołowej: możemy teraz edytować dokumenty; tworzyć grupy robocze; omawiać raporty - razem!

Celem portalu jest skrócenie łańcucha komunikacji pomiędzy użytkownikami do absolutnego minimum. Portal będąc szybkim i nowoczesnym w kontekście komunikacji stanowi najlepsze rozwiązanie dla uniknięcia roboty papierkowej i przenosi obieg informacji biznesowej w sferę elektroniczną.
 
Używając intranetowego portalu użytkownicy mogą uzyskać techniczne, specjalistyczne lub prawne informacje włącznie z korporacyjnymi standardami i tożsamością.

Użytkownicy mogą umieszczać posty na forach; wysyłać zapytania do wsparcia technicznego i świadczeniodawcy usług; udostępniać i wymieniać się dokumentami; omawiać nowości związane z organizacją i uzyskiwać najnowsze informacje od kierownictwa.

Portal stanie się ważną częścią naszej organizacji!";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "Otwarcie portalu intranetowego organizacji";
$MESS["BLOG_DESTINATION_ALL"] = "Do wszystkich użytkowników";
$MESS["BLOG_GRATMEDAL_1"] = "Dodaj użytkowników";
$MESS["BM_USR_CNT"] = "Liczba użytkowników";
$MESS["BX24_EXTRANET2INTRANET_DESC"] = "<b>#FULL_NAME#</b> jest zewnętrznym użytkownikiem.<br><br>Zauważ, że <b>nie będziesz</b> w stanie później przenieść użytkownika z intranetu ponownie do extranetu.<br><br>Aby przenieść #FULL_NAME# do intranetu, proszę wybrać odpowiedni dział.";
$MESS["BX24_EXTRANET2INTRANET_SUCCESS"] = "Użytkownik został przeniesiony pomyślnie. Teraz może uzyskać dostęp do zawartości twojego intranetu zgodnie z uzyskanymi uprawnieniami.";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Gratulacje!</b><br>Powiadomienie o dołączeniu do intranetu zostało wysłane do tego użytkownika.<br><br>Przejrzyj nowych użytkowników, których dodałeś w <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">katalogu użytkowników</a>.";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Wprowadź adresy e-mail osób, które chcesz zaprosić. Oddzielaj maile kropką lub spacją.";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "użytkownik";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Gotowe!</b><br>Zaproszenia zostały wysłane na wskazane adresy.<br><br>Aby zobaczyć uzytkowników, których zaprosiłeś <a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">kliknij</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Nie możesz zaprosić więcej użytkowników, ponieważ przekroczy to liczbę określoną w licencji.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Dołącz do nas na naszym koncie Bitrix24. To miejsce, w którym każdy może komunikować się, współpracować przy zadaniach i projektach, zarządzać klientami i robić wiele więcej.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Liczba zaproszonych przekracza twoje warunki licencji.";
$MESS["BX24_INVITE_TITLE_ADD"] = "Dodaj użytkownika";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Zaproś użytkowników";
$MESS["CAL_COMPANY_SECT_DESC_0"] = "Pokazy i konferencje, w które jesteśmy zaangażowani";
$MESS["CAL_COMPANY_SECT_DESC_1"] = "Wydarzenia mające miejsce w oddziale londyńskim";
$MESS["CAL_COMPANY_SECT_DESC_2"] = "Wydarzenia mające miejsce w oddziale paryskim";
$MESS["CAL_COMP_EVENT_DESC_3"] = "Szkolenie dla nowych użytkowników";
$MESS["CAL_TYPE_COMPANY_NAME"] = "Kalendarze organizacji";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Znajdź użytkownika";
$MESS["COMPANY_MENU_STRUCTURE"] = "Struktura organizacyjna";
$MESS["COMPANY_TITLE"] = "Znajdź użytkownika";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Opublikuj powiadomienie o nowym użytkowniku na Aktualności";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo organizacji";
$MESS["CONFIG_COMPANY_NAME"] = "Nazwa organizacji";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nazwa organizacji do wyświetlania w nagłówku";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Opublikuj powiadomienie o nowym użytkowniku na czacie publicznym";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Opublikuj powiadomienie o odrzuceniu użytkownika na czacie publicznym";
$MESS["CONFIG_LOGO_24"] = "Dodaj \"24\" do loga organizacji";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Przed przeniesieniem swoich użytkowników na system dwustopniowego uwierzytelniania, ustaw go najpierw dla swojego konta.<br/><br/>Proszę wykonać to poprzez włączenie opcji na stronie Zabezpieczenia na swoim profilu.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Dzisiaj użyj swojego loginu i hasła, aby zalogować się do swojego Bitrix24. Dane firmowe i osobiste są chronione przez technologię kodowania danych. Jednakże, istnieją narzędzia, które złośliwe osoby mogą wykorzystać do dostania się do twojego komputera i ukraść twoje dane logowania.

Z myślą o ochronie przed możliwymi atakami włączyliśmy dodatkową opcję ochrony dla Bitrix 24: dwustopniowe uwierzytelnienie.

Dwustopniowe uwierzytelnienie jest specjalną metodą ochrony przed hakowaniem oprogramowania, a zwłaszcza kradzieżą hasła. Przy każdym logowaniu do systemu musisz przejść dwa poziomy weryfikacji. Po pierwsze wprowadzasz swój e-mail i hasło, a następnie wprowadzasz jednorazowy kod bezpieczeństwa pozyskany z twojego telefonu komórkowego.

Daje to bezpieczeństwo naszym danym handlowym nawet w przypadku wykradzenia loginu i hasła od któregokolwiek użytkownika.

Masz 5 dni na włączenie tej funkcji.

Aby skonfigurować nową procedurę uwierzytelniania, proszę przejść do swojego profilu i wybrać stronę \"Ustawienia zabezpieczeń\".

Jeżeli nie posiadasz odpowiedniego urządzenia mobilnego, które by mogło obsługiwać aplikację lub masz jakikolwiek inny problem, proszę nasz powiadomić poprzez pozostawienie komentarza do tego posta.";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "To jest tekst jaki możesz umieścić na Aktualności<br/>dla swoich użytkowników do przeczytania.<br/><br/>
Powiadom swoich współpracowników o dwustopniowym uwierzytelnieniu,<br/>procedurze konfiguracji i nowej metodzie uwierzytelniania<br/>będą musieli wykorzystać logowanie do swojego Bitrix24.";
$MESS["CONFIG_OTP_SECURITY"] = "Włącz dwustopniowe uwierzytelnianie dla wszystkich użytkowników";
$MESS["CONFIG_OTP_SECURITY2"] = "Uczyń dwustopniowe uwierzytelnianie obowiązkowym dla wszystkich użytkowników";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Wyznacz okres czasu, w trakcie którego wszyscy użytkownicy<br/>będą musieli włączyć dwustopniowe uwierzytelnianie";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Stworzyliśmy przyjazną dla użytkownika procedurę włączania dwustopniowego uwierzytwlniania, którą może wykonać każdy użytkownik bez kożystania z pomocy eksperta.<br/><br/>
Wiadomość zostanie wysłana do każdego użytkownika powiadamiając ich o konieczności włączenia dwustopniowego uwierzytelniania w określonym przedziale czasowym. Użytkownicy, którzy tego nie zrobią nie będą w stanie się logować.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Aby włączyć dwustopniowe uwierzytelnienie, użytkownik musi zainstalować aplikację Bitrix24 OTP na swoim telefonie komórkowym. Tą aplikację można pobrać z AppStore lub GooglePlay.<br/><br/>
Użytkownicy, którzy nie posiadają odpowiednich telefonów muszą zaopatrzyć się w specjalne urządzenia - eToken Pass. Jest wielu dostawców, u których można je nabyć na przykład:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Find other vendors on <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Alternatywnie możesz wyłączyć dwustopniowe uwierzystelnianie dla niektórych użytkowników. Jednakże, zwiększa to ryzyko nieautoryzowanego dostępu do twojego Bitrix24, jeżeli login i hasło któregoś z użytkowników zostaną skradzione.Jako administrator możesz wyłączyć dwustopniowe uwierzytelnianie dla użytkowników w profilu użytkownika.";
$MESS["CONFIG_USERS2"] = "Użytkownicy:";
$MESS["CRM_DEMO_EVENT_12_EVENT_NAME"] = "Zmieniono pole \"Użytkownicy\"";
$MESS["CRM_DEMO_EVENT_15_EVENT_NAME"] = "Zmieniono pole \"Użytkownicy\"";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nowe firmy";
$MESS["CT_BST_SEARCH_HINT"] = "znajdź ludzi, dokumenty i więcej";
$MESS["DEMO_FEATURE_1"] = "Nielimitowani użytkownicy";
$MESS["DEMO_INFO_1"] = "Użytkownicy";
$MESS["DEMO_INFO_DESC_1"] = "Jeżeli zaprosiłeś więcej niż 12 użytkowników w trakcie trwania okresu próbnego, tylko pierwszych 12 będzie miało dostęp do twojego konta Bitrix24. Jeżeli chcesz przywrócić dostęp dla wszystkich użytkowników musisz <a class=\"link\" href=\"/settings/license_all.php\">upgradować</a>.";
$MESS["DIRECTION_GROUP_DESC"] = "Zarządzaj.";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "Ze struktury organizacyjnej";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "Dodaj uczestników wydarzenia ze struktury organizacyjnej";
$MESS["EC_COMPANY_CALENDAR"] = "Kalendarze organizacji";
$MESS["EC_COMPANY_STRUCTURE"] = "Struktura organizacyjna";
$MESS["EC_DESTINATION_1"] = "Dodaj użytkowników, grupy lub działy";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "Brak struktury organizacyjnej.";
$MESS["EMPLOYEES_GROUP_DESC"] = "Wszyscy użytkownicy zarejestrowni w portalu.";
$MESS["FIELD_empl_num"] = "Liczba użytkowników";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Publiczne forum dla użytkowników z organizacji. Omawiaj tutaj sprawy biznesowe i wymieniaj opinie.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Uwaga! Użytkownicy mogą teraz zarządzać przechowywaniem swoich plików w swojej strefie prywatnej.

Szczegółowe informacje o zarządzaniu przechowywaniem plików i metodach mapowania schowka na dysk sieciowy znajdziesz w sekcji pomocy: \"Mój profil - Pliki\".

W razie pytań odnośnie konfiguracji schowka, proszę wysłać zapytanie do wsparcia technicznego używając formularza.\"";
$MESS["HONOR_FORM_2"] = "Zaznacz użytkownika do uhonorowania";
$MESS["IFS_EF_Blog"] = "Kanały blogów grupy i użytkownika";
$MESS["IFS_EF_Gallery"] = "Galerie zdjęć użytkowników";
$MESS["IFS_EF_staff"] = "Personel; dokumenty referencyjne; struktura organizacyjna; uhonorowani użytkownicy";
$MESS["IM_CL_STRUCTURE"] = "Społeczność";
$MESS["IM_CL_USER_B24"] = "Członek";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "Struktura społeczności";
$MESS["IM_C_ABOUT_CHAT"] = "Prywatny czat jest dostępny tylko dla zaproszonych użytkowników. #BR##BR# Możesz zapraszać nie tylko współpracowników ale także klientów, partnerów oraz inne osoby związane z twoim biznesem, które używają Bitrix24. Aby dodać nowego członka czatu wpisz jego imię i nazwisko, adres e-mail lub login. #BR##BR#Uzupełnij #PROFILE_START#swój profil#PROFILE_END#, aby inni mogli cię znaleźć.";
$MESS["IM_C_ABOUT_OPEN"] = "Czat publiczny jest dostępny dla wszystkich. #BR##BR# Używaj tego czatu aby dyskutować na tematy, które są ważne dla wszystkich w twojej firmie.#BR##BR# Gdy czat publiczny zostanie stworzony, zostaje wysłane powiadomienie do #CHAT_START#Czat wspólny#CHAT_END#. Twoi współpracownicy będą widzieć wiadomości i będą mogli dołączyć do czatu jeśli będą chcieli.";
$MESS["IM_C_ABOUT_PRIVATE"] = "Czat w cztery oczy jest widoczny ciebie i drugiego kontaktu. #BR##BR# Znajdź osobę, z którą chcesz rozmawiać, po imieniu i nazwisku, stanowisku lub dziale. #BR##BR# Możesz zapraszać nie tylko współpracowników ale także klientów, partnerów oraz inne osoby związane z twoim biznesem, które używają Bitrix24. #BR##BR# Uzupełnij #PROFILE_START#swój profil#PROFILE_END#, aby inni mogli cię znaleźć.";
$MESS["IM_M_CALL_ST_TRANSFER"] = "Przekierowane połączenie: oczekiwanie na odpowiedź";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "Przekierowane połączenie: użytkownik nie odpowiada";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Zauważyliśmy, że podjąłeś próbę utworzenia domeny dla swojej organizacji, ale nie zakończyłeś rejestracji.</p>

<p>Jeżeli potrzebujesz pomocy, otwórz stronę ustawień e-maila, przeczytaj szczegółowe instrukcje jak ustwić swoją usługę e-mail i zakończ swoją rejestrację domeny Bitrix24.</p>

<p>Jeżeli chcesz zobaczyć jak zazwyczaj to przebiega, mamy kilka przykładów użycia domen e-mail. Przykłady wyjaśnią jak zarejestrować domenę, utworzyć skrzynkę e-mail i wykorzystywania ich z Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Dowiedz się więcej</a></p>

<p>Jeżeli nadal masz pytania, proszę skontaktuj się z naszym <a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Zarejestrowałeś domenę i utworzyłeś swoją skrzynkę mailową. Jednakże twoi pracownicy lub użytkownicy wciąż nie mają pojęcia w jaki sposób mogą utworzyć swoje skrzynki mailowe w twojej domenie.</p>

<p>Podziel się swoją wiedzą i pomóż im nauczyć się jak sami mogą utworzyć skrzynkę mailową lub zarejestrować skrzynki na swojej stronie Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Jak zarejestrować skrzynki mailowe dla swoich pracowników</a></p>

<p>Jeżeli nadal masz pytania, proszę skontaktuj się z naszym<a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Utwórz skrzynki mailowe użytkowników w domenie";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "Firmowy e-mail dla twoich użytkowników";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Chciałeś zarejestrować domenę dla swojej firmy, ale nie udało ci się wybrać dobrej nazwy.</p>

<p>Mamy kilka przykładów domen z wykorzystaniem e-maila dla ciebie do obejrzenia. Na przykładach, nauczysz się jak utworzyć skrzynkę mailową i wykorzystywać ją z Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Jak zarejestrować skrzynki mailowe dla swoich pracowników</a></p>

<p>Jeżeli nadal masz pytania, proszę skontaktuj się z naszym <a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>\"";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Dodatkowa ochrona dla twoich danych biznesowych";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "Wartość bazująca na danych, obliczonych ze \"Struktury organizacyjnej\".";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Dodatkowe głosy bazujące na strukturze organizacyjnej";
$MESS["INTRANET_TAB_USER_STRUCTURE"] = "Organizacja";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - adres e-mail nowego użytkownika
#LINK# - URL intranetu";
$MESS["INTRANET_USER_ADD_NAME"] = "Dodaj użytkowników";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - adres e-mail osoby zaproszonej
#LINK# - link aktywujący dla nowego użytkownika";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Zaproś ludzi";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Puls Organizacji";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Indeks aktywności: składa się z aktywności wszystkich użytkowników funkcji intranetu dla danego okresu czasu. Indeks pokazuje jak wygląda aktywność użytkowników względem wykorzystania poszczególnych narzędzi.";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "Puls Organizacji jest ogólnym wskaźnikiem aktywności użytkowników na portalu w chwili obecnej (złożony ze wszystkich użytkowników z przeciągu ostatniej godziny).";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Poziom zaangażowania: jest to kluczowy wskaźnik pokazujący jak użytkownicy są zaznajomieni z możliwościami Bitrix24. Pokazuje jaki procent członków organizacji używa przynajmniej jednej czwartej dostarczonych narzędzi.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "Ocena jest zależna od średniej indeksu aktywności indywidualnej wszystkich użytkowników, którzy wykonywali przynajmniej 1 działanie w danym przedziale czasowym.";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Użycie funkcji: CRM";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Użycie funkcji: Dysk";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Użycie funkcji: Czat";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Użycie funkcji: Lajki";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Użycie funkcji: Aplikacja na telefon";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Użycie funkcji: Sieć społecznościowa";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Użycie funkcji: Zadania";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy aktywnie wykorzystywali CRM przez dany okres czasu. Liczba ta jest/może być ograniczona do osób mających dostęp do CRM.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Procent użytkowników implementujących Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Pokazuje procent użytkowników, którzy korzystali z komunikatora lub wykonali połączenie wideo/audio w danym okresie czasu.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy używali funkji \"Like\" w danym okresie czasu.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy wykorzystują aplikację mobilną w danym okresie czasu.";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Pokazuje procentowe wykorzystanie przez pracowników funkcji mediów społecznościowych w określonym przedziale czasu.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy używali zadań w danym okresie czasu.";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Jedność w sile pracy";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Organizacja";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Średnia<br>organizacji";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Średnia organizacji";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Średnia wartość działań przeprowadzonych przez twoją organizację w Bitrix24 z wykorzystaniem jednego z siedmiu dostępnych narzędzi w danym okresie czasu.<br><br> <b>Wykorzystaj średnią wartość organizacji, aby zobaczyć jak plasuje się twoja działalność na tle innych.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Średnia wartość działań przeprowadzonych przez twój dział w Bitrix24 z wykorzystaniem jednego z siedmiu dostępnych narzędzi w danym okresie czasu.<br><br> <b>Wykorzystaj średnią wartość działu, aby zobaczyć jak plasuje się twoja działalność na tle innych.";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Twoja pozycja w <b>ocenie aktywności</b> na liście podsumowującej wszystkich użytkowników wykorzytujących Bitrix24 przynajmniej raz w danym okresie czasu.";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Bieżący poziom aktywności organizacji (bierze pod uwagę wszystkich użytkowników w ciągu ostatniej godziny)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Bieżące zaangażowanie użytkowników. Pokazuje procentowo wszystkich użytkowników, którzy dzisiaj użyli przynajmniej czterech różnych narzędzi w intranecie.";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Puls Organizacji";
$MESS["INTR_ABSC_TPL_EDIT_ENTRIES"] = "Zarządzanie użytkownikiem";
$MESS["INTR_ABSC_TPL_IMPORT"] = "Importuj użytkownika";
$MESS["INTR_ABSENCE_USER"] = "Wybierz nieobecną osobę *";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Możesz eksportować użytkowników jako kontakty dla Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Synchronizuj rejestry użytkowników z oprogramowaniem i sprzętem obsługującym CardDAV (iPhone, iPad itp.)";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Eksportuj listę użytkowników jako kontakty Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ACTIVE"] = "Użytkownicy";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ADD"] = "Zaproś użytkownika";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_INVITE"] = "Zaproś użytkownika";
$MESS["INTR_CONFIRM_DELETE"] = "Zaproszenie zostanie usunięte nieodwracalnie.\\Na pewno chcesz usunąć użytkownika?";
$MESS["INTR_CONFIRM_FIRE"] = "Użytkownik nie będzie miał możliwości logowania się do portalu i nie będzie widoczny w strukturze firmy. Jednakże jego lub jej wszystkie dane osobiste (pliki, wiadomości, zadania itp.) pozostaną nietknięte.\\n\\nNa pewno chcesz zabronić dostępu temu użytkownikowi?";
$MESS["INTR_CONFIRM_RESTORE"] = "Użytkownik będzie mógł logować się do portalu i będzie widoczny w strukturze firmy.\\n\\nNa pewno chcesz zezwolić na dostęp temu użytkownikowi?";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Anuluj zaznaczenie użytkownika";
$MESS["INTR_EMP_SEARCH"] = "wyszukaj użytkownika";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Wybierz zaznaczonego użytkownika";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Zaznacz użytkownika";
$MESS["INTR_IBLOCK_TOP_SECTION_WARNING"] = "Struktura organizacyjna może zawierać jedynie najwyższy poziom sekcji.";
$MESS["INTR_INSTALL_RATING_RULE"] = "Oblicz dodatkowe głosy dla użytkownika bazując na strukturze organizacyjnej.";
$MESS["INTR_ISBN_TPL_FILTER_ALL"] = "Dla całej organizacji";
$MESS["INTR_ISE_TPL_NOTE_NULL"] = "Nie ma takiego użytkownika.";
$MESS["INTR_ISIN_TPL_ALL"] = "Dla całej organizacji";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "Nie ma takiego użytkownika.";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "Zaproś użytkowników";
$MESS["INTR_IS_TPL_OUTLOOK"] = "Eksportuj użytkowników do Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "Eksportuj listę użytkowników jako kontakty Outlook";
$MESS["INTR_IS_TPL_SEARCH"] = "Znajdź użytkownika";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "Znajdź użytkownika w tym dziale";
$MESS["INTR_MAIL_DESCR_B24"] = "Darmowy serwer e-mail dla twojego biznesu! Nielimitowane miejsce dla twoich e-maili, antywirus i antyspam, aby twoja skrzynka była niezaśmiecona. Przenieś swój firmowy e-mail do Bitrix24! Twórz własne skrzynki mailowe na własnej firmowej domenie lub dołącz istniejącą już usługę e-mailową.";
$MESS["INTR_MAIL_DESCR_BOX"] = "Przygotuj się do używania serwera e-mail dla swojego biznesu. Ciesz się nielimitowanym miejscem, antywirusem i antyspamem. Twórz skrzynki mailowe na swojej firmowej domenie lub dołącz istniejącą już usługę e-mail.";
$MESS["INTR_MAIL_DOMAIN_PUBLIC"] = "zezwalaj użytkownikom na rejestrowanie skrzynek mailowych na nowej domenie";
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "Użytkownicy mogą rejestrować skrzynki mailowe na tej domenie";
$MESS["INTR_MAIL_MANAGE"] = "Konfiguruj skrzynki mailowe użytkowników";
$MESS["INTR_MAIL_MANAGE_HINT"] = "Utwórz skrzynkę mailową dla każdego użtrkownika w swojej organizacji. Uzywaj interfejsu do zarządzania firmowymi skrzynkami: twórz, dołączaj lub usuwaj skrzynki mailowe, tak samo jak zmieniasz hasło w skrzynce mailowej.";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "Członkowie mogą podłączać skrzynki pocztowe do CRM CRM";
$MESS["INTR_MAIL_NODOMAIN_USER_INFO"] = "Proszę skontaktować się ze swoim administratorem Intranet w celu dołączenia firmowej domeny,";
$MESS["INTR_OPTION_IBLOCK_CALENDAR"] = "Blok informacji dla Kalendarzy użytkowników";
$MESS["INTR_OPTION_IBLOCK_STRUCTURE"] = "Blok informacji struktury organizacyjnej";
$MESS["INTR_PROP_EMP_TITLE"] = "Link do użytkownika";
$MESS["ISL_TPL_NOTE_NULL"] = "Nie ma takiego użytkownika.";
$MESS["ISL_WORK_CITY"] = "Miasto";
$MESS["ISL_WORK_COUNTRY"] = "Kraj";
$MESS["ISL_WORK_LOGO"] = "Logo organizacji";
$MESS["ISV_B24_INVITE"] = "Zaproś użytkowników";
$MESS["ISV_EMPLOYEES"] = "UŻYTKOWNICY";
$MESS["ISV_EMP_COUNT_1"] = "#NUM# użytkownik";
$MESS["ISV_EMP_COUNT_2"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_3"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_4"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_21"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_22"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_23"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_24"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_31"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_32"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_33"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_34"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_41"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_42"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_43"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_44"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_COUNT_MUL"] = "#NUM# użytkowników ";
$MESS["ISV_EMP_LIST"] = "Lista użytkowników";
$MESS["ISV_ERROR_dpt_not_empty"] = "Musisz przenieść wszystkich użytkowników z działu przed usunięciem go.";
$MESS["ISV_add_emp"] = "Dodaj użytkownika";
$MESS["ISV_change_department"] = "<b>#NAME#</b> został/a przeniesiony/a do <b>#DEPARTMENT#</b>.";
$MESS["ISV_confirm_change_department_0"] = "Przenieść #EMP_NAME# do #DPT_NAME#?";
$MESS["ISV_confirm_change_department_1"] = "Przydzielić #EMP_NAME# do #DPT_NAME#?";
$MESS["ISV_confirm_delete_department"] = "Na pewno chcesz usunąć dział?Spowoduje to przeniesienie wszystkich podległych poddziałów i użytkowników.";
$MESS["ISV_confirm_set_head"] = "Przydzielić #EMP_NAME# jako kierownika #DPT_NAME# działu?";
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> został/a wyznaczony/a na kierownika działu <b>#DEPARTMENT#</b>.";
$MESS["I_NEW_USER_MENTION"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["I_NEW_USER_MENTION_M"] = "wspomniał/a ciebie w komentarzu w wiadomości o nowododanym użytkowniku #title#.";
$MESS["I_NEW_USER_TITLE"] = "Nowy użytkownik został dodany";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nowy użytkownik";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nowi użytkownicy";
$MESS["LICENSE_CRM_FEATURE_16"] = "Dziennik dostępu CRM";
$MESS["LICENSE_MORE_USERS"] = "Więcej użytkowników";
$MESS["LICENSE_TEL_FEATURE_11"] = "Rozszerzenia użytkownika";
$MESS["LICENSE_TEL_FEATURE_16"] = "Jednoczesne połączenie do wszystkich dostępnych użytkowników";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Użytkownicy wszystkich działów i poddziałów";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Użytkownicy i działy ";
$MESS["MEETING_DESCRIPTION"] = "Musimy się spotkać, aby omówić wdrożenie portalu intranetowego w naszej organizacji.";
$MESS["MENU_COMPANY"] = "ORGANIZACJA";
$MESS["MENU_COMPANY_CALENDAR"] = "Kalendarze organizacji";
$MESS["MENU_COMPANY_SECTION"] = "Społeczność";
$MESS["MENU_EMPLOYEE"] = "Użytkownicy";
$MESS["MENU_STRUCTURE"] = "Struktura organizacyjna";
$MESS["ML_COL_DESC_0"] = "Kolekcja zdjęć użytkownika";
$MESS["ML_COL_NAME_0"] = "Zdjęcia użytkownika";
$MESS["MPF_DESTINATION_1"] = "Dodaj użytkowników, grupy lub działy";
$MESS["MPF_DESTINATION_3"] = "Wszystkich użytkowników";
$MESS["MP_ADD_APP_SCOPE_DEPARTMENT"] = "Struktura organizacyjna";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Struktura organizacyjna";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Oblicz dodatkowe głosy dla użytkownika bazując na strukturze organizacyjnej.";
$MESS["SERVICES_MENU_NOVICE"] = "Dla nowych użytkowników";
$MESS["SERVICES_ORG_LIST"] = "Moja organizacja";
$MESS["SERVICES_TITLE"] = "Informacje dla nowych użytkowników";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "Struktura organizacyjna";
$MESS["SOCNET_CONFIRM_DELETE"] = "Zaproszenie zostanie usunięte nieodwracalnie.

Na pewno chcesz usunąć użytkownika?";
$MESS["SOCNET_CONFIRM_FIRE"] = "Użytkownik nie będzie mógł logowac się do portalu i nie będzie widoczny w strukturze firmy. Jednakże jego lub jej dane osobiste (pliki, wiadomości, zadania itp.) pozostaną nietknięte.

Na pewno chcesz zamknąć dostęp dla tego użytkownika?";
$MESS["SOCNET_CONFIRM_FIRE1"] = "Użytkownik nie będzie mógł logowac się do portalu i nie będzie widoczny w strukturze firmy. Jednakże jego lub jej dane osobiste (pliki, wiadomości, zadania itp.) pozostaną nietknięte.\\n\\nNa pewno chcesz odmówić dostępu temu użytkownikowi?";
$MESS["SOCNET_CONFIRM_RECOVER"] = "Użytkownik będzie mógł logować się do portalu i będzie widoczny w strukturze organizacyjnej.

Na pewno chcesz otworzyć dostęp dla tego użytkownika?";
$MESS["SOCNET_CONFIRM_RECOVER1"] = "Użytkownik będzie mógł logować się do portalu i będzie widoczny w strukturze firmy.\\n\\nNa pewno chcesz zezwolić na dostęp temu użytkownikowi?";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Dodaj użytkownika";
$MESS["SONET_GCE_T_DEST_TITLE_EMPLOYEE"] = "Zaproś użytkowników:";
$MESS["SONET_GCE_T_INVITATION_EMPLOYEES"] = "Zaproś użytkowników";
$MESS["SONET_GCE_T_USER_INTRANET"] = "Użytkownicy z organizacji:";
$MESS["SONET_GL_DESTINATION_G2"] = "Do wszystkich użytkowników";
$MESS["SONET_GROUP_DESCRIPTION_3"] = "Aktywność sportowa";
$MESS["SONET_LIVEFEED_RENDERPARTS_USER_ALL"] = "Do wszystkich użytkowników";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Zaproś nowych użytkowników, aby dołączyli do portalu intranetowego";
$MESS["SONET_TASK_TITLE_2"] = "Zaproś nowych użytkowników";
$MESS["SONET_UP1_WORK_POSITION"] = "Pozycja w społeczności";
$MESS["STATE_FORM_2"] = "Wybierz użytkownika, którego status zostal zmieniony";
$MESS["SUBSCRIBE_POSTING_BODY"] = "Cześć! Dodaliśmy nowe zdjęcia z wakacji.";
$MESS["SUBSCRIBE_POSTING_SUBJECT"] = "Nasze zdjęcia!";
$MESS["TEMPLATE_DESCRIPTION"] = "Ten szablon został zaprojektowany dla zaakcentowania społecznościowych aspektów Intranetu i łączy w sobie konwencjonalne narzędzia pracy z możliwością społecznościowej komunikacji pomiędzy placówkami. Układ Intranetu społecznościowego jest wysoce intuicyjnym stymulatorem produktywności i wymaga minimum czasu do zaadaptowania i zorientowania się.";
$MESS["TITLE1"] = "Struktura organizacyjna";
$MESS["TITLE2"] = "Struktura organizacyjna";
$MESS["UF_PUBLIC"] = "Widoczne dla wszystkich w Extranecie";
$MESS["WD_USER"] = "Dokumenty użytkowników";
$MESS["W_IB_ABSENCE_2_PREV"] = "Delegacja do filii firmy.";
$MESS["W_IB_CLIENTS_TAB1"] = "edit1--#--Klient--,--NAME--#--*Nazwa Firmy--,--PROPERTY_PERSON--#--Kierownik--,--PROPERTY_PHONE--#--Telefon--;--";
$MESS["authprov_check_d"] = "Użytkownicy wszystkich działów";
$MESS["authprov_check_dr"] = "Użytkownicy wszystkich działów i poddziałów";
$MESS["authprov_name"] = "Użytkownicy i działy";
$MESS["iblock_dep_name1"] = "Moja organizacja";
$MESS["main_opt_user_comp_logo"] = "Logo organizacji";
$MESS["main_opt_user_comp_name"] = "Nazwa organizacji";
$MESS["main_site_name"] = "Moja organizacja";
$MESS["wiz_company_logo"] = "Logo organizacji:";
$MESS["wiz_company_name"] = "Nazwa organizacji:";
$MESS["wiz_demo_structure"] = "Zainstaluj przykładową strukturę organizacyjną";
$MESS["wiz_slogan"] = "Moja organizacja";
