<?php
$MESS["WS_OUTLOOK_APP_COMMENT"] = "Wygenerowano automatycznie";
$MESS["WS_OUTLOOK_APP_DESC"] = "Uzyskaj hasło dla Microsoft Outlook, aby ustawić synchronizację swoich kalendarzy, kontaktów i zadań.";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "Połącz";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT"] = "Synchronizuj z programem Microsoft Outlook";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT_TYPE"] = "Synchronizuj z programem Microsoft Outlook: #TYPE#";
$MESS["WS_OUTLOOK_APP_TITLE"] = "Usługi Microsoft Outlook";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "Kontakty, kalendarze, zadania";
$MESS["WS_OUTLOOK_APP_TYPE_calendar"] = "kalendarzowe";
$MESS["WS_OUTLOOK_APP_TYPE_contacts"] = "kontakty";
$MESS["WS_OUTLOOK_APP_TYPE_tasks"] = "zadania";
