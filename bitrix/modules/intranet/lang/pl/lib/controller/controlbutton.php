<?php
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_FIRST_MESSAGE"] = "Czat utworzony na podstawie wydarzenia \"#EVENT_TITLE#\" z #DATETIME_FROM#";
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_TITLE"] = "Czat \"#EVENT_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_CREATE_CHAT_LOCK_ERROR"] = "Błąd podczas tworzenia czatu. Spróbuj ponownie.";
$MESS["INTRANET_CONTROL_BUTTON_DELETE_TASK_FILE_ERROR"] = "Błąd podczas usuwania pliku.";
$MESS["INTRANET_CONTROL_BUTTON_ENTITY_ERROR"] = "Nie znaleziono pozycji.";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_CALENDAR"] = "Utworzono na podstawie [url=#LINK#]wydarzenia z kalendarza[/url]";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_TASK"] = "Utworzono na podstawie [url=#LINK#]zadania[/url]";
$MESS["INTRANET_CONTROL_BUTTON_TASK_CHAT_FIRST_MESSAGE"] = "Czat utworzony na podstawie zadania \"#TASK_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_LIMIT"] = "Przekroczono maksymalną liczbę uczestników wideorozmowy";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_SELF_ERROR"] = "Rozmowa wideo wymaga co najmniej dwóch uczestników. Dodaj więcej użytkowników i zadzwoń ponownie.";
$MESS["INTRANET_CONTROL_MEETING_CREATED"] = "Wydarzenie utworzone na podstawie zadania";
