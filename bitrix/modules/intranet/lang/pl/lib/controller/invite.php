<?php
$MESS["INTRANET_CONTROLLER_INVITE_DELETE_FAILED"] = "Błąd podczas usuwania zaproszenia";
$MESS["INTRANET_CONTROLLER_INVITE_FAILED"] = "Błąd podczas tworzenia zaproszenia";
$MESS["INTRANET_CONTROLLER_INVITE_NO_PERMISSIONS"] = "Nie masz uprawnień do zapraszania użytkowników.";
$MESS["INTRANET_CONTROLLER_INVITE_NO_USER_ID"] = "Brak parametru userId";
$MESS["INTRANET_CONTROLLER_INVITE_USER_NOT_FOUND"] = "Nie znaleziono użytkownika z określonym ID.";
