<?php
$MESS["INTRANET_INVITATION_EMAIL_ERROR"] = "Adresy e-mail są nieprawidłowe:";
$MESS["INTRANET_INVITATION_EMAIL_LIMIT_EXCEEDED"] = "Przekroczono maksymalną liczbę adresów e-mail w zaproszeniu.";
$MESS["INTRANET_INVITATION_ERROR_USER_TRANSFER"] = "Nie można przekonwertować użytkownika poczty e-mail.";
$MESS["INTRANET_INVITATION_INVITE_MESSAGE_TEXT"] = "Dołącz do naszego korporacyjnego Bitrix24! To tutaj współpracujemy, zarządzamy projektami, zadaniami i dokumentami, planujemy wydarzenia i spotkania, komunikujemy się i angażujemy w inne aktywności.";
$MESS["INTRANET_INVITATION_MAX_COUNT_ERROR"] = "Liczba zaproszonych osób przekracza warunki Twojej licencji.";
$MESS["INTRANET_INVITATION_PHONE_ERROR"] = "Numery telefonów są nieprawidłowe:";
$MESS["INTRANET_INVITATION_PHONE_LIMIT_EXCEEDED"] = "Do zaproszenia dodano zbyt wiele numerów telefonów.";
$MESS["INTRANET_INVITATION_USER_EXIST_ERROR"] = "Użytkownicy o następujących adresach e-mail już istnieją: #EMAIL_LIST#";
$MESS["INTRANET_INVITATION_USER_PHONE_EXIST_ERROR"] = "Użytkownicy o następujących numerach telefonów już istnieją: #PHONE_LIST#";
