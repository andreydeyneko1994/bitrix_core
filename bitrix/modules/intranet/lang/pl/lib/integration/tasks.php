<?php
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "Zainstaluj aplikację mobilną Bitrix24 i pozostań połączony!

Zainstaluj aplikację desktopową na swoim komputerze (Mac, Windows lub Linux) aby kontaktować się ze swoimi współpracownikami kiedy twoja przeglądarka jest zamknięta.

[URL=https://www.bitrix24.com/features/mobile.php]Download Bitrix24 applications[/URL]";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "Pobierz aplikację Bitrix24";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "Zaproś znajomych do Bitrix24.

#ANCHOR_INVITE#Zaproś#ANCHOR_END#
";
$MESS["SONET_INVITE_TASK_DESCRIPTION_V2"] = "Zaproś znajomych do Bitrix24.

[URL=/company/vis_structure.php]Invite[/URL]
";
$MESS["SONET_INVITE_TASK_TITLE"] = "Zaproś znajomych";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "Uzupełnij swój profil, wgraj zdjęcie profilowe i uzupełnij dane o sobie. 

#ANCHOR_EDIT_PROFILE#Przejdź do swojego profilu#ANCHOR_END#
";
$MESS["SONET_TASK_TITLE"] = "Wypełnij profil";
