<?php
$MESS["MAIN_UI_SELECTOR_DEPARTMENT_FLAT_PATTERN"] = "#NAME# (bez poddziałów)";
$MESS["MAIN_UI_SELECTOR_EXTRANET"] = "Ekstranet";
$MESS["MAIN_UI_SELECTOR_SELECT_FLAT_TEXT_DEPARTMENTS"] = "Pracownicy wszystkich działów";
$MESS["MAIN_UI_SELECTOR_SELECT_TEXT_DEPARTMENTS"] = "Pracownicy wszystkich działów i poddziałów";
$MESS["MAIN_UI_SELECTOR_TAB_DEPARTMENTS"] = "Pracownicy i działy";
$MESS["MAIN_UI_SELECTOR_TITLE_DEPARTMENTS"] = "Działy:";
