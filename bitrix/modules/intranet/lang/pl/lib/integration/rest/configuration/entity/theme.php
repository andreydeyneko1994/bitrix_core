<?php
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_NOT_FOUND"] = "Wybrany motyw nie został znaleziony w Twoim Bitrix24.";
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_SET_DEFAULT"] = "Nie można ustawić zaimportowanego motywu jako motywu domyślnego.";
