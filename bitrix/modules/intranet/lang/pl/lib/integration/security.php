<?php
$MESS["intranet_otp_push_code"] = "Twój kod do logowania: #KOD#";
$MESS["intranet_push_otp_notification"] = "Próbowałeś zalogować się na konto Bitrix24. [br][br] Kod potwierdzający: [b] #CODE# [/ b]. [br][br] Nie pokazuj nikomu tego kodu ani wprowadzaj go na innych stronach internetowych. Jeśli nie prosiłeś o kod, pilnie zmień hasło w ustawieniach konta.";
$MESS["intranet_push_otp_notification1"] = "Wykryliśmy próbę zalogowania się do Twojego Bitrix24.[br][br]Kod potwierdzenia logowania: [b]#CODE#[/b]. Nigdy nie przekazuj tego kodu osobom trzecim ani nie wprowadzaj go na innych stronach internetowych.[br][br]Jeśli to Ty zażądałeś kodu, nie masz się czym martwić. W przeciwnym razie natychmiast zmień swoje hasło w Ustawieniach.[br][br]Adres IP: #IP#[br]Urządzenie: #USER_AGENT#";
