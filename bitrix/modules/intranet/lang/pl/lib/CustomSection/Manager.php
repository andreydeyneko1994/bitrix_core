<?php
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_COMPONENT_NOT_FOUND"] = "Nie można wyświetlić bieżącej strony sekcji";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_INVALID_URL"] = "Nieprawidłowy format linku";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_AVAILABLE"] = "Sekcja nie zawiera stron do wyświetlenia";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_FOUND"] = "Nie znaleziono sekcji";
