<?
$MESS["INTR_USER_ABSENCE_TYPE_ASSIGNMENT"] = "Wyjazd służbowy";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVEMATERINITY"] = "Urlop macierzyński";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVESICK"] = "Zwolnienie chorobowe";
$MESS["INTR_USER_ABSENCE_TYPE_LEAVEUNPAYED"] = "Urlop bezpłatny";
$MESS["INTR_USER_ABSENCE_TYPE_OTHER"] = "Inne";
$MESS["INTR_USER_ABSENCE_TYPE_PERSONAL"] = "Kalendarze osobiste";
$MESS["INTR_USER_ABSENCE_TYPE_UNKNOWN"] = "Nieusprawiedliwione nieobecności";
$MESS["INTR_USER_ABSENCE_TYPE_VACATION"] = "Urlop wypoczynkowy";
?>