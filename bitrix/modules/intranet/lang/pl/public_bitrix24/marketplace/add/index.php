<?php
$MESS["MARKETPLACE_BLOCK1_INFO"] = "<p>- stwórz aplikację</p>
<p>- dodaj ją do swojego portalu</p>";
$MESS["MARKETPLACE_BLOCK1_TITLE"] = "Tylko moje konto";
$MESS["MARKETPLACE_BLOCK2_INFO"] = "<p>- zostań partnerem</p>
<p>- twórz aplikacje</p>
<p>- dodaj aplikacje do swojego profilu partnera</p>
<p>- opublikuj swoją aplikację</p>";
$MESS["MARKETPLACE_BLOCK2_TITLE"] = "Lista w Marketplace";
$MESS["MARKETPLACE_BUTTON"] = "Kontynuuj";
$MESS["MARKETPLACE_BUTTON_ADD"] = "Dodaj";
$MESS["MARKETPLACE_OR"] = "lub";
$MESS["MARKETPLACE_PAGE_TITLE"] = "Jak dodać swoją aplikację do Bitrix24?";
$MESS["MARKETPLACE_PAGE_TITLE2"] = "Aplikację mogą stworzyć Twoi programiści lub możesz ją zamówić u <a target='_blank' href=\"https://www.bitrix24.com/partners/\">jednego z naszych partnerów</a>.";
$MESS["MARKETPLACE_TITLE"] = "Dodaj aplikację";
