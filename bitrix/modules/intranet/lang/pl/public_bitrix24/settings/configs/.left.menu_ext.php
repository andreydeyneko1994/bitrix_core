<?php
$MESS["MENU_ADMIN_PANEL"] = "Panel sterowania sklepem internetowym";
$MESS["MENU_CONFIGS"] = "Ustawienia konta";
$MESS["MENU_EVENT_LOG"] = "Dziennik wydarzeń";
$MESS["MENU_MAIL_BLACKLIST"] = "Czarna lista";
$MESS["MENU_MAIL_MANAGE"] = "Zarządzanie kontami e-mail";
$MESS["MENU_UPDATE_DESC"] = "Historia wersji";
$MESS["MENU_VM"] = "Urządzenie wirtualne";
