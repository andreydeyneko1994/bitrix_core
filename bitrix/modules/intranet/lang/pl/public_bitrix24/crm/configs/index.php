<?
$MESS["BP"] = "Procesy biznesowe";
$MESS["CONFIG"] = "Inne ustawienia";
$MESS["CURRENCY"] = "Waluty";
$MESS["EXCH1C"] = "Integracja 1C";
$MESS["EXTERNAL_SALE"] = "Sklepy internetowe";
$MESS["FIELDS"] = "Pola dodatkowe";
$MESS["LOCATIONS"] = "Lokacje";
$MESS["MAIL_TEMPLATES"] = "Szablony e-mail";
$MESS["MEASURE"] = "Jednostki miar";
$MESS["PERMS"] = "Uprawnienia dostępu";
$MESS["PRODUCT_PROPS"] = "Ustawienia produktów";
$MESS["PS"] = "Zapłaty i faktury";
$MESS["SENDSAVE"] = "Integracja e-mail";
$MESS["SLOT"] = "Raporty analityczne";
$MESS["STATUS"] = "Listy wyboru";
$MESS["TAX"] = "Podatki";
$MESS["TITLE"] = "Ustawienia";
?>