<?
$MESS["TARIFF_RESTRICTION_TEXT"] = "Zarządzanie czasem pracy jest podstawowym narzędziem każdego supervisora. Sprawdź, kiedy każdy pracownik zaczął i zakończył swój dzień roboczy, ile godzin przepracowali, nad czym pracowali oraz czy ktoś się spóźnił lub wyszedł wcześniej. Cyfrowa karta Bitrix24 działa na każdym urządzeniu i daje szczegółowy raport pracy każdego pracownika, zarówno w firmie, jak i poza nią. <a href=\"javascript:void(0)\" onclick='top.BX.Helper.show(\"redirect=detail&code=1429531\");'>Szczegóły</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Zarządzanie czasem pracy jest dostępne tylko w <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planach płatnych</a>.";
$MESS["TITLE"] = "Start";
?>