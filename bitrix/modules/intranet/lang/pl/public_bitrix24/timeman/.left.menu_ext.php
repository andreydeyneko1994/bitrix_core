<?php
$MESS["MENU_ABSENCE"] = "Grafik Nieobecności";
$MESS["MENU_MEETING"] = "Spotkania i Odprawy";
$MESS["MENU_MONITOR_REPORT"] = "Osobisty asystent czasu pracy";
$MESS["MENU_SCHEDULES"] = "Harmonogramy pracy";
$MESS["MENU_TIMEMAN"] = "Czas pracy";
$MESS["MENU_WORKTIME_SETTINGS_PERMISSIONS"] = "Uprawnienia dostępu";
$MESS["MENU_WORKTIME_STATS"] = "Czas pracy";
$MESS["MENU_WORK_REPORT"] = "Raporty Pracy";
