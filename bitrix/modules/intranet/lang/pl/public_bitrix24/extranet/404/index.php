<?php
$MESS["EXTRANET_404_TEXT"] = "<p>Nie masz dostępu do ekstranetu.</p>
<p>- kliknij \"Wstecz\" w swojej przeglądarce, aby przejść do poprzedniej strony</p>
<p>- lub otwórz <a href=\"/\">Aktualności</a></p>";
$MESS["EXTRANET_404_TITLE"] = "Nie znaleziono wpisów";
