<?
$MESS["SERVICES_INFO"] = "Cette section représente des sondages actifs du portail, ainsi que l'historique de tous les votes précédents. Vous pouvez voter pour tout sondage actif.";
$MESS["SERVICES_TITLE"] = "Enquêtes";
?>