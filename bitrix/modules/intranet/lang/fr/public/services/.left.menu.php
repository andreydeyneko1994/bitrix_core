<?
$MESS["SERVICES_MENU_BOARD"] = "Tableau d'affichage";
$MESS["SERVICES_MENU_BP"] = "Procédures d'entreprise";
$MESS["SERVICES_MENU_CONTACT_CENTER"] = "Centre de contact";
$MESS["SERVICES_MENU_EVENTLIST"] = "Historique des modifications";
$MESS["SERVICES_MENU_FAQ"] = "FAQ";
$MESS["SERVICES_MENU_IDEA"] = "Avez-vous des suggestions ?";
$MESS["SERVICES_MENU_LEARNING"] = "Apprentissage";
$MESS["SERVICES_MENU_LINKS"] = "Catalogue de liens";
$MESS["SERVICES_MENU_LISTS"] = "Listes";
$MESS["SERVICES_MENU_MEETING"] = "Réunions et briefings";
$MESS["SERVICES_MENU_MEETING_ROOM"] = "Points de négociations";
$MESS["SERVICES_MENU_OPENLINES"] = "Canaux ouverts";
$MESS["SERVICES_MENU_PROCESSES"] = "Processus";
$MESS["SERVICES_MENU_REQUESTS"] = "Requêtes électroniques";
$MESS["SERVICES_MENU_SALARY"] = "Paie et vacances";
$MESS["SERVICES_MENU_SUBSCR"] = "Abonnement";
$MESS["SERVICES_MENU_SUPPORT"] = "Support technique";
$MESS["SERVICES_MENU_TELEPHONY"] = "Téléphonie";
$MESS["SERVICES_MENU_VOTE"] = "Enquêtes";
$MESS["SERVICES_MENU_WIKI"] = "Wiki";
?>