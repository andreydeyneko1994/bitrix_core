<?php
$MESS["TOP_MENU_ABSENCE"] = "Graphique des absences";
$MESS["TOP_MENU_MEETING"] = "Réunions et briefings";
$MESS["TOP_MENU_MONITOR_REPORT"] = "Assistant personnel de temps de travail";
$MESS["TOP_MENU_SETTINGS_PERMISSIONS"] = "Droits d'accès";
$MESS["TOP_MENU_TIMEMAN"] = "Temps de travail";
$MESS["TOP_MENU_WORK_REPORT"] = "Rapports de travail";
$MESS["TOP_MENU_WORK_SCHEDULES"] = "Emplois du temps";
