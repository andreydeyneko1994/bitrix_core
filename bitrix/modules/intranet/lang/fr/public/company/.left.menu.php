<?php
$MESS["COMPANY_MENU_ABSENCE"] = "Graphique des absences";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Anniversaires";
$MESS["COMPANY_MENU_CONFERENCES"] = "Réunions en ligne";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Rechercher un employé";
$MESS["COMPANY_MENU_EMPLOYEE_LIST"] = "Employés";
$MESS["COMPANY_MENU_EVENTS"] = "Changements du personnel";
$MESS["COMPANY_MENU_GALLERY"] = "Galerie photos";
$MESS["COMPANY_MENU_KNOWLEDGE_BASE"] = "Base de connaissances";
$MESS["COMPANY_MENU_LEADERS"] = "Tableau d'honneur";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Mes demandes";
$MESS["COMPANY_MENU_REPORT"] = "Efficacité";
$MESS["COMPANY_MENU_STRUCTURE"] = "Structure de l'entreprise";
$MESS["COMPANY_MENU_TELEPHONES"] = "Annuaire téléphonique";
$MESS["COMPANY_MENU_TIMEMAN"] = "Temps de travail";
$MESS["COMPANY_MENU_TIMEMAN_SECTION"] = "Temps et rapports";
$MESS["COMPANY_MENU_WORKREPORT"] = "Rapports de travail";
