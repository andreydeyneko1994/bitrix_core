<?
$MESS["MENU_CRM_ACTIVITY"] = "Mes activités";
$MESS["MENU_CRM_BUTTON"] = "Widget du site";
$MESS["MENU_CRM_COMPANY"] = "Entreprises";
$MESS["MENU_CRM_CONTACT"] = "Contacts";
$MESS["MENU_CRM_DEAL"] = "Transactions";
$MESS["MENU_CRM_DESKTOP"] = "Bureau CRM";
$MESS["MENU_CRM_FUNNEL"] = "Entonnoir des ventes";
$MESS["MENU_CRM_HISTORY"] = "Evénements";
$MESS["MENU_CRM_INVOICE"] = "Factures";
$MESS["MENU_CRM_LEAD"] = "Prospects";
$MESS["MENU_CRM_PRODUCT"] = "Catalogue";
$MESS["MENU_CRM_QUOTE"] = "Liste d'offres";
$MESS["MENU_CRM_REPORT"] = "Liste de rapports";
$MESS["MENU_CRM_SETTINGS"] = "Paramètres";
$MESS["MENU_CRM_STREAM"] = "Flux d'activités";
$MESS["MENU_CRM_WEBFORM"] = "Formulaires CRM";
?>