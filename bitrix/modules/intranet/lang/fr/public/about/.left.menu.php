<?
$MESS["ABOUT_MENU_ABOUT"] = "À propos de nous";
$MESS["ABOUT_MENU_CALENDAR"] = "Calendrier d'évènements";
$MESS["ABOUT_MENU_CAREER"] = "Carrière, postes vacants";
$MESS["ABOUT_MENU_LIFE"] = "Notre vie";
$MESS["ABOUT_MENU_NEWS"] = "Actualités (RSS)";
$MESS["ABOUT_MENU_OFFICIAL"] = "Actualités";
$MESS["ABOUT_MENU_PHOTO"] = "Galerie photos";
$MESS["ABOUT_MENU_VIDEO"] = "Galerie vidéo";
?>