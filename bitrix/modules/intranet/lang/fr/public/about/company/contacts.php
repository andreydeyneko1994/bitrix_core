<?
$MESS["ABOUT_INFO"] = "<p><b>Adresse du bureau central : </b></p>
 <dl> <dd><b>Company</b>, Inc.</dd><dt>
    <br />
  </dt> <dd>*** Pine Avenue,</dd><dt>
    <br />
  </dt> <dd>Long Beach, CA 90802</dd><dt>
    <br />
  </dt> <dd>USA</dd><dt>
    <br />
  </dt> </dl>


<p><b>Plan d'accès au bureau : </b></p>
<p><img height='449' width='500' src='#SITE#images/en/company/about/address_1.png' /></p>

<p></p>


<p><b>Horaires du bureau : </b></p>
Lundi-jeudi : 9.00 - 18.00
<br />
Vendredi : 9.00 - 17.00
<br />


<p><b>Téléphone : </b> </p>

<p>+1 (555) 432-2920 - secrétariat</p>

<p><b>Fax : </b></p>

<p>+1 (555) 432-2857</p>

<p><b>E-mail : </b></p>

<p>info@company.com</p>

<p><b>Téléphones supplémentaires</b> : </p>

<p>+1 (555) 432-2920 - Service du personnel
 <br />
+1 (555) 432-2920 - Service de gardiennage</p>

<br />";
$MESS["ABOUT_TITLE"] = "Contacts";
?>