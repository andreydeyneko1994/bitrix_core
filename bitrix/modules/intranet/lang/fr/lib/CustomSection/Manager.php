<?php
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_COMPONENT_NOT_FOUND"] = "Impossible d'afficher la page de la section actuelle";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_INVALID_URL"] = "Le format du lien est incorrect";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_AVAILABLE"] = "La section n'a pas de pages affichables";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_FOUND"] = "La section est introuvable";
