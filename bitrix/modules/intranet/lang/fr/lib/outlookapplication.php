<?php
$MESS["WS_OUTLOOK_APP_COMMENT"] = "Généré automatiquement";
$MESS["WS_OUTLOOK_APP_DESC"] = "Obtenez un mot de passe pour Microsoft Outlook pour configurer la synchronisation de vos calendriers, contacts ou les tâches.";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "Connecter";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT"] = "Synchroniser avec Microsoft Outlook";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT_TYPE"] = "Synchroniser avec Microsoft Outlook : #TYPE#";
$MESS["WS_OUTLOOK_APP_TITLE"] = "Microsoft Outlook services";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "Les contacts, calendriers, tâches";
$MESS["WS_OUTLOOK_APP_TYPE_calendar"] = "calendrier";
$MESS["WS_OUTLOOK_APP_TYPE_contacts"] = "contacts";
$MESS["WS_OUTLOOK_APP_TYPE_tasks"] = "tâches";
