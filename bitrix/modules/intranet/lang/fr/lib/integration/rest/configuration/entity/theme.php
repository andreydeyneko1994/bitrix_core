<?php
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_NOT_FOUND"] = "Le thème que vous avez sélectionné est introuvable dans votre Bitrix24.";
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_SET_DEFAULT"] = "Impossible de faire du thème importé un thème par défaut.";
