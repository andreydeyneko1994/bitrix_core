<?php
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "Installez l'application Bitrix24 sur votre appareil mobile et restez toujours connecté !

Installez l'application de bureau sur votre ordinateur (Mac, Windows ou Linux) pour communiquer avec vos collègues quand votre navigateur est fermé.

[URL=https://www.bitrix24.com/features/mobile.php]Télécharger les applications Bitrix24[/URL]
";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "Télécharger l'application Bitrix24";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "Invitez des collègues à rejoindre votre Bitrix24.

#ANCHOR_INVITE#Inviter#ANCHOR_END#
";
$MESS["SONET_INVITE_TASK_DESCRIPTION_V2"] = "Invitez des collègues à rejoindre votre Bitrix24.

[URL=/company/vis_structure.php]Inviter[/URL]
";
$MESS["SONET_INVITE_TASK_TITLE"] = "Inviter des collègues";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "Remplissez votre profil, téléchargez votre image d'utilisateur et fournissez vos informations personnelles.

#ANCHOR_EDIT_PROFILE#Aller sur votre profil#ANCHOR_END#
";
$MESS["SONET_TASK_TITLE"] = "Remplir le profil";
