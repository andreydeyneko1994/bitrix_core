<?php
$MESS["MAIN_UI_SELECTOR_DEPARTMENT_FLAT_PATTERN"] = "#NAME# (sans les sous-départements)";
$MESS["MAIN_UI_SELECTOR_EXTRANET"] = "Extranet";
$MESS["MAIN_UI_SELECTOR_SELECT_FLAT_TEXT_DEPARTMENTS"] = "Tous les employés du service";
$MESS["MAIN_UI_SELECTOR_SELECT_TEXT_DEPARTMENTS"] = "Tous les employés du service et sous-service";
$MESS["MAIN_UI_SELECTOR_TAB_DEPARTMENTS"] = "Les employés et les départements";
$MESS["MAIN_UI_SELECTOR_TITLE_DEPARTMENTS"] = "Départements :";
