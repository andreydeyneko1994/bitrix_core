<?php
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_GREEN"] = "Bon";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_GREEN1"] = "Faible";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_GREEN2"] = "Léger";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_GREEN3"] = "Optimal";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED"] = "Mauvais";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED1"] = "Épuisant";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED2"] = "Fatigant";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED3"] = "Exténuant";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED4"] = "Éprouvant ";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED5"] = "Élevé";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED6"] = "Super stressé";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_RED7"] = "Extrême";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_UNKNOWN"] = "Non défini";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_YELLOW"] = "OK";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_YELLOW1"] = "Génant";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_YELLOW2"] = "Un peu élevé";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_DESCRIPTION_YELLOW3"] = "Élevé";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_GREEN1"] = "Votre corps est détendu. Vous ne montrez aucun signe de surmenage.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_GREEN2"] = "Votre corps est soumis à une légère pression, mais vous vous en sortez bien. Ce type de stress ne renforcera pas votre résilience, mais ne vous fera aucun mal non plus.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_GREEN3"] = "Votre corps est juste assez stressé. Il est temps de s'y mettre. C'est ce qui s'appelle l'eustress : une quantité saine de stress qui aide à renforcer la résilience du corps.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED1"] = "Votre niveau de stress est bas, mais vous avez du mal à le gérer. Il semblerait que vous avez récemment dû vous occuper de quelque chose d'éprouvant physiquement, mentalement ou émotionnellement.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED2"] = "Votre niveau de stress est bas, mais vous avez du mal à le gérer. Il semblerait que vous avez récemment dû vous occuper de quelque chose d'éprouvant physiquement, mentalement ou émotionnellement.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED3"] = "Votre corps utilise beaucoup de ressources pour gérer le stress et cela pèse sur vos systèmes. Mais vous pouvez encore vous sentir bien.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED4"] = "Votre corps est surmené. Vous en faites trop. Ce n'est pas une bonne situation, surtout sur le long terme, étant donné qu'elle a un vrai impact négatif sur votre santé.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED5"] = "Votre corps est soumis à trop de stress. Il consomme bien trop de ressources pour gérer la pression.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED6"] = "Votre corps est soumis à trop de stress. Vous êtes dans un état de détresse, ce qui vous draine rapidement de vos ressources et de votre énergie.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_RED7"] = "Votre corps est soumis à trop de stress. Vos systèmes ont du mal à fonctionner correctement. Cette situation nuit petit à petit à votre santé.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_TITLE"] = "Que signifie le niveau de stress #LEVEL# ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_TITLE_GREEN"] = "Que signifie un faible niveau de stress ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_TITLE_RED"] = "Que signifie un niveau de stress élevé ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_TITLE_UNKNOWN"] = "Que signifie un niveau de stress indéfini ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_TITLE_YELLOW"] = "Que signifie un niveau de stress moyen ?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_YELLOW1"] = "Votre corps est trop détendu et n'arrive pas à se ressaisir. Il semblerait que vous en avez trop fait récemment et qu'il est occupé à récupérer.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_YELLOW2"] = "Votre corps est légèrement surmené. Il gère correctement, mais utilise plus de ressources que d'habitude pour gérer la pression.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_TYPE_TEXT_YELLOW3"] = "Votre corps est surmené. Il gère correctement, mais utilise plus de ressources et d'énergie que d'habitude pour gérer la pression.";
