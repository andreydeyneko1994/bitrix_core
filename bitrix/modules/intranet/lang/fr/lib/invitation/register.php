<?php
$MESS["INTRANET_INVITATION_EMAIL_ERROR"] = "Les adresses e-mail ne sont pas correctes :";
$MESS["INTRANET_INVITATION_EMAIL_LIMIT_EXCEEDED"] = "Le nombre maximum d’adresses e-mail dans l'invitation est dépassé.";
$MESS["INTRANET_INVITATION_ERROR_USER_TRANSFER"] = "Impossible de convertir l'utilisateur de l'adresse e-mail.";
$MESS["INTRANET_INVITATION_INVITE_MESSAGE_TEXT"] = "Vous avez été invité à rejoindre Bitrix24. Acceptez l'invitation et accédez à votre compte ou vous pourrez collaborer, gérer des projets et des tâches, partager des documents, planifier des évènements et des réunions, communiquer et organiser des évènements.";
$MESS["INTRANET_INVITATION_MAX_COUNT_ERROR"] = "Le nombre d'employés invités dépasse les conditions de votre licence.";
$MESS["INTRANET_INVITATION_PHONE_ERROR"] = "Les numéros de téléphone ne sont pas corrects :";
$MESS["INTRANET_INVITATION_PHONE_LIMIT_EXCEEDED"] = "Trop de numéros de téléphone ajoutés à l'invitation.";
$MESS["INTRANET_INVITATION_USER_EXIST_ERROR"] = "Les utilisateurs avec ces adresses e-mail existent déjà : #EMAIL_LIST#";
$MESS["INTRANET_INVITATION_USER_PHONE_EXIST_ERROR"] = "Les utilisateurs avec ces numéros de téléphone existent déjà : #PHONE_LIST#";
