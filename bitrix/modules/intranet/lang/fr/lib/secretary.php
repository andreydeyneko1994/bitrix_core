<?php
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_EMPTY_SUBJECT"] = "Message par e-mail #MESSAGE_ID#";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_FIRST_MESSAGE"] = "Chat créé à partir d'un message par e-mail \"#MAIL_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_TITLE"] = "Chat créé à partir d'un message par e-mail \"#MAIL_TITLE#\"";
