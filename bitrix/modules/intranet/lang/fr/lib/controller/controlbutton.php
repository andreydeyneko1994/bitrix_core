<?php
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_FIRST_MESSAGE"] = "Chat créé à partir de l'évènement \"#EVENT_TITLE#\" du #DATETIME_FROM#";
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_TITLE"] = "Chat \"#EVENT_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_CREATE_CHAT_LOCK_ERROR"] = "Erreur lors de la création du chat. Veuillez réessayer.";
$MESS["INTRANET_CONTROL_BUTTON_DELETE_TASK_FILE_ERROR"] = "Erreur de suppression du fichier.";
$MESS["INTRANET_CONTROL_BUTTON_ENTITY_ERROR"] = "L'article est introuvable.";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_CALENDAR"] = "Créé à partir d'un [url=#LINK#]évènement du calendrier[/url]";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_TASK"] = "Créé à partir d'une [url=#LINK#]tâche[/url]";
$MESS["INTRANET_CONTROL_BUTTON_TASK_CHAT_FIRST_MESSAGE"] = "Chat créé à partir de la tâche \"#TASK_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_LIMIT"] = "Le nombre maximum d'utilisateurs d'appels vidéo est dépassé";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_SELF_ERROR"] = "Un appel vidéo nécessite deux participants ou plus. Veuillez ajouter d'autres utilisateurs et appeler à nouveau.";
$MESS["INTRANET_CONTROL_MEETING_CREATED"] = "Évènement créé sur la base d'une tâche";
