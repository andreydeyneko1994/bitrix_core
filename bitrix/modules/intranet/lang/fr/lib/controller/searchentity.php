<?php
$MESS["INTRANET_CONTROLLER_SEARCHENTITY_GETALL_ENTITY_EMPTY"] = "Le type d'entité n'est pas spécifié.";
$MESS["INTRANET_CONTROLLER_SEARCHENTITY_GETALL_ENTITY_INCORRECT"] = "Le type d'entité spécifié est incorrect.";
