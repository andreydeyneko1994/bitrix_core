<?php
$MESS["INTRANET_CONTROLLER_INVITE_DELETE_FAILED"] = "Erreur lors de la suppression de l'invitation";
$MESS["INTRANET_CONTROLLER_INVITE_FAILED"] = "Erreur lors de la création de l'invitation";
$MESS["INTRANET_CONTROLLER_INVITE_NO_PERMISSIONS"] = "Vous n'avez pas la permission d'inviter des utilisateurs.";
$MESS["INTRANET_CONTROLLER_INVITE_NO_USER_ID"] = "Le paramètre userId manque";
$MESS["INTRANET_CONTROLLER_INVITE_USER_NOT_FOUND"] = "L'utilisateur avec l'ID n'est pas trouvé.";
