<?
$MESS["UF_1C"] = "Utilisateur de 1C";
$MESS["UF_DEPARTMENT"] = "Départements";
$MESS["UF_DISTRICT"] = "Arrondissement";
$MESS["UF_EMPLOYMENT_DATE"] = "Date d'embauche";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "INN";
$MESS["UF_INTERESTS"] = "Intérêts";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Numéro d'extension";
$MESS["UF_SKILLS"] = "Compétences";
$MESS["UF_SKYPE"] = "Pseudo Skype";
$MESS["UF_SKYPE_LINK"] = "Lien de chat Skype";
$MESS["UF_STATE_HISTORY"] = "Historique des statuts";
$MESS["UF_STATE_LAST"] = "Dernier statut";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Autres sites internet";
$MESS["UF_XING"] = "Xing";
$MESS["UF_ZOOM"] = "Zoom";
?>