<?
$MESS["INTR_ADD_TITLE"] = "Ajouter un département";
$MESS["INTR_EDIT_TITLE"] = "Éditer la sous-section";
$MESS["INTR_STRUCTURE_ADD"] = "Ajouter";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Ajouter plus";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Département supérieur";
$MESS["INTR_STRUCTURE_EDIT"] = "Enregistrer";
$MESS["INTR_STRUCTURE_HEAD"] = "Dirigeant";
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INTR_STRUCTURE_NAME"] = "Nom du département";
$MESS["INTR_STRUCTURE_SUCCESS"] = "Un service été ajouté.";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Choisir dans la structure";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Vous ne disposez pas des droits nécessaires pour modifier";
?>