<?
$MESS["INTASK_2LOG_ADD"] = "Une nouvelle tâche #TITLE# a été  ajouté.";
$MESS["INTASK_2LOG_DELETE"] = "La tâche #TITLE# a été supprimée.";
$MESS["INTASK_2LOG_UPDATE"] = "La tâche #TITLE# est modifiée.";
$MESS["INTASK_FROM_DATE_TLP"] = "dès #DATE#";
$MESS["INTASK_I_ASSIGNED2ME_ACT"] = "Assignés pour moi (actifs)";
$MESS["INTASK_I_ASSIGNED2ME_FIN"] = "Assignés à moi (terminés)";
$MESS["INTASK_I_BY_PRIORITY"] = "Assignés à moi (par la priorité)";
$MESS["INTASK_I_COMPLETED"] = "Achevé(e)s";
$MESS["INTASK_I_CREATED_BY_ACT"] = "Créés par moi (actives)";
$MESS["INTASK_I_CREATED_BY_FIN"] = "Créés par moi (terminés)";
$MESS["INTASK_I_FIN"] = "Achevé(e)s";
$MESS["INTASK_I_GANT"] = "Chat de Gantt";
$MESS["INTASK_I_PERSONAL"] = "Personnel";
$MESS["INTASK_I_TODAY"] = "Aujourd'hui";
$MESS["INTASK_L_TASKCOMPLETE"] = "Achevé(e)s #PRC#%";
$MESS["INTASK_TO_DATE_TLP"] = "jusqu'à #DATE#";
$MESS["INTAST_T6654_LOG"] = "Statut : #STATUS#
Importance : #PRIORITY#
Durée : #TIME#
Responsable : #RESP#";
$MESS["INTE_ADD_TASK_DATES_EMPTY"] = "non";
$MESS["INTE_REMIND_TASK_MESSAGE"] = "Rappel sur la tâche '#NAME#'

[url=#URL_VIEW#]Voir[/url]";
$MESS["INTI_ACTIVE_DATE"] = "Actuellement en cours d'exécution";
$MESS["INTI_CODE"] = "Achèvement";
$MESS["INTI_CODE_F"] = "Achèvement";
$MESS["INTI_CREATED_BY"] = "Auteur";
$MESS["INTI_CREATED_BY_F"] = "Auteur";
$MESS["INTI_DATE_ACTIVE_FROM"] = "Commencer";
$MESS["INTI_DATE_ACTIVE_FROM_F"] = "Commencer";
$MESS["INTI_DATE_ACTIVE_TO"] = "Achèvement";
$MESS["INTI_DATE_ACTIVE_TO_F"] = "Achèvement";
$MESS["INTI_DATE_CREATE"] = "Créé";
$MESS["INTI_DATE_CREATE_F"] = "Date de création";
$MESS["INTI_DETAIL_TEXT"] = "Description de la tâche";
$MESS["INTI_DETAIL_TEXT_F"] = "Description de la tâche";
$MESS["INTI_IBLOCK_SECTION"] = "Dossier";
$MESS["INTI_IBLOCK_SECTION_F"] = "Dossier";
$MESS["INTI_ID"] = "ID";
$MESS["INTI_ID_F"] = "ID";
$MESS["INTI_MODIFIED_BY"] = "Modifié par";
$MESS["INTI_MODIFIED_BY_F"] = "Modifié par";
$MESS["INTI_NAME"] = "Titre";
$MESS["INTI_NAME_F"] = "Titre";
$MESS["INTI_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTI_TASKALERT"] = "Aviser des modifications";
$MESS["INTI_TASKALERT_L"] = "Aviser des modifications";
$MESS["INTI_TASKASSIGNEDTO"] = "Responsable";
$MESS["INTI_TASKASSIGNEDTO_L"] = "Responsable";
$MESS["INTI_TASKCOMPLETE"] = "Achevé(e)s (%)";
$MESS["INTI_TASKCOMPLETE_L"] = "Achevé(e)s (%)";
$MESS["INTI_TASKFILES"] = "Fichier";
$MESS["INTI_TASKFILES_L"] = "Fichier";
$MESS["INTI_TASKFINISH"] = "La date de terminaison effective";
$MESS["INTI_TASKFINISH_L"] = "La date de terminaison effective";
$MESS["INTI_TASKPRIORITY"] = "criticité";
$MESS["INTI_TASKPRIORITY_1"] = "Augmenté";
$MESS["INTI_TASKPRIORITY_2"] = "Moyen";
$MESS["INTI_TASKPRIORITY_3"] = "Bas";
$MESS["INTI_TASKPRIORITY_L"] = "criticité";
$MESS["INTI_TASKREMIND"] = "Rappeler";
$MESS["INTI_TASKREMIND_L"] = "Rappeler";
$MESS["INTI_TASKREPORT"] = "Rapport";
$MESS["INTI_TASKREPORT_L"] = "Rapport";
$MESS["INTI_TASKSIZE"] = "Volume (h)";
$MESS["INTI_TASKSIZEREAL"] = "Combien d'heures";
$MESS["INTI_TASKSIZEREAL_L"] = "Combien d'heures";
$MESS["INTI_TASKSIZE_L"] = "Volume (h)";
$MESS["INTI_TASKSTATUS"] = "Statut";
$MESS["INTI_TASKSTATUS_1"] = "Non accepté(e)";
$MESS["INTI_TASKSTATUS_2"] = "N'a pas commencé";
$MESS["INTI_TASKSTATUS_3"] = "En cours d'exécution";
$MESS["INTI_TASKSTATUS_4"] = "Achevé(e)s";
$MESS["INTI_TASKSTATUS_5"] = "Attend l'exécution";
$MESS["INTI_TASKSTATUS_6"] = "Différé";
$MESS["INTI_TASKSTATUS_L"] = "Statut";
$MESS["INTI_TASKTRACKERS"] = "Observateurs";
$MESS["INTI_TASKVERSION"] = "Version API";
$MESS["INTI_TIMESTAMP_X"] = "Modifié";
$MESS["INTI_TIMESTAMP_X_F"] = "Modifié";
$MESS["INTI_VERSION"] = "Version";
$MESS["INTI_XML_ID"] = "ID externe";
$MESS["INTI_XML_ID_F"] = "ID externe";
?>