<?
$MESS["BX24_CLOSE_BUTTON"] = "Fermer";
$MESS["BX24_INVITE_BUTTON"] = "Inviter";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Email : <br/> sont incorrectement spécifiés";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Le nombre maximum d'adresses e-mail dans l'invitation est dépassé.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "Adresse email inconnu.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "Les e-mails ne sont pas indiqués.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "Le nom de famille n'est pas donné.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "Le mot de passe n'est pas indiqué.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "Impossible de convertir l'utilisateur de l'e-mail.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "Confirmation erronée du mot de passe.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Un utilisateur de l'e-mail incorrect a été spécifié.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Veuillez nous rejoindre dans notre compte Bitrix24. C'est un endroit où tout le monde peut communiquer, collaborer sur des tâches et des projets, gérer des clients, et bien plus encore.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Le nombre d'employés invités surpasse la limite prévue du plan tarifaire.";
$MESS["BX24_INVITE_DIALOG_PHONE_ERROR"] = "Ces numéros de téléphone sont incorrects :<br/>";
$MESS["BX24_INVITE_DIALOG_PHONE_LIMIT_EXCEEDED"] = "Trop de numéros de téléphone ajoutés à l'invitation.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR1"] = "Cet adresse e-mail #EMAIL# est déjà utilisée par un utilisateur.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR2"] = "Ces adresses e-mail #EMAIL# sont déjà utilisées par des utilisateurs.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "L'ID de l'utilisateur est incorrect.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR1"] = "Il existe déjà un utilisateur associé au numéro de téléphone #PHONE#.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR2"] = "Il existe déjà des utilisateurs associés aux numéros de téléphone #PHONE_LIST#.";
$MESS["BX24_INVITE_TITLE_ADD"] = "Ajouter un employé (utilisateur)";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Inviter les employés";
$MESS["BX24_LOADING"] = "En cours de chargement...";
?>