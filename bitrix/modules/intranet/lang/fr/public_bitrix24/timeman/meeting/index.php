<?
$MESS["TARIFF_RESTRICTION_TEXT"] = "Bitrix24 peut vous aider à planifier et organiser des évènements et réunions grâce à un planificateur spécial d'évènements et de réunions. Les tâches seront assignées aux personnes désignées, la discussion et les procédures seront enregistrées, les personnes responsables seront invitées à créer un rapport sur leurs ordres du jour. <a href=\"javascript:void(0)\" onclick='top.BX.Helper.show(\"redirect=detail&code=1436032\");'>Détails</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Les réunions et briefings sont uniquement disponibles avec les <a href=\"/settings/license_all.php\" target=\"_blank\">offres commerciales sélectionnées</a>.";
$MESS["TITLE"] = "Réunions et briefings";
?>