<?php
$MESS["MENU_ABSENCE"] = "Graphique des absences";
$MESS["MENU_MEETING"] = "Réunions et briefings";
$MESS["MENU_MONITOR_REPORT"] = "Assistant personnel de temps de travail";
$MESS["MENU_SCHEDULES"] = "Emplois du temps";
$MESS["MENU_TIMEMAN"] = "Temps de travail";
$MESS["MENU_WORKTIME_SETTINGS_PERMISSIONS"] = "Droits d'accès";
$MESS["MENU_WORKTIME_STATS"] = "Temps de travail";
$MESS["MENU_WORK_REPORT"] = "Rapports de travail";
