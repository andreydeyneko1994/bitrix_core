<?php
$MESS["MENU_ADMIN_PANEL"] = "Panneau de configuration de la boutique en ligne";
$MESS["MENU_CONFIGS"] = "Paramètres du compte";
$MESS["MENU_EVENT_LOG"] = "Journal d'évènements";
$MESS["MENU_MAIL_BLACKLIST"] = "Liste noire";
$MESS["MENU_MAIL_MANAGE"] = "Gérer les comptes e-mail";
$MESS["MENU_UPDATE_DESC"] = "Historique des versions";
$MESS["MENU_VM"] = "Machine virtuelle";
