<?php
$MESS["MENU_BIZPROC_ACTIVE"] = "Flux de travail en cours d'exécution";
$MESS["MENU_BIZPROC_TASKS"] = "Mes affectations";
$MESS["MENU_BIZPROC_TASKS_1"] = "Mes affectations";
$MESS["MENU_MY_PROCESS"] = "Mes flux de travail";
$MESS["MENU_MY_PROCESS_1"] = "Mes flux de travail";
$MESS["MENU_PROCESS_STREAM"] = "Flux de travail dans le flux d'activité";
$MESS["MENU_PROCESS_STREAM2"] = "Flux de travail dans les Actualités";
