<?
$MESS["BP"] = "Procédures d'entreprise";
$MESS["CONFIG"] = "Autres paramètres";
$MESS["CURRENCY"] = "Devises";
$MESS["EXCH1C"] = "Intégration 1C";
$MESS["EXTERNAL_SALE"] = "Boutiques en ligne";
$MESS["FIELDS"] = "Champs personnalisés";
$MESS["LOCATIONS"] = "Situations";
$MESS["MAIL_TEMPLATES"] = "Modèles d'e-mails";
$MESS["MEASURE"] = "Unités de mesure";
$MESS["PERMS"] = "Droits d'accès";
$MESS["PRODUCT_PROPS"] = "Propriétés des produits";
$MESS["PS"] = "Mode de paiement";
$MESS["SENDSAVE"] = "Intégration avec l'email";
$MESS["SLOT"] = "Rapports analytiques";
$MESS["STATUS"] = "Listes de sélection";
$MESS["TAX"] = "Impôts & taxes";
$MESS["TITLE"] = "Paramètres";
?>