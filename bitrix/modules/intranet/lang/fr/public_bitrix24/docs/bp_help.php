<?
$MESS["CONTENT"] = "<div> Contenus :
  <ul>
    <li><a href='#bizproc' title='Qu'est-ce que les procédures d'entreprise ?'>Processus d'entreprise</a></li>

    <li><a href='#typique' title='Quelles sont les procédures standards les plus répandues ?'>Processus d'entreprise standards</a></li>

    <li><a href='#travail' title='Comment créer une procédure d'entreprise ?'>Création d'un processus d'entreprise</a></li>

    <li><a href='#performance' title='Comment réaliser une procédure d'entreprise '>Exécution d'un processus d'entreprise</a></li>
  </ul>
  <h1><a name='bizproc'></a>Processus d'entreprise</h1>

  <p>La notion de <b>processus d'entreprise</b> fait référence à un instrument de création, d'entretien et de gestion des flux d'informations.</p>

  <p><i>Un <b>processus d'entreprise</b> est un système de traitement d'informations (ou de documents) selon un ordre ou une procédure bien définis. Le processus d'entreprise peut indiquer : </i></p>
  <ul>
    <li>un ou plusieurs <i>points d'entrée et de sortie</i> (début et fin du processus) ; </li>
    <li>une <i>séquence d'actions (étapes, niveaux, fonctions)</i> qui seront exécutées par l'algorithme du processus d'entreprise.</li>
  </ul>
  <p>Le monde réel prend en compte un large éventail de flux d'informations différents, des plus simples aux plus compliqués. Un simple processus de publication de document peut contenir diverses actions possibles et branches de conditions. Il peut aussi être nécessaire d'intégrer des données et des notifications d'utilisateur.</p>

  <p>Les <b>Processus d'entreprise</b> permettent à un utilisateur ordinaire de créer et d'éditer n'importe quelle combinaison de flux d'informations et d'actions. Le créateur de processus d'entreprise a été conçu pour être aussi simple que possible, ce qui signifie qu'un utilisateur commercial ordinaire, pas un programmeur, pourra accéder à une grande gamme de fonctions et de fonctionnalités. Cependant, la notion même que processus d'entreprise implique qu'un niveau plus élevé que la normale de pensée analytique et de connaissance approfondie de ce qui se passe réellement au sein de la société doivent être combinés pour obtenir un maximum de résultats.</p>

  <p>Le Créateur de processus d'entreprise est une sorte de créateur de blocs visuel basé sur du <b>glisser/poser</b>. Les modèles de processus d'entreprise sont créés dans une version spécialisée. Un auteur de processus d'entreprise peut indiquer les étapes du processus et leur séquence, ainsi que mettre en valeur des détails propres au processus grâce à de simples plans visuels.</p>
<p>Un flux d'informations spécifique est défini par le modèle de processus d'entreprise et est constitué de plusieurs actions. Une action peut être un évènement de votre choix : création de documents, envoi de message par e-mail, ajout d'un enregistrement dans la base de données, etc.</p>
<p>Le système contient déjà des dizaines d'actions intégrées et certains processus d'entreprise typiques qui peuvent être utilisés pour modeler les activités professionnelles le plus communes.</p>
<p>Deux types de processus d'entreprise sont particulièrement répandus : </p>
 <ul>
    <li>un <b>processus d'entreprise séquentiel</b> pour effectuer une série d'actions consécutives sur un document, dans un ordre prédéfini ; </li>
    <li>un <b>processus d'entreprise basé sur les statuts</b> sans début ni fin : le statut du processus est modifié pendant la résolution du flux de travail. Ce genre de processus peut théoriquement s'arrêter dans n'importe quelle étape.</li>
   </ul>

  <h2>Processus d'entreprise séquentiel</h2>

  <p>Le mode séquentiel est généralement utilisé pour les processus dont le cycle de vie est prédéfini et limité. Un exemple typique serait la création et l'approbation d'un document texte. Les processus séquentiels comprennent généralement plusieurs actions entre leurs points de départ et de fin.
</p>
  <p><img border=\"1\" alt=\"Exemple : simple processus linéaire\" title=\"Exemple : simple processus linéaire\" src=\"/images/bp/en/2.png\" /></p>

  <h2>Processus d'entreprise basé sur les statuts</h2>

  <p>Une approche basée sur les statuts est utilisée quand un processus n'a pas un cadre temporel défini et peut être répété ou renvoyé à un statut donné du fait de la nature du processus (par exemple : mise à jour continue d'une documentation de produit). Un statut n'est alors pas qu'un marqueur utilisé pour montrer la progression du document, c'est également un moyen de décrire un cycle de processus dans le monde réel.</p>
  <p>La création d'un modèle de processus basé sur les statuts n'est pas aussi simple que celle d'un processus séquentiel, mais ses possibilités en matière d'automatisation du traitement des informations sont vastes. Un plan typique est constitué de plusieurs statuts qui, à leur tour, comprennent des actions et des conditions de modification.</p>
 <img border=\"1\" alt=\"Exemple : processus avec statuts\" title=\"Exemple : processus avec statuts\" src=\"/images/bp/en/3.png\" />
  <p>Chaque action d'un statut est généralement un processus séquentiel fini.</p>

  <h1><a name=\"typique\"></a>Processus d'entreprise typiques</h1>

<p>Le système est livré avec plusieurs processus d'entreprise typiques prêt à l'emploi. Vous pouvez les ajuster pour qu'ils correspondent aux flux d'informations de votre société grâce au Créateur visuel de processus d'entreprise.</p>
  <h2>Processus séquentiel \"Simple approbation/vote</h2>

  <p>Recommandé quand une prise de décision implique une simple majorité de votes.</p>

  <h2>Processus séquentiel \"Première approbation\"</h2>

  <p>Recommandé quand une simple approbation ou réponse (\"J'ai besoin de volontaires\") suffit.</p>

  <h2>Processus basé sur les statuts &quot;Approuver un document avec étapes&quot;</h2>

  <p>Recommandé quand un accord mutuel est requis pour approuver un document. </p>

  <h2>Processus séquentiel &quot;Approbation en deux étapes&quot;</h2>

<p>Recommandé quand un document nécessite l'évaluation préliminaire d'un expert avant approbation.</p>

  <h2>Processus séquentiel &quot;Avis d'expert&quot;</h2>

  <p>Recommandé quand une personne qui doit approuver ou rejeter un document a besoin de commentaires experts sur le sujet.</p>

  <h2>Processus séquentiel &quot;Lire le document&quot;</h2>

  <p>Recommandé quand les employés doivent se familiariser avec un document.</p>
  <p>Vous pouvez voir les processus d'entreprise (standards et définis par les utilisateurs) liés à un certain type de document en cliquant sur <b>Plus</b> et en sélectionnant <b>Processus d'entreprise</b> dans le menu : </p>
  <p><img border=\"1\" src=\"/images/bp/en/4.png\" alt=\"Afficher les processus d'entreprise\" title=\"Afficher les processus d'entreprise\" /></p>
<p>Cela ouvrira la page <b>Modèles de processus d'entrepris</b> dans laquelle vous pouvez éditer les processus existants et en créer de nouveaux.</p>
  <p><img border=\"1\" src=\"/images/bp/en/11.png\" alt=\"Page des processus d'entreprise\" title=\"Page des processus d'entreprise\" /></p>
  <h1><a name=\"travail\"></a>Créer un processus d'entreprise</h1>

  <p>Pour créer et éditer un processus d'entreprise, vous devez utiliser l'éditeur visuel de processus d'entreprise.</p>

  <p>Avant de créer un processus d'entreprise, vous devez sélectionner son type, séquentiel ou basé sur les statuts, ce qui va définir l'agencement de l'éditeur visuel. Le type peut être sélectionné grâce aux contrôles de la barre d'outils contextuelle du formulaire <b>Modèles de processus d'entreprise</b>.</p>

  <p>La priorité, lors de la création d'un processus d'entreprise, est de définir les paramètres. Ces paramètres de processus sont des données qui peuvent être accédées dans n'importe quelles commande, action ou condition. Une fois les paramètres définis, vous pouvez passer à la création du processus.</p>
 <img border=\"1\" title=\"Définir les paramètres du processus\" alt=\"Définir les paramètres du processus\" src=\"/images/bp/en/6.png\" />

  <h2>Création d'un processus basé sur les statuts</h2>

  <p>Tout d'abord, créez et configurez les statuts nécessaires grâce au bouton Ajouter un statut. Créez ensuite des commandes pour chaque statut. Chaque commande représente un processus séquentiel séparé.</p>
   <img border=\"1\" src=\"/images/bp/en/7.png\" alt=\"Affecter des actions dans des statuts\" title=\"Affecter des actions dans des statuts\" />

  <h2>Création d'un processus séquentiel</h2>

  <p>Quand vous créez un processus séquentiel, l'éditeur visuel présente un jeu d'actions que vous pouvez personnaliser.</p>

  <p>L'éditeur visuel utilise le glisser-poser populaire pour ajouter les actions. Une fois une commande ajoutée, configurez-en les paramètres. Chacune dispose d'une boîte de dialogue de paramètres unique.</p>
<img border=\"1\" title=\"Ajouter des actions dans l'éditeur visuel\" alt=\"Ajouter des actions dans l'éditeur visuel\" src=\"/images/bp/en/8.png\" /><br /><br />
  <h1><a name=\"performance\"></a>Exécuter un processus d'entreprise</h1>
<p>Un processus d'entreprise peut être exécuté manuellement ou automatiquement, selon ses paramètres. Le mode de lancement n'affecte pas l'exécution. Plusieurs instances d'un même processus peuvent coexister, chacune exécutée séparément.</p>
  <p>Pour exécuter un processus d'entreprise sur un document spécifique, cliquez sur la commande <b>Nouveau processus d'entreprise</b> du menu action du document, et sélectionnez le processus nécessaire dans la liste.</p>
 <img border=\"1\" src=\"/images/bp/en/5.png\" alt=\"Lancement d'un processus d'entreprise pour un document\" title=\"Lancement d'un processus d'entreprise pour un document\" />
<p>Une fois la fenêtre des paramètres du processus d'entreprise ouverte, indiquez les paramètres et cliquez sur <b>Démarrer</b>.</p>
 <img border=\"1\" title=\"Configurer un processus d'entreprise\" alt=\"Configurer un processus d'entreprise\" src=\"/images/bp/en/9.png\" />
  <p>Si un processus d'entreprise fournit des options de notification, une notification sera envoyée à un employé quand le processus atteindra un point demandant une action de la part de l'employé en question. Pour afficher et effectuer les tâches affectées par le processus d'entreprise en cours d'exécution, un utilisateur peut cliquer sur le lien <b>Processus d'entreprise</b> du menu de gauche, sous le groupe <b>Mon espace de travail</b>.</p>
</div>";
$MESS["TITLE"] = "Procédures d'entreprise";
?>