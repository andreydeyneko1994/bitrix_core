<?php
$MESS["MENU_BLOG"] = "Conversations";
$MESS["MENU_CONTACT"] = "Contacts";
$MESS["MENU_EMPLOYEE"] = "Employés";
$MESS["MENU_FILES"] = "Mon disque";
$MESS["MENU_GROUPS"] = "Groupes de travail";
$MESS["MENU_GROUPS_EXTRANET_ALL"] = "Tous les groupes de travail";
$MESS["MENU_LIVE_FEED"] = "Flux d'activités";
$MESS["MENU_LIVE_FEED2"] = "Actualités";
$MESS["MENU_TASKS"] = "Tâches";
