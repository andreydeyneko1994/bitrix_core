<?
$MESS["INTR_MAIL_DOMAINREADY_NOTICE"] = "O domínio [b]#DOMAIN#[/b] foi conectado. <a href=\"#SERVER# /empresa/pessoal/e-mail/?page=gerenciar\">Administrar caixas de correio</a>";
$MESS["INTR_MAIL_DOMAIN_SUPPORT_LINK"] = "http://www.bitrixsoft.com/support/index.php?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_SYNC_OUTLOOK_NOWEBSERVICE"] = "O módulo de Serviços Web é necessário para sincronizar com o Outlook. Entre em contato com o administrador do seu portal.";
?>