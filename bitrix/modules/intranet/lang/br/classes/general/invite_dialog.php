<?
$MESS["BX24_CLOSE_BUTTON"] = "Fechar";
$MESS["BX24_INVITE_BUTTON"] = "Convidar";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Os endereço de e-mail está incorreto: <br/>";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Número máximo de endereços de e-mail no convite excedido.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "O endereço de e-mail não foi especificado.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "Os endereços de e-mail não foram especificados.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "O sobrenome é obrigatório.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "A senha não é obrigatória.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "Não é possível converter o usuário de e-mail.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "Senha não coincide com a confirmação.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Especificado usuário de e-mail incorreto.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Junte-se a nós na nossa conta Bitrix24, onde todos podem colaborar em projetos, ter uma comunicação ideal, coordenar tarefas, gerenciar clientes e mais recursos.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "O número de convidados excedem as os termos da licença.";
$MESS["BX24_INVITE_DIALOG_PHONE_ERROR"] = "Estes números de telefone estão incorretos:<br/>";
$MESS["BX24_INVITE_DIALOG_PHONE_LIMIT_EXCEEDED"] = "Muitos números de telefone adicionados ao convite.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR1"] = "Já existe um usuário com o e-mail #EMAIL#.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR2"] = "Já existem usuários com os e-mails #EMAIL_LIST#.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "O ID do usuário está incorreto.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR1"] = "Já existe um usuário com o número de telefone #PHONE#.";
$MESS["BX24_INVITE_DIALOG_USER_PHONE_EXIST_ERROR2"] = "Já existem usuários com os números de telefone #PHONE_LIST#.";
$MESS["BX24_INVITE_TITLE_ADD"] = "Adicionar colaborador";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Convidar colaboradores";
$MESS["BX24_LOADING"] = "Carregando...";
?>