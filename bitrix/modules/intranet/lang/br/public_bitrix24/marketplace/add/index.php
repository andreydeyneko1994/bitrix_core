<?php
$MESS["MARKETPLACE_BLOCK1_INFO"] = "<p>- criar um aplicativo</p> 
<p>- adicioná-lo ao seu portal</p>";
$MESS["MARKETPLACE_BLOCK1_TITLE"] = "Somente minha conta";
$MESS["MARKETPLACE_BLOCK2_INFO"] = "<p>- torne-se um parceiro</p> 
<p>- criar um aplicativo</p> 
<p>- carregar o app em seu perfil de parceiro</p> 
<p>- publicar seu aplicativo</p>";
$MESS["MARKETPLACE_BLOCK2_TITLE"] = "Listar no Marketplace";
$MESS["MARKETPLACE_BUTTON"] = "Continuar";
$MESS["MARKETPLACE_BUTTON_ADD"] = "Adicionar";
$MESS["MARKETPLACE_OR"] = "ou";
$MESS["MARKETPLACE_PAGE_TITLE"] = "Como você adiciona seu aplicativo ao Bitrix24?";
$MESS["MARKETPLACE_PAGE_TITLE2"] = "Можна самостійно або замовити розробку у наших <a target='_blank' href=\"https://www.1c-bitrix.ua/partners/list.php?project_type=&level=&activ=&in_qc=&competence%5B%5D=b24&country%5B%5D=20\">партнерів</a>.";
$MESS["MARKETPLACE_TITLE"] = "Adicionar Aplicativo";
