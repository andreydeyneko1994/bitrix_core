<?php
$MESS["MENU_BLOG"] = "Conversas";
$MESS["MENU_CONTACT"] = "Contatos";
$MESS["MENU_EMPLOYEE"] = "Funcionários";
$MESS["MENU_FILES"] = "Arquivos";
$MESS["MENU_GROUPS"] = "Grupos de Trabalho";
$MESS["MENU_GROUPS_EXTRANET_ALL"] = "Todos os Grupos de Trabalho";
$MESS["MENU_LIVE_FEED"] = "Feed";
$MESS["MENU_LIVE_FEED2"] = "Feed";
$MESS["MENU_TASKS"] = "Tarefas";
