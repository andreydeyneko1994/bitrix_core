<?
$MESS["TARIFF_RESTRICTION_TEXT"] = "O gerenciamento de horas trabalhadas é uma ferramenta essencial para qualquer supervisor. Ver quando cada funcionário começou e terminou o seu dia de trabalho, quantas horas eles trabalharam, no que trabalharam e se alguém chegou tarde ou saiu mais cedo. O relógio digital de marcação de ponto Bitrix24 funciona em qualquer dispositivo e fornece um relatório detalhado de horas trabalhadas para cada funcionário, no local ou remoto. <a href=\"javascript:void(0)\" onclick='top.BX.Helper.show(\"redirect=detail&code=1429531\");'>Informações</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "O gerenciamento de horas trabalhadas está disponível somente no plano Profissional.";
$MESS["TITLE"] = "Relatório de Tempo de Trabalho";
?>