<?
$MESS["TARIFF_RESTRICTION_TEXT"] = "O Bitrix24 pode ajudá-lo a planejar e organizar eventos e reuniões com um organizador especial de reuniões e briefings. As tarefas serão atribuídas a pessoas designadas, a discussão e os procedimentos serão gravados, as pessoas responsáveis serão solicitadas a criar um relatório para seus respectivos itens da agenda do dia. <a href=\"javascript:void(0)\" onclick='top.BX.Helper.show(\"redirect=detail&code=1436032\");'>Informações</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Reuniões e briefings estão disponíveis somente no plano Profissional.";
$MESS["TITLE"] = "Reuniões e Briefings";
?>