<?php
$MESS["MENU_ABSENCE"] = "Gráfico de Ausência";
$MESS["MENU_MEETING"] = "Reuniões e Briefings";
$MESS["MENU_MONITOR_REPORT"] = "Assistente pessoal de horário de trabalho";
$MESS["MENU_SCHEDULES"] = "Agendas de Trabalho";
$MESS["MENU_TIMEMAN"] = "Horas Trabalhadas";
$MESS["MENU_WORKTIME_SETTINGS_PERMISSIONS"] = "Permissões de Acesso";
$MESS["MENU_WORKTIME_STATS"] = "Horas Trabalhadas";
$MESS["MENU_WORK_REPORT"] = "Relatórios de Trabalho";
