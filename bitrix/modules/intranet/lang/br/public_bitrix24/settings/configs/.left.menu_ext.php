<?php
$MESS["MENU_ADMIN_PANEL"] = "Painel de controle da loja on-line";
$MESS["MENU_CONFIGS"] = "Configurações de Conta";
$MESS["MENU_EVENT_LOG"] = "Log de Eventos";
$MESS["MENU_MAIL_BLACKLIST"] = "Contatos bloqueados";
$MESS["MENU_MAIL_MANAGE"] = "Gerenciar Contas de E-mail";
$MESS["MENU_UPDATE_DESC"] = "Histórico de versões";
$MESS["MENU_VM"] = "Solução de Virtualização";
