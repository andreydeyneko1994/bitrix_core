<?
$MESS["BP"] = "Processos de Negócios";
$MESS["CONFIG"] = "Configurações";
$MESS["CURRENCY"] = "Moedas";
$MESS["EXCH1C"] = "Integração 1C";
$MESS["EXTERNAL_SALE"] = "Lojas On-line";
$MESS["FIELDS"] = "Campos Personalizados";
$MESS["LOCATIONS"] = "Endereços";
$MESS["MAIL_TEMPLATES"] = "Modelos de E-mail";
$MESS["MEASURE"] = "Unidades de Medida";
$MESS["PERMS"] = "Permissões de Acesso";
$MESS["PRODUCT_PROPS"] = "Características do Produto";
$MESS["PS"] = "Pagamentos e Faturas";
$MESS["SENDSAVE"] = "Enviar e Salvar Integração";
$MESS["SLOT"] = "Relatórios Analíticos";
$MESS["STATUS"] = "Listas de Seleção";
$MESS["TAX"] = "Impostos";
$MESS["TITLE"] = "Configurações";
?>