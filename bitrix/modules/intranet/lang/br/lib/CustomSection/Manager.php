<?php
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_COMPONENT_NOT_FOUND"] = "Não é possível exibir a página da seção atual";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_INVALID_URL"] = "O formato do link está incorreto";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_AVAILABLE"] = "A seção não tem páginas exibíveis";
$MESS["INTRANET_CUSTOM_SECTION_MANAGER_SECTION_NOT_FOUND"] = "A seção não foi encontrada";
