<?php
$MESS["MAIN_UI_SELECTOR_DEPARTMENT_FLAT_PATTERN"] = "#NAME# (sem subdepartamentos)";
$MESS["MAIN_UI_SELECTOR_EXTRANET"] = "Extranet";
$MESS["MAIN_UI_SELECTOR_SELECT_FLAT_TEXT_DEPARTMENTS"] = "Todos os funcionários do departamento";
$MESS["MAIN_UI_SELECTOR_SELECT_TEXT_DEPARTMENTS"] = "Todos os funcionários do departamento e subdepartamento";
$MESS["MAIN_UI_SELECTOR_TAB_DEPARTMENTS"] = "Colaboradores e departamentos";
$MESS["MAIN_UI_SELECTOR_TITLE_DEPARTMENTS"] = "Departamentos:";
