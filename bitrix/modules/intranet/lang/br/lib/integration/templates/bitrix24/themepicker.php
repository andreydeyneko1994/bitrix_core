<?
$MESS["INTRANET_B24_INTEGRATION_CANT_CREATE_THEME"] = "Não é possível criar um tema usando os dados fornecidos";
$MESS["INTRANET_B24_INTEGRATION_THEMES_LIMIT_EXCEEDED"] = "Número máximo de temas personalizados excedido (#NUM#).";
$MESS["INTRANET_B24_INTEGRATION_UPLOAD_ERROR"] = "Erro no carregamento de arquivo.";
?>