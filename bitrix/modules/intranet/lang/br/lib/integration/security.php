<?php
$MESS["intranet_otp_push_code"] = "Seu código para login: #CODE#";
$MESS["intranet_push_otp_notification"] = "Houve uma tentativa de entrar no seu Bitrix24.[br][br]Código de confirmação: [b]#CODE#[/b].[br][br]Nunca mostre esse código a terceiros nem insira-o em qualquer outro site. Se você não solicitou esse código, altere a senha imediatamente.";
$MESS["intranet_push_otp_notification1"] = "Detectamos uma tentativa de logon em seu Bitrix24.[br][br]Código de confirmação de logon: [b]#CODE#[/b]. Nunca passe este código a terceiros nem o digite em outros sites.[br][br]Se foi você quem solicitou o código, você não precisa se preocupar com nada. Caso contrário, altere sua senha imediatamente nas Configurações.[br][br]Endereço IP: #IP#[br]Dispositivo: #USER_AGENT#";
