<?php
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_NOT_FOUND"] = "O tema que você selecionou não foi encontrado no seu Bitrix24.";
$MESS["INTRANET_CONFIGURATION_THEME_ERROR_SET_DEFAULT"] = "Não foi possível tornar padrão o tema importado.";
