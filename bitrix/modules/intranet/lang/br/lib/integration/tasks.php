<?php
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "Instale o aplicativo Bitrix24 no seu celular e mantenha-se conectado!

Instale o aplicativo desktop no seu computador (Mac, Windows ou Linux) para se comunicar com seus colegas quando o seu navegador estiver fechado.

[URL = https: //www.bitrix24.com/features/mobile.php]Baixar aplicativos Bitrix24[/URL]
";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "Baixar o aplicativo Bitrix24";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "Convidar colegas para o seu Bitrix24.

#ANCHOR_INVITE#Convidar#ANCHOR_END#
";
$MESS["SONET_INVITE_TASK_DESCRIPTION_V2"] = "Convidar colegas para o seu Bitrix24.

[URL=/company/vis_structure.php]Convidar[/URL]
";
$MESS["SONET_INVITE_TASK_TITLE"] = "Convidar colegas";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "Complete seu perfil, coloque sua foto e forneça suas informações pessoais.

#ANCHOR_EDIT_PROFILE#Acesse o seu perfil#ANCHOR_END#
";
$MESS["SONET_TASK_TITLE"] = "Preencha perfil";
