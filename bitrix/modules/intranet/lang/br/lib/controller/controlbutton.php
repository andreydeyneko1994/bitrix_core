<?php
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_FIRST_MESSAGE"] = "Bate-papo criado com base no evento \"#EVENT_TITLE#\" de #DATETIME_FROM#";
$MESS["INTRANET_CONTROL_BUTTON_CALENDAR_CHAT_TITLE"] = "Bate-papo \"#EVENT_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_CREATE_CHAT_LOCK_ERROR"] = "Erro ao criar o bate-papo. Por favor, tente novamente.";
$MESS["INTRANET_CONTROL_BUTTON_DELETE_TASK_FILE_ERROR"] = "Erro ao excluir arquivo.";
$MESS["INTRANET_CONTROL_BUTTON_ENTITY_ERROR"] = "O item não foi encontrado.";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_CALENDAR"] = "Criado com base em [url=#LINK#]evento do calendário[/url]";
$MESS["INTRANET_CONTROL_BUTTON_POST_MESSAGE_TASK"] = "Criado com base em [url=#LINK#]tarefa[/url]";
$MESS["INTRANET_CONTROL_BUTTON_TASK_CHAT_FIRST_MESSAGE"] = "Bate-papo criado com base na tarefa \"#TASK_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_LIMIT"] = "Excedido o número máximo de usuários de chamadas de vídeo";
$MESS["INTRANET_CONTROL_BUTTON_VIDEOCALL_SELF_ERROR"] = "Uma chamada de vídeo requer dois ou mais participantes. Adicione mais usuários e ligue novamente.";
$MESS["INTRANET_CONTROL_MEETING_CREATED"] = "Evento criado com base na tarefa";
