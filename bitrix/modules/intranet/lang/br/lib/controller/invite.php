<?php
$MESS["INTRANET_CONTROLLER_INVITE_DELETE_FAILED"] = "Erro ao excluir o convite";
$MESS["INTRANET_CONTROLLER_INVITE_FAILED"] = "Erro ao criar o convite";
$MESS["INTRANET_CONTROLLER_INVITE_NO_PERMISSIONS"] = "Você não tem permissão para convidar usuários.";
$MESS["INTRANET_CONTROLLER_INVITE_NO_USER_ID"] = "O parâmetro userId está faltando";
$MESS["INTRANET_CONTROLLER_INVITE_USER_NOT_FOUND"] = "O usuário com o ID especificado não foi encontrado.";
