<?php
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_EMPTY_SUBJECT"] = "Mensagem de e-mail #MESSAGE_ID#";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_FIRST_MESSAGE"] = "Bate-papo criado a partir da mensagem de e-mail \"#MAIL_TITLE#\"";
$MESS["INTRANET_CONTROL_BUTTON_MAIL_CHAT_TITLE"] = "Bate-papo criado a partir da mensagem de e-mail \"#MAIL_TITLE#\"";
