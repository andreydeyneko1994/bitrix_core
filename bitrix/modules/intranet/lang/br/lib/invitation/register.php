<?php
$MESS["INTRANET_INVITATION_EMAIL_ERROR"] = "Os endereços de e-mail estão incorretos:";
$MESS["INTRANET_INVITATION_EMAIL_LIMIT_EXCEEDED"] = "Número máximo de endereços de e-mail no convite excedido.";
$MESS["INTRANET_INVITATION_ERROR_USER_TRANSFER"] = "Não é possível converter o usuário de e-mail.";
$MESS["INTRANET_INVITATION_INVITE_MESSAGE_TEXT"] = "Junte-se ao nosso Bitrix24 corporativo! É aqui que colaboramos, administramos projetos, tarefas e documentos, planejamos eventos e reuniões, nos comunicamos e nos envolvemos em outras atividades.";
$MESS["INTRANET_INVITATION_MAX_COUNT_ERROR"] = "O número de pessoas convidadas excede os termos de licença.";
$MESS["INTRANET_INVITATION_PHONE_ERROR"] = "Os números de telefone estão incorretos:";
$MESS["INTRANET_INVITATION_PHONE_LIMIT_EXCEEDED"] = "Muitos números de telefone adicionados ao convite.";
$MESS["INTRANET_INVITATION_USER_EXIST_ERROR"] = "Já existem usuários com estes endereços de e-mail: #EMAIL_LIST#";
$MESS["INTRANET_INVITATION_USER_PHONE_EXIST_ERROR"] = "Já existem usuários com estes números de telefone: #PHONE_LIST#";
