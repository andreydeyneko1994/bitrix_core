<?php
$MESS["WS_OUTLOOK_APP_COMMENT"] = "Gerado automaticamente";
$MESS["WS_OUTLOOK_APP_DESC"] = "Obtenha uma senha para o Microsoft Outlook para definir sincronização do seu calendário, contatos e tarefas.";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "Conectar";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT"] = "Sincronizar com Microsoft Outlook";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT_TYPE"] = "Sincronizar com Microsoft Outlook: #TYPE#";
$MESS["WS_OUTLOOK_APP_TITLE"] = "Microsoft Outlook Services";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "Contatos, calendários, tarefas";
$MESS["WS_OUTLOOK_APP_TYPE_calendar"] = "calendário";
$MESS["WS_OUTLOOK_APP_TYPE_contacts"] = "contatos";
$MESS["WS_OUTLOOK_APP_TYPE_tasks"] = "tarefas";
