<?
$MESS["ABOUT_INFO"] = "<p><b>Endereços de E-mail:</b></p>

<table border='0' width='100%'>
  <tbody>
    <tr> 		<td width='20%'>Informações gerais:</td> 		<td>info@company.com</td> 	</tr>

    <tr> 		<td>Vendas:</td> 		<td>sales@company.com</td> 	</tr>

    <tr> 		<td>Suporte técnico:</td> 		<td>support@company.com</td> 	</tr>
   </tbody>
</table>

<p><b>Números de telefone:</b></p>

<table border='0' width='100%'>
  <tbody>
    <tr> 		<td width='20%'>Principal:</td> 		<td>+1 (555) 432-2920</td> 	</tr>

    <tr> 		<td>Linha de vendas gratuito:</td> 		<td>800-555-0685</td> 	</tr>

    <tr> 		<td>Vendas internacionais:</td> 		<td>+1 (555) 432-2920</td> 	</tr>

    <tr> 		<td>Suporte técnico:</td> 		<td>+1 (555) 432-2920</td> 	</tr>

    <tr> 		<td>Fax:</td> 		<td>+1 (555) 432-2857</td> 	</tr>
   </tbody>
</table>

<p><b>Empresa</b> é uma California Corporation.</p>

<p><b>Sede Corporativa:</b></p>
 <dl> <dd><b>Empresa</b>, Inc.</dd><dt>
    <br />
  </dt> <dd>*** Pine Avenue,</dd><dt>
    <br />
  </dt> <dd>Long Beach, CA 90802</dd><dt>
    <br />
  </dt> <dd>EUA</dd><dt>
    <br />
  </dt> </dl>
  
<p><b>Horários:</b></p>
<p>Segunda-feira - Sexta-feira (fechado durante a maior parte dos principais feriados), 08:00-17:00 GMT.</p>

<p><b>Veja o esquema de como chegar ao escritório:</b></p>
<p><img height='449' width='500' src='#SITE#images/en/company/about/address_1.png' /></p>


<p><b>Minnesota Escritório:</b></p>
 <dl> <dd>*** Gilbert Building</dd><dt>
    <br />
  </dt> <dd>*** Wacouta Rua</dd><dt>
    <br />
  </dt> <dd>St. Paul, MN 55101</dd><dt>
    <br />
  </dt> <dd>EUA</dd><dt>
    <br />
  </dt> </dl>
<p><b>Veja o esquema de como chegar ao escritório:</b></p>

<p><img height='418' width='490' src='#SITE#images/en/company/about/address_2.png' /></p>
";
$MESS["ABOUT_TITLE"] = "Contatos";
?>