<?
$MESS["ABOUT_MENU_ABOUT"] = "Sobre a Empresa";
$MESS["ABOUT_MENU_CALENDAR"] = "Calendário de Eventos";
$MESS["ABOUT_MENU_CAREER"] = "Carreira";
$MESS["ABOUT_MENU_LIFE"] = "Notícias";
$MESS["ABOUT_MENU_NEWS"] = "Notícias Externas (RSS)";
$MESS["ABOUT_MENU_OFFICIAL"] = "Informação Oficial";
$MESS["ABOUT_MENU_PHOTO"] = "Galeria de Fotos";
$MESS["ABOUT_MENU_VIDEO"] = "Vídeos";
?>