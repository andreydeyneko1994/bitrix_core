<?
$MESS["CRM_MENU_BP"] = "Processos de Negócio (BPM)";
$MESS["CRM_MENU_CONFIG"] = "Outras Configurações";
$MESS["CRM_MENU_CURRENCY"] = "Moedas";
$MESS["CRM_MENU_DEAL_CATEGORY"] = "Pipelines de negócios";
$MESS["CRM_MENU_FIELDS"] = "Campos Personalizados";
$MESS["CRM_MENU_INFO"] = "Ajuda";
$MESS["CRM_MENU_LOCATIONS"] = "Endereços";
$MESS["CRM_MENU_MAILTEMPLATE"] = "Modelos de E-mail ";
$MESS["CRM_MENU_MEASURE"] = "Unidades de Medida";
$MESS["CRM_MENU_PERMS"] = "Permissões de Acesso";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Características do Produto";
$MESS["CRM_MENU_PS"] = "Meios de Pagamento";
$MESS["CRM_MENU_SALE"] = "Lojas On-line";
$MESS["CRM_MENU_SENDSAVE"] = "Integração com E-mail";
$MESS["CRM_MENU_SLOT"] = "Relatórios Analíticos";
$MESS["CRM_MENU_STATUS"] = "Listas de Seleção";
$MESS["CRM_MENU_TAX"] = "Taxas";
$MESS["CRM_TITLE"] = "Configurações";
?>