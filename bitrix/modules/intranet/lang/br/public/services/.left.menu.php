<?
$MESS["SERVICES_MENU_BOARD"] = "Anúncios";
$MESS["SERVICES_MENU_BP"] = "Processos de Negócio";
$MESS["SERVICES_MENU_CONTACT_CENTER"] = "Contact Center";
$MESS["SERVICES_MENU_EVENTLIST"] = "Registro de Alterações";
$MESS["SERVICES_MENU_FAQ"] = "FAQ";
$MESS["SERVICES_MENU_IDEA"] = "Ideias ";
$MESS["SERVICES_MENU_LEARNING"] = "Treinamento";
$MESS["SERVICES_MENU_LINKS"] = "Diretório de Links";
$MESS["SERVICES_MENU_LISTS"] = "Listas";
$MESS["SERVICES_MENU_MEETING"] = "Reuniões e Briefings";
$MESS["SERVICES_MENU_MEETING_ROOM"] = "Reserva da Sala de Reuniões";
$MESS["SERVICES_MENU_OPENLINES"] = "Canais Abertos";
$MESS["SERVICES_MENU_PROCESSES"] = "Processos";
$MESS["SERVICES_MENU_REQUESTS"] = "Ordens de Serviço";
$MESS["SERVICES_MENU_SUBSCR"] = "Inscrição";
$MESS["SERVICES_MENU_SUPPORT"] = "Suporte Técnico";
$MESS["SERVICES_MENU_TELEPHONY"] = "Telefonia";
$MESS["SERVICES_MENU_VOTE"] = "Enquetes";
$MESS["SERVICES_MENU_WIKI"] = "Wiki";
?>