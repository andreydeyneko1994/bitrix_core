<?php
$MESS["COMPANY_MENU_ABSENCE"] = "Gráfico de Ausência";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Aniversários ";
$MESS["COMPANY_MENU_CONFERENCES"] = "Reuniões online";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Procurar Colaborador";
$MESS["COMPANY_MENU_EMPLOYEE_LIST"] = "Funcionários";
$MESS["COMPANY_MENU_EVENTS"] = "Mudança de Equipe";
$MESS["COMPANY_MENU_GALLERY"] = "Imagens Compartilhadas";
$MESS["COMPANY_MENU_KNOWLEDGE_BASE"] = "Base de conhecimento";
$MESS["COMPANY_MENU_LEADERS"] = "Colaboradores Honrados ";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Meus Pedidos";
$MESS["COMPANY_MENU_REPORT"] = "Relatório de Eficiência ";
$MESS["COMPANY_MENU_STRUCTURE"] = "Estrutura da Empresa";
$MESS["COMPANY_MENU_TELEPHONES"] = "Lista Telefónica";
$MESS["COMPANY_MENU_TIMEMAN"] = "Tempo de trabalho";
$MESS["COMPANY_MENU_TIMEMAN_SECTION"] = "Horário e Relatórios";
$MESS["COMPANY_MENU_WORKREPORT"] = "Relatórios de Trabalho";
