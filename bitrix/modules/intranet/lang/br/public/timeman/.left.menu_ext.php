<?php
$MESS["TOP_MENU_ABSENCE"] = "Gráfico de Ausência";
$MESS["TOP_MENU_MEETING"] = "Reuniões e Briefings";
$MESS["TOP_MENU_MONITOR_REPORT"] = "Assistente pessoal de horário de trabalho";
$MESS["TOP_MENU_SETTINGS_PERMISSIONS"] = "Permissões de Acesso";
$MESS["TOP_MENU_TIMEMAN"] = "Horas Trabalhadas";
$MESS["TOP_MENU_WORK_REPORT"] = "Relatórios de Trabalho";
$MESS["TOP_MENU_WORK_SCHEDULES"] = "Agendas de Trabalho";
