<?
$MESS["VI_ERROR_COULD_NOT_CREATE_ACCOUNT"] = "Não foi possível criar uma conta de telefonia no controlador de telefonia";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "Você tem que confirmar seu endereço de e-mail para conectar telefones.";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "Você tem que verificar seu endereço de e-mail";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "O usuário não foi encontrado.";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "O usuário não está registrado no controlador de telefonia";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Os usuários da Extranet não podem usar a telefonia";
$MESS["VI_USER_PASS_ERROR"] = "Senha incorreta";
?>