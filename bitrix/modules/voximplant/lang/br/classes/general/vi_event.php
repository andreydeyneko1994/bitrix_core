<?php
$MESS["ERROR_NUMBER"] = "O número de telefone deve estar no formato internacional. Exemplo: +55 11 932145 5687";
$MESS["ERROR_PERSONAL_MOBILE"] = "O campo \"Mobile\" está incorreto.";
$MESS["ERROR_PERSONAL_PHONE"] = "O campo \"Telefone\" é incorreto.";
$MESS["ERROR_PHONE_INNER_2"] = "Valor incorreto inserido no campo \"número interno\". O número deve estar entre 1 e 999.";
$MESS["ERROR_PHONE_INNER_IN_USAGE"] = "O número do ramal está incorreto. Este número já está em uso por outro usuário ou grupo.";
$MESS["ERROR_WORK_PHONE"] = "O campo \"Telefone do Trabalho\" está incorreto.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Notificações de telefonia";
