<?
$MESS["CALL_ENTITY_ACCESS_URL_FIELD"] = "Gerenciar URL da chamada";
$MESS["CALL_ENTITY_CALLER_ID_FIELD"] = "Chamando número";
$MESS["CALL_ENTITY_CALL_ID_FIELD"] = "ID da chamada";
$MESS["CALL_ENTITY_CONFIG_ID_FIELD"] = "Configurando identificador";
$MESS["CALL_ENTITY_DATE_CREATE_FIELD"] = "Chamada começou em";
$MESS["CALL_ENTITY_ID_FIELD"] = "ID";
$MESS["CALL_ENTITY_PORTAL_USER_ID_FIELD"] = "ID de Usuário do Portal";
$MESS["CALL_ENTITY_SEARCH_ID_FIELD"] = "Pesquisar texto";
$MESS["CALL_ENTITY_STATUS_FIELD"] = "Status";
$MESS["CALL_ENTITY_USER_ID_FIELD"] = "Identificador do usuário";
?>