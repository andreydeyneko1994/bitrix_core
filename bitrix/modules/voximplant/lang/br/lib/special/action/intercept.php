<?
$MESS["VOX_ACTION_INTERCEPT_CALL_NOT_FOUND"] = "Não é possível encontrar uma chamada para interceptar";
$MESS["VOX_ACTION_INTERCEPT_HANGUP_TO_ACCEPT_CALL"] = "Desligue para aceitar a chamada";
$MESS["VOX_ACTION_INTERCEPT_LICENSE_ERROR"] = "A interceptação de chamadas não está disponível em seu plano";
?>