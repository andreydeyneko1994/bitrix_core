<?php
$MESS["SIP_LONG_STATUS_INFORMER_FAILED"] = "Conexão SIP com #PROXY# [b]encerrada[/b].\nLogin: #LOGIN#.\nCódigo de erro: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_LONG_STATUS_INFORMER_FAILED_WITH_LINK"] = "A conexão SIP com #PROXY# foi [b]encerrada[/b].\nLogin: #LOGIN#.\nCódigo de erro: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php? ID=#PHONE_ID#] Certifique-se de que as informações fornecidas estão corretas.[/url]";
$MESS["SIP_LONG_STATUS_INFORMER_RECOVERED"] = "Conexão SIP com #PROXY# [b]estabelecida[/b].\nLogin: #LOGIN#.";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED"] = "Conexão SIP com #PHONE_NAME# [b]encerrada[/b].\nCódigo de erro: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED_WITH_LINK"] = "A conexão SIP com #PHONE_NAME# foi [b]encerrada[/b].\nLogin: #LOGIN#.\nCódigo de erro: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php? ID=#PHONE_ID#] Certifique-se de que as informações fornecidas estão corretas.[/url]";
