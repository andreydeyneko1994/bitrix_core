<?
$MESS["VOX_ACTION_INTERCEPT_CALL_NOT_FOUND"] = "No se puede encontrar una llamada para interceptar";
$MESS["VOX_ACTION_INTERCEPT_HANGUP_TO_ACCEPT_CALL"] = "Cuelgue para aceptar llamada";
$MESS["VOX_ACTION_INTERCEPT_LICENSE_ERROR"] = "La intercepción de llamada no está disponible en su plan.";
?>