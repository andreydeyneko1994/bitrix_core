<?php
$MESS["SIP_LONG_STATUS_INFORMER_FAILED"] = "La conexión SIP a #PROXY# [b]finalizó[/b].\nInicio de sesión: #LOGIN#.\nCódigo de error: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_LONG_STATUS_INFORMER_FAILED_WITH_LINK"] = "La conexión SIP a #PROXY# [b]finalizó[/b].\nInicio de sesión: #LOGIN#.\nCódigo de error: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Asegúrese de que la información que proporcionó es correcta.[/url]";
$MESS["SIP_LONG_STATUS_INFORMER_RECOVERED"] = "La conexión SIP a #PROXY# [b]se estableció[/b].\nInicio de sesión: #LOGIN#.";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED"] = "La conexión SIP a #PHONE_NAME# [b]finalizó[/b].\nCódigo de error: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED_WITH_LINK"] = "La conexión SIP a #PHONE_NAME# [b]finalizó[/b].\nInicio de sesión: #LOGIN#.\nCódigo de error: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Asegúrese de que la información que proporcionó es correcta.[/url]";
