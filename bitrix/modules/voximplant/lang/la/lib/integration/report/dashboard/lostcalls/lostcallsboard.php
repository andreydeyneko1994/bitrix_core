<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "Dinámica de las llamadas perdidas";
$MESS["TELEPHONY_REPORT_LOST_CALLS_CHART_TITLE"] = "Dinámica de las llamadas sin contestar";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TABLE_VIEW"] = "Estadísticas de las llamadas perdidas";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TABLE_VIEW_2"] = "Estadísticas de las llamadas sin contestar";
