<?php
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_ALL_CALLS"] = "Total de llamadas";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_CALLBACK"] = "Devolución";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_CURRENT"] = "Período actual de reporte";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_INCOMING"] = "Entrante";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_MISSED"] = "Perdidas";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_OUTGOING"] = "Saliente";
$MESS["TELEPHONY_REPORT_GRAPH_PERIOD_COMPARE_PREVIOUS"] = "Período anterior de reporte";
