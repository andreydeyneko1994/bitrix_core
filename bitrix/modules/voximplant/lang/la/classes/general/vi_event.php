<?php
$MESS["ERROR_NUMBER"] = "El número de telfono debe estar en formato internacional. Ejemplo: +44 20 1234 5678";
$MESS["ERROR_PERSONAL_MOBILE"] = "El campo \"Mvil\"  no es correcto.";
$MESS["ERROR_PERSONAL_PHONE"] = "El campo \"Telfono\"  no es correcto.";
$MESS["ERROR_PHONE_INNER_2"] = "Valor introducido en el campo \"número interno\" es incorrecto. El número debe estar entre 1 y 9999.";
$MESS["ERROR_PHONE_INNER_IN_USAGE"] = "El número de extensión es incorrecto. Este número ya lo está utilizando otro usuario o grupo.";
$MESS["ERROR_WORK_PHONE"] = "El campo \"Telfono del trabajo\" no es correcto.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Notificaciones telefónicas";
