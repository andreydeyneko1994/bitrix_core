<?
$MESS["VI_ACCOUNT_BALANCE"] = "Bilans";
$MESS["VI_ACCOUNT_DEBUG"] = "Tryb debugowania";
$MESS["VI_ACCOUNT_ERROR"] = "Błąd pozyskiwania danych. Proszę spróbować później.";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Błąd weryfikacji klucza licencji. Proszę sprawdzić wprowadzone dane i spróbować ponownie.";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Został wprowadzony błędny adres publiczny.";
$MESS["VI_ACCOUNT_NAME"] = "Nazwa konta";
$MESS["VI_ACCOUNT_URL"] = "Publiczny adres strony";
$MESS["VI_TAB_SETTINGS"] = "Ustawienia";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Parametry połączenia";
?>