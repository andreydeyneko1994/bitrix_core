<?
$MESS["IVR_LICENSE_POPUP_FOOTER_2"] = "Menu głosowe (IVR) dostępne jest tylko w wybranych planach płatnych.";
$MESS["IVR_LICENSE_POPUP_HEADER_2"] = "Dostępne tylko w planach płatnych.";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "Zagnieżdżone menu";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "Użyj własnych plików audio w w menu głosowym";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "Użyj syntezatora mowy (dostępne są różne głosy, a także możliwość ustawienia prędkości i głośności)";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "Przekaż połączenie pracownikowi, grupie kolejkowej lub na zewnętrzny numer";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "Przekaż połączenie na wybrany numer wewnętrzny";
$MESS["IVR_LICENSE_POPUP_MORE"] = "Dowiedz się więcej";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "Menu głosowe (IVR) rozdzieli rozmowy przychodzące do odpowiednich pracowników lub działów w Twojej firmie. Masz możliwość wyboru różnych opcji, aby stworzyć w pełni funkcjonalne menu głosowe:";
?>