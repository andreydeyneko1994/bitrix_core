<?
$MESS["ADDRESS_VERIFICATION_NOTIFY"] = "Dokumentacja wymagana do wynajęcia numeru telefonu została zweryfikowana.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_ACCEPTED"] = "Teraz możesz wynająć numer telefonu i obsługiwać połączenia w swoim Bitrix24!";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_REJECTED"] = "Powód: #REJECT_REASON#. Proszę kliknij \"Zarządzaj numerami\", aby złożyć dokumentację ponownie.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_ACCEPTED"] = "Dokumentacja została zweryfikowana.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_REJECTED"] = "Niestety twoja dokumentacja została odrzucona.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_ACCEPTED"] = "Zarządzaj numerami";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_REJECTED"] = "Zarządzaj numerami";
?>