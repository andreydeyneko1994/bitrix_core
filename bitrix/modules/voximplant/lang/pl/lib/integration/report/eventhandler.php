<?php
$MESS["TELEPHONY_REPORT_AVERAGE_CALL_TIME_TITLE"] = "Średni czas trwania połączenia";
$MESS["TELEPHONY_REPORT_BATCH_GENERAL_CALL_ANALYSIS_TITLE"] = "Podsumowanie połączenia";
$MESS["TELEPHONY_REPORT_BATCH_GENERAL_CALL_COSTS_TITLE"] = "Koszt połączenia";
$MESS["TELEPHONY_REPORT_BATCH_GENERAL_MANAGERS_EFFICIENCY_TITLE"] = "Wyniki pracowników";
$MESS["TELEPHONY_REPORT_CALL_DURATION_TITLE"] = "Czas trwania połączenia";
$MESS["TELEPHONY_REPORT_CALL_DYNAMICS_TITLE"] = "Statystyka połączeń";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_TITLE"] = "Zaangażowanie pracowników";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TITLE"] = "Nieodebrane połączenia";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TITLE_2"] = "Nieodebrane połączenia";
$MESS["TELEPHONY_REPORT_LOST_CALL_ACTIVITY"] = "Gęstość połączeń";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_TITLE"] = "Reakcja na nieodebrane połączenie";
$MESS["TELEPHONY_REPORT_PERIOD_COMPARE_TITLE"] = "Porównaj okresy";
