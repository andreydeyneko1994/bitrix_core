<?php
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_ALL_CALLS"] = "Łączna liczba połączeń";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_CALL_CALLBACK"] = "Oddzwonienia";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_CALL_INCOMING"] = "Przychodzące";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_CALL_MISSED"] = "Pominięte";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_CALL_OUTGOING"] = "Zewnętrzne";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_CALL_TYPE"] = "Typ połączenia";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_INCOMING_PRESET_TITLE"] = "Przychodzące co miesiąc";
$MESS["TELEPHONY_REPORT_FILTER_PERIOD_COMPARE_PREVIOUS_TIME_PERIOD"] = "Poprzedni okres";
