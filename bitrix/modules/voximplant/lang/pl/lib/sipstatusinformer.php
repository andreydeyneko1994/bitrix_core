<?php
$MESS["SIP_LONG_STATUS_INFORMER_FAILED"] = "Połączenie SIP z #PROXY# zostało [b]zakończone[/b].\nLogin: #LOGIN#.\nKod błędu: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_LONG_STATUS_INFORMER_FAILED_WITH_LINK"] = "Połączenie SIP z #PROXY# zostało [b]zakończone[/b].\nLogin: #LOGIN#.\nKod błędu: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Upewnij się, że podane informacje są prawidłowe.[/url]";
$MESS["SIP_LONG_STATUS_INFORMER_RECOVERED"] = "Połączenie SIP z #PROXY# zostało [b]nawiązane[/b].\nLogin: #LOGIN#.";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED"] = "Połączenie SIP z #PHONE_NAME# zostało [b]zakończone[/b].\nKod błędu: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED_WITH_LINK"] = "Połączenie SIP z #PHONE_NAME# zostało [b]zakończone[/b].\nLogin: #LOGIN#.\nKod błędu: [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Upewnij się, że podane informacje są prawidłowe.[/url]";
