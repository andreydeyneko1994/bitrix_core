<?
$MESS["CALL_ENTITY_ACCESS_URL_FIELD"] = "URL zarządzania połączeniem telefonicznym";
$MESS["CALL_ENTITY_CALLER_ID_FIELD"] = "Dzwoniący numer";
$MESS["CALL_ENTITY_CALL_ID_FIELD"] = "ID połączenia telefonicznego";
$MESS["CALL_ENTITY_CONFIG_ID_FIELD"] = "Ustawienie identyfikatora";
$MESS["CALL_ENTITY_DATE_CREATE_FIELD"] = "Połączenie telefoniczne zaczęte o";
$MESS["CALL_ENTITY_ID_FIELD"] = "ID";
$MESS["CALL_ENTITY_PORTAL_USER_ID_FIELD"] = "ID użytkownika portalu";
$MESS["CALL_ENTITY_SEARCH_ID_FIELD"] = "Wyszukaj string";
$MESS["CALL_ENTITY_STATUS_FIELD"] = "Status";
$MESS["CALL_ENTITY_USER_ID_FIELD"] = "Identyfikator użytkownika";
?>