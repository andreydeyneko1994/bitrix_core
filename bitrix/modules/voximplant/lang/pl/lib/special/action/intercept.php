<?
$MESS["VOX_ACTION_INTERCEPT_CALL_NOT_FOUND"] = "Nie można znaleźć połączenia do przechwycenia";
$MESS["VOX_ACTION_INTERCEPT_HANGUP_TO_ACCEPT_CALL"] = "Rozłącz się, aby odebrać połączenie";
$MESS["VOX_ACTION_INTERCEPT_LICENSE_ERROR"] = "Przechwytywanie połączeń nie jest dostępne w Twoim planie";
?>