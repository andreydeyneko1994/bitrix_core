<?
$MESS["VI_CHECK_IM"] = "The Instant Messenger module is not installed.";
$MESS["VI_CHECK_INTRANET"] = "Proszę zaktualizować moduł Intranetu do wersji 14.5.6.";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "Moduł intranetowy nie jest zainstalowany.";
$MESS["VI_CHECK_MAIN"] = "Proszę zaktualizować system rdzenia (moduł Główny) do wersji 14.9.2.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Został wprowadzony błędny adres publiczny.";
$MESS["VI_CHECK_PULL"] = "Moduł  \"Push and Pull\" nie jest zainstalowany lub moduł nginx-push-stream nie jest skonfigurowany.";
$MESS["VI_INSTALL_TITLE_2"] = "Instalacja modułu telefonnicznego";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Zapewnia obsługę telefonii opartej na chmurze.";
$MESS["VI_MODULE_NAME_2"] = "Telefony";
$MESS["VI_UNINSTALL_TITLE_2"] = "Deinstalacja modułu telefonicznego";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Domyślna grupa kolejkowa";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Dziękujemy za skontaktowanie się z nami. Prosimy o wybranie 0, aby połączyć się z konsultantem.";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Domyślne menu";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrator";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Dyrektor generalny";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Kierownik działu";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Manager";
?>