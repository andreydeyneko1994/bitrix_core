<?
$MESS["VI_PUBLIC_PATH"] = "Publiczny adres strony:";
$MESS["VI_PUBLIC_PATH_DESC"] = "Aby telefonia działa poprawnie musi zostać wprowadzony ważny adres strony.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Jeżeli dostęp do strony z Internetu jest ograniczony, dostęp musi być gwarantowany do pewnych stron. Szczegóły na ten temat można znaleźć w #LINK_START#documentation#LINK_END#.";
?>