<?php
$MESS["ERROR_NUMBER"] = "Numer telefonu musi być w formacie międzynarodowym.  Przykład: +44 20 1234 5678";
$MESS["ERROR_PERSONAL_MOBILE"] = "Pole \"Telefon komórkowy\" jest nieprawidłowe.";
$MESS["ERROR_PERSONAL_PHONE"] = "Pole \"Telefon\" jest nieprawidłowe.";
$MESS["ERROR_PHONE_INNER_2"] = "Nieprawidłowa wartość wprowadzona w polu 'Numer wewnętrzny'. Numer musi zawierać się pomiędzy 1 a 9999.";
$MESS["ERROR_PHONE_INNER_IN_USAGE"] = "Numer wewnętrzny jest nieprawidłowy. Ten numer jest już używany przez innego użytkownika lub grupę.";
$MESS["ERROR_WORK_PHONE"] = "Pole \"Telefon służbowy\" jest nieprawidłowe.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Powiadomienia dotyczące telefonii";
