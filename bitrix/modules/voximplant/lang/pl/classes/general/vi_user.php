<?
$MESS["VI_ERROR_COULD_NOT_CREATE_ACCOUNT"] = "Nie można utworzyć konta telefonicznego na kontrolerze telefonii";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "Aby połączyć telefony, należy potwierdzić adres e-mail.";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "Musisz zweryfikować swój adres e-mail";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "Nie znaleziono użytkownika.";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "Użytkownik nie jest zarejestrowany na kontrolerze telefonii";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Użytkownicy ekstranetu nie mogą korzystać z telefonii";
$MESS["VI_USER_PASS_ERROR"] = "Nieprawidłowe hasło";
?>