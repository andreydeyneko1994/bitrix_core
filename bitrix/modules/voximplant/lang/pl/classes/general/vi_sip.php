<?
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "Nie możesz połączyć więcej niż #NUMBER# obsługiwanych przez chmurę PBX.";
$MESS["VI_SIP_CONFIG_ID_NULL"] = "ID konfiguracji numeru nie jest określone.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "Nie znaleziono konfiguracji.";
$MESS["VI_SIP_DESCRIPTION"] = "Serwer #SIP_SERVER#, nazwa użytkownika #SIP_LOGIN#";
$MESS["VI_SIP_INC_LOGIN_0"] = "Login dla serwera przychodzących połączeń telefonicznych nie jest określony.";
$MESS["VI_SIP_INC_LOGIN_100"] = "Login dla serwera przychodzących połączeń telefonicznych nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "Hasło dla przychodzącego połączenia telefonicznego nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_INC_SERVER_0"] = "Adres serwera przychodzących połączeń telefonicznych nie jest okręlony.";
$MESS["VI_SIP_INC_SERVER_100"] = "Adres dla serwera przychodzących połączeń telefonicznych nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_LOGIN_0"] = "Login serwera nie jest określony.";
$MESS["VI_SIP_LOGIN_100"] = "Login serwerra nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_NEW_PBX"] = "Nowe PBX";
$MESS["VI_SIP_NUMBER_0"] = "Numer telefonu SIP nie jest okreslony.";
$MESS["VI_SIP_NUMBER_ERR"] = "Numer musi zostać okreslony w pełnym formacie międzynarodowym.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Musisz wprowadzić numer telefonu, który będzie wyświetlany dla wychodzących połączeń z twoejgo PBX";
$MESS["VI_SIP_NUMBER_EXISTS"] = "Ten numer telefonu już został zarejestrowany.";
$MESS["VI_SIP_PASSWORD_100"] = "Hasło serwera nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "Określone ID połączenia jest już używane w systemie.";
$MESS["VI_SIP_SERVER_0"] = "Adres serwera nie jest określony.";
$MESS["VI_SIP_SERVER_100"] = "Adres serwera nie może przekroczyć 100 znaków.";
$MESS["VI_SIP_TITLE_EXISTS"] = "Nazwa określonego połączenia jest już używana w systemie.";
$MESS["VI_SIP_TYPE_ERR"] = "Typ PBX nie jest określony.";
?>