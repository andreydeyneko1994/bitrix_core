<?
$MESS["VI_CRM_ACTIVITY_SUBJECT_CALLBACK"] = "Callback na numer #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_INCOMING"] = "Rozmowa przychodząca z numeru #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_OUTGOING"] = "Rozmowa wychodząca do #NUMBER#";
$MESS["VI_CRM_CALLBACK_TITLE"] = "Oddzwonienie";
$MESS["VI_CRM_CALL_CALLBACK"] = "Oddzwonienie";
$MESS["VI_CRM_CALL_CANCEL"] = "Połączenie telefoniczne anulowane";
$MESS["VI_CRM_CALL_DURATION"] = "Czas trwania połączenia telefonicznego: #DURATION#";
$MESS["VI_CRM_CALL_INCOMING"] = "Przychodzące połączenie telefoniczne";
$MESS["VI_CRM_CALL_MISSED"] = "Nieodebrane";
$MESS["VI_CRM_CALL_OUTGOING"] = "Wychodzące połączenie telefoniczne";
$MESS["VI_CRM_CALL_STATUS"] = "Status połączenia telefonicznego:";
$MESS["VI_CRM_CALL_TITLE"] = "Rozmowa telefoniczna";
$MESS["VI_CRM_CALL_TO_PORTAL_NUMBER"] = "Połączenie przekierowane do: #PORTAL_NUMBER#.";
$MESS["VI_WORKTIME_SKIPPED_CALL"] = "Połączenie telefoniczne zostało wykonane poza godzinami pracy.";
?>