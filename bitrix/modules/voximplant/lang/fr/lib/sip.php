<?
$MESS["SIP_CONFIG_ENTITY_APP_ID_FIELD"] = "ID de l'application";
$MESS["SIP_CONFIG_ENTITY_AUTH_USER_FIELD"] = "Se connecter en tant qu'utilisateur";
$MESS["SIP_CONFIG_ENTITY_CONFIG_ID_FIELD"] = "ID de configuration";
$MESS["SIP_CONFIG_ENTITY_ID_FIELD"] = "ID";
$MESS["SIP_CONFIG_ENTITY_INCOMING_LOGIN_FIELD"] = "Identifiant de connexion (entrant)";
$MESS["SIP_CONFIG_ENTITY_INCOMING_PASSWORD_FIELD"] = "Mot de passe (entrant)";
$MESS["SIP_CONFIG_ENTITY_INCOMING_SERVER_FIELD"] = "Adresse du serveur (entrant)";
$MESS["SIP_CONFIG_ENTITY_LOGIN_FIELD"] = "Identifiant de connexion";
$MESS["SIP_CONFIG_ENTITY_OUTBOUND_PROXY_FIELD"] = "Serveur proxy";
$MESS["SIP_CONFIG_ENTITY_PASSWORD_FIELD"] = "Mot de passe";
$MESS["SIP_CONFIG_ENTITY_REG_ID_FIELD"] = "ID d'enregistrement";
$MESS["SIP_CONFIG_ENTITY_SERVER_FIELD"] = "Adresse du serveur";
$MESS["SIP_CONFIG_ENTITY_TYPE_FIELD"] = "Type";
?>