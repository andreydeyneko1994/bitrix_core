<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "Dynamique des appels manqués";
$MESS["TELEPHONY_REPORT_LOST_CALLS_CHART_TITLE"] = "Dynamiques des appels sans réponse";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TABLE_VIEW"] = "Statistiques des appels manqués";
$MESS["TELEPHONY_REPORT_LOST_CALLS_TABLE_VIEW_2"] = "Statistiques des appels sans réponse";
