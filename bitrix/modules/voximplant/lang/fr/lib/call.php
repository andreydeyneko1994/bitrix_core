<?
$MESS["CALL_ENTITY_ACCESS_URL_FIELD"] = "URL de gestion des appels";
$MESS["CALL_ENTITY_CALLER_ID_FIELD"] = "Numéro d'appelant";
$MESS["CALL_ENTITY_CALL_ID_FIELD"] = "ID de l'appel";
$MESS["CALL_ENTITY_CONFIG_ID_FIELD"] = "Créer identifiant";
$MESS["CALL_ENTITY_DATE_CREATE_FIELD"] = "Appel commencé le";
$MESS["CALL_ENTITY_ID_FIELD"] = "ID";
$MESS["CALL_ENTITY_PORTAL_USER_ID_FIELD"] = "ID utilisateur du portail";
$MESS["CALL_ENTITY_SEARCH_ID_FIELD"] = "Rechercher une chaîne";
$MESS["CALL_ENTITY_STATUS_FIELD"] = "Statut";
$MESS["CALL_ENTITY_USER_ID_FIELD"] = "Identifiant de l'utilisateur";
?>