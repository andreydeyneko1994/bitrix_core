<?php
$MESS["SIP_LONG_STATUS_INFORMER_FAILED"] = "Connexion SIP à #PROXY# [b]terminée[/b].\nIdentifiant : #LOGIN#.\nCode d'erreur : [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_LONG_STATUS_INFORMER_FAILED_WITH_LINK"] = "La connexion SIP avec #PROXY# a été [b]terminée[/b].\nIdentifiant : #LOGIN#.\nCode d'erreur : [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Vérifiez que les informations fournies sont correctes.[/url]";
$MESS["SIP_LONG_STATUS_INFORMER_RECOVERED"] = "Connexion SIP avec #PROXY# [b]établie[/b].\nIdentifiant : #LOGIN#.";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED"] = "Connexion SIP à #PHONE_NAME# [b]terminée[/b].\nCode d'erreur : [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].";
$MESS["SIP_SHORT_STATUS_INFORMER_FAILED_WITH_LINK"] = "La connexion SIP avec #PHONE_NAME# a été [b]terminée[/b].\nIdentifiant : #LOGIN#.\nCode d'erreur : [B]#STATUS_CODE#[/B] [B]#ERROR_MESSAGE#[/B].\n[url=/telephony/edit.php?ID=#PHONE_ID#]Vérifiez que les informations fournies sont correctes.[/url]";
