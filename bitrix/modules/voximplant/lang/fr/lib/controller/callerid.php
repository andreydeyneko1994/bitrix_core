<?
$MESS["VOX_CALLER_ID_ALREADY_EXISTS"] = "Ce numéro est déjà connecté à Bitrix24";
$MESS["VOX_CALLER_ID_NOT_FOUND"] = "Le numéro est introuvable";
$MESS["VOX_CALLER_ID_WRONG_NUMBER"] = "Le numéro doit être saisi au format international";
?>