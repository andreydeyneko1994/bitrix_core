<?php
$MESS["ERROR_NUMBER"] = "Le numéro de téléphone doit être indiqué au format international. Exemple : +44 20 1234 5678";
$MESS["ERROR_PERSONAL_MOBILE"] = "Le champ \"Mobile\" est incorrect.";
$MESS["ERROR_PERSONAL_PHONE"] = "Le champ \"Téléphone\" est incorrect.";
$MESS["ERROR_PHONE_INNER_2"] = "Mauvaise valeur saisie dans le champ \"Numéro de poste\". Le nombre doit être compris entre 1 et 9999.";
$MESS["ERROR_PHONE_INNER_IN_USAGE"] = "Le numéro d'extension est incorrect. Ce numéro est déjà utilisé par un autre utilisateur ou groupe.";
$MESS["ERROR_WORK_PHONE"] = "Le champ de \"Téléphone au Travail\" est incorrect.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Notifications de téléphonie";
