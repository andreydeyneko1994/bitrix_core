<?
$MESS["VI_ERROR_COULD_NOT_CREATE_ACCOUNT"] = "Impossible de créer un compte de téléphonie sur le contrôleur de téléphonie";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "Vous devez confirmer votre adresse e-mail pour connecter des téléphones.";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "Vous devez vérifier votre adresse e-mail";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "Utilisateur introuvable.";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "L'utilisateur n'est pas enregistré sur le contrôleur de téléphonie";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Les utilisateurs d'extranet ne peuvent pas utiliser la téléphonie";
$MESS["VI_USER_PASS_ERROR"] = "Mot de passe incorrect";
?>