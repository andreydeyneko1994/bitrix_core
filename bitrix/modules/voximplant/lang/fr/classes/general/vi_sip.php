<?
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "Vous ne pouvez pas vous connecter à plus de #NUMBER# PBX hébergés sur Cloud.";
$MESS["VI_SIP_CONFIG_ID_NULL"] = "L'ID de configuration du numéro est pas spécifié.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "La configuration n'a pas été trouvée.";
$MESS["VI_SIP_DESCRIPTION"] = "Serveur #SIP_SERVER#, nom d'utilisateur #SIP_LOGIN#";
$MESS["VI_SIP_INC_LOGIN_0"] = "L'identifiant de connexion pour le serveur d'appel entrant est pas spécifié.";
$MESS["VI_SIP_INC_LOGIN_100"] = "L'identifiant de connexion au serveur d'appels entrants ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "Le mot de passe pour le serveur d'appel entrant ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_INC_SERVER_0"] = "L'adresse du serveur d'appels entrants n'est pas spécifiée.";
$MESS["VI_SIP_INC_SERVER_100"] = "L'adresse du serveur d'appel entrant ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_LOGIN_0"] = "L'identifiant de connexion n'est pas spécifié.";
$MESS["VI_SIP_LOGIN_100"] = "L'identifiant de connexion au serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_NEW_PBX"] = "Nouveau PBX";
$MESS["VI_SIP_NUMBER_0"] = "Le numéro de téléphone SIP n'est pas spécifié.";
$MESS["VI_SIP_NUMBER_ERR"] = "Le numéro doit être spécifié au format international complet.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Vous devez saisir un numéro de téléphone qui sera affiché pour les appels sortants de votre PBX";
$MESS["VI_SIP_NUMBER_EXISTS"] = "Ce numéro de téléphone a déjà été enregistré.";
$MESS["VI_SIP_PASSWORD_100"] = "Le mot de passe du serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "L'identifiant de connexion spécifié est déjà utilisé dans le système.";
$MESS["VI_SIP_SERVER_0"] = "L'adresse du serveur n'est pas spécifiée.";
$MESS["VI_SIP_SERVER_100"] = "L'adresse du serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_TITLE_EXISTS"] = "Le nom de connexion spécifié est déjà utilisé dans le système.";
$MESS["VI_SIP_TYPE_ERR"] = "Le type de PBX n'est pas spécifié.";
?>