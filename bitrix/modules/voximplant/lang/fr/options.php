<?
$MESS["VI_ACCOUNT_BALANCE"] = "Solde";
$MESS["VI_ACCOUNT_DEBUG"] = "Le mode de débogage";
$MESS["VI_ACCOUNT_ERROR"] = "Erreur lors de la récupération des données. Veuillez réessayer plus tard.";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Erreur lors de la vérification de la clé de licence. Veuillez vérifier votre saisie et réessayer.";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Vous avez saisi une adresse publique invalide.";
$MESS["VI_ACCOUNT_NAME"] = "Nom du compte";
$MESS["VI_ACCOUNT_URL"] = "Adresse publique du site";
$MESS["VI_TAB_SETTINGS"] = "Paramètres";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Paramètres de connexion";
?>