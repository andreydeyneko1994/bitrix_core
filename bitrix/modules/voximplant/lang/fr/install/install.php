<?
$MESS["VI_CHECK_IM"] = "Le module de messagerie instantanée n'est pas installé.";
$MESS["VI_CHECK_INTRANET"] = "Veuillez mettre à jour le module Intranet à la version 14.5.6.";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "Le module Intranet n'a pas été installé.";
$MESS["VI_CHECK_MAIN"] = "Veuillez mettre à jour le noyau du système (le module principal) à la version 14.9.2.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Vous avez saisi une adresse publique invalide.";
$MESS["VI_CHECK_PULL"] = "Le module \"Push and Pull\" n'est pas installé ou le module nginx-push-stream n'est pas configuré.";
$MESS["VI_INSTALL_TITLE_2"] = "Installation du module Téléphonie";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Fournit une assistance téléphonie en dématérialisé.";
$MESS["VI_MODULE_NAME_2"] = "Téléphonie";
$MESS["VI_UNINSTALL_TITLE_2"] = "Désinstallation du module Téléphonie";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Groupe de file d'attente par défaut";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Merci de nous avoir contactés. Veuillez composer le 0 pour être connecté à un opérateur.";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Menu par défaut";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrateur";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Directeur général";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Chef de département";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Manager";
?>