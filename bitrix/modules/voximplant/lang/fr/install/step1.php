<?
$MESS["VI_PUBLIC_PATH"] = "Adresse publique du site : ";
$MESS["VI_PUBLIC_PATH_DESC"] = "Pour que la téléphonie fonctionne correctement, une adresse de site public valide doit être entrée.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Si l'accès au site à partir d'Internet est limité, l'accès à certaines pages doit être accordé. Les détails à ce sujet peuvent être trouvés dans #LINK_START#documentation#LINK_END#.";
?>