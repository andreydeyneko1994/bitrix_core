<?php
$MESS["VOX_JS_COMMON_AGREE"] = "J'accepte";
$MESS["VOX_JS_COMMON_CANCEL"] = "Annuler";
$MESS["VOX_JS_COMMON_CLOSE"] = "Fermer";
$MESS["VOX_JS_COMMON_ERROR"] = "Erreur";
$MESS["VOX_JS_COMMON_OK"] = "OK";
$MESS["VOX_JS_COMMON_TERMS_OF_SERVICE"] = "Conditions d'utilisation";
