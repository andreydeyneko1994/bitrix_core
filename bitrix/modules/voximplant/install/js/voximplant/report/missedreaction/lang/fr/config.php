<?php
$MESS["TELEPHONY_REPORT_MISSED_REACTION_AVG_RESPONSE_TIME"] = "Temps de réponse moyen";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_MISSED"] = "Appels manqués";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_UNANSWERED"] = "Sans réponse pendant 24 heures";
