<?
$MESS["VOX_CALLER_ID_BUTTON_CONFIRM"] = "Confirmar";
$MESS["VOX_CALLER_ID_BUTTON_LINK"] = "Conectar";
$MESS["VOX_CALLER_ID_BUTTON_PROLONG"] = "Extender la conexión";
$MESS["VOX_CALLER_ID_BUTTON_REPEAT_CALL"] = "Llamar de nuevo";
$MESS["VOX_CALLER_ID_ENTER_CODE"] = "Introduzca el código de verificación";
$MESS["VOX_CALLER_ID_HINT_P1"] = "Se hará una llamada automática a este número de teléfono y el código de verificación se proporcionará mediante voz.";
$MESS["VOX_CALLER_ID_HINT_P2"] = "Esta llamada automatizada no es gratuita. Por lo tanto se le cobrará.";
$MESS["VOX_CALLER_ID_HINT_P3"] = "Tiene 10 intentos para introducir correctamente el código de verificación, después de los cuales se desconectará el número especificado.";
$MESS["VOX_CALLER_ID_PROVIDE_INTERNATIONAL_NUMBER"] = "Introduzca el número en formato internacional";
$MESS["VOX_CALLER_ID_TITLE"] = "Conectar un número";
$MESS["VOX_CALLER_ID_UNVERIFIED"] = "Número conectado no confirmado";
$MESS["VOX_CALLER_ID_VERIFIED_UNTIL"] = "Número conectado hasta el #DATE#";
?>