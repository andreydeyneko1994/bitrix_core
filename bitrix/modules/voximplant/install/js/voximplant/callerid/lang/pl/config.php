<?
$MESS["VOX_CALLER_ID_BUTTON_CONFIRM"] = "Potwierdź";
$MESS["VOX_CALLER_ID_BUTTON_LINK"] = "Połącz";
$MESS["VOX_CALLER_ID_BUTTON_PROLONG"] = "Rozszerz połączenie";
$MESS["VOX_CALLER_ID_BUTTON_REPEAT_CALL"] = "Połącz ponownie";
$MESS["VOX_CALLER_ID_ENTER_CODE"] = "Wprowadź kod weryfikacyjny";
$MESS["VOX_CALLER_ID_HINT_P1"] = "Wykonane zostanie teraz automatyczne połączenie telefoniczne z tym numerem, a kod weryfikacyjny zostanie podany głosowo.";
$MESS["VOX_CALLER_ID_HINT_P2"] = "To automatyczne połączenie nie jest bezpłatne. Zostanie naliczona odpowiednia opłata.";
$MESS["VOX_CALLER_ID_HINT_P3"] = "Na wprowadzenie poprawnego kodu weryfikacyjnego masz 10 prób, po których określony numer zostanie odłączony.";
$MESS["VOX_CALLER_ID_PROVIDE_INTERNATIONAL_NUMBER"] = "Wprowadź numer w formacie międzynarodowym";
$MESS["VOX_CALLER_ID_TITLE"] = "Podłącz numer";
$MESS["VOX_CALLER_ID_UNVERIFIED"] = "Podłączony numer nie został potwierdzony";
$MESS["VOX_CALLER_ID_VERIFIED_UNTIL"] = "Numer podłączony do #DATE#";
?>