<?
$MESS["BPVICA_ERROR_AUDIO_FILE"] = "Plik audio nie jest określony.";
$MESS["BPVICA_ERROR_NUMBER"] = "Numer subskrybenta nie jest określony.";
$MESS["BPVICA_ERROR_OUTPUT_NUMBER"] = "Numer telefonu nie jest określony.";
$MESS["BPVICA_ERROR_TEXT"] = "Nie podano tekstu do wypowiedzenia.";
$MESS["BPVICA_INCLUDE_MODULE"] = "Moduł \"Telefonia\" nie jest zainstalowany.";
$MESS["BPVICA_RESULT_FALSE"] = "Nieudane";
$MESS["BPVICA_RESULT_TRUE"] = "Udane";
$MESS["BPVICA_TRACK_SUBSCR"] = "Oczekiwanie na wyniki automatycznego połączenia telefonicznego";
?>