<?
$MESS["VOXIMPLANT_PERM_ACCESS_DENIED"] = "Permisos de acceso insuficientes";
$MESS["VOXIMPLANT_PERM_LICENSING_ERROR"] = "Su licencia no le permite editar los permisos de acceso.";
$MESS["VOXIMPLANT_PERM_UNKNOWN_ACCESS_CODE"] = "(ID de acceso desconocido)";
$MESS["VOXIMPLANT_PERM_UNKNOWN_SAVE_ERROR"] = "Error al guardar los datos";
?>