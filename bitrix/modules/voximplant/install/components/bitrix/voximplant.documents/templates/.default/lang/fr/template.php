<?php
$MESS["VI_DOCS_BODY"] = "Pour certains pays, la loi exige qu'un fournisseur de téléphonie vérifie votre statut de résidence afin d'accorder des numéros de téléphone loués. <br>Vous pouvez être tenu de soumettre les documents correspondants pour pouvoir louer un numéro de téléphone pour toute la durée du bail.";
$MESS["VI_DOCS_BODY_2"] = "Les lois de certains pays exigent qu'une société téléphonique vous demande une documentation légale avant de vous permettre d'utiliser vos numéros loués.<br>Vous devez le faire si vous voulez conserver et utiliser les numéros pendant la durée du bail. Vous pouvez pas utiliser les numéros tant que la documentation requise n'est pas téléversée et approuvée.";
$MESS["VI_DOCS_COUNTRY_RU"] = "Russie";
$MESS["VI_DOCS_SERVICE_ERROR"] = "Erreur lors de l'envoi de la demande au service de téléchargement de documents";
$MESS["VI_DOCS_SERVICE_UPLOAD"] = "Télécharger";
$MESS["VI_DOCS_STATUS"] = "Statut : ";
$MESS["VI_DOCS_TABLE_COMMENT"] = "Commentaire";
$MESS["VI_DOCS_TABLE_LINK"] = "Historique des téléchargements de document";
$MESS["VI_DOCS_TABLE_OWNER"] = "Propriétaire";
$MESS["VI_DOCS_TABLE_STATUS"] = "Vérifier le statut";
$MESS["VI_DOCS_TABLE_TYPE"] = "Entité légale";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "Date de téléchargement";
$MESS["VI_DOCS_TITLE"] = "Télécharger la documentation pour la location de numéros de téléphone";
$MESS["VI_DOCS_UNTIL_DATE"] = "Vous avez jusqu'au #DATE# pour télécharger les documents";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "Une fois la date indiquée passée, les numéros réservés seront déconnectés et les fonds retourneront sur votre compte.<br><br>Les numéros loués resteront actifs jusqu'à la fin de la location.";
$MESS["VI_DOCS_UPDATE_BTN"] = "Télécharger de nouveaux documents";
$MESS["VI_DOCS_UPLOAD_BTN"] = "Télécharger les documents";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "Veuillez noter que les documents que vous vous apprêtez à envoyer seront téléchargés directement sur Voximplant, Inc, et seront traités conformément à la législation du pays correspondant. Bitrix24 ne recueille, ne stocke ni traite aucune donnée associée à ces documents.";
$MESS["VI_DOCS_UPLOAD_WHILE_RENT"] = "Si votre documentation doit être vérifiée pour louer un numéro de téléphone, l'interface utilisateur du numéro loué affichera le formulaire de téléchargement de la documentation";
$MESS["VI_DOCS_WAIT"] = "Téléchargement... Veuillez patienter";
