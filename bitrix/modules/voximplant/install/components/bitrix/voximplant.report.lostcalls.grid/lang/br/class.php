<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "Chamadas perdidas";
$MESS["TELEPHONY_REPORT_LOST_CALLS_COUNT"] = "Chamadas não atendidas";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DATE"] = "Data";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DYNAMICS"] = "Mudança vs. período anterior";
