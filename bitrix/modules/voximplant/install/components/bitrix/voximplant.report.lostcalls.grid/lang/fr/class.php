<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "Appels manqués";
$MESS["TELEPHONY_REPORT_LOST_CALLS_COUNT"] = "Appels sans réponse";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DATE"] = "Date";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DYNAMICS"] = "Différence par rapport à la période précédente";
