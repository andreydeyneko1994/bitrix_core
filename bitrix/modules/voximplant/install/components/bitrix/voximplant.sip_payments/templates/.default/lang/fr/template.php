<?
$MESS["VI_NOTICE_BUTTON_DONE"] = "Terminé";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "Pour accélérer le travail du connecteur SIP, vous devez changer l'adresse du serveur d'appels entrants <br>de <b>incoming.#ACCOUNT_NAME#</b><br>à <b>ip.#ACCOUNT_NAME#</b><br> dans les paramètres de connexion PBX.";
$MESS["VI_SIP_BUTTON"] = "Prolonger";
$MESS["VI_SIP_BUTTON_BUY"] = "Connecter";
$MESS["VI_SIP_CONFIG"] = "Gérer les numéros SIP";
$MESS["VI_SIP_PAID_BEFORE"] = "Le Module expirera le #DATE#";
$MESS["VI_SIP_PAID_FREE"] = "Vous avez #COUNT# minutes gratuites pour configurer et tester votre équipement.";
$MESS["VI_SIP_PAID_NOTICE"] = "Les numéros de téléphone SIP seront déconnectés après l'expiration de l'abonnement de module.";
$MESS["VI_SIP_PAID_NOTICE_2"] = "Après l'expiration des minutes gratuites, les numéros de SIP seront déconnectés jusqu'à ce que vous payez pour l'abonnement mensuel du connecteur SIP.";
$MESS["VI_SIP_TITLE"] = "Connecteur SIP";
?>