<?php
$MESS["VI_CONFIG_ERROR_ACCESS_DENIED"] = "Vous n'avez pas les droits pour modifier les paramètres de téléphonie.";
$MESS["VI_CONFIG_ERROR_EMPTY_NAME"] = "Le nom du groupe de file d'attente est obligatoire";
$MESS["VI_CONFIG_ERROR_MAX_GROUP_COUNT_REACHED"] = "Votre Bitrix24 a atteint le nombre maximal de groupes d'utilisateurs de téléphonie";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_GROUP"] = "Ce numéro d'extension est déjà utilisé par le groupe #NAME#.";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_USER"] = "Ce numéro d'extension est déjà utilisé par #NAME#.";
$MESS["VI_CONFIG_ERROR_PHONE_NUMBER_TOO_LONG"] = "La longueur des numéros d'extension ne doit pas dépasser 4 numéros.";
