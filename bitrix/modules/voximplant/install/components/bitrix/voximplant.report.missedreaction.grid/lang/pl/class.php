<?php
$MESS["TELEPHONY_REPORT_MISSED_REACTION_AVG_RESPONSE_TIME"] = "Średni czas reakcji";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_DYNAMICS"] = "Statystyka połączeń";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_EMPLOYEE"] = "Pracownik";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_MISSED"] = "Nieodebrane połączenia";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_TIME_HOUR_SHORT"] = "godz.";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_TIME_MIN_SHORT"] = "min";
$MESS["TELEPHONY_REPORT_MISSED_REACTION_UNANSWERED"] = "Bez odpowiedzi przez 24 godziny";
