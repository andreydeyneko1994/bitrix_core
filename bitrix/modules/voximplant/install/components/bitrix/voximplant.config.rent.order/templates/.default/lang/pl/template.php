<?php
$MESS["VI_CONFIG_RENT_BUY_CONFIGURE"] = "Konfiguruj numer";
$MESS["VI_CONFIG_RENT_FORM_BTN"] = "Aplikuj o numer telefonu";
$MESS["VI_CONFIG_RENT_FORM_TITLE_KZ"] = "Aby ubiegać się o numer telefonu, podaj swoje dane i dane kontaktowe:";
$MESS["VI_CONFIG_RENT_FORM_TITLE_UA"] = "Aby ubiegać się o numer telefonu, podaj swoje dane i dane kontaktowe w języku ukraińskim.";
$MESS["VI_CONFIG_RENT_GROUP_CONFIGURE"] = "Konfiguruj pulę numerów";
$MESS["VI_CONFIG_RENT_INCLUDE_2"] = "Będziesz mógł cieszyć się:<br>- nielimitowanymi liniami przychodzącymi<br>- nielimitowanymi połączeniami przychodzącymi<br>-
wewnętrznymi numerami dla wszystkich pracowników<br>- zasadami przetwarzania połączeń przychodzących<br>-
powiadomieniami o nieodebranych połączeniach<br>- nagrywaniem rozmów telefonicznych<br>- dziennikiem przychodzących, wychodzących i nieodebranych połączeń<br>- szczegółowymi statystykami dla wszystkich połączeń<br>- zintegrowanym CRM<br>-
możliwością podłączenia standardowych telefonów";
$MESS["VI_CONFIG_RENT_NA"] = "W tej chwili brak dostępnych numerów telefonów do wynajęcia, ale spróbuj wkrótce ponownie, jest to tylko tymczasowy problem.";
$MESS["VI_CONFIG_RENT_ORDER_ALL_FIELD_REQUIRED"] = "Musisz wypełnić wszystkie pola przed wysłaniem aplikacji.";
$MESS["VI_CONFIG_RENT_ORDER_BTN"] = "Złóż aplikację";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_BIN"] = "BIN";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_CONTACT"] = "Osoba do kontaktu";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_EMAIL"] = "E-mail kontaktowy";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_NAME"] = "Nazwa firmy";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_PHONE"] = "Numer kontaktowy";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_REG_CODE"] = "KRS";
$MESS["VI_CONFIG_RENT_ORDER_COMPLETE"] = "Dziękujemy! Twoja aplikacja została złożona. Przedstawiciel firmy telekomunikacyjnej skontaktuje się z tobą w ciągu 3 godzin roboczych.";
$MESS["VI_CONFIG_RENT_ORDER_DESC"] = "Wynajęcie numeru telefonu z Bitrix24 jest proste! Złóż swoje podanie i czekaj na telefon od przedstawiciela firmy telekomunikacyjnej.";
$MESS["VI_CONFIG_RENT_ORDER_ERROR"] = "Błąd";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_BTN"] = "Zamówienie";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CHANGE"] = "Zmień bieżące usługi";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CITY"] = "Inne lokalne numery";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_LINE"] = "Dodatkowe linie telefoniczne";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_NUMBER"] = "Dodatkowe numery";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TITLE"] = "Zamów dodatkowe usługi";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TOLLFREE"] = "Bezpłatny numer";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCEPT"] = "Zaakceptowane";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCOUNT"] = "Konto telefoniczne";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_BLOCKED"] = "Całkowicie zablokowane";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_PARTIAL_BLOCKED"] = "Częściowo zablokowane";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_TERMINATION"] = "Zakończone";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE"] = "Data aplikacji:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE_MODIFY"] = "Data zmiany statusu:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DECLINE"] = "Odrzucone";
$MESS["VI_CONFIG_RENT_ORDER_INFO_IN_PROCESS"] = "Oczekujące";
$MESS["VI_CONFIG_RENT_ORDER_INFO_NA"] = "Skontaktuj się z dostawcą telecom po szczegóły";
$MESS["VI_CONFIG_RENT_ORDER_INFO_OID"] = "Numer konta firmy telekomunikacyjnej:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_STATUS"] = "Status aplikacji:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_1"] = "Podsumowanie aplikacji:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_2"] = "Informacje o koncie:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_WAIT"] = "Podczas przeglądu";
$MESS["VI_CONFIG_RENT_ORDER_ORDER"] = "Zażądaj wynajęcia numerów telefonu";
$MESS["VI_CONFIG_RENT_PHONES"] = "Wynajmujesz następujące numery telefonu:";
$MESS["VI_CONFIG_RENT_PHONE_CONFIGURE"] = "Konfiguruj numer";
