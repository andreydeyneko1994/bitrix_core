<?php
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_COUNT"] = "Łączna liczba połączeń";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_DYNAMICS"] = "Dynamika";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_EMPLOYEE"] = "Pracownik";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_INCOMING"] = "Przychodzące";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_MISSED"] = "Pominięte";
$MESS["TELEPHONY_REPORT_EMPLOYEES_WORKLOAD_OUTGOING"] = "Zewnętrzne";
