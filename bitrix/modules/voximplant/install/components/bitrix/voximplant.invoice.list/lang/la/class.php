<?php
$MESS["VOX_INVOICES_DATE_RANGE"] = "Intervalo de fechas";
$MESS["VOX_INVOICES_DOCUMENT_DATE"] = "Fecha de la factura";
$MESS["VOX_INVOICES_DOCUMENT_NUMBER"] = "#";
$MESS["VOX_INVOICES_DOCUMENT_TYPE"] = "Tipo de documento";
$MESS["VOX_INVOICES_DOWNLOAD"] = "Descargar";
$MESS["VOX_INVOICES_TOTAL_AMOUNT"] = "Importe total";
