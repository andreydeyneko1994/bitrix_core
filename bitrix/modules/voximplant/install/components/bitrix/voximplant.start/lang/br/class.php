<?php
$MESS["VOX_START_5_NUMBER_RENT"] = "Pacote de 5 números";
$MESS["VOX_START_10_NUMBER_RENT"] = "Pacote de 10 números";
$MESS["VOX_START_ACCESS_CONTROL"] = "Permissões de acesso";
$MESS["VOX_START_CALLER_ID"] = "Conectar o próprio número";
$MESS["VOX_START_COMMON_SETTINGS"] = "Configurações de telefonia";
$MESS["VOX_START_CONFIGURE_BLACK_LIST"] = "Contatos bloqueados";
$MESS["VOX_START_CONFIGURE_GROUPS"] = "Grupos de fila";
$MESS["VOX_START_CONFIGURE_IVR"] = "Menu de voz";
$MESS["VOX_START_CONFIGURE_NUMBERS"] = "Configurar números";
$MESS["VOX_START_CONFIGURE_TELEPHONY"] = "Configurar telefonia";
$MESS["VOX_START_CONFIGURE_USERS"] = "Usuários de telefonia";
$MESS["VOX_START_CONTRACTOR_DOCUMENTS"] = "Faturas de telefonia";
$MESS["VOX_START_INTEGRATION_REQUIRED"] = "Precisa de integração?";
$MESS["VOX_START_NUMBER_RENT"] = "Alugar um número";
$MESS["VOX_START_SEE_ALL"] = "Ver tudo";
$MESS["VOX_START_SIP_PBX_CLOUD"] = "PBX em Nuvem";
$MESS["VOX_START_SIP_PBX_OFFICE"] = "Office PBX";
$MESS["VOX_START_SIP_PHONES"] = "Conectar telefone";
$MESS["VOX_START_TOTAL_APPLICATIONS"] = "Total de apps";
$MESS["VOX_START_UPLOAD_DOCUMENTS"] = "Fazer o upload da documentação";
