<?php
$MESS["VOX_START_5_NUMBER_RENT"] = "Pakiet 5 numerów";
$MESS["VOX_START_10_NUMBER_RENT"] = "Pakiet 10 numerów";
$MESS["VOX_START_ACCESS_CONTROL"] = "Uprawnienia dostępu";
$MESS["VOX_START_CALLER_ID"] = "Podłącz własny numer";
$MESS["VOX_START_COMMON_SETTINGS"] = "Ustawienia telefonii";
$MESS["VOX_START_CONFIGURE_BLACK_LIST"] = "Czarna lista";
$MESS["VOX_START_CONFIGURE_GROUPS"] = "Grupy kolejki";
$MESS["VOX_START_CONFIGURE_IVR"] = "Menu głosowe";
$MESS["VOX_START_CONFIGURE_NUMBERS"] = "Konfiguruj numery";
$MESS["VOX_START_CONFIGURE_TELEPHONY"] = "Konfiguruj telefonię";
$MESS["VOX_START_CONFIGURE_USERS"] = "Użytkownicy telefonii";
$MESS["VOX_START_CONTRACTOR_DOCUMENTS"] = "Faktury telefoniczne";
$MESS["VOX_START_INTEGRATION_REQUIRED"] = "Potrzebna jest integracja?";
$MESS["VOX_START_NUMBER_RENT"] = "Wynajmij numer";
$MESS["VOX_START_SEE_ALL"] = "Wyświetl wszystkie";
$MESS["VOX_START_SIP_PBX_CLOUD"] = "PBX w chmurze";
$MESS["VOX_START_SIP_PBX_OFFICE"] = "Biurowa centrala PBX";
$MESS["VOX_START_SIP_PHONES"] = "Podłącz telefon";
$MESS["VOX_START_TOTAL_APPLICATIONS"] = "Łączna liczba aplikacji";
$MESS["VOX_START_UPLOAD_DOCUMENTS"] = "Prześlij dokumentację";
