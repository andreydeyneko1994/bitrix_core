<?
$MESS["VOX_LINES_ADD_NUMBER_GROUP"] = "Utwórz pulę numerów";
$MESS["VOX_LINES_BUNDLE_WILL_BE_DELETED"] = "Pakiet zostanie odłączony w ciągu 24 godzin.";
$MESS["VOX_LINES_BUTTON_CANCEL"] = "Anuluj";
$MESS["VOX_LINES_BUTTON_CREATE"] = "Utwórz";
$MESS["VOX_LINES_CALLERID_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć numer #NUMBER#?";
$MESS["VOX_LINES_CONFIRM_ACTION"] = "Potwierdź działanie";
$MESS["VOX_LINES_CONFIRM_BUNDLE_DISCONNECTION"] = "Pakietu nie można odłączyć częściowo. Czy chcesz odłączyć wszystkie numery zawarte w pakiecie?";
$MESS["VOX_LINES_CREATE_NUMBER_GROUP"] = "Utwórz pulę numerów";
$MESS["VOX_LINES_ERROR"] = "Błąd";
$MESS["VOX_LINES_NUMBER_DELETE_CONFIRM"] = "Czy na pewno chcesz odłączyć numer #NUMBER# od swojego Bitrix24?";
$MESS["VOX_LINES_NUMBER_GROUP_NAME"] = "Nazwa puli numerów";
$MESS["VOX_LINES_NUMBER_RENTED_IN_BUNDLE"] = "Ten numer został wynajęty w pakiecie #COUNT# numerów:";
$MESS["VOX_LINES_NUMBER_WILL_BE_DELETED"] = "Numer zostanie odłączony w ciągu 24 godzin.";
$MESS["VOX_LINES_SELECT_UNASSIGNED_NUMBERS"] = "Wybierz numery, które chcesz dodać do puli numerów";
$MESS["VOX_LINES_SIP_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć to połączenie?";
?>