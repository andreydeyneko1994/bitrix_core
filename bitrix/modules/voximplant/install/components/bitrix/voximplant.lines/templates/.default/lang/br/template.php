<?
$MESS["VOX_LINES_ADD_NUMBER_GROUP"] = "Criar grupo de números";
$MESS["VOX_LINES_BUNDLE_WILL_BE_DELETED"] = "O pacote será desconectado em 24 horas.";
$MESS["VOX_LINES_BUTTON_CANCEL"] = "Cancelar";
$MESS["VOX_LINES_BUTTON_CREATE"] = "Criar";
$MESS["VOX_LINES_CALLERID_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir o número #NUMBER#?";
$MESS["VOX_LINES_CONFIRM_ACTION"] = "Confirmar ação";
$MESS["VOX_LINES_CONFIRM_BUNDLE_DISCONNECTION"] = "O pacote não pode ser parcialmente desconectado. Deseja desconectar todos os números do pacote?";
$MESS["VOX_LINES_CREATE_NUMBER_GROUP"] = "Criar grupo de números";
$MESS["VOX_LINES_ERROR"] = "Erro";
$MESS["VOX_LINES_NUMBER_DELETE_CONFIRM"] = "Você tem certeza que deseja desconectar o número #NUMBER# do seu Bitrix24?";
$MESS["VOX_LINES_NUMBER_GROUP_NAME"] = "Nome do grupo de números";
$MESS["VOX_LINES_NUMBER_RENTED_IN_BUNDLE"] = "Este número foi alugado como um pacote de #COUNT# números:";
$MESS["VOX_LINES_NUMBER_WILL_BE_DELETED"] = "O número será desconectado em 24 horas.";
$MESS["VOX_LINES_SELECT_UNASSIGNED_NUMBERS"] = "Selecionar números para adicionar ao grupo de números";
$MESS["VOX_LINES_SIP_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir esta conexão?";
?>