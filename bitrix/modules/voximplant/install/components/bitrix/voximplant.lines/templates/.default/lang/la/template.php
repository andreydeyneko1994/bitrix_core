<?
$MESS["VOX_LINES_ADD_NUMBER_GROUP"] = "Crear grupo de números";
$MESS["VOX_LINES_BUNDLE_WILL_BE_DELETED"] = "El paquete se desconectará en 24 horas.";
$MESS["VOX_LINES_BUTTON_CANCEL"] = "Cancelar";
$MESS["VOX_LINES_BUTTON_CREATE"] = "Crear";
$MESS["VOX_LINES_CALLERID_DELETE_CONFIRM"] = "¿Seguro que desea eliminar el número #NUMBER#?";
$MESS["VOX_LINES_CONFIRM_ACTION"] = "Confirmar acción";
$MESS["VOX_LINES_CONFIRM_BUNDLE_DISCONNECTION"] = "El paquete no se puede desconectar parcialmente. ¿Quiere desconectar todos los números que hay en el paquete?";
$MESS["VOX_LINES_CREATE_NUMBER_GROUP"] = "Crear grupo de números";
$MESS["VOX_LINES_ERROR"] = "Error";
$MESS["VOX_LINES_NUMBER_DELETE_CONFIRM"] = "¿Seguro que desea desconectar el número #NUMBER# de su Bitrix24?";
$MESS["VOX_LINES_NUMBER_GROUP_NAME"] = "Nombre del grupo de números";
$MESS["VOX_LINES_NUMBER_RENTED_IN_BUNDLE"] = "Este número se alquiló junto con un paquete de #COUNT# números:";
$MESS["VOX_LINES_NUMBER_WILL_BE_DELETED"] = "El número se desconectará en 24 horas.";
$MESS["VOX_LINES_SELECT_UNASSIGNED_NUMBERS"] = "Seleccionar números para agregar al grupo de números";
$MESS["VOX_LINES_SIP_DELETE_CONFIRM"] = "¿Seguro que quiere eliminar esta conexión?";
?>