<?php
$MESS["VOX_LINES_ERROR_EMPTY_NAME"] = "Wprowadź nazwę puli numerów";
$MESS["VOX_LINES_ERROR_GROUP_NOT_FOUND"] = "Nie znaleziono puli numerów";
$MESS["VOX_LINES_ERROR_NAME_EXISTS"] = "Pula numerów lub połączenie SIP o tej nazwie już istnieje";
$MESS["VOX_LINES_ERROR_NO_NUMBERS"] = "Wybierz co najmniej jeden numer";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_FOUND"] = "Nie znaleziono numeru";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_IN_GROUP"] = "Numer nie należy do żadnej znanej puli numerów";
