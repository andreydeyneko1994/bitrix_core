<?php
$MESS["VOX_LINES_ERROR_EMPTY_NAME"] = "Saisissez le nom du pool de numéros";
$MESS["VOX_LINES_ERROR_GROUP_NOT_FOUND"] = "Le pool de numéros est introuvable";
$MESS["VOX_LINES_ERROR_NAME_EXISTS"] = "Un pool de numéros ou une connexion SIP utilise déjà ce nom";
$MESS["VOX_LINES_ERROR_NO_NUMBERS"] = "Sélectionnez au moins un numéro";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_FOUND"] = "Le numéro est introuvable";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_IN_GROUP"] = "Le numéro n'appartient à aucun pool de numéros connu";
