<?php
$MESS["VOX_LINES_ERROR_EMPTY_NAME"] = "Digite o nome do grupo de números";
$MESS["VOX_LINES_ERROR_GROUP_NOT_FOUND"] = "O grupo de números não foi encontrado";
$MESS["VOX_LINES_ERROR_NAME_EXISTS"] = "Já existe um grupo de números ou conexão SIP com este nome";
$MESS["VOX_LINES_ERROR_NO_NUMBERS"] = "Selecione pelo menos um número";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_FOUND"] = "O número não foi encontrado";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_IN_GROUP"] = "O número não pertence a nenhum grupo de números conhecidos";
