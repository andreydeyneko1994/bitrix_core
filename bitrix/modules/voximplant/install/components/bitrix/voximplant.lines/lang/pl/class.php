<?php
$MESS["VOX_LINES_ACTION_ADD_TO_GROUP"] = "Dodaj do puli numerów";
$MESS["VOX_LINES_ACTION_CONFIGURE"] = "Konfiguruj";
$MESS["VOX_LINES_ACTION_DELETE"] = "Usuń";
$MESS["VOX_LINES_ACTION_PROLONG_CALLER_ID"] = "Wydłuż podłączony numer";
$MESS["VOX_LINES_ACTION_REMOVE_FROM_GROUP"] = "Usuń z puli numerów";
$MESS["VOX_LINES_ACTION_UNGROUP"] = "Rozwiąż pulę numerów";
$MESS["VOX_LINES_ACTION_VERIFY_CALLER_ID"] = "Potwierdź numer";
$MESS["VOX_LINES_CANCEL_NUMBER_DISCONNECT"] = "Podłącz ponownie odłączony numer";
$MESS["VOX_LINES_DESCRIPTION_GROUP"] = "Łączna miesięczna płatność #PRICE#";
$MESS["VOX_LINES_DISCONNECT_NUMBER"] = "Odłącz numer";
$MESS["VOX_LINES_HEADER_DESCRIPTION"] = "Opis";
$MESS["VOX_LINES_HEADER_ID"] = "ID";
$MESS["VOX_LINES_HEADER_NAME"] = "Nazwa";
$MESS["VOX_LINES_HEADER_STATE"] = "Status połączenia";
$MESS["VOX_LINES_HEADER_TYPE"] = "Typ";
$MESS["VOX_LINES_MODE_GROUP"] = "Pula numerów";
$MESS["VOX_LINES_MODE_LINK"] = "Podłączono";
$MESS["VOX_LINES_MODE_RENT"] = "Wynajęto";
$MESS["VOX_LINES_MODE_SIP"] = "Połączenie SIP";
$MESS["VOX_LINES_REMOVE_CONNECTION"] = "Usuń połączenie";
$MESS["VOX_LINES_SIP_STATUS_FAIL"] = "Błąd";
$MESS["VOX_LINES_SIP_STATUS_RECOVERED"] = "Połączone";
