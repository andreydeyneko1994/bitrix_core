<?
$MESS["VOX_QUEUE_CLOSE"] = "Fermer";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Erreur lors de la suppression du groupe de files d'attente.";
$MESS["VOX_QUEUE_IS_USED"] = "Ce groupe de files d'attente est actuellement utilisé dans :";
$MESS["VOX_QUEUE_IVR"] = "SVI";
$MESS["VOX_QUEUE_LIST_ADD"] = "Ajouter un groupe de files d'attente";
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Total des groupes de files d'attente";
$MESS["VOX_QUEUE_NUMBER"] = "Numéro";
?>