<?
$MESS["VI_NUMBERS_APPLY"] = "Zastosuj";
$MESS["VI_NUMBERS_CANCEL"] = "Anuluj";
$MESS["VI_NUMBERS_CONFIG"] = "Konfiguruj";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE"] = "Domyślny numer dla połączeń wychodzących";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE_TITLE"] = "Twój rozmówca zobaczy ten numer, gdy będziesz do niego dzwonić wykorzystując do tego Bitrix24";
$MESS["VI_NUMBERS_EDIT"] = "Edytuj";
$MESS["VI_NUMBERS_SAVE"] = "Zapisz";
$MESS["VI_NUMBERS_TITLE_2"] = "Konfiguruj domyślne numery";
?>