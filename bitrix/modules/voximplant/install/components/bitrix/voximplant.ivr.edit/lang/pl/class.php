<?php
$MESS["VOX_IVR_EDIT_ERROR_ACCESS_DENIED"] = "Nie masz uprawnień dostępu do edycji ustawień telefonii";
$MESS["VOX_IVR_EDIT_ERROR_IVR_DEPTH_TOO_LARGE"] = "Ilość poziomów IVR przekroczyła limit.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_NOT_AVAILABLE"] = "Menu głosowe nie jest dostępne w ramach Twojego obecnego abonamentu.";
$MESS["VOX_IVR_FILE_TOO_LARGE"] = "Rozmiar przekracza maksymalną dopuszczalną wartość.";
$MESS["VOX_IVR_FILE_UPLOAD_ERROR"] = "Błąd przesyłania pliku.";
