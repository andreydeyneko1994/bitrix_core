<?php
$MESS["MOBILE_TASKS_LIST_WELCOME_SCREEN_EMPTY_SUBTITLE"] = "Esta vista mostrará las tareas de las que es responsable o delega a sus empleados.";
$MESS["MOBILE_TASKS_LIST_WELCOME_SCREEN_EMPTY_TITLE"] = "Crear una tarea";
$MESS["MOBILE_TASKS_LIST_WELCOME_SCREEN_PRIVATE_PROJECT_SUBTITLE"] = "No puede ver los elementos de este proyecto. Envíe una solicitud al propietario para unirse al proyecto.";
$MESS["MOBILE_TASKS_LIST_WELCOME_SCREEN_PRIVATE_PROJECT_TITLE"] = "Este proyecto es privado";
$MESS["MOBILE_TASKS_VIEW_TAB_CHECKLIST"] = "Lista de verificación";
$MESS["MOBILE_TASKS_VIEW_TAB_COMMENT"] = "Comentarios";
$MESS["MOBILE_TASKS_VIEW_TAB_FILES"] = "Archivos";
$MESS["MOBILE_TASKS_VIEW_TAB_TASK"] = "Tarea";
$MESS["TASKS_CONFIRM_DELETE"] = "¿Mover a la papelera de reciclaje?";
$MESS["TASKS_CONFIRM_DELETE_NO"] = "No";
$MESS["TASKS_CONFIRM_DELETE_YES"] = "Sí";
$MESS["TASKS_COUNTER_EXPIRED"] = "Atrasada";
$MESS["TASKS_COUNTER_EXPIRED_SOON"] = "Casi atrasada";
$MESS["TASKS_COUNTER_NEW_COMMENTS"] = "Nuevos comentarios";
$MESS["TASKS_COUNTER_NOT_VIEWED"] = "No vista";
$MESS["TASKS_COUNTER_WAIT_CTRL"] = "Revisión pendiente";
$MESS["TASKS_COUNTER_WO_DEADLINE"] = "Sin fecha límite";
$MESS["TASKS_ERROR_TITLE"] = "Error";
$MESS["TASKS_LIST_ACTION_COMPLETE"] = "Completado";
$MESS["TASKS_LIST_ACTION_DEADLINE"] = "Fecha límite";
$MESS["TASKS_LIST_ACTION_DONT_FOLLOW"] = "Dejar de seguir";
$MESS["TASKS_LIST_ACTION_REMOVE"] = "Eliminar";
$MESS["TASKS_LIST_ACTION_REOPEN"] = "Reanudar";
$MESS["TASKS_LIST_ACTION_RESPONSIBLE"] = "Asignar";
$MESS["TASKS_LIST_ACTION_STATE_PAUSE"] = "Detener";
$MESS["TASKS_LIST_ACTION_STATE_START"] = "Iniciar";
$MESS["TASKS_LIST_BUTTON_NEXT"] = "Cargar más";
$MESS["TASKS_LIST_BUTTON_NEXT_PROCESS"] = "Cargando";
$MESS["TASKS_LIST_EMPTY_RESULT"] = "No hay elementos";
$MESS["TASKS_LIST_HEADER_DEADLINES"] = "Fechas límite";
$MESS["TASKS_LIST_HEADER_ROLE_ACCOMPLICE"] = "Asistiendo#DEADLINES#";
$MESS["TASKS_LIST_HEADER_ROLE_ALL"] = "Tareas#DEADLINES#";
$MESS["TASKS_LIST_HEADER_ROLE_ALL_V2"] = "Tareas y proyectos#DEADLINES#";
$MESS["TASKS_LIST_HEADER_ROLE_AUDITOR"] = "Siguiendo#DEADLINES#";
$MESS["TASKS_LIST_HEADER_ROLE_ORIGINATOR"] = "Establecidas por mí#DEADLINES#";
$MESS["TASKS_LIST_HEADER_ROLE_RESPONSIBLE"] = "En curso#DEADLINES#";
$MESS["TASKS_LIST_HEADER_TASKS"] = "Tareas";
$MESS["TASKS_LIST_NOTHING_NOT_FOUND"] = "No se encontraron tareas";
$MESS["TASKS_LIST_PING_NOTIFICATION"] = "Se envió un ping";
$MESS["TASKS_LIST_POPUP_PROJECT"] = "Proyecto";
$MESS["TASKS_LIST_POPUP_RESPONSIBLE"] = "Persona responsable";
$MESS["TASKS_LIST_POPUP_SELECT_DATE"] = "Fecha límite";
$MESS["TASKS_LIST_READ_ALL_NOTIFICATION"] = "Comentarios leídos";
$MESS["TASKS_LIST_SEARCH_EMPTY_RESULT"] = "Su búsqueda no devolvió resultados.";
$MESS["TASKS_LIST_SEARCH_HINT"] = "Introduzca el nombre de la tarea, el nombre de la persona responsable o algún otro término de búsqueda";
$MESS["TASKS_LIST_SEARCH_SECTION_LAST"] = "BÚSQUEDA RECIENTE";
$MESS["TASKS_LIST_SEARCH_SECTION_SEARCH_RESULTS"] = "RESULTADOS DE LA BÚSQUEDA";
$MESS["TASKS_LIST_STATE_INPROGRESS"] = "En progreso";
$MESS["TASKS_LIST_SUB_HEADER_DEADLINES"] = " (Fechas límite)";
$MESS["TASKS_NO_INTERNET_CONNECTION"] = "Probablemente no hay conexión a internet. Intente de nuevo más tarde.";
$MESS["TASKS_POPUP_MENU_COUNTER_EXPIRED"] = "Atrasado";
$MESS["TASKS_POPUP_MENU_COUNTER_NEW_COMMENTS"] = "Con nuevos comentarios";
$MESS["TASKS_POPUP_MENU_COUNTER_SUPPOSEDLY_COMPLETED"] = "Revisión pendiente";
$MESS["TASKS_POPUP_MENU_DEADLINE"] = "Fechas límite";
$MESS["TASKS_POPUP_MENU_EFFICIENCY"] = "Eficiencia";
$MESS["TASKS_POPUP_MENU_HIDE_CLOSED_TASKS"] = "Ocultar las completadas";
$MESS["TASKS_POPUP_MENU_HIDE_GROUPS"] = "Ocultar grupos";
$MESS["TASKS_POPUP_MENU_ORDER_ACTIVITY"] = "Ordenar por estado activo";
$MESS["TASKS_POPUP_MENU_ORDER_DEADLINE"] = "Ordenar por fecha límite";
$MESS["TASKS_POPUP_MENU_ORDER_DIRECTION_ASC"] = "Descendente";
$MESS["TASKS_POPUP_MENU_ORDER_DIRECTION_DESC"] = "Ascendente";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD"] = "Ordenar campo";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_CLOSED_DATE"] = "Completada el";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_CREATED_DATE"] = "Creada el";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_DEADLINE"] = "Fecha límite";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_GROUP_ID"] = "Proyecto";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_ID"] = "ID";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_ORIGINATOR_NAME"] = "Creado por";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_PRIORITY"] = "Prioridad";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_REAL_STATUS"] = "Estatus 	";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_RESPONSIBLE_NAME"] = "Persona responsable";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_SORTING"] = "Mi clasificación";
$MESS["TASKS_POPUP_MENU_ORDER_FIELD_TITLE"] = "Título";
$MESS["TASKS_POPUP_MENU_READ_ALL"] = "Marcar todo como leído";
$MESS["TASKS_POPUP_MENU_READ_ALL_V2"] = "Leer todos los comentarios";
$MESS["TASKS_POPUP_MENU_ROLE_ACCOMPLICE"] = "Asistiendo";
$MESS["TASKS_POPUP_MENU_ROLE_ALL"] = "Tareas";
$MESS["TASKS_POPUP_MENU_ROLE_AUDITOR"] = "Siguiendo";
$MESS["TASKS_POPUP_MENU_ROLE_ORIGINATOR"] = "Establecidas por mí";
$MESS["TASKS_POPUP_MENU_ROLE_RESPONSIBLE"] = "En curso";
$MESS["TASKS_POPUP_MENU_SECTION_HEADER_ORDER_DIRECTION"] = "Orden de clasificación";
$MESS["TASKS_POPUP_MENU_SECTION_HEADER_ORDER_FIELD"] = "Ordenar campo";
$MESS["TASKS_POPUP_MENU_SHOW_CLOSED_TASKS"] = "Mostrar las completadas";
$MESS["TASKS_POPUP_MENU_SHOW_GROUPS"] = "Mostrar grupos";
$MESS["TASKS_ROLE_ACCOMPLICE"] = "Asistiendo";
$MESS["TASKS_ROLE_AUDITOR"] = "Siguiendo";
$MESS["TASKS_ROLE_ORIGINATOR"] = "Establecidas por mí";
$MESS["TASKS_ROLE_RESPONSIBLE"] = "En curso";
$MESS["TASKS_ROLE_VIEW_ALL"] = "Todas las tareas";
$MESS["TASKS_SNACKBAR_CANCEL_REMOVE_TASK"] = "Deshacer la eliminación de la tarea";
$MESS["TASKS_SNACKBAR_HIDE_CLOSED_TASKS"] = "Ocultar las tareas completadas";
$MESS["TASKS_SNACKBAR_SHOW_CLOSED_TASKS"] = "Mostrar las tareas completadas";
$MESS["TASKS_SOME_THING_WENT_WRONG"] = "Se produjo un error. Intente de nuevo más tarde.";
$MESS["TASKS_UNEXPECTED_ANSWER"] = "Hay problemas de conexión con el servidor. Intente de nuevo más tarde.";
