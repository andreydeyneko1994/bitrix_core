<?php
$MESS["TASK_RESTRICTED_ADMIN1"] = "<h1>Twój plan ogranicza dostęp do tej funkcji</h1><p>W Twoim planie dostęp do narzędzi biznesowych posiada 24 użytkowników.</p><p>Jeżeli potrzebujesz dostępu do narzędzi biznesowych, skontaktuj się ze swoim administratorem.</p>";
$MESS["TASK_RESTRICTED_ADMIN2"] = "Otwórz pełną wersję";
$MESS["TASK_RESTRICTED_USER1"] = "<h1>Twój plan ogranicza dostęp do tej funkcji</h1><p>W Twoim planie dostęp do narzędzi biznesowych posiada 24 użytkowników.</p><p>Jeżeli potrzebujesz dostępu do narzędzi biznesowych, skontaktuj się ze swoim administratorem.</p>";
$MESS["TASK_RESTRICTED_USER2"] = "Wyślij zapytanie";
$MESS["TASK_RESTRICTED_USER3"] = "Zapytanie zostało wysłane";
