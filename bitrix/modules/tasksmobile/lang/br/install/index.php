<?php
$MESS["TASKSMOBILE_DENIED"] = "Acesso negado";
$MESS["TASKSMOBILE_INSTALL_TITLE"] = "Instalação do Módulo";
$MESS["TASKSMOBILE_MODULE_DESCRIPTION"] = "Este módulo gerencia tarefas no aplicativo móvel.";
$MESS["TASKSMOBILE_MODULE_NAME"] = "Tarefas no celular";
$MESS["TASKSMOBILE_UNINSTALL_TITLE"] = "Desinstalação do Módulo";
