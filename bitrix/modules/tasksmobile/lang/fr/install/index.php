<?php
$MESS["TASKSMOBILE_DENIED"] = "Accès refusé";
$MESS["TASKSMOBILE_INSTALL_TITLE"] = "Installation du module";
$MESS["TASKSMOBILE_MODULE_DESCRIPTION"] = "Ce module gère les tâches dans l'application mobile.";
$MESS["TASKSMOBILE_MODULE_NAME"] = "Tâches mobiles";
$MESS["TASKSMOBILE_UNINSTALL_TITLE"] = "Désinstallation du module";
