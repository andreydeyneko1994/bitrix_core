<?php
$MESS["TASKSMOBILE_DENIED"] = "Acceso denegado";
$MESS["TASKSMOBILE_INSTALL_TITLE"] = "Instalación de módulo";
$MESS["TASKSMOBILE_MODULE_DESCRIPTION"] = "Este módulo administra tareas en la aplicación móvil.";
$MESS["TASKSMOBILE_MODULE_NAME"] = "Tareas móviles";
$MESS["TASKSMOBILE_UNINSTALL_TITLE"] = "Desinstalación de módulo";
