<?php
$MESS["TASKSMOBILE_DENIED"] = "Odmowa dostępu";
$MESS["TASKSMOBILE_INSTALL_TITLE"] = "Instalacja modułu";
$MESS["TASKSMOBILE_MODULE_DESCRIPTION"] = "Moduł zarządza zadaniami w aplikacji mobilnej.";
$MESS["TASKSMOBILE_MODULE_NAME"] = "Zadania ze smartfona";
$MESS["TASKSMOBILE_UNINSTALL_TITLE"] = "Dezinstalacja modułu";
