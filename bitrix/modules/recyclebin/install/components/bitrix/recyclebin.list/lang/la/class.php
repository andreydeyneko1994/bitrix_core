<?
$MESS["RECYCLEBIN_COLUMN_ENTITY_ID"] = "ID";
$MESS["RECYCLEBIN_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["RECYCLEBIN_COLUMN_MODULE_ID"] = "Módulo";
$MESS["RECYCLEBIN_COLUMN_NAME"] = "Nombre";
$MESS["RECYCLEBIN_COLUMN_TIMESTAMP"] = "Eliminado el ";
$MESS["RECYCLEBIN_COLUMN_USER_ID"] = "Usuario";
$MESS["RECYCLEBIN_PRESET_CURRENT_DAY"] = "Eliminado hoy";
$MESS["RECYCLEBIN_PRESET_CURRENT_MONTH"] = "Eliminado este mes";
$MESS["RECYCLEBIN_PRESET_CURRENT_WEEK"] = "Eliminado esta semana";
?>