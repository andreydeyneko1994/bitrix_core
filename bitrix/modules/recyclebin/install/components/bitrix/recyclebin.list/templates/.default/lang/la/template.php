<?
$MESS["RECYCLEBIN_CONFIRM_REMOVE"] = "Los elementos seleccionados serán eliminados permanentemente. ¿Esta seguro que desea continuar?";
$MESS["RECYCLEBIN_CONFIRM_RESTORE"] = "¿Desea recuperar los elementos seleccionados y moverlos de nuevo a la lista?";
$MESS["RECYCLEBIN_CONTEXT_MENU_TITLE_PREVIEW"] = "Vista previa";
$MESS["RECYCLEBIN_CONTEXT_MENU_TITLE_REMOVE"] = "Eliminar";
$MESS["RECYCLEBIN_CONTEXT_MENU_TITLE_RESTORE"] = "Recuperar";
$MESS["RECYCLEBIN_DELETE_SUCCESS"] = "El elemento ha sido eliminado";
$MESS["RECYCLEBIN_GROUP_ACTIONS_DELETE"] = "Eliminar";
$MESS["RECYCLEBIN_GROUP_ACTIONS_RESTORE"] = "Recuperar";
$MESS["RECYCLEBIN_LICENSE_POPUP_TEXT"] = "Las tareas eliminadas se pueden recuperar de la Papelera de Reciclaje solo en los planes comerciales. Las tareas que eliminas se colocan cuidadosamente en la Papelera de Reciclaje para que puedas recuperarlas o eliminarlas permanentemente en cualquier momento.<br/><a href=\"https://www.bitrix24.com/prices/\" target=\"_blank\">Si, Me gustaría aprender más sobre planes comerciales.</a>";
$MESS["RECYCLEBIN_LICENSE_POPUP_TITLE"] = "Migrar al Plan Extendido";
$MESS["RECYCLEBIN_RESTORE_SUCCESS"] = "El artículo ha sido recuperado exitosamente.";
?>