<?
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module bizproc n'a pas été installé.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "Le bloc d'information n'a pas été sélectionné.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["WIKI_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "Module wiki n'a pas été installé.";
$MESS["WIKI_NOT_CHANGE_BIZPROC"] = "Le bloc d'informations Wiki doit être enregistré dans les processus d'affaires pour conserver l'historique des versions.";
$MESS["WIKI_PAGE_RECOVER"] = "Page correctement restaurée.";
$MESS["WIKI_PAGE_RECOVER_ERROR"] = "Erreur de restauration.";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "Erreur d'initialisation du réseau social.";
?>