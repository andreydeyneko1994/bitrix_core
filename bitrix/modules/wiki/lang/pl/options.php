<?
$MESS["WIKI_OPTIONS_IMAGE_DESCR"] = "Są to wymiary obrazu po zmianie rozmiaru.";
$MESS["WIKI_OPTIONS_IMAGE_MAX_HEIGHT"] = "Maksymalna wysokość obrazu (w pikselach):";
$MESS["WIKI_OPTIONS_IMAGE_MAX_WIDTH"] = "Maksymalna szerokość obrazu (w pikselach):";
$MESS["WIKI_OPTIONS_SOCNET_ENABLE"] = "Włącz obsługę sieci społecznościowej";
$MESS["WIKI_OPTIONS_SOCNET_FORUM_ID"] = "ID forum dyskusji:";
$MESS["WIKI_OPTIONS_SOCNET_IBLOCK_ID"] = "Blok informacji:";
$MESS["WIKI_OPTIONS_SOCNET_MESSAGE_PER_PAGE"] = "Liczba postów na stronę:";
$MESS["WIKI_OPTIONS_SOCNET_USE_CAPTCHA"] = "Użyj CAPTCHA";
$MESS["WIKI_OPTIONS_SOCNET_USE_REVIEW"] = "Włącz dyskusje";
$MESS["WIKI_TAB_SOCNET"] = "Sieć Społecznościowa";
$MESS["WIKI_TAB_TITLE_SOCNET"] = "Wiki dla parametrów integracji sieci społecznościowej";
?>