<?
$MESS["WIKI_CREATE"] = "Utwórz";
$MESS["WIKI_CREATE_NEW_FORUM"] = "Create forum";
$MESS["WIKI_CREATE_NEW_FORUM_NAME"] = "Nazwa forum";
$MESS["WIKI_CREATE_NEW_IBLOCK"] = "Utwórz blok informacji";
$MESS["WIKI_CREATE_NEW_IBLOCK_NAME"] = "Nazwa bloku informacji";
$MESS["WIKI_CREATE_NEW_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM"] = "Utwórz forum dla sieci społecznościowej";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM_NAME"] = "Nazwa forum sieci społecznościowej";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK"] = "Utwórz blok informacji dla sieci społecznościowej";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_NAME"] = "Nazwa bloku informacji sieci społecznościowej";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_TYPE"] = "Typ bloku informacji sieci społecznościowej";
$MESS["WIKI_ID"] = "ID";
$MESS["WIKI_SELECT"] = "wybierz";
?>