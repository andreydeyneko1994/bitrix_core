<?
$MESS["WIKI_INSTALL_DESCRIPTION"] = "This module provides Wiki features for your website.";
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_TITLE"] = "Wiki Module Installation";
$MESS["WIKI_PERM_D"] = "Odmowa dostępu";
$MESS["WIKI_PERM_R"] = "Wyświetl strony";
$MESS["WIKI_PERM_W"] = "Utwórz i edytuj strony";
$MESS["WIKI_PERM_X"] = "Utwórz i edytuj strony w edytorze";
$MESS["WIKI_PERM_Y"] = "Usuń strony i historię";
$MESS["WIKI_PERM_Z"] = "Ustaw Zgodę";
$MESS["WIKI_UNINSTALL_TITLE"] = "Wiki Module Uninstallation";
?>