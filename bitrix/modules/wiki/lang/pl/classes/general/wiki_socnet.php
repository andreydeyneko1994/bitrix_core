<?
$MESS["F_FORUM_MESSAGE_CNT"] = "Licznik komentarzy";
$MESS["F_FORUM_TOPIC_ID"] = "Komentarze tematu";
$MESS["SOCNET_LOG_WIKI_DEL_GROUP"] = "Wiki (usuń)";
$MESS["SOCNET_LOG_WIKI_GROUP"] = "Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS"] = "wszystkie zmiany w Wiki tej grupy";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_1"] = "#TITLE#: Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_2"] = "#TITLE# Aktualizacje Wiki";
$MESS["SONET_ADD_COMMENT_SOURCE_ERROR"] = "Nie powiodło się dodanie komentarza do źródła wydarzenia.";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE"] = "usunięta strona Wiki #TITLE#";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_24"] = "Strona Wiki usunięta";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# usunął/a stronę Wiki \"#TITLE#\" w grupie \"#ENTITY#\"";
$MESS["WIKI_PERM_DELETE"] = "Usuń strony i historię";
$MESS["WIKI_PERM_READ"] = "Wyświetl strony";
$MESS["WIKI_PERM_WRITE"] = "Utwórz i edytuj strony";
$MESS["WIKI_PERM_WRITE_W"] = "Utwórz i edytuj strony w edytorze";
$MESS["WIKI_SOCNET_LOG_ANONYMOUS_USER"] = "Anonim";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE"] = "Komentuj na stronie Wiki: #TITLE#";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE_MAIL"] = "#CREATED_BY# dodał/a nowy komentarz do strony Wiki \"#TITLE#\"w grupie \"#ENTITY#\"";
$MESS["WIKI_SOCNET_LOG_ENTITY_G"] = "Grupa";
$MESS["WIKI_SOCNET_LOG_TITLE"] = "Strona Wiki #TITLE#";
$MESS["WIKI_SOCNET_LOG_TITLE_24"] = "Strona Wiki";
$MESS["WIKI_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# utworzył/a lub zaktualizował/a stronę Wiki \"#TITLE#\" w grupie \"#ENTITY#\"";
$MESS["WIKI_SOCNET_LOG_USER"] = "Użytkownik";
$MESS["WIKI_SOCNET_TAB"] = "Wiki";
$MESS["WIKI_SONET_FROM_LOG_IM_COMMENT"] = "Dodano komentarz do twojego artykułu Wiki \"#title#\"";
?>