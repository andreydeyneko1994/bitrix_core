<?
$MESS["CATEGORY_NAME"] = "Kategoria";
$MESS["FILE_NAME"] = "Plik";
$MESS["PAGE_ACCESS"] = "Chroń";
$MESS["PAGE_ACCESS_TITLE"] = "Chroń";
$MESS["PAGE_ADD"] = "Utwórz";
$MESS["PAGE_ADD_TITLE"] = "Utwórz";
$MESS["PAGE_ARTICLE"] = "Artykuł";
$MESS["PAGE_ARTICLE_TITLE"] = "Artykuł";
$MESS["PAGE_DELETE"] = "Usuń";
$MESS["PAGE_DELETE_TITLE"] = "Usuń";
$MESS["PAGE_DISCUSSION"] = "Forum";
$MESS["PAGE_DISCUSSION_TITLE"] = "Forum";
$MESS["PAGE_EDIT"] = "Edytuj";
$MESS["PAGE_EDIT_TITLE"] = "Edytuj";
$MESS["PAGE_HISTORY"] = "Historia";
$MESS["PAGE_HISTORY_TITLE"] = "Historia";
$MESS["PAGE_SERVICE"] = "Strona Usług";
$MESS["PAGE_SERVICE_TITLE"] = "Strona Usług";
$MESS["WIKI_CATEGORY_ALL"] = "Wszystkie strony";
$MESS["WIKI_CATEGORY_NOCAT"] = "Nieskategoryzowane strony";
$MESS["WIKI_NEW_PAGE_TITLE"] = "Nowa strona";
$MESS["WIKI_PAGE_RENAME"] = "Zmień nazwę";
$MESS["WIKI_PAGE_RENAME_TITLE"] = "Zmień nazwę i zaktualizuj wszystkie linki do tej strony";
?>