<?
$MESS["CATEGORY_NAME"] = "Catégorie";
$MESS["FILE_NAME"] = "Fichier";
$MESS["PAGE_ACCESS"] = "Protéger";
$MESS["PAGE_ACCESS_TITLE"] = "Protéger";
$MESS["PAGE_ADD"] = "Ajouter";
$MESS["PAGE_ADD_TITLE"] = "Ajouter";
$MESS["PAGE_ARTICLE"] = "Article";
$MESS["PAGE_ARTICLE_TITLE"] = "Article";
$MESS["PAGE_DELETE"] = "Supprimer";
$MESS["PAGE_DELETE_TITLE"] = "Supprimer";
$MESS["PAGE_DISCUSSION"] = "Discussion";
$MESS["PAGE_DISCUSSION_TITLE"] = "Discussion";
$MESS["PAGE_EDIT"] = "Éditer";
$MESS["PAGE_EDIT_TITLE"] = "Éditer";
$MESS["PAGE_HISTORY"] = "Historique";
$MESS["PAGE_HISTORY_TITLE"] = "Historique";
$MESS["PAGE_SERVICE"] = "Page de service";
$MESS["PAGE_SERVICE_TITLE"] = "Page de service";
$MESS["WIKI_CATEGORY_ALL"] = "Toutes les pages";
$MESS["WIKI_CATEGORY_NOCAT"] = "Pages sans catégorie";
$MESS["WIKI_NEW_PAGE_TITLE"] = "Nouvelle page";
$MESS["WIKI_PAGE_RENAME"] = "Renommer";
$MESS["WIKI_PAGE_RENAME_TITLE"] = "Renommer et rafraîchir tous les liens vers cette page";
?>