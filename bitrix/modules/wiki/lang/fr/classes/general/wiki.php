<?
$MESS["Service:Categories"] = "Catégories";
$MESS["Service:Categories_TITLE"] = "Liste de toutes les Catégories";
$MESS["WIKI_ERROR_IMAGE_ATTACHED"] = "Une image existe déjà avec ce nom.";
$MESS["WIKI_RECOVER_COMMENT"] = "Récupération du document à partir de";
$MESS["WIKI_USER_T_ADD_ERR"] = "Erreur de la sauvegarde de la première page.";
?>