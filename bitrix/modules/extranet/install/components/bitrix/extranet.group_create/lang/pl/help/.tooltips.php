<?
$MESS["ID_TIP"] = "Określa kod, który koresponduje z ID użytkownika.";
$MESS["PAGE_VAR_TIP"] = "Określ nazwę zmiennej, do której strona sieci społecznościowej będzie przekazywana tutaj.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "Ścieżka do strony edycji profilu użytkownikae. Na przykład: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PATH_TO_USER_TIP"] = "Ścieżka do strony profilu użytkownika. Na przykład: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["USER_PROPERTY_TIP"] = "Wybierz dodatkowe właściwości, które będą pokazywane w profilu użytkownika.";
$MESS["USER_VAR_TIP"] = "Określ nazwę zmiennej, do której ID użytkownika sieci społecznościowej będzie przekazywana tutaj.";
?>