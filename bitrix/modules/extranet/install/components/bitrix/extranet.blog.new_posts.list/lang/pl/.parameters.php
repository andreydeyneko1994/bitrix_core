<?
$MESS["BB_NAV_TEMPLATE"] = "Nazwa szablonu stronicowania";
$MESS["BB_PATH_TO_BLOG_CATEGORY"] = "Szablon URL strony bloga grupy (filtrowane według tagów)";
$MESS["BC_DATE_TIME_FORMAT"] = "Format daty i godziny";
$MESS["BLG_BLOG_URL"] = "Adres URL bloga";
$MESS["BLG_GROUP_ID"] = "Grupa blogów";
$MESS["B_VARIABLE_ALIASES"] = "Zmienne aliasów";
$MESS["EBMNP_BLOG_VAR"] = "Zmienna identyfikująca blog";
$MESS["EBMNP_CATEGORY_NAME_VAR"] = "Kategoria Nazwy Zmiennej";
$MESS["EBMNP_MESSAGE_LENGTH"] = "Maksymalna długość wyświetlanej wiadomości";
$MESS["EBMNP_MESSAGE_PER_PAGE"] = "Wiadomości na stronę";
$MESS["EBMNP_PAGE_VAR"] = "Zmienna strony";
$MESS["EBMNP_PATH_TO_BLOG"] = "Szablon ścieżki do strony bloga";
$MESS["EBMNP_PATH_TO_BLOG_CATEGORY"] = "Strona Listy Kategorii Wiadomości";
$MESS["EBMNP_PATH_TO_GROUP_BLOG_POST"] = "Szablon URL Strony Postów Bloga Grupy";
$MESS["EBMNP_PATH_TO_POST"] = "Szablon strony wiadomości bloga";
$MESS["EBMNP_PATH_TO_SMILE"] = "Ścieżka do folderu z emotikonami, w stosunku do katalogu głównego serwisu";
$MESS["EBMNP_PATH_TO_USER"] = "Szablon ścieżki do strony bloga użytkownika";
$MESS["EBMNP_POST_VAR"] = "Zmienna identyfikująca wiadomości bloga";
$MESS["EBMNP_PREVIEW_HEIGHT"] = "Wysokość podglądu obrazu";
$MESS["EBMNP_PREVIEW_WIDTH"] = "Szerokość podglądu obrazu";
$MESS["EBMNP_USER_VAR"] = "Zmienna identyfikująca uzytkownika bloga";
?>