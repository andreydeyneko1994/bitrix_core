<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Comentários: Extranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Comentários em arquivos do usuário: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Comentários em galerias de fotos de usuários e de grupos: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Comentários sobre as tarefas de usuário e grupo: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Fóruns ocultos: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Fóruns de usuários individuais e grupos do site extranet.";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Fóruns de Usuário e Grupo: Extranet";
?>