<?
$MESS["EXTRANET_NEW_MESSAGE_MESSAGE"] = "Message à caractère informatif du site #SITE_NAME#!
------------------------------------------

Bonjour #USER_NAME#!

Vous avez un nouveau message de #SENDER_NAME# #SENDER_LAST_NAME# :

------------------------------------------
#MESSAGE#
------------------------------------------

Lien vers message:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Ce message est généré automatiquement.";
$MESS["EXTRANET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME# : Vous avez un nouveau message";
?>