<?php
$MESS["COPYRIGHT"] = "2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "O assistente requer que o JavaScript esteja habilitado no seu sistema. O JavaScript está desabilitado ou não é suportado pelo seu navegador. Por favor, altere as configurações do navegador e <a href=>tente novamente</a>.";
$MESS["SUPPORT"] = "<a href=\"https://www.bitrix24.com.br/\" target=\"_blank\">www.bitrix24.com.br</a> | <a href=\"https://helpdesk.bitrix24.com.br/\" target=\"_blank\">Suporte técnico</a>";
$MESS["WIZARD_TITLE"] = "Bitrix Intranet 12.0:<br>Configuração do Site Extranet";
