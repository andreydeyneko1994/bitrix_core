<?php
$MESS["COPYRIGHT"] = "&copy; 2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "El asistente requiere que el JavaScript este habilitado en su sistema. El JavaScript esta deshabilitado o no es compatible con su navegador. Por favor modifique la configuración de su nabegador e <a href=\"\">inténtelo de nuevo</a>.";
$MESS["SUPPORT"] = "<a href=\"https://www.bitrix24.es/\" target=\"_blank\">www.bitrix24.es</a> | <a href=\"https://helpdesk.bitrix24.es/\" target=\"_blank\">Soporte Tecnico</a>";
$MESS["WIZARD_TITLE"] = "Bitrix Intranet Portal 9.0:<br>Configuración del sitio web extranet";
