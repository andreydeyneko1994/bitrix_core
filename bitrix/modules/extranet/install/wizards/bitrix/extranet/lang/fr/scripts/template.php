<?php
$MESS["COPYRIGHT"] = "&copy; 2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "Pour installer l'assistant il faut activer JavaScript. Apparemment, soit JavaScript n'est pas soutenu par le navigateur soit il est désactivé. Modifiez les paramètres du navigateur et ensuite <a href=''>essayez de nouveau</a>.";
$MESS["SUPPORT"] = "<a href=\"https://www.bitrix24.fr/\" target=\"_blank\">www.bitrix24.fr</a> | <a href=\"https://helpdesk.bitrix24.fr/\" target=\"_blank\">Support technique</a>";
$MESS["WIZARD_TITLE"] = "Configuration du site extranet du produit <br>&laquo;Bitrix: portail d'entreprise 14.0&raquo;";
