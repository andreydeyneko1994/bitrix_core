<?
$MESS["SERVICE_BLOG"] = "Blog";
$MESS["SERVICE_FILEMAN"] = "Gestion de la structure";
$MESS["SERVICE_FILES"] = "Fichiers du site extranet";
$MESS["SERVICE_FORM"] = "Formulaires web";
$MESS["SERVICE_FORUM"] = "Forum";
$MESS["SERVICE_INTRANET"] = "Paramètres du portail corporatif";
$MESS["SERVICE_LEARNING"] = "apprentissage";
$MESS["SERVICE_MAIN_SETTINGS"] = "Réglages du site extranet";
$MESS["SERVICE_PHOTOGALLERY"] = "Galerie photos";
$MESS["SERVICE_SEARCH"] = "Recherche";
$MESS["SERVICE_SOCIALNETWORK"] = "Réseau social";
$MESS["SERVICE_STATISTIC"] = "Statistique";
$MESS["SERVICE_USERS"] = "Utilisateurs";
$MESS["SERVICE_VOTE"] = "Enquêtes";
$MESS["SERVICE_WORKFLOW"] = "Flux de documents";
?>