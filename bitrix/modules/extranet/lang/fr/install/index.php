<?
$MESS["EXTRANET_INSTALL_TITLE"] = "Installation du module Extranet";
$MESS["EXTRANET_MODULE_DESC"] = "Module qui soutient le travail de collaborateurs de entreprise avec des partenaires extérieurs.";
$MESS["EXTRANET_MODULE_NAME"] = "Extranet";
$MESS["EXTRANET_UNINSTALL_TITLE"] = "Suppression du module Extranet";
$MESS["MOD_EXTRANET_RUN_WIZARD"] = "Activer l'assistant de réglage du site extranet";
?>