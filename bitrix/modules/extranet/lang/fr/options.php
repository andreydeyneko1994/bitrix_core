<?
$MESS["EXTRANET_RESET"] = "Annuler";
$MESS["EXTRANET_SAVE"] = "Enregistrer";
$MESS["EXTRANET_SITE_ID"] = "Code du site d' Intranet : ";
$MESS["EXTRANET_UF_PUBLIC_CODE"] = "Code de symbole d'utilisateur confirmant la publicité du collaborateur : ";
$MESS["EXTRANET_USER_GROUP"] = "Groupe d'utilisateurs Extranet : ";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Par défaut";
?>