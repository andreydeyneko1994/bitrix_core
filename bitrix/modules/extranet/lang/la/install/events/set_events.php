<?
$MESS["EXTRANET_INVITATION_DESC"] = "#CHECKWORD# - palabra clave
#USER_ID# - ID del usuario
#EMAIL# - correo electrónico del usuario
#SITE_NAME# - Nombre del sitio web
#SERVER_NAME# - Dirección del sitio web (sin http://)";
$MESS["EXTRANET_INVITATION_MESSAGE"] = "Ha sido invitado al sitio web. Para confirmar su registro y establecer una contraseña, por favor haga clic aquí:
http://#SERVER_NAME#/extranet/confirm/?checkword=#CHECKWORD#&user_id=#USER_ID#

#USER_TEXT#

Este mensaje se ha generado de forma automática.";
$MESS["EXTRANET_INVITATION_NAME"] = "Nueva Invitación de usuario al sitio web";
$MESS["EXTRANET_INVITATION_SUBJECT"] = "#SITE_NAME#: Usted fue invitado";
$MESS["EXTRANET_WG_FROM_ARCHIVE_DESC"] = "#WG_ID# - ID del grupo de trabajo
#WG_NAME# - Nombre del grupo de trabajo
#MEMBER_EMAIL# - correos electrónicos de los miembros del grupo de trabajo
#SITE_NAME# - Nombre del sitio web";
$MESS["EXTRANET_WG_FROM_ARCHIVE_MESSAGE"] = "El grupo de trabajo '#WG_NAME#' [#WG_ID#] en #SITE_NAME# ha sido reactivado.
---------------------------------------------------------------------------

Este grupo de trabajo no es más de sólo lectura y puede ser editado.

Siga el siguiente link para ver el grupo de trabajo:
http://#SERVER_NAME#/extranet/workgroups/group/#WG_ID#/

Este mensaje fue generado automáticamente.";
$MESS["EXTRANET_WG_FROM_ARCHIVE_NAME"] = "El grupo de trabajo ha sido reactivado.";
$MESS["EXTRANET_WG_FROM_ARCHIVE_SUBJECT"] = "#SITE_NAME#: El grupo de trabajo '#WG_NAME#' ha sido reactivado.";
$MESS["EXTRANET_WG_TO_ARCHIVE_DESC"] = "#WG_ID# - ID del grupo de trabajo
#WG_NAME# - Nombre del grupo de trabajo
#MEMBER_EMAIL# - correos electrónicos de los miembros del grupo de trabajo
#SITE_NAME# - Nombre del sitio web";
$MESS["EXTRANET_WG_TO_ARCHIVE_MESSAGE"] = "El grupo de trabajo '#WG_NAME#' [#WG_ID#] en #SITE_NAME# ha sido archivado
---------------------------------------------------------------------------
Este grupo de trabajo es ahora de sólo lectura.

Haga clic en el siguiente enlace para ver el grupo de trabajo:
http://#SERVER_NAME#/extranet/workgroups/group/#WG_ID#/

Este mensaje ha sido creado automáticamente.";
$MESS["EXTRANET_WG_TO_ARCHIVE_NAME"] = "El grupo de trabajo ha sido archivado";
$MESS["EXTRANET_WG_TO_ARCHIVE_SUBJECT"] = "#SITE_NAME#: El grupo de trabajo '#WG_NAME#' ha sido archivado.";
?>