<?
$MESS["EXTRANET_INSTALL_TITLE"] = "Instalar Extranet";
$MESS["EXTRANET_MODULE_DESC"] = "Permitir al personal de la compañía cooperar con socios externos.";
$MESS["EXTRANET_MODULE_NAME"] = "Extranet";
$MESS["EXTRANET_UNINSTALL_TITLE"] = "Desinstalar Extranet";
$MESS["MOD_EXTRANET_RUN_WIZARD"] = "Ejecutar el asistente de instalación de la web extranet";
?>