<?
$MESS["EXTRANET_INVITATION_NAME"] = "Zaproszenie Użytkownika Nowej Strony";
$MESS["EXTRANET_INVITATION_SUBJECT"] = "#SITE_NAME#: Jesteś zaproszony";
$MESS["EXTRANET_WG_FROM_ARCHIVE_NAME"] = "Grupa robocza została przywrócona z archiwum.";
$MESS["EXTRANET_WG_FROM_ARCHIVE_SUBJECT"] = "#SITE_NAME#: Grupa robocza '#WG_NAME#' została przywrócona z archiwum.";
$MESS["EXTRANET_WG_TO_ARCHIVE_NAME"] = "Grupa robocza została zarchiwizowana.";
$MESS["EXTRANET_WG_TO_ARCHIVE_SUBJECT"] = "#SITE_NAME#: Grupa robocza '#WG_NAME#' została zarchiwizowana.";
?>