<?php
$MESS["EXTRANET_INSTALL_TITLE"] = "Instaluj Ekstranet";
$MESS["EXTRANET_MODULE_DESC"] = "Umożliwia personelowi firmy współpracę z partnerami zewnętrznymi.";
$MESS["EXTRANET_MODULE_NAME"] = "Ekstranet";
$MESS["EXTRANET_UNINSTALL_TITLE"] = "Odinstaluj Ekstranet";
$MESS["MOD_EXTRANET_RUN_WIZARD"] = "Uruchom Kreatora Ustawień Strony Ekstranetu";
