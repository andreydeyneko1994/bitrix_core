<?
$MESS["EXTRANET_INVITATION_DESC"] = "#CHECKWORD# - palavra de verificação
#USER_ID# - ID de usuário
#EMAIL# - e-mail do usuário
#SITE_NAME# - nome do site
#SERVER_NAME# - URL do site (sem http://) ";
$MESS["EXTRANET_INVITATION_MESSAGE"] = "Notificação de #SITE_NAME#
------------------------------------------

Você foi convidado para o site. Para confirmar seu cadastro e definir uma senha, por favor clique aqui:
http://#SERVER_NAME#/extranet/confirm/?checkword=#CHECKWORD#&user_id=#USER_ID#

Esta mensagem foi gerada automaticamente.";
$MESS["EXTRANET_INVITATION_NAME"] = "Convite a usuário para novo site";
$MESS["EXTRANET_INVITATION_SUBJECT"] = "#SITE_NAME#: Você está convidado";
$MESS["EXTRANET_WG_FROM_ARCHIVE_DESC"] = "#WG_ID# - ID do grupo de trabalho
#WG_NAME# - nome do grupo de trabalho
#MEMBER_EMAIL# - e-mails de membros de grupo de trabalho
#SITE_NAME# - nome do site ";
$MESS["EXTRANET_WG_FROM_ARCHIVE_MESSAGE"] = "O grupo de trabalho '#WG_NAME#' [#WG_ID#] em #SITE_NAME# foi desarquivado.
---------------------------------------------------------------------------

O grupo de trabalho não é mais somente leitura e pode ser modificado.

Clique no link a seguir para visualizar o grupo de trabalho:
http://#SERVER_NAME#/extranet/workgroups/group/#WG_ID#/

Esta mensagem foi gerada automaticamente.";
$MESS["EXTRANET_WG_FROM_ARCHIVE_NAME"] = "O grupo de trabalho foi desarquivado.";
$MESS["EXTRANET_WG_FROM_ARCHIVE_SUBJECT"] = "#SITE_NAME#: O grupo de trabalho '#WG_NAME#' foi desarquivado.";
$MESS["EXTRANET_WG_TO_ARCHIVE_DESC"] = "#WG_ID# - ID do grupo de trabalho
#WG_NAME# - nome do grupo de trabalho
#MEMBER_EMAIL# - e-mails de membros de grupo de trabalho
#SITE_NAME# - nome do site";
$MESS["EXTRANET_WG_TO_ARCHIVE_MESSAGE"] = "O grupo de trabalho '#WG_NAME#' [#WG_ID#] em #SITE_NAME# foi arquivado.
---------------------------------------------------------------------------

Este grupo de trabalho é somente leitura. 

Clique no link a seguir para visualizar o grupo de trabalho:
http://#SERVER_NAME#/extranet/workgroups/group/#WG_ID#/

Esta mensagem foi criada automaticamente.";
$MESS["EXTRANET_WG_TO_ARCHIVE_NAME"] = "O grupo de trabalho foi arquivado.";
$MESS["EXTRANET_WG_TO_ARCHIVE_SUBJECT"] = "#SITE_NAME#: O grupo de trabalho '#WG_NAME#' foi arquivado.";
?>