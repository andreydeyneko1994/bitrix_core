<?
$MESS["EXTRANET_INSTALL_TITLE"] = "Instalar extranet";
$MESS["EXTRANET_MODULE_DESC"] = "Permite pessoal da empresa a co-operar com parceiros externos.";
$MESS["EXTRANET_MODULE_NAME"] = "Extranet";
$MESS["EXTRANET_UNINSTALL_TITLE"] = "Desinstalar extranet";
$MESS["MOD_EXTRANET_RUN_WIZARD"] = "Executar assistente de configuração de site extranet";
?>