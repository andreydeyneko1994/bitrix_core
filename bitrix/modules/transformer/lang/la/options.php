<?
$MESS["TRANSFORMER_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["TRANSFORMER_ACCOUNT_ERROR_PUBLIC"] = "Se especificó una dirección pública incorrecta.";
$MESS["TRANSFORMER_CONNECTION_TIME"] = "Tiempo de espera de la conexión del controlador (segundos)";
$MESS["TRANSFORMER_PUBLIC_URL"] = "Dirección pública del sitio web";
$MESS["TRANSFORMER_STREAM_TIME"] = "Tiempo de espera de respuesta del controlador (segundos)";
$MESS["TRANSFORMER_TAB_SETTINGS"] = "Ajustes";
$MESS["TRANSFORMER_TAB_TITLE_SETTINGS_2"] = "Parámetros de conexión";
$MESS["TRANSFORMER_WAIT_RESPONSE"] = "Permitir un tiempo de espera más largo";
$MESS["TRANSFORMER_WAIT_RESPONSE_DESC"] = "Habilite esta opción si desea la respuesta del controlador en el registro";
?>