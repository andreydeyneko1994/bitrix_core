<?
$MESS["TRANSFORMER_CHECK_PUBLIC_PATH"] = "Se especificó una dirección pública incorrecta.";
$MESS["TRANSFORMER_PUBLIC_PATH"] = "Dirección pública del sitio web:";
$MESS["TRANSFORMER_PUBLIC_PATH_DESC"] = "El módulo requiere una dirección correcta del sitio web público para funcionar correctamente.";
$MESS["TRANSFORMER_PUBLIC_PATH_DESC_2"] = "Si el acceso externo a su red está restringido, habilite el acceso a solo ciertas páginas. Por favor refiérase a #LINK_START#documentación#LINK_END# para detalles.";
?>