<?
$MESS["TRANSFORMER_INSTALL_TITLE"] = "Instalación del módulo \"Convertidor de Archivos\"";
$MESS["TRANSFORMER_MODULE_DESCRIPTION"] = "El módulo envía archivos a un motor de conversión.";
$MESS["TRANSFORMER_MODULE_NAME"] = "Convertidor de archivos";
$MESS["TRANSFORMER_UNINSTALL_QUESTION"] = "¿Seguro que quiere eliminar el módulo?";
$MESS["TRANSFORMER_UNINSTALL_TITLE"] = "Desinstalación del Módulo \"Convertidor de Archivos\"";
?>