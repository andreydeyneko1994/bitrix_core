<?
$MESS["TRANSFORMER_INSTALL_TITLE"] = "Installation du module \"Convertisseur de fichiers\"";
$MESS["TRANSFORMER_MODULE_DESCRIPTION"] = "Le module envoie des fichiers à un moteur de conversion";
$MESS["TRANSFORMER_MODULE_NAME"] = "Convertisseur de fichiers";
$MESS["TRANSFORMER_UNINSTALL_QUESTION"] = "Voulez-vous vraiment supprimer le module  ?";
$MESS["TRANSFORMER_UNINSTALL_TITLE"] = "Désinstallation du module \"Convertisseur de fichiers\"";
?>