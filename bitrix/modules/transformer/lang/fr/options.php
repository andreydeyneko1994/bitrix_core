<?
$MESS["TRANSFORMER_ACCOUNT_DEBUG"] = "Mode de débogage";
$MESS["TRANSFORMER_ACCOUNT_ERROR_PUBLIC"] = "Adresse publique spécifiée incorrecte.";
$MESS["TRANSFORMER_CONNECTION_TIME"] = "Délai d'attente de la connexion du contrôleur (secondes)";
$MESS["TRANSFORMER_PUBLIC_URL"] = "Adresse public du site";
$MESS["TRANSFORMER_STREAM_TIME"] = "Délai d'attente de la réponse du contrôleur (secondes)";
$MESS["TRANSFORMER_TAB_SETTINGS"] = "Paramètres";
$MESS["TRANSFORMER_TAB_TITLE_SETTINGS_2"] = "Paramètres de connexion";
$MESS["TRANSFORMER_WAIT_RESPONSE"] = "Autoriser un temps d'attente plus long";
$MESS["TRANSFORMER_WAIT_RESPONSE_DESC"] = "Activez cette option si vous voulez la réponse du contrôleur dans le journal";
?>