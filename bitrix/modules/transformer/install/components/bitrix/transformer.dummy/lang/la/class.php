<?
$MESS["VIDEO_TRANSFORMATION_ERROR_DESC"] = "error de conversión";
$MESS["VIDEO_TRANSFORMATION_ERROR_TITLE"] = "Algo ha ido mal...";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM"] = "Convertir de nuevo";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_NOT_ALLOWED"] = "Permisos insuficientes";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_NOT_INSTALLED"] = "El módulo \"Convertidor de Archivos\" no está instalado.";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_TRANSFORMED"] = "Este video ya se ha convertido.";
$MESS["VIDEO_TRANSFORMATION_IN_PROCESS_DESC"] = "tomará un poco de tiempo...";
$MESS["VIDEO_TRANSFORMATION_IN_PROCESS_TITLE"] = "El video estará disponible una vez que se haya convertido.";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_DESC"] = "Puedes convertirlo ahora";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_TITLE"] = "Este video aún no se ha convertido";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_TRANSFORM"] = "Convertir Vídeo";
?>