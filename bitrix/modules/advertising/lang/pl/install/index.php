<?
$MESS["AD_ADMIN"] = "administrator reklamy";
$MESS["AD_ADVERTISER"] = "reklamodawca";
$MESS["AD_ATTENTION"] = "Ostrzeżenie! Moduł zostanie odinstalowany.";
$MESS["AD_BACK"] = "Powrót";
$MESS["AD_BANNERS_MANAGER"] = "kierownik banera";
$MESS["AD_DELETE"] = "Dezinstalacja";
$MESS["AD_DELETE_TITLE"] = "Dezinstalacja modułu zarządzania reklamą";
$MESS["AD_DEMO"] = "dostęp demo";
$MESS["AD_DENIED"] = "odmowa";
$MESS["AD_ERRORS"] = "Błędy:";
$MESS["AD_INSTALL"] = "Instalacja modułu zarządzania reklamą";
$MESS["AD_INSTALLATION_COMPLETED"] = "Instalacja zakończona.";
$MESS["AD_INSTALL_ADV"] = "Reklama na Stronie";
$MESS["AD_INSTALL_ADV_ALT"] = "Zarządzanie Reklamą";
$MESS["AD_INSTALL_MODULE_DESCRIPTION"] = "Moduł Zarządzania Reklamą.";
$MESS["AD_INSTALL_MODULE_NAME"] = "Reklamy i banery";
$MESS["AD_INSTALL_TYPE"] = "Typy reklamy";
$MESS["AD_INSTALL_TYPE_ALT"] = "Ustaw typy reklamy";
$MESS["AD_REMOVING_COMPLETED"] = "Dezinstalacja zakończona.";
$MESS["AD_SAVE_DATA"] = "Zabisz Tabele";
?>