<?
$MESS["AD_ACTIVE"] = "Aktywny:";
$MESS["AD_ADD_NEW_TYPE"] = "Dodaj nowy typ";
$MESS["AD_ADD_NEW_TYPE_TITLE"] = "Dodaj nowy typ banera";
$MESS["AD_BACK_TO_TYPE_LIST"] = "Lista Typów";
$MESS["AD_CREATED"] = "Utworzony:";
$MESS["AD_DELETE_TYPE"] = "Usuń obecny typ";
$MESS["AD_DELETE_TYPE_CONFIRM"] = "Na pewno chcesz usunąć typ i wszystkie jego banery?";
$MESS["AD_DESCRIPTION"] = "Opis:";
$MESS["AD_MODIFIED"] = "Zmodyfikowany:";
$MESS["AD_NAME"] = "Nazwa:";
$MESS["AD_NEW_TYPE"] = "Nowy typ";
$MESS["AD_SID"] = "Symboliczny identyfikator:";
$MESS["AD_SORT"] = "Kolejność sortowania:";
$MESS["AD_STATISTICS"] = "Wykres współczynnika CTR dla tego typu banera";
$MESS["AD_STATISTICS_TITILE"] = "Zobacz statystyki tego typu bannerów";
$MESS["AD_TYPE"] = "Typ baneru";
$MESS["AD_TYPE_EDIT"] = "Edytuj typ banera";
$MESS["AD_TYPE_EDIT_TITLE"] = "Edytuj typ banera";
$MESS["AD_TYPE_VIEW_SETTINGS"] = "Wyświetl typ";
$MESS["AD_TYPE_VIEW_SETTINGS_TITLE"] = "Wyświetl parametry typu banera";
$MESS["AD_USER_ALT"] = "Wyświetl informacje użytkownika";
?>