<?
$MESS["AD_CONTRACT_DISABLE"] = "Uwaga! Ograniczenia kontraktu są wyłączone w ustawieniach modułu.";
$MESS["AD_ERROR_FORGOT_SID"] = "Błąd! Nie udało się wprowadzić symbolicznego identyfikatora (ID).";
$MESS["AD_ERROR_FROMTO_DATE_HAVETOBE_SET"] = "Błąd! Interwał czasu wyświetlania musi zostać zdefiniowany.";
$MESS["AD_ERROR_INCORRECT_BANNER_ID"] = "Błąd! Nieprawidłowe ID banera";
$MESS["AD_ERROR_INCORRECT_CONTRACT_ID"] = "Błąd! Nieprawidłowe ID kontraktu";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_BANNER"] = "Błąd! Odmowa dostępu do banera.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_CONTRACT"] = "Błąd! Odmowa dostępu do kontraktu.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_CREATE_TYPE"] = "Błąd! Brak wystarczających uprawnień, aby utworzyć typ.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_NEW_CONTRACT"] = "Błąd! Niewystarczające uprawnienia do tworzenia kontraktu.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_TYPE"] = "Błąd! Odmowa dostępu do typów.";
?>