<?php
$MESS["AD_BANNER_DAYS"] = "Liczba dni do prowadzenia statystyk banera:";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Średnica wykresu kołowego:";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "Wysokość wykresu:";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "Szerokość wykresu:";
$MESS["AD_CLEAR"] = "Usuń przy zapisie";
$MESS["AD_COOKIE_DAYS"] = "Liczba dni do zapamiętania prezentowanych wyświetleń banera odwiedzającemu:";
$MESS["AD_DONT_USE_CONTRACT"] = "Ignoruj warunki kontraktu podczas wyświetlania banerów";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "Nigdzie nie śledź wyświetleń banera";
$MESS["AD_RECORDS"] = "obecnie przechowywane zapisy:";
$MESS["AD_UPLOAD_SUBDIR"] = "Prześlij banery do podfolderu:";
$MESS["AD_USE_HTML_EDIT"] = "Użyj wizualny edytora HTML, aby edytować kod banera";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Ustaw domyślne";
