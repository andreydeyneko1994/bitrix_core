<?
$MESS["AD_ACTIVE"] = "Actif(ve) : ";
$MESS["AD_ADD_NEW_TYPE"] = "Ajouter un nouveau type";
$MESS["AD_ADD_NEW_TYPE_TITLE"] = "Ajouter un nouveau type de bannières";
$MESS["AD_BACK_TO_TYPE_LIST"] = "Liste des types";
$MESS["AD_CREATED"] = "Créé:";
$MESS["AD_DELETE_TYPE"] = "Supprimer le type de courant";
$MESS["AD_DELETE_TYPE_CONFIRM"] = "tes-vous sûr de vouloir supprimer le type et tous ses bannières ?";
$MESS["AD_DESCRIPTION"] = "Description";
$MESS["AD_EDIT_TYPE"] = "Type '#SID#'";
$MESS["AD_MODIFIED"] = "Changé:";
$MESS["AD_NAME"] = "Dénomination : ";
$MESS["AD_NEW_TYPE"] = "Nouveau type";
$MESS["AD_SID"] = "Identificateur de caractères : ";
$MESS["AD_SID_ALT"] = "(seuls les lettres latines, chiffres et soulignent symbole '_' autorisé)";
$MESS["AD_SORT"] = "Ordre de triage : ";
$MESS["AD_STATISTICS"] = "CTR graphique de ce type de bannière";
$MESS["AD_STATISTICS_TITILE"] = "Voir les statistiques de ce type bannières";
$MESS["AD_TYPE"] = "Type de bannière";
$MESS["AD_TYPE_EDIT"] = "Changer le type";
$MESS["AD_TYPE_EDIT_TITLE"] = "Changer le type";
$MESS["AD_TYPE_VIEW_SETTINGS"] = "Voir le type";
$MESS["AD_TYPE_VIEW_SETTINGS_TITLE"] = "Voir paramètres de type bannière";
$MESS["AD_USER_ALT"] = "Voir les paramètres utilisateur";
?>