<?
$MESS["AD_ADMIN"] = "administrateur de la publicité";
$MESS["AD_ADVERTISER"] = "annonceur";
$MESS["AD_ATTENTION"] = "Attention ! Le module sera supprimé du système.";
$MESS["AD_BACK"] = "Retourner";
$MESS["AD_BANNERS_MANAGER"] = "gestionnaire de bannière";
$MESS["AD_DELETE"] = "Supprimer";
$MESS["AD_DELETE_TITLE"] = "Publicité module de gestion désinstallation";
$MESS["AD_DEMO"] = "accès démo";
$MESS["AD_DENIED"] = "fermé";
$MESS["AD_ERRORS"] = "Erreurs : ";
$MESS["AD_INSTALL"] = "Installation du module de gestion de la publicité";
$MESS["AD_INSTALLATION_COMPLETED"] = "L'installation est terminée.";
$MESS["AD_INSTALL_ADV"] = "Publicité sur le site";
$MESS["AD_INSTALL_ADV_ALT"] = "La gestion de la publicité";
$MESS["AD_INSTALL_MODULE_DESCRIPTION"] = "Module de gestion de la publicité.";
$MESS["AD_INSTALL_MODULE_NAME"] = "Bannières publicitaires";
$MESS["AD_INSTALL_TYPE"] = "Types de publicité";
$MESS["AD_INSTALL_TYPE_ALT"] = "Mise en place des types de publicité";
$MESS["AD_REMOVING_COMPLETED"] = "La suppression est terminée.";
$MESS["AD_SAVE_DATA"] = "Enregistrer tableaux";
$MESS["AD_SAVE_TABLES"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Enregistrer les tables&quot;";
?>