<?php
$MESS["AD_BANNER_DAYS"] = "Nombre de jours de conservation des statistiques de bannières : ";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Diamètre de diagrammes ronds : ";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "Hauteur du graphique : ";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "Largeur du graphique : ";
$MESS["AD_CLEAR"] = "Effacer";
$MESS["AD_COOKIE_DAYS"] = "Nombre de jours de se rappeler une impression de bannière servi à un visiteur : ";
$MESS["AD_DELETE_ALL"] = "Tout supprimer";
$MESS["AD_DONT_USE_CONTRACT"] = "Ne pas utiliser les restrictions des contrats lors de l'affichage d'une bannière publicitaire";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "Ne pas fixer les affichages de toutes les bannières";
$MESS["AD_RECORDS"] = "notes : ";
$MESS["AD_SHOW_COMPONENT_PREVIEW"] = "Pré visualiser la bannière avec le modèle sélectionné";
$MESS["AD_UPLOAD_SUBDIR"] = "Télécharge bannières à sous-dossier : ";
$MESS["AD_USE_HTML_EDIT"] = "Utilisez éditeur HTML visuel pour modifier le code de la bannière";
$MESS["MAIN_RESTORE_DEFAULTS"] = "par défaut";
