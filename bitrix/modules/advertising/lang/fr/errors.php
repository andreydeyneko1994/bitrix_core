<?
$MESS["AD_CONTRACT_DISABLE"] = "Attention ! restrictions contractuelles sont désactivés dans les paramètres du module.";
$MESS["AD_ERROR_FIXSHOW_HAVETOBE_SET"] = "Erreur ! La case 'Fixer le nombre d'affichages de la bannière publicitaire' doit être cochée";
$MESS["AD_ERROR_FORGOT_SID"] = "Erreur ! Vous avez échoué à entrer identifiant symbolique (ID).";
$MESS["AD_ERROR_FROMTO_DATE_HAVETOBE_SET"] = "Erreur ! Statut est introuvable.";
$MESS["AD_ERROR_FROM_TILL_DATE_MODIFY"] = "Erreur ! Date de modification 'jusqu'à' doit être supérieur à 'partir'.";
$MESS["AD_ERROR_FROM_TILL_PERIOD"] = "Erreur ! La date 'jusqu'à' doit être plus grand que le 'de' dans le filtre";
$MESS["AD_ERROR_INCORRECT_BANNER_ID"] = "Erreur ! Mauvaise Bannière ID";
$MESS["AD_ERROR_INCORRECT_CONTRACT_ID"] = "Erreur ! Mauvaise ID de contrat";
$MESS["AD_ERROR_INCORRECT_SID"] = "Erreur ! Identificateur symbolique incorrecte (uniquement des lettres latines, chiffres et souligner symbole '_' autorisé).";
$MESS["AD_ERROR_MAX_SHOW_COUNT_HAVETOBE_SET"] = "Erreur ! Le paramètre 'Nombre maximal d'affichage de la bannière publicitaire' doit être spécifié";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_BANNER"] = "Erreur ! Accès à bannière refusée.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_CONTRACT"] = "Erreur ! Accès de contracter refusée.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_CREATE_TYPE"] = "Erreur ! Pas assez de droits d'accès pour créer un type.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_FOR_NEW_CONTRACT"] = "Erreur ! Pas assez de droits d'accès pour créer contrat.";
$MESS["AD_ERROR_NOT_ENOUGH_PERMISSIONS_TYPE"] = "Erreur ! L'accès aux types refusée.";
$MESS["AD_ERROR_ON_HANDLER"] = "Erreur inconnue dans le gestionnaire #HANDLER#.";
$MESS["AD_ERROR_SID_EXISTS"] = "Erreur ! Identifier '#SID#' déjà en usage.";
$MESS["AD_ERROR_WRONG_DATE_MODIFY_FROM"] = "Erreur ! Entrez la date de modification correcte 'de'.";
$MESS["AD_ERROR_WRONG_DATE_MODIFY_TILL"] = "Erreur ! Entrez la date de modification correcte 'jusqu'à'.";
$MESS["AD_ERROR_WRONG_DATE_SHOW_FROM_BANNER"] = "Erreur ! Mauvaise date de début dans le champ 'Afficher période'.";
$MESS["AD_ERROR_WRONG_DATE_SHOW_FROM_CONTRACT"] = "Erreur ! Mauvaise date de début dans le champ 'Période de bannières spectacles'.";
$MESS["AD_ERROR_WRONG_DATE_SHOW_TO_BANNER"] = "Erreur ! Mauvaise date de fin dans le champ 'Show période'.";
$MESS["AD_ERROR_WRONG_DATE_SHOW_TO_CONTRACT"] = "Erreur ! Mauvaise date de fin dans le champ 'Période de bannières spectacles'.";
$MESS["AD_ERROR_WRONG_PERIOD_FROM"] = "Erreur ! S'il vous plaît entrez la bonne 'de' la date de la 'Période'";
$MESS["AD_ERROR_WRONG_PERIOD_TILL"] = "Erreur ! S'il vous plaît entrez la bonne 'jusqu'à' la date de la 'Période'";
?>