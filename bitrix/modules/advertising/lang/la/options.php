<?php
$MESS["AD_BANNER_DAYS"] = "Número de días para mantener estadísticas de banner:";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "Diametro del gráfico:";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "Altura del gráfico:";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "Tamaño del gráfico:";
$MESS["AD_CLEAR"] = "Eliminar en guardar";
$MESS["AD_COOKIE_DAYS"] = "Número de días para recordar la impresión de banner que sirvió para un visitante:";
$MESS["AD_DELETE_ALL"] = "Eliminar todo";
$MESS["AD_DONT_USE_CONTRACT"] = "No aplicar restricciones de contratos para banners";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "No hacer ningun seguimiento de las impresiones de banner";
$MESS["AD_RECORDS"] = "registros almacenados actualmente:";
$MESS["AD_SHOW_COMPONENT_PREVIEW"] = "Vista previa del banner con la plantilla seleccionada";
$MESS["AD_UPLOAD_SUBDIR"] = "Subcarpeta para cargar banners:";
$MESS["AD_USE_HTML_EDIT"] = "Utilizar el editor HTML visual para editar código de banner";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Fijar por defecto";
