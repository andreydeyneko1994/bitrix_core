<?php
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_MODULE_ABSENT"] = "El módulo Catálogo comercial no está instalado.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_PRODUCT_ABSENT"] = "No se encontró el catálogo de productos CRM";
$MESS["CRM_CATALOG_CONTROLLER_ERR_IBLOCK_MODULE_ABSENT"] = "El módulo \"Bloques de información\" no está instalado.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_PAGE_UNKNOWN"] = "Página inválida";
