<?php
$MESS["CPD_CREATE_DOCUMENT_BUTTON"] = "Utwórz obiekt magazynowy";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_ADJUSTMENT"] = "Kwit składowy";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_DEDUCT"] = "Odpis";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_MOVING"] = "Transfer";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_SHIPMENT"] = "Zamówienie sprzedaży";
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Nie można utworzyć słownika o tej nazwie";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Dodaj elementy do listy słowników";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nowy produkt";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "Nie znaleziono produktu. Być może został usunięty.";
$MESS["CPD_SETS_NOT_SUPPORTED_LINK"] = "Przejdź do sklepu online";
$MESS["CPD_SETS_NOT_SUPPORTED_TITLE"] = "Wybrany typ produktu w pakiecie nie jest obsługiwany przez nowy formularz produktu. Możesz przeglądać i edytować zestawy w obszarze sklepu internetowego.<br>#LINK#";
