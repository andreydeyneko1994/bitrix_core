<?php
$MESS["CPD_CREATE_DOCUMENT_BUTTON"] = "Criar objeto de inventário";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_ADJUSTMENT"] = "Recibo de estoque";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_DEDUCT"] = "Baixa";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_MOVING"] = "Transferência";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_SHIPMENT"] = "Pedido de venda";
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Não foi possível criar um dicionário com este nome";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Adicionar itens à lista do dicionário";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Novo produto";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "O produto não foi encontrado. Pode ter sido excluído.";
$MESS["CPD_SETS_NOT_SUPPORTED_LINK"] = "Prosseguir para a loja online agora";
$MESS["CPD_SETS_NOT_SUPPORTED_TITLE"] = "O tipo de produto de pacote selecionado não é suportado pelo novo formulário de produto. Você pode ver e editar conjuntos na área da Loja online.<br>#LINK#";
