<?php
$MESS["CPD_CREATE_DOCUMENT_BUTTON"] = "Créer un objet d'inventaire";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_ADJUSTMENT"] = "Réception en stock";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_DEDUCT"] = "Radiation";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_MOVING"] = "Transfert";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_SHIPMENT"] = "Ordre de vente";
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Création d'un dictionnaire portant ce nom impossible";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Ajouter des articles à la liste du dictionnaire";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nouveau produit";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "Le produit est introuvable. Il a peut-être été supprimé.";
$MESS["CPD_SETS_NOT_SUPPORTED_LINK"] = "Passer maintenant à la Boutique en ligne";
$MESS["CPD_SETS_NOT_SUPPORTED_TITLE"] = "Le type de produit Pack que vous avez sélectionné n'est pas pris en charge par le nouveau formulaire de produit. Vous pouvez afficher et modifier les ensembles dans la zone Boutique en ligne.<br>#LINK#";
