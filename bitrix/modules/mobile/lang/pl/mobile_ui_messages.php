<?
$MESS["MUI_B24DISK"] = "Bitrix24.Drive";
$MESS["MUI_CAMERA_ROLL"] = "Zrób zdjęcie";
$MESS["MUI_CANCEL"] = "Anuluj";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "Pliki";
$MESS["MUI_CHOOSE_PHOTO"] = "Wybierz z galerii";
$MESS["MUI_COPY"] = "Kopiuj";
$MESS["MUI_COPY_TEXT"] = "Skopiuj tekst";
$MESS["MUI_PROCESSING"] = "Przetwarzanie...";
$MESS["MUI_TEXT_COPIED"] = "Tekst został skopiowany do schowka";
?>