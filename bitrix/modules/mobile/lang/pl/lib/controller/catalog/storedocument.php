<?php
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION"] = "Niewystarczające uprawnienia do anulowania przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION_PERMS"] = "Niewystarczające uprawnienia do anulowania przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT"] = "Błąd podczas przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT_PERMS"] = "Niewystarczające uprawnienia do przetwarzania obiektów magazynowych.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE"] = "Błąd podczas usuwania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE_PERMS"] = "Niewystarczające uprawnienia do anulowania przetwarzania obiektu magazynowego.";
