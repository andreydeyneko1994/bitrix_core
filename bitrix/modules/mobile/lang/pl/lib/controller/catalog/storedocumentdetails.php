<?php
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD"] = "Błąd podczas tworzenia obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD_PERMS"] = "Niewystarczające uprawnienia do utworzenia obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_BARCODE_ALREADY_EXISTS"] = "Kod kreskowy \"#BARCODE#\" jest już przypisany do innego produktu w katalogu.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION"] = "Błąd podczas anulowania przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION_PERMS"] = "Niewystarczające uprawnienia do anulowania przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT"] = "Błąd podczas przetwarzania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT_PERMS"] = "Niewystarczające uprawnienia do przetwarzania.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE"] = "Błąd podczas usuwania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE_PERMS"] = "Niewystarczające uprawnienia do usunięcia obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_FILES_SAVE_ERROR"] = "Błąd podczas zapisywania pliku obiektu magazynowego";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_PRODUCTS_SAVE_ERROR"] = "Błąd podczas zapisywania produktu obiektu magazynowego";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_NOT_FOUND"] = "Nie znaleziono obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PERMS"] = "Niewystarczające uprawnienia do wyświetlania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PRODUCTS_PERMS"] = "Niewystarczające uprawnienia do wyświetlania produktów.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE"] = "Błąd podczas aktualizowania obiektu magazynowego.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE_PERMS"] = "Niewystarczające uprawnienia do edycji obiektu magazynowego.";
