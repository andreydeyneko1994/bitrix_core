<?
$MESS["CRM_JS_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_JS_BUTTON_OK"] = "Tak";
$MESS["CRM_JS_DELETE"] = "Usuń";
$MESS["CRM_JS_DELETE_CONFIRM"] = "Czy na pewno usunąć ten element?";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "Usuń element";
$MESS["CRM_JS_EDIT"] = "Edycja";
$MESS["CRM_JS_ERROR"] = "Błąd";
$MESS["CRM_JS_ERROR_DELETE"] = "Błąd podczas usuwania elementu.";
$MESS["CRM_JS_GRID_FIELDS"] = "Widoczne pola";
$MESS["CRM_JS_GRID_FILTER"] = "Konfiguruj filtr";
$MESS["CRM_JS_GRID_SORT"] = "Sortuj po";
$MESS["CRM_JS_MORE"] = "Więcej ...";
?>