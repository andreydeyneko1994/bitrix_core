<?
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_COMPANY_DELETE_ERROR"] = "Błąd podczas usuwania elementu.";
$MESS["CRM_COMPANY_ID_NOT_DEFINED"] = "ID firmy nie został odnaleziony";
$MESS["CRM_COMPANY_NOT_FOUND"] = "Firma nie została odnaleziona.";
?>