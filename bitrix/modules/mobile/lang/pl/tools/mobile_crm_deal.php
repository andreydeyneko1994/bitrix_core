<?
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Błąd podczas usuwania elementu.";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Błąd zmiany statusu";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "ID deala nie został odnaleziony.";
$MESS["CRM_DEAL_NOT_FOUND"] = "Deal nie został odnaleziony.";
?>