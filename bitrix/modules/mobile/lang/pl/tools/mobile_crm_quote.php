<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_QUOTE_DELETE_ERROR"] = "Błąd podczas usuwania elementu.";
$MESS["CRM_QUOTE_ERROR_CHANGE_STATUS"] = "Błąd zmiany statusu";
$MESS["CRM_QUOTE_ID_NOT_DEFINED"] = "ID oferty nie został odnaleziony";
$MESS["CRM_QUOTE_NOT_FOUND"] = "Oferta nie została odnaleziona.";
?>