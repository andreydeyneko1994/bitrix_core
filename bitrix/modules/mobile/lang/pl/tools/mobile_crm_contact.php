<?
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "Błąd podczas usuwania elementu.";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "ID kontaktu nie został odnaleziony";
$MESS["CRM_CONTACT_NOT_FOUND"] = "Nie odnaleziono kontaktu.";
?>