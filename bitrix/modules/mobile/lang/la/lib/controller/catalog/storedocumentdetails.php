<?php
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD"] = "Error al crear el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD_PERMS"] = "Permisos insuficientes para crear los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_BARCODE_ALREADY_EXISTS"] = "El código de barras \"#BARCODE#\" ya está asignado a un producto diferente en el catálogo.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION"] = "Error al cancelar el procesamiento de los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION_PERMS"] = "Permisos insuficientes para cancelar el procesamiento de los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT"] = "Error al procesar el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT_PERMS"] = "Permisos insuficientes para el procesamiento.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE"] = "Error al eliminar el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE_PERMS"] = "Permisos insuficientes para eliminar los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_FILES_SAVE_ERROR"] = "Error al guardar el objeto del inventario";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_PRODUCTS_SAVE_ERROR"] = "Error al guardar los productos del objeto del inventario";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_NOT_FOUND"] = "No se encontró el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PERMS"] = "Permisos insuficientes para ver los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PRODUCTS_PERMS"] = "Permisos insuficientes para ver los productos.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE"] = "Error al actualizar el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE_PERMS"] = "Permisos insuficientes para editar los objetos del inventario.";
