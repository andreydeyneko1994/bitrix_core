<?php
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION"] = "Permisos insuficientes para cancelar el procesamiento de los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION_PERMS"] = "Permisos insuficientes para cancelar el procesamiento de los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT"] = "Error al procesar el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT_PERMS"] = "Permisos insuficientes para procesar los objetos del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE"] = "Error al eliminar el objeto del inventario.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE_PERMS"] = "Permisos insuficientes para cancelar el procesamiento de los objetos del inventario.";
