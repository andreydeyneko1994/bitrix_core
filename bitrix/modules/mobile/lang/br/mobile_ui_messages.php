<?
$MESS["MUI_B24DISK"] = "Bitrix24.Drive";
$MESS["MUI_CAMERA_ROLL"] = "Tirar foto";
$MESS["MUI_CANCEL"] = "Cancelar";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "Arquivos";
$MESS["MUI_CHOOSE_PHOTO"] = "Selecionar da galeria";
$MESS["MUI_COPY"] = "Copiar";
$MESS["MUI_COPY_TEXT"] = "Copiar texto";
$MESS["MUI_PROCESSING"] = "Processando...";
$MESS["MUI_TEXT_COPIED"] = "O texto foi copiado para a Área de Transferência";
?>