<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_QUOTE_DELETE_ERROR"] = "Ocorreu um erro ao excluir um objeto.";
$MESS["CRM_QUOTE_ERROR_CHANGE_STATUS"] = "Erro ao alterar o status";
$MESS["CRM_QUOTE_ID_NOT_DEFINED"] = "O ID do orçamento não foi encontrado";
$MESS["CRM_QUOTE_NOT_FOUND"] = "O orçamento não foi encontrado.";
?>