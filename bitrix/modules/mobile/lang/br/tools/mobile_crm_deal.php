<?
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Ocorreu um erro ao excluir um objeto.";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Erro ao alterar o status";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "O ID do negócio não foi encontrado.";
$MESS["CRM_DEAL_NOT_FOUND"] = "O negócio não foi encontrado.";
?>