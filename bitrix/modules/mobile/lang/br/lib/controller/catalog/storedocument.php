<?php
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION"] = "Permissões insuficientes para cancelar processamento do objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION_PERMS"] = "Permissões insuficientes para cancelar processamento do objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT"] = "Erro ao processar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT_PERMS"] = "Permissões insuficientes para processar objetos de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE"] = "Erro ao excluir objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE_PERMS"] = "Permissões insuficientes para cancelar processamento do objeto de inventário.";
