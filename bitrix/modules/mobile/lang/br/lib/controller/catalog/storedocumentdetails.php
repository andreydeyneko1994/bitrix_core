<?php
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD"] = "Erro ao criar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD_PERMS"] = "Permissões insuficientes para criar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_BARCODE_ALREADY_EXISTS"] = "O código de barras \"#BARCODE#\" já está atribuído a um produto diferente no catálogo.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION"] = "Erro ao cancelar processamento do objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION_PERMS"] = "Permissões insuficientes para cancelar processamento do objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT"] = "Erro ao processar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT_PERMS"] = "Permissões insuficientes para processamento.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE"] = "Erro ao excluir o objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE_PERMS"] = "Permissões insuficientes para excluir objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_FILES_SAVE_ERROR"] = "Erro ao salvar arquivo do objeto de inventário";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_PRODUCTS_SAVE_ERROR"] = "Erro ao salvar produtos do objeto de inventário";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_NOT_FOUND"] = "O objeto de inventário não foi encontrado.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PERMS"] = "Permissões insuficientes para visualizar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PRODUCTS_PERMS"] = "Permissões insuficientes para visualizar produtos.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE"] = "Erro ao atualizar objeto de inventário.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE_PERMS"] = "Permissões insuficientes para editar objeto de inventário.";
