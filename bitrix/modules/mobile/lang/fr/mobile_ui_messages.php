<?
$MESS["MUI_B24DISK"] = "Bitrix24.Drive";
$MESS["MUI_CAMERA_ROLL"] = "Prendre une photo";
$MESS["MUI_CANCEL"] = "Annuler";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "Fichiers";
$MESS["MUI_CHOOSE_PHOTO"] = "Sélectionner à partir de la galerie";
$MESS["MUI_COPY"] = "Copier";
$MESS["MUI_COPY_TEXT"] = "Copier texte";
$MESS["MUI_PROCESSING"] = "Traitement...";
$MESS["MUI_TEXT_COPIED"] = "Le texte a été copié dans le Presse-papiers";
?>