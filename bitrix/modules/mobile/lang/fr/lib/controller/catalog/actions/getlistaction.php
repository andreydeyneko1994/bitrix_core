<?php
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DOCUMENTS_NOT_FOUND"] = "Les entités sont introuvables.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_READ_PERMISSIONS"] = "Droits insuffisants pour voir les entités.";
