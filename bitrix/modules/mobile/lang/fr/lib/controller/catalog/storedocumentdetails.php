<?php
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD"] = "Erreur lors de la création de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_ADD_PERMS"] = "Droits insuffisants pour créer un objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_BARCODE_ALREADY_EXISTS"] = "Le code-barres \"#BARCODE#\" est déjà attribué à un autre produit dans le catalogue.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION"] = "Erreur lors de l'annulation du traitement de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CANCELLATION_PERMS"] = "Droits insuffisants pour annuler le traitement des objets d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT"] = "Erreur lors du traitement de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_CONDUCT_PERMS"] = "Droits insuffisants pour le traitement.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE"] = "Erreur lors de la suppression de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DELETE_PERMS"] = "Droits insuffisants pour supprimer l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_FILES_SAVE_ERROR"] = "Erreur lors de l'enregistrement du fichier de l'objet d'inventaire";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_DOCUMENT_PRODUCTS_SAVE_ERROR"] = "Erreur lors de l'enregistrement des produits de l'objet d'inventaire";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_NOT_FOUND"] = "L'objet d'inventaire est introuvable.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PERMS"] = "Droits insuffisants pour visualiser l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_READ_PRODUCTS_PERMS"] = "Droits insuffisantes pour visualiser les produits.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE"] = "Erreur lors de la mise à jour de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_DETAILS_ERROR_UPDATE_PERMS"] = "Droits insuffisants pour modifier l'objet d'inventaire.";
