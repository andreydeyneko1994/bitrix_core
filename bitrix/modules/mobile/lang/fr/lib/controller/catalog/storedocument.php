<?php
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION"] = "Droits insuffisants pour annuler le traitement des objets d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CANCELLATION_PERMS"] = "Droits insuffisants pour annuler le traitement des objets d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT"] = "Erreur lors du traitement de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_CONDUCT_PERMS"] = "Droits insuffisants pour traiter les objets d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE"] = "Erreur lors de la suppression de l'objet d'inventaire.";
$MESS["MOBILE_CONTROLLER_CATALOG_ERROR_DELETE_PERMS"] = "Droits insuffisants pour annuler le traitement des objets d'inventaire.";
