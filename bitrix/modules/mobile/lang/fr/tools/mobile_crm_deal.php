<?
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Une erreur s'est produite lors de la suppression d'un objet.";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Erreur de changement du statut";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "ID de transaction introuvable";
$MESS["CRM_DEAL_NOT_FOUND"] = "Transaction introuvable.";
?>