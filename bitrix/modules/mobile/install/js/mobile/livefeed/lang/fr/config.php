<?php
$MESS["MOBILE_EXT_LIVEFEED_ALERT_ERROR_BUTTON"] = "Précédent";
$MESS["MOBILE_EXT_LIVEFEED_ALERT_ERROR_POST_NOT_FOUND_TEXT"] = "L'enregistrement est introuvable.";
$MESS["MOBILE_EXT_LIVEFEED_ALERT_ERROR_TITLE"] = "Erreur";
$MESS["MOBILE_EXT_LIVEFEED_COUNTER_TITLE_1"] = "nouveau message";
$MESS["MOBILE_EXT_LIVEFEED_COUNTER_TITLE_2"] = "nouveaux messages";
$MESS["MOBILE_EXT_LIVEFEED_COUNTER_TITLE_3"] = "nouveaux messages";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_ERROR_GET_DATA"] = "Impossible d'obtenir les données pour créer une tâche.";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_FAILURE_TITLE"] = "Erreur";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_SUCCESS_BUTTON_CANCEL"] = "Fermer";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_SUCCESS_BUTTON_OK"] = "OK";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_SUCCESS_DESCRIPTION"] = "Vous pouvez maintenant voir et modifier la tâche.";
$MESS["MOBILE_EXT_LIVEFEED_CREATE_TASK_SUCCESS_TITLE"] = "La tâche a été créée";
$MESS["MOBILE_EXT_LIVEFEED_DELETE_CONFIRM_BUTTON_CANCEL"] = "Annuler";
$MESS["MOBILE_EXT_LIVEFEED_DELETE_CONFIRM_BUTTON_OK"] = "Supprimer";
$MESS["MOBILE_EXT_LIVEFEED_DELETE_CONFIRM_DESCRIPTION"] = "Voulez-vous vraiment supprimer ce sujet ?";
$MESS["MOBILE_EXT_LIVEFEED_DELETE_CONFIRM_TITLE"] = "Supprimer le sujet";
$MESS["MOBILE_EXT_LIVEFEED_DETAIL_NEW_PULL_LOADING"] = "Chargement de nouveaux commentaires";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_ADD_POST"] = "Ajouter un sujet";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_GROUP_FILES"] = "Fichiers de groupe de travail";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_GROUP_TASKS"] = "Tâches de groupe de travail";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_PRESET_BIZPROC"] = "Processus d'entreprise";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_PRESET_FAVORITES"] = "Favoris";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_PRESET_IMPORTANT"] = "Annonces";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_PRESET_MY"] = "Mon activité";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_PRESET_WORK"] = "Travail";
$MESS["MOBILE_EXT_LIVEFEED_LIST_MENU_REFRESH"] = "Actualiser";
$MESS["MOBILE_EXT_LIVEFEED_NEW_PULL"] = "Tirer vers le bas pour actualiser";
$MESS["MOBILE_EXT_LIVEFEED_NEW_PULL_LOADING"] = "Chargement de nouveaux messages";
$MESS["MOBILE_EXT_LIVEFEED_NEW_PULL_RELEASE"] = "Relâcher pour actualiser";
$MESS["MOBILE_EXT_LIVEFEED_PLAYER_ERROR_MESSAGE"] = "Malheureusement, l'application ne peut lire ce fichier.<br />Vous pouvez <span class=\"disk-mobile-player-download\">le télécharger</span> et le lire depuis votre appareil.";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_CREATE_TASK"] = "Créer une tâche";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_CREATE_TASK_LINK"] = "Tâche créée sur base de #ENTITY#";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_DELETE"] = "Supprimer le sujet";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_EDIT"] = "Actualiser le sujet";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_FAVORITES_N"] = "Ajouter aux favoris";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_FAVORITES_Y"] = "Supprimer des favoris";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_FOLLOW_N"] = "Abonnement";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_FOLLOW_Y"] = "Désabonnement";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_GET_LINK"] = "Copier le lien";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_GET_LINK_SUCCESS"] = "Lien copié dans le presse-papiers";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_PINNED_N"] = "Épingler";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_PINNED_Y"] = "Détacher";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_REFRESH_COMMENTS"] = "Actualiser les commentaires";
$MESS["MOBILE_EXT_LIVEFEED_POST_MENU_SHARE"] = "Partager un sujet";
$MESS["MOBILE_EXT_LIVEFEED_POST_PINNED_CANCEL_BUTTON"] = "détacher";
$MESS["MOBILE_EXT_LIVEFEED_POST_PINNED_CANCEL_DESCRIPTION"] = "Elle est maintenant toujours en haut des actualités";
$MESS["MOBILE_EXT_LIVEFEED_POST_PINNED_CANCEL_TITLE"] = "Publication épinglée";
$MESS["MOBILE_EXT_LIVEFEED_PUBLICATION_ERROR"] = "Erreur lors de l'ajout de sujet";
$MESS["MOBILE_EXT_LIVEFEED_PUBLICATION_QUEUE_ITEM_TITLE"] = "Publier";
$MESS["MOBILE_EXT_LIVEFEED_PUBLICATION_QUEUE_SUCCESS_TITLE"] = "Publié";
$MESS["MOBILE_EXT_LIVEFEED_SHARE_TABLE_BUTTON_CANCEL"] = "Annuler";
$MESS["MOBILE_EXT_LIVEFEED_SHARE_TABLE_BUTTON_OK"] = "Sélectionner";
$MESS["MOBILE_EXT_LIVEFEED_USERS_LIST_TITLE"] = "Utilisateurs";
