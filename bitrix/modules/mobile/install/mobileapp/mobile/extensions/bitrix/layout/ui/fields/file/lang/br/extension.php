<?php
$MESS["FIELDS_FILE_ADD_FILE"] = "Adicionar arquivo";
$MESS["FIELDS_FILE_ADD_FILES"] = "Adicionar arquivos";
$MESS["FIELDS_FILE_ADD_IMAGE"] = "Adicionar imagem";
$MESS["FIELDS_FILE_ADD_VIDEO"] = "Adicionar vídeo";
$MESS["FIELDS_FILE_ATTACHMENTS_DIALOG_TITLE"] = "Arquivos anexados: #NUM#";
$MESS["FIELDS_FILE_B24_DISK"] = "Bitrix24.Drive";
$MESS["FIELDS_FILE_CAMERA"] = "Tirar uma foto";
$MESS["FIELDS_FILE_MEDIATEKA"] = "Selecionar da galeria";
$MESS["FIELDS_FILE_MEDIA_TYPE_ALERT_TEXT"] = "Erro ao enviar o arquivo";
$MESS["FIELDS_FILE_OPEN_GALLERY"] = "Visualizar";
