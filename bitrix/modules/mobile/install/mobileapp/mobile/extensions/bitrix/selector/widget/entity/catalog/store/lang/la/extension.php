<?php
$MESS["SELECTOR_COMPONENT_CREATE_STORE"] = "Crear un almacén";
$MESS["SELECTOR_COMPONENT_CREATING_STORE"] = "Creando el almacén...";
$MESS["SELECTOR_COMPONENT_PICK_STORE_2"] = "Almacén";
$MESS["SELECTOR_COMPONENT_START_TYPING_TO_SEARCH_STORE"] = "Empiece a escribir para encontrar un almacén";
