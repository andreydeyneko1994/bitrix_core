<?php
$MESS["SIMPLELIST_LIST_EMPTY"] = "A lista está vazia.";
$MESS["SIMPLELIST_PULL_NOTIFICATION_ADD"] = "Novos itens: %COUNT%";
$MESS["SIMPLELIST_PULL_NOTIFICATION_UPDATE"] = "Atualizar lista...";
$MESS["SIMPLELIST_SEARCH_EMPTY"] = "Não foram encontradas inserções.";
