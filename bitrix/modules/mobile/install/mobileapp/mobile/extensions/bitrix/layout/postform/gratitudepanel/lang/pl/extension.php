<?php
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_MEDALS_DIALOG_TITLE"] = "Odznaki";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_MEDALS_EMPLOYEES_SMALL_MORE"] = "+#NUM#";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_MENU_CANCEL"] = "Anuluj";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_MENU_DELETE"] = "Usuń";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_MENU_MEDAL"] = "Zmień odznakę";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDEPANEL_SELECT_EMPLOYEE_TITLE"] = "Wybierz pracowników";
