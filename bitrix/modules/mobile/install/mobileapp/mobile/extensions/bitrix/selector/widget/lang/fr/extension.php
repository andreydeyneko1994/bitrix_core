<?php
$MESS["PROVIDER_SEARCH_CREATE_PLACEHOLDER"] = "Trouver ou créer";
$MESS["PROVIDER_WIDGET_CREATE_ITEM"] = "Créer un article";
$MESS["PROVIDER_WIDGET_CREATING_ITEM"] = "Création d'un article...";
$MESS["PROVIDER_WIDGET_DONE"] = "Terminé";
$MESS["PROVIDER_WIDGET_START_TYPING_TO_SEARCH"] = "Commencez à saisir pour trouver des articles";
