<?
$MESS["SE_SYS_ALLOW_INVITE_USERS"] = "Zezwól wszystkim na zaproszenie";
$MESS["SE_SYS_ALLOW_INVITE_USERS_DESC"] = "Włączenie tej opcji pozwoli każdemu pracownikowi zarejestrowanemu na Bitrix24 zaprosić nowych pracowników.";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "Uruchom w tle";
$MESS["SE_SYS_INVITE"] = "Zaproszenia do Bitrix24";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "Zwiększ interwał między powiadomieniami";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "Użyj tej opcji, aby oszczędzać baterię. Będziesz otrzymywać mniej powiadomień o usługach i aktualizacji liczników. Nie wpłynie to na powiadomienia zależne od treści.";
$MESS["SE_SYS_TITLE"] = "Inne ustawienia";
?>