<?php
$MESS["PROVIDER_SEARCH_CREATE_PLACEHOLDER"] = "Encontrar o crear";
$MESS["PROVIDER_WIDGET_CREATE_ITEM"] = "Crear un elemento";
$MESS["PROVIDER_WIDGET_CREATING_ITEM"] = "Creando el elemento...";
$MESS["PROVIDER_WIDGET_DONE"] = "Listo";
$MESS["PROVIDER_WIDGET_START_TYPING_TO_SEARCH"] = "Empiece a escribir para buscar elementos";
