<?
$MESS["ACTION_DELETE"] = "Borrar";
$MESS["INVITE_USERS_ERROR"] = "Error";
$MESS["LOAD_MORE_RESULT"] = "Mostrar más";
$MESS["LOAD_MORE_USERS"] = "Carga más";
$MESS["RECENT_SEARCH"] = "Busqueda reciente";
$MESS["SEARCH_EMPTY_RESULT"] = "Desafortunadamente su solicitud de búsqueda no encontró nada.";
$MESS["SEARCH_LOADING"] = "Buscar...";
$MESS["SEARCH_PLACEHOLDER"] = "Ingrese nombre o departamento";
$MESS["USER_LIST_COMPANY"] = "Empleados";
$MESS["USER_LIST_CONTACTS"] = "Contactos";
$MESS["USER_LIST_NO_NAME"] = "Sin nombre";
$MESS["USER_LOADING"] = "Cargando...";
?>