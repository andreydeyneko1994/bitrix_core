<?php
$MESS["CSPL_CREATE_PRODUCT_IN_DESKTOP_VERSION"] = "Criar produto na versão completa";
$MESS["CSPL_MENU_CHOOSE_FROM_DB"] = "Selecionar do catálogo";
$MESS["CSPL_MENU_CREATE_ARTNUMBER"] = "Criar produto com S/N";
$MESS["CSPL_MENU_CREATE_SKU"] = "Criar SKU";
$MESS["CSPL_VALIDATION_ERROR_EMPTY_NAME"] = "O item ##NUM# não tem nome do produto";
