<?php
$MESS["CRM_DESKTOP_OPEN"] = "Ouvrez le CRM dans votre navigateur sur un ordinateur de bureau. Une fois que vous avez terminé la configuration initiale, vous pouvez revenir ici pour continuer à utiliser le CRM.";
$MESS["CRM_TITLE"] = "Commencez à utiliser le CRM en fournissant la configuration initiale et les canaux de connexion.";
$MESS["QR_EXTERNAL_AUTH"] = "Connexion avec un QR code";
