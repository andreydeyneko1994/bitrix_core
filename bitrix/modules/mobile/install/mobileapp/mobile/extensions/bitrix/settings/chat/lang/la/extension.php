<?
$MESS["SE_CHAT_AUTOPLAY_TITLE"] = "Auto-reproducción";
$MESS["SE_CHAT_AUTOPLAY_VIDEO_TITLE"] = "Vídeo";
$MESS["SE_CHAT_BACKGROUND_COLOR_CREAMY"] = "Cremoso";
$MESS["SE_CHAT_BACKGROUND_COLOR_DARK"] = "Oscuro";
$MESS["SE_CHAT_BACKGROUND_COLOR_LIGHT_GRAY"] = "Brillante";
$MESS["SE_CHAT_BACKGROUND_COLOR_LIGHT_GREEN"] = "Verde claro";
$MESS["SE_CHAT_BACKGROUND_COLOR_PINK"] = "Rosa";
$MESS["SE_CHAT_BACKGROUND_COLOR_TITLE"] = "Color de fondo";
$MESS["SE_CHAT_BACKGROUND_DESC"] = "El tema se aplicará después de volver a abrir el diálogo.";
$MESS["SE_CHAT_BACKGROUND_TITLE"] = "Temas";
$MESS["SE_CHAT_DESC"] = "Cualquier cambio que haga en la configuración de este grupo se aplicará la próxima vez que abra la ventana de diálogo.";
$MESS["SE_CHAT_ENABLE_TITLE"] = "Usar chat nuevo";
$MESS["SE_CHAT_HISTORY_SHOW_TITLE"] = "Mostrar el historial al invitar";
$MESS["SE_CHAT_HISTORY_TITLE"] = "Historial de chat";
$MESS["SE_CHAT_QUOTE_ENABLE_TITLE"] = "Habilitar";
$MESS["SE_CHAT_QUOTE_FROM_LEFT_TITLE"] = "A la derecha";
$MESS["SE_CHAT_QUOTE_FROM_RIGHT_TITLE"] = "A la izquierda";
$MESS["SE_CHAT_QUOTE_TITLE"] = "Respuesta con la opción deslizar para citar";
$MESS["SE_CHAT_TITLE"] = "Chats";
?>