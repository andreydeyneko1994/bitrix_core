<?php
$MESS["PROVIDER_SEARCH_CREATE_PLACEHOLDER"] = "Знайти чи створити";
$MESS["PROVIDER_WIDGET_CREATE_ITEM"] = "Створити елемент";
$MESS["PROVIDER_WIDGET_CREATING_ITEM"] = "Створення елемента...";
$MESS["PROVIDER_WIDGET_DONE"] = "Готово";
$MESS["PROVIDER_WIDGET_START_TYPING_TO_SEARCH"] = "Почніть друкувати для пошуку елементів";
