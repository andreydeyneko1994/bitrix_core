<?
$MESS["RECENT_SEARCH"] = "Pesquisa recente";
$MESS["USER_DISK_ACCESS_DENIED"] = "Você não tem permissão para visualizar os arquivos deste usuário.";
$MESS["USER_DISK_CONFIRM_NO"] = "Não";
$MESS["USER_DISK_CONFIRM_YES"] = "Sim";
$MESS["USER_DISK_EMPTY_FOLDER"] = "A pasta está vazia";
$MESS["USER_DISK_ERROR"] = "Não é possível obter a lista de arquivos. Algo deu errado.";
$MESS["USER_DISK_FILE_NOT_SEND"] = "Erro ao enviar o arquivo";
$MESS["USER_DISK_FILE_SENT"] = "O arquivo foi enviado para o usuário";
$MESS["USER_DISK_GET_PUBLIC_LINK"] = "Link público";
$MESS["USER_DISK_LINK_COPIED"] = "O link público foi copiado para a Área de Transferência";
$MESS["USER_DISK_LINK_COPIED_FAIL"] = "Infelizmente, não conseguimos criar um link público. Por favor, tente novamente mais tarde.";
$MESS["USER_DISK_MENU_SORT"] = "Classificar";
$MESS["USER_DISK_MENU_SORT_DATE_CREATE"] = "Por data de modificação";
$MESS["USER_DISK_MENU_SORT_DATE_UPDATE"] = "Por data de criação";
$MESS["USER_DISK_MENU_SORT_MIX"] = "Classificação mista";
$MESS["USER_DISK_MENU_SORT_NAME"] = "Por nome";
$MESS["USER_DISK_MENU_SORT_TYPE"] = "Por tipo";
$MESS["USER_DISK_MENU_UPLOAD"] = "Carregar arquivo...";
$MESS["USER_DISK_OPEN"] = "Abrir";
$MESS["USER_DISK_PUBLIC_LINK_GETTING"] = "Criando link público...";
$MESS["USER_DISK_REMOVE"] = "Excluir";
$MESS["USER_DISK_REMOVE_FILE_CONFIRM"] = "Você deseja mover o arquivo \"%@\" para a Lixeira?";
$MESS["USER_DISK_REMOVE_FOLDER_CONFIRM"] = "Você deseja mover a pasta \"%@\" para a Lixeira?";
$MESS["USER_DISK_ROLLBACK"] = "Recuperar o item excluído \"%@\"?";
$MESS["USER_DISK_SEND_TO_USER"] = "Compartilhar com o usuário";
$MESS["USER_DISK_SHARE"] = "Compartilhar";
?>