<?php
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ATTACHMENTS_DIALOG_TITLE"] = "Pliki w poście: #NUM#";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_BACKGROUNDS_DIALOG_TITLE"] = "Wybierz tło";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_BACKGROUND_CONFIRM_MESSAGE"] = "Zamierzasz zmienić typ posta. Tło posta zostanie zresetowane";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_CONFIRM_BUTTON_CANCEL"] = "Anuluj";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_CONFIRM_BUTTON_OK"] = "OK";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_CONFIRM_TITLE"] = "Ostrzeżenie";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_IMPORTANT_CONFIRM_MESSAGE"] = "Zamierzasz zmienić typ posta. Dane ważności posta zostaną utracone";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_MEDAL_CONFIRM_MESSAGE"] = "Zamierzasz zmienić typ posta. Dane nagrody zostaną utracone";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CHANGETYPE_VOTE_CONFIRM_MESSAGE"] = "Zamierzasz zmienić typ posta. Dane ankiety powiązanej z postem zostaną utracone";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CLOSE_CONFIRM_BUTTON_CANCEL"] = "Anuluj";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CLOSE_CONFIRM_BUTTON_OK"] = "OK";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CLOSE_CONFIRM_MESSAGE"] = "Jeśli zamkniesz formularz, Twoja wiadomość zostanie utracona. Kontynuować?";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_CLOSE_CONFIRM_TITLE"] = "Ostrzeżenie";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_DESTINATIONS_EMPTY"] = "Wybierz co najmniej jednego odbiorcę";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_DIALOG_MENTION_TITLE"] = "Wzmianka";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_GRATITUDE_EMPLOYEE_EMPTY"] = "Nie wybrano żadnego pracownika do nagrodzenia.";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_PANEL_ITEM_SELECTOR_VALUE_ALL"] = "Do wszystkich pracowników";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_TEXT_EMPTY"] = "Wprowadź tekst postu na kanale";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_TITLE_PLACEHOLDER"] = "Wprowadź tytuł postu na kanale";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_TITLE_PLACEHOLDER2"] = "Pokaż tytuł (jeżeli jest wymagany)";
