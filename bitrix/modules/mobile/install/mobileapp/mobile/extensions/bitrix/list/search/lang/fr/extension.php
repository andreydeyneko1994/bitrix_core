<?
$MESS["ACTION_DELETE"] = "Supprimer";
$MESS["LOAD_MORE_RESULT"] = "Plus de résultats";
$MESS["RECENT_SEARCH"] = "Recherche récente";
$MESS["SEARCH_EMPTY_RESULT"] = "Malheureusement, rien n'a été trouvé.";
$MESS["SEARCH_LOADING"] = "Recherche…";
?>