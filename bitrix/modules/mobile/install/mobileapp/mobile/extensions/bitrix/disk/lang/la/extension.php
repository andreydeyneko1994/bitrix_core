<?
$MESS["RECENT_SEARCH"] = "Busqueda reciente";
$MESS["USER_DISK_ACCESS_DENIED"] = "No tiene permiso para ver los archivos de este usuario.";
$MESS["USER_DISK_CONFIRM_NO"] = "No";
$MESS["USER_DISK_CONFIRM_YES"] = "Sí";
$MESS["USER_DISK_EMPTY_FOLDER"] = "La carpeta está vacía";
$MESS["USER_DISK_ERROR"] = "No se puede obtener la lista de archivos. Algo salió mal.";
$MESS["USER_DISK_FILE_NOT_SEND"] = "Error al enviar el archivo";
$MESS["USER_DISK_FILE_SENT"] = "El archivo se envió al usuario";
$MESS["USER_DISK_GET_PUBLIC_LINK"] = "Enlace público";
$MESS["USER_DISK_LINK_COPIED"] = "El enlace público se copió al portapapeles";
$MESS["USER_DISK_LINK_COPIED_FAIL"] = "Lamentablemente no pudimos crear un enlace público. Intente de nuevo más tarde.";
$MESS["USER_DISK_MENU_SORT"] = "Ordenar";
$MESS["USER_DISK_MENU_SORT_DATE_CREATE"] = "Por fecha de modificación";
$MESS["USER_DISK_MENU_SORT_DATE_UPDATE"] = "Por fecha de creación";
$MESS["USER_DISK_MENU_SORT_MIX"] = "Clasificación mixta";
$MESS["USER_DISK_MENU_SORT_NAME"] = "Por nombre";
$MESS["USER_DISK_MENU_SORT_TYPE"] = "Por tipo";
$MESS["USER_DISK_MENU_UPLOAD"] = "Cargar archivo...";
$MESS["USER_DISK_OPEN"] = "Abrir";
$MESS["USER_DISK_PUBLIC_LINK_GETTING"] = "Creando enlace público...";
$MESS["USER_DISK_REMOVE"] = "Eliminar";
$MESS["USER_DISK_REMOVE_FILE_CONFIRM"] = "¿Desea mover la carpeta \"%@\" a la Papelera de reciclaje?";
$MESS["USER_DISK_REMOVE_FOLDER_CONFIRM"] = "¿Desea mover la carpeta \"%@\" a la Papelera de reciclaje?";
$MESS["USER_DISK_ROLLBACK"] = "¿Recuperar el elemento eliminado \"%@\"?";
$MESS["USER_DISK_SEND_TO_USER"] = "Compartir con el usuario";
$MESS["USER_DISK_SHARE"] = "Compartir";
?>