<?
$MESS["SE_SYNC_CAL_TITLE"] = "Synchroniser le calendrier";
$MESS["SE_SYNC_CONTACTS_TITLE"] = "Synchroniser les contacts";
$MESS["SE_SYNC_PROFILE_DESCRIPTION"] = "La synchronisation de contacts et de calendrier est fournie en ajoutant les profils de service à votre iOS. Pour arrêter la synchronisation, ouvrez l'affichage des profils et supprimez celui de la synchronisation. Le nom du profil est le même que votre domaine Bitrix24.";
$MESS["SE_SYNC_SUBTITLE_TITLE"] = "Calendriers et contacts";
$MESS["SE_SYNC_TITLE"] = "Synchroniser";
?>