<?php
$MESS["CSPL_CREATE_PRODUCT_IN_DESKTOP_VERSION"] = "Створити товар у повній версії";
$MESS["CSPL_MENU_CHOOSE_FROM_DB"] = "Обрати з каталогу";
$MESS["CSPL_MENU_CREATE_ARTNUMBER"] = "Товар із серійним номером";
$MESS["CSPL_MENU_CREATE_SKU"] = "Товар із варіаціями";
$MESS["CSPL_VALIDATION_ERROR_EMPTY_NAME"] = "Позиція №#NUM# не містить найменування товару";
