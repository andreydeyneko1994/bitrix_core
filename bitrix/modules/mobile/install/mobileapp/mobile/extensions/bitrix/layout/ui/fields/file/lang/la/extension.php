<?php
$MESS["FIELDS_FILE_ADD_FILE"] = "Agregar archivo";
$MESS["FIELDS_FILE_ADD_FILES"] = "Agregar archivos";
$MESS["FIELDS_FILE_ADD_IMAGE"] = "Agregar imagen";
$MESS["FIELDS_FILE_ADD_VIDEO"] = "Agregar video";
$MESS["FIELDS_FILE_ATTACHMENTS_DIALOG_TITLE"] = "Archivos adjuntos: #NUM#";
$MESS["FIELDS_FILE_B24_DISK"] = "Bitrix24.Drive";
$MESS["FIELDS_FILE_CAMERA"] = "Tomar una foto";
$MESS["FIELDS_FILE_MEDIATEKA"] = "Seleccionar de la galería";
$MESS["FIELDS_FILE_MEDIA_TYPE_ALERT_TEXT"] = "Error al subir el archivo";
$MESS["FIELDS_FILE_OPEN_GALLERY"] = "Ver";
