<?
$MESS["SE_SYNC_CAL_TITLE"] = "Sincronizar calendario";
$MESS["SE_SYNC_CONTACTS_TITLE"] = "Sincronizar contactos";
$MESS["SE_SYNC_PROFILE_DESCRIPTION"] = "La sincronización de contacto y calendario se proporciona al agregar perfiles de servicio a su iOS. Para detener la sincronización, abra la vista de perfiles y elimine el perfil de sincronización. El nombre del perfil es el mismo que su dominio de Bitrix24.";
$MESS["SE_SYNC_SUBTITLE_TITLE"] = "Calendarios y contactos";
$MESS["SE_SYNC_TITLE"] = "Sincronizar";
?>