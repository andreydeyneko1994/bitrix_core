<?php
$MESS["CSPL_PRICE_EMPTY"] = "No especificado";
$MESS["CSPL_PURCHASE_PRICE"] = "Precio de compra";
$MESS["CSPL_SELLING_PRICE"] = "Precio de venta";
$MESS["CSPL_STORE_AMOUNT_INCREMENT"] = "Recibo de existencias";
$MESS["CSPL_STORE_EMPTY"] = "No se seleccionó ningún almacén";
