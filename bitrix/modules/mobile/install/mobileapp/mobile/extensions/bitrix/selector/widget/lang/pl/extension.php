<?php
$MESS["PROVIDER_SEARCH_CREATE_PLACEHOLDER"] = "Znajdź lub utwórz";
$MESS["PROVIDER_WIDGET_CREATE_ITEM"] = "Utwórz pozycję";
$MESS["PROVIDER_WIDGET_CREATING_ITEM"] = "Tworzenie pozycji...";
$MESS["PROVIDER_WIDGET_DONE"] = "Gotowe";
$MESS["PROVIDER_WIDGET_START_TYPING_TO_SEARCH"] = "Zacznij wprowadzanie, aby znaleźć pozycje.";
