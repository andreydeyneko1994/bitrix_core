<?php
$MESS["FIELDS_FILE_ADD_FILE"] = "Dodaj plik";
$MESS["FIELDS_FILE_ADD_IMAGE"] = "Dodaj obraz";
$MESS["FIELDS_FILE_ADD_VIDEO"] = "Dodaj wideo";
$MESS["FIELDS_FILE_ATTACHMENTS_DIALOG_TITLE"] = "Załączone pliki: #NUM#";
$MESS["FIELDS_FILE_B24_DISK"] = "Bitrix24.Drive";
$MESS["FIELDS_FILE_CAMERA"] = "Zrób zdjęcie";
$MESS["FIELDS_FILE_MEDIATEKA"] = "Wybierz z galerii";
$MESS["FIELDS_FILE_MEDIA_TYPE_ALERT_TEXT"] = "Błąd podczas przesyłania pliku";
$MESS["FIELDS_FILE_OPEN_GALLERY"] = "Wyświetl";
