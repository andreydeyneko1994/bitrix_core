<?php
$MESS["MOBILE_EXT_NOTIFICATION_ITEM_AUTHOR_SYSTEM"] = "Mensaje del sistema";
$MESS["MOBILE_EXT_NOTIFICATION_ITEM_FOLD"] = "Contraer";
$MESS["MOBILE_EXT_NOTIFICATION_ITEM_MORE_USERS"] = "#COUNT# personas más";
$MESS["MOBILE_EXT_NOTIFICATION_ITEM_UNFOLD"] = "Ampliar";
$MESS["MOBILE_EXT_NOTIFICATION_ITEM_USERS_LIST"] = "Usuarios";
