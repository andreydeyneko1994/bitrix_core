<?php
$MESS["CSPL_CREATE_PRODUCT_IN_DESKTOP_VERSION"] = "Utwórz produkt w pełnej wersji";
$MESS["CSPL_MENU_CHOOSE_FROM_DB"] = "Wybierz z katalogu";
$MESS["CSPL_MENU_CREATE_ARTNUMBER"] = "Utwórz produkt z S/N";
$MESS["CSPL_MENU_CREATE_SKU"] = "Utwórz kod SKU";
$MESS["CSPL_VALIDATION_ERROR_EMPTY_NAME"] = "Pozycja ##NUM# nie ma nazwy produktu";
