<?php
$MESS["MOBILE_PROJECT_LIST_EMPTY"] = "Nenhum grupo foi encontrado";
$MESS["MOBILE_PROJECT_LIST_ERROR"] = "Não é possível obter lista de grupos";
$MESS["MOBILE_PROJECT_LIST_TAB_NEWS"] = "Notícias";
$MESS["MOBILE_PROJECT_LIST_TAB_TASKS"] = "Tarefas";
