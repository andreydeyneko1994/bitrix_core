<?php
$MESS["CSPL_PRICE_EMPTY"] = "Non spécifié";
$MESS["CSPL_PURCHASE_PRICE"] = "Prix d'achat";
$MESS["CSPL_SELLING_PRICE"] = "Prix de vente";
$MESS["CSPL_STORE_AMOUNT_INCREMENT"] = "Réception en stock";
$MESS["CSPL_STORE_EMPTY"] = "Aucun entrepôt sélectionné";
