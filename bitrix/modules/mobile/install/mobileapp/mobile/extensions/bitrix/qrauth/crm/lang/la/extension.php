<?php
$MESS["CRM_DESKTOP_OPEN"] = "Abra el CRM en su navegador en una computadora de escritorio. Una vez que haya finalizado la configuración inicial, puede volver aquí para seguir usando el CRM.";
$MESS["CRM_TITLE"] = "Comience a usar el CRM proporcionando la configuración inicial y conectando los canales.";
$MESS["QR_EXTERNAL_AUTH"] = "Iniciar sesión con código QR";
