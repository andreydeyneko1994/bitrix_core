<?php
$MESS["SELECTOR_COMPONENT_CREATE_CONTRACTOR"] = "Créer un fournisseur";
$MESS["SELECTOR_COMPONENT_CREATING_CONTRACTOR"] = "Création d'un fournisseur...";
$MESS["SELECTOR_COMPONENT_PICK_CONTRACTOR_2"] = "Fournisseur";
$MESS["SELECTOR_COMPONENT_START_TYPING_TO_SEARCH_CONTRACTOR"] = "Commencez à saisir pour trouver une fournisseur";
