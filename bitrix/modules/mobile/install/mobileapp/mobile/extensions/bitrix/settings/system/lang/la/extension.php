<?
$MESS["SE_SYS_ALLOW_INVITE_USERS"] = "Cualquiera puede invitar";
$MESS["SE_SYS_ALLOW_INVITE_USERS_DESC"] = "Si habilita esta opción, cualquier empleado registrado en Bitrix24 podrá invitar a nuevos empleados.";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "Ejecutar en segundo plano";
$MESS["SE_SYS_INVITE"] = "Invitaciones a Bitrix24";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "Aumentar el intervalo de notificación";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "Utilice esta opción para ahorrar batería. Obtendrá menos notificaciones sobre el servicio y actualizaciones del contador. Esto no afectará a las notificaciones que estén basadas en el contenido.";
$MESS["SE_SYS_TITLE"] = "Otras configuraciones";
?>