<?php
$MESS["SIMPLELIST_LIST_EMPTY"] = "La liste est vide.";
$MESS["SIMPLELIST_PULL_NOTIFICATION_ADD"] = "Nouveaux articles : %COUNT%";
$MESS["SIMPLELIST_PULL_NOTIFICATION_UPDATE"] = "Actualiser la liste...";
$MESS["SIMPLELIST_SEARCH_EMPTY"] = "Aucune entrée n'a été trouvée.";
