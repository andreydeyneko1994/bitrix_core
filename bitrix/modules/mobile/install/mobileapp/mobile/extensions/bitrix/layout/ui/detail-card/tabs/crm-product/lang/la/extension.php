<?php
$MESS["CSPL_CREATE_PRODUCT_IN_DESKTOP_VERSION"] = "Crear un producto en la versión completa";
$MESS["CSPL_MENU_CHOOSE_FROM_DB"] = "Seleccionar del catálogo";
$MESS["CSPL_MENU_CREATE_ARTNUMBER"] = "Crear un producto con S/N";
$MESS["CSPL_MENU_CREATE_SKU"] = "Crear un SKU";
$MESS["CSPL_VALIDATION_ERROR_EMPTY_NAME"] = "El artículo ##NUM# no tiene nombre de producto";
