<?php
$MESS["CRM_DESKTOP_OPEN"] = "Otwórz CRM w przeglądarce na komputerze stacjonarnym. Po zakończeniu wstępnej konfiguracji możesz wrócić tutaj, aby kontynuować korzystanie z CRM.";
$MESS["CRM_TITLE"] = "Zacznij korzystać z CRM, dostarczając wstępną konfigurację i kanały połączeń.";
$MESS["QR_EXTERNAL_AUTH"] = "Zaloguj się za pomocą kodu QR";
