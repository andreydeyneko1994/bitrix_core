<?php
$MESS["MOBILE_PROJECT_LIST_EMPTY"] = "Aucun groupe n'a été trouvé";
$MESS["MOBILE_PROJECT_LIST_ERROR"] = "Impossible de récupérer la liste des groupes";
$MESS["MOBILE_PROJECT_LIST_TAB_NEWS"] = "News";
$MESS["MOBILE_PROJECT_LIST_TAB_TASKS"] = "Tâches";
