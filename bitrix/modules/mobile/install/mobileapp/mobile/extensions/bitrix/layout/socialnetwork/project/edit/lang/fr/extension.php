<?php
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_CANCEL"] = "Annuler";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_ERROR_AVATAR_IS_UPLOADING"] = "Icône du projet toujours en cours de téléchargement";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_ERROR_NO_TITLE"] = "Le nom du projet n'est pas spécifié";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_HEADER_TITLE"] = "Éditer le projet";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_SAVE"] = "Enregistrer";
