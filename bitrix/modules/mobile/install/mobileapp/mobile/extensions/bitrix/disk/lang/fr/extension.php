<?
$MESS["RECENT_SEARCH"] = "Recherche récente";
$MESS["USER_DISK_ACCESS_DENIED"] = "Vous n'avez pas l'autorisation de consulter les fichiers de cet utilisateur.";
$MESS["USER_DISK_CONFIRM_NO"] = "Non";
$MESS["USER_DISK_CONFIRM_YES"] = "Oui";
$MESS["USER_DISK_EMPTY_FOLDER"] = "Le répertoire est vide";
$MESS["USER_DISK_ERROR"] = "Impossible de récupérer la liste des fichiers. Un problème est survenu.";
$MESS["USER_DISK_FILE_NOT_SEND"] = "Erreur lors de l'envoi du fichier";
$MESS["USER_DISK_FILE_SENT"] = "Le fichier a été envoyé à l'utilisateur";
$MESS["USER_DISK_GET_PUBLIC_LINK"] = "Lien public";
$MESS["USER_DISK_LINK_COPIED"] = "Le lien public a été copié dans le Presse-papiers";
$MESS["USER_DISK_LINK_COPIED_FAIL"] = "Malheureusement, nous n'avons pas pu créer de lien public. Veuillez réessayer plus tard.";
$MESS["USER_DISK_MENU_SORT"] = "Trier";
$MESS["USER_DISK_MENU_SORT_DATE_CREATE"] = "Par date de modification";
$MESS["USER_DISK_MENU_SORT_DATE_UPDATE"] = "Par date de création";
$MESS["USER_DISK_MENU_SORT_MIX"] = "Tri mixte";
$MESS["USER_DISK_MENU_SORT_NAME"] = "Par nom";
$MESS["USER_DISK_MENU_SORT_TYPE"] = "Par type";
$MESS["USER_DISK_MENU_UPLOAD"] = "Télécharger le fichier...";
$MESS["USER_DISK_OPEN"] = "Ouvrir";
$MESS["USER_DISK_PUBLIC_LINK_GETTING"] = "Création d'un lien public...";
$MESS["USER_DISK_REMOVE"] = "Supprimer";
$MESS["USER_DISK_REMOVE_FILE_CONFIRM"] = "Voulez-vous déplacer le fichier \"%@\" vers la Corbeille ?";
$MESS["USER_DISK_REMOVE_FOLDER_CONFIRM"] = "Voulez-vous déplacer le dossier \"%@\" vers la Corbeille ?";
$MESS["USER_DISK_ROLLBACK"] = "Récupérer l'élément supprimé \"%@\" ?";
$MESS["USER_DISK_SEND_TO_USER"] = "Partager avec l'utilisateur";
$MESS["USER_DISK_SHARE"] = "Partager";
?>