<?php
$MESS["CSPL_CREATE_PRODUCT_IN_DESKTOP_VERSION"] = "Créez un produit dans la version complète";
$MESS["CSPL_MENU_CHOOSE_FROM_DB"] = "Sélectionner à partir du catalogue";
$MESS["CSPL_MENU_CREATE_ARTNUMBER"] = "Créer un produit avec un S/N";
$MESS["CSPL_MENU_CREATE_SKU"] = "Créer une UGS";
$MESS["CSPL_VALIDATION_ERROR_EMPTY_NAME"] = "L'article ##NUM# n'a pas de nom de produit";
