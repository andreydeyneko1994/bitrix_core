<?php
$MESS["ACCEPT_QR_AUTH"] = "Sí, continuar";
$MESS["AUTH_WAIT"] = "Iniciando sesión en...";
$MESS["DECLINE_QR_AUTH"] = "No";
$MESS["GET_MORE"] = "Obtenga aún más herramientas de negocios con la versión completa de Bitrix24.";
$MESS["OPEN_BROWSER"] = "Abra [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] en su computadora";
$MESS["QR_HOW_TO_AUTH"] = "¿Cómo abro la versión completa? ";
$MESS["QR_SCANNER_HINT"] = "Esta función está disponible solo en la versión completa";
$MESS["QR_WARNING"] = "Va a iniciar sesión en el Bitrix24 de [B]#DOMAIN#[/B] usando el código QR. ¿Desea continuar?";
$MESS["SCAN_QR"] = "Escanee el [B]código QR[/B] con la cámara de su móvil";
$MESS["SCAN_QR_BUTTON"] = "Escanear el código QR";
$MESS["STEP_CAMERA_TITLE"] = "Escanee el código QR con la cámara de su móvil";
$MESS["STEP_OPEN_SITE"] = "Abra [B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B] en su navegador";
$MESS["STEP_PRESS_CLOUD"] = "Haga clic en [B]Iniciar sesión[/B] y [IMG WIDTH=\"22\" HEIGHT=\"22\"]#URL#[/IMG][B]Código QR[/B]";
$MESS["STEP_PRESS_SELF_HOSTED"] = "Haga clic en [B]Iniciar sesión con un código QR[/B]";
$MESS["STEP_SCAN"] = "Escanee el código QR";
$MESS["WRONG_QR"] = "El código QR es incorrecto";
