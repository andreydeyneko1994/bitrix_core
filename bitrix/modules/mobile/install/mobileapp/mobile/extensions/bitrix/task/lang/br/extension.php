<?php
$MESS["MOBILE_TASKS_LIST_TITLE"] = "Tarefas";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_DAY"] = "- #TIME# d";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_HOUR"] = "- #TIME# h";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_MINUTE"] = "- #TIME# min";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_MONTH"] = "- #TIME# mês";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_WEEK"] = "- #TIME# sem";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_YEAR"] = "- #TIME# ano";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_MORE_THAN_TWO_WEEKS"] = "Mais de duas semanas";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_NEXT_WEEK"] = "Na próxima semana";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_NO_DEADLINE"] = "Sem prazo";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_THIS_WEEK"] = "Esta semana";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_TODAY"] = "Hoje";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_TOMORROW"] = "Amanhã";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_DEFERRED"] = "Adiada";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_EXPIRED"] = "Atrasado";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_EXPIRED_SOON"] = "Quase atrasado";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_NEW"] = "Novo";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_SUPPOSEDLY_COMPLETED"] = "Na dependência de revisão";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_WITHOUT_DEADLINE"] = "Sem prazo";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_APPROVE"] = "Aceitar tarefa";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CANCEL"] = "Cancelar";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_DEADLINE"] = "Prazo";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_GROUP"] = "Anexar ao projeto";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_RESPONSIBLE"] = "Atribuir";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DELEGATE"] = "Delegar";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DISAPPROVE"] = "Retornar para revisão";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DONT_FOLLOW"] = "Deixar de seguir";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_MORE"] = "Mais";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_MUTE"] = "Mudo";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PAUSE"] = "Pausar";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PIN"] = "Fixar";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PING"] = "Executar ping";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_READ"] = "Marcar como lido";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_REMOVE"] = "Excluir";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_RENEW"] = "Retomar";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_START"] = "Início";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_UNMUTE"] = "Ativar som";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_UNPIN"] = "Desafixar";
$MESS["TASKS_ROLE_ACCOMPLICE"] = "Assistindo";
$MESS["TASKS_ROLE_AUDITOR"] = "Seguindo";
$MESS["TASKS_ROLE_ORIGINATOR"] = "Estabelecidas por mim";
$MESS["TASKS_ROLE_RESPONSIBLE"] = "Em andamento";
$MESS["TASKS_ROLE_VIEW_ALL"] = "Todas as tarefas";
