<?php
$MESS["SELECTOR_COMPONENT_PICK_CONTRACTOR"] = "Sélectionner un fournisseur";
$MESS["SELECTOR_COMPONENT_PICK_PRODUCT"] = "Sélectionner un produit";
$MESS["SELECTOR_COMPONENT_PICK_SECTION"] = "Sélectionner une section";
$MESS["SELECTOR_COMPONENT_PICK_STORE"] = "Sélectionnez un entrepôt";
