<?
$MESS["ACTION_DELETE"] = "Eliminar";
$MESS["INVITE_USERS"] = "Invitar usuarios";
$MESS["INVITE_USERS_ERROR"] = "Error";
$MESS["LOAD_MORE_RESULT"] = "Mostrar más";
$MESS["LOAD_MORE_USERS"] = "Carga más";
$MESS["RECENT_SEARCH"] = "Búsqueda reciente";
$MESS["SEARCH_EMPTY_RESULT"] = "Lamentablemente, su solicitud de búsqueda no encontró nada.";
$MESS["SEARCH_LOADING"] = "Buscar...";
$MESS["SEARCH_PLACEHOLDER"] = "Ingrese nombre o departamento";
$MESS["USER_LOADING"] = "Cargando...";
?>