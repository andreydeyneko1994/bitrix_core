<?
$MESS["IM_USER_API_ERROR"] = "Błąd podczas wykonywania żądania. Spróbuj ponownie później.";
$MESS["IM_USER_CONNECTION_ERROR"] = "Błąd podczas łączenia z Bitrix24. Sprawdź połączenie sieciowe.";
$MESS["IM_USER_LIST_EMPTY"] = "- Lista użytkowników jest pusta -";
$MESS["IM_USER_LIST_KICK"] = "Usuń";
$MESS["IM_USER_LIST_OWNER"] = "Ustaw jako właściciela";
?>