<?php
$MESS["MB_BP_MAIN_STRESS_LEVEL"] = "Poziom stresu";
$MESS["MEASURE_STRESS"] = "Zmierz";
$MESS["MENU_EDIT_PROFILE"] = "Edytuj profil";
$MESS["MENU_SETTINGS_INFO"] = "Witaj! Formularz „Ustawienia” został przeniesiony do menu wyskakującego. 
Czy pokazać ci, jak uzyskać do niego dostęp?";
$MESS["MENU_SETTINGS_INFO_YES"] = "Tak, pokaż";
$MESS["MENU_VIEW_PROFILE"] = "Wyświetl profil";
$MESS["WELLTORY_SPOTLIGHT"] = "Zmierz swój poziom stresu";
