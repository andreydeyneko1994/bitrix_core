<?
$MESS["ACCOUNT"] = "Cuenta";
$MESS["EMAIL"] = "E-mail";
$MESS["ERROR"] = "Error";
$MESS["EXTRA"] = "Más";
$MESS["FEMALE"] = "Femenino";
$MESS["GROUP_ID"] = "Grupo";
$MESS["LAST_NAME"] = "Apellido";
$MESS["MAIN"] = "General";
$MESS["MALE"] = "Masculino";
$MESS["NAME"] = "Nombre";
$MESS["PASSWORD"] = "Contraseña";
$MESS["PERSONAL_BIRTHDAY"] = "Fecha de nacimiento";
$MESS["PERSONAL_CITY"] = "Ciudad";
$MESS["PERSONAL_GENDER"] = "Sexo";
$MESS["PERSONAL_MOBILE"] = "Móvil";
$MESS["PERSONAL_WWW"] = "Sitio web";
$MESS["SAVE_FORM"] = "Hecho";
$MESS["SECOND_NAME"] = "Segundo nombre";
$MESS["SOMETHING_WENT_WRONG"] = "Eso es un error. Por favor, inténtelo de nuevo más tarde.";
$MESS["TIME_ZONE"] = "Zona horaria";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Número de extensión";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_XING"] = "Xing";
$MESS["WORK_PHONE"] = "Teléfono del trabajo";
$MESS["WORK_POSITION"] = "Cargo";
?>