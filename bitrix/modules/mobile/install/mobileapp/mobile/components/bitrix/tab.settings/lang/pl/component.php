<?php
$MESS["ERROR"] = "Błąd";
$MESS["PRESET_APPLY_ERROR"] = "Nie można zastosować ustawień menu";
$MESS["PRESET_NAME_DEFAULT"] = "Domyślny";
$MESS["PRESET_NAME_MANUAL"] = "Ustawienia niestandardowe";
$MESS["PRESET_NAME_OL"] = "Agent Otwartego kanału";
$MESS["PRESET_NAME_STREAM"] = "Komunikacja";
$MESS["PRESET_NAME_TASK"] = "Zadania";
$MESS["PRESET_TITLE"] = "Ustawienia wstępne";
$MESS["SETTINGS_TAB_ACTIVE_TITLE"] = "Aktywne elementy";
$MESS["SETTINGS_TAB_APPLIED"] = "Twoje preferencje zostały zastosowane! 
Chwileczkę...";
$MESS["SETTINGS_TAB_BUTTON_DONE"] = "Gotowe";
$MESS["SETTINGS_TAB_CANT_MOVE"] = "Nie można przenieść elementu „#title#”";
$MESS["SETTINGS_TAB_INACTIVE_TITLE"] = "Nieaktywne elementy";
$MESS["SETTINGS_TAB_MAKE_INACTIVE"] = "Usuń";
$MESS["TAB_NAME_CHAT"] = "Czaty na żywo";
$MESS["TAB_NAME_MENU"] = "Menu";
$MESS["TAB_NAME_NOTIFY"] = "Powiadomienia";
$MESS["TAB_NAME_OL"] = "Otwarte kanały";
$MESS["TAB_NAME_STREAM"] = "Aktualności";
$MESS["TAB_NAME_TASK"] = "Zadania";
