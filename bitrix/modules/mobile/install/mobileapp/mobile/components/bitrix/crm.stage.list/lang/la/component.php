<?php
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST"] = "Lista";
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST_SUBTEXT"] = "(todas las etapas)";
$MESS["CRM_STAGE_LIST_CHANGE_TUNNEL"] = "Cambiar embudo";
$MESS["CRM_STAGE_LIST_CREATE_STAGE"] = "Crear una etapa";
$MESS["CRM_STAGE_LIST_SELECT_STAGE"] = "Haga clic para seleccionar una etapa";
$MESS["CRM_STAGE_LIST_TITLE"] = "Ventas";
$MESS["CRM_STAGE_LIST_TUNNEL"] = "Túnel";
