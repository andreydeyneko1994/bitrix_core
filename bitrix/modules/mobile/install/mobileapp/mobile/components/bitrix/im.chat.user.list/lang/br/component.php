<?
$MESS["IM_USER_API_ERROR"] = "Erro ao executar a solicitação. Por favor, tente novamente mais tarde.";
$MESS["IM_USER_CONNECTION_ERROR"] = "Erro ao conectar o Bitrix24. Por favor, verifique a conexão de rede.";
$MESS["IM_USER_LIST_EMPTY"] = "- A lista do usuário está vazia -";
$MESS["IM_USER_LIST_KICK"] = "Remover";
$MESS["IM_USER_LIST_OWNER"] = "Definir como proprietário";
?>