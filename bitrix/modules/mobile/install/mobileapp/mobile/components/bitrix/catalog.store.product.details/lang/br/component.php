<?php
$MESS["CSPD_ADD_SKU_OR_SERIAL_NUMBER"] = "Adicionar SKU ou S/N";
$MESS["CSPD_CLOSE"] = "Fechar";
$MESS["CSPD_DONE"] = "Concluído";
$MESS["CSPD_FIELDS_BARCODE"] = "Código de barras";
$MESS["CSPD_FIELDS_MEASURES"] = "Unidade de medida";
$MESS["CSPD_FIELDS_PHOTOS"] = "Imagens do produto";
$MESS["CSPD_FIELDS_PRODUCT_NAME"] = "Nome do produto";
$MESS["CSPD_FIELDS_PRODUCT_SECTIONS"] = "Vincular à seção";
$MESS["CSPD_FIELDS_PURCHASING_PRICE"] = "Preço de compra";
$MESS["CSPD_FIELDS_SELLING_PRICE"] = "Preço de venda";
$MESS["CSPD_FIELDS_STORE"] = "Depósito";
$MESS["CSPD_FIELDS_STORE_TO_AMOUNT"] = "Quantidade recebida";
$MESS["CSPD_MORE_OPPORTUNITIES"] = "Opções adicionais";
$MESS["CSPD_OPEN_PRODUCT_IN_DESKTOP_VERSION"] = "Abrir produto na versão completa";
