<?php
$MESS["CSPD_ADD_SKU_OR_SERIAL_NUMBER"] = "Agregar SKU o N/S";
$MESS["CSPD_CLOSE"] = "Cerrar";
$MESS["CSPD_DONE"] = "Listo";
$MESS["CSPD_FIELDS_BARCODE"] = "Código de barras";
$MESS["CSPD_FIELDS_MEASURES"] = "Unidad de medida";
$MESS["CSPD_FIELDS_PHOTOS"] = "Imágenes del producto";
$MESS["CSPD_FIELDS_PRODUCT_NAME"] = "Nombre del producto";
$MESS["CSPD_FIELDS_PRODUCT_SECTIONS"] = "Enlazar a la sección";
$MESS["CSPD_FIELDS_PURCHASING_PRICE"] = "Precio de compra";
$MESS["CSPD_FIELDS_SELLING_PRICE"] = "Precio de venta";
$MESS["CSPD_FIELDS_STORE"] = "Almacén";
$MESS["CSPD_FIELDS_STORE_TO_AMOUNT"] = "Cantidad que llegó";
$MESS["CSPD_MORE_OPPORTUNITIES"] = "Opciones adicionales";
$MESS["CSPD_OPEN_PRODUCT_IN_DESKTOP_VERSION"] = "Abrir el producto en la versión completa";
