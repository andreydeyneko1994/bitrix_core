<?php
$MESS["CSPD_ADD_SKU_OR_SERIAL_NUMBER"] = "Ajouter une UGS ou un S/N";
$MESS["CSPD_CLOSE"] = "Fermer";
$MESS["CSPD_DONE"] = "Terminé";
$MESS["CSPD_FIELDS_BARCODE"] = "Code-barres";
$MESS["CSPD_FIELDS_MEASURES"] = "Unité de mesure";
$MESS["CSPD_FIELDS_PHOTOS"] = "Images des produits";
$MESS["CSPD_FIELDS_PRODUCT_NAME"] = "Nom du produit";
$MESS["CSPD_FIELDS_PRODUCT_SECTIONS"] = "Lier à la section";
$MESS["CSPD_FIELDS_PURCHASING_PRICE"] = "Prix d'achat";
$MESS["CSPD_FIELDS_SELLING_PRICE"] = "Prix de vente";
$MESS["CSPD_FIELDS_STORE"] = "Entrepôt";
$MESS["CSPD_FIELDS_STORE_TO_AMOUNT"] = "Quantité arrivée";
$MESS["CSPD_MORE_OPPORTUNITIES"] = "Options supplémentaires";
$MESS["CSPD_OPEN_PRODUCT_IN_DESKTOP_VERSION"] = "Ouvrir un produit dans la version complète";
