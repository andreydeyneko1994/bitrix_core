<?
$MESS["IM_DEPARTMENT_START"] = "Saisissez le nom du service pour démarrer la recherche.";
$MESS["IM_USER_SELECTOR_API_ERROR"] = "Erreur lors de la connexion des nouveaux participants. Veuillez réessayer plus tard.";
$MESS["IM_USER_SELECTOR_CONNECTION_ERROR"] = "Erreur de connexion à Bitrix24. Veuillez vérifier la connexion au réseau.";
?>