<?php
$MESS["CRM_CALL_TRACKER_ADD_TO_IGNORED_NOTIFICATION"] = "Numer dodano do wyjątków";
$MESS["CRM_CALL_TRACKER_ADD_TO_MENU_BUTTON"] = "Dodawanie";
$MESS["CRM_CALL_TRACKER_CONTEXT_MENU_TITLE"] = "Działania";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_DETAILS"] = "Szczegóły";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_ENABLE"] = "Włącz";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Zapisz na później";
$MESS["CRM_CALL_TRACKER_SUBTITLE_CONGRATULATIONS"] = "Odbierasz zbyt wiele połączeń? Dodanie monitora połączeń do menu zapewni szybszy dostęp.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_EMPTY_LIST"] = "Ten widok na bieżąco pokazuje otrzymywane połączenia.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_IS_NOT_SIMPLE_CRM"] = "Tryb CRM możesz zmienić, logując się do swojego Bitrix24 na komputerze.";
$MESS["CRM_CALL_TRACKER_TITLE_CONGRATULATIONS"] = "Gratulacje! Monitor połączeń został włączony";
$MESS["CRM_CALL_TRACKER_TITLE_EMPTY_LIST"] = "Wszystkie połączenia zostały przetworzone!";
$MESS["CRM_CALL_TRACKER_TITLE_IS_NOT_SIMPLE_CRM"] = "Aby użyć monitora połączeń, włącz prosty tryb CRM";
$MESS["CRM_CALL_TRACKER_TITLE_LICENSE_RESTRICTIONS"] = "Monitor połączeń jest dostępny wyłącznie w planach komercyjnych";
$MESS["CRM_CALL_TRACKER_TITLE_WELCOME"] = "Jednym kliknięciem kopiuj połączenia z klientami z telefonu do dealów CRM";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Dodaj do wyjątków";
$MESS["CRM_CALL_TRACKER_UPDATED_NOTIFICATION"] = "Deal został zapisany";
