<?php
$MESS["CRM_CALL_TRACKER_ADD_TO_IGNORED_NOTIFICATION"] = "Número adicionado a exceções";
$MESS["CRM_CALL_TRACKER_ADD_TO_MENU_BUTTON"] = "Adicionar";
$MESS["CRM_CALL_TRACKER_CONTEXT_MENU_TITLE"] = "Ações";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_DETAILS"] = "Detalhes";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_ENABLE"] = "Ativar";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Salvar para mais tarde";
$MESS["CRM_CALL_TRACKER_SUBTITLE_CONGRATULATIONS"] = "Recebe muitas ligações? Adicione o Rastreador de Chamada ao menu para acesso mais rápido.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_EMPTY_LIST"] = "Esta exibição mostrará chamadas assim que você receber.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_IS_NOT_SIMPLE_CRM"] = "Você pode alterar o modo CRM entrando no Bitrix24 no seu computador.";
$MESS["CRM_CALL_TRACKER_TITLE_CONGRATULATIONS"] = "Parabéns! O rastreador de chamada foi ativado";
$MESS["CRM_CALL_TRACKER_TITLE_EMPTY_LIST"] = "Todas as chamadas foram processadas!";
$MESS["CRM_CALL_TRACKER_TITLE_IS_NOT_SIMPLE_CRM"] = "Ative o modo CRM Simples para usar o rastreador de chamada";
$MESS["CRM_CALL_TRACKER_TITLE_LICENSE_RESTRICTIONS"] = "O rastreador de chamada só está disponível em planos comerciais";
$MESS["CRM_CALL_TRACKER_TITLE_WELCOME"] = "Copiar chamadas telefônicas de clientes para negócios do CRM com um clique";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Adicionar às exceções";
$MESS["CRM_CALL_TRACKER_UPDATED_NOTIFICATION"] = "O negócio foi salvo";
