<?php
$MESS["CRM_CALL_TRACKER_ADD_TO_IGNORED_NOTIFICATION"] = "El número se agregó a las excepciones";
$MESS["CRM_CALL_TRACKER_ADD_TO_MENU_BUTTON"] = "Agregar";
$MESS["CRM_CALL_TRACKER_CONTEXT_MENU_TITLE"] = "Acciones";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_DETAILS"] = "Detalles";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_ENABLE"] = "Habilitar";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Guardar para más tarde";
$MESS["CRM_CALL_TRACKER_SUBTITLE_CONGRATULATIONS"] = "¿Recibe demasiadas llamadas? Agregue el rastreador de llamadas al menú para disfrutar un acceso más rápido.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_EMPTY_LIST"] = "Esta vista mostrará las llamadas tan pronto como reciba una.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_IS_NOT_SIMPLE_CRM"] = "Puede cambiar el modo CRM iniciando sesión en Bitrix24 desde su computadora.";
$MESS["CRM_CALL_TRACKER_TITLE_CONGRATULATIONS"] = "¡Felicidades! Se habilitó el rastreador de llamadas";
$MESS["CRM_CALL_TRACKER_TITLE_EMPTY_LIST"] = "¡Todas las llamadas fueron procesadas!";
$MESS["CRM_CALL_TRACKER_TITLE_IS_NOT_SIMPLE_CRM"] = "Habilite el modo CRM simple para utilizar el rastreador de llamadas";
$MESS["CRM_CALL_TRACKER_TITLE_LICENSE_RESTRICTIONS"] = "El rastreador de llamadas solo está disponible en los planes comerciales";
$MESS["CRM_CALL_TRACKER_TITLE_WELCOME"] = "Copie las llamadas de los clientes, del teléfono a las negociaciones del CRM, con un solo clic";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Agregar a las excepciones";
$MESS["CRM_CALL_TRACKER_UPDATED_NOTIFICATION"] = "Se guardó la negociación";
