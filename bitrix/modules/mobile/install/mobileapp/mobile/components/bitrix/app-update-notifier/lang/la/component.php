<?php
$MESS["APP_UPDATE_NOTIFIER_CLOSE"] = "Cerrar";
$MESS["APP_UPDATE_NOTIFIER_NEED_UPDATE"] = "Debe actualizar su aplicación Bitrix24 para usar esta sección.";
$MESS["APP_UPDATE_NOTIFIER_OPEN_APP_STORE"] = "Abrir la App Store";
$MESS["APP_UPDATE_NOTIFIER_OPEN_PLAY_MARKET"] = "Abrir Play Market";
