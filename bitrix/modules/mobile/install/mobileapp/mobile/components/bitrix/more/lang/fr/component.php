<?php
$MESS["MB_BP_MAIN_STRESS_LEVEL"] = "Niveau de stress";
$MESS["MEASURE_STRESS"] = "Mesurer";
$MESS["MENU_EDIT_PROFILE"] = "Éditer le profil";
$MESS["MENU_SETTINGS_INFO"] = "Bonjour ! Le formulaire \"Paramètres\" a été déplacé dans un menu contextuel. 
Voulez-vous que nous vous montrions comment y accéder ?";
$MESS["MENU_SETTINGS_INFO_YES"] = "Oui, merci de me montrer";
$MESS["MENU_VIEW_PROFILE"] = "Voir le profil";
$MESS["WELLTORY_SPOTLIGHT"] = "Mesurez votre niveau de stress";
