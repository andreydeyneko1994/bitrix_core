<?
$MESS["IM_CHAT_TYPE_CHAT_NEW"] = "Czat prywatny";
$MESS["IM_CHAT_TYPE_OPEN_NEW"] = "Czat publiczny";
$MESS["IM_CREATE_API_ERROR"] = "Błąd podczas tworzenia czatu. Spróbuj ponownie później.";
$MESS["IM_CREATE_CONNECTION_ERROR"] = "Błąd podczas łączenia z Bitrix24. Sprawdź połączenie sieciowe.";
$MESS["IM_DEPARTMENT_START"] = "Aby rozpocząć wyszukiwanie, wprowadź nazwę działu.";
?>