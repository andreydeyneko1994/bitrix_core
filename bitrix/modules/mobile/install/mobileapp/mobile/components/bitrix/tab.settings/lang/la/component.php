<?php
$MESS["ERROR"] = "Error";
$MESS["PRESET_APPLY_ERROR"] = "No se pudo aplicar la configuración del menú";
$MESS["PRESET_NAME_DEFAULT"] = "Por defecto";
$MESS["PRESET_NAME_MANUAL"] = "Ajustes personalizados";
$MESS["PRESET_NAME_OL"] = "Agente del canal abierto";
$MESS["PRESET_NAME_STREAM"] = "Comunicaciones";
$MESS["PRESET_NAME_TASK"] = "Tareas";
$MESS["PRESET_TITLE"] = "Predeterminados";
$MESS["SETTINGS_TAB_ACTIVE_TITLE"] = "Elementos activos";
$MESS["SETTINGS_TAB_APPLIED"] = "¡Sus preferencias se aplicaron! 
Un momento por favor...";
$MESS["SETTINGS_TAB_BUTTON_DONE"] = "Listo";
$MESS["SETTINGS_TAB_CANT_MOVE"] = "El elemento \"#title#\" no se puede mover";
$MESS["SETTINGS_TAB_INACTIVE_TITLE"] = "Elementos inactivos";
$MESS["SETTINGS_TAB_MAKE_INACTIVE"] = "Quitar";
$MESS["TAB_NAME_CHAT"] = "Chats en vivo";
$MESS["TAB_NAME_MENU"] = "Menú";
$MESS["TAB_NAME_NOTIFY"] = "Notificaciones";
$MESS["TAB_NAME_OL"] = "Canales abiertos";
$MESS["TAB_NAME_STREAM"] = "Noticias";
$MESS["TAB_NAME_TASK"] = "Tareas";
