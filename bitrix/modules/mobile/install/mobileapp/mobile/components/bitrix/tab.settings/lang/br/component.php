<?php
$MESS["ERROR"] = "Erro";
$MESS["PRESET_APPLY_ERROR"] = "Não foi possível aplicar as configurações do menu";
$MESS["PRESET_NAME_DEFAULT"] = "Padrão";
$MESS["PRESET_NAME_MANUAL"] = "Configurações Personalizadas";
$MESS["PRESET_NAME_OL"] = "Agente de Canal Aberto";
$MESS["PRESET_NAME_STREAM"] = "Comunicações";
$MESS["PRESET_NAME_TASK"] = "Tarefas";
$MESS["PRESET_TITLE"] = "Predefinições";
$MESS["SETTINGS_TAB_ACTIVE_TITLE"] = "Itens ativos";
$MESS["SETTINGS_TAB_APPLIED"] = "Suas preferências foram aplicadas! 
Um momento, por favor...";
$MESS["SETTINGS_TAB_BUTTON_DONE"] = "Concluído";
$MESS["SETTINGS_TAB_CANT_MOVE"] = "O item \"#title#\" não pode ser movido";
$MESS["SETTINGS_TAB_INACTIVE_TITLE"] = "Itens inativos";
$MESS["SETTINGS_TAB_MAKE_INACTIVE"] = "Remover";
$MESS["TAB_NAME_CHAT"] = "Bate-papos ao vivo";
$MESS["TAB_NAME_MENU"] = "Menu";
$MESS["TAB_NAME_NOTIFY"] = "Notificações";
$MESS["TAB_NAME_OL"] = "Canais Abertos";
$MESS["TAB_NAME_STREAM"] = "Feed";
$MESS["TAB_NAME_TASK"] = "Tarefas";
