<?
$MESS["M_CRM_LEAD_ADD"] = "Créer un client potentiel";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Choisir une entreprise de la liste...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Choisir un contact de la liste...";
$MESS["M_CRM_LEAD_LIST_TITLE"] = "Tous les clients potentiels";
?>