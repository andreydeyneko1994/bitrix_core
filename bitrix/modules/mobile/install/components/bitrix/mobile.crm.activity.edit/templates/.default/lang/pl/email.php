<?
$MESS["M_CRM_ACTIVITY_EDIT_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_ADD_COMM"] = "Dodaj odbiorców";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_CREATE_BTN"] = "Utwórz";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_DETAIL_SECTION"] = "Szczegóły wiadomości";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_FIELD_COMM"] = "do";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_FIELD_DESCRIPTION"] = "Wiadomość";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_FIELD_FROM"] = "od";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_FIELD_SUBJECT"] = "Temat";
$MESS["M_CRM_ACTIVITY_EDIT_EMAIL_UPDATE_BTN"] = "Zapisz";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_OWNER"] = "Deal";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_OWNER_NOT_SPECIFIED"] = "[nie określone]";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_RESPONSIBLE_NOT_SPECIFIED"] = "[nie określone]";
$MESS["M_CRM_ACTIVITY_EDIT_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_ACTIVITY_EDIT_NEW_EMAIL"] = "Nowa wiadomość";
$MESS["M_CRM_ACTIVITY_EDIT_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_ACTIVITY_EDIT_USER_SELECTOR_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_ACTIVITY_EDIT_USER_SELECTOR_OK_BTN"] = "wybierz";
?>