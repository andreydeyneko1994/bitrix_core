<?
$MESS["CRM_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["CRM_ACTIVITY_COULD_NOT_DELETE"] = "Działanie nie mogło zostać usunięte.";
$MESS["CRM_ACTIVITY_EDIT_ACTION_DEFAULT_SUBJECT"] = "Nowa aktywność (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Nowy e -mail (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_FROM_FIELD"] = "Proszę określić nadawcę wiadomości.";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_TO_FIELD"] = "Proszę określić odbiorcę wiadomości.";
$MESS["CRM_ACTIVITY_EDIT_INCOMING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Przychodzące połączenie telefoniczne #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_INVALID_EMAIL"] = "#VALUE#' nie jest prawidłowym adresem e-mail.";
$MESS["CRM_ACTIVITY_EDIT_MEETING_ACTION_DEFAULT_SUBJECT_EXT"] = "Spotkanie z #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_OUTGOING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Wychodzące połączenie telefoniczne #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_FROM"] = "od";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_SUBJECT"] = "Temat";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_TO"] = "do";
$MESS["CRM_ACTIVITY_NOT_FOUND"] = "Nie można znaleźć działania ##ID#.";
$MESS["CRM_ACTIVITY_OWNER_ID_NOT_FOUND"] = "Nie znaleziono ID właściciela.";
$MESS["CRM_ACTIVITY_OWNER_NOT_FOUND"] = "Nie można określić obiektu.";
$MESS["CRM_ACTIVITY_OWNER_TYPE_NOT_FOUND"] = "Nie znaleziono typu właściciela.";
$MESS["CRM_ACTIVITY_SELECT_COMMUNICATION"] = "Wybierz kontakt";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: the '#ENTITY_TYPE#' is not supported in the current context.";
?>