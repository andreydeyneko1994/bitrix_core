<?
$MESS["M_CRM_LOCATION_LIST_DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["M_CRM_LOCATION_LIST_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_LOCATION_LIST_PULL_TEXT"] = "Tire hacia abajo para actualizar...";
$MESS["M_CRM_LOCATION_LIST_SEARCH_BUTTON"] = "Búsqueda";
$MESS["M_CRM_LOCATION_LIST_SEARCH_PLACEHOLDER"] = "Buscar por ciudad o provincia";
?>