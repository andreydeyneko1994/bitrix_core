<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_DEAL_LIST_ADD_DEAL"] = "Dodaj aktywność";
$MESS["M_CRM_DEAL_LIST_BILL"] = "Utwórz fakturę";
$MESS["M_CRM_DEAL_LIST_CALL"] = "Wykonaj połączenie";
$MESS["M_CRM_DEAL_LIST_CHANGE_STAGE"] = "Zmień etap";
$MESS["M_CRM_DEAL_LIST_CREATE_BASE"] = "Utwórz ze źródła";
$MESS["M_CRM_DEAL_LIST_CREATE_DEAL"] = "Deal";
$MESS["M_CRM_DEAL_LIST_DELETE"] = "Usuń";
$MESS["M_CRM_DEAL_LIST_EDIT"] = "Edycja";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Wyniki wyszukiwania";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Wszystkie deale";
$MESS["M_CRM_DEAL_LIST_MAIL"] = "Wyślij wiadomość";
$MESS["M_CRM_DEAL_LIST_MEETING"] = "Ustaw spotkanie";
$MESS["M_CRM_DEAL_LIST_MORE"] = "Więcej ...";
$MESS["M_CRM_DEAL_LIST_PRESET_MY"] = "Moje deale";
$MESS["M_CRM_DEAL_LIST_PRESET_NEW"] = "Nowe deale";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Własny filtr";
?>