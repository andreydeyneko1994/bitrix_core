<?php
$MESS["INTERVAL_DAY_PLURAL"] = "días";
$MESS["INTERVAL_DAY_SINGLE"] = "día";
$MESS["INTERVAL_HOUR_PLURAL"] = "horas";
$MESS["INTERVAL_HOUR_SINGLE"] = "hora";
$MESS["INTERVAL_MINUTE_PLURAL"] = "min";
$MESS["INTERVAL_MINUTE_SINGLE"] = "min";
$MESS["INTERVAL_SECOND_PLURAL"] = "s";
$MESS["INTERVAL_SECOND_SINGLE"] = "s";
$MESS["MPL_CALL_IS_PROCESSED"] = "Llamada procesada";
$MESS["MPL_MOBILE_CALL"] = "LLamada";
$MESS["MPL_MOBILE_INCOMING_CALL"] = "Llamada entrante";
$MESS["MPL_MOBILE_OUTBOUND_CALL"] = "Llamada saliente";
$MESS["MPL_MOBILE_PUBLISHING"] = "Publicación&hellip;";
$MESS["MPT_ADD_FIRST_COMMENT"] = "Agregar el primer comentario";
$MESS["MPT_PREVIOUS_COMMENTS"] = "Comentarios anteriores";
