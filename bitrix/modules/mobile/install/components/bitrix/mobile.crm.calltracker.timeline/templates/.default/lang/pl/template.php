<?php
$MESS["INTERVAL_DAY_PLURAL"] = "dni";
$MESS["INTERVAL_DAY_SINGLE"] = "dzień";
$MESS["INTERVAL_HOUR_PLURAL"] = "godz.";
$MESS["INTERVAL_HOUR_SINGLE"] = "godz.";
$MESS["INTERVAL_MINUTE_PLURAL"] = "min";
$MESS["INTERVAL_MINUTE_SINGLE"] = "min";
$MESS["INTERVAL_SECOND_PLURAL"] = "s";
$MESS["INTERVAL_SECOND_SINGLE"] = "s";
$MESS["MPL_CALL_IS_PROCESSED"] = "Połączenie przetwarzane";
$MESS["MPL_MOBILE_CALL"] = "Połączenie";
$MESS["MPL_MOBILE_INCOMING_CALL"] = "Połączenie przychodzące";
$MESS["MPL_MOBILE_OUTBOUND_CALL"] = "Połączenie wychodzące";
$MESS["MPL_MOBILE_PUBLISHING"] = "Publikowanie&hellip;";
$MESS["MPT_ADD_FIRST_COMMENT"] = "Dodaj pierwszy komentarz";
$MESS["MPT_PREVIOUS_COMMENTS"] = "Poprzednie komentarze";
