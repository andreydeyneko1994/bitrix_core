<?
$MESS["SONET_LOG_COMMENT_CANT_DELETE"] = "Nie można usunąć komentarza";
$MESS["SONET_LOG_COMMENT_DELETE_NO_PERMISSIONS"] = "Brak uprawnień do usunięcia komentarza";
$MESS["SONET_LOG_COMMENT_EDIT_NO_PERMISSIONS"] = "Brak uprawnień do edycji komentarza";
$MESS["SONET_LOG_COMMENT_EMPTY"] = "Tekst wiadomości jest pusty.";
$MESS["SONET_LOG_COMMENT_FORMAT_DATE"] = "d F, g:i a";
$MESS["SONET_LOG_COMMENT_FORMAT_DATE_YEAR"] = "d F Y, g:i a";
$MESS["SONET_LOG_COMMENT_FORMAT_TIME"] = "g:i a";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Nie masz zezwolenia na dodanie komentarza.";
$MESS["SONET_LOG_CREATED_BY_ANONYMOUS"] = "Nieautoryzowany Gość";
?>