<?
$MESS["M_CRM_REQUISITE_EDIT_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_REQUISITE_EDIT_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_REQUISITE_EDIT_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_REQUISITE_EDIT_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_REQUISITE_EDIT_SAVE_BTN"] = "Zapisz";
$MESS["M_CRM_REQUISITE_EDIT_TITLE"] = "Edytuj szczegóły płatności";
?>