<?
$MESS["M_CRM_REQUISITE_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_REQUISITE_EDIT_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_REQUISITE_EDIT_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_REQUISITE_EDIT_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
$MESS["M_CRM_REQUISITE_EDIT_SAVE_BTN"] = "Enregistrer";
$MESS["M_CRM_REQUISITE_EDIT_TITLE"] = "Modifier les détails du paiement";
?>