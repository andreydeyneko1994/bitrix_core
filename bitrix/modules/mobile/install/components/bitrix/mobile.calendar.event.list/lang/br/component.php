<?
$MESS["CALENDAR_MODULE_IS_NOT_INSTALLED"] = "O módulo \"Calendário de Eventos\" não está instalado.";
$MESS["EVENTS_GROUP_LATE"] = "Mais tarde";
$MESS["EVENTS_GROUP_TODAY"] = "Hoje";
$MESS["EVENTS_GROUP_TOMORROW"] = "Amanhã";
$MESS["MB_CAL_EVENTS_COUNT"] = "Total de eventos: #COUNT#";
$MESS["MB_CAL_EVENT_ALL_DAY"] = "durante todo o dia";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "d/m/Y";
$MESS["MB_CAL_EVENT_DATE_FROM_TO"] = "De #DATE_FROM# a #DATE_TO#";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G:i";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "h:i a";
$MESS["MB_CAL_EVENT_TIME_FROM_TO_TIME"] = "de #TIME_FROM# a #TIME_TO#";
$MESS["MB_CAL_NO_EVENTS"] = "Não há eventos programados";
?>