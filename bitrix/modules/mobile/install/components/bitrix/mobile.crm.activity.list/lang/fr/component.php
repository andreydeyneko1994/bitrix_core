<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module du CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_ALL"] = "Toutes les activités";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED"] = "Achevé(e)s";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED1"] = "Toutes les activités achevées";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY"] = "Mes activités";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED"] = "Mes transactions accomplies";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED1"] = "Mes activités achevées";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED"] = "Ce que moi n'a pas achevé";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED1"] = "Mes activités en cours";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED"] = "Inachevé(e)s";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED1"] = "Toutes les activités en cours";
?>