<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_ALL"] = "Wszystkie działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED"] = "Zakończone";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED1"] = "Wszystkie zakończone działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY"] = "Moje działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED"] = "Moje zakończone działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED1"] = "Moje zakończone działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED"] = "Moje niedpokończone działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED1"] = "Moje bieżące działania";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED"] = "Nieskończone";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED1"] = "Wszystkie bieżące działania";
?>