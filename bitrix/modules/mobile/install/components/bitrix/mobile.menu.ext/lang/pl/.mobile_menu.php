<?php
$MESS["MB_BP_MAIN_MENU_ITEM"] = "Workflow";
$MESS["MB_CALENDAR_LIST"] = "Kalendarz";
$MESS["MB_CHAT_AND_CALLS"] = "Komunikator";
$MESS["MB_COMPANY"] = "Pracownicy";
$MESS["MB_CONTACTS"] = "Kontakty";
$MESS["MB_CRM_ACTIVITY"] = "Moje działania";
$MESS["MB_CRM_COMPANY"] = "Firmy";
$MESS["MB_CRM_CONTACT"] = "Kontakty";
$MESS["MB_CRM_DEAL"] = "Deale";
$MESS["MB_CRM_INVOICE"] = "Faktury";
$MESS["MB_CRM_LEAD"] = "Leady";
$MESS["MB_CRM_PRODUCT"] = "Produkty";
$MESS["MB_CRM_QUOTE"] = "Oferty";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM"] = "Mój Dysk";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM_NEW"] = "Mój Dysk";
$MESS["MB_LIVE_FEED"] = "Aktualności";
$MESS["MB_MARKETPLACE_GROUP_TITLE_2"] = "Market";
$MESS["MB_MESSAGES"] = "Wiadomości";
$MESS["MB_SEC_EXTRANET"] = "Grupy Extranetu";
$MESS["MB_SEC_FAVORITE"] = "Moja praca";
$MESS["MB_SEC_GROUPS"] = "Grupy";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM"] = "Dokumenty udostępnione";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM_NEW"] = "Dokumenty udostępnione";
$MESS["MB_TASKS_MAIN_MENU_ITEM"] = "Zadania";
