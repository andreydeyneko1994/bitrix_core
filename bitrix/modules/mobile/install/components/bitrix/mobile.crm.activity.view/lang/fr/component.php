<?
$MESS["CRM_ACTIVITY_VIEW_NOT_FOUND"] = "Échec de recherche du dossier à identificateur ##ID#.";
$MESS["CRM_ACTIVITY_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[non affecté]";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module du CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>