<?
$MESS["interface_filter_earlier"] = "przed";
$MESS["interface_filter_exact"] = "dokładnie";
$MESS["interface_filter_interval"] = "zakres dat";
$MESS["interface_filter_last"] = "ostatnie";
$MESS["interface_filter_later"] = "po";
$MESS["interface_filter_month"] = "ten miesiąc";
$MESS["interface_filter_month_ago"] = "ostatni miesiąc";
$MESS["interface_filter_no_no_no_1"] = "(nie)";
$MESS["interface_filter_today"] = "dzisiaj";
$MESS["interface_filter_week"] = "ten tydzień";
$MESS["interface_filter_week_ago"] = "ostatni tydzień";
$MESS["interface_filter_yesterday"] = "wczoraj";
?>