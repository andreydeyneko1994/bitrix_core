<?
$MESS["M_FILTER_BUTTON_APPLY"] = "Aplicar";
$MESS["M_FILTER_BUTTON_CANCEL"] = "Cancelar";
$MESS["M_FILTER_DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["M_FILTER_LOAD_TEXT"] = "Actualización del filtro...";
$MESS["M_FILTER_PULL_TEXT"] = "Tire hacia abajo para actualizar...";
$MESS["M_FILTER_TITLE"] = "Configurar el filtro";
?>