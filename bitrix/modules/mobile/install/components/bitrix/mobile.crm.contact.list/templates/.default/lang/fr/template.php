<?
$MESS["M_CRM_CONTACT_ADD"] = "Ajouter un contact";
$MESS["M_CRM_CONTACT_ADD2"] = "Ajouter";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Ajouter manuellement";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Carte de visite";
$MESS["M_CRM_CONTACT_BIZCARD_CLOSE"] = "Fermer";
$MESS["M_CRM_CONTACT_BIZCARD_GO_TO_TARIFF_TABLE"] = "Afficher les prix";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Chargement de l'image...";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Malheureusement, vous avez dépassé votre quota mensuel de travail de reconnaissance. Vous devez passer à un abonnement supérieur afin de pouvoir traiter plus de cartes de visite.";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Reconnaissance...";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Une erreur s'est produite. Veuillez réessayer plus tard.";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Scanner une carte de visite";
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Tous les contacts";
?>