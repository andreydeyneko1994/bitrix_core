<?
$MESS["M_CRM_CONTACT_ADD"] = "Dodaj kontakt";
$MESS["M_CRM_CONTACT_ADD2"] = "Dodaj";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Dodaj ręcznie";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Wizytówka";
$MESS["M_CRM_CONTACT_BIZCARD_CLOSE"] = "Zamknij";
$MESS["M_CRM_CONTACT_BIZCARD_GO_TO_TARIFF_TABLE"] = "Wyświetl ceny";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Przesyłanie obrazu...";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Niestety, przekroczyłeś miesięczny limit rozpoznawania. 
Należy przejść na wyższy plan, aby móc przetworzyć więcej wizytówek.";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Rozpoznaję...";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Wystąpił błąd. Proszę spróbować później.";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Skanuj wizytówkę";
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Wszystkie kontakty";
?>