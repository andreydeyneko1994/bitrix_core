<?
$MESS["CRM_COMM_LIST_ENTITY_ID_NOT_DEFINED"] = "ID właściciela nie jest określone.";
$MESS["CRM_COMM_LIST_ENTITY_TYPE_NOT_DEFINED"] = "Czcionka właściciela nie jest określona.";
$MESS["CRM_COMM_LIST_INVALID_ENTITY_TYPE"] = "Ta czcionka właściciela nie jest obsługiwana.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>