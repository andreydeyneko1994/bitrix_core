<?php
$MESS["CRM_ADD_CONTACT"] = "Dodaj kontakt";
$MESS["CRM_AMOUNT_AND_CURRENCY"] = "Okazja";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Zapisz na później";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Dodaj do wyjątków";
$MESS["CRM_COMPANY"] = "Firma";
$MESS["CRM_COMPANY_PLACEHOLDER"] = "Wprowadź nazwę firmy";
$MESS["CRM_CONTACT"] = "Kontakt";
$MESS["CRM_CONTACT_PLACEHOLDER"] = "Wprowadź nazwę kontaktu";
