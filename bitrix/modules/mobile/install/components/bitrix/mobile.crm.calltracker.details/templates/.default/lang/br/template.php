<?php
$MESS["CRM_ADD_CONTACT"] = "Adicionar contato";
$MESS["CRM_AMOUNT_AND_CURRENCY"] = "Oportunidade";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Salvar para mais tarde";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Adicionar às exceções";
$MESS["CRM_COMPANY"] = "Empresa";
$MESS["CRM_COMPANY_PLACEHOLDER"] = "Digite o nome da empresa";
$MESS["CRM_CONTACT"] = "Contato";
$MESS["CRM_CONTACT_PLACEHOLDER"] = "Digite o nome do contato";
