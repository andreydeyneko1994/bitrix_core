<?php
$MESS["TASKS_MOBILE_EFFICIENCY_COMPLETED"] = "Tâches accomplies";
$MESS["TASKS_MOBILE_EFFICIENCY_IN_PROGRESS"] = "Total des tâches";
$MESS["TASKS_MOBILE_EFFICIENCY_LIMIT_EXCEEDED"] = "Votre Bitrix24 a maintenant 100 tâches ! La page \"Efficacité\" n'est désormais disponible qu'avec les offres majeures.";
$MESS["TASKS_MOBILE_EFFICIENCY_VIOLATIONS"] = "Commentaire sur les tâches";
