<?php
$MESS["TASKS_MOBILE_EFFICIENCY_COMPLETED"] = "Tarefas concluídas";
$MESS["TASKS_MOBILE_EFFICIENCY_IN_PROGRESS"] = "Total de tarefas";
$MESS["TASKS_MOBILE_EFFICIENCY_LIMIT_EXCEEDED"] = "Seu Bitrix24 agora tem 100 tarefas! A página \"Eficiência\" agora está disponível apenas nos planos principais.";
$MESS["TASKS_MOBILE_EFFICIENCY_VIOLATIONS"] = "Comentários da tarefa";
