<?
$MESS["TASK_COLUMN_ALLOW_TIME_TRACKING"] = "Śledź czas spędzony";
$MESS["TASK_COLUMN_CHANGED_DATE"] = "Zmodyfikowano";
$MESS["TASK_COLUMN_CLOSED_DATE"] = "Zakończone";
$MESS["TASK_COLUMN_CREATED_BY"] = "Stworzone przez";
$MESS["TASK_COLUMN_CREATED_DATE"] = "Utworzone";
$MESS["TASK_COLUMN_DEADLINE"] = "Termin ostateczny";
$MESS["TASK_COLUMN_MARK"] = "Ocena";
$MESS["TASK_COLUMN_PRIORITY"] = "Priorytet";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["TASK_COLUMN_SORTING"] = "Moje sortowanie";
$MESS["TASK_COLUMN_TIME_ESTIMATE"] = "Wymagany szacowany czas";
$MESS["TASK_COLUMN_TITLE"] = "Nazwa zadania";
?>