<?php
$MESS["MB_TASKS_BASE_SETTINGS"] = "Paramètres principaux des tâches";
$MESS["MB_TASKS_BASE_SETTINGS_DESCRIPTION"] = "choses à faire";
$MESS["MB_TASKS_BASE_SETTINGS_DESCRIPTION_PLACEHOLDER"] = "Description de la tâche";
$MESS["MB_TASKS_BASE_SETTINGS_PRIORITY"] = "priorité";
$MESS["MB_TASKS_BASE_SETTINGS_PRIORITY_VALUE"] = "Priorité élevée";
$MESS["MB_TASKS_BASE_SETTINGS_TITLE_TASK"] = "tâche ##TASK_ID#";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tâche";
$MESS["MB_TASKS_TASK_ADD"] = "ajouter";
$MESS["MB_TASKS_TASK_ADD_SEPARATOR"] = "séparateur";
$MESS["MB_TASKS_TASK_CHECK"] = "Exécuter";
$MESS["MB_TASKS_TASK_CHECKLIST_PLACEHOLDER"] = "Type dans un élément de liste de contrôle";
$MESS["MB_TASKS_TASK_COMMENT"] = "Commentaire";
$MESS["MB_TASKS_TASK_DELETE"] = "Supprimer";
$MESS["MB_TASKS_TASK_DETAIL_BTN_ACCEPT_TASK"] = "Accepter la tâche";
$MESS["MB_TASKS_TASK_DETAIL_BTN_ADD_FAVORITE_TASK"] = "Ajouter aux favoris";
$MESS["MB_TASKS_TASK_DETAIL_BTN_APPROVE_TASK"] = "Approuver";
$MESS["MB_TASKS_TASK_DETAIL_BTN_CLOSE_TASK"] = "Terminer";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DECLINE_TASK"] = "Refuser";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DELEGATE_TASK"] = "Déléguer";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DELETE_FAVORITE_TASK"] = "Supprimer des favoris";
$MESS["MB_TASKS_TASK_DETAIL_BTN_EDIT"] = "Modifier";
$MESS["MB_TASKS_TASK_DETAIL_BTN_PAUSE_TASK"] = "Pause";
$MESS["MB_TASKS_TASK_DETAIL_BTN_REDO_TASK"] = "Terminer";
$MESS["MB_TASKS_TASK_DETAIL_BTN_REMOVE"] = "Supprimer";
$MESS["MB_TASKS_TASK_DETAIL_BTN_RENEW_TASK"] = "Reprendre";
$MESS["MB_TASKS_TASK_DETAIL_BTN_START_TASK"] = "Démarrer la tâche";
$MESS["MB_TASKS_TASK_DETAIL_CONFIRM_REMOVE"] = "Voulez-vous vraiment supprimer la tâche ?";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_EXPIRED"] = "La tâche est en retard !";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_EXPIRED_BRIEF"] = "En retard";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_EXPIRED_SOON_BRIEF"] = "Presque en retard";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_NEW_BRIEF"] = "Nouvelle";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_STATE_SUPPOSEDLY_COMPLETED"] = "La tâche est en attente de passage en revue.";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_WO_DEADLINE_BRIEF"] = "Pas de date limite";
$MESS["MB_TASKS_TASK_DETAIL_NO_COMMENTS_STUB_TEXT"] = "Ajoutez votre premier commentaire";
$MESS["MB_TASKS_TASK_DETAIL_TASK_ADD"] = "Nouvelle tâche";
$MESS["MB_TASKS_TASK_DETAIL_TASK_ADD_SUBTASK"] = "Créer une sous-tâche";
$MESS["MB_TASKS_TASK_DETAIL_TASK_WAS_REMOVED"] = "La tâche ##TASK_ID# a été supprimée.";
$MESS["MB_TASKS_TASK_DETAIL_TASK_WAS_REMOVED_OR_NOT_ENOUGH_RIGHTS"] = "La tâche ##TASK_ID# a été supprimée ou vous ne disposez pas des permissions.";
$MESS["MB_TASKS_TASK_DETAIL_UP_BUTTON_TEXT"] = "Faites défiler jusqu'à la description";
$MESS["MB_TASKS_TASK_DETAIL_USER_SELECTOR_BTN_CANCEL"] = "Annuler";
$MESS["MB_TASKS_TASK_EDIT"] = "Modifier";
$MESS["MB_TASKS_TASK_ERROR1"] = "Erreur inconnue.";
$MESS["MB_TASKS_TASK_ERROR2"] = "Réenregistrer.";
$MESS["MB_TASKS_TASK_PLACEHOLDER"] = "Pas sélectionné";
$MESS["MB_TASKS_TASK_REMOVE_CONFIRM_TITLE"] = "Supprimer la tâche";
$MESS["MB_TASKS_TASK_SETTINGS_ACCOMPLICES"] = "participants";
$MESS["MB_TASKS_TASK_SETTINGS_AUDITORS"] = "Observateurs";
$MESS["MB_TASKS_TASK_SETTINGS_AUTHOR_ID"] = "créé par";
$MESS["MB_TASKS_TASK_SETTINGS_CHECKLIST"] = "liste de contrôle";
$MESS["MB_TASKS_TASK_SETTINGS_CRM_TASK"] = "Objets du CRM";
$MESS["MB_TASKS_TASK_SETTINGS_DEADLINE"] = "date limite";
$MESS["MB_TASKS_TASK_SETTINGS_DEADLINE_PLACEHOLDER"] = "sans restriction";
$MESS["MB_TASKS_TASK_SETTINGS_DISK_UF"] = "fichiers";
$MESS["MB_TASKS_TASK_SETTINGS_DURATION_PLAN"] = "Temps estimé";
$MESS["MB_TASKS_TASK_SETTINGS_GROUP_ID"] = "tâche dans le projet";
$MESS["MB_TASKS_TASK_SETTINGS_MARK"] = "évaluation";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_N"] = "Négative";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_NULL"] = "Aucune évaluation";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_P"] = "Positive";
$MESS["MB_TASKS_TASK_SETTINGS_PARENT_ID"] = "tâche parente";
$MESS["MB_TASKS_TASK_SETTINGS_RESPONSIBLE"] = "Personne responsable";
$MESS["MB_TASKS_TASK_SETTINGS_STATUS"] = "statut";
$MESS["MB_TASKS_TASK_SETTINGS_SUBTASKS"] = "sous-tâches";
$MESS["MB_TASKS_TASK_SETTINGS_TAGS"] = "mots-clés";
$MESS["MB_TASKS_TASK_SETTINGS_TAGS_PLACEHOLDER"] = "rechercher des mots-clés";
$MESS["MB_TASKS_TASK_SETTINGS_TIMETRACKING"] = "Suivi du temps";
$MESS["MB_TASKS_TASK_UNCHECK"] = "Annuler";
$MESS["TASKS_DEPENDENCY_END"] = "Fin";
$MESS["TASKS_DEPENDENCY_START"] = "Démarrer";
$MESS["TASKS_LIST_GROUP_ACTION_ERROR1"] = "Réponse inattendue du serveur. Veuillez réessayer.";
$MESS["TASKS_PRIORITY_0"] = "Priorité élevée";
$MESS["TASKS_PRIORITY_2"] = "Priorité élevée";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "En retard";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Terminé(e)s";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Refusé";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Reportée";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "En cours";
$MESS["TASKS_STATUS_STATE_NEW"] = "Nouvelle";
$MESS["TASKS_STATUS_STATE_PENDING"] = "En attente";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "En attente de passage en revue";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Inconnu";
$MESS["TASKS_TT_CANCEL"] = "Annuler";
$MESS["TASKS_TT_CONTINUE"] = "Continuer";
$MESS["TASKS_TT_ERROR1_DESC"] = "Vous suivez actuellement le temps pour la tâche \"#TITLE#\". Cette action va suspendre cela. Voulez-vous continuer ?";
$MESS["TASKS_TT_ERROR1_TITLE"] = "Le tracker du temps de début est pour l'instant utilisé avec une autre tâche.";
$MESS["TASKS_TT_PAUSE"] = "Arrêter";
$MESS["TASKS_TT_START"] = "Tracker du temps de début";
