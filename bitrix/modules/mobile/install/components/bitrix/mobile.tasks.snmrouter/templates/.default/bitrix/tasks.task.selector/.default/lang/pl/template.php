<?
$MESS["TASKS_ALL_TASKS"] = "Wszystko";
$MESS["TASKS_EMPTY_LIST"] = "Lista zadań jest pusta";
$MESS["TASKS_EMPTY_LIST1"] = "Utwórz zadanie";
$MESS["TASKS_EMPTY_LIST2"] = "Nie znaleziono zadań";
$MESS["TASKS_PRIORITY_2"] = "Wysoki Priorytet";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "Przeterminowane";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Zakończone";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Odmówiono";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Odroczone";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "W toku";
$MESS["TASKS_STATUS_STATE_NEW"] = "Nowy";
$MESS["TASKS_STATUS_STATE_PENDING"] = "Oczekujące";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "Przegląd w toku";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Nieznany";
$MESS["TASK_COLUMN_CREATED_BY"] = "utworzone przez";
$MESS["TASK_COLUMN_DEADLINE"] = "deadline";
$MESS["TASK_COLUMN_PRIORITY"] = "priorytet";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "osoba odpowiedzialna";
$MESS["TASK_COLUMN_STATUS"] = "status";
$MESS["TASK_EXPIRED"] = "Przeterminowane";
?>