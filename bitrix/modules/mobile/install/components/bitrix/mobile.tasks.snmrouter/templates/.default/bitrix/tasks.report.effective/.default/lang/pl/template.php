<?php
$MESS["TASKS_MOBILE_EFFICIENCY_COMPLETED"] = "Ukończone zadania";
$MESS["TASKS_MOBILE_EFFICIENCY_IN_PROGRESS"] = "Łącznie zadań";
$MESS["TASKS_MOBILE_EFFICIENCY_LIMIT_EXCEEDED"] = "Twój Bitrix24 ma teraz 100 zadań! Strona \"Wydajność\" jest teraz dostępna tylko w głównych planach.";
$MESS["TASKS_MOBILE_EFFICIENCY_VIOLATIONS"] = "Komentarze do zadania";
