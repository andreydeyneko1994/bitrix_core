<?
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Confirmar a matrícula";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Digite os endereços de e-mail de pessoas que você deseja convidar. Separe múltiplas entradas com vírgula ou espaço.";
$MESS["BX24_INVITE_DIALOG_INVITE"] = "Convidar";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Parabéns!</b><br> Os convites foram enviados para o endereço de e-mails.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Você não pode convidar mais funcionários, porque ele vai ultrapassar os termos da licença.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Junte-se a nós na nossa conta Bitrix24, onde todos podem colaborar em projetos, ter uma comunicação ideal, coordenar tarefas, gerenciar clientes e mais recursos.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Texto do Convite";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Convidar mais usuários";
$MESS["BX24_INVITE_DIALOG_INVITE_TITLE"] = "Convidar um usuário";
$MESS["DOWN_TEXT"] = "Solte para atualizar...";
$MESS["LOAD_TEXT"] = "Atualizando...";
$MESS["PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>