<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Email incorrect.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Veuillez nous rejoindre dans notre compte Bitrix24. C'est un endroit où tout le monde peut communiquer, collaborer sur des tâches et des projets, gérer des clients, et bien plus encore.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Le nombre d'employés invités surpasse la limite prévue du plan tarifaire.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Les utilisateurs avec ces adresses Adresse emailexistent déjà.";
?>