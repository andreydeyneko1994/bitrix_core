<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["M_CRM_QUOTE_LIST_CHANGE_STATUS"] = "Modifier le statut";
$MESS["M_CRM_QUOTE_LIST_COMPANY"] = "Lié à l'entreprise";
$MESS["M_CRM_QUOTE_LIST_CONTACT"] = "Lié au contact";
$MESS["M_CRM_QUOTE_LIST_CREATE_BASE"] = "Créer en utilisant la source";
$MESS["M_CRM_QUOTE_LIST_DEAL"] = "Lié à la transaction";
$MESS["M_CRM_QUOTE_LIST_DELETE"] = "Supprimer";
$MESS["M_CRM_QUOTE_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_QUOTE_LIST_FILTER_NONE"] = "Tous les devis";
$MESS["M_CRM_QUOTE_LIST_LEAD"] = "Lié au client potentiel";
$MESS["M_CRM_QUOTE_LIST_MORE"] = "Plus...";
$MESS["M_CRM_QUOTE_LIST_PRESET_MY"] = "Mes devis";
$MESS["M_CRM_QUOTE_LIST_PRESET_NEW"] = "Nouveaux devis";
$MESS["M_CRM_QUOTE_LIST_PRESET_USER"] = "Filtre personnalisé";
$MESS["M_CRM_QUOTE_LIST_SEND"] = "Envoyer";
?>