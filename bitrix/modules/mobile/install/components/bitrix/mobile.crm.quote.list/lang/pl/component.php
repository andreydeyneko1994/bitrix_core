<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_QUOTE_LIST_CHANGE_STATUS"] = "Zmień status";
$MESS["M_CRM_QUOTE_LIST_COMPANY"] = "Połączone z firmą";
$MESS["M_CRM_QUOTE_LIST_CONTACT"] = "Połączone z kontaktem";
$MESS["M_CRM_QUOTE_LIST_CREATE_BASE"] = "Utwórz ze źródła";
$MESS["M_CRM_QUOTE_LIST_DEAL"] = "Połączone z dealem";
$MESS["M_CRM_QUOTE_LIST_DELETE"] = "Usuń";
$MESS["M_CRM_QUOTE_LIST_EDIT"] = "Edycja";
$MESS["M_CRM_QUOTE_LIST_FILTER_NONE"] = "Wszystkie oferty";
$MESS["M_CRM_QUOTE_LIST_LEAD"] = "Połączony z leadem";
$MESS["M_CRM_QUOTE_LIST_MORE"] = "Więcej ...";
$MESS["M_CRM_QUOTE_LIST_PRESET_MY"] = "Moje oferty";
$MESS["M_CRM_QUOTE_LIST_PRESET_NEW"] = "Nowe oferty";
$MESS["M_CRM_QUOTE_LIST_PRESET_USER"] = "Własny filtr";
$MESS["M_CRM_QUOTE_LIST_SEND"] = "Wyślij";
?>