<?php
$MESS["NM_DOWNTEXT"] = "Suelte para actualizar...";
$MESS["NM_EMPTY"] = "No hay nuevas notificaciones";
$MESS["NM_FOLD"] = "Contraer";
$MESS["NM_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g:i a";
$MESS["NM_LOADTEXT"] = "Volver a cargar...";
$MESS["NM_MORE"] = "Detalles";
$MESS["NM_MORE_USERS"] = "#COUNT# personas más";
$MESS["NM_PULLTEXT"] = "Tire hacia abajo para actualizar...";
$MESS["NM_SYSTEM_USER"] = "Mensaje del sistema";
$MESS["NM_TITLE"] = "Notificaciones";
$MESS["NM_TITLE_2"] = "Actualizando...";
$MESS["NM_UNFOLD"] = "Expandir";
