<?php
$MESS["BPIMNA_PD_FROM"] = "Odbiorca";
$MESS["BPIMNA_PD_MESSAGE"] = "Tekst powiadomienia";
$MESS["BPIMNA_PD_MESSAGE_BBCODE"] = "Język BBCode jest włączony";
$MESS["BPIMNA_PD_MESSAGE_OUT"] = "Tekst powiadomienia do poczty elektronicznej / Jabbera";
$MESS["BPIMNA_PD_MESSAGE_OUT_EMPTY"] = "jeśli jest pusty, zostanie użyty „Tekst powiadomienia”.";
$MESS["BPIMNA_PD_NOTIFY_TYPE"] = "Typ powiadomienia";
$MESS["BPIMNA_PD_NOTIFY_TYPE_FROM"] = "Spersonalizowane powiadomienie (z awatarem)";
$MESS["BPIMNA_PD_NOTIFY_TYPE_SYSTEM"] = "Powiadomienie systemowe (bez awatara)";
$MESS["BPIMNA_PD_TO"] = "Nadawca";
