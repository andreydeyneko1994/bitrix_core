<?php
$MESS["IM_COMPONENT_DEFAULT_OG_TITLE"] = "Wideokonferencja";
$MESS["IM_COMPONENT_OG_DESCRIPTION"] = "Jakość HD to prawie jak spotkanie twarzą w twarz.";
$MESS["IM_COMPONENT_OG_DESCRIPTION_2"] = "Dołącz bez rejestracji z dowolnego urządzenia. Bezpłatnie dla 24 użytkowników.";
$MESS["IM_COMPONENT_OG_TITLE"] = "Korzystaj z połączeń wideo, aby Twoja firma była jeszcze wydajniejsza!";
$MESS["IM_COMPONENT_OG_TITLE_2"] = "Bezpłatne wideokonferencje HD Bitrix24";
