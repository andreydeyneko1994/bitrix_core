<?php
$MESS["IM_COMPONENT_DEFAULT_OG_TITLE"] = "Videoconferência";
$MESS["IM_COMPONENT_OG_DESCRIPTION"] = "Qualidade HD, a melhor opção para uma reunião pessoal.";
$MESS["IM_COMPONENT_OG_DESCRIPTION_2"] = "Participe sem cadastro a partir de qualquer dispositivo. Grátis para 24 usuários.";
$MESS["IM_COMPONENT_OG_TITLE"] = "Use chamadas de vídeo para tornar seu negócio ainda mais eficiente!";
$MESS["IM_COMPONENT_OG_TITLE_2"] = "Videoconferência Bitrix24 HD grátis";
