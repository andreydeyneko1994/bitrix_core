<?php
$MESS["IM_VIEW_POPUP_CONTENT_GROUP_CHAT"] = "Bate-papo privado";
$MESS["IM_VIEW_POPUP_CONTENT_NO_ACCESS"] = "Acesso negado.";
$MESS["IM_VIEW_POPUP_CONTENT_OPEN_CHAT"] = "Bate-papo aberto";
$MESS["IM_VIEW_POPUP_CONTENT_OPEN_HISTORY"] = "Histórico";
$MESS["IM_VIEW_POPUP_USER_OPEN_CHAT"] = "Enviar mensagem";
