<?php
$MESS["BX_MESSENGER_TEXTAREA_BUTTON_SEND"] = "Envoyer un message";
$MESS["BX_MESSENGER_TEXTAREA_FILE"] = "Transférer le fichier";
$MESS["BX_MESSENGER_TEXTAREA_GIPHY"] = "Sélectionner le smiley";
$MESS["BX_MESSENGER_TEXTAREA_PLACEHOLDER"] = "Saisissez un message…";
$MESS["BX_MESSENGER_TEXTAREA_SMILE"] = "Sélectionner une émoticône";
