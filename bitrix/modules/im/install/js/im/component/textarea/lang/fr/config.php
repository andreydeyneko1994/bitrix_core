<?php
$MESS["BX_MESSENGER_TEXTAREA_BUTTON_SEND"] = "Envoyer un message";
$MESS["BX_MESSENGER_TEXTAREA_FILE"] = "Envoyer un fichier";
$MESS["BX_MESSENGER_TEXTAREA_GIPHY"] = "Parcourir les images en GIF";
$MESS["BX_MESSENGER_TEXTAREA_PLACEHOLDER"] = "Saisissez un message...";
$MESS["BX_MESSENGER_TEXTAREA_SMILE"] = "Sélectionner une émoticône";
