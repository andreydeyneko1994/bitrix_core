<?php
$MESS["BX_MESSENGER_TEXTAREA_BUTTON_SEND"] = "Wyślij wiadomość";
$MESS["BX_MESSENGER_TEXTAREA_FILE"] = "Wyślij plik";
$MESS["BX_MESSENGER_TEXTAREA_GIPHY"] = "Przeglądaj obrazy GIF";
$MESS["BX_MESSENGER_TEXTAREA_PLACEHOLDER"] = "Wprowadź wiadomość...";
$MESS["BX_MESSENGER_TEXTAREA_SMILE"] = "Wybierz emotikonę";
