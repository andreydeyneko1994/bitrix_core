<?php
$MESS["IM_RECENT_USERS_TYPING"] = "écrivent des messages...";
$MESS["IM_RECENT_USER_TYPING"] = "écrit un message...";
$MESS["IM_RECENT_WEEKDAY_0"] = "Lun";
$MESS["IM_RECENT_WEEKDAY_1"] = "Mar";
$MESS["IM_RECENT_WEEKDAY_2"] = "Mer";
$MESS["IM_RECENT_WEEKDAY_3"] = "Jeu";
$MESS["IM_RECENT_WEEKDAY_4"] = "Ven";
$MESS["IM_RECENT_WEEKDAY_5"] = "Sam";
$MESS["IM_RECENT_WEEKDAY_6"] = "Dim";
