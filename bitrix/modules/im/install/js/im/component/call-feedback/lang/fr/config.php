<?php
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_CHOOSE_ISSUE"] = "Sélectionnez votre problème si il est pertinent";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_FILLED"] = "Votre évaluation a été transmise à Bitrix24 !";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_AUDIO_QUALITY"] = "Problème de qualité du son";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_CALL_INTERFACE_PROBLEM"] = "Problème d'interface utilisateur des appels";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_CANT_HEAR_EACH_OTHER"] = "Problème de son, pas de son";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_CANT_SEE_EACH_OTHER"] = "Les participants ne peuvent pas se voir";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_DESCRIPTION"] = "Décrivez le problème rencontré";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_GOT_DISCONNECTED"] = "Déconnexion soudaine";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_OTHER"] = "Autre";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_RECORDING_PROBLEM"] = "Impossible d'enregistrer un appel";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_SCREEN_SHARING_PROBLEM"] = "Le partage d'écran ne fonctionne pas";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_ISSUE_VIDEO_QUALITY"] = "Mauvaise qualité vidéo";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_NO_ISSUE"] = "Aucun problème";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_RATE_QUALITY"] = "Évaluer la qualité de la connexion";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_SEND"] = "Envoyer";
$MESS["BX_IM_COMPONENT_CALL_FEEDBACK_VIDEOCALL_FINISHED"] = "Appel vidéo terminé";
