<?php
$MESS["BX_IM_COMPONENT_CALL_ALL_CHATS"] = "Wszystkie czaty";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_AS_GUEST"] = "Kontynuuj jako gość";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_AUTHORIZE"] = "Zaloguj się";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_CHECK_DEVICES"] = "Sprawdź sprzęt";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_CREATE_OWN"] = "Utwórz wideokonferencję w Bitrix24";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_DETAILS"] = "Dowiedz się więcej";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_DOWNLOAD_APP"] = "Nie masz aplikacji?";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_DOWNLOAD_APP_LINK"] = "Pobierz";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_JOIN"] = "Dołącz";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_OPEN_APP"] = "Możesz otworzyć wideokonferencję";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_OPEN_APP_LINK"] = "Możesz otworzyć konferencję w aplikacji Bitrix24";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_RELOAD"] = "Przeładuj";
$MESS["BX_IM_COMPONENT_CALL_BUTTON_START"] = "Rozpocznij połączenie";
$MESS["BX_IM_COMPONENT_CALL_CHANGE_LINK_CONFIRM_TEXT"] = "Wszyscy użytkownicy-goście zostaną usunięci z czatu. Czy na pewno tego chcesz?";
$MESS["BX_IM_COMPONENT_CALL_CHAT_TITLE"] = "Czat";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_AUDIO_SETTINGS"] = "Ustawienia dźwięku";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_AUTO_MIC_OPTIONS"] = "Automatyczna regulacja parametrów mikrofonu";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_BUTTON_BACK"] = "Wstecz";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_BUTTON_SAVE"] = "Zapisz";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_CAMERA"] = "Kamera";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_CHOOSE_CAMERA"] = "Wybierz kamerę";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_CHOOSE_MICRO"] = "Wybierz mikrofon";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_CHOOSE_SPEAKER"] = "Wybierz urządzenie wyjściowe dźwięku";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_DISABLED_CAMERA"] = "Kamera jest wyłączona";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_GETTING_CAMERA"] = "Pobieranie obrazu z kamery...";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_MICRO"] = "Mikrofon";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_NO_CAMERA"] = "Nie znaleziono kamery";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_NO_MICRO"] = "Nie znaleziono mikrofonu";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_NO_SPEAKER"] = "Nie znaleziono urządzeń wyjściowych dźwięku";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_NO_VIDEO"] = "Brak wideo";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_RECEIVE_HD"] = "Poproś o wideo w jakości HD";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_SPEAKER"] = "Dźwięk";
$MESS["BX_IM_COMPONENT_CALL_CHECK_DEVICES_VIDEO_SETTINGS"] = "Ustawienia wideo";
$MESS["BX_IM_COMPONENT_CALL_CLOSE_CONFIRM"] = "Czy na pewno chcesz zamknąć tę stronę? Jeśli zamkniesz tę stronę, połączenie zostanie rozłączone.";
$MESS["BX_IM_COMPONENT_CALL_DEFAULT_USER_NAME"] = "Gość";
$MESS["BX_IM_COMPONENT_CALL_ENABLE_DEVICES_BUTTON"] = "Włącz kamerę i mikrofon";
$MESS["BX_IM_COMPONENT_CALL_ERROR_FINISHED"] = "Wideokonferencja dobiegła końca";
$MESS["BX_IM_COMPONENT_CALL_ERROR_KICK_FROM_CALL"] = "Usunięto Cię z tej wideokonferencji.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_MAX_FILE_SIZE"] = "Przekroczono maksymalny rozmiar pliku: (#LIMIT# MB).";
$MESS["BX_IM_COMPONENT_CALL_ERROR_MESSAGE_B24_ONLY"] = "Wideokonferencje są dostępne tylko dla Bitrix24.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_MESSAGE_KICKED"] = "Usunięto Cię z tej wideokonferencji.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_MESSAGE_PLEASE_LOG_IN"] = "Znaleźliśmy Twoje konto w tym Bitrix24. Zaloguj się.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_MESSAGE_USER_LIMIT"] = "Osiągnięto maksymalną liczbę uczestników wideokonferencji.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_NOT_STARTED"] = "Wideokonferencja jeszcze się nie rozpoczęła.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_NO_HTTPS"] = "Użyj protokołu HTTPS";
$MESS["BX_IM_COMPONENT_CALL_ERROR_NO_MIC"] = "Aby dołączyć do wideokonferencji, musisz mieć mikrofon.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_NO_SIGNAL_FROM_CAMERA"] = "Nie mogliśmy pobrać obrazu wideo z kamery. Aby spróbować ponownie, kliknij poniższy przycisk.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_UNSUPPORTED_BROWSER"] = "Niestety Twoja przeglądarka nie obsługuje wideokonferencji.";
$MESS["BX_IM_COMPONENT_CALL_ERROR_USER_LEFT_THE_CALL"] = "Opuszczono konferencję. Aby ponownie dołączyć, zamknij kartę lub odśwież stronę.";
$MESS["BX_IM_COMPONENT_CALL_FILE"] = "[File]";
$MESS["BX_IM_COMPONENT_CALL_HARDWARE_ERROR"] = "Błąd podczas sprawdzania urządzeń";
$MESS["BX_IM_COMPONENT_CALL_INTRANET_LOGOUT"] = "Wyloguj";
$MESS["BX_IM_COMPONENT_CALL_INTRANET_NAME_TITLE"] = "Zalogowano się za pomocą konta Bitrix24";
$MESS["BX_IM_COMPONENT_CALL_INTRODUCE_YOURSELF"] = "Aby dołączyć do rozmowy, przedstaw się:";
$MESS["BX_IM_COMPONENT_CALL_INTRODUCE_YOURSELF_BUTTON"] = "Wyślij";
$MESS["BX_IM_COMPONENT_CALL_INTRODUCE_YOURSELF_PLACEHOLDER"] = "Twoja nazwa";
$MESS["BX_IM_COMPONENT_CALL_JOIN"] = "Dołącz";
$MESS["BX_IM_COMPONENT_CALL_JOIN_WITH_AUDIO"] = "Dołącz tylko z dźwiękiem";
$MESS["BX_IM_COMPONENT_CALL_JOIN_WITH_VIDEO"] = "Dołącz z obrazem wideo";
$MESS["BX_IM_COMPONENT_CALL_LINK_CHANGED"] = "Link do konferencji został zmieniony";
$MESS["BX_IM_COMPONENT_CALL_LINK_COPIED"] = "Odnośnik skopiowany";
$MESS["BX_IM_COMPONENT_CALL_LOADING"] = "Łączenie z wideokonferencją...";
$MESS["BX_IM_COMPONENT_CALL_NAME_PLACEHOLDER"] = "Wprowadź swoją nazwę";
$MESS["BX_IM_COMPONENT_CALL_NOT_ALLOWED_ERROR"] = "Do udziału w konferencji wymagana jest kamera i mikrofon.<br>Dostęp do kamery i mikrofonu jest teraz zabroniony.<br><br> Zezwól na dostęp w ustawieniach przeglądarki i kliknij \"Włącz kamerę i mikrofon\".";
$MESS["BX_IM_COMPONENT_CALL_OPEN_CHAT"] = "Otwórz czat";
$MESS["BX_IM_COMPONENT_CALL_OPEN_USER_LIST"] = "Uczestnicy";
$MESS["BX_IM_COMPONENT_CALL_PASSWORD_JOIN"] = "Dołącz";
$MESS["BX_IM_COMPONENT_CALL_PASSWORD_PLACEHOLDER"] = "Wprowadź hasło";
$MESS["BX_IM_COMPONENT_CALL_PASSWORD_TITLE"] = "Wymagane jest hasło";
$MESS["BX_IM_COMPONENT_CALL_PASSWORD_WRONG"] = "Nieprawidłowe hasło";
$MESS["BX_IM_COMPONENT_CALL_PERMISSIONS_BUTTON"] = "Zezwól";
$MESS["BX_IM_COMPONENT_CALL_PERMISSIONS_LOADING"] = "Uzyskiwanie dostępu do mikrofonu i kamery...";
$MESS["BX_IM_COMPONENT_CALL_PERMISSIONS_TEXT"] = "Aby dołączyć do wideokonferencji, potrzebny jest mikrofon i kamera";
$MESS["BX_IM_COMPONENT_CALL_PREPARE_NO_USERS"] = "Obecnie nie ma nikogo na tej wideokonferencji. Oczekiwanie na innych użytkowników.";
$MESS["BX_IM_COMPONENT_CALL_PREPARE_TITLE_JOIN_CALL"] = "Połączenie w toku. Aby dołączyć, kliknij poniższy link.";
$MESS["BX_IM_COMPONENT_CALL_PREPARE_TITLE_START_CALL"] = "Nikt Cię teraz nie widzi ani nie słyszy. Aby rozpocząć połączenie, kliknij poniższy przycisk.";
$MESS["BX_IM_COMPONENT_CALL_PREPARE_USER_COUNT"] = "Użytkownicy biorący udział w połączeniu: ";
$MESS["BX_IM_COMPONENT_CALL_ROTATE_DEVICE"] = "Obróć urządzenie";
$MESS["BX_IM_COMPONENT_CALL_SPEAKER"] = "Mówca";
$MESS["BX_IM_COMPONENT_CALL_SPEAKERS_MULTIPLE"] = "Mówcy";
$MESS["BX_IM_COMPONENT_CALL_START_WITH_AUDIO"] = "Zacznij tylko z dźwiękiem";
$MESS["BX_IM_COMPONENT_CALL_START_WITH_VIDEO"] = "Zacznij z obrazem wideo";
$MESS["BX_IM_COMPONENT_CALL_STATUS_LOADING"] = "Uzyskiwanie statusu";
$MESS["BX_IM_COMPONENT_CALL_STATUS_NOT_STARTED"] = "Jeszcze nie rozpoczęto";
$MESS["BX_IM_COMPONENT_CALL_STATUS_STARTED"] = "Rozpoczęto";
$MESS["BX_IM_COMPONENT_CALL_USERS_LIST_TITLE"] = "Uczestnicy";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_CATEGORY_PARTICIPANTS"] = "Uczestnicy";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_CATEGORY_PRESENTERS"] = "Mówcy";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_CURRENT_USER"] = "Ty";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_HEADER_MENU_CHANGE_LINK"] = "Edytuj link do konferencji";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_HEADER_MENU_COPY_LINK"] = "Skopiuj link do konferencji";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_INTRODUCE_YOURSELF"] = "Przedstaw się";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_LOADING"] = "Ładowanie...";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_MENU_INSERT_NAME"] = "Wstaw nazwę do czatu";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_MENU_KICK"] = "Usuń";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_MENU_RENAME"] = "Zmień nazwę";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_MENU_RENAME_SELF"] = "Zmień nazwę";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_STATUS_CURRENT_USER"] = "Ty";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_STATUS_OFFLINE"] = "Nieaktywny";
$MESS["BX_IM_COMPONENT_CALL_USER_LIST_STATUS_OWNER"] = "host";
$MESS["BX_IM_COMPONENT_CALL_WAIT_START_TITLE"] = "Zaczekaj na rozpoczęcie wideokonferencji";
$MESS["BX_IM_COMPONENT_CALL_WAIT_START_USER_COUNT"] = "Obecni użytkownicy czatu:";
