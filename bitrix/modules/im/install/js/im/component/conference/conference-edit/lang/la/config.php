<?php
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_CHECKBOX_LABEL"] = "Utilizar el modo de transmisión";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_HINT"] = "En el modo de transmisión, puede permitir que solo los oradores seleccionados transmitan video y sonido. Otros participantes (asistentes) pueden ver la transmisión y comunicarse mediante el chat.";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_LABEL"] = "Modo de transmisión";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_OFF"] = "No";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_ON"] = "Sí";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CANCEL"] = "Cancelar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CHAT"] = "Chat";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CREATE"] = "Crear";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_EDIT"] = "Editar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_INVITATION_COPY"] = "Copiar la invitación";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_SAVE"] = "Guardar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_START"] = "Iniciar";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_INVITATION"] = "#CREATOR# le invita a asistir a la conferencia
#TITLE#
Utilice este enlace para unirse: #LINK#";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_TITLE"] = "Nueva conferencia";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION"] = "Duracion";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_HOURS"] = "horas";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_MINUTES"] = "minutos";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_COPIED"] = "Invitación copiada";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_TITLE"] = "Mensaje de invitación a la videoconferencia";
$MESS["BX_IM_COMPONENT_CONFERENCE_NETWORK_ERROR"] = "No pudimos conectarnos a su Bitrix24, probablemente debido a una mala conexión a Internet. Intente de nuevo más tarde.";
$MESS["BX_IM_COMPONENT_CONFERENCE_NO_PASSWORD"] = "No";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_CHECKBOX_LABEL"] = "Utilice contraseña";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_EXISTS"] = "Sí";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_LABEL"] = "Contraseña";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_PLACEHOLDER"] = "Ingresar la contraseña";
$MESS["BX_IM_COMPONENT_CONFERENCE_PRESENTER_SELECTOR_LABEL"] = "Ponentes";
$MESS["BX_IM_COMPONENT_CONFERENCE_PUSH_ERROR"] = "Actualice su servidor Push para utilizar las conferencias";
$MESS["BX_IM_COMPONENT_CONFERENCE_START_DATE_AND_TIME"] = "Fecha y hora del evento";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_LABEL"] = "Nombre";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_PLACEHOLDER"] = "Ingrese su nombre";
$MESS["BX_IM_COMPONENT_CONFERENCE_USER_SELECTOR_LABEL"] = "Asistentes";
$MESS["BX_IM_COMPONENT_CONFERENCE_VOXIMPLANT_ERROR_WITH_LINK"] = "Debe configurar el entorno para utilizar la función de conferencia. <a onclick=\"top.BX.Helper.show('redirect=detail&code=11392174')\"> Más información</a>";
