<?php
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_CHECKBOX_LABEL"] = "Usar modo de Transmissão";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_HINT"] = "No modo de Transmissão, você pode permitir que apenas os palestrantes selecionados transmitam vídeo e som. Outros participantes (presentes) podem assistir à transmissão e se comunicar no chat.";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_LABEL"] = "Modo de Transmissão";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_OFF"] = "Não";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_ON"] = "Sim";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CANCEL"] = "Cancelar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CHAT"] = "Bate-papo";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CREATE"] = "Criar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_EDIT"] = "Editar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_INVITATION_COPY"] = "Copiar convite";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_SAVE"] = "Salvar";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_START"] = "Iniciar";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_INVITATION"] = "#CREATOR# convida você a participar da conferência
#TITLE#
Use este link para entrar: #LINK#";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_TITLE"] = "Nova conferência";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION"] = "Duração";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_HOURS"] = "horas";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_MINUTES"] = "minutos";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_COPIED"] = "Convite copiado";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_TITLE"] = "Mensagem de convite de videoconferência";
$MESS["BX_IM_COMPONENT_CONFERENCE_NETWORK_ERROR"] = "Não foi possível conectar seu Bitrix24, provavelmente devido à má conexão de Internet. Por favor, tente novamente mais tarde.";
$MESS["BX_IM_COMPONENT_CONFERENCE_NO_PASSWORD"] = "Não";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_CHECKBOX_LABEL"] = "Utilizar senha";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_EXISTS"] = "Sim";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_LABEL"] = "Senha";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_PLACEHOLDER"] = "Digitar senha";
$MESS["BX_IM_COMPONENT_CONFERENCE_PRESENTER_SELECTOR_LABEL"] = "Palestrantes";
$MESS["BX_IM_COMPONENT_CONFERENCE_PUSH_ERROR"] = "Por favor, atualize seu servidor Push para usar a conferência";
$MESS["BX_IM_COMPONENT_CONFERENCE_START_DATE_AND_TIME"] = "Data e horário do evento";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_LABEL"] = "Nome";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_PLACEHOLDER"] = "Inserir nome";
$MESS["BX_IM_COMPONENT_CONFERENCE_USER_SELECTOR_LABEL"] = "Participantes";
$MESS["BX_IM_COMPONENT_CONFERENCE_VOXIMPLANT_ERROR_WITH_LINK"] = "Você deve configurar o ambiente para usar o recurso de conferência. <a onclick=\"top.BX.Helper.show('redirect=detail&code=11392174')\"> Saiba mais</a>";
