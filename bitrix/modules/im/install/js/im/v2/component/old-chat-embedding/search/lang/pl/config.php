<?php
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_GROUP"] = "Czat prywatny";
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_OPEN"] = "Czat publiczny";
$MESS["IM_SEARCH_RESULT_NOT_FOUND"] = "Nie znaleziono wpisów.";
$MESS["IM_SEARCH_RESULT_NOT_FOUND_DESCRIPTION"] = "Wypróbuj inną frazę wyszukiwana.";
$MESS["IM_SEARCH_SECTION_CHAT_USERS"] = "Czatuje z użytkownikiem";
$MESS["IM_SEARCH_SECTION_NETWORK"] = "Zewnętrzne otwarte kanały";
$MESS["IM_SEARCH_SECTION_NETWORK_BUTTON"] = "Przeszukaj Bitrix24.Network";
$MESS["IM_SEARCH_SECTION_OPENLINES"] = "Otwarte kanały";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_LESS"] = "Pokaż mniej";
