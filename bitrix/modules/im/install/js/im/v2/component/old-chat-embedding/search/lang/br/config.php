<?php
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_GROUP"] = "Bate-papo privado";
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_OPEN"] = "Bate-papo público";
$MESS["IM_SEARCH_RESULT_NOT_FOUND"] = "Não foram encontradas entradas.";
$MESS["IM_SEARCH_RESULT_NOT_FOUND_DESCRIPTION"] = "Tente uma frase de pesquisa diferente.";
$MESS["IM_SEARCH_SECTION_CHAT_USERS"] = "Bate-papos com o usuário";
$MESS["IM_SEARCH_SECTION_NETWORK"] = "Canais Abertos Externos";
$MESS["IM_SEARCH_SECTION_NETWORK_BUTTON"] = "Pesquisar Bitrix24.Network";
$MESS["IM_SEARCH_SECTION_OPENLINES"] = "Canais Abertos";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_LESS"] = "Mostrar menos";
