<?php
$MESS["IM_RECENT_ACTIVE_CALL_HANGUP"] = "Colgar";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN"] = "Unirse";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_AUDIO"] = "Solo audio";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_VIDEO"] = "Con video";
$MESS["IM_RECENT_BIRTHDAY"] = "¡hoy es su cumpleaños!";
$MESS["IM_RECENT_BIRTHDAY_DATE"] = "hoy";
$MESS["IM_RECENT_CHAT_SELF"] = "es usted";
$MESS["IM_RECENT_CHAT_TYPE_GROUP"] = "Chat privado";
$MESS["IM_RECENT_CHAT_TYPE_OPEN"] = "Chat público";
$MESS["IM_RECENT_CONNECT_ERROR"] = "No pudimos conectarnos a su Bitrix24, probablemente debido a una mala conexión a Internet. Intente de nuevo más tarde.";
$MESS["IM_RECENT_DEFAULT_USER_TITLE"] = "Usuario";
$MESS["IM_RECENT_EMPTY"] = "No hay chats";
$MESS["IM_RECENT_INVITATION_NOT_ACCEPTED"] = "La invitación no fue aceptada";
$MESS["IM_RECENT_MESSAGE_DRAFT"] = "Borrador";
$MESS["IM_RECENT_NEW_USER_POPUP_TEXT"] = "¡Ahora estoy en el equipo!";
