<?php
$MESS["IM_RECENT_ACTIVE_CALL_HANGUP"] = "Desligar";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN"] = "Participar";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_AUDIO"] = "Apenas áudio";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_VIDEO"] = "Com vídeo";
$MESS["IM_RECENT_BIRTHDAY"] = "faz aniversário hoje!";
$MESS["IM_RECENT_BIRTHDAY_DATE"] = "hoje";
$MESS["IM_RECENT_CHAT_SELF"] = "você";
$MESS["IM_RECENT_CHAT_TYPE_GROUP"] = "Bate-papo privado";
$MESS["IM_RECENT_CHAT_TYPE_OPEN"] = "Bate-papo público";
$MESS["IM_RECENT_CONNECT_ERROR"] = "Não foi possível conectar seu Bitrix24, provavelmente devido à má conexão de Internet. Por favor, tente novamente mais tarde.";
$MESS["IM_RECENT_DEFAULT_USER_TITLE"] = "Usuário";
$MESS["IM_RECENT_EMPTY"] = "Não há bate-papos";
$MESS["IM_RECENT_INVITATION_NOT_ACCEPTED"] = "O convite não foi aceito";
$MESS["IM_RECENT_MESSAGE_DRAFT"] = "Rascunho";
$MESS["IM_RECENT_NEW_USER_POPUP_TEXT"] = "Já estou na sua equipe!";
