<?php
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_GROUP"] = "Chat privé";
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_OPEN"] = "Chat public";
$MESS["IM_SEARCH_RESULT_NOT_FOUND"] = "Aucune entrée n'a été trouvée.";
$MESS["IM_SEARCH_RESULT_NOT_FOUND_DESCRIPTION"] = "Essayez une autre phrase de recherche.";
$MESS["IM_SEARCH_SECTION_CHAT_USERS"] = "Chats avec les utilisateurs";
$MESS["IM_SEARCH_SECTION_NETWORK"] = "Canaux ouverts externes";
$MESS["IM_SEARCH_SECTION_NETWORK_BUTTON"] = "Rechercher dans Bitrix24.Network";
$MESS["IM_SEARCH_SECTION_OPENLINES"] = "Canaux ouverts";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_LESS"] = "Afficher moins";
