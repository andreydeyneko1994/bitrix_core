<?php
$MESS["IM_SEARCH_SECTION_CHAT_PARTICIPANTS"] = "Membres du chat";
$MESS["IM_SEARCH_SECTION_DEPARTMENTS"] = "Départements";
$MESS["IM_SEARCH_SECTION_RECENT"] = "Recherche récente";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_MORE"] = "Plus d'informations";
$MESS["IM_SEARCH_SECTION_USERS_AND_CHATS"] = "Utilisateurs et chats";
