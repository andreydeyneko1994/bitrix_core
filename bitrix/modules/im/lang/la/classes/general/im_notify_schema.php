<?php
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notificaciones de procesos de negocio";
$MESS["IM_NS_CHAT_NEW"] = "Mensajes en chat privado";
$MESS["IM_NS_DEFAULT"] = "Notificaciones no definidas";
$MESS["IM_NS_IM"] = "Chat & Llamadas";
$MESS["IM_NS_LIKE"] = "Likes de chats (“me gusta”)";
$MESS["IM_NS_MAIN"] = "Valoraciones y “me gusta”";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notificaciones de \"me gusta\"";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notificaciones de “me gusta” en las publicaciones donde le mencionaron";
$MESS["IM_NS_MENTION"] = "Usted ha sido mencionado en un chat público";
$MESS["IM_NS_MENTION_2"] = "Le mencionaron en un chat";
$MESS["IM_NS_MESSAGE_NEW"] = "Mensajes en chat";
$MESS["IM_NS_OPEN_NEW"] = "Mensajes en chat público";
