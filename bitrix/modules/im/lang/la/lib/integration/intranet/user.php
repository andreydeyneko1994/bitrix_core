<?
$MESS["IM_INT_USR_INVITE_USERS"] = "#USER_NAME# enviou convite para #USERS#.";
$MESS["IM_INT_USR_INVITE_USERS_F"] = "#USER_NAME# enviou convite para #USERS#.";
$MESS["IM_INT_USR_JOIN_2"] = "Me uní al equipo.";
$MESS["IM_INT_USR_JOIN_GENERAL_2"] = "Me uní al equipo.";
$MESS["IM_INT_USR_LINK_COPIED"] = "#USER_NAME# copió un enlace para invitar a nuevos empleados.";
$MESS["IM_INT_USR_LINK_COPIED_F"] = "#USER_NAME# copió un enlace para invitar a nuevos empleados.";
$MESS["IM_INT_USR_REGISTER_USERS"] = "#USER_NAME# registró a #USERS#.";
$MESS["IM_INT_USR_REGISTER_USERS_F"] = "#USER_NAME# registró a #USERS#";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_F"] = "#USER_NAME# revocó los permisos de administrador de #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_M"] = "#USER_NAME# revocó los permisos de administrador de #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_F"] = "#USER_NAME# concedió permisos de administrador a #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_M"] = "#USER_NAME# concedió permisos de administrador a #USERS#.";
?>