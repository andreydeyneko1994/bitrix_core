<?php
$MESS["IM_ZOOM_MESSAGE_CONFERENCE_CREATED"] = "Se creó una sala de reuniones en Zoom";
$MESS["IM_ZOOM_MESSAGE_INVITE_LINK"] = "Enlace de invitación";
$MESS["IM_ZOOM_MESSAGE_JOIN_LINK"] = "Enlace para unirse a la videoconferencia";
