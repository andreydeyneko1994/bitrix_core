<?php
$MESS["IM_CALL_CONFERENCE_ERROR_ADDING_USERS"] = "Error al agregar usuarios al chat";
$MESS["IM_CALL_CONFERENCE_ERROR_CREATING"] = "Error al crear la videoconferencia";
$MESS["IM_CALL_CONFERENCE_ERROR_DELETING_USERS"] = "Error al eliminar usuarios del chat";
$MESS["IM_CALL_CONFERENCE_ERROR_ID_NOT_PROVIDED"] = "ID de la videoconferencia inválido";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_LENGTH"] = "La invitación puede incluir hasta 255 caracteres";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_TYPE"] = "Invitación incorrecta";
$MESS["IM_CALL_CONFERENCE_ERROR_MAX_USERS"] = "Se excedió el máximo de participantes";
$MESS["IM_CALL_CONFERENCE_ERROR_NO_PRESENTERS"] = "La lista de ponentes no puede estar vacía";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_LENGTH_NEW"] = "La contraseña de la conferencia debe incluir tres o más caracteres";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_TYPE"] = "Contraseña de la videoconferencia incorrecta";
$MESS["IM_CALL_CONFERENCE_ERROR_RENAMING_CHAT"] = "Error al renombrar el chat";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_LENGTH"] = "El nombre de la videoconferencia debe tener dos caracteres o más";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_REQUIRED"] = "No se especificó el nombre de la videoconferencia";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_TYPE"] = "Nombre de la videoconferencia incorrecto";
$MESS["IM_CALL_CONFERENCE_ERROR_TOO_MANY_PRESENTERS"] = "Se seleccionaron demasiados ponentes";
