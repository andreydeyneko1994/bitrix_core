<?php
$MESS["CALL_RECORD_ERROR"] = "Możesz słuchać nagrań w swojej przeglądarce.";
$MESS["IM_AN_ACCESS"] = "Na tym czacie nie możesz publikować nowych wiadomości.";
$MESS["IM_APPS_LIST"] = "Pokaż dostępne aplikacje";
$MESS["IM_APPS_SALESCENTER_BUTTON"] = "Zacznij sprzedawać";
$MESS["IM_AV_NEXT_VERSION"] = "Ta opcja będzie niedostępna w przyszłych wersjach.";
$MESS["IM_BB_PUT"] = "Kliknij, aby wprowadzić wiadomość w pole tekstowe";
$MESS["IM_BB_SEND"] = "Kliknij, aby wykonać działanie";
$MESS["IM_BLOCK_BTN_ANSWER"] = "Odpowiedz";
$MESS["IM_BLOCK_BTN_BLOCK"] = "Zablokuj";
$MESS["IM_BLOCK_BTN_UNBLOCK"] = "Odblokuj";
$MESS["IM_CALL_BY_B24"] = "używając Bitrix24";
$MESS["IM_CALL_BY_MOBILE"] = "używając telefonu";
$MESS["IM_CALL_CHANGE_BACKGROUND"] = "Zmień tło";
$MESS["IM_CALL_CLOSE_DOCUMENT_EDITOR_NO"] = "Powrót do dokumentu";
$MESS["IM_CALL_CLOSE_DOCUMENT_EDITOR_TO_ANSWER"] = "Edytujesz dokument dołączony do poprzedniego połączenia. <br>Odebranie nowego połączenia spowoduje zamknięcie dokumentu i zapisanie go na czacie.<br><br>Zalecamy ręczne zamknięcie dokumentu, a następnie odebranie połączenia.";
$MESS["IM_CALL_CLOSE_DOCUMENT_EDITOR_YES"] = "Odbierz połączenie";
$MESS["IM_CALL_DEVICES_CLOSE"] = "Zamknij";
$MESS["IM_CALL_DEVICES_DETACHED"] = "Odłączone urządzenia:";
$MESS["IM_CALL_DEVICES_FOUND"] = "Wykryto nowe urządzenia:";
$MESS["IM_CALL_DEVICES_USE"] = "Użyj";
$MESS["IM_CALL_DOCUMENT_PROMO_ACTION"] = "Utwórz";
$MESS["IM_CALL_DOCUMENT_PROMO_ACTION_CLOSE"] = "Zamknij";
$MESS["IM_CALL_DOCUMENT_PROMO_DONT_SHOW_AGAIN"] = "Nie pokazuj ponownie";
$MESS["IM_CALL_DOCUMENT_PROMO_TEXT"] = "Zanotuj wyniki wydarzenia i plany na przyszłość.<br/>Edytuj dokument razem z innymi uczestnikami połączenia.";
$MESS["IM_CALL_DOCUMENT_PROMO_TITLE"] = "Utwórz agendę spotkania";
$MESS["IM_CALL_GUEST_INTRODUCE_YOURSELF"] = "Przedstaw się";
$MESS["IM_CALL_HELP"] = "Pomoc";
$MESS["IM_CALL_HELP_TEXT"] = "Uczestnicy połączenia będący w tej samej lokalizacji są automatycznie przypisywani do tego samego pokoju online. <br><br>Gdy ktoś mówi, wyciszamy inne mikrofony i dźwięk, aby uniknąć echa i zniekształceń dźwięku.";
$MESS["IM_CALL_INVITE_CALL_PARTICIPANTS"] = "Zaproś użytkownika";
$MESS["IM_CALL_INVITE_CANCEL"] = "Anuluj";
$MESS["IM_CALL_INVITE_INVITE"] = "Zaproś";
$MESS["IM_CALL_INVITE_INVITE_USER"] = "Zaproś użytkownika";
$MESS["IM_CALL_INVITE_MORE"] = "więcej";
$MESS["IM_CALL_INVITE_RECENT"] = "Ostatnie";
$MESS["IM_CALL_INVITE_SEARCH_RESULTS"] = "Wyniki wyszukiwania";
$MESS["IM_CALL_JOIN_ROOM"] = "Dołącz do pokoju";
$MESS["IM_CALL_LEAVE_ROOM"] = "Opuść pokój";
$MESS["IM_CALL_MIC_MUTED_WHILE_TALKING"] = "Nie słyszymy Cię. Sprawdź przycisk mikrofonu.";
$MESS["IM_CALL_MIC_MUTED_WHILE_TALKING_FOLDED_CALL_HOTKEY"] = "Aby wyłączyć wyciszenie mikrofonu, naciśnij <b>#HOTKEY#</b>";
$MESS["IM_CALL_MIC_MUTED_WHILE_TALKING_HOTKEY"] = "Aby tymczasowo wyłączyć wyciszenie, przytrzymaj <b>Spację</b>";
$MESS["IM_CALL_PIN"] = "Przypnij";
$MESS["IM_CALL_RECORD_NAME"] = "#CHAT_TITLE# – Połączenie #CALL_ID# – #DATE#";
$MESS["IM_CALL_ROOM_DESCRIPTION"] = "Pokój ##ROOM_ID# (#PARTICIPANTS_LIST#)";
$MESS["IM_CALL_ROOM_DETAILS"] = "Szczegóły";
$MESS["IM_CALL_ROOM_JOINED_AUTO"] = "Najwyraźniej dzielisz pokój z #PARTICIPANTS_LIST#. Aby zapewnić lepsze wrażenia, wyłączyliśmy Twój dźwięk i mikrofon.";
$MESS["IM_CALL_ROOM_JOINED_AUTO_SPEAKER"] = "Najwyraźniej dzielisz pokój z #PARTICIPANTS_LIST#. Aby uniknąć zniekształceń dźwięku, wyciszyliśmy pozostałych uczestników.";
$MESS["IM_CALL_ROOM_JOINED_MANUALLY"] = "Dołączono do pokoju ##ROOM_ID#. Aby zapewnić lepsze wrażenia, wyłączyliśmy Twój mikrofon.";
$MESS["IM_CALL_ROOM_JOINED_P2"] = "Wyciszenie mikrofonu można cofnąć, klikając ikonę.";
$MESS["IM_CALL_ROOM_JOINED_UNDERSTOOD"] = "OK, rozumiem";
$MESS["IM_CALL_ROOM_MIC_TAKEN_BY"] = "Dźwięk i mikrofon przejęte przez #USER_NAME#";
$MESS["IM_CALL_ROOM_MIC_TAKEN_FROM"] = "Przejęto dźwięk i mikrofon od #USER_NAME#";
$MESS["IM_CALL_ROOM_WRONG_ROOM"] = "Nie, jestem w innym pokoju";
$MESS["IM_CALL_SETTINGS_COPY"] = "Kopiuj";
$MESS["IM_CALL_SETTINGS_LINK"] = "Link publiczny";
$MESS["IM_CALL_SOUND_PLAYS_VIA"] = "Dźwięk odtwarzany przez";
$MESS["IM_CALL_UNMUTE_MIC"] = "Wyłącz wyciszenie";
$MESS["IM_CALL_UNPIN"] = "Odepnij";
$MESS["IM_CALL_USERS_SCREEN"] = "#NAME# (ekran)";
$MESS["IM_CALL_USER_OFFLINE"] = "Nie można wykonać połączenia, ponieważ użytkownik jest offline.";
$MESS["IM_CALL_WANTS_TO_SAY_F"] = "Użytkownik #NAME# podniósł rękę";
$MESS["IM_CALL_WANTS_TO_SAY_M"] = "Użytkownik #NAME# podniósł rękę";
$MESS["IM_CALL_WEB_SCREENSHARE_STATUS"] = "Udostępniasz ekran";
$MESS["IM_CALL_WEB_SCREENSHARE_STOP"] = "Zatrzymaj";
$MESS["IM_CL_BOT"] = "Czat bot";
$MESS["IM_CL_CHAT"] = "Czat grupy";
$MESS["IM_CL_CHAT_CALENDAR"] = "Czat wydarzenia";
$MESS["IM_CL_CHAT_CRM"] = "Czat CRM";
$MESS["IM_CL_CHAT_NEW"] = "Czat prywatny";
$MESS["IM_CL_CHAT_SONET_GROUP"] = "Czat grupy roboczej";
$MESS["IM_CL_CHAT_TASKS"] = "Czat zadania";
$MESS["IM_CL_CREATE_CHAT_NEW"] = "Utwórz czat prywatny";
$MESS["IM_CL_CREATE_OPEN_NEW"] = "Utwórz czat publiczny";
$MESS["IM_CL_CREATE_PRIVATE_NEW"] = "Utwórz czat osobisty";
$MESS["IM_CL_GROUP"] = "Grupy";
$MESS["IM_CL_HIDE"] = "ukryj";
$MESS["IM_CL_LINES"] = "Otwórz kanał";
$MESS["IM_CL_LOAD"] = "Ładowanie…";
$MESS["IM_CL_MORE"] = "#COUNT# więcej";
$MESS["IM_CL_MORE_2"] = "pokaż #COUNT#";
$MESS["IM_CL_MORE_BOT"] = "Pokaż czat boty";
$MESS["IM_CL_MORE_CALL"] = "Pokaż połączenia";
$MESS["IM_CL_MORE_CHAT_NEW"] = "Pokaż czaty prywatne";
$MESS["IM_CL_MORE_EXTRANET_NEW"] = "Pokaż czaty zewnętrzne";
$MESS["IM_CL_MORE_GROUP"] = "Pokaż więcej kategorii użytkowników";
$MESS["IM_CL_MORE_LINES"] = "Pokaż otwarte kanały";
$MESS["IM_CL_MORE_OPEN_NEW"] = "Pokaż czaty publiczne";
$MESS["IM_CL_MORE_PRIVATE_NEW"] = "Pokaż czaty osobiste";
$MESS["IM_CL_MORE_QUEUE"] = "Pokaż wszystkie kolejki";
$MESS["IM_CL_MORE_STRUCTURE"] = "Pokaż więcej działów";
$MESS["IM_CL_OL_QUEUE"] = "Kolejka Otwartego kanału";
$MESS["IM_CL_OPEN_CHAT_NEW"] = "Czat publiczny";
$MESS["IM_CL_PHONE"] = "Rozmowa telefoniczna";
$MESS["IM_CL_PRIVATE_CHAT_NEW"] = "Czat osobisty";
$MESS["IM_CL_RESENT_FORMAT_DATE"] = "D, j F";
$MESS["IM_CL_STRUCTURE"] = "Dział firmy";
$MESS["IM_CL_USER"] = "Użytkownik";
$MESS["IM_CL_USER_B24"] = "Pracownik";
$MESS["IM_CL_USER_EXTRANET"] = "Zewnętrzny użytkownik";
$MESS["IM_CL_USER_LINES"] = "Użytkownik Otwartego Kanału";
$MESS["IM_CL_VIDEOCONF"] = "Wideokonferencja HD";
$MESS["IM_CL_VIDEOCONF_CREATE_BUTTON_GET_LINK"] = "Uzyskaj link";
$MESS["IM_CL_VIDEOCONF_CREATE_BUTTON_START_MESSAGING"] = "Rozpocznij konferencję";
$MESS["IM_CL_VIDEOCONF_CREATE_DESC_1"] = "Z łatwością twórz wideokonferencje z osobami spoza Twojego Bitrix24.";
$MESS["IM_CL_VIDEOCONF_CREATE_DESC_2"] = "Wyślij link do klientów, partnerów i kontrahentów, aby mogli połączyć się z wideokonferencją.";
$MESS["IM_CL_VIDEOCONF_CREATE_ERROR_LINK"] = "Szczegóły";
$MESS["IM_CL_VIDEOCONF_CREATE_ERROR_TITLE"] = "Niestety nie możesz tworzyć wideokonferencji. Aby dowiedzieć się o możliwych problemach, przeczytaj stronę pomocy.";
$MESS["IM_CL_VIDEOCONF_CREATE_LINK_POPUP_COPY"] = "Kopiuj";
$MESS["IM_CL_VIDEOCONF_CREATE_LINK_POPUP_DESC"] = "Zaproś kolegów i partnerów na wideokonferencję";
$MESS["IM_CL_VIDEOCONF_CREATE_SERVER_CALL_WARNING"] = "Połączenia serwerowe są niedostępne. Wideokonferencja nie będzie w stanie obsłużyć więcej niż cztery osoby.";
$MESS["IM_CL_VIDEOCONF_CREATE_TITLE"] = "Utwórz wideokonferencję";
$MESS["IM_CL_VI_QUEUE"] = "Kolejka Telefonii";
$MESS["IM_COPIED"] = "Skopiowano";
$MESS["IM_CRM_ABOUT_COMPANY"] = "Firma";
$MESS["IM_CRM_ABOUT_CONTACT"] = "Kontakt";
$MESS["IM_CRM_ACTIVITIES"] = "Działania";
$MESS["IM_CRM_BTN_ACTIVITY"] = "Działania";
$MESS["IM_CRM_BTN_CURRENT_CALL"] = "komentarz do połączenia";
$MESS["IM_CRM_BTN_DEAL"] = "Deale";
$MESS["IM_CRM_BTN_INVOICE"] = "Faktury";
$MESS["IM_CRM_BTN_NEW_CONTACT"] = "Dodaj nowy kontakt";
$MESS["IM_CRM_BTN_NEW_LEAD"] = "Dodaj nowy lead";
$MESS["IM_CRM_DEALS"] = "Deale";
$MESS["IM_CRM_RESPONSIBILITY"] = "Osoba odpowiedzialna";
$MESS["IM_CTL_CHAT_BLOCKED"] = "Użytkownik zablokowany";
$MESS["IM_CTL_CHAT_BLOCKED_LIST"] = "Zablokowane";
$MESS["IM_CTL_CHAT_BOT"] = "Czat boty";
$MESS["IM_CTL_CHAT_CALL"] = "Połączenia";
$MESS["IM_CTL_CHAT_CHAT_NEW"] = "Czaty prywatne";
$MESS["IM_CTL_CHAT_EXTRANET"] = "Zewnętrzne czaty person-to-person";
$MESS["IM_CTL_CHAT_LINES"] = "Otwarte kanały";
$MESS["IM_CTL_CHAT_OL"] = "Open Channels";
$MESS["IM_CTL_CHAT_OL_QUEUE"] = "Kolejki Otwartego kanału";
$MESS["IM_CTL_CHAT_OPEN_NEW"] = "Czaty publiczne";
$MESS["IM_CTL_CHAT_PRIVATE_NEW"] = "Czaty osobiste";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "Działy firmy";
$MESS["IM_CTL_CHAT_VI_QUEUE"] = "Kolejki Telefonii";
$MESS["IM_C_ABOUT_CHAT"] = "Prywatny czat jest dostępny tylko dla zaproszonych użytkowników. #BR##BR# Możesz zapraszać nie tylko współpracowników ale także klientów, partnerów oraz inne osoby związane z twoim biznesem, które używają Bitrix24. Aby dodać nowego członka czatu wpisz jego imię i nazwisko, adres e-mail lub login. #BR##BR#Uzupełnij #PROFILE_START#swój profil#PROFILE_END#, aby inni mogli cię znaleźć.";
$MESS["IM_C_ABOUT_CHAT_CHAT"] = "Prywatny czat jest dostępny tylko dla indywidualnych użytkowników. #BR##BR# Ten czat jest doskonały dla rozmów biznesowych wymagających zaangażowania i uwagi konkretnych osób.";
$MESS["IM_C_ABOUT_CHAT_NEW"] = "Czat prywatny jest widoczny tylko dla zaproszonych użytkowników. #BR##BR# Użyj tego typu czatu, aby omawiać tematy dotyczące wyłącznie określonych osób. #BR##BR# Oprócz pracowników Twojej firmy, prywatny czat może obejmować klientów, partnerów, dostawców i inne osoby, które mają własne Bitrix24. Aby dodać nowego uczestnika czatu, wprowadź jego nazwę, adres e-mail lub alias. #BR##BR#Nie zapomnij #PROFILE_START#uzupełnić swojego profilu#PROFILE_END#, aby inni mogli Cię znaleźć.";
$MESS["IM_C_ABOUT_OPEN"] = "Czat publiczny jest dostępny dla wszystkich. #BR##BR# Używaj tego czatu aby dyskutować na tematy, które są ważne dla wszystkich w twojej firmie.#BR##BR# Gdy czat publiczny zostanie stworzony, zostaje wysłane powiadomienie do #CHAT_START#Czat ogólny#CHAT_END#. Twoi współpracownicy będą widzieć wiadomości i będą mogli dołączyć do czatu jeśli będą chcieli.";
$MESS["IM_C_ABOUT_OPEN_2"] = "Publiczny czat jest otwarty dla wszystkich twoich kolegów.#BR##BR# Używaj tego czatu do omawiania tematów ważnych dla wszystkich w firmie.#BR##BR# Twoi koledzy mogą przeglądać rozmowy z czatu i przyłączyć się jeżeli chcą.#BR##BR# Twoja wiadomość rozpoczynająca jest kluczowa, aby zachęcić innych do twojego nowego czatu.";
$MESS["IM_C_ABOUT_OPEN_NEW"] = "Czat publiczy jest widoczny dla wszystkich współpracowników. #BR##BR# Używaj czatu publicznego, aby omawiać kwestie dotyczące całej firmy. #BR##BR# Współpracownicy mogą przeglądać dziennik wiadomości czatu publicznego i dołączyć do rozmowy. #BR##BR# Twoja pierwsza wiadomość w czacie publicznym będzie używana jako opis czatu publicznego i będzie widoczna dla nowych uczestników.";
$MESS["IM_C_ABOUT_OPEN_SITE_NEW"] = "Czat publiczny jest widoczny dla wszystkich użytkowników. #BR##BR# Korzystaj z czatów publicznych, aby omawiać sprawy ważne dla wielu użytkowników. #BR##BR# Czytelnicy mogą przeglądać dziennik wiadomości czatu publicznego i przyłączyć się do rozmowy. #BR##BR# Twoja pierwsza wiadomość na czacie publicznym jest ważna, gdyż będzie używana jako opis czatu publicznego dla nowych uczestników.";
$MESS["IM_C_ABOUT_PRIVATE"] = "Czast z osobą jest widoczny dla Ciebie i drugiego rozmówcy. #BR##BR# Znajdź osobę, z którą chcesz rozmawiać poprzez jej imię i nazwisko, stanowisko lub dział. #BR##BR# Możesz zapraszać nie tylko współpracowników ale także klientów, partnerów oraz inne osoby związane z Twoim bizesem, które używają Bitrix24. #BR##BR# Uzupełnij #PROFILE_START#swój profil#PROFILE_END# aby inni mogli Cię znaleźć.";
$MESS["IM_C_ABOUT_PRIVATE_NEW"] = "Czat osobisty jest widoczny tylko dla Ciebie i Twojego kontaktu. #BR##BR# Możesz znaleźć osobę do rozmowy, podając jej imię i nazwisko, stanowisko lub dział. #BR##BR# Możesz wysyłać wiadomości do współpracowników, klientów, partnerów, dostawców i innych osób, które mają własne Bitrix24. Po prostu wprowadź ich nazwę, adres e-mail lub alias, którego użyli na profilu. #BR##BR# Nie zapomnij #PROFILE_START#uzupełnić swojego profilu#PROFILE_END#, aby inni mogli Cię znaleźć.";
$MESS["IM_C_ABOUT_PRIVATE_SITE"] = "Czat person-to-person jest widoczny tylko dla ciebie i twojego rozmówcy. #BR##BR# Wyszukaj osobę, z którą chcesz rozmawiać po imieniu, stanowisku lub dziale.";
$MESS["IM_C_ABOUT_PRIVATE_SITE_NEW"] = "Czat osobisty jest widoczny tylko dla Ciebie i Twojego kontaktu. #BR##BR# Możesz znaleźć osobę, z którą chcesz porozmawiać, podając jej imię lub nazwisko.";
$MESS["IM_C_CHAT_TITLE_NEW"] = "Wpisz tytuł czatu publicznego";
$MESS["IM_C_EMPTY"] = "Brak członków";
$MESS["IM_C_PRIVATE_TITLE"] = "Wpisz imię i nazwisko; stanowisko lub dział";
$MESS["IM_DIALOG_ID_COPY_DONE"] = "Identyfikator czatu zewnętrznego został skopiowany do schowka (dialogId: #DIALOG_ID#)";
$MESS["IM_D_SEND_PUBLIC_ADMIN"] = "Musisz włączyć linkowanie do dokumentów publicznych w swoich Bitrix24 przed wysłaniem plików do klientów.";
$MESS["IM_D_SEND_PUBLIC_ADMIN_ON"] = "Włącz";
$MESS["IM_D_SEND_PUBLIC_ADMIN_READY"] = "Linkowanie do publicznych dokumentów jest teraz włączone w Twoim Bitrix24.";
$MESS["IM_D_SEND_PUBLIC_USER"] = "Musisz włączyć linkowanie do dokumentów publicznych w swoich Bitrix24 przed wysłaniem plików do klientów. Prosimy o skontaktowanie się z Twoim administratorem Bitrix24, aby uruchomić publiczne linkowanie.";
$MESS["IM_D_ZOOM_LEVEL"] = "Przybliżenie: #PERCENT#%";
$MESS["IM_D_ZOOM_LEVEL_RESET"] = "Reset";
$MESS["IM_ERROR_CODE"] = "ID";
$MESS["IM_FORM_BUTTON_HIDE"] = "Ukryj";
$MESS["IM_FORM_BUTTON_SEND"] = "Wyślij";
$MESS["IM_FORM_REQUIRED_FIELD"] = "To pole jest wymagane.";
$MESS["IM_FORM_SELECT_NO"] = "Nie";
$MESS["IM_FORM_SELECT_YES"] = "Tak";
$MESS["IM_FORM_SEND_DONE"] = "Dziękujemy! Formularz został przesłany pomyślnie.";
$MESS["IM_FUNCTION_FOR_BROWSER"] = "Niestety ta funkcja jest tymczasowo dostępna tylko w wersji internetowej.";
$MESS["IM_F_ATTACH"] = "Załącznik";
$MESS["IM_F_BOT_NOT_SUPPORT"] = "Wysyłanie plików do czat bota jest chwilowo niedostępne";
$MESS["IM_F_CANCEL"] = "Anuluj ładowanie";
$MESS["IM_F_CLICK_TO_PREVIEW"] = "Kliknij, aby uzyskać podgląd";
$MESS["IM_F_DELETE"] = "Usuń";
$MESS["IM_F_DELETED"] = "Plik został usunięty.";
$MESS["IM_F_DELETE_CONFIRM"] = "Plik zostanie usunięty i będzie niedostępny dla wszystkich. Kontynuować?";
$MESS["IM_F_DELETE_CONFIRM_YES"] = "Tak, usuń";
$MESS["IM_F_DELETE_SELF_CONFIRM"] = "Czy jesteś pewien, że chcesz usunąć ten plik?";
$MESS["IM_F_DND_TEXT"] = "Załaduj plik lub obraz";
$MESS["IM_F_DOWNLOAD"] = "Pobierz";
$MESS["IM_F_DOWNLOAD_DISK"] = "Zapisz na dysku";
$MESS["IM_F_EFP"] = "Transfer pliku zostanie przerwany jeżeli zamkniesz stronę lub użyjesz przycisków nawigacji";
$MESS["IM_F_ERROR"] = "Błąd ładowania";
$MESS["IM_F_FILE"] = "Plik";
$MESS["IM_F_NW20_NOT_SUPPORT"] = "Wysyłanie plików do użytkownika Bitrix24.Network jest czasowo niedostępne.";
$MESS["IM_F_PREVIEW_WINDOW"] = "Widok pliku";
$MESS["IM_F_RATING"] = "Ocena";
$MESS["IM_F_UPLOAD"] = "Ładowanie pliku";
$MESS["IM_F_UPLOAD_2"] = "Plik załadowany: #PERCENT#%";
$MESS["IM_G_ACCESS"] = "Twoje konto Bitrix24 zostało ograniczone przez administratora do postów na tym czacie";
$MESS["IM_G_JOIN"] = "Czat ogólny zawiera wszystkie konta. Aby nie otrzymywać powiadomień z czatu ogólnego kliknij #ICON# #LINK_START#Dowiedz się więcej o \"Czacie ogólnym\"#LINK_END#";
$MESS["IM_G_JOIN_F"] = "Przeczytałam to";
$MESS["IM_G_JOIN_LINK"] = "https://helpdesk.bitrix24.com/open/1545004/";
$MESS["IM_G_JOIN_M"] = "Przeczytałem to";
$MESS["IM_LINK_MORE"] = "Szczegóły";
$MESS["IM_LS_F"] = "#POSITION#. Ostatnio dostępny #LAST_SEEN#";
$MESS["IM_LS_M"] = "#POSITION#. Ostatnio dostępny #LAST_SEEN#";
$MESS["IM_LS_SHORT_F"] = "Ostatnio dostępny #LAST_SEEN#";
$MESS["IM_LS_SHORT_M"] = "Ostatnio dostępny #LAST_SEEN#";
$MESS["IM_MENU_TO_CALEND"] = "Stwórz nowy kalendarz wydarzeń";
$MESS["IM_MENU_TO_CHAT"] = "Porozmawiaj na nowym czacie";
$MESS["IM_MENU_TO_OL_QA"] = "Zapisz jako automatyczną odpowiedź";
$MESS["IM_MENU_TO_OL_QA_ADDED"] = "Już dodano do zapisanych odpowiedzi";
$MESS["IM_MENU_TO_OL_START"] = "Rozpocznij nową rozmowę";
$MESS["IM_MENU_TO_POST_2"] = "Omów w Aktualnościach";
$MESS["IM_MENU_TO_TASK"] = "Utwórz zadanie";
$MESS["IM_MESSAGE_DISLIKE"] = "Nie lubię";
$MESS["IM_MESSAGE_LIKE"] = "Lubię to";
$MESS["IM_MESSAGE_LIKE_LIST"] = "Kliknij, aby wyświetlic listę";
$MESS["IM_M_BIRTHDAY_MESSAGE_SHORT"] = "Dzisiaj urodziny!";
$MESS["IM_M_CALL_ADVANCED_SETTINGS"] = "Ustawienia zaawansowane";
$MESS["IM_M_CALL_BACKGROUND_CHANGE"] = "Zmień tło";
$MESS["IM_M_CALL_BTN_ADD"] = "Dodaj";
$MESS["IM_M_CALL_BTN_ANSWER"] = "Odpowiedz";
$MESS["IM_M_CALL_BTN_ANSWER_CONFERENCE"] = "Otwórz wideokonferencję";
$MESS["IM_M_CALL_BTN_ANSWER_VIDEO"] = "Odpowiedz w formie wideo";
$MESS["IM_M_CALL_BTN_CAMERA"] = "Kamera";
$MESS["IM_M_CALL_BTN_CHAT"] = "Czat";
$MESS["IM_M_CALL_BTN_CLOSE"] = "Zamknij";
$MESS["IM_M_CALL_BTN_DECLINE"] = "Odrzuć";
$MESS["IM_M_CALL_BTN_DISCONNECT"] = "Rozłącz";
$MESS["IM_M_CALL_BTN_DOCUMENT"] = "Dokument";
$MESS["IM_M_CALL_BTN_FULLSCREEN"] = "Pełny ekran";
$MESS["IM_M_CALL_BTN_GRID"] = "Siatka";
$MESS["IM_M_CALL_BTN_HANGUP"] = "Rozłącz";
$MESS["IM_M_CALL_BTN_HISTORY"] = "Historia";
$MESS["IM_M_CALL_BTN_HOLD"] = "Wstrzymaj";
$MESS["IM_M_CALL_BTN_JOIN"] = "Dołącz";
$MESS["IM_M_CALL_BTN_JOIN_MENU_AUDIO"] = "Brak wideo";
$MESS["IM_M_CALL_BTN_JOIN_MENU_VIDEO"] = "Włącz wideo";
$MESS["IM_M_CALL_BTN_LINK"] = "Link";
$MESS["IM_M_CALL_BTN_MENU"] = "Menu";
$MESS["IM_M_CALL_BTN_MIC"] = "Mikrofon";
$MESS["IM_M_CALL_BTN_MIC_OFF"] = "wyłączony";
$MESS["IM_M_CALL_BTN_MIC_ON"] = "włączony";
$MESS["IM_M_CALL_BTN_MIC_TITLE"] = "Aktywuj/Dezaktywuj mikrofon";
$MESS["IM_M_CALL_BTN_NEXT"] = "Następne";
$MESS["IM_M_CALL_BTN_NOTIFY_ADMIN"] = "Powiadom administratora";
$MESS["IM_M_CALL_BTN_RECALL"] = "Ponowne wybieranie";
$MESS["IM_M_CALL_BTN_RECORD"] = "Rekord";
$MESS["IM_M_CALL_BTN_RETURN"] = "Kontynuuj połączenie";
$MESS["IM_M_CALL_BTN_RETURN_TO_CALL"] = "Wróć do połączenia";
$MESS["IM_M_CALL_BTN_SCREEN"] = "Ekran";
$MESS["IM_M_CALL_BTN_SKIP_CONFERENCE"] = "Pomiń";
$MESS["IM_M_CALL_BTN_SPEAKER"] = "Audio";
$MESS["IM_M_CALL_BTN_TRANSFER"] = "Przekieruj";
$MESS["IM_M_CALL_BTN_UNFOLD"] = "Rozwiń";
$MESS["IM_M_CALL_BTN_USERS"] = "Uczestnicy";
$MESS["IM_M_CALL_BTN_WANT_TO_SAY"] = "Podnieś rękę";
$MESS["IM_M_CALL_CURRENT_PRESENTER"] = "Obecny prezenter";
$MESS["IM_M_CALL_DEVICE_NO_NAME"] = "Brak nazwy";
$MESS["IM_M_CALL_ERR"] = "Nie można ustanowić połączenia";
$MESS["IM_M_CALL_FROM"] = "Połączenie od";
$MESS["IM_M_CALL_FROM_CHAT"] = "Połączenie z czatu";
$MESS["IM_M_CALL_FULLSCREEN_MODE"] = "Pełny ekran";
$MESS["IM_M_CALL_GRID_MODE"] = "Tryb siatki";
$MESS["IM_M_CALL_HD_VIDEO"] = "Wideo HD";
$MESS["IM_M_CALL_HELP"] = "Pomoc";
$MESS["IM_M_CALL_MENU_CREATE_FILE"] = "Utwórz dokument";
$MESS["IM_M_CALL_MENU_CREATE_FILE_DOC"] = "Dokument";
$MESS["IM_M_CALL_MENU_CREATE_FILE_PPT"] = "Prezentacja";
$MESS["IM_M_CALL_MENU_CREATE_FILE_XLS"] = "Arkusz kalkulacyjny";
$MESS["IM_M_CALL_MENU_CREATE_RESUME"] = "Agenda spotkania";
$MESS["IM_M_CALL_MENU_LOADING_RESUME_LIST"] = "Ładowanie listy...";
$MESS["IM_M_CALL_MENU_NO_RESUME"] = "Niestety na tym czacie nie ma żadnych agend";
$MESS["IM_M_CALL_MENU_OPEN_LAST_RESUME"] = "Otwórz ostatnie agendy";
$MESS["IM_M_CALL_MENU_RECORD_AUDIO"] = "Tylko dźwięk";
$MESS["IM_M_CALL_MENU_RECORD_VIDEO"] = "Wideo i dźwięk";
$MESS["IM_M_CALL_MIC_AUTO_GAIN"] = "Użyj automatycznej regulacji wzmocnienia mikrofonu";
$MESS["IM_M_CALL_MIRRORING_VIDEO"] = "Odbicie lustrzane wideo";
$MESS["IM_M_CALL_MOBILE_MENU_CANCEL"] = "Anuluj";
$MESS["IM_M_CALL_MOBILE_MENU_CHANGE_MY_NAME"] = "Zmień moją nazwę";
$MESS["IM_M_CALL_MOBILE_MENU_COPY_INVITE"] = "Skopiuj zaproszenie";
$MESS["IM_M_CALL_MOBILE_MENU_PARTICIPANTS_LIST"] = "Uczestnicy";
$MESS["IM_M_CALL_MOBILE_MENU_PIN"] = "Przypnij";
$MESS["IM_M_CALL_MOBILE_MENU_UNPIN"] = "Odepnij";
$MESS["IM_M_CALL_MOBILE_MENU_WRITE_TO_PRIVATE_CHAT"] = "Wyślij wiadomość prywatną";
$MESS["IM_M_CALL_MOBILE_RENAME_CONFIRM"] = "Zapisz";
$MESS["IM_M_CALL_MUTE_SPEAKERS_OFF"] = "Dźwięk wyłączony";
$MESS["IM_M_CALL_MUTE_SPEAKERS_ON"] = "Dźwięk włączony ";
$MESS["IM_M_CALL_PARTICIPANTS"] = "Uczestników: #COUNT#";
$MESS["IM_M_CALL_PINNED_USER"] = "Przypięty użytkownik";
$MESS["IM_M_CALL_PROTECTED"] = "CHRONIONE";
$MESS["IM_M_CALL_PROTECTED_HINT"] = "Twoje połączenie jest szyfrowane. Nikt nie może podsłuchiwać Twojej rozmowy.";
$MESS["IM_M_CALL_RECORD_HINT"] = "Konferencja rejestrowana przez #USER_NAME#";
$MESS["IM_M_CALL_RECORD_TITLE"] = "Rekord";
$MESS["IM_M_CALL_SCREENSHARE_BACK_TO_CALL"] = "Powrót do połączenia";
$MESS["IM_M_CALL_SCREENSHARE_CHANGE_SCREEN"] = "Zmień okno";
$MESS["IM_M_CALL_SCREENSHARE_STOP"] = "Zatrzymaj udostępnianie ekranu";
$MESS["IM_M_CALL_SCREENSHARE_TITLE"] = "Udostępnianie ekranu jest aktywne";
$MESS["IM_M_CALL_SPEAKER_MODE"] = "Tryb mówcy";
$MESS["IM_M_CALL_STATUS_CONNECTION_ERROR"] = "Błąd połączenia";
$MESS["IM_M_CALL_STATUS_DECLINED"] = "Użytkownik odrzucił połączenie";
$MESS["IM_M_CALL_STATUS_UNAVAILABLE"] = "Brak odpowiedzi";
$MESS["IM_M_CALL_STATUS_VIDEO_PAUSED"] = "Wideo wstrzymane";
$MESS["IM_M_CALL_STATUS_WAIT_ANSWER"] = "Oczekiwanie na odpowiedź";
$MESS["IM_M_CALL_STATUS_WAIT_CONNECT"] = "Oczekiwanie na połączenie";
$MESS["IM_M_CALL_ST_BUSY"] = "Nie można wykonać połączenia, ponieważ rozmówca prowadzi rozmowę.";
$MESS["IM_M_CALL_ST_CONNECT"] = "Łączenie...";
$MESS["IM_M_CALL_ST_DECLINE"] = "Użytkownik anulował połączenie";
$MESS["IM_M_CALL_ST_END"] = "Połączenie zostało zakończone";
$MESS["IM_M_CALL_ST_INVITE"] = "Użytkownik oczekuje.";
$MESS["IM_M_CALL_ST_INVITE_2"] = "Użytkownicy oczekujący w kolejce";
$MESS["IM_M_CALL_ST_NO_ACCESS"] = "Nie można wykonać połączenia, ponieważ brak dostępu do kamery i/lub mikrofonu.";
$MESS["IM_M_CALL_ST_NO_ACCESS_2"] = "Rozłączono połączenie: być może rozmówca nie posiada kamery lub mikrofonu.";
$MESS["IM_M_CALL_ST_NO_ACCESS_3"] = "Nie zrealizowano połączenia, ponieważ brak dostępu do kamery lub mikrofonu.";
$MESS["IM_M_CALL_ST_NO_ACCESS_HTTPS"] = "Przeglądarki internetowe umożliwiają prowadzenie rozmów telefonicznych jedynie przez strony z ważnym certyfikatem SSL.";
$MESS["IM_M_CALL_ST_NO_ANSWER"] = "Użytkownik nie odpowiada.";
$MESS["IM_M_CALL_ST_NO_ANSWER_2"] = "Użytkownik nie odbiera";
$MESS["IM_M_CALL_ST_ONLINE"] = "Ustanowiono połączenie.";
$MESS["IM_M_CALL_ST_TRANSFER"] = "Przekierowane połączenie: oczekiwanie na odpowiedź";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "Przekierowane połączenie: osoba wywoływana nie odpowiada";
$MESS["IM_M_CALL_ST_TRANSFER_403"] = "Przekierowanie połączenia: niewystarczające uprawnienia";
$MESS["IM_M_CALL_ST_TRANSFER_410"] = "Przekierowanie połączenia: pracownik rozłączył się";
$MESS["IM_M_CALL_ST_TRANSFER_486"] = "Przekierowanie połączenia: pracownik obsługuje inne połączenie";
$MESS["IM_M_CALL_ST_TRANSFER_CONNECTED"] = "Przekierowanie połączenia: połączenie nawiązane";
$MESS["IM_M_CALL_ST_WAIT"] = "Oczekiwanie na odpowiedź użytkownika…";
$MESS["IM_M_CALL_ST_WAIT_2"] = "Oczekiwana odpowiedź od użytkownika";
$MESS["IM_M_CALL_ST_WAIT_ACCESS_2"] = "Oczekiwanie na połączenie użytkownika…";
$MESS["IM_M_CALL_ST_WAIT_ACCESS_3"] = "Oczekiwanie aż użytkownika się połączy";
$MESS["IM_M_CALL_TURN_UNAVAILABLE"] = "Błąd sieci: nie można połączyć z serwerem STUN lub TURN";
$MESS["IM_M_CALL_USER_BUSY_F"] = "#NAME# jest w trakcie innego połączenia";
$MESS["IM_M_CALL_USER_BUSY_M"] = "#NAME# jest w trakcie innego połączenia";
$MESS["IM_M_CALL_USER_CONNECTED_F"] = "Dołączył do połączenia: #NAME#";
$MESS["IM_M_CALL_USER_CONNECTED_M"] = "Dołączył do połączenia: #NAME#";
$MESS["IM_M_CALL_USER_DECLINED_F"] = "Odrzucił połączenie: #NAME#";
$MESS["IM_M_CALL_USER_DECLINED_M"] = "Odrzucił połączenie: #NAME#";
$MESS["IM_M_CALL_USER_DISCONNECTED_F"] = "Opuścił połączenie: #NAME#";
$MESS["IM_M_CALL_USER_DISCONNECTED_M"] = "Opuścił połączenie: #NAME#";
$MESS["IM_M_CALL_USER_FAILED"] = "Nie można nawiązać połączenia z użytkownikiem #NAME#";
$MESS["IM_M_CALL_VOICE_FROM"] = "Połączenie od #USER#";
$MESS["IM_M_CALL_VOICE_TO"] = "Połączenie z #USER#";
$MESS["IM_M_CALL_WINDOW_MODE"] = "Tryb okienkowy";
$MESS["IM_M_CALL_WITH"] = "Połączenie z #USER_NAME#";
$MESS["IM_M_CL_EMPTY"] = "Brak kontaktów";
$MESS["IM_M_CL_SEARCH"] = "Wyszukiwanie...";
$MESS["IM_M_CL_UNREAD"] = "Nowe wiadomości";
$MESS["IM_M_CODE_BLOCK"] = "Blok kodu";
$MESS["IM_M_CTL_EMPTY"] = "- Brak czatów -";
$MESS["IM_M_DELETED"] = "Ta wiadomość została usunięta.";
$MESS["IM_M_DELIVERED"] = "wiadomość została wysłana";
$MESS["IM_M_GROUP_CALL_WITH"] = "Połączenie grupowe z #CHAT_NAME#";
$MESS["IM_M_ICON"] = "ikonka";
$MESS["IM_M_LOAD_ERROR"] = "Nie udało się załadować wiadomości. Proszę sprawdzić swoje ustawienia sieciowe.";
$MESS["IM_M_LOAD_MESSAGE"] = "Ładowanie wiadomości…";
$MESS["IM_M_LOAD_NEXT"] = "Załaduj nowsze wiadomości";
$MESS["IM_M_LOAD_PREVIOUS"] = "Załaduj wcześniejsze wiadomości";
$MESS["IM_M_LOAD_USER"] = "Ładowanie danych";
$MESS["IM_M_MENU_APP_EXISTS"] = "Dostępnych jest więcej akcji.";
$MESS["IM_M_MESSAGE_FORMAT_TIME"] = "G:i";
$MESS["IM_M_MESSAGE_TITLE_FORMAT_DATE"] = "l, j F Y";
$MESS["IM_M_NOT_DELIVERED"] = "wiadomość nie została dostarczona";
$MESS["IM_M_NO_MESSAGE"] = "Brak wiadomości";
$MESS["IM_M_NO_MESSAGE_2"] = "Brak wiadomości z ostatnich 30 dni";
$MESS["IM_M_NO_MESSAGE_BOT"] = "Cześć, tutaj #BOT_NAME#, czat bot.";
$MESS["IM_M_NO_MESSAGE_LOAD"] = "Ładuj wcześniejsze wiadomości";
$MESS["IM_M_OL_ADD_LEAD"] = "Zapisz do CRM";
$MESS["IM_M_OL_ANSWER_AND_CLOSE"] = "Zaakceptuj i zamknij rozmowę";
$MESS["IM_M_OL_ASSIGN_OFF"] = "Odblokuj";
$MESS["IM_M_OL_ASSIGN_ON"] = "Zablokuj jako mój";
$MESS["IM_M_OL_CLOSE"] = "Zakończ rozmowę";
$MESS["IM_M_OL_CLOSE_ON_OPERATOR"] = "Zamknij rozmowę z bieżącego poziomu";
$MESS["IM_M_OL_EMPTY"] = "Brak rozmów.";
$MESS["IM_M_OL_FORCE_CLOSE"] = "Oznacz jako spam/ <br> Wymuś zamknięcie";
$MESS["IM_M_OL_GOTO_CRM"] = "Przejdź do CRM";
$MESS["IM_M_OL_GOTO_CRM_COMPANY"] = "Wyświetl firmę";
$MESS["IM_M_OL_GOTO_CRM_CONTACT"] = "Wyświetl kontakt";
$MESS["IM_M_OL_GOTO_CRM_DEAL"] = "Wyświetl deala";
$MESS["IM_M_OL_GOTO_CRM_LEAD"] = "Wyświetl leada";
$MESS["IM_M_OL_GOTO_LINK"] = "Przejdź do wiadomości";
$MESS["IM_M_OL_INTERCEPT"] = "Weź rozmowę";
$MESS["IM_M_OL_PAUSE_OFF"] = "Wznów automatyczne zamykanie";
$MESS["IM_M_OL_PAUSE_ON"] = "Wstrzymaj automatyczne zamykanie";
$MESS["IM_M_OL_PIN_OFF"] = "Odepnij rozmowę";
$MESS["IM_M_OL_PIN_ON"] = "Przypnij rozmowę";
$MESS["IM_M_OL_SILENT_OFF"] = "Tryb cichy wyłączony";
$MESS["IM_M_OL_SILENT_ON"] = "Tryb cichy włączony";
$MESS["IM_M_OL_SPAM"] = "Oznacz jako spam";
$MESS["IM_M_OPEN"] = "Otwórz";
$MESS["IM_M_OPEN_EXTRA_TITLE"] = "Kliknij, aby otworzyć menu lub kliknij trzymając #SHORTCUT#, aby zacytować wiadomość";
$MESS["IM_M_PAUSE"] = "Kliknij, aby wykonać";
$MESS["IM_M_QUOTE_BLOCK"] = "Cytat";
$MESS["IM_M_READED"] = "Wiadomość przeczytana #DATE#";
$MESS["IM_M_READED_CHAT"] = "Wiadomość odczytana przez: #USERS#";
$MESS["IM_M_READED_CHAT_MORE"] = "#USER# i #LINK_START##COUNT# więcej użytkowników#LINK_END#";
$MESS["IM_M_RECENT_MORE"] = "Ponad 30 dni temu";
$MESS["IM_M_REPLY"] = "Skomentuj";
$MESS["IM_M_REPLY_TITLE"] = "Dodaj komentarz do tej wiadomości";
$MESS["IM_M_RETRY"] = "Kliknij, aby spróbować ponownie";
$MESS["IM_M_ST_ONLINE_F"] = "zalogowana do sieci";
$MESS["IM_M_ST_ONLINE_F_B24"] = "zalogowana do Bitrix24";
$MESS["IM_M_ST_ONLINE_M"] = "zalogowany do sieci";
$MESS["IM_M_ST_ONLINE_M_B24"] = "zalogowany do Bitrix24";
$MESS["IM_M_USER_NO_ACCESS"] = "Niedozwolone.";
$MESS["IM_M_VIDEO_CALL_FROM"] = "Połączenie wideo od";
$MESS["IM_M_VIDEO_CALL_FROM_CHAT"] = "Połączenie wideo z czatu";
$MESS["IM_M_WRITING"] = "#USER_NAME# pisze wiadomość…";
$MESS["IM_M_WRITING_BOX"] = "pisze wiadomość…";
$MESS["IM_N_REPLY"] = "Odpowiedź";
$MESS["IM_N_REPLY_TEXT"] = "Twoja odpowiedź została wysłana.";
$MESS["IM_OL_ANSWERS_SOON"] = "Typowa odpowiedź będzie dostępna w nadchodzących aktualizacjach Bitrix24.";
$MESS["IM_OL_CHAT_BLOCK_DEFAULT"] = "Upłynął limit czasu odpowiedzi. Możesz zakończyć rozmowę lub poczekać na nowe wiadomości od klienta.";
$MESS["IM_OL_CHAT_BLOCK_USER"] = "Użytkownik zdecydował się blokować dalsze odpowiedzi. Możesz zakończyć rozmowę.";
$MESS["IM_OL_CHAT_STEALTH_OFF"] = "<b>Tryb cichy wyłączony.</b><br>Klient zobaczy twoje wiadomości.";
$MESS["IM_OL_CHAT_STEALTH_ON"] = "<b>Tryb cichy włączony.</b><br>Klient NIE zobaczy twoich wiadomości.";
$MESS["IM_OL_CLIENT_NAME"] = "Klient";
$MESS["IM_OL_CLOSE_VOTE"] = "Niestety, nie możesz już ocenić tej rozmowy (minęła następująca liczba dni na przesłanie oceny: #DAYS#)";
$MESS["IM_OL_CLOSE_VOTE_NO_DAY"] = "Niestety nie możesz już ocenić tej rozmowy";
$MESS["IM_OL_COMMENT_HEAD"] = "Komentarz przełożonego";
$MESS["IM_OL_COMMENT_HEAD_ADD"] = "dodaj";
$MESS["IM_OL_COMMENT_HEAD_BUTTON_SAVE"] = "Zapisz";
$MESS["IM_OL_COMMENT_HEAD_BUTTON_VOTE"] = "Oceń";
$MESS["IM_OL_COMMENT_HEAD_TEXT"] = "Treść komentarza...";
$MESS["IM_OL_DIALOG_NUMBER"] = "Rozmowa nr #NUMBER#";
$MESS["IM_OL_FORMS_SOON"] = "Formularze CRM będą dostępne w nadchodzących aktualizacjach Bitrix24.";
$MESS["IM_OL_GUEST_NAME"] = "Gość";
$MESS["IM_OL_INVITE_ANSWER"] = "Odpowiedz";
$MESS["IM_OL_INVITE_CLOSE"] = "Anuluj";
$MESS["IM_OL_INVITE_JOIN"] = "Start";
$MESS["IM_OL_INVITE_JOIN_2"] = "Dołącz";
$MESS["IM_OL_INVITE_SKIP"] = "Pomiń";
$MESS["IM_OL_INVITE_TEXT"] = "Otrzymano wiadomość z nowego kanału";
$MESS["IM_OL_INVITE_TEXT_JOIN"] = "Dołącz do bieżącej rozmowy";
$MESS["IM_OL_INVITE_TEXT_OPEN"] = "Rozpocznij nową sesję Otwartego Kanału";
$MESS["IM_OL_INVITE_TRANSFER"] = "Przenieś";
$MESS["IM_OL_LIST_ANSWERED"] = "Obsłużone";
$MESS["IM_OL_LIST_NEW"] = "Oczekiwanie na pierwszą odpowiedź";
$MESS["IM_OL_LIST_UNANSWERED"] = "Nieobsłużone";
$MESS["IM_OL_VOTE"] = "Ocena";
$MESS["IM_OL_VOTE_DISLIKE"] = "Złe";
$MESS["IM_OL_VOTE_END"] = "Możesz ocenić jakoś obsługi 24 godziny po rozmowie.";
$MESS["IM_OL_VOTE_HEAD"] = "Ocena przełożonego";
$MESS["IM_OL_VOTE_LIKE"] = "Dobre";
$MESS["IM_OL_VOTE_USER"] = "Ocena użytkownika";
$MESS["IM_OL_VOTE_WO"] = "nie oceniono";
$MESS["IM_O_INVITE_JOIN"] = "Dołącz";
$MESS["IM_O_INVITE_TEXT_NEW"] = "Dołącz do kolegów na czacie publicznym";
$MESS["IM_O_INVITE_TEXT_SITE_NEW"] = "Dołącz do innych użytkowników na czacie publicznym";
$MESS["IM_PHONE_401"] = "Błąd uwierzytelnienia użytkownika.";
$MESS["IM_PHONE_403"] = "Połączenie nie może być wykonane.";
$MESS["IM_PHONE_ACTIONS"] = "Wybrane działanie";
$MESS["IM_PHONE_ACTION_CRM_COMMENT"] = "Skomentuj";
$MESS["IM_PHONE_ACTION_CRM_DEAL"] = "Deal";
$MESS["IM_PHONE_ACTION_CRM_INVOICE"] = "Faktura";
$MESS["IM_PHONE_ACTION_C_DEAL"] = "Utwórz deal";
$MESS["IM_PHONE_ACTION_C_INVOICE"] = "Utwórz ofertę";
$MESS["IM_PHONE_ACTION_C_MENU"] = "Utwórz w oparciu o deal";
$MESS["IM_PHONE_ACTION_T_COMPANY"] = "Otwórz firmę";
$MESS["IM_PHONE_ACTION_T_CONTACT"] = "Otwórz kontakt";
$MESS["IM_PHONE_ACTION_T_DEAL"] = "Otwórz deal";
$MESS["IM_PHONE_ACTION_T_LEAD"] = "Otwórz lead";
$MESS["IM_PHONE_BTN_ANSWER"] = "Odpowiedz";
$MESS["IM_PHONE_BTN_BUSY"] = "Zignoruj";
$MESS["IM_PHONE_CALLBACK_TO"] = "Oddzwoń #PHONE#";
$MESS["IM_PHONE_CALL_TO_PHONE"] = "z #PHONE# otrzymane";
$MESS["IM_PHONE_CALL_TRANSFER"] = "Połączenie od #PHONE# (przekierowane)";
$MESS["IM_PHONE_CALL_TRANSFERED"] = "(przekierowano)";
$MESS["IM_PHONE_CALL_VOICE_FROM"] = "Połączenie od #PHONE#";
$MESS["IM_PHONE_CALL_VOICE_TO"] = "Dzwoni #PHONE#";
$MESS["IM_PHONE_DECLINE"] = "Rozmówca odrzucił połączenie.";
$MESS["IM_PHONE_END"] = "Połączenie zakończone";
$MESS["IM_PHONE_ERROR"] = "Błąd połączenia";
$MESS["IM_PHONE_ERROR_BUSY"] = "Osoba wywoływana obecnie prowadzi rozmowę.";
$MESS["IM_PHONE_ERROR_BUSY_PHONE"] = "Telefon jest obecnie zajęty.";
$MESS["IM_PHONE_ERROR_CONNECT"] = "Błąd połączenia z serwerem telefonii";
$MESS["IM_PHONE_ERR_BLOCK_RENT"] = "Numer telefonu, na który usiłujesz się połączyć został zablokowany przez dostawcę.";
$MESS["IM_PHONE_ERR_LICENSE"] = "Błąd licencji. Proszę o kontakt ze wsparciem technicznym.";
$MESS["IM_PHONE_ERR_NEED_RENT"] = "Musisz wynająć numer telefoniczny aby wykonywać połączenia.";
$MESS["IM_PHONE_ERR_SIP_LICENSE"] = "Aby wykonać połączenie z tego numeru, potrzebujesz licencji dla SIP Connector.";
$MESS["IM_PHONE_GRADE"] = "Jakość połączenia:";
$MESS["IM_PHONE_GRADE_1"] = "Brak";
$MESS["IM_PHONE_GRADE_2"] = "Niskie";
$MESS["IM_PHONE_GRADE_3"] = "Dobre";
$MESS["IM_PHONE_GRADE_4"] = "Doskonałe";
$MESS["IM_PHONE_HIDDEN_NUMBER"] = "Numer jest ukryty";
$MESS["IM_PHONE_INCOMPLETED"] = "Ta trasa jest niedostępna.";
$MESS["IM_PHONE_INVITE"] = "Rozmowa zawieszona";
$MESS["IM_PHONE_INVITE_CALLBACK"] = "Odpowiedz na zaproszenie";
$MESS["IM_PHONE_LEAD_SAVED"] = "Lead został zapisany. Możesz wrócić do niego później.";
$MESS["IM_PHONE_NO_ANSWER"] = "Użytkownik nie odpowiedział.";
$MESS["IM_PHONE_NO_EMERGENCY"] = "Telefon Bitrix24 nie może być użyty do połączeń alarmowych.";
$MESS["IM_PHONE_NO_MONEY"] = "Twoje środki są niewystarczające aby wykonywać połączenia.";
$MESS["IM_PHONE_OUTGOING"] = "rozmowa przychodząca";
$MESS["IM_PHONE_PAY_URL_NEW"] = "Możesz zasilić swoje środki w \"Ustawieniach Telefonu\"";
$MESS["IM_PHONE_REC_DONE"] = "Nagranie połączenie zostało zapisane";
$MESS["IM_PHONE_REC_NOW"] = "Połączenie jest nagrywane";
$MESS["IM_PHONE_REC_OFF"] = "Połączenie nie będzie nagrywane. Możesz włączyć nagrywanie w ustawieniach telefonu.";
$MESS["IM_PHONE_REC_ON"] = "Połączenie będzie nagrywane";
$MESS["IM_PHONE_UNAVAILABLE"] = "Kontakt jest czasowo niedostępny.";
$MESS["IM_PHONE_WAIT_ANSWER"] = "Oczekiwanie na odpowiedź";
$MESS["IM_PHONE_WRONG_NUMBER"] = "Numer telefonu jest nieprawidłowy.";
$MESS["IM_PHONE_WRONG_NUMBER_DESC"] = "Numer telefonu musi być w formacie międzynarodowym.<br/> Przykład: +44 20 1234 5678";
$MESS["IM_P_HOLD"] = "Wstrzymaj";
$MESS["IM_P_MENU"] = "Otwórz menu";
$MESS["IM_P_TRANSFER"] = "Przenieś";
$MESS["IM_RECENT_CALLS"] = "Aktywne połączenia";
$MESS["IM_RECENT_PINNED"] = "Przypięte";
$MESS["IM_RESENT_NEVER"] = "nigdy";
$MESS["IM_R_COMMENT_PLURAL_0"] = "komentarz";
$MESS["IM_R_COMMENT_PLURAL_1"] = "komentarze";
$MESS["IM_R_COMMENT_PLURAL_2"] = "komentarzy";
$MESS["IM_R_COMMENT_ZERO"] = "Brak komentarzy";
$MESS["IM_R_DIALOG_TITLE"] = "Komentarze";
$MESS["IM_R_LOAD_COMMENT"] = "Ładowanie komentarzy...";
$MESS["IM_R_LOAD_ERROR"] = "Nie udało się załadować Komentarzy. Sprawdź połączenie sieciowe.";
$MESS["IM_R_NO_COMMENT"] = "Brak komentarzy";
$MESS["IM_SAVE_TO_QUICK_ANSWERS_ERROR"] = "Wystąpił błąd podczas dodawania wiadomości do gotowych odpowiedzi";
$MESS["IM_SAVE_TO_QUICK_ANSWERS_SUCCESS"] = "Ta wiadomość została dodana do gotowych odpowiedzi";
$MESS["IM_SEARCH_B24"] = "Przeszukaj Bitrix24.Network";
$MESS["IM_SEARCH_SITE"] = "Przeszukaj stronę internetową";
$MESS["IM_SHARE_POST_ERROR"] = "Nie można opublikować wiadomości w Aktualnościach. Sprawdź swoje uprawnienia dostępu.";
$MESS["IM_SPACE_HOTKEY"] = "Spacja (przytrzymaj)";
$MESS["IM_STATUS_AWAY"] = "Poza zasięgiem";
$MESS["IM_STATUS_AWAY_TITLE"] = "Niedostępny przez #TIME#";
$MESS["IM_STATUS_BIRTHDAY"] = "Urodziny";
$MESS["IM_STATUS_BOT"] = "Czat bot";
$MESS["IM_STATUS_BREAK"] = "Przerwa";
$MESS["IM_STATUS_CALL"] = "Łączenie";
$MESS["IM_STATUS_DND"] = "Nie przeszkadzać";
$MESS["IM_STATUS_GUEST"] = "niedostępne";
$MESS["IM_STATUS_IDLE"] = "Poza zasięgiem";
$MESS["IM_STATUS_LINES-ONLINE"] = "Użytkownik Otwartego Kanału";
$MESS["IM_STATUS_MOBILE"] = "Mobile App";
$MESS["IM_STATUS_NA"] = "niedostępne";
$MESS["IM_STATUS_NETWORK"] = "Bitrix24.Network";
$MESS["IM_STATUS_OFFLINE"] = "Nieaktywny";
$MESS["IM_STATUS_ONLINE"] = "Online";
$MESS["IM_STATUS_VACATION"] = "Na urlopie";
$MESS["IM_STATUS_VACATION_TITLE"] = "Na urlop do #DATE#";
$MESS["IM_STATUS_VIDEO"] = "Trwa połączenie";
$MESS["IM_TRANSFER_SCOPE_DEPARTMENTS"] = "Działy";
$MESS["IM_TRANSFER_SCOPE_LINES"] = "Otwarte kanały";
$MESS["IM_TRANSFER_SCOPE_USERS"] = "Pracownicy";
$MESS["IM_UNKNOWN_ERROR"] = "Coś poszło nie tak. Prosimy spróbować ponownie.";
$MESS["IM_VIDEOCONF_MENU_CHANGE_LINK"] = "Zmień link";
$MESS["IM_VIDEOCONF_MENU_CONFIRM_CHANGE"] = "Zmień link";
$MESS["IM_VIDEOCONF_MENU_CONFIRM_CHANGE_CANCEL"] = "Anuluj";
$MESS["IM_VIDEOCONF_MENU_CONFIRM_CHANGE_TEXT"] = "Wszyscy użytkownicy-goście zostaną usunięci z czatu. Czy na pewno tego chcesz?";
$MESS["IM_VIDEOCONF_MENU_COPY"] = "Kopiuj link";
$MESS["IM_VIDEOCONF_MENU_COPY_DONE"] = "Link wideokonferencji skopiowano do schowka.";
$MESS["IM_YOU"] = "to Ty";
