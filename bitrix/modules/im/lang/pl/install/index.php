<?
$MESS["IM_CONVERT_MESSAGE"] = "Zainstalowałeś moduł \"Instant Messenger\". Aby uzyskać dostęp do historii wiadomości, należy #A_TAG_START#skonwertować dane historii#A_TAG_END#.";
$MESS["IM_INSTALL_TITLE"] = "Instalacja Modułu Komunikator";
$MESS["IM_MODULE_DESCRIPTION"] = "Umożliwia obsługę komunikatora i usługi powiadomień.";
$MESS["IM_MODULE_NAME"] = "Komunikator";
$MESS["IM_UNINSTALL_TITLE"] = "Deinstalacja Modułu Komunikator";
?>