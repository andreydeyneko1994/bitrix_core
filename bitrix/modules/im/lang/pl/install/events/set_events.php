<?
$MESS["IM_NEW_MESSAGE_GROUP_NAME"] = "Nowa wiadomość (grupa)";
$MESS["IM_NEW_MESSAGE_GROUP_SUBJECT"] = "#SITE_NAME#: Błyskawiczne wiadomości od #FROM_USERS#";
$MESS["IM_NEW_MESSAGE_NAME"] = "Nowa wiadomość";
$MESS["IM_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Błyskawiczne wiadomości od #FROM_USER#";
$MESS["IM_NEW_NOTIFY_GROUP_NAME"] = "Nowe powiadomienie (grupa)";
$MESS["IM_NEW_NOTIFY_GROUP_SUBJECT"] = "#SITE_NAME#: Powiadomienie \"#MESSAGE_50#\"";
$MESS["IM_NEW_NOTIFY_NAME"] = "Nowe powiadomienie";
$MESS["IM_NEW_NOTIFY_SUBJECT"] = "#SITE_NAME#: Powiadomienie \"#MESSAGE_50#\"";
?>