<?php
$MESS["IM_CL_GROUP_ALL"] = "Wszystkie Kontakty";
$MESS["IM_CL_GROUP_CHATS"] = "Czat grupowy";
$MESS["IM_CL_GROUP_EXTRANET"] = "Extranet";
$MESS["IM_CL_GROUP_FRIENDS"] = "Znajomi";
$MESS["IM_CL_GROUP_LAST"] = "Ostatnie kontakty";
$MESS["IM_CL_GROUP_OTHER"] = "Kontakty zewnętrzne";
$MESS["IM_CL_GROUP_OTHER_2"] = "Inne kontakty";
$MESS["IM_CL_GROUP_SEARCH"] = "Szukaj wyników";
$MESS["IM_CL_GROUP_SG"] = "Ekstranet:";
$MESS["IM_CL_SEARCH_EMPTY"] = "Nie podano żadnej frazy";
$MESS["IM_FILE"] = "Plik";
$MESS["IM_QUOTE"] = "Cytat";
$MESS["IM_SEARCH_OL"] = "Open Channel";
