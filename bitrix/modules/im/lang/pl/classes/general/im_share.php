<?php
$MESS["IM_SHARE_CHAT"] = "Czat";
$MESS["IM_SHARE_CHAT_CALEND"] = "Na podstawie wydarzenia utworzono [URL=#LINK#]wydarzenie kalendarzowe[/URL].";
$MESS["IM_SHARE_CHAT_CHAT"] = "Czat został utworzony na podstawie wiadomości.";
$MESS["IM_SHARE_CHAT_CHAT_JOIN"] = "Dołącz";
$MESS["IM_SHARE_CHAT_CHAT_WELCOME"] = "Ten czat utworzono na podstawie wiadomości z czatu #CHAT#.";
$MESS["IM_SHARE_CHAT_POST_2"] = "Na podstawie tej wiadomości utworzono [URL=#LINK#]Post w Aktualnościach[/URL].";
$MESS["IM_SHARE_CHAT_TASK"] = "Na podstawie wiadomości utworzono [URL=#LINK#]zadanie[/URL].";
$MESS["IM_SHARE_FILE"] = "Plik";
$MESS["IM_SHARE_POST_WELCOME"] = "Podyskutujmy :)";
