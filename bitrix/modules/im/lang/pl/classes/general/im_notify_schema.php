<?php
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Powiadomienia z procesów biznesowych";
$MESS["IM_NS_CHAT_NEW"] = "Wiadomości czatu prywatnego";
$MESS["IM_NS_DEFAULT"] = "Niezorganizowane powiadomienia";
$MESS["IM_NS_IM"] = "Komunikator";
$MESS["IM_NS_LIKE"] = "Polubienia w czatach";
$MESS["IM_NS_MAIN"] = "Oceny i Polubienia";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Powiadomienie o \"Lubię to\"";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Powiadomienie o głosowaniu w Twoim poście";
$MESS["IM_NS_MENTION"] = "Zostałeś wspomniany w publicznym czacie";
$MESS["IM_NS_MENTION_2"] = "Ktoś wspomniał o Tobie w wiadomości na forum";
$MESS["IM_NS_MESSAGE_NEW"] = "Wiadomości czatu osobowego";
$MESS["IM_NS_OPEN_NEW"] = "Wiadomości czatu publicznego";
