<?
$MESS["IM_HISTORY_ERROR_MESSAGE_ID"] = "Nie podano żadnej frazy";
$MESS["IM_HISTORY_ERROR_TO_USER_ID"] = "Brak identyfikatora użytkownika.";
$MESS["IM_HISTORY_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_HISTORY_FORMAT_TIME"] = "g:i a";
$MESS["IM_HISTORY_SEARCH_DATE_EMPTY"] = "Nie określono daty, według której mają być wyszukiwane wiadomości.";
$MESS["IM_HISTORY_SEARCH_EMPTY"] = "Nie podano żadnej frazy";
?>