<?
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "Nadawca wiadomości nie jest określony.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "Tekst wiadomości jest pusty.";
$MESS["IM_ERROR_EMPTY_TO_USER_ID"] = "Adresat wiadomości nie jest określony.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "Brak określonego identyfikatora użytkownika.";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g:i a";
?>