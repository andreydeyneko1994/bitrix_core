<?php
$MESS["BOT_DEFAULT_WORK_POSITION"] = "Czat bot";
$MESS["BOT_MESSAGE_FROM"] = "Czat bot #BOT_NAME#";
$MESS["BOT_MESSAGE_INSTALL_SYSTEM"] = "Czat bot został dodany do twojego konta Bitrix24";
$MESS["BOT_MESSAGE_INSTALL_USER"] = "#USER_NAME# dodał nowego bota czatu";
$MESS["BOT_MESSAGE_INSTALL_USER_F"] = "#USER_NAME# dodał nowego bota czatu";
$MESS["BOT_SUPERVISOR_NOTICE_ALL_MESSAGES"] = "Czat bot #BOT_NAME# ma dostęp do wszystkich wiadomości na czacie.";
$MESS["BOT_SUPERVISOR_NOTICE_NEW_MESSAGES"] = "Czat bot #BOT_NAME# ma dostęp do wszystkich nowych wiadomości na czacie.";
