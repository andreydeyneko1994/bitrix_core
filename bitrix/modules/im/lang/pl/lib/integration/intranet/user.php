<?
$MESS["IM_INT_USR_INVITE_USERS"] = "#USER_NAME# wysłał zaproszenie #USERS#.";
$MESS["IM_INT_USR_INVITE_USERS_F"] = "#USER_NAME# wysłał zaproszenie #USERS#.";
$MESS["IM_INT_USR_JOIN_2"] = "Dołączyłem do zespołu.";
$MESS["IM_INT_USR_JOIN_GENERAL_2"] = "Dołączyłem do zespołu.";
$MESS["IM_INT_USR_LINK_COPIED"] = "#USER_NAME# skopiował link, aby zaprosić nowych pracowników.";
$MESS["IM_INT_USR_LINK_COPIED_F"] = "#USER_NAME# skopiował link, aby zaprosić nowych pracowników.";
$MESS["IM_INT_USR_REGISTER_USERS"] = "#USER_NAME# zarejestrował #USERS#.";
$MESS["IM_INT_USR_REGISTER_USERS_F"] = "#USER_NAME# zarejestrował #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_F"] = "#USER_NAME# cofnął uprawnienia administratora #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_M"] = "#USER_NAME# cofnął uprawnienia administratora #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_F"] = "#USER_NAME# przyznał uprawnienia administratora #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_M"] = "#USER_NAME# przyznał uprawnienia administratora #USERS#.";
?>