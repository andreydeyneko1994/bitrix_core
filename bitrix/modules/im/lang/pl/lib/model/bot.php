<?
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "REST app ID";
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "Bot ID";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Typ Bota";
$MESS["BOT_ENTITY_COUNT_CHAT_FIELD"] = "Licznik zaproszonych do czatu";
$MESS["BOT_ENTITY_COUNT_MESSAGE_FIELD"] = "Licznik wiadomości przetworzonych";
$MESS["BOT_ENTITY_COUNT_SLASH_FIELD"] = "Licznik komend przetworzonych";
$MESS["BOT_ENTITY_COUNT_USER_FIELD"] = "Licznik zaproszonych do czatu publicznego";
$MESS["BOT_ENTITY_LANGUAGE_FIELD"] = "Język Bota";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Metoda obsługi usuwania Bota";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Metoda obsługi wiadomości nadchodzących";
$MESS["BOT_ENTITY_METHOD_MESSAGE_DELETE_FIELD"] = "Program do usuwania wiadomości (metoda klasy)";
$MESS["BOT_ENTITY_METHOD_MESSAGE_UPDATE_FIELD"] = "Program do aktualizacji wiadomości (metoda klasy)";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Dodaj metodę obsługi wiadomości powitalnych";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID modułu";
$MESS["BOT_ENTITY_OPENLINE_FIELD"] = "flaga obsługi Otwartego Kanału";
$MESS["BOT_ENTITY_TEXT_CHAT_WELCOME_MESSAGE_FIELD"] = "Metoda dla dodawania wiadomości powitalnej (czat grupowy)";
$MESS["BOT_ENTITY_TEXT_PRIVATE_WELCOME_MESSAGE_FIELD"] = "Tekst wiadomości powitalnej";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Klasa obsługi Bota";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Metoda obsługi Bota";
$MESS["BOT_ENTITY_VERIFIED_FIELD"] = "Flaga identyfikująca";
?>