<?
$MESS["STATUS_ENTITY_COLOR_FIELD"] = "Określone przez użytkownika";
$MESS["STATUS_ENTITY_DESKTOP_LAST_DATE_FIELD"] = "Ostatnie działanie zarejestrowane na (pulpit)";
$MESS["STATUS_ENTITY_EVENT_ID_FIELD"] = "ID wydarzenia";
$MESS["STATUS_ENTITY_EVENT_UNTIL_DATE_FIELD"] = "Wydarzenie ważne do";
$MESS["STATUS_ENTITY_IDLE_FIELD"] = "Okres bezczynności";
$MESS["STATUS_ENTITY_MOBILE_LAST_DATE_FIELD"] = "Ostatnie działanie zarejestrowane na (aplikacja mobilna)";
$MESS["STATUS_ENTITY_STATUS_FIELD"] = "Status";
$MESS["STATUS_ENTITY_USER_ID_FIELD"] = "ID użytkownika";
?>