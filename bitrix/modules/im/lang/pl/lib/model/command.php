<?php
$MESS["COMMAND_ENTITY_APP_ID_FIELD"] = "REST app ID";
$MESS["COMMAND_ENTITY_BOT_ID_FIELD"] = "Czat bot ID";
$MESS["COMMAND_ENTITY_CLASS_FIELD"] = "Obsługa komend";
$MESS["COMMAND_ENTITY_COMMAND_FIELD"] = "Komenda";
$MESS["COMMAND_ENTITY_COMMON_FIELD"] = "Ogólny status komendy";
$MESS["COMMAND_ENTITY_HIDDEN_FIELD"] = "Ukryty status komendy";
$MESS["COMMAND_ENTITY_ID_FIELD"] = "ID komendy";
$MESS["COMMAND_ENTITY_METHOD_COMMAND_ADD_FIELD"] = "Metoda obsługi dla komend przychodzących";
$MESS["COMMAND_ENTITY_METHOD_LANG_GET_FIELD"] = "Metod dla otrzymania lokalizacji";
$MESS["COMMAND_ENTITY_MODULE_ID_FIELD"] = "ID modułu";
$MESS["COMMAND_ENTITY_SONET_SUPPORT_FIELD"] = "Status obsługi na Aktualności";
