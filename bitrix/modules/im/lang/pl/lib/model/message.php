<?
$MESS["MESSAGE_ENTITY_AUTHOR_ID_FIELD"] = "Utworzone przez";
$MESS["MESSAGE_ENTITY_CHAT_ID_FIELD"] = "Czat ID";
$MESS["MESSAGE_ENTITY_DATE_CREATE_FIELD"] = "Utworzone";
$MESS["MESSAGE_ENTITY_EMAIL_TEMPLATE_FIELD"] = "Szablon email";
$MESS["MESSAGE_ENTITY_ID_FIELD"] = "Unikalne ID";
$MESS["MESSAGE_ENTITY_IMPORT_ID_FIELD"] = "Zewnętrzne ID";
$MESS["MESSAGE_ENTITY_MESSAGE_FIELD"] = "Wiadomość";
$MESS["MESSAGE_ENTITY_MESSAGE_OUT_FIELD"] = "Wiadomość typu Rich";
$MESS["MESSAGE_ENTITY_NOTIFY_BUTTONS_FIELD"] = "Przyciski";
$MESS["MESSAGE_ENTITY_NOTIFY_EVENT_FIELD"] = "Pole wydarzenia";
$MESS["MESSAGE_ENTITY_NOTIFY_MODULE_FIELD"] = "Moduł powiadomień";
$MESS["MESSAGE_ENTITY_NOTIFY_READ_FIELD"] = "Odczyt";
$MESS["MESSAGE_ENTITY_NOTIFY_SUB_TAG_FIELD"] = "Dodatkowa flaga powiadomienia";
$MESS["MESSAGE_ENTITY_NOTIFY_TAG_FIELD"] = "Tag powiadomienia";
$MESS["MESSAGE_ENTITY_NOTIFY_TITLE_FIELD"] = "Tytuł";
$MESS["MESSAGE_ENTITY_NOTIFY_TYPE_FIELD"] = "Typ powiadomienia";
?>