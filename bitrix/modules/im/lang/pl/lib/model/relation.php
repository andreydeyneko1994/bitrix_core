<?php
$MESS["RELATION_ENTITY_CALL_STATUS_FIELD"] = "Status połączenia";
$MESS["RELATION_ENTITY_CHAT_ID_FIELD"] = "Czat ID";
$MESS["RELATION_ENTITY_ID_FIELD"] = "Unikalne ID";
$MESS["RELATION_ENTITY_LAST_FILE_ID_FIELD"] = "ID ostatniego pliku";
$MESS["RELATION_ENTITY_LAST_ID_FIELD"] = "ID ostatniej wiadomości";
$MESS["RELATION_ENTITY_LAST_READ_FIELD"] = "ID ostatniej odczytanej wiadomości";
$MESS["RELATION_ENTITY_LAST_SEND_ID_FIELD"] = "ID ostatniej wysłanej wiadomości";
$MESS["RELATION_ENTITY_MESSAGE_TYPE_FIELD"] = "Typ czatu";
$MESS["RELATION_ENTITY_NOTIFY_BLOCK_FIELD"] = "Powiadomienie";
$MESS["RELATION_ENTITY_START_ID_FIELD"] = "ID pierwszej wiadomości";
$MESS["RELATION_ENTITY_STATUS_FIELD"] = "Status";
$MESS["RELATION_ENTITY_UNREAD_ID_FIELD"] = "Identyfikator pierwszej nieprzeczytanej wiadomości";
$MESS["RELATION_ENTITY_USER_ID_FIELD"] = "ID użytkownika";
