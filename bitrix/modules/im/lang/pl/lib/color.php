<?
$MESS["IM_COLOR_AQUA"] = "Morski";
$MESS["IM_COLOR_AZURE"] = "Lazurowy";
$MESS["IM_COLOR_BROWN"] = "Brązowy";
$MESS["IM_COLOR_DARK_BLUE"] = "Niebieski";
$MESS["IM_COLOR_GRAPHITE"] = "Grafitowy";
$MESS["IM_COLOR_GRAY"] = "Szary";
$MESS["IM_COLOR_GREEN"] = "Zielony";
$MESS["IM_COLOR_KHAKI"] = "Khaki";
$MESS["IM_COLOR_LIGHT_BLUE"] = "Błękitny";
$MESS["IM_COLOR_LIME"] = "Limonkowy";
$MESS["IM_COLOR_MARENGO"] = "Marengo";
$MESS["IM_COLOR_MINT"] = "Miętowy";
$MESS["IM_COLOR_ORANGE"] = "Pomarańczowy";
$MESS["IM_COLOR_PINK"] = "Różowy";
$MESS["IM_COLOR_PURPLE"] = "Fioletowy";
$MESS["IM_COLOR_RED"] = "Czerwony";
$MESS["IM_COLOR_SAND"] = "Piaskowy";
?>