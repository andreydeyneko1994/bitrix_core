<?php
$MESS["IM_CALL_CONFERENCE_ERROR_ADDING_USERS"] = "Błąd podczas dodawania użytkowników do czatu";
$MESS["IM_CALL_CONFERENCE_ERROR_CREATING"] = "Błąd podczas tworzenia wideokonferencji";
$MESS["IM_CALL_CONFERENCE_ERROR_DELETING_USERS"] = "Błąd podczas usuwania użytkowników z czatu";
$MESS["IM_CALL_CONFERENCE_ERROR_ID_NOT_PROVIDED"] = "Nieprawidłowe ID wideokonferencji";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_LENGTH"] = "Zaproszenie może zawierać do 255 znaków";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_TYPE"] = "Nieprawidłowe zaproszenie";
$MESS["IM_CALL_CONFERENCE_ERROR_MAX_USERS"] = "Przekroczono maksymalną liczbę uczestników";
$MESS["IM_CALL_CONFERENCE_ERROR_NO_PRESENTERS"] = "Lista mówców nie może być pusta";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_LENGTH_NEW"] = "Hasło konferencji musi składać się z co najmniej trzech znaków";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_TYPE"] = "Nieprawidłowe hasło do wideokonferencji";
$MESS["IM_CALL_CONFERENCE_ERROR_RENAMING_CHAT"] = "Błąd podczas zmiany nazwy czatu";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_LENGTH"] = "Nazwa wideokonferencji musi mieć co najmniej dwa znaki";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_REQUIRED"] = "Nie określono nazwy wideokonferencji";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_TYPE"] = "Nieprawidłowa nazwa wideokonferencji";
$MESS["IM_CALL_CONFERENCE_ERROR_TOO_MANY_PRESENTERS"] = "Wybrano zbyt wielu mówców";
