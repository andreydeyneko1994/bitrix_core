<?php
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_FINISHED"] = "Połączenie zakończone";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_MISSED_F"] = "Nieodebrane połączenie od #NAME#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_MISSED_M"] = "Nieodebrane połączenie od #NAME#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_STARTED"] = "Rozpoczęto połączenie ##ID#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_STARTED_F"] = "Inicjator połączenia: #NAME#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_STARTED_M"] = "Inicjator połączenia: #NAME#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_USER_BUSY_F"] = "#NAME# jest w trakcie innego połączenia";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_USER_BUSY_M"] = "#NAME# jest w trakcie innego połączenia";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_USER_DECLINED_F"] = "Odrzucił połączenie: #NAME#";
$MESS["IM_CALL_INTEGRATION_CHAT_CALL_USER_DECLINED_M"] = "Odrzucił połączenie: #NAME#";
