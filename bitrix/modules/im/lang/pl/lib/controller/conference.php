<?php
$MESS["IM_CONFERENCE_EDIT_CREATION_ERROR"] = "Błąd podczas tworzenia konferencji";
$MESS["IM_CONFERENCE_EDIT_ERROR_CANT_EDIT"] = "Nie możesz edytować wideokonferencji";
$MESS["IM_CONFERENCE_EDIT_ERROR_WRONG_ALIAS"] = "Błąd podczas tworzenia wideokonferencji";
$MESS["IM_CONFERENCE_EDIT_ERROR_WRONG_ID"] = "Nieprawidłowe ID wideokonferencji";
