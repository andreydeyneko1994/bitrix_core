<?php
$MESS["IM_MESSAGE_ATTACH"] = "Załącznik";
$MESS["IM_MESSAGE_DELETE"] = "Ta wiadomość została usunięta.";
$MESS["IM_MESSAGE_EMOJI"] = "emotikona";
$MESS["IM_MESSAGE_FILE"] = "Plik";
$MESS["IM_MESSAGE_ICON"] = "ikonka";
$MESS["IM_MESSAGE_RATING"] = "Ocena";
$MESS["IM_MESSAGE_SMILE"] = "uśmieszek";
$MESS["IM_QUOTE"] = "Cytat";
