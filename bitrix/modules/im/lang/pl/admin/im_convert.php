<?
$MESS["IM_CONVERT_ABOUT_TIME"] = "w przybliżeniu";
$MESS["IM_CONVERT_ABOUT_TIME_MINUTE"] = "minut(y)";
$MESS["IM_CONVERT_ABOUT_TIME_SEC"] = "sekundy";
$MESS["IM_CONVERT_BUTTON"] = "Konwertuj";
$MESS["IM_CONVERT_COMPLETE"] = "Konwersja została zakończona.";
$MESS["IM_CONVERT_COMPLETE_ALL_OK"] = "Wszystkie dane zostały przekonwertowane pomyślnie, brak wymaganych dodatkowych konwersji.";
$MESS["IM_CONVERT_IN_PROGRESS"] = "Konwertuję...";
$MESS["IM_CONVERT_NEXT_STEP"] = "Następny krok";
$MESS["IM_CONVERT_STEP"] = "Czas etapu maksymalnej konwersacji:";
$MESS["IM_CONVERT_STEP_sec"] = "sek.";
$MESS["IM_CONVERT_STOP"] = "zatrzymaj";
$MESS["IM_CONVERT_TAB"] = "Konwersacja";
$MESS["IM_CONVERT_TAB_TITLE"] = "Parametry konwersji";
$MESS["IM_CONVERT_TITLE"] = "Konwertuj historię wiadomości";
$MESS["IM_CONVERT_TOTAL"] = "Wiadomości skonwertowane:";
?>