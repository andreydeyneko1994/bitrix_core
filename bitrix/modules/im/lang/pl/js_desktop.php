<?php
$MESS["BXD_CALL_BG_TITLE"] = "Wybierz tło";
$MESS["BXD_CONFIRM_CLOSE"] = "Zamknij";
$MESS["BXD_DEFAULT_TITLE"] = "Aplikacja Bitrix24 (wersja #VERSION#)";
$MESS["BXD_LOGOUT"] = "Zmień użytkownika";
$MESS["BXD_NEED_UPDATE"] = "Wersja twojej aplikacji jest nieaktualna. Musisz zainstalować nową wersję, aby używac aplikacji.";
$MESS["BXD_NEED_UPDATE_BTN"] = "Aktualizacja";
$MESS["BXD_QUOTE_BLOCK"] = "Cytat";
$MESS["BXD_RECONNECT"] = "Połącz ponownie";
