<?
$MESS["IM_INT_USR_INVITE_USERS"] = "#USER_NAME# enviou convite para #USERS#.";
$MESS["IM_INT_USR_INVITE_USERS_F"] = "#USER_NAME# enviou convite para #USERS#.";
$MESS["IM_INT_USR_JOIN_2"] = "Eu me juntei à equipe.";
$MESS["IM_INT_USR_JOIN_GENERAL_2"] = "Eu me juntei à equipe.";
$MESS["IM_INT_USR_LINK_COPIED"] = "#USER_NAME# copiou um link para convidar novos funcionários.";
$MESS["IM_INT_USR_LINK_COPIED_F"] = "#USER_NAME# copiou um link para convidar novos funcionários.";
$MESS["IM_INT_USR_REGISTER_USERS"] = "#USER_NAME# registrou #USERS#.";
$MESS["IM_INT_USR_REGISTER_USERS_F"] = "#USER_NAME# registrou #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_F"] = "#USER_NAME# revogou permissões de administrador de #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_M"] = "#USER_NAME# revogou permissões de administrador de #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_F"] = "#USER_NAME# concedeu permissões de administrador para #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_M"] = "#USER_NAME# concedeu permissões de administrador para #USERS#.";
?>