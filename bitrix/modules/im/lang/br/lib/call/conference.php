<?php
$MESS["IM_CALL_CONFERENCE_ERROR_ADDING_USERS"] = "Erro ao adicionar usuários ao bate-papo";
$MESS["IM_CALL_CONFERENCE_ERROR_CREATING"] = "Erro ao criar videoconferência";
$MESS["IM_CALL_CONFERENCE_ERROR_DELETING_USERS"] = "Erro ao excluir usuários do bate-papo";
$MESS["IM_CALL_CONFERENCE_ERROR_ID_NOT_PROVIDED"] = "ID da videoconferência inválido";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_LENGTH"] = "O convite pode incluir até 255 caracteres";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_TYPE"] = "Convite incorreto";
$MESS["IM_CALL_CONFERENCE_ERROR_MAX_USERS"] = "Máximo de participantes excedido";
$MESS["IM_CALL_CONFERENCE_ERROR_NO_PRESENTERS"] = "A lista de palestrantes não pode estar vazia";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_LENGTH_NEW"] = "A senha da conferência deve incluir três ou mais caracteres";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_TYPE"] = "Senha incorreta da videoconferência";
$MESS["IM_CALL_CONFERENCE_ERROR_RENAMING_CHAT"] = "Erro ao renomear bate-papo";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_LENGTH"] = "O nome da videoconferência deve ter dois caracteres ou mais";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_REQUIRED"] = "O nome da videoconferência não foi especificado";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_TYPE"] = "Nome incorreto da videoconferência";
$MESS["IM_CALL_CONFERENCE_ERROR_TOO_MANY_PRESENTERS"] = "Muitos palestrantes selecionados";
