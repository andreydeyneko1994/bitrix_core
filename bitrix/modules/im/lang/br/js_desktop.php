<?php
$MESS["BXD_CALL_BG_TITLE"] = "Selecionar fundo";
$MESS["BXD_CONFIRM_CLOSE"] = "Fechar";
$MESS["BXD_DEFAULT_TITLE"] = "Bitrix24 Desktop Application (versão #VERSION#)";
$MESS["BXD_LOGOUT"] = "Trocar usuário";
$MESS["BXD_NEED_UPDATE"] = "A versão do seu aplicativo é muito antiga. Você deve instalar uma nova versão para utilizar o Bitrix24.";
$MESS["BXD_NEED_UPDATE_BTN"] = "Atualizar";
$MESS["BXD_QUOTE_BLOCK"] = "Citação";
$MESS["BXD_RECONNECT"] = "Reconectar";
