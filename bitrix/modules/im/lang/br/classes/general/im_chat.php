<?php
$MESS["IM_CHAT_CHANGE_COLOR_F"] = "#USER_NAME# mudou a cor do bate-papo para \"#CHAT_COLOR#\"";
$MESS["IM_CHAT_CHANGE_COLOR_M"] = "#USER_NAME# mudou a cor do bate-papo para \"#CHAT_COLOR#\"";
$MESS["IM_CHAT_CHANGE_TITLE"] = "O assunto do bate-papo \"#CHAT_TITLE#\" mudou";
$MESS["IM_CHAT_CHANGE_TITLE_F"] = "#USER_NAME# mudou o título de bate-papo \"#CHAT_TITLE#\".";
$MESS["IM_CHAT_CHANGE_TITLE_M"] = "#USER_NAME# mudou o título de bate-papo \"#CHAT_TITLE#\".";
$MESS["IM_CHAT_CREATE_OPEN_F_NEW"] = "#USER_NAME# criou um novo bate-papo público \"#CHAT_TITLE#\"";
$MESS["IM_CHAT_CREATE_OPEN_M_NEW"] = "#USER_NAME# criou um novo bate-papo público \"#CHAT_TITLE#\"";
$MESS["IM_CHAT_CREATE_OPEN_NEW"] = "O bate-papo público \"#CHAT_TITLE#\" foi criado";
$MESS["IM_CHAT_GENERAL_JOIN"] = "#USER_NAME# foi contratado";
$MESS["IM_CHAT_GENERAL_JOIN_F"] = "#USER_NAME# foi contratado";
$MESS["IM_CHAT_GENERAL_JOIN_PLURAL"] = "#USER_NAME# foi contratado";
$MESS["IM_CHAT_GENERAL_LEAVE_F"] = "#USER_NAME# foi demitido";
$MESS["IM_CHAT_GENERAL_LEAVE_M"] = "#USER_NAME# foi demitido";
$MESS["IM_CHAT_JOIN_F"] = "#USER_1_NAME# convidou #USER_2_NAME# para conversar.";
$MESS["IM_CHAT_JOIN_M"] = "#USER_1_NAME# convidou #USER_2_NAME# para conversar.";
$MESS["IM_CHAT_KICK_F"] = "#USER_1_NAME# ejetou #USER_2_NAME# de chat.";
$MESS["IM_CHAT_KICK_M"] = "#USER_1_NAME# ejetou #USER_2_NAME# de chat.";
$MESS["IM_CHAT_KICK_NOTIFICATION_F"] = "#USER_NAME# removeu você do bate-papo";
$MESS["IM_CHAT_KICK_NOTIFICATION_M"] = "#USER_NAME# removeu você do bate-papo";
$MESS["IM_CHAT_LEAVE_F"] = "#USER_NAME# deixou o chat.";
$MESS["IM_CHAT_LEAVE_M"] = "#USER_NAME# deixou o chat.";
$MESS["IM_CHAT_NAME_FORMAT"] = "#COLOR# Bate-papo ##NUMBER#";
$MESS["IM_CHAT_SELF_JOIN"] = "#USERS_NAME# entrou no bate-papo";
$MESS["IM_CHAT_SELF_JOIN_F"] = "#USER_NAME# entrou no bate-papo";
$MESS["IM_CHAT_SELF_JOIN_M"] = "#USER_NAME# entrou no bate-papo";
$MESS["IM_ERROR_ACCESS_JOIN"] = "Você não pode participar deste bate-papo.";
$MESS["IM_ERROR_AUTHORIZE_ERROR"] = "Apenas os membros de bate-papo podem convidar outros usuários.";
$MESS["IM_ERROR_CHAT_NOT_EXISTS"] = "O bate-papo especificado não existe.";
$MESS["IM_ERROR_EMPTY_CHAT_ID"] = "O ID do bate-papo está faltando.";
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "O remetente da mensagem não foi especificado.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "O texto da mensagem está vazio.";
$MESS["IM_ERROR_EMPTY_TO_CHAT_ID"] = "O destinatário da mensagem não foi especificado.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "O ID do usuário não foi especificado.";
$MESS["IM_ERROR_EMPTY_USER_ID_BY_PRIVACY"] = "Esses usuários negam terem sido convidados para o bate-papo";
$MESS["IM_ERROR_EMPTY_USER_OR_CHAT"] = "O bate-papo ou ID de usuário não foi especificado.";
$MESS["IM_ERROR_KICK"] = "Somente o proprietário bate-papo pode banir usuários.";
$MESS["IM_ERROR_LEAVE_OWNER_FORBIDDEN"] = "Não é possível dispensar o proprietário do bate-papo";
$MESS["IM_ERROR_MAX_USER"] = "O bate-papo não podem incluir mais de #COUNT# usuários.";
$MESS["IM_ERROR_MIN_USER"] = "Por favor seleccione os usuários antes de criar um novo bate-papo.";
$MESS["IM_ERROR_MIN_USER_BY_PRIVACY"] = "Não é possível iniciar bate-papo, porque os usuários negaram terem sido convidados";
$MESS["IM_ERROR_NOTHING_TO_ADD"] = "Todos os usuários selecionados já estão no chat.";
$MESS["IM_ERROR_USER_NOT_FOUND"] = "O usuário especificado não está no chat.";
$MESS["IM_ERROR_VIDEOCONF_MAX_USER"] = "A videoconferência não pode acomodar mais do que #COUNT# pessoas";
$MESS["IM_GENERAL_CREATE_BY_USER_NEW"] = "#USER_NAME# convida para um novo bate-papo público ";
$MESS["IM_GENERAL_CREATE_NEW"] = "O bate-papo público foi criado";
$MESS["IM_GENERAL_DESCRIPTION"] = "Um bate-papo geral está aberto a todos os seus colegas. Use este bate-papo para discutir temas que são de importância para qualquer pessoa na sua empresa.";
$MESS["IM_GENERAL_TITLE"] = "Bate-papo geral";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g: i a";
$MESS["IM_PATH_TITLE_CALENDAR_EVENT"] = "Ver Evento";
$MESS["IM_PATH_TITLE_SONET"] = "Visualizar grupo";
$MESS["IM_PATH_TITLE_TASKS"] = "Visualizar tarefa";
$MESS["IM_PERSONAL_DESCRIPTION"] = "Apenas você pode ver mensagens neste bate-papo.[br] Coloque, aqui, rascunhos de mensagens, notas, links e arquivos, para mantê-los à mão.";
$MESS["IM_VIDEOCONF_COPY_LINK"] = "Copiar link";
$MESS["IM_VIDEOCONF_JOIN_LINK"] = "Abrir videoconferência";
$MESS["IM_VIDEOCONF_LINK_TITLE"] = "Link público";
$MESS["IM_VIDEOCONF_NAME_FORMAT"] = "Videoconferência ##NUMBER# - #DATE_CREATE#";
$MESS["IM_VIDEOCONF_NAME_FORMAT_NEW"] = "Videoconferência ##NUMBER#";
$MESS["IM_VIDEOCONF_NEW_GUEST"] = "Novo convidado entrou na conferência \"#CHAT_TITLE#\".";
$MESS["IM_VIDEOCONF_SHARE_LINK"] = "Convide colegas e parceiros para a videoconferência!";
$MESS["IM_VIDEOCONF_SHARE_UPDATED_LINK"] = "#USER_NAME# alterou o link da videoconferência";
