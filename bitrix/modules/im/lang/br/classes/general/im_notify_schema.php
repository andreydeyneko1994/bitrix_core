<?php
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notificação sobre atividade em processos de negócio";
$MESS["IM_NS_CHAT_NEW"] = "Mensagens de bate-papos privados";
$MESS["IM_NS_DEFAULT"] = "Notificações indefinidas";
$MESS["IM_NS_IM"] = "Bate-papo e Chamadas";
$MESS["IM_NS_LIKE"] = "Curtidas do bate-papo";
$MESS["IM_NS_MAIN"] = "Avaliações e Curtidas";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notificação de \"curtir\"";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notificação sobre votos em posts que mencionam você";
$MESS["IM_NS_MENTION"] = "Você foi mencionado num bate-papo público";
$MESS["IM_NS_MENTION_2"] = "Alguém mencionou você no bate-papo";
$MESS["IM_NS_MESSAGE_NEW"] = "Mensagens de bate-papo pessoal";
$MESS["IM_NS_OPEN_NEW"] = "Mensagens de bate-papo público";
