<?php
$MESS["IM_CALL_CONFERENCE_ERROR_ADDING_USERS"] = "Erreur lors de l'ajout d'utilisateurs au chat";
$MESS["IM_CALL_CONFERENCE_ERROR_CREATING"] = "Erreur lors de la création de la vidéoconférence";
$MESS["IM_CALL_CONFERENCE_ERROR_DELETING_USERS"] = "Erreur lors de la suppression des utilisateurs du chat";
$MESS["IM_CALL_CONFERENCE_ERROR_ID_NOT_PROVIDED"] = "ID de la vidéoconférence invalide";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_LENGTH"] = "L'invitation peut comprendre jusqu'à 255 caractères";
$MESS["IM_CALL_CONFERENCE_ERROR_INVITATION_TYPE"] = "Invitation incorrecte";
$MESS["IM_CALL_CONFERENCE_ERROR_MAX_USERS"] = "Nombre maximal de participants dépassé";
$MESS["IM_CALL_CONFERENCE_ERROR_NO_PRESENTERS"] = "La liste des intervenants ne peut pas être vide";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_LENGTH_NEW"] = "Le mot de passe de la conférence doit comporter trois caractères ou plus";
$MESS["IM_CALL_CONFERENCE_ERROR_PASSWORD_TYPE"] = "Mot de passe de la vidéoconférence incorrect";
$MESS["IM_CALL_CONFERENCE_ERROR_RENAMING_CHAT"] = "Erreur lors du changement de nom du chat";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_LENGTH"] = "Le nom de la vidéoconférence doit contenir au moins deux caractères";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_REQUIRED"] = "Le nom de la vidéoconférence n'est pas précisé";
$MESS["IM_CALL_CONFERENCE_ERROR_TITLE_TYPE"] = "Nom de la vidéoconférence incorrect";
$MESS["IM_CALL_CONFERENCE_ERROR_TOO_MANY_PRESENTERS"] = "Trop d'intervenants sélectionnés";
