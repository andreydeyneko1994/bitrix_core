<?php
$MESS["IM_ZOOM_MESSAGE_CONFERENCE_CREATED"] = "La salle de réunion Zoom a été créée";
$MESS["IM_ZOOM_MESSAGE_INVITE_LINK"] = "Lien d'invitation";
$MESS["IM_ZOOM_MESSAGE_JOIN_LINK"] = "Lien pour rejoindre la vidéoconférence";
