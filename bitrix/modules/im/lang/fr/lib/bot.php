<?
$MESS["BOT_DEFAULT_WORK_POSITION"] = "Bot de chat";
$MESS["BOT_MESSAGE_FROM"] = "Chat Bot #BOT_NAME#";
$MESS["BOT_MESSAGE_INSTALL_SYSTEM"] = "Un bot de chat a été ajouté à votre compte Bitrix24";
$MESS["BOT_MESSAGE_INSTALL_USER"] = "#USER_NAME# a ajouté un nouveau bot de chat";
$MESS["BOT_MESSAGE_INSTALL_USER_F"] = "#USER_NAME# a ajouté un nouveau bot de chat";
$MESS["BOT_SUPERVISOR_NOTICE_ALL_MESSAGES"] = "Le bot de chat #BOT_NAME# a accès à tous les messages du chat.";
$MESS["BOT_SUPERVISOR_NOTICE_NEW_MESSAGES"] = "Le bot de chat #BOT_NAME# a accès à tous les nouveaux messages du chat.";
?>