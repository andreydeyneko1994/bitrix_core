<?
$MESS["RECENT_ENTITY_ITEM_CID_FIELD"] = "ID du chat";
$MESS["RECENT_ENTITY_ITEM_ID_FIELD"] = "ID de l'enregistrement";
$MESS["RECENT_ENTITY_ITEM_MID_FIELD"] = "ID du message";
$MESS["RECENT_ENTITY_ITEM_RID_FIELD"] = "ID de la relation de chat";
$MESS["RECENT_ENTITY_ITEM_TYPE_FIELD"] = "Type d'enregistrement";
$MESS["RECENT_ENTITY_USER_ID_FIELD"] = "ID de l'utilisateur";
?>