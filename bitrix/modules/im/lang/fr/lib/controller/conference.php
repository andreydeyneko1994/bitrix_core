<?php
$MESS["IM_CONFERENCE_EDIT_CREATION_ERROR"] = "Erreur lors de la création de la conférence";
$MESS["IM_CONFERENCE_EDIT_ERROR_CANT_EDIT"] = "Vous ne pouvez pas modifier cette vidéoconférence";
$MESS["IM_CONFERENCE_EDIT_ERROR_WRONG_ALIAS"] = "Erreur lors de la création de la vidéoconférence";
$MESS["IM_CONFERENCE_EDIT_ERROR_WRONG_ID"] = "ID de la vidéoconférence invalide";
