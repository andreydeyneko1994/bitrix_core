<?
$MESS["IM_INT_USR_INVITE_USERS"] = "#USER_NAME# a envoyé une invitation à #USERS#.";
$MESS["IM_INT_USR_INVITE_USERS_F"] = "#USER_NAME# a envoyé une invitation à #USERS#.";
$MESS["IM_INT_USR_JOIN_2"] = "J'ai rejoint l'équipe.";
$MESS["IM_INT_USR_JOIN_GENERAL_2"] = "J'ai rejoint l'équipe.";
$MESS["IM_INT_USR_LINK_COPIED"] = "#USER_NAME# a copié un lien pour inviter de nouveaux employés.";
$MESS["IM_INT_USR_LINK_COPIED_F"] = "#USER_NAME# a copié un lien pour inviter de nouveaux employés.";
$MESS["IM_INT_USR_REGISTER_USERS"] = "#USER_NAME# a inscrit #USERS#.";
$MESS["IM_INT_USR_REGISTER_USERS_F"] = "#USER_NAME# a inscrit #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_F"] = "#USER_NAME# a retiré les permissions d'administrateur de #USERS#.";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_M"] = "#USER_NAME# a retiré les permissions d'administrateur de #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_F"] = "#USER_NAME# a accordé les permissions d'administrateur à #USERS#.";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_M"] = "#USER_NAME# a accordé les permissions d'administrateur à #USERS#.";
?>