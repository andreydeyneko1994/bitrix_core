<?php
$MESS["IM_CHAT_CHANGE_COLOR_F"] = "#USER_NAME# a changé la couleur du chat en \"#CHAT_COLOR#\"";
$MESS["IM_CHAT_CHANGE_COLOR_M"] = "#USER_NAME# a changé la couleur du chat en \"#CHAT_COLOR#\"";
$MESS["IM_CHAT_CHANGE_TITLE"] = "Sujet du chat \"#CHAT_TITLE#\" modifié";
$MESS["IM_CHAT_CHANGE_TITLE_F"] = "#USER_NAME# a changé le sujet du chat pour '#CHAT_TITLE#'.";
$MESS["IM_CHAT_CHANGE_TITLE_M"] = "#USER_NAME# a changé le sujet du chat pour '#CHAT_TITLE#'.";
$MESS["IM_CHAT_CREATE_OPEN_F_NEW"] = "#USER_NAME# a créé un nouveau chat public \"#CHAT_TITLE#\"";
$MESS["IM_CHAT_CREATE_OPEN_M_NEW"] = "#USER_NAME# a créé un nouveau chat public \"#CHAT_TITLE#\"";
$MESS["IM_CHAT_CREATE_OPEN_NEW"] = "Le chat public \"#CHAT_TITLE#\" a été créé";
$MESS["IM_CHAT_GENERAL_JOIN"] = "#USER_NAME# a été engagé";
$MESS["IM_CHAT_GENERAL_JOIN_F"] = "#USER_NAME# a été engagé";
$MESS["IM_CHAT_GENERAL_JOIN_PLURAL"] = "#USER_NAME# ont été engagés";
$MESS["IM_CHAT_GENERAL_LEAVE_F"] = "#USER_NAME# a été renvoyé";
$MESS["IM_CHAT_GENERAL_LEAVE_M"] = "#USER_NAME# a été renvoyé";
$MESS["IM_CHAT_JOIN_F"] = "#USER_1_NAME# a invité #USER_2_NAME# au chat.";
$MESS["IM_CHAT_JOIN_M"] = "#USER_1_NAME# a invité #USER_2_NAME# au chat.";
$MESS["IM_CHAT_KICK_F"] = "#USER_1_NAME# a exclu #USER_2_NAME# du chat.";
$MESS["IM_CHAT_KICK_M"] = "#USER_1_NAME# a exclu #USER_2_NAME# du chat.";
$MESS["IM_CHAT_KICK_NOTIFICATION_F"] = "#USER_NAME# vous a retiré du chat";
$MESS["IM_CHAT_KICK_NOTIFICATION_M"] = "#USER_NAME# vous a retiré du chat";
$MESS["IM_CHAT_LEAVE_F"] = "#USER_NAME#  a quitté le chat";
$MESS["IM_CHAT_LEAVE_M"] = "#USER_NAME#  a quitté le chat";
$MESS["IM_CHAT_NAME_FORMAT"] = "Chat #COLOR# ##NUMBER#";
$MESS["IM_CHAT_SELF_JOIN"] = "#USERS_NAME# ont rejoint le chat";
$MESS["IM_CHAT_SELF_JOIN_F"] = "#USER_NAME# a rejoint le chat";
$MESS["IM_CHAT_SELF_JOIN_M"] = "#USER_NAME# a rejoint le chat";
$MESS["IM_ERROR_ACCESS_JOIN"] = "Vous ne pouvez pas rejoindre ce chat.";
$MESS["IM_ERROR_AUTHORIZE_ERROR"] = "Seuls les utilisateurs du chat peuvent inviter de nouveaux participants.";
$MESS["IM_ERROR_CHAT_NOT_EXISTS"] = "Le chat spécifié n'existe pas.";
$MESS["IM_ERROR_EMPTY_CHAT_ID"] = "Identifiant du chat non renseigné.";
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "Expéditeur du message n'est pas indiqué.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "Le texte du message n'est pas indiqué.";
$MESS["IM_ERROR_EMPTY_TO_CHAT_ID"] = "Destinataire du message non renseigné.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "Identifiant de l'utilisateur non renseigné.";
$MESS["IM_ERROR_EMPTY_USER_ID_BY_PRIVACY"] = "Ces utilisateurs ont interdit de les inviter au chat";
$MESS["IM_ERROR_EMPTY_USER_OR_CHAT"] = "Identifiant de l'utilisateur ou de chat n'est pas indiqué.";
$MESS["IM_ERROR_KICK"] = "L'expulsion de participants est le droit exclusif du propriétaire du chat.";
$MESS["IM_ERROR_LEAVE_OWNER_FORBIDDEN"] = "Impossible de renvoyer le propriétaire du chat";
$MESS["IM_ERROR_MAX_USER"] = "Un chat ne peut pas compter plus de #COUNT# participants.";
$MESS["IM_ERROR_MIN_USER"] = "Pour créer un chat, il faut indiquer ses participants.";
$MESS["IM_ERROR_MIN_USER_BY_PRIVACY"] = "Impossible de créer un chat, parce que les utilisateurs vous ont interdit de les inviter";
$MESS["IM_ERROR_NOTHING_TO_ADD"] = "Tous les utilisateurs sélectionnés sont déjà inscrits au chat.";
$MESS["IM_ERROR_USER_NOT_FOUND"] = "L'utilisateur spécifié ne chatte pas.";
$MESS["IM_ERROR_VIDEOCONF_MAX_USER"] = "La vidéoconférence ne peut pas accueillir plus de #COUNT# personnes";
$MESS["IM_GENERAL_CREATE_BY_USER_NEW"] = "#USER_NAME# invite à rejoindre un nouveau chat public ";
$MESS["IM_GENERAL_CREATE_NEW"] = "Le chat public a été créé";
$MESS["IM_GENERAL_DESCRIPTION"] = "Un chat général est ouvert à tous vos collègues. Utilisez ce chat pour discuter des sujets importants pour tout employé de votre société.";
$MESS["IM_GENERAL_TITLE"] = "Chat général";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g:i a";
$MESS["IM_PATH_TITLE_CALENDAR_EVENT"] = "Consultation de l'évènement";
$MESS["IM_PATH_TITLE_SONET"] = "Afficher le groupe";
$MESS["IM_PATH_TITLE_TASKS"] = "Afficher la tâche";
$MESS["IM_PERSONAL_DESCRIPTION"] = "Vous seul(e) pouvez voir les messages dans ce chat.[br] Postez ici des brouillons de messages, notes, liens et fichiers afin de les avoir sous la main.";
$MESS["IM_VIDEOCONF_COPY_LINK"] = "Copier le lien";
$MESS["IM_VIDEOCONF_JOIN_LINK"] = "Ouvrir la vidéoconférence";
$MESS["IM_VIDEOCONF_LINK_TITLE"] = "Lien public";
$MESS["IM_VIDEOCONF_NAME_FORMAT"] = "Vidéoconférence ##NUMBER# - #DATE_CREATE#";
$MESS["IM_VIDEOCONF_NAME_FORMAT_NEW"] = "Vidéoconférence ##NUMBER#";
$MESS["IM_VIDEOCONF_NEW_GUEST"] = "Un nouvel invité s'est joint à la conférence « #CHAT_TITLE# ».";
$MESS["IM_VIDEOCONF_SHARE_LINK"] = "Invitez vos collègues et partenaires à rejoindre une vidéoconférence !";
$MESS["IM_VIDEOCONF_SHARE_UPDATED_LINK"] = "#USER_NAME# a changé le lien de la vidéoconférence";
