<?php
$MESS["IM_SHARE_CHAT"] = "Chat";
$MESS["IM_SHARE_CHAT_CALEND"] = "Un [URL=#LINK#]évènement[/URL] a été créé à partir de l'évènement.";
$MESS["IM_SHARE_CHAT_CHAT"] = "Un chat a été créé à partir du message.";
$MESS["IM_SHARE_CHAT_CHAT_JOIN"] = "Rejoindre";
$MESS["IM_SHARE_CHAT_CHAT_WELCOME"] = "Ce chat a été créé à partir d'un message du chat #CHAT#.";
$MESS["IM_SHARE_CHAT_POST_2"] = "Une [URL=#LINK#]publication Actualités[/URL] a été créée sur la base de ce message.";
$MESS["IM_SHARE_CHAT_TASK"] = "Une [URL=#LINK#]tâche[/URL] a été créée à partir du message.";
$MESS["IM_SHARE_FILE"] = "Fichier";
$MESS["IM_SHARE_POST_WELCOME"] = "Discutons :)";
