<?php
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notifications des processus d'affaires";
$MESS["IM_NS_CHAT_NEW"] = "Messages de chat privés";
$MESS["IM_NS_DEFAULT"] = "Notifications non reconnues";
$MESS["IM_NS_IM"] = "Chat et appels";
$MESS["IM_NS_LIKE"] = "Likes de chat";
$MESS["IM_NS_MAIN"] = "Notations et likes";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notification (J'aime)";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notification de vote dans les messages qui vous mentionnent";
$MESS["IM_NS_MENTION"] = "On a parlé de vous dans un chat public";
$MESS["IM_NS_MENTION_2"] = "Quelqu'un vous a mentionné dans le chat";
$MESS["IM_NS_MESSAGE_NEW"] = "Messages de chat";
$MESS["IM_NS_OPEN_NEW"] = "Messages de chat public";
