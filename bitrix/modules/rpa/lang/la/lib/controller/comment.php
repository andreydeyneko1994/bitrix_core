<?php
$MESS["RPA_ADD_COMMENT_ACCESS_DENIED"] = "No tiene permiso para agregar comentarios a este elemento.";
$MESS["RPA_COMMENT_MENTION_NOTIFY"] = "le ha mencionado en el comentario a \"#ITEM#\" en el procesos de negocio #TYPE#, texto del comentario: \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_F"] = "le ha mencionado en el comentario a \"#ITEM#\" en el procesos de negocio #TYPE#, texto del comentario: \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_M"] = "le ha mencionado en el comentario a \"#ITEM#\" en el procesos de negocio #TYPE#, texto del comentario: \"#COMMENT#\"";
$MESS["RPA_COMMENT_NOT_FOUND_ERROR"] = "No se encontró el comentario.";
$MESS["RPA_DELETE_COMMENT_ACCESS_DENIED"] = "No tiene permiso para eliminar este comentario.";
$MESS["RPA_MODIFY_COMMENT_ACCESS_DENIED"] = "No tiene permiso para editar este comentario.";
