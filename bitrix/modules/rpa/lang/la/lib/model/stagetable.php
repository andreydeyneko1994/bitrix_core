<?php
$MESS["RPA_STAGE_TABLE_ADD_SEMANTIC_SUCCESS"] = "Un procesos de negocio solo puede tener una etapa exitosa";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_ITEMS"] = "No puede eliminar esta etapa porque contiene elementos";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_SUCCESS"] = "No puede eliminar una etapa exitosa";
$MESS["RPA_STAGE_TABLE_UPDATE_SEMANTIC_SUCCESS"] = "No puede editar la semántica de una etapa exitosa";
$MESS["RPA_STAGE_TABLE_UPDATE_TYPE_ID"] = "No puede cambiar la ID del procesos de negocio de una etapa existente";
