<?php
$MESS["RPA_PERMISSION_STAGE_MODIFY_DENIED"] = "No tiene permiso para editar las etapas en este procesos de negocio.";
$MESS["RPA_PERMISSION_STAGE_VIEW_DENIED"] = "No tiene permiso para ver las etapas en este procesos de negocio.";
$MESS["RPA_PERMISSION_TYPE_CREATE_DENIED"] = "No puede crear nuevos flujos de trabajo";
$MESS["RPA_PERMISSION_TYPE_MODIFY_DENIED"] = "No puede administrar este procesos de negocio";
$MESS["RPA_PERMISSION_TYPE_NOT_FOUND"] = "No se encontró el ID del tipo. Acceso restringido.";
$MESS["RPA_PERMISSION_TYPE_VIEW_DENIED"] = "No puede ver este procesos de negocio";
