<?php
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_BY"] = "Creado por";
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_TIME"] = "Creado el";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_BY"] = "Movido por";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_TIME"] = "Movido el";
$MESS["RPA_BP_DOCUMENT_ITEM_NAME"] = "Nombre";
$MESS["RPA_BP_DOCUMENT_ITEM_PREVIOUS_STAGE_ID"] = "Etapa previa";
$MESS["RPA_BP_DOCUMENT_ITEM_STAGE_ID"] = "Etapa";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_BY"] = "Modificado por";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_TIME"] = "Última actualización el";
$MESS["RPA_BP_DOCUMENT_ITEM_USER_GROUP_HEAD"] = "Supervisor del creador del procesos de negocio";
$MESS["RPA_BP_DOCUMENT_ITEM_XML_ID"] = "ID externo";
$MESS["RPA_BP_ITEM_ENTITY_NAME"] = "Flujos de trabajo";
