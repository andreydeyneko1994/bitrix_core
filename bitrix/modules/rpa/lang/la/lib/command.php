<?
$MESS["RPA_COMMAND_ITEM_DELETE_PERMISSION_DENIED"] = "No tiene permiso para eliminar el elemento #NAME#";
$MESS["RPA_COMMAND_ITEM_MODIFY_PERMISSION"] = "No tiene permiso para editar elementos en la etapa #STAGE#";
$MESS["RPA_COMMAND_ITEM_MOVE_PERMISSION"] = "No tiene permiso para cambiar la etapa del elemento de la etapa #STAGE#";
$MESS["RPA_COMMAND_ITEM_TASKS_NOT_COMPLETED"] = "Este elemento tiene tareas incompletas";
$MESS["RPA_COMMAND_ITEM_USER_HAS_TASKS"] = "Tiene asignaciones incompletas para este elemento";
$MESS["RPA_COMMAND_ITEM_WRONG_STAGE"] = "No puede cambiar la etapa a #STAGE#";
$MESS["RPA_COMMAND_MANDATORY_FIELD_IS_EMPTY"] = "El campo #FIELD# es requerido.";
?>