<?php
$MESS["RPA_AUTOMATION_ROBOTS"] = "Reglas de automatización";
$MESS["RPA_COMMON_ACTION_DELETE"] = "Eliminar";
$MESS["RPA_COMMON_ACTION_MODIFY"] = "Editar";
$MESS["RPA_COMMON_ACTION_SETTINGS"] = "Configurar";
$MESS["RPA_COMMON_ADD"] = "Agregar";
$MESS["RPA_COMMON_BUTTON_ACTIONS"] = "Acciones";
$MESS["RPA_COMMON_CANCEL"] = "Cancelar";
$MESS["RPA_COMMON_COLOR"] = "Color";
$MESS["RPA_COMMON_FIELDS_SETTINGS"] = "Configuraciones de campos";
$MESS["RPA_COMMON_HAS"] = "Sí";
$MESS["RPA_COMMON_HAS_NO"] = "No";
$MESS["RPA_COMMON_IMAGE"] = "Imagen";
$MESS["RPA_COMMON_ITEM_CREATE_ACCESS_DENIED"] = "No tiene permiso para crear elementos en este procesos de negocio";
$MESS["RPA_COMMON_KANBAN"] = "Kanban";
$MESS["RPA_COMMON_LAUNCH"] = "Inicio del procesos de negocio";
$MESS["RPA_COMMON_LIST"] = "Lista";
$MESS["RPA_COMMON_NEW_PROCESS"] = "Nuevo procesos de negocio";
$MESS["RPA_COMMON_PERMISSIONS"] = "Permisos de acceso";
$MESS["RPA_COMMON_PERMISSIONS_BUTTON"] = "Editar los permisos de acceso";
$MESS["RPA_COMMON_PERMISSIONS_DESCRIPTION"] = "Puede hacer que las etapas y los formularios de visualización de las asignaciones en su procesos de negocio sean visibles o invisibles para los grupos de usuarios seleccionados.";
$MESS["RPA_COMMON_PROCESS"] = "Procesos de negocio";
$MESS["RPA_COMMON_SAVE"] = "Guardar";
$MESS["RPA_COMMON_STAGE"] = "Etapa";
$MESS["RPA_COMMON_STAGES"] = "Etapas";
$MESS["RPA_COMMON_STAGES_SETTINGS"] = "Configuraciones de la etapa";
$MESS["RPA_COMMON_TILES"] = "Detalles";
$MESS["RPA_COMMON_TITLE"] = "Nombre";
$MESS["RPA_COMMON_TYPE_FIELDS_SETTINGS"] = "Configuraciones de campos para #TYPE#";
$MESS["RPA_COMMON_TYPE_SETTINGS"] = "Configuraciones del procesos de negocio";
$MESS["RPA_COMMON_TYPE_VIEW_ACCESS_DENIED"] = "No tiene permiso para ver elementos en este procesos de negocio.";
$MESS["RPA_FILTER_HAS_TASKS_TITLE"] = "Asignaciones";
$MESS["RPA_IS_DISABLED"] = "El uso de las funciones del módulo está restringido.";
$MESS["RPA_ITEM_CREATED_BY"] = "Creado por";
$MESS["RPA_ITEM_CREATED_TIME"] = "Creado el";
$MESS["RPA_ITEM_MOVED_BY"] = "Movido por";
$MESS["RPA_ITEM_MOVED_TIME"] = "Movido el";
$MESS["RPA_ITEM_NOT_FOUND_ERROR"] = "No se encontró el elemento.";
$MESS["RPA_ITEM_UPDATED_BY"] = "Actualizado por";
$MESS["RPA_ITEM_UPDATED_TIME"] = "Actualizado el";
$MESS["RPA_LIMIT_CREATE_ITEM_ERROR"] = "Su plan actual no le permite crear nuevos elementos en este procesos de negocio.";
$MESS["RPA_LIMIT_CREATE_TYPE_ERROR"] = "Su plan actual no le permite crear nuevos flujos de trabajo.";
$MESS["RPA_LIMIT_SETTINGS_TYPE_ERROR"] = "Su plan actual no le permite administrar este procesos de negocio.";
$MESS["RPA_MODIFY_TYPE_ACCESS_DENIED"] = "No tiene permiso para editar este procesos de negocio.";
$MESS["RPA_NOT_FOUND_ERROR"] = "No se encontró el procesos de negocio";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "No se encontró la etapa";
$MESS["RPA_TOP_PANEL_KANBAN"] = "Flujos de trabajo";
$MESS["RPA_TOP_PANEL_PANEL"] = "Automatización Robótica de Procesos (RPA)";
$MESS["RPA_TOP_PANEL_TASK"] = "Todas las asignaciones";
$MESS["RPA_VIEW_ITEM_ACCESS_DENIED"] = "No tiene permiso para ver este elemento.";
