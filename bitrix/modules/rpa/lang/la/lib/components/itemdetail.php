<?php
$MESS["RPA_FIRST_STAGE_NOT_FOUND_ERROR"] = "No se encontró la etapa inicial";
$MESS["RPA_FIRST_STAGE_PERMISSION_DENIED"] = "No tiene permiso para agregar elementos a la etapa inicial.";
$MESS["RPA_ITEM_CREATE_TITLE"] = "Crear un elemento #TYPE#";
$MESS["RPA_ITEM_EDIT_TITLE"] = "Elemento #TYPE# #ID#";
$MESS["RPA_ITEM_NOT_FOUND"] = "No se encontró el elemento.";
$MESS["RPA_STAGE_MOVE_PERMISSION_DENIED"] = "No se puede mover el elemento de la etapa #STAGE_FROM# a #STAGE_TO#";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "No se encontró la etapa";
$MESS["RPA_STAGE_PERMISSION_DENIED"] = "No tiene permiso para editar elementos en esta etapa.";
$MESS["RPA_TYPE_PERMISSION_ERROR"] = "No tiene permiso para ver elementos en este procesos de negocio.";
