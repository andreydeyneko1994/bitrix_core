<?
$MESS["RPA_INSTALL_TITLE"] = "Instalación del módulo de Automatización Robótica de Procesos (RPA)";
$MESS["RPA_MODULE_DESCRIPTION"] = "Este módulo implementa el módulo de Automatización Robótica de Procesos (RPA)";
$MESS["RPA_MODULE_NAME"] = "Automatización Robótica de Procesos (RPA)";
$MESS["RPA_UNINSTALL_QUESTION"] = "¿Seguro que desea eliminar el módulo?";
$MESS["RPA_UNINSTALL_TITLE"] = "Desinstalación del módulo de Automatización Robótica de Procesos (RPA)";
?>