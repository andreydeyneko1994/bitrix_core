<?
$MESS["RPA_COMMAND_ITEM_DELETE_PERMISSION_DENIED"] = "Vous n'avez pas les permissions de supprimer l'élément #NAME#";
$MESS["RPA_COMMAND_ITEM_MODIFY_PERMISSION"] = "Vous n'avez pas les permissions pour modifier les éléments de l'étape #STAGE#";
$MESS["RPA_COMMAND_ITEM_MOVE_PERMISSION"] = "Vous n'avez pas les permissions de modifier l'étape de l'élément à partir de l'étape #STAGE#";
$MESS["RPA_COMMAND_ITEM_TASKS_NOT_COMPLETED"] = "Cet élément a des affectations inachevées";
$MESS["RPA_COMMAND_ITEM_USER_HAS_TASKS"] = "Vous avez des affectations inachevées pour cet élément";
$MESS["RPA_COMMAND_ITEM_WRONG_STAGE"] = "Vous ne pouvez pas modifier l'étape en #STAGE#";
$MESS["RPA_COMMAND_MANDATORY_FIELD_IS_EMPTY"] = "Le champ obligatoire #FIELD# n'est pas rempli";
?>