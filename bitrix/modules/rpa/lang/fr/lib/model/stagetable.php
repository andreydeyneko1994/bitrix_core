<?
$MESS["RPA_STAGE_TABLE_ADD_SEMANTIC_SUCCESS"] = "Un flux de travail ne peut avoir qu'une seule étape réussie";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_ITEMS"] = "Vous ne pouvez pas supprimer cette étape parce qu'elle contient des éléments";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_SUCCESS"] = "Vous ne pouvez pas supprimer une étape réussie";
$MESS["RPA_STAGE_TABLE_UPDATE_SEMANTIC_SUCCESS"] = "Vous ne pouvez pas éditer la sémantique d'une étape réussie";
$MESS["RPA_STAGE_TABLE_UPDATE_TYPE_ID"] = "Vous ne pouvez pas modifier l'ID de flux de travail d'une étape existante";
?>