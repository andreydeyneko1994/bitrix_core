<?
$MESS["RPA_ADD_COMMENT_ACCESS_DENIED"] = "Vous n'avez pas le droit d'ajouter des commentaires à cet élément";
$MESS["RPA_COMMENT_MENTION_NOTIFY"] = "Vous êtes mentionné dans un commentaire sur l'élément \"#ITEM#\" du processus #TYPE#, texte du commentaire : \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_F"] = "Vous êtes mentionné dans un commentaire sur l'élément \"#ITEM#\" du processus #TYPE#, texte du commentaire : \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_M"] = "Vous êtes mentionné dans un commentaire sur l'élément \"#ITEM#\" du processus #TYPE#, texte du commentaire : \"#COMMENT#\"";
$MESS["RPA_COMMENT_NOT_FOUND_ERROR"] = "Le commentaire n'a pas été trouvé.";
$MESS["RPA_DELETE_COMMENT_ACCESS_DENIED"] = "Vous n'avez pas le droit de supprimer ce commentaire";
$MESS["RPA_MODIFY_COMMENT_ACCESS_DENIED"] = "Vous n'avez pas le droit de modifier ce commentaire";
?>