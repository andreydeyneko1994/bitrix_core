<?
$MESS["RPA_FIRST_STAGE_NOT_FOUND_ERROR"] = "L'étape initiale est introuvable";
$MESS["RPA_FIRST_STAGE_PERMISSION_DENIED"] = "Vous n'avez pas les permissions pour ajouter des éléments à l'étape initiale.";
$MESS["RPA_ITEM_CREATE_TITLE"] = "Créer un élément #TYPE#";
$MESS["RPA_ITEM_EDIT_TITLE"] = "Élément #TYPE# #ID#";
$MESS["RPA_ITEM_NOT_FOUND"] = "L'élément est introuvable.";
$MESS["RPA_STAGE_MOVE_PERMISSION_DENIED"] = "Impossible de déplacer l'élément de l'étape #STAGE_FROM# à l'étape #STAGE_TO#";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "L'étape est introuvable";
$MESS["RPA_STAGE_PERMISSION_DENIED"] = "Vous n'avez pas les permissions pour modifier les éléments de cette étape.";
$MESS["RPA_TYPE_PERMISSION_ERROR"] = "Vous n'avez pas les permissions pour afficher les éléments de ce flux de travail.";
?>