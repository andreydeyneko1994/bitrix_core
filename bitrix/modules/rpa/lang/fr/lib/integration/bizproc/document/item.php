<?
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_BY"] = "Créé par";
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_TIME"] = "Date de création";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_BY"] = "Déplacé par";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_TIME"] = "Date de déplacement";
$MESS["RPA_BP_DOCUMENT_ITEM_NAME"] = "Nom";
$MESS["RPA_BP_DOCUMENT_ITEM_PREVIOUS_STAGE_ID"] = "Étape précédente";
$MESS["RPA_BP_DOCUMENT_ITEM_STAGE_ID"] = "Étape";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_BY"] = "Modifié par";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_TIME"] = "Date de la dernière mise à jour";
$MESS["RPA_BP_DOCUMENT_ITEM_USER_GROUP_HEAD"] = "Superviseur du créateur de flux de travail";
$MESS["RPA_BP_DOCUMENT_ITEM_XML_ID"] = "ID externe";
$MESS["RPA_BP_ITEM_ENTITY_NAME"] = "Flux de travail";
?>