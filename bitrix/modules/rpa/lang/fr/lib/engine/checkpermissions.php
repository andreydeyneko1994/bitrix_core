<?
$MESS["RPA_PERMISSION_STAGE_MODIFY_DENIED"] = "Vous n'avez pas les permissions pour modifier les étapes de ce flux de travail.";
$MESS["RPA_PERMISSION_STAGE_VIEW_DENIED"] = "Vous n'avez pas les permissions pour afficher les étapes de ce flux de travail.";
$MESS["RPA_PERMISSION_TYPE_CREATE_DENIED"] = "Vous ne pouvez pas créer de nouveau flux de travail";
$MESS["RPA_PERMISSION_TYPE_MODIFY_DENIED"] = "Vous ne pouvez pas gérer ce flux de travail";
$MESS["RPA_PERMISSION_TYPE_NOT_FOUND"] = "L'ID de type est introuvable. Accès restreint.";
$MESS["RPA_PERMISSION_TYPE_VIEW_DENIED"] = "Vous ne pouvez pas afficher ce flux de travail";
?>