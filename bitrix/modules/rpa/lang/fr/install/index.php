<?
$MESS["RPA_INSTALL_TITLE"] = "Installation du module Automatisation robotisée des processus (RPA)";
$MESS["RPA_MODULE_DESCRIPTION"] = "Ce module implémente le module Automatisation robotisée des processus (RPA)";
$MESS["RPA_MODULE_NAME"] = "Automatisation robotisée des processus (RPA)";
$MESS["RPA_UNINSTALL_QUESTION"] = "Voulez-vous vraiment supprimer le module ?";
$MESS["RPA_UNINSTALL_TITLE"] = "Désinstallation du module Automatisation robotisée des processus (RPA)";
?>