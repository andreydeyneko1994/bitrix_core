<?
$MESS["RPA_FIRST_STAGE_NOT_FOUND_ERROR"] = "Nie znaleziono etapu początkowego";
$MESS["RPA_FIRST_STAGE_PERMISSION_DENIED"] = "Nie masz uprawnień do dodawania elementów do etapu początkowego.";
$MESS["RPA_ITEM_CREATE_TITLE"] = "Utwórz element #TYPE#";
$MESS["RPA_ITEM_EDIT_TITLE"] = "Element #TYPE# #ID#";
$MESS["RPA_ITEM_NOT_FOUND"] = "Nie znaleziono elementu.";
$MESS["RPA_STAGE_MOVE_PERMISSION_DENIED"] = "Nie można przenieść elementu z etapu #STAGE_FROM# do #STAGE_TO#";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "Nie znaleziono etapu";
$MESS["RPA_STAGE_PERMISSION_DENIED"] = "Nie masz uprawnień do edycji elementu na tym etapie.";
$MESS["RPA_TYPE_PERMISSION_ERROR"] = "Nie masz uprawnień do wyświetlania elementów tego workflowu.";
?>