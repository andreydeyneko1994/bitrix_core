<?
$MESS["RPA_STAGE_TABLE_ADD_SEMANTIC_SUCCESS"] = "Workflow może mieć wyłącznie jeden etap sukcesu";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_ITEMS"] = "Nie możesz usunąć tego etapu, ponieważ zawiera on elementy";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_SUCCESS"] = "Nie możesz usunąć etapu sukcesu";
$MESS["RPA_STAGE_TABLE_UPDATE_SEMANTIC_SUCCESS"] = "Nie możesz edytować semantyki etapu sukcesu";
$MESS["RPA_STAGE_TABLE_UPDATE_TYPE_ID"] = "Nie możesz zmienić ID workflowu istniejącego etapu";
?>