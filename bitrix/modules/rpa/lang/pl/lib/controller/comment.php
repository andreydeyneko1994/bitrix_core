<?
$MESS["RPA_ADD_COMMENT_ACCESS_DENIED"] = "Nie masz uprawnień do dodawania komentarzy do tego elementu.";
$MESS["RPA_COMMENT_MENTION_NOTIFY"] = "wspomniał o Tobie w komentarzu do elementu \"#ITEM#\" w workflow #TYPE#, komentarz: \"#COMMENT:#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_F"] = "wspomniał o Tobie w komentarzu do elementu \"#ITEM#\" w workflow #TYPE#, komentarz: \"#COMMENT:#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_M"] = "wspomniał o Tobie w komentarzu do elementu \"#ITEM#\" w workflow #TYPE#, komentarz: \"#COMMENT:#\"";
$MESS["RPA_COMMENT_NOT_FOUND_ERROR"] = "Nie znaleziono komentarza.";
$MESS["RPA_DELETE_COMMENT_ACCESS_DENIED"] = "Nie masz uprawnień do usunięcia tego komentarza.";
$MESS["RPA_MODIFY_COMMENT_ACCESS_DENIED"] = "Nie masz uprawnień do edycji tego komentarza.";
?>