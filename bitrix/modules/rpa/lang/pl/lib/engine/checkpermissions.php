<?
$MESS["RPA_PERMISSION_STAGE_MODIFY_DENIED"] = "Nie masz uprawnień do edycji etapów tego workflowu.";
$MESS["RPA_PERMISSION_STAGE_VIEW_DENIED"] = "Nie masz uprawnień do wyświetlania etapów tego workflowu.";
$MESS["RPA_PERMISSION_TYPE_CREATE_DENIED"] = "Nie możesz tworzyć nowych workflowów";
$MESS["RPA_PERMISSION_TYPE_MODIFY_DENIED"] = "Nie możesz zarządzać tym workflowem";
$MESS["RPA_PERMISSION_TYPE_NOT_FOUND"] = "Nie znaleziono ID typu. Dostęp ograniczony.";
$MESS["RPA_PERMISSION_TYPE_VIEW_DENIED"] = "Nie możesz wyświetlić tego workflowu";
?>