<?
$MESS["RPA_COMMAND_ITEM_DELETE_PERMISSION_DENIED"] = "Nie masz uprawnień do usunięcia elementu #NAME#";
$MESS["RPA_COMMAND_ITEM_MODIFY_PERMISSION"] = "Nie masz uprawnień do edycji elementów w etapie #STAGE#";
$MESS["RPA_COMMAND_ITEM_MOVE_PERMISSION"] = "Nie masz uprawnień do zmiany elementu etapu z etapu #STAGE#";
$MESS["RPA_COMMAND_ITEM_TASKS_NOT_COMPLETED"] = "Ten element ma niedokończone zadania";
$MESS["RPA_COMMAND_ITEM_USER_HAS_TASKS"] = "Masz niedokończone zadania dla tego elementu";
$MESS["RPA_COMMAND_ITEM_WRONG_STAGE"] = "Nie możesz zmienić etapu na #STAGE#";
$MESS["RPA_COMMAND_MANDATORY_FIELD_IS_EMPTY"] = "Pole #FIELD# jest wymagane.";
?>