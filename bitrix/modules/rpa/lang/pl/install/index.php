<?
$MESS["RPA_INSTALL_TITLE"] = "Instalacja modułu Robotycznej automatyzacji procesów (RPA)";
$MESS["RPA_MODULE_DESCRIPTION"] = "Ten moduł implementuje moduł Robotycznej automatyzacji procesów (RPA)";
$MESS["RPA_MODULE_NAME"] = "Robotyczna automatyzacja procesów (RPA)";
$MESS["RPA_UNINSTALL_QUESTION"] = "Na pewno usunąć ten moduł?";
$MESS["RPA_UNINSTALL_TITLE"] = "Dezinstalacja modułu Robotycznej automatyzacji procesów (RPA)";
?>