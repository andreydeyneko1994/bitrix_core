<?
$MESS["RPA_INSTALL_TITLE"] = "Instalação do módulo Automação Robótica de Processos (RPA)";
$MESS["RPA_MODULE_DESCRIPTION"] = "Este módulo implementa o módulo Automação Robótica de Processos (RPA)";
$MESS["RPA_MODULE_NAME"] = "Automação Robótica de Processos (RPA)";
$MESS["RPA_UNINSTALL_QUESTION"] = "Tem certeza de que deseja excluir o módulo?";
$MESS["RPA_UNINSTALL_TITLE"] = "Desinstalação do módulo Automação Robótica de Processos (RPA)";
?>