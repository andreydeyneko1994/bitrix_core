<?
$MESS["RPA_ADD_COMMENT_ACCESS_DENIED"] = "Você não tem permissão para adicionar comentários a este item.";
$MESS["RPA_COMMENT_MENTION_NOTIFY"] = "mencionou você no comentário sobre \"#ITEM#\" no fluxo de trabalho #TYPE#, texto do comentário: \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_F"] = "mencionou você no comentário sobre \"#ITEM#\" no fluxo de trabalho #TYPE#, texto do comentário: \"#COMMENT#\"";
$MESS["RPA_COMMENT_MENTION_NOTIFY_M"] = "mencionou você no comentário sobre \"#ITEM#\" no fluxo de trabalho #TYPE#, texto do comentário: \"#COMMENT#\"";
$MESS["RPA_COMMENT_NOT_FOUND_ERROR"] = "O comentário não foi encontrado.";
$MESS["RPA_DELETE_COMMENT_ACCESS_DENIED"] = "Você não tem permissão para excluir este comentário.";
$MESS["RPA_MODIFY_COMMENT_ACCESS_DENIED"] = "Você não tem permissão para editar este comentário.";
?>