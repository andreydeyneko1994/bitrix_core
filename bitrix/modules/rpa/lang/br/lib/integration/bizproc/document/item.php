<?
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_BY"] = "Criado por";
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_TIME"] = "Criado em";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_BY"] = "Movido por";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_TIME"] = "Movido em";
$MESS["RPA_BP_DOCUMENT_ITEM_NAME"] = "Nome";
$MESS["RPA_BP_DOCUMENT_ITEM_PREVIOUS_STAGE_ID"] = "Estágio anterior";
$MESS["RPA_BP_DOCUMENT_ITEM_STAGE_ID"] = "Etapa";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_BY"] = "Modificado por";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_TIME"] = "Última atualização em";
$MESS["RPA_BP_DOCUMENT_ITEM_USER_GROUP_HEAD"] = "Supervisor do autor do processo";
$MESS["RPA_BP_DOCUMENT_ITEM_XML_ID"] = "ID externo";
$MESS["RPA_BP_ITEM_ENTITY_NAME"] = "Fluxos de trabalho";
?>