<?
$MESS["RPA_COMMAND_ITEM_DELETE_PERMISSION_DENIED"] = "Você não tem permissão para excluir o item #NAME#";
$MESS["RPA_COMMAND_ITEM_MODIFY_PERMISSION"] = "Você não tem permissão para editar itens na etapa #STAGE#";
$MESS["RPA_COMMAND_ITEM_MOVE_PERMISSION"] = "Você não tem permissão para alterar a etapa do item da etapa #STAGE#";
$MESS["RPA_COMMAND_ITEM_TASKS_NOT_COMPLETED"] = "Este item tem atribuições incompletas";
$MESS["RPA_COMMAND_ITEM_USER_HAS_TASKS"] = "Você tem atribuições incompletas para este item";
$MESS["RPA_COMMAND_ITEM_WRONG_STAGE"] = "Você não pode alterar a etapa para #STAGE#";
$MESS["RPA_COMMAND_MANDATORY_FIELD_IS_EMPTY"] = "O campo #FIELD# é obrigatório.";
?>