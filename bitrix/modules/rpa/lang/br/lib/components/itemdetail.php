<?
$MESS["RPA_FIRST_STAGE_NOT_FOUND_ERROR"] = "A etapa inicial não foi encontrada";
$MESS["RPA_FIRST_STAGE_PERMISSION_DENIED"] = "Você não tem permissão para adicionar itens na etapa inicial.";
$MESS["RPA_ITEM_CREATE_TITLE"] = "Criar item #TYPE#";
$MESS["RPA_ITEM_EDIT_TITLE"] = "#ID# do item #TYPE#";
$MESS["RPA_ITEM_NOT_FOUND"] = "O item não foi encontrado.";
$MESS["RPA_STAGE_MOVE_PERMISSION_DENIED"] = "Não é possível mover o item da etapa #STAGE_FROM# para #STAGE_TO#";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "A etapa não foi encontrada";
$MESS["RPA_STAGE_PERMISSION_DENIED"] = "Você não tem permissão para editar itens nesta etapa.";
$MESS["RPA_TYPE_PERMISSION_ERROR"] = "Você não tem permissão para visualizar itens neste fluxo de trabalho.";
?>