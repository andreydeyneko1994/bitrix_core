<?
$MESS["RPA_PERMISSION_STAGE_MODIFY_DENIED"] = "Você não tem permissão para editar etapas neste fluxo de trabalho.";
$MESS["RPA_PERMISSION_STAGE_VIEW_DENIED"] = "Você não tem permissão para visualizar etapas neste fluxo de trabalho.";
$MESS["RPA_PERMISSION_TYPE_CREATE_DENIED"] = "Você não pode criar novos fluxos de trabalho";
$MESS["RPA_PERMISSION_TYPE_MODIFY_DENIED"] = "Você não pode gerenciar este fluxo de trabalho";
$MESS["RPA_PERMISSION_TYPE_NOT_FOUND"] = "O ID do tipo não foi encontrado. Acesso restrito.";
$MESS["RPA_PERMISSION_TYPE_VIEW_DENIED"] = "Você não pode visualizar este fluxo de trabalho";
?>