<?
$MESS["RPA_STAGE_TABLE_ADD_SEMANTIC_SUCCESS"] = "Um fluxo de trabalho pode ter apenas uma etapa bem sucedida";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_ITEMS"] = "Você não pode excluir esta etapa porque ela contém itens";
$MESS["RPA_STAGE_TABLE_DELETE_ERROR_SUCCESS"] = "Você não pode excluir uma etapa bem sucedida";
$MESS["RPA_STAGE_TABLE_UPDATE_SEMANTIC_SUCCESS"] = "Você não pode editar a semântica de uma etapa bem sucedida";
$MESS["RPA_STAGE_TABLE_UPDATE_TYPE_ID"] = "Você não pode alterar o ID do fluxo de trabalho de uma etapa existente";
?>