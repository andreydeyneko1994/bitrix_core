<?
$MESS["RPA_KANBAN_COLUMN_ADD_TASK_BTN"] = "Dodaj zadanie";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_BTN"] = "Usuń";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_CONFIRM"] = "Na pewno usunąć to zadanie?";
$MESS["RPA_KANBAN_FIELDS_MODIFY_SETTINGS"] = "Dostosuj pola formularza szybkiego tworzenia";
$MESS["RPA_KANBAN_FIELDS_VIEW_SETTINGS"] = "Dostosuj pola formularza widoku";
$MESS["RPA_KANBAN_MOVE_EMPTY_MANDATORY_FIELDS_ERROR"] = "Ten element ma puste pola wymagane, ale nie masz uprawnień do edycji tego elementu.";
$MESS["RPA_KANBAN_MOVE_ITEM_HAS_TASKS_ERROR"] = "Nie możesz przenieść elementu, ponieważ zawiera on niedokończone zadania.";
$MESS["RPA_KANBAN_MOVE_ITEM_PERMISSION_NOTIFY"] = "Nie możesz zmienić elementu etapu dla #ITEM# gdy jego bieżący etap to #STAGE#";
$MESS["RPA_KANBAN_MOVE_PERMISSION_NOTIFY"] = "Nie masz uprawnień do zmiany elementu etapu z etapu #STAGE#";
$MESS["RPA_KANBAN_MOVE_WRONG_STAGE_NOTIFY"] = "Nie można przenieść elementu z etapu #STAGE_FROM# do #STAGE_TO#";
$MESS["RPA_KANBAN_QUICK_FORM_CANCEL_BUTTON"] = "Anuluj";
$MESS["RPA_KANBAN_QUICK_FORM_SAVE_BUTTON"] = "Zapisz";
$MESS["RPA_KANBAN_TASKS"] = "Zadania";
?>