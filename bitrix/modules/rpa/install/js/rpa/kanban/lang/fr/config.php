<?
$MESS["RPA_KANBAN_COLUMN_ADD_TASK_BTN"] = "Ajouter l'affectation";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_BTN"] = "Supprimer";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_CONFIRM"] = "Voulez-vous vraiment supprimer l'affectation ?";
$MESS["RPA_KANBAN_FIELDS_MODIFY_SETTINGS"] = "Personnaliser les champs du formulaire de création rapide";
$MESS["RPA_KANBAN_FIELDS_VIEW_SETTINGS"] = "Personnaliser les champs du formulaire d'affichage";
$MESS["RPA_KANBAN_MOVE_EMPTY_MANDATORY_FIELDS_ERROR"] = "L'élément compte des champs obligatoires vides, mais vous ne disposez pas des permissions pour le modifier.";
$MESS["RPA_KANBAN_MOVE_ITEM_HAS_TASKS_ERROR"] = "Vous ne pouvez pas déplacer l'élément parce qu'il contient des affectations inachevées.";
$MESS["RPA_KANBAN_MOVE_ITEM_PERMISSION_NOTIFY"] = "Vous ne pouvez pas modifier l'étape de l'élément de #ITEM# quand son étape actuelle est #STAGE#";
$MESS["RPA_KANBAN_MOVE_PERMISSION_NOTIFY"] = "Vous n'avez pas les permissions de modifier l'étape de l'élément à partir de l'étape #STAGE#";
$MESS["RPA_KANBAN_MOVE_WRONG_STAGE_NOTIFY"] = "Impossible de déplacer l'élément de l'étape #STAGE_FROM# à l'étape #STAGE_TO#";
$MESS["RPA_KANBAN_QUICK_FORM_CANCEL_BUTTON"] = "Annuler";
$MESS["RPA_KANBAN_QUICK_FORM_SAVE_BUTTON"] = "Enregistrer";
$MESS["RPA_KANBAN_TASKS"] = "Affectations";
?>