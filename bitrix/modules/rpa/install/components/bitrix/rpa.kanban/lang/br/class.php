<?
$MESS["RPA_KANBAN_PERMISSION_ERROR"] = "Você não tem permissões para editar as preferências do fluxo de trabalho";
$MESS["RPA_KANBAN_POPUP_CANCEL"] = "Cancelar";
$MESS["RPA_KANBAN_POPUP_SAVE"] = "Salvar";
$MESS["RPA_KANBAN_STAGE_NOT_FOUND"] = "A etapa não foi encontrada";
?>