<?
$MESS["RPA_COMPONENT_STAGE_ACTIONS"] = "Actions";
$MESS["RPA_COMPONENT_STAGE_EDITABLE_FIELDS"] = "Champs modifiables pour cette étape";
$MESS["RPA_COMPONENT_STAGE_FIELD_CHOOSE"] = "Sélectionner les champs";
$MESS["RPA_COMPONENT_STAGE_FIELD_CODE"] = "Code";
$MESS["RPA_COMPONENT_STAGE_FIELD_NAME"] = "Nom";
$MESS["RPA_COMPONENT_STAGE_FIELD_STAGES"] = "Étape de transition possible";
$MESS["RPA_COMPONENT_STAGE_MANDATORY_FIELDS"] = "Champs obligatoires pour cette étape";
$MESS["RPA_COMPONENT_STAGE_NEXT_POSSIBLE_STAGES"] = "Prochaines étapes";
$MESS["RPA_COMPONENT_STAGE_PERMISSION"] = "Permission pour";
$MESS["RPA_COMPONENT_STAGE_VISIBLE_FIELDS"] = "Champs visibles pour cette étape";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_CREATE"] = "Utilisateurs autorisés à ajouter des éléments";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MODIFY"] = "Utilisateurs autorisés à modifier les éléments de l'étape";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MOVE"] = "Utilisateurs autorisés à modifier l'étape de l'élément";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_VIEW"] = "Utilisateurs autorisés à afficher les éléments de l'étape";
?>