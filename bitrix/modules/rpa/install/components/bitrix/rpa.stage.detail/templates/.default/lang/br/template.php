<?
$MESS["RPA_COMPONENT_STAGE_ACTIONS"] = "Ações";
$MESS["RPA_COMPONENT_STAGE_EDITABLE_FIELDS"] = "Campos editáveis para esta etapa";
$MESS["RPA_COMPONENT_STAGE_FIELD_CHOOSE"] = "Selecionar campos";
$MESS["RPA_COMPONENT_STAGE_FIELD_CODE"] = "Código";
$MESS["RPA_COMPONENT_STAGE_FIELD_NAME"] = "Nome";
$MESS["RPA_COMPONENT_STAGE_FIELD_STAGES"] = "Possível estágio de destino:";
$MESS["RPA_COMPONENT_STAGE_MANDATORY_FIELDS"] = "Campos obrigatórios para esta etapa";
$MESS["RPA_COMPONENT_STAGE_NEXT_POSSIBLE_STAGES"] = "Próximas etapas";
$MESS["RPA_COMPONENT_STAGE_PERMISSION"] = "Permissão para";
$MESS["RPA_COMPONENT_STAGE_VISIBLE_FIELDS"] = "Campos visíveis para esta etapa";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_CREATE"] = "Usuários com permissão para adicionar itens";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MODIFY"] = "Usuários com permissão para editar itens da etapa";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MOVE"] = "Usuários com permissão para alterar a etapa do item";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_VIEW"] = "Usuários com permissão para visualizar itens da etapa";
?>