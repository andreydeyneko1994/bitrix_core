<?
$MESS["RPA_COMPONENT_STAGE_ACTIONS"] = "Działania";
$MESS["RPA_COMPONENT_STAGE_EDITABLE_FIELDS"] = "Pola edytowalne dla tego etapu";
$MESS["RPA_COMPONENT_STAGE_FIELD_CHOOSE"] = "Wybierz pola";
$MESS["RPA_COMPONENT_STAGE_FIELD_CODE"] = "Kod";
$MESS["RPA_COMPONENT_STAGE_FIELD_NAME"] = "Nazwa";
$MESS["RPA_COMPONENT_STAGE_FIELD_STAGES"] = "Możliwy etap do przeniesienia";
$MESS["RPA_COMPONENT_STAGE_MANDATORY_FIELDS"] = "Pola wymagane dla tego etapu";
$MESS["RPA_COMPONENT_STAGE_NEXT_POSSIBLE_STAGES"] = "Następne etapy";
$MESS["RPA_COMPONENT_STAGE_PERMISSION"] = "Uprawnienie do";
$MESS["RPA_COMPONENT_STAGE_VISIBLE_FIELDS"] = "Pola widoczne dla tego etapu";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_CREATE"] = "Użytkownicy mogący dodawać elementy";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MODIFY"] = "Użytkownicy mogący edytować elementy etapu";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MOVE"] = "Użytkownicy mogący zmienić element etapu";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_VIEW"] = "Użytkownicy mogący wyświetlać elementy etapu";
?>