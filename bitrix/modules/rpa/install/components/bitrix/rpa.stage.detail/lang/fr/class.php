<?
$MESS["RPA_STAGE_CREATE_TITLE"] = "Créer une nouvelle étape";
$MESS["RPA_STAGE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cette étape ?";
$MESS["RPA_STAGE_DETAIL_TITLE"] = "Modifier l'étape #TITLE#";
$MESS["RPA_STAGE_EDITABLE_FIELDS_TITLE"] = "Champs modifiables";
$MESS["RPA_STAGE_MANDATORY_FIELDS_TITLE"] = "Champs obligatoires";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "L'étape du flux de travail est introuvable.";
$MESS["RPA_STAGE_TYPE_ACCESS_DENIED"] = "Vous ne pouvez pas modifier les étapes de ce flux de travail.";
$MESS["RPA_STAGE_VISIBLE_FIELDS_TITLE"] = "Champs visibles";
?>