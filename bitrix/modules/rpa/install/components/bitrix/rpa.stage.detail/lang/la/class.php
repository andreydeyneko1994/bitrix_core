<?php
$MESS["RPA_STAGE_CREATE_TITLE"] = "Crear nueva etapa";
$MESS["RPA_STAGE_DELETE_CONFIRM"] = "¿Seguro que desea eliminar la etapa?";
$MESS["RPA_STAGE_DETAIL_TITLE"] = "Editar etapa la etapa #TITLE#";
$MESS["RPA_STAGE_EDITABLE_FIELDS_TITLE"] = "Campos editables";
$MESS["RPA_STAGE_MANDATORY_FIELDS_TITLE"] = "Campos obligatorios";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "No se encontró la etapa del procesos de negocio.";
$MESS["RPA_STAGE_TYPE_ACCESS_DENIED"] = "No puede editar las etapas en este procesos de negocio.";
$MESS["RPA_STAGE_VISIBLE_FIELDS_TITLE"] = "Campos visibles";
