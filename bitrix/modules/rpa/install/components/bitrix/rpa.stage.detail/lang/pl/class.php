<?
$MESS["RPA_STAGE_CREATE_TITLE"] = "Utwórz nowy etap";
$MESS["RPA_STAGE_DELETE_CONFIRM"] = "Na pewno usunąć ten etap?";
$MESS["RPA_STAGE_DETAIL_TITLE"] = "Edytuj etap #TITLE#";
$MESS["RPA_STAGE_EDITABLE_FIELDS_TITLE"] = "Pola edytowalne";
$MESS["RPA_STAGE_MANDATORY_FIELDS_TITLE"] = "Pola wymagane";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "Nie znaleziono etapu workflowu.";
$MESS["RPA_STAGE_TYPE_ACCESS_DENIED"] = "Nie możesz edytować etapów w tym workflow.";
$MESS["RPA_STAGE_VISIBLE_FIELDS_TITLE"] = "Widoczne pola";
?>