<?
$MESS["RPA_STAGES_COMMON_STAGES_LABEL"] = "Etapas adicionales";
$MESS["RPA_STAGES_FAIL_STAGE_LABEL"] = "Etapas fallidas";
$MESS["RPA_STAGES_FAIL_STAGE_TITLE"] = "Se produjo un fallo";
$MESS["RPA_STAGES_FINAL_STAGES_TITLE"] = "Etapas finales";
$MESS["RPA_STAGES_FIRST_STAGE_LABEL"] = "Etapa inicial";
$MESS["RPA_STAGES_NEW_STAGE_NAME"] = "Nueva etapa";
$MESS["RPA_STAGES_STAGE_ADD"] = "Añadir etapa";
$MESS["RPA_STAGES_STAGE_CHANGE_TITLE"] = "Editar el nombre";
$MESS["RPA_STAGES_STAGE_PANEL_COLOR"] = "Cambiar el color";
$MESS["RPA_STAGES_STAGE_PANEL_RELOAD"] = "Restablecer vista";
$MESS["RPA_STAGES_SUCCESS_STAGE_LABEL"] = "Etapa exitosa";
$MESS["RPA_STAGES_SUCCESS_STAGE_TITLE"] = "Éxito";
?>