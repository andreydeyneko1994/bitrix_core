<?
$MESS["RPA_PANEL_DELETE_CONFIRM_TEXT"] = "Voulez-vous vraiment supprimer ce flux de travail ? Cette action est irréversible.";
$MESS["RPA_PANEL_DELETE_CONFIRM_TITLE"] = "Supprimer le flux de travail";
?>