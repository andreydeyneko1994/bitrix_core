<?
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_NAME"] = "Terminer le processus";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_FAIL"] = "Rejeté";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_TITLE"] = "Sélectionnez le résultat du flux de travail.";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_SELECTOR_TITLE"] = "Achèvement du processus :";
$MESS["RPA_ITEM_DETAIL_ITEM_EXTERNAL_UPDATE_NOTIFY"] = "L'élément a été modifié. Voulez-vous recharger la page ?";
$MESS["RPA_ITEM_DETAIL_TAB_MAIN"] = "Commun";
$MESS["RPA_ITEM_DETAIL_TAB_ROBOTS"] = "Règles d'automatisation";
?>