<?
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_NAME"] = "Zakończ workflow";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_FAIL"] = "Odmowa";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_TITLE"] = "Wybierz wynik workflow";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_SELECTOR_TITLE"] = "Zakończ workflow:";
$MESS["RPA_ITEM_DETAIL_ITEM_EXTERNAL_UPDATE_NOTIFY"] = "Element został zmieniony. Czy chcesz ponownie załadować stronę?";
$MESS["RPA_ITEM_DETAIL_TAB_MAIN"] = "Typowe";
$MESS["RPA_ITEM_DETAIL_TAB_ROBOTS"] = "Reguły automatyzacji";
?>