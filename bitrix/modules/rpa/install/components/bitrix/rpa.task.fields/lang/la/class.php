<?php
$MESS["RPA_AUTOMATION_TASK_FIELDS_SET"] = "Completar la información";
$MESS["RPA_AUTOMATION_TASK_FIELDS_SHOW"] = "Revisar la información";
$MESS["RPA_AUTOMATION_TASK_FIELDS_WARNING"] = "Algunos campos se eliminaron del procesos de negocio y no pueden especificarse";
