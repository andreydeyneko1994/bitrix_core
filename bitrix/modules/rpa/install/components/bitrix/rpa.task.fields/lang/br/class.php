<?php
$MESS["RPA_AUTOMATION_TASK_FIELDS_SET"] = "Preencha as informações";
$MESS["RPA_AUTOMATION_TASK_FIELDS_SHOW"] = "Revise as informações";
$MESS["RPA_AUTOMATION_TASK_FIELDS_WARNING"] = "Alguns campos foram excluídos do fluxo de trabalho e não podem ser especificados";
