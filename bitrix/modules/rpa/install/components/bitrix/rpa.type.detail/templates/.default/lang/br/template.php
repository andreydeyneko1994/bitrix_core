<?
$MESS["RPA_COMPONENT_TYPE_WHO_CAN_MODIFY"] = "Usuários com permissão para editar o fluxo de trabalho";
$MESS["RPA_FIELDS_SELECTOR_TITLE"] = "Campos a serem preenchidos no início";
$MESS["RPA_LAUNCH_AUTOMATION"] = "Após o fluxo de trabalho especificado ser concluído";
$MESS["RPA_LAUNCH_MANUAL"] = "Início manual";
$MESS["RPA_LAUNCH_MANUAL_PERMISSION"] = "Usuários com permissão para iniciar o fluxo de trabalho";
$MESS["RPA_LAUNCH_SCHEDULE"] = "Usar agenda";
$MESS["RPA_MOVE_TO_THE_NEXT_STAGE"] = "Ao criar, mova os itens para a próxima etapa após criados";
$MESS["RPA_TYPE_IMAGE_SECTION"] = "Imagem do fluxo de trabalho";
$MESS["RPA_TYPE_IMAGE_UPLOAD"] = "Carregar imagem";
?>