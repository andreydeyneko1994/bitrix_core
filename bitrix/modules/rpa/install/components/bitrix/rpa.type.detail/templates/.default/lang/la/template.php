<?php
$MESS["RPA_COMPONENT_TYPE_WHO_CAN_MODIFY"] = "Los usuarios pueden editar el procesos de negocio";
$MESS["RPA_FIELDS_SELECTOR_TITLE"] = "Campos para completar al inicio";
$MESS["RPA_LAUNCH_AUTOMATION"] = "Después de completar el procesos de negocio especificado";
$MESS["RPA_LAUNCH_MANUAL"] = "Inicio manual";
$MESS["RPA_LAUNCH_MANUAL_PERMISSION"] = "Los usuarios pueden iniciar el procesos de negocio";
$MESS["RPA_LAUNCH_SCHEDULE"] = "Utilizar el horario";
$MESS["RPA_MOVE_TO_THE_NEXT_STAGE"] = "Cuando se crea, mueve los elementos a la siguiente etapa después de que se creó";
$MESS["RPA_TYPE_IMAGE_SECTION"] = "Imagen del procesos de negocio";
$MESS["RPA_TYPE_IMAGE_UPLOAD"] = "Cargar imagen";
