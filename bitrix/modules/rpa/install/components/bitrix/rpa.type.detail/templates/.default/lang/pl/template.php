<?
$MESS["RPA_COMPONENT_TYPE_WHO_CAN_MODIFY"] = "Użytkownicy mogący edytować workflow";
$MESS["RPA_FIELDS_SELECTOR_TITLE"] = "Pola do wypełnienia przy starcie";
$MESS["RPA_LAUNCH_AUTOMATION"] = "Po zakończeniu określonego workflowu";
$MESS["RPA_LAUNCH_MANUAL"] = "Start ręczny";
$MESS["RPA_LAUNCH_MANUAL_PERMISSION"] = "Użytkownicy mogą rozpocząć workflow";
$MESS["RPA_LAUNCH_SCHEDULE"] = "Użyj harmonogram";
$MESS["RPA_MOVE_TO_THE_NEXT_STAGE"] = "Po utworzeniu przenieś elementy do następnego etapu po utworzeniu";
$MESS["RPA_TYPE_IMAGE_SECTION"] = "Obraz workflowu";
$MESS["RPA_TYPE_IMAGE_UPLOAD"] = "Prześlij obraz";
?>