<?
$MESS["RPA_COMPONENT_TYPE_WHO_CAN_MODIFY"] = "Utilisateurs autorisés à éditer le flux de travail";
$MESS["RPA_FIELDS_SELECTOR_TITLE"] = "Les champs à remplir au démarrage";
$MESS["RPA_LAUNCH_AUTOMATION"] = "Après que le flux de travail indiqué soit terminé";
$MESS["RPA_LAUNCH_MANUAL"] = "Démarrage manuel";
$MESS["RPA_LAUNCH_MANUAL_PERMISSION"] = "Les utilisateurs autorisés à démarrer un flux de travail";
$MESS["RPA_LAUNCH_SCHEDULE"] = "Utiliser le programme";
$MESS["RPA_MOVE_TO_THE_NEXT_STAGE"] = "À sa création, déplacer les éléments à la prochaine étape après la création";
$MESS["RPA_TYPE_IMAGE_SECTION"] = "Image du flux de travail";
$MESS["RPA_TYPE_IMAGE_UPLOAD"] = "Télécharger l'image";
?>