<?
$MESS["RPA_BP_CHS_EMPTY_TARGET_STAGE_ID"] = "No se especificó la etapa de destino";
$MESS["RPA_BP_CHS_INCORRECT_TARGET_STAGE_ID"] = "Etapa de destino incorrecta";
$MESS["RPA_BP_CHS_MODIFIED_BY"] = "Modificar en nombre de";
$MESS["RPA_BP_CHS_NEXT_STAGE_ID"] = "Siguiente etapa";
$MESS["RPA_BP_CHS_PREVIOUS_STAGE_ID"] = "Etapa anterior";
$MESS["RPA_BP_CHS_TARGET_STAGE_ID"] = "Etapa de destino";
$MESS["RPA_BP_CHS_TERMINATED"] = "Etapa cambiada";
$MESS["RPA_BP_CHS_TERMINATED_CYCLING"] = "El cambio de etapa puede resultar en un bucle infinito";
?>