<?
$MESS["RPA_BP_CHS_EMPTY_TARGET_STAGE_ID"] = "A etapa de destino não está especificada";
$MESS["RPA_BP_CHS_INCORRECT_TARGET_STAGE_ID"] = "Etapa de destino incorreta";
$MESS["RPA_BP_CHS_MODIFIED_BY"] = "Modificar em nome de";
$MESS["RPA_BP_CHS_NEXT_STAGE_ID"] = "Próxima etapa";
$MESS["RPA_BP_CHS_PREVIOUS_STAGE_ID"] = "Etapa anterior";
$MESS["RPA_BP_CHS_TARGET_STAGE_ID"] = "Etapa de destino";
$MESS["RPA_BP_CHS_TERMINATED"] = "Etapa alterada";
$MESS["RPA_BP_CHS_TERMINATED_CYCLING"] = "A alteração do estágio pode resultar em um loop infinito";
?>