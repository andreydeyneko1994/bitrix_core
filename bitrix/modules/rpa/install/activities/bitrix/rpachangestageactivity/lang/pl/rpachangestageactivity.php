<?
$MESS["RPA_BP_CHS_EMPTY_TARGET_STAGE_ID"] = "Nie określono etapu docelowego";
$MESS["RPA_BP_CHS_INCORRECT_TARGET_STAGE_ID"] = "Nieprawidłowy etap docelowy";
$MESS["RPA_BP_CHS_MODIFIED_BY"] = "Modyfikuj w imieniu";
$MESS["RPA_BP_CHS_NEXT_STAGE_ID"] = "Następny etap";
$MESS["RPA_BP_CHS_PREVIOUS_STAGE_ID"] = "Poprzedni etap";
$MESS["RPA_BP_CHS_TARGET_STAGE_ID"] = "Etap docelowy";
$MESS["RPA_BP_CHS_TERMINATED"] = "Zmieniono etap";
$MESS["RPA_BP_CHS_TERMINATED_CYCLING"] = "Podejrzenie niekończącej się zmiany etapów";
?>