<?
$MESS["RPA_BP_CHS_EMPTY_TARGET_STAGE_ID"] = "L'étape ciblée n'est pas spécifiée";
$MESS["RPA_BP_CHS_INCORRECT_TARGET_STAGE_ID"] = "Étape de la cible incorrecte";
$MESS["RPA_BP_CHS_MODIFIED_BY"] = "Modifier de la part de";
$MESS["RPA_BP_CHS_NEXT_STAGE_ID"] = "Prochaine étape";
$MESS["RPA_BP_CHS_PREVIOUS_STAGE_ID"] = "Étape précédente";
$MESS["RPA_BP_CHS_TARGET_STAGE_ID"] = "Étape ciblée";
$MESS["RPA_BP_CHS_TERMINATED"] = "Étape modifiée";
$MESS["RPA_BP_CHS_TERMINATED_CYCLING"] = "Le changement d'étape peut entraîner une boucle infinie";
?>