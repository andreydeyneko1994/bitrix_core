<?php
$MESS["RPA_BP_APR_DESCR_DESCRIPTION"] = "Creates a task with approve/decline option.";
$MESS["RPA_BP_APR_DESCR_LA"] = "Last approved by";
$MESS["RPA_BP_APR_DESCR_NAME"] = "Approve or decline";
