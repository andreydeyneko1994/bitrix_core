<?
$MESS["RPA_BP_APR_ACT_NO_ACTION"] = "Aucune action spécifiée.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY1"] = "Aucun responsable sélectionné pour l'affectation.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY2"] = "Le type d'approbation n'est pas spécifié.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY3"] = "Type d'approbation incorrect";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY4"] = "Le nom de l'affectation n'est pas spécifié.";
$MESS["RPA_BP_APR_ERROR_STAGE_ID"] = "Étape cible incorrecte";
$MESS["RPA_BP_APR_FIELD_ACTIONS"] = "Boutons";
$MESS["RPA_BP_APR_FIELD_ALTER_RESPONSIBLE"] = "Remplaçants autorisés";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_NO"] = "Refuser";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_YES"] = "Approuver";
$MESS["RPA_BP_APR_FIELD_APPROVE_FIXED_COUNT"] = "Personnes approuvées";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE"] = "Type d'approbation";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ALL"] = "Tout";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ANY"] = "Un des éléments listés";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_FIXED"] = "N'importe lequel";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_QUEUE"] = "Tout ce qui est dans la file d'attente";
$MESS["RPA_BP_APR_FIELD_DESCRIPTION"] = "Texte";
$MESS["RPA_BP_APR_FIELD_EXECUTIVE_RESPONSIBLE"] = "Approbateur final";
$MESS["RPA_BP_APR_FIELD_FIELDS_TO_SHOW"] = "Afficher les champs";
$MESS["RPA_BP_APR_FIELD_NAME"] = "Nom";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE"] = "Type de responsable";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_HEADS"] = "Superviseurs d'employé";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_PLAIN"] = "File d'attente des utilisateurs";
$MESS["RPA_BP_APR_FIELD_SKIP_ABSENT"] = "Ignorer les utilisateurs absents";
$MESS["RPA_BP_APR_FIELD_USERS"] = "Responsables";
$MESS["RPA_BP_APR_RUNTIME_TERMINATED"] = "Étape modifiée";
?>