<?
$MESS["MAIL_INSTALL_BACK"] = "Powrót do sekcji zarządzania modułem";
$MESS["MAIL_INSTALL_TITLE"] = "Deinstalacja modułu wiadomości";
$MESS["MAIL_MODULE_DESC"] = "Moduł wiadomości służy do odbierania wiadomości, filtrowania oraz wykonywania określonych działań.";
$MESS["MAIL_MODULE_NAME"] = "Poczta";
$MESS["MAIL_UNINSTALL_COMPLETE"] = "Odinstalowywanie zakończone.";
$MESS["MAIL_UNINSTALL_DEL"] = "Dezinstalacja";
$MESS["MAIL_UNINSTALL_ERROR"] = "Błędy deinstalacji:";
$MESS["MAIL_UNINSTALL_SAVETABLE"] = "Zabisz Tabele";
$MESS["MAIL_UNINSTALL_WARNING"] = "Ostrzeżenie! Moduł zostanie odinstalowany z systemu.";
?>