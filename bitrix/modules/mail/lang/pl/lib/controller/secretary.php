<?php
$MESS["MAIL_SECRETARY_ACCESS_DENIED"] = "To działanie jest dostępne wyłącznie dla właściciela skrzynki pocztowej";
$MESS["MAIL_SECRETARY_ACCESS_DENIED_CALENDAR"] = "To działanie jest dostępne wyłącznie dla twórcy wydarzenia w kalendarzu";
$MESS["MAIL_SECRETARY_CALENDAR_EVENT_DESC"] = "
Wysłane przez: [url=#LINK_FROM#]#FROM#[/url]

Temat: #SUBJECT#

Data: #DATE#

[url=#LINK#]Wyświetl wiadomość e-mail[/url]
";
$MESS["MAIL_SECRETARY_CREATE_CHAT_LOCK_ERROR"] = "Błąd podczas tworzenia czatu. Spróbuj ponownie.";
$MESS["MAIL_SECRETARY_POST_MESSAGE_CALENDAR_EVENT"] = "Utworzono na podstawie [url=#LINK#]wiadomości e-mail[/url]";
