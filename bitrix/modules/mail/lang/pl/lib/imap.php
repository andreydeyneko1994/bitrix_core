<?
$MESS["MAIL_IMAP_ERR_APPEND"] = "Błąd zapisywania wiadomości";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Błąd uwierzytelnienia";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Serwer nie obsługuje wymaganej metody uwierzytelniania.";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "Nie można uzyskać pozwolenia";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Serwer zwrócił nieznaną odpowiedź.";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "Błąd podczas uzyskiwania możliwości";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "Polecenie odrzucone";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Błąd komunikacyjny.";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Błąd połączenia z serwerem";
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "Nieznany błąd";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "Serwer nie odpowiedział";
$MESS["MAIL_IMAP_ERR_FETCH"] = "Błąd uzyskiwania wiadomości";
$MESS["MAIL_IMAP_ERR_LIST"] = "Błąd pobierania listy folderów";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "Połączenie odrzucone";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "Wyszukiwanie poczty nie powiodło się";
$MESS["MAIL_IMAP_ERR_SELECT"] = "Błąd wybierania folderu";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "Błąd podczas inicjowania crypto API";
$MESS["MAIL_IMAP_ERR_STORE"] = "Błąd w trakcie aktualizacji wiadomości";
?>