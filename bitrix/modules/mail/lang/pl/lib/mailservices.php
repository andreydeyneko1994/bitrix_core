<?
$MESS["mail_mailservice_bitrix24_icon"] = "post-bitrix24-icon-en.png";
$MESS["mail_mailservice_entity_active_field"] = "Aktywne";
$MESS["mail_mailservice_entity_encryption_field"] = "Zabezpiecz połączenie";
$MESS["mail_mailservice_entity_flags_field"] = "Flagi";
$MESS["mail_mailservice_entity_icon_field"] = "Logo";
$MESS["mail_mailservice_entity_link_field"] = "Strona WWW interfejsu";
$MESS["mail_mailservice_entity_name_field"] = "Nazwa";
$MESS["mail_mailservice_entity_port_field"] = "Port";
$MESS["mail_mailservice_entity_server_field"] = "Adres serwera";
$MESS["mail_mailservice_entity_site_field"] = "Strona internetowa";
$MESS["mail_mailservice_entity_sort_field"] = "Sortuj";
$MESS["mail_mailservice_entity_token_field"] = "Token";
$MESS["mail_mailservice_entity_type_field"] = "Rodzaj";
$MESS["mail_mailservice_not_found"] = "Nie znaleziono usługi e-mail.";
?>