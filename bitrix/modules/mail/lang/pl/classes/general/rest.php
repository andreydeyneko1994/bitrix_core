<?
$MESS["MAIL_MAILSERVICE_ACTIVE"] = "Aktywne";
$MESS["MAIL_MAILSERVICE_EMPTY"] = "Nie znaleziono usługi e-mail.";
$MESS["MAIL_MAILSERVICE_EMPTY_ID"] = "ID usługi pocztowej nie zostało określone.";
$MESS["MAIL_MAILSERVICE_ENCRYPTION"] = "Zabezpiecz połączenie";
$MESS["MAIL_MAILSERVICE_ICON"] = "Logo";
$MESS["MAIL_MAILSERVICE_LINK"] = "Strona WWW interfejsu";
$MESS["MAIL_MAILSERVICE_LIST_EMPTY"] = "Nie znaleziono usług e-mail.";
$MESS["MAIL_MAILSERVICE_NAME"] = "Nazwa";
$MESS["MAIL_MAILSERVICE_PORT"] = "Port";
$MESS["MAIL_MAILSERVICE_SERVER"] = "Adres serwera";
$MESS["MAIL_MAILSERVICE_SITE_ID"] = "Strona internetowa";
$MESS["MAIL_MAILSERVICE_SORT"] = "Sortuj";
?>