<?
$MESS["MAIL_IMAP_ERR_AUTH"] = "Błąd uwierzytelnienia";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Serwer nie obsługuje wymaganej metody uwierzytelniania.";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Serwer zwrócił nieznaną odpowiedź.";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Błąd komunikacji.";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Błąd połączenia";
?>