<?
$MESS["MAIL_CHECK_CHECK"] = "Otrzymane wiadomości:";
$MESS["MAIL_CHECK_CHECK_ALL"] = "(wszystkie skrzynki)";
$MESS["MAIL_CHECK_CHECK_OK"] = "OK";
$MESS["MAIL_CHECK_CNT"] = "Otrzymane";
$MESS["MAIL_CHECK_CNT_NEW"] = "nowa wiadomość.";
$MESS["MAIL_CHECK_ERR"] = "Błąd podczas odbierania wiadomości.";
$MESS["MAIL_CHECK_LOG"] = "Pokaż logowania";
$MESS["MAIL_CHECK_MBOX_PARAMS"] = "Ustawienia skrzynki odbiorczej";
$MESS["MAIL_CHECK_TEXT"] = "Skrzynka odbiorcza sprawdzona";
$MESS["MAIL_CHECK_TITLE"] = "Sprawdź nowe wiadomości";
$MESS["MAIL_CHECK_VIEW"] = "Pokaż wiadomości";
?>