<?
$MESS["MAIL_MENU_LOG"] = "Dziennik korespondencji";
$MESS["MAIL_MENU_LOG_ALT"] = "Dziennik przetwarzania poczty";
$MESS["MAIL_MENU_MAIL"] = "Poczta";
$MESS["MAIL_MENU_MAILBOXES"] = "Skrzynki";
$MESS["MAIL_MENU_MAILBOXES_ALT"] = "ustawienia poczty i profilu";
$MESS["MAIL_MENU_MAILSERVICES"] = "Usługi e-mail";
$MESS["MAIL_MENU_MAILSERVICES_ALT"] = "Konfiguracja usług e-mail";
$MESS["MAIL_MENU_MAIL_TITLE"] = "Zarządzanie pocztą";
$MESS["MAIL_MENU_MSG"] = "wiadomości";
$MESS["MAIL_MENU_MSG_ALT"] = "Zobacz wiadomości";
$MESS["MAIL_MENU_RULES"] = "Zasady";
$MESS["MAIL_MENU_RULES_ALT"] = "Ustawienia profili dla wiadomości";
?>