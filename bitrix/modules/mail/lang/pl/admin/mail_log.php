<?
$MESS["MAIL_LOG_FILT_ANY"] = "(jakikolwiek)";
$MESS["MAIL_LOG_FILT_MBOX"] = "Skrzynka pocztowa";
$MESS["MAIL_LOG_FILT_MSG"] = "Wiadomość";
$MESS["MAIL_LOG_FILT_RULE"] = "Reguły poczty";
$MESS["MAIL_LOG_FILT_SHOW_COLUMN"] = "Pokaż kolumny";
$MESS["MAIL_LOG_LISTEMPTY"] = "Lista jest pusta";
$MESS["MAIL_LOG_LISTTOTAL"] = "Suma:";
$MESS["MAIL_LOG_MBOX"] = "Skrzynka pocztowa";
$MESS["MAIL_LOG_MSG"] = "Wiadomość";
$MESS["MAIL_LOG_NAVIGATION"] = "linie";
$MESS["MAIL_LOG_RULE"] = "Profil";
$MESS["MAIL_LOG_TEXT"] = "Tekst";
$MESS["MAIL_LOG_TIME"] = "Czas";
$MESS["MAIL_LOG_TITLE"] = "Dziennik przetwarzania poczty";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
?>