<?
$MESS["MAIL_MBOX_ADM_ACT"] = "Aktywne";
$MESS["MAIL_MBOX_ADM_ACTIONS"] = "Działania";
$MESS["MAIL_MBOX_ADM_CHANGE"] = "Edytuj ustawienia skrzynki pocztowej";
$MESS["MAIL_MBOX_ADM_CHANGE2"] = "Edytuj";
$MESS["MAIL_MBOX_ADM_DATECH"] = "Modyfikuj";
$MESS["MAIL_MBOX_ADM_DELERR"] = "Błąd podczas usuwania skrzynki pocztowej";
$MESS["MAIL_MBOX_ADM_DELETE"] = "Usuń";
$MESS["MAIL_MBOX_ADM_DELETE_ALT"] = "Usuń skrzynkę pocztową";
$MESS["MAIL_MBOX_ADM_DEL_CONFIRM"] = "skrzynki pocztowej i wszystkie wiadomości, filtry i proile zostaną usunięte. Kontynuować?";
$MESS["MAIL_MBOX_ADM_FILT_ACT"] = "Aktywne";
$MESS["MAIL_MBOX_ADM_FILT_ADR"] = "adres serwera:";
$MESS["MAIL_MBOX_ADM_FILT_ANY"] = "(jakikolwiek)";
$MESS["MAIL_MBOX_ADM_FILT_LANG"] = "Strona";
$MESS["MAIL_MBOX_ADM_FILT_NAME"] = "Nazwa";
$MESS["MAIL_MBOX_ADM_FILT_TYPE"] = "wpisz:";
$MESS["MAIL_MBOX_ADM_FILT_USER_TYPE"] = "Typ:";
$MESS["MAIL_MBOX_ADM_LANG"] = "Strona";
$MESS["MAIL_MBOX_ADM_LISTEMPTY"] = "Lista jest pusta";
$MESS["MAIL_MBOX_ADM_LISTTOTAL"] = "Suma:";
$MESS["MAIL_MBOX_ADM_LOG"] = "Zapis";
$MESS["MAIL_MBOX_ADM_LOG_ALT"] = "Pokaż logowania";
$MESS["MAIL_MBOX_ADM_MESSAGES"] = "wiadomości";
$MESS["MAIL_MBOX_ADM_MESSAGES_NEW"] = "Nowe wiadomości";
$MESS["MAIL_MBOX_ADM_MESSAGES_TOTAL"] = "Całkowita wiadomość";
$MESS["MAIL_MBOX_ADM_MSGS"] = "Poczta wiadomości";
$MESS["MAIL_MBOX_ADM_NAME"] = "Nazwa";
$MESS["MAIL_MBOX_ADM_NAVIGATION"] = "Skrzynki";
$MESS["MAIL_MBOX_ADM_NEWRULE"] = "Dodaj nowy profil";
$MESS["MAIL_MBOX_ADM_RULES"] = "Zasady zarządzania";
$MESS["MAIL_MBOX_ADM_RULES_LINK"] = "Zasady";
$MESS["MAIL_MBOX_ADM_TITLE"] = "Skrzynki";
$MESS["MAIL_MBOX_ADM_TYPE"] = "Typ";
$MESS["MAIL_MBOX_ADM_USER_TYPE"] = "Rodzaj";
$MESS["MAIL_MBOX_ADM_USER_TYPE_ADM"] = "system";
$MESS["MAIL_MBOX_ADM_USER_TYPE_USER"] = "Użytkownik";
$MESS["MAIL_MBOX_ADM_VIEW"] = "Wyświetl";
$MESS["MAIL_MBOX_ADR"] = "Adres serwera";
$MESS["MAIL_SAVE_ERROR"] = "Błąd podczas aktualizowania skrzynki pocztowej";
$MESS["MAIN_ADD"] = "Dodawanie";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "aktywuj";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "deaktywuj";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
?>