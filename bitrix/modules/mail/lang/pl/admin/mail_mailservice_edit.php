<?
$MESS["MAIL_MSERVICE_EDT_ACT"] = "Aktywny:";
$MESS["MAIL_MSERVICE_EDT_BACK_LINK"] = "Usługi e-mail";
$MESS["MAIL_MSERVICE_EDT_COMMENT1"] = "OpenSSL dla PHP wymaga użycia bezpiecznego połączenia.";
$MESS["MAIL_MSERVICE_EDT_DELETE"] = "Usuń to";
$MESS["MAIL_MSERVICE_EDT_DELETE_CONFIRM"] = "Usunąć tą usługę?";
$MESS["MAIL_MSERVICE_EDT_DOMAIN"] = "Nazwa domeny:";
$MESS["MAIL_MSERVICE_EDT_ENCRYPTION"] = "Użyć bezpiecznego połączenia (TLS)";
$MESS["MAIL_MSERVICE_EDT_ERROR"] = "Błąd zapisu usługi pocztowej";
$MESS["MAIL_MSERVICE_EDT_ICON"] = "Logo:";
$MESS["MAIL_MSERVICE_EDT_ICON_REMOVE"] = "Usuń plik";
$MESS["MAIL_MSERVICE_EDT_ID"] = "ID:";
$MESS["MAIL_MSERVICE_EDT_LINK"] = "Strona WWW interfejsu";
$MESS["MAIL_MSERVICE_EDT_NAME"] = "Nazwa:";
$MESS["MAIL_MSERVICE_EDT_NEW"] = "Nowa usługa e-mail";
$MESS["MAIL_MSERVICE_EDT_PUBLIC"] = "Zezwól pracownikom na rejestrowanie e-maili w domenie";
$MESS["MAIL_MSERVICE_EDT_SERVER"] = "E-mail serwer (IMAP) / port:";
$MESS["MAIL_MSERVICE_EDT_SITE_ID"] = "Strona WWW";
$MESS["MAIL_MSERVICE_EDT_SORT"] = "Kolejność sortowania:";
$MESS["MAIL_MSERVICE_EDT_TAB"] = "Usługa e-mail";
$MESS["MAIL_MSERVICE_EDT_TITLE_1"] = "Edytuj usługę e-mail ##ID#";
$MESS["MAIL_MSERVICE_EDT_TITLE_2"] = "Nowa usługa e-mail";
$MESS["MAIL_MSERVICE_EDT_TOKEN"] = "Token:";
$MESS["MAIL_MSERVICE_EDT_TYPE"] = "Typ:";
?>