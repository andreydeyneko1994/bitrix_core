<?
$MESS["MAIL_MSERVICE_ADM_ACTIVE"] = "Aktywne";
$MESS["MAIL_MSERVICE_ADM_CHANGE"] = "Edytuj";
$MESS["MAIL_MSERVICE_ADM_DELETE"] = "Usuń";
$MESS["MAIL_MSERVICE_ADM_DELETE_CONFIRM"] = "Czy chcesz usunąć usługę e-mail?";
$MESS["MAIL_MSERVICE_ADM_FILT_ANY"] = "(jakikolwiek)";
$MESS["MAIL_MSERVICE_ADM_NAME"] = "Nazwa";
$MESS["MAIL_MSERVICE_ADM_SERVER"] = "Adres serwera";
$MESS["MAIL_MSERVICE_ADM_SITE_ID"] = "Strona internetowa";
$MESS["MAIL_MSERVICE_ADM_TITLE"] = "Usługi e-mail";
$MESS["MAIL_MSERVICE_ADM_TYPE"] = "Rodzaj";
$MESS["MAIL_MSERVICE_DELETE_ERROR"] = "Błąd usuwania usługi pocztowej";
$MESS["MAIL_MSERVICE_SAVE_ERROR"] = "Błąd aktualizacji usługi pocztowej";
$MESS["MAIN_ADD"] = "Dodawanie";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "aktywuj";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "deaktywuj";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
?>