<?
$MESS["MAIL_IMAP_ERR_APPEND"] = "Erro ao salvar a mensagem";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Erro de autenticação";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "O servidor não suporta o método de autenticação necessário.";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "Não é possível obter permissão";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "O servidor retornou uma resposta desconhecida.";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "Erro ao obter recursos";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "Comando recusado";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Erro de comunicação.";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Erro de conexão ao servidor";
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "Erro desconhecido";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "O servidor não retornou resposta";
$MESS["MAIL_IMAP_ERR_FETCH"] = "Erro ao receber mensagens";
$MESS["MAIL_IMAP_ERR_LIST"] = "Erro ao obter lista de pastas";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "Conexão recusada";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "Falha na pesquisa de e-mail";
$MESS["MAIL_IMAP_ERR_SELECT"] = "Erro ao selecionar uma pasta";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "Erro ao inicializar a API de criptografia";
$MESS["MAIL_IMAP_ERR_STORE"] = "Erro ao atualizar a mensagem";
?>