<?php
$MESS["MAIL_SECRETARY_ACCESS_DENIED"] = "Esta ação está disponível apenas para o proprietário da caixa de correio";
$MESS["MAIL_SECRETARY_ACCESS_DENIED_CALENDAR"] = "Esta ação está disponível apenas para o criador do evento do calendário";
$MESS["MAIL_SECRETARY_CALENDAR_EVENT_DESC"] = "
Enviado por: [url=#LINK_FROM#]#FROM#[/url]

Assunto: #SUBJECT#

Data: #DATE#

[url =# LINK#]Visualizar e-mail[/url]
";
$MESS["MAIL_SECRETARY_CREATE_CHAT_LOCK_ERROR"] = "Erro ao criar bate-papo. Tente novamente.";
$MESS["MAIL_SECRETARY_POST_MESSAGE_CALENDAR_EVENT"] = "Criado com base em [url=#LINK#]mensagem de e-mail[/url]";
