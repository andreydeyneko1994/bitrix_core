<?php
$MESS["MAIL_SECRETARY_ACCESS_DENIED"] = "Esta acción solo está disponible para el propietario del buzón";
$MESS["MAIL_SECRETARY_ACCESS_DENIED_CALENDAR"] = "Esta acción solo está disponible para el creador de los eventos del calendario";
$MESS["MAIL_SECRETARY_CALENDAR_EVENT_DESC"] = "
Enviado por: [url=#LINK_FROM#]#FROM#[/url]

Asunto: #SUBJECT#

Fecha: #DATE#

[url=#LINK#]Ver el correo electrónico[/url]
";
$MESS["MAIL_SECRETARY_CREATE_CHAT_LOCK_ERROR"] = "Error al crear el chat. Intente de nuevo.";
$MESS["MAIL_SECRETARY_POST_MESSAGE_CALENDAR_EVENT"] = "Creado con base en un [url=#LINK#]mensaje de correo electrónico[/url]";
