<?php
$MESS["MAIL_SECRETARY_ACCESS_DENIED"] = "Cette action est réservée au propriétaire de la messagerie";
$MESS["MAIL_SECRETARY_ACCESS_DENIED_CALENDAR"] = "Cette action est réservée aux créateur de l'évènement de calendrier";
$MESS["MAIL_SECRETARY_CALENDAR_EVENT_DESC"] = "
Envoyé par : [url=#LINK_FROM#]#FROM#[/url]

Sujet : #SUBJECT#

Date : #DATE#

[url=#LINK#]Voir l'email[/url]
";
$MESS["MAIL_SECRETARY_CREATE_CHAT_LOCK_ERROR"] = "Erreur lors de la création du chat. Veuillez réessayer.";
$MESS["MAIL_SECRETARY_POST_MESSAGE_CALENDAR_EVENT"] = "Créé à partir d'un [url=#LINK#]message par e-mail[/url]";
