<?php
$MESS["MAIL_CLIENT_ACTIVITY_COMMUNICATIONS_EMPTY_ERROR"] = "Określ odbiorców, którzy mają zapisać wiadomość e-mail w CRM";
$MESS["MAIL_CLIENT_ACTIVITY_CREATE_ERROR"] = "Błąd podczas zapisywania wiadomości e-mail w CRM";
$MESS["MAIL_CLIENT_ACTIVITY_PERMISSION_DENIED_ERROR"] = "Niewystarczające uprawnienia do zapisywania wiadomości e-mail w CRM";
$MESS["MAIL_MESSAGE_BAD_SENDER"] = "Nieprawidłowy nadawca";
$MESS["MAIL_MESSAGE_EMPTY_RCPT"] = "Określ odbiorców";
$MESS["MAIL_MESSAGE_EMPTY_SENDER"] = "Określ nadawcę";
$MESS["MAIL_MESSAGE_SEND_ERROR"] = "Nie można wysłać wiadomości";
$MESS["MAIL_MESSAGE_TO_MANY_RECIPIENTS"] = "Nie możesz dodać więcej niż 10 odbiorców";
