<?php
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR"] = "Dirección de correo electrónico no válida";
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR_EMAIL_IS_ALREADY_EXISTS"] = "<a title=\"Open contact view form\" data-role=\"contact-email\">Ya existe un contacto</a> con este correo electrónico";
$MESS["MAIL_DIALOG_EDIT_CONTACT_TITLE_BAR_ADD"] = "Nuevo contacto";
