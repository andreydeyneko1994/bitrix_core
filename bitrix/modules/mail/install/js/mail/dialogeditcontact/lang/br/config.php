<?php
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR"] = "Endereço de e-mail inválido";
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR_EMAIL_IS_ALREADY_EXISTS"] = "<a title=\"Open contact view form\" data-role=\"contact-email\">Já existe um contato</a> com este e-mail";
$MESS["MAIL_DIALOG_EDIT_CONTACT_TITLE_BAR_ADD"] = "Novo contato";
