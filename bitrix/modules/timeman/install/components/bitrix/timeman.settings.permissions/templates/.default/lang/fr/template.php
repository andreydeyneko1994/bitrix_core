<?
$MESS["TIMEMAN_SETTINGS_PERMS_ADD"] = "Ajouter";
$MESS["TIMEMAN_SETTINGS_PERMS_ADD_ACCESS"] = "Ajouter un droit d'accès";
$MESS["TIMEMAN_SETTINGS_PERMS_DELETE"] = "Supprimer";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT"] = "Éditer";
$MESS["TIMEMAN_SETTINGS_PERMS_ERROR"] = "Erreur";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_CANCEL"] = "Annuler";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_DELETE"] = "Supprimer le rôle";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le rôle ?";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_DELETE_ERROR"] = "Erreur de suppression du rôle.";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_LIST"] = "Rôles";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_OK"] = "OK";
$MESS["TIMEMAN_SETTINGS_PERMS_ROLE_TITLE"] = "Rôle";
$MESS["TIMEMAN_SETTINGS_PERMS_SAVE"] = "Enregistrer";
?>