<?
$MESS["OP_NAME_TM_MANAGE"] = "Mi horario de entrada y de salida";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Entrada y salida de otras personas";
$MESS["OP_NAME_TM_READ"] = "Ver todos los registros";
$MESS["OP_NAME_TM_READ_SCHEDULES_ALL"] = "Ver todos los horarios";
$MESS["OP_NAME_TM_READ_SHIFT_PLANS_ALL"] = "Ver todos los horarios de los turnos";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Ver mis registros y mis subordinados";
$MESS["OP_NAME_TM_SETTINGS"] = "Editar la configuración del módulo";
$MESS["OP_NAME_TM_UPDATE_SCHEDULES_ALL"] = "Editar todos los horarios de trabajo";
$MESS["OP_NAME_TM_UPDATE_SHIFT_PLANS_ALL"] = "Editar todos los horarios de los turnos";
$MESS["OP_NAME_TM_WRITE"] = "Editar todos los registros";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Editar mis registros y los de mis subordinados";
$MESS["TASK_NAME_TIMEMAN_DENIED_CONVERTED_EDITABLE"] = "Acceso denegado";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS_CONVERTED_EDITABLE"] = "Acceso completo";
$MESS["TASK_NAME_TIMEMAN_READ_CONVERTED_EDITABLE"] = "Departamento de Recursos Humanos";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE_CONVERTED_EDITABLE"] = "Empleado o supervisor del departamento";
$MESS["TASK_NAME_TIMEMAN_WRITE_CONVERTED_EDITABLE"] = "Administración de la compañía";
$MESS["TIMEMAN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Crear Rol";
$MESS["TIMEMAN_SETTINGS_PERMS_CAN_NOT_EDIT_SYSTEM_TASK"] = "Los permisos de acceso al sistema están disponibles solo para visualización.";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "No se encontró el rol";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Editar el rol: #ROLE#";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_SETTINGS"] = "El usuario puede editar la configuración";
$MESS["TIMEMAN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "No tiene autorización para editar los permisos de acceso";
$MESS["TIMEMAN_SETTINGS_PERMS_TITLE"] = "Permisos de acceso";
$MESS["TIMEMAN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(ID de acceso desconocido)";
?>