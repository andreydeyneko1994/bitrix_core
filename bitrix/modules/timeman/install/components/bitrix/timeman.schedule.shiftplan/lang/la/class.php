<?
$MESS["TIMEMAN_MODULE_NOT_INSTALLED"] = "El módulo <strong>Administración del horario de trabajo</strong> no está instalado.";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM"] = "¿Seguro que desea eliminar a #USER_NAME# del horario? (Todos sus turnos respectivos también se eliminarán de la programación).";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_NO"] = "No";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_TITLE"] = "Quitar la asignación al empleado";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_YES"] = "Sí";
$MESS["TM_SCHEDULE_PLAN_TITLE"] = "#SCHEDULE_NAME# - horario";
$MESS["TM_SCHEDULE_PLAN_TODAY"] = "Hoy";
$MESS["TM_SCHEDULE_PLAN_UNBIND_USER_CONFIRM"] = "¿Seguro que desea quitar la asignación del horario a este empleado?";
$MESS["TM_SCHEDULE_PLAN_USER_ADD"] = "Agregar empleado";
$MESS["TM_SCHEDULE_SHIFT_PLAN_ACCESS_DENIED"] = "Permisos insuficientes.";
$MESS["TM_SCHEDULE_SHIFT_PLAN_SCHEDULE_NOT_FOUND"] = "No se encontró el horario";
$MESS["TM_SHIFT_PLAN_MENU_ADD_SHIFT_TITLE"] = "Agregar turno";
$MESS["TM_SHIFT_PLAN_MENU_DELETE_SHIFT_TITLE"] = "Eliminar turno";
?>