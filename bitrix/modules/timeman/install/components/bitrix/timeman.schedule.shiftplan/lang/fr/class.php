<?
$MESS["TIMEMAN_MODULE_NOT_INSTALLED"] = "Le module <strong>Working Time Management</strong> n'est pas installé.";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM"] = "Voulez-vous vraiment supprimer #USER_NAME# de l'emploi du temps (toutes ses périodes de travail seront également supprimées de l'emploi du temps) ?";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_NO"] = "Non";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_TITLE"] = "Ne plus affecter l'employé";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_YES"] = "Oui";
$MESS["TM_SCHEDULE_PLAN_TITLE"] = "#SCHEDULE_NAME# - emploi du temps";
$MESS["TM_SCHEDULE_PLAN_TODAY"] = "Aujourd'hui";
$MESS["TM_SCHEDULE_PLAN_UNBIND_USER_CONFIRM"] = "Voulez-vous vraiment retirer l'employé de l'emploi du temps ?";
$MESS["TM_SCHEDULE_PLAN_USER_ADD"] = "Ajouter un employé";
$MESS["TM_SCHEDULE_SHIFT_PLAN_ACCESS_DENIED"] = "Permissions insuffisantes.";
$MESS["TM_SCHEDULE_SHIFT_PLAN_SCHEDULE_NOT_FOUND"] = "Le programme est introuvable";
$MESS["TM_SHIFT_PLAN_MENU_ADD_SHIFT_TITLE"] = "Ajouter une période de travail";
$MESS["TM_SHIFT_PLAN_MENU_DELETE_SHIFT_TITLE"] = "Supprimer la période de travail";
?>