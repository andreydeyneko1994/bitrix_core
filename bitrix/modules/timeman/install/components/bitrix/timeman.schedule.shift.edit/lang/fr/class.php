<?
$MESS["TIMEMAN_ERROR_SCHEDULE_NOT_FOUND"] = "L'emploi du temps est introuvable.";
$MESS["TIMEMAN_SHIFT_EDIT_BREAK_DURATION_TITLE"] = "Pause";
$MESS["TIMEMAN_SHIFT_EDIT_BREAK_DURATION_TOTAL_TITLE"] = "total";
$MESS["TIMEMAN_SHIFT_EDIT_BTN_CANCEL_TITLE"] = "Fermer";
$MESS["TIMEMAN_SHIFT_EDIT_BTN_SAVE_TITLE"] = "Enregistrer";
$MESS["TIMEMAN_SHIFT_EDIT_BTN_SET_TITLE"] = "Appliquer";
$MESS["TIMEMAN_SHIFT_EDIT_DEFAULT_SHIFT_NAME"] = "Nom de la période";
$MESS["TIMEMAN_SHIFT_EDIT_ERROR_ACCESS_DENIED"] = "Permissions insuffisantes pour modifier le programme de travail.";
$MESS["TIMEMAN_SHIFT_EDIT_ERROR_SCHEDULE_ID_IS_REQUIRED"] = "Le quart n'a pu être créé sans l'ID de l'emploi du temps.";
$MESS["TIMEMAN_SHIFT_EDIT_ERROR_SHIFT_NOT_FOUND"] = "La période est introuvable";
$MESS["TIMEMAN_SHIFT_EDIT_NAME_TITLE"] = "Nom";
$MESS["TIMEMAN_SHIFT_EDIT_PAGE_TITLE"] = "Éditer la période";
$MESS["TIMEMAN_SHIFT_EDIT_POPUP_FORMAT_HOUR"] = "h";
$MESS["TIMEMAN_SHIFT_EDIT_POPUP_FORMAT_MINUTE"] = "min";
$MESS["TIMEMAN_SHIFT_EDIT_POPUP_PICK_TIME_TITLE"] = "Sélectionner une heure";
$MESS["TIMEMAN_SHIFT_EDIT_POPUP_WORK_TIME_TITLE"] = "Heures de travail";
$MESS["TIMEMAN_SHIFT_EDIT_SHIFT_DURATION_TITLE"] = "Durée";
$MESS["TIMEMAN_SHIFT_EDIT_SHIFT_WORK_TIME_TITLE"] = "Heures de travail";
?>