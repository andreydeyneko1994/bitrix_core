<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NEGATIVE_DURATION"] = "La durée d'un jour de travail ne peut être négative";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_FEMALE"] = "#USER_NAME# a pointé un départ avant celui prévu.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_MALE"] = "#USER_NAME# a pointé un départ avant celui prévu.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_FEMALE"] = "#USER_NAME# a pointé une arrivée avant celle prévue.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_MALE"] = "#USER_NAME# a pointé une arrivée avant celle prévue.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_FEMALE"] = "#USER_NAME# a modifié son temps de travail du #DATE#. <a href=\"#URL#\">Confirmation requise</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_MALE"] = "#USER_NAME# a modifié son temps de travail du #DATE#. <a href=\"#URL#\">Confirmation requise</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_B24TIME"] = "Désolé, vous ne pouvez pas utiliser Bitrix24.Time pour gérer vos heures de travail.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_BROWSER"] = "Désolé, vous ne pouvez pas utiliser le navigateur pour gérer vos heures de travail.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_MOBILE"] = "Désolé, vous ne pouvez pas utiliser les appareils mobiles pour gérer vos heures de travail.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_FEMALE"] = "#USER_NAME# a pointé un départ après celui prévu.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_MALE"] = "#USER_NAME# a pointé un départ après celui prévu.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_FEMALE"] = "#USER_NAME# a commencé sa période de travail après l'heure prévue";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_MALE"] = "#USER_NAME# a commencé sa période de travail après l'heure prévue";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_FEMALE"] = "#USER_NAME# a pointé une arrivée après celle prévue.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_MALE"] = "#USER_NAME# a pointé une arrivée après celle prévue.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_FEMALE"] = "Les heures de travail de #USER_NAME# sont inférieures à ce qui était prévu.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_MALE"] = "Les heures de travail de #USER_NAME# sont inférieures à ce qui était prévu.";
?>