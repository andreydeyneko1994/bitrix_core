<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_CALENDAR_NOT_FOUND"] = "Aucun calendrier des jours fériés n'a été trouvé pour l'emploi du temps";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NOTHING_TO_START"] = "Trop tôt pour pointer";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_OTHER_RECORD_FOR_DATES_EXISTS"] = "L'enregistrement d'heures de travail pour le jour spécifié existe déjà";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_PROHIBITED_ACTION"] = "Impossible d'effectuer cette action";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_REASON_IS_REQUIRED"] = "Le motif de la modification n'est pas indiqué";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_RECORD_EXPIRED_TIME_END_REQUIRED"] = "L'heure de pointage de départ est requis";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SCHEDULE_NOT_FOUND"] = "L'emploi du temps est introuvable.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_NOT_FOUND"] = "La période est introuvable.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_PLAN_NOT_FOUND"] = "Le plan des périodes est introuvable.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_START_GREATER_THAN_NOW"] = "Impossible de pointer le départ plus tard que maintenant";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_VIOLATION_RULES_NOT_FOUND"] = "Les paramètres personnalisés de suivi du programme sont introuvables";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_WORKTIME_RECORD_NOT_FOUND"] = "L'enregistrement des heures de travail est introuvable.";
?>