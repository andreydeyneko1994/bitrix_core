<?
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_FEMALE"] = "#USER_NAME# a manqué le pointage d'arrivée et/ou de départ";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_MALE"] = "#USER_NAME# a manqué le pointage d'arrivée et/ou de départ";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_FEMALE"] = "Les heures de travail de #USER_NAME# sont inférieures à celles prévues par l'exercice comptable";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_MALE"] = "Les heures de travail de #USER_NAME# sont inférieures à celles prévues par l'exercice comptable";
?>