<?
$MESS["TM_SHIFT_FORM_BREAK_DURATION_TITLE"] = "Durée de la pause";
$MESS["TM_SHIFT_FORM_END_TIME_TITLE"] = "La période termine à";
$MESS["TM_SHIFT_FORM_NAME_ERROR"] = "Nom de la période de travail incorrect";
$MESS["TM_SHIFT_FORM_NAME_TITLE"] = "Nom de la période";
$MESS["TM_SHIFT_FORM_NUMBER_INTEGER_ONLY_ERROR"] = "Le champ \"#FIELD_NAME#\" doit être un entier";
$MESS["TM_SHIFT_FORM_NUMBER_LESS_MIN_ERROR"] = "Le champ \"#FIELD_NAME#\" doit être supérieur à #MIN#";
$MESS["TM_SHIFT_FORM_NUMBER_TOO_BIG_ERROR"] = "Le champ \"#FIELD_NAME#\" doit être inférieur à #MAX#";
$MESS["TM_SHIFT_FORM_REQUIRED_ERROR"] = "Le champ \"#FIELD_NAME#\" est requis.";
$MESS["TM_SHIFT_FORM_SCHEDULE_ID_TITLE"] = "ID du programme";
$MESS["TM_SHIFT_FORM_SHIFT_ID_TITLE"] = "ID de la période de travail";
$MESS["TM_SHIFT_FORM_START_TIME_TITLE"] = "La période démarre à";
$MESS["TM_SHIFT_FORM_TIME_FORMATTED_ERROR"] = "Format d'heure incorrect utilisé dans \"#FIELD_NAME#\"";
?>