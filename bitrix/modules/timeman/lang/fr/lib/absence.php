<?
$MESS["TIMEMAN_ABSENCE_CALENDAR_ENTRY_TITLE"] = "Événement du calendrier";
$MESS["TIMEMAN_ABSENCE_EMPTY_INFO"] = "aucune donnée";
$MESS["TIMEMAN_ABSENCE_FORMAT_HOUR"] = "h";
$MESS["TIMEMAN_ABSENCE_FORMAT_LESS_MINUTE"] = "moins d'une minute";
$MESS["TIMEMAN_ABSENCE_FORMAT_MINUTE"] = "min";
$MESS["TIMEMAN_ABSENCE_REPORT_FROM_CALENDAR"] = "Rapport automatique. Événement du calendrier : \"#TITLE#\"";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_LAST_DATE"] = "Dernière mise à jour : #TIME#";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_FIRST_TIME"] = "L'application a démarré pour la première fois.";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_REPORT"] = "Désactivation : #OFFLINE_DATE# #BR# Activation : #ONLINE_DATE# #BR# Durée : #DURATION#";
?>