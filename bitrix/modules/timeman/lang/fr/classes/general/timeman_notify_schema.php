<?
$MESS["TIMEMAN_NS_ENTRY"] = "Ajout d'un rapport de travail";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Confirmation/annulation de la validation du rapport de travail";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Commentaires sur le rapport du travail";
$MESS["TIMEMAN_NS_REPORT"] = "Nouvelles entrées dans le rapport de temps de travail";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Confirmation de la modification d'un enregistrement dans le rapport de temps de travail";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Commentaires sur le rapport de temps de travail";
?>