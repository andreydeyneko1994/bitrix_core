<?
$MESS["TIMEMAN_ABSENCE_CALENDAR_ENTRY_TITLE"] = "Evento del calendario";
$MESS["TIMEMAN_ABSENCE_EMPTY_INFO"] = "sin datos";
$MESS["TIMEMAN_ABSENCE_FORMAT_HOUR"] = "h";
$MESS["TIMEMAN_ABSENCE_FORMAT_LESS_MINUTE"] = "menos de un minuto";
$MESS["TIMEMAN_ABSENCE_FORMAT_MINUTE"] = "m";
$MESS["TIMEMAN_ABSENCE_REPORT_FROM_CALENDAR"] = "Reporte automático. Evento del calendario: \"#TITLE#\"";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_LAST_DATE"] = "Última actualización: #TIME#";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_FIRST_TIME"] = "La aplicación se inició por primera vez.";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_REPORT"] = "Deshabilitado el: #OFFLINE_DATE# #BR# Habilitado el: #ONLINE_DATE# #BR# Duración: #DURATION#";
?>