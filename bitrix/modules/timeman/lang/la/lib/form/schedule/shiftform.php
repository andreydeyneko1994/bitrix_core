<?
$MESS["TM_SHIFT_FORM_BREAK_DURATION_TITLE"] = "Duración del descanso";
$MESS["TM_SHIFT_FORM_END_TIME_TITLE"] = "El turno finaliza a las";
$MESS["TM_SHIFT_FORM_NAME_ERROR"] = "Nombre de turno incorrecto";
$MESS["TM_SHIFT_FORM_NAME_TITLE"] = "Nombre del turno";
$MESS["TM_SHIFT_FORM_NUMBER_INTEGER_ONLY_ERROR"] = "El campo \"#FIELD_NAME#\" debe ser un número entero";
$MESS["TM_SHIFT_FORM_NUMBER_LESS_MIN_ERROR"] = "El campo \"#FIELD_NAME#\" debe ser mayor que #MIN#";
$MESS["TM_SHIFT_FORM_NUMBER_TOO_BIG_ERROR"] = "El campo \"#FIELD_NAME#\" debe ser menor que #MAX#";
$MESS["TM_SHIFT_FORM_REQUIRED_ERROR"] = "El campo \"#FIELD_NAME#\" es obligatorio.";
$MESS["TM_SHIFT_FORM_SCHEDULE_ID_TITLE"] = "ID del horario";
$MESS["TM_SHIFT_FORM_SHIFT_ID_TITLE"] = "ID del turno";
$MESS["TM_SHIFT_FORM_START_TIME_TITLE"] = "El turno comienza a las";
$MESS["TM_SHIFT_FORM_TIME_FORMATTED_ERROR"] = "Formato de hora incorrecto utilizado en \"#FIELD_NAME#\"";
?>