<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_CALENDAR_NOT_FOUND"] = "No se encontró ningún calendario de vacaciones para este horario";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NOTHING_TO_START"] = "Es demasiado temprano para registrar la entrada";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_OTHER_RECORD_FOR_DATES_EXISTS"] = "El registro de las horas de trabajo para el día especificado ya existe";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_PROHIBITED_ACTION"] = "No se puede realizar esta acción";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_REASON_IS_REQUIRED"] = "No se especificó la razón del cambio";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_RECORD_EXPIRED_TIME_END_REQUIRED"] = "Se requiere el horario de salida";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SCHEDULE_NOT_FOUND"] = "No se encontró el horario";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_NOT_FOUND"] = "No se encontró el turno.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_PLAN_NOT_FOUND"] = "No se encontró el plan de turnos.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_START_GREATER_THAN_NOW"] = "No puede registrar el horario de entrada más tarde que ahora";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_VIOLATION_RULES_NOT_FOUND"] = "No se encontró la configuración de seguimiento personalizada del horario";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_WORKTIME_RECORD_NOT_FOUND"] = "No se encontró el registro de las horas de trabajo.";
?>