<?
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_FEMALE"] = "#USER_NAME# no registro su horario de entrada y/o de salida";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_MALE"] = "#USER_NAME# no registro su horario de entrada y/o de salida";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_FEMALE"] = "Las horas de trabajo de #USER_NAME# fueron menos que las que se programaron para el periodo del reporte";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_MALE"] = "Las horas de trabajo de #USER_NAME# fueron menos que las que se programaron para el periodo del reporte";
?>