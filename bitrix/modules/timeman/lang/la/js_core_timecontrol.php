<?
$MESS["JS_CORE_TC_ABSENCE_END"] = "Final: #DATE#";
$MESS["JS_CORE_TC_ABSENCE_PRIVATE_F"] = "Asuntos personales";
$MESS["JS_CORE_TC_ABSENCE_PRIVATE_M"] = "Asuntos personales";
$MESS["JS_CORE_TC_ABSENCE_START"] = "Inicio: #DATE#";
$MESS["JS_CORE_TC_ABSENCE_WORK_F"] = "Asuntos de negocios";
$MESS["JS_CORE_TC_ABSENCE_WORK_M"] = "Asuntos de negocios";
$MESS["JS_CORE_TC_CONFIRM_CLOSE"] = "Cerrar";
$MESS["JS_CORE_TC_DIALOG_CLOSE"] = "cerrar";
$MESS["JS_CORE_TC_HOURS_0"] = "#NUMBER# hora";
$MESS["JS_CORE_TC_HOURS_1"] = "#NUMBER# horas";
$MESS["JS_CORE_TC_HOURS_2"] = "#NUMBER# horas";
$MESS["JS_CORE_TC_MESSAGE_LINE_1"] = "Me doy cuenta de que has estado lejos por #TIME#";
$MESS["JS_CORE_TC_MESSAGE_LINE_2"] = "¿Qué has estado haciendo?";
$MESS["JS_CORE_TC_MINUTES_0"] = "#NUMBER# minuto";
$MESS["JS_CORE_TC_MINUTES_1"] = "#NUMBER# minutos";
$MESS["JS_CORE_TC_MINUTES_2"] = "#NUMBER# minutos";
$MESS["JS_CORE_TC_MINUTES_ZERO"] = "menos de un minuto";
$MESS["JS_CORE_TC_SAVE_TO_CALENDAR"] = "Enviar ausencia al calendario";
$MESS["JS_CORE_TC_SEND_ERROR"] = "Error al conectar con Bitrix24. Por favor, compruebe la conexión de red.";
$MESS["JS_CORE_TC_SEND_FORM"] = "Guardar";
$MESS["JS_CORE_TC_TEXTAREA_HELP"] = "Especifique el motivo de su ausencia";
$MESS["JS_CORE_TC_TITLE"] = "Su Asistente Personal";
?>