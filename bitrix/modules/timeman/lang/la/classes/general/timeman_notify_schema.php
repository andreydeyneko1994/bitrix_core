<?
$MESS["TIMEMAN_NS_ENTRY"] = "Nuevo reporte de trabajo";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Reporte de trabajo aprobado o no aprobado";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Nuevos comentarios al reporte de trabajo";
$MESS["TIMEMAN_NS_REPORT"] = "Nueva entrada en el reporte de tiempo de trabajo";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Actualización del reporte de tiempo de trabajo confirmada";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Comentarios del reporte de tiempo de trabajo";
?>