<?
$MESS["OP_DESC_TM_MANAGE"] = "Osobista kontrola czasu pracy";
$MESS["OP_DESC_TM_MANAGE_ALL"] = "Kogokolwiek kontrola czasu pracy";
$MESS["OP_DESC_TM_READ"] = "Dostęp do wszystkich raportów";
$MESS["OP_DESC_TM_READ_SUBORDINATE"] = "Podporządkowany kontrolowany dostęp do raportów";
$MESS["OP_DESC_TM_SETTINGS"] = "Edytuj ustawienia modułu";
$MESS["OP_DESC_TM_WRITE"] = "Każdy rekord edytować i Potwierdźić dostęp";
$MESS["OP_DESC_TM_WRITE_SUBORDINATE"] = "Podporządkowania kontrolowane edycja i Potwierdź dostęp";
$MESS["OP_NAME_TM_MANAGE"] = "Zarejestrowania i wyrejestrowania";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Ktoś jest zarejestrowany i wyrejestrowany";
$MESS["OP_NAME_TM_READ"] = "Przeczytaj wszystkie rejestry";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Podporządkowania kontrolowanego odczytu";
$MESS["OP_NAME_TM_SETTINGS"] = "Edytuj ustawienia modułu";
$MESS["OP_NAME_TM_WRITE"] = "Edytuj wszystkie rejestry";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Edytuj podporządkowania kontrolowane";
?>