<?
$MESS["TASK_DESC_TIMEMAN_DENIED"] = "Odmowa dostępu do zarządzania czasem pracy";
$MESS["TASK_DESC_TIMEMAN_FULL_ACCESS"] = "Pełny dostęp do zarządzania czasem pracy";
$MESS["TASK_DESC_TIMEMAN_READ"] = "Pełny dostęp do odczytu, podporządkowany kontrolowany dostęp do zapisu";
$MESS["TASK_DESC_TIMEMAN_SUBORDINATE"] = "Podporządkowania kontrolą dostępu";
$MESS["TASK_DESC_TIMEMAN_WRITE"] = "Pełen odczyt/Zapis dostępu";
$MESS["TASK_NAME_TIMEMAN_DENIED"] = "Odmowa dostępu";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS"] = "Pełny dostęp";
$MESS["TASK_NAME_TIMEMAN_READ"] = "Dział HR";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE"] = "Kierownik działu lub pracownik";
$MESS["TASK_NAME_TIMEMAN_WRITE"] = "Zarząd firmy";
?>