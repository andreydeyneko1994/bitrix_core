<?
$MESS["TM_SHIFT_FORM_BREAK_DURATION_TITLE"] = "Czas trwania przerwy";
$MESS["TM_SHIFT_FORM_END_TIME_TITLE"] = "Zmiana kończy się o";
$MESS["TM_SHIFT_FORM_NAME_ERROR"] = "Nieprawidłowa nazwa zmiany";
$MESS["TM_SHIFT_FORM_NAME_TITLE"] = "Nazwa zmiany";
$MESS["TM_SHIFT_FORM_NUMBER_INTEGER_ONLY_ERROR"] = "Pole „#FIELD_NAME#” musi zawierać liczbę całkowitą";
$MESS["TM_SHIFT_FORM_NUMBER_LESS_MIN_ERROR"] = "Pole „#FIELD_NAME#” musi zawierać liczbę większą niż #MIN#";
$MESS["TM_SHIFT_FORM_NUMBER_TOO_BIG_ERROR"] = "Pole „#FIELD_NAME#” musi zawierać liczbę większą niż #MAX#";
$MESS["TM_SHIFT_FORM_REQUIRED_ERROR"] = "Pole „#FIELD_NAME#” jest wymagane.";
$MESS["TM_SHIFT_FORM_SCHEDULE_ID_TITLE"] = "ID harmonogramu";
$MESS["TM_SHIFT_FORM_SHIFT_ID_TITLE"] = "ID zmiany";
$MESS["TM_SHIFT_FORM_START_TIME_TITLE"] = "Shift zaczyna się o";
$MESS["TM_SHIFT_FORM_TIME_FORMATTED_ERROR"] = "Nieprawidłowy format godziny w „#FIELD_NAME#”";
?>