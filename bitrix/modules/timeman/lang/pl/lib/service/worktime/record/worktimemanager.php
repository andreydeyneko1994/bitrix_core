<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NEGATIVE_DURATION"] = "Długość dnia roboczego nie może być ujemna";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_FEMALE"] = "#USER_NAME# — wyrejestrowano wcześniej niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_MALE"] = "#USER_NAME# — wyrejestrowano wcześniej niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_FEMALE"] = "#USER_NAME# — zarejestrowano wcześniej niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_MALE"] = "#USER_NAME# — zarejestrowano wcześniej niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_FEMALE"] = "#USER_NAME# zmienił czas pracy w dniu #DATE#. <a href=\"#URL#\">Wymaga potwierdzenia</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_MALE"] = "#USER_NAME# zmienił czas pracy w dniu #DATE#. <a href=\"#URL#\">Wymaga potwierdzenia</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_B24TIME"] = "Niestety, nie możesz używać Bitrix24.Time do zarządzania godzinami pracy.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_BROWSER"] = "Niestety, nie możesz używać przeglądarki do zarządzania godzinami pracy.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_MOBILE"] = "Niestety, nie możesz używać urządzeń mobilnych do zarządzania godzinami pracy.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_FEMALE"] = "#USER_NAME# — wyrejestrowano później niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_MALE"] = "#USER_NAME# — wyrejestrowano później niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_FEMALE"] = "#USER_NAME# rozpoczęła zmianę później niż przewiduje harmonogram";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_MALE"] = "#USER_NAME# rozpoczął zmianę później niż przewiduje harmonogram";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_FEMALE"] = "#USER_NAME# — zarejestrowano później niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_MALE"] = "#USER_NAME# — zarejestrowano później niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_FEMALE"] = "#USER_NAME# ma mniej godzin pracy niż przewiduje harmonogram.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_MALE"] = "#USER_NAME# ma mniej godzin pracy niż przewiduje harmonogram.";
?>