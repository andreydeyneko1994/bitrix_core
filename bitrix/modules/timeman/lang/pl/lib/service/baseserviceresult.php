<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_CALENDAR_NOT_FOUND"] = "Nie znaleziono kalendarza świąt dotyczącego harmonogramu";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NOTHING_TO_START"] = "Za wcześnie na wyrejestrowanie";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_OTHER_RECORD_FOR_DATES_EXISTS"] = "Rekord godzin pracy dla określonego dnia już istnieje";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_PROHIBITED_ACTION"] = "Nie można wykonać tego działania";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_REASON_IS_REQUIRED"] = "Nie określono powodu zmiany";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_RECORD_EXPIRED_TIME_END_REQUIRED"] = "Wymagana godzina wyrejestrowania";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SCHEDULE_NOT_FOUND"] = "Nie znaleziono harmonogramu";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_NOT_FOUND"] = "Nie znaleziono zmiany.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_PLAN_NOT_FOUND"] = "Nie znaleziono planu zmian.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_START_GREATER_THAN_NOW"] = "Nie możesz zarejestrować się później niż teraz";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_VIOLATION_RULES_NOT_FOUND"] = "Nie znaleziono niestandardowych ustawień śledzenia harmonogramu";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_WORKTIME_RECORD_NOT_FOUND"] = "Nie znaleziono rekordu godzin pracy.";
?>