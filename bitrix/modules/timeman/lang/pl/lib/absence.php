<?
$MESS["TIMEMAN_ABSENCE_CALENDAR_ENTRY_TITLE"] = "Wydarzenie w kalendarzu";
$MESS["TIMEMAN_ABSENCE_EMPTY_INFO"] = "brak danych";
$MESS["TIMEMAN_ABSENCE_FORMAT_HOUR"] = "h";
$MESS["TIMEMAN_ABSENCE_FORMAT_LESS_MINUTE"] = "poniżej minuty";
$MESS["TIMEMAN_ABSENCE_FORMAT_MINUTE"] = "m";
$MESS["TIMEMAN_ABSENCE_REPORT_FROM_CALENDAR"] = "Automatyczny raport. Wydarzenie w kalendarzu: „#TITLE#”";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_LAST_DATE"] = "Ostatnia aktualizacja: #TIME#";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_FIRST_TIME"] = "Aplikacja została uruchomiona po raz pierwszy.";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_REPORT"] = "Wyłączono: #OFFLINE_DATE# #BR# Włączono: #ONLINE_DATE# #BR# Czas trwania: #DURATION#";
?>