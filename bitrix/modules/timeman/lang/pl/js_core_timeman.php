<?php
$MESS["JS_CORE_CL"] = "Wybierz czas";
$MESS["JS_CORE_EMPTYTPL"] = "(pusty szablon)";
$MESS["JS_CORE_HINT_EVENTS"] = "Nadchodzące wydarzenie";
$MESS["JS_CORE_HINT_STATE"] = "Bieżący czas; czas trwania dzisiejszego dnia roboczego";
$MESS["JS_CORE_HINT_TASKS"] = "Dzisiejsze zadania oczekujące";
$MESS["JS_CORE_HOST"] = "(właściciel)";
$MESS["JS_CORE_PAUSE_NOT_FINISHED"] = "przerwa się jeszcze nie skończyła.";
$MESS["JS_CORE_SS_EVENT_SETUP"] = "Konfiguruj";
$MESS["JS_CORE_SS_SEND_ERROR"] = "nie wysłano";
$MESS["JS_CORE_SS_SEND_SUCCESS"] = "Zakończone Pomyślnie";
$MESS["JS_CORE_SS_SEND_TO_END"] = "Wyślij w clock-out";
$MESS["JS_CORE_SS_SEND_TO_SOCSERV"] = "Podziel się w sieci społecznościowej";
$MESS["JS_CORE_SS_SEND_TO_START"] = "Wyślij w clock-in";
$MESS["JS_CORE_SS_WORKDAY_END"] = "Zakończyłem mój dzień pracy z #bitrix24. Wykonane: #task# zadania, #event# spotkania.";
$MESS["JS_CORE_SS_WORKDAY_START"] = "Zacząłem mój dzień pracy z #bitrix24.";
$MESS["JS_CORE_TM"] = "Zarządzanie czasem pracy";
$MESS["JS_CORE_TMR_A"] = "Zmienione";
$MESS["JS_CORE_TMR_ACCEPT_DATE"] = "Data zatwierdzenia";
$MESS["JS_CORE_TMR_ADD_COMMENT"] = "Dodaj komentarz";
$MESS["JS_CORE_TMR_ADMIN"] = "Zmienione przez nadzorce";
$MESS["JS_CORE_TMR_APPROVING_REPORT"] = "Zatwierdź raport";
$MESS["JS_CORE_TMR_DELAY_WEEKLY"] = "Przełożyć o godzinę";
$MESS["JS_CORE_TMR_DURATION"] = "Czas trwania";
$MESS["JS_CORE_TMR_FROM"] = "od";
$MESS["JS_CORE_TMR_INHERIT"] = "(Dziedziczy ustawienia działu)";
$MESS["JS_CORE_TMR_INHERIT_T"] = "Użyj parametrów działu";
$MESS["JS_CORE_TMR_MARK"] = "Wynik raportu";
$MESS["JS_CORE_TMR_MARK_B"] = "Negative";
$MESS["JS_CORE_TMR_MARK_B_W"] = "Ocena negatywna";
$MESS["JS_CORE_TMR_MARK_G"] = "Pozytywna";
$MESS["JS_CORE_TMR_MARK_G_W"] = "Ocena pozytywna";
$MESS["JS_CORE_TMR_MARK_N"] = "brak wyniku";
$MESS["JS_CORE_TMR_MARK_N_W"] = "brak wyniku";
$MESS["JS_CORE_TMR_MARK_X"] = "Niezatwierdzony";
$MESS["JS_CORE_TMR_NA"] = "Niepotwierdzone";
$MESS["JS_CORE_TMR_NOT_ACCEPT"] = "Raport niepotwierdzony";
$MESS["JS_CORE_TMR_OFF"] = "Wyłączony";
$MESS["JS_CORE_TMR_ON"] = "Włączony";
$MESS["JS_CORE_TMR_OVERDUE_REPORT"] = "Raport jest ze względu na następny okres czasu:";
$MESS["JS_CORE_TMR_PARENT_EDIT"] = "Edytuj";
$MESS["JS_CORE_TMR_PARENT_SETTINGS"] = "Przejmij ustawienia w działu";
$MESS["JS_CORE_TMR_PAUSE"] = "Przerwa";
$MESS["JS_CORE_TMR_PLAN"] = "Plan";
$MESS["JS_CORE_TMR_PLAN_EMPTY"] = "Brak tekstu planu";
$MESS["JS_CORE_TMR_REPORT"] = "Raport";
$MESS["JS_CORE_TMR_REPORT_APPROVE"] = "Raport potwierdzony";
$MESS["JS_CORE_TMR_REPORT_APPROVER"] = "Zatwierdzony przez";
$MESS["JS_CORE_TMR_REPORT_DURATION"] = "Powód zmiany (czas trwania)";
$MESS["JS_CORE_TMR_REPORT_EMPTY"] = "Brak tekstu raportu";
$MESS["JS_CORE_TMR_REPORT_FINISH"] = "Powód zmiany (koniec czasu)";
$MESS["JS_CORE_TMR_REPORT_FULL_DAY"] = "Dzienny raport czasu pracy";
$MESS["JS_CORE_TMR_REPORT_FULL_MONTH"] = "raport miesięczny";
$MESS["JS_CORE_TMR_REPORT_FULL_WEEK"] = "raport tygodniowy";
$MESS["JS_CORE_TMR_REPORT_INC"] = "uwzględnić w raporcie";
$MESS["JS_CORE_TMR_REPORT_ORIG"] = "Oryginalne";
$MESS["JS_CORE_TMR_REPORT_START"] = "Powód zmiany (początek czasu)";
$MESS["JS_CORE_TMR_REPORT_TPL"] = "Szablony";
$MESS["JS_CORE_TMR_REPORT_WEEKLY"] = "raport pracy";
$MESS["JS_CORE_TMR_SEND_COMMENT"] = "Wyślij";
$MESS["JS_CORE_TMR_SN"] = "Proszę włączyć zarządzanie czasem pracy do edytowania tych wartości.";
$MESS["JS_CORE_TMR_SONET_LOG_B"] = "raport potwierdzenie, oznaczone <span class=\"tm-mark-log-B\">negatywnie</span>";
$MESS["JS_CORE_TMR_SONET_LOG_G"] = "raport potwierdzenie, oznaczone <span class=\"tm-mark-log-G\">pozytywnie</span>";
$MESS["JS_CORE_TMR_SONET_LOG_N"] = "raport potwierdzenie, <span class=\"tm-mark-log-N\">bez oznaczeń</span>";
$MESS["JS_CORE_TMR_SONET_LOG_X"] = "raport <span class=\"tm-mark-log-X\">nie potwierdzone</span>";
$MESS["JS_CORE_TMR_SUBMIT_WEEKLY"] = "Wyślij do przełożonego";
$MESS["JS_CORE_TMR_TITLE"] = "Dzienny raport czasu pracy";
$MESS["JS_CORE_TMR_TITLE_HINT"] = "strefa czasowa pracownika: <b>#TIME_OFFSET#</b><br /><small>(twoja strefa czasowa: #TIME_OFFSET_SELF#)</small><br /><br />zarejestrowany z IP:<br /><b>#IP_OPEN#</b><br />wyrejestrowany z IP:<br /><b>#IP_CLOSE#</b>";
$MESS["JS_CORE_TMR_TO"] = "do";
$MESS["JS_CORE_TMR_WORKTIME"] = "Czas pracy";
$MESS["JS_CORE_TM_ARR"] = "Początek";
$MESS["JS_CORE_TM_ARRIVAL"] = "Rozpoczęcie dnia pracy";
$MESS["JS_CORE_TM_B_ADD"] = "Dodawanie";
$MESS["JS_CORE_TM_B_CLOSE"] = "Zamknij";
$MESS["JS_CORE_TM_B_SAVE"] = "Zapisz";
$MESS["JS_CORE_TM_B_SAVING"] = "Zapisywanie…";
$MESS["JS_CORE_TM_CHTIME"] = "zmień czas";
$MESS["JS_CORE_TM_CHTIME_CAUSE"] = "Powód";
$MESS["JS_CORE_TM_CHTIME_CLOSED"] = "Zmiana godziny zarejestrowania";
$MESS["JS_CORE_TM_CHTIME_DAY"] = "Edytuj godziny pracy";
$MESS["JS_CORE_TM_CHTIME_EXPIRED"] = "Godzina wyrejestrowania z poprzedniego dnia";
$MESS["JS_CORE_TM_CHTIME_OPENED"] = "Zmiana godziny wyrejestrowania";
$MESS["JS_CORE_TM_CLOSE"] = "Wyrejestruj";
$MESS["JS_CORE_TM_CLOSED"] = "Dzień pracy zakończony";
$MESS["JS_CORE_TM_CONFIRM"] = "Potwierdź";
$MESS["JS_CORE_TM_CONFIRM_TO_DELETE"] = "Usunąć plik z raportu?";
$MESS["JS_CORE_TM_DEP"] = "Koniec";
$MESS["JS_CORE_TM_ERROR"] = "<b>Warning!</b> Wystąpił błąd w module zarządzanie czasem.";
$MESS["JS_CORE_TM_EVENTS"] = "Wydarzenia";
$MESS["JS_CORE_TM_EVENTS_ADD"] = "Nowe wydarzenie";
$MESS["JS_CORE_TM_EVENT_ABSENT"] = "poza biurem";
$MESS["JS_CORE_TM_EVENT_SET"] = "ustaw";
$MESS["JS_CORE_TM_FILES"] = "Pliki";
$MESS["JS_CORE_TM_OPEN"] = "Start";
$MESS["JS_CORE_TM_PAUSE"] = "Przerwa";
$MESS["JS_CORE_TM_PLAN"] = "plan dzienny";
$MESS["JS_CORE_TM_POPUP_HIDE"] = "Ukryj";
$MESS["JS_CORE_TM_POPUP_OPEN"] = "dzień roboczy:";
$MESS["JS_CORE_TM_REM"] = "Nie pytaj ponownie";
$MESS["JS_CORE_TM_REOPEN"] = "Kontynuuj dzień pracy";
$MESS["JS_CORE_TM_REPORT"] = "dzienne podsumowanie";
$MESS["JS_CORE_TM_REPORT_PH"] = "Skomponuj krótki raport o swojej pracy";
$MESS["JS_CORE_TM_RESTRICTION_TITLE"] = "Ta opcja jest dostępna tylko częściowo na twoim bieżącym planie.";
$MESS["JS_CORE_TM_TASKS"] = "Zadania";
$MESS["JS_CORE_TM_TASKS_ADD"] = "Wprowadź tekst zadania";
$MESS["JS_CORE_TM_TASKS_CHOOSE"] = "Wybierz z listy";
$MESS["JS_CORE_TM_TIME_NOW"] = "Teraz";
$MESS["JS_CORE_TM_UNPAUSE"] = "Kontynuuj";
$MESS["JS_CORE_TM_UPLOAD_FILES"] = "Dodaj plik";
$MESS["JS_CORE_TM_WD"] = "dzień roboczy";
$MESS["JS_CORE_TM_WD_CLOCK_SET_CUSTOM_DATE"] = "Zmień dzień";
$MESS["JS_CORE_TM_WD_CLOSED"] = "Uwaga! Dzień pracy nie został rozpoczęty.";
$MESS["JS_CORE_TM_WD_EXPIRED"] = "Nie wyrejestrowałeś się jeszcze";
$MESS["JS_CORE_TM_WD_OPENED"] = "Czas trwania dnia roboczego:";
$MESS["JS_CORE_TM_WD_PAUSED"] = "Przerwa:";
$MESS["JS_CORE_TM_WD_PAUSED_1"] = "Przerwa";
