<?
$MESS["TM_FIELD_UF_DELAY_TIME"] = "Czas odroczonego raportu";
$MESS["TM_FIELD_UF_LAST_REPORT_DATE"] = "Data ostatniego raportu";
$MESS["TM_FIELD_UF_NONE"] = "Raport nie jest wymagany";
$MESS["TM_FIELD_UF_REPORT_PERIOD"] = "Okres raportowania";
$MESS["TM_FIELD_UF_SETTING_DATE"] = "Ustawienia zapisane na";
$MESS["TM_FIELD_UF_TIMEMAN"] = "Zarządzanie czasem pracy";
$MESS["TM_FIELD_UF_TIMEMAN_N"] = "Wyłączone";
$MESS["TM_FIELD_UF_TIMEMAN_Y"] = "Włączone";
$MESS["TM_FIELD_UF_TM_ALLOWED_DELTA"] = "Dopuszczalny czas dopasowania odstępu";
$MESS["TM_FIELD_UF_TM_DAY"] = "Dzień";
$MESS["TM_FIELD_UF_TM_FREE"] = "Elastyczny harmonogram pracy";
$MESS["TM_FIELD_UF_TM_FREE_N"] = "Wyłączone";
$MESS["TM_FIELD_UF_TM_FREE_Y"] = "Włączone";
$MESS["TM_FIELD_UF_TM_MAX_START"] = "Najpóźniejszy czas zarejestrowania";
$MESS["TM_FIELD_UF_TM_MIN_DURATION"] = "Czas minimalny dnia roboczego";
$MESS["TM_FIELD_UF_TM_MIN_FINISH"] = "Najwcześniejszy czas wyrejestowania";
$MESS["TM_FIELD_UF_TM_MONTH"] = "Miesiąc";
$MESS["TM_FIELD_UF_TM_NOVALUE"] = "(dziedziczenie)";
$MESS["TM_FIELD_UF_TM_REPORT_DATE"] = "Dzień miesiąca";
$MESS["TM_FIELD_UF_TM_REPORT_REQ"] = "Dzienny raport czasu pracy";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_A"] = "Nie pokazuj formularza raportu";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_N"] = "Nie wymagany";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_Y"] = "Wymagany";
$MESS["TM_FIELD_UF_TM_REPORT_TPL"] = "Szablony raportów";
$MESS["TM_FIELD_UF_TM_TIME"] = "Raport spowodowany przez";
$MESS["TM_FIELD_UF_TM_WEEK"] = "Tydzień";
?>