<?
$MESS["TIMEMAN_NS_ENTRY"] = "Nowy raport z dnia pracy";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Raport dnia roboczego zatwierdzony lub niezatwierdzony";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Komentarz nowego raportu z dnia pracy";
$MESS["TIMEMAN_NS_REPORT"] = "Nowy raport czasu pracy";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Zatwierdzenie aktualizacji raportu czasu pracy";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Komentarz do raportu dnia pracy";
?>