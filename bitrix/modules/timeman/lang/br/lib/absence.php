<?
$MESS["TIMEMAN_ABSENCE_CALENDAR_ENTRY_TITLE"] = "Evento do calendário";
$MESS["TIMEMAN_ABSENCE_EMPTY_INFO"] = "nenhum dado";
$MESS["TIMEMAN_ABSENCE_FORMAT_HOUR"] = "h";
$MESS["TIMEMAN_ABSENCE_FORMAT_LESS_MINUTE"] = "menos de um minuto";
$MESS["TIMEMAN_ABSENCE_FORMAT_MINUTE"] = "m";
$MESS["TIMEMAN_ABSENCE_REPORT_FROM_CALENDAR"] = "Relatório automático. Evento de calendário: \"#TITLE#\"";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_LAST_DATE"] = "Última atualização: #TIME#";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_FIRST_TIME"] = "O aplicativo foi iniciado pela primeira vez.";
$MESS["TIMEMAN_ABSENCE_TEXT_DESKTOP_ONLINE_REPORT"] = "Desativado em: #OFFLINE_DATE# #BR# Ativado em: #ONLINE_DATE# #BR# Duração: #DURATION#";
?>