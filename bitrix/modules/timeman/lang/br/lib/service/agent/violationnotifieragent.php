<?
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_FEMALE"] = "#USER_NAME# não marcou a entrada e/ou a saída";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_MISSED_SHIFT_NOTIFICATION_MALE"] = "#USER_NAME# não marcou a entrada e/ou a saída";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_FEMALE"] = "As horas de trabalho de #USER_NAME# foram a menos do que o programado para o período do relatório";
$MESS["TM_VIOLATION_NOTIFIER_AGENT_PERIOD_TIME_LACK_MALE"] = "As horas de trabalho de #USER_NAME# foram a menos do que o programado para o período do relatório";
?>