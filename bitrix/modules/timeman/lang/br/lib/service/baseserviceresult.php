<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_CALENDAR_NOT_FOUND"] = "Não foi encontrado calendário de feriados para a agenda";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NOTHING_TO_START"] = "Muito cedo para marcar a entrada";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_OTHER_RECORD_FOR_DATES_EXISTS"] = "O registro de horas de trabalho para o dia especificado já existe";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_PROHIBITED_ACTION"] = "Não é possível executar esta ação";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_REASON_IS_REQUIRED"] = "Motivo da alteração não especificado";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_RECORD_EXPIRED_TIME_END_REQUIRED"] = "O horário de saída é obrigatório";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SCHEDULE_NOT_FOUND"] = "A agenda não foi encontrada";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_NOT_FOUND"] = "O turno não foi encontrado.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_PLAN_NOT_FOUND"] = "O plano de turno não foi encontrado.";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_START_GREATER_THAN_NOW"] = "Não é possível registra a entrada mais tarde do que agora";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_VIOLATION_RULES_NOT_FOUND"] = "As configurações personalizadas de rastreamento de horários não foram encontradas";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_WORKTIME_RECORD_NOT_FOUND"] = "O registro de horas de trabalho não foi encontrado.";
?>