<?
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NEGATIVE_DURATION"] = "A duração do dia útil não pode ser negativa";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_FEMALE"] = "#USER_NAME# marcou a saída antes do programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_END_MALE"] = "#USER_NAME# marcou a saída antes do programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_FEMALE"] = "#USER_NAME# marcou a entrada antes do programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EARLY_START_MALE"] = "#USER_NAME# marcou a entrada antes do programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_FEMALE"] = "#USER_NAME# alterou seu horário de trabalho para #DATE#. <a href=\"#URL#\">A confirmação é obrigatória</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_EDIT_WITH_URL_MALE"] = "#USER_NAME# alterou seu horário de trabalho para #DATE#. <a href=\"#URL#\">A confirmação é obrigatória</a>";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_B24TIME"] = "Desculpe, você não pode usar o Bitrix24.Time para gerenciar suas horas de trabalho.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_BROWSER"] = "Desculpe, você não pode usar o navegador para gerenciar suas horas de trabalho.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_FORBIDDEN_DEVICE_MOBILE"] = "Desculpe, você não pode usar dispositivos móveis para gerenciar suas horas de trabalho.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_FEMALE"] = "#USER_NAME# marcou a saída mais tarde do que o programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_END_MALE"] = "#USER_NAME# marcou a saída mais tarde do que o programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_FEMALE"] = "#USER_NAME# marcou seu turno mais tarde do que o programado";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_SHIFT_START_MALE"] = "#USER_NAME# começou seu turno mais tarde do que o programado";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_FEMALE"] = "#USER_NAME# marcou a entrada mais tarde do que o programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_LATE_START_MALE"] = "#USER_NAME# marcou a entrada mais tarde do que o programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_FEMALE"] = "As horas de trabalho de #USER_NAME# foram a menos do que o programado.";
$MESS["TM_VIOLATION_WORKTIME_MANAGER_MIN_DAY_DURATION_MALE"] = "As horas de trabalho de #USER_NAME# foram a menos do que o programado.";
?>