<?
$MESS["TM_SHIFT_FORM_BREAK_DURATION_TITLE"] = "Duração do intervalo";
$MESS["TM_SHIFT_FORM_END_TIME_TITLE"] = "O turno termina às";
$MESS["TM_SHIFT_FORM_NAME_ERROR"] = "Nome de turno incorreto";
$MESS["TM_SHIFT_FORM_NAME_TITLE"] = "Nome do turno";
$MESS["TM_SHIFT_FORM_NUMBER_INTEGER_ONLY_ERROR"] = "O campo \"#FIELD_NAME#\" deve ser um número inteiro";
$MESS["TM_SHIFT_FORM_NUMBER_LESS_MIN_ERROR"] = "O campo \"#FIELD_NAME#\" deve ser maior do que #MIN#";
$MESS["TM_SHIFT_FORM_NUMBER_TOO_BIG_ERROR"] = "O campo \"#FIELD_NAME#\" deve ser menor do que #MAX#";
$MESS["TM_SHIFT_FORM_REQUIRED_ERROR"] = "O campo \"#FIELD_NAME#\" é obrigatório.";
$MESS["TM_SHIFT_FORM_SCHEDULE_ID_TITLE"] = "ID da agenda";
$MESS["TM_SHIFT_FORM_SHIFT_ID_TITLE"] = "ID do turno";
$MESS["TM_SHIFT_FORM_START_TIME_TITLE"] = "O turno começa às";
$MESS["TM_SHIFT_FORM_TIME_FORMATTED_ERROR"] = "Formato de horário incorreto usado em \"#FIELD_NAME#\"";
?>