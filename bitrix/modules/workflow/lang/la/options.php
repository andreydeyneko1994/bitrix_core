<?
$MESS["FLOW_ADMIN"] = "Grupo de los administradores del Workflow: ";
$MESS["FLOW_CLEAR"] = "limpiar";
$MESS["FLOW_DAYS_AFTER_PUBLISHING"] = "Cuantos días se guardará el documento despues de publicarlo (<0 - siempre):";
$MESS["FLOW_HISTORY_COPIES"] = "Número máximo de <i>últimos registros</i> en el historial:";
$MESS["FLOW_HISTORY_DAYS"] = "Cuantos días de guardará la historia(<0 - siempre):";
$MESS["FLOW_HISTORY_SIMPLE_EDITING"] = "Almacenar historial de cambios en el archivo durante la edición común (no en módulo del workflow)";
$MESS["FLOW_MAX_LOCK"] = "Máximo tiempo de observación (min.):";
$MESS["FLOW_RESET"] = "Resetear";
$MESS["FLOW_SAVE"] = "Guardar";
$MESS["FLOW_USE_HTML_EDIT"] = "Use el editor HTML (sólo para IE 5.0 o superior):";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Configuración por defecto";
?>