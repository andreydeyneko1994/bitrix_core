<?
$MESS["FLOW_DENIED"] = "denegado";
$MESS["FLOW_INSTALL_DRAFT"] = "Borrador";
$MESS["FLOW_INSTALL_PUBLISHED"] = "Publicación";
$MESS["FLOW_INSTALL_READY"] = "Listo";
$MESS["FLOW_INSTALL_TITLE"] = "Instalación del módulo del workflow";
$MESS["FLOW_MODIFY"] = "editar documentos en el workflow";
$MESS["FLOW_MODULE_DESCRIPTION"] = "Módulo para organizar y procesar los documentos del sitio web.";
$MESS["FLOW_MODULE_NAME"] = "Flujo de trabajo";
$MESS["FLOW_READ"] = "sólo-lectura";
$MESS["FLOW_WRITE"] = "acceso total";
?>