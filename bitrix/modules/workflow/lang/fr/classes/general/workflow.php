<?
$MESS["FLOW_ACCESS_DENIED_FOLDER"] = "Vous n'êtes pas autorisé à écrire à #FILENAME#.";
$MESS["FLOW_ACCESS_DENIED_FOR_FILE_WRITE"] = "Vous n'êtes pas autorisé à créer le fichier '#FILENAME#' (vous devriez avoir accès 'Écrire' pour ce fichier ou répertoire).";
$MESS["FLOW_ACCESS_DENIED_PHP_VIEW"] = "Vous n'êtes pas autorisé à afficher les fichiers exécutables.";
$MESS["FLOW_CAN_NOT_WRITE_FILE"] = "Vous ne pouvez pas enregistrer le fichier '#FILENAME#'.";
$MESS["FLOW_CURRENT_STATUS"] = "Statut courant : ";
$MESS["FLOW_DOCUMENT_NOT_PUBLISHED"] = "Le document ne peut pas être publié.";
$MESS["FLOW_ERROR"] = "Erreur !";
$MESS["FLOW_PANEL_CREATE_ALT"] = "Créer une nouvelle page dans le dossier courant via le workflow";
$MESS["FLOW_PANEL_CREATE_WITH_WF"] = "Via le flux de travaux";
$MESS["FLOW_PANEL_EDIT_ALT"] = "Modifier la page actuelle via le workflow";
$MESS["FLOW_PANEL_EDIT_WITH_WF"] = "Via le flux de travaux";
$MESS["FLOW_PANEL_HISTORY"] = "Historique";
$MESS["FLOW_PANEL_HISTORY_ALT"] = "Historique des modifications pour la page courante";
?>