<?
$MESS["FLOW_MENU_DOCUMENTS"] = "Documents";
$MESS["FLOW_MENU_DOCUMENTS_ALT"] = "Documents dans le traitement de flux de travail";
$MESS["FLOW_MENU_HISTORY"] = "Historique";
$MESS["FLOW_MENU_HISTORY_ALT"] = "Document de changer l'histoire";
$MESS["FLOW_MENU_MAIN"] = "Flux de documents";
$MESS["FLOW_MENU_MAIN_TITLE"] = "Gestion de documents";
$MESS["FLOW_MENU_STAGE"] = "Statuts";
$MESS["FLOW_MENU_STAGE_ALT"] = "Les états possibles pour le document";
?>