<?
$MESS["FLOW_ACTIVE"] = "Actif(ve) : ";
$MESS["FLOW_DELETE_STATUS"] = "Supprimer état actuel";
$MESS["FLOW_DELETE_STATUS_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce statut ?";
$MESS["FLOW_DESCRIPTION"] = "Description";
$MESS["FLOW_DOCUMENTS"] = "Documents à ce statut : ";
$MESS["FLOW_DOCUMENTS_ALT"] = "Voir les documents dans ce statut";
$MESS["FLOW_EDIT_RECORD"] = "Préférences de statut";
$MESS["FLOW_EDIT_RIGHTS"] = "Autorisé à modifier et enregistrer<br>le document dans cet état : ";
$MESS["FLOW_ERROR"] = "Erreur !";
$MESS["FLOW_MOVE_RIGHTS"] = "Autorisé pour que ce statut<br>au document : ";
$MESS["FLOW_NEW_RECORD"] = "Nouveau statut";
$MESS["FLOW_NEW_STATUS"] = "Créer un nouveau statut";
$MESS["FLOW_NOTIFY"] = "Prévenir tous les utilisateurs autorisés à<br>modifier ou afficher le document<br>quand il entre ce statut";
$MESS["FLOW_RECORDS_LIST"] = "Liste des statuts";
$MESS["FLOW_SORTING"] = "Index du tri : ";
$MESS["FLOW_TIMESTAMP"] = "Changé:";
$MESS["FLOW_TITLE"] = "Dénomination : ";
$MESS["FLOW_VIEW"] = "Autorisé à voir<br>le document dans ce statut";
?>