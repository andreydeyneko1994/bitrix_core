<?
$MESS["FLOW_DENIED"] = "fermé";
$MESS["FLOW_INSTALL_DRAFT"] = "Brouillon";
$MESS["FLOW_INSTALL_PUBLISHED"] = "Année de publication";
$MESS["FLOW_INSTALL_READY"] = "Pour Approbation";
$MESS["FLOW_INSTALL_TITLE"] = "Installation du module de Workflow";
$MESS["FLOW_MODIFY"] = "modifier des documents en flux";
$MESS["FLOW_MODULE_DESCRIPTION"] = "Module d'organiser et de traiter les documents du site.";
$MESS["FLOW_MODULE_NAME"] = "Flux de documents";
$MESS["FLOW_READ"] = "en lecture seule";
$MESS["FLOW_WRITE"] = "accès complet";
?>