<?
$MESS["FLOW_ADMIN"] = "Workflow groupe d'administrateurs : ";
$MESS["FLOW_CLEAR"] = "Effacer";
$MESS["FLOW_DAYS_AFTER_PUBLISHING"] = "Nombre de jours de conservation dernière version du fichier : <br>(utiliser des valeurs négatives de garder pour l'éternité) : ";
$MESS["FLOW_HISTORY_COPIES"] = "Le nombre maximum de versions de fichiers : ";
$MESS["FLOW_HISTORY_DAYS"] = "Nombre de jours de conservation des versions antérieures de fichiers : <br>(utiliser des valeurs négatives de garder pour l'éternité) : ";
$MESS["FLOW_HISTORY_SIMPLE_EDITING"] = "Gardez l'histoire de version pour les fichiers édités directement (en dehors du module Workflow)";
$MESS["FLOW_MAX_LOCK"] = "Verrouiller le fichier pour l'édition de (minutes) : ";
$MESS["FLOW_RESET"] = "Annuler";
$MESS["FLOW_SAVE"] = "Enregistrer";
$MESS["FLOW_USE_HTML_EDIT"] = "Utilisez éditeur HTML visuel";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Par défaut";
?>