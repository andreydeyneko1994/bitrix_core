<?
$MESS["FLOW_ACCESS_DENIED_FOLDER"] = "Nie masz pozwolenia żeby pisać do #FILENAME#.";
$MESS["FLOW_ACCESS_DENIED_PHP_VIEW"] = "Nie masz pozwolenia żeby obejrzeć wykonywalne pliki.";
$MESS["FLOW_CURRENT_STATUS"] = "Bieżący status:";
$MESS["FLOW_DOCUMENT_NOT_PUBLISHED"] = "Dokument nie może zostać opublikowany.";
$MESS["FLOW_ERROR"] = "Błąd!";
$MESS["FLOW_PANEL_CREATE_ALT"] = "Utwórz nową stronę w obecnej sekcji przez workflow";
$MESS["FLOW_PANEL_CREATE_WITH_WF"] = "Przez workflow";
$MESS["FLOW_PANEL_EDIT_ALT"] = "Edytuj obecną stronę przez workflow";
$MESS["FLOW_PANEL_EDIT_WITH_WF"] = "Przez workflow";
$MESS["FLOW_PANEL_HISTORY"] = "Historia";
$MESS["FLOW_PANEL_HISTORY_ALT"] = "Historia zmian dla obecnej strony";
?>