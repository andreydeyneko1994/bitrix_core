<?
$MESS["FLOW_ADMIN"] = "Grupy administratorów workflow:";
$MESS["FLOW_CLEAR"] = "Usuń teraz";
$MESS["FLOW_DAYS_AFTER_PUBLISHING"] = "Liczba dni trzymania ostatniej wersji pliku:<br>(Użyj negatywnych wartości do zachowania na zawsze):";
$MESS["FLOW_HISTORY_COPIES"] = "Maksymalna liczba wersji pliku:";
$MESS["FLOW_HISTORY_DAYS"] = "Liczba dni do zachowania poprzednich wersji pliku:<br>(Użyj negatywnych wartości do zachowania na zawsze):";
$MESS["FLOW_HISTORY_SIMPLE_EDITING"] = "Zachowaj historię wersji dla plików edytowanych bezpośrednio (poza modułem Workflow)";
$MESS["FLOW_MAX_LOCK"] = "Blokuj plik dla edytowania (w minutach):";
$MESS["FLOW_RESET"] = "Wyczyść";
$MESS["FLOW_SAVE"] = "Zapisz";
$MESS["FLOW_USE_HTML_EDIT"] = "Użyj edytora wizualnego HTML";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Ustawienia domyślne";
?>