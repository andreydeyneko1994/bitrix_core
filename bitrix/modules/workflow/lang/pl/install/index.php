<?
$MESS["FLOW_DENIED"] = "odmówiono";
$MESS["FLOW_INSTALL_DRAFT"] = "Wersja Robocza";
$MESS["FLOW_INSTALL_PUBLISHED"] = "Opublikowany";
$MESS["FLOW_INSTALL_READY"] = "Gotowy";
$MESS["FLOW_INSTALL_TITLE"] = "Instalacja modułu Workflow";
$MESS["FLOW_MODIFY"] = "edytuj dokumenty w workflow";
$MESS["FLOW_MODULE_DESCRIPTION"] = "Moduł do organizacji procesu zarządzania i obiegu dokumentów na stronie";
$MESS["FLOW_MODULE_NAME"] = "Workflow";
$MESS["FLOW_READ"] = "Tylko do odczytu";
$MESS["FLOW_WRITE"] = "Pełny dostęp";
?>