<?
$MESS["FLOW_ACTIVE"] = "Aktywny:";
$MESS["FLOW_DELETE_STATUS"] = "Usuń bieżący status";
$MESS["FLOW_DELETE_STATUS_CONFIRM"] = "Na pewno chcesz usunąć ten status?";
$MESS["FLOW_DESCRIPTION"] = "Opis:";
$MESS["FLOW_DOCUMENTS"] = "Dokumenty w tym statusie:";
$MESS["FLOW_DOCUMENTS_ALT"] = "Zobacz dokumenty w tym statusie";
$MESS["FLOW_EDIT_RECORD"] = "Ustawienia statusu";
$MESS["FLOW_EDIT_RIGHTS"] = "Zezwolono na edycję i zapisywanie dokumentu<br>o tym statusie:";
$MESS["FLOW_ERROR"] = "Błąd!";
$MESS["FLOW_MOVE_RIGHTS"] = "Zezwolono na ustawienia tego statusu<br>do dokumentu:";
$MESS["FLOW_NEW_RECORD"] = "Nowy status";
$MESS["FLOW_NEW_STATUS"] = "Utwórz nowy status";
$MESS["FLOW_NOTIFY"] = "Powiadom wszystkich użytkowników upoważnionych do<br>edytowania lub oglądania dokumentu<br>kiedy wprowadzi ten status";
$MESS["FLOW_RECORDS_LIST"] = "Lista stanów";
$MESS["FLOW_SORTING"] = "Indeks sortowania:";
$MESS["FLOW_TIMESTAMP"] = "Zmodyfikowany:";
$MESS["FLOW_TITLE"] = "Tytuł:";
$MESS["FLOW_VIEW"] = "Zezwolono na wyświetlanie<br>dokumentu o tym statusie";
?>