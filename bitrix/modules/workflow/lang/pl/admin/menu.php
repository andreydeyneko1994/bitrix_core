<?
$MESS["FLOW_MENU_DOCUMENTS"] = "Dokumenty";
$MESS["FLOW_MENU_DOCUMENTS_ALT"] = "Dokumenty w przetwarzaniu workflow";
$MESS["FLOW_MENU_HISTORY"] = "Historia";
$MESS["FLOW_MENU_HISTORY_ALT"] = "Historia zmiany dokumentu";
$MESS["FLOW_MENU_MAIN"] = "Workflow";
$MESS["FLOW_MENU_MAIN_TITLE"] = "Zarządzanie dokumentami";
$MESS["FLOW_MENU_STAGE"] = "Statusy";
$MESS["FLOW_MENU_STAGE_ALT"] = "Możiwe statusy dla dokumentu";
?>