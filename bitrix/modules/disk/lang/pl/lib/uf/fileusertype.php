<?
$MESS["DISK_FILE_USER_TYPE_ERROR_BAD_RIGHTS"] = "Niewystarczające uprawnienia.";
$MESS["DISK_FILE_USER_TYPE_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "Nie można znaleźć dołączonego obiektu.";
$MESS["DISK_FILE_USER_TYPE_ERROR_COULD_NOT_FIND_FILE"] = "Nie można znaleźć pliku";
$MESS["DISK_FILE_USER_TYPE_ERROR_INVALID_VALUE"] = "Niewłaściwa wartość: musi być numer.";
$MESS["DISK_FILE_USER_TYPE_ERROR_USER_ID"] = "Użytkownik nie jest zalogowany.";
$MESS["DISK_FILE_USER_TYPE_NAME"] = "Plik (Dysk)";
?>