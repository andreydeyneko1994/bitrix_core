<?php
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Anuluj";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "Dokument nie został opublikowany. Przeprowadź proces biznesowy publikowania dokumentu.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "Nie można znaleźć folderu";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Nie można znaleźć magazynu dla użytkownika";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Usługa społecznościowa #NAME# nie jest skonfigurowana. Proszę o kontakt z administratorem portalu.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "Usługa społecznościowa #NAME# nie jest skonfigurowana. Proszę o kontakt z administratorem twojego Bitrix24.";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "Plik wideo jest zbyt duży, aby wyświetlić go w Bitrix24. <a class=\"transformer-upgrade-popup\" href=\"javascript:void(0);\">Szczegóły</a>";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Mój Dysk";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Dokumenty Grupy";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Ostatnie elementy";
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Wybierz folder, aby zapisać w nim dokument";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Wybierz dokument";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Wybierz jeden lub kilka dokumentów";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Wybierz obecny folder";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Wybierz dokument";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Dokumenty udostępnione";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Rozmiar";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Zmodyfikowane przez";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Nazwa";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Wybierz dokument";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Zmodyfikowany";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "
<b>Niestety rozmiar plik wideo jest zbyt duży, aby go wyświetlić w Bitrix24.</b><br />Przechowujemy plik, ale użytkownicy będą musieli go pobrać, aby go obejrzeć.<br /><br />
Pliki do 300 MB można odtwarzać w ramach bezpłatnego abonamentu.<br />
Plany komercyjne umożliwiają odtwarzanie plików do 3 GB.<br /><br />
Uaktualnij teraz i korzystaj również z innych przydatnych funkcji:
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Nieograniczone kopie zapasowe</li>
<li class=\"hide-features-list-item\">Blokada dokumentów</li>
<li class=\"hide-features-list-item\">Odnośniki do folderów publicznych</li>
<li class=\"hide-features-list-item\">Używaj procesów pracy workflow na dysku firmowym</li>
</ul>Zobacz zestawienie porównawcze planów i pełny opis funkcji <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/index.php\">tutaj</a>.";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "Dostępne wyłącznie w wersji zaawansowanej Dysku Bitrix24";
