<?php
$MESS["DISK_OBJECT_MODEL_ERROR_NON_UNIQUE_NAME"] = "Nazwa obiektu nie jest unikalna";
$MESS["DISK_OBJECT_MODEL_INVALID_MOVEMENT_TO_CHILD"] = "Nie można przenieść obiektu do katalogu podrzędnego";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_F"] = "Usunięto plik #NAME# do Kosza.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_M"] = "Usunięto plik #NAME# do Kosza.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "Usunięto folder #NAME# do Kosza.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "Usunięto folder #NAME# do Kosza.";
