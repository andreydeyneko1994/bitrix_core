<?
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_DESCR"] = "Udostępnij link do dokumentu na swoim Bitrix24. Wybierz, kiedy link ma wygasnąć, i ustaw hasło, aby upewnić się, że Twoje informacje są bezpieczne. Udostępnione linki i wiele innych funkcji są dostępne w wybranych planach płatnych.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_TITLE"] = "Linki publiczne do plików i folderów";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_DESCR"] = "Udostępnij link do dokumentu na swoim Bitrix24. Wybierz, kiedy link ma wygasnąć, i ustaw hasło, aby upewnić się, że Twoje informacje są bezpieczne. Udostępnione linki i wiele innych funkcji są dostępne w wybranych planach płatnych.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_TITLE"] = "Linki publiczne do plików i folderów";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_DESCR"] = "Jeśli chcesz udostępnić plik lub cały folder innym użytkownikom w celu przeglądania, edycji lub przesyłania plików, rozszerz do planu płatnego.";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_TITLE"] = "Rozszerz do planów płatnych, aby udostępniać pliki i foldery na swoim Dysku innym pracownikom";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_DESCR"] = "Jeśli chcesz udostępnić plik lub cały folder innym użytkownikom w celu przeglądania, edycji lub przesyłania plików, rozszerz do planu płatnego.";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_TITLE"] = "Rozszerz do planów płatnych, aby udostępniać pliki i foldery na swoim Dysku innym pracownikom";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_DESCR"] = "Zgodnie z bieżącym planem, pliki można udostępniać na dysku tylko współpracownikom. Aby cały folder był dostępny dla innych użytkowników do przeglądania, edytowania lub przesyłania plików, należy przejść na plan płatny. Udostępnianie folderów jest dostępne w wybranych planach płatnych.";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_TITLE"] = "Rozszerz do planów płatnych, aby udostępniać foldery na swoim Dysku innym pracownikom";
?>