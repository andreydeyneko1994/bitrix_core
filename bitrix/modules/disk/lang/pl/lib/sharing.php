<?
$MESS["DISK_SHARING_MODEL_APPROVE_N"] = "Rozłącz";
$MESS["DISK_SHARING_MODEL_APPROVE_N_2_DECLINE"] = "Anuluj";
$MESS["DISK_SHARING_MODEL_APPROVE_Y"] = "Połącz folder";
$MESS["DISK_SHARING_MODEL_APPROVE_Y_FILE"] = "Dołącz plik";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY"] = "Udostępniony folder <b>\"#NAME#\"</b> Jest teraz podłączony do twojego dysku. #DISCONNECT_LINK# #DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY_FILE"] = "Udostępniony plik <b>\"#NAME#\"</b> został podłączony do twojego dysku. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_CREATE_LINK"] = "Nie można stworzyć linku do obiektu.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_STORAGE"] = "Magazyn nie może zostać znaleziony";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Nie można znaleźć magazynu dla użytkownika (#USER_ID#).";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_REAL_OBJECT"] = "Nie podano obiektu do udostępnienia.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_USER_ID"] = "Użytkownik nie jest określony.";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM"] = "Czy chcesz podłączyć udostępniony folder <b>\"#NAME#\"</b> do twojego Dysku?";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM_FILE"] = "Czy chcesz podłączyć udostępniony plik <b>\"#NAME#\"</b> do twojego dysku?";
$MESS["DISK_SHARING_MODEL_TEXT_DISCONNECT_LINK"] = "Anuluj";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT"] = "#USERNAME# odłączył się od twojego udostępnionego folderu <b>\"#NAME#\"</b>";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT_FILE"] = "#USERNAME# odłączył się od twojego udostępnionego folderu <b>\"#NAME#\"</b>";
?>