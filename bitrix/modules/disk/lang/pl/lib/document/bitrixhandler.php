<?
$MESS["DISK_BITRIX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "#NAME# nie obsługuje tego działania.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW"] = "Dokumentu nie można wyświetlić w #NAME#.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW_SEND_TO_TRANSFORM"] = "Teraz przygotowujemy przeglądarkę dokumentów. Spróbuj ponownie później.";
$MESS["DISK_BITRIX_HANDLER_NAME"] = "Bitrix24.Viewer";
$MESS["DISK_BITRIX_HANDLER_NAME_2"] = "Bitrix24";
?>