<?
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_DOCX"] = "Nowy Dokument";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_PPTX"] = "Nowa prezentacja";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_XLSX"] = "Nowy arkusz kalkulacyjny";
$MESS["DISK_BLANK_FILE_DATA_TYPE_DOCX"] = "Dokument";
$MESS["DISK_BLANK_FILE_DATA_TYPE_PPTX"] = "Prezentacja";
$MESS["DISK_BLANK_FILE_DATA_TYPE_XLSX"] = "Arkusz kalkulacyjny";
?>