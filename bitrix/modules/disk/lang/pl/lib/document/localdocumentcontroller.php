<?
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Niewystarczające uprawnienia do zarządzania plikiem";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Nie można dodać nowej wersji pliku.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Nie można stworzyć pliku w magazynie";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "Nie można znaleźć pliku";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER"] = "Nie można znaleźć folderu";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Nie można znaleźć folderu aby zapisać w nim plik.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "Magazyn nie może zostać znaleziony";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "Nie można znaleźć wersji";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Nie można zapisać pliku";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME"] = "Bitrix.Drive";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME_2_WORK_WITH"] = "Aplikacji lokalnej";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME_3_WORK_WITH"] = "Aplikacje desktopowe";
?>