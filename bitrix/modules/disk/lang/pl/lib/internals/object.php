<?
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "Nie można usunąć węzła drzewa. Usuń najpierw wszystkie podrzędne pozycje.";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "Nazwa zawiera nieprawidłowe znaki.";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "Podano #FIELD# dla pliku.";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "Nieprawidłowa wartość #FIELD#.";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "Nie podano #FIELD# pliku.";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "Nie można zaktualizować \"#FIELD#\" dla istniejącego obiektu. Proszę użyć akcji \"przenieś\".";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "Nie można zaktualizować \"#FIELD#\" dla istniejącego obiektu.";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "Nazwa";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "Nadrzędny";
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "Magazyn";
?>