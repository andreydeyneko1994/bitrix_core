<?php
$MESS["DISK_FILE_MODEL_ERROR_ALREADY_LOCKED"] = "Nie można zablokować pliku, ponieważ jest on zablokowany przez innego użytkownika.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_COPY_FILE"] = "Nie można skopiować pliku.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_RESTORE_FROM_ANOTHER_OBJECT"] = "Nie można przywrócić wersji z innego pliku.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Nie można zapisać pliku.";
$MESS["DISK_FILE_MODEL_ERROR_EXCLUSIVE_LOCK"] = "Nie można zaktualizować pliku, ponieważ jest on zablokowany przez innego użytkownika.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_LOCK_TOKEN"] = "Nie można odblokować pliku: nieprawidłowy token.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK"] = "Nie można odblokować pliku, ponieważ jest on zablokowany przez innego użytkownika.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK_2"] = "Nie można odblokować pliku, ponieważ jest on zablokowany przez #USER#.";
$MESS["DISK_FILE_MODEL_ERROR_SIZE_RESTRICTION"] = "Nie można zapisać pliku, ponieważ przekracza on dopuszczalny rozmiar.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_F"] = "Edytowany plik";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_M"] = "Edytowany plik";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "załadowano nową wesję pliku";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "załadowano nową wesję pliku";
