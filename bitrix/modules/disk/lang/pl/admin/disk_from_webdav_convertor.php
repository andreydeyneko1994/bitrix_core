<?
$MESS["DISK_FW_CONVERT_COMPLETE"] = "Dane zostały przetworzone.";
$MESS["DISK_FW_CONVERT_FAILED"] = "Błąd konwertowania danych modułu Biblioteki Dokumentów";
$MESS["DISK_FW_CONVERT_HELP_10"] = "Publikuj";
$MESS["DISK_FW_CONVERT_HELP_11"] = "Niepublikowane dokumenty urlopowe";
$MESS["DISK_FW_CONVERT_HELP_3"] = "Stary moduł Workflow (Procesy biznesowe są obecne)";
$MESS["DISK_FW_CONVERT_HELP_7"] = "Wersje plików, które obecnie są przedmiotem Procesu Biznesowego";
$MESS["DISK_FW_CONVERT_HELP_75"] = "Tagi";
$MESS["DISK_FW_CONVERT_IN_PROGRESS"] = "W trakcie przetwarzania";
$MESS["DISK_FW_CONVERT_START_BUTTON"] = "Konwertuj";
$MESS["DISK_FW_CONVERT_STOP_BUTTON"] = "zatrzymaj";
$MESS["DISK_FW_CONVERT_TAB"] = "Przetworzenie danych";
$MESS["DISK_FW_CONVERT_TAB_TITLE"] = "Przetworzenie danych";
$MESS["DISK_FW_CONVERT_TITLE"] = "Konwersja Dokumentu Biblioteki Danych";
$MESS["DISK_FW_PROCESSED_SUMMARY"] = "Pozycje przetworzone (łącznie):";
?>