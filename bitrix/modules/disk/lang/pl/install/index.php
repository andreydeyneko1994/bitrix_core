<?
$MESS["DISK_INSTALL_DESCRIPTION"] = "Moduł pozwalający tworzyć, edytować i zarządzać plikami i folderami";
$MESS["DISK_INSTALL_NAME"] = "Dysk";
$MESS["DISK_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "Proszę <a href=\"#LINK#\">skonwertować dane modułu Biblioteka Dokumentów</a>, aby migrować do modułu Dysk.";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "Moduł nie może zostać odinstalowany, ponieważ dane modułu Biblioteka Dokumentów są wciąż konwertowane.";
$MESS["DISK_UNINSTALL_TITLE"] = "Deinstalacja modułu";
?>