<?
$MESS["DISK_JS_INF_POPUPS_EDIT_IN_LOCAL_SERVICE"] = "Edytuj na moim komputerze";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Idź do pliku";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "Dokument nie został zapisany!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "Dokument został zablokowany kiedy go edytowałeś. Wszystkie zmiany zostały zapisane do nowego dokumentu w folderze <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Zachowane</a>.";
$MESS["DISK_JS_INF_POPUPS_SERVICE_LOCAL_INSTALL_DESKTOP"] = "Aby wydajniej obsługiwać dokumenty na komputerze i cieszyć się lepszą obsługą, zainstaluj aplikację komputerową i podłącz Bitrix24.Drive.";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_DESCR"] = "#A#Podłącz Bitrix24.Drive#A_END#, aby łatwiej obsługiwać dokumenty na komputerze";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_TITLE"] = "Korzystanie z Dokumentów";
?>