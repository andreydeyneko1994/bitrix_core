<?
$MESS["DISK_DESKTOP_JS_SETTINGS"] = "Ustawienia";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_ENABLE"] = "Włącz Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION"] = "Kliknięcie pliku \"Ostatnio Otwierane\"";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FILE"] = "Otwiera plik";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FOLDER"] = "Otwiera folder zawierający";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_ALL"] = "Wszystko";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_OWN_FOLDER_NOTICE"] = "Twój dysk nie ma folderów. Masz tylko pliki, z których wszystkie są automatycznie synchronizowane z komputerem. Aby synchronizować tylko określone pliki, utwórz dla nich foldery.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_SHARED_FOLDER_NOTICE"] = "Nie masz udostępnionych folderów na dysku. Żaden z Twoich kolegów nie udostępnił Ci jeszcze folderu.

Gdy na dysku zostaną udostępnione foldery, możesz wybrać jeden lub więcej do synchronizacji z komputerem.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_FOLDER_NOTICE"] = "Te foldery są dla Ciebie widoczne, ponieważ zostały Ci udostępnione, chociaż nie jesteś ich właścicielem. Ta lista obejmuje również foldery grup, do których należysz.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_MAKE_CHOICE"] = "Wybierz foldery do synchronizacji z tym komputerem";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_OWNER"] = "Foldery prywatne";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_SHARED"] = "Foldery udostępnione";
$MESS["DISK_DESKTOP_JS_SETTINGS_TITLE"] = "Bitrix24.Drive";
?>