<?php
$MESS["DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Wszyscy zaangażowani użytkownicy mogą edytować dokumenty dołączone do dyskusji, zadań, <br/>komentarzy lub innych wydarzeń. To domyślne zachowanie może zostać zmienione <br/>dla każdego wydarzenia indywidualnie";
$MESS["DISK_ALLOW_USE_EXTERNAL_LINK"] = "Zezwól na publiczne linki";
$MESS["DISK_DEFAULT_VIEWER_SERVICE"] = "Przeglądanie dokumentów przy pomocy";
$MESS["DISK_ENABLE_OBJECT_LOCK_SUPPORT"] = "Zezwól na blokowanie dokumentu";
$MESS["DISK_SETTINGS_B24_DOCS_LIMIT_INFO"] = "Maksymalna liczba jednocześnie edytowanych plików: #limit#.";
$MESS["DISK_SETTINGS_B24_DOCS_REGISTER_BUTTON"] = "Aktywuj #NAME#";
$MESS["DISK_SETTINGS_B24_DOCS_UNREGISTER_BUTTON"] = "Rozłącz #NAME#";
$MESS["DISK_SETTINGS_ONLYOFFICE_MAX_FILESIZE"] = "Maksymalny rozmiar pliku do wyświetlania";
$MESS["DISK_VERSION_LIMIT_PER_FILE"] = "Maks. ilość wpisów w historii dokumentu";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Nieograniczone";
