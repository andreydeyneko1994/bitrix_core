<?
$MESS["DISK_JS_INF_POPUPS_EDIT_IN_LOCAL_SERVICE"] = "Editar no meu computador";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Ir para arquivo";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "O documento não foi salvo!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "O documento foi bloqueado enquanto você estava editando. Todas as suas alterações foram salvas num novo documento na <a class=\"disco-locked-documento-pop-content-link\" href=\"#LINK#\" target=\"_blank\">Pasta</a> armazenada.";
$MESS["DISK_JS_INF_POPUPS_SERVICE_LOCAL_INSTALL_DESKTOP"] = "Para lidar com documentos de forma mais eficiente em seu computador e aproveitar melhor a experiência do usuário, instale o aplicativo desktop e conecte o Bitrix24.Drive.";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_DESCR"] = "#A#Conecte o Bitrix24.Drive#A_END# para lidar mais facilmente com documentos no seu computador";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_TITLE"] = "Usando Documentos";
?>