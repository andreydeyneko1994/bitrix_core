<?
$MESS["DISK_DESKTOP_JS_SETTINGS"] = "Configurações";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_ENABLE"] = "Ativar Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION"] = "Ao clicar em um arquivo em \"Últimos Carregamentos\"";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FILE"] = "Abre arquivo";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FOLDER"] = "Abre pasta que contém";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_ALL"] = "Tudo";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_OWN_FOLDER_NOTICE"] = "Seu drive não possui pastas. Você tem apenas os arquivos que são todos auto sincronizados com o seu PC. Para sincronizar apenas alguns dos arquivos, crie pastas para eles.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_SHARED_FOLDER_NOTICE"] = "Você não tem pastas compartilhadas em seu drive. Seus colegas ainda não compartilharam pastas com você. Quando houver pastas compartilhadas no seu drive, você poderá selecionar uma ou mais delas para sincronizar com o seu PC.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_FOLDER_NOTICE"] = "Estas pastas são visíveis para você porque elas são compartilhadas com você, embora você não seja um proprietário destas pastas. Esta lista também inclui pastas dos grupos dos quais você é membro.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_MAKE_CHOICE"] = "Selecione pastas para sincronizar com este computador";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_OWNER"] = "Pastas particulares";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_SHARED"] = "Pastas compartilhadas";
$MESS["DISK_DESKTOP_JS_SETTINGS_TITLE"] = "Bitrix24.Drive";
?>