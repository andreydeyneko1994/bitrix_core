<?php
$MESS["DISK_SETTINGS_B24_DOCS_LIMIT_INFO"] = "Número máximo de arquivos editados simultaneamente: #limit#.";
$MESS["DISK_SETTINGS_B24_DOCS_REGISTER_BUTTON"] = "Ativar #NAME#";
$MESS["DISK_SETTINGS_B24_DOCS_UNREGISTER_BUTTON"] = "Desconectar #NAME#";
$MESS["DISK_SETTINGS_ONLYOFFICE_MAX_FILESIZE"] = "Tamanho máximo do arquivo para visualização";
