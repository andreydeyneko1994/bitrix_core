<?
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_DESCR"] = "Compartilhe um link para um documento no seu Bitrix24. Escolha quando você deseja que o link expire e defina uma senha para garantir que suas informações estejam seguras. Links compartilhados e muitos outros recursos estão disponíveis em planos comerciais selecionados.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_TITLE"] = "Links públicos para arquivos e pastas";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_DESCR"] = "Compartilhe um link para um documento no seu Bitrix24. Escolha quando você deseja que o link expire e defina uma senha para garantir que suas informações estejam seguras. Links compartilhados e muitos outros recursos estão disponíveis em planos comerciais selecionados.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_TITLE"] = "Links públicos para arquivos e pastas";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_DESCR"] = "Se você deseja tornar um arquivo ou uma pasta inteira disponível para outros usuários visualizarem, editarem ou fazerem upload de arquivos, faça um upgrade para um plano comercial.";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_TITLE"] = "Faça um upgrade para planos comerciais para compartilhar seus arquivos e pastas do Drive com outros funcionários";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_DESCR"] = "Se você deseja tornar um arquivo ou uma pasta inteira disponível para outros usuários visualizarem, editarem ou fazerem upload de arquivos, faça um upgrade para um plano comercial.";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_TITLE"] = "Faça um upgrade para planos comerciais para compartilhar seus arquivos e pastas do Drive com outros funcionários";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_TITLE"] = "Faça um upgrade para planos comerciais para compartilhar suas pastas do Drive com outros funcionários";
?>