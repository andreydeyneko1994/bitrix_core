<?
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Cancelar";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "O documento não foi publicado. Executar a publicação do documento do processo comercial.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "A pasta não pode ser encontrada.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Não é possível encontrar armazenamento para usuário.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "O Serviço Social #NAME# não está configurado. Entre em contato com o administrador do portal.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "O serviço de rede social #NAME# não está configurado. Entre em contato com seu administrador do Bitrix24.";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "O arquivo de vídeo é muito grande para visualizar no Bitrix24. <a class=\"transformer-upgrade-popup\" href=\"javascript:void(0);\">Informações</a>";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Meu drive";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Drives do grupo";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Itens recentes";
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Selecionar uma pasta para salvar o documento";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Selecionar um ou mais documentos";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Selecionar pasta atual";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Drive compartilhado";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Tamanho";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Modificado por";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Nome";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Modificado em";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "
<b>Infelizmente, o tamanho do seu arquivo de vídeo é muito grande para visualizar no Bitrix24.</b><br /> Estamos mantendo o arquivo, mas os usuários terão que baixá-lo antes de poder vê-lo.<br /><br /> Arquivos até 300 MB podem ser reproduzidos no plano Grátis.<br />
Planos comerciais podem reproduzir arquivos de até 3 GB.<br /><br />
Faça upgrade agora e desfrute de mais recursos úteis:
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Cópias de arquivos de backup ilimitadas</li>
<li class=\"hide-features-list-item\">Bloqueio de documento</li>
<li class=\"hide-features-list-item\">Links de pasta pública</li>
<li class=\"hide-features-list-item\">Use fluxos de trabalho com o Drive da Empresa</li>
</ul>
Veja o gráfico de comparação de plano e a descrição completa de recursos <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/index.php\">aqui</a>.";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "Disponível apenas em Bitrix24.Drive prolongado";
?>