<?php
$MESS["DISK_OBJECT_MODEL_ERROR_NON_UNIQUE_NAME"] = "O nome do objeto não é exclusivo.";
$MESS["DISK_OBJECT_MODEL_INVALID_MOVEMENT_TO_CHILD"] = "Não é possível mover o objeto para um diretório secundário";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_F"] = "Arquivo #NAME# excluído na Lixeira.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_M"] = "Arquivo #NAME# excluído na Lixeira.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "Pasta #NAME# excluída na Lixeira.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "Pasta #NAME# excluída na Lixeira.";
