<?
$MESS["DISK_CONTROLLER_DENIED_ERROR_MESSAGE"] = "Permissões insuficientes para executar a operação";
$MESS["DISK_CONTROLLER_ERROR_COULD_NOT_FIND_OBJECT"] = "O objeto não pôde ser encontrado.";
$MESS["DISK_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "A versão não pôde ser encontrada.";
?>