<?
$MESS["DISK_ERROR_MESSAGE_DENIED"] = "Permissões insuficientes para executar a operação";
$MESS["DISK_FILE_C_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Não é possível criar ou encontrar um link público para o objeto.";
$MESS["DISK_FILE_C_ERROR_INVALID_FILE_VERSION"] = "A versão especificada não pertence ao arquivo.";
?>