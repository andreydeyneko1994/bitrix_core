<?
$MESS["DISK_BITRIX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "#NAME# não é compatível com esta ação.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW"] = "O documento não pode ser visualizado em #NAME#.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW_SEND_TO_TRANSFORM"] = "Estamos agora preparando a visualização do documento. Tente novamente mais tarde.";
$MESS["DISK_BITRIX_HANDLER_NAME"] = "Bitrix24.Viewer";
$MESS["DISK_BITRIX_HANDLER_NAME_2"] = "Bitrix24";
?>