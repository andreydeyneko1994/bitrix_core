<?
$MESS["DISK_TYPE_FILE_ARCHIVE"] = "Arquivos";
$MESS["DISK_TYPE_FILE_AUDIO"] = "Áudio";
$MESS["DISK_TYPE_FILE_DOCUMENT"] = "Documentos";
$MESS["DISK_TYPE_FILE_IMAGE"] = "Imagens";
$MESS["DISK_TYPE_FILE_SCRIPT"] = "Scripts";
$MESS["DISK_TYPE_FILE_UNKNOWN"] = "Outro";
$MESS["DISK_TYPE_FILE_VECTOR_IMAGE"] = "Imagens vetoriais";
$MESS["DISK_TYPE_FILE_VIDEO"] = "Vídeo";
?>