<?
$MESS["DISK_JS_INF_POPUPS_EDIT_IN_LOCAL_SERVICE"] = "Editar en mi computadora";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Ir al archivo";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "El documento no se guardó!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "El documento ha sido bloqueado mientras se estaba editando. Todos los cambios se han guardado en un nuevo documento en la <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Stored</a> folder.";
$MESS["DISK_JS_INF_POPUPS_SERVICE_LOCAL_INSTALL_DESKTOP"] = "Para administrar documentos en su computadora de manera más eficiente y disfrutar de una mejor experiencia de usuario, instale la aplicación de escritorio y conéctese a Bitrix24.Drive.";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_DESCR"] = "#A#Conéctese a Bitrix24.Drive#A_END# para manejar documentos más fácilmente en su computadora";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_TITLE"] = "Uso de los documentos";
?>