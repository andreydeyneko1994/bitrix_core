<?
$MESS["DISK_ERROR_MESSAGE_DENIED"] = "Permisos insuficientes para realizar la operación";
$MESS["DISK_FILE_C_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "No se pudo crear o encontrar un enlace público al objeto.";
$MESS["DISK_FILE_C_ERROR_INVALID_FILE_VERSION"] = "La versión especificada no pertenece al archivo.";
?>