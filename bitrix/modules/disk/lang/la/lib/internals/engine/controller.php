<?
$MESS["DISK_CONTROLLER_DENIED_ERROR_MESSAGE"] = "Permisos insuficientes para realizar la operación";
$MESS["DISK_CONTROLLER_ERROR_COULD_NOT_FIND_OBJECT"] = "El objeto no se pudo encontrar.";
$MESS["DISK_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "La versión no pudo ser encontrada.";
?>