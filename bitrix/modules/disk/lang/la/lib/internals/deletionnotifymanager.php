<?php
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FILE_F"] = "El archivo #NAME# fue enviado a la Papelera de reciclaje.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FILE_M"] = "El archivo #NAME# fue enviado a la Papelera de reciclaje.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "La carpeta #NAME# fue enviada a la Papelera de reciclaje.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "La carpeta #NAME# fue enviada a la Papelera de reciclaje.";
