<?
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_DESCR"] = "Comparta un enlace a un documento en su Bitrix24. Elija cuándo desea que caduque el enlace y establezca una contraseña para asegurarse de que su información permanezca segura. Los enlaces compartidos y muchas otras funciones están disponibles en los planes comerciales seleccionados.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_TITLE"] = "Enlaces públicos a archivos y carpetas";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_DESCR"] = "Comparta un enlace a un documento en su Bitrix24. Elija cuándo desea que caduque el enlace y establezca una contraseña para asegurarse de que su información permanezca segura. Los enlaces compartidos y muchas otras funciones están disponibles en los planes comerciales seleccionados.";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_TITLE"] = "Enlaces públicos a archivos y carpetas";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_DESCR"] = "Si desea que un archivo o una carpeta completa esté disponible para que otros usuarios puedan ver, editar o cargar archivos, actualice a un plan comercial.";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_TITLE"] = "Actualice a los planes comerciales para compartir sus archivos de Drive con otros empleados";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_DESCR"] = "Si desea que un archivo o una carpeta completa esté disponible para que otros usuarios puedan ver, editar o cargar archivos, actualice a un plan comercial.";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_TITLE"] = "Actualice a los planes comerciales para compartir sus archivos de Drive con otros empleados";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_DESCR"] = "En su plan actual, solo puede compartir los archivos de su Drive con sus colegas. Si desea poner una carpeta completa a disposición de otros usuarios para ver, editar o cargar archivos, actualice a un plan comercial. El uso compartido de carpetas está disponible en los planes comerciales seleccionados.";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_TITLE"] = "Actualice a los planes comerciales para compartir sus carpetas de Drive con otros empleados";
?>