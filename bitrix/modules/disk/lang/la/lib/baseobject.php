<?php
$MESS["DISK_OBJECT_MODEL_ERROR_NON_UNIQUE_NAME"] = "El nombre de objeto no es único.";
$MESS["DISK_OBJECT_MODEL_INVALID_MOVEMENT_TO_CHILD"] = "No se puede mover el objeto a un directorio secundario";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_F"] = "El archivo #NAME# fue enviado a la Papelera de reciclaje.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_M"] = "El archivo #NAME# fue enviado a la Papelera de reciclaje.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "La carpeta #NAME# fue enviada a la Papelera de reciclaje.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "La carpeta #NAME# fue enviada a la Papelera de reciclaje.";
