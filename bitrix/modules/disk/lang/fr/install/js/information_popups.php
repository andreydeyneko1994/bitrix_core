<?
$MESS["DISK_JS_INF_POPUPS_EDIT_IN_LOCAL_SERVICE"] = "Éditer sur mon ordinateur";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Aller au fichier";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "Le document n'a pas été enregistré !";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "Le document a été verrouillé pendant que vous l'éditiez. Toutes vos modifications ont été enregistrées dans un nouveau document dans le dossier <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Conservés</a>.";
$MESS["DISK_JS_INF_POPUPS_SERVICE_LOCAL_INSTALL_DESKTOP"] = "Pour gérer plus efficacement les documents sur votre ordinateur et profiter d'une meilleure expérience utilisateur, installez l'application de bureau et connectez Bitrix24.Drive.";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_DESCR"] = "#A#Connectez Bitrix24.Drive#A_END# pour gérer plus facilement les documents sur votre ordinateur";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_TITLE"] = "Utilisation des documents";
?>