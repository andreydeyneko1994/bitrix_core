<?
$MESS["DISK_ERROR_MESSAGE_DENIED"] = "Permissions insuffisantes pour cette opération";
$MESS["DISK_FILE_C_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Impossible de créer ou de trouver le lien public vers l'objet.";
$MESS["DISK_FILE_C_ERROR_INVALID_FILE_VERSION"] = "Le version spécifiée n'appartient pas au fichier.";
?>