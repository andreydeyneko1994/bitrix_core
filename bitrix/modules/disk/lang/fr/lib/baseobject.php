<?php
$MESS["DISK_OBJECT_MODEL_ERROR_NON_UNIQUE_NAME"] = "Le nom de l'objet n'est pas unique.";
$MESS["DISK_OBJECT_MODEL_INVALID_MOVEMENT_TO_CHILD"] = "Impossible de déplacer l'objet dans un répertoire enfant";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_F"] = "Suppression du fichier #NAME# dans la Corbeille.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FILE_M"] = "Suppression du fichier #NAME# dans la Corbeille.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "Suppression du dossier #NAME# dans la Corbeille.";
$MESS["DISK_OBJECT_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "Suppression du dossier #NAME# dans la Corbeille.";
