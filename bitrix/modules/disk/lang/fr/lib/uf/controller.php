<?
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Annuler";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "Le document n'a pas été publié. Veuillez lancer la procédure d'entreprise de publication du document.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "Dossier introuvable.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Impossible de trouver le stockage de l'utilisateur.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Le service social #NAME# n'est pas configuré. Veuillez contacter votre administrateur portail.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "Le service de réseau social #NAME# n'est pas configuré. Veuillez contacter votre administrateur Bitrix24.";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "Ce fichier vidéo est trop volumineux pour être lu dans Bitrix24. <a class=\"transformer-upgrade-popup\" href=\"javascript:void(0);\">Détails</a>";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Mon Drive";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Disques des groupes";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Éléments récents";
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Sélectionnez un dossier pour enregistrer le document à";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Choisissez le document";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Choisissez un ou quelques documents";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Choisir le dossier courant";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Sélectionnez un document";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Drive Entreprise";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Taille";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Modifié(e)s par";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Nom";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Choisissez le document";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Date de modification";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "
<b>Malheureusement, le fichier vidéo est trop volumineux pour être lu dans Bitrix24.</b><br />
Nous conservons le fichier, mais les utilisateurs devront le télécharger pour pouvoir le lire.<br /><br />
Les fichiers dont la taille ne dépasse pas 300 Mo peuvent être lu avec l'offre gratuite.<br />
Les offres commerciales permettent de lire des fichiers dont la taille peut atteindre 3Go.<br /><br />
Mettez maintenant à niveau et profiter de plus de fonctionnalités utiles :
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Copies de sauvegarde illimitées</li>
<li class=\"hide-features-list-item\">Verrouillage de document</li>
<li class=\"hide-features-list-item\">Liens vers des répertoires publics</li>
<li class=\"hide-features-list-item\">Utilisation des flux de travail avec le Lecteur de la société</li>
</ul>
Retrouvez <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/index.php\">here</a> un graphique de comparaison des offres et les descriptions complètes.";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "Disponible uniquement dans la version étendue de Bitrix24.Drive";
?>