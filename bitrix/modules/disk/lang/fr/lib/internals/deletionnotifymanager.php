<?php
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FILE_F"] = "Suppression du fichier #NAME# dans la Corbeille.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FILE_M"] = "Suppression du fichier #NAME# dans la Corbeille.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FOLDER_F"] = "Suppression du dossier #NAME# dans la Corbeille.";
$MESS["DISK_DELETION_MANAGER_NOTIFY_ABOUT_DELETION_FOLDER_M"] = "Suppression du dossier #NAME# dans la Corbeille.";
