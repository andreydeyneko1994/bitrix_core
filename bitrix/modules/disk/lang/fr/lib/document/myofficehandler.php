<?
$MESS["DISK_MYOFFICE_HANDLER_ERROR_COULD_NOT_FIND_EMBED_LINK"] = "Impossible de trouver le lien à utiliser.";
$MESS["DISK_MYOFFICE_HANDLER_ERROR_COULD_NOT_VIEW_FILE"] = "Impossible de visualiser le fichier avec MyOffice";
$MESS["DISK_MYOFFICE_HANDLER_NAME"] = "MyOffice";
$MESS["DISK_MYOFFICE_HANDLER_NAME_STORAGE"] = "MyOffice";
?>