<?php
$MESS["DISK_JS_EL_SETTINGS_LINK_CAN_EDIT_DOCUMENT"] = "pozwól na edycję";
$MESS["DISK_JS_EL_SETTINGS_LINK_HINT_PROTECT_BY_LIFETIME"] = "ustaw czas istnienia linku";
$MESS["DISK_JS_EL_SETTINGS_LINK_HINT_PROTECT_BY_PASSWORD"] = "zabezpiecz ten link hasłem";
$MESS["DISK_JS_EL_SETTINGS_LINK_LABEL_DAY"] = "dni";
$MESS["DISK_JS_EL_SETTINGS_LINK_LABEL_HOUR"] = "godz.";
$MESS["DISK_JS_EL_SETTINGS_LINK_LABEL_MINUTE"] = "min";
$MESS["DISK_JS_EL_SETTINGS_LINK_PROTECT_BY_PASSWORD"] = "ustaw hasło";
$MESS["DISK_JS_EL_SETTINGS_LINK_VALUE_PROTECT_BY_LIFETIME"] = "do {{deathTime}}";
$MESS["DISK_JS_EL_SETTINGS_LINK_VALUE_PROTECT_BY_LIFETIME_2"] = "ważne do {{getFormattedDeathTime}}";
$MESS["DISK_JS_EL_SETTINGS_LINK_VALUE_PROTECT_BY_PASSWORD"] = "********";
