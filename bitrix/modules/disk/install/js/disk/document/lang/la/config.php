<?
$MESS["JS_DISK_DOC_PROCESS_CREATE_DESCR_SAVE_DOC_F"] = "Actualmente está editando este documento en una de las ventanas. Una vez que haya terminado, haga clic en \"#SAVE_AS_DOC#\" para cargar el documento en su Intranet.";
$MESS["JS_DISK_DOC_PROCESS_EDIT_IN_SERVICE"] = "Editar con #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_DESCR_SAVE_DOC_F"] = "Actualmente está editando este documento en una de las ventanas. Una vez que haya terminado, haga clic en \"#SAVE_DOC#\" para cargar el documento en su Intranet.";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_PROCESS_SAVE_DOC"] = "Guardar documento";
$MESS["JS_DISK_DOC_PROCESS_NOW_CREATING_IN_SERVICE"] = "Crear el documento con #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_DOWNLOAD_FROM_SERVICE"] = "Descargar el documento de #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_EDITING_IN_SERVICE"] = "Editando con #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_SAVE"] = "Guardar";
$MESS["JS_DISK_DOC_PROCESS_SAVE_AS"] = "Guardar como";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM"] = "Trate de <a href='#' id='#ID#'>abrir el documento</a> utilizando Bitrix24.Desktop";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_OR_DOWNLOAD"] = "o <a href=\"#DOWNLOAD_LINK#\" target=\"_blank\">descárguelo</a>.";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_TITLE"] = "No puede abrir el archivo. Intente <a href='#' id='#ID#'>abrirlo</a> utilizando Bitrix24.Drive.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP"] = "Ejecute la aplicación Bitrix24";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DESCR"] = "Desafortunadamente, no pudimos abrir el archivo para editarlo. Asegúrese de que tiene instalada la aplicación Bitrix24. <br><br>Si la aplicación está instalada, espere a que se abra el documento o intente de nuevo más tarde.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DOWNLOAD"] = "Descargar la aplicación";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_HELP"] = "Obtenga más información sobre el uso de los documentos";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_TITLE"] = "Editar";
?>