<?
$MESS["JS_DISK_DOC_PROCESS_CREATE_DESCR_SAVE_DOC_F"] = "No momento, você está editando um novo documento em uma das janelas. Depois de terminar, clique em \"#SAVE_AS_DOC#\" para adicionar o documento na sua Intranet.";
$MESS["JS_DISK_DOC_PROCESS_EDIT_IN_SERVICE"] = "Editar com #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_DESCR_SAVE_DOC_F"] = "No momento, você está editando este documento em uma das janelas. Depois de terminar, clique em \"#SAVE_DOC#\" para carregar o documento na sua Intranet.";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_PROCESS_SAVE_DOC"] = "Salvar documento";
$MESS["JS_DISK_DOC_PROCESS_NOW_CREATING_IN_SERVICE"] = "Criar documento com #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_DOWNLOAD_FROM_SERVICE"] = "Fazer download do documento a partir de #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_EDITING_IN_SERVICE"] = "Editando com #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_SAVE"] = "Salvar";
$MESS["JS_DISK_DOC_PROCESS_SAVE_AS"] = "Salvar como";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM"] = "Tente <a href='#' id='#ID#'>abrir o documento</a> usando o Bitrix24.Desktop";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_OR_DOWNLOAD"] = "ou <a href=\"#DOWNLOAD_LINK#\" target=\"_blank\">baixe-o</a>.";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_TITLE"] = "Não é possível abrir o arquivo. Tente <a href='#' id='#ID#'>abri-lo</a> usando o Bitrix24.Drive.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP"] = "Executar o aplicativo Bitrix24";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DESCR"] = "Infelizmente, não foi possível abrir o arquivo para edição. Verifique se você tem o aplicativo Bitrix24 instalado. <br><br>Se o aplicativo estiver instalado, por favor, aguarde o documento abrir ou tente novamente mais tarde.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DOWNLOAD"] = "Baixar aplicativo";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_HELP"] = "Saiba mais sobre o uso de documentos";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_TITLE"] = "Editar";
?>