<?php
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_BUTTON"] = "Zarejestruj się";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_AFTER_REG"] = "Niestety Twoja licencja nie obsługuje #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_COMMON"] = "Nie można zarejestrować. Spróbuj ponownie później.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_DOMAIN_VERIFICATION"] = "Nie można zweryfikować domeny (#DOMAIN#) podczas rejestracji #NAME#. Możliwe, że strona jest hostowana w intranecie i niedostępna z sieci zewnętrznej.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_SELECT_SERVER_LABEL"] = "Wybierz serwer:";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_WARNING"] = "Korzystanie z #NAME# wymaga aktywnej licencji.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_ERROR_COMMON"] = "Błąd: nie można rozłączyć. Spróbuj ponownie.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_MSG"] = "Czy na pewno chcesz przestać używać #NAME#? <br> Zawsze możesz połączyć się ponownie później. <br><br> Uwaga: możesz utracić zmiany wprowadzone w aktualnie otwartych dokumentach.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_TITLE"] = "Rozłącz #NAME#";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_UNREGISTER_BTN"] = "Rozłącz";
