<?php
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_BUTTON"] = "Inscription";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_AFTER_REG"] = "Malheureusement, votre licence ne prend pas en charge #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_COMMON"] = "Inscription impossible. Veuillez réessayer plus tard.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_DOMAIN_VERIFICATION"] = "Impossible de valider le domaine (#DOMAIN#) lors de l'inscription de #NAME#. L'une des raisons possibles est que le site est potentiellement hébergé sur l'Intranet et n'est pas disponible sur le Web.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_SELECT_SERVER_LABEL"] = "Sélectionner le serveur :";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_WARNING"] = "Veuillez noter que vous devez avoir une licence active pour utiliser #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_ERROR_COMMON"] = "Erreur : déconnexion impossible. Veuillez réessayer.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_MSG"] = "Voulez-vous vraiment arrêter d'utiliser #NAME# ? <br>Vous pouvez toujours vous reconnecter plus tard. <br><br>Attention : vous pouvez perdre les modifications apportées aux documents actuellement ouverts.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_TITLE"] = "Déconnecter #NAME#";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_UNREGISTER_BTN"] = "Déconnexion";
