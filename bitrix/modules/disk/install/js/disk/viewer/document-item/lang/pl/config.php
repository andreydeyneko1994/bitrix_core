<?php
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_DESCR_OFFICE365"] = "Aby wyświetlić plik, otwórz go w nowym oknie";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_FILE_OFFICE365"] = "Otwórz";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_HELP_HINT_OFFICE365"] = "<a href=\"#\" onclick='top.BX.Helper.show(\"redirect=detail&code=9562101\");event.preventDefault();'>Dowiedz się więcej</a> na temat pracy z plikami w Office365.";
$MESS["JS_VIEWER_DOCUMENT_ITEM_SHOW_FILE_DIALOG_OAUTH_NOTICE"] = "Aby wyświetlić pliki, zaloguj się na konto <a id=\"bx-js-disk-run-oauth-modal\" href=\"#\">#SERVICE#</a>.";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVED"] = "Dokument #name# został zaktualizowany";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVE_PROCESS"] = "Zapisywanie dokumentu: #name#";
