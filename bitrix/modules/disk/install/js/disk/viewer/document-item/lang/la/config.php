<?php
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_DESCR_OFFICE365"] = "Para ver el archivo, ábralo en una nueva ventana";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_FILE_OFFICE365"] = "Abrir";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_HELP_HINT_OFFICE365"] = "<a href=\"#\" onclick='top.BX.Helper.show(\"redirect=detail&code=9562101\");event.preventDefault();'>Obtenga más información</a> sobre cómo trabajar con archivos en Office365.";
$MESS["JS_VIEWER_DOCUMENT_ITEM_SHOW_FILE_DIALOG_OAUTH_NOTICE"] = "Inicie sesión en su <a id=\"bx-js-disk-run-oauth-modal\" href=\"#\">#SERVICE#</a> cuenta para ver el archivo.";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVED"] = "Se actualizó el documento #name#";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVE_PROCESS"] = "Guardando el documento: #name#";
