<?
$MESS["BPDCM_PD_ENTITY"] = "Do";
$MESS["BPDCM_PD_ENTITY_ID_COMMON"] = "Dysk";
$MESS["BPDCM_PD_ENTITY_ID_FOLDER"] = "Folder";
$MESS["BPDCM_PD_ENTITY_ID_SG"] = "Kategoria użytkownika";
$MESS["BPDCM_PD_ENTITY_ID_USER"] = "Użytkownik";
$MESS["BPDCM_PD_ENTITY_TYPE_COMMON"] = "Dysk publiczny";
$MESS["BPDCM_PD_ENTITY_TYPE_FOLDER_1"] = "Folder na Dysku";
$MESS["BPDCM_PD_ENTITY_TYPE_SG"] = "Dysk kategorii użytkownika sieci społecznościowej";
$MESS["BPDCM_PD_ENTITY_TYPE_USER"] = "Dysk użytkownika";
$MESS["BPDCM_PD_LABEL_CHOOSE"] = "Wybierz:";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FILE"] = "Wybierz plik";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Wybierz folder";
$MESS["BPDCM_PD_LABEL_DISK_EMPTY"] = "Brak wyboru";
$MESS["BPDCM_PD_OPERATION"] = "Operacja";
$MESS["BPDCM_PD_OPERATION_COPY"] = "Kopiuj";
$MESS["BPDCM_PD_OPERATION_MOVE"] = "Przenieś";
$MESS["BPDCM_PD_OPERATOR"] = "Uruchom jako";
$MESS["BPDCM_PD_SOURCE_ID"] = "Obiekt źródłowy";
$MESS["BPDCM_PD_SOURCE_ID_DESCR"] = "Plik lub folder na Dysku";
?>