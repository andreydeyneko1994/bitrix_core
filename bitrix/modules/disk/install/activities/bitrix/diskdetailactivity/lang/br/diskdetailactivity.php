<?
$MESS["BPDD_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDD_EMPTY_SOURCE_ID"] = "O arquivo de origem não está especificado.";
$MESS["BPDD_SOURCE_ID_ERROR"] = "O objeto de origem não foi encontrado.";
?>