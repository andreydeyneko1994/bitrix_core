<?
$MESS["BPDD_DESCR_CATEGORY"] = "Dysk";
$MESS["BPDD_DESCR_DESCR2"] = "Pobierz szczegółowe dane obiektu Dysku";
$MESS["BPDD_DESCR_DETAIL_URL"] = "Zobacz URL";
$MESS["BPDD_DESCR_DOWNLOAD_URL"] = "Pobierz URL";
$MESS["BPDD_DESCR_NAME"] = "Nazwa";
$MESS["BPDD_DESCR_NAME2"] = "Szczegółowe dane obiektu Dysku";
$MESS["BPDD_DESCR_SIZE_BYTES"] = "Rozmiar, bajty";
$MESS["BPDD_DESCR_SIZE_FORMATTED"] = "Rozmiar (sformatowany)";
$MESS["BPDD_DESCR_TYPE"] = "Typ obiektu";
?>