<?
$MESS["BPDRMV_ACCESS_DENIED"] = "Odmowa dostępu dla wszystkich poza administratorami portalu.";
$MESS["BPDRMV_EMPTY_SOURCE_ID"] = "Nie określono przedmiotu źródła.";
$MESS["BPDRMV_REMOVE_ERROR"] = "Nie można usunąć obiektu.";
$MESS["BPDRMV_SOURCE_ERROR"] = "Nie znaleziono obiektu źródłowego.";
?>