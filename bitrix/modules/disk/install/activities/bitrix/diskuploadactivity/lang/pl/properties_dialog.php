<?
$MESS["BPDUA_PD_CREATED_BY"] = "Przesyłanie jako";
$MESS["BPDUA_PD_ENTITY"] = "Lokalizacja docelowa";
$MESS["BPDUA_PD_ENTITY_ID_COMMON"] = "Dysk";
$MESS["BPDUA_PD_ENTITY_ID_FOLDER"] = "Folder";
$MESS["BPDUA_PD_ENTITY_ID_SG"] = "Kategoria użytkownika";
$MESS["BPDUA_PD_ENTITY_ID_USER"] = "Użytkownik";
$MESS["BPDUA_PD_ENTITY_TYPE_COMMON"] = "Dysk publiczny";
$MESS["BPDUA_PD_ENTITY_TYPE_FOLDER_1"] = "Folder na Dysku";
$MESS["BPDUA_PD_ENTITY_TYPE_SG"] = "Dysk kategorii użytkownika sieci społecznościowej";
$MESS["BPDUA_PD_ENTITY_TYPE_USER"] = "Dysk użytkownika";
$MESS["BPDUA_PD_LABEL_CHOOSE"] = "Wybierz:";
$MESS["BPDUA_PD_LABEL_DISK_CHOOSE"] = "Wybierz folder";
$MESS["BPDUA_PD_LABEL_DISK_EMPTY"] = "Brak wyboru";
$MESS["BPDUA_PD_SOURCE_FILE"] = "Plik do przesłania";
?>