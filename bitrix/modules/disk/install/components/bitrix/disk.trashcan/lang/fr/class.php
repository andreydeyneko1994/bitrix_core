<?php
$MESS["DISK_TRASHCAN_ACT_DESTROY"] = "Supprimer";
$MESS["DISK_TRASHCAN_ACT_DOWNLOAD"] = "Télécharger";
$MESS["DISK_TRASHCAN_ACT_OPEN"] = "Aller";
$MESS["DISK_TRASHCAN_ACT_RESTORE"] = "Restaurer";
$MESS["DISK_TRASHCAN_COLUMN_CREATE_TIME"] = "Créé le";
$MESS["DISK_TRASHCAN_COLUMN_CREATE_USER"] = "Créé par";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_TIME"] = "Supprimé";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_USER"] = "Supprimé par";
$MESS["DISK_TRASHCAN_COLUMN_FORMATTED_SIZE"] = "Taille";
$MESS["DISK_TRASHCAN_COLUMN_ID"] = "ID";
$MESS["DISK_TRASHCAN_COLUMN_NAME"] = "Nom";
$MESS["DISK_TRASHCAN_COLUMN_UPDATE_TIME"] = "Date de modification";
$MESS["DISK_TRASHCAN_COLUMN_UPDATE_USER"] = "Modifié(e)s par";
$MESS["DISK_TRASHCAN_DEFAULT_ACTION"] = "Sélectionnez une action";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de trouver l'objet.";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_CREATED_BY"] = "Créé par";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_CREATE_TIME"] = "Date de création";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_DELETE_TIME"] = "Date de suppression";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_ID"] = "ID";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_NAME"] = "Nom";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_PRESETS_RECENTLY_DELETED"] = "Suppression récente";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_PRESETS_RECENTLY_UPDATED"] = "Modification récente";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_SEARCH_IN_CURRENT_FOLDER"] = "Dans le dossier actuel";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_UPDATE_TIME"] = "Date de modification";
$MESS["DISK_TRASHCAN_NAME"] = "Corbeille";
$MESS["DISK_TRASHCAN_TRASH_CANCEL_DELETE_BUTTON"] = "Annuler";
$MESS["DISK_TRASHCAN_TRASH_CANCEL_STOP_BUTTON"] = "Stop";
$MESS["DISK_TRASHCAN_TRASH_COUNT_ELEMENTS"] = "Fichiers et/ou dossiers à supprimer : ";
$MESS["DISK_TRASHCAN_TRASH_DELETE_BUTTON"] = "Déplacer dans la corbeille";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "Etes-vous sur de vouloir supprimer définitivement '#NAME#'?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "Etes-vous sur de vouloir supprimer définitivement le dossier '#NAME#' ?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_FILE_CONFIRM"] = "Etes-vous sur de vouloir déplacer '#NAME#' dans la corbeille ?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_FOLDER_CONFIRM"] = "Etes-vous sur de vouloir déplacer '#NAME#' dans la corbeille ?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_GROUP_CONFIRM"] = "Êtes-vous sûr de vouloir supprimer définitivement les éléments sélectionnés ?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_TITLE"] = "Confirmation de suppression";
$MESS["DISK_TRASHCAN_TRASH_DESTROY_BUTTON"] = "Supprimer définitivement";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_BUTTON"] = "Vide";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_FOLDER_CONFIRM"] = "Etes-vous sur de vouloir vider la corbeille ?";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_TITLE"] = "Vider la Corbeille";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_BUTTON"] = "Restaurer";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FILE_CONFIRM"] = "Restaurer le document '#NAME#'?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FOLDER_CONFIRM"] = "Restaurer le fichier '#NAME#'?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_GROUP_CONFIRM"] = "Etes-vous sur de vouloir restaurer les éléments sélectionnés ?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_TITLE"] = "Confirmer la restauration";
