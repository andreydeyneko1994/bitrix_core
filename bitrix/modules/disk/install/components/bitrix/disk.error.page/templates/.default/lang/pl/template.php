<?php
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "Nie można uzyskać żądanych informacji. Możliwe, że nie masz uprawnień dostępu lub link jest uszkodzony.";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION_V2"] = "Upewnij się, że link jest prawidłowy lub poproś właściciela zasobu o dostęp";
$MESS["DISK_ERROR_PAGE_TITLE"] = "Niczego nie znaleziono.";
$MESS["DISK_ERROR_PAGE_TITLE_V2"] = "Odmowa dostępu lub nieprawidłowy link";
