<?php
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "No puede obtener la información solicitada. Es posible que usted no tenga permisos de acceso o el enlace está dañado.";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION_V2"] = "Asegúrese de que el enlace es válido o pida acceso al propietario del recurso";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_1"] = "- Intente comprobar los errores del enlace o póngase en contacto con el autor.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_2"] = "- Haga clic Atrás en su navegador para volver a la página anterior.";
$MESS["DISK_ERROR_PAGE_TITLE"] = "No se encontró nada.";
$MESS["DISK_ERROR_PAGE_TITLE_V2"] = "El acceso fue denegado o el enlace es incorrecto";
