<?php
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "Não consegui obter as informações que você solicitou. É possível que você não tenha permissão de acesso ou que o link não esteja funcionando.";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION_V2"] = "Certifique-se de que o link é válido ou peça acesso ao proprietário do recurso";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_1"] = "- Tente verificar o link em busca de erros ou entre em contato com o autor.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_2"] = "- Clique em Voltar no seu navegador para retornar à página anterior.";
$MESS["DISK_ERROR_PAGE_TITLE"] = "Nada foi encontrado.";
$MESS["DISK_ERROR_PAGE_TITLE_V2"] = "O acesso foi negado ou o link está incorreto";
