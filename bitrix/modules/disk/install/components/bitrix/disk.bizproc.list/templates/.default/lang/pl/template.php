<?php
$MESS["BPATT_ALL"] = "Razem";
$MESS["BPATT_AUTO_EXECUTE"] = "Autouruchamianie";
$MESS["BPATT_HELP1_TEXT"] = "Proces biznesowy oparty na stanach jest ciągłym procesem biznesowym rozdzielającym uprawnienia dostępu do obsługi dokumentów o różnych statusach.";
$MESS["BPATT_HELP2_TEXT"] = "Sekwencyjny proces biznesowy jest prostym procesem biznesowym, który przeprowadza serię następujących po sobie działań na dokumencie.";
$MESS["BPATT_MODIFIED"] = "Zmodyfikowany";
$MESS["BPATT_NAME"] = "Nazwa";
$MESS["BPATT_USER"] = "Zmodyfikowane przez";
$MESS["DISK_BIZPROC_LIST_CREATE_BP"] = "Utwórz";
$MESS["PROMPT_OLD_TEMPLATE"] = "Ostatnia aktualizacja Dysku sprawiła, że niektóre z szablonów procesów biznesowych stały się przestarzałe. Nadal możesz ich używać. Formularze edycji nowych szablonów będą jednak nieco inne. Starsze szablony są podświetlone.";
$MESS["WD_EMPTY"] = "Brak szblonów Procesów Biznesowych. Utwórz standardowy <a href=#HREF#>Proces Biznesowy</a>.";
$MESS["WD_EMPTY_NEW"] = "Jest nowa wersja szablonów Procesów biznesowych. <a href=#HREF#>Utwórz</a>.";
