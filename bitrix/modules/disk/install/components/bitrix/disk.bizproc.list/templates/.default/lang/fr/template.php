<?php
$MESS["BPATT_ALL"] = "Total";
$MESS["BPATT_AUTO_EXECUTE"] = "Autochargement";
$MESS["BPATT_HELP1_TEXT"] = "La procédure d'entreprise à statut est une procédure d'entreprise continue avec distribution de droits d'accès pour traiter les documents dans différents statuts.";
$MESS["BPATT_HELP2_TEXT"] = "La procédure d'entreprise séquentielle est une procédure d'entreprise simple qui effectue une série d'actions consécutives sur un document.";
$MESS["BPATT_MODIFIED"] = "Modifié";
$MESS["BPATT_NAME"] = "Nom";
$MESS["BPATT_USER"] = "Modifié par";
$MESS["DISK_BIZPROC_LIST_CREATE_BP"] = "Ajouter";
$MESS["PROMPT_OLD_TEMPLATE"] = "La récente mise à jour de Lecteur a rendu certains des modèles de processus d'affaires obsolètes. Vous pouvez toujours les utiliser. Les nouveaux formulaires d'édition de modèle seront légèrement différents, cependant. Les modèles existants sont mis en évidence.";
$MESS["WD_EMPTY"] = "Il n'y a pas de modèle de procédure d'entreprise. Créez un <a href=#HREF#>Business Processes</a> standard.";
$MESS["WD_EMPTY_NEW"] = "Il n'y a pas de nouveaux modèles Version Business Process.  <a href=#HREF#>Créer</a>.";
