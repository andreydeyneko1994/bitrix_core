<?
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "Plik został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DETACH"] = "Plik został odłączony";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "Plik został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED_2"] = "Plik przeniesiono do kosza";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "Folder został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DETACH"] = "Folder został odłączony";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "Folder został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED_2"] = "Folder przeniesiono do kosza";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_COPY_OBJECT"] = "Obiekt nie może zostać skopiowany";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Nie można było utworzyć ani odnaleźć publicznego linku do obiektu.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_OBJECT"] = "Nie można znaleźć obiektu";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_MOVE_OBJECT"] = "Obiekt nie może zostać przeniesiony";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_SAVE"] = "Błąd zapisywania.";
$MESS["DISK_FOLDER_LIST_ERROR_DISK_QUOTA"] = "Za mało miejsca na dysku, aby zakończyć działanie";
$MESS["DISK_FOLDER_LIST_ERROR_VALIDATE_BIZPROC"] = "Proszę wypełnić wszystkie wymagane pola.";
$MESS["DISK_FOLDER_LIST_LOCK_IS_DISABLED"] = "Blokowanie dokumentów jest wyłączone. Skontaktuj się z administratorem Bitrix24.";
$MESS["DISK_FOLDER_LIST_MESSAGE_CONNECT_GROUP_DISK"] = "Dysk grupy został pomyślnie połączony z twoim Bitrix24.Drive";
?>