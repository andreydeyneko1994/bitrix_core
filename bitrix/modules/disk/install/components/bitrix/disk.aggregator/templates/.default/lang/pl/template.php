<?
$MESS["DISK_AGGREGATOR_BTN_CLOSE"] = "Zamknij";
$MESS["DISK_AGGREGATOR_CREATE_STORAGE"] = "Dodaj Dysk";
$MESS["DISK_AGGREGATOR_DESCRIPTION"] = "Dokumenty do których ma dostęp użytkownik: prywatne dokumenty; udostepnione dokumenty innych użytkowników; dokumenty z grup roboczych.";
$MESS["DISK_AGGREGATOR_ND"] = "Mapuj dysk sieciowy";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE"] = "Aby mieć dostęp do wszystkich bibliotek jako struktury dyskowej, podłącz tę stronę jako dysk sieciowy.";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE_URL_TITLE"] = "Użyj tego adresu aby zmapować dysk sieciowy";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE"] = "Dysk sieciowy";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE_DESCR_MODAL"] = "Użyj tego adresu do połączenia";
?>