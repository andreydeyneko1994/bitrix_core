<?php
$MESS["DISK_DOCUMENTS_ERROR_1"] = "Você não é o proprietário. Acesso negado. ";
$MESS["DISK_DOCUMENTS_ERROR_2"] = "Entre em contato com o proprietário do documento para obter permissões de arquivo adicionais.";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "Documentos";
