<?php
$MESS["DISK_DOCUMENTS_ERROR_1"] = "Usted no es el propietario. Acceso denegado. ";
$MESS["DISK_DOCUMENTS_ERROR_2"] = "Comuníquese con el propietario del documento para obtener permisos de archivo adicionales.";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "Documentos";
