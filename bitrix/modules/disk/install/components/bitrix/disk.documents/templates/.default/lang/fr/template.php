<?php
$MESS["DISK_DOCUMENTS_ACT_DELETE_MESSAGE"] = "Voulez-vous déplacer \"#NAME#\" dans la Corbeille ?";
$MESS["DISK_DOCUMENTS_ACT_DELETE_OK_BUTTON"] = "Déplacer dans la Corbeille";
$MESS["DISK_DOCUMENTS_ACT_DELETE_TITLE"] = "Confirmer la suppression";
$MESS["DISK_DOCUMENTS_ACT_LOADING"] = "En cours de chargement...";
$MESS["DISK_DOCUMENTS_GRID_STUB_DESCRIPTION"] = "Modifiez. Discutez. Partagez.";
$MESS["DISK_DOCUMENTS_GRID_STUB_TITLE"] = "Créez de nouveaux documents, feuilles de calcul ou présentations pour collaborer avec vos collègues !";
$MESS["DISK_DOCUMENTS_GRID_TILE_EMPTY_BLOCK_CREATE"] = "Créer un fichier";
$MESS["DISK_DOCUMENTS_GRID_TILE_EMPTY_BLOCK_TITLE"] = "Ce dossier est vide";
$MESS["DISK_DOCUMENTS_GRID_TILE_EMPTY_BLOCK_TITLE_FOR_FILTERED"] = "Impossible de trouver des documents correspondant à vos filtres";
$MESS["DISK_DOCUMENTS_GRID_TILE_EMPTY_BLOCK_UPLOAD"] = "Télécharger fichier";
$MESS["DISK_DOCUMENTS_GRID_VIEW_LIST"] = "Liste";
$MESS["DISK_DOCUMENTS_GRID_VIEW_SMALL_TILE"] = "Grille";
$MESS["DISK_DOCUMENTS_GRID_VIEW_TILE"] = "Carreau";
$MESS["DISK_DOCUMENTS_MY_LIBRARY"] = "Mon Drive";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "Documents";
$MESS["DISK_DOCUMENTS_TOOLBAR_CHOOSE_DISK"] = "Depuis Bitrix24.Drive";
$MESS["DISK_DOCUMENTS_TOOLBAR_CREATE"] = "Créer";
$MESS["DISK_DOCUMENTS_TOOLBAR_CREATE_DOC"] = "Document";
$MESS["DISK_DOCUMENTS_TOOLBAR_CREATE_DOC_TEXT"] = "Nouveau document";
$MESS["DISK_DOCUMENTS_TOOLBAR_CREATE_PPT"] = "Présentation";
$MESS["DISK_DOCUMENTS_TOOLBAR_CREATE_XLS"] = "Feuille de calcul";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN"] = "Ouvrir";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN_DISK"] = "Depuis Bitrix24.Drive";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN_DROPBOX"] = "Depuis Dropbox";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN_GOOGLE_DOCS"] = "Depuis Google Docs";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN_LOCAL"] = "Depuis mon ordinateur";
$MESS["DISK_DOCUMENTS_TOOLBAR_OPEN_OFFICE_365"] = "Depuis Office 365";
$MESS["DISK_DOCUMENTS_TOOLBAR_SETTINGS"] = "Modifier les préférences";
$MESS["DISK_DOCUMENTS_TOOLBAR_TRASH"] = "Corbeille";
