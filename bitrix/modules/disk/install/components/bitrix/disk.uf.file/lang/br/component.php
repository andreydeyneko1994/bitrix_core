<?
$MESS["DISK_UF_ACTION_SAVE_TO_OWN_FILES"] = "Salvar no Bitrix24.Drive";
$MESS["SONET_GROUP_PREFIX"] = "Grupo: ";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "O arquivo foi anexado à mensagem.";
$MESS["WDUF_FILE_DOWNLOAD"] = "Baixar";
$MESS["WDUF_FILE_DOWNLOAD_ARCHIVE"] = "Baixar todos os arquivos como arquivo";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "está sendo editado no momento em #SERVICE#";
$MESS["WDUF_FILE_PREVIEW"] = "Pré-visualizar";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Histórico de revisão";
$MESS["WDUF_HISTORY_FILE"] = "Versão #NUMBER#:";
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Selecione arquivo neste computador";
$MESS["WD_LOCAL_COPY_ONLY"] = "Nesta mensagem";
$MESS["WD_MY_LIBRARY"] = "Minha unidade";
$MESS["WD_SAVED_PATH"] = "Salvo";
?>