<?
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE"] = "Pobierz z zewnętrznego dysku";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_DROPBOX"] = "Dropbox";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_GDRIVE"] = "Google Drive";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_OFFICE365"] = "Office 365";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_ONEDRIVE"] = "OneDrive";
$MESS["WDUF_CREATE_DOCX"] = "Dokument";
$MESS["WDUF_CREATE_IN_SERVICE"] = "Utwórz przy użyciu #SERVICE#";
$MESS["WDUF_CREATE_PPTX"] = "Prezentacja";
$MESS["WDUF_CREATE_XLSX"] = "Arkusz kalkulacyjny";
$MESS["WDUF_DROP_ATTACHMENTS"] = "Przeciągnij pliki tutaj, aby załadować";
$MESS["WDUF_SELECT_ATTACHMENTS"] = "Załaduj pliki i obrazy";
$MESS["WD_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["WD_FILE_EXISTS"] = "Plik o tej nazwie już istnieje. Można nadal użyć bieżącego folderu, w tym przypadku istniejąca wersja dokumentu zostanie zapisana w historii.";
$MESS["WD_FILE_LOADING"] = "Ładowanie";
$MESS["WD_SELECT_FILE_LINK"] = "Wybierz dokumenty z Bitrix24";
$MESS["WD_SELECT_FILE_LINK_ALT"] = "Dostępne biblioteki";
?>