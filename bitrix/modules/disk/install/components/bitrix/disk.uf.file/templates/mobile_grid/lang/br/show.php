<?php
$MESS["DISK_UF_FILE_DISABLE_AUTO_COMMENT"] = "Desabilitar comentário automático";
$MESS["DISK_UF_FILE_ENABLE_AUTO_COMMENT"] = "Habilitar comentário automático";
$MESS["DISK_UF_FILE_IS_DELETED"] = "(Excluído)";
$MESS["DISK_UF_FILE_MOBILE_GRID_FILES_MORE_LINK"] = "Mais arquivos: #NUM#";
$MESS["DISK_UF_FILE_MOBILE_GRID_TOGGLE_VIEW_GALLERY"] = "Exibir como galeria";
$MESS["DISK_UF_FILE_RUN_FILE_IMPORT"] = "Obtenha a última versão";
$MESS["DISK_UF_FILE_SETTINGS_DOCS"] = "Parâmetros de manuseio do documento";
$MESS["DISK_UF_FILE_STATUS_FAIL_LOADING"] = "Erro ao carregar a versão mais recente.";
$MESS["DISK_UF_FILE_STATUS_HAS_LAST_VERSION"] = "Você tem a versão mais recente.";
$MESS["DISK_UF_FILE_STATUS_PROCESS_LOADING"] = "Carregando";
$MESS["DISK_UF_FILE_STATUS_SUCCESS_LOADING"] = "A versão mais recente foi carregada com sucesso.";
$MESS["WDUF_FILES"] = "Arquivos:";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_MORE_ACTIONS"] = "Mais";
$MESS["WDUF_PHOTO"] = "Foto:";
