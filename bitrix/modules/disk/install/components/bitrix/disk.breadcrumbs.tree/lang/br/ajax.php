<?
$MESS["DISK_BREADCRUMBS_TREE_ERROR_COULD_NOT_FIND_FOLDER"] = "A pasta não pôde ser encontrada.";
$MESS["DISK_BREADCRUMBS_TREE_ERROR_COULD_NOT_FIND_OBJECT"] = "O objeto não pôde ser encontrado.";
$MESS["DISK_BREADCRUMBS_TREE_ERROR_COULD_NOT_MOVE_OBJECT"] = "O objeto não pôde ser movido.";
?>