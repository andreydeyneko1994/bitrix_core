<?
$MESS["BPABS_DESCRIPTION"] = "Descripción de plantilla de procesos de negocios";
$MESS["BPABS_DO_CANCEL"] = "Cancelar";
$MESS["BPABS_DO_START"] = "Inicio";
$MESS["BPABS_INVALID_TYPE"] = "El tipo de parámetro no está definido.";
$MESS["BPABS_MESSAGE_ERROR"] = "No se puede iniciar un tipo de proceso de negocio '#TEMPLATE#'.";
$MESS["BPABS_MESSAGE_SUCCESS"] = "Un '#TEMPLATE#' proceso de negocio se ha iniciado con éxito.";
$MESS["BPABS_NAME"] = "Nombre de plantilla de procesos de negocios";
$MESS["BPABS_NO"] = "No";
$MESS["BPABS_NO_TEMPLATES"] = "No se encontro la plantilla de procesos de negocio para este tipo de documento.";
$MESS["BPABS_TAB"] = "Procesos de negocios";
$MESS["BPABS_TAB_TITLE"] = "Ejecutar parámetros de procesos de negocios";
$MESS["BPABS_YES"] = "Si";
$MESS["DISK_TITLE_TEMPLATES_OLD"] = "Plantillas creadas antes de la conversión en el nuevo drive";
?>