<?
$MESS["DISK_VOLUME_CRM_COUNT"] = "Aktywności CRM";
$MESS["DISK_VOLUME_EXTERNAL_LINK_COUNT"] = "Odniesienia zewnętrzne";
$MESS["DISK_VOLUME_IM_AUTHOR"] = "Utworzone przez";
$MESS["DISK_VOLUME_IM_CALL"] = "Rozmowa telefoniczna";
$MESS["DISK_VOLUME_IM_CHAT"] = "Czat grupy";
$MESS["DISK_VOLUME_IM_CHAT_2"] = "Czat prywatny";
$MESS["DISK_VOLUME_IM_CHAT_OWNER"] = "Czat utworzony przez";
$MESS["DISK_VOLUME_IM_LINES"] = "Otwarty kanał";
$MESS["DISK_VOLUME_IM_LIVECHAT"] = "Otwarty kanał";
$MESS["DISK_VOLUME_IM_OPEN"] = "Czat publiczny";
$MESS["DISK_VOLUME_IM_PRIVATE"] = "Czat osoba-do-osoby osobami";
$MESS["DISK_VOLUME_MORE"] = "Szczegóły";
$MESS["DISK_VOLUME_OPEN"] = "Otwórz";
$MESS["DISK_VOLUME_PAGE_TITLE"] = "Oczyszczanie dysku";
$MESS["DISK_VOLUME_PARENT_FOLDER"] = "Folder nadrzędny";
$MESS["DISK_VOLUME_ROOT_FILES"] = "Pliki w folderze głównym";
$MESS["DISK_VOLUME_SHARING_COUNT"] = "Udostępniony plik";
$MESS["DISK_VOLUME_USING_CHAT"] = "Wiadomość czatu";
$MESS["DISK_VOLUME_USING_COUNT_END1"] = "miejsce";
$MESS["DISK_VOLUME_USING_COUNT_END2"] = "miejsce";
$MESS["DISK_VOLUME_USING_COUNT_END3"] = "miejsce";
$MESS["DISK_VOLUME_USING_COUNT_NONE"] = "Brak";
?>