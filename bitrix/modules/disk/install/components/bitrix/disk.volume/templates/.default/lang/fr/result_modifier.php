<?
$MESS["DISK_VOLUME_CRM_COUNT"] = "Activités CRM";
$MESS["DISK_VOLUME_EXTERNAL_LINK_COUNT"] = "Références externes";
$MESS["DISK_VOLUME_IM_AUTHOR"] = "Créé par";
$MESS["DISK_VOLUME_IM_CALL"] = "Appel téléphonique";
$MESS["DISK_VOLUME_IM_CHAT"] = "Chat de groupe";
$MESS["DISK_VOLUME_IM_CHAT_2"] = "Chat privé";
$MESS["DISK_VOLUME_IM_CHAT_OWNER"] = "Chat créé par";
$MESS["DISK_VOLUME_IM_LINES"] = "Canal ouvert";
$MESS["DISK_VOLUME_IM_LIVECHAT"] = "Canal ouvert";
$MESS["DISK_VOLUME_IM_OPEN"] = "Chat public";
$MESS["DISK_VOLUME_IM_PRIVATE"] = "Chat de personne à personne";
$MESS["DISK_VOLUME_MORE"] = "Détails";
$MESS["DISK_VOLUME_OPEN"] = "Ouvrir";
$MESS["DISK_VOLUME_PAGE_TITLE"] = "Nettoyage de lecteur";
$MESS["DISK_VOLUME_PARENT_FOLDER"] = "Dossier parent";
$MESS["DISK_VOLUME_ROOT_FILES"] = "Fichiers du dossier racine";
$MESS["DISK_VOLUME_SHARING_COUNT"] = "Fichier partagé";
$MESS["DISK_VOLUME_USING_CHAT"] = "Message de chat";
$MESS["DISK_VOLUME_USING_COUNT_END1"] = "espace";
$MESS["DISK_VOLUME_USING_COUNT_END2"] = "espaces";
$MESS["DISK_VOLUME_USING_COUNT_END3"] = "espaces";
$MESS["DISK_VOLUME_USING_COUNT_NONE"] = "Aucun";
?>