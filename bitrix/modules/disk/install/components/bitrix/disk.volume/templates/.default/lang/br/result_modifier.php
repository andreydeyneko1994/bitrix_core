<?
$MESS["DISK_VOLUME_CRM_COUNT"] = "Atividades de CRM";
$MESS["DISK_VOLUME_EXTERNAL_LINK_COUNT"] = "Referências externas";
$MESS["DISK_VOLUME_IM_AUTHOR"] = "Criado por";
$MESS["DISK_VOLUME_IM_CALL"] = "Chamada telefônica";
$MESS["DISK_VOLUME_IM_CHAT"] = "Bate-papo em grupo";
$MESS["DISK_VOLUME_IM_CHAT_2"] = "Bate-papo privado";
$MESS["DISK_VOLUME_IM_CHAT_OWNER"] = "Bate-papo criado por";
$MESS["DISK_VOLUME_IM_LINES"] = "Canal Aberto";
$MESS["DISK_VOLUME_IM_LIVECHAT"] = "Canal Aberto";
$MESS["DISK_VOLUME_IM_OPEN"] = "Bate-papo público";
$MESS["DISK_VOLUME_IM_PRIVATE"] = "Bate-papo de pessoa para pessoa";
$MESS["DISK_VOLUME_MORE"] = "Informações";
$MESS["DISK_VOLUME_OPEN"] = "Abrir";
$MESS["DISK_VOLUME_PAGE_TITLE"] = "Limpeza do Drive";
$MESS["DISK_VOLUME_PARENT_FOLDER"] = "Pasta principal";
$MESS["DISK_VOLUME_ROOT_FILES"] = "Arquivos na pasta raiz";
$MESS["DISK_VOLUME_SHARING_COUNT"] = "Arquivo compartilhado";
$MESS["DISK_VOLUME_USING_CHAT"] = "Mensagem de bate-papo";
$MESS["DISK_VOLUME_USING_COUNT_END1"] = "lugar";
$MESS["DISK_VOLUME_USING_COUNT_END2"] = "lugares";
$MESS["DISK_VOLUME_USING_COUNT_END3"] = "lugares";
$MESS["DISK_VOLUME_USING_COUNT_NONE"] = "Nenhuma";
?>