<?
$MESS["DISK_VOLUME_ACTION_DELETE_FILE"] = "Supprimer le fichier";
$MESS["DISK_VOLUME_ACTION_DELETE_FOLDER"] = "Supprimer le dossier";
$MESS["DISK_VOLUME_ACTION_DELETE_UNNECESSARY_VERSION"] = "Supprimez les versions de fichiers inutilisées";
$MESS["DISK_VOLUME_ACTION_EMPTY_FOLDER"] = "Supprimer le contenu du dossier";
$MESS["DISK_VOLUME_ACTION_SAFE_CLEAR"] = "Nettoyage sécurisé";
$MESS["DISK_VOLUME_ACTION_SEND"] = "Envoyer une notification";
$MESS["DISK_VOLUME_AGENT_STEPPER"] = "Nettoyage en cours";
$MESS["DISK_VOLUME_ATTACHED_COUNT"] = "Fichiers utilisés";
$MESS["DISK_VOLUME_CHOOSE_ACTION"] = "Sélectionnez une action";
$MESS["DISK_VOLUME_COUNT_FILES"] = "Nombre de fichiers";
$MESS["DISK_VOLUME_DELETED"] = "Déplacé dans la corbeille";
$MESS["DISK_VOLUME_DELETED_YES"] = "Oui";
$MESS["DISK_VOLUME_DISK_OBJECT"] = "Rechercher";
$MESS["DISK_VOLUME_DISK_TYPE"] = "Type de lecteur";
$MESS["DISK_VOLUME_FILE_TITLE"] = "Nom du fichier";
$MESS["DISK_VOLUME_FILE_VERSION_COUNT"] = "Compteur de version";
$MESS["DISK_VOLUME_FOLDER_SIZE"] = "Taille du dossier";
$MESS["DISK_VOLUME_FOLDER_TITLE"] = "Nom du dossier";
$MESS["DISK_VOLUME_GROUP_FILE_DROP"] = "Supprimer les fichiers";
$MESS["DISK_VOLUME_GROUP_FOLDER_DROP"] = "Supprimer les dossiers";
$MESS["DISK_VOLUME_GROUP_FOLDER_EMPTY"] = "Nettoyer les dossiers";
$MESS["DISK_VOLUME_IM_STORAGE_FAIL"] = "Le module Web Messenger n'est pas installé. Les opérations de stockage ne sont pas disponibles.";
$MESS["DISK_VOLUME_LINK_COUNT"] = "Nombre de références externes";
$MESS["DISK_VOLUME_OPEN"] = "Ouvrir";
$MESS["DISK_VOLUME_OPEN_HISTORY"] = "Ouvrir les versions de fichier";
$MESS["DISK_VOLUME_OTHER"] = "Autre";
$MESS["DISK_VOLUME_PERCENT"] = "Pourcentage";
$MESS["DISK_VOLUME_SIZE"] = "Total utilisé";
$MESS["DISK_VOLUME_SIZE_FILE"] = "Taille du fichier";
$MESS["DISK_VOLUME_STORAGE_OWNER"] = "Propriétaire";
$MESS["DISK_VOLUME_STORAGE_TITLE"] = "Nom du lecteur";
$MESS["DISK_VOLUME_TITLE"] = "Nom";
$MESS["DISK_VOLUME_TOP_COMMON"] = "Lecteurs partagés";
$MESS["DISK_VOLUME_TOP_FILE"] = "Fichiers volumineux";
$MESS["DISK_VOLUME_TOP_FOLDER"] = "Dossiers volumineux";
$MESS["DISK_VOLUME_TOP_GROUP"] = "Groupes de travail";
$MESS["DISK_VOLUME_TOP_TRASHCAN"] = "Corbeilles";
$MESS["DISK_VOLUME_TOP_UPLOADED"] = "Fichiers téléversés";
$MESS["DISK_VOLUME_TOP_USER"] = "Utilisateurs";
$MESS["DISK_VOLUME_TRASHCAN_SIZE"] = "Taille de la Corbeille";
$MESS["DISK_VOLUME_TYPE_FILE"] = "Type de fichier";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_COUNT"] = "Nombre de versions inutilisées";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_SIZE"] = "Taille des versions inutilisées";
$MESS["DISK_VOLUME_UPDATE_TIME"] = "Utilisation";
$MESS["DISK_VOLUME_USING_COUNT"] = "Utilisé dans";
$MESS["DISK_VOLUME_VERSION_COUNT"] = "Nombre de versions";
$MESS["DISK_VOLUME_VERSION_SIZE"] = "Taille de la version";
?>