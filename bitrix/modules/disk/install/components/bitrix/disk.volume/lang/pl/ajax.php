<?
$MESS["DISK_VOLUME_DATA_DELETED"] = "Dane zostały usunięte";
$MESS["DISK_VOLUME_DATA_DELETED_QUEUE"] = "Dane zostały usunięte. Krok #QUEUE_STEP# z #QUEUE_LENGTH#";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FILE"] = "Niewystarczające prawa dostępu.";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FOLDER"] = "Niewystarczające prawa dostępu do folderu.";
$MESS["DISK_VOLUME_FILE_DELETE_OK"] = "Plik został usunięty";
$MESS["DISK_VOLUME_FILE_VERSION_DELETE_OK"] = "Nieużywane wersje plików zostały usunięte";
$MESS["DISK_VOLUME_FOLDER_DELETE_OK"] = "Folder został usunięty";
$MESS["DISK_VOLUME_FOLDER_EMPTY_OK"] = "Zawartość folderów została usunięta";
$MESS["DISK_VOLUME_GROUP_FILE_DELETE_OK"] = "Plik został usunięty";
$MESS["DISK_VOLUME_GROUP_FILE_VERSION_DELETE_OK"] = "Nieużywane wersje plików zostały usunięte";
$MESS["DISK_VOLUME_NOTIFICATION_SEND_OK"] = "Ostrzeżenie zostało wysłane";
$MESS["DISK_VOLUME_NOTIFY_CHAT"] = "Uwaga! Pliki na czacie \"#TITLE#\" zajmują zbyt dużo miejsca na dysku (#FILE_SIZE#).";
$MESS["DISK_VOLUME_NOTIFY_FOLDER"] = "Uwaga! Twoje pliki w folderze <a href=\"#URL#\">\"#TITLE#\"</a> zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_RECOMMENDATION"] = "Porada: przenieś pliki na inne urządzenie pamięci masowej; <a href=\"#URL_TRASHCAN#\">opróżnij Kosz</a>; <a href=\"#URL_CLEAR#\">usuń nieużywane pliki</a>. Jeśli wkrótce nie zwolnisz miejsca, administrator może zwolnić miejsce na twoim dysku według własnego uznania.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_GROUP"] = "Uwaga! Twoje pliki na <a href=\"#URL#\">\"dysku\"</a> grupy roboczej #TITLE# zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_STORAGE"] = "Uwaga! Twoje pliki na dysku <a href=\"#URL#\">\"#TITLE#\"</a> zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_TRASHCAN"] = "Uwaga! Twoje pliki w<a href=\"#URL#\">Koszu</a> zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_UPLOADED"] = "Uwaga! Twoje pliki w folderze<a href=\"#URL#\">Załadowane pliki</a> zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_USER"] = "Uwaga! Twoje pliki na <a href=\"#URL#\">\"dysku\"</a> zajmują dużo miejsca: #FILE_SIZE#.";
$MESS["DISK_VOLUME_PERFORMING_QUEUE"] = "Zbieranie danych. Krok #QUEUE_STEP# z #QUEUE_LENGTH#";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_DELETE_OK"] = "Nieużywane wersje zostały usunięte";
?>