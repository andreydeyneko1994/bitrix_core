<?
$MESS["DISK_FILE_VIEW_BPABL_RES_1"] = "Nie";
$MESS["DISK_FILE_VIEW_BPABL_RES_2"] = "Zakończone Pomyślnie";
$MESS["DISK_FILE_VIEW_BPABL_RES_3"] = "Anuluowane";
$MESS["DISK_FILE_VIEW_BPABL_RES_4"] = "Błąd";
$MESS["DISK_FILE_VIEW_BPABL_RES_5"] = "Nie zainicjowano";
$MESS["DISK_FILE_VIEW_BPABL_RES_6"] = "Niezdefiniowany";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_1"] = "Zainicjowany";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_2"] = "W toku";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_3"] = "Anulowanie";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_4"] = "Zakończone";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_5"] = "Błąd";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_6"] = "Niezdefiniowany";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_1"] = "Uruchomione działanie '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_2"] = "Zakończone działanie '#ACTIVITY#', status zwrotny: '#STATUS#', wynik: '#RESULT#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_3"] = "Anulowane działanie '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_4"] = "Błąd działania '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_5"] = "Działanie '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_6"] = "Coś wykonywane na działaniu '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_COPY_LINK_TEXT"] = "Kopiuj link";
$MESS["DISK_FILE_VIEW_COPY_LINK_TITLE"] = "Kopiuj link do pliku";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "Obiekt nie może zostać znaleziony.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "Wersja nie może zostać znaleziona.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_SAVE_FILE"] = "Nie można zapisać pliku.";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_DAY"] = "Dni";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_HOUR"] = "Godziny";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_MIN"] = "Minuty";
$MESS["DISK_FILE_VIEW_FILE_DOWNLOAD_COUNT_BY_EXT_LINK"] = "Pliki do pobrania ze strony publicznej";
$MESS["DISK_FILE_VIEW_GO_BACK_TEXT"] = "Wstecz";
$MESS["DISK_FILE_VIEW_GO_BACK_TITLE"] = "Wstecz";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DELETE"] = "Usuń";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DOWNLOAD"] = "Pobierz";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_RESTORE"] = "Przywróć";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME"] = "Utworzony";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME_2"] = "Zmodyfikowany";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER"] = "Utworzony przez";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER_2"] = "Zmodyfikowane przez";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_FORMATTED_SIZE"] = "Rozmiar";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_NAME"] = "Nazwa";
?>