<?
$MESS["DISK_EXTERNAL_LINK_ERROR_DISABLED_MODE"] = "Linki publiczne są na tym portalu wyłączone.";
$MESS["DISK_EXTERNAL_OBJECT_ACT_COPY"] = "Kopiuj";
$MESS["DISK_EXTERNAL_OBJECT_ACT_DOWNLOAD"] = "Pobierz";
$MESS["DISK_EXTERNAL_OBJECT_ACT_MARK_DELETED"] = "Usuń";
$MESS["DISK_EXTERNAL_OBJECT_ACT_OPEN"] = "Otwórz";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_CREATE_TIME"] = "Utworzono";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_FORMATTED_SIZE"] = "Rozmiar";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_NAME"] = "Nazwa";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_UPDATE_TIME"] = "Zmodyfikowano";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_FORMATTED_SIZE"] = "Według rozmiaru";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_NAME"] = "Według nazwy";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_UPDATE_TIME"] = "Według daty";
?>