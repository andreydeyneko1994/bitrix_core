<?
$MESS["DISK_UF_ATTACHED_ACTION_SAVE_TO_OWN_FILES"] = "Zapisz na dysku Bitrix24";
$MESS["SONET_GROUP_PREFIX"] = "Grupa: ";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "Plik został załączony do wiadomości.";
$MESS["WDUF_FILE_DOWNLOAD"] = "Pobierz";
$MESS["WDUF_FILE_EDIT"] = "Edytuj";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "jest właśnie edytowany w #SERVICE#";
$MESS["WDUF_FILE_PREVIEW"] = "Podgląd";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Historia zmian";
$MESS["WDUF_HISTORY_FILE"] = "Wersja #NUMBER#:";
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Wybierz plik na tym komputerze";
$MESS["WD_LOCAL_COPY_ONLY"] = "W tej wiadomości";
$MESS["WD_MY_LIBRARY"] = "Mój Dysk";
$MESS["WD_SAVED_PATH"] = "Zapisano";
?>