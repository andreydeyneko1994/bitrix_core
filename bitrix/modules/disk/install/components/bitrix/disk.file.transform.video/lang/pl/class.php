<?
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_DESC"] = "błąd konwersji";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TITLE"] = "Coś poszło nie tak...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM"] = "Konwertuj ponownie";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_ALLOWED"] = "Niewystarczające uprawnienia";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_INSTALLED"] = "Nie zainstalowano modułu „Konwertera plików”.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_TRANSFORMED"] = "Ten film został już przekonwertowany.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_DESC"] = "to zajmie trochę czasu...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_TITLE"] = "Film będzie dostępny po przekonwertowaniu";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_DESC"] = "Konwersji możesz dokonać teraz";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TITLE"] = "Ten film nie został jeszcze przekonwertowany";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TRANSFORM"] = "Konwertuj film";
?>