<?
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_DESC"] = "erreur de conversion";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TITLE"] = "Un problème est survenu...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM"] = "Convertir de nouveau";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_ALLOWED"] = "Permissions insuffisantes";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_INSTALLED"] = "Le module \"Convertisseur de fichiers\" n'est pas installé.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_TRANSFORMED"] = "Cette vidéo a déjà été convertie.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_DESC"] = "cela prendra du temps...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_TITLE"] = "La vidéo sera disponible après sa conversion";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_DESC"] = "Vous pouvez la convertir maintenant";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TITLE"] = "Cette vidéo n'a pas encore été convertie";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TRANSFORM"] = "Convertir la vidéo";
?>