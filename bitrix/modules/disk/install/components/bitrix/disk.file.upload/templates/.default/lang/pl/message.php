<?
$MESS["DFU_CANCEL"] = "Anuluj";
$MESS["DFU_CLOSE"] = "Zamknij";
$MESS["DFU_DND1"] = "Załaduj plik lub obraz";
$MESS["DFU_DND2"] = "Dodaj używając przeciągania";
$MESS["DFU_DUPLICATE"] = "Plik o tej nazwie już istnieje.";
$MESS["DFU_REPLACE"] = "Zastąp";
$MESS["DFU_SAVE_BP"] = "Zapisz parametry";
$MESS["DFU_SAVE_BP_DIALOG"] = "Załaduj";
$MESS["DFU_UPLOAD"] = "Załaduj więcej";
$MESS["DFU_UPLOAD_CANCELED"] = "Anulowane ładowanie";
$MESS["DFU_UPLOAD_TITLE1"] = "Załaduj nowy dokument";
$MESS["DFU_UPLOAD_TITLE2"] = "Załadowano #NUMBER# plików z #COUNT#";
?>