<?
$MESS["STATWIZ_IMPORT_ALL_DONE"] = "La création de l'index est terminée.";
$MESS["STATWIZ_IMPORT_ERROR_ACCESS_DENIED"] = "Erreur ! Il n'y a pas d'accès.";
$MESS["STATWIZ_IMPORT_ERROR_FILE"] = "Erreur ! Nom de fichier incorrect.";
$MESS["STATWIZ_IMPORT_FILE_LOADING"] = "Nous sommes en train de traiter le fichier...";
?>