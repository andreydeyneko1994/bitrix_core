<?
$MESS["STATWIZ_CANCELSTEP_BUTTONTITLE"] = "Fermer";
$MESS["STATWIZ_CANCELSTEP_CONTENT"] = "Le travail de l'assistant a été interrompu.";
$MESS["STATWIZ_CANCELSTEP_TITLE"] = "Le travail de l'assistant est suspendu";
$MESS["STATWIZ_FILES_NOT_FOUND"] = "Fichier approprié introuvable. Veuillez télécharger les fichiers à partir du site www.maxmind.com ipgeobase.ru ou ip-à-country.webhosting.info dans le répertoire indiqué ci-dessus et essayez de relancer l'assistant.";
$MESS["STATWIZ_FINALSTEP_BUTTONTITLE"] = "Prêt";
$MESS["STATWIZ_FINALSTEP_CITIES"] = "Des villes : #COUNT#.";
$MESS["STATWIZ_FINALSTEP_CITY_IPS"] = "IP de plages : #COUNT#.";
$MESS["STATWIZ_FINALSTEP_COUNTRIES"] = "Nombre de pays : #COUNT#.";
$MESS["STATWIZ_FINALSTEP_TITLE"] = "Le travail du master est terminé";
$MESS["STATWIZ_NO_MODULE_ERROR"] = "Le module de la statistique n'a pas été installé. Le fonctionnement de l'assistant va s'interrompre.";
$MESS["STATWIZ_STEP1_CITY"] = "Création d'un index pour déterminer le <b>Pays</b> et la <b>Ville</b> à l'aide d'adresse IP.";
$MESS["STATWIZ_STEP1_CITY_NOTE"] = "Compatible avec les formats suivants:
<ul>
<li><a target='_blank' href='#GEOIP_HREF#'>GeoIP City</a>.</li>
<li><a target='_blank' href='#GEOIPLITE_HREF#'>GeoLite City</a>.</li>
<li><a target='_blank' href='#IPGEOBASE_HREF#'>IpGeoBase</a>.</li>
</ul>";
$MESS["STATWIZ_STEP1_COMMON_NOTE"] = "Les fichiers téléchargés et décompressés doivent être placés dans le répertoire #PATH#. Ensuite vous pouvez passer à l'étape suivante de l'assistant.";
$MESS["STATWIZ_STEP1_CONTENT"] = "L'Assistant de création de codes postaux pour déterminer le pays et la ville par adresse IP vous salue.<br />Sélectionnez une des options suivantes.:";
$MESS["STATWIZ_STEP1_COUNTRY"] = "Création d'un index pour déterminer la <b>Ville</b> à l'aide d'adresse IP.";
$MESS["STATWIZ_STEP1_COUNTRY_NOTE_V2"] = "Formats pris en charge :
<ul>
<li><a target=\"_blank\" href=\"#GEOIP_HREF#\">GeoIP Country</a>.</li>
<li><a target=\"_blank\" href=\"#GEOIPLITE_HREF#\">GeoLite Country</a>.</li>
</ul>";
$MESS["STATWIZ_STEP1_TITLE"] = "Assistant de la création d'index";
$MESS["STATWIZ_STEP2_CITY_CHOOSEN"] = "La création de l'index a été choisie pour déterminer <b>le pays</b> et <b>la ville</b> par adresse IP.";
$MESS["STATWIZ_STEP2_CONTENT"] = "La recherche des fichiers appropriés a été faite dans le répertoire '/bitrix/modules/statistic/ip2country'.";
$MESS["STATWIZ_STEP2_COUNTRY_CHOOSEN"] = "Vous avez choisi la création du code postal pour la détermination <b>du pays</b> à l'aide d'adresse IP.";
$MESS["STATWIZ_STEP2_DESCRIPTION"] = "Description";
$MESS["STATWIZ_STEP2_FILE_ERROR"] = "Le fichier à charger n'est pas indiqué.";
$MESS["STATWIZ_STEP2_FILE_NAME"] = "Nom du fichier";
$MESS["STATWIZ_STEP2_FILE_SIZE"] = "Taille";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE"] = "Base de données des blocs des adresses IP IpGeoBase (uniquement la Russie). Pour la localisation du pays veuillez charger tout d'abord l'index des pays.";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE2"] = "La deuxième partie de la base de données des blocs d'adresses IP ipGeoBase contient des correspondances entre des blocs d'adresses IP et des blocs d'emplacements. Elle doit être chargée après la première partie.";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE2_CITY"] = "La première partie de la base de données des blocs d'adresses IP IpGeoBase contient les emplacements. Pour déterminer le pays, charger d'abord les indicatifs des pays.";
$MESS["STATWIZ_STEP2_FILE_TYPE_IP_TO_COUNTRY"] = "la base de données ip-to-country.";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_CITY_LOCATION"] = "Première partie de la base de données GeoIP City ou GeoLite City. Comprend les emplacements.";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_IP_COUNTRY"] = "La base de données GeoIP Country ou GeoLite Country.";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_IP_LOCATION"] = "La deuxième partie de la base de données GeoIP City ou GeoLite City contient des correspondances de blocs d'adresses IP et des emplacements. Elle doit être chargée après la première partie.";
$MESS["STATWIZ_STEP2_FILE_TYPE_UNKNOWN"] = "Inconnu ID de langue.";
$MESS["STATWIZ_STEP2_TITLE"] = "Sélection des fichiers CSV";
$MESS["STATWIZ_STEP3_LOADING"] = "Le traitement est en cours";
$MESS["STATWIZ_STEP3_TITLE"] = "Création de l'index en cours.";
?>