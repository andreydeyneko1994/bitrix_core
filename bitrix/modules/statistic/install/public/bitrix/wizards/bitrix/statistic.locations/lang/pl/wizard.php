<?
$MESS["STATWIZ_CANCELSTEP_BUTTONTITLE"] = "Zamknij";
$MESS["STATWIZ_CANCELSTEP_CONTENT"] = "Kreator został anulowany.";
$MESS["STATWIZ_CANCELSTEP_TITLE"] = "Kreator został anulowany";
$MESS["STATWIZ_FINALSTEP_BUTTONTITLE"] = "Zakończ";
$MESS["STATWIZ_STEP1_CITY"] = "Indeks adresu IP dla wyszukiwania <b>Miasta</b> i <b>Kraju</b>.";
$MESS["STATWIZ_STEP1_COMMON_NOTE"] = "Rozpakuj archiwa i załaduj pliki do katalogu #PATH#. Następnie można przejść do wykonywania następnego kroku kreatora.";
$MESS["STATWIZ_STEP1_CONTENT"] = "Witaj w kreatorze tworzenia idneksu adresu IP! Ten kreator pomoże ci utworzyć indeks adresu IP dla wyszukiwania miast i krajów.";
$MESS["STATWIZ_STEP1_COUNTRY"] = "Indeks adresu IP dla wyszukiwania <b>Miasta</b>.";
$MESS["STATWIZ_STEP2_CITY_CHOOSEN"] = "Wybrałeś utworzenie indeksu adresu IP dla wyszukiwania <b>Kraju</b> i <b>Miasta</b>.";
$MESS["STATWIZ_STEP2_COUNTRY_CHOOSEN"] = "Wybrałeś utworzenie indeksu adresu IP dla wyszukiwania <b>Kraju</b>";
$MESS["STATWIZ_STEP2_DESCRIPTION"] = "Opis";
$MESS["STATWIZ_STEP2_FILE_ERROR"] = "Brak pliku do załadowania.";
$MESS["STATWIZ_STEP2_FILE_NAME"] = "Nazwa pliku";
$MESS["STATWIZ_STEP2_FILE_SIZE"] = "Rozmiar";
$MESS["STATWIZ_STEP3_LOADING"] = "Przetwarzanie…";
?>