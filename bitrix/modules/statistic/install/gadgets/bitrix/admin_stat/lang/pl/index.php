<?
$MESS["GD_STAT_B_YESTERDAY"] = "Dwa dni wcześniej";
$MESS["GD_STAT_EVENTS"] = "Wydarzenia";
$MESS["GD_STAT_HITS"] = "Trafienia";
$MESS["GD_STAT_SESSIONS"] = "Sesje";
$MESS["GD_STAT_TAB_COMMON"] = "Popularność";
$MESS["GD_STAT_TAB_EVENT"] = "Wydarzenia";
$MESS["GD_STAT_TAB_PHRASE"] = "frazy";
$MESS["GD_STAT_TAB_REF"] = "Strony Internetowe";
$MESS["GD_STAT_TODAY"] = "dzisiaj";
$MESS["GD_STAT_TOTAL"] = "Razem";
$MESS["GD_STAT_VISITORS"] = "Odwiedzający";
$MESS["GD_STAT_YESTERDAY"] = "wczoraj";
?>