<?
$MESS["GD_STAT_ADV_NAME"] = "Kampania reklamowa";
$MESS["GD_STAT_BEFORE_YESTERDAY"] = "Dwa dni wcześniej";
$MESS["GD_STAT_EVENT"] = "Wydarzenia";
$MESS["GD_STAT_EVENT_GRAPH"] = "Wykres typu dziennych wydarzeń";
$MESS["GD_STAT_NO_DATA"] = "Brak danych.";
$MESS["GD_STAT_PHRASE"] = "słowo klucz";
$MESS["GD_STAT_SERVER"] = "Strona internetowa";
$MESS["GD_STAT_TODAY"] = "dzisiaj";
$MESS["GD_STAT_TOTAL_1"] = "Razem";
$MESS["GD_STAT_YESTERDAY"] = "wczoraj";
?>