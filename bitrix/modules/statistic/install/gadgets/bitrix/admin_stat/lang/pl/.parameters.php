<?
$MESS["GD_STAT_P_EVENT"] = "Wydarzenia";
$MESS["GD_STAT_P_GRAPH_HEIGHT"] = "Wysokość";
$MESS["GD_STAT_P_GRAPH_PARAMS"] = "Parametry wykresu";
$MESS["GD_STAT_P_GRAPH_WIDTH"] = "Szerokość";
$MESS["GD_STAT_P_GUEST"] = "Odwiedzający";
$MESS["GD_STAT_P_HIDE_GRAPH"] = "Ukryj wykres";
$MESS["GD_STAT_P_HIT"] = "Trafienia";
$MESS["GD_STAT_P_SESSION"] = "Sesje";
$MESS["GD_STAT_P_SITE_ID"] = "Strona internetowa";
$MESS["GD_STAT_P_SITE_ID_ALL"] = "Wszystkie";
?>