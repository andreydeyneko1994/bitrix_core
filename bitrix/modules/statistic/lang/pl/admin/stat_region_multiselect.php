<?
$MESS["STAT_REGION_MSEL_COUNTRY_ID"] = "Kod kraju";
$MESS["STAT_REGION_MSEL_COUNTRY_NAME"] = "Skrót kraju";
$MESS["STAT_REGION_MSEL_COUNTRY_SHORT_NAME"] = "Pełna nazwa kraju";
$MESS["STAT_REGION_MSEL_FIND"] = "Znajdź";
$MESS["STAT_REGION_MSEL_PAGES"] = "Województwa/Gminy";
$MESS["STAT_REGION_MSEL_REGION_NAME"] = "Województwo/Gmina";
$MESS["STAT_REGION_MSEL_SELECT"] = "wybierz";
$MESS["STAT_REGION_MSEL_SELECT_TITLE"] = "Dodaj wybrane regiony";
$MESS["STAT_REGION_MSEL_TITLE"] = "Województwa/Gminy";
?>