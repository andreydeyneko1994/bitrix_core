<?
$MESS["STAT_ADV_LIST"] = "Lista kampanii reklamowych";
$MESS["STAT_ALL_DYNAMICS"] = "Dynamika kampanii reklamowej";
$MESS["STAT_ALL_GRAPHICS"] = "Wykres kampanii reklamowej";
$MESS["STAT_BACK"] = "Wstecz";
$MESS["STAT_DYNAMIC"] = "Dynamika kampanii reklamowej # #ID#";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_F_SELECT_EVENTS"] = "Wydarzenia";
$MESS["STAT_F_SET_PERIOD"] = "Stwórz wykres";
$MESS["STAT_GRAPH_1"] = "Wykres popularności";
$MESS["STAT_GRAPH_2"] = "Wykres wydarzeń";
$MESS["STAT_GUESTS"] = "Odwiedzający";
$MESS["STAT_HITS"] = "Trafienia";
$MESS["STAT_INCORRECT_ADV_ID"] = "Nie znaleziono kampanii reklamowej";
$MESS["STAT_NEW_GUESTS"] = "nowi goście";
$MESS["STAT_NOT_ENOUGH_DATA"] = "Brak wystarczającej ilości danych do stworzenia wykresu";
$MESS["STAT_RECORDS_LIST"] = "Wykres dla kampanii reklamowej # #ID#";
$MESS["STAT_SESSIONS"] = "Sesje";
$MESS["STAT_SHOW"] = "Wykresy będą zawierać";
$MESS["STAT_STRAIGHT"] = "Prosto";
?>