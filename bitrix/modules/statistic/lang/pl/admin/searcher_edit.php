<?
$MESS["STAT_ACTIVE"] = "Aktywny:";
$MESS["STAT_CHAR_SET"] = "Odkodowanie szukanej frazy";
$MESS["STAT_CHECK_ACTIVITY"] = "Sprawdź limit aktywności:";
$MESS["STAT_DELETE_SEARCHER"] = "Usuń silnik wyszukiwania";
$MESS["STAT_DELETE_SEARCHER_CONFIRM"] = "NA pewno chcesz usunąć ten silnik wyszukiwania?";
$MESS["STAT_DOMAIN"] = "Domena [%_]";
$MESS["STAT_DYNAMIC_KEEP_DAYS"] = "Dni do trzymania dynamiki:";
$MESS["STAT_EDIT_RECORD"] = "Ustawienia dla silnik wyszukiwarki # #ID#";
$MESS["STAT_FORM_ERROR_SAVE"] = "Błąd zapisu";
$MESS["STAT_HIT_KEEP_DAYS"] = "Dni do trzymania trafień:";
$MESS["STAT_NAME"] = "Nazwa:";
$MESS["STAT_NEW_RECORD"] = "Nowy silnik wyszukiwarki";
$MESS["STAT_NEW_SEARCHER"] = "Nowy silnik wyszukiwarki";
$MESS["STAT_PIE_CHART"] = "Zawiera w tabeli indeksowania i wykresie<br> domyślnie:";
$MESS["STAT_PROP_TITLE"] = "Parametry silnika wyszukiwarki";
$MESS["STAT_RECORDS_LIST"] = "Lista silników wyszukiwarki";
$MESS["STAT_SEARCHER_DOMAINS"] = "Domeny:";
$MESS["STAT_STATISTICS"] = "Zachowaj trafienia:";
$MESS["STAT_USER_AGENT"] = "Aplikacja użytkownika";
$MESS["STAT_VARIABLE"] = "Zmienne<br>(oddzielane przecinkiem)";
?>