<?
$MESS["STATM_ACTIVITY"] = "Aktywne";
$MESS["STATM_ADV"] = "Kampanie reklamowe";
$MESS["STATM_ADVLIST"] = "Kampanie reklamowe";
$MESS["STATM_ADV_ALT"] = "Lista kampanii reklamowych";
$MESS["STATM_ADV_ANALYSIS"] = "Porównanie kampanii";
$MESS["STATM_ADV_ANALYSIS_ALT"] = "Analiza porównawcza kampanii reklamowych";
$MESS["STATM_ATTENTIVENESS"] = "Uwaga";
$MESS["STATM_ATTENTIVENESS_ALT"] = "Wykresy i tabele gości strony";
$MESS["STATM_AUTODETECT"] = "Autowykrywanie";
$MESS["STATM_AUTODETECT_ALT"] = "Autowykrywanie nieznanej aplikacji użytkownika";
$MESS["STATM_BY_SEARCHERS"] = "Według silnika wyszukiwarki";
$MESS["STATM_CITY_GEOGRAPHY"] = "Ruch oparty na miastach";
$MESS["STATM_CITY_GEOGRAPHY_ALT"] = "Ruch na stronie oparty na miastach";
$MESS["STATM_COUNTRY_GEOGRAPHY"] = "Ruch oparty na krajach";
$MESS["STATM_COUNTRY_GEOGRAPHY_ALT"] = "Ruch na stronie oparty na krajach";
$MESS["STATM_DIAGRAM"] = "Wykres kołowy";
$MESS["STATM_DIAGRAM_ALT"] = "Wykres kołowy typu wydarzenia";
$MESS["STATM_DYN"] = "Dynamika";
$MESS["STATM_DYN_D"] = "Codziennie";
$MESS["STATM_DYN_ENT"] = "Punkty wejścia";
$MESS["STATM_DYN_EX"] = "Punkty wyjścia";
$MESS["STATM_DYN_H"] = "co godzinę";
$MESS["STATM_DYN_M"] = "Co miesiąc";
$MESS["STATM_DYN_W"] = "Dniami tygodnia";
$MESS["STATM_EVENTS"] = "Wydarzenia";
$MESS["STATM_EVENTS_ALT"] = "Lista wydarzeń";
$MESS["STATM_EVENTS_LOADING"] = "Ładowanie wydarzenia";
$MESS["STATM_EVENTS_LOADING_ALT"] = "Ładowanie zewnętrznych wydarzeń (np. zlecenie płatności w innej firmie)";
$MESS["STATM_EVENT_TYPE"] = "Typy wydarzenia";
$MESS["STATM_EVENT_TYPE_ALT"] = "Typy wydarzenia definiowane przez zmienne wydarzenie1, wydarzenie2";
$MESS["STATM_FULL"] = "Pełne ścieżki";
$MESS["STATM_GRAPH"] = "Wykresy";
$MESS["STATM_GRAPH_ALT"] = "Wykres indeksowania strony\"";
$MESS["STATM_GRAPH_FULL"] = "Wykres";
$MESS["STATM_GRAPH_FULL_ALT"] = "Wykres typu wydarzenia";
$MESS["STATM_GUESTS"] = "Odwiedzający";
$MESS["STATM_GUESTS_ALT"] = "Lista gości strony";
$MESS["STATM_GUEST_LIST"] = "Lista Gości";
$MESS["STATM_HITS"] = "Trafienia";
$MESS["STATM_HITS_ALT"] = "Lista stron trafień gości";
$MESS["STATM_INT_SEARCH"] = "Wewnętrzne wyszukiwanie";
$MESS["STATM_LIST"] = "Lista";
$MESS["STATM_NEWG"] = "nowi goście";
$MESS["STATM_ONLINE_ALT"] = "Goście strony obecnie online";
$MESS["STATM_OTR"] = "Gałęzie ścieżki";
$MESS["STATM_PAGES"] = "Strony";
$MESS["STATM_PHRASES"] = "Szukaj Słów Kluczowych";
$MESS["STATM_PHRASES_ALT"] = "Lista kluczowych słów wyszukiwania";
$MESS["STATM_PHRASES_LIST"] = "frazy";
$MESS["STATM_REFENT_TITLE"] = "Odwiedziny z innych stron";
$MESS["STATM_REFERERS"] = "Powiązane strony";
$MESS["STATM_REFERERS_ALT"] = "Lista powiązanych stron";
$MESS["STATM_SEARCHERS"] = "Silniki wyszukiwarki";
$MESS["STATM_SEARCHERS_ALT"] = "Lista silników wyszukiwarki";
$MESS["STATM_SEARCHERS_ENTRY"] = "Trafienia wyszukiwarki";
$MESS["STATM_SEARCHERS_ENTRY_LIST"] = "Trafienia";
$MESS["STATM_SEARCHERS_HITS"] = "Trafienia wyszukiwarki";
$MESS["STATM_SEARCHERS_HITS_ALT"] = "Lista stron indeksowanych przez wyszukiwarkę";
$MESS["STATM_SEARCHERS_LIST"] = "Indeksacja";
$MESS["STATM_SEARCHER_DIAGRAM_ALT"] = "Wykres silnika wyszukiwarki";
$MESS["STATM_SEARCH_ENT"] = "Odwiedziny";
$MESS["STATM_SESS"] = "Sesje";
$MESS["STATM_SESSIONS"] = "Sesje gości";
$MESS["STATM_SESSIONS_ALT"] = "Lista stron sesji gości";
$MESS["STATM_SESSION_DURATION"] = "Czas trwania sesji";
$MESS["STATM_SITES"] = "Strony";
$MESS["STATM_SITE_PATH"] = "Ścieżki strony";
$MESS["STATM_SITE_PATH_ALT"] = "Wykres ścieżki strony";
$MESS["STATM_STATISTIC"] = "Statystyki łączne";
$MESS["STATM_STATISTIC_ALT"] = "Przegląd podsumowania statystyk strony";
$MESS["STATM_STOPLIST"] = "zatrzymaj listę";
$MESS["STATM_TRAFFIC"] = "Ruch na stronie\"";
$MESS["STATM_TRAFFIC_ALT"] = "Wykres ruchu według dni, dnitygodnia, godzin, miesięcy";
$MESS["STATM_TRAF_TITLE"] = "Statystyki ruchu";
$MESS["STATM_VISITS_SECTION"] = "Popularność strony";
$MESS["STATM_VISITS_SECTION_ALT"] = "Sekcja strony i wykres popularności strony";
?>