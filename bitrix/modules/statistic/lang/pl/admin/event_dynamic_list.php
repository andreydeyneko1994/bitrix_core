<?
$MESS["STAT_CHOOSE_BTN"] = "Wybierz...";
$MESS["STAT_COUNTER"] = "Licz";
$MESS["STAT_DATE"] = "Data";
$MESS["STAT_EVENT_DYN_PAGES"] = "Wyniki";
$MESS["STAT_F_EVENT_ID"] = "Typ wydarzenia:";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_GRAPH"] = "Dzienny wykres typu wydarzenia";
$MESS["STAT_MNU_GRAPH"] = "Wykres";
$MESS["STAT_RECORDS_LIST"] = "Dzienna dynamika typu wydarzenia";
$MESS["STAT_TOTAL_TIME"] = "Łącznie dni:";
?>