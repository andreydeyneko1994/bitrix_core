<?
$MESS["STAT_ADV_DYN_PAGES"] = "dni";
$MESS["STAT_ADV_LIST"] = "Lista kampanii reklamowych";
$MESS["STAT_BACK"] = "Powrót";
$MESS["STAT_BACK_ALT"] = "zwrócone po bezpośrendim trafieniu w kampanii reklamowej";
$MESS["STAT_DATE"] = "Data";
$MESS["STAT_EVENTS"] = "Wydarzenia";
$MESS["STAT_EVENTS_LIST"] = "Lista wydarzeń";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_GRAPH"] = "Wykresy kampanii reklaowej";
$MESS["STAT_GUESTS"] = "Odwiedzający";
$MESS["STAT_HITS"] = "Trafienia";
$MESS["STAT_INCORRECT_ADV_ID"] = "Nie znaleziono kampanii reklamowej";
$MESS["STAT_NEW"] = "Nowe";
$MESS["STAT_NEW_ALT"] = "Nowi goście strony będący na niej pierwszy raz";
$MESS["STAT_RECORDS_LIST"] = "Dynamika kampanii reklamowej";
$MESS["STAT_SESSIONS"] = "Sesje";
$MESS["STAT_STRAIGHT"] = "bezpośrednie";
$MESS["STAT_STRAIGHT_ALT"] = "bezpośrednie trafienie w kampanii reklamowej";
$MESS["STAT_VIEW_EVENT_LIST"] = "Idź do listy wydarzeń";
$MESS["STAT_VIEW_EVENT_LIST_BACK"] = "Powrót do listy wydarzeń";
?>