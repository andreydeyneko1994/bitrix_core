<?
$MESS["STAT_DYNAMIC_GRAPH"] = "Dla danego okresu czasu";
$MESS["STAT_EVENTS"] = "Wydarzenia";
$MESS["STAT_F_COUNTRY_ID"] = "Kraj";
$MESS["STAT_F_DATA_TYPE"] = "typ danych";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_HITS"] = "Trafienia";
$MESS["STAT_NEW_GUESTS"] = "nowi goście";
$MESS["STAT_NO_DATA"] = "Brak wystarczających danych do stworzenia diagramu";
$MESS["STAT_RECORDS_LIST"] = "Ruch oparty na krajach";
$MESS["STAT_SESSIONS"] = "Sesje";
$MESS["STAT_STATIC_GRAPH"] = "Dla całego czasu trzymania statystyk";
?>