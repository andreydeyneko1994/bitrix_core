<?
$MESS["STAT_ADDED_BROWSERS"] = "Dodana maska przeglądarki:\"";
$MESS["STAT_ADDED_SEARCHERS"] = "Dodana maska silnika wyszukiwarki:";
$MESS["STAT_ADD_AS_BROWSER"] = "Dodaj jako przeglądarkę";
$MESS["STAT_ADD_AS_SEARCHER"] = "Dodaj jako silnik wysuzkiwarki";
$MESS["STAT_BROWSER"] = "Przeglądarka";
$MESS["STAT_FL_DAY"] = "Ramy czasowe zapytania";
$MESS["STAT_FL_SESS"] = "Liczba sesji";
$MESS["STAT_F_COUNTER"] = "Sesje:";
$MESS["STAT_F_LAST_DAY"] = "Ramy czasowe zapytania";
$MESS["STAT_F_USER_AGENT"] = "Aplikacja użytkownika";
$MESS["STAT_MASK"] = "Maskuj aby dodać [%_]";
$MESS["STAT_RECORDS_LIST"] = "Wykrycie nieznanej aplikacji użytkownika";
$MESS["STAT_SEARCHER"] = "Silnik wyszukiwarki";
$MESS["STAT_SESSIONS"] = "Sesje";
$MESS["STAT_SESS_LIST"] = "Lista sesji";
$MESS["STAT_USER_AGENT"] = "Aplikacja użytkownika";
$MESS["STAT_USER_AGENT_PAGES"] = "Aplikacja użytkownika";
?>