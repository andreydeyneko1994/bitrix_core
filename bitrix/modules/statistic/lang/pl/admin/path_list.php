<?
$MESS["STAT_ADV_BACK"] = "po zwrotach";
$MESS["STAT_ADV_NO_BACK"] = "po bezpośrednim trafieniu";
$MESS["STAT_ADV_SUMMA"] = "wszystkie dla bezpośrednich trafień i zawrotów";
$MESS["STAT_ENTER_POINTS_S"] = "Punkt startowy ścieżki";
$MESS["STAT_F_ADV"] = "Kampanie reklamowe";
$MESS["STAT_F_ADV_DATA_TYPE"] = "Rodzaj danych dla kampanii reklamowych";
$MESS["STAT_F_FIRST_PAGE"] = "Pierwsza strona w ścieżce";
$MESS["STAT_F_FULL_PATH"] = "Pełne ścieżki";
$MESS["STAT_F_LAST_PAGE"] = "Ostatnia strona ścieżki";
$MESS["STAT_F_PAGE"] = "Strona ścieżki";
$MESS["STAT_F_SEGMENT_PATH"] = "Ścieżki kroków";
$MESS["STAT_F_STEPS"] = "Liczba stron w ścieżce";
$MESS["STAT_GO"] = "Link nawigujący";
$MESS["STAT_NEXT_PAGES"] = "Przeskocz z tego etapu ścieżki";
$MESS["STAT_NEXT_STEP"] = "Krok następnej ścieżki";
$MESS["STAT_NUM"] = "Liczba";
$MESS["STAT_PAGE"] = "Strona";
$MESS["STAT_PATH_ALT_1"] = "Liczba skoków dla teg etapu ścieżki";
$MESS["STAT_PATH_ALT_2"] = "Liczba gości w zależności od początku ścieżki";
$MESS["STAT_PATH_ALT_3"] = "Procentowy ubytek";
$MESS["STAT_PATH_PAGES"] = "Wyniki";
$MESS["STAT_PATH_PART"] = "Etap ścieżki";
$MESS["STAT_PATH_START"] = "Początek ścieżki";
$MESS["STAT_PERCENT"] = "Procent";
$MESS["STAT_RECORDS_LIST"] = "Ścieżki strony";
$MESS["STAT_TRANSFER"] = "Przeskocz";
?>