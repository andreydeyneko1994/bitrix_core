<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "dodaj IP do listy zatrzymanych";
$MESS["STAT_CITY"] = "Miasto";
$MESS["STAT_CLOSE"] = "Zamknij";
$MESS["STAT_COOKIES"] = "Ciasteczka:";
$MESS["STAT_COUNTRY"] = "Kraj";
$MESS["STAT_EDIT_USER"] = "Profil Użytkownika";
$MESS["STAT_IP"] = "Adres IP:";
$MESS["STAT_METHOD"] = "Metoda zapytania:";
$MESS["STAT_NEW_GUEST"] = "(pierwszy raz)";
$MESS["STAT_NOT_AUTH"] = "(nieautoryzowane)";
$MESS["STAT_NOT_FOUND"] = "nie znaleziono";
$MESS["STAT_NOT_REGISTERED"] = "nie zarejestrowany";
$MESS["STAT_OLD_GUEST"] = "(powrót)";
$MESS["STAT_PAGE"] = "Do:";
$MESS["STAT_REFERER"] = "Od:";
$MESS["STAT_REGION"] = "Region";
$MESS["STAT_SESSION_ID"] = "Sesja:";
$MESS["STAT_SITE"] = "Strona";
$MESS["STAT_STOP"] = "zatrzymaj listę";
$MESS["STAT_STOPED"] = "zatrzymaj";
$MESS["STAT_TIME"] = "Data:";
$MESS["STAT_TITLE"] = "Szczegóły trafienia";
$MESS["STAT_USER"] = "Gość:";
$MESS["STAT_USER_AGENT"] = "Aplikacja użytkownika";
$MESS["STAT_VIEW_SESSION_LIST"] = "Lista sesji gościa";
?>