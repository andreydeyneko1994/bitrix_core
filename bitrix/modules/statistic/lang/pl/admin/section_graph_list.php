<?
$MESS["STAT_ADV_BACK"] = "zwroty";
$MESS["STAT_ADV_LIST"] = "Pokrycie następujących kampanii reklamowych:";
$MESS["STAT_ADV_NO_BACK"] = "bezpośrednie trafenia";
$MESS["STAT_ADV_SUMMA"] = "łącznie na bezpośrednie trafienia i zwroty";
$MESS["STAT_CREATE_GRAPH"] = "Stwórz wykres";
$MESS["STAT_ENTER_POINTS"] = "punkt wejścia";
$MESS["STAT_EXIT_POINTS"] = "punkt wyjścia";
$MESS["STAT_GO_LINK"] = "Idź do tej strony";
$MESS["STAT_HITS"] = "Trafienia";
$MESS["STAT_TITLE_PAGE"] = "Ruch na stronie";
$MESS["STAT_TITLE_SECTION"] = "Ruch sekcji";
?>