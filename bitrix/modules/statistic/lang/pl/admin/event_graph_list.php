<?
$MESS["STAT_COUNT"] = "Ilość";
$MESS["STAT_EVENT_DYNAMIC"] = "Dynamika typu wydarzenia";
$MESS["STAT_F_EVENTS"] = "Typy wydarzenia:";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_MONEY"] = "Pieniądze";
$MESS["STAT_MULTI_GRAPH"] = "wykresy rozdzielone";
$MESS["STAT_RECORDS_LIST"] = "Wykres typu wydarzenia";
$MESS["STAT_SHOW_COUNT"] = "Wyświetl ilości";
$MESS["STAT_SHOW_MONEY"] = "Wyświetl kwoty";
$MESS["STAT_SUMMARIZED"] = "wykres sumaryczny dla wybranych typów wydarzenia";
$MESS["STAT_SUMMARIZED_GRAPH"] = "wykres sumaryczny";
?>