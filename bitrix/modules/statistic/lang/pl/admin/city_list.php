<?
$MESS["STAT_DYNAMIC_GRAPH2"] = "Dla pewnego okresu czasu";
$MESS["STAT_EVENTS"] = "Wydarzenia";
$MESS["STAT_F_COUNTRY_ID"] = "Kraj";
$MESS["STAT_F_PERIOD"] = "Okres";
$MESS["STAT_HITS"] = "Trafienia";
$MESS["STAT_NEW_GUESTS"] = "nowi goście";
$MESS["STAT_NO_COUNTRY_ID"] = "Filtr krajów musi zostać określony.";
$MESS["STAT_NO_DATA"] = "Brak wystarczających danych do stworzenia diagramu";
$MESS["STAT_RECORDS_LIST"] = "Oparta na mieście dynamika ruchu";
$MESS["STAT_SESSIONS"] = "Sesje";
$MESS["STAT_STATIC_GRAPH"] = "Dla całego czasu trzymania statystyk";
?>