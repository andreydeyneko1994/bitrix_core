<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "dodaj IP do listy zatrzymanych";
$MESS["STAT_DATE"] = "Data";
$MESS["STAT_FL_DATE"] = "Data";
$MESS["STAT_FL_ID"] = "ID silnika wyszukiwarki";
$MESS["STAT_FL_IP"] = "adres IP";
$MESS["STAT_FL_LOGIC"] = "Logika";
$MESS["STAT_FL_PAGE"] = "Strona";
$MESS["STAT_FL_UA"] = "Aplikacja użytkownika";
$MESS["STAT_F_ID"] = "ID:";
$MESS["STAT_F_IP"] = "Adres IP:";
$MESS["STAT_F_PAGE"] = "Strona:";
$MESS["STAT_F_SEARCH_SYSTEM"] = "Silnik wyszukiwarki:";
$MESS["STAT_F_USER_AGENT"] = "Aplikacja użytkownika";
$MESS["STAT_HIT_PAGES"] = "Trafienia";
$MESS["STAT_IP"] = "adres IP";
$MESS["STAT_LINK_OPEN"] = "Otwórz link";
$MESS["STAT_PAGE"] = "Strona";
$MESS["STAT_RECORDS_LIST"] = "Trafienia silnika wyszukiwarki dla ostatnich #STATISTIC_DAYS# dni";
$MESS["STAT_SEARCHER"] = "Silnik wyszukiwarki";
$MESS["STAT_SRCH_LIST"] = "Lista silników wyszukiwarki";
$MESS["STAT_USER_AGENT"] = "Aplikacja użytkownika";
?>