<?
$MESS["STAT_ADV_VISIBLE"] = "Zawarte w <a href='/bitrix/admin/adv_list.php?lang=#LANG#'>raport reklam</a>:";
$MESS["STAT_CLEAR"] = "Wymaż";
$MESS["STAT_DELETE_EVENT_TYPE"] = "Usuń typ wydarzenia";
$MESS["STAT_DELETE_EVENT_TYPE_CONFIRM"] = "Na pewno chcesz usunąć ten typ wydarzenia?";
$MESS["STAT_DESCRIPTION"] = "Opis:";
$MESS["STAT_DYNAMIC_KEEP_DAYS"] = "Dni do przechowywania dynamiki typu wydarzenia:";
$MESS["STAT_EDIT_RECORD"] = "Parametry typu wydarzenia # #ID#";
$MESS["STAT_EVENT_TYPE"] = "Typ wydarzenia";
$MESS["STAT_KEEP_DAYS"] = "Dni do przechowywania każdego przykładu<br>tego typu wydarzenia:";
$MESS["STAT_LIST"] = "Lista";
$MESS["STAT_NAME"] = "Nazwa:";
$MESS["STAT_NEW_EVENT_TYPE"] = "Typ nowego wydarzenia";
$MESS["STAT_NEW_RECORD"] = "Typ nowego wydarzenia";
$MESS["STAT_PIE_CHART"] = "Zawarte w domyślnej tabeli i wykresie:";
$MESS["STAT_RECORDS_LIST"] = "Typy wydarzenia";
$MESS["STAT_RESET_EVENT_TYPE"] = "Resetuj typ wydarzenia";
$MESS["STAT_RESET_EVENT_TYPE_CONFIRM"] = "Na pewno chcesz zresetować ten typ wydarzenia?";
$MESS["STAT_SAVE_ERROR"] = "Błąd zapisu typu wydarzenia.";
$MESS["STAT_SORT"] = "Sortowanie:";
?>