<?
$MESS["STAT_ADMIN"] = "Pełny dostęp administratora";
$MESS["STAT_DENIED"] = "Odmowa dostępu";
$MESS["STAT_INSTALL_DATABASE"] = "Instalacja modułu do bazy danych:";
$MESS["STAT_INSTALL_TITLE"] = "Instalacja modułu statystyk";
$MESS["STAT_MAIN_DATABASE"] = "Główny";
$MESS["STAT_MODULE_DESCRIPTION"] = "Moduł do statystyk strony";
$MESS["STAT_MODULE_NAME"] = "Analityka Sieciowa";
$MESS["STAT_UNINSTALL_TITLE"] = "Odistalowanie modułu statystyk";
$MESS["STAT_VIEW"] = "zobacz wszystkie statystyki";
?>