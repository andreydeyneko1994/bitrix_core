<?
$MESS["STAT_CLICKS"] = "Kliknięcia";
$MESS["STAT_DEFENCE_LOG_EVENT"] = "Limit aktywności przekroczony.";
$MESS["STAT_DEFENCE_LOG_MESSAGE"] = "Aktywność gościa: #ACTIVITY_HITS# trafień wykonanych na #ACTIVITY_TIME_LIMIT# sek. Limit aktywności został przekroczony przez #ACTIVITY_EXCEEDING# trafień.";
$MESS["STAT_LINK"] = "Link";
$MESS["STAT_LINK_STAT"] = "Kliknięcia linku na stronie";
$MESS["STAT_LINK_STAT_HIDE_PANEL_BUTTON"] = "Ukryj statystyki kliknięcia linku dla tej strony";
$MESS["STAT_LINK_STAT_PANEL_BUTTON"] = "Pokaż diagram kliknięć linku dla tej strony";
$MESS["STAT_LINK_STAT_PANEL_BUTTON_ALERT"] = "Brak statystyk kliknięcia linku dla tej strony";
$MESS["STAT_LINK_STAT_SHOW_PANEL_BUTTON"] = "Pokaż statystyki kliknięcia linku dla tej strony";
$MESS["STAT_LINK_STAT_TITLE"] = "Diagram kliknięć linku";
$MESS["STAT_PAGE_GRAPH_PANEL_BUTTON"] = "Ruch na stronie";
$MESS["STAT_PANEL_BUTTON"] = "Statystyki";
$MESS["STAT_PANEL_BUTTON_HINT"] = "Zobacz ruch na stronie lub kliknij statystyki";
?>