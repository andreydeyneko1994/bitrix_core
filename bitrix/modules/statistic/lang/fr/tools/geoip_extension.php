<?
$MESS["STAT_CITY_GEOIP_EXT_DESCR"] = "L'extension PECL php <a href='http://pecl.php.net/package/geoip' target='_blank'>geoip</a>. Une version supérieure ou équivalente à 1.0.3 est requise.";
$MESS["STAT_CITY_GEOIP_EXT_LATITUDE"] = "Latitude";
$MESS["STAT_CITY_GEOIP_EXT_LONGITUDE"] = "Longitude";
$MESS["STAT_CITY_GEOIP_EXT_POSTAL_CODE"] = "Code Postal";
?>