<?
$MESS["STAT_CITY_GEOIP_MOD_CONTINENT"] = "Continent";
$MESS["STAT_CITY_GEOIP_MOD_DESCR"] = "Module apache <a href='http://www.maxmind.com/app/mod_geoip' target='_blank'>mod_geoip</a>. Nécessite la version plus ou égale à 1.2.5.";
$MESS["STAT_CITY_GEOIP_MOD_LATITUDE"] = "Latitude";
$MESS["STAT_CITY_GEOIP_MOD_LONGITUDE"] = "Longitude";
?>