<?
$MESS["STAT_CITY_GEOIP_PHP_DESCR"] = "Module d'accès à la base de données geoip, écrit en php. Il est conseillé de télécharger <a href='http://www.maxmind.com/app/php' target='_blank'>Assure-toi</a> la dernière version. Dans le fichier dbconn.php il faut définir une constante GEOIP_DATABASE_FILE avec le chemin d'accès complet à la base de données.";
$MESS["STAT_CITY_GEOIP_PHP_LATITUDE"] = "Latitude";
$MESS["STAT_CITY_GEOIP_PHP_LONGITUDE"] = "Longitude";
$MESS["STAT_CITY_GEOIP_PHP_POSTAL_CODE"] = "Code Postal";
?>