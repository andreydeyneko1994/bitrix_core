<?
$MESS["STAT_ADMIN"] = "l'accès administratif complet";
$MESS["STAT_CREATE_I2C_DB"] = "Remake index pour les recherches IP au pays ?";
$MESS["STAT_DENIED"] = "l'L'accès est refusé";
$MESS["STAT_INSTALL_DATABASE"] = "Base de Données pour l'installation du Module : ";
$MESS["STAT_INSTALL_TITLE"] = "Installation du module de statistiques";
$MESS["STAT_MAIN_DATABASE"] = "principal(e)s";
$MESS["STAT_MODULE_DESCRIPTION"] = "Ce module recueille et affiche les statistiques du site.";
$MESS["STAT_MODULE_NAME"] = "Du web-analyste";
$MESS["STAT_START_GUESTS"] = "Valeur initiale des visiteurs : ";
$MESS["STAT_START_HITS"] = "Valeur initiale hits : ";
$MESS["STAT_START_HOSTS"] = "Valeur initiale hôtes : ";
$MESS["STAT_UNINSTALL_TITLE"] = "Le module de statistiques désinstallation";
$MESS["STAT_VIEW"] = "voir toutes les statistiques";
$MESS["STAT_VIEW_WITHOUT_MONEY"] = "Voir les statistiques sans aucune paramètres financiers";
?>