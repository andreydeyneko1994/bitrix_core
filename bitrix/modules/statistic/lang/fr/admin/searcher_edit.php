<?
$MESS["STAT_ACTIVE"] = "Actif(ve) : ";
$MESS["STAT_CHAR_SET"] = "Phrase de recherche encodage";
$MESS["STAT_CHECK_ACTIVITY"] = "Vérifiez limite d'activité:";
$MESS["STAT_DELETE_SEARCHER"] = "Supprimer moteur de recherche";
$MESS["STAT_DELETE_SEARCHER_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce moteur de recherche ?";
$MESS["STAT_DOMAIN"] = "Domaine [% _]";
$MESS["STAT_DYNAMIC_KEEP_DAYS"] = "Jours pour garder la dynamique : ";
$MESS["STAT_EDIT_RECORD"] = "Réglages pour le moteur de recherche # #ID#";
$MESS["STAT_FORGOT_NAME"] = "Vous avez oublié de saisir le champ 'Nom'";
$MESS["STAT_FORM_ERROR_SAVE"] = "Erreur de sauvegarde";
$MESS["STAT_HIT_KEEP_DAYS"] = "Combien de temps il faut conserver les hits : ";
$MESS["STAT_NAME"] = "Dénomination : ";
$MESS["STAT_NEW_RECORD"] = "Nouveau moteur de recherche";
$MESS["STAT_NEW_SEARCHER"] = "Nouveau moteur de recherche";
$MESS["STAT_PIE_CHART"] = "Inclure dans le tableau d'indexation et représenter graphiquement et vidéos<br>par défaut : ";
$MESS["STAT_PROP_TITLE"] = "Paramètres du moteur de recherche";
$MESS["STAT_RECORDS_LIST"] = "Liste des moteurs de recherche";
$MESS["STAT_SEARCHER_DOMAINS"] = "Domaines : ";
$MESS["STAT_STATISTICS"] = "Enregistrer des résultats : ";
$MESS["STAT_USER_AGENT"] = "UserAgent : ";
$MESS["STAT_VARIABLE"] = "De variables (la séparées par des virgules)";
?>