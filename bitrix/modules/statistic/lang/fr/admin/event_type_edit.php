<?
$MESS["STAT_ADV_VISIBLE"] = "Inclure dans <a href='/bitrix/admin/adv_list.php?lang=#LANG#'>rapport sur la publicité</a>:";
$MESS["STAT_CLEAR"] = "Effacer";
$MESS["STAT_DELETE_EVENT_TYPE"] = "Supprimer type d'évènement";
$MESS["STAT_DELETE_EVENT_TYPE_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce type d'évènement ?";
$MESS["STAT_DESCRIPTION"] = "Description";
$MESS["STAT_DYNAMIC_KEEP_DAYS"] = "Jours pour garder la dynamique de type d'évènement : ";
$MESS["STAT_EDIT_RECORD"] = "Type de l'évènement Paramètres # #ID#";
$MESS["STAT_EVENT_TYPE"] = "Type d'évènement";
$MESS["STAT_KEEP_DAYS"] = "Jours pour garder chaque instance<br>de ce type d'évènement : ";
$MESS["STAT_LIST"] = "Liste";
$MESS["STAT_NAME"] = "Dénomination : ";
$MESS["STAT_NEW_EVENT_TYPE"] = "Nouveau type d'évènement";
$MESS["STAT_NEW_RECORD"] = "Nouveau type d'évènement";
$MESS["STAT_PIE_CHART"] = "Inclure dans le tableau et le graphique par défaut : ";
$MESS["STAT_RECORDS_LIST"] = "Les types d'évènements";
$MESS["STAT_RESET_EVENT_TYPE"] = "Type d'évènement de Reset";
$MESS["STAT_RESET_EVENT_TYPE_CONFIRM"] = "tes-vous sûr que vous souhaitez réinitialiser ce type d'évènement ?";
$MESS["STAT_SAVE_ERROR"] = "L'enregistrement d'un type d'évènement erreur.";
$MESS["STAT_SORT"] = "Ordre de triage : ";
?>