<?
$MESS["STAT_FL_SEACHERS"] = "Moteurs de recherche";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_F_SEACHERS"] = "Moteurs de recherche : ";
$MESS["STAT_NO_DATA"] = "Pas assez de données pour créer graphique";
$MESS["STAT_RECORDS_LIST"] = "Graphiques d'indexation du site";
$MESS["STAT_SEARCHER_DYNAMIC"] = "Moteur de recherche : la dynamique d'indexation du site";
$MESS["STAT_SUMMARIZED"] = "graphe résumé sur les moteurs de recherche sélectionnés";
?>