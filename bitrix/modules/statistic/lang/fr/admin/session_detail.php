<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "Ajouter IP à la liste d'exclusion ";
$MESS["STAT_ADV"] = "Campagne de publicité:";
$MESS["STAT_CITY"] = "Ville";
$MESS["STAT_CLOSE"] = "Fermer";
$MESS["STAT_COUNTRY"] = "Pays";
$MESS["STAT_DATE_FIRST"] = "Début : ";
$MESS["STAT_DATE_LAST"] = "Achèvement : ";
$MESS["STAT_EDIT_USER"] = "Evènements du profil utilisateur";
$MESS["STAT_HOUR"] = "h.";
$MESS["STAT_ID"] = "ID : ";
$MESS["STAT_IP_FIRST"] = "Première adresse IP : ";
$MESS["STAT_IP_LAST"] = "Dernière adresse IP : ";
$MESS["STAT_LAST_PAGE"] = "Dernière page : ";
$MESS["STAT_MIN"] = "min.";
$MESS["STAT_NEW_GUEST"] = "(pour la première fois)";
$MESS["STAT_NOT_AUTH"] = "(non autorisé)";
$MESS["STAT_NOT_FOUND"] = "Désolé, votre recherche n'a donné aucun résultat.";
$MESS["STAT_NOT_REGISTERED"] = "non enregistré";
$MESS["STAT_NUM_PAGES"] = "Hits : ";
$MESS["STAT_OLD_GUEST"] = "(retourné)";
$MESS["STAT_REFERER"] = "De la part de : ";
$MESS["STAT_REGION"] = "Arrondissement";
$MESS["STAT_SEC"] = "(sec.)";
$MESS["STAT_SITE"] = "Site";
$MESS["STAT_STOP"] = "stop-liste";
$MESS["STAT_STOPED"] = "arrêter";
$MESS["STAT_TIME"] = "Temps : ";
$MESS["STAT_TITLE"] = "Détails de la session";
$MESS["STAT_URL_TO"] = "Première page : ";
$MESS["STAT_USER"] = "Visiteur : ";
$MESS["STAT_USER_AGENT"] = "UserAgent : ";
$MESS["STAT_VIEW_HITS_LIST_1"] = "Hit liste pour ce visiteur ID";
$MESS["STAT_VIEW_HITS_LIST_2"] = "Hit liste pour cette session ID";
?>