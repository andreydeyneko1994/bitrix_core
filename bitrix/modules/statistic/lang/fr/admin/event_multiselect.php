<?
$MESS["STAT_CHOOSE"] = "Choisir";
$MESS["STAT_CHOOSE_TITLE"] = "Sélectionne le type d'évènement et ferme la fenêtre.";
$MESS["STAT_DESCRIPTION"] = "Description";
$MESS["STAT_EVENT_TYPE_PAGES"] = "Types d'évènements";
$MESS["STAT_NAME"] = "Dénomination";
$MESS["STAT_SELECT"] = "choisir";
$MESS["STAT_SELECT_TITLE"] = "Sélectionnez les types marqués";
$MESS["STAT_TITLE"] = "Sélection du type de l'évènement";
?>