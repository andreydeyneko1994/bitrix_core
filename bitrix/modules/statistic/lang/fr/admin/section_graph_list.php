<?
$MESS["STAT_ADV_BACK"] = "A restituer";
$MESS["STAT_ADV_LIST"] = "Les campagnes de publicité suivantes couverts : ";
$MESS["STAT_ADV_NO_BACK"] = "sur coup direct";
$MESS["STAT_ADV_SUMMA"] = "totale sur les coups directs et retours";
$MESS["STAT_CREATE_GRAPH"] = "Construire graphique";
$MESS["STAT_ENTER_POINTS"] = "point d'entrée";
$MESS["STAT_EXIT_POINTS"] = "point de sortie";
$MESS["STAT_GO_LINK"] = "Accédez à cette page";
$MESS["STAT_HITS"] = "des hits";
$MESS["STAT_TITLE_PAGE"] = "Calendrier de la fréquentation de la page";
$MESS["STAT_TITLE_SECTION"] = "Trafic de la section";
?>