<?
$MESS["STAT_DYNAMIC_GRAPH2"] = "Durant un laps de temps bien déterminé";
$MESS["STAT_EVENTS"] = "Evénements";
$MESS["STAT_F_COUNTRY_ID"] = "Pays";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_HITS"] = "Hits";
$MESS["STAT_NEW_GUESTS"] = "Nouveaux visiteurs";
$MESS["STAT_NO_COUNTRY_ID"] = "Filtre par pays non renseigné.";
$MESS["STAT_NO_DATA"] = "Pas de données pour construire un diagramme";
$MESS["STAT_RECORDS_LIST"] = "Dynamique de visites par ville";
$MESS["STAT_SESSIONS"] = "Accéder à la liste des sessions";
$MESS["STAT_STATIC_GRAPH"] = "Pour Approbation";
?>