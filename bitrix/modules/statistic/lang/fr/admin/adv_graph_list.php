<?
$MESS["STAT_ADV_LIST"] = "Liste des campagnes publicitaires";
$MESS["STAT_ALL_DYNAMICS"] = "Dynamique de la campagne publicitaire";
$MESS["STAT_ALL_GRAPHICS"] = "Abaques de campagne publicitaire";
$MESS["STAT_BACK"] = "Précédent";
$MESS["STAT_DYNAMIC"] = "Dynamique de la campagne de publicité # #ID#";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_F_SELECT_EVENTS"] = "Evénements";
$MESS["STAT_F_SET_PERIOD"] = "Construire graphiques";
$MESS["STAT_GRAPH_1"] = "Graphique de popularité";
$MESS["STAT_GRAPH_2"] = "Graphique des évènements";
$MESS["STAT_GUESTS"] = "visiteurs";
$MESS["STAT_HITS"] = "hits";
$MESS["STAT_HOSTS"] = "Hébergers";
$MESS["STAT_INCORRECT_ADV_ID"] = "Campagne de publicité introuvable";
$MESS["STAT_NEW_GUESTS"] = "nouveaux visiteurs";
$MESS["STAT_NOT_ENOUGH_DATA"] = "Pas assez de données pour construire le graphique";
$MESS["STAT_RECORDS_LIST"] = "Graphiques pour la campagne de publicité # #ID#";
$MESS["STAT_SESSIONS"] = "séances";
$MESS["STAT_SHOW"] = "Graphiques incluront";
$MESS["STAT_STRAIGHT"] = "Tout droit";
?>