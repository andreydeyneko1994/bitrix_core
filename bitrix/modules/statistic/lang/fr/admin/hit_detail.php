<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "Ajouter IP à la liste d'exclusion ";
$MESS["STAT_CITY"] = "Ville";
$MESS["STAT_CLOSE"] = "Fermer";
$MESS["STAT_COOKIES"] = "Cookies : ";
$MESS["STAT_COUNTRY"] = "Pays";
$MESS["STAT_EDIT_USER"] = "Evènements du profil utilisateur";
$MESS["STAT_IP"] = "Adresse IP : ";
$MESS["STAT_METHOD"] = "Méthode de requête : ";
$MESS["STAT_NEW_GUEST"] = "(pour la première fois)";
$MESS["STAT_NOT_AUTH"] = "(non autorisé)";
$MESS["STAT_NOT_FOUND"] = "Désolé, votre recherche n'a donné aucun résultat.";
$MESS["STAT_NOT_REGISTERED"] = "non enregistré";
$MESS["STAT_OLD_GUEST"] = "(retourné)";
$MESS["STAT_PAGE"] = "A qui : ";
$MESS["STAT_REFERER"] = "De la part de : ";
$MESS["STAT_REGION"] = "Arrondissement";
$MESS["STAT_SESSION_ID"] = "Session : ";
$MESS["STAT_SITE"] = "Site";
$MESS["STAT_STOP"] = "stop-liste";
$MESS["STAT_STOPED"] = "arrêter";
$MESS["STAT_TIME"] = "Date : ";
$MESS["STAT_TITLE"] = "Détails de hit";
$MESS["STAT_USER"] = "Visiteur : ";
$MESS["STAT_USER_AGENT"] = "UserAgent : ";
$MESS["STAT_VIEW_SESSION_LIST"] = "Visiteur liste des sessions";
?>