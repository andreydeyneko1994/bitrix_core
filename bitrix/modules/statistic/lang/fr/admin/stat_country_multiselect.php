<?
$MESS["STAT_COUNTRY_MSEL_FIND"] = "Trouver";
$MESS["STAT_COUNTRY_MSEL_ID"] = "ID";
$MESS["STAT_COUNTRY_MSEL_NAME"] = "Dénomination";
$MESS["STAT_COUNTRY_MSEL_PAGES"] = "Choix du pays";
$MESS["STAT_COUNTRY_MSEL_SELECT"] = "Choisir";
$MESS["STAT_COUNTRY_MSEL_SELECT_TITLE"] = "Ajouter les pays sélectionnés";
$MESS["STAT_COUNTRY_MSEL_SHORT_NAME"] = "Dénomination brève";
$MESS["STAT_COUNTRY_MSEL_TITLE"] = "Choix du pays";
?>