<?
$MESS["STAT_REGION_MSEL_COUNTRY_ID"] = "Code du pays";
$MESS["STAT_REGION_MSEL_COUNTRY_NAME"] = "Nom raccourci du pays";
$MESS["STAT_REGION_MSEL_COUNTRY_SHORT_NAME"] = "Nom complet du pays";
$MESS["STAT_REGION_MSEL_FIND"] = "Trouver";
$MESS["STAT_REGION_MSEL_PAGES"] = "Choix des régions";
$MESS["STAT_REGION_MSEL_REGION_NAME"] = "Région";
$MESS["STAT_REGION_MSEL_SELECT"] = "Choisir";
$MESS["STAT_REGION_MSEL_SELECT_TITLE"] = "Ajouter les régions sélectionnées";
$MESS["STAT_REGION_MSEL_TITLE"] = "Choix des régions";
?>