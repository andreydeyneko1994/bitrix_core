<?
$MESS["STAT_ADV_BACK"] = "A restituer";
$MESS["STAT_ADV_NO_BACK"] = "par coup direct";
$MESS["STAT_ADV_SUMMA"] = "total des tirs directs et retours";
$MESS["STAT_ENTER_POINTS_S"] = "Chemin des points de départ";
$MESS["STAT_F_ADV"] = "Campagnes de publicité";
$MESS["STAT_F_ADV_DATA_TYPE"] = "Type de données pour des campagnes publicitaires";
$MESS["STAT_F_FIRST_PAGE"] = "Première page sur le chemin";
$MESS["STAT_F_FULL_PATH"] = "Adresse complète (Adresse réelle)";
$MESS["STAT_F_LAST_PAGE"] = "Dernière page de la voie";
$MESS["STAT_F_PAGE"] = "Toute page du chemin";
$MESS["STAT_F_SEGMENT_PATH"] = "Segments de chemin d'accès";
$MESS["STAT_F_STEPS"] = "Nombre de pages dans le chemin";
$MESS["STAT_GO"] = "Naviguez lien";
$MESS["STAT_NEXT_PAGES"] = "Sauts de cette étape du chemin";
$MESS["STAT_NEXT_STEP"] = "L'étape suivante de la voie";
$MESS["STAT_NUM"] = "Chiffre";
$MESS["STAT_PAGE"] = "Page";
$MESS["STAT_PATH_ALT_1"] = "Le nombre de transitions avant de ce tronçon de route";
$MESS["STAT_PATH_ALT_2"] = "Par rapport au début du chemin çela s'élève";
$MESS["STAT_PATH_ALT_3"] = "Pourcentage de pertes";
$MESS["STAT_PATH_PAGES"] = "Inscriptions";
$MESS["STAT_PATH_PART"] = "Segment du chemin";
$MESS["STAT_PATH_START"] = "Début du chemin";
$MESS["STAT_PERCENT"] = "La part quantitative";
$MESS["STAT_RECORDS_LIST"] = "Chemins du site";
$MESS["STAT_TRANSFER"] = "Sauts";
?>