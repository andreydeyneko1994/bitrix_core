<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "Ajouter IP à la liste d'exclusion ";
$MESS["STAT_DATE"] = "Date";
$MESS["STAT_FL_DATE"] = "Date";
$MESS["STAT_FL_ID"] = "ID de moteur de recherche";
$MESS["STAT_FL_IP"] = "Adresse IP";
$MESS["STAT_FL_LOGIC"] = "Logique";
$MESS["STAT_FL_PAGE"] = "Page";
$MESS["STAT_FL_UA"] = "UserAgent";
$MESS["STAT_F_ID"] = "ID : ";
$MESS["STAT_F_IP"] = "Adresse IP : ";
$MESS["STAT_F_PAGE"] = "Page : ";
$MESS["STAT_F_SEARCH_SYSTEM"] = "Moteur de recherche : ";
$MESS["STAT_F_USER_AGENT"] = "UserAgent : ";
$MESS["STAT_HIT_PAGES"] = "Hits";
$MESS["STAT_IP"] = "Adresse IP";
$MESS["STAT_LINK_OPEN"] = "Ouvrir le lien";
$MESS["STAT_PAGE"] = "Page";
$MESS["STAT_RECORDS_LIST"] = "Moteur de recherche Résultats pour les derniers #STATISTIC_DAYS# jours";
$MESS["STAT_SEARCHER"] = "Moteur de recherche";
$MESS["STAT_SRCH_LIST"] = "Liste des moteurs de recherche";
$MESS["STAT_USER_AGENT"] = "UserAgent";
?>