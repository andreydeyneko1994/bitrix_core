<?
$MESS["STAT_ADDED_BROWSERS"] = "Ajouté masques du navigateur : ";
$MESS["STAT_ADDED_SEARCHERS"] = "Ajouté masques des moteurs de recherche : ";
$MESS["STAT_ADD_AS_BROWSER"] = "Ajouter comme navigateur";
$MESS["STAT_ADD_AS_SEARCHER"] = "Ajouter comme moteur de recherche";
$MESS["STAT_BROWSER"] = "Navigateur";
$MESS["STAT_FL_DAY"] = "Rechercher pour la journée";
$MESS["STAT_FL_SESS"] = "Nombre de séances";
$MESS["STAT_F_COUNTER"] = "Sessions : ";
$MESS["STAT_F_LAST_DAY"] = "Rechercher pour la journée";
$MESS["STAT_F_USER_AGENT"] = "UserAgent : ";
$MESS["STAT_MASK"] = "Masque pour ajouter [% _]";
$MESS["STAT_RECORDS_LIST"] = "Inconnu UserAgent auto-detecte";
$MESS["STAT_SEARCHER"] = "Moteur de recherche";
$MESS["STAT_SESSIONS"] = "Accéder à la liste des sessions";
$MESS["STAT_SESS_LIST"] = "Liste des statuts";
$MESS["STAT_USER_AGENT"] = "UserAgent";
$MESS["STAT_USER_AGENT_PAGES"] = "UserAgents";
?>