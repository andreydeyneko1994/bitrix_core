<?
$MESS["STAT_CITY_MSEL_CITY_NAME"] = "Ville";
$MESS["STAT_CITY_MSEL_COUNTRY_ID"] = "Code du pays";
$MESS["STAT_CITY_MSEL_COUNTRY_NAME"] = "Nom raccourci du pays";
$MESS["STAT_CITY_MSEL_COUNTRY_SHORT_NAME"] = "Nom complet du pays";
$MESS["STAT_CITY_MSEL_FIND"] = "Trouver";
$MESS["STAT_CITY_MSEL_PAGES"] = "Choix de villes";
$MESS["STAT_CITY_MSEL_REGION_NAME"] = "Région";
$MESS["STAT_CITY_MSEL_SELECT"] = "Choisir";
$MESS["STAT_CITY_MSEL_SELECT_TITLE"] = "Ajouter les villes sélectionnées";
$MESS["STAT_CITY_MSEL_TITLE"] = "Choix de villes";
?>