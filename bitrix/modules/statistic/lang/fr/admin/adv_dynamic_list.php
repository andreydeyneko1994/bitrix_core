<?
$MESS["STAT_ADV_DYN_PAGES"] = "Jours";
$MESS["STAT_ADV_LIST"] = "Liste des campagnes publicitaires";
$MESS["STAT_BACK"] = "Retour de marchandises";
$MESS["STAT_BACK_ALT"] = "revenir après une approche directe à la campagne de publicité";
$MESS["STAT_DATE"] = "Date";
$MESS["STAT_EVENTS"] = "Evénements";
$MESS["STAT_EVENTS_LIST"] = "Liste des évènements";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_GRAPH"] = "Abaque de campagne de publicité";
$MESS["STAT_GUESTS"] = "Visiteurs";
$MESS["STAT_HITS"] = "Hits";
$MESS["STAT_HOSTS"] = "Hébergeurs";
$MESS["STAT_INCORRECT_ADV_ID"] = "Campagne de publicité introuvable";
$MESS["STAT_NEW"] = "Créer";
$MESS["STAT_NEW_ALT"] = "Nouveau site visiteurs pour la première fois";
$MESS["STAT_RECORDS_LIST"] = "Dynamique de la campagne de publicité";
$MESS["STAT_SESSIONS"] = "Accéder à la liste des sessions";
$MESS["STAT_STRAIGHT"] = "Direct";
$MESS["STAT_STRAIGHT_ALT"] = "coup direct sur une campagne de publicité";
$MESS["STAT_VIEW_EVENT_LIST"] = "Aller à la liste des évènements";
$MESS["STAT_VIEW_EVENT_LIST_BACK"] = "Liste d'évènements au retour";
?>