<?
$MESS["STAT_DATE"] = "Date";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_F_SEARCHER_ID"] = "Moteur de recherche : ";
$MESS["STAT_GRAPH"] = "Moteur de recherche : Site indexation graphique";
$MESS["STAT_GRAPH_TITLE"] = "Calendrier de l'indexation du site par un moteur de recherche";
$MESS["STAT_HITS"] = "Hits";
$MESS["STAT_HITS_LIST_OPEN"] = "Voir capture d'écran";
$MESS["STAT_RECORDS_LIST"] = "Moteur de recherche : la dynamique d'indexation du site";
$MESS["STAT_SEARCHER_PAGES"] = "Inscriptions";
$MESS["STAT_TOTAL_HITS"] = "Total hits : ";
?>