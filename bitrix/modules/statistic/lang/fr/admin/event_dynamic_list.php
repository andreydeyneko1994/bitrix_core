<?
$MESS["STAT_CHOOSE_BTN"] = "Choisissez...";
$MESS["STAT_COUNTER"] = "Calcul";
$MESS["STAT_DATE"] = "Date";
$MESS["STAT_EVENT_DYN_PAGES"] = "Inscriptions";
$MESS["STAT_F_EVENT_ID"] = "Type de l'évènement : ";
$MESS["STAT_F_PERIOD"] = "Délais";
$MESS["STAT_GRAPH"] = "Type de l'évènement graphique quotidien";
$MESS["STAT_MNU_GRAPH"] = "Abaques";
$MESS["STAT_RECORDS_LIST"] = "Dynamique quotidienne de type de l'évènement";
$MESS["STAT_TOTAL_TIME"] = "Nombre de jours : ";
?>