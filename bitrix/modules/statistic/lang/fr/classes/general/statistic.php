<?
$MESS["STAT_CLICKS"] = "déplacements";
$MESS["STAT_DEFENCE_LOG_EVENT"] = "Dépassement de limite d'activité.";
$MESS["STAT_DEFENCE_LOG_MESSAGE"] = "Pendant #ACTIVITY_TIME_LIMIT# secondes il y a eu de hits:#ACTIVITY_HITS#, ce qui dépasse la limite d'activité en #ACTIVITY_EXCEEDING#.";
$MESS["STAT_LINK"] = "Lien au commentaire";
$MESS["STAT_LINK_STAT"] = "Suivre des liens à partir d'un site";
$MESS["STAT_LINK_STAT_HIDE_PANEL_BUTTON"] = "Lien caché clic statistiques de cette page";
$MESS["STAT_LINK_STAT_PANEL_BUTTON"] = "Diagramme des sauts suivant les liens depuis la page donnée";
$MESS["STAT_LINK_STAT_PANEL_BUTTON_ALERT"] = "Il n'y a aucun lien de navigation à partir de cette page.";
$MESS["STAT_LINK_STAT_SHOW_PANEL_BUTTON"] = "Voir les statistiques pour les redirections depuis cette page";
$MESS["STAT_LINK_STAT_TITLE"] = "Diagramme des passages par les liens";
$MESS["STAT_PAGE_GRAPH_PANEL_BUTTON"] = "Calendrier de la fréquentation de la page";
$MESS["STAT_PANEL_BUTTON"] = "Statistique";
$MESS["STAT_PANEL_BUTTON_HINT"] = "Affichage du registre des consultations de la page ou de la statistique des transitions.";
?>