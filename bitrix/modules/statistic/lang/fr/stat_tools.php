<?
$MESS["STAT_FROM_TILL_DATE"] = "Le champ \"jusqu'à\" la date doit être supérieure à la date \"de\" dans le filtre pour le \"premier sondage\"";
$MESS["STAT_LABEL_TITLE"] = "Lien :&nbsp;#LINK#
Clics :&nbsp;#CNT#
Pour cent :&nbsp;#PERCENT#
";
$MESS["STAT_WRONG_DATE_FROM"] = "S'il vous plaît entrer la date 'de' dans le filtre";
$MESS["STAT_WRONG_DATE_TILL"] = "S'il vous plaît entrer un valide 'jusqu'à' date dans le filtre";
?>