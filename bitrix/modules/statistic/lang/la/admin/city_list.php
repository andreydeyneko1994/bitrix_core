<?
$MESS["STAT_DYNAMIC_GRAPH2"] = "Para un periodo de tiempo";
$MESS["STAT_EVENTS"] = "Eventos";
$MESS["STAT_F_COUNTRY_ID"] = "País";
$MESS["STAT_F_PERIOD"] = "Período";
$MESS["STAT_HITS"] = "Éxitos";
$MESS["STAT_NEW_GUESTS"] = "Nuevos visitantes";
$MESS["STAT_NO_COUNTRY_ID"] = "No se especifica País.";
$MESS["STAT_NO_DATA"] = "No hay datos suficientes para construir el diagrama";
$MESS["STAT_RECORDS_LIST"] = "Tráfico citadino basado en la dinámica";
$MESS["STAT_SESSIONS"] = "Sesiones";
$MESS["STAT_STATIC_GRAPH"] = "Durante todo el tiempo de almacenamiento de estadísticas.";
?>