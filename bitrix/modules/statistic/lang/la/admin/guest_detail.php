<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "agregar IP para detener lista";
$MESS["STAT_ADV"] = "Campaña&nbsp;publicitaria:";
$MESS["STAT_ADV_BACK"] = "devuelto en campaña publicitaria";
$MESS["STAT_CITY"] = "Ciudad";
$MESS["STAT_CLOSE"] = "Cerrar";
$MESS["STAT_COOKIE"] = "Cookie:";
$MESS["STAT_COUNTRY"] = "País";
$MESS["STAT_CURRENT_GUEST"] = "Este número de visitante es el suyo";
$MESS["STAT_DATE"] = "Fecha:";
$MESS["STAT_EDIT_USER"] = "Perfil de usuario";
$MESS["STAT_EVENTS"] = "Eventos:";
$MESS["STAT_FIRST_ENTER"] = "Primera Visita";
$MESS["STAT_FROM"] = "Desde:";
$MESS["STAT_HITS"] = "Hits:";
$MESS["STAT_HOUR"] = "h";
$MESS["STAT_IP"] = "IP (servidor):";
$MESS["STAT_LANGUAGE"] = "Idiomas:";
$MESS["STAT_LAST_ENTER"] = "Última visita";
$MESS["STAT_LAST_PAGE"] = "Último:";
$MESS["STAT_MIN"] = "min";
$MESS["STAT_NEW_GUEST"] = "(nuevo)";
$MESS["STAT_NOT_AUTH"] = "(no autorizado)";
$MESS["STAT_NOT_FOUND"] = "Registro no encontrado";
$MESS["STAT_OLD_GUEST"] = "(antiguo)";
$MESS["STAT_REGION"] = "Región";
$MESS["STAT_SEC"] = "seg";
$MESS["STAT_SESSIONS"] = "Sesiones:";
$MESS["STAT_SESSION_ID"] = "ID de sesión:";
$MESS["STAT_SITE"] = "Sitio web";
$MESS["STAT_STOP"] = "listado de detenidos";
$MESS["STAT_TIME"] = "Tiempo:";
$MESS["STAT_TITLE"] = "Detalles de visitantes";
$MESS["STAT_TO"] = "A:";
$MESS["STAT_USER"] = "Usuario:";
$MESS["STAT_USER_AGENT"] = "UserAgent:";
$MESS["STAT_VIEW_EVENTS_LIST"] = "Lista de eventos para este número de visitantes";
$MESS["STAT_VIEW_HITS_LIST"] = "Lista de hits para este número de visitante";
$MESS["STAT_VIEW_SESSIONS_LIST"] = "Lista de sesión para este número de visitante";
$MESS["STAT_VIEW_SESSIONS_LIST_BY_REF_1"] = "Lista de sesión para esta referer1";
$MESS["STAT_VIEW_SESSIONS_LIST_BY_REF_2"] = "Lista de sesión para esta referer2";
$MESS["STAT_VIEW_SESSIONS_LIST_BY_REF_3"] = "Lista de sesión para esta referer3";
?>