<?
$MESS["STAT_DATE"] = "Fecha";
$MESS["STAT_F_PERIOD"] = "Perido";
$MESS["STAT_F_SEARCHER_ID"] = "Motor de búsqueda:";
$MESS["STAT_GRAPH"] = "Motor de búsqueda: gráfico de indexación del sitio web";
$MESS["STAT_GRAPH_TITLE"] = "Gráfico de indexación de el sitio web con un motor de búsqueda";
$MESS["STAT_HITS"] = "Hits";
$MESS["STAT_HITS_LIST_OPEN"] = "Ver hits por motores de búsqueda";
$MESS["STAT_RECORDS_LIST"] = "Motor de búsqueda: dinámica de indexación del sitio web";
$MESS["STAT_SEARCHER_PAGES"] = "Registros";
$MESS["STAT_TOTAL_HITS"] = "Total de hits:";
?>