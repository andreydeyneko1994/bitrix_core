<?
$MESS["STAT_ADD_TO_STOPLIST_TITLE"] = "add IP to stop list";
$MESS["STAT_DATE"] = "Fecha";
$MESS["STAT_FL_DATE"] = "Fecha";
$MESS["STAT_FL_ID"] = "ID de motor de búsqueda";
$MESS["STAT_FL_IP"] = "Dirección IP";
$MESS["STAT_FL_LOGIC"] = "Lógica";
$MESS["STAT_FL_PAGE"] = "Página";
$MESS["STAT_FL_UA"] = "UserAgent";
$MESS["STAT_F_ID"] = "ID:";
$MESS["STAT_F_IP"] = "Dirección de IP:";
$MESS["STAT_F_PAGE"] = "Página:";
$MESS["STAT_F_SEARCH_SYSTEM"] = "Motor de búsqueda:";
$MESS["STAT_F_USER_AGENT"] = "UserAgent:";
$MESS["STAT_HIT_PAGES"] = "Hits";
$MESS["STAT_IP"] = "Dirección de IP";
$MESS["STAT_LINK_OPEN"] = "Abrir link";
$MESS["STAT_PAGE"] = "Página";
$MESS["STAT_RECORDS_LIST"] = "Motores de búsqueda de los último #STATISTIC_DAYS# días";
$MESS["STAT_SEARCHER"] = "Motor de búsqueda";
$MESS["STAT_SRCH_LIST"] = "Lista de motores de búsqueda";
$MESS["STAT_USER_AGENT"] = "UserAgent";
?>