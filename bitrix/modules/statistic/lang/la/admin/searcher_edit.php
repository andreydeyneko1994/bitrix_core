<?
$MESS["STAT_ACTIVE"] = "Activo:";
$MESS["STAT_CHAR_SET"] = "Búsqueda Frase codificada";
$MESS["STAT_CHECK_ACTIVITY"] = "Revisar límite de activiad:";
$MESS["STAT_DELETE_SEARCHER"] = "Eliminar motor de búsqueda";
$MESS["STAT_DELETE_SEARCHER_CONFIRM"] = "¿Esta seguro que desea eliminar este motor de búsqueda?";
$MESS["STAT_DOMAIN"] = "Dominio [%_]";
$MESS["STAT_DYNAMIC_KEEP_DAYS"] = "Días para guardar dinámicas:";
$MESS["STAT_EDIT_RECORD"] = "Configuración de motor de búsqueda # #ID#";
$MESS["STAT_FORGOT_NAME"] = "Complete el campo \"nombre\" por favor";
$MESS["STAT_FORM_ERROR_SAVE"] = "Guardar error";
$MESS["STAT_HIT_KEEP_DAYS"] = "Días para guardar hits:";
$MESS["STAT_NAME"] = "Nombre";
$MESS["STAT_NEW_RECORD"] = "Nuevo motor de búsqueda";
$MESS["STAT_NEW_SEARCHER"] = "Nuevo motor de búsqueda";
$MESS["STAT_PIE_CHART"] = "Incluir en los cuadros de indexación y los gráficos<br>por defecto:";
$MESS["STAT_PROP_TITLE"] = "Parámetros de motor de búsqueda";
$MESS["STAT_RECORDS_LIST"] = "Lista de motores de búsqueda";
$MESS["STAT_SEARCHER_DOMAINS"] = "Dominios:";
$MESS["STAT_STATISTICS"] = "Guardar hits:";
$MESS["STAT_USER_AGENT"] = "UserAgent:";
$MESS["STAT_VARIABLE"] = "Variables<br>(separadas por comas)";
?>