<?
$MESS["STAT_ADMIN"] = "acceso administrativo completo";
$MESS["STAT_CREATE_I2C_DB"] = "¿Regenerar índex para buscar el IP-para-país?";
$MESS["STAT_DENIED"] = "acceso denegado";
$MESS["STAT_INSTALL_DATABASE"] = "Instalar Módulo Para la Base de Datos:";
$MESS["STAT_INSTALL_TITLE"] = "Instalación del módulo de estadísticas";
$MESS["STAT_MAIN_DATABASE"] = "principal";
$MESS["STAT_MODULE_DESCRIPTION"] = "Este módulo colecta y visualiza las estadísticas del sitio web.";
$MESS["STAT_MODULE_NAME"] = "Estadísticas";
$MESS["STAT_START_GUESTS"] = "Valor inicial de visitantes:";
$MESS["STAT_START_HITS"] = "Valor inicial de hits:";
$MESS["STAT_START_HOSTS"] = "Valor inicial de invitados:";
$MESS["STAT_UNINSTALL_TITLE"] = "Desinstalar módulo de estadísticas";
$MESS["STAT_VIEW"] = "ver todas las estadísticas";
$MESS["STAT_VIEW_WITHOUT_MONEY"] = "ver estadísticas sin ningún parámetros financiero";
?>