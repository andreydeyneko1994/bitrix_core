<?
$MESS["STAT_CITY_GEOIP_MOD_CONTINENT"] = "Continente";
$MESS["STAT_CITY_GEOIP_MOD_DESCR"] = "Módulo Apache <a href=\"http://www.maxmind.com/app/mod_geoip\" target=\"_blank\">mod_geoip</a>. Versión requerida del módulo: 1.2.5 ó superior.";
$MESS["STAT_CITY_GEOIP_MOD_LATITUDE"] = "Longitud";
$MESS["STAT_CITY_GEOIP_MOD_LONGITUDE"] = "Latitud";
?>