<?
$MESS["STAT_CITY_GEOIP_PHP_DESCR"] = "Módulo para el acceso de la base de datos GeoIp en PHP. Esto es recomendado para usar la versión <a href=\"http://www.maxmind.com/app/php\" target=\"_blank\">latest module</a> GEOIP_DATABASE_FILE constante con la ruta completa para el IP de la base de datos deberá ser definido en dbconn.php.";
$MESS["STAT_CITY_GEOIP_PHP_LATITUDE"] = "Longitud";
$MESS["STAT_CITY_GEOIP_PHP_LONGITUDE"] = "Latitud";
$MESS["STAT_CITY_GEOIP_PHP_POSTAL_CODE"] = "Código Zip";
?>