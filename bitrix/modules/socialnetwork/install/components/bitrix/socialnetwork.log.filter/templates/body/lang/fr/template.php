<?
$MESS["SONET_C30_PRESET_FILTER_ALL"] = "Tous les évènements";
$MESS["SONET_C30_SMART_FOLLOW"] = "Mode Suivi intelligent";
$MESS["SONET_C30_SMART_FOLLOW_HINT"] = "Le mode « Suivi intelligent » est activé. Seuls les messages dont vous êtes l'auteur, le destinataire ou dans lesquels vous êtes mentionnés seront déplacés vers le haut. En cas d'ajout d'un commentaire vous suivrez automatiquement toutes les mises à jour du message.";
$MESS["SONET_C30_T_FILTER_COMMENTS"] = "Rechercher dans les commentaires";
$MESS["SONET_C30_T_FILTER_CREATED_BY"] = "Auteur";
$MESS["SONET_C30_T_FILTER_DATE"] = "Date";
$MESS["SONET_C30_T_FILTER_GROUP"] = "Groupe";
$MESS["SONET_C30_T_FILTER_TITLE"] = "Recherche";
$MESS["SONET_C30_T_RESET"] = "Annuler";
$MESS["SONET_C30_T_SHOW_HIDDEN"] = "Montrer les messages cachés";
$MESS["SONET_C30_T_SUBMIT"] = "Choisir";
?>