<?php
$MESS["SONET_GCE_AJAX_DELETE_FILE_FAILED"] = "Não é possível excluir o arquivo";
$MESS["SONET_GCE_AJAX_ERROR_NO_FILE"] = "O arquivo não foi enviado.";
$MESS["SONET_GCE_AJAX_ERROR_NO_IMAGE"] = "O arquivo não é um arquivo de imagem";
$MESS["SONET_GCE_AJAX_ERROR_WRONG_WORKGROUP_ID"] = "Grupo de trabalho/projeto não encontrado";
$MESS["SONET_GCE_AJAX_SAVE_FILE_FAILED"] = "Não é possível salvar o arquivo.";
