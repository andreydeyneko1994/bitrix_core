<?php
$MESS["SONET_GCE_T_KEYWORDS"] = "Mots-clés";
$MESS["SONET_GCE_T_KEYWORDS_ADD_TAG"] = "Ajouter plus";
$MESS["SONET_GCE_T_TAG_ADD"] = "Ajouter un tag";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_FOOTER_LABEL"] = "Créer un tag :";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_HINT"] = "Confirmer ?";
$MESS["SONET_GCE_T_TAG_SEARCH_FAILED"] = "Ce tag n'existe pas";
