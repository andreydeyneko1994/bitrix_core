<?php
$MESS["SONET_GCE_T_ADD_DEPT_HINT"] = "Seleccionar un departamento hará que todos los empleados del departamento sean miembros del grupo de trabajo sin confirmación.";
$MESS["SONET_GCE_T_ADD_DEPT_HINT_PROJECT"] = "Al seleccionar un departamento, todos los empleados del departamento serán los miembros del proyecto sin confirmación.";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Agregar empleado";
$MESS["SONET_GCE_T_ADD_OWNER2"] = "Asignar";
$MESS["SONET_GCE_T_ADD_USER"] = "Agregar usuario";
$MESS["SONET_GCE_T_DEST_LINK_2"] = "Agregar más";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS"] = "Moderadores del grupo de trabajo";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_PROJECT"] = "Moderadores del proyecto";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER"] = "Propietario del grupo de trabajo";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER_PROJECT"] = "Propietario del proyecto";
$MESS["SONET_GCE_T_SCRUM_OWNER"] = "Propietario del producto";
$MESS["SONET_GCE_T_TEAM_TITLE"] = "Miembros del equipo";
$MESS["SONET_GCE_T_TEAM_TITLE_USER"] = "Equipo";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_PROJECT"] = "Equipo";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_SCRUM"] = "Partes interesadas";
