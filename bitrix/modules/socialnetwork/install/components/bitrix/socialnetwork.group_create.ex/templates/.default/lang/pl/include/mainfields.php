<?php
$MESS["SONET_GCE_T_DESCRIPTION"] = "Opis grupy roboczej";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL"] = "Dodaj opis grupy roboczej";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_PROJECT"] = "Dodaj opis projektu dla członków zespołu";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_SCRUM"] = "Dodaj opis Scrum dla członków zespołu";
$MESS["SONET_GCE_T_DESCRIPTION_PROJECT"] = "Opis projektu";
$MESS["SONET_GCE_T_DESCRIPTION_SCRUM"] = "Opis zespołu Scrum";
$MESS["SONET_GCE_T_DESCRIPTION_SWITCHER"] = "dodaj opis";
$MESS["SONET_GCE_T_NAME3"] = "Nazwa grupy roboczej";
$MESS["SONET_GCE_T_NAME3_PROJECT"] = "Nazwa projektu";
$MESS["SONET_GCE_T_NAME3_SCRUM"] = "Nazwa zespołu Scrum";
