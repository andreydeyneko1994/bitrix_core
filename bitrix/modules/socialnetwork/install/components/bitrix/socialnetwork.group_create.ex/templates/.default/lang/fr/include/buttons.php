<?php
$MESS["SONET_GCE_T_DO_CREATE"] = "Créer un groupe";
$MESS["SONET_GCE_T_DO_CREATE_PROJECT"] = "Créer un projet";
$MESS["SONET_GCE_T_DO_EDIT_2"] = "Changer";
$MESS["SONET_GCE_T_DO_INVITE"] = "Envoyer un message";
$MESS["SONET_GCE_T_DO_NEXT"] = "Suivant";
$MESS["SONET_GCE_T_T_BACK"] = "Retour";
$MESS["SONET_GCE_T_T_CANCEL"] = "Annuler";
