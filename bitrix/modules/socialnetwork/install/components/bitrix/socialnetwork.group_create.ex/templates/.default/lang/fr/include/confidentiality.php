<?php
$MESS["SONET_GCE_T_SLIDE_CONFIDENTIALITY_SUBTITLE"] = "Le niveau de confidentialité permet de déterminer qui peut rejoindre et consulter les détails de l'équipe.";
$MESS["SONET_GCE_T_SLIDE_CONFIDENTIALITY_TITLE"] = "Niveau de confidentialité";
