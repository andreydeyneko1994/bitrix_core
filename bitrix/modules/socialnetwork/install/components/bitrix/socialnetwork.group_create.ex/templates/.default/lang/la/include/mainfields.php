<?php
$MESS["SONET_GCE_T_DESCRIPTION"] = "Descripción del grupo de trabajo";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL"] = "Agregar la descripción del grupo de trabajo";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_PROJECT"] = "Agregue la descripción del proyecto para los miembros del equipo";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_SCRUM"] = "Agregue la descripción del scrum para los miembros del equipo";
$MESS["SONET_GCE_T_DESCRIPTION_PROJECT"] = "Descripción del proyecto";
$MESS["SONET_GCE_T_DESCRIPTION_SCRUM"] = "Descripción del equipo Scrum";
$MESS["SONET_GCE_T_DESCRIPTION_SWITCHER"] = "agregar descripción";
$MESS["SONET_GCE_T_NAME3"] = "Nombre del grupo de trabajo";
$MESS["SONET_GCE_T_NAME3_PROJECT"] = "Nombre del proyecto";
$MESS["SONET_GCE_T_NAME3_SCRUM"] = "Nombre del equipo Scrum";
