<?php
$MESS["SONET_GCE_T_IMAGE3"] = "Icône du groupe de travail";
$MESS["SONET_GCE_T_IMAGE3_PROJECT"] = "Icône du projet";
$MESS["SONET_GCE_T_IMAGE3_SCRUM"] = "Icône de l'équipe scrum";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cette image ?";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_NO"] = "Non";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_YES"] = "Oui";
$MESS["SONET_GCE_T_UPLOAD"] = "Télécharger";
