<?php
$MESS["SONET_GCE_T_KEYWORDS"] = "Palabras claves";
$MESS["SONET_GCE_T_KEYWORDS_ADD_TAG"] = "Agregar más";
$MESS["SONET_GCE_T_TAG_ADD"] = "Agregar etiqueta";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_FOOTER_LABEL"] = "Crear etiqueta:";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_HINT"] = "¿Está seguro?";
$MESS["SONET_GCE_T_TAG_SEARCH_FAILED"] = "Esta etiqueta no existe";
