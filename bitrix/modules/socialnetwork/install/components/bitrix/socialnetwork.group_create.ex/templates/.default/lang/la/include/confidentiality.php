<?php
$MESS["SONET_GCE_T_SLIDE_CONFIDENTIALITY_SUBTITLE"] = "El nivel de privacidad ayuda a administrar quién puede unirse y ver los detalles del equipo.";
$MESS["SONET_GCE_T_SLIDE_CONFIDENTIALITY_TITLE"] = "Nivel de privacidad";
