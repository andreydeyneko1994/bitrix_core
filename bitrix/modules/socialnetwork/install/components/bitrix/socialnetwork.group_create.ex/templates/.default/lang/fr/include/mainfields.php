<?php
$MESS["SONET_GCE_T_DESCRIPTION"] = "Description du groupe de travail";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL"] = "Ajouter une description du groupe de travail";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_PROJECT"] = "Ajouter une description du projet pour les membres de l'équipe";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_SCRUM"] = "Ajouter une description scrum pour les membres de l'équipe";
$MESS["SONET_GCE_T_DESCRIPTION_PROJECT"] = "Description du projet";
$MESS["SONET_GCE_T_DESCRIPTION_SCRUM"] = "Description de l'équipe Scrum";
$MESS["SONET_GCE_T_DESCRIPTION_SWITCHER"] = "ajouter une description";
$MESS["SONET_GCE_T_NAME3"] = "Nom du groupe de travail";
$MESS["SONET_GCE_T_NAME3_PROJECT"] = "Nom du projet";
$MESS["SONET_GCE_T_NAME3_SCRUM"] = "Nom de l'équipe scrum";
