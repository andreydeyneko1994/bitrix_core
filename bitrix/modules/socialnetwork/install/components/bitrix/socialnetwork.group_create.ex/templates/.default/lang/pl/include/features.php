<?php
$MESS["SONET_GCE_TAB_2"] = "Właściwości";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION"] = "Narzędzia dla grup roboczych";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_PROJECT"] = "Narzędzia projektowe";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_SCRUM"] = "Narzędzia zespołu Scrum";
$MESS["SONET_GCE_T_FEATURES_HINT"] = "Wybierz narzędzia dla zespołu. W razie potrzeby możesz zawsze zmienić wybór.";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Rozszerzone parametry";
