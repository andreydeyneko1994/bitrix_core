<?php
$MESS["SONET_GCE_T_AJAX_ERROR"] = "Error al procesar los datos del formulario. Vuelva a intentarlo.";
$MESS["SONET_GCE_T_DEST_EXTRANET"] = "Extranet";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_EMAIL_TITLE"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_LAST_NAME_TITLE"] = "Apellido";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_NAME_TITLE"] = "Nombre";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_SEND_PASSWORD_TITLE"] = "Enviar datos de inicio de sesión al e-mail especificado.";
$MESS["SONET_GCE_T_DEST_EXTRANET_EMAIL_SHORT"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_INVITE_MESSAGE_TITLE"] = "Este es el mensaje de invitación que las personas recibirán:";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR"] = "y/o #ACTION# un nuevo";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_ADD"] = "agregar";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_INVITE"] = "invitar";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_SCRUM_PROJECT"] = "Equipo SCRUM";
$MESS["SONET_GCE_T_STRING_FIELD_ERROR"] = "El campo es obligatorio.";
$MESS["SONET_GCE_T_SUCCESS_CREATE"] = "El grupo se ha creado correctamente.";
$MESS["SONET_GCE_T_SUCCESS_EDIT"] = "Los parámetros del grupo se han cambiado correctamente.";
$MESS["SONET_GCE_T_WIZARD_DESCRIPTION"] = "Forme un equipo con otros usuarios y utilice las herramientas disponibles para obtener los máximos resultados.";
$MESS["SONET_GCE_T_WIZARD_TITLE"] = "Administre su equipo";
