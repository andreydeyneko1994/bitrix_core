<?php
$MESS["SONET_GCE_TAB_2"] = "Características";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION"] = "Ferramentas do grupo de trabalho";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_PROJECT"] = "Ferramentas do projeto";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_SCRUM"] = "Ferramentas da equipe Scrum";
$MESS["SONET_GCE_T_FEATURES_HINT"] = "Selecione as ferramentas para a sua equipe. Você sempre pode alterar sua seleção, se necessário.";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Parâmetros ampliados";
