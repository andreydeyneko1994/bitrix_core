<?php
$MESS["SONET_GCE_T_IMAGE3"] = "Ícone do grupo de trabalho";
$MESS["SONET_GCE_T_IMAGE3_PROJECT"] = "Ícone do projeto";
$MESS["SONET_GCE_T_IMAGE3_SCRUM"] = "Ícone da equipe Scrum";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir esta imagem?";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_NO"] = "Não";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_YES"] = "Sim";
$MESS["SONET_GCE_T_UPLOAD"] = "Carregar";
