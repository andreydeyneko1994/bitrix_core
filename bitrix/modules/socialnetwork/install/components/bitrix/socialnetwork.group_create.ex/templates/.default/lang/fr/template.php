<?php
$MESS["SONET_GCE_T_AJAX_ERROR"] = "Erreur lors du traitement des données du formulaire. Veuillez réessayer plus tard.";
$MESS["SONET_GCE_T_DEST_EXTRANET"] = "Extranet";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_EMAIL_TITLE"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_LAST_NAME_TITLE"] = "Nom de famille";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_NAME_TITLE"] = "Prénom";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_SEND_PASSWORD_TITLE"] = "Envoyer les données pour accèder à l'adresse email indiquée";
$MESS["SONET_GCE_T_DEST_EXTRANET_EMAIL_SHORT"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_INVITE_MESSAGE_TITLE"] = "Voici le message que recevront les personnes invitées : ";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR"] = "et/ou #ACTION# un nouveau";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_ADD"] = "ajouter";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_INVITE"] = "inviter";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_SCRUM_PROJECT"] = "Équipe SCRUM";
$MESS["SONET_GCE_T_STRING_FIELD_ERROR"] = "Le champ est requis.";
$MESS["SONET_GCE_T_SUCCESS_CREATE"] = "Groupe créé avec succès.";
$MESS["SONET_GCE_T_SUCCESS_EDIT"] = "Les paramètres du groupe sont modifiés avec succès.";
$MESS["SONET_GCE_T_WIZARD_DESCRIPTION"] = "Faites équipe avec d'autres utilisateurs et utilisez les outils disponibles pour obtenir un maximum de résultats.";
$MESS["SONET_GCE_T_WIZARD_TITLE"] = "Gérez votre équipe";
