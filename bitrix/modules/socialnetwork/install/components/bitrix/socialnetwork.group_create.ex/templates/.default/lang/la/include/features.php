<?php
$MESS["SONET_GCE_TAB_2"] = "Características";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION"] = "Herramientas del grupo de trabajo";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_PROJECT"] = "Herramientas del proyecto";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_SCRUM"] = "Herramientas del equipo Scrum";
$MESS["SONET_GCE_T_FEATURES_HINT"] = "Seleccione las herramientas para su equipo. Siempre podrá cambiar la selección si es necesario.";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Parámetros extendidos";
