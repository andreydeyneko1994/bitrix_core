<?php
$MESS["SONET_GCE_T_AJAX_ERROR"] = "Błąd przetwarzania danych formularza. Prosimy spróbować ponownie.";
$MESS["SONET_GCE_T_DEST_EXTRANET"] = "Extranet";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_EMAIL_TITLE"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_LAST_NAME_TITLE"] = "Nazwisko";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_NAME_TITLE"] = "Imię";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_SEND_PASSWORD_TITLE"] = "Wyślij dane logowania na podane adresy email";
$MESS["SONET_GCE_T_DEST_EXTRANET_EMAIL_SHORT"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_INVITE_MESSAGE_TITLE"] = "Zaproszone osoby otrzymają następującą wiadomość:";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR"] = "i/lub #ACTION# nowy";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_ADD"] = "Dodawanie";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_INVITE"] = "Zaproś";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_SCRUM_PROJECT"] = "Zespół SCRUM";
$MESS["SONET_GCE_T_STRING_FIELD_ERROR"] = "Pole jest wymagane.";
$MESS["SONET_GCE_T_SUCCESS_CREATE"] = "Grupa została utworzona pomyślnie.";
$MESS["SONET_GCE_T_SUCCESS_EDIT"] = "Parametry grupy zostały pomyślnie zmienione.";
$MESS["SONET_GCE_T_WIZARD_DESCRIPTION"] = "Połącz siły z innymi użytkownikami i wykorzystaj dostępne narzędzia, aby uzyskać maksymalne wyniki.";
$MESS["SONET_GCE_T_WIZARD_TITLE"] = "Zarządzaj zespołem";
