<?php
$MESS["SONET_GCE_T_IMAGE3"] = "Ícono del grupo de trabajo";
$MESS["SONET_GCE_T_IMAGE3_PROJECT"] = "Ícono del proyecto";
$MESS["SONET_GCE_T_IMAGE3_SCRUM"] = "Ícono del equipo Scrum";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM"] = "¿Seguro que desea eliminar esta imagen?";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_NO"] = "No";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_YES"] = "Sí";
$MESS["SONET_GCE_T_UPLOAD"] = "Cargar";
