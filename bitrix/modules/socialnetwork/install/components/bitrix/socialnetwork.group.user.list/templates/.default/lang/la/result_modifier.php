<?php
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST"] = "Aceptar solicitud";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_PROJECT_TITLE"] = "Aceptar solicitud para unirse al proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_SCRUM_TITLE"] = "Aceptar solicitud para unirse al scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_TITLE"] = "Aceptar solicitud para unirse al grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST"] = "Cancelar solicitud";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_PROJECT_TITLE"] = "Cancelar la solicitud del proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_SCRUM_TITLE"] = "Cancelar la solicitud del scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_TITLE"] = "Cancelar la solicitud del grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST"] = "Cancelar invitación";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_PROJECT_TITLE"] = "Cancelar invitación al proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_SCRUM_TITLE"] = "Cancelar invitación al scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_TITLE"] = "Cancelar invitación al grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE"] = "Eliminar";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_PROJECT_TITLE"] = "Eliminar del proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_SCRUM_TITLE"] = "Eliminar del scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_TITLE"] = "Eliminar del grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST"] = "Rechazar solicitud";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_PROJECT_TITLE"] = "Rechazar solicitud para unirse al proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_SCRUM_TITLE"] = "Rechazar solicitud para unirse al scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_TITLE"] = "Rechazar solicitud para unirse al grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR"] = "Eliminar de los moderadores";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_PROJECT"] = "Eliminar de los asistentes del administrador";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_PROJECT_TITLE"] = "Eliminar de los asistentes del administrador del proyecto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_SCRUM"] = "Eliminar del equipo de desarrollo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_SCRUM_TITLE"] = "Eliminar del equipo de desarrollo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_TITLE"] = "Eliminar de los moderadores del grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR"] = "Convertir en moderador";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_PROJECT"] = "Convertir en asistente del administrador";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_PROJECT_TITLE"] = "Convertir en asistente del administrador de proyectos";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_SCRUM"] = "Agregar al equipo de desarrollo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_SCRUM_TITLE"] = "Agregar al equipo de desarrollo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_TITLE"] = "Convertir en moderador del grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER"] = "Convertir en propietario";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_PROJECT"] = "Convertir en administrador";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_PROJECT_TITLE"] = "Convertir en jefe de proyectos";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_SCRUM"] = "Convertir en propietario";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_SCRUM_TITLE"] = "Convertir el propietario del producto";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_TITLE"] = "Convertir en propietario del grupo de trabajo";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_SCRUM_MASTER"] = "Convertir en scrum master";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_VIEW"] = "Ver perfil";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_VIEW_TITLE"] = "Ver el perfil del usuario";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_BUTTON_INVITE_TITLE"] = "Invitar";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_DEPARTMENT_DISCONNECT"] = "desconectar";
