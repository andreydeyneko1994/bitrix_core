<?php
$MESS["SCVC_TEMPLATE_LICENSE_TEXT"] = "El contador de vista del post del noticias está disponible en los <a href=\"/settings/license_all.php\" target=\"_blank\">planes comerciales seleccionados</a>. <br><br>Para ver la cantidad de vistas y lectores de una publicación en particular, desplace el puntero del mouse sobre el icono.";
$MESS["SCVC_TEMPLATE_LICENSE_TITLE"] = "Ampliación del noticias";
$MESS["SCVC_TEMPLATE_POPUP_TITLE"] = "Vistas";
