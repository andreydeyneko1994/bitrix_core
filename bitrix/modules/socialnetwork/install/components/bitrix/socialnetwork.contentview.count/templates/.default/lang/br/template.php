<?php
$MESS["SCVC_TEMPLATE_LICENSE_TEXT"] = "O contador de exibição de postagem do Feed está disponível nos <a href=\"/settings/license_all.php\" target=\"_blank\">planos comerciais selecionados</a>.<br><br>Para exibir o número de visualizações e leitores de um determinado lugar, passe o cursor do mouse sobre o ícone.";
$MESS["SCVC_TEMPLATE_LICENSE_TITLE"] = "Feed Estendido";
$MESS["SCVC_TEMPLATE_POPUP_TITLE"] = "Visualizações";
