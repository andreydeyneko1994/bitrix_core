<?php
$MESS["SONET_LOG_ENTRY_COMMENT_ADD_ERROR"] = "Não é possível adicionar comentário.";
$MESS["SONET_LOG_ENTRY_COMMENT_DELETE_ERROR"] = "Não é possível excluir comentário.";
$MESS["SONET_LOG_ENTRY_COMMENT_EDIT_ERROR"] = "Não é possível editar comentário.";
