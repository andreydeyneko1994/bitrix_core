<?php
$MESS["SONET_LOG_ENTRY_COMMENT_ADD_ERROR"] = "Ajout de commentaire impossible.";
$MESS["SONET_LOG_ENTRY_COMMENT_DELETE_ERROR"] = "Suppression de commentaire impossible.";
$MESS["SONET_LOG_ENTRY_COMMENT_EDIT_ERROR"] = "Modification de commentaire impossible.";
