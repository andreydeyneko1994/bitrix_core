<?php
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SONET_GROUP"] = "Skontaktuj się z właścicielem grupy roboczej lub administratorem Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_USER"] = "Skontaktuj się z administratorem Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SONET_GROUP"] = "Nie znaleziono grupy roboczej lub odmowa dostępu";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_USER"] = "Nie znaleziono użytkownika lub odmowa dostępu";
