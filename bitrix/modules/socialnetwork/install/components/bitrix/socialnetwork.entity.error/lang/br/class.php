<?php
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SONET_GROUP"] = "Por favor, entre em contato com o proprietário do grupo de trabalho ou com seu administrador Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_USER"] = "Por favor, entre em contato com seu administrador Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SONET_GROUP"] = "O grupo de trabalho não foi encontrado ou o acesso foi negado";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_USER"] = "O usuário não foi encontrado ou o acesso foi negado";
