<?php
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SONET_GROUP"] = "Veuillez contacter le propriétaire du groupe de travail ou votre administrateur Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_USER"] = "Veuillez contacter votre administrateur Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SONET_GROUP"] = "Le groupe de travail est introuvable ou son accès est refusé";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_USER"] = "Utilisateur introuvable ou accès refusé";
