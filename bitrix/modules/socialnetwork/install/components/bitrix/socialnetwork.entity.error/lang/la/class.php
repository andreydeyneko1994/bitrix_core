<?php
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SONET_GROUP"] = "Comuníquese con el propietario del grupo de trabajo o con su administrador de Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_USER"] = "Comuníquese con su administrador de Bitrix24";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SONET_GROUP"] = "No se encontró el grupo de trabajo o se denegó el acceso";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_USER"] = "No se encontró el usuario o se denegó el acceso";
