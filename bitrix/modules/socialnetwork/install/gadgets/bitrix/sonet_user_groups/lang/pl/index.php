<?
$MESS["GD_SONET_USER_GROUPS_ALL_GROUPS"] = "Wszystkie grupy";
$MESS["GD_SONET_USER_GROUPS_CREATE_GROUP"] = "Utwórz Grupę";
$MESS["GD_SONET_USER_GROUPS_GR_UNAVAIL"] = "Lista grup jest niedostępna.";
$MESS["GD_SONET_USER_GROUPS_LOG"] = "Aktualizacje";
$MESS["GD_SONET_USER_GROUPS_NO_GROUPS"] = "Brak grup.";
$MESS["GD_SONET_USER_GROUPS_SEARCH_GROUP"] = "Wyszukuj grup";
?>