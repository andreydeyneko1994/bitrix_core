<?
$MESS["GD_SONET_GROUP_LINKS_ACT_BAN"] = "Lista banów";
$MESS["GD_SONET_GROUP_LINKS_ACT_DELETE"] = "Usuń grupę";
$MESS["GD_SONET_GROUP_LINKS_ACT_EDIT"] = "Edytuj grupę";
$MESS["GD_SONET_GROUP_LINKS_ACT_EXIT"] = "Opuść grupę";
$MESS["GD_SONET_GROUP_LINKS_ACT_FEAT"] = "Edytuj ustawienia";
$MESS["GD_SONET_GROUP_LINKS_ACT_JOIN"] = "Dołącz do grupy";
$MESS["GD_SONET_GROUP_LINKS_ACT_MOD"] = "Edytuj moderatorów";
$MESS["GD_SONET_GROUP_LINKS_ACT_MOD1"] = "Moderatorzy";
$MESS["GD_SONET_GROUP_LINKS_ACT_REQU"] = "Zaproś do grupy";
$MESS["GD_SONET_GROUP_LINKS_ACT_SUBSCRIBE"] = "Subskrypcja";
$MESS["GD_SONET_GROUP_LINKS_ACT_USER"] = "Edytuj członków";
$MESS["GD_SONET_GROUP_LINKS_ACT_USER1"] = "Członkowie";
$MESS["GD_SONET_GROUP_LINKS_ACT_VREQU"] = "Zaproszenia do członkostwa w grupie roboczej i zapytania";
$MESS["GD_SONET_GROUP_LINKS_ACT_VREQU_OUT"] = "Zaproszenia do grupy";
$MESS["GD_SONET_GROUP_LINKS_SEND_MESSAGE_GROUP"] = "Napisz wiadomość";
$MESS["GD_SONET_GROUP_LINKS_SEND_MESSAGE_GROUP_TITLE"] = "Napisz wiadomość do członków grupy";
?>