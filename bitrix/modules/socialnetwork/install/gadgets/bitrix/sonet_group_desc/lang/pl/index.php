<?
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "Archiwum grupy";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "Utworzony";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "Opis";
$MESS["GD_SONET_GROUP_DESC_NAME"] = "Grupa";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "Członkowie";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE"] = "Już wysłałeś podanie o dołączenie do grupy roboczej.";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE_BY_GROUP"] = "Zaproszenie o dołączenie do tej grupy roboczej zostało już do ciebie wysłane. Możesz potwierdzić swoje członkostwo na <a href='#LINK#'>stronie zaproszenia</a>.";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "Temat";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "Typ grupy";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "To jest grupa publiczna. Każdy może do niej dołączyć";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "To jest grupa prywatna. Członkostwo zależy od akceptacji przez administratora.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "Ta grupa jest widoczna dla każdego.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "Ta grupa jest niewidoczna. Tylko członkowie grupy mogą ją widzieć.";
?>