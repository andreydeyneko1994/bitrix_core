<?
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "Archivo de Grupo";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "Creado";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "Descripción";
$MESS["GD_SONET_GROUP_DESC_NAME"] = "Grupo";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "Miembros";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE"] = "Usted ya ha enviado una solicitud para unirse a este grupo de trabajo.";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE_BY_GROUP"] = "La invitación a unirse a este grupo de trabajo ya ha sido enviada a usted. Puede confirmar su membresía en <a href='#LINK#'>la página de inviaciones</a>.";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "Tema";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "Tipo de Grupo";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "Este es un grupo público. Cualquiera puede unirse.";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "Este es un grupo privado. La membresía del usuario está sujeto a la aprobación del administrador.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "Este grupo es visible para cualquiera.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "Este grupo es no visible. Sólo los miembros del grupo pueden verlo.";
?>