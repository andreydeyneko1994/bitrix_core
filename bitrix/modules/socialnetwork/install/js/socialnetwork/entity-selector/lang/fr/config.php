<?php
$MESS["SOCNET_ENTITY_SELECTOR_CREATE_PROJECT"] = "Créer un groupe de travail";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT"] = "<employee>Inviter un utilisateur</employee><span>ou</span><project>créer un groupe de travail</project>";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT_OR_GUEST"] = "<employee>Inviter un utilisateur</employee><span>ou</span><project>une personne externe</project><span>ou</span><guest>créer un groupe de travail</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_GUEST_HINT"] = "Vous pouvez également ajouter votre partenaire ou client via l'e-mail.";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_USERS_TAB_TITLE"] = "Personnes invitées";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE"] = "Inviter l'employé";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_GUEST"] = "<employee>Inviter un utilisateur</employee><span>ou</span><guest>Inviter une personne externe</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_GUEST"] = "Inviter un invité";
$MESS["SOCNET_ENTITY_SELECTOR_PROJECT_OR_GUEST"] = "<project>Créer un groupe de travail</project><span>ou</span><guest>inviter une personne externe</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_TAG_FOOTER_LABEL"] = "Commencez à taper pour créer un nouveau tag";
