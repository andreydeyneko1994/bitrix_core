<?php
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "A tarefa \"#TASK_NAME#\" foi criada numa mensagem de Feed.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_FORUM_TOPIC"] = "A tarefa \"#TASK_NAME#\" foi criada com base em uma postagem do fórum.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_TASK"] = "A tarefa \"#TASK_NAME#\" foi criada com base em outra tarefa.";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "editado o arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "editado o arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "editado o arquivo";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Compartilhado com: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Compartilhado com: #SHARE_LIST#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BITRIX24_NEW_USER"] = "A tarefa foi criada com base em uma #A_BEGIN#inserção adicional de novo usuário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_COMMENT"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de postagem do Feed#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_POST"] = "A tarefa foi criada com base em uma #A_BEGIN#postagem do Feed#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_CALENDAR_EVENT"] = "A tarefa foi criada com base em um #A_BEGIN#evento do calendário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de postagem do Feed#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_CALENDAR"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de evento do calendário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_FORUM_TOPIC"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de postagem do fórum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_LISTS_NEW_ELEMENT"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de uma inserção do fluxo de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_PHOTO_PHOTO"] = "A tarefa foi criada com base em um #A_BEGIN#comentário da foto do álbum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TASK"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de tarefa#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_ENTRY"] = "A tarefa foi criada com base em uma #A_BEGIN#atualização das horas de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_REPORT"] = "A tarefa foi criada com base em um #A_BEGIN#comentário do relatório de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_WIKI"] = "A tarefa foi criada com base em um #A_BEGIN#comentário da página Wiki#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_TOPIC"] = "A tarefa foi criada com base em uma #A_BEGIN#postagem do fórum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_INTRANET_NEW_USER"] = "A tarefa foi criada com base em uma #A_BEGIN#inserção adicional de novo usuário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LISTS_NEW_ELEMENT"] = "A tarefa foi criada com base em uma #A_BEGIN#inserção do fluxo de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT"] = "A tarefa foi criada com base em um #A_BEGIN#comentário de postagem do Feed#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_BITRIX24_NEW_USER"] = "A tarefa foi criada com base em um #A_BEGIN#comentário deixado em uma inserção adicional de novo usuário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_INTRANET_NEW_USER"] = "A tarefa foi criada com base em um #A_BEGIN#comentário deixado em uma inserção adicional de novo usuário#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_ENTRY"] = "A tarefa foi criada com base em uma #A_BEGIN#postagem do Feed#A_END# obtida de uma fonte externa";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_ALBUM"] = "A tarefa foi criada com base em #A_BEGIN#álbum da Galeria de Fotos#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_PHOTO"] = "A tarefa foi criada com base na #A_BEGIN#foto do álbum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TASK"] = "A tarefa foi criada com base em #A_BEGIN#outra tarefa#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_ENTRY"] = "A tarefa foi criada com base em uma #A_BEGIN#atualização das horas de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_REPORT"] = "A tarefa foi criada com base em um #A_BEGIN#relatório de trabalho#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_WIKI"] = "A tarefa foi criada com base em uma #A_BEGIN#página Wiki#A_END#";
