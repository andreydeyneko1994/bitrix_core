<?
$MESS["BLOG_VIDEO_RECORD_AGREE"] = "Permitir";
$MESS["BLOG_VIDEO_RECORD_ASK_PERMISSIONS"] = "Você tem que permitir o acesso à sua câmera e microfone para gravar um vídeo.";
$MESS["BLOG_VIDEO_RECORD_BUTTON"] = "Gravar Vídeo";
$MESS["BLOG_VIDEO_RECORD_CANCEL_BUTTON"] = "Cancelar";
$MESS["BLOG_VIDEO_RECORD_CLOSE"] = "Fechar";
$MESS["BLOG_VIDEO_RECORD_ERROR_CHROME_HTTPS"] = "Infelizmente, seu navegador não suporta o protocolo HTTP.<br /><br />Por favor, tente outro navegador, por exemplo, o Firefox.";
$MESS["BLOG_VIDEO_RECORD_IN_PROGRESS_LABEL"] = "Gravação em Andamento";
$MESS["BLOG_VIDEO_RECORD_LOGO"] = "<span class=\"logo\"><span class=\"logo-text\">Bitrix</span><span class=\"logo-color\">24</span></span>";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR"] = "Não é possível acessar câmera e microfone.";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR_TITLE"] = "Erro";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_TITLE"] = "Acesso ao Dispositivo";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS"] = "Infelizmente, o seu navegador não é compatível com gravação de vídeo. <br/><br/> Você pode tentar outro navegador, como uma versão mais recente do FireFox ou Chrome.";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS_TITLE"] = "Atenção";
$MESS["BLOG_VIDEO_RECORD_RESTART_BUTTON"] = "Grave novamente";
$MESS["BLOG_VIDEO_RECORD_SPOTLIGHT_MESSAGE"] = "<b>Grave vídeos e compartilhe com sua equipe.</b>";
$MESS["BLOG_VIDEO_RECORD_STOP_BUTTON"] = "Interromper";
$MESS["BLOG_VIDEO_RECORD_TRANFORM_LIMIT_TEXT"] = "Para tornar o vídeo visível no Bitrix24, pare de gravar em <span class=\"bx-videomessage-transform-time-tip\">60 segundos</span>. Os arquivos de vídeo mais longos certamente serão salvos, mas podem não ser reproduzidos em todos os navegadores.";
$MESS["BLOG_VIDEO_RECORD_USE_BUTTON"] = "Usar Vídeo";
?>