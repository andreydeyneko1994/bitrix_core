<?php
$MESS["BLG_NAME"] = "Iniciado por";
$MESS["BLG_SHARE"] = "Compartilhado com: ";
$MESS["BLG_SHARE_1"] = "Compartilhado com: ";
$MESS["BLG_SHARE_ALL"] = "Todos os empregados";
$MESS["BLG_SHARE_ALL_BUS"] = "Todos os usuários";
$MESS["BLG_SHARE_HIDDEN_1"] = "Destinatário oculto";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Por favor, especifique pelo menos uma pessoa.";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "A conversa não pode ser endereçada a todos, especifique pelo menos um destinatário.";
$MESS["SBPE_EXISTING_POST_PREMODERATION"] = "Não é possível publicar porque um grupo pré-moderado de usuários está selecionado como destinatário.";
$MESS["SBPE_MULTIPLE_PREMODERATION"] = "Não é possível publicar a mensagem porque alguns dos grupos selecionados são pré-moderados.";
$MESS["SBPE_MULTIPLE_PREMODERATION2"] = "Não é possível publicar itens porque a pré-moderação está ativada nestes grupos de trabalho: #GROUPS_LIST#";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Visitante não autorizado";
$MESS["SONET_HELPER_NO_PERMISSIONS"] = "O acesso a um ou mais destinatários da mensagem foi negado";
$MESS["SONET_HELPER_PAGE_TITLE_WORKGROUP_TEMPLATE"] = "#WORKGROUP#: #TITLE#";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Reindexação do Feed";
$MESS["SONET_HELPER_STEPPER_LIVEFEED2"] = "Reindexar publicações do Feed";
$MESS["SONET_HELPER_VIDEO_CONVERSION_COMPLETED"] = "O arquivo de vídeo anexado à sua mensagem \"#POST_TITLE#\" foi convertido. Agora ele está disponível para os destinatários.";
