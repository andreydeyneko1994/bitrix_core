<?
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE"] = "Grupo de trabalho: \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE_PROJECT"] = "Projeto: \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED"] = "O grupo de trabalho \"#GROUP_NAME#\" não está mais vinculado a este bate-papo.";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED_PROJECT"] = "O projeto \"#GROUP_NAME#\" não está mais vinculado a este bate-papo.";
?>