<?php
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_FOOTER_INFO"] = "Esta visualização mostra apenas os funcionários demitidos que possuem links para itens do CRM pelo campo que você selecionou. Se a visualização não mostrar o funcionário que você está procurando, significa que ele não tem dependências de CRM.";
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_TAB_TITLE"] = "Pessoas dispensadas";
