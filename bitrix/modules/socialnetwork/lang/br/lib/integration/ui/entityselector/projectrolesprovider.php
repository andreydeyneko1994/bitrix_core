<?php
$MESS["SES_PROJECT_EMPLOYER_ROLE"] = "Membros";
$MESS["SES_PROJECT_MODERATOR_ROLE"] = "Moderadores";
$MESS["SES_PROJECT_OWNER_ROLE"] = "Proprietário do projeto";
$MESS["SES_PROJECT_ROLES_TAB_TITLE"] = "Funções";
$MESS["SES_PROJECT_SCRUM_MASTER_ROLE"] = "Scrum master";
$MESS["SES_PROJECT_SCRUM_MODERATOR_ROLE"] = "Equipe de desenvolvimento";
$MESS["SES_PROJECT_SCRUM_OWNER_ROLE"] = "Proprietário do projeto";
