<?php
$MESS["SONET_LOG_COMMENT_EMPTY"] = "O texto da mensagem está vazio.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Você não tem permissão para adicionar comentários.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS_UPDATE"] = "Sem permissão para editar comentário";
