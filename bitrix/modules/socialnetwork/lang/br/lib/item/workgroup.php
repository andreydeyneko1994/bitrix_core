<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Proprietário e moderadores do grupo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Proprietário e moderadores do projeto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Apenas o proprietário do grupo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Apenas o proprietário do projeto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Todos os membros do grupo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Todos os participantes do projeto";
