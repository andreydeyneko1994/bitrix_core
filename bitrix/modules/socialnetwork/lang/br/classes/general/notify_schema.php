<?php
$MESS["SONET_NS_FRIEND"] = "Listado ou não listado como amigo";
$MESS["SONET_NS_INOUT_GROUP"] = "Alterações na participação do grupo (para moderadores)";
$MESS["SONET_NS_INVITE_GROUP_BTN"] = "Convite ou solicitação para entrar no grupo de trabalho recebidos; removido do grupo de trabalho";
$MESS["SONET_NS_INVITE_GROUP_INFO"] = "Alteração de membros do grupo de trabalho";
$MESS["SONET_NS_INVITE_USER"] = "Notificação de Amizade";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Você foi atribuído ou não atribuído como um moderador de grupo de trabalho";
$MESS["SONET_NS_OWNER_GROUP"] = "Alteração de proprietário do Grupo de Trabalho";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Atualizações de grupo de trabalho";
