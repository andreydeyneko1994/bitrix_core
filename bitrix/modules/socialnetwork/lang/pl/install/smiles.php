<?
$MESS["FICON_BIGGRIN"] = "Szeroki uśmiech";
$MESS["FICON_COOL"] = "Spoko";
$MESS["FICON_CRY"] = "Płaczący";
$MESS["FICON_EEK"] = "Amazed";
$MESS["FICON_EVIL"] = "Zły";
$MESS["FICON_EXCLAIM"] = "Okrzyk";
$MESS["FICON_IDEA"] = "Pomysł";
$MESS["FICON_KISS"] = "Całus";
$MESS["FICON_NEUTRAL"] = "Sceptic";
$MESS["FICON_QUESTION"] = "Pytanie";
$MESS["FICON_REDFACE"] = "Zmieszany";
$MESS["FICON_SAD"] = "Smutny";
$MESS["FICON_SMILE"] = "Uśmiechnięty";
$MESS["FICON_WINK"] = "Mrugnięcie";
?>