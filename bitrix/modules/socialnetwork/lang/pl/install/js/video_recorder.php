<?
$MESS["BLOG_VIDEO_RECORD_AGREE"] = "Zezwól";
$MESS["BLOG_VIDEO_RECORD_ASK_PERMISSIONS"] = "Musisz zezwolić na dostęp do kamery i mikrofonu, aby nagrać film.";
$MESS["BLOG_VIDEO_RECORD_BUTTON"] = "Nagrywanie wideo";
$MESS["BLOG_VIDEO_RECORD_CANCEL_BUTTON"] = "Anuluj";
$MESS["BLOG_VIDEO_RECORD_CLOSE"] = "Zamknij";
$MESS["BLOG_VIDEO_RECORD_ERROR_CHROME_HTTPS"] = "Niestety twoja przeglądarka nie obsługuje protokołu HTTP.<br /><br />Spróbuj użyć innej przeglądarki, na przykład Firefoxa.";
$MESS["BLOG_VIDEO_RECORD_IN_PROGRESS_LABEL"] = "Trwa nagrywanie";
$MESS["BLOG_VIDEO_RECORD_LOGO"] = "<span class=\"logo\"> <span class=\"logo-text\">Bitrix</span> <span class=\"logo-color\">24</span></span>";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR"] = "Brak dostępu do kamery i mikrofonu.";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR_TITLE"] = "Błąd";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_TITLE"] = "Dostęp do urządzenia";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS"] = "Niestety, Twoja przeglądarka nie wspiera nagrywania wideo. <br/><br/> Spróbuj użyć innej przeglądarki, jak na przykład najnowszej wersji Firefoksa lub Chrome.";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS_TITLE"] = "Uwaga";
$MESS["BLOG_VIDEO_RECORD_RESTART_BUTTON"] = "Nagraj ponownie";
$MESS["BLOG_VIDEO_RECORD_SPOTLIGHT_MESSAGE"] = "<b>Nagrywaj filmy i udostępniaj je zespołowi.</b>";
$MESS["BLOG_VIDEO_RECORD_STOP_BUTTON"] = "Zatrzymaj";
$MESS["BLOG_VIDEO_RECORD_TRANFORM_LIMIT_TEXT"] = "Aby nagranie było widoczne w Bitrix24, zatrzymaj nagrywanie w ciągu <span class=\"bx-videomessage-transform-time-tip\">60 sekund</span>. Dłuższe pliki wideo z pewnością zostaną zapisane, ale mogą nie być odtwarzane we wszystkich przeglądarkach.";
$MESS["BLOG_VIDEO_RECORD_USE_BUTTON"] = "Użyj wideo";
?>