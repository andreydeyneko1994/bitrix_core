<?
$MESS["SONET_AGREE_FRIEND_NAME"] = "Confirmation of Friend Invitation";
$MESS["SONET_AGREE_FRIEND_SUBJECT"] = "#SITE_NAME#: Confirmation of friend invitation";
$MESS["SONET_BAN_FRIEND_NAME"] = "Blacklisting";
$MESS["SONET_BAN_FRIEND_SUBJECT"] = "#SITE_NAME#: Blacklisting";
$MESS["SONET_INVITE_FRIEND_NAME"] = "Invitation to Join Friends";
$MESS["SONET_INVITE_FRIEND_SUBJECT"] = "#SITE_NAME#: Friends Invitation";
$MESS["SONET_INVITE_GROUP_NAME"] = "Zaproszenie do dołączenia do grupy!";
$MESS["SONET_INVITE_GROUP_SUBJECT"] = "#SITE_NAME#: Invitation to Join Group";
$MESS["SONET_LOG_NEW_COMMENT_NAME"] = "Dodano nowy komentarz";
$MESS["SONET_LOG_NEW_ENTRY_NAME"] = "Dodano nową wiadomość";
$MESS["SONET_NEW_EVENT_GROUP_NAME"] = "Nowe wydarzenie grupy";
$MESS["SONET_NEW_EVENT_GROUP_SUBJECT"] = "#SITE_NAME#: Nowe wydarzenie grupy";
$MESS["SONET_NEW_EVENT_NAME"] = "Nowe wydarzenie";
$MESS["SONET_NEW_EVENT_SUBJECT"] = "#SITE_NAME#: #ENTITY# - Nowe wydarzenie w #ENTITY_TYPE#";
$MESS["SONET_NEW_EVENT_USER_NAME"] = "Nowe wydarzenie użytkownika";
$MESS["SONET_NEW_EVENT_USER_SUBJECT"] = "#SITE_NAME#: Nowe wydarzenie użytkownika";
$MESS["SONET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Masz nową wiadomość";
$MESS["SONET_REQUEST_GROUP_NAME"] = "Wniosek o członkostwo grupy";
$MESS["SONET_REQUEST_GROUP_SUBJECT"] = "#TITLE#";
?>