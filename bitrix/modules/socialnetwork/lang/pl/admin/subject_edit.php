<?
$MESS["SONETE_2FLIST"] = "Tematy";
$MESS["SONETE_ADDING"] = "Utwórz Nowy Temat";
$MESS["SONETE_DELETE_SUBJECT"] = "Usuń Temat";
$MESS["SONETE_DELETE_SUBJECT_CONFIRM"] = "Czy jesteś pewny, że chcesz usunąć ten temat? Temat nie zostanie usunięty, jeśli zawiera grupy.";
$MESS["SONETE_ERROR_SAVING"] = "Błąd w trakcie zapisu tematu.";
$MESS["SONETE_NAME"] = "Tytuł tematu";
$MESS["SONETE_NEW_SUBJECT"] = "Nowy temat";
$MESS["SONETE_NO_PERMS2ADD"] = "Brak wystarczających uprawnień do tworzenia tematów.";
$MESS["SONETE_SITE"] = "Strona tematu";
$MESS["SONETE_SORT"] = "Sortowanie tematu";
$MESS["SONETE_TAB_SUBJECT"] = "Temat Grupy";
$MESS["SONETE_TAB_SUBJECT_DESCR"] = "Parametry tematu grupy";
$MESS["SONETE_UPDATING"] = "Edytuj parametry tematu";
?>