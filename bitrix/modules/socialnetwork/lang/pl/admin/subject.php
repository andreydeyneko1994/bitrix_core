<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
$MESS["SONET_ADD_NEW"] = "Nowy temat";
$MESS["SONET_ADD_NEW_ALT"] = "Kliknij, aby dodać nowy temat";
$MESS["SONET_DELETE_ALT"] = "Usuń temat";
$MESS["SONET_DELETE_CONF"] = "Na pewno chcesz usunąć ten temat?";
$MESS["SONET_DELETE_ERROR"] = "Błąd w trakcie usuwania tematu";
$MESS["SONET_ERROR_UPDATE"] = "Błąd w trakcie aktualizacji parametrów tematów.";
$MESS["SONET_FILTER_SITE_ID"] = "Strona";
$MESS["SONET_SPT_ALL"] = "[wszyscy]";
$MESS["SONET_SUBJECT_NAME"] = "Nazwa";
$MESS["SONET_SUBJECT_NAV"] = "Tematy";
$MESS["SONET_SUBJECT_SITE_ID"] = "Strona";
$MESS["SONET_SUBJECT_SORT"] = "Sort.";
$MESS["SONET_TITLE"] = "Tematy grup";
$MESS["SONET_UPDATE_ALT"] = "Edytuj parametry tematu";
?>