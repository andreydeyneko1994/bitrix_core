<?
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Zaznaczone:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Usuń";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Wybrany:";
$MESS["SONET_ADMIN_LIST_CHANGE_OWNER"] = "Zmień właściciela";
$MESS["SONET_DELETE_ALT"] = "Usuń grupę";
$MESS["SONET_DELETE_CONF"] = "Na pewno chcesz usunąć tę grupę?";
$MESS["SONET_DELETE_ERROR"] = "Błąd usuwania grupy.";
$MESS["SONET_ERROR_UPDATE"] = "Błąd w trakcie aktualizacji parametrów grupy.";
$MESS["SONET_FILTER_SITE_ID"] = "Strona";
$MESS["SONET_FILTER_SUBJECT_ID"] = "Temat";
$MESS["SONET_GROUP_NAME"] = "Nazwa";
$MESS["SONET_GROUP_NAV"] = "Grupy";
$MESS["SONET_GROUP_OWNER_ID"] = "Właściciel";
$MESS["SONET_GROUP_SUBJECT_ID"] = "Temat";
$MESS["SONET_OWNER_ID"] = "Właściciel ID";
$MESS["SONET_OWNER_USER"] = "Właściciel";
$MESS["SONET_SPT_ALL"] = "[wszyscy]";
$MESS["SONET_SUBJECT_SORT"] = "Sortuj";
$MESS["SONET_TITLE"] = "Grupy";
$MESS["SONET_UPDATE_ALT"] = "Edytuj parametry grupy";
$MESS["USER_NAME_TEMPLATE"] = "[#ID#] #NAME# #LAST_NAME#";
?>