<?
$MESS["BLG_AM_SONETS"] = "Sieć Społecznościowa";
$MESS["BLG_AM_SONETS_ALT"] = "Zarządzaj siecią społecznościową";
$MESS["SONET_MENU_GROUP"] = "Grupy";
$MESS["SONET_MENU_GROUP_ALT"] = "Zarządzaj grupami";
$MESS["SONET_MENU_SMILES"] = "Emotikony";
$MESS["SONET_MENU_SMILES_ALT"] = "Zarządzaj emotikonami i ikonami";
$MESS["SONET_MENU_SUBJECT"] = "Tematy";
$MESS["SONET_MENU_SUBJECT_ALT"] = "Zarządzaj tematami grupy";
?>