<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Właściciel grupy i moderatorzy";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Właściciel projektu i moderatorzy";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Tylko właściciel grupy";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Tylko właściciel projektu";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Wszyscy członkowie grupy";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Wszyscy członkowie projektu";
