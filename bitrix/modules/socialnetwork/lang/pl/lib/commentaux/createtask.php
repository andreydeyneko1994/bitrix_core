<?
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_BLOG_COMMENT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza opublikowanego w strumieniu aktywności#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_CALENDAR"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza odnośnie wydarzenia w kalendarzu#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_FORUM_TOPIC"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza postu na forum#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_LISTS_NEW_ELEMENT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza odnośnie przepływu pracy#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_PHOTO_PHOTO"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza zdjęcia z albumu#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TASK"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza zadania#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TIMEMAN_ENTRY"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza odnośnie aktualizacji godzin pracy#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TIMEMAN_REPORT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza raportu pracy#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_WIKI"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN#komentarza strony Wiki#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie #A_BEGIN# komentarza postu opublikowanego w strumieniu aktywności #A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT_BITRIX24_NEW_USER"] = "Zadanie „#TASK_NAME#” zostało utworzone na podstawie #A_BEGIN#komentarza do wpisu dodania nowego użytkownika#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT_INTRANET_NEW_USER"] = "Zadanie „#TASK_NAME#” zostało utworzone na podstawie #A_BEGIN#komentarza do wpisu dodania nowego użytkownika#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;nie znaleziono zadania.&gt;";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_BITRIX24_NEW_USER"] = "Zadanie „#TASK_NAME#” zostało utworzone, ponieważ dodano nowego użytkownika.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_BLOG_POST"] = "Zadanie \"#TASK_NAME#\" zostało utworzone w poście strumienia aktywności.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_CALENDAR_EVENT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie wydarzenia w kalendarzu.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_FORUM_TOPIC"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie postu na forum.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_INTRANET_NEW_USER"] = "Zadanie „#TASK_NAME#” zostało utworzone, ponieważ dodano nowego użytkownika.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_LISTS_NEW_ELEMENT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie przepływu pracy.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_LOG_ENTRY"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na podstawie postu strumienia aktywności uzyskanego z zewnętrznego źródła.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_PHOTO_ALBUM"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie albumu Foto Galerii.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_PHOTO_PHOTO"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie zdjęcia z albumu.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TASK"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie innego zadania.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TIMEMAN_ENTRY"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie aktualizacji godzin pracy.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TIMEMAN_REPORT"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie raportu pracy.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_WIKI"] = "Zadanie \"#TASK_NAME#\" zostało utworzone na bazie strony z Wiki.";
?>