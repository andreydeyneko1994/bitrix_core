<?
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE"] = "Grupa robocza: #GROUP_NAME#";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE_PROJECT"] = "Projekt: #GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED"] = "Grupa robocza \"#GROUP_NAME#\" nie jest już połączona z tym czatem.";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED_PROJECT"] = "Projekt \"#GROUP_NAME#\" nie jest już połączony z tym czatem.";
?>