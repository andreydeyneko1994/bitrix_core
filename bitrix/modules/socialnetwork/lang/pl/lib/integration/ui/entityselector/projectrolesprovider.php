<?php
$MESS["SES_PROJECT_EMPLOYER_ROLE"] = "Członkowie";
$MESS["SES_PROJECT_MODERATOR_ROLE"] = "Moderatorzy";
$MESS["SES_PROJECT_OWNER_ROLE"] = "Właściciel projektu";
$MESS["SES_PROJECT_ROLES_TAB_TITLE"] = "Role";
$MESS["SES_PROJECT_SCRUM_MASTER_ROLE"] = "Scrum master";
$MESS["SES_PROJECT_SCRUM_MODERATOR_ROLE"] = "Zespół programistów";
$MESS["SES_PROJECT_SCRUM_OWNER_ROLE"] = "Właściciel produktu";
