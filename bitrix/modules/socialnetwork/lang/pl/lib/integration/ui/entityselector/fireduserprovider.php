<?php
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_FOOTER_INFO"] = "W tym widoku pokazywani są tylko zwolnieni pracownicy, mający linki do elementów CRM według wybranego pola. Jeśli wyszukiwany pracownik nie jest wyświetlany w widoku, znaczy to, że nie ma on zależności CRM.";
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_TAB_TITLE"] = "Zwolnione osoby";
