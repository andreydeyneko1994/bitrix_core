<?php
$MESS["BLG_NAME"] = "Rozpoczęte przez";
$MESS["BLG_SHARE"] = "Udostępnione dla:";
$MESS["BLG_SHARE_1"] = "Udostępnione dla:";
$MESS["BLG_SHARE_ALL"] = "Wszyscy pracownicy";
$MESS["BLG_SHARE_ALL_BUS"] = "Wszyscy użytkownicy";
$MESS["BLG_SHARE_HIDDEN_1"] = "Ukryty odbiorca";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Prosimy określić przynajmniej jedną osobę.";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "Rozmowa nie może być skierowana do wszystkich, należy określić co najmniej jednego odbiorcę.";
$MESS["SBPE_EXISTING_POST_PREMODERATION"] = "Nie można opublikować, ponieważ jako odbiorca jest wybrana pre-moderowana grupa użytkowników.";
$MESS["SBPE_MULTIPLE_PREMODERATION"] = "Nie można opublikować posta, ponieważ niektóre z wybranych grup są premoderowane.";
$MESS["SBPE_MULTIPLE_PREMODERATION2"] = "Nie można publikować pozycji z powodu wstępnej moderacji włączonej w następujących grupach roboczych: #GROUPS_LIST#";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Nieautoryzowany gość";
$MESS["SONET_HELPER_NO_PERMISSIONS"] = "Odmowa dostępu do jednego lub większej liczby odbiorców wiadomości";
$MESS["SONET_HELPER_PAGE_TITLE_WORKGROUP_TEMPLATE"] = "#WORKGROUP#: #TITLE#";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Re-indeksowanie Aktualności";
$MESS["SONET_HELPER_STEPPER_LIVEFEED2"] = "Ponownie zindeksuj posty na kanale";
$MESS["SONET_HELPER_VIDEO_CONVERSION_COMPLETED"] = "Plik wideo dołączony do Twojej wiadomości „#POST_TITLE#” został skonwertowany. Jest teraz dostępny dla odbiorców.";
