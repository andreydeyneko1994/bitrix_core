<?php
$MESS["SONET_LOG_COMMENT_EMPTY"] = "Tekst wiadomości jest pusty.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Nie masz zezwolenia na dodanie komentarza.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS_UPDATE"] = "Brak uprawnień do edycji komentarza";
