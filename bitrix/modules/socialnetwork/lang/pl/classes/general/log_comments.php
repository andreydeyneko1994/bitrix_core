<?php
$MESS["SONET_GLC_EMPTY_DATE_CREATE"] = "Data utworzenia jest nieprawidłowa.";
$MESS["SONET_GLC_EMPTY_ENTITY_ID"] = "ID jednostki nie jest określone.";
$MESS["SONET_GLC_EMPTY_ENTITY_TYPE"] = "Typ jednostki nie jest określony.";
$MESS["SONET_GLC_EMPTY_EVENT_ID"] = "Wydarzenie nie jest określone.";
$MESS["SONET_GLC_EMPTY_LOG_ID"] = "ID wydarzenia Aktualności jest nieprawidłowe.";
$MESS["SONET_GLC_ERROR_CALC_ENTITY_TYPE"] = "Błąd w trakcie definiowania rodzaju jednostki.";
$MESS["SONET_GLC_ERROR_CHECKFIELDS_FAILED"] = "Nieprawidłowa data";
$MESS["SONET_GLC_ERROR_NO_ENTITY_ID"] = "ID jednostki jest nieprawidłowe.";
$MESS["SONET_GLC_ERROR_NO_ENTITY_TYPE"] = "Typ jednostki jest nieprawidłowy.";
$MESS["SONET_GLC_ERROR_NO_FEATURE_ID"] = "Określone wydarzenie jest nieprawidłowe.";
$MESS["SONET_GLC_ERROR_NO_USER_ID"] = "ID użytkownika jest niepoprawne.";
$MESS["SONET_GLC_FORUM_MENTION"] = "wspomniano cię w komentarzu do posta \"#title#\"";
$MESS["SONET_GLC_FORUM_MENTION_F"] = "wspomniała cię w komentarzu do posta \"#title#\"";
$MESS["SONET_GLC_FORUM_MENTION_M"] = "wspomniał cię w komentarzu do posta \"#title#\"";
$MESS["SONET_GLC_SEND_EVENT_LINK"] = "Idź do:";
$MESS["SONET_GLC_WRONG_PARAMETER_ID"] = "ID jest nieprawidłowe.";
