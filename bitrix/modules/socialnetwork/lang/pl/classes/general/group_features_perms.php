<?
$MESS["SONET_GFP_BAD_OPERATION_ID"] = "Nie można potwierdzić operacji.";
$MESS["SONET_GFP_EMPTY_GROUP_FEATURE_ID"] = "Właściwość jest nieokreślona.";
$MESS["SONET_GFP_EMPTY_OPERATION_ID"] = "Operacja jest nieokreślona.";
$MESS["SONET_GFP_EMPTY_ROLE"] = "Funkcja jest nieokreślona";
$MESS["SONET_GFP_ERROR_NO_GROUP_FEATURE_ID"] = "Właściwość jest nieprawidłowa.";
$MESS["SONET_GFP_ERROR_NO_ROLE"] = "Funkcja jest nieprawidłowa.";
$MESS["SONET_GFP_NO_OPERATION_ID"] = "Operacja jest nieprawidłowa.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "ID jednostki nie jest określone.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "Typ jednostki nie jest określony.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "Właściwość jest nieokreślona.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "Jednostka jest nieprawidłowa.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "Typ jednostki jest nieprawidłowy.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "Właściwość jest nieprawidłowa.";
$MESS["SONET_GF_ERROR_SET"] = "Błąd w trakcie tworzenia zapisu.";
?>