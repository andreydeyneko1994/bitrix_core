<?
$MESS["SONET_GB_EMPTY_DATE_ACTIVITY"] = "Data ostatniej wizyty jest nieprawidłowa.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "Data utworzenia jest nieprawidłowa.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "Data aktualizacji jest nieprawidłowa.";
$MESS["SONET_GB_EMPTY_NAME"] = "Tytuł nie jest określony.";
$MESS["SONET_GB_EMPTY_OWNER_ID"] = "Właściciel jest nieokreślony.";
$MESS["SONET_GB_EMPTY_SUBJECT_ID"] = "Temat nie jest określony.";
$MESS["SONET_GB_ERROR_NO_OWNER_ID"] = "ID właściciela jest nieprawidłowe.";
$MESS["SONET_GB_ERROR_NO_SUBJECT_ID"] = "Temat jest nieprawidłowy.";
$MESS["SONET_GG_EMPTY_SITE_ID"] = "Strona jest nieokreślona.";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_1"] = "Użytkownik posiada następujące grupy sieci społecznościowej:<br>";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_2"] = "Proszę otworzyć Serwisy > Sieć społeczna > Grupy i zmienić właściciela grupy lub usunąć grupę.";
$MESS["SONET_GG_ERROR_NO_SITE"] = "Strona jest nieprawidłowa.";
$MESS["SONET_GP_ERROR_IMAGE_ID"] = "Obraz jest nieprawidłowy.";
$MESS["SONET_NO_GROUP"] = "Nie znaleziono zapisu.";
$MESS["SONET_UG_EMPTY_INITIATE_PERMS"] = "Uprawnienie do zapraszania jest nieokreślone.";
$MESS["SONET_UG_EMPTY_SPAM_PERMS"] = "Uprawnienia do wysyłania wiadomości do grupy nie są ustawione.";
$MESS["SONET_UG_ERROR_NO_INITIATE_PERMS"] = "Uprawnienie do zapraszania jest nieprawidłowe.";
$MESS["SONET_UG_ERROR_NO_SPAM_PERMS"] = "Uprawnienia do wysyłania wiadomości do grup są nieprawidłowe";
$MESS["SONET_UR_EMPTY_FIELDS"] = "Parametry grupy są nieokreślone.";
$MESS["SONET_UR_EMPTY_OWNERID"] = "ID właściciela grupy jest nieokreślone.";
$MESS["SONET_UR_ERROR_CREATE_GROUP"] = "Błąd tworzenia grupy";
$MESS["SONET_UR_ERROR_CREATE_U_GROUP"] = "Błąd w trakcie dodawania użytkownika do grupy.";
$MESS["SONET_WRONG_PARAMETER_ID"] = "Nieprawidłowy ID został przekazany do funkcji.";
?>