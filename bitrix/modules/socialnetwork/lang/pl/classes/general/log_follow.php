<?
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_N"] = "Nie, dziękuję";
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_Y"] = "Włącz tryb inteligentnego śledzenia";
$MESS["SONET_LF_UNFOLLOW_IM_MESSAGE"] = "Otrzymujesz mnóstwo wiadomości na Tablicę? Włącz tryb inteligentnego śledzenia, aby wyświetlać tylko te wydarzenia, które są dla ciebie ważne. Tylko wiadomości, których jesteś autorem, adresatem lub w jesteś w nich wymieniony/a zostaną przesunięte do góry. Gdy tylko dodasz komentarz do wiadomości, zaczynasz ją śledzić.";
?>