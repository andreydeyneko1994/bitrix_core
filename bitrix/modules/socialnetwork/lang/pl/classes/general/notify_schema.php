<?php
$MESS["SONET_NS_FRIEND"] = "Wskazał lub usunął wskazanie listowania jako znajomy.";
$MESS["SONET_NS_INOUT_GROUP"] = "Zmiany wśród członków grupy";
$MESS["SONET_NS_INVITE_GROUP_BTN"] = "Otrzymano zaproszenie lub prośbę o dołączenie do grupy roboczej; usunięto z grupy roboczej";
$MESS["SONET_NS_INVITE_GROUP_INFO"] = "Zmiana członków grupy roboczej";
$MESS["SONET_NS_INVITE_USER"] = "Zaproszenie przyjaciela";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Dołączenie lub odłączenie moderatora";
$MESS["SONET_NS_OWNER_GROUP"] = "Zmiana właściciela grupy";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Aktualizacje w Grupie Roboczej";
