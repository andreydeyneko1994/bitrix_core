<?
$MESS["SONET_EUV_EMPTY_ENTITY_ID"] = "ID jednostki nie zostało określone";
$MESS["SONET_EUV_EMPTY_ENTITY_TYPE"] = "Rodzaj jednostki jest nieokreślony";
$MESS["SONET_EUV_EMPTY_ROLE"] = "Funkcja jest nieokreślona";
$MESS["SONET_EUV_EMPTY_USER_ID"] = "Użytkownik nie jest określony";
$MESS["SONET_EUV_ERROR_DELETE"] = "Nie można usunąć zapisu uprawnień dostępu do widoku.";
$MESS["SONET_EUV_ERROR_SET"] = "Nie można zaktualizować zapisu uprawnień dostępu do widoku.";
$MESS["SONET_EUV_INCORRECT_ENTITY_TYPE"] = "Rodzaj jednostki jest nieprawidłowy";
$MESS["SONET_EUV_NO_ENTITY"] = "Nie znaleziono zapisu.";
$MESS["SONET_EUV_RECORD_EXISTS"] = "Rekord już istnieje.";
?>