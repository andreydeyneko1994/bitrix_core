<?
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "Data utworzenia jest nieprawidłowa.";
$MESS["SONET_MM_EMPTY_DATE_VIEW"] = "Data wyświetlenia jest nieprawidłowa";
$MESS["SONET_M_EMPTY_FROM_USER_ID"] = "Nadawca jest nieokreślony.";
$MESS["SONET_M_EMPTY_TO_USER_ID"] = "Odbiorca nie jest określony.";
$MESS["SONET_M_ERROR_DELETE_MESSAGE"] = "Nie można usunąć wiadomości.";
$MESS["SONET_M_ERROR_NO_FROM_USER_ID"] = "Nadawca jest nieprawidłowy.";
$MESS["SONET_M_ERROR_NO_TO_USER_ID"] = "Określony odbiorca jest nieprawidłowy.";
$MESS["SONET_UR_EMPTY_MESSAGE"] = "Wiadomość jest nieokreślona";
$MESS["SONET_UR_EMPTY_MESSAGE_ID"] = "ID wiadomości jest nieokreślone";
$MESS["SONET_UR_EMPTY_SENDER_USER_ID"] = "Nadawca wiadomości nie jest określony.";
$MESS["SONET_UR_EMPTY_TARGET_USER_ID"] = "Adresat wiadomości nie jest określony.";
$MESS["SONET_UR_ERROR_CREATE_MESSAGE"] = "Błąd w trakcie tworzenia wiadomości.";
$MESS["SONET_UR_ERROR_UPDATE_MESSAGE"] = "Nie można zaktualizować wiadomości.";
$MESS["SONET_UR_NO_MESSAGE"] = "Nie znaleziono wiadomości.";
?>