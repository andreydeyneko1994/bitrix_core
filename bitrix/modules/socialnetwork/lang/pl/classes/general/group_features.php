<?
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "Zapis daty utworzenia jest nieprawidłowy.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "Data aktualizacji jest nieprawidłowa.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "ID jednostki nie jest określone.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "Typ jednostki nie jest określony.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "Właściwość jest nieokreślona.";
$MESS["SONET_GF_ERROR_CALC_ENTITY_TYPE"] = "Nie można określić rodzaju jednostki.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "Jednostka jest nieprawidłowa.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "Typ jednostki jest nieprawidłowy.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "Właściwość jest nieprawidłowa.";
$MESS["SONET_GF_ERROR_SET"] = "Nie można zapisać zapisu.";
?>