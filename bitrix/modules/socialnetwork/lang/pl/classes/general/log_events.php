<?
$MESS["SONET_LE_EMPTY_ENTITY_ID"] = "ID jednostki nie jest określone.";
$MESS["SONET_LE_EMPTY_ENTITY_TYPE"] = "Typ jednostki nie jest określony.";
$MESS["SONET_LE_EMPTY_EVENT_ID"] = "Wydarzenie nie jest określone.";
$MESS["SONET_LE_EMPTY_SITE_ID"] = "Strona jest nieokreślona.";
$MESS["SONET_LE_ERROR_CALC_ENTITY_TYPE"] = "Błąd w trakcie definiowania rodzaju jednostki.";
$MESS["SONET_LE_ERROR_NO_ENTITY_ID"] = "ID jednostki jest nieprawidłowe.";
$MESS["SONET_LE_ERROR_NO_ENTITY_TYPE"] = "Typ jednostki jest nieprawidłowy.";
$MESS["SONET_LE_ERROR_NO_FEATURE_ID"] = "Określone wydarzenie jest nieprawidłowe.";
$MESS["SONET_LE_ERROR_NO_SITE"] = "Określona strona jest nieprawidłowa.";
$MESS["SONET_LE_WRONG_PARAMETER_ID"] = "ID jest nieprawidłowe.";
?>