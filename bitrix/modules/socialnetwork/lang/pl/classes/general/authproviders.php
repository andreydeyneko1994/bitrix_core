<?
$MESS["authprov_sg_a"] = "Właściciel grupy";
$MESS["authprov_sg_current"] = "Obecna grupa";
$MESS["authprov_sg_e"] = "Moderatorzy grupy";
$MESS["authprov_sg_k"] = "Wszyscy członkowie grupy";
$MESS["authprov_sg_name"] = "Grupy Sieci Społecznościowej";
$MESS["authprov_sg_name_out"] = "Grupa sieci społecznościowej";
$MESS["authprov_sg_panel_last"] = "Ostatnie";
$MESS["authprov_sg_panel_my_group"] = "Moje Grupy";
$MESS["authprov_sg_panel_search"] = "Szukaj";
$MESS["authprov_sg_panel_search_text"] = "Wpisz nazwę grupy.";
$MESS["authprov_sg_socnet_group"] = "Grupy Sieci Społecznościowej";
?>