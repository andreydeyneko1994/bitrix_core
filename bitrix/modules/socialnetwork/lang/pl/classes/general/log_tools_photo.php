<?
$MESS["SONET_IM_NEW_PHOTO"] = "Dodano nowe zdjęcie do albumu \"#title#\" w \"#group_name#\".";
$MESS["SONET_PHOTOALBUM_IM_COMMENT"] = "pozostawił/a komentarz do twojego albumu \"#album_title#\"";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# dodał/a nowe zdjęcie \"#TITLE#\".";
$MESS["SONET_PHOTO_ADD_COMMENT_SOURCE_ERROR"] = "Nie powiodło się dodanie komentarza do źródła wydarzenia.";
$MESS["SONET_PHOTO_DELETE_COMMENT_SOURCE_ERROR"] = "Nie można usunąć komentarza do źródła wydarzenia";
$MESS["SONET_PHOTO_IM_COMMENT"] = "pozostawił/a komentarz do twojego zdjęcia \"#photo_title#\" w albumie \"#album_title#\"";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# dodał zdjęcie #TITLE#";
$MESS["SONET_PHOTO_LOG_2"] = "Zdjęcia (#COUNT#)";
$MESS["SONET_PHOTO_LOG_GUEST"] = "Gość";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Nowe zdjęcia: #LINKS# i inne.";
$MESS["SONET_PHOTO_UPDATE_COMMENT_SOURCE_ERROR"] = "Nie można zaktualizować komentarza do źródła wydarzenia";
?>