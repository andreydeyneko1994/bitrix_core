<?
$MESS["BLOG_VIDEO_RECORD_AGREE"] = "Autoriser";
$MESS["BLOG_VIDEO_RECORD_ASK_PERMISSIONS"] = "Vous devez autoriser l'accès à votre caméra et à votre micro pour enregistrer une vidéo.";
$MESS["BLOG_VIDEO_RECORD_BUTTON"] = "Enregistrer une vidéo";
$MESS["BLOG_VIDEO_RECORD_CANCEL_BUTTON"] = "Annuler";
$MESS["BLOG_VIDEO_RECORD_CLOSE"] = "Fermer";
$MESS["BLOG_VIDEO_RECORD_ERROR_CHROME_HTTPS"] = "Malheureusement, votre navigateur ne prend pas en charge le protocole HTTP.<br /><br />Veuillez en essayer un autre, comme, par exemple, Firefox.";
$MESS["BLOG_VIDEO_RECORD_IN_PROGRESS_LABEL"] = "Enregistrement en cours";
$MESS["BLOG_VIDEO_RECORD_LOGO"] = "<span class=\"logo\"><span class=\"logo-text\">Bitrix</span><span class=\"logo-color\">24</span></span>";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR"] = "Impossible d'accéder à votre caméra et micro.";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR_TITLE"] = "Erreur";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_TITLE"] = "Accès à l'appareil";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS"] = "Malheureusement, votre navigateur ne supporte pas l'enregistrement vidéo. <br/><br/> Vous pouvez essayer un autre navigateur, par exemple une version plus récente de FireFox ou Chrome.";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS_TITLE"] = "Attention";
$MESS["BLOG_VIDEO_RECORD_RESTART_BUTTON"] = "Enregistrer à nouveau";
$MESS["BLOG_VIDEO_RECORD_SPOTLIGHT_MESSAGE"] = "<b>Enregistrez des vidéos et partagez-les avec votre équipe.</b>";
$MESS["BLOG_VIDEO_RECORD_STOP_BUTTON"] = "Arrêter";
$MESS["BLOG_VIDEO_RECORD_TRANFORM_LIMIT_TEXT"] = "Pour permettre de visualiser la vidéo dans Bitrix24, arrêtez l'enregistrement dans les <span class=\"bx-videomessage-transform-time-tip\">60 secondes</span>. Les fichiers vidéos plus longs seront sûrement enregistrés, mais ne seront pas lus sur tous les navigateurs.";
$MESS["BLOG_VIDEO_RECORD_USE_BUTTON"] = "Utiliser la vidéo";
?>