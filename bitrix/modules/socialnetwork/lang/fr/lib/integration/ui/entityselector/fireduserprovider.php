<?php
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_FOOTER_INFO"] = "Cet affichage montre uniquement les employés licenciés qui ont des liens avec des éléments CRM via le champ que vous avez sélectionné. Si l'affichage ne montre pas l'employé que vous recherchez, cela signifie qu'il n'a pas de liens avec le CRM.";
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_TAB_TITLE"] = "Personnes renvoyées";
