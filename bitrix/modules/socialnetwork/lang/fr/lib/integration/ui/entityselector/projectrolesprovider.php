<?php
$MESS["SES_PROJECT_EMPLOYER_ROLE"] = "Membres";
$MESS["SES_PROJECT_MODERATOR_ROLE"] = "Modérateurs";
$MESS["SES_PROJECT_OWNER_ROLE"] = "Propriétaire du projet";
$MESS["SES_PROJECT_ROLES_TAB_TITLE"] = "Rôles";
$MESS["SES_PROJECT_SCRUM_MASTER_ROLE"] = "Maître Scrum";
$MESS["SES_PROJECT_SCRUM_MODERATOR_ROLE"] = "Équipe de développement";
$MESS["SES_PROJECT_SCRUM_OWNER_ROLE"] = "Propriétaire du produit";
