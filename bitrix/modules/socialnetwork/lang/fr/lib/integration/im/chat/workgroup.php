<?
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE"] = "Groupe de travail : \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE_PROJECT"] = "Projet : \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED"] = "Le groupe de travail \"#GROUP_NAME#\" n'est plus relié à ce chat.";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED_PROJECT"] = "Le projet \"#GROUP_NAME#\" n'est plus associé à ce chat.";
?>