<?php
$MESS["BLG_NAME"] = "Démarré par";
$MESS["BLG_SHARE"] = "Partagé avec : ";
$MESS["BLG_SHARE_1"] = "Partagé avec : ";
$MESS["BLG_SHARE_ALL"] = "Tous les employés";
$MESS["BLG_SHARE_ALL_BUS"] = "Tous les utilisateurs";
$MESS["BLG_SHARE_HIDDEN_1"] = "Destinataire caché";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Indiquez au moins un destinataire.";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "Vous ne pouvez pas éEcrire le message à tous, indiquez au moins un destinataire";
$MESS["SBPE_EXISTING_POST_PREMODERATION"] = "Impossible de publier car un groupe utilisateur pré-modéré est sélectionné comme destinataire.";
$MESS["SBPE_MULTIPLE_PREMODERATION"] = "Impossible de publier parce que certains des groupes sélectionnés sont pré-modérés.";
$MESS["SBPE_MULTIPLE_PREMODERATION2"] = "Impossible de publier les éléments parce que la pré-modération est activée dans ces groupes de travail : #GROUPS_LIST#";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Utilisateur non-autorisé";
$MESS["SONET_HELPER_NO_PERMISSIONS"] = "L'accès à un ou plusieurs destinataires du message a été refusé";
$MESS["SONET_HELPER_PAGE_TITLE_WORKGROUP_TEMPLATE"] = "#WORKGROUP# : #TITLE#";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Réindexation du Flux d'activités";
$MESS["SONET_HELPER_STEPPER_LIVEFEED2"] = "Réindexer les publications des Actualités";
$MESS["SONET_HELPER_VIDEO_CONVERSION_COMPLETED"] = "Le fichier vidéo joint à votre message « #POST_TITLE# » a été converti. Il est maintenant disponible pour les destinataires.";
