<?
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_BLOG_COMMENT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de publication du flux d'activités#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_CALENDAR"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire d'évènement du calendrier#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_FORUM_TOPIC"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de publication du forum#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_LISTS_NEW_ELEMENT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée de flux de travail#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_PHOTO_PHOTO"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de photo d'album#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TASK"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de tâche#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TIMEMAN_ENTRY"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire laissé sur une mise à jour d'heures de travail#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_TIMEMAN_REPORT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de rapport de travail#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_FORUM_POST_WIKI"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de page wiki#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire de publication du flux d'activités#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT_BITRIX24_NEW_USER"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée d'ajout de nouvel utilisateur#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_COMMENT_LOG_COMMENT_INTRANET_NEW_USER"] = "La tâche \"#TASK_NAME#\" a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée d'ajout de nouvel utilisateur#A_END#.";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;tâche introuvable.&gt;";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_BITRIX24_NEW_USER"] = "La tâche \"#TASK_NAME#\" a été créée parce qu'un nouvel utilisateur a été ajouté.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_BLOG_POST"] = "La tâche \"#TASK_NAME#\" a été créée sur une publication du Flux d'activités.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_CALENDAR_EVENT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un évènement du calendrier.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_FORUM_TOPIC"] = "La tâche \"#TASK_NAME#\" a été créée d'après une publication du forum.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_INTRANET_NEW_USER"] = "La tâche \"#TASK_NAME#\" a été créée parce qu'un nouvel utilisateur a été ajouté.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_LISTS_NEW_ELEMENT"] = "La tâche \"#TASK_NAME#\" a été créée d'après entrée d'un flux de travail.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_LOG_ENTRY"] = "La tâche \"#TASK_NAME#\" a été créée d'après une publication du flux d'activités obtenue d'une source externe.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_PHOTO_ALBUM"] = "La tâche \"#TASK_NAME#\" a été créée d'après un album de la galerie de photos.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_PHOTO_PHOTO"] = "La tâche \"#TASK_NAME#\" a été créée d'après une photo d'album.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TASK"] = "La tâche \"#TASK_NAME#\" a été créée d'après une autre tâche.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TIMEMAN_ENTRY"] = "La tâche \"#TASK_NAME#\" a été créée d'après une mise à jour des heures de travail.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_TIMEMAN_REPORT"] = "La tâche \"#TASK_NAME#\" a été créée d'après un rapport de travail.";
$MESS["SONET_COMMENTAUX_CREATETASK_POST_WIKI"] = "La tâche \"#TASK_NAME#\" a été créée d'après une page wiki.";
?>