<?php
$MESS["SONET_LOG_COMMENT_EMPTY"] = "Le texte du message n'est pas indiqué.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Il n'y a pas de droits pour ajouter un commentaire.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS_UPDATE"] = "Pas de permission pour modifier commentaire";
