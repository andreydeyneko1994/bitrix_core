<?php
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_AUTO_MEMBER"] = "Connecté via le service";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_DEPARTMENT"] = "Service (avec sous-services)";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_EMAIL"] = "Adresse e-mail";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_EXTRANET"] = "Extranet";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_FIRED"] = "Renvoyé";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_INITIATED_BY_GROUP"] = "Invitations";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_INITIATED_BY_TYPE"] = "Type de demande";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_INITIATED_BY_USER"] = "Demandes entrantes";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_LAST_NAME"] = "Nom de famille";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_NAME"] = "Prénom";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_ROLE"] = "Rôle";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_ROLE_MODERATOR"] = "Modérateur / Assistant responsable";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_ROLE_OWNER"] = "Propriétaire / Responsable";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_ROLE_REQUEST"] = "Demander / Inviter";
$MESS["SOCIALNETWORK_USERTOGROUP_FILTER_ROLE_USER"] = "Membre";
