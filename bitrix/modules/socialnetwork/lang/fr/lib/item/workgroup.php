<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Propriétaire du groupe et modérateurs du groupe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Propriétaire et modérateurs du projet";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Seulement le propriétaire du groupe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Propriétaire du projet uniquement";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Tous les membres du groupe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Tous les participants au projet";
