<?php
$MESS["SONET_NS_FRIEND"] = "Inclusion/exclusion à/de votre liste d'amis";
$MESS["SONET_NS_INOUT_GROUP"] = "Changement d'appartenance au groupe (pour les modérateurs)";
$MESS["SONET_NS_INVITE_GROUP_BTN"] = "A reçu une invitation ou une demande à rejoindre le groupe de travail ; a été retiré(e) du groupe de travail";
$MESS["SONET_NS_INVITE_GROUP_INFO"] = "Modification des membres du groupe travail";
$MESS["SONET_NS_INVITE_USER"] = "Demande d'ami";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Inclure/supprimer de la liste des modérateurs du groupe";
$MESS["SONET_NS_OWNER_GROUP"] = "Changement du propriétaire de groupe";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Activité et mises à jour dans le groupe";
