<?php
$MESS["BLG_NAME"] = "Iniciado por";
$MESS["BLG_SHARE"] = "Compartido con:";
$MESS["BLG_SHARE_1"] = "Compartido con:";
$MESS["BLG_SHARE_ALL"] = "Todos los empleados";
$MESS["BLG_SHARE_ALL_BUS"] = "Todos los usuarios";
$MESS["BLG_SHARE_HIDDEN_1"] = "Ocultar destinatario";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "Por favor especifique al menos una persona.";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "La conversación no puede dirigirse a todos, por favor especifique al menos un destinatario.";
$MESS["SBPE_EXISTING_POST_PREMODERATION"] = "No se puede publicar porque un usuario pre-moderador del grupo está seleccionado como destinatario.";
$MESS["SBPE_MULTIPLE_PREMODERATION"] = "No puede publicar el post porque algunos de los grupos seleccionados están pre-moderados.";
$MESS["SBPE_MULTIPLE_PREMODERATION2"] = "No se pueden publicar elementos porque la moderación previa está habilitada en estos grupos de trabajo: #GROUPS_LIST#";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Visitante no autorizado";
$MESS["SONET_HELPER_NO_PERMISSIONS"] = "Se negó el acceso a uno o más destinatarios del mensaje";
$MESS["SONET_HELPER_PAGE_TITLE_WORKGROUP_TEMPLATE"] = "#WORKGROUP#: #TITLE#";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Re-indexación del noticiases";
$MESS["SONET_HELPER_STEPPER_LIVEFEED2"] = "Reindexar las publicaciones del Feed";
$MESS["SONET_HELPER_VIDEO_CONVERSION_COMPLETED"] = "El archivo de video adjunto a su mensaje \"#POST_TITLE#\" fue convertido. Ahora está disponible para los destinatarios.";
