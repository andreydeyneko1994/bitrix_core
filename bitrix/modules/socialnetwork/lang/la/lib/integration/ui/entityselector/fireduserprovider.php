<?php
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_FOOTER_INFO"] = "Esta vista muestra solamente los empleados despedidos que tienen enlaces a elementos del CRM para el campo que seleccionó. Si la vista no muestra al empleado que está buscando, significa que no tiene dependencias en el CRM.";
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_TAB_TITLE"] = "Usuarios desactivados";
