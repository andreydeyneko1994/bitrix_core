<?php
$MESS["SES_PROJECT_EMPLOYER_ROLE"] = "Miembros";
$MESS["SES_PROJECT_MODERATOR_ROLE"] = "Moderadores";
$MESS["SES_PROJECT_OWNER_ROLE"] = "Propietario del proyecto";
$MESS["SES_PROJECT_ROLES_TAB_TITLE"] = "Roles";
$MESS["SES_PROJECT_SCRUM_MASTER_ROLE"] = "Scrum master";
$MESS["SES_PROJECT_SCRUM_MODERATOR_ROLE"] = "Equipo de desarrollo";
$MESS["SES_PROJECT_SCRUM_OWNER_ROLE"] = "Propietario del producto";
