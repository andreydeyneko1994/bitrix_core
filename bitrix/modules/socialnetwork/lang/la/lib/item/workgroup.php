<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Propietario del grupo y moderadores";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Propietario y moderadores del proyecto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Solo propietario del grupo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Propietario del proyecto solamente";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Todos los miembros del grupo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Todos los participantes del proyecto";
