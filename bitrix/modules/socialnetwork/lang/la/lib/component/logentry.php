<?php
$MESS["SONET_LOG_COMMENT_EMPTY"] = "El texto del mensaje está vacío.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Usted no tiene permiso para añadir comentarios.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS_UPDATE"] = "No tiene permiso para editar comentarios";
