<?php
$MESS["SONET_NS_FRIEND"] = "Inclusión / exclusión de la lista de amigos";
$MESS["SONET_NS_INOUT_GROUP"] = "Cambio de membresía de grupo (para moderadores)";
$MESS["SONET_NS_INVITE_GROUP_BTN"] = "Recibió una invitación o solicitud para unirse a un grupo de trabajo; eliminado del grupo de trabajo";
$MESS["SONET_NS_INVITE_GROUP_INFO"] = "Cambio en los miembros del grupo de trabajo";
$MESS["SONET_NS_INVITE_USER"] = "Notificación de amistad";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Inclusión / exclusión de la lista de moderadores del grupo";
$MESS["SONET_NS_OWNER_GROUP"] = "Cambio de propietario de grupo de trabajo";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Actualizar grupo de trabajo";
