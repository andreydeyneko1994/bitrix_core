<?php
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_LESS"] = "#QUANTITY# szt. \"#PRODUCT_NAME#\" dodano już do koszyka. Możesz dodać jeszcze #ADD#.";
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_ZERO"] = "Maksymalna dostępna ilość (#QUANTITY#) \"#PRODUCT_NAME#\" została już dodana do koszyka.";
