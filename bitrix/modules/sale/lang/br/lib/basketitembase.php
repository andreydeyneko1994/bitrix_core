<?php
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_LESS"] = "#QUANTITY# unidades de \"#PRODUCT_NAME#\" já foram adicionadas ao carrinho. Você pode adicionar até #ADD# mais.";
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_ZERO"] = "A quantidade máxima disponível de (#QUANTITY#) \"#PRODUCT_NAME#\" já foi adicionada ao carrinho.";
