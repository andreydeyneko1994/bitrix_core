<?php
$MESS["CHECKOUT_VIEW_PROPERTY_LIST_EDIT_FILL_YOUR_DATA"] = "Fournissez vos informations";
$MESS["CHECKOUT_VIEW_PROPERTY_LIST_VIEW_ORDER_TITLE"] = "Commande ##ORDER_NUMBER#";
$MESS["CHECKOUT_VIEW_PROPERTY_LIST_VIEW_SHIPPING_CONTACTS"] = "Information de contact";
$MESS["CHECKOUT_VIEW_PROPERTY_LIST_VIEW_SHIPPING_METHOD"] = "Mode de livraison";
$MESS["CHECKOUT_VIEW_PROPERTY_LIST_VIEW_SHIPPING_METHOD_DESCRIPTION"] = "Nous vous rappellerons pour discuter du mode et de l'heure de livraison.";
