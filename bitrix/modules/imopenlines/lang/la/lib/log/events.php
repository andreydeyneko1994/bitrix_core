<?php
$MESS["IMOL_EVENTLOG_EMPTY_LINE_ID_ERROR"] = "ID del canal abierto vacío";
$MESS["IMOL_EVENTLOG_NOT_ACTUAL_EVENT_ERROR"] = "Evento no registrado #EVENT#";
$MESS["IMOL_EVENTLOG_WRONG_FIELD_RESULT_TYPE_EXCEPTION"] = "Pasó un tipo de valor no válido";
