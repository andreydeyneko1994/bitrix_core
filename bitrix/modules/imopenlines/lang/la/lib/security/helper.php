<?php
$MESS["IMOL_ROLE_ADMIN"] = "Administrador";
$MESS["IMOL_ROLE_CHIEF"] = "Director ejecutivo";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Jefe del Departamento";
$MESS["IMOL_ROLE_MANAGER"] = "Gerente";
