<?
$MESS["CALCULATE_COUNT_OF_ALL_CALLS"] = "Total";
$MESS["FILTER"] = "Filtrado por:";
$MESS["FILTER_ALL_DEFAULT_TITLE"] = "Todo";
$MESS["FILTER_BY_CHANNEL"] = "Canales de comunicación:";
$MESS["FILTER_BY_OPEN_LINE"] = "Canal abierto:";
$MESS["FILTER_BY_RESPONSIBLE"] = "Responsable:";
$MESS["GROUPING"] = "Agrupar por:";
$MESS["GROUPING_CHANEL"] = "Canal de comunicación";
$MESS["GROUPING_DATE"] = "Fecha";
$MESS["GROUPING_LINE"] = "Canal Abierto";
$MESS["GROUPING_RESPONSIBLE"] = "Responsable";
$MESS["REPORT_OPERATOR_DEMO_NAME_PREFIX_NEW"] = "Agente";
$MESS["WHAT_WILL_CALCULATE"] = "Cálcular:";
?>