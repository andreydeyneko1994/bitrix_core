<?
$MESS["OPEN_LINES_TREATMENT"] = "Sesiones";
$MESS["WHAT_WILL_CALCULATE_ALL_APPOINTED"] = "Sesiones asignadas";
$MESS["WHAT_WILL_CALCULATE_ALL_MARK"] = "Sesiones calificadas";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT"] = "Total de sesiones";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT_BY_HOUR"] = "Sesiones por hora";
$MESS["WHAT_WILL_CALCULATE_ANSWERED"] = "Sesiones sin asignar";
$MESS["WHAT_WILL_CALCULATE_CONTENTMENT"] = "Tasa de satisfacción";
$MESS["WHAT_WILL_CALCULATE_DUPLICATE_TREATMENT_NEW"] = "Sesiones, consultas recurrentes";
$MESS["WHAT_WILL_CALCULATE_FIRST_TREATMENT_NEW"] = "Sesiones, nuevas consultas";
$MESS["WHAT_WILL_CALCULATE_NEGATIVE_MARK"] = "Sesiones con calificación negativa";
$MESS["WHAT_WILL_CALCULATE_POSITIVE_MARK"] = "Sesiones con calificación positiva";
$MESS["WHAT_WILL_CALCULATE_SKIPPED"] = "Sesiones omitidas";
$MESS["WHAT_WILL_CALCULATE_WITHOUT_MARK"] = "Sesiones sin calificar";
?>