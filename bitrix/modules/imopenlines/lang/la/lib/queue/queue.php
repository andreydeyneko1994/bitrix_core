<?php
$MESS["IMOL_QUEUE_NOTIFICATION_EMPTY_QUEUE"] = "Le faltan mensajes publicados por sus clientes mediante los canales del Centro de contacto. Actualmente no hay empleados asignados a la cola de mensajes correspondiente. [URL = #URL#]Configurar la cola de mensajes[/URL]";
$MESS["IMOL_QUEUE_SESSION_SKIP_ALONE"] = "No puede rechazar la conversación porque es la única persona que está disponible en la cola.";
$MESS["IMOL_QUEUE_SESSION_TRANSFER_OPERATOR_OFFLINE"] = "La sesión se reenvió a la cola porque el agente estaba desconectado";
