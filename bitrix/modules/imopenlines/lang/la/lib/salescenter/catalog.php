<?php
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_LINK_TEXT"] = "Quiero vender en Facebook";
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_MESSAGE_1"] = "[B]Envíe selecciones de productos, al estilo de Facebook[/B]\r\n\r\nPuede elegir varios productos y enviarlos al chat del cliente para las personas que se comunican con usted mediante Facebook. sus clientes navegarán y seleccionarán los productos sin salir del chat. Todos los productos de la selección de Facebook se sincronizarán automáticamente con el catálogo de productos.";
