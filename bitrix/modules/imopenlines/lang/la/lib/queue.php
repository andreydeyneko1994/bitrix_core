<?php
$MESS["IMOL_QUEUE_OPERATOR_DISMISSAL"] = "Se eliminó la conversación del agente porque el agente fue despedido";
$MESS["IMOL_QUEUE_OPERATOR_NONWORKING"] = "Se eliminó la conversación del agente porque el agente aún no registra su hora de entrada o de salida.";
$MESS["IMOL_QUEUE_OPERATOR_NOT_AVAILABLE"] = "Se eliminó la conversación del agente porque el agente no está disponible.";
$MESS["IMOL_QUEUE_OPERATOR_OFFLINE"] = "Se eliminó la conversación del agente porque el agente está no está conectado.";
$MESS["IMOL_QUEUE_OPERATOR_REMOVING"] = "Se eliminó la conversación del agente porque el agente fue eliminado del canal abierto";
$MESS["IMOL_QUEUE_OPERATOR_VACATION"] = "Se eliminó la conversación del agente porque el agente está en licencia";
$MESS["QUEUE_OPERATOR_DEFAULT_NAME"] = "Agente";
