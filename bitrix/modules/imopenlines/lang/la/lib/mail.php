<?
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Gracias por contactarnos";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Usted";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "F d, G:i";
$MESS["IMOL_MAIL_FILE"] = "Archivo";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Su conversación ##SESSION_ID# con el representante el #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Gracias por ponerse en contacto con nosotros";
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Su conversación ##SESSION_ID# con el representante el #SITE_URL#";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "G:i";
?>