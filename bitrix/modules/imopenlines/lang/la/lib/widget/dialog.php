<?
$MESS["IMOL_WIDGET_CHAT_ERROR_CONFIG_NOT_FOUND"] = "El Canal Abierto especificado no existe.";
$MESS["IMOL_WIDGET_CHAT_ERROR_CREATE"] = "Error al crear el chat.";
$MESS["IMOL_WIDGET_CHAT_ERROR_USER_NOT_FOUND"] = "El usuario especificado no existe.";
$MESS["IMOL_WIDGET_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_WIDGET_CHAT_NOT_FOUND"] = "Error al crear el chat.";
$MESS["IMOL_WIDGET_OPERATOR_NAME"] = "Agente";
?>