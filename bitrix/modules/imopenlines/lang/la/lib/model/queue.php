<?
$MESS["QUEUE_ENTITY_CONFIG_ID_FIELD"] = "ID de configuración";
$MESS["QUEUE_ENTITY_ID_FIELD"] = "ID";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_EXACT_FIELD"] = "Tiempo de la reacción más reciente, ms.";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_FIELD"] = "Última hora de actividad";
$MESS["QUEUE_ENTITY_USER_AVATAR_FIELD"] = "Enlace de la imagen del usuario";
$MESS["QUEUE_ENTITY_USER_AVATAR_FILE_ID_FIELD"] = "ID del archivo de imagen del usuario";
$MESS["QUEUE_ENTITY_USER_ID_FIELD"] = "Id de usuario";
$MESS["QUEUE_ENTITY_USER_NAME_FIELD"] = "Persona responsable";
$MESS["QUEUE_ENTITY_USER_WORK_POSITION_FIELD"] = "Puesto";
?>