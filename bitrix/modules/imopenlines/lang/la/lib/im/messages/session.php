<?php
$MESS["IMOL_MESSAGE_SESSION_REOPEN"] = "La conversación ##LINK# se reanudó";
$MESS["IMOL_MESSAGE_SESSION_REPLY_TIME_LIMIT_DEFAULT"] = "Este canal abierto limita el tiempo de respuesta del agente. Obtenga más información en #A_START#este artículo#A_END#.";
$MESS["IMOL_MESSAGE_SESSION_START"] = "La conversación ##LINK# inició";
$MESS["IMOL_MESSAGE_SESSION_START_BY_MESSAGE"] = "Nueva conversación ##LINK# (derivada de ##LINK2#)";
