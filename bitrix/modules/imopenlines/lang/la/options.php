<?php
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "No se ha especificado una dirección pública correcta.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_DESCRIPTION"] = "Las actividades se pueden configurar para que se ejecuten en agentes o en cron.<br>
				1. <b>Agentes</b>: pueden ejecutarse con cada nueva ocurrencia o pueden asignarse a cron (es decir, programados). No se recomienda ejecutar por ocurrencia.<br>
				2. <b>Cron</b> es el modo recomendado. Debe asegurarse de que su servidor esté configurado correctamente antes de cambiar a esta opción. Configure /bitrix/tools/imopenlines/agents.php para que se ejecute cada minuto.<br>
				<a style=\"cursor: pointer\" class=\"bx-helpdesk-link\" onclick=\"top.BX.Helper.show('redirect=detail&code=9283081')\">Consulte la documentación para obtener más detalles</a>.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE"] = "Ejecutar como";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_AGENT"] = "Agente";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_CRON"] = "Cron";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Dirección pública de su sitio web";
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Ajustes";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Ajustes del módulo";
