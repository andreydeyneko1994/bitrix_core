<?php
$MESS["IMOL_IMESSAGE_CUSTOM_APP_BUTTON_SEND"] = "Enviar mensaje con geolocalización";
$MESS["IMOL_IMESSAGE_FAIL"] = "Error al enviar el mensaje.";
$MESS["IMOL_IMESSAGE_FORM_TEXT"] = "Puede enviar un mensaje con la ubicación de su oficina.";
$MESS["IMOL_IMESSAGE_LOCATION_MESSAGE_SENT"] = "Se envió un mensaje que contiene geolocalización.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_BUTTON_SEND"] = "Solicitar autorización";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST"] = "Puede enviar una solicitud de autorización a Facebook.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST_MESSAGE_SENT"] = "Se envió la solicitud de autorización.";
$MESS["IMOL_IMESSAGE_SUCCESS"] = "El mensaje se envió correctamente.";
