<?php
$MESS["IMOL_HISTORY_LOG_NAME"] = "Registro de conversación del Canal Abierto";
$MESS["IMOL_MAIL_PARAMS_DESC_NEW"] = "#EMAIL_TO# - Correo electrónico del destinatario
#TITLE# - Asunto del mensaje
#TEMPLATE_SESSION_ID# - ID de la sesión
#TEMPLATE_ACTION_TITLE# - Título de la acción
#TEMPLATE_ACTION_DESC# - Descripción de la acción
#TEMPLATE_WIDGET_DOMAIN# - Dominio del sitio del widget
#TEMPLATE_WIDGET_URL# - URL de la página del widget
#TEMPLATE_LINE_NAME# - Nombre del canal abierto";
$MESS["IMOL_OPERATOR_ANSWER_NAME_NEW"] = "Mensajes del cliente y respuestas del agente en la sesión de canal abiertol";
