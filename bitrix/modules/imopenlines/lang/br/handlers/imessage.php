<?php
$MESS["IMOL_IMESSAGE_CUSTOM_APP_BUTTON_SEND"] = "Enviar mensagem com geolocalização";
$MESS["IMOL_IMESSAGE_FAIL"] = "Erro ao enviar mensagem.";
$MESS["IMOL_IMESSAGE_FORM_TEXT"] = "Você pode enviar uma mensagem com a localização do seu escritório.";
$MESS["IMOL_IMESSAGE_LOCATION_MESSAGE_SENT"] = "A mensagem contendo geolocalização foi enviada.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_BUTTON_SEND"] = "Pedir autorização";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST"] = "Você pode enviar um pedido de autorização ao Facebook.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST_MESSAGE_SENT"] = "O pedido de autorização foi enviado.";
$MESS["IMOL_IMESSAGE_SUCCESS"] = "A mensagem foi enviada com sucesso.";
