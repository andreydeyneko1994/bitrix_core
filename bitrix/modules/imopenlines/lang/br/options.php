<?php
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Modo de depuração";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Nenhum endereço público correto especificado.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_DESCRIPTION"] = "As atividades podem ser configuradas para serem executadas por agentes ou no cron.<br>
				1. <b>Agentes</b>: podem executar a cada nova ocorrência ou podem ser atribuídos ao cron (ou seja, programados). Executar nas ocorrências não é recomendado.<br>
				2. <b>Cron</b> é o modo recomendado. Você deve certificar-se de que seu servidor está configurado corretamente antes de mudar para esta opção. Configire o /bitrix/tools/imopenlines/agents.php para ser executado a cada minuto.<br>
				<a style=\"cursor: pointer\" class=\"bx-helpdesk-link\" onclick=\"top.BX.Helper.show('redirect=detail&code=9283081')\">Consulte a documentação para obter mais detalhes</a>.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE"] = "Executar como";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_AGENT"] = "Agente";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_CRON"] = "Cron";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Endereço público do site";
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Configurações";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Configurações do módulo";
