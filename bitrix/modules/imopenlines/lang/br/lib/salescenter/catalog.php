<?php
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_LINK_TEXT"] = "Quero vender no Facebook";
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_MESSAGE_1"] = "[B]Enviar seleções de produtos, estilo Facebook[/B]\r\n\r\nVocê pode escolher vários produtos e enviá-los ao bate-papo do cliente para as pessoas que entrarem em contato com você pelo Facebook. Seus clientes vão navegar e selecionar os produtos sem sair do bate-papo. Todos os produtos da seleção do Facebook serão sincronizados automaticamente com o catálogo de produtos.";
