<?
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Obrigado por nos contatar";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Você";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "F d, G:i";
$MESS["IMOL_MAIL_FILE"] = "Arquivo";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Sua conversa ##SESSION_ID# com o representante em #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Obrigado por entrar em contato conosco";
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Sua conversa ##SESSION_ID# com o representante em #SITE_URL#";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "G:i";
?>