<?php
$MESS["IMOL_QUEUE_NOTIFICATION_EMPTY_QUEUE"] = "Estão faltando mensagens postadas por seus clientes através dos canais do Contact Center. Atualmente não há funcionários atribuídos à respectiva fila de mensagens. [URL=#URL#]Configurar fila de mensagens[/URL]";
$MESS["IMOL_QUEUE_SESSION_SKIP_ALONE"] = "Você não pode rejeitar a conversa porque é a única pessoa disponível na fila.";
$MESS["IMOL_QUEUE_SESSION_TRANSFER_OPERATOR_OFFLINE"] = "Sessão reenviada para a fila porque o agente estava offline";
