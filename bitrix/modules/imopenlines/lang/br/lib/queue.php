<?php
$MESS["IMOL_QUEUE_OPERATOR_DISMISSAL"] = "A conversa foi removida do agente porque o agente foi dispensado";
$MESS["IMOL_QUEUE_OPERATOR_NONWORKING"] = "A conversa foi removida do agente porque o agente já saiu ou ainda não chegou.";
$MESS["IMOL_QUEUE_OPERATOR_NOT_AVAILABLE"] = "A conversa foi removida do agente porque o agente não está disponível.";
$MESS["IMOL_QUEUE_OPERATOR_OFFLINE"] = "A conversa foi removida do agente porque este agente está offline";
$MESS["IMOL_QUEUE_OPERATOR_REMOVING"] = "A conversa foi removida do agente porque o agente foi removido do Canal Aberto";
$MESS["IMOL_QUEUE_OPERATOR_VACATION"] = "A conversa foi removida do agente porque o agente está de licença";
$MESS["QUEUE_OPERATOR_DEFAULT_NAME"] = "Agente";
