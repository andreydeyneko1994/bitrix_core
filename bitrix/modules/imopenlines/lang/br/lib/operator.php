<?
$MESS["IMOL_OPERATOR_ERROR_ACCESS_DENIED"] = "Você não tem permissão para acessar esta página.";
$MESS["IMOL_OPERATOR_ERROR_CANT_SAVE_QUICK_ANSWER"] = "Erro ao salvar resposta automática";
$MESS["IMOL_OPERATOR_ERROR_CHAT_ID"] = "O ID do bate-papo é inválido.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_TYPE"] = "Este bate-papo não é Canal Aberto";
$MESS["IMOL_OPERATOR_ERROR_IM_LOAD"] = "Erro ao iniciar o módulo Instant Messenger.";
$MESS["IMOL_OPERATOR_ERROR_PULL_LOAD"] = "Erro ao inicializar o módulo Push & Pull.";
$MESS["IMOL_OPERATOR_ERROR_USER_ID"] = "O ID do usuário é inválido.";
$MESS["IMOL_OPERATOR_USER_NAME"] = "Agente";
?>