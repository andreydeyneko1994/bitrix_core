<?
$MESS["QUEUE_ENTITY_CONFIG_ID_FIELD"] = "ID de configuração";
$MESS["QUEUE_ENTITY_ID_FIELD"] = "ID";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_EXACT_FIELD"] = "Tempo da reação mais recente, ms";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_FIELD"] = "Horário da última atividade";
$MESS["QUEUE_ENTITY_USER_AVATAR_FIELD"] = "Link da imagem do usuário";
$MESS["QUEUE_ENTITY_USER_AVATAR_FILE_ID_FIELD"] = "ID do arquivo de imagem do usuário";
$MESS["QUEUE_ENTITY_USER_ID_FIELD"] = "ID do usuário";
$MESS["QUEUE_ENTITY_USER_NAME_FIELD"] = "Pessoa responsável";
$MESS["QUEUE_ENTITY_USER_WORK_POSITION_FIELD"] = "Cargo";
?>