<?
$MESS["TRACKER_ENTITY_ACTION_FIELD"] = "Ação";
$MESS["TRACKER_ENTITY_CHAT_ID_FIELD"] = "ID do Bate-papo";
$MESS["TRACKER_ENTITY_CRM_ENTITY_ID_FIELD"] = "ID da entidade de CRM";
$MESS["TRACKER_ENTITY_CRM_ENTITY_TYPE_FIELD"] = "Tipo de entidade de CRM";
$MESS["TRACKER_ENTITY_DATE_CREATE_FIELD"] = "Criado em";
$MESS["TRACKER_ENTITY_FIELD_ID_FIELD"] = "ID do campo";
$MESS["TRACKER_ENTITY_FIELD_TYPE_FIELD"] = "Tipo de campo";
$MESS["TRACKER_ENTITY_FIELD_VALUE_FIELD"] = "Valor do campo";
$MESS["TRACKER_ENTITY_ID_FIELD"] = "ID";
$MESS["TRACKER_ENTITY_MESSAGE_ID_FIELD"] = "ID da mensagem";
$MESS["TRACKER_ENTITY_MESSAGE_ORIGIN_ID_FIELD"] = "ID da mensagem de origem";
$MESS["TRACKER_ENTITY_SESSION_ID_FIELD"] = "ID da sessão";
$MESS["TRACKER_ENTITY_USER_ID_FIELD"] = "ID do usuário";
?>