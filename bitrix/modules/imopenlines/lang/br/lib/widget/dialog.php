<?
$MESS["IMOL_WIDGET_CHAT_ERROR_CONFIG_NOT_FOUND"] = "O Canal Aberto especificado não existe.";
$MESS["IMOL_WIDGET_CHAT_ERROR_CREATE"] = "Erro ao criar bate-papo.";
$MESS["IMOL_WIDGET_CHAT_ERROR_USER_NOT_FOUND"] = "O usuário especificado não existe.";
$MESS["IMOL_WIDGET_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_WIDGET_CHAT_NOT_FOUND"] = "Erro ao criar bate-papo.";
$MESS["IMOL_WIDGET_OPERATOR_NAME"] = "Agente";
?>