<?php
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "Você não tem permissões suficientes para acessar essa conversa.";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "O ID do bate-papo é inválido.";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Este bate-papo não é Canal Aberto";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "O ID do formulário não foi enviado";
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Erro ao iniciar o módulo Instant Messenger.";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Erro ao inicializar o módulo Push & Pull.";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "O ID do usuário é inválido.";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-mail";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Histórico de conversa solicitado pelo cliente ##LINK#";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "Um histórico de conversa ##LINK# foi enviado para o cliente conforme solicitação dele";
$MESS["IMOL_LCC_FORM_NAME"] = "Nome";
$MESS["IMOL_LCC_FORM_NONE"] = "nenhum";
$MESS["IMOL_LCC_FORM_PHONE"] = "Telefone";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Formulário enviado";
$MESS["IMOL_LCC_GUEST_NAME"] = "Convidado";
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Alterar";
