<?
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Alterar";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Informações de contato salvas na empresa";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Informações de contato salvas no contato";
$MESS["IMOL_TRACKER_ERROR_NO_REQUIRED_PARAMETERS"] = "Parâmetros exigidos não foram enviados ou têm formato inválido";
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Um novo lead foi criado usando as informações de contato";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Informações de contato salvas no lead";
$MESS["IMOL_TRACKER_LIMIT_1"] = "As informações de contato não foram salvas no CRM porque o limite de reconhecimento estipulado pelo seu plano atual foi excedido. #LINK_START#Atualizar agora#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "A pesquisa de CRM não foi executada porque o limite de reconhecimento mensal estipulado pelo seu plano atual foi excedido. #LINK_START#Atualizar agora#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Fazer upgrade";
$MESS["IMOL_TRACKER_SESSION_COMPANY"] = "Empresa";
$MESS["IMOL_TRACKER_SESSION_CONTACT"] = "Contatar";
$MESS["IMOL_TRACKER_SESSION_DEAL"] = "Negócio";
?>