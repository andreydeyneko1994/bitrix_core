<?php
$MESS["IMOL_CHAT_ANSWER_F"] = "#USER# aceitou a conversa";
$MESS["IMOL_CHAT_ANSWER_M"] = "#USER# aceitou a conversa";
$MESS["IMOL_CHAT_APP_ICON_QUICK_DESCRIPTION"] = "Abrir banco de dados de respostas predefinidas";
$MESS["IMOL_CHAT_APP_ICON_QUICK_TITLE"] = "Respostas predefinidas";
$MESS["IMOL_CHAT_ASSIGN_OFF"] = "A conversa não foi atribuída ao agente, data de encerramento automático configurada de acordo com as regras da fila.";
$MESS["IMOL_CHAT_ASSIGN_ON"] = "A conversa foi atribuída ao agente, data de encerramento automático transferida para #DATE#";
$MESS["IMOL_CHAT_ASSIGN_OPERATOR_CRM_NEW"] = "Consulta redirecionada para #USER# (pessoa responsável pelo CRM)";
$MESS["IMOL_CHAT_ASSIGN_OPERATOR_LIST_NEW"] = "Consulta atribuída a todos os agentes na fila";
$MESS["IMOL_CHAT_ASSIGN_OPERATOR_NEW"] = "Consulta atribuída a #USER#";
$MESS["IMOL_CHAT_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_CHAT_CHAT_NAME_COLOR_GUEST"] = "#COLOR# convidado ##NUMBER#";
$MESS["IMOL_CHAT_CHAT_NAME_GUEST"] = "Convidado ##NUMBER#";
$MESS["IMOL_CHAT_CLOSE_F"] = "#USER# encerrou a conversa atual";
$MESS["IMOL_CHAT_CLOSE_FOR_OPEN_F"] = "#USER# encerrou a conversa atual e começou uma nova conversa";
$MESS["IMOL_CHAT_CLOSE_FOR_OPEN_M"] = "#USER# encerrou a conversa atual e começou uma nova conversa";
$MESS["IMOL_CHAT_CLOSE_M"] = "#USER# encerrou a conversa atual";
$MESS["IMOL_CHAT_END_BOT_SESSION"] = "O bot encaminhou a conversa para um agente";
$MESS["IMOL_CHAT_ERROR_ANSWER_ALREADY_RESPONSIBLE"] = "O agente que você está tentando atribuir já está atribuído a esta conversa";
$MESS["IMOL_CHAT_ERROR_ANSWER_COMPETITIVE_REQUEST"] = "Há um pedido simultâneo para aceitar esta conversa";
$MESS["IMOL_CHAT_ERROR_ANSWER_NOT_LOAD_SESSION"] = "Não é possível carregar a sessão";
$MESS["IMOL_CHAT_ERROR_CONNECTOR_LOAD"] = "Erro ao inicializar o módulo Conectores IM Externos.";
$MESS["IMOL_CHAT_ERROR_CONNECTOR_SEND"] = "Erro ao enviar mensagem";
$MESS["IMOL_CHAT_ERROR_IM_LOAD"] = "Erro ao iniciar o módulo Instant Messenger.";
$MESS["IMOL_CHAT_ERROR_NOT_LOAD_DATA"] = "Não foi possível carregar os dados do bate-papo";
$MESS["IMOL_CHAT_ERROR_PULL_LOAD"] = "Erro ao inicializar o módulo Push & Pull.";
$MESS["IMOL_CHAT_INTERCEPT_F"] = "#USER_1# aceitou a conversa do #USER_2#";
$MESS["IMOL_CHAT_INTERCEPT_M"] = "#USER_1# aceitou a conversa do #USER_2#";
$MESS["IMOL_CHAT_LEAD_ADD"] = "Novo Lead criado: \"#LEAD_NAME#\"";
$MESS["IMOL_CHAT_MARK_SPAM_F"] = "#USER# marcou a conversa atual como spam";
$MESS["IMOL_CHAT_MARK_SPAM_M"] = "#USER# marcou a conversa atual como spam";
$MESS["IMOL_CHAT_NEW_QUESTION_F"] = "#USER# tem uma nova pergunta";
$MESS["IMOL_CHAT_NEW_QUESTION_M"] = "#USER# tem uma nova pergunta";
$MESS["IMOL_CHAT_NEXT_IN_QUEUE_NEW"] = "Consulta transferida para #USER_TO# de acordo com as regras de enfileiramento";
$MESS["IMOL_CHAT_NOTIFY_RATING_CLIENT_DISLIKE_NEW"] = "O cliente avaliou a conversa ##NUMBER# como: [b]#RATING#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_CLIENT_LIKE_NEW"] = "O cliente avaliou a conversa ##NUMBER# como: [b]#RATING#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_F_COMMENT"] = "O supervisor #USER# deixou um comentário na conversa ##NUMBER#: [b]#COMMENT#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_F_LIKE"] = "O supervisor #USER# avaliou a conversa ##NUMBER# como: [b]#RATING#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_F_LIKE_AND_COMMENT"] = "O supervisor #USER# avaliou a conversa ##NUMBER# como [b]#RATING#[/b] e deixou um comentário: [b]#COMMENT#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_M_COMMENT"] = "O supervisor #USER# deixou um comentário na conversa ##NUMBER#: [b]#COMMENT#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_M_LIKE"] = "O supervisor #USER# avaliou a conversa ##NUMBER# como: [b]#RATING#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_HEAD_M_LIKE_AND_COMMENT"] = "O supervisor #USER# avaliou a conversa ##NUMBER# como [b]#RATING#[/b] e deixou um comentário: [b]#COMMENT#[/b]";
$MESS["IMOL_CHAT_NOTIFY_RATING_TITLE"] = "Avaliação da qualidade do serviço";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_1"] = "1";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_2"] = "2";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_3"] = "3";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_4"] = "4";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_5"] = "5";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_DISLIKE"] = "negativa";
$MESS["IMOL_CHAT_NOTIFY_RATING_VALUE_LIKE"] = "positiva";
$MESS["IMOL_CHAT_NOTIFY_SCHEMA_RATING_CLIENT_new"] = "Notificação de avaliação do cliente";
$MESS["IMOL_CHAT_NOTIFY_SCHEMA_RATING_HEAD"] = "Notificação de avaliação do supervisor";
$MESS["IMOL_CHAT_NO_OPERATOR_AVAILABLE_IN_QUEUE_NEW"] = "A consulta foi reenviada para a fila porque não há agentes disponíveis";
$MESS["IMOL_CHAT_PAUSE_OFF_NEW"] = "Data de encerramento automático estabelecida de acordo com as regras da fila";
$MESS["IMOL_CHAT_PAUSE_ON"] = "Fechamento automático adiado até #DATE#";
$MESS["IMOL_CHAT_RETURNED_TO_QUEUE_NEW"] = "A consulta foi reenviada para a fila.";
$MESS["IMOL_CHAT_SKIP_F"] = "#USER# rejeitou a conversa";
$MESS["IMOL_CHAT_SKIP_M"] = "#USER# rejeitou a conversa";
$MESS["IMOL_CHAT_STEALTH_OFF"] = "[b]O modo Silencioso está desativado.[/b][br]Seu contato pode ler suas mensagens subsequentes.";
$MESS["IMOL_CHAT_STEALTH_ON"] = "[b]O modo Silencioso está ativado.[/b][br]Suas mensagens não ficarão visíveis ao seu contato.";
$MESS["IMOL_CHAT_TO_QUEUE_NEW"] = "Consulta redirecionada de acordo com as regras da fila";
$MESS["IMOL_CHAT_TRANSFER_F"] = "#USER_FROM# transferiu a conversa para #USER_TO#";
$MESS["IMOL_CHAT_TRANSFER_LINE_F"] = "#USER_FROM# redirecionou a conversa da fila \"#LINE_FROM#\" para a fila \"#LINE_TO#\"";
$MESS["IMOL_CHAT_TRANSFER_LINE_M"] = "#USER_FROM# redirecionou a conversa da fila \"#LINE_FROM#\" para a fila \"#LINE_TO#\"";
$MESS["IMOL_CHAT_TRANSFER_M"] = "#USER_FROM# transferiu a conversa para #USER_TO#";
$MESS["IMOL_CRM_ERROR_NOT_GIVEN_CORRECT_DATA"] = "Nenhum dado válido foi enviado";
