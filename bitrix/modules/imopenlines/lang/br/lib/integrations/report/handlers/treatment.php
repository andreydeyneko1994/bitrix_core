<?
$MESS["OPEN_LINES_TREATMENT"] = "Sessões";
$MESS["WHAT_WILL_CALCULATE_ALL_APPOINTED"] = "Sessões designadas";
$MESS["WHAT_WILL_CALCULATE_ALL_MARK"] = "Sessões avaliadas";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT"] = "Total de sessões";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT_BY_HOUR"] = "Sessões por hora";
$MESS["WHAT_WILL_CALCULATE_ANSWERED"] = "Sessões respondidas";
$MESS["WHAT_WILL_CALCULATE_CONTENTMENT"] = "Índice de satisfação";
$MESS["WHAT_WILL_CALCULATE_DUPLICATE_TREATMENT_NEW"] = "Sessões, consultas de retorno";
$MESS["WHAT_WILL_CALCULATE_FIRST_TREATMENT_NEW"] = "Sessões, novas consultas";
$MESS["WHAT_WILL_CALCULATE_NEGATIVE_MARK"] = "Sessões com avaliação negativa";
$MESS["WHAT_WILL_CALCULATE_POSITIVE_MARK"] = "Sessões com avaliação positiva";
$MESS["WHAT_WILL_CALCULATE_SKIPPED"] = "Sessões ignoradas";
$MESS["WHAT_WILL_CALCULATE_WITHOUT_MARK"] = "Sessões sem avaliação";
?>