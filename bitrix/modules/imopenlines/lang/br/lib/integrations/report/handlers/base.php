<?
$MESS["CALCULATE_COUNT_OF_ALL_CALLS"] = "Total";
$MESS["FILTER"] = "Filtrar por:";
$MESS["FILTER_ALL_DEFAULT_TITLE"] = "Todos";
$MESS["FILTER_BY_CHANNEL"] = "Canais de comunicação:";
$MESS["FILTER_BY_OPEN_LINE"] = "Canal Aberto:";
$MESS["FILTER_BY_RESPONSIBLE"] = "Responsável:";
$MESS["GROUPING"] = "Agrupar por:";
$MESS["GROUPING_CHANEL"] = "Canal de comunicação";
$MESS["GROUPING_DATE"] = "Data";
$MESS["GROUPING_LINE"] = "Canal Aberto";
$MESS["GROUPING_RESPONSIBLE"] = "Responsável";
$MESS["REPORT_OPERATOR_DEMO_NAME_PREFIX_NEW"] = "Agente";
$MESS["WHAT_WILL_CALCULATE"] = "Contagem:";
?>