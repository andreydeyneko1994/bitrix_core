<?php
$MESS["IMOL_MESSAGE_SESSION_REOPEN"] = "Conversa ##LINK# retomada";
$MESS["IMOL_MESSAGE_SESSION_REPLY_TIME_LIMIT_DEFAULT"] = "Este Canal Aberto limita o tempo de resposta do agente. Saiba mais #A_START#neste artigo#A_END#";
$MESS["IMOL_MESSAGE_SESSION_START"] = "Conversa ##LINK# iniciada";
$MESS["IMOL_MESSAGE_SESSION_START_BY_MESSAGE"] = "Nova conversa ##LINK# iniciada (bifurcada de ##LINK2#)";
