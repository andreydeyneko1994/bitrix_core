<?
$MESS["IMOPENLINES_PUBLIC_PATH"] = "Endereço público do site:";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC"] = "É necessário um endereço público do site para que o Canal Aberto funcione corretamente.";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC_2"] = "Se o acesso externo à sua rede for restrito, ative o acesso a apenas algumas páginas. Consulte #LINK_START#documentação#LINK_END# para informações.";
?>