<?php
$MESS["IMOL_HISTORY_LOG_NAME"] = "Histórico de conversa do Canal Aberto";
$MESS["IMOL_MAIL_PARAMS_DESC_NEW"] = "#EMAIL_TO# - E-mail do destinatário
#TITLE# - Assunto da mensagem
#TEMPLATE_SESSION_ID# - ID da sessão
#TEMPLATE_ACTION_TITLE# - Título da ação
#TEMPLATE_ACTION_DESC# - Descrição da ação
#TEMPLATE_WIDGET_DOMAIN# - Domínio do site do Widget
#TEMPLATE_WIDGET_URL# - URL da página do Widget
#TEMPLATE_LINE_NAME# - Nome do Canal Aberto";
$MESS["IMOL_OPERATOR_ANSWER_NAME_NEW"] = "Mensagens do cliente e respostas do agente na sessão do Canal Aberto";
