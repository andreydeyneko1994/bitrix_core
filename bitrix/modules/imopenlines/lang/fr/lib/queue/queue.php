<?php
$MESS["IMOL_QUEUE_NOTIFICATION_EMPTY_QUEUE"] = "Il vous manque des messages publiés par vos clients via les canaux du Centre de contact. Aucun employé n'est actuellement affecté à la file d'attente de messages correspondante. [URL=#URL#]Configurer la file d'attente des messages[/URL]";
$MESS["IMOL_QUEUE_SESSION_SKIP_ALONE"] = "Vous ne pouvez pas refuser la conversation parce que vous êtes la seule personne disponible dans la file d'attente.";
$MESS["IMOL_QUEUE_SESSION_TRANSFER_OPERATOR_OFFLINE"] = "Session renvoyée en file d'attente parce que l'agent était hors ligne";
