<?
$MESS["OPEN_LINES_TREATMENT"] = "Sessions";
$MESS["WHAT_WILL_CALCULATE_ALL_APPOINTED"] = "Sessions affectées";
$MESS["WHAT_WILL_CALCULATE_ALL_MARK"] = "Sessions avec évaluation";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT"] = "Total des sessions";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT_BY_HOUR"] = "Sessions par heure";
$MESS["WHAT_WILL_CALCULATE_ANSWERED"] = "Sessions répondues";
$MESS["WHAT_WILL_CALCULATE_CONTENTMENT"] = "Niveau de satisfaction";
$MESS["WHAT_WILL_CALCULATE_DUPLICATE_TREATMENT_NEW"] = "Sessions, demandes récurrentes";
$MESS["WHAT_WILL_CALCULATE_FIRST_TREATMENT_NEW"] = "Sessions, nouvelles demandes";
$MESS["WHAT_WILL_CALCULATE_NEGATIVE_MARK"] = "Sessions dotées d'une évaluation négative";
$MESS["WHAT_WILL_CALCULATE_POSITIVE_MARK"] = "Sessions dotées d'une évaluation positive";
$MESS["WHAT_WILL_CALCULATE_SKIPPED"] = "Sessions ignorées";
$MESS["WHAT_WILL_CALCULATE_WITHOUT_MARK"] = "Sessions sans évaluation";
?>