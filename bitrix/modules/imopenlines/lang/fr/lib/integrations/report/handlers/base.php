<?
$MESS["CALCULATE_COUNT_OF_ALL_CALLS"] = "Total";
$MESS["FILTER"] = "Filtrer par : ";
$MESS["FILTER_ALL_DEFAULT_TITLE"] = "Tout";
$MESS["FILTER_BY_CHANNEL"] = "Canaux de communication : ";
$MESS["FILTER_BY_OPEN_LINE"] = "Canal ouvert : ";
$MESS["FILTER_BY_RESPONSIBLE"] = "Responsable : ";
$MESS["GROUPING"] = "Grouper par : ";
$MESS["GROUPING_CHANEL"] = "Canal de communication";
$MESS["GROUPING_DATE"] = "Date";
$MESS["GROUPING_LINE"] = "Canal ouvert";
$MESS["GROUPING_RESPONSIBLE"] = "Responsable";
$MESS["REPORT_OPERATOR_DEMO_NAME_PREFIX_NEW"] = "Agent";
$MESS["WHAT_WILL_CALCULATE"] = "Décompte : ";
?>