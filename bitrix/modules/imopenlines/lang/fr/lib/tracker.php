<?
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Annuler";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Modifier";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Les informations de contact ont été enregistrées dans la société";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Les informations de contact ont été enregistrées dans le contact";
$MESS["IMOL_TRACKER_ERROR_NO_REQUIRED_PARAMETERS"] = "Les paramètres requis n'ont pas été envoyés ou l'ont été dans un format non valide";
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Un nouveau prospect a été créé à partir des informations de contact";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Les informations de contact ont été enregistrées dans le prospect";
$MESS["IMOL_TRACKER_LIMIT_1"] = "Les informations de contact n'ont pas été enregistrées dans le CRM parce que vous avez dépassé la limite de reconnaissance de votre offre.
#LINK_START#Changez d'offre maintenant#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "La recherche CRM n'a pu être effectuée parce que vous avez dépassé la limite de reconnaissance mensuelle de votre offre. #LINK_START#Changez d'offre maintenant#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Changez d'offre";
$MESS["IMOL_TRACKER_SESSION_COMPANY"] = "Entreprise";
$MESS["IMOL_TRACKER_SESSION_CONTACT"] = "Contact";
$MESS["IMOL_TRACKER_SESSION_DEAL"] = "Transaction";
?>