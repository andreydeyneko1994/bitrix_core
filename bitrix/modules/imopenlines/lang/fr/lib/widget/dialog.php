<?
$MESS["IMOL_WIDGET_CHAT_ERROR_CONFIG_NOT_FOUND"] = "Le canal ouvert indiqué n'existe pas.";
$MESS["IMOL_WIDGET_CHAT_ERROR_CREATE"] = "Erreur lors de la création du chat.";
$MESS["IMOL_WIDGET_CHAT_ERROR_USER_NOT_FOUND"] = "L'utilisateur indiqué n'existe pas.";
$MESS["IMOL_WIDGET_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_WIDGET_CHAT_NOT_FOUND"] = "Erreur de création dans le chat.";
$MESS["IMOL_WIDGET_OPERATOR_NAME"] = "Agent";
?>