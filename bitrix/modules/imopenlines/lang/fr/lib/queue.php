<?php
$MESS["IMOL_QUEUE_OPERATOR_DISMISSAL"] = "La conversation a été retirée à l'agent parce que celui-ci a été renvoyé";
$MESS["IMOL_QUEUE_OPERATOR_NONWORKING"] = "La conversation a été retirée à l'agent parce que celui-ci a pointé son départ ou n'a pas encore pointé son arrivée.";
$MESS["IMOL_QUEUE_OPERATOR_NOT_AVAILABLE"] = "La conversation a été retirée à l'agent parce que celui-ci n'est pas disponible.";
$MESS["IMOL_QUEUE_OPERATOR_OFFLINE"] = "La conversation a été retirée à l'agent parce que celui-ci est hors ligne";
$MESS["IMOL_QUEUE_OPERATOR_REMOVING"] = "La conversation a été retirée à l'agent parce que celui-ci a été retiré du Canal ouvert";
$MESS["IMOL_QUEUE_OPERATOR_VACATION"] = "La conversation a été retirée à l'agent parce que celui-ci est en congé";
$MESS["QUEUE_OPERATOR_DEFAULT_NAME"] = "Agent";
