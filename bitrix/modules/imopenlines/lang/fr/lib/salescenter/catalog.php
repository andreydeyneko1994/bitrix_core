<?php
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_LINK_TEXT"] = "Je veux vendre sur Facebook";
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_MESSAGE_1"] = "[B]Envoyer des sélections de produits, à la manière de Facebook[/B]\r\n\r\nVous pouvez sélectionner plusieurs produits et les envoyer dans le chat avec les clients aux personnes qui vous contactent via Facebook. Vos clients pourront parcourir et sélectionner les produits sans quitter le chat. Tous les produits de la sélection Facebook seront synchronisés automatiquement avec le catalogue des produits.";
