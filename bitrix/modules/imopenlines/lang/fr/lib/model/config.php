<?php
$MESS["CONFIG_ENTITY_ACTIVE_FIELD"] = "Actif(ve)";
$MESS["CONFIG_ENTITY_AUTO_CLOSE_BOT_ID_FIELD"] = "ID du bot de diaglogue fermé automatiquement";
$MESS["CONFIG_ENTITY_AUTO_CLOSE_FORM_ID_FIELD"] = "L'ID du formulaire CRM à utiliser lors de la fermeture automatique de la conversation";
$MESS["CONFIG_ENTITY_AUTO_CLOSE_RULE_FIELD"] = "Règle de fermeture automatique de conversation";
$MESS["CONFIG_ENTITY_AUTO_CLOSE_TEXT_FIELD_NEW"] = "Texte de fermeture auto de la conversation";
$MESS["CONFIG_ENTITY_AUTO_CLOSE_TIME_FIELD_NEW"] = "Délai d'attente de fermeture automatique de la conversation";
$MESS["CONFIG_ENTITY_AUTO_EXPIRE_TIME_FIELD"] = "Texte d'expiration de la conversation";
$MESS["CONFIG_ENTITY_BOT_ID_FIELD"] = "ID du chat bot";
$MESS["CONFIG_ENTITY_BOT_TIME_FIELD"] = "Délai de file d'attente du message";
$MESS["CONFIG_ENTITY_CATEGORY_ENABLE_FIELD"] = "Disponibilité de catégorie";
$MESS["CONFIG_ENTITY_CATEGORY_ID_FIELD"] = "ID de catégorie par défaut";
$MESS["CONFIG_ENTITY_CHECK_AVAILABLE_FIELD"] = "Vérifier la disponibilité d'un agent";
$MESS["CONFIG_ENTITY_CLOSE_BOT_ID_FIELD"] = "ID du bot de dialogue fermé";
$MESS["CONFIG_ENTITY_CLOSE_FORM_ID_FIELD"] = "L'ID du formulaire CRM à utiliser lors de la fermeture de la conversation";
$MESS["CONFIG_ENTITY_CLOSE_RULE_FIELD"] = "Règle de fermeture de conversation";
$MESS["CONFIG_ENTITY_CLOSE_TEXT_FIELD"] = "Texte à utiliser lors de la fermeture de la conversation";
$MESS["CONFIG_ENTITY_CRM_CHAT_TRACKER_FIELD"] = "Tracker de chat";
$MESS["CONFIG_ENTITY_CRM_CREATE_FIELD"] = "Règle à appliquer quand aucune entrée CRM n'est trouvée";
$MESS["CONFIG_ENTITY_CRM_CREATE_SECOND_FIELD"] = "Règle secondaire à appliquer en cas d'absence d'entrée dans le CRM";
$MESS["CONFIG_ENTITY_CRM_CREATE_THIRD_FIELD"] = "Règle tertiaire à appliquer en cas d'absence d'entrée dans le CRM";
$MESS["CONFIG_ENTITY_CRM_FIELD"] = "Trouver dans la base de données CRM";
$MESS["CONFIG_ENTITY_CRM_FORWARD_FIELD"] = "Transférer à la personne responsable";
$MESS["CONFIG_ENTITY_CRM_SOURCE_FIELD"] = "Source d'appel pour l'entité CRM";
$MESS["CONFIG_ENTITY_CRM_TRANSFER_CHANGE_FIELD"] = "Changer la personne responsable lors du transfert";
$MESS["CONFIG_ENTITY_DATE_CREATE_FIELD"] = "Créé le";
$MESS["CONFIG_ENTITY_DATE_MODIFY_FIELD"] = "Modifié le";
$MESS["CONFIG_ENTITY_DEFAULT_OPERATOR_DATA_FIELD"] = "Informations d'agent par défaut";
$MESS["CONFIG_ENTITY_FULL_CLOSE_TIME_FIELD"] = "Délai avant fermeture complète de la conversation";
$MESS["CONFIG_ENTITY_ID_FIELD"] = "ID";
$MESS["CONFIG_ENTITY_KPI_ANSWER_ALERT"] = "Envoyer une notification si des messages du chat sont en retard de traitement";
$MESS["CONFIG_ENTITY_KPI_ANSWER_LIST"] = "Envoyez les notifications à";
$MESS["CONFIG_ENTITY_KPI_ANSWER_TEXT"] = "Message de notification";
$MESS["CONFIG_ENTITY_KPI_CHECK_OPERATOR_ACTIVITY"] = "Arrêter le minuteur si l'agent est indisponible";
$MESS["CONFIG_ENTITY_KPI_FIRST_ANSWER_TIME"] = "Temps de réponse initial";
$MESS["CONFIG_ENTITY_KPI_FURTHER_ANSWER_TIME"] = "Temps de réponse ultérieur";
$MESS["CONFIG_ENTITY_LINE_NAME_FIELD"] = "Nom";
$MESS["CONFIG_ENTITY_MODIFY_USER_ID_FIELD"] = "Préférences modifiées par";
$MESS["CONFIG_ENTITY_NO_ANSWER_BOT_ID_FIELD"] = "ID du bot aucune réponse";
$MESS["CONFIG_ENTITY_NO_ANSWER_FORM_ID_FIELD"] = "L'ID du formulaire CRM à utiliser quand aucune réponse n'est produite";
$MESS["CONFIG_ENTITY_NO_ANSWER_RULE_FIELD"] = "Règle à appliquer quand aucune réponse n'est produite";
$MESS["CONFIG_ENTITY_NO_ANSWER_TEXT_FIELD"] = "Texte à envoyer quand aucune réponse n'est produite";
$MESS["CONFIG_ENTITY_NO_ANSWER_TIME_FIELD"] = "Marquer la demande comme sans réponse dans";
$MESS["CONFIG_ENTITY_OFFLINE_FORM_ID"] = "Formulaire (ID) à afficher si l'agent est hors ligne";
$MESS["CONFIG_ENTITY_OPERATOR_DATA_FIELD"] = "Informations d'agent";
$MESS["CONFIG_ENTITY_QUEUE_TIME_FIELD_NEW"] = "Transférer au prochain délai d'attente de la file";
$MESS["CONFIG_ENTITY_QUEUE_TYPE_FIELD"] = "Mode de distribution des appels";
$MESS["CONFIG_ENTITY_SEND_NOTIFICATION_EMPTY_QUEUE"] = "Envoie une notification de Canal ouvert vide";
$MESS["CONFIG_ENTITY_SEND_WELCOME_EACH_SESSION"] = "Envoyer une réponse automatisée lorsque le client initie une nouvelle conversation";
$MESS["CONFIG_ENTITY_TEMPORARY_FIELD"] = "Préférences temporaires";
$MESS["CONFIG_ENTITY_USE_WELCOME_FORM"] = "Afficher le formulaire quand la conversation commence";
$MESS["CONFIG_ENTITY_VOTE_BEFORE_FINISH_FIELD"] = "Autoriser les utilisateurs de noter les conversations en cours";
$MESS["CONFIG_ENTITY_VOTE_CLOSING_DELAY_FIELD_NEW"] = "Fermer la session quand l'évaluation du client est reçue";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_1_DISLIKE_FIELD"] = "Texte d'évaluation négative (catégorie 1)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_1_LIKE_FIELD"] = "Texte d'évaluation positive (catégorie 1)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_1_TEXT_FIELD"] = "Texte de demande d'évaluation (catégorie 1)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_2_DISLIKE_FIELD"] = "Texte d'évaluation négative (catégorie 2)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_2_LIKE_FIELD"] = "Texte d'évaluation positive (catégorie 2)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_2_TEXT_FIELD"] = "Texte de demande d'évaluation (catégorie 2)";
$MESS["CONFIG_ENTITY_VOTE_MESSAGE_FIELD"] = "Envoyer une demande d'évaluation au client";
$MESS["CONFIG_ENTITY_WATCH_TYPING_FIELD"] = "Saisie en direct";
$MESS["CONFIG_ENTITY_WELCOME_BOT_ENABLE_FIELD"] = "Connecter le chat bot lors de la création d'une session";
$MESS["CONFIG_ENTITY_WELCOME_BOT_JOIN_FIELD"] = "Type de connexion du chat bot";
$MESS["CONFIG_ENTITY_WELCOME_BOT_LEFT_FIELD"] = "Mode de désactivation du chat bot";
$MESS["CONFIG_ENTITY_WELCOME_FORM_DELAY"] = "Afficher le formulaire d'accueil";
$MESS["CONFIG_ENTITY_WELCOME_FORM_ID"] = "Formulaire (ID) à afficher lorsque la conversation commence";
$MESS["CONFIG_ENTITY_WELCOME_MESSAGE_FIELD"] = "Envoyer une réponse automatique au message initial";
$MESS["CONFIG_ENTITY_WELCOME_MESSAGE_TEXT_FIELD_NEW"] = "Message de réponse automatique initial";
$MESS["CONFIG_ENTITY_WORKTIME_DAYOFF_BOT_ID_FIELD"] = "ID du bot en dehors des heures de fonctionnement";
$MESS["CONFIG_ENTITY_WORKTIME_DAYOFF_FIELD"] = "Weekends";
$MESS["CONFIG_ENTITY_WORKTIME_DAYOFF_FORM_ID_FIELD"] = "L'ID du formulaire CRM à utiliser en dehors des heures de fonctionnement";
$MESS["CONFIG_ENTITY_WORKTIME_DAYOFF_RULE_FIELD"] = "Règle à appliquer en dehors des heures de fonctionnement";
$MESS["CONFIG_ENTITY_WORKTIME_DAYOFF_TEXT_FIELD"] = "Texte à envoyer en dehors des heures de fonctionnement";
$MESS["CONFIG_ENTITY_WORKTIME_ENABLE_FIELD"] = "Activer la restriction du temps de service du canal";
$MESS["CONFIG_ENTITY_WORKTIME_FROM_FIELD"] = "Le temps du service commence à";
$MESS["CONFIG_ENTITY_WORKTIME_HOLIDAYS_FIELD"] = "Jours fériés";
$MESS["CONFIG_ENTITY_WORKTIME_TIMEZONE_FIELD"] = "Fuseau horaire du canal";
$MESS["CONFIG_ENTITY_WORKTIME_TO_FIELD"] = "Le temps du service termine à";
