<?php
$MESS["SESSION_CHECK_ENTITY_DATE_CLOSE_FIELD"] = "Date de fermeture";
$MESS["SESSION_CHECK_ENTITY_DATE_MAIL_FIELD"] = "Date d'envoi de l'e-mail";
$MESS["SESSION_CHECK_ENTITY_DATE_NO_ANSWER_FIELD"] = "Message marqué comme inactif le";
$MESS["SESSION_CHECK_ENTITY_DATE_QUEUE_FIELD"] = "Date de remise en file d'attente";
$MESS["SESSION_CHECK_ENTITY_SESSION_ID_FIELD"] = "ID de la session";
