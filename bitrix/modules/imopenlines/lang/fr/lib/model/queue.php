<?
$MESS["QUEUE_ENTITY_CONFIG_ID_FIELD"] = "ID de configuration";
$MESS["QUEUE_ENTITY_ID_FIELD"] = "ID";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_EXACT_FIELD"] = "Temps de la réaction la plus récente, ms";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_FIELD"] = "Heure de dernière activité";
$MESS["QUEUE_ENTITY_USER_AVATAR_FIELD"] = "Lien de l'image d'utilisateur";
$MESS["QUEUE_ENTITY_USER_AVATAR_FILE_ID_FIELD"] = "ID du fichier de l'image d'utilisateur";
$MESS["QUEUE_ENTITY_USER_ID_FIELD"] = "ID de l'utilisateur";
$MESS["QUEUE_ENTITY_USER_NAME_FIELD"] = "Responsable";
$MESS["QUEUE_ENTITY_USER_WORK_POSITION_FIELD"] = "Poste";
?>