<?php
$MESS["IMOL_IM_CLOSE_VOTE_MESSAGE"] = "Malheureusement, vous ne pouvez plus évaluer cette conversation (vous aviez #DAYS# pour envoyer une évaluation)";
$MESS["IMOL_IM_CLOSE_VOTE_MESSAGE_NO_DAY"] = "Malheureusement, vous ne pouvez plus évaluer cette conversation";
