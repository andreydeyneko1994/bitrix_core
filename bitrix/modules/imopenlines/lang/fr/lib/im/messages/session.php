<?php
$MESS["IMOL_MESSAGE_SESSION_REOPEN"] = "La conversation ##LINK# a repris";
$MESS["IMOL_MESSAGE_SESSION_REPLY_TIME_LIMIT_DEFAULT"] = "Ce Canal ouvert limite le temps de réponse des agents. Pour en savoir plus, consultez #A_START#cet article#A_END#.";
$MESS["IMOL_MESSAGE_SESSION_START"] = "La conversation ##LINK# a démarré";
$MESS["IMOL_MESSAGE_SESSION_START_BY_MESSAGE"] = "Nouvelle conversation ##LINK# démarrée (dérivée de ##LINK2#)";
