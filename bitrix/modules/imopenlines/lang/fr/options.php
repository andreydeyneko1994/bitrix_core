<?php
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Mode de débogage";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Aucune adresse publique correcte spécifiée.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_DESCRIPTION"] = "Les activités peuvent être configurées pour fonctionner sur des agents ou sur cron.<br>
				1. <b>Agents</b> : elles peuvent être exécutées à chaque nouvelle occurrence ou peuvent être assignées à cron (c'est-à-dire programmées). Exécuter via hits n'est pas recommandé.<br>
				2. <b>Cron</b> est le mode recommandé. Vous devez vous assurer que votre serveur est correctement configuré avant de passer à cette option. Configurez le fichier /bitrix/tools/imopenlines/agents.php pour qu'il s'exécute toutes les minutes.<br>
				<a style=\"cursor: pointer\" class=\"bx-helpdesk-link\" onclick=\"top.BX.Helper.show('redirect=detail&code=9283081')\">Veuillez vous référer à la documentation pour plus de détails</a>.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE"] = "Exécuter en tant que";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_AGENT"] = "Agent";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_CRON"] = "Cron";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Adresse public du site";
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Paramètres";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Paramètres du module";
