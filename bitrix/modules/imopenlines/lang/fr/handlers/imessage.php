<?php
$MESS["IMOL_IMESSAGE_CUSTOM_APP_BUTTON_SEND"] = "Envoyer un message avec la géolocalisation";
$MESS["IMOL_IMESSAGE_FAIL"] = "Erreur lors de l'envoi du message.";
$MESS["IMOL_IMESSAGE_FORM_TEXT"] = "Vous pouvez envoyer un message indiquant la localisation de vos bureaux.";
$MESS["IMOL_IMESSAGE_LOCATION_MESSAGE_SENT"] = "Un message contenant la géolocalisation a été envoyé.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_BUTTON_SEND"] = "Demander une autorisation";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST"] = "Vous pouvez envoyer une demande d'autorisation à Facebook.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST_MESSAGE_SENT"] = "La demande d'autorisation a été envoyée.";
$MESS["IMOL_IMESSAGE_SUCCESS"] = "Le message a été bien envoyé.";
