<?php
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Tryb debugowania";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Nie określono prawidłowego adresu publicznego.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_DESCRIPTION"] = "Działania można skonfigurować do uruchamiania na agentach lub na cronie.<br>
				1. <b>Agenci</b>: mogą działać przy każdym nowym trafieniu lub mogą być przypisani do crona (tj. według harmonogramu). Działanie przy trafieniach nie jest zalecane.<br>
				2. <b>Cron</b> to zalecany tryb. Przed przełączeniem się na tę opcję musisz upewnić się, że Twój serwer jest prawidłowo skonfigurowany. Ustaw /bitrix/tools/imopenlines/agents.php do uruchamiania co minutę.<br>
				<a style=\"cursor: pointer\" class=\"bx-helpdesk-link\" onclick=\"top.BX.Helper.show('redirect=detail&code=9283081')\">Więcej informacji można znaleźć w dokumentacji</a>.";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE"] = "Wykonaj jako";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_AGENT"] = "Konsultant";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_CRON"] = "Cron";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Adres publiczny strony internetowej";
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Ustawienia";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Ustawienia modułu";
