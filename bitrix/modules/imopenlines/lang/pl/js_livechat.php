<?
$MESS["DEFAULT_TITLE"] = "Otwarty kanał";
$MESS["ERROR_3RD_PARTY_COOKIE_DESC"] = "Pozwól przeglądarce na akceptowanie plików cookies lub użyj innych metod komunikacji.";
$MESS["ERROR_INTRANET_USER_DESC"] = "By użyć czatu, należy się wylogować z bieżącego konta Bitrix24: #URL#";
$MESS["ERROR_INTRANET_USER_DESC_2"] = "Nie możesz pisać na tym chacie, ponieważ jesteś już zalogowany w Bitrix24 z tej przeglądarki jako pracownik; Jednakże, może postować na Otwartym Kanale z #URL_START#Twojego portalu#URL_END# lub korzystać jednego z tych kanałów komunikacji:";
$MESS["ERROR_TITLE"] = "Niestety, nie mogliśmy uruchomić chatu na żywo.";
$MESS["ERROR_UNKNOWN"] = "Prosimy o skorzystanie z innych środków komunikacji.";
$MESS["LOADING_MESSAGE"] = "Daj nam znać, a odezwiemy się tak szybko, jak to możliwe.";
$MESS["POWERED_BY"] = "Wspierane przez";
$MESS["READY_TO_RESPOND"] = "Nasz zespół wykwalifikowanych doradców tylko czeka, by Ci pomóc!";
$MESS["RESPOND_LATER"] = "Jeden z naszych reprezentantów skontaktuje się z Tobą tak szybko, jak to możliwe.";
$MESS["SONET_ICONS"] = "Wolisz skorzystać z innego komunikatora?";
$MESS["SONET_ICONS_CLICK"] = "kliknij ikonę, aby zacząć używać wybranego przez Ciebie komunikatora";
$MESS["TEXTAREA_FILE"] = "Wyślij plik";
$MESS["TEXTAREA_HOTKEY"] = "Kliknij, aby zmienić skrót klawiaturowy";
$MESS["TEXTAREA_PLACEHOLDER"] = "Wpisz wiadomość";
$MESS["TEXTAREA_SEND"] = "Wyślij wiadomość";
$MESS["TEXTAREA_SMILE"] = "Wybierz emotikonę";
?>