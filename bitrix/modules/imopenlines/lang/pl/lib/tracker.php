<?
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Anuluj";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Zmień";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Dane kontaktowe zostały zapisane jako dane firmy";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Dane kontaktowe zostały zapisane jako dane kontaktu";
$MESS["IMOL_TRACKER_ERROR_NO_REQUIRED_PARAMETERS"] = "Wymagane parametry nie zostały przesłane lub mają nieprawidłowy format";
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Nowy lead został utworzony przy użyciu danych kontaktowych";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Dane kontaktowe zostały zapisane jako dane leada";
$MESS["IMOL_TRACKER_LIMIT_1"] = "Dane kontaktowe nie zostały zapisane w CRM, ponieważ przekroczony został limit rozpoznania określony w aktualnym planie. #LINK_START#Uaktualnij teraz#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "Dane kontaktowe nie zostały zapisane w CRM, ponieważ przekroczony został limit miesięcznego rozpoznania określony w aktualnym planie. #LINK_START#Uaktualnij teraz#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Aktualizacja";
$MESS["IMOL_TRACKER_SESSION_COMPANY"] = "Firma";
$MESS["IMOL_TRACKER_SESSION_CONTACT"] = "Kontakt";
$MESS["IMOL_TRACKER_SESSION_DEAL"] = "Oferta";
?>