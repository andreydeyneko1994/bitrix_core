<?php
$MESS["IMOL_MESSAGE_SESSION_REOPEN"] = "Wznowiono rozmowę ##LINK#";
$MESS["IMOL_MESSAGE_SESSION_REPLY_TIME_LIMIT_DEFAULT"] = "Ten Otwarty kanał ogranicza czas odpowiedzi konsultanta. Dowiedz się więcej w #A_START#artykule#A_END#.";
$MESS["IMOL_MESSAGE_SESSION_START"] = "Rozpoczęto rozmowę ##LINK#";
$MESS["IMOL_MESSAGE_SESSION_START_BY_MESSAGE"] = "Rozpoczęto rozmowę ##LINK# (odgałęzienie od ##LINK2#)";
