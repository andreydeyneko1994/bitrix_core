<?php
$MESS["IMOL_ADD_ERROR"] = "Błąd podczas tworzenia Otwartego Kanału";
$MESS["IMOL_CONFIG_CLOSE_TEXT"] = "Dziękujemy za kontakt! Prosimy powiedz nam jak podoba Ci się obsługa.";
$MESS["IMOL_CONFIG_CLOSE_TEXT_2"] = "Dziękujemy za współpracę z nami.";
$MESS["IMOL_CONFIG_KPI_FIRST_ANSWER_TEXT"] = "Operator #OPERATOR# przekroczył maksymalny czas początkowej reakcji w rozmowie ##DIALOG#.";
$MESS["IMOL_CONFIG_KPI_FURTHER_ANSWER_TEXT"] = "Operator #OPERATOR# przekroczył maksymalny czas reakcji w rozmowie ##DIALOG#.";
$MESS["IMOL_CONFIG_LINE_NAME"] = "Otwarty Kanał #NAME#";
$MESS["IMOL_CONFIG_NO_ANSWER_NEW"] = "Niestety nie możemy w tej chwili odpowiedzieć na Twoje zgłoszenie. Odezwiemy się tak szybko jak to możliwe, w godzinach pracy naszej firmy.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_DISLIKE"] = "Bardzo nam przykro, że nie mogliśmy Ci pomóc. Twoja opinia pozwoli nam udoskonalić nasze usługi.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_LIKE"] = "Dziękujemy!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_TEXT"] = "Chętnie poznamy Twoją opinię o tym, jak daliśmy sobie radę.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_DISLIKE"] = "Bardzo nam przykro, że nie mogliśmy Ci pomóc. Twoja opinia pozwoli nam udoskonalić nasze usługi.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_LIKE"] = "Dziękujemy!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_TEXT"] = "Daj nam znać, jak się sprawiliśmy. [br][br]Wystarczy, że wyślesz 1, jeśli jesteś zadowolony lub 0, jeśli coś możemy poprawić.";
$MESS["IMOL_CONFIG_WELCOME_MESSAGE"] = "Witamy na otwartym kanale [b]#COMPANY_NAME#[/b]![br]Wkrótce otrzymasz odpowiedź, prosimy o cierpliwość.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_2"] = "Niestety nie możemy teraz rozmawiać.[br][br]Prosimy wpisać pytanie, a my odpowiemy na nie możliwie najszybciej.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_3"] = "Witaj w Otwartym Kanale [b]#COMPANY_NAME#[/b] [br]Niestety nie możemy teraz rozmawiać.[br][br]Prosimy wpisać pytanie, a my odpowiemy na nie możliwie najszybciej.";
$MESS["IMOL_ERROR_UPDATE_EMPTY_DEPARTMENT_QUEUE"] = "Kolejka wymaga co najmniej jednej osoby (dodany dział nie zawiera pracowników)";
$MESS["IMOL_ERROR_UPDATE_EMPTY_QUEUE"] = "Kolejka wymaga co najmniej jednej osoby";
$MESS["IMOL_ERROR_UPDATE_NO_LOAD_LINE"] = "Spróbuj zaktualizować nieistniejący Otwarty kanał";
$MESS["IMOL_ERROR_UPDATE_NO_VALID_QUEUE"] = "Próba zapisania nieprawidłowych danych kolejki";
$MESS["IMOL_UPDATE_ERROR"] = "Błąd podczas aktualizowania Otwartego Kanału.";
