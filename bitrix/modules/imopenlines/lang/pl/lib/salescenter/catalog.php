<?php
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_LINK_TEXT"] = "Chcę sprzedawać na Facebooku";
$MESS["IMOL_SALESCENTER_CATALOG_FACEBOOK_OPENLINES_INFORMATION_MESSAGE_1"] = "[B]Wyślij wybór produktów, w stylu Facebooka[/B]\r\n\r\nMożesz wybrać wiele produktów i wysłać je na czat kliencki do osób, które kontaktują się z Tobą za pośrednictwem Facebooka. Klienci będą przeglądać i wybierać produkty bez opuszczania czatu. Wszystkie produkty wybrane na Facebooku zostaną automatycznie zsynchronizowane z katalogiem produktów.";
