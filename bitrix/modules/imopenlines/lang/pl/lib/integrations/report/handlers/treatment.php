<?
$MESS["OPEN_LINES_TREATMENT"] = "Sesje";
$MESS["WHAT_WILL_CALCULATE_ALL_APPOINTED"] = "Przypisane sesje";
$MESS["WHAT_WILL_CALCULATE_ALL_MARK"] = "Ocenione sesje";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT"] = "Wszystkie sesje";
$MESS["WHAT_WILL_CALCULATE_ALL_TREATMENT_BY_HOUR"] = "Sesje według godziny";
$MESS["WHAT_WILL_CALCULATE_ANSWERED"] = "Sesje na które odpowiedziano";
$MESS["WHAT_WILL_CALCULATE_CONTENTMENT"] = "Poziom zadowolenia";
$MESS["WHAT_WILL_CALCULATE_DUPLICATE_TREATMENT_NEW"] = "Sesje, powracające zapytania";
$MESS["WHAT_WILL_CALCULATE_FIRST_TREATMENT_NEW"] = "Sesje, nowe zapytania";
$MESS["WHAT_WILL_CALCULATE_NEGATIVE_MARK"] = "Sesje z negatywną oceną";
$MESS["WHAT_WILL_CALCULATE_POSITIVE_MARK"] = "Sesje z pozytywną oceną";
$MESS["WHAT_WILL_CALCULATE_SKIPPED"] = "Pominięte sesje";
$MESS["WHAT_WILL_CALCULATE_WITHOUT_MARK"] = "Nieocenione sesje";
?>