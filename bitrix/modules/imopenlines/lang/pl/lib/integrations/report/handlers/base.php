<?
$MESS["CALCULATE_COUNT_OF_ALL_CALLS"] = "Suma";
$MESS["FILTER"] = "Filtruj według:";
$MESS["FILTER_ALL_DEFAULT_TITLE"] = "Wszystko";
$MESS["FILTER_BY_CHANNEL"] = "Kanały komunikacji:";
$MESS["FILTER_BY_OPEN_LINE"] = "Otwarty kanał:";
$MESS["FILTER_BY_RESPONSIBLE"] = "Odpowiedzialny:";
$MESS["GROUPING"] = "Grupowane według:";
$MESS["GROUPING_CHANEL"] = "Kanał komunikacji";
$MESS["GROUPING_DATE"] = "Data";
$MESS["GROUPING_LINE"] = "Otwórz kanał";
$MESS["GROUPING_RESPONSIBLE"] = "Odpowiedzialny";
$MESS["REPORT_OPERATOR_DEMO_NAME_PREFIX_NEW"] = "Konsultant";
$MESS["WHAT_WILL_CALCULATE"] = "Liczenie:";
?>