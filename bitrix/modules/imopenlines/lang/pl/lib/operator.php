<?
$MESS["IMOL_OPERATOR_ERROR_ACCESS_DENIED"] = "Nie masz uprawnień dostępu do tego czatu.";
$MESS["IMOL_OPERATOR_ERROR_CANT_SAVE_QUICK_ANSWER"] = "Błąd zapisu gotowej odpowiedzi";
$MESS["IMOL_OPERATOR_ERROR_CHAT_ID"] = "ID czatu jest nieprawidłowe.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_TYPE"] = "Ten czat nie jest Otwartym Kanałem";
$MESS["IMOL_OPERATOR_ERROR_IM_LOAD"] = "Błąd przy inicjowaniu modułu Instant Messenger.";
$MESS["IMOL_OPERATOR_ERROR_PULL_LOAD"] = "Błąd podczas inicjowania modułu Push & Pull.";
$MESS["IMOL_OPERATOR_ERROR_USER_ID"] = "ID użytkownika jest nieprawidłowe.";
$MESS["IMOL_OPERATOR_USER_NAME"] = "Konsultant";
?>