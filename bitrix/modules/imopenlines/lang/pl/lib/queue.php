<?php
$MESS["IMOL_QUEUE_OPERATOR_DISMISSAL"] = "Rozmowa została usunięta z przypisania do konsultanta, ponieważ został on zwolniony";
$MESS["IMOL_QUEUE_OPERATOR_NONWORKING"] = "Rozmowa została usunięta z przypisania do konsultanta, który już się wyrejestrował lub jeszcze nie zarejestrował.";
$MESS["IMOL_QUEUE_OPERATOR_NOT_AVAILABLE"] = "Rozmowa została usunięta z przypisania do konsultanta, ponieważ nie jest on dostępny.";
$MESS["IMOL_QUEUE_OPERATOR_OFFLINE"] = "Rozmowa została usunięta z przypisania do konsultanta, ponieważ jest on offline";
$MESS["IMOL_QUEUE_OPERATOR_REMOVING"] = "Rozmowa została usunięta z przypisania do konsultanta, ponieważ został on usunięty z Otwartego kanału";
$MESS["IMOL_QUEUE_OPERATOR_VACATION"] = "Rozmowa została usunięta z przypisania do konsultanta, ponieważ jest on na urlopie";
$MESS["QUEUE_OPERATOR_DEFAULT_NAME"] = "Konsultant";
