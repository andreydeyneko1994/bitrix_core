<?
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Dziękujemy za kontaktowanie się z nami";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Ty";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "F d, G:i";
$MESS["IMOL_MAIL_FILE"] = "Plik";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Twoja rozmowa ##SESSION_ID# z przedstawicielem na #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Dziękujemy za skontaktowanie się z nami";
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Twoja rozmowa ##SESSION_ID# z przedstawicielem na #SITE_URL#";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "G:i";
?>