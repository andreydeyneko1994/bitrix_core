<?php
$MESS["IMOL_QUEUE_NOTIFICATION_EMPTY_QUEUE"] = "Brakuje Ci wiadomości publikowanych przez Twoich klientów za pośrednictwem kanałów Centrum kontaktowego. Obecnie nie ma żadnych pracowników przypisanych do odpowiedniej kolejki wiadomości. [URL=#URL#]Skonfiguruj kolejkę wiadomości[/URL]";
$MESS["IMOL_QUEUE_SESSION_SKIP_ALONE"] = "Nie możesz odrzucić rozmowy, ponieważ jesteś jedyną osobą dostępną w kolejce.";
$MESS["IMOL_QUEUE_SESSION_TRANSFER_OPERATOR_OFFLINE"] = "Sesja została ponownie przesłana do kolejki, ponieważ konsultant był offline";
