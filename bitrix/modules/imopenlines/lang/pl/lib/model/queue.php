<?
$MESS["QUEUE_ENTITY_CONFIG_ID_FIELD"] = "Konfiguracja ID";
$MESS["QUEUE_ENTITY_ID_FIELD"] = "ID";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_EXACT_FIELD"] = "Czas ostatniej reakcji, ms";
$MESS["QUEUE_ENTITY_LAST_ACTIVITY_DATE_FIELD"] = "Ostatni czas aktywności";
$MESS["QUEUE_ENTITY_USER_AVATAR_FIELD"] = "Link do obrazu użytkownika";
$MESS["QUEUE_ENTITY_USER_AVATAR_FILE_ID_FIELD"] = "ID pliku obrazu użytkownika";
$MESS["QUEUE_ENTITY_USER_ID_FIELD"] = "ID użytkownika";
$MESS["QUEUE_ENTITY_USER_NAME_FIELD"] = "Osoba odpowiedzialna";
$MESS["QUEUE_ENTITY_USER_WORK_POSITION_FIELD"] = "Stanowisko";
?>