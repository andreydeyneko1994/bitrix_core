<?
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "Ogólne";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "Dostawa";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "Zawiera gotowe odpowiedzi do użycia w Otwartych Kanałach";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "Odpowiedzi";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "Dodaj odpowiedź";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "Usuń odpowiedź";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "Edytuj odpowiedź";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "Odpowiedź";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "Wiadomość powitalna";
$MESS["IMOL_QA_IBLOCK_NAME"] = "Gotowe odpowiedzi";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "Nazwa";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "Płatność";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "Ocena";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "Sekcje";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "Dodaj sekcję";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "Usuń sekcję";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "Edytuj sekcję";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "Sekcja";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "Tekst gotowej odpowiedzi";
?>