<?
$MESS["IMOL_WIDGET_CHAT_ERROR_CONFIG_NOT_FOUND"] = "Podany otwarty kanał nie istnieje.";
$MESS["IMOL_WIDGET_CHAT_ERROR_CREATE"] = "Błąd podczas tworzenia czatu.";
$MESS["IMOL_WIDGET_CHAT_ERROR_USER_NOT_FOUND"] = "Podany użytkownik nie istnieje.";
$MESS["IMOL_WIDGET_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_WIDGET_CHAT_NOT_FOUND"] = "Błąd podczas tworzenia czatu.";
$MESS["IMOL_WIDGET_OPERATOR_NAME"] = "Konsultant";
?>