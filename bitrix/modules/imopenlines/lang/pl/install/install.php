<?php
$MESS["IMOPENLINES_CHECK_CONNECTOR"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMOPENLINES_CHECK_IM"] = "Moduł \"Komunikator\" nie jest zainstalowany.";
$MESS["IMOPENLINES_CHECK_IM_VERSION"] = "Proszę zaktualizować moduł \"Komunikator\" do wersji 16.5.0";
$MESS["IMOPENLINES_CHECK_PUBLIC_PATH"] = "Nie określono prawidłowego adresu.";
$MESS["IMOPENLINES_CHECK_PULL"] = "Moduł \"Push and Pull\" nie jest zainstalowany lub serwer kolejkowania nie jest skonfigurowany.";
$MESS["IMOPENLINES_INSTALL_TITLE"] = "Instalacja modułu \"Otwarte Kanały\"";
$MESS["IMOPENLINES_MODULE_DESCRIPTION"] = "Moduł Otwartych Kanałów.";
$MESS["IMOPENLINES_MODULE_NAME"] = "Otwarte kanały";
$MESS["IMOPENLINES_UNINSTALL_QUESTION"] = "Czy na pewno usunąć ten moduł?";
$MESS["IMOPENLINES_UNINSTALL_TITLE"] = "Deinstalacja modułu \"Otwarte Kanały\"";
