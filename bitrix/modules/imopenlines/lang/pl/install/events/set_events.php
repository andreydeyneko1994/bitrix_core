<?php
$MESS["IMOL_HISTORY_LOG_NAME"] = "Dziennik rozmów Otwartego Kanału";
$MESS["IMOL_MAIL_PARAMS_DESC_NEW"] = "#EMAIL_TO# – Adres e-mail odbiorcy
#TITLE# – Temat wiadomości
#TEMPLATE_SESSION_ID# – ID sesji
#TEMPLATE_ACTION_TITLE# – Nazwa działania
#TEMPLATE_ACTION_DESC# – Opis działania
#TEMPLATE_WIDGET_DOMAIN# – Domena strony widżeta
#TEMPLATE_WIDGET_URL# – URL strony widżeta
#TEMPLATE_LINE_NAME# – Nazwa Otwartego kanału";
$MESS["IMOL_OPERATOR_ANSWER_NAME_NEW"] = "Wiadomości klienta i odpowiedzi konsultantów w sesji Otwartego kanału";
