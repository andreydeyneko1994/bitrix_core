<?
$MESS["IMOPENLINES_PUBLIC_PATH"] = "Adres publiczny strony internetowej:";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC"] = "Aby strona Otwartego Kanału działała poprawnie, wymagany jest publiczny adres strony internetowej.";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC_2"] = "Jeśli zewnętrzny dostęp do Twojej sieci jest ograniczony, włącz dostęp tylko do niektórych stron. Więcej informacji znajdziesz w #LINK_START#dokumentacji#LINK_END#.";
?>