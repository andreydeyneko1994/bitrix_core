<?php
$MESS["IMOL_IMESSAGE_CUSTOM_APP_BUTTON_SEND"] = "Wyślij wiadomość z geolokalizacją";
$MESS["IMOL_IMESSAGE_FAIL"] = "Błąd podczas wysyłania wiadomości.";
$MESS["IMOL_IMESSAGE_FORM_TEXT"] = "Możesz wysłać wiadomość z lokalizacją swojego biura.";
$MESS["IMOL_IMESSAGE_LOCATION_MESSAGE_SENT"] = "Wiadomość zawierająca geolokalizację została wysłana.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_BUTTON_SEND"] = "Poproś o autoryzację";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST"] = "Możesz wysłać prośbę o autoryzację do Facebooka.";
$MESS["IMOL_IMESSAGE_OAUTH_FACEBOOK_REQUEST_MESSAGE_SENT"] = "Prośba o autoryzację została wysłana.";
$MESS["IMOL_IMESSAGE_SUCCESS"] = "Wiadomość została wysłana.";
