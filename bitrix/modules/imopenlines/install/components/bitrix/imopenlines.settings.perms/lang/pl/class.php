<?
$MESS["IMOL_PERM_ACCESS_DENIED"] = "Niewystarczające uprawnienia dostępu";
$MESS["IMOL_PERM_LICENSING_ERROR"] = "Twój plan nie zezwala na zarządzanie uprawnieniami dostępu do Otwartych Kanałów.";
$MESS["IMOL_PERM_UNKNOWN_ACCESS_CODE"] = "(nieznany ID dostępu)";
$MESS["IMOL_PERM_UNKNOWN_SAVE_ERROR"] = "Błąd podczas zapisywania danych";
?>