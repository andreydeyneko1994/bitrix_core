<?
$MESS["IMOL_ROLE_ERROR_EMPTY_NAME"] = "Nazwa roli nie jest określona";
$MESS["IMOL_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Niewystarczające uprawnienia dostępu";
$MESS["IMOL_ROLE_LICENSE_ERROR"] = "Błąd zapisu roli.";
$MESS["IMOL_ROLE_NOT_FOUND"] = "Rola nie została odnaleziona. Wypełnij formularz, aby utworzyć nową rolę.";
$MESS["IMOL_ROLE_SAVE_ERROR"] = "Błąd zapisu roli.";
?>