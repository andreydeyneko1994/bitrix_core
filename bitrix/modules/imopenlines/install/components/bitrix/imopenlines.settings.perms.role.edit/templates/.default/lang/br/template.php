<?php
$MESS["IMOL_PERM_RESTRICTION"] = "Por favor, faça um upgrade para um dos <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">planos comerciais</a> para atribuir permissões de acesso aos funcionários.";
$MESS["IMOL_ROLE_ACTION"] = "Ação";
$MESS["IMOL_ROLE_ENTITY"] = "Entidade";
$MESS["IMOL_ROLE_LABEL"] = "Função";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Alguns limites se aplicam. Clique, aqui, para saber mais.";
$MESS["IMOL_ROLE_PERMISSION"] = "Permissão";
$MESS["IMOL_ROLE_SAVE"] = "Salvar";
