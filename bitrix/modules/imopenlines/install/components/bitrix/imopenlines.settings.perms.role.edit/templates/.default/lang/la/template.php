<?php
$MESS["IMOL_PERM_RESTRICTION"] = "Actualice a uno de los <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">planes comerciales</a> para dar permisos de acceso a los empleados.";
$MESS["IMOL_ROLE_ACTION"] = "Acción";
$MESS["IMOL_ROLE_ENTITY"] = "Entidad";
$MESS["IMOL_ROLE_LABEL"] = "Rol";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Se aplican algunos límites. Haga clic aquí para obtener más información.";
$MESS["IMOL_ROLE_PERMISSION"] = "Permiso";
$MESS["IMOL_ROLE_SAVE"] = "Guardar";
