<?
$MESS["IMOL_QUICK_ANSWERS_EDIT_ALL"] = "Wszystko";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CANCEL"] = "Anuluj";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CREATE"] = "Utwórz";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR"] = "Nie można zapisać gotowej odpowiedzi. Proszę spróbuj później.";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR_EMPTY_TEXT"] = "Tekst nie powinien być pusty";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SECTION_TITLE"] = "Utwórz gotową odpowiedź w tej sekcji";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SUCCESS"] = "Twoja gotowa odpowiedź została zapisana!";
$MESS["IMOL_QUICK_ANSWERS_EDIT_TEXT_PLACEHOLDER"] = "Tekst gotowej odpowiedzi";
$MESS["IMOL_QUICK_ANSWERS_EDIT_UPDATE"] = "Aktualizuj";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_1"] = "zapisz wysłaną odpowiedź jako gotową poprzez kliknięcie przycisku <span class=\"imopenlines-iframe-quick-check-icon\"></span> obok wiadomości";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_2"] = "utwórz nowy od zera";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_3"] = "lub otwórz <a id=\"quick-info-all-url\" class=\"imopenlines-iframe-quick-link\">gotowe odpowiedzi od</a>";
$MESS["IMOL_QUICK_ANSWERS_INFO_TITLE_NEW"] = "Używaj gotowych odpowiedzi, aby szybko odpowiadać na często zadawane pytania";
$MESS["IMOL_QUICK_ANSWERS_NOT_FOUND"] = "Przepraszamy, ale nie mogliśmy niczego znaleźć";
$MESS["IMOL_QUICK_ANSWERS_SEARCH_PROGRESS"] = "Wyszukiwanie w toku...";
?>