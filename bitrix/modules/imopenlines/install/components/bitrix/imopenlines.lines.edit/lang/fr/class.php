<?php
$MESS["OL_COMPONENT_AUTOMATIC_MESSAGE_CLOSE_TITLE"] = "Mettre fin à la conversation";
$MESS["OL_COMPONENT_AUTOMATIC_MESSAGE_CONTINUE_TITLE"] = "Poursuivre la conversation";
$MESS["OL_COMPONENT_AUTOMATIC_MESSAGE_NEW_TITLE"] = "Nouvelle conversation";
$MESS["OL_COMPONENT_AUTOMATIC_MESSAGE_TITLE"] = "Nous n'avons apparemment jamais reçu de réponse de votre part. Avez-vous d'autres questions ?";
$MESS["OL_COMPONENT_CONFIG_EDIT_CRM_CREATE_DEAL"] = "Créer automatiquement une nouvelle transaction";
$MESS["OL_COMPONENT_CONFIG_EDIT_CRM_CREATE_DEAL_SECOND"] = "Créer une transaction dans cet entonnoir";
$MESS["OL_COMPONENT_CONFIG_EDIT_CRM_CREATE_DEAL_THIRD"] = "Utiliser une transaction active, ne pas en créer une nouvelle";
$MESS["OL_COMPONENT_CONFIG_EDIT_CRM_CREATE_IN_CHAT"] = "Créer manuellement dans la fenêtre du chat";
$MESS["OL_COMPONENT_CONFIG_EDIT_CRM_CREATE_LEAD"] = "Créer automatiquement un nouveau prospect";
$MESS["OL_COMPONENT_DISABLES_POPUP_HEAD_DEPARTMENT_EXCLUDED_QUEUE_TITLE"] = "Ne plus afficher";
$MESS["OL_COMPONENT_ERROR_EMPTY_DATA_UPDATE"] = "Il n'y a aucune donnée à enregistrer";
$MESS["OL_COMPONENT_ERROR_NO_PERMISSION_FOR_UPDATE"] = "Droits insuffisants pour enregistrer les préférences";
$MESS["OL_COMPONENT_ERROR_SAVE_CONFIG"] = "Erreur lors de l'enregistrement de la configuration du canal ouvert.";
$MESS["OL_COMPONENT_ERROR_UPDATE_SESSION_EXPIRED"] = "L'enregistrement a expiré. Veuillez réessayer.";
$MESS["OL_COMPONENT_FORMS_ADD_NEW"] = "Ajouter un nouveau formulaire";
$MESS["OL_COMPONENT_KPI_FIRST_ANSWER_TEXT"] = "#OPERATOR# a dépassé le temps de réponse initial maximum dans la conversation ##DIALOG#.";
$MESS["OL_COMPONENT_KPI_FURTHER_ANSWER_TEXT"] = "#OPERATOR# a dépassé le temps de réponse maximum dans la conversation ##DIALOG#.";
$MESS["OL_COMPONENT_LE_BOT_LIST"] = "le chat bot n'est pas sélectionné";
$MESS["OL_COMPONENT_LE_CRM_SOURCE_CREATE"] = "Source du Canal ouvert";
$MESS["OL_COMPONENT_LE_ERROR_PERMISSION"] = "Vous ne disposez pas des permissions suffisantes pour effectuer cette opération sur ce Canal ouvert";
$MESS["OL_COMPONENT_LE_KPI_ANSWER_TIME_MINUTES"] = "minutes";
$MESS["OL_COMPONENT_LE_KPI_ANSWER_TIME_MINUTES_1"] = "minute";
$MESS["OL_COMPONENT_LE_KPI_ANSWER_TIME_OWN"] = "Personnalisé";
$MESS["OL_COMPONENT_LE_KPI_ANSWER_TIME_SECONDS"] = "secondes";
$MESS["OL_COMPONENT_LE_MENU_AGREEMENTS"] = "Accord";
$MESS["OL_COMPONENT_LE_MENU_AGREEMENTS_1"] = "Consentement aux cookies";
$MESS["OL_COMPONENT_LE_MENU_AUTOMATIC_ACTIONS"] = "Actions automatiques";
$MESS["OL_COMPONENT_LE_MENU_BOTS"] = "Bots de chat";
$MESS["OL_COMPONENT_LE_MENU_KPI"] = "ICP du temps de réponse";
$MESS["OL_COMPONENT_LE_MENU_OTHERS"] = "Autre";
$MESS["OL_COMPONENT_LE_MENU_QUALITY_MARK"] = "Évaluation de la qualité";
$MESS["OL_COMPONENT_LE_MENU_QUEUE"] = "File d'attente";
$MESS["OL_COMPONENT_LE_MENU_WORKTIME"] = "Horaires";
$MESS["OL_COMPONENT_LE_OPTION_BOT"] = "Connecter le chat bot";
$MESS["OL_COMPONENT_LE_OPTION_BOT_ID"] = "Chat bot ";
$MESS["OL_COMPONENT_LE_OPTION_FORM"] = "Envoyer le formulaire CRM";
$MESS["OL_COMPONENT_LE_OPTION_FORM_ID"] = "Formulaire CRM";
$MESS["OL_COMPONENT_LE_OPTION_NONE"] = "Ne rien faire";
$MESS["OL_COMPONENT_LE_OPTION_QUALITY"] = "Envoyer le texte et l'évaluation de la qualité";
$MESS["OL_COMPONENT_LE_OPTION_TEXT"] = "Envoyer le texte";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_NO_ACCESS_CREATE"] = "Vous n'êtes pas autorisé à modifier ce Canal ouvert";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE"] = "Créer automatiquement";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR"] = "Erreur lors de la création de la liste de réponses prédéfinies. Veuillez contacter l'assistance technique.";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR_UNIQUE"] = "Une liste de Réponses prédéfinies existe déjà pour ce Canal ouvert.";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_NOT_SELECTED"] = "Non sélectionné";
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "Le module \"Canaux ouverts\" n'est pas installé.";
