<?
$MESS["CT_BLL_SELECTED"] = "Licznik rekordów";
$MESS["OL_STAT_BACK"] = "Powrót";
$MESS["OL_STAT_BACK_TITLE"] = "Powrót";
$MESS["OL_STAT_EXCEL"] = "Eksport do Microsoft Excel";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_CLOSE"] = "Zamknij";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_START"] = "Kontynuuj";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_STOP"] = "Stop";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_REQUEST_ERR"] = "Błąd przetwarzania żądania.";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_WAIT"] = "Proszę czekać...";
$MESS["OL_STAT_FILTER_CANCEL"] = "Wyczyść filtr";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Wyczyść filtr";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# – statystyki";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Wyczyść filtr pracownika";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Wyczyść filtr pracownika";
?>