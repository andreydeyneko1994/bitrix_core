<?
$MESS["CT_BLL_SELECTED"] = "Número de registros";
$MESS["OL_STAT_BACK"] = "Volver";
$MESS["OL_STAT_BACK_TITLE"] = "Volver atrás";
$MESS["OL_STAT_EXCEL"] = "Exportar a Microsoft Excel";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_CLOSE"] = "Cerrar";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_START"] = "Continuar";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_STOP"] = "Detener";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_REQUEST_ERR"] = "Error al procesar la solicitud.";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_WAIT"] = "Por favor, espere...";
$MESS["OL_STAT_FILTER_CANCEL"] = "Restablecer filtro";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Restablecer filtro";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Estadística";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Restablecer filto de empleado";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Restablecer filtro de empleado";
?>