<?php
$MESS["IMOL_MA_ATTACHMENT"] = "Załączniki";
$MESS["IMOL_MA_ATTACHMENT_DISK"] = "Dysk";
$MESS["IMOL_MA_ATTACHMENT_TYPE"] = "Typ załącznika";
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "Brakuje parametru \"Wiadomość Tekstowa\".";
$MESS["IMOL_MA_IS_SYSTEM"] = "Ukryta wiadomość (tryb cichy)";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "Opublikowana wiadomość nie będzie widoczna dla kontaktu zewnętrznego (tryb cichy)";
$MESS["IMOL_MA_MESSAGE"] = "Tekst wiadomości";
$MESS["IMOL_MA_NO_CHAT"] = "Nie odnaleziono czatu klienta";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "Brak klienta z podłączonym Otwartym Kanałem";
$MESS["IMOL_MA_TIMELINE_ERROR"] = "Wiadomość nie została wysłana. #ERROR_TEXT#";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "Bieżący element nie obsługuje tego typu działania";
