<?
$MESS["ME_ACTION"] = "Rozpocznij spotkanie";
$MESS["ME_AGENDA"] = "Program";
$MESS["ME_CHANGE"] = "zmień";
$MESS["ME_CLOSE"] = "Zakończ spotkanie";
$MESS["ME_COMMENTS"] = "Komentarze";
$MESS["ME_COPY"] = "Utwórz nastęone spotkanie";
$MESS["ME_CURRENT_STATE"] = "Status";
$MESS["ME_DATE_START"] = "Początek";
$MESS["ME_DESCR_TITLE"] = "Opis spotkania";
$MESS["ME_EDIT_TITLE"] = "Edytuj";
$MESS["ME_FILES"] = "Pliki";
$MESS["ME_GROUP"] = "Projekt";
$MESS["ME_KEEPER"] = "Sekretarz spotkania";
$MESS["ME_LIST_TITLE"] = "Pokaż spotkania";
$MESS["ME_MEMBERS"] = "Uczestnicy";
$MESS["ME_OWNER"] = "Właściciel";
$MESS["ME_PLACE"] = "Zasób";
$MESS["ME_PREPARE"] = "Wznów spotkanie";
$MESS["ME_REFUSED"] = "Odmówione";
?>