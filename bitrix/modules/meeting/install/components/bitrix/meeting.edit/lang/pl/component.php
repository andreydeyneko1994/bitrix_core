<?
$MESS["ME_MEETING_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["ME_MEETING_ADD"] = "Utwórz nowe spotkanie";
$MESS["ME_MEETING_COPY"] = "Utwórz kolejne spoktanie";
$MESS["ME_MEETING_EDIT"] = "Numer spotkania #ID# w dniu #DATE#";
$MESS["ME_MEETING_EDIT_NO_DATE"] = "Numer Spotkania #ID#";
$MESS["ME_MEETING_NOT_FOUND"] = "Nie znaleziono spotkania.";
$MESS["ME_MEETING_TITLE_DEFAULT"] = "(bez tytułu)";
$MESS["ME_MEETING_TITLE_DEFAULT_1"] = "Temat";
$MESS["ME_MODULE_NOT_INSTALLED"] = "Moduł \"Spotkania i Odprawy\" nie jest zainstalowany.";
?>