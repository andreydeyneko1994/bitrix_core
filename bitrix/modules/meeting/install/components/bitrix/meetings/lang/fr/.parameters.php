<?
$MESS["INTL_IBLOCK"] = "Bloc d'information salles de conférences";
$MESS["INTL_IBLOCK_TYPE"] = "Type du bloc d'Informations de points de négociations";
$MESS["INTL_IBLOCK_TYPE_V"] = "Type du bloc d'information des salles de réunion vidéo";
$MESS["INTL_IBLOCK_V"] = "Bloc d'information des salles de vidéoconférences";
$MESS["M_MEETINGS_COUNT"] = "Nombre de réunions par page";
$MESS["M_NAME_TEMPLATE"] = "Nommer le modèle d'affichage";
$MESS["M_PARAM_list"] = "Modèle de chemin d'accès à la page de la liste des réunions";
$MESS["M_PARAM_meeting"] = "Modèle de chemin d'accès à la page d'information sur la réunion";
$MESS["M_PARAM_meeting_copy"] = "Modèle de chemin d'accès à la page de création de la réunion suivante";
$MESS["M_PARAM_meeting_edit"] = "Modèle de chemin d'accès à la page d'édition d'informations sur la réunion";
$MESS["M_PARAM_meeting_item"] = "Modèle de chemin d'accès à la page d'information détaillée sur un point de l'ordre du jour";
?>