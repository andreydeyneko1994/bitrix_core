<?php
$MESS["MEETING_INSTALL_GO"] = "Otwórz sekcję publiczną";
$MESS["MEETING_INSTALL_PUBLIC"] = "Instalować sekcję publiczną?";
$MESS["MEETING_INSTALL_TITLE"] = "Instalacja Modułu Spotkań i Odpraw";
$MESS["MEETING_MODULE_DESCRIPTION"] = "Moduł do organizacj spotkań i odpraw.";
$MESS["MEETING_MODULE_NAME"] = "Spotkania i odprawy";
$MESS["MEETING_UNINSTALL_TITLE"] = "Odinstalowanie Modułu Spotkań i Odpraw";
