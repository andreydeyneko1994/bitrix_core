<?php
$MESS["WEBS_INSTALL_TITLE"] = "Installation du module pour sondages";
$MESS["WEBS_MODULE_DESCRIPTION"] = "Module permettant d'organiser le système des sondages et des votes des visiteurs du site.";
$MESS["WEBS_MODULE_NAME"] = "Services web";
$MESS["WEBS_UNINSTALL_TITLE"] = "Suppression du module d'interrogations";
