<?php
$MESS["WEBS_INSTALL_TITLE"] = "Instalacja modułu Web Services";
$MESS["WEBS_MODULE_DESCRIPTION"] = "Moduł serwisowy web i SOAP";
$MESS["WEBS_MODULE_NAME"] = "Usługi sieciowe";
$MESS["WEBS_UNINSTALL_TITLE"] = "Odinstalowanie modułu Web Services";
$MESS["WS_GADGET_DESCR"] = "Urzytkownicy Windows Vista mogą pobrać specjalne urządzenie Vista Sidebar do wyświetlania statystyk podsumowania dla witryny i zainstalować go w pasku bocznym lub na pulpicie.";
$MESS["WS_GADGET_LINK"] = "Pobież urządzenie Vista Sidebar";
