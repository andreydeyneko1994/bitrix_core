<?php
$MESS["WEBS_INSTALL_TITLE"] = "Instalar módulo Web Services";
$MESS["WEBS_MODULE_DESCRIPTION"] = "Brinda funciones para implementar y usar Web Services y SOAP.";
$MESS["WEBS_MODULE_NAME"] = "Servicios web";
$MESS["WEBS_UNINSTALL_TITLE"] = "Desinstalar módulo Web Services";
