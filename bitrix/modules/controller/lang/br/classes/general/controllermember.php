<?
$MESS["CTRLR_MEM_COUNTERS_ERR1"] = "Erro ao salvar contadores:";
$MESS["CTRLR_MEM_ERR1"] = "O servidor especificado não pôde ser encontrado:";
$MESS["CTRLR_MEM_ERR2"] = "Erro na execução do comando: responsta não reconhecida ou acesso negado.";
$MESS["CTRLR_MEM_ERR3"] = "Erro na execução do comando:";
$MESS["CTRLR_MEM_ERR4"] = "Grupo não foi encontrado:";
$MESS["CTRLR_MEM_ERR6"] = "O cliente não foi encontrado:";
$MESS["CTRLR_MEM_ERR7"] = "O cliente está desconectado do controlador. Nenhuma operação pode ser realizada.";
$MESS["CTRLR_MEM_ERR_MEMBER_ID"] = "A ID única não pode começar com um dígito. Por favor use algum outro valor.";
$MESS["CTRLR_MEM_ERR_MEMBER_NAME"] = "O campo nome não pode estar vazio.";
$MESS["CTRLR_MEM_ERR_MEMBER_UID"] = "O valor especificado no campo \"ID Única\" já existe. Por favor use on valor diferente.";
$MESS["CTRLR_MEM_ERR_MEMBER_URL"] = "O campo URL não pode estar vazio.";
$MESS["CTRLR_MEM_LOG_DESC_COMMAND"] = "Execução de comando redirecionada";
$MESS["CTRLR_MEM_LOG_DESC_GROUP"] = "Grupo:";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET"] = "Conectando cliente usando credenciais de administrador local";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET2"] = "Conectando cliente usando ticket temporário";
$MESS["CTRLR_MEM_LOG_DISCON"] = "Desconectando cliente do controlador";
$MESS["CTRLR_MEM_LOG_DISCON_ERR"] = "Erro ao desconectar.";
$MESS["CTRLR_MEM_LOG_SITE_CLO"] = "Site cliente fechado";
$MESS["CTRLR_MEM_LOG_SITE_OPE"] = "site cliente aberto";
?>