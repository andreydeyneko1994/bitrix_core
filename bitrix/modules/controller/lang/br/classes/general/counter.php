<?php
$MESS["CTRL_COUNTER_ERR_COMMAND"] = "O campo \"Comando\" não pode estar vazio.";
$MESS["CTRL_COUNTER_ERR_NAME"] = "O campo \"Nome\" não pode estar vazio.";
$MESS["CTRL_COUNTER_FORMAT_NONE"] = "(Nenhum)";
$MESS["CTRL_COUNTER_TYPE_DATETIME"] = "Data e hora";
$MESS["CTRL_COUNTER_TYPE_FILE_SIZE"] = "como o tamanho do arquivo";
$MESS["CTRL_COUNTER_TYPE_FLOAT"] = "Número";
$MESS["CTRL_COUNTER_TYPE_INT"] = "Número inteiro";
$MESS["CTRL_COUNTER_TYPE_STRING"] = "Série";
