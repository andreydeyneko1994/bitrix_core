<?
$MESS["CTRLR_LOG_ERR_NAME"] = "O campo nome não pode estar vazio.";
$MESS["CTRLR_LOG_ERR_UID"] = "O ID do cliente não pode estar vazio.";
$MESS["CTRLR_LOG_TYPE_AUTH"] = "Autorização do controlador";
$MESS["CTRLR_LOG_TYPE_REGISTRATION"] = "Registro de novo cliente";
$MESS["CTRLR_LOG_TYPE_REMOTE_COMMAND"] = "Execução de comando remoto";
$MESS["CTRLR_LOG_TYPE_SET_SETTINGS"] = "Propagar confiurações para site cliente]";
$MESS["CTRLR_LOG_TYPE_SITE_CLOSE"] = "Fechar/abrir sites cliente";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE"] = "Instalar atualizações";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL"] = "Atualizar núcleo comum";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL_DB"] = "Atualizar banco de dados de sites com núcleo comum";
$MESS["CTRLR_LOG_TYPE_UNREGISTRATION"] = "Remover cliente do controlador";
$MESS["CTRLR_LOG_TYPE_UPDATE_COUNTERS"] = "Atualização do contador";
?>