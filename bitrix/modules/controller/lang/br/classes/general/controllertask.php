<?
$MESS["CTRLR_TASK_ERR_ALREADY"] = "Esta tarefa já está atribuída ao cliente";
$MESS["CTRLR_TASK_ERR_BAD_ID"] = "ID da tarefa não reconhecida.";
$MESS["CTRLR_TASK_ERR_CLIENTID"] = "O ID do cliente não pode estar vazio.";
$MESS["CTRLR_TASK_ERR_ID"] = "O ID da tarefa não pode estar vazio.";
$MESS["CTRLR_TASK_ERR_KERNEL"] = "Sites de núcleo comuns devem ser atualizados usando um formulário de atualização especial.";
$MESS["CTRLR_TASK_STATUS_COMPL"] = "Completado com sucesso";
$MESS["CTRLR_TASK_STATUS_FAIL"] = "A execução da tarefa falhou";
$MESS["CTRLR_TASK_STATUS_NEW"] = "Novo";
$MESS["CTRLR_TASK_STATUS_PART"] = "Parcialmente completo";
$MESS["CTRLR_TASK_TYPE_CLOSE_MEMBER"] = "Fechar/Abrir clientes";
$MESS["CTRLR_TASK_TYPE_COUNTERS_UPDATE"] = "Atualizar contadores";
$MESS["CTRLR_TASK_TYPE_REMOTE_COMMAND"] = "Executar comando";
$MESS["CTRLR_TASK_TYPE_SET_SETTINGS"] = "Propagar configurações do grupo";
$MESS["CTRLR_TASK_TYPE_UPDATE"] = "Atualizar site";
$MESS["CTRLR_TASK_UPD_COMPL"] = "Nenhuma atualização disponível";
?>