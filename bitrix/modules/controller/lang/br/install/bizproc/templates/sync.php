<?
$MESS["BPT_SYNC_DESC"] = "Permite a publicação de elementos debloco de informação em sites controlados. O documento será publicado primeiro no site controlador.";
$MESS["BPT_SYNC_NAME"] = "Publicação em sites controlados";
$MESS["BPT_SYNC_PUBLISH"] = "Publicar documento";
$MESS["BPT_SYNC_SEQ"] = "Processo de negócio sequencial";
$MESS["BPT_SYNC_SYNC"] = "Publicação em sites controlados";
?>