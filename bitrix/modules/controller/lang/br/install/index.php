<?
$MESS["CTRL_INST_DESC"] = "Gerenciamento remoto do site";
$MESS["CTRL_INST_NAME"] = "Controlador do site";
$MESS["CTRL_INST_STEP1"] = "Instalação do Módulo Controlador: Passo 1";
$MESS["CTRL_INST_STEP1_UN"] = "Desinstalação do Módulo Controlador: Passo 1";
$MESS["CTRL_PERM_D"] = "Acesso negado";
$MESS["CTRL_PERM_L"] = "Autorizar em sites";
$MESS["CTRL_PERM_R"] = "Somente leitura";
$MESS["CTRL_PERM_T"] = "Adicionar novos sites";
$MESS["CTRL_PERM_V"] = "Gerenciar sites";
$MESS["CTRL_PERM_W"] = "Acesso completo:";
?>