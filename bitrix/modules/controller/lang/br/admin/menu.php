<?
$MESS["CTRLR_MENU_AUTH"] = "Autenticação";
$MESS["CTRLR_MENU_COUNTERS"] = "Contadores";
$MESS["CTRLR_MENU_COUNTERS_TITLE"] = "Configurar contadores";
$MESS["CTRLR_MENU_GROUP_NAME"] = "Grupos";
$MESS["CTRLR_MENU_GROUP_TYPE"] = "Gerenciar as configurações do grupo local";
$MESS["CTRLR_MENU_LOG_NAME"] = "Diário";
$MESS["CTRLR_MENU_LOG_TITLE"] = "Diário do controlador";
$MESS["CTRLR_MENU_NAME"] = "Controlador";
$MESS["CTRLR_MENU_RUN_NAME"] = "Execução de comando remoto";
$MESS["CTRLR_MENU_RUN_TITLE"] = "Executar comandos PHP em sites remotos";
$MESS["CTRLR_MENU_SITE_NAME"] = "Sites";
$MESS["CTRLR_MENU_SITE_TITLE"] = "Gerenciar sites clientes";
$MESS["CTRLR_MENU_TASK_NAME"] = "Tarefas";
$MESS["CTRLR_MENU_TASK_TITLE"] = "Tarefas para execução remota em sites cliente";
$MESS["CTRLR_MENU_TITLE"] = "Controlador do site";
$MESS["CTRLR_MENU_UPD_NAME"] = "Atualizar Sites";
$MESS["CTRLR_MENU_UPD_TYPE"] = "Atualizar sites cliente";
$MESS["CTRLR_MENU_UPLOAD_NAME"] = "Enviar arquivos";
$MESS["CTRLR_MENU_UPLOAD_TITLE"] = "Enviar arquivos para sites cliente";
?>