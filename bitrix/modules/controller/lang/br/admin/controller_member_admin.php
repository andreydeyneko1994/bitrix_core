<?
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_DISC"] = "desconectar do controlador";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPD"] = "atualizar sites";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPDCNT"] = "atualizar valores dos contadores do cliente";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPDSETT"] = "atualizar as configurações do grupo no lado cliente";
$MESS["CTRL_MEMB_ADMIN_COLUMN_ACTIVE"] = "Ativo";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_FREE"] = "Espaço livre, KB";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_LAST_AU"] = "Última autorização";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_SITES"] = "Sites";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_UPD"] = "Última atualização dos contadores";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_USERS"] = "Usuários";
$MESS["CTRL_MEMB_ADMIN_COLUMN_CREATED"] = "Criado";
$MESS["CTRL_MEMB_ADMIN_COLUMN_CREATEDBY"] = "Criador";
$MESS["CTRL_MEMB_ADMIN_COLUMN_MODIFIED"] = "Modificado";
$MESS["CTRL_MEMB_ADMIN_COLUMN_MODIFIEDBY"] = "Modificado por";
$MESS["CTRL_MEMB_ADMIN_COLUMN_NAME"] = "Nome";
$MESS["CTRL_MEMB_ADMIN_COLUMN_NOTES"] = "Notas";
$MESS["CTRL_MEMB_ADMIN_COLUMN_SHARED_KERN"] = "núcleo comum";
$MESS["CTRL_MEMB_ADMIN_CONTACT_PERSON"] = "Pessoa para contato";
$MESS["CTRL_MEMB_ADMIN_DEL_ERR"] = "Erro ao excluir um site";
$MESS["CTRL_MEMB_ADMIN_DISCON"] = "não foi conectado";
$MESS["CTRL_MEMB_ADMIN_DISC_ERR"] = "Erro ao desconectar um site cliente";
$MESS["CTRL_MEMB_ADMIN_EMAIL"] = "E-mail";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACTIVE"] = "Ativo";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACT_FROM"] = "Ativo desde";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACT_TO"] = "Ativo até";
$MESS["CTRL_MEMB_ADMIN_FILTER_ANY"] = "(Qualquer)";
$MESS["CTRL_MEMB_ADMIN_FILTER_ANY2"] = "(Qualquer)";
$MESS["CTRL_MEMB_ADMIN_FILTER_CREATED"] = "Criado";
$MESS["CTRL_MEMB_ADMIN_FILTER_DISCONN"] = "Desabilitado";
$MESS["CTRL_MEMB_ADMIN_FILTER_DISCONNECTED"] = "Desconectado";
$MESS["CTRL_MEMB_ADMIN_FILTER_GROUP"] = "Grupo";
$MESS["CTRL_MEMB_ADMIN_FILTER_MODIFIED"] = "Modificado";
$MESS["CTRL_MEMB_ADMIN_FILTER_UNIQID"] = "ID Única";
$MESS["CTRL_MEMB_ADMIN_FILTER_URL"] = "URL do site";
$MESS["CTRL_MEMB_ADMIN_MENU_DEL"] = "Excluir";
$MESS["CTRL_MEMB_ADMIN_MENU_DEL_ALERT"] = "O site será excluído da lista, no entanto, isso não irá remover todas as restrições site cliente. O site será desligado do controlador para ser removido correctamente. Continuar?";
$MESS["CTRL_MEMB_ADMIN_MENU_DISC"] = "Desconectar";
$MESS["CTRL_MEMB_ADMIN_MENU_DISC_CONFIRM"] = "Isto irá desconectar o site do controlador. Administração remota do site não estará mais disponível. Continuar?";
$MESS["CTRL_MEMB_ADMIN_MENU_EDIT"] = "Editar";
$MESS["CTRL_MEMB_ADMIN_MENU_GOADMIN"] = "Entrar como administrador";
$MESS["CTRL_MEMB_ADMIN_MENU_LOG"] = "Registro";
$MESS["CTRL_MEMB_ADMIN_MENU_RUNPHP"] = "Executar PHP";
$MESS["CTRL_MEMB_ADMIN_MENU_UPD"] = "Atualizar site";
$MESS["CTRL_MEMB_ADMIN_MENU_UPDCNT"] = "Atualizar contadores";
$MESS["CTRL_MEMB_ADMIN_MENU_UPDSETT"] = "Atualizar configurações";
$MESS["CTRL_MEMB_ADMIN_NAVSTRING"] = "Clientes do controlador";
$MESS["CTRL_MEMB_ADMIN_SAVE_ERR"] = "Erro ao atualizar os parâmetros do site";
$MESS["CTRL_MEMB_ADMIN_TITLE"] = "Gerenciar sites clientes";
$MESS["CTRL_MEMB_ADMIN_UPDCNT_ERR"] = "Erro ao atualizar contador do cliente";
$MESS["CTRL_MEMB_ADMIN_UPDSET_ERR"] = "Erro ao atualizar parâmetros do cliente";
$MESS["CTRL_MEMB_ADMIN_UPD_ERR"] = "Erro ao atualizar o cliente";
?>