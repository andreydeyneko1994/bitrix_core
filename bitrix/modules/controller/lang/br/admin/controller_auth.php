<?
$MESS["CTRLR_AUTH_CS_LABEL"] = "Permitir que a autorização de usuários controladores";
$MESS["CTRLR_AUTH_CS_TAB_TITLE"] = "Configurar a autenticação do usuário do controlador em sites clientes";
$MESS["CTRLR_AUTH_LOC_GROUPS"] = "Mapear grupos";
$MESS["CTRLR_AUTH_NOTE"] = "Administradores do controlador sempre autorizam em sites cliente com permissões máximas";
$MESS["CTRLR_AUTH_SC_LABEL"] = "Permitir autenticação colateral do controlador";
$MESS["CTRLR_AUTH_SC_TAB_TITLE"] = "Configurar a auenticação de usuário no site cliente no controlador";
$MESS["CTRLR_AUTH_SS_LABEL"] = "Permitir autorização transparente";
$MESS["CTRLR_AUTH_SS_TAB_TITLE"] = "Configurar a autenticação pass-through através de sites clientes";
$MESS["CTRLR_AUTH_TITLE"] = "Autorização";
?>