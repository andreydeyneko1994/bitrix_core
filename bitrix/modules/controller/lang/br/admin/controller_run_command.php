<?
$MESS["CTRLR_RUN_ADD_TASK"] = "Executar comando pelo agendador de tarefas";
$MESS["CTRLR_RUN_ADD_TASK_LABEL"] = "Adicionar comando para a fila de tarefas";
$MESS["CTRLR_RUN_BUTT_CLEAR"] = "Limpar";
$MESS["CTRLR_RUN_BUTT_RUN"] = "Executar";
$MESS["CTRLR_RUN_COMMAND_FIELD"] = "Script PHP";
$MESS["CTRLR_RUN_COMMAND_TAB_TITLE"] = "Script PHP para execução remota";
$MESS["CTRLR_RUN_CONFIRM"] = "O comando inserido será executado no servidor remoto como um script PHP. Continuar?";
$MESS["CTRLR_RUN_ERR_NSELECTED"] = "Selecione um cliente para execução";
$MESS["CTRLR_RUN_FILTER_GROUP"] = "Grupo";
$MESS["CTRLR_RUN_FILTER_GROUP_ANY"] = "(Qualquer)";
$MESS["CTRLR_RUN_FILTER_SITE"] = "Site";
$MESS["CTRLR_RUN_FILTER_SITE_ALL"] = "(Todos)";
$MESS["CTRLR_RUN_SUCCESS"] = "#SUCCESS_CNT# de #CNT# comandos remotos foram adicionados com sucesso para a <a href=\"controller_task.php?lang=#LANG#\">fila de tarefas</a>.";
$MESS["CTRLR_RUN_TITLE"] = "Linha de comando PHP remoto";
?>