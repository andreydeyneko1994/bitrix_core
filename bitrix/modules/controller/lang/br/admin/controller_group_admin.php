<?
$MESS["CTRLE_GR_AD_COUNTER_FREE"] = "Atualizar contador de espaço livre em disco";
$MESS["CTRLE_GR_AD_COUNTER_LAST_AU"] = "Atualizar contador da última autorização";
$MESS["CTRLE_GR_AD_COUNTER_SITES"] = "Atualizar contador do site";
$MESS["CTRLE_GR_AD_COUNTER_UPD_PER"] = "Intervalo de atualização do contador";
$MESS["CTRLE_GR_AD_COUNTER_USERS"] = "Atualizar contador de usuário";
$MESS["CTRLR_GR_AD_COL_CRE"] = "Criado";
$MESS["CTRLR_GR_AD_COL_CREBY"] = "Criador";
$MESS["CTRLR_GR_AD_COL_DESC"] = "Descrição";
$MESS["CTRLR_GR_AD_COL_MOD"] = "Modificado";
$MESS["CTRLR_GR_AD_COL_MODBY"] = "Modificado por";
$MESS["CTRLR_GR_AD_COL_NAME"] = "Nome";
$MESS["CTRLR_GR_AD_ERR1"] = "Erro ao atualizar os parâmetros do grupo";
$MESS["CTRLR_GR_AD_ERR2"] = "Erro ao excluir o grupo";
$MESS["CTRLR_GR_AD_FLT_ACT_FROM"] = "Ativo desde";
$MESS["CTRLR_GR_AD_FLT_ACT_TO"] = "Ativo até";
$MESS["CTRLR_GR_AD_FLT_CREAT"] = "Criado";
$MESS["CTRLR_GR_AD_FLT_MODIF"] = "Modificado";
$MESS["CTRLR_GR_AD_MENU_COPY"] = "Cópia";
$MESS["CTRLR_GR_AD_MENU_DEL"] = "Excluir";
$MESS["CTRLR_GR_AD_MENU_DEL_CONFIRM"] = "Isto irá excluir o grupo e todas suas configurações. Continuar?";
$MESS["CTRLR_GR_AD_MENU_EDIT"] = "Editar";
$MESS["CTRLR_GR_AD_NAV"] = "Grupos";
$MESS["CTRLR_GR_AD_TITLE"] = "Gerenciar grupos de sites";
?>