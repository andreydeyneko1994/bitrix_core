<?
$MESS["SUPC_HE_C_UPDATED"] = "Ajuda atualizados";
$MESS["SUPC_HE_UPD"] = "Ajuda atualização seção falhou";
$MESS["SUPC_LE_C_UPDATED"] = "Arquivos de idioma atualizados";
$MESS["SUPC_LE_UPD"] = "Atualização do arquivo de linguagem falhou";
$MESS["SUPC_ME_CHECK"] = "Erro na verificação de gravação do arquivo";
$MESS["SUPC_ME_C_UPDATED"] = "O núcleo comum atualizado";
$MESS["SUPC_ME_LOAD"] = "Não é possível baixar atualizações";
$MESS["SUPC_ME_PACK"] = "Não é possível extrair arquivos a partir do pacote";
$MESS["SUPC_ME_UPDATE"] = "Atualização do módulo falhou";
?>