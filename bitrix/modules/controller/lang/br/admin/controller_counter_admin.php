<?
$MESS["CTRL_CNT_ADMIN_COMMAND"] = "Comando";
$MESS["CTRL_CNT_ADMIN_COUNTER_FORMAT"] = "Formato";
$MESS["CTRL_CNT_ADMIN_COUNTER_TYPE"] = "Tipo de dados";
$MESS["CTRL_CNT_ADMIN_DELETE_ERROR"] = "Erro ao excluir o contador ##ID#: #ERROR#";
$MESS["CTRL_CNT_ADMIN_FILTER_ANY"] = "(Qualquer)";
$MESS["CTRL_CNT_ADMIN_FILTER_GROUP"] = "Grupo";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE"] = "Excluir";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE_ALERT"] = "O contador e todos os valores associados serão excluídos. Continuar?";
$MESS["CTRL_CNT_ADMIN_MENU_EDIT"] = "Editar";
$MESS["CTRL_CNT_ADMIN_NAME"] = "Nome";
$MESS["CTRL_CNT_ADMIN_NAV"] = "Contadores";
$MESS["CTRL_CNT_ADMIN_TITLE"] = "Contadores";
$MESS["CTRL_CNT_ADMIN_UPDATE_ERROR"] = "Erro ao atualizar o contador ##ID#: #ERROR#";
?>