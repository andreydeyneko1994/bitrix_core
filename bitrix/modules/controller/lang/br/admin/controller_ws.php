<?
$MESS["CTRLR_WS_ERR_BAD_COMMAND"] = "O comando necessário não foi encontrada ou expirou";
$MESS["CTRLR_WS_ERR_BAD_LEVEL"] = "Permissões insuficientes para adicionar um novo cliente";
$MESS["CTRLR_WS_ERR_BAD_OPERID"] = "ID da operação desconhecida:";
$MESS["CTRLR_WS_ERR_BAD_PASSW"] = "Senha incorreta.";
$MESS["CTRLR_WS_ERR_FILE_NOT_FOUND"] = "Arquivo não encontrado";
$MESS["CTRLR_WS_ERR_MEMB_DISCN"] = "Permissões insuficientes para desconectar um cliente";
$MESS["CTRLR_WS_ERR_MEMB_NFOUND"] = "O cliente não foi encontrado";
$MESS["CTRLR_WS_ERR_RUN"] = "Erro de execução:";
$MESS["CTRLR_WS_ERR_RUN_BACK"] = "Voltar";
$MESS["CTRLR_WS_ERR_RUN_TRY"] = "Por favor, tente novamente.";
?>