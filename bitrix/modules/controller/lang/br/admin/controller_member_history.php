<?
$MESS["CTRL_MEMB_HIST_ANY"] = "(Qualquer)";
$MESS["CTRL_MEMB_HIST_BACK"] = "Website";
$MESS["CTRL_MEMB_HIST_BACK_TITLE"] = "Voltar ao editor de website";
$MESS["CTRL_MEMB_HIST_CONTROLLER_GROUP_ID"] = "Grupo do site";
$MESS["CTRL_MEMB_HIST_CONTROLLER_MEMBER_ID"] = "ID do site";
$MESS["CTRL_MEMB_HIST_CREATED_DATE"] = "Tempo";
$MESS["CTRL_MEMB_HIST_FIELD"] = "Campo alterado";
$MESS["CTRL_MEMB_HIST_FROM_VALUE"] = "Valor inicial";
$MESS["CTRL_MEMB_HIST_NAVSTRING"] = "Registro";
$MESS["CTRL_MEMB_HIST_NOTES"] = "Notas";
$MESS["CTRL_MEMB_HIST_SITE_ACTIVE"] = "Ativo";
$MESS["CTRL_MEMB_HIST_TITLE"] = "Histórico do website";
$MESS["CTRL_MEMB_HIST_TO_VALUE"] = "Novo valor";
$MESS["CTRL_MEMB_HIST_USER_ID"] = "Usuário";
?>