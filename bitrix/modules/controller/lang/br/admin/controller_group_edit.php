<?
$MESS["CTRLR_GR_ED_AUTOUPD"] = "Configurações de atualização automática do intervalo, minutos:";
$MESS["CTRLR_GR_ED_AUTOUPD_HELP"] = "(0 - não atualizar, vazio - usar valor padrão de configurações do módulo)";
$MESS["CTRLR_GR_ED_CHCKBOX"] = "Substituir as configurações do site cliente";
$MESS["CTRLR_GR_ED_CREATED"] = "Criado:";
$MESS["CTRLR_GR_ED_DESC"] = "Descrição:";
$MESS["CTRLR_GR_ED_ER1"] = "Ocorreu um erro ao salvar";
$MESS["CTRLR_GR_ED_GROUP"] = "Grupo (string id)";
$MESS["CTRLR_GR_ED_GROUP_MORE"] = "Outro grupo ...";
$MESS["CTRLR_GR_ED_INSTALLED"] = "Módulos instalados:";
$MESS["CTRLR_GR_ED_LEVEL"] = "Permissão de acesso";
$MESS["CTRLR_GR_ED_LEVEL_DEF"] = "(Padrão)";
$MESS["CTRLR_GR_ED_LINK_BACK"] = "Voltar a grupos";
$MESS["CTRLR_GR_ED_LINK_COPY"] = "Cópia";
$MESS["CTRLR_GR_ED_LINK_DEL"] = "Excluir atual";
$MESS["CTRLR_GR_ED_LINK_DEL_CONFIRM"] = "Você não será capaz de restaurar o grupo depois de excluir. Continuar?";
$MESS["CTRLR_GR_ED_LINK_NEW"] = "Novo grupo";
$MESS["CTRLR_GR_ED_MODIFIED"] = "Modificado:";
$MESS["CTRLR_GR_ED_MODULE"] = "Módulo";
$MESS["CTRLR_GR_ED_NAME"] = "Nome:";
$MESS["CTRLR_GR_ED_NOADMIN"] = "Privar os administradores de acesso completo:";
$MESS["CTRLR_GR_ED_PERM"] = "Permissões de grupos locais em sites clientes:";
$MESS["CTRLR_GR_ED_PHP_INST"] = "Executar o script PHP ao definir esta política de grupo:";
$MESS["CTRLR_GR_ED_PHP_UNINST"] = "Executar o script PHP quando ao revogar essa política de grupo:";
$MESS["CTRLR_GR_ED_SUBORD"] = "Grupos de usuários cujos perfis serão atualizados";
$MESS["CTRLR_GR_ED_TAB1"] = "Parâmetros";
$MESS["CTRLR_GR_ED_TAB2"] = "Módulos";
$MESS["CTRLR_GR_ED_TAB2_TITLE"] = "Módulos instalados";
$MESS["CTRLR_GR_ED_TAB3"] = "Configurações do módulo";
$MESS["CTRLR_GR_ED_TAB4"] = "Permissões de acesso";
$MESS["CTRLR_GR_ED_TAB4_TITLE"] = "Módulo de acesso permissões";
$MESS["CTRLR_GR_ED_TAB5"] = "Mais";
$MESS["CTRLR_GR_ED_TAB5_TITLE"] = "Instalação PHP/scripts de desinstalação";
$MESS["CTRLR_GR_ED_TITLE_1"] = "Editar #ID# do grupo";
$MESS["CTRLR_GR_ED_TITLE_2"] = "Novo Grupo";
$MESS["CTRLR_GR_ED_TO_TASKS"] = "Alistar configurações da tarefa de atualização";
$MESS["CTRLR_GR_ED_UPD"] = "Atualizar as configurações de sites neste grupo:";
$MESS["CTRLR_GR_ED_UPD_NOW"] = "Atualizar as configurações de sites deste grupo imediatamente após salvar:";
$MESS["CTRL_GR_ED_COUNTERS_FREE"] = "Espaço livre em disco:";
$MESS["CTRL_GR_ED_COUNTERS_LAST_AU"] = "Última autorização:";
$MESS["CTRL_GR_ED_COUNTERS_REF"] = "Atualizar contadores a cada:";
$MESS["CTRL_GR_ED_COUNTERS_REF_DA"] = "dias";
$MESS["CTRL_GR_ED_COUNTERS_REF_HO"] = "horas";
$MESS["CTRL_GR_ED_COUNTERS_REF_MI"] = "minutos";
$MESS["CTRL_GR_ED_COUNTERS_REF_MO"] = "meses";
$MESS["CTRL_GR_ED_COUNTERS_REF_WE"] = "semanas";
$MESS["CTRL_GR_ED_COUNTERS_SITES"] = "Sites:";
$MESS["CTRL_GR_ED_COUNTERS_TITLE"] = "Contadores:";
$MESS["CTRL_GR_ED_COUNTERS_USERS"] = "Usuários:";
$MESS["CTRL_GR_ED_TAB5"] = "Contadores";
$MESS["CTRL_GR_ED_TAB5_TITLE"] = "Contadores de performance";
?>