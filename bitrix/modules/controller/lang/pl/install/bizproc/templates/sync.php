<?
$MESS["BPT_SYNC_DESC"] = "Uruchom publikowanie elementów bloku informacji na kontrolowanych stronach. Dokument będzie w pierwszej kolejności publikowany na stronie kontrolera.";
$MESS["BPT_SYNC_NAME"] = "Publikowanie na kontrolowanych stronach";
$MESS["BPT_SYNC_PUBLISH"] = "Publikuj Dokument";
$MESS["BPT_SYNC_SEQ"] = "Sekwencyjny proces biznesowy";
$MESS["BPT_SYNC_SYNC"] = "Publikowanie na kontrolowanych stronach";
?>