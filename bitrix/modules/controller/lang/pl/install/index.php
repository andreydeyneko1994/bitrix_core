<?
$MESS["CTRL_INST_DESC"] = "Zdalne zarządzanie stroną";
$MESS["CTRL_INST_NAME"] = "Sterownik Strony";
$MESS["CTRL_INST_STEP1"] = "Sterownik Instalacji Modułu: Krok 1";
$MESS["CTRL_INST_STEP1_UN"] = "Sterownik Deinstalacji Modułu: Krok 1";
$MESS["CTRL_PERM_D"] = "Odmowa dostępu";
$MESS["CTRL_PERM_L"] = "Autoryzacja na Stronach";
$MESS["CTRL_PERM_R"] = "Tylko do Odczytu";
$MESS["CTRL_PERM_T"] = "Dodaj nową stronę";
$MESS["CTRL_PERM_V"] = "Zarządzaj stronami";
$MESS["CTRL_PERM_W"] = "Pełny dostęp";
?>