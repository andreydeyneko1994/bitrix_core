<?
$MESS["CTRLR_TASK_ERR_ALREADY"] = "To zadanie jest już przypisane do klienta";
$MESS["CTRLR_TASK_ERR_BAD_ID"] = "ID zadania nierozpoznane.";
$MESS["CTRLR_TASK_ERR_CLIENTID"] = "ID klienta nie może być puste.";
$MESS["CTRLR_TASK_ERR_ID"] = "ID zadania nie może być puste.";
$MESS["CTRLR_TASK_ERR_KERNEL"] = "Wspólny rdzeń stron powinien być aktualizowany z wykorzystaniem specjalnego formularza aktualizacyjnego.";
$MESS["CTRLR_TASK_STATUS_COMPL"] = "Pomyślnie zakończony";
$MESS["CTRLR_TASK_STATUS_FAIL"] = "Nieudane wykonanie zadania";
$MESS["CTRLR_TASK_STATUS_NEW"] = "Nowe";
$MESS["CTRLR_TASK_STATUS_PART"] = "Częściowo zakończony";
$MESS["CTRLR_TASK_TYPE_CLOSE_MEMBER"] = "Zamknij/Otwórz klienta";
$MESS["CTRLR_TASK_TYPE_COUNTERS_UPDATE"] = "Aktualizuj liczniki";
$MESS["CTRLR_TASK_TYPE_REMOTE_COMMAND"] = "Wykonaj komendę";
$MESS["CTRLR_TASK_TYPE_SET_SETTINGS"] = "Rozpowszechnij ustawienia grup";
$MESS["CTRLR_TASK_TYPE_UPDATE"] = "Aktualizuj stronę";
$MESS["CTRLR_TASK_UPD_COMPL"] = "Żadne aktualizacje nie są w tej chwili dostępne";
?>