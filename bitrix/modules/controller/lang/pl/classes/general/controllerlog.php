<?
$MESS["CTRLR_LOG_ERR_NAME"] = "Pole nazwy nie może być puste.";
$MESS["CTRLR_LOG_ERR_UID"] = "Pole ID klienta nie może być puste.";
$MESS["CTRLR_LOG_TYPE_AUTH"] = "Sterownik aktualizacji";
$MESS["CTRLR_LOG_TYPE_REGISTRATION"] = "Rejestracja nowego klienta";
$MESS["CTRLR_LOG_TYPE_REMOTE_COMMAND"] = "Zdalne Wykonanie Komend";
$MESS["CTRLR_LOG_TYPE_SET_SETTINGS"] = "Rozszerz ustawienia na stronę klienta";
$MESS["CTRLR_LOG_TYPE_SITE_CLOSE"] = "Zamknij/otwórz strony klienta";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE"] = "Instaluj aktualizacje";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL"] = "Aktualizuj wspólny rdzeń";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL_DB"] = "Aktualizuj bazę danych stron o wspólnym rdzeniu";
$MESS["CTRLR_LOG_TYPE_UNREGISTRATION"] = "Usuń klienta ze sterownika";
$MESS["CTRLR_LOG_TYPE_UPDATE_COUNTERS"] = "Aktualizacja licznika";
?>