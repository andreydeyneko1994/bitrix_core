<?
$MESS["CTRLR_MEM_COUNTERS_ERR1"] = "Błąd zapisu liczników:";
$MESS["CTRLR_MEM_ERR1"] = "Nie znaleziono określonego serwera:";
$MESS["CTRLR_MEM_ERR2"] = "Błąd wykonania komendy: nieznana odpowiedż lub odmowa dostępu.";
$MESS["CTRLR_MEM_ERR3"] = "Błąd wykonania komendy:";
$MESS["CTRLR_MEM_ERR4"] = "Nie znaleziono grupy:";
$MESS["CTRLR_MEM_ERR6"] = "Nie znaleziono klienta:";
$MESS["CTRLR_MEM_ERR7"] = "Klient został odłączony od sterownika. Nie można przerpowadzić żadnej operacji.";
$MESS["CTRLR_MEM_ERR_MEMBER_ID"] = "Unikalne ID nie może się zaczynać od cyfry. Proszę użyć innej wartości.";
$MESS["CTRLR_MEM_ERR_MEMBER_NAME"] = "Pole nazwy nie może być puste.";
$MESS["CTRLR_MEM_ERR_MEMBER_URL"] = "Pole URL nie może być puste.";
$MESS["CTRLR_MEM_LOG_DESC_COMMAND"] = "Wykonanie komendy przekierowania";
$MESS["CTRLR_MEM_LOG_DESC_GROUP"] = "Grupa:";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET"] = "Łączenie klienta z wykorzystaniem danych uwierzytelniających lokalnego administratora";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET2"] = "Łączenie klienta z wykorzytaniem tymczasowego uwierzytelnienia";
$MESS["CTRLR_MEM_LOG_DISCON"] = "Odłączanie klienta od sterownika";
$MESS["CTRLR_MEM_LOG_DISCON_ERR"] = "Błąd odłączenia.";
$MESS["CTRLR_MEM_LOG_SITE_CLO"] = "Strona klienta została zamknięta";
$MESS["CTRLR_MEM_LOG_SITE_OPE"] = "Strona klienta została otwarta";
?>