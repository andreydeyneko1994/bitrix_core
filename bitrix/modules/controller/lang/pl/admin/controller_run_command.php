<?
$MESS["CTRLR_RUN_ADD_TASK"] = "Wykonaj komendę przez listę zadań";
$MESS["CTRLR_RUN_ADD_TASK_LABEL"] = "Dodaj komendę do kolejki zadań";
$MESS["CTRLR_RUN_BUTT_CLEAR"] = "Wyczyść";
$MESS["CTRLR_RUN_BUTT_RUN"] = "Wykonaj";
$MESS["CTRLR_RUN_COMMAND_FIELD"] = "Skrypt PHP";
$MESS["CTRLR_RUN_COMMAND_TAB_TITLE"] = "Skrypt PHP dla zdalnego wykonania";
$MESS["CTRLR_RUN_CONFIRM"] = "Komenda wejścia zostanie wykonana na zdalnym serwerze jako skrypt PHP. Kontynuować?";
$MESS["CTRLR_RUN_ERR_NSELECTED"] = "Wybierz klienta do wykonania";
$MESS["CTRLR_RUN_FILTER_GROUP"] = "Grupa";
$MESS["CTRLR_RUN_FILTER_GROUP_ANY"] = "(jakikolwiek)";
$MESS["CTRLR_RUN_FILTER_SITE"] = "Strona";
$MESS["CTRLR_RUN_FILTER_SITE_ALL"] = "(Wszystko)";
$MESS["CTRLR_RUN_TITLE"] = "Zdalne linie komend PHP";
?>