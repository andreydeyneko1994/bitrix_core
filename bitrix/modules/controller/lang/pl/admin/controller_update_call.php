<?
$MESS["SUPC_HE_C_UPDATED"] = "Sekcja pomocy zaktualizowana";
$MESS["SUPC_HE_UPD"] = "Nie udała się aktualizacja sekcji pomocy";
$MESS["SUPC_LE_C_UPDATED"] = "Pliki językowe zaktualizowane";
$MESS["SUPC_LE_UPD"] = "Nie udała się aktualizacja plików językowych";
$MESS["SUPC_ME_CHECK"] = "Błąd weryfikacji zapisu pliku";
$MESS["SUPC_ME_C_UPDATED"] = "Wspólny rdzeń zaktualizowany";
$MESS["SUPC_ME_LOAD"] = "Nie można pobrać aktualizacji";
$MESS["SUPC_ME_PACK"] = "Nie można wyodrębnić plików z archiwum";
$MESS["SUPC_ME_UPDATE"] = "Nieudana aktualizacja modułu";
?>