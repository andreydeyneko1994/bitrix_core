<?
$MESS["SUPA_ACE_ACT"] = "Błąd próby aktywacji kuponu";
$MESS["SUPA_ACE_CPN"] = "Kod kuponu nie został dostarczony";
$MESS["SUPA_AERR_CONTACT_EMAIL"] = "Proszę wpisać adres e-mail osoby kontaktowej";
$MESS["SUPA_AERR_CONTACT_EMAIL1"] = "Proszę sprawdzić czy adres e-mail osoby kontaktowej jest poprawny";
$MESS["SUPA_AERR_CONTACT_PERSON"] = "Proszę wpisać imię i nazwisko osoby kontaktowej";
$MESS["SUPA_AERR_CONTACT_PHONE"] = "Proszę wpisać numer telefonu osoby kontaktowej";
$MESS["SUPA_AERR_EMAIL"] = "Proszę wprowadzić e-mail kontaktowy";
$MESS["SUPA_AERR_EMAIL1"] = "Proszę sprawdzić poprawność adresu e-mail";
$MESS["SUPA_AERR_NAME"] = "Proszę wprowadzić nazwę firmy, która posiada klucz";
$MESS["SUPA_AERR_PASSW_CONF"] = "Hasło i potwierdzenie hasła są różne.";
$MESS["SUPA_AERR_PHONE"] = "Proszę wpisać numer telefonu właściciela kopii produktu";
$MESS["SUPA_AERR_URI"] = "Proszę wprowadzić adres strony, która będzie wykorzystywać klucz";
$MESS["SUPA_AME_EMAIL"] = "Brak adresu e-mail do subskrypcji";
$MESS["SUPA_AME_SUBSCR"] = "Błąd subskrypcji";
$MESS["SUPA_ASE_CHECK"] = "Błąd weryfikacji zapisu pliku";
$MESS["SUPA_ASE_NO_LIST"] = "Nie można odzyskać listy modułów";
$MESS["SUPA_ASE_PACK"] = "Nie można wyodrębnić plików z archiwum";
$MESS["SUPA_ASE_SOURCES"] = "Nie można pobrać kodu zabezpieczającego";
$MESS["SUPA_ASE_UPD"] = "Nieudana aktualizacja modułu";
$MESS["SUPA_ASTE_FLAG"] = "Nie można określić trybu instalacji wersji beta";
$MESS["SUPA_AUE_UPD"] = "Wystąpił błąd w trakcie próby aktualizacji StronyAktualizacji";
$MESS["SUP_CANT_OPEN_FILE"] = "Nie można otworzyć pliku do zapisu";
$MESS["SUP_ENTER_CORRECT_KEY"] = "Proszę wprowadzić poprawny klucz licencji";
$MESS["SUP_ENTER_KEY"] = "Proszę wprowadzić swój kod licencji";
?>