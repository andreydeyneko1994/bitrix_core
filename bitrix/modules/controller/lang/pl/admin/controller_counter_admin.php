<?
$MESS["CTRL_CNT_ADMIN_COMMAND"] = "Komenda";
$MESS["CTRL_CNT_ADMIN_COUNTER_FORMAT"] = "Format";
$MESS["CTRL_CNT_ADMIN_COUNTER_TYPE"] = "Rodzaj danych";
$MESS["CTRL_CNT_ADMIN_DELETE_ERROR"] = "Błąd usunięcia licznika ##ID#: #ERROR#";
$MESS["CTRL_CNT_ADMIN_FILTER_ANY"] = "(jakikolwiek)";
$MESS["CTRL_CNT_ADMIN_FILTER_GROUP"] = "Grupa";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE"] = "Usuń";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE_ALERT"] = "Licznik i wszystkie powiązane wartości zostaną usunięte. Kontynuować?";
$MESS["CTRL_CNT_ADMIN_MENU_EDIT"] = "Edytuj";
$MESS["CTRL_CNT_ADMIN_NAME"] = "Nazwa";
$MESS["CTRL_CNT_ADMIN_NAV"] = "Liczniki";
$MESS["CTRL_CNT_ADMIN_TITLE"] = "Liczniki";
$MESS["CTRL_CNT_ADMIN_UPDATE_ERROR"] = "Błąd aktualizacji licznika ##ID#: #ERROR#";
?>