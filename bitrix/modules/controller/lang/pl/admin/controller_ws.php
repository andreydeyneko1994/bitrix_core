<?
$MESS["CTRLR_WS_ERR_BAD_COMMAND"] = "Wymagana komenda nie została znaleziona lub wygasła";
$MESS["CTRLR_WS_ERR_BAD_LEVEL"] = "Niewystarczające uprawnienia na dodatnie nowego klienta";
$MESS["CTRLR_WS_ERR_BAD_OPERID"] = "Nieznana operacja ID:";
$MESS["CTRLR_WS_ERR_BAD_PASSW"] = "Niepoprawne hasło.";
$MESS["CTRLR_WS_ERR_FILE_NOT_FOUND"] = "Nie znaleziono pliku";
$MESS["CTRLR_WS_ERR_MEMB_DISCN"] = "Niewsytarczające uprawnienia do rozłączenia klienta";
$MESS["CTRLR_WS_ERR_MEMB_NFOUND"] = "Nie znaleziono klienta";
$MESS["CTRLR_WS_ERR_RUN"] = "Błąd wykonania:";
$MESS["CTRLR_WS_ERR_RUN_BACK"] = "Wstecz";
$MESS["CTRLR_WS_ERR_RUN_TRY"] = "Proszę spróbować ponownie.";
?>