<?
$MESS["CTRLR_MENU_AUTH"] = "Uwierzytelnienie";
$MESS["CTRLR_MENU_COUNTERS"] = "Liczniki";
$MESS["CTRLR_MENU_COUNTERS_TITLE"] = "Konfiguruj liczniki";
$MESS["CTRLR_MENU_GROUP_NAME"] = "Grupy";
$MESS["CTRLR_MENU_GROUP_TYPE"] = "Zarządzaj ustawieniami strony grupy";
$MESS["CTRLR_MENU_LOG_NAME"] = "Dziennik";
$MESS["CTRLR_MENU_LOG_TITLE"] = "Dziennik sterownika";
$MESS["CTRLR_MENU_NAME"] = "Sterownik";
$MESS["CTRLR_MENU_RUN_NAME"] = "Zdalne Wykonanie Komend";
$MESS["CTRLR_MENU_RUN_TITLE"] = "Wykonaj komendy PHP na zdalnych stronach";
$MESS["CTRLR_MENU_SITE_NAME"] = "Strony";
$MESS["CTRLR_MENU_SITE_TITLE"] = "Zarządzanie Stronami Klienta";
$MESS["CTRLR_MENU_TASK_NAME"] = "Zadania";
$MESS["CTRLR_MENU_TASK_TITLE"] = "Zadania do zdalnego wykonania na stronie klienta";
$MESS["CTRLR_MENU_TITLE"] = "Sterownik Strony";
$MESS["CTRLR_MENU_UPD_NAME"] = "strona aktualizacji";
$MESS["CTRLR_MENU_UPD_TYPE"] = "Aktualizuj stronę klienta";
$MESS["CTRLR_MENU_UPLOAD_NAME"] = "Dodaj plik";
$MESS["CTRLR_MENU_UPLOAD_TITLE"] = "Wyślij plik do strony klienta";
?>