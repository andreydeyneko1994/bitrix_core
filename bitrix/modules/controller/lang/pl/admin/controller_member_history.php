<?
$MESS["CTRL_MEMB_HIST_ANY"] = "(jakikolwiek)";
$MESS["CTRL_MEMB_HIST_BACK"] = "Strona internetowa";
$MESS["CTRL_MEMB_HIST_BACK_TITLE"] = "Powrót do edytora strony internetowej";
$MESS["CTRL_MEMB_HIST_CONTROLLER_GROUP_ID"] = "Grupa strony internetowej";
$MESS["CTRL_MEMB_HIST_CONTROLLER_MEMBER_ID"] = "ID Strony internetowej";
$MESS["CTRL_MEMB_HIST_CREATED_DATE"] = "Czas";
$MESS["CTRL_MEMB_HIST_FIELD"] = "Zmienione pole";
$MESS["CTRL_MEMB_HIST_FROM_VALUE"] = "Wartość początkowa";
$MESS["CTRL_MEMB_HIST_NAVSTRING"] = "Zapis";
$MESS["CTRL_MEMB_HIST_NOTES"] = "Notatki";
$MESS["CTRL_MEMB_HIST_SITE_ACTIVE"] = "Aktywne";
$MESS["CTRL_MEMB_HIST_TITLE"] = "Historia strony internetowej";
$MESS["CTRL_MEMB_HIST_TO_VALUE"] = "Nowa wartość";
$MESS["CTRL_MEMB_HIST_USER_ID"] = "Użytkownik";
?>