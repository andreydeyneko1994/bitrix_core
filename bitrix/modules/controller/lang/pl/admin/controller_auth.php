<?
$MESS["CTRLR_AUTH_CS_LABEL"] = "Zezwól na autoryzację od użytkowników sterownika";
$MESS["CTRLR_AUTH_CS_TAB_TITLE"] = "Konfiguruj sterownik uwierzytelnienia użytkownika na stronie internetowej klienta";
$MESS["CTRLR_AUTH_LOC_GROUPS"] = "Mapa grup";
$MESS["CTRLR_AUTH_NOTE"] = "Sterownik administratora zawsze dokonuje autoryzacji na stronie klienta strony ze wszystkimi uprawnieniami";
$MESS["CTRLR_AUTH_SC_LABEL"] = "Pozwól sterownikowi na osdunięcie uwierzytelnienia";
$MESS["CTRLR_AUTH_SC_TAB_TITLE"] = "Konfiguruj uwierzytelnienie użytkownika strony internetowej klienta w sterowniku";
$MESS["CTRLR_AUTH_SS_LABEL"] = "Zezwól na przejrzystą autoryzację";
$MESS["CTRLR_AUTH_SS_TAB_TITLE"] = "Konfiguruj uwierzytelnienie przejścia przez stronę internetową kilienta";
$MESS["CTRLR_AUTH_TITLE"] = "Autoryzacja";
?>