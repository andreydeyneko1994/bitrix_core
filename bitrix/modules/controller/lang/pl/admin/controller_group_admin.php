<?
$MESS["CTRLE_GR_AD_COUNTER_FREE"] = "Aktualizauj licznik wolnej przestrzeni na dysku";
$MESS["CTRLE_GR_AD_COUNTER_LAST_AU"] = "Aktualizuj licznik ostatnich autoryzacji";
$MESS["CTRLE_GR_AD_COUNTER_SITES"] = "Aktualizuj licznik strony";
$MESS["CTRLE_GR_AD_COUNTER_UPD_PER"] = "Licznik odstępów aktualizacji";
$MESS["CTRLE_GR_AD_COUNTER_USERS"] = "Aktualizuj licznik użytkowników";
$MESS["CTRLR_GR_AD_COL_CRE"] = "Utworzony";
$MESS["CTRLR_GR_AD_COL_CREBY"] = "Twórca";
$MESS["CTRLR_GR_AD_COL_DESC"] = "Opis";
$MESS["CTRLR_GR_AD_COL_MOD"] = "Zmodyfikowany";
$MESS["CTRLR_GR_AD_COL_MODBY"] = "Zmodyfikowane przez";
$MESS["CTRLR_GR_AD_COL_NAME"] = "Nazwa";
$MESS["CTRLR_GR_AD_ERR1"] = "Błąd aktualizacji parametrów grupy";
$MESS["CTRLR_GR_AD_ERR2"] = "Błąd usuwania grupy";
$MESS["CTRLR_GR_AD_FLT_ACT_FROM"] = "Aktywne od";
$MESS["CTRLR_GR_AD_FLT_ACT_TO"] = "Aktywny do";
$MESS["CTRLR_GR_AD_FLT_CREAT"] = "Utworzony";
$MESS["CTRLR_GR_AD_FLT_MODIF"] = "Zmodyfikowany";
$MESS["CTRLR_GR_AD_MENU_COPY"] = "Kopiuj";
$MESS["CTRLR_GR_AD_MENU_DEL"] = "Usuń";
$MESS["CTRLR_GR_AD_MENU_DEL_CONFIRM"] = "Spowoduje to usunięcie grup wraz z ich ustawieniami. Kontynuować?";
$MESS["CTRLR_GR_AD_MENU_EDIT"] = "Edytuj";
$MESS["CTRLR_GR_AD_NAV"] = "Grupy";
$MESS["CTRLR_GR_AD_TITLE"] = "Zarządzaj Stroną Grup";
?>