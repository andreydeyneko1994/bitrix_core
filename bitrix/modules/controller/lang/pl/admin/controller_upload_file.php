<?
$MESS["CTRLR_UPLOAD_BUTT_CLEAR"] = "Wyczyść";
$MESS["CTRLR_UPLOAD_BUTT_RUN"] = "Załaduj";
$MESS["CTRLR_UPLOAD_CONFIRM"] = "Plik zostanie wysłany do wybranych stron. Kontynuować?";
$MESS["CTRLR_UPLOAD_ERROR_NO_PATH_TO"] = "Ścieżka miejsca docelowego nie została określona.";
$MESS["CTRLR_UPLOAD_ERR_FILE"] = "Nie znaleziono określonego pliku na serwerze.";
$MESS["CTRLR_UPLOAD_ERR_NSELECTED"] = "Żaden klient nie został wybrany do przesłania mu pliku.";
$MESS["CTRLR_UPLOAD_ERR_PACK"] = "Błąd przygotowania pliku do przeniesienia.";
$MESS["CTRLR_UPLOAD_FILE_TAB"] = "Plik";
$MESS["CTRLR_UPLOAD_FILE_TAB_TITLE"] = "Wybierz plik do wysłania na stronę klienta.";
$MESS["CTRLR_UPLOAD_FILTER_GROUP"] = "Grupa";
$MESS["CTRLR_UPLOAD_FILTER_GROUP_ANY"] = "(jakikolwiek)";
$MESS["CTRLR_UPLOAD_FILTER_SITE"] = "Strona";
$MESS["CTRLR_UPLOAD_FILTER_SITE_ALL"] = "(Wszystko)";
$MESS["CTRLR_UPLOAD_OPEN_FILE_BUTTON"] = "Otwórz...";
$MESS["CTRLR_UPLOAD_SEND_FILE_FROM"] = "Od:";
$MESS["CTRLR_UPLOAD_SEND_FILE_TO"] = "Do:";
$MESS["CTRLR_UPLOAD_TITLE"] = "Załaduj Pliki Na Stronę Klienta";
?>