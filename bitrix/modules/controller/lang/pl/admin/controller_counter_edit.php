<?
$MESS["CTRL_CNT_EDIT_TITLE"] = "Edytuj licznik ##ID#";
$MESS["CTRL_CNT_EDIT_TITLE_NEW"] = "Nowy licznik";
$MESS["CTRL_COUNTER_EDIT_COMMAND"] = "Komenda";
$MESS["CTRL_COUNTER_EDIT_CONTROLLER_GROUP"] = "Grupy";
$MESS["CTRL_COUNTER_EDIT_COUNTER_FORMAT"] = "Format";
$MESS["CTRL_COUNTER_EDIT_COUNTER_TYPE"] = "Rodzaj";
$MESS["CTRL_COUNTER_EDIT_ERROR"] = "Wystąpił błąd podczas zapisywania obiektu.";
$MESS["CTRL_COUNTER_EDIT_NAME"] = "Nazwa";
$MESS["CTRL_COUNTER_EDIT_TAB1"] = "Licznik";
$MESS["CTRL_COUNTER_EDIT_TAB1_TITLE"] = "Parametry licznika";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_DELETE"] = "Usuń";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć ten licznik (spowoduje to również usunięcie wszystkich zebranych danych)?";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_LIST"] = "Lista";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_NEW"] = "Nowe";
?>