<?
$MESS["CTRLR_WS_ERR_BAD_COMMAND"] = "L'instruction demandée n'a pas été trouvée ou son délai de validité a expiré";
$MESS["CTRLR_WS_ERR_BAD_LEVEL"] = "Il n'y a pas de droits d'accès pour ajouter un nouveau client";
$MESS["CTRLR_WS_ERR_BAD_OPERID"] = "Identificateur d'opération inconnu : ";
$MESS["CTRLR_WS_ERR_BAD_PASSW"] = "La quantité de produit incorrecte spécifiée pour bundle.";
$MESS["CTRLR_WS_ERR_FILE_NOT_FOUND"] = "Fichier introuvable";
$MESS["CTRLR_WS_ERR_MEMB_DISCN"] = "Manque de droit d'accès à une déconnexion du client";
$MESS["CTRLR_WS_ERR_MEMB_NFOUND"] = "Le client ne peut être trouvé sur le contrôleur";
$MESS["CTRLR_WS_ERR_RUN"] = "Erreur d'exécution : ";
$MESS["CTRLR_WS_ERR_RUN_BACK"] = "Précédent";
$MESS["CTRLR_WS_ERR_RUN_TRY"] = "Veuillez réessayer s'il vous plaît.";
?>