<?
$MESS["CTRLR_RUN_ADD_TASK"] = "Exécuter la commande via l'éditeur de tâches";
$MESS["CTRLR_RUN_ADD_TASK_LABEL"] = "Ajouter une commande dans la liste des tâches";
$MESS["CTRLR_RUN_BUTT_CLEAR"] = "Annuler";
$MESS["CTRLR_RUN_BUTT_RUN"] = "Exécuter";
$MESS["CTRLR_RUN_COMMAND_FIELD"] = "Script PHP";
$MESS["CTRLR_RUN_COMMAND_TAB_TITLE"] = "Script PHP arbitraire pour l'exécution sur un serveur distant";
$MESS["CTRLR_RUN_CONFIRM"] = "La commande sera exécutée sur le serveur distant comme un script PHP. Voulez-vous continuer ?";
$MESS["CTRLR_RUN_ERR_NSELECTED"] = "Veuillez choisir un client pour l'exécution";
$MESS["CTRLR_RUN_ERR_TOO_MANY_SELECTED"] = "La commande sera exécutée dans un très grand nombre de sites connectés. Cochez la case correspondante si vous êtes sûr de ce que vous faites.";
$MESS["CTRLR_RUN_FILTER_GROUP"] = "Groupe";
$MESS["CTRLR_RUN_FILTER_GROUP_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRLR_RUN_FILTER_SITE"] = "Site";
$MESS["CTRLR_RUN_FILTER_SITE_ALL"] = "(partout)";
$MESS["CTRLR_RUN_FORCE_RUN"] = "Exécuter une commande dans n'importe quel nombre de sites";
$MESS["CTRLR_RUN_SUCCESS"] = "#SUCCESS_CNT# des commandes #CNT# effacées ont été ajoutées avec succès à la<a href='controller_task.php?lang=#LANG#'>liste d'attente d'exécution des tâches</a>.";
$MESS["CTRLR_RUN_TITLE"] = "La ligne de commande PHP supprimée";
?>