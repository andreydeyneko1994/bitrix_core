<?
$MESS["SUPC_HE_C_UPDATED"] = "Mise à jour du système d'assistance";
$MESS["SUPC_HE_UPD"] = "Erreur de renouvellement de l'assistance";
$MESS["SUPC_LE_C_UPDATED"] = "Mise à jour de fichiers de langue";
$MESS["SUPC_LE_UPD"] = "Échec de la mise à jour des langues";
$MESS["SUPC_ME_CHECK"] = "Erreur de la vérification de l'inscription";
$MESS["SUPC_ME_C_UPDATED"] = "Le noyau commun est rafraîchi";
$MESS["SUPC_ME_LOAD"] = "Impossible de télécharger les mises à jour";
$MESS["SUPC_ME_PACK"] = "Erreur d'extraction du paquet";
$MESS["SUPC_ME_UPDATE"] = "Échec de la mise à jour des modules";
?>