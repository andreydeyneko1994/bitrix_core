<?
$MESS["CTRLR_MENU_AUTH"] = "Autorisation";
$MESS["CTRLR_MENU_COUNTERS"] = "Compteurs";
$MESS["CTRLR_MENU_COUNTERS_TITLE"] = "Réglage des compteurs";
$MESS["CTRLR_MENU_GROUP_NAME"] = "Groupes";
$MESS["CTRLR_MENU_GROUP_TYPE"] = "Gérer les sites dans le système";
$MESS["CTRLR_MENU_LOG_NAME"] = "Journal";
$MESS["CTRLR_MENU_LOG_TITLE"] = "Journal de fonctionnement du contrôleur";
$MESS["CTRLR_MENU_NAME"] = "Contrôleur";
$MESS["CTRLR_MENU_RUN_NAME"] = "Exécution à distance des instructions";
$MESS["CTRLR_MENU_RUN_TITLE"] = "Exécution des commandes PHP sur les sites subordonnés au contrôleur";
$MESS["CTRLR_MENU_SITE_NAME"] = "Le nombre de sites";
$MESS["CTRLR_MENU_SITE_TITLE"] = "Gestion des sites soumis";
$MESS["CTRLR_MENU_TASK_NAME"] = "Tâches";
$MESS["CTRLR_MENU_TASK_TITLE"] = "Tâches à effectuer sur des clients du Contrôleur";
$MESS["CTRLR_MENU_TITLE"] = "Contrôleur";
$MESS["CTRLR_MENU_UPD_NAME"] = "Mise à jour de sites";
$MESS["CTRLR_MENU_UPD_TYPE"] = "Mise à jour des sites subordonnés au contrôleur";
$MESS["CTRLR_MENU_UPLOAD_NAME"] = "Transmission du fichier";
$MESS["CTRLR_MENU_UPLOAD_TITLE"] = "Transfert d'un fichier vers les sites esclaves du contrôleur";
?>