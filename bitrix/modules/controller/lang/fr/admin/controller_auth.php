<?
$MESS["CTRLR_AUTH_CS_LABEL"] = "Autoriser l'authentification d'utilisateurs du contrôleur";
$MESS["CTRLR_AUTH_CS_TAB_TITLE"] = "Configuration de l'autorisation des utilisateurs du contrôleur sur les sites enfants";
$MESS["CTRLR_AUTH_LOC_GROUPS"] = "Concordance des groupes";
$MESS["CTRLR_AUTH_NOTE"] = "Les administrateurs du contrôleur peuvent toujours s'authentifier sur les sites enfants avec les droits les plus élevés";
$MESS["CTRLR_AUTH_SC_LABEL"] = "Permettre l'autorisation sur le contrôleur";
$MESS["CTRLR_AUTH_SC_TAB_TITLE"] = "Réglage de l'autorisation des utilisateurs des sites soumis au contrôleur";
$MESS["CTRLR_AUTH_SS_LABEL"] = "Autoriser le téléchargement";
$MESS["CTRLR_AUTH_SS_TAB_TITLE"] = "Réglage d'une autorisation de part en part des utilisateurs entre les sites subordonnés";
$MESS["CTRLR_AUTH_TITLE"] = "Autorisation";
?>