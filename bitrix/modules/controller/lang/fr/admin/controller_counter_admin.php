<?
$MESS["CTRL_CNT_ADMIN_COMMAND"] = "Commande";
$MESS["CTRL_CNT_ADMIN_COUNTER_FORMAT"] = "Affichage";
$MESS["CTRL_CNT_ADMIN_COUNTER_TYPE"] = "Type de données";
$MESS["CTRL_CNT_ADMIN_DELETE_ERROR"] = "Erreur de suppression du compteur ##ID# : #ERROR#";
$MESS["CTRL_CNT_ADMIN_FILTER_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRL_CNT_ADMIN_FILTER_GROUP"] = "Groupe";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE"] = "Supprimer";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE_ALERT"] = "Le compteur et toutes les valeurs précédemment recueillies seront supprimés. Continuer la suppression ?";
$MESS["CTRL_CNT_ADMIN_MENU_EDIT"] = "Éditer";
$MESS["CTRL_CNT_ADMIN_NAME"] = "Dénomination";
$MESS["CTRL_CNT_ADMIN_NAV"] = "Compteurs";
$MESS["CTRL_CNT_ADMIN_TITLE"] = "Compteurs";
$MESS["CTRL_CNT_ADMIN_UPDATE_ERROR"] = "Erreur de changement du compteur ##ID# : #ERROR#";
?>