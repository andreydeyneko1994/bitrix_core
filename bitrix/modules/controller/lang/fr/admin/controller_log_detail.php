<?
$MESS["CTRL_LOG_DETAIL_CLOSE"] = "Fermer";
$MESS["CTRL_LOG_DETAIL_CONTROLLER_MEMBER"] = "Client";
$MESS["CTRL_LOG_DETAIL_DESCRIPTION"] = "Description";
$MESS["CTRL_LOG_DETAIL_ID"] = "ID";
$MESS["CTRL_LOG_DETAIL_NAME"] = "Dénomination";
$MESS["CTRL_LOG_DETAIL_NOT_FOUND"] = "La note indiquée n'est pas trouvée.";
$MESS["CTRL_LOG_DETAIL_STATUS"] = "Statut";
$MESS["CTRL_LOG_DETAIL_STATUS_ERR"] = "Erreur";
$MESS["CTRL_LOG_DETAIL_STATUS_OK"] = "Avec succès";
$MESS["CTRL_LOG_DETAIL_TASK"] = "La tâche";
$MESS["CTRL_LOG_DETAIL_TIMESTAMP_X"] = "Créé";
$MESS["CTRL_LOG_DETAIL_TITLE"] = "Examen de la note du journal";
$MESS["CTRL_LOG_DETAIL_USER"] = "Utilisateur";
?>