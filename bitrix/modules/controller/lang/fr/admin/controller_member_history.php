<?
$MESS["CTRL_MEMB_HIST_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRL_MEMB_HIST_BACK"] = "Site web";
$MESS["CTRL_MEMB_HIST_BACK_TITLE"] = "Revenir à l'édition du site";
$MESS["CTRL_MEMB_HIST_CONTROLLER_GROUP_ID"] = "Groupe du site";
$MESS["CTRL_MEMB_HIST_CONTROLLER_MEMBER_ID"] = "ID du site";
$MESS["CTRL_MEMB_HIST_CREATED_DATE"] = "Temps";
$MESS["CTRL_MEMB_HIST_FIELD"] = "Champ modifié";
$MESS["CTRL_MEMB_HIST_FROM_VALUE"] = "Valeur initiale";
$MESS["CTRL_MEMB_HIST_NAVSTRING"] = "Enregistrement";
$MESS["CTRL_MEMB_HIST_NOTES"] = "Notes";
$MESS["CTRL_MEMB_HIST_SITE_ACTIVE"] = "Actif(ve)";
$MESS["CTRL_MEMB_HIST_TITLE"] = "Journal du site";
$MESS["CTRL_MEMB_HIST_TO_VALUE"] = "Nouvelle valeur";
$MESS["CTRL_MEMB_HIST_USER_ID"] = "Utilisateur";
?>