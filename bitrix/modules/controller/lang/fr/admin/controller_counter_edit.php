<?
$MESS["CTRL_CNT_EDIT_TITLE"] = "Edition du compteur ##ID#";
$MESS["CTRL_CNT_EDIT_TITLE_NEW"] = "Nouveau compteur";
$MESS["CTRL_COUNTER_EDIT_COMMAND"] = "Commande";
$MESS["CTRL_COUNTER_EDIT_COMMAND_TITLE"] = "Code de commande du compteur";
$MESS["CTRL_COUNTER_EDIT_CONTROLLER_GROUP"] = "Groupes";
$MESS["CTRL_COUNTER_EDIT_CONTROLLER_GROUP_TITLE"] = "Lier le compteur aux groupes de site";
$MESS["CTRL_COUNTER_EDIT_COUNTER_FORMAT"] = "Affichage";
$MESS["CTRL_COUNTER_EDIT_COUNTER_TYPE"] = "Entité";
$MESS["CTRL_COUNTER_EDIT_ERROR"] = "Erreur de sauvegarde.";
$MESS["CTRL_COUNTER_EDIT_NAME"] = "Dénomination";
$MESS["CTRL_COUNTER_EDIT_TAB1"] = "Compteur";
$MESS["CTRL_COUNTER_EDIT_TAB1_TITLE"] = "Les paramètres du compteur";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_DELETE"] = "Supprimer";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_DELETE_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce compteur (toutes les données collectées seront supprimées)?";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_LIST"] = "Liste";
$MESS["CTRL_COUNTER_EDIT_TOOLBAR_NEW"] = "Créer";
?>