<?
$MESS["CTRL_LOG_ADMIN_COLUMN_CREATED"] = "Créé";
$MESS["CTRL_LOG_ADMIN_COLUMN_NAME"] = "Dénomination";
$MESS["CTRL_LOG_ADMIN_COLUMN_STATUS_ERR"] = "Erreur";
$MESS["CTRL_LOG_ADMIN_COLUMN_STATUS_OK"] = "Avec succès";
$MESS["CTRL_LOG_ADMIN_COLUMN_USER"] = "Utilisateur";
$MESS["CTRL_LOG_ADMIN_ERR_DELETE"] = "Erreur de suppression";
$MESS["CTRL_LOG_ADMIN_FILTER_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRL_LOG_ADMIN_FILTER_CLIENT"] = "Client";
$MESS["CTRL_LOG_ADMIN_FILTER_CREATED"] = "Créé";
$MESS["CTRL_LOG_ADMIN_FILTER_DESC"] = "Description";
$MESS["CTRL_LOG_ADMIN_FILTER_IDCLIENT"] = "ID du client";
$MESS["CTRL_LOG_ADMIN_FILTER_STATUS"] = "Statut";
$MESS["CTRL_LOG_ADMIN_FILTER_TASK"] = "La tâche";
$MESS["CTRL_LOG_ADMIN_FILTER_TASKID"] = "ID de la tâche";
$MESS["CTRL_LOG_ADMIN_MENU_DEL"] = "Supprimer";
$MESS["CTRL_LOG_ADMIN_MENU_DEL_CONFIRM"] = "La note sera supprimée de la liste. Continuer ?";
$MESS["CTRL_LOG_ADMIN_MENU_DETAIL"] = "Détails";
$MESS["CTRL_LOG_ADMIN_PAGETITLE"] = "Enregistrements du journal";
$MESS["CTRL_LOG_ADMIN_TITLE"] = "Journal de fonctionnement du contrôleur de sites";
?>