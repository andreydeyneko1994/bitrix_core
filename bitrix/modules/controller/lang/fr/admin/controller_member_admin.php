<?
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_DISC"] = "déconnecter du contrôleur";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPD"] = "mise à jour de sites";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPDCNT"] = "mettre à jour les valeurs de compteurs chez clients";
$MESS["CTRL_MEMB_ADMIN_ACTIONBAR_UPDSETT"] = "mettre à jour les paramètres du groupe de clients";
$MESS["CTRL_MEMB_ADMIN_COLUMN_ACTIVE"] = "Actif(ve)";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_FREE"] = "Espace libre, KB";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_LAST_AU"] = "Dernière autorisation";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_SITES"] = "Le nombre de sites";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_UPD"] = "Dernière mise à jour des compteurs";
$MESS["CTRL_MEMB_ADMIN_COLUMN_COUNTER_USERS"] = "Utilisateurs";
$MESS["CTRL_MEMB_ADMIN_COLUMN_CREATED"] = "Créé";
$MESS["CTRL_MEMB_ADMIN_COLUMN_CREATEDBY"] = "Créateur";
$MESS["CTRL_MEMB_ADMIN_COLUMN_HOSTNAME"] = "Nom de l'hôte";
$MESS["CTRL_MEMB_ADMIN_COLUMN_MODIFIED"] = "Modifié";
$MESS["CTRL_MEMB_ADMIN_COLUMN_MODIFIEDBY"] = "Modifié(e)s par";
$MESS["CTRL_MEMB_ADMIN_COLUMN_NAME"] = "Dénomination";
$MESS["CTRL_MEMB_ADMIN_COLUMN_NOTES"] = "Notes";
$MESS["CTRL_MEMB_ADMIN_COLUMN_SHARED_KERN"] = "Noyau commun";
$MESS["CTRL_MEMB_ADMIN_CONTACT_PERSON"] = "Personne de contact";
$MESS["CTRL_MEMB_ADMIN_DEL_ERR"] = "Échec de suppression du site";
$MESS["CTRL_MEMB_ADMIN_DISCON"] = "n'a pas été joint";
$MESS["CTRL_MEMB_ADMIN_DISC_ERR"] = "Erreur de désactivation du client";
$MESS["CTRL_MEMB_ADMIN_EMAIL"] = "Adresse électronique (E-mail)";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACTIVE"] = "Actif(ve)";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACT_FROM"] = "Actif(ve) du";
$MESS["CTRL_MEMB_ADMIN_FILTER_ACT_TO"] = "Actif(ve) jusqu'au";
$MESS["CTRL_MEMB_ADMIN_FILTER_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRL_MEMB_ADMIN_FILTER_ANY2"] = "(n'importe lesquel(el)s)";
$MESS["CTRL_MEMB_ADMIN_FILTER_CREATED"] = "Créé";
$MESS["CTRL_MEMB_ADMIN_FILTER_DISCONN"] = "Désactivé(e)s";
$MESS["CTRL_MEMB_ADMIN_FILTER_DISCONNECTED"] = "Déconnecté";
$MESS["CTRL_MEMB_ADMIN_FILTER_GROUP"] = "Groupe";
$MESS["CTRL_MEMB_ADMIN_FILTER_MODIFIED"] = "Modifié";
$MESS["CTRL_MEMB_ADMIN_FILTER_UNIQID"] = "Code unique";
$MESS["CTRL_MEMB_ADMIN_FILTER_URL"] = "Adresse du site";
$MESS["CTRL_MEMB_ADMIN_MENU_DEL"] = "Supprimer";
$MESS["CTRL_MEMB_ADMIN_MENU_DEL_ALERT"] = "Le site est éliminé de la liste, mais toutes restrictions ne seront pas annulées. Pour la suppression correcte le site sera précédemment déconnecté du contrôleur. Continuer la suppression ?";
$MESS["CTRL_MEMB_ADMIN_MENU_DISC"] = "Déconnecter";
$MESS["CTRL_MEMB_ADMIN_MENU_DISC_CONFIRM"] = "Le site sera déconnecté du contrôleur et ne sera plus contrôlable à distance. Continuer ?";
$MESS["CTRL_MEMB_ADMIN_MENU_EDIT"] = "Éditer";
$MESS["CTRL_MEMB_ADMIN_MENU_GOADMIN"] = "Se connecter en tant qu'administrateur";
$MESS["CTRL_MEMB_ADMIN_MENU_LOG"] = "evènements";
$MESS["CTRL_MEMB_ADMIN_MENU_RUNPHP"] = "Exécuter PHP";
$MESS["CTRL_MEMB_ADMIN_MENU_UPD"] = "Mettre à jour le site";
$MESS["CTRL_MEMB_ADMIN_MENU_UPDCNT"] = "Mettre à jour les compteurs";
$MESS["CTRL_MEMB_ADMIN_MENU_UPDSETT"] = "Mettre à jour les réglages";
$MESS["CTRL_MEMB_ADMIN_NAVSTRING"] = "Clients du contrôleur";
$MESS["CTRL_MEMB_ADMIN_SAVE_ERR"] = "Erreur de sauvegarde des paramètres du site";
$MESS["CTRL_MEMB_ADMIN_TITLE"] = "Gestion des sites soumis";
$MESS["CTRL_MEMB_ADMIN_UPDCNT_ERR"] = "Échec de la mise à jour des compteurs chez le client";
$MESS["CTRL_MEMB_ADMIN_UPDSET_ERR"] = "Erreur de mise à jour des paramètres chez le client";
$MESS["CTRL_MEMB_ADMIN_UPD_ERR"] = "Erreur de mise à jour du client";
?>