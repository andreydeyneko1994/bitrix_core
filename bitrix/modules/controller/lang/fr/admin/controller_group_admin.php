<?
$MESS["CTRLE_GR_AD_COUNTER_FREE"] = "Rafraîchir le compteur de sa place";
$MESS["CTRLE_GR_AD_COUNTER_LAST_AU"] = "Rafraîchir le compteur de la dernière autorisation";
$MESS["CTRLE_GR_AD_COUNTER_SITES"] = "Rafraîchir le compteur des sites";
$MESS["CTRLE_GR_AD_COUNTER_UPD_PER"] = "Période de mise à jour de compteurs";
$MESS["CTRLE_GR_AD_COUNTER_USERS"] = "Rafraîchir le compteur des utilisateurs";
$MESS["CTRLR_GR_AD_COL_CRE"] = "Créé";
$MESS["CTRLR_GR_AD_COL_CREBY"] = "Créateur";
$MESS["CTRLR_GR_AD_COL_DESC"] = "Description";
$MESS["CTRLR_GR_AD_COL_MOD"] = "Modifié";
$MESS["CTRLR_GR_AD_COL_MODBY"] = "Modifié(e)s par";
$MESS["CTRLR_GR_AD_COL_NAME"] = "Dénomination";
$MESS["CTRLR_GR_AD_ERR1"] = "Erreur de la sauvegarde des paramètres du groupe";
$MESS["CTRLR_GR_AD_ERR2"] = "Impossible de supprimer le groupe";
$MESS["CTRLR_GR_AD_FLT_ACT_FROM"] = "Actif(ve) du";
$MESS["CTRLR_GR_AD_FLT_ACT_TO"] = "Actif(ve) jusqu'au";
$MESS["CTRLR_GR_AD_FLT_CREAT"] = "Créé";
$MESS["CTRLR_GR_AD_FLT_MODIF"] = "Modifié";
$MESS["CTRLR_GR_AD_MENU_COPY"] = "Copie";
$MESS["CTRLR_GR_AD_MENU_DEL"] = "Supprimer";
$MESS["CTRLR_GR_AD_MENU_DEL_CONFIRM"] = "Le groupe et tous ses paramètres seront supprimés. Continuer ?";
$MESS["CTRLR_GR_AD_MENU_EDIT"] = "Éditer";
$MESS["CTRLR_GR_AD_NAV"] = "Groupes";
$MESS["CTRLR_GR_AD_TITLE"] = "Gestion des Groupes des sites";
?>