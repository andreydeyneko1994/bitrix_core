<?
$MESS["CTRLR_UPLOAD_BUTT_CLEAR"] = "Annuler";
$MESS["CTRLR_UPLOAD_BUTT_RUN"] = "Chargement";
$MESS["CTRLR_UPLOAD_CONFIRM"] = "Le fichier sera transféré vers des sites sélectionnés. Continuer ?";
$MESS["CTRLR_UPLOAD_ERROR_NO_PATH_TO"] = "Un chemin pour copier le fichier n'est pas indiqué.";
$MESS["CTRLR_UPLOAD_ERR_FILE"] = "Le fichier indiqué n'est pas retrouvé sur le serveur.";
$MESS["CTRLR_UPLOAD_ERR_NSELECTED"] = "Aucun client sélectionné pour le transfert du fichier.";
$MESS["CTRLR_UPLOAD_ERR_PACK"] = "Erreur préparation du dossier pour le transfert.";
$MESS["CTRLR_UPLOAD_ERR_TOO_MANY_SELECTED"] = "Le fichier sera transféré vers un très grand nombre de sites connectés. Cochez la case correspondante si vous êtes sûr de ce que vous faites.";
$MESS["CTRLR_UPLOAD_FILE_TAB"] = "Fichier";
$MESS["CTRLR_UPLOAD_FILE_TAB_TITLE"] = "Sélectionnez un fichier pour le transfert aux sites connectés.";
$MESS["CTRLR_UPLOAD_FILTER_GROUP"] = "Groupe";
$MESS["CTRLR_UPLOAD_FILTER_GROUP_ANY"] = "(n'importe lesquel(el)s)";
$MESS["CTRLR_UPLOAD_FILTER_SITE"] = "Site";
$MESS["CTRLR_UPLOAD_FILTER_SITE_ALL"] = "(partout)";
$MESS["CTRLR_UPLOAD_FORCE_RUN"] = "Envoyer un fichier vers n'importe quel nombre de sites";
$MESS["CTRLR_UPLOAD_OPEN_FILE_BUTTON"] = "Ouvrir";
$MESS["CTRLR_UPLOAD_SEND_FILE_FROM"] = "De la part de : ";
$MESS["CTRLR_UPLOAD_SEND_FILE_TO"] = "A qui : ";
$MESS["CTRLR_UPLOAD_TITLE"] = "Transmission du fichier aux sites connectés";
?>