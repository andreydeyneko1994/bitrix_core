<?php
$MESS["SUP_ACTIVE"] = "Mises à jour disponibles : ";
$MESS["SUP_ACTIVE_PERIOD"] = "partir de #DATE_FROM# jusqu'à #DATE_TO#";
$MESS["SUP_APPLY"] = "Appliquer";
$MESS["SUP_CANT_CONNECT"] = "La connexion avec le serveur de mise à jour n'est pas réalisée";
$MESS["SUP_CHECK_UPDATES"] = "Vérifiez des mises à jour";
$MESS["SUP_EDITION"] = "Edition du produit : ";
$MESS["SUP_ERROR"] = "Erreur";
$MESS["SUP_HISTORY"] = "Journal de mises à jour";
$MESS["SUP_INITIAL"] = "Initialisation...";
$MESS["SUP_LICENSE_KEY"] = "Clé de licence";
$MESS["SUP_REGISTERED"] = "Enregistré au nom de : ";
$MESS["SUP_SERVER"] = "Serveur des mises à jour : ";
$MESS["SUP_SERVER_ANSWER"] = "Réponse du serveur des mises à jour";
$MESS["SUP_SETTINGS"] = "Modifier les paramètres";
$MESS["SUP_STABLE_OFF_PROMT"] = "Sur votre site l'installation des versions beta des mises à jour du produit est activée. Si vous voulez installer uniquement les versions stables des mises à jour, veuillez désactiver s'il vous plaît, l'installation des versions beta dans les paramètres du système de mise à jour.";
$MESS["SUP_STABLE_ON_PROMT"] = "Sur votre site seule l'installation des mises à jour des versions stables du produit est activée. Si vous voulez installer les versions beta des mises à jour, veuillez activer s'il vous plaît, l'installation des versions beta dans les paramètres du système de mise à jour.";
$MESS["SUP_SUAC_BUTTON"] = "Activer le coupon";
$MESS["SUP_SUAC_COUP"] = "Activer le coupon";
$MESS["SUP_SUAC_ERROR"] = "Erreur d'activation du coupon";
$MESS["SUP_SUAC_HINT"] = "Activer votre coupon pour prolonger la durée de l'assistance technique, d'activer d'autres sites ou de passer à un autre éditeur.";
$MESS["SUP_SUAC_LIMIT"] = "Vous ne pouvez pas créer plus de #NUM# sites avec cette copie du produit.";
$MESS["SUP_SUAC_NO_COUP"] = "Le coupon pour l'activation n'a pas été saisi";
$MESS["SUP_SUAC_PROMT"] = "Veuillez saisir le coupon";
$MESS["SUP_SUAC_SUCCESS"] = "La date de création est pas spécifié";
$MESS["SUP_SUBA_ACTIVATE"] = "Activer la clé de licence";
$MESS["SUP_SUBA_ACTIVATE_BUTTON"] = "Activer la clé";
$MESS["SUP_SUBA_ACTIVATE_HINT"] = "Avant d'utiliser le système des mises à jours, vous devez activer votre clé de licence";
$MESS["SUP_SUBA_FE_CONF_ERR"] = "Le mot de passe et sa confirmation ne coïncident pas";
$MESS["SUP_SUBA_FE_CONTACT"] = "Information de contact";
$MESS["SUP_SUBA_FE_CONTACT_EMAIL"] = "E-mail";
$MESS["SUP_SUBA_FE_CONTACT_PERSON"] = "Personne de contact";
$MESS["SUP_SUBA_FE_CONTACT_PHONE"] = "Numéro de téléphone";
$MESS["SUP_SUBA_FE_EMAIL"] = "Adresse e-mail";
$MESS["SUP_SUBA_FE_ERRGEN"] = "Erreur d'activation de la clé";
$MESS["SUP_SUBA_FE_FNAME"] = "Prénom";
$MESS["SUP_SUBA_FE_LNAME"] = "Nom";
$MESS["SUP_SUBA_FE_LOGIN"] = "Nom d'utilisateur";
$MESS["SUP_SUBA_FE_NAME"] = "Nom de entreprise / nom du propriétaire";
$MESS["SUP_SUBA_FE_PASSWORD"] = "Mot de passe";
$MESS["SUP_SUBA_FE_PASSWORD_CONF"] = "Confirmation";
$MESS["SUP_SUBA_FE_PHONE"] = "Numéro de téléphone";
$MESS["SUP_SUBA_FE_PROMT"] = "Erreur de remplissage des champs suivants";
$MESS["SUP_SUBA_FE_URI"] = "Adresse du site";
$MESS["SUP_SUBA_REGINFO"] = "Informations d'enregistrement";
$MESS["SUP_SUBA_RI_CONTACT"] = "Information de contact";
$MESS["SUP_SUBA_RI_CONTACT1"] = "Adresse et autres informations de contact du propriétaire de cette copie du produit";
$MESS["SUP_SUBA_RI_CONTACT_EMAIL"] = "Adresse emailde la personne de contact";
$MESS["SUP_SUBA_RI_CONTACT_EMAIL1"] = "Adresse email de la personne à contacter pour l'envoi d'information au sujet inscriptions et des rappels sur l'allongement de la période d'assistance technique, ainsi que pour toute question d'ordre technique";
$MESS["SUP_SUBA_RI_CONTACT_PERSON"] = "Le représentant responsable de l'utilisation de cette copie";
$MESS["SUP_SUBA_RI_CONTACT_PERSON1"] = "Nom et prénom de la personne responsable de l'utilisation de cette copie du produit";
$MESS["SUP_SUBA_RI_CONTACT_PHONE"] = "Téléphone de la personne à joindre";
$MESS["SUP_SUBA_RI_CONTACT_PHONE1"] = "Téléphone de la personne à joindre";
$MESS["SUP_SUBA_RI_EMAIL"] = "Adresse emaildu propriétaire pour traiter des questions de licences et d'utilisation du logiciel";
$MESS["SUP_SUBA_RI_EMAIL1"] = "Adresse emailde l'organisation ou du particulier chargé(e) des questions relatives à l'obtention d'une licence et de l'utilisation d'une copie du produit";
$MESS["SUP_SUBA_RI_NAME"] = "Dénomination complète de l'entreprise propriétaire du produit ou le nom complet du propriétaire privé";
$MESS["SUP_SUBA_RI_NAME1"] = "Nom de entreprise ou le nom et le prénom d'une personne privée, propriétaire de cette copie du produit";
$MESS["SUP_SUBA_RI_PHONE"] = "Numéro de téléphone du propriétaire de cette copie de produit (avec le code de la ville)";
$MESS["SUP_SUBA_RI_PHONE1"] = "Téléphone du propriétaire de cette copie (organisation ou personne physique)";
$MESS["SUP_SUBA_RI_URI"] = "Adresse du site";
$MESS["SUP_SUBA_RI_URI1"] = "Tous les domaines à utiliser pour le site y compris des domaines de test";
$MESS["SUP_SUBA_UI_CREATE"] = "Créer un utilisateur sur le site www.bitrixsoft.com";
$MESS["SUP_SUBA_UI_HINT"] = "Si vous n'êtes pas inscrit sur <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a>, veuillez vous assurer que l'option \"Créer un utilisateur\" est cochée et saisissez vos informations (vos nom, prénom, identifiant et mot de passe) dans les champs du formulaire. L'inscription à www.bitrixsoft.com vous permet d'utiliser le <a href=\"http://www.bitrixsoft.com/support/\" target=\"_blank\">service d'assistance technique</a> et le <a href=\"http://www.bitrixsoft.com/support/forum/\" target=\"_blank\">forum privé</a> pour résoudre vos problèmes et obtenir des réponses à vos questions.";
$MESS["SUP_SUBA_UI_LASTNAME"] = "Nom";
$MESS["SUP_SUBA_UI_LOGIN"] = "Nom d'utilisateur (3 symboles, au moins)";
$MESS["SUP_SUBA_UI_PASSWORD"] = "Mot de passe";
$MESS["SUP_SUBA_UI_PASSWORD_CONF"] = "Confirmation";
$MESS["SUP_SUBA_USERINFO"] = "Utilisateur sur le site du produit";
$MESS["SUP_SUBA__UI_NAME"] = "Prénom";
$MESS["SUP_SUBI_CHECK"] = "Dernière vérification des mises à jour";
$MESS["SUP_SUBI_UPD"] = "Les mises à jour ont été installées";
$MESS["SUP_SUBK_BUTTON"] = "Saisie de la clé de licence";
$MESS["SUP_SUBK_ERROR"] = "Erreur de sauvegarde de la clé";
$MESS["SUP_SUBK_GET_KEY"] = "Obtenir une clé de licence d'essai";
$MESS["SUP_SUBK_HINT"] = "S'il vous plaît entrez la clé de votre copie de produit de licence. Vous pouvez trouver la clé de licence dans la boîte de CD ou dans le message e-mail. La clé de licence doit être dans le format suivant : <span style='white-space:nowrap'>SMX-XX-XXXXXXXXXXXXXXX</span>, où X est un chiffre arbitraire ou lettre.";
$MESS["SUP_SUBK_KEY"] = "Saisie de la clé de licence";
$MESS["SUP_SUBK_NO_KEY"] = "La clé de licence n'est pas indiquée";
$MESS["SUP_SUBK_PROMT"] = "Clé de licence";
$MESS["SUP_SUBK_SAVE"] = "Enregistrer la clé de licence";
$MESS["SUP_SUBR_BUTTON"] = "Enregistrement";
$MESS["SUP_SUBR_ERR"] = "Erreur d'enregistrement";
$MESS["SUP_SUBR_HINT"] = "Votre licence vous permet d'obtenir une version entièrement fonctionnelle du système. La version entièrement fonctionnelle n'a pas de restrictions sur les heures de travail.";
$MESS["SUP_SUBR_REG"] = "Enregistrement du produit";
$MESS["SUP_SUBS_BUTTON"] = "Télécharger les textes sources";
$MESS["SUP_SUBS_HINT"] = "Votre licence vous permet d'obtenir les textes sources de modules du système. Pour le faire, vous devez installer les dernières versions de tous les modules du système (c.-à-d les mises à jour de modules devraient être indisponibles). Si une mise à jour d'un module est disponible - il doit être installé en premier lieu.";
$MESS["SUP_SUBS_MED"] = "Les textes initiaux du module sont chargés";
$MESS["SUP_SUBS_SOURCES"] = "Télécharger les textes sources";
$MESS["SUP_SUBS_SUCCESS"] = "Les textes initiaux sont chargés avec succès";
$MESS["SUP_SUBT_AGREE"] = "J'accepte l'accord de licence";
$MESS["SUP_SUBT_ERROR_LICENCE"] = "Erreur de confirmation de licence";
$MESS["SUP_SUBT_LICENCE"] = "Contrat de licence";
$MESS["SUP_SUBT_LICENCE_BUTTON"] = "Ouvrir le contrat de licence";
$MESS["SUP_SUBT_LICENCE_HINT"] = "Avant d'utiliser le système des mises à jour vous devez accepter un nouvel accord de licence";
$MESS["SUP_SUBU_BUTTON"] = "Mise à jour du système SiteUpdate";
$MESS["SUP_SUBU_ERROR"] = "Erreur de mise à jour";
$MESS["SUP_SUBU_HINT"] = "Une mise a jour de la système SiteUpdate est disponible ! Vous devez installer cette mise à jour avant de continuer";
$MESS["SUP_SUBU_UPDATE"] = "Renouvellement du système des mises à jour";
$MESS["SUP_SUBV_BETA"] = "Commutation de l'installation des versions bêta";
$MESS["SUP_SUBV_BETB"] = "Activer l'installation de versions beta";
$MESS["SUP_SUBV_ERROR"] = "Erreur de changement";
$MESS["SUP_SUBV_HINT"] = "La 'Version bêta' est la version testée au sein de l'entreprise conceptrice avant la sortie officielle du produit. Elle contient les dernières mises à jours du produit. La version bêta est relativement stable, mais dans certains cas, peut ne pas fonctionner correctement.";
$MESS["SUP_SUBV_STABB"] = "Activer l'installation que des versions stables";
$MESS["SUP_SUB_ERROR"] = "Certaines mises à jour n'ont pas été installés";
$MESS["SUP_SUB_PROGRESS"] = "Stockage Instant Messenger";
$MESS["SUP_SUB_STOP"] = "Arrêter l'installation";
$MESS["SUP_SUB_SUCCESS"] = "Mises à jour installées avec succès";
$MESS["SUP_SUG_NOTES"] = "Le système des mises à jour permet de mettre à jour le noyau, les modules, les fichiers de langue et le support technique jusqu'à la dernière version. De plus, ce système permet d'enregistrer la copie du logiciel si elle n'est pas encore enregistrée et télécharger des codes initiaux pour la version enregistrée.";
$MESS["SUP_SUG_NOTES1"] = "Attention. Le système des mises à jour ne collecte aucunes données des utilisateurs sur votre site.";
$MESS["SUP_SULD_CLOSE"] = "Fermer la description";
$MESS["SUP_SULD_DESC"] = "Description";
$MESS["SUP_SULL_ADD"] = "En supplément";
$MESS["SUP_SULL_ADD1"] = "Support dans d'autres langues";
$MESS["SUP_SULL_BUTTON"] = "Installer les mises à jour";
$MESS["SUP_SULL_CBT"] = "Marquer tous/Enlever la marque pour tous";
$MESS["SUP_SULL_CNT"] = "Des mises à jour sont accessibles";
$MESS["SUP_SULL_HELP"] = "Système d'aide dans la langue de &quot;#NAME#&quot;";
$MESS["SUP_SULL_LANG"] = "Fichiers pour la langue &quot;#NAME#&quot;";
$MESS["SUP_SULL_MODULE"] = "Module &quot; #NAME# &quot;";
$MESS["SUP_SULL_NAME"] = "Dénomination";
$MESS["SUP_SULL_NOTE"] = "Détails";
$MESS["SUP_SULL_NOTE_D"] = "Détails";
$MESS["SUP_SULL_REF_N"] = "Créer";
$MESS["SUP_SULL_REF_O"] = "Mettre à jour";
$MESS["SUP_SULL_REL"] = "Version";
$MESS["SUP_SULL_TYPE"] = "Entité";
$MESS["SUP_SULL_VERSION"] = "Version #VER#";
$MESS["SUP_SUSU_BUTTON"] = "Abonnement";
$MESS["SUP_SUSU_EMAIL"] = "Adresses email";
$MESS["SUP_SUSU_ERROR"] = "Erreur d'inscription";
$MESS["SUP_SUSU_HINT"] = "S'abonner à recevoir les informations sur les mises à jour par e-mail";
$MESS["SUP_SUSU_NO_EMAIL"] = "adresse email non indiquée";
$MESS["SUP_SUSU_SUCCESS"] = "Vous avez été abonné de recevoir des informations sur les mises à jour par e-mail";
$MESS["SUP_SUSU_TITLE"] = "Inscription pour la réception d'informations sur les renouvellements";
$MESS["SUP_SU_OPTION"] = "Mises à jours optionnelles";
$MESS["SUP_SU_OPTION_HELP"] = "<b>#NUM#</b> pour le système d'assistance";
$MESS["SUP_SU_OPTION_LAN"] = "<b>#NUM#</b> pour les fichiers de langues";
$MESS["SUP_SU_RECOMEND"] = "Mises à jour recommandées";
$MESS["SUP_SU_RECOMEND_LAN"] = "<b>#NUM#</b> pour les fichiers de langues";
$MESS["SUP_SU_RECOMEND_MOD"] = "<b>#NUM#</b> pour les modules";
$MESS["SUP_SU_RECOMEND_NO"] = "aucun";
$MESS["SUP_SU_TITLE1"] = "Installer les mises à jour";
$MESS["SUP_SU_TITLE2"] = "Le système a été mise à jour";
$MESS["SUP_SU_UPD_BUTTON"] = "Installer les mises à jours recommandées";
$MESS["SUP_SU_UPD_HINT"] = "Pour obtenir une nouvelle fonctionnalité, accélérer le fonctionnement du site et augmenter le niveau de sécurité, installez régulièrement des mises à jour pour les modules.";
$MESS["SUP_SU_UPD_INSMED"] = "La mise à jour est installée";
$MESS["SUP_SU_UPD_INSSUC"] = "Les mises à jour installées avec succès";
$MESS["SUP_SU_UPD_VIEW"] = "Voir la liste des mises à jour";
$MESS["SUP_TAB_SETTINGS"] = "Paramètres";
$MESS["SUP_TAB_SETTINGS_ALT"] = "Paramètres";
$MESS["SUP_TAB_UPDATES"] = "Installer les mises à jour";
$MESS["SUP_TAB_UPDATES_ALT"] = "Installer les mises à jour sur votre site Web";
$MESS["SUP_TAB_UPDATES_LIST"] = "Listes de mises à jours";
$MESS["SUP_TAB_UPDATES_LIST_ALT"] = "Sélectionnez les mises à jour que vous souhaitez installer";
$MESS["SUP_TITLE_BASE"] = "Page principale du système des mises à jour";
