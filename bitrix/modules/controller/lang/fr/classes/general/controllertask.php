<?
$MESS["CTRLR_TASK_ERR_ALREADY"] = "Cette tâche est déjà assignée à ce client";
$MESS["CTRLR_TASK_ERR_BAD_ID"] = "Identificateur inconnu de la tâche.";
$MESS["CTRLR_TASK_ERR_CLIENTID"] = "Le code du client ne peut pas rester vide.";
$MESS["CTRLR_TASK_ERR_ID"] = "L'identificateur d'une tâche ne peut pas être vide.";
$MESS["CTRLR_TASK_ERR_KERNEL"] = "Il faut mettre à jour les sites avec le noyau commun en utilisant une page spéciale de mises à jour.";
$MESS["CTRLR_TASK_STATUS_COMPL"] = "Résolu avec succès";
$MESS["CTRLR_TASK_STATUS_FAIL"] = "Erreur d'exécution";
$MESS["CTRLR_TASK_STATUS_NEW"] = "Créer";
$MESS["CTRLR_TASK_STATUS_PART"] = "Réalisée partiellement";
$MESS["CTRLR_TASK_TYPE_CLOSE_MEMBER"] = "Fermeture/ouverture des clients";
$MESS["CTRLR_TASK_TYPE_COUNTERS_UPDATE"] = "Mettre à jour les compteurs";
$MESS["CTRLR_TASK_TYPE_REMOTE_COMMAND"] = "Exécuter la commande";
$MESS["CTRLR_TASK_TYPE_SET_SETTINGS"] = "Définir les paramètres de groupe";
$MESS["CTRLR_TASK_TYPE_UPDATE"] = "Mettre à jour le site";
$MESS["CTRLR_TASK_UPD_COMPL"] = "Il n'y a pas de mises à jour disponibles";
?>