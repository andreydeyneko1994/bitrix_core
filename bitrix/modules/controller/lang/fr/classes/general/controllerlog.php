<?
$MESS["CTRLR_LOG_ERR_NAME"] = "Le champ nom ne peut être vide.";
$MESS["CTRLR_LOG_ERR_UID"] = "Le champ du code de client ne peut pas être vide.";
$MESS["CTRLR_LOG_TYPE_AUTH"] = "Autorisation sur le contrôleur";
$MESS["CTRLR_LOG_TYPE_REGISTRATION"] = "Enregistrement du nouveau client";
$MESS["CTRLR_LOG_TYPE_REMOTE_COMMAND"] = "Exécution d'une commande à distance";
$MESS["CTRLR_LOG_TYPE_SET_SETTINGS"] = "Installation des réglages sur le site du client";
$MESS["CTRLR_LOG_TYPE_SITE_CLOSE"] = "Fermeture/ouverture des sites du client";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE"] = "Installer les mises à jour";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL"] = "Mise à jour Contact Accès Attributs";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL_DB"] = "Mise à jour de base de données pour les sites avec un noyau commun";
$MESS["CTRLR_LOG_TYPE_UNREGISTRATION"] = "Déconnexion du client du contrôleur";
$MESS["CTRLR_LOG_TYPE_UPDATE_COUNTERS"] = "Mise à jour des compteurs";
?>