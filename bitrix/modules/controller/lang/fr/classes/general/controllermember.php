<?
$MESS["CTRLR_MEM_COUNTERS_ERR1"] = "Erreur lors de la sauvegarde des compteurs : ";
$MESS["CTRLR_MEM_ERR1"] = "Le serveur indiqué n'est pas trouvé:";
$MESS["CTRLR_MEM_ERR2"] = "Erreur d'exécution de l'instruction : réponse non identifiée du client ou accès interdit.";
$MESS["CTRLR_MEM_ERR3"] = "Erreur d'exécution de commande : ";
$MESS["CTRLR_MEM_ERR4"] = "Le groupe indiqué est introuvable";
$MESS["CTRLR_MEM_ERR6"] = "Le client spécifié n'est pas trouvé:";
$MESS["CTRLR_MEM_ERR7"] = "Le client spécifié est déconnecté du contrôleur, les opérations ne sont pas possibles avec lui.";
$MESS["CTRLR_MEM_ERR_MEMBER_ID"] = "Champ identifiant unique ne doit pas commencer par un chiffre. Veuillez introduire une autre valeur.";
$MESS["CTRLR_MEM_ERR_MEMBER_NAME"] = "Le champ nom ne peut être vide.";
$MESS["CTRLR_MEM_ERR_MEMBER_UID"] = "La valeur du champ saisie -Identifiant unique- existe déjà. Veuillez en saisir une autre.";
$MESS["CTRLR_MEM_ERR_MEMBER_URL"] = "Champ URL ne peut pas être vide.";
$MESS["CTRLR_MEM_LOG_DESC_COMMAND"] = "Exécution de la commande redirigés";
$MESS["CTRLR_MEM_LOG_DESC_GROUP"] = "Groupe : ";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET"] = "Connexion du client avec l'utilisation des droits de l'administrateur local";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET2"] = "Adjonction de l'utilisateur par un ticket temporaire";
$MESS["CTRLR_MEM_LOG_DISCON"] = "Déconnexion du client du contrôleur";
$MESS["CTRLR_MEM_LOG_DISCON_ERR"] = "Erreur lors de la déconnexion.";
$MESS["CTRLR_MEM_LOG_SITE_CLO"] = "Le site du client est clôturé";
$MESS["CTRLR_MEM_LOG_SITE_OPE"] = "Le site du client est ouvert";
?>