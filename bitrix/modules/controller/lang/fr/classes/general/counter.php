<?php
$MESS["CTRL_COUNTER_ERR_COMMAND"] = "Le champ 'Commande' ne peut pas être vide.";
$MESS["CTRL_COUNTER_ERR_NAME"] = "Le champ 'Nom' ne peut pas être vide.";
$MESS["CTRL_COUNTER_FORMAT_NONE"] = "(non)";
$MESS["CTRL_COUNTER_TYPE_DATETIME"] = "Date et temps";
$MESS["CTRL_COUNTER_TYPE_FILE_SIZE"] = "comme taille du fichier";
$MESS["CTRL_COUNTER_TYPE_FLOAT"] = "Chiffre";
$MESS["CTRL_COUNTER_TYPE_INT"] = "Nombre entier";
$MESS["CTRL_COUNTER_TYPE_STRING"] = "Chaîne";
