<?
$MESS["CTRLR_OPTIONS_DEF_GROUP"] = "Groupe par défaut pour les sites à rejoindre : ";
$MESS["CTRLR_OPTIONS_SHARED_KERNEL_PATH"] = "Le chemin d'accès avec un noyau commun (par rapport à la racine du site contrôleur) : ";
$MESS["CTRLR_OPTIONS_SHOW_HOSTNAME"] = "Afficher le champ \"Nom de l'hôte\" : ";
$MESS["CTRLR_OPTIONS_TIME_AUTOUPDATE"] = "Intervalle pour l'installation automatique des paramètres du groupe sur les sites de clients, par défaut, en minutes (0 - ne pas mettre à jour automatiquement) : ";
?>