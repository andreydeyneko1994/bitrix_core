<?
$MESS["CONTROLLER_MEMBER_CLOSED_DESC"] = "#ID# - Identificateur numérique,
#MEMBER_ID# - identificateur global du client,
#NAME# - nom du client,
#EMAIL# - Adresse email du client,
#CONTACT_PERSON# - contact,
#URL# - adresse du site du client,
#CONTROLLER_GROUP_ID# - identificateur du groupe du client,
#SHARED_KERNEL# - noyau commun de fichier (Y|N),
#ACTIVE# - actif,
#DISCONNECTED# - déconnecté,
#DATE_ACTIVE_FROM# - date de début de l'activité,
#DATE_ACTIVE_TO# - date de la fin de l'activité,
#DATE_CREATE# - temps de création,
#NOTES# - description";
$MESS["CONTROLLER_MEMBER_CLOSED_NAME"] = "Fermeture du site du client";
$MESS["CONTROLLER_MEMBER_REGISTER_DESC"] = "#ID# - Identificateur numérique,
#MEMBER_ID# - identificateur global du client,
#NAME# - nom du client,
#EMAIL# - Adresse email du client,
#CONTACT_PERSON# - contact,
#URL# - adresse du site du client,
#CONTROLLER_GROUP_ID# - identificateur du groupe du client,
#SHARED_KERNEL# - noyau commun de fichier (Y|N),
#ACTIVE# - actif,
#DISCONNECTED# - déconnecté,
#DATE_ACTIVE_FROM# - date de début de l'activité,
#DATE_ACTIVE_TO# - date de la fin de l'activité,
#DATE_CREATE# - temps de création,
#NOTES# - description";
$MESS["CONTROLLER_MEMBER_REGISTER_NAME"] = "Nouveau cours";
?>