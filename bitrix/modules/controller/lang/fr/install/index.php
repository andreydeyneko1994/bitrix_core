<?
$MESS["CTRL_INST_DESC"] = "Gestion de sites à distance";
$MESS["CTRL_INST_NAME"] = "Contrôleur";
$MESS["CTRL_INST_STEP1"] = "Installation du module du contrôleur : Pas 1";
$MESS["CTRL_INST_STEP1_UN"] = "Suppression du module du contrôleur : Pas 1";
$MESS["CTRL_PERM_D"] = "Accès interdit";
$MESS["CTRL_PERM_L"] = "Autorisation (Identification) sur les sites";
$MESS["CTRL_PERM_R"] = "Lecture seule";
$MESS["CTRL_PERM_T"] = "Ajouter de nouveaux sites";
$MESS["CTRL_PERM_V"] = "Gestion des sites";
$MESS["CTRL_PERM_W"] = "Accès complet";
?>