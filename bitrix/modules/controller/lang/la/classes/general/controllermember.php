<?
$MESS["CTRLR_MEM_COUNTERS_ERR1"] = "Error al guardar contadores:";
$MESS["CTRLR_MEM_ERR1"] = "El servidor especificado no puede ser encontrado:";
$MESS["CTRLR_MEM_ERR2"] = "Error en la ejecución del comando: respuesta no reconocida o acceso denegado.";
$MESS["CTRLR_MEM_ERR3"] = "Error en la ejecución del comando:";
$MESS["CTRLR_MEM_ERR4"] = "El grupo no fue encontrado:";
$MESS["CTRLR_MEM_ERR6"] = "El cliente no fue encontrado:";
$MESS["CTRLR_MEM_ERR7"] = "El cliente esta desconectado desde el controlador. No hay operaciones que puedan realizar.";
$MESS["CTRLR_MEM_ERR_MEMBER_ID"] = "El ID único no puede empezar con un dígito. Por favor use algún otro valor.";
$MESS["CTRLR_MEM_ERR_MEMBER_NAME"] = "El nombre del campo no puede estar vacío.";
$MESS["CTRLR_MEM_ERR_MEMBER_UID"] = "El valor especificado en el campo \"ID único\" ya existe. Por favor usar un valor diferente.";
$MESS["CTRLR_MEM_ERR_MEMBER_URL"] = "El URL del campo no puede estar vacío.";
$MESS["CTRLR_MEM_LOG_DESC_COMMAND"] = "Redirigir la ejecución de comandos";
$MESS["CTRLR_MEM_LOG_DESC_GROUP"] = "Grupo:";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET"] = "Conectando al cliente usando credenciales del administrador local";
$MESS["CTRLR_MEM_LOG_DESC_JOIN_BY_TICKET2"] = "Conectando al cliente usando una entrada temporal";
$MESS["CTRLR_MEM_LOG_DISCON"] = "Desconectando cliente desde el controlador";
$MESS["CTRLR_MEM_LOG_DISCON_ERR"] = "Error en la Desconexión";
$MESS["CTRLR_MEM_LOG_SITE_CLO"] = "Sitio web cliente cerrado";
$MESS["CTRLR_MEM_LOG_SITE_OPE"] = "Sitio web cliente abierto";
?>