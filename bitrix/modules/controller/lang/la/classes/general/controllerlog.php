<?
$MESS["CTRLR_LOG_ERR_NAME"] = "El nombre no puede estar vacío.";
$MESS["CTRLR_LOG_ERR_UID"] = "El campo ID del cliente no puede estar vacío.";
$MESS["CTRLR_LOG_TYPE_AUTH"] = "Controlador de autorización";
$MESS["CTRLR_LOG_TYPE_REGISTRATION"] = "Registro de nuevo cliente";
$MESS["CTRLR_LOG_TYPE_REMOTE_COMMAND"] = "Ejecución remota de comandos";
$MESS["CTRLR_LOG_TYPE_SET_SETTINGS"] = "Propagar la configuración de cliente del sitio web";
$MESS["CTRLR_LOG_TYPE_SITE_CLOSE"] = "Cerrar / Abrir sitios web de clientes";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE"] = "Instalar las actualizaciones";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL"] = "Actualización del núcleo común";
$MESS["CTRLR_LOG_TYPE_SITE_UPDATE_KERNEL_DB"] = "Actualización de la base de datos de sitios web con núcleo común";
$MESS["CTRLR_LOG_TYPE_UNREGISTRATION"] = "Remover al cliente desde el controlador";
$MESS["CTRLR_LOG_TYPE_UPDATE_COUNTERS"] = "Contador de actualización";
?>