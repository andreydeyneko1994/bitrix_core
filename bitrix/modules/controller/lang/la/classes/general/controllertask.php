<?
$MESS["CTRLR_TASK_ERR_ALREADY"] = "Esta tarea ya se ha asignado al cliente";
$MESS["CTRLR_TASK_ERR_BAD_ID"] = "ID de la tarea Irreconocible.";
$MESS["CTRLR_TASK_ERR_CLIENTID"] = "El ID de cliente no puede estar vacío.";
$MESS["CTRLR_TASK_ERR_ID"] = "El ID de la tarea no puede estar vacío.";
$MESS["CTRLR_TASK_ERR_KERNEL"] = "Los sitios web de núcleo común deben ser actualizados mediante un formulario de actualización especial.";
$MESS["CTRLR_TASK_STATUS_COMPL"] = "Completado satisfactoriamente";
$MESS["CTRLR_TASK_STATUS_FAIL"] = "Tarea de ejecución ha fallado";
$MESS["CTRLR_TASK_STATUS_NEW"] = "Nuevo";
$MESS["CTRLR_TASK_STATUS_PART"] = "Parcialmente terminado";
$MESS["CTRLR_TASK_TYPE_CLOSE_MEMBER"] = "Cerrar / Abrir clientes";
$MESS["CTRLR_TASK_TYPE_COUNTERS_UPDATE"] = "Actualización de los contadores";
$MESS["CTRLR_TASK_TYPE_REMOTE_COMMAND"] = "Ejecutar un comando";
$MESS["CTRLR_TASK_TYPE_SET_SETTINGS"] = "Propagar la configuración del grupo";
$MESS["CTRLR_TASK_TYPE_UPDATE"] = "Actualización del sitio web";
$MESS["CTRLR_TASK_UPD_COMPL"] = "Las actualizaciones no estas disponibles actualmente";
?>