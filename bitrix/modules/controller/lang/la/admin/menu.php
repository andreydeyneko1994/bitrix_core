<?
$MESS["CTRLR_MENU_AUTH"] = "Autenticación";
$MESS["CTRLR_MENU_COUNTERS"] = "Contadores";
$MESS["CTRLR_MENU_COUNTERS_TITLE"] = "Configurar contadores";
$MESS["CTRLR_MENU_GROUP_NAME"] = "Grupos";
$MESS["CTRLR_MENU_GROUP_TYPE"] = "Administrar los ajustes del grupo de sitio web";
$MESS["CTRLR_MENU_LOG_NAME"] = "Diario de navegación";
$MESS["CTRLR_MENU_LOG_TITLE"] = "Controlador del diario de navegación";
$MESS["CTRLR_MENU_NAME"] = "Controlador";
$MESS["CTRLR_MENU_RUN_NAME"] = "Ejecución remota de comandos";
$MESS["CTRLR_MENU_RUN_TITLE"] = "Ejecutar comandos de PHP en sitios web remotos";
$MESS["CTRLR_MENU_SITE_NAME"] = "Sitios web";
$MESS["CTRLR_MENU_SITE_TITLE"] = "Administrar los sitios web cliente";
$MESS["CTRLR_MENU_TASK_NAME"] = "Tareas";
$MESS["CTRLR_MENU_TASK_TITLE"] = "Tareas para la ejecución remota en sitios web cliente";
$MESS["CTRLR_MENU_TITLE"] = "Controlador del sitio web";
$MESS["CTRLR_MENU_UPD_NAME"] = "Actualizar sitios web";
$MESS["CTRLR_MENU_UPD_TYPE"] = "Actualizar sitios web cliente";
$MESS["CTRLR_MENU_UPLOAD_NAME"] = "Cargar archvo";
$MESS["CTRLR_MENU_UPLOAD_TITLE"] = "Enviar archivos a los sitios web del cliente";
?>