<?
$MESS["CTRLE_GR_AD_COUNTER_FREE"] = "Actualización gratis del contador del espacio del disco";
$MESS["CTRLE_GR_AD_COUNTER_LAST_AU"] = "Actualización del contador de la ultima autorización";
$MESS["CTRLE_GR_AD_COUNTER_SITES"] = "Actualización del contador del sitio web";
$MESS["CTRLE_GR_AD_COUNTER_UPD_PER"] = "Contador de intervalo de actualización";
$MESS["CTRLE_GR_AD_COUNTER_USERS"] = "Actualización del contador de usuario";
$MESS["CTRLR_GR_AD_COL_CRE"] = "Creado";
$MESS["CTRLR_GR_AD_COL_CREBY"] = "Creador";
$MESS["CTRLR_GR_AD_COL_DESC"] = "Descripción";
$MESS["CTRLR_GR_AD_COL_ID"] = "ID";
$MESS["CTRLR_GR_AD_COL_MOD"] = "Modificado";
$MESS["CTRLR_GR_AD_COL_MODBY"] = "Modificado por";
$MESS["CTRLR_GR_AD_COL_NAME"] = "Nombre";
$MESS["CTRLR_GR_AD_ERR1"] = "Error actualizando los parámetros del grupo";
$MESS["CTRLR_GR_AD_ERR2"] = "Error eliminando el grupo";
$MESS["CTRLR_GR_AD_FLT_ACT_FROM"] = "Activar desde";
$MESS["CTRLR_GR_AD_FLT_ACT_TO"] = "Activar hasta";
$MESS["CTRLR_GR_AD_FLT_CREAT"] = "Creado";
$MESS["CTRLR_GR_AD_FLT_MODIF"] = "Modificado";
$MESS["CTRLR_GR_AD_MENU_COPY"] = "Copiar";
$MESS["CTRLR_GR_AD_MENU_DEL"] = "Eliminar";
$MESS["CTRLR_GR_AD_MENU_DEL_CONFIRM"] = "Esto eliminara el grupo y todo sus ajustes. Desea continuar?";
$MESS["CTRLR_GR_AD_MENU_EDIT"] = "Editar";
$MESS["CTRLR_GR_AD_NAV"] = "Grupos";
$MESS["CTRLR_GR_AD_TITLE"] = "Administrar grupos de sitio web";
?>