<?
$MESS["TASK_BINDING_CONTROLLER"] = "Controlador";
$MESS["TASK_DESC_CONTROLLER_ADD"] = "Agregar nuevos sitios web";
$MESS["TASK_DESC_CONTROLLER_AUTH"] = "Autenticación en sitios web remotos";
$MESS["TASK_DESC_CONTROLLER_DENY"] = "Acceso al módulo Controller fue denegada.";
$MESS["TASK_DESC_CONTROLLER_FULL"] = "Acceso completo a las funciones de administración del módulo";
$MESS["TASK_DESC_CONTROLLER_READ"] = "Ver la configuración del módulo";
$MESS["TASK_DESC_CONTROLLER_SITE"] = "Gestionar sitios web";
$MESS["TASK_NAME_CONTROLLER_ADD"] = "Agregar";
$MESS["TASK_NAME_CONTROLLER_AUTH"] = "Autenticación";
$MESS["TASK_NAME_CONTROLLER_DENY"] = "Acceso denegado.";
$MESS["TASK_NAME_CONTROLLER_FULL"] = "Acceso completo";
$MESS["TASK_NAME_CONTROLLER_READ"] = "Leer";
$MESS["TASK_NAME_CONTROLLER_SITE"] = "Gestionar";
?>