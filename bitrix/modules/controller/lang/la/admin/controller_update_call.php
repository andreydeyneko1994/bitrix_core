<?
$MESS["SUPC_HE_C_UPDATED"] = "Actualizar sección de ayuda";
$MESS["SUPC_HE_UPD"] = "Actualización de la sección de ayuda fallida";
$MESS["SUPC_LE_C_UPDATED"] = "Actualizar archivos de idioma";
$MESS["SUPC_LE_UPD"] = "Actualización del archivo de idioma fallida";
$MESS["SUPC_ME_CHECK"] = "Error de escritura del archivo de verificación";
$MESS["SUPC_ME_C_UPDATED"] = "Actualizar el núcleo común";
$MESS["SUPC_ME_LOAD"] = "No puede descargar actualizaciones";
$MESS["SUPC_ME_PACK"] = "No puede extraer archivos desde achivo";
$MESS["SUPC_ME_UPDATE"] = "Actualización del modulo fallida";
?>