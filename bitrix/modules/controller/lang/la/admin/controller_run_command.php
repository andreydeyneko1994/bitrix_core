<?
$MESS["CTRLR_RUN_ADD_TASK"] = "Ejecutar comando mediante el programador de tarea";
$MESS["CTRLR_RUN_ADD_TASK_LABEL"] = "Agregar comando a la tarea en cola";
$MESS["CTRLR_RUN_BUTT_CLEAR"] = "Limpiar";
$MESS["CTRLR_RUN_BUTT_RUN"] = "Iniciar";
$MESS["CTRLR_RUN_COMMAND_FIELD"] = "script PHP";
$MESS["CTRLR_RUN_COMMAND_TAB_TITLE"] = "script PHP para inicio remoto";
$MESS["CTRLR_RUN_CONFIRM"] = "El comando ingresado sera ejecutado en el servidor remoto como un script PHP. Desea continuar?";
$MESS["CTRLR_RUN_ERR_NSELECTED"] = "Seleccionar un cliente para iniciar";
$MESS["CTRLR_RUN_ERR_TOO_MANY_SELECTED"] = "El comando se ejecutará en un gran número de sitios web conectados. Verificar la casilla correspondiente si estás seguro de lo que estás haciendo.";
$MESS["CTRLR_RUN_FILTER_GROUP"] = "Grupo";
$MESS["CTRLR_RUN_FILTER_GROUP_ANY"] = "(cualquiera)";
$MESS["CTRLR_RUN_FILTER_SITE"] = "Sitio web";
$MESS["CTRLR_RUN_FILTER_SITE_ALL"] = "(todo)";
$MESS["CTRLR_RUN_FORCE_RUN"] = "Ejecutar el comando en cualquier número de sitios web";
$MESS["CTRLR_RUN_SUCCESS"] = "#SUCCESS_CNT# para #CNT# los comandos remotos han sido satisfactoriamente agregados a la <a href=\"controller_task.php?lang=#LANG#\">tarea pendiente</a>.";
$MESS["CTRLR_RUN_TITLE"] = "Linea de comando de PHP remoto";
?>