<?
$MESS["CTRLR_UPLOAD_BUTT_CLEAR"] = "Limpiar";
$MESS["CTRLR_UPLOAD_BUTT_RUN"] = "Cargar";
$MESS["CTRLR_UPLOAD_CONFIRM"] = "El archivo podrá ser enviado a los sitios web seleccionados. Continuar?";
$MESS["CTRLR_UPLOAD_ERROR_NO_PATH_TO"] = "La ruta del destino no está especificada.";
$MESS["CTRLR_UPLOAD_ERR_FILE"] = "El archivo especificado no fue encontrado en el servidor.";
$MESS["CTRLR_UPLOAD_ERR_NSELECTED"] = "Ningún cliente ha sido seleccionado para enviar los archivos a.";
$MESS["CTRLR_UPLOAD_ERR_PACK"] = "Erroral preparar el archivo a transferir.";
$MESS["CTRLR_UPLOAD_ERR_TOO_MANY_SELECTED"] = "El archivo se transfiere a un número muy grande de sitios web conectados. Marque la casilla correspondiente si está seguro de lo que estás haciendo.";
$MESS["CTRLR_UPLOAD_FILE_TAB"] = "Archivo";
$MESS["CTRLR_UPLOAD_FILE_TAB_TITLE"] = "Seleccionar el archivo para enviar a los sitios web del cliente.";
$MESS["CTRLR_UPLOAD_FILTER_GROUP"] = "Grupo";
$MESS["CTRLR_UPLOAD_FILTER_GROUP_ANY"] = "(cualquiera)";
$MESS["CTRLR_UPLOAD_FILTER_SITE"] = "Sitio web";
$MESS["CTRLR_UPLOAD_FILTER_SITE_ALL"] = "(todo)";
$MESS["CTRLR_UPLOAD_FORCE_RUN"] = "Enviar archivo a cualquier número de sitios web";
$MESS["CTRLR_UPLOAD_OPEN_FILE_BUTTON"] = "Abrir...";
$MESS["CTRLR_UPLOAD_SEND_FILE_FROM"] = "Desde:";
$MESS["CTRLR_UPLOAD_SEND_FILE_TO"] = "Para:";
$MESS["CTRLR_UPLOAD_TITLE"] = "Cargar archivos para los sitios web de los clientes";
?>