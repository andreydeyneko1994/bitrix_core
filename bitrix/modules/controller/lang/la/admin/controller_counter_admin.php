<?
$MESS["CTRL_CNT_ADMIN_COMMAND"] = "Comando";
$MESS["CTRL_CNT_ADMIN_COUNTER_FORMAT"] = "Formato";
$MESS["CTRL_CNT_ADMIN_COUNTER_TYPE"] = "Tipo de datos";
$MESS["CTRL_CNT_ADMIN_DELETE_ERROR"] = "Error al eliminar el contador ##ID#: #ERROR#";
$MESS["CTRL_CNT_ADMIN_FILTER_ANY"] = "Cualquiera";
$MESS["CTRL_CNT_ADMIN_FILTER_GROUP"] = "Grupo";
$MESS["CTRL_CNT_ADMIN_ID"] = "ID";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE"] = "Eliminar";
$MESS["CTRL_CNT_ADMIN_MENU_DELETE_ALERT"] = "El contador y todos los valores asociados serán eliminados. Continue?";
$MESS["CTRL_CNT_ADMIN_MENU_EDIT"] = "Editar";
$MESS["CTRL_CNT_ADMIN_NAME"] = "Nombre";
$MESS["CTRL_CNT_ADMIN_NAV"] = "Contadores";
$MESS["CTRL_CNT_ADMIN_TITLE"] = "Contadores";
$MESS["CTRL_CNT_ADMIN_UPDATE_ERROR"] = "Error al actualizar el contador ##ID#: #ERROR#";
?>