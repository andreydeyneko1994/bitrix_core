<?
$MESS["CTRL_MEMB_HIST_ANY"] = "(cualquier)";
$MESS["CTRL_MEMB_HIST_BACK"] = "Historia";
$MESS["CTRL_MEMB_HIST_BACK_TITLE"] = "Volver al editor de sitios web";
$MESS["CTRL_MEMB_HIST_CONTROLLER_GROUP_ID"] = "Grupo de sitio web";
$MESS["CTRL_MEMB_HIST_CONTROLLER_MEMBER_ID"] = "ID del sitio web";
$MESS["CTRL_MEMB_HIST_CREATED_DATE"] = "Tiempo";
$MESS["CTRL_MEMB_HIST_FIELD"] = "Campo modificado";
$MESS["CTRL_MEMB_HIST_FROM_VALUE"] = "Valor inicial";
$MESS["CTRL_MEMB_HIST_NAVSTRING"] = "Registro";
$MESS["CTRL_MEMB_HIST_NOTES"] = "Notas";
$MESS["CTRL_MEMB_HIST_SITE_ACTIVE"] = "Activo";
$MESS["CTRL_MEMB_HIST_TITLE"] = "Historia de sitio Web";
$MESS["CTRL_MEMB_HIST_TO_VALUE"] = "Nuevo valor";
$MESS["CTRL_MEMB_HIST_USER_ID"] = "Usuario";
?>