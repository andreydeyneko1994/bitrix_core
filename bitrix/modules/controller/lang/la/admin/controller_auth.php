<?
$MESS["CTRLR_AUTH_CS_LABEL"] = "Permitir la autorización de los usuarios del controlador";
$MESS["CTRLR_AUTH_CS_TAB_TITLE"] = "Configurar la autenticación de usuario del controlador en sitios Web de cliente";
$MESS["CTRLR_AUTH_LOC_GROUPS"] = "Mapa de grupos";
$MESS["CTRLR_AUTH_NOTE"] = "Los administradores del controlador siempre autorizan los sitios web del cliente con permisos máximos";
$MESS["CTRLR_AUTH_SC_LABEL"] = "Permitir autenticación del controlador";
$MESS["CTRLR_AUTH_SC_TAB_TITLE"] = "Configurar la autenticación del usuario del sitio Web del cliente en el controlador";
$MESS["CTRLR_AUTH_SETUP"] = "configurar";
$MESS["CTRLR_AUTH_SS_LABEL"] = "Permitir la autorización transparente";
$MESS["CTRLR_AUTH_SS_TAB_TITLE"] = "Configurar la autenticación a través del sitios web del clientes";
$MESS["CTRLR_AUTH_TAB"] = "Configurar la autenticación de usuario";
$MESS["CTRLR_AUTH_TITLE"] = "Autorización";
?>