<?
$MESS["CTRL_LOG_ADMIN_COLUMN_CREATED"] = "Creado";
$MESS["CTRL_LOG_ADMIN_COLUMN_NAME"] = "Nombre";
$MESS["CTRL_LOG_ADMIN_COLUMN_STATUS_ERR"] = "Error";
$MESS["CTRL_LOG_ADMIN_COLUMN_STATUS_OK"] = "Con éxito";
$MESS["CTRL_LOG_ADMIN_COLUMN_USER"] = "Usuario";
$MESS["CTRL_LOG_ADMIN_ERR_DELETE"] = "Error eliminando un registro";
$MESS["CTRL_LOG_ADMIN_FILTER_ANY"] = "(cualquiera)";
$MESS["CTRL_LOG_ADMIN_FILTER_CLIENT"] = "Cliente";
$MESS["CTRL_LOG_ADMIN_FILTER_CREATED"] = "Creado";
$MESS["CTRL_LOG_ADMIN_FILTER_DESC"] = "Descripción";
$MESS["CTRL_LOG_ADMIN_FILTER_ID"] = "ID";
$MESS["CTRL_LOG_ADMIN_FILTER_IDCLIENT"] = "ID del cliente";
$MESS["CTRL_LOG_ADMIN_FILTER_STATUS"] = "Estado";
$MESS["CTRL_LOG_ADMIN_FILTER_TASK"] = "Tarea";
$MESS["CTRL_LOG_ADMIN_FILTER_TASKID"] = "ID de la tarea";
$MESS["CTRL_LOG_ADMIN_MENU_DEL"] = "Eliminar";
$MESS["CTRL_LOG_ADMIN_MENU_DEL_CONFIRM"] = "Esto removerá el registro desde la lista. Desea continuar? ";
$MESS["CTRL_LOG_ADMIN_MENU_DETAIL"] = "Detallado";
$MESS["CTRL_LOG_ADMIN_PAGETITLE"] = "Historial";
$MESS["CTRL_LOG_ADMIN_TITLE"] = "Historial del controlador del sitio web";
?>