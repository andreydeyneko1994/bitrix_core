<?
$MESS["CONTROLLER_GROUP_MAP_"] = "Esto eliminará el grupo y todas sus configuraciones. ¿Desea continuar?";
$MESS["CONTROLLER_GROUP_MAP_ADD"] = "Agregar";
$MESS["CONTROLLER_GROUP_MAP_ADD_BTN"] = "Agregar";
$MESS["CONTROLLER_GROUP_MAP_ADD_TITLE"] = "Agregar mapeo";
$MESS["CONTROLLER_GROUP_MAP_CANCEL_BTN"] = "Cancelar";
$MESS["CONTROLLER_GROUP_MAP_CONFIRM_DEL"] = "¿Está seguro de que quiere eliminar este mapeo?";
$MESS["CONTROLLER_GROUP_MAP_CONTROLLER_GROUP_ID"] = "ID del controlador de grupo";
$MESS["CONTROLLER_GROUP_MAP_CONTROLLER_GROUP_ID_ERROR"] = "Controlador de grupo no está especificado.";
$MESS["CONTROLLER_GROUP_MAP_CS_TITLE"] = "Configuración de la autenticación de usuario del controlador en los sitios web del cliente";
$MESS["CONTROLLER_GROUP_MAP_DELETE"] = "Eliminar";
$MESS["CONTROLLER_GROUP_MAP_ID"] = "ID";
$MESS["CONTROLLER_GROUP_MAP_LOCAL_GROUP_CODE"] = "Código local del sitio web del grupo";
$MESS["CONTROLLER_GROUP_MAP_LOCAL_GROUP_CODE_ERROR"] = "Código local del grupo no se especifica.";
$MESS["CONTROLLER_GROUP_MAP_NEW_TAB"] = "Nuevo";
$MESS["CONTROLLER_GROUP_MAP_PAGES"] = "Mapeo";
$MESS["CONTROLLER_GROUP_MAP_REMOTE_GROUP_CODE"] = "Código remoto del sitio web del grupo";
$MESS["CONTROLLER_GROUP_MAP_REMOTE_GROUP_CODE_ERROR"] = "Código remoto del grupo no se especifica.";
$MESS["CONTROLLER_GROUP_MAP_SC_TITLE"] = "Configurar la autenticación del usuario en el sitio web del cliente en el controlador";
$MESS["CONTROLLER_GROUP_MAP_SS_TITLE"] = "Configurar la autenticación a través del sitio web del cliente";
?>