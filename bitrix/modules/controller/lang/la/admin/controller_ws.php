<?
$MESS["CTRLR_WS_ERR_BAD_COMMAND"] = "El comando requerido no se encuentra o esta expirado.";
$MESS["CTRLR_WS_ERR_BAD_LEVEL"] = "Permisos insuficientes para agregar a un nuevo cliente";
$MESS["CTRLR_WS_ERR_BAD_OPERID"] = "ID de la operación desconocida:";
$MESS["CTRLR_WS_ERR_BAD_PASSW"] = "Contraseña incorrecta.";
$MESS["CTRLR_WS_ERR_FILE_NOT_FOUND"] = "Archivo no encontrado";
$MESS["CTRLR_WS_ERR_MEMB_DISCN"] = "Permisos insuficientes para desconectar un cliente";
$MESS["CTRLR_WS_ERR_MEMB_NFOUND"] = "El cliente no se puede encontrar";
$MESS["CTRLR_WS_ERR_RUN"] = "Error de Ejecución:";
$MESS["CTRLR_WS_ERR_RUN_BACK"] = "Atrás";
$MESS["CTRLR_WS_ERR_RUN_TRY"] = "Por favor intente de nuevo";
?>