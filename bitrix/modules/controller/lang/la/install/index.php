<?
$MESS["CTRL_INST_DESC"] = "Administración remota del sitio web";
$MESS["CTRL_INST_NAME"] = "Controlador del sitio web";
$MESS["CTRL_INST_STEP1"] = "Instalación del modulo del Controlador: Paso 1";
$MESS["CTRL_INST_STEP1_UN"] = "Desinstalación del modulo del Controlador: Paso 1";
$MESS["CTRL_PERM_D"] = "Acceso denegado";
$MESS["CTRL_PERM_L"] = "Autorizar en sitios web";
$MESS["CTRL_PERM_R"] = "Solo leer";
$MESS["CTRL_PERM_T"] = "Agregar nuevos sitios web";
$MESS["CTRL_PERM_V"] = "Administración de sitios web";
$MESS["CTRL_PERM_W"] = "Acceso total";
?>