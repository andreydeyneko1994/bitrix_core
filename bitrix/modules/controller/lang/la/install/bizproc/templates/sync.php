<?
$MESS["BPT_SYNC_DESC"] = "Permite la publicación de elementos de bloque de información en los sitios web controlados. El primer documento se publicará en el sitio web de control.";
$MESS["BPT_SYNC_NAME"] = "Publicado en los sitios web controlados";
$MESS["BPT_SYNC_PUBLISH"] = "Publicar Documento";
$MESS["BPT_SYNC_SEQ"] = "Proceso de negocios secuencial";
$MESS["BPT_SYNC_SYNC"] = "Publicado en los sitios web controlados";
?>