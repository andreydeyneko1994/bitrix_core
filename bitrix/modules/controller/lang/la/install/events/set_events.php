<?
$MESS["CONTROLLER_MEMBER_CLOSED_DESC"] = "#ID# - El ID numérico del controlador de miembros,
#MEMBER_ID# - El ID global del controlador de miembros,
#NAME# - Nombre del controlador de miembros,
#EMAIL# - Dirección de correo electrónico del controlador de miembros,
#CONTACT_PERSON# - la persona de contacto,
#URL# - La dirección URL del controlador de miembros del sitio web,
#CONTROLLER_GROUP_ID# - El ID del controlador de miembros del grupo,
#SHARED_KERNEL# - cuota de sistema de archivos del núcleo(S|N),
#ACTIVE# - El estado activo,
#DISCONNECTED# - Estado desconectado,
#DATE_ACTIVE_FROM# - La fecha de activación,
#DATE_ACTIVE_TO# - La fecha de expiración,
#DATE_CREATE# - La fecha de creación,
#NOTES# - Texto descriptivo";
$MESS["CONTROLLER_MEMBER_CLOSED_NAME"] = "Clausura del controlador de miembros del sitio web";
$MESS["CONTROLLER_MEMBER_REGISTER_DESC"] = "#ID# - El ID numérico del controlador de miembros,
#MEMBER_ID# - El ID global del controlador de miembros,
#NAME# - Nombre del controlador de miembros,
#EMAIL# - Dirección de correo electrónico del controlador de miembros,
#CONTACT_PERSON# - la persona de contacto,
#URL# - La dirección URL del controlador de miembros del sitio web,
#CONTROLLER_GROUP_ID# - El ID del controlador de miembros del grupo,
#SHARED_KERNEL# - cuota de sistema de archivos del núcleo(S|N),
#ACTIVE# - El estado activo,
#DISCONNECTED# - Estado desconectado,
#DATE_ACTIVE_FROM# - La fecha de activación,
#DATE_ACTIVE_TO# - La fecha de expiración,
#DATE_CREATE# - La fecha de creación,
#NOTES# - Texto descriptivo";
$MESS["CONTROLLER_MEMBER_REGISTER_NAME"] = "Nuevo controlador de miembros";
?>