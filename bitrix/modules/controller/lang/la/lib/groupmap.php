<?
$MESS["GROUP_MAP_ENTITY_CONTROLLER_GROUP_ID_FIELD"] = "ID del grupo controlador";
$MESS["GROUP_MAP_ENTITY_ID_FIELD"] = "ID único";
$MESS["GROUP_MAP_ENTITY_LOCAL_GROUP_CODE_FIELD"] = "Código de grupo en el sitio web local";
$MESS["GROUP_MAP_ENTITY_REMOTE_GROUP_CODE_FIELD"] = "Código de grupo en el sitio web remoto";
?>