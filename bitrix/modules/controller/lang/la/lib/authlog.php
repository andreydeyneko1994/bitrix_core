<?
$MESS["AUTH_LOG_ENTITY_FROM_CONTROLLER_MEMBER_ID_FIELD"] = "Autenticación desde (ID del sitio web remoto)";
$MESS["AUTH_LOG_ENTITY_ID_FIELD"] = "ID único";
$MESS["AUTH_LOG_ENTITY_STATUS_FIELD"] = "Estados";
$MESS["AUTH_LOG_ENTITY_TIMESTAMP_X_FIELD"] = "Registro actualizado el";
$MESS["AUTH_LOG_ENTITY_TO_CONTROLLER_MEMBER_ID_FIELD"] = "Autenticación hasta (ID del sitio web remoto)";
$MESS["AUTH_LOG_ENTITY_USER_ID_FIELD"] = "ID de usuario";
?>