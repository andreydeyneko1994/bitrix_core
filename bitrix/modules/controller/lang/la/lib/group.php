<?
$MESS["GROUP_ENTITY_CHECK_COUNTER_FREE_SPACE_FIELD"] = "Contador de cuotas de disco habilitar indicador";
$MESS["GROUP_ENTITY_CHECK_COUNTER_LAST_AUTH_FIELD"] = "Última autenticación habilitar indicador contador";
$MESS["GROUP_ENTITY_CHECK_COUNTER_SITES_FIELD"] = "Contador del sitio web habilitar indicador";
$MESS["GROUP_ENTITY_CHECK_COUNTER_USERS_FIELD"] = "Contador de usuarios habilitar el indicador";
$MESS["GROUP_ENTITY_COUNTER_UPDATE_PERIOD_FIELD"] = "Intervalo de actualización del contador";
$MESS["GROUP_ENTITY_CREATED_BY_FIELD"] = "Creado por";
$MESS["GROUP_ENTITY_DATE_CREATE_FIELD"] = "Creado el";
$MESS["GROUP_ENTITY_DESCRIPTION_FIELD"] = "Descripción";
$MESS["GROUP_ENTITY_DISABLE_DEACTIVATED_FIELD"] = "Desactivado";
$MESS["GROUP_ENTITY_ID_FIELD"] = "ID del grupo";
$MESS["GROUP_ENTITY_INSTALL_PHP_FIELD"] = "Script PHP para ejecutar cuando se agrega a un grupo";
$MESS["GROUP_ENTITY_MODIFIED_BY_FIELD"] = "Modificado por";
$MESS["GROUP_ENTITY_NAME_FIELD"] = "Nombre del grupo";
$MESS["GROUP_ENTITY_TIMESTAMP_X_FIELD"] = "Modificado el";
$MESS["GROUP_ENTITY_UNINSTALL_PHP_FIELD"] = "Script PHP para ejecutar durante la extracción de un grupo";
$MESS["GROUP_ENTITY_UPDATE_PERIOD_FIELD"] = "Intervalo de actualización de la configuración";
?>