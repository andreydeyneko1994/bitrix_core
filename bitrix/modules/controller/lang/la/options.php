<?
$MESS["CTRLR_OPTIONS_AUTH_LOG_DAYS"] = "Días para mantener el registro de autenticación (0 - No eliminar nunca)";
$MESS["CTRLR_OPTIONS_DEF_GROUP"] = "Grupo predeterminado por el controlador de sitios web:";
$MESS["CTRLR_OPTIONS_SHARED_KERNEL_PATH"] = "Ruta a la carpeta del núcleo común:";
$MESS["CTRLR_OPTIONS_SHOW_HOSTNAME"] = "Mostrar el campo \"Nombre de host\":";
$MESS["CTRLR_OPTIONS_TIME_AUTOUPDATE"] = "Intervalo predeterminado por la actualización de los ajustes del sitio web cliente, minutos (0 - sin actualizar):";
?>