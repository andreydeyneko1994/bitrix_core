<?
$MESS["APACHE_ROOT_TIP"] = "El catalogo que contiene los archivos de configuración del sitio web virtual para Apache";
$MESS["CONTROLLER_URL_TIP"] = "Especificar el URL del controlador del sitio web";
$MESS["DIR_PERMISSIONS_TIP"] = "Especificar los permisos para una carpeta que contenga sitios web alquilados";
$MESS["FILE_PERMISSIONS_TIP"] = "Especificar los permisos para archivos de sitios web rentados";
$MESS["MEMORY_LIMIT_TIP"] = "Limite de memoria para los sitios web rentados";
$MESS["MYSQL_DB_PATH_TIP"] = "La ruta al archivo que contiene la base de datos inicial de almacenamiento";
$MESS["MYSQL_PASSWORD_TIP"] = "Contraseña ce usuario que tenga permiso para la creación de base de datos en MySQL";
$MESS["MYSQL_PATH_TIP"] = "La ruta al archivo ejecutable del MySQL";
$MESS["MYSQL_USER_TIP"] = "Nombre de usuario que tenga permiso para la creación de base de datos en MySQL";
$MESS["NGINX_ROOT_TIP"] = "El catalogo que contiene los archivos de configuración del sitio web virtual para nginx";
$MESS["PATH_PUBLIC_TIP"] = "Publicar archivos de sitios web rentados";
$MESS["PATH_VHOST_TIP"] = "Especificar la ruta completa a una carpeta que contenga sitios web alquilados";
$MESS["RELOAD_FILE_TIP"] = "El archivo principal para provocar el reinicio del servidor web";
$MESS["SET_TITLE_TIP"] = "Establecer el titulo de la página.";
$MESS["URL_SUBDOMAIN_TIP"] = "Especificar el nivel superior del subdominio";
?>