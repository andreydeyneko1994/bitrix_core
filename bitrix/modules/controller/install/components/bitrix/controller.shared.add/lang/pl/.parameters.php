<?
$MESS["CSA_PARAM_APACHE_ROOT"] = "Katalog zawierający konfiguracyjne pliki Apache wirtualnych stron";
$MESS["CSA_PARAM_CONTROLLER_URL"] = "Sterownik URL";
$MESS["CSA_PARAM_DIR_PERMISSIONS"] = "Uprawnienia dla folderów zawierających wynajęte strony";
$MESS["CSA_PARAM_FILE_PERMISSIONS"] = "Uprawnienia dla plików wynajętych stron";
$MESS["CSA_PARAM_MEMORY_LIMIT"] = "Limit pamięci dla wynajętych stron";
$MESS["CSA_PARAM_MYSQL_DB_PATH"] = "Ścieżka do pliku śmietnika wstępnej bazy danych";
$MESS["CSA_PARAM_MYSQL_PASSWORD"] = "Hasło użytkownika MySQL z uprawnieniem tworzenia bazy danych";
$MESS["CSA_PARAM_MYSQL_PATH"] = "Scieżka do wykonywalnego pliku mysql";
$MESS["CSA_PARAM_MYSQL_USER"] = "Login użytkownika MySQL z uprawnieniem tworzenia bazy danych";
$MESS["CSA_PARAM_NGINX_ROOT"] = "Katalog zawierający konfiguracyjne pliki nginx wirtualnych stron";
$MESS["CSA_PARAM_PATH_PUBLIC"] = "Publiczne pliki wynajętych stron";
$MESS["CSA_PARAM_PATH_VHOST"] = "Pełna ścieżka do folderu z wynajętymi stronami";
$MESS["CSA_PARAM_REGISTER_IMMEDIATE"] = "Zarejestruj stronę po utworzeniu";
$MESS["CSA_PARAM_RELOAD_FILE"] = "Oznaczony plik do wyzwalania restartu serwera WWW";
$MESS["CSA_PARAM_URL_SUBDOMAIN"] = "Domena wysokiego poziomu";
?>