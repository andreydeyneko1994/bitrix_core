<?
$MESS["ACCESS_RESTRICTION_TIP"] = "Limiter l'accès";
$MESS["ACTION_TIP"] = "Action";
$MESS["COMMAND_TIP"] = "Commande";
$MESS["GROUP_PERMISSIONS_TIP"] = "Les groupes dont les utilisateurs sont autorisés à se charger de la gestion";
$MESS["SEPARATOR_TIP"] = "Diviseur de champs";
$MESS["SITE_URL_TIP"] = "Adresse du site";
?>