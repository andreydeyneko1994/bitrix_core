<?
$MESS["CC_BCSC_DELETE_ERROR"] = "Error eliminando el sitio web #ID#.";
$MESS["CC_BCSC_EMAIL_ERROR"] = "Error cambiando el e-mail del administrador del sitio web #ID#: #MESSAGE#";
$MESS["CC_BCSC_ERROR_MODULE"] = "El modulo de Controlador del sitio no esta instalado";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "Error cambiando la contraseña del sitio web #ID#: #MESSAGE#";
$MESS["CC_BCSC_RESERVE_ERROR"] = "Error reservando el sitio web: #MESSAGE#.";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "Un sitio web con esta dirección ya existe.";
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "El URL del sitio web esta desaparecido o es incorrecto.";
$MESS["CC_BCSC_TITLE"] = "Componente de Administración del sitio web.";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "Grupo desconocido";
$MESS["CC_BCSC_UPDATE_ERROR"] = "Error actualizando el sitio web #ID#: #MESSAGE# ";
?>