<?
$MESS["ACCESS_RESTRICTION_TIP"] = "Acceso restringido";
$MESS["ACTION_TIP"] = "Acción";
$MESS["COMMAND_TIP"] = "Comando";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especificar los grupos de usuarios con permisos de administración";
$MESS["SEPARATOR_TIP"] = "Separador del campo";
$MESS["SITE_URL_TIP"] = "URL del sitio web";
?>