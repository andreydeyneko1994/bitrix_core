<?
$MESS["CP_BCSC_ACCESS_RESTRICTION"] = "Ogranicz dostęp";
$MESS["CP_BCSC_ACTION"] = "Działanie";
$MESS["CP_BCSC_BY_GROUP"] = "Według grupy";
$MESS["CP_BCSC_BY_IP"] = "Według IP";
$MESS["CP_BCSC_COMMAND"] = "Komenda";
$MESS["CP_BCSC_GROUP_PERMISSIONS"] = "Zezwól na zarządzanie połączeniem dla użytkownika grup";
$MESS["CP_BCSC_IP_PERMISSIONS"] = "Zezwól na zarządzanie połączeniem przez IP";
$MESS["CP_BCSC_SEPARATOR"] = "Pole separatora";
$MESS["CP_BCSC_SITE_URL"] = "URL Strony";
?>