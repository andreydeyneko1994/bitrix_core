<?
$MESS["BPCRIA_NO_MODULE"] = "El modulo de Controlador del sitio web no esta instalado";
$MESS["BPCRIA_SITES_FILTER_ALL"] = "Todos los sitios web";
$MESS["BPCRIA_SITES_FILTER_GROUPS"] = "Sitios web de grupos específicos";
$MESS["BPCRIA_SITES_FILTER_SITES"] = "Los sitios web seleccionados de un grupo";
$MESS["BPCRIA_SITES_FILTER_TYPE"] = "Tipo de selección del sitio web";
$MESS["BPCRIA_SITES_GROUPS"] = "Grupos de sitios web controlados";
$MESS["BPCRIA_SITES_SITES"] = "Sitios web";
$MESS["BPCRIA_SYNC_IMMEDIATE"] = "inmediantamente";
$MESS["BPCRIA_SYNC_TASKS"] = "utilizando tareas de cliente remoto";
$MESS["BPCRIA_SYNC_TIME"] = "Sincronizar los elementos";
?>