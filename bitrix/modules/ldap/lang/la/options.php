<?
$MESS["LDAP_BITRIXVM_BLOCK"] = "Redirigir la autenticación NTLM a Puertos 8890 y 8891";
$MESS["LDAP_BITRIXVM_HINT"] = "Especifique aquí la subred cuyos usuarios serán redirigidos al autenticar mediante NTLM.<br> Por ejemplo: <b>192.168.1.0/24</b> o <b>192.168.1.0/255.255.255.0</b>.<br>Separar varios rangos de IP con un punto y coma.<br> Deje el campo vacío para redirigir todos los usuarios.";
$MESS["LDAP_BITRIXVM_NET"] = "Restringir la redirección de NTLM para esta subred:";
$MESS["LDAP_BITRIXVM_SUPPORT"] = "Redirigir la autenticación NTLM";
$MESS["LDAP_CURRENT_USER"] = "Conexión de usuario actual para la autorización NTLM (dominio\\login):";
$MESS["LDAP_CURRENT_USER_ABS"] = "Indefinido";
$MESS["LDAP_DEFAULT_NTLM_SERVER"] = "Dominio predeterminado del servidor:";
$MESS["LDAP_DUPLICATE_LOGIN_USER"] = "Crear un usuario incluso si existe un usuario con nombre de inicio de sesión especificado:";
$MESS["LDAP_NOT_USE_DEFAULT_NTLM_SERVER"] = "No utilice";
$MESS["LDAP_OPTIONS_DEFAULT_EMAIL"] = "Dirección de correo predeterminada del usuario (si no está especificada):";
$MESS["LDAP_OPTIONS_GROUP_LIMIT"] = "Registros máximo de concurrentes LDAP:";
$MESS["LDAP_OPTIONS_NEW_USERS"] = "Crear nuevos usuarios una vez que tuvo éxito la primera conexión:";
$MESS["LDAP_OPTIONS_NTLM_VARNAME"] = "variable para contener NTLM de usuario de acceso PHP:";
$MESS["LDAP_OPTIONS_RESET"] = "Resetear";
$MESS["LDAP_OPTIONS_SAVE"] = "Guardar";
$MESS["LDAP_OPTIONS_USE_NTLM"] = "Usar autorización NTLM ";
$MESS["LDAP_OPTIONS_USE_NTLM_MSG"] = "Nota: la autorización NTLM requiere que su propia configuración la relacionada al módulo del servidor web y especificar la autorización de dominio NTLM en el portal de las configuraciones del servidor AD.";
$MESS["LDAP_WITHOUT_PREFIX"] = "Compruebe la autenticación en todos los servidores LDAP si la sesión no incluya prefijo";
$MESS["LDAP_WRONG_NET_MASK"] = "Marca y dirección de subred de autenticación NTLM son incorrectas.<br> Utilice el siguiente formato:<br> subnet/mask <br> xxx.xxx.xxx.xxx/xxx.xxx.xxx.xxx <br> xxx.xxx.xxx.xxx/xx<br>Separar varios rangos de IP con un punto y coma.";
?>