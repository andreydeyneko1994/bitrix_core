<?
$MESS["LDAP_INSTALL_BACK"] = "Regresar al administrador del módulo";
$MESS["LDAP_INSTALL_TITLE"] = "Instalación del módulo conector AD/LDAP";
$MESS["LDAP_MODULE_DESC"] = "Módulo conector para AD/LDAP";
$MESS["LDAP_MODULE_NAME"] = "Conector AD/LDAP ";
$MESS["LDAP_MOD_INST_ERROR"] = "Error al instalar el módulo conector AD/LDAP";
$MESS["LDAP_MOD_INST_ERROR_PHP"] = "Para el correcto funcionamiento del módulo PHP con el módulo de LDAP debe ser instalado. Entrar en contacto con el administrador de sistema por favor.";
$MESS["LDAP_UNINSTALL_COMPLETE"] = "Desintalación completa.";
$MESS["LDAP_UNINSTALL_DEL"] = "Desinstalar";
$MESS["LDAP_UNINSTALL_ERROR"] = "Error al eliminar:";
$MESS["LDAP_UNINSTALL_SAVEDATA"] = "Para guardar los datos almacenados en la base de datos, haga check en \"guardar tablas\"";
$MESS["LDAP_UNINSTALL_SAVETABLE"] = "Guardar tabla";
$MESS["LDAP_UNINSTALL_TITLE"] = "Desinstalar el módulo conector AD/LDAP ";
$MESS["LDAP_UNINSTALL_WARNING"] = "Peligro¡ El módulo será desintalado.";
?>