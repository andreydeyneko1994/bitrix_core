<?
$MESS["LDAP_BITRIXVM_BLOCK"] = "Przekieruj uwierzytelnienie NTLM do portów 8890 i 8891";
$MESS["LDAP_BITRIXVM_HINT"] = "Określ podsieci, których użytkownicy będą przekierowywani podczas uwierzytelniania za pomocą NTLM.<br> Na przykład: <b>192.168.1.0/24</b> or <b>192.168.1.0/255.255.255.0</b>.<br>Wiele zakresów IP oddziel średnikami.<br> Pozostaw to pole puste, aby przekierować wszystkich użytkowników.";
$MESS["LDAP_BITRIXVM_NET"] = "Ogranicz przekierowanie NTLM do tej podsieci:";
$MESS["LDAP_BITRIXVM_SUPPORT"] = "Przekieruj uwierzytelnienie NTLM";
$MESS["LDAP_CURRENT_USER"] = "Bieżącegy login użytkownika dla uwierzytelniania NTLM. (domena\\login):";
$MESS["LDAP_CURRENT_USER_ABS"] = "Niezdefiniowany";
$MESS["LDAP_DEFAULT_NTLM_SERVER"] = "Domyślny serwer domen:";
$MESS["LDAP_NOT_USE_DEFAULT_NTLM_SERVER"] = "Nie używaj";
$MESS["LDAP_OPTIONS_DEFAULT_EMAIL"] = "Domyślny adres e-mail użytkownika (jeśli nie określono):";
$MESS["LDAP_OPTIONS_GROUP_LIMIT"] = "Maksymalna liczba wpisów, które mogą być zwrócone na pojedynczej operacji wyszukiwania LDAP:";
$MESS["LDAP_OPTIONS_NEW_USERS"] = "Utwórz nowe konto użytkownika na pierwsze pomyślne logowanie";
$MESS["LDAP_OPTIONS_NTLM_VARNAME"] = "Zmienne PHP zawierają login użytkownika NTLM (najczęściej REMOTE_USER):";
$MESS["LDAP_OPTIONS_RESET"] = "Wyczyść";
$MESS["LDAP_OPTIONS_SAVE"] = "Zapisz";
$MESS["LDAP_WITHOUT_PREFIX"] = "Sprawdź uwierzytelnianie na wszystkich dostępnych serwerach LDAP, jeśli logowanie nie uwzględnia prefiksu";
$MESS["LDAP_WRONG_NET_MASK"] = "Adres podsieci uwierzytelniania NTLM i oznaczenie są niepoprawne.<br> Użyj następującego formatu:<br> subnet/mask <br> xxx.xxx.xxx.xxx/xxx.xxx.xxx.xxx <br> xxx.xxx.xxx.xxx/xx<br>Oddziel kilka zakresów IP za pomocą średników.";
?>