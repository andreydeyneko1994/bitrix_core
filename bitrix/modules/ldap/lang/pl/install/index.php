<?
$MESS["LDAP_INSTALL_BACK"] = "Wróć do modułu zarządzania";
$MESS["LDAP_INSTALL_TITLE"] = "Instalacja modułu łącznika dla AD/LDAP";
$MESS["LDAP_MODULE_DESC"] = "Moduł łącznika AD/LDAP";
$MESS["LDAP_MODULE_NAME"] = "Złącze AD/LDAP";
$MESS["LDAP_MOD_INST_ERROR"] = "Błąd podczas instalacji modułu łącznika dla AD/LDAP";
$MESS["LDAP_MOD_INST_ERROR_PHP"] = "Dla poprawnego funkcjonowania modułu PHP z LDAP moduł powinien być zainstalowany. Skontaktuj się z administratorem systemu.";
$MESS["LDAP_UNINSTALL_COMPLETE"] = "Odinstalowywanie zakończone.";
$MESS["LDAP_UNINSTALL_DEL"] = "Dezinstalacja";
$MESS["LDAP_UNINSTALL_ERROR"] = "Usuwanie błędu:";
$MESS["LDAP_UNINSTALL_SAVETABLE"] = "Zabisz Tabele";
$MESS["LDAP_UNINSTALL_TITLE"] = "Odinstalowywanie modułu łącznika AD/LDAP";
$MESS["LDAP_UNINSTALL_WARNING"] = "Ostrzeżenie! Moduł zostanie odinstalowany.";
?>