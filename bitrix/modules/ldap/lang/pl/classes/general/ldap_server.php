<?
$MESS["LDAP_ERR_BASE_DN"] = "Korzeń drzewa (bazowa nazwa wyróżniająca)";
$MESS["LDAP_ERR_EMPTY"] = "Pole jest puste:";
$MESS["LDAP_ERR_GROUP_ATTR"] = "Atrybut identyfikatora grupy";
$MESS["LDAP_ERR_GROUP_FILT"] = "Filter grupy użytkownika";
$MESS["LDAP_ERR_NAME"] = "Nazwa";
$MESS["LDAP_ERR_PORT"] = "Porty serwerów AD/LDAP";
$MESS["LDAP_ERR_SERVER"] = "Adresy serwerów AD/LDAP";
$MESS["LDAP_ERR_USER_ATTR"] = "Atrybut identyfikatora użytkownika";
$MESS["LDAP_ERR_USER_FILT"] = "Filtr użytkownika";
?>