<?
$MESS["LDAP_ADMIN_ACT"] = "Akt.";
$MESS["LDAP_ADMIN_ACTIONS"] = "Działania";
$MESS["LDAP_ADMIN_CHANGE"] = "Zmodyfikowane ustawienia serwera";
$MESS["LDAP_ADMIN_CHANGE_LINK"] = "Modyfikuj";
$MESS["LDAP_ADMIN_CODE"] = "Kod podatku";
$MESS["LDAP_ADMIN_DEL_ALT"] = "Usuń serwer";
$MESS["LDAP_ADMIN_DEL_CONF"] = "Serwer zostanie usunięty i wszyscy użytkownicy zarejestrowani przez to nie będzie możliwa już autoryzacja. Usunąć mimo wszystko?";
$MESS["LDAP_ADMIN_DEL_ERR"] = "Błąd podczas usuwania ustawień serwera.";
$MESS["LDAP_ADMIN_DEL_LINK"] = "Usuń";
$MESS["LDAP_ADMIN_EMPTY"] = "Lista jest pusta";
$MESS["LDAP_ADMIN_F_ACT"] = "Aktywne";
$MESS["LDAP_ADMIN_F_ACT_ANY"] = "(jakikolwiek)";
$MESS["LDAP_ADMIN_F_NAME"] = "Nazwa";
$MESS["LDAP_ADMIN_NAME"] = "Nazwa";
$MESS["LDAP_ADMIN_NAVSTRING"] = "Serwery";
$MESS["LDAP_ADMIN_SERV"] = "Serwer";
$MESS["LDAP_ADMIN_SYNC"] = "Synchronizuj";
$MESS["LDAP_ADMIN_SYNC_LAST"] = "Ostatnio zsynchronizowano";
$MESS["LDAP_ADMIN_SYNC_PERIOD"] = "Okres synchronizacji";
$MESS["LDAP_ADMIN_TITLE"] = "Active Directory / ustawienia serwera LDAP";
$MESS["LDAP_ADMIN_TOTAL"] = "Suma:";
$MESS["LDAP_ADMIN_TSTAMP"] = "Zmodyfikowany";
$MESS["SAVE_ERROR"] = "Błąd podczas aktualizowania rekordu #";
?>