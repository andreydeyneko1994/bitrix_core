<?
$MESS["LDAP_ADMIN_ACT"] = "Act.";
$MESS["LDAP_ADMIN_ACTIONS"] = "Actions";
$MESS["LDAP_ADMIN_CHANGE"] = "Modifier les paramètres du site";
$MESS["LDAP_ADMIN_CHANGE_LINK"] = "Éditer";
$MESS["LDAP_ADMIN_CODE"] = "Code à symboles";
$MESS["LDAP_ADMIN_DEL_ALT"] = "Supprimer le serveur";
$MESS["LDAP_ADMIN_DEL_CONF"] = "Le serveur sera supprimé et tous les utilisateurs qui s'enregistrent à l'aide de lui ne pourront pas être autorisés. Effectuer la suppression ?";
$MESS["LDAP_ADMIN_DEL_ERR"] = "Erreur d'élimination du serveur.";
$MESS["LDAP_ADMIN_DEL_LINK"] = "Supprimer";
$MESS["LDAP_ADMIN_EMPTY"] = "La liste est vide";
$MESS["LDAP_ADMIN_F_ACT"] = "Actif(ve)";
$MESS["LDAP_ADMIN_F_ACT_ANY"] = "(n'importe lesquel(el)s)";
$MESS["LDAP_ADMIN_F_NAME"] = "Dénomination";
$MESS["LDAP_ADMIN_NAME"] = "Dénomination";
$MESS["LDAP_ADMIN_NAVSTRING"] = "Serveurs";
$MESS["LDAP_ADMIN_SERV"] = "Adresse du serveur";
$MESS["LDAP_ADMIN_SYNC"] = "Importation";
$MESS["LDAP_ADMIN_SYNC_LAST"] = "Dernière synchronisation";
$MESS["LDAP_ADMIN_SYNC_PERIOD"] = "Période de synchronisation";
$MESS["LDAP_ADMIN_TITLE"] = "Active Directory / serveurs LDAP";
$MESS["LDAP_ADMIN_TOTAL"] = "Au total : ";
$MESS["LDAP_ADMIN_TSTAMP"] = "Modifié";
$MESS["SAVE_ERROR"] = "Erreur de modification de note #";
?>