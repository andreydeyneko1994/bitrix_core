<?
$MESS["LDAP_INSTALL_BACK"] = "Revenir à la commande des modules";
$MESS["LDAP_INSTALL_TITLE"] = "Installation du module d'intégration AD/LDAP";
$MESS["LDAP_MODULE_DESC"] = "Module de travail avec Active Directory et LDAP.";
$MESS["LDAP_MODULE_NAME"] = "AD/LDAP connecteur";
$MESS["LDAP_MOD_INST_ERROR"] = "Erreur d'installation du module d'intégration AD/LDAP";
$MESS["LDAP_MOD_INST_ERROR_PHP"] = "Pour le bon fonctionnement du module PHP avec le module LDAP doit être installé. S'il vous plaît contactez votre administrateur système.";
$MESS["LDAP_UNINSTALL_COMPLETE"] = "La suppression est terminée";
$MESS["LDAP_UNINSTALL_DEL"] = "Supprimer";
$MESS["LDAP_UNINSTALL_ERROR"] = "Erreurs lors de la suppression : ";
$MESS["LDAP_UNINSTALL_SAVEDATA"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Enregistrer les tables&quot;";
$MESS["LDAP_UNINSTALL_SAVETABLE"] = "Enregistrer les tables";
$MESS["LDAP_UNINSTALL_TITLE"] = "Désinstallation du module d'intégration AD/LDAP";
$MESS["LDAP_UNINSTALL_WARNING"] = "Attention ! Le module sera supprimé du système.";
?>