<?
$MESS["LDAP_ERR_BASE_DN"] = "Racine de l'arbre (Base DN)";
$MESS["LDAP_ERR_EMPTY"] = "Champ non renseigné:";
$MESS["LDAP_ERR_GROUP_ATTR"] = "Attribut d'ID du groupe";
$MESS["LDAP_ERR_GROUP_FILT"] = "Filtre pour les groupes d'utilisateurs";
$MESS["LDAP_ERR_NAME"] = "Dénomination";
$MESS["LDAP_ERR_PORT"] = "Le port du serveur AD/LDAP";
$MESS["LDAP_ERR_SERVER"] = "Adresse du serveur AD/LDAP";
$MESS["LDAP_ERR_USER_ATTR"] = "Attribut d'ID d'utilisateur";
$MESS["LDAP_ERR_USER_FILT"] = "Filtre pour les utilisateurs";
?>