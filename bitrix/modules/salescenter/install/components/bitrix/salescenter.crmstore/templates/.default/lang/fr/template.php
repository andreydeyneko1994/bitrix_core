<?php
$MESS["SC_CRM_STORE_CONTAINER_CONTENT_2"] = "Le CRM est maintenant une plateforme de vente en ligne entièrement fonctionnelle, avec des moyens de paiement, des reçus et des livraisons contemporains. Un processus de paiement simple permet de s'assurer que le client finalise sa commande.<br>Activez CRM.Payment et regardez la conversion augmenter !";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Détails";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Commencez à vendre";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE_2"] = "Regardez cette vidéo pour apprendre <br>comment CRM.Payment vous aidera à vendre plus";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "Une nouvelle ère pour la vente en ligne";
$MESS["SC_CRM_STORE_TITLE_2"] = "CRM.Paiements";
