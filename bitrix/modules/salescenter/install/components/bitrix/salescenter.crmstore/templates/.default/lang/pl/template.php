<?php
$MESS["SC_CRM_STORE_CONTAINER_CONTENT_2"] = "CRM jest teraz w pełni funkcjonalną platformą sprzedaży online ze współczesnymi metodami płatności, pokwitowania i dostawy. Prosty proces płatności zapewnia, że klient zrealizuje zamówienie.<br>Włącz moduł CRM.Payment i obserwuj wzrost konwersji!";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Szczegóły";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Zacznij sprzedawać";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE_2"] = "Obejrzyj wideo, aby dowiedzieć się <br>w jaki sposób moduł CRM.Payment zwiększa sprzedaż";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "Nowa era sprzedaży online";
$MESS["SC_CRM_STORE_TITLE_2"] = "CRM.Płatność";
