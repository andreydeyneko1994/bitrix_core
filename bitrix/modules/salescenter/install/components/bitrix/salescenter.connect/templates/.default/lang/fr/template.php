<?
$MESS["SALESCENTER_CONNECT"] = "Connecter";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_DESCRIPTION"] = "Fournissez une assistance à vos clients en discutant avec eux dans le chat de leur réseau social préféré à partir de votre Bitrix24. Envoyez des FAQ, des pages de détails sur les marchandises et des formulaires directement au chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_HOW_DESCRIPTION"] = "Cliquez sur le bouton \"Ventes dans le chat\" dans la fenêtre de chat Bitrix24 pour envoyer une page requise au client.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_TITLE"] = "Assistance rapide";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_DESCRIPTION"] = "Recevez des paiements et envoyez des liens vers les pages de marchandises à n'importe quel chat de réseaux sociaux et de messagerie instantanée depuis votre Bitrix24. Laissez vos clients profiter d'un paiement rapide sans quitter le chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_TITLE"] = "Paiements dans le chat";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_DESCRIPTION"] = "Recevez des paiements et envoyez des liens vers les pages des marchandises en utilisant SMS. Effectuez des paiements facilement accessibles à vos clients.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_HOW_DESCRIPTION"] = "Pour envoyer des informations d'entreprise ou de commande par SMS, utilisez le formulaire de l'élément. Cliquez sur l'icône \"Paiements par SMS \" et sélectionnez la page que vous voulez envoyer. Le lien de la page sera ajouté au texte du message SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_TITLE"] = "Paiements par SMS";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_DESCRIPTION"] = "Les clients pourront facilement réserver vos services et événements, sélectionner l'heure souhaitée et effectuer un paiement à l'aide de chats des réseaux sociaux et des messageries instantanées Envoyez simplement un formulaire CRM de votre Bitrix24 au chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_HOW_DESCRIPTION"] = "Cliquez sur le bouton \"Ventes dans le chat\" dans la fenêtre de chat Bitrix24 pour envoyer un formulaire CRM au client. Cette option est disponible dans la version web ainsi que dans l'application de bureau.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_TITLE"] = "Réservation des ressources dans le chat";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_DESCRIPTION"] = "Les clients pourront facilement réserver vos services et événements à l'aide de messages SMS. Envoyez simplement un formulaire CRM de votre Bitrix24 au chat immédiatement après l'appel téléphonique.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_HOW_DESCRIPTION"] = "Pour envoyer le formulaire de réservation par SMS, utilisez le formulaire du client. Cliquez sur l'icône \"Paiements par SMS\" et sélectionnez le formulaire que vous voulez envoyer. Le lien du formulaire sera ajouté au texte du message SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_TITLE"] = "Réservation des ressources par SMS";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW"] = "Comment ca fonctionne";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_DESCRIPTION"] = "Vous travaillerez avec vos clients en utilisant le chat en direct de Bitrix24. Vous pouvez envoyer des pages et recevoir des commandes dans le navigateur ou sur l'application de bureau.";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK"] = "Comment fonctionnent les ventes avec chat activé ?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK_SMS"] = "Plus d'informations";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_SOCIAL"] = "Comment connecter les réseaux sociaux et messageries à votre Bitrix24 ?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_TITLE"] = "Ventes avec chat activé";
?>