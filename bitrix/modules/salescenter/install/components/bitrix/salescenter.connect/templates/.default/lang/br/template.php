<?
$MESS["SALESCENTER_CONNECT"] = "Conectar";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_DESCRIPTION"] = "Preste assistência aos seus clientes, conversando com eles no bate-papo da sua rede social preferida, diretamente do seu Bitrix24. Envie perguntas frequentes, páginas detalhadas de produtos e formulários enquanto estiver conversando.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_HOW_DESCRIPTION"] = "Clique no botão \"Vendas por chat\" na janela de bate-papo Bitrix24 para enviar a página necessária para o cliente.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_TITLE"] = "Assistência rápida";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_DESCRIPTION"] = "Receba pagamentos e envie links para as páginas de produtos a quaisquer bate-papos de redes socias e mensageiros diretamente do seu Bitrix24. Permita que seus clientes desfrutem de pagamentos rápidos sem sair do chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_TITLE"] = "Pagamentos por chat";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_DESCRIPTION"] = "Receba pagamentos e envie links para as páginas de produtos usando mensagens SMS. Torne os pagamentos facilmente acessíveis a seus clientes.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_HOW_DESCRIPTION"] = "Para enviar informações da empresa ou do pedido por mensagem SMS, use a página detalhada do item. Clique no ícone \"Pagamentos por SMS\" e selecione a página que deseja enviar. O link da página será adicionado ao texto da mensagem SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_TITLE"] = "Pagamentos por SMS";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_DESCRIPTION"] = "Forneça aos seus clientes uma maneira rápida e fácil de agendar uma consulta ou se registrar para um evento, selecionar o horário desejado e fazer um pagamento usando bate-papos de redes sociais ou mensageiros. Basta enviar um formulário CRM do seu Bitrix24 para o chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_HOW_DESCRIPTION"] = "Clique no botão \"Vendas por chat\" na janela de bate-papo Bitrix24 para enviar um formulário CRM para o cliente. Esta opção está disponível na versão web e no aplicativo desktop.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_TITLE"] = "Agendamento de consultas por chat";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_DESCRIPTION"] = "Forneça aos seus clientes uma maneira rápida e fácil de agendar uma consulta ou se registrar para um evento usando mensagens SMS. Basta enviar um formulário CRM logo após encerrar a ligação.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_HOW_DESCRIPTION"] = "Para enviar um formulário de agendamento de consultas via mensagem SMS, use a página detalhada do cliente. Clique no ícone \"Vendas por SMS\" e selecione o formulário que deseja enviar. O link do formulário será adicionado ao texto da mensagem SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_TITLE"] = "Agendamento de consultas por SMS";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW"] = "Como funciona";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_DESCRIPTION"] = "Você trabalhará com seus clientes usando o Bitrix24 Live Chat. Você pode enviar páginas e receber pedidos no navegador ou aplicativo de desktop.";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK"] = "Como funcionam as vendas ativadas por bate-papo?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK_SMS"] = "Saiba mais";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_SOCIAL"] = "Como conectar redes sociais e aplicativos de mensagens ao seu Bitrix24?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_TITLE"] = "Vendas ativadas por bate-papo";
?>