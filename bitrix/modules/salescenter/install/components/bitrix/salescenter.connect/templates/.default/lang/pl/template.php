<?
$MESS["SALESCENTER_CONNECT"] = "Podłącz";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_DESCRIPTION"] = "Rozmawiaj z klientami na preferowanym czacie bezpośrednio w Bitrix24. Wysyłaj często zadawane pytania, strony ze szczegółami produktu i formularze podczas rozmowy.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_HOW_DESCRIPTION"] = "Kliknij przycisk „Sprzedaż na czacie” w oknie czatu Bitrix24, aby wysłać wymaganą stronę do klienta.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_TITLE"] = "Szybka pomoc";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_DESCRIPTION"] = "Odbieraj płatności i wysyłaj strony produktów do dowolnych czatów i komunikatorów bezpośrednio z Bitrix24. Twoi klienci mogą wybrać wygodny system płatności i szybko zapłacić za towary i usługi na czacie.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_TITLE"] = "Płatności na czacie";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_DESCRIPTION"] = "Odbieraj płatności i wysyłaj linki do stron produktów za pomocą wiadomości SMS. Twoi klienci mogą wybrać wygodny system płatności i szybko zapłacić za towary i usługi.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_HOW_DESCRIPTION"] = "Aby wysłać informacje o firmie lub zamówieniu za pomocą wiadomości SMS, skorzystaj z formularzu elementu. Kliknij ikonę \"Płatności za pomocą SMS\" i wybierz stronę, którą chcesz wysłać. Link do strony będzie dodany do tekstu wiadomości SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_TITLE"] = "Płatności za pomocą SMS";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_DESCRIPTION"] = "Zapewnij swoim klientom szybki i łatwy sposób na umówienie spotkania lub zarejestrowanie się na wydarzenie. Wybierz czas i dokonaj płatności za pomocą czatów lub komunikatorów. Wystarczy wysłać formularz CRM ze swojego Bitrix24 na czat.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_HOW_DESCRIPTION"] = "Kliknij przycisk „Sprzedaż na czacie” w oknie czatu Bitrix24, aby wysłać formularz CRM do klienta. Ta opcja jest dostępna w przeglądarce, a także w aplikacji desktopowej.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_TITLE"] = "Spotkanie za pomocą czatu";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_DESCRIPTION"] = "Zapewnij swoim klientom szybki i łatwy sposób na umówienie spotkania lub zarejestrowanie się na wydarzenie za pomocą wiadomości SMS. Wystarczy wysłać formularz CRM po rozłączeniu się.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_HOW_DESCRIPTION"] = "Aby wysłać formularz spotkania za pomocą wiadomości SMS, skorzystaj z formularzu danych klienta. Kliknij ikonę \"Sprzedaż za pomocą SMS\" i wybierz formularz, który chcesz wysłać. Link do formularza zostanie dodany do tekstu wiadomości SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_TITLE"] = "Spotkanie za pomocą SMS";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW"] = "Jak to działa";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_DESCRIPTION"] = "Będziesz współpracować ze swoimi klientami za pomocą czatu na żywo Bitrix24. Możesz wysyłać strony i odbierać zamówienia w przeglądarce lub aplikacji komputerowej.";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK"] = "Jak działa sprzedaż z możliwością czatu?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK_SMS"] = "Więcej informacji";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_SOCIAL"] = "Jak podłączyć sieci społecznościowe i komunikatory do Bitrix24?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_TITLE"] = "Sprzedaż z możliwością czatu";
?>