<?
$MESS["SALESCENTER_ORDERS_ADD_ORDER"] = "Novo pedido";
$MESS["SALESCENTER_ORDERS_LIMITS_MESSAGE"] = "Faça um upgrade para um dos planos comerciais para continuar processando pedidos e recebendo pagamentos por meio da Central de Vendas.";
$MESS["SALESCENTER_ORDERS_LIMITS_TITLE"] = "Você atingiu o número máximo de pedidos que pode criar usando a Central de Vendas.";
$MESS["SALESCENTER_SESSION_ERROR"] = "O ID da sessão não foi especificado";
?>