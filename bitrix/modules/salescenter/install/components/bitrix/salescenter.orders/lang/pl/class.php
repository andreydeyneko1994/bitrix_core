<?
$MESS["SALESCENTER_ORDERS_ADD_ORDER"] = "Nowe zamówienie";
$MESS["SALESCENTER_ORDERS_LIMITS_MESSAGE"] = "Rozszerz swoją subskrypcję do jednego z planów komercyjnych, aby kontynuować obsługę zamówień i otrzymywać płatności za pośrednictwem Centrum sprzedaży.";
$MESS["SALESCENTER_ORDERS_LIMITS_TITLE"] = "Osiągnięto maksymalną liczbę zamówień, które można utworzyć za pomocą Centrum sprzedaży.";
$MESS["SALESCENTER_SESSION_ERROR"] = "Nie określono ID sesji";
?>