<?
$MESS["SALESCENTER_ORDERS_ADD_ORDER"] = "Nuevo pedido";
$MESS["SALESCENTER_ORDERS_LIMITS_MESSAGE"] = "Actualice a uno de los planes comerciales para continuar con el manejo de pedidos y recibir pagos mediante el Centro de ventas.";
$MESS["SALESCENTER_ORDERS_LIMITS_TITLE"] = "Ya alcanzó el número máximo de pedidos que puede crear utilizando el Centro de ventas.";
$MESS["SALESCENTER_SESSION_ERROR"] = "El ID de la sesión no está especificado";
?>