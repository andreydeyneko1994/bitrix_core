<?
$MESS["SALESCENTER_USERCONSENT_ACTIVE"] = "Mostrar advertencias sobre la recopilación de datos personales del usuario";
$MESS["SALESCENTER_USERCONSENT_CONFIG_EDIT_AGREEMENT_MESSAGE"] = "Aceptar automáticamente el Acuerdo (establece como marcada el estado de la casilla de verificación inicial)";
$MESS["SALESCENTER_USERCONSENT_TITLE"] = "Acuerdo de términos y condiciones";
?>