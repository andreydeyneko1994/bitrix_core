<?
$MESS["SALESCENTER_USERCONSENT_ACTIVE"] = "Afficher un avertissement sur la collecte des données personnelles de l'utilisateur";
$MESS["SALESCENTER_USERCONSENT_CONFIG_EDIT_AGREEMENT_MESSAGE"] = "Accepter automatiquement le Contrat (règle l'état initial de la case à cocher sur cochée)";
$MESS["SALESCENTER_USERCONSENT_TITLE"] = "Conditions générales";
?>