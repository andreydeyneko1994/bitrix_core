<?
$MESS["SP_MENU_ITEM_PARAMS"] = "Paramètres";
$MESS["SP_MENU_ITEM_RESTRICTION"] = "Restrictions";
$MESS["SP_MENU_ITEM_RESTRICTION_SET"] = "Mes restrictions";
$MESS["SP_MENU_ITEM_SORT"] = "Trier";
$MESS["SP_PAYMENT_SUB_TITLE"] = "(paiement via #SUB_TITLE#)";
$MESS["SP_RP_CONFIRM_DEL_MESSAGE"] = "Voulez-vous vraiment supprimer cette restriction ?";
$MESS["SP_RP_DELETE_DESCR"] = "Supprimer";
$MESS["SP_RP_EDIT_DESCR"] = "Modifier";
$MESS["SP_SALESCENTER_SALE_ACCESS_DENIED"] = "Accès refusé.";
?>