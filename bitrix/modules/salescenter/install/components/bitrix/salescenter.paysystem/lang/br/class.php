<?
$MESS["SP_MENU_ITEM_PARAMS"] = "Parâmetros";
$MESS["SP_MENU_ITEM_RESTRICTION"] = "Restrições";
$MESS["SP_MENU_ITEM_RESTRICTION_SET"] = "Minhas restrições";
$MESS["SP_MENU_ITEM_SORT"] = "Classificar";
$MESS["SP_PAYMENT_SUB_TITLE"] = "(pagamento por #SUB_TITLE#)";
$MESS["SP_RP_CONFIRM_DEL_MESSAGE"] = "Você tem certeza que deseja excluir esta restrição?";
$MESS["SP_RP_DELETE_DESCR"] = "Excluir";
$MESS["SP_RP_EDIT_DESCR"] = "Editar";
$MESS["SP_SALESCENTER_SALE_ACCESS_DENIED"] = "Acesso negado.";
?>