<?
$MESS["SALESCENTER_FEATURE_MESSAGE"] = "W Twoim planie subskrypcji Centrum sprzedaży jest niedostępne. Aby móc korzystać z Centrum sprzedaży, rozszerz swoją subskrypcję do jednego z głównych planów komercyjnych";
$MESS["SALESCENTER_FEATURE_MODULE_ERROR"] = "Moduł „Centrum sprzedaży” nie jest zainstalowany";
$MESS["SALESCENTER_FEATURE_TITLE"] = "Dostępne wyłącznie w głównych planach komercyjnych";
$MESS["SALESCENTER_LIMITS_MESSAGE"] = "Gratulacje! <br />Za pomocą czatu na żywo i SMS-ów utworzyłeś/-aś 100 zamówień.<br /><br />Rozszerz swoją subskrypcję do jednego z planów komercyjnych, aby kontynuować obsługę zamówień i otrzymywać płatności za pośrednictwem Centrum sprzedaży.";
$MESS["SALESCENTER_LIMITS_TITLE"] = "Dostępne wyłącznie w wybranych planach komercyjnych";
?>