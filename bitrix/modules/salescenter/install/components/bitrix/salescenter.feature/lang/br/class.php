<?
$MESS["SALESCENTER_FEATURE_MESSAGE"] = "A Central de Vendas não está disponível para o seu plano de assinatura. Faça um upgrade para um dos principais planos comerciais para usar a Central de Vendas";
$MESS["SALESCENTER_FEATURE_MODULE_ERROR"] = "O módulo \"Central de Vendas\" não está instalado";
$MESS["SALESCENTER_FEATURE_TITLE"] = "Disponível apenas para os principais planos comerciais";
$MESS["SALESCENTER_LIMITS_MESSAGE"] = "Parabéns! <br />Você criou 100 pedidos usando o Bate-papo ao vivo e o SMS.<br /><br />Faça um upgrade para um dos planos comerciais para continuar processando pedidos e recebendo pagamentos por meio da Central de Vendas.";
$MESS["SALESCENTER_LIMITS_TITLE"] = "Disponível apenas em planos comerciais selecionados";
?>