<?php
$MESS["SC_COMPANY_CONTACTS_COMPANY_NAME"] = "Nome da empresa";
$MESS["SC_COMPANY_CONTACTS_COMPANY_PHONE"] = "Telefone da empresa";
$MESS["SC_COMPANY_CONTACTS_HOW_TO_WORK"] = "Como funciona";
$MESS["SC_COMPANY_CONTACTS_TITLE"] = "Contatos da empresa";
$MESS["SC_COMPANY_CONTACTS_YOUR_CONTACTS"] = "Seus contatos";
