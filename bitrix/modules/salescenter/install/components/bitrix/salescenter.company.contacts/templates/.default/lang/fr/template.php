<?php
$MESS["SC_COMPANY_CONTACTS_COMPANY_NAME"] = "Nom de l'entreprise";
$MESS["SC_COMPANY_CONTACTS_COMPANY_PHONE"] = "Téléphone de l'entreprise";
$MESS["SC_COMPANY_CONTACTS_HOW_TO_WORK"] = "Comment ça fonctionne";
$MESS["SC_COMPANY_CONTACTS_TITLE"] = "Contacts de l'entreprise";
$MESS["SC_COMPANY_CONTACTS_YOUR_CONTACTS"] = "Vos contacts";
