<?php
$MESS["SC_COMPANY_CONTACTS_COMPANY_NAME"] = "Nombre de la compañía";
$MESS["SC_COMPANY_CONTACTS_COMPANY_PHONE"] = "Teléfono de la compañía";
$MESS["SC_COMPANY_CONTACTS_HOW_TO_WORK"] = "Cómo funciona";
$MESS["SC_COMPANY_CONTACTS_TITLE"] = "Contactos de la compañía";
$MESS["SC_COMPANY_CONTACTS_YOUR_CONTACTS"] = "Sus contactos";
