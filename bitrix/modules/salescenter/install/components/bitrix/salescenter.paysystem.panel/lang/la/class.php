<?
$MESS["SPP_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["SPP_ACTIONBOX_APPS"] = "Marketplace";
$MESS["SPP_PAYSYSTEM_ADD"] = "Agregar un método de pago";
$MESS["SPP_PAYSYSTEM_APP_INTEGRATION_REQUIRED"] = "¿Necesita integración?";
$MESS["SPP_PAYSYSTEM_APP_RECOMMEND"] = "Recomendar";
$MESS["SPP_PAYSYSTEM_APP_SEE_ALL"] = "Ver todo";
$MESS["SPP_PAYSYSTEM_APP_TOTAL_APPLICATIONS"] = "Total de aplicaciones";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Pago en efectivo";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Otros métodos de pago";
$MESS["SPP_PAYSYSTEM_PAYPAL_TITLE"] = "Pago con PayPal";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurar #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_SKB_SKB_TITLE"] = "Método de pago rápido";
$MESS["SPP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_TITLE"] = "Tarjeta de crédito/débito";
?>