<?
$MESS["SPP_ACCESS_DENIED"] = "Acesso negado.";
$MESS["SPP_ACTIONBOX_APPS"] = "Marketplace";
$MESS["SPP_PAYSYSTEM_ADD"] = "Adicionar sistema de pagamento";
$MESS["SPP_PAYSYSTEM_APP_INTEGRATION_REQUIRED"] = "Precisa de integração?";
$MESS["SPP_PAYSYSTEM_APP_RECOMMEND"] = "Recomendar";
$MESS["SPP_PAYSYSTEM_APP_SEE_ALL"] = "Ver tudo";
$MESS["SPP_PAYSYSTEM_APP_TOTAL_APPLICATIONS"] = "Total de apps";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Pagamento em dinheiro";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Outros sistemas de pagamento";
$MESS["SPP_PAYSYSTEM_PAYPAL_TITLE"] = "Pagamento por PayPal";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurar #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_SKB_SKB_TITLE"] = "Sistema de pagamento rápido";
$MESS["SPP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_TITLE"] = "Cartão de crédito/débito";
?>