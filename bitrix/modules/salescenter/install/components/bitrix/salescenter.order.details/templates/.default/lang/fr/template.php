<?php
$MESS["SOD_COMMON_DISCOUNT"] = "Vous économisez";
$MESS["SOD_COMMON_SUM_NEW"] = "Total des articles";
$MESS["SOD_DELIVERY"] = "Livraison";
$MESS["SOD_FREE"] = "Gratuit";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "Paiement ##ACCOUNT_NUMBER# du #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "Paiement ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Total";
$MESS["SOD_TAX"] = "Taux d'imposition";
$MESS["SOD_TOTAL_WEIGHT"] = "Poids total";
$MESS["SOD_TPL_SUMOF"] = "montant de la commande";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "Télécharger";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "Ouvrir";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "Transférer";
$MESS["SPOD_DOCUMENT_TITLE"] = "Votre facture";
