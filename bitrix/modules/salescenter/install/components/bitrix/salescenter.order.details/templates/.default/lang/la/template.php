<?php
$MESS["SOD_COMMON_DISCOUNT"] = "Usted ahorra";
$MESS["SOD_COMMON_SUM_NEW"] = "Total de artículos";
$MESS["SOD_DELIVERY"] = "Entrega";
$MESS["SOD_FREE"] = "Gratis";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "Pago para ##ACCOUNT_NUMBER# del #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "Pago ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Total";
$MESS["SOD_TAX"] = "Tasa de impuestos";
$MESS["SOD_TOTAL_WEIGHT"] = "Peso total";
$MESS["SOD_TPL_SUMOF"] = "total del pedido";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "Descargar";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "Abrir";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "Reenviar";
$MESS["SPOD_DOCUMENT_TITLE"] = "Su factura";
