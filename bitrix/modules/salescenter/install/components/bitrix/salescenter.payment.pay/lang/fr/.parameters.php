<?
$MESS["SPP_ALLOW_PAYMENT_REDIRECT"] = "Rediriger vers la page de paiement une fois le système de paiement sélectionné";
$MESS["SPP_TEMPLATE_MODE"] = "Mode d'affichage";
$MESS["SPP_TEMPLATE_MODE_DARK_VALUE"] = "Sombre";
$MESS["SPP_TEMPLATE_MODE_LIGHT_VALUE"] = "Clair";
?>