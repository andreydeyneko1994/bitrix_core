<?php
$MESS["SPP_DEFAULT_TEMPLATE_DESCRIPTION"] = "Affiche les détails du paiement de la commande dans l'Espace des ventes";
$MESS["SPP_DEFAULT_TEMPLATE_NAME"] = "Détails du paiement de la commande dans l'Espace des ventes";
$MESS["SPP_NAME"] = "Espace personnel";
