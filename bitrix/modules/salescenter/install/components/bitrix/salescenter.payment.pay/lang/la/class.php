<?php
$MESS["SPOD_ACCESS_DENIED"] = "Acceso restringido";
$MESS["SPP_CANNOT_CREATE_PAYMENT"] = "No se puede crear el pago";
$MESS["SPP_ORDER_NOT_FOUND"] = "No se encontró el pedido";
$MESS["SPP_PAYMENT_NOT_FOUND"] = "No se encontró el pago";
$MESS["SPP_PAYSYSTEM_NOT_FOUND"] = "No se encontró el método de pago";
