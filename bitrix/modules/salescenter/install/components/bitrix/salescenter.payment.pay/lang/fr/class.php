<?php
$MESS["SPOD_ACCESS_DENIED"] = "Accès restreint";
$MESS["SPP_CANNOT_CREATE_PAYMENT"] = "Impossible de créer un paiement";
$MESS["SPP_ORDER_NOT_FOUND"] = "La commande est introuvable";
$MESS["SPP_PAYMENT_NOT_FOUND"] = "Paiement introuvable";
$MESS["SPP_PAYSYSTEM_NOT_FOUND"] = "Système de paiement introuvable";
