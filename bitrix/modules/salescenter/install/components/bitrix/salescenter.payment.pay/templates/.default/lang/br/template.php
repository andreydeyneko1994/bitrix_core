<?php
$MESS["SPP_CHECK_PRINT_TITLE"] = "O recibo ##ID# de #DATE_CREATE# será criado em breve";
$MESS["SPP_CHECK_TITLE"] = "Ver recibo ##CHECK_ID# de #DATE_CREATE#";
$MESS["SPP_EMPTY_TEMPLATE_FOOTER"] = "Selecione um sistema de pagamento diferente para pagar on-line";
$MESS["SPP_EMPTY_TEMPLATE_PAY_SYSTEM_NAME_FIELD"] = "Método de pagamento:";
$MESS["SPP_EMPTY_TEMPLATE_SUM_WITH_CURRENCY_FIELD"] = "Total do pedido:";
$MESS["SPP_EMPTY_TEMPLATE_TITLE"] = "Obrigado pelo seu pedido!";
$MESS["SPP_INFO_BUTTON"] = "Informações";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT"] = "Infelizmente, encontramos um erro. Selecione uma opção de pagamento diferente ou entre em contato com o representante de vendas.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_FOOTER"] = "Por favor, selecione uma opção de pagamento diferente ou entre em contato com o representante de vendas.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_HEADER"] = "Infelizmente, encontramos um erro.";
$MESS["SPP_PAID"] = "Pago";
$MESS["SPP_PAID_TITLE"] = "Pagamento ##ACCOUNT_NUMBER# de #DATE_INSERT#";
$MESS["SPP_PAY_BUTTON"] = "Pagar";
$MESS["SPP_PAY_RELOAD_BUTTON_NEW"] = "Selecionar outro método";
$MESS["SPP_SELECT_PAYMENT_TITLE_NEW_NEW"] = "Selecionar o método de pagamento";
$MESS["SPP_SUM"] = "Valor: #SUM#";
