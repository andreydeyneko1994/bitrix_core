<?php
$MESS["SPP_CHECK_PRINT_TITLE"] = "El recibo ##ID# del #DATE_CREATE# se generará en breve";
$MESS["SPP_CHECK_TITLE"] = "Ver el recibo ##CHECK_ID# del #DATE_CREATE#";
$MESS["SPP_EMPTY_TEMPLATE_FOOTER"] = "Seleccione un método de pago diferente para pagar en línea";
$MESS["SPP_EMPTY_TEMPLATE_PAY_SYSTEM_NAME_FIELD"] = "Método de pago:";
$MESS["SPP_EMPTY_TEMPLATE_SUM_WITH_CURRENCY_FIELD"] = "Total del pedido:";
$MESS["SPP_EMPTY_TEMPLATE_TITLE"] = "¡Gracias por hacer su pedido!";
$MESS["SPP_INFO_BUTTON"] = "Información";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT"] = "Desafortunadamente encontramos un error. Seleccione un método de pago diferente o póngase en contacto con el representante de ventas.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_FOOTER"] = "Seleccione una opción de pago diferente o póngase en contacto con el representante de ventas.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_HEADER"] = "Desafortunadamente encontramos un error.";
$MESS["SPP_PAID"] = "Pagado";
$MESS["SPP_PAID_TITLE"] = "Pago a ##ACCOUNT_NUMBER# el #DATE_INSERT#";
$MESS["SPP_PAY_BUTTON"] = "Pago";
$MESS["SPP_PAY_RELOAD_BUTTON_NEW"] = "Seleccione otro método";
$MESS["SPP_SELECT_PAYMENT_TITLE_NEW_NEW"] = "Seleccionar el método de pago";
$MESS["SPP_SUM"] = "Cantidad: #SUM#";
