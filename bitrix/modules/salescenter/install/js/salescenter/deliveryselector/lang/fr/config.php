<?php
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_ADD_MORE"] = "Ajouter plus";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CALCULATE"] = "CALCULER";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CALCULATE_UPDATE"] = "RECALCULER";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CALCULATING_LABEL"] = "Calcul en cours";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CALCULATING_REQUEST_SENT"] = "demande envoyée au service de livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CHANGE_RESPONSIBLE"] = "MODIFIER";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CLARIFY_ADDRESS"] = "Précisez l'adresse exacte";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CLIENT_DELIVERY_PRICE"] = "Coût de la livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_CLIENT_DELIVERY_PRICE_FREE"] = "GRATUITE";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_DELIVERY_DELIVERY"] = "Livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_DELIVERY_METHOD"] = "Méthode de livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_DELIVERY_SERVICE"] = "Service de livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_EXCLUDE_DELIVERY"] = "Exclure du total de la commande";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_EXPECTED_DELIVERY_PRICE"] = "Prix de livraison calculé";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_HEIGHT"] = "Hauteur";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_INCLUDE_DELIVERY"] = "Inclure dans le total de la commande";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_LENGTH"] = "Longueur";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_LENGTH_DIMENSION_UNIT"] = "cm";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_PROFITABLE"] = "Meilleur choix";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_RESPONSIBLE_MANAGER"] = "Responsable";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_SHIPPING_SERVICES"] = "services de livraison";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_TOTAL"] = "Total";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_UPDATE"] = "Recalculer";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_WEIGHT"] = "Poids";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_WEIGHT_UNIT"] = "kg";
$MESS["SALE_DELIVERY_SERVICE_SELECTOR_WIDTH"] = "Largeur";
$MESS["SALE_DELIVERY_SERVICE_SHIPMENT_ADDRESS_FROM_LABEL"] = "Récupérez la commande à";
$MESS["SALE_DELIVERY_SERVICE_SHIPMENT_ADDRESS_TO_LABEL"] = "Adresse de la livraison";
