<?php
$MESS["SC_STORE_SETTINGS_CANCEL"] = "Cancelar";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME"] = "El nombre de su empresa";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME_HINT"] = "Los clientes verán el nombre de su empresa en la página de pago";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE"] = "Teléfono de la compañía";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD"] = "Agregar";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD_SMALL"] = "agregar";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DEFAULT"] = "El primer número de teléfono que aparece en los detalles de la compañía";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DELIMETER"] = "seleccione el número de teléfono";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_EMPTY"] = "No se especificó el número de teléfono de la compañía";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_NUMBER"] = "Teléfono del contacto";
$MESS["SC_STORE_SETTINGS_COMPANY_TITLE"] = "Nombre de la compañía";
$MESS["SC_STORE_SETTINGS_SAVE"] = "Guardar";
