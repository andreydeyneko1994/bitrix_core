<?php
$MESS["SC_STORE_SETTINGS_CANCEL"] = "Annuler";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME"] = "Nom de votre entreprise";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME_HINT"] = "Les clients verront ce nom sur la page de paiement";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE"] = "Téléphone de l'entreprise";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD"] = "Ajouter";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD_SMALL"] = "ajouter";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DEFAULT"] = "Premier numéro de téléphone dans les détails de l'entreprise";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DELIMETER"] = "sélectionner le numéro de téléphone";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_EMPTY"] = "Pas de téléphone d'entreprise";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_NUMBER"] = "Téléphone de contact";
$MESS["SC_STORE_SETTINGS_COMPANY_TITLE"] = "Nom de l’entreprise";
$MESS["SC_STORE_SETTINGS_SAVE"] = "Enregistrer";
