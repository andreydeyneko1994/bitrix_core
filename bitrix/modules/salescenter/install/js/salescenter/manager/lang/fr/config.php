<?
$MESS["SALESCENTER_ACTION_ADD_CUSTOM_TITLE"] = "Ajouter une page personnalisée";
$MESS["SALESCENTER_MANAGER_ADD_URL_SUCCESS"] = "Page ajoutée";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_DESCRIPTION"] = "Ajoutez les informations de votre société et démarrez des ventes via messageries ou réseaux sociaux.";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_GO_BUTTON"] = "Modifier les informations de la société";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_TITLE"] = "Votre boutique en ligne a été créée !";
$MESS["SALESCENTER_MANAGER_COPY_URL_SUCCESS"] = "Lien copié dans le Presse-papiers";
$MESS["SALESCENTER_MANAGER_DELETE_URL_SUCCESS"] = "La page a été supprimée";
$MESS["SALESCENTER_MANAGER_ERROR_POPUP"] = "Une erreur est survenue";
$MESS["SALESCENTER_MANAGER_HIDE_URL_SUCCESS"] = "La page a été masquée";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_COMPLETE"] = "Une nouvelle page a été créée";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_POPUP_TITLE"] = "Créer une nouvelle page";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_WAIT"] = "Veuillez patienter, une nouvelle page est en cours de création";
$MESS["SALESCENTER_MANAGER_UPDATE_URL_SUCCESS"] = "La page a été mise à jour";
?>