<?
$MESS["SALESCENTER_ACTION_ADD_CUSTOM_TITLE"] = "Adicionar página personalizada";
$MESS["SALESCENTER_MANAGER_ADD_URL_SUCCESS"] = "Página adicionada";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_DESCRIPTION"] = "Adicione as informações da sua empresa e comece a vender por meio de aplicativos de mensagens e redes sociais.";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_GO_BUTTON"] = "Editar informações da empresa";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_TITLE"] = "Sua loja on-line foi criada!";
$MESS["SALESCENTER_MANAGER_COPY_URL_SUCCESS"] = "Link copiado para a Área de Transferência";
$MESS["SALESCENTER_MANAGER_DELETE_URL_SUCCESS"] = "A página foi excluída";
$MESS["SALESCENTER_MANAGER_ERROR_POPUP"] = "Ocorreu um erro";
$MESS["SALESCENTER_MANAGER_HIDE_URL_SUCCESS"] = "A página foi oculta";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_COMPLETE"] = "A nova página foi criada";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_POPUP_TITLE"] = "Criar nova página";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_WAIT"] = "Aguarde, a nova página está sendo criada";
$MESS["SALESCENTER_MANAGER_UPDATE_URL_SUCCESS"] = "A página foi atualizada";
?>