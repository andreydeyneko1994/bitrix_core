<?
$MESS["SALESCENTER_ACTION_ADD_CUSTOM_TITLE"] = "Agregar una página personalizada";
$MESS["SALESCENTER_MANAGER_ADD_URL_SUCCESS"] = "Página agregada";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_DESCRIPTION"] = "Agregue la información de su compañía y comience a vender mediante mensajeros y redes sociales.";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_GO_BUTTON"] = "Editar la información de la compañía";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_TITLE"] = "¡Su tienda online ha sido creada!";
$MESS["SALESCENTER_MANAGER_COPY_URL_SUCCESS"] = "Enlace copiado al Portapapeles";
$MESS["SALESCENTER_MANAGER_DELETE_URL_SUCCESS"] = "Se eliminó la página";
$MESS["SALESCENTER_MANAGER_ERROR_POPUP"] = "Se produjo un error";
$MESS["SALESCENTER_MANAGER_HIDE_URL_SUCCESS"] = "La página fue ocultada";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_COMPLETE"] = "Se creó una nueva página";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_POPUP_TITLE"] = "Crear una nueva página";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_WAIT"] = "Espere, se está creando una nueva página ahora";
$MESS["SALESCENTER_MANAGER_UPDATE_URL_SUCCESS"] = "La página fue actualizada";
?>