<?php
$MESS["SPP_PAYSYSTEM_ADD"] = "Adicionar sistema de pagamento";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Pagamento em dinheiro";
$MESS["SPP_PAYSYSTEM_DEFAULT_TITLE"] = "Pagamento via #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Outros sistemas de pagamento";
$MESS["SPP_PAYSYSTEM_ORDERDOCUMENT_TITLE"] = "Fatura para impressão";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurar #PAYSYSTEM_NAME#";
