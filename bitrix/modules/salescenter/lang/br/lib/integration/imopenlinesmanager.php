<?php
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_PREVIEW"] = "Escolhemos estes itens exclusivamente para você. Entre em contato conosco se você tiver dúvidas! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_TOP"] = "Escolhemos estes itens exclusivamente para você.";
$MESS["SALESCENTER_IMOPMANAGER_EMPTY_COMPILATION_LINK"] = "Está faltando o link da seleção de produtos";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_BOTTOM"] = "Pagamento fácil com um clique. Envie-nos uma mensagem se tiver alguma dúvida!";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_DISCOUNT"] = "Desconto do pedido #DISCOUNT#";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_TOP"] = "Seu pedido de #SUM# do dia #DATE#";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_BOTTOM_2"] = "Entre em contato conosco se tiver alguma dúvida!";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_TOP_2"] = "Use este link para pagar seu pedido.";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_MESSAGE_PREVIEW_2"] = "Use este link para pagar seu pedido. Entre em contato conosco se tiver alguma dúvida! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_CLIENT_MAKE_ORDER"] = "O cliente fez um pedido.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_LINK"] = "Pedido ##ORDER_ID# de #ORDER_DATE#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_TEXT"] = "O pedido de #SUM# foi criado";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_NOTIFY_REORDER"] = "O negócio já tem um pedido. Um novo negócio será criado caso o cliente opte por fazer pedido de produtos da seleção.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_BOTTOM"] = "Clique no link para visualizar as informações do pedido. Obrigado pelo negócio!";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_TOP"] = "O valor total do pedido de #SUM# do dia #DATE# foi pago";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_TEXT"] = "Pedido de #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_PAYMENT_PAID_TEXT_TOP_2"] = "O valor total do pedido de #SUM# do dia #DATE# foi pago";
