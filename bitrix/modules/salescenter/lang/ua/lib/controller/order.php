<?php
$MESS["SALESCENTER_CONTROLLER_ORDER_DELIVERY"] = "Доставка";
$MESS["SALESCENTER_CONTROLLER_ORDER_PAYMENTS_LIMIT_REACHED"] = "Ви досягли ліміту створення оплат для вашого тарифного плану";
$MESS["SALESCENTER_CONTROLLER_ORDER_SEND_SMS_ERROR"] = "Помилка при надсиланні повідомлення. Повідомлення не було надіслано";
