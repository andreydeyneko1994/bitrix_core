<?php
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_PREVIEW"] = "Elegimos estos artículos solo para usted. ¡Escríbanos si tiene alguna duda! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_TOP"] = "Elegimos estos artículos solo para usted";
$MESS["SALESCENTER_IMOPMANAGER_EMPTY_COMPILATION_LINK"] = "Falta el enlace para la selección de productos";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_BOTTOM"] = "Pagos fáciles con tan solo un clic. ¡Envíenos un mensaje si tiene alguna duda!";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_DISCOUNT"] = "Descuento del pedido #DESCUENTO#";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_TOP"] = "Su pedido por #SUM# del #DATE#";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_BOTTOM_2"] = "¡Escríbanos si tiene alguna duda!";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_TOP_2"] = "Utilice este enlace para pagar su pedido.";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_MESSAGE_PREVIEW_2"] = "Utilice este enlace para pagar su pedido. ¡Escríbanos si tiene alguna duda! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_CLIENT_MAKE_ORDER"] = "El cliente hizo un pedido.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_LINK"] = "Pedido ##ORDER_ID# del #ORDER_DATE#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_TEXT"] = "Se creó un pedido por #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_NOTIFY_REORDER"] = "La negociación ya tiene un pedido. Se creará una nueva negociación si el cliente pide productos de la selección.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_BOTTOM"] = "Haga clic en el enlace para ver la información del pedido. ¡Gracias por su preferencia!";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_TOP"] = "El total del pedido #SUM# se pagó el #DATE#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_TEXT"] = "Pedido por #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_PAYMENT_PAID_TEXT_TOP_2"] = "Se pagó el total del pedido #SUM# con fecha #DATE#";
