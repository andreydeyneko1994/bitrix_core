<?php
$MESS["SPP_PAYSYSTEM_ADD"] = "Agregar sistema de pago";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Pago en efectivo";
$MESS["SPP_PAYSYSTEM_DEFAULT_TITLE"] = "Pago mediante #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Otros sistemas de pago";
$MESS["SPP_PAYSYSTEM_ORDERDOCUMENT_TITLE"] = "Factura imprimible";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurar #PAYSYSTEM_NAME#";
