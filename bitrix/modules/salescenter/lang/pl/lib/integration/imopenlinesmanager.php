<?php
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_PREVIEW"] = "Wybraliśmy te pozycje specjalnie dla Ciebie. Jeśli masz pytania, napisz do nas! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_TOP"] = "Wybraliśmy te pozycje specjalnie dla Ciebie.";
$MESS["SALESCENTER_IMOPMANAGER_EMPTY_COMPILATION_LINK"] = "Brak linku do wyboru produktów";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_BOTTOM"] = "Łatwa płatność jednym kliknięciem. Jeśli masz pytania, wyślij nam wiadomość!";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_DISCOUNT"] = "Zniżka na zamówienie #DISCOUNT#";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_TOP"] = "Twoje zamówienie na kwotę #SUM# z #DATE#";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_BOTTOM_2"] = "Jeśli masz jakieś pytania, skontaktuj się z nami!";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_TOP_2"] = "Użyj tego linku, aby zapłacić za zamówienie.";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_MESSAGE_PREVIEW_2"] = "Użyj tego linku, aby zapłacić za zamówienie. Jeśli masz jakieś pytania, skontaktuj się z nami! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_CLIENT_MAKE_ORDER"] = "Klient złożył zamówienie.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_LINK"] = "Zamówienie ##ORDER_ID# z #ORDER_DATE#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_TEXT"] = "Utworzono zamówienie na kwotę #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_NOTIFY_REORDER"] = "Ten deal ma już zamówienie. Nowy deal zostanie utworzony, jeśli klient zdecyduje się zamówić produkty z wyboru.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_BOTTOM"] = "Kliknij link, aby wyświetlić informacje o zamówieniu. Dziękujemy!";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_TOP"] = "Zamówienie na kwotę #SUM# z #DATE# zostało opłacone";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_TEXT"] = "Zamówienie na kwotę #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_PAYMENT_PAID_TEXT_TOP_2"] = "Zamówienie na kwotę #SUM# z #DATE# zostało opłacone";
