<?php
$MESS["SPP_PAYSYSTEM_ADD"] = "Dodaj system płatności";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Płatność gotówką";
$MESS["SPP_PAYSYSTEM_DEFAULT_TITLE"] = "Płatność przez #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Inne systemy płatności";
$MESS["SPP_PAYSYSTEM_ORDERDOCUMENT_TITLE"] = "Faktura do druku";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Konfiguruj #PAYSYSTEM_NAME#";
