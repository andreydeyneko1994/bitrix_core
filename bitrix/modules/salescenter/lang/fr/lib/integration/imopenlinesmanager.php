<?php
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_PREVIEW"] = "Nous avons choisi ces articles juste pour vous. N'hésitez pas à nous contacter si vous avez des questions ! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_COMPILATION_MESSAGE_TOP"] = "Nous avons sélectionné ces articles juste pour vous.";
$MESS["SALESCENTER_IMOPMANAGER_EMPTY_COMPILATION_LINK"] = "Le lien vers la sélection des produits manque";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_BOTTOM"] = "Paiement facile en un clic. Envoyez-nous un message si vous avez des questions !";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_DISCOUNT"] = "Réduction de la commande #DISCOUNT#";
$MESS["SALESCENTER_IMOPMANAGER_ORDER_ADD_MESSAGE_TOP"] = "Votre commande pour #SUM# du #DATE#";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_BOTTOM_2"] = "N'hésitez pas à nous contacter si vous avez des questions !";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_ADD_MESSAGE_TOP_2"] = "Veuillez utiliser ce lien pour régler votre commande.";
$MESS["SALESCENTER_IMOPMANAGER_PAYMENT_MESSAGE_PREVIEW_2"] = "Veuillez utiliser ce lien pour payer votre commande. N'hésitez pas à nous contacter si vous avez des questions ! #LINK#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_CLIENT_MAKE_ORDER"] = "Le client a passé une commande.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_LINK"] = "Commande ##ORDER_ID# du #ORDER_DATE#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_ADD_TEXT"] = "Une commande pour #SUM# a été créée";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_NOTIFY_REORDER"] = "La transaction a déjà une commande. Une nouvelle transaction sera créée si le client choisit de commander des produits de la sélection.";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_BOTTOM"] = "Cliquez sur le lien pour afficher les informations de commande. Merci de votre choix !";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_PAID_TEXT_TOP"] = "Le total de #SUM# de la commande du #DATE# a été payé";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_ORDER_TEXT"] = "Commande pour #SUM#";
$MESS["SALESCENTER_IMOPMANAGER_SYSTEM_PAYMENT_PAID_TEXT_TOP_2"] = "Le total de #SUM# de la commande du #DATE# a été payé";
