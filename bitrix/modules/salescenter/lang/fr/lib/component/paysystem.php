<?php
$MESS["SPP_PAYSYSTEM_ADD"] = "Ajouter un système de paiement";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Paiement en espèces";
$MESS["SPP_PAYSYSTEM_DEFAULT_TITLE"] = "Paiement via #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Autres systèmes de paiement";
$MESS["SPP_PAYSYSTEM_ORDERDOCUMENT_TITLE"] = "Facture imprimable";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurer #PAYSYSTEM_NAME#";
