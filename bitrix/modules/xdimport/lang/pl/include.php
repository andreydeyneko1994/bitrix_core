<?
$MESS["LFP_SOCNET_LOG_DATA"] = "Dane zewnętrzne";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_G_TITLE_MAIL"] = "Komentarz dodany do \"#TITLE#\" w grupie \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_P_TITLE_MAIL"] = "Komentarz dodany do \"#TITLE#\" pozyskano z \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_TITLE"] = "komentarz do \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_U_TITLE_MAIL"] = "Komentarz dodany do danych z \"#TITLE#\" w profilu #ENTITY#";
$MESS["LFP_SOCNET_LOG_DATA_G_TITLE_MAIL"] = "Zewnętrzne dane pozyskane w grupie \"#ENTITY#\": \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_P_TITLE_MAIL"] = "Pozyskano dane z zewnętrznego źródła \"#ENTITY#\": \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS"] = "Wszystkie wydarzenia w tym źródle danych";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_1"] = "wydarzenia zewnętrzne, grupa #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_2"] = "Zapisy z zewnętrznego źródła danych w grupie #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_1"] = "wydarzenia zewnętrzne, #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_2"] = "Zapisy z zewnętrznego źródła danych #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_1"] = "zdarzenia zewnętrzne, użytkownik #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_2"] = "Zapisy z zewnętrznego źródła danych #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_TITLE"] = "Pozyskano dane z zewnętrznego źródła: \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_24"] = "Wiadomość ze źródła wewnętrznego";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_IMPORTANT_24"] = "Ważne!";
$MESS["LFP_SOCNET_LOG_DATA_U_TITLE_MAIL"] = "Pozyskano dane zewnętrzne na stronie #ENTITY#: \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_LIST_P_ALL"] = "Wszystkie wydarzenia zewnętrzne";
$MESS["LFP_SOCNET_LOG_P"] = "Zewnętrzne źródło danych";
$MESS["LFP_SOCNET_LOG_XDI_P"] = "Zewnętrzne źródło danych";
?>