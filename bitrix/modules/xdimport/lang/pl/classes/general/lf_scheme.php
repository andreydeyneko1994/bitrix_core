<?
$MESS["LFP_CLASS_SCHEME_DELETE_ERROR"] = "Błąd podczas usuwania schematu połączenia: #error_msg#.";
$MESS["LFP_CLASS_SCHEME_ERR_DAYS_MISSING"] = "Autopublikowanie harmonogramu wymaga dni miesiąca i/lub dni tygodnia.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM"] = "Nieprawidłowy format harmonogramu dnia.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM2"] = "Nieprawidłowy format harmonogramu dnia.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW"] = "Nieprawidłowy format harmonogramu dni tygodnia.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW2"] = "Nieprawidłowy format harmonogramu dni tygodnia.";
$MESS["LFP_CLASS_SCHEME_ERR_ENTITY_TYPE"] = "Typ encji jest wymagany.";
$MESS["LFP_CLASS_SCHEME_ERR_EVENT_ID"] = "Wydarzenie jest wymagane.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_MISSING"] = "Autopublikowanie harmonogramu wymaga ostatnio publikowanego czasu.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_WRONG"] = "Ostatnio publikowany format czasu jest niepoprawny.";
$MESS["LFP_CLASS_SCHEME_ERR_NAME"] = "Wymagana jest nazwa schematu.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE"] = "Określona strona internetowa jest niepoprawna.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE2"] = "strona internetowa jest wymagana";
$MESS["LFP_CLASS_SCHEME_ERR_TIMES_MISSING"] = "Autopublikowanie harmonogramu wymaga czasu.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD"] = "Nieprawidłowy format harmonogramu czasu.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD2"] = "Nieprawidłowy format harmonogramu czasu.";
$MESS["LFP_CLASS_SCHEME_ERR_TYPE"] = "Typ schematu jest wymagany.";
$MESS["LFP_CLASS_SCHEME_ERR_URI_HOST"] = "Ten typ połączenia wymaga serwera.";
?>