<?
$MESS["LFP_SOCNET_LOG_DATA"] = "Dados externos";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_G_TITLE_MAIL"] = "Um comentário adicionado à \"#TITLE#\" no grupo \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_P_TITLE_MAIL"] = "Um comentário adicionado à \"#TITLE#\" obtido de \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_TITLE"] = "comentário sobre \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_U_TITLE_MAIL"] = "Um comentário adicionado aos dados de \"#TITLE\"# no perfil de #ENTITY#";
$MESS["LFP_SOCNET_LOG_DATA_G_TITLE_MAIL"] = "Dados externos obtidos no grupo \"#ENTITY#\": \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_P_TITLE_MAIL"] = "Obtidos dados de fonte externa \"#ENTITY#\": \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS"] = "Todos os eventos nesta fonte de dados";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_1"] = "eventos externos, grupo #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_2"] = "Registros de fonte de dados externos no grupo #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_1"] = "eventos externos, #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_2"] = "Registros de fonte de dados externos #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_1"] = "eventos externos, usuário #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_2"] = "Registros de fonte de dados externos do #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_TITLE"] = "Obtidos dados de fonte externa: \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_24"] = "Mensagem de fonte externa";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_IMPORTANT_24"] = "Importante!";
$MESS["LFP_SOCNET_LOG_DATA_U_TITLE_MAIL"] = "Obtidos dados externos na página de #ENTITY#: \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_LIST_P_ALL"] = "Todos os eventos externos";
$MESS["LFP_SOCNET_LOG_P"] = "Fonte de dados externa";
$MESS["LFP_SOCNET_LOG_XDI_P"] = "fonte de dados externa";
?>