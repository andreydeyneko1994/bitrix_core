<?
$MESS["LFP_SOCNET_LOG_DATA"] = "Base de datos externa";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_G_TITLE_MAIL"] = "Se ha agregado un comentario al \"#TITLE#\" en el grupo \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_P_TITLE_MAIL"] = "Se ha agregado un comentario al \"#TITLE#\" obtenido de \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_TITLE"] = "comentar a \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_U_TITLE_MAIL"] = "Se ha agregado un comentario a la data desde \"#TITLE#\" en el perfil de \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_G_TITLE_MAIL"] = "Data externa obtenida en el grupo de \"#ENTITY#\":  \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_P_TITLE_MAIL"] = "Data obtenida desde la fuente externa: \"#ENTITY#\":  \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS"] = "Todos los eventos en esta fuente de la base de datos";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_1"] = "evento externos, grupo #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_2"] = "Registros de la fuente externa de la base de datos en el grupo #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_1"] = "eventos externos, #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_2"] = "Registros de la fuente externa de la base de datos #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_1"] = "evento externos, usuario #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_2"] = "Registros de la fuente externa de la base del #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_TITLE"] = "Data obtenida desde la fuente externa: \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_24"] = "Mensaje de fuente externa";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_IMPORTANT_24"] = "Importante!";
$MESS["LFP_SOCNET_LOG_DATA_U_TITLE_MAIL"] = "Data externa obtenida en la página de \"#ENTITY#\":  \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_LIST_P_ALL"] = "Todos los eventos externos";
$MESS["LFP_SOCNET_LOG_P"] = "Fuente de base de datos externa";
$MESS["LFP_SOCNET_LOG_XDI_P"] = "Fuente de base de datos externa";
?>