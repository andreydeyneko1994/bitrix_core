<?
$MESS["XDI_INSTALL_TITLE"] = "Módulo de instalación de \"Importación de data externa\"";
$MESS["XDI_MODULE_DESCRIPTION"] = "Herramientas proveidas para importar datos de recursos externos";
$MESS["XDI_MODULE_NAME"] = "Importación de data externa";
$MESS["XDI_UNINSTALL_TITLE"] = "Módulo de desinstalación de \"Importación de data externa\"";
?>