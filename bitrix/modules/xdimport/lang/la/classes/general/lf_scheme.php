<?
$MESS["LFP_CLASS_SCHEME_DELETE_ERROR"] = "Error al eliminar el esquema de conexión: #error_msg#.";
$MESS["LFP_CLASS_SCHEME_ERR_DAYS_MISSING"] = "Para Autopublicar la agenda se requiere días del mes/o días de la semana.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM"] = "El formato de la fecha de la agenda es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM2"] = "El formato de la fecha de la agenda es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW"] = "El formato de la fecha de la agenda semanal es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW2"] = "El formato de la fecha de la agenda semanal es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_ENTITY_TYPE"] = "El tipo de entidad es requerido.";
$MESS["LFP_CLASS_SCHEME_ERR_EVENT_ID"] = "El evento es requerido.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_MISSING"] = "Para Autopublicar la agenda se requiere la hora de la última vez que se publicó.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_WRONG"] = "La hora del último formato publicado es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_NAME"] = "El nombre del esquema es requerido.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE"] = "El sitio web que ha especificado es incorrecto.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE2"] = "El sitio web es requerido.";
$MESS["LFP_CLASS_SCHEME_ERR_TIMES_MISSING"] = "Para Autopublicar la agenda se requiere hora.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD"] = "El formato de la fecha de la agenda es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD2"] = "El formato de la fecha de la agenda es inválido.";
$MESS["LFP_CLASS_SCHEME_ERR_TYPE"] = "El tipo de esquema es requerido.";
$MESS["LFP_CLASS_SCHEME_ERR_URI_HOST"] = "Este tipo de conexión requiere un servidor.";
$MESS["LFP_CLASS_SCHEME_IM_ADD"] = "Un mensaje nuevo \"#title#\" fue publicada a partir de una fuente externa a #group_name#.";
?>