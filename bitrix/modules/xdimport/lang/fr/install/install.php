<?
$MESS["XDI_INSTALL_TITLE"] = "Installation du module d'Importation des données provenant de sources externes";
$MESS["XDI_MODULE_DESCRIPTION"] = "Outils pour l'importation de données vers le système à partir de sources externes";
$MESS["XDI_MODULE_NAME"] = "Importer les données provenant de sources externes";
$MESS["XDI_UNINSTALL_TITLE"] = "Suppression du module Importation des données venant des sources externes";
?>