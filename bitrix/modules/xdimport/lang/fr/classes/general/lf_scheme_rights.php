<?
$MESS["LFP_CLASS_SCHEME_RIGHT_DELETE_ERROR"] = "Impossible de supprimer les droits pour le schéma de connexion : #error_msg#.";
$MESS["LFP_CLASS_SCHEME_RIGHT_ERR_GROUP_CODE"] = "Enregistrement des droits du schéma de connexion : le code du groupe est un champ obligatoire.";
$MESS["LFP_CLASS_SCHEME_RIGHT_ERR_SCHEME_ID"] = "La préservation de droits sur le schéma de connexion : ID de schémas est un champ obligatoire.";
?>