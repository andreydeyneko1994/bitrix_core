<?
$MESS["LFP_CLASS_USER_DELETE_ERROR"] = "Impossible de supprimer les notes sur les groupes d'utilisateurs : #error_msg#.";
$MESS["LFP_CLASS_USER_ERR_GROUP_CODE"] = "Stockage des groupes d'utilisateurs : code de groupe est un champ obligatoire.";
$MESS["LFP_CLASS_USER_ERR_USER_ID"] = "Sauvegarde de l'enregistrement sur les groupes de l'utilisateur : ID de l'utilisateur est le champ obligatoire.";
?>