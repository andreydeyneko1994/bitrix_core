<?
$MESS["LFP_CLASS_SCHEME_DELETE_ERROR"] = "Erreur de la suppression du schéma de la connexion : #error_msg#.";
$MESS["LFP_CLASS_SCHEME_ERR_DAYS_MISSING"] = "Pour planifier une publication automatique vous devez indiquer les jours du mois et/ou les jours de la semaine.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM"] = "Format incorrect des jours du mois pour le planning.";
$MESS["LFP_CLASS_SCHEME_ERR_DOM2"] = "Format incorrect des jours du mois pour le planning.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW"] = "Format incorrect des jours de semaine pour le calendrier.";
$MESS["LFP_CLASS_SCHEME_ERR_DOW2"] = "Format incorrect des jours de semaine pour le calendrier.";
$MESS["LFP_CLASS_SCHEME_ERR_ENTITY_TYPE"] = "Le champ 'Type d'entité' est obligatoire.";
$MESS["LFP_CLASS_SCHEME_ERR_EVENT_ID"] = "L'évènement est un champ obligatoire.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_MISSING"] = "Pour la publication automatique, l'heure de la dernière publication doit être indiquée.";
$MESS["LFP_CLASS_SCHEME_ERR_LE_WRONG"] = "Format erroné de la publication la plus récente.";
$MESS["LFP_CLASS_SCHEME_ERR_NAME"] = "Le nom du schéma est un champ obligatoire.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE"] = "Site incorrect.";
$MESS["LFP_CLASS_SCHEME_ERR_SITE2"] = "Indiquez le site.";
$MESS["LFP_CLASS_SCHEME_ERR_TIMES_MISSING"] = "Pour planifier la publication automatique le temps doit être indiqué.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD"] = "Format de l'heure incorrect pour l'horaire.";
$MESS["LFP_CLASS_SCHEME_ERR_TOD2"] = "Format de l'heure incorrect pour l'horaire.";
$MESS["LFP_CLASS_SCHEME_ERR_TYPE"] = "Le champ 'type de schéma' est obligatoire.";
$MESS["LFP_CLASS_SCHEME_ERR_URI_HOST"] = "Pour ce type de connexion, il est nécessaire d'indiquer le serveur.";
$MESS["LFP_CLASS_SCHEME_IM_ADD"] = "Dans le groupe #group_name# est ajouté le message de la source extérieure '#title#'.";
?>