<?
$MESS["LFP_SOCNET_LOG_DATA"] = "Données externes";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_G_TITLE_MAIL"] = "Un commentaire ajouté à \"#TITLE#\" dans le groupe \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_P_TITLE_MAIL"] = "Un commentaire ajouté à \"#TITLE#\" obtenu de \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_TITLE"] = "commentaire sur \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_COMMENT_U_TITLE_MAIL"] = "Un commentaire ajouté aux données de \"#TITLE#\" dans le profil de \"#ENTITY#\"";
$MESS["LFP_SOCNET_LOG_DATA_G_TITLE_MAIL"] = "Données externes obtenues dans le groupe \"#ENTITY#\" : \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_P_TITLE_MAIL"] = "Données obtenues à partir de la source externe \"#ENTITY#\":  \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS"] = "Tous les évènements dans cette source de données";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_1"] = "évènements externes, groupe #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_G_2"] = "Enregistrements de la source de données externe dans le groupe #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_1"] = "évènements externes, #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_P_2"] = "Enregistrements de la source de données externe #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_1"] = "évènements externes, utilisateur #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_SETTINGS_U_2"] = "Enregistrements de la source de données externe de #TITLE#";
$MESS["LFP_SOCNET_LOG_DATA_TITLE"] = "Données obtenues à partir de la source externe : '#TITLE#'";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_24"] = "Message parvenu d'une source extérieure";
$MESS["LFP_SOCNET_LOG_DATA_TITLE_IMPORTANT_24"] = "Important !";
$MESS["LFP_SOCNET_LOG_DATA_U_TITLE_MAIL"] = "Données externes obtenues sur la page de \"#ENTITY#\" : \"#TITLE#\"";
$MESS["LFP_SOCNET_LOG_LIST_P_ALL"] = "Tous les évènements externes";
$MESS["LFP_SOCNET_LOG_P"] = "Source de données externe";
$MESS["LFP_SOCNET_LOG_XDI_P"] = "source de données externe";
?>