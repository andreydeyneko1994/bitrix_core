<?
$MESS["LFP_SCHEME_LIST_ACTIVATE"] = "Activer";
$MESS["LFP_SCHEME_LIST_ACTIVE"] = "Actif(ve)";
$MESS["LFP_SCHEME_LIST_ADD"] = "Ajouter une connexion";
$MESS["LFP_SCHEME_LIST_ADD_TITLE"] = "Créer une nouvelle connexion aux fournisseurs de données";
$MESS["LFP_SCHEME_LIST_DEACTIVATE"] = "Désactiver";
$MESS["LFP_SCHEME_LIST_DEACTIVATE_CONF"] = "Désactivez la connexion aux fournisseurs de données ?";
$MESS["LFP_SCHEME_LIST_DELETE"] = "Supprimer";
$MESS["LFP_SCHEME_LIST_DELETE_CONF"] = "Retirez la connexion à un fournisseur de données ?";
$MESS["LFP_SCHEME_LIST_EDIT"] = "Éditer";
$MESS["LFP_SCHEME_LIST_ID"] = "ID";
$MESS["LFP_SCHEME_LIST_LID"] = "Site web";
$MESS["LFP_SCHEME_LIST_NAME"] = "Dénomination";
$MESS["LFP_SCHEME_LIST_NO_RECORD"] = "Enregistrement n'est pas trouvé.";
$MESS["LFP_SCHEME_LIST_POST"] = "Demande POST";
$MESS["LFP_SCHEME_LIST_RSS"] = "Flux RSS";
$MESS["LFP_SCHEME_LIST_SAVE_ERROR"] = "Erreur de modification de l'enregistrement : ";
$MESS["LFP_SCHEME_LIST_SORT"] = "Trier";
$MESS["LFP_SCHEME_LIST_TITLE"] = "Connexions aux fournisseurs de données pour le Flux d'activités";
$MESS["LFP_SCHEME_LIST_TYPE"] = "Type de la connexion";
$MESS["LFP_SCHEME_LIST_XML"] = "Service web (XML)";
?>