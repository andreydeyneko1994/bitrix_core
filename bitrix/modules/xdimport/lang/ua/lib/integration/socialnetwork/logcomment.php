<?php
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE"] = "Згадав вас у #A_BEGIN# повідомленні із зовнішнього джерела #A_END#.";
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE_F"] = "Згадала вас у #A_BEGIN# повідомленні із зовнішнього джерела #A_END#.";
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE_OUT"] = "Згадав вас у повідомленні із зовнішнього джерела. #URL#";
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE_OUT_F"] = "Згадала вас у повідомленні із зовнішнього джерела. #URL#";
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE_PUSH"] = "#AUTHOR# згадав вас у повідомленні із зовнішнього джерела.";
$MESS["XDIMPORT_COMMENT_MENTION_NOTIFICATION_MESSAGE_PUSH_F"] = "#AUTHOR# згадала вас у повідомленні із зовнішнього джерела.";
