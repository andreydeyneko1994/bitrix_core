<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ACTIVATE"] = "Activar";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CHANGE_ANY_TIME"] = "Puede editarlo o desconectarlo en cualquier momento.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECTED"] = "Yandex chat conectado";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_CONNECT_STEP"] = "Por favor, active el canal primero. Para conectar el chat de Yandex, use el <a href=\"#URL#\" target=\"_blank\">YPágina de conversaciones de Yandex</a>. Sigue la guía de conexión. <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">aquí</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_COPY"] = "copiar";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_FINAL_FORM_DESCRIPTION"] = "Yandex Chat se ha conectado a su Canal Abierto. Para configurar la conexión, use su <a href=\"#URL#\" target=\"_blank\">Perfil de Conversaciones Yandex</a>. Siga el manual de configuración. <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">aquí</a>.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_ID_CHAT"] = "ID del canal (el ID que se transmitirá a un chat):";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_ID"] = "redirect=detail&code=7837755";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_INFO_CONNECT_URL"] = "https://dialogs.yandex.ru/developer";
?>