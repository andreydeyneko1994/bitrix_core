<?
$MESS["IMCONNECTOR_COMPONENT_YANDEX_MODULE_NOT_INSTALLED"] = "Elmódulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_CONNECT"] = "No se puede establecer una conexión de prueba utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_NO_REGISTER"] = "Error de registro";
$MESS["IMCONNECTOR_COMPONENT_YANDEX_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
?>