<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_MODULE_NOT_INSTALLED"] = "Le module \"External IM Connectors\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_ACTIVE_CONNECTOR"] = "Le connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_CONNECT"] = "Échec de la création de la connexion test avec les paramètres fournis";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_DATA_SAVE"] = "Il n'y a aucune donnée à enregistrer";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_REGISTER"] = "Erreur d'inscription";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_SAVE"] = "Erreur d'enregistrement des données. Veuillez vérifier votre entrée et réessayer.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_CONNECT"] = "La connexion test a bien été établie";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_REGISTER"] = "Inscription réussie";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_SAVE"] = "Les informations ont bien été enregistrées.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
