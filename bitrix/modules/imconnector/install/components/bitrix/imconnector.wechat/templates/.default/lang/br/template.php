<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ENCRYPT_KEY"] = "Chave de criptografia de mensagem:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ID"] = "ID do desenvolvedor (AppID):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_SECRET"] = "Senha do desenvolvedor (AppSecret):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CHANGE_ANY_TIME"] = "Você pode editar ou desconectar a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECTED"] = "Agora o WeChat está conectado";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Eu gostaria de </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectar uma</span> conta WeChat</div>";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP"] = "Você precisa ter uma conta oficial para conectar. Se você não tiver uma conta, precisará <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">criar uma conta oficial</a>. Podemos ajudar você a criar uma conta oficial e conectá-la à sua conta Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP_NEW"] = "Você terá que #LINK_START#criar uma conta oficial#LINK_END# ou usar a já existente para conectar. Se você não tem uma conta, ajudaremos você a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_TITLE"] = "Conectar WeChat ao Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_FINAL_FORM_DESCRIPTION"] = "O WeChat foi conectado ao seu Canal Aberto. Todas as mensagens publicadas na sua conta oficial serão redirecionadas para a sua conta Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Como obter informações da conta oficial</span>:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_LANG"] = "Selecionar idioma da conta";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NAME_CHAT_LINK"] = "Link de bate-papo ao vivo";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SERVER_IP_ADDRESS_DESCRIPTION"] = "Especifique esses valores no campo Lista de permissões de IP no perfil da sua conta oficial (cada valor em uma nova linha):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_TOKEN_DESCRIPTION"] = "Especifique este valor no campo Token no perfil da sua conta oficial:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_WEBHOOK_DESCRIPTION"] = "Especifique este endereço no campo URL no perfil da sua conta oficial:";
