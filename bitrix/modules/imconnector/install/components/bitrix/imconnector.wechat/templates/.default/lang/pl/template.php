<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ENCRYPT_KEY"] = "Klucz szyfrowania wiadomości:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ID"] = "ID programisty (AppID):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_SECRET"] = "Hasło programisty (AppSecret):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub odłączyć";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECTED"] = "WeChat jest teraz podłączony";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę</div>
	<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">podłączyć</span> konto WeChat</div>";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP"] = "Aby się podłączyć, musisz mieć oficjalne konto. Jeśli go nie masz, musisz <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">utworzyć oficjalne konto</a>. Pomożemy Ci utworzyć oficjalne konto i podłączyć je do konta Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP_NEW"] = "#LINK_START#Utwórz oficjalne konto#LINK_END# lub użyj już istniejącego do podłączenia. Jeśli nie masz konta, pomożemy Ci je utworzyć i podłączyć do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_TITLE"] = "Podłącz WeChat do Otwartego kanału";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_COPY"] = "Kopiuj";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_FINAL_FORM_DESCRIPTION"] = "WeChat został podłączony do Otwartego kanału. Wszystkie wiadomości publikowane na Twoim oficjalnym koncie będą przekazywane do Twojego Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Jak uzyskać oficjalne informacje o koncie</span>:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_LANG"] = "Wybierz język konta";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NAME_CHAT_LINK"] = "Link do czatu na żywo";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SERVER_IP_ADDRESS_DESCRIPTION"] = "Określ te wartości w polu Biała lista adresów IP na oficjalnym profilu konta (każda wartość w nowej linii):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_TOKEN_DESCRIPTION"] = "Podaj tę wartość w polu Token na oficjalnym profilu konta:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_WEBHOOK_DESCRIPTION"] = "Podaj ten adres w polu URL na oficjalnym profilu konta:";
