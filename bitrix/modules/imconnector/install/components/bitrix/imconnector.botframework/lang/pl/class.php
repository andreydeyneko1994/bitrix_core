<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_MODULE_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_CONNECT"] = "Nie udało się ustanowić połączenia testu przy użyciu podanych parametrów";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_DATA_SAVE"] = "Brak danych do zapisania";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_SAVE"] = "Wystąpił błąd podczas zapisywania danych. Sprawdź swoje dane wejściowe i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_CONNECT"] = "Połączenie testowe zostało pomyślnie ustanowione";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Proszę prześlij formularz ponownie.";
?>