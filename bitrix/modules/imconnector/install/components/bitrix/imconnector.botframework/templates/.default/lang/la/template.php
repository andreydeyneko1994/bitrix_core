<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Siga estos pasos para conectar Microsoft Bot Framework al Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID de la Aplicación (ID Microsoft App):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Contraseña secreta de la aplicación:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID del bot (Bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Se puede editar o desactivar en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Botframework conectado";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Su bot ha encontrado un error. Por favor, compruebe las preferencias y probar el nuevo bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Complete el formulario para conectar Microsoft Bot Framework a su Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Quiero </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectarme</span> al bot</div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Crear un nuevo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Ya tengo un bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtener enlaces en sitio web de Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Conecte Microsoft Bot Framework a Open Channel para recibir mensajes de sus clientes desde Skype y otras fuentes compatibles (Slack, Kik, GroupMe, SMS, correo electrónico, etc.) directamente a su Bitrix24.
Le ayudaremos a crear un bot y conectarlo a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instrucciones para la conexión del bot</span>:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Puede especificar enlaces públicos a los canales que conectó con Microsoft para mostrarlos en el widget (chat en directo) del sitio web.
<br><br>
Para obtener los enlaces, continúe con #LINK_BEGIN#su cuenta de Microsoft Bot Framework#LINK_END# haga click en \"Get bot embed codes\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Puede especificar vínculos públicos con los canales que conectó con Microsoft para mostrarlos en el widget (chat en directo) del sitio web en el #LINK_BEGIN#formato de configuración de canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "especificar enlaces a canales de comunicación";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "Especifique esta dirección en el campo Mensajería Webhook en el sitio web de Microsoft, en la página de configuración del bot:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Si su bot ya se ha registrado, conéctelo a Bitrix24.<br><br>
Especifique esta dirección en el campo \"Messaging Webhook\" del sitio web de Microsoft:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Usted puede comprobar la exactitud de los datos.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Conexión de prueba";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "En su #LINK_BEGIN#Microsoft Bot Framework account#LINK_END#, copie los enlaces a los canales que desee mostrar a sus clientes en Live Chat y péguelos a continuación.
<br><br>
Observe que solo una parte de un enlace necesita ser copiada. Encontrará ejemplos de lo que tiene que copiar y pegar.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.es</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.es";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_EXAMPLE"] = "&lt;a href='<strong>https://www.messenger.es/t/1343073279053039</strong>'&gt;&lt;img src='https://facebook.botframework.com/Content/MessageUs.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_PLACEHOLDER"] = "https://www.messenger.es/t/1343073279053039";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_EXAMPLE"] = "&lt;a href='<strong>https://groupme.botframework.es/?botId=bitrix</strong>'&gt;@bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_PLACEHOLDER"] = "https://groupme.botframework.es/?botId=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_EXAMPLE"] = "&lt;a href='<strong>https://bots.kik.es/#/bitrix</strong>'&gt;bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_PLACEHOLDER"] = "https://bots.kik.es/#/testbitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_EXAMPLE"] = "&lt;a href='<strong>https://teams.microsoft.es/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img height=\"30\" width=\"113\" src='https://dev.botframework.com/Client/Images/Add-To-MSTeams-Buttons.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_PLACEHOLDER"] = "https://teams.microsoft.es/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_EXAMPLE"] = "&lt;a href='<strong>https://join.skype.es/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img src='https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png'/&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_PLACEHOLDER"] = "https://join.skype.es/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_EXAMPLE"] = "&lt;a href=\"<strong>https://slack.es/oauth/authorize?scope=bot&client_id=58933734192.70978425654 &redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix</strong>\"&gt;&lt;img height=\"40\" width=\"139\" src=\"https://platform.slack-edge.com/img/add_to_slack.png\" srcset=\"https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x\"&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_PLACEHOLDER"] = "https://slack.es/oauth/authorize?scope=bot&client_id=58933734192.70978425654&redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.es/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.es/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "+15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Get the link:<br>
									&lt;iframe src=\"<strong>https://webchat.botframework.es/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Replace YOUR_SECRET_HERE with the contents of the \"Secret\" field.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_PLACEHOLDER"] = "https://webchat.botframework.es/embed/bitrix?s=SZfvj3Wdqrw.cwA.7RE.iiQcU8IlUJsYFASNYXusu3BmAsHitiyvVSH4A0xjTF0";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework ha sido conectado a su Canal Abierto.
A partir de ahora, todos los mensajes enviados al bot serán remitidos a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Puede conectar otras fuentes de comunicación para el bot o configurar las ya existentes para reenviar todos los mensajes al chat.";
?>