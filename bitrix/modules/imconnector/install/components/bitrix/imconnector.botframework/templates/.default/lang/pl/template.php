<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Aby podłączyć Microsoft Bot Framework do Otwartego kanału, wykonaj następujące czynności";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "App ID (Identyfikator aplikacji Microsoft):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Tajne hasło aplikacji:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID bota (obsługa bota):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub wyłączyć";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Podłączono Bot Framework";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Bot napotkał błąd. Sprawdź preferencje i ponownie wypróbuj bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Aby podłączyć Microsoft Bot Framework do Otwartego kanału, wypełnij formularz";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">podłączyć</span> bota</div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Kopiuj";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Utwórz nowego bota";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Mam już bota";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Pobierz linki ze strony Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Podłącz Microsoft Bot Framework do Otwartego Kanału, aby odbierać wiadomości od klientów ze Skype'a i innych obsługiwanych źródeł (Slack, Kik, GroupMe, SMS, e-mail itp.) bezpośrednio do Bitrix24. <br><br>Pomożemy stworzyć bota i podłączyć go do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instrukcje podłączania bota</span>:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Można określić publiczne odnośniki do kanałów połączonych z rozwiązaniami firmy Microsoft, aby wyświetlić je w widżecie (czat na żywo) na stronie.
<br><br>
Aby otrzyzmać odnośniki, przejdź do #LINK_BEGIN#swojego konta Microsoft Bot Framework #LINK_END# i kliknij \"Get bot embed codes\" [Pobierz kody do osadzania bota].";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Można określić publiczne odnośniki do kanałów połączonych z rozwiązaniem Microsoft, aby wyświetlać je w widżecie (czatu na żywo) na stronie w #LINK_BEGIN#formularzu ustawień kanału#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "wskaż odnośniki do kanałów komunikacji";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "Podaj ten adres w polu Messaging Webhook w witrynie firmy Microsoft na stronie konfiguracji bota:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Jeśli Twój bot ma już zarejestrowany, należy go podłączyć do Bitrix24. <br><br>W polu \"Messaging Webhook\" [\"Komunikacja z Webhook\"] w witrynie firmy Microsoft należy wpisać ten adres:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Można sprawdzić poprawność danych.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Test połączenia";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "W swoim #LINK_BEGIN#koncie na Microsoft Bot Framework#LINK_END# skopiuj odnośniki do kanałów, które chcesz pokazać klientom w czacie na żywo i wklej je poniżej.
<br><br>
Uwaga: tylko część odnośnika musi być skopiowana. Przykłady elementów do skopiowania i wklejenia podano poniżej.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://Telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Pobierz odnośnik:<br>
									&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Zastąp YOUR_SECRET_HERE treścią pola \"Tajny\".";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework został podłączony do kanału otwartego. Teraz wszystkie wiadomości wysyłane do bota będą przekazywane do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Można podłączyć inne źródła komunikacji do bota lub skonfigurować istniejące do przesyłania dalej wszystkie wiadomości na czat.";
?>