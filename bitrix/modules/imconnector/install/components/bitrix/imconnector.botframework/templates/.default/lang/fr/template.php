<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Suivez ces étapes pour connecter Microsoft Bot Framework au Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID d'App (Microsoft App ID) : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Mot de passe secret d'App : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID du bot (bot handle) : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Peut être édité ou désactivé à tout moment";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Botframework connecté";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Votre bot a rencontré une erreur. Veuillez vérifier les préférences et refaire le test du bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Remplissez le formulaire pour connecter Microsoft Bot Framework à votre canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Je veux </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">connecter</span> un bot</div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copier";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Créer un nouveau bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "J'ai déjà un bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtenir les liens sur le site Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Connectez Microsoft Bot Framework au Canal Ouvert pour recevoir les messages de vos clients depuis Skype et d'autres sources supportées (Slack, Kik, GroupMe, SMS, e-mail etc.) directement dans votre Bitrix24.<br><br>Nous vous aiderons à créer un bot et à y connecter votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instructions pour connecter un bot</span> : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site.<br><br>Pour obtenir les liens, allez dans LINK_BEGIN#votre compte Microsoft Bot Framework#LINK_END# et cliquez sur \"Obtenir les codes embarqués du bot\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site dans le #LINK_BEGIN#formulaire des paramètres du canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "spécifiez les liens vers les canaux de communication";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "Indiquez cette adresse dans le champ Messaging Webhook du site Microsoft, sur la page de configuration du bot : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Si votre bot a déjà été inscrit, connectez-le à Bitirix24.<br><br>Spécifiez cette adresse dans le champ \"Messaging Webhook\" sur le site Microsoft : ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Vous pouvez vérifier l'exactitude de vos données.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Tester la connexion";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "Dans votre #LINK_BEGIN#compte Microsoft Bot Framework#LINK_END#, copiez les liens vers les canaux que vous souhaitez afficher à vos clients dans le Chat Live et collez-les ci-dessous.<br><br>
Notez que seule une partie d'un lien doit être copiée. Vous trouverez des exemples de ce que vous devez copier et coller.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site dans le #LINK_BEGIN#formulaire des paramètres du canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Obtenez le lien : <br>
&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height:
502px; max-height:
502px;\"&gt;&lt;/iframe&gt;<br>Remplacez YOUR_SECRET_HERE par le contenu du champ \"Secret\".";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework a été connecté à votre Canal Ouvert. Désormais, tous les messages envoyés sur le bot seront transférés vers votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Vous pouvez connecter d'autres sources de communication au bot ou configurer les sources existantes pour transférer tous les messages vers le chat.";
?>