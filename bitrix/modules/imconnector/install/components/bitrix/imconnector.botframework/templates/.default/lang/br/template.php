<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Siga estas etapas para conectar o Microsoft Bot Framework ao Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID do App (ID do App Microsoft):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Senha secreta do App:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID do Bot (Identificador do Bot):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CHANGE_ANY_TIME"] = "Pode ser editado ou desligado a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTED"] = "Botframework conectado";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "O seu bot encontrou um erro. Verifique as preferências e testar o bot novamente.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_DESCRIPTION"] = "Preencha o formulário para conectar o Microsoft Bot Framework ao seu Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Quero </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectar</span> um bot</div>";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Crie um novo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Eu já tenho um bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtenha links no site da Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Conecte o Microsoft Bot Framework ao Canal Aberto para receber mensagens de seus clientes pelo Skype e outras fontes compatíveis (Slack, Kik, GroupMe, SMS, e-mail etc) diretamente no seu Bitrix24.<br><br>
Vamos ajudar você a criar um bot e conectá-lo ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instruções de conexão de bot</span>:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Você pode especificar links públicos para os canais que você conectou com a Microsoft para mostrá-los no widget (bate-papo ao vivo) no site.
<br><br>
Para obter os links, acesse #LINK_BEGIN#sua conta Microsoft Bot Framework#LINK_END# e clique em \"Obter códigos de inserção do bot\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Você pode especificar links públicos para os canais que você conectou com a Microsoft para mostrá-los no widget (bate-papo ao vivo) no site no #LINK_BEGIN#formulário de configurações do canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "especifique links para canais de comunicação";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION"] = "Especifique esse endereço no campo Messaging Webhook no site da Microsoft, na página de configuração do bot:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Se o seu bot já tiver sido registado, conecte-o ao Bitrix24.<br><br>
Especifique esse endereço no campo \"Mensagens Webhook\" no site da Microsoft:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Você pode verificar seus dados para correção.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Conexão de teste";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "Na sua #LINK_BEGIN#conta Microsoft Bot Framework#LINK_END#, copie os links para os canais que você deseja mostrar a seus clientes no Bate-papo ao vivo e cole-os abaixo.
<br><br>
Observe que apenas uma parte do link precisa ser copiada. Você vai encontrar exemplos do que você tem de copiar e colar.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Obtenha o link:<br>
									&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Substitua SEU_SEGREDO_AQUI pelo conteúdo do campo \"Segredo\".";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "O Microsoft Bot Framework foi conectado ao seu Canal Aberto.
A partir de agora, todas as mensagens enviadas ao bot serão encaminhadas para o seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Você pode conectar outras fontes de comunicação ao bot ou configurar as existentes para encaminhar todas as mensagens ao bate-papo.";
?>