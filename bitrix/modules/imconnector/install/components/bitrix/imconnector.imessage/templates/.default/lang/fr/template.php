<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CHANGE_ANY_TIME"] = "Vous pouvez éditer ou déconnecter à tout moment";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECTED"] = "Apple Business Chat est connecté";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_STEP"] = "Pour finaliser la connexion, sélectionnez un Canal ouvert auquel connecter.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_TITLE"] = "Le chat professionnel est maintenant presque connecté au Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Je voudrais :</div>
				<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">Connecter</span> un chat professionnel</div>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_STEP"] = "Avant de vous connecter, vous devez <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">créer un chat professionnel</a>.
 ou <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">en connecter un existant</a>.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_STEP_NEW"] = "Avant de vous connecter, vous devez #LINK1_START#créer un chat professionnel#LINK1_END# ou #LINK2_START#connecter un chat existant#LINK2_END#.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_TITLE"] = "Connecter un chat professionnel au Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FINAL_FORM_DESCRIPTION"] = "Un chat professionnel a été connecté à votre Canal ouvert. Tous les messages publiés sur votre chat professionnel seront redirigés vers votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_ADDITIONAL_DESCRIPTION"] = "Vous devez <a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\') ; return false;\">créer un chat professionnel avec Apple Messages for Business</a> ou <a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\') ; return false;\">connecter celui que vous avez déjà</a>. Seuls les identifiants vérifiés de l'Apple Business Chat peuvent être connectés. Connecter ou configurer manuellement";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Avant la connexion, vous devez #LINK1_START#créer un chat professionnel#LINK1_END# ou #LINK2_START#connecter un chat existant#LINK2_END#. Seul l'identifiant vérifié du chat professionnel Apple peut être connecté. Connectez-le ou configurez-le manuellement.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_3"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_4"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_SUBTITLE"] = "Répondez à vos clients comme ils le souhaitent. Si un client trouve Apple Messages plus pratique que les autres moyens de communication, recevez ses messages sur votre Bitrix24 et répondez-lui immédiatement.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_TITLE"] = "Utilisez Apple Messages pour communiquer avec les propriétaires d'appareils Apple";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_NEW_CONNECT"] = "Chat professionnel à connecter";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_OLD_CONNECT"] = "Informations sur le chat professionnel connecté";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instructions pour la connexion :</span>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_I_KNOW_KEY"] = "J'ai un ID professionnel";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_ID"] = "ID professionnel";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_NAME"] = "Nom";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_CHAT_LINK"] = "Lien de chat en direct";
