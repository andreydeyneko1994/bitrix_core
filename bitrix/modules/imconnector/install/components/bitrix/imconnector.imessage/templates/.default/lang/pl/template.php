<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub odłączyć";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECTED"] = "Apple Business Chat jest podłączony";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_STEP"] = "Aby dokończyć podłączanie, wybierz Otwarty kanał, do którego chcesz się podłączyć.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_TITLE"] = "Czat biznesowy jest już prawie podłączony do Otwartego kanału";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę:</div>
				<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">Podłączyć</span> czat biznesowy</div>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_STEP"] = "Przed podłączeniem musisz <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">utworzyć czat biznesowy</a>
 lub <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">podłączyć już istniejący</a>.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_STEP_NEW"] = "Przed podłączeniem #LINK1_START#utwórz czat biznesowy#LINK1_END# lub #LINK2_START#podłącz już istniejący#LINK2_END#.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_TITLE"] = "Podłącz czat biznesowy do Otwartego kanału";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FINAL_FORM_DESCRIPTION"] = "Czat biznesowy został podłączony do Otwartego kanału. Wszystkie wiadomości publikowane na Twoim czacie biznesowym będą przekazywane do Twojego Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_ADDITIONAL_DESCRIPTION"] = "Należy <a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\'); return false;\">utworzyć czat biznesowy za pośrednictwem Apple Messages for Business</a> lub <a href=\"#\"  onclick\"top.BX.Helper.show(\'#ID#\'); return false;\">podłączyć istniejący</a>. Można podłączyć wyłącznie zweryfikowane identyfikatory Apple Business Chat ID. Podłącz lub skonfiguruj ręcznie";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Przed podłączeniem #LINK1_START#utwórz czat biznesowy#LINK1_END# lub #LINK2_START#podłącz już istniejący#LINK2_END#. Można podłączyć wyłącznie zweryfikowany identyfikator Apple Business Chat ID. Podłącz lub skonfiguruj go ręcznie.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_3"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_4"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_SUBTITLE"] = "Odpowiadaj klientom zgodnie z ich preferencjami. Jeśli klient uzna Apple Messages za wygodniejszy niż inne środki komunikacji, odbieraj jego wiadomości w Bitrix24 i natychmiast na nie odpowiadaj.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_TITLE"] = "Używaj Apple Messages do komunikacji z właścicielami urządzeń Apple";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_NEW_CONNECT"] = "Czat biznesowy do podłączenia";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_OLD_CONNECT"] = "Informacje o podłączonym czacie biznesowym";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instrukcje podłączania:</span>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_I_KNOW_KEY"] = "Mam ID biznesowe";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_ID"] = "ID biznesowe";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_NAME"] = "Nazwa";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_CHAT_LINK"] = "Link do czatu na żywo";
