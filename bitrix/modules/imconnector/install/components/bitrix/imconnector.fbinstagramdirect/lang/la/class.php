<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_DISABLED_ACCESS_INSTAGRAM_DIRECT_MESSAGES"] = "Para conectar IG Direct, habilite el acceso a los mensajes en su cuenta de Instagram. #A#Más información.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API"] = "Desafortunadamente, debido a las normas de Facebook, solo los usuarios de cuentas de Instagram que tengan entre 10,000 y 100,000 suscriptores pueden conectarse a IG Direct. #A#Más información.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API_3"] = "Desafortunadamente, su IG Direct no cumple con los requisitos de Facebook. #A#Ver más información#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Error del servidor.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INVALID_OAUTH_ACCESS_TOKEN"] = "No se puede administrar la página pública porque se perdió el acceso. Debe conectarse de nuevo a su página pública.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_AUTHORIZATION_PAGE"] = "No se puede vincular la página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_PAGE"] = "No se puede desvincular la página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_USER"] = "No se puede desvincular su cuenta de usuario";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_AUTHORIZATION_PAGE"] = "La página fue vinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_PAGE"] = "La página fue desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_USER"] = "Su cuenta de usuario fue desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REMOVED_REFERENCE_TO_PAGE"] = "Esta conexión se configuró para su uso con un grupo de trabajo en el que actualmente no tiene acceso de administrador.<br> Configure el conector de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REPEATING_ERROR"] = "Si el problema persiste, es posible que deba desconectar el canal y configurarlo de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_SESSION_HAS_EXPIRED"] = "Su sesión expiró. Envíe el formulario de nuevo.";
