<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ACTIVE_COMMENTS"] = "Podłączone komentarze";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_CANCEL"] = "Anuluj";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_OK"] = "Kontynuuj";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_DESCRIPTION"] = "To konto jest używane z konektorem Instagram Business w Twoim Bitrix24. Aby uniknąć powielania komentarzy, połączenie to zostanie wyłączone.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_TITLE"] = "Podłączyć mimo to?";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Połącz konto firmowe Instagram, aby otrzymywać wiadomości od swoich klientów na czacie Bitrix24. Aby się połączyć, musisz mieć publiczną stronę na Facebooku lub ją utworzyć i połączyć z nią konto firmowe na Instagramie.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">natychmiastowa komunikacja z użytkownikami Twojego konta na Instagramie</li>
					<li class=\"im-connector-settings-header-list-item\">automatyczne kierowanie komentarzy do dostępnych operatorów</li>
					<li class=\"im-connector-settings-header-list-item\">komunikacja z klientami bezpośrednio z czatu Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">automatyczne zapisywanie klientów w CRM</li>
				</ul>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_ADDITIONAL_DESCRIPTION"] = "Należy stworzyć publiczną stronę na Facebooku lub podłączyć już posiadaną. Twoje konto biznesowe na Instagramie musi być podłączone do owej strony na Facebooku. Wyłącznie administrator strony może podłączyć ją do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_3"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_4"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_SUBTITLE"] = "Za każdym razem, gdy klient przesyła pytanie do Twojego posta na Instagramie, otrzymujesz je w Bitrix24. Odpowiadaj szybciej i poprawiaj konwersję.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_TITLE"] = "Wiadomości bezpośrednie na Instagramie w Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_COMMENTS"] = "Niepodłączone komentarze";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TITLE_NEW"] = "Wiadomości bezpośrednie na Instagramie w Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_DIRECT_AND_BUSINESS"] = "Podłącz także komentarze pozostawione do postów i transmisji";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT"] = "Wydłuż okno wiadomości z 24 godzin do 7 dni";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Pamiętaj, że Facebook nie zezwala na wysyłanie reklam (w tym rabatów, kuponów) ani automatycznych wiadomości do klientów po wygaśnięciu 24-godzinnego okna. Jeśli nie zastosujesz się do tych wytycznych, Twoje konto zostanie zablokowane. #START_HELP_DESC#Szczegóły#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_OFF"] = "Okno wiadomości nie zostało przedłużone z 24 godzin do 7 dni";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_ON"] = "Wydłużenie okna wiadomości z 24 godzin do 7 dni";
