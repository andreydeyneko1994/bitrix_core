<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ACTIVE_COMMENTS"] = "Comentários conectados";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_OK"] = "Continuar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_DESCRIPTION"] = "Esta conta é usada com o conector Instagram Business no seu Bitrix24. Esta conexão será desativada para evitar comentários duplicados.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_TITLE"] = "Conectar mesmo assim?";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Conecte a conta comercial do Instagram para receber mensagens dos seus clientes no bate-papo Bitrix24. Para conectar, você precisa ter uma página pública do Facebook ou criar uma e conectar uma conta comercial do Instagram a ela.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">comunicação instantânea com os visitantes da sua conta do Instagram</li>
					<li class=\"im-connector-settings-header-list-item\">encaminhamento automático dos comentários para operadores disponíveis</li>
					<li class=\"im-connector-settings-header-list-item\">comunicação com os clientes diretamente do seu bate-papo Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">salvamento automático de clientes no CRM</li>
				</ul>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_ADDITIONAL_DESCRIPTION"] = "Você terá que criar uma página pública no Facebook ou conectar a que já tem. Sua conta comercial do Instagram deve estar conectada a esta página do Facebook. Apenas o administrador da página pode conectá-la ao Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_1"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_2"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_3"] = "Responda aos seus clientes quando e onde eles preferirem";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_4"] = "As consultas dos clientes são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_SUBTITLE"] = "Sempre que um cliente envia uma pergunta para sua postagem do Instagram, você a recebe no seu Bitrix24. Responda mais rápido e melhore a conversão.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_TITLE"] = "Mensagens do Instagram Direct no Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_COMMENTS"] = "Comentários não conectados";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TITLE_NEW"] = "Mensagens Direct do Instagram no Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_DIRECT_AND_BUSINESS"] = "Conectar também comentários deixados em postagens e transmissões";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT"] = "Prorrogue a janela de mensagens de 24 horas para 7 dias";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Observe que o Facebook não permite o envio de anúncios (incluindo descontos, cupons) ou mensagens automáticas para os clientes após a expiração da janela de 24 horas. Sua conta será bloqueada se você não seguir essas diretrizes. #START_HELP_DESC#Detalhes#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_OFF"] = "A janela de mensagens não foi prorrogada de 24 horas para 7 dias";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_ON"] = "Janela de mensagens prorrogada de 24 horas para 7 dias";
