<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ACTIVE_COMMENTS"] = "Comentarios conectados";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_OK"] = "Continuar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_DESCRIPTION"] = "Esta cuenta se utiliza con el conector Instagram Business en su Bitrix24. Esta conexión se desactivará para evitar comentarios duplicados.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_TITLE"] = "¿Conectar de todos modos?";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Conecte su cuenta comercial de Instagram para recibir mensajes de sus clientes en el chat de Bitrix24. Para conectarse, debe tener una página pública de Facebook o crear una, y conectar una cuenta comercial de Instagram a ella.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">comuníquese instantáneamente con los visitantes de su cuenta de Instagram</li>
					<li class=\"im-connector-settings-header-list-item\">dirija automáticamente los comentarios a los operadores disponibles</li>
					<li class=\"im-connector-settings-header-list-item\">comuníquese con sus clientes directamente desde su chat de Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">guarde a sus clientes automáticamente en el CRM</li>
				</ul>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_ADDITIONAL_DESCRIPTION"] = "Debe crear una página pública de Facebook o conectar la que ya tiene. Su cuenta comercial de Instagram debe estar conectada a esta página de Facebook. Solo el administrador de la página puede conectarlo a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_1"] = "Guarde los contactos y el historial de comunicaciones en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_2"] = "Guíe al cliente a lo largo del embudo de ventas en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_3"] = "Responda a sus clientes cuando y donde prefieran";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_4"] = "Las consultas de los clientes se distribuyen entre los agentes de ventas según las reglas de la cola.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_SUBTITLE"] = "Cada vez que un cliente envíe una pregunta a su publicación de Instagram, la recibirá en su Bitrix24. Responda más rápidamente y mejore la conversión.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_TITLE"] = "Mensajes directos de Instagram en Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_COMMENTS"] = "Comentarios no conectados";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TITLE_NEW"] = "Mensajes directos de Instagram en Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_DIRECT_AND_BUSINESS"] = "Conecte también los comentarios que se dejan en las publicaciones y transmisiones";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT"] = "Amplíe la ventana de mensajería de 24 horas a 7 días";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Tenga en cuenta que Facebook no permite enviar anuncios (incluidos descuentos, cupones) ni mensajes automáticos a los clientes una vez que expira la ventana de 24 horas. Su cuenta se bloqueará si no sigue estos lineamientos. #START_HELP_DESC#Detalles#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_OFF"] = "La ventana de mensajería no se amplió de 24 horas a 7 días";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_ON"] = "La ventana de mensajería se amplió de 24 horas a 7 días";
