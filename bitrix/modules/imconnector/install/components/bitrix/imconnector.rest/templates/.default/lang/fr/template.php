<?php
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Connecter";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue pendant l'utilisation du connecteur. Veuillez reconnecter.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Utilisez le connecteur avec votre canal ouvert pour recevoir dans le chat Bitrix24 les messages publiés par vos clients.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_SUBTITLE"] = "Utilisez le connecteur avec votre canal ouvert pour que les messages de vos clients soient postés sur le chat Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Plus d'informations sur la page des paramètres";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "Le canal a bien été configuré";
