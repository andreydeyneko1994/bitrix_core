<?
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd. Sprawdź preferencje.";
$MESS["IMCONNECTOR_COMPONENT_REST_MODULE_NOT_INSTALLED"] = "Moduł \"Konektory zewnętrznych IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_REST_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_REST_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
?>