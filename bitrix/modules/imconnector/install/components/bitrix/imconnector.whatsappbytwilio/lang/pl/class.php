<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ANSWER_CLIENT"] = "Serwer nie może uzyskać dostępu do portalu. Sprawdź ustawienia sieciowe.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_CONNECT"] = "Nie można ustanowić połączenia testowego przy użyciu podanych parametrów";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_DATA_SAVE"] = "Brak danych do zapisania";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_SAVE"] = "Wystąpił błąd podczas zapisywania danych. Sprawdź dane wejściowe i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_CONNECT"] = "Połączenie testowe zostało pomyślnie ustanowione";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SETTINGS_INCORRECT"] = "Podano nieprawidłowe parametry";
