<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZATION"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_BUSINESS_ACCOUNT"] = "Conta comercial";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_ANY_TIME"] = "Você pode editar ou desconectar a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_PAGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED"] = "Instagram conectado";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED_PAGE"] = "Conta conectada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Houve um erro. Verifique suas preferências.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_FACEBOOK_PAGE"] = "Conecte a página do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Eu gostaria de </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectar</span> uma conta de negócio</div>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONVERT_TO_BUSINESS_HELP"] = "Converter para uma conta comercial";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DEL_REFERENCE"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Conecte a conta comercial do Instagram para receber mensagens dos seus clientes no bate-papo Bitrix24. Para conectar, você precisa ter uma página pública do Facebook ou criar uma e conectar uma conta comercial do Instagram a ela.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">comentários dos seus clientes são postados diretamente no bate-papo Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">comentários são encaminhados automaticamente para operadores disponíveis</li>
					<li class=\"im-connector-settings-header-list-item\">clientes são salvos automaticamente diretamente no seu CRM</li>
				</ul>

";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN"] = "Fazer login usando sua conta do Facebook para fazer alterações.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que a página será desconectada do Bitrix24 se você fizer login usando uma conta que não está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_ADDITIONAL_DESCRIPTION"] = "Você terá que criar uma página pública no Facebook ou conectar a que já tem. Sua conta comercial do Instagram deve estar conectada a esta página do Facebook. Apenas o administrador da página pode conectá-la ao Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_1"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_2"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_3"] = "Reaja aos comentários imediatamente";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_4"] = "Os comentários são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_SUBTITLE"] = "Conecte sua página do Instagram ao seu Bitrix24 para receber os comentários dos seus clientes. Responda mais rápido e melhore a conversão.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_TITLE"] = "Receba comentários das suas postagens do Instagram diretamente no seu Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO_CONNECT_ID"] = "redirect=detail&code=4779109";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Como conectar sua conta de negócio do Instagram:</span>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Fazer login usando a conta de administrador da página do Facebook para gerenciar os comentários deixados em uma conta de negócio associada do Instagram a partir do seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MEDIA"] = "mídia";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_SPECIFIC_PAGE"] = "Está faltando uma conta obrigatória? Encontre o possível motivo #A#neste artigo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OTHER_PAGES"] = "Outras contas";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PERSONAL_ACCOUNT"] = "Conta pessoal";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PREFIX_NAMING_PAGE"] = "usando";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SELECT_THE_PAGE"] = "Selecione uma conta de negócio do Instagram vinculada à página pública do Facebook que você deseja conectar ao Canal Aberto Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não tem uma página no Facebook com uma conta de negócio associada do Instagram da qual você é um administrador.<br>Crie uma página pública no Facebook agora mesmo ou vincule uma conta do Instagram a uma página existente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TITLE_NEW"] = "Comentários em postagens de contas comerciais do Instagram";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TO_CREATE_A_PAGE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_USER"] = "Conta";
