<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZATION"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZE"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_BUSINESS_ACCOUNT"] = "Compte professionnel";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_ANY_TIME"] = "Vous pouvez éditer ou déconnecter à tout moment";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_PAGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED"] = "Instagram connecté";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED_PAGE"] = "Compte connecté";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos préférences.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_FACEBOOK_PAGE"] = "Connecter une page Facebook";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Je voudrais</div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">connecter</span> un compte professionnel</div>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONVERT_TO_BUSINESS_HELP"] = "Convertir en compte professionnel";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DEL_REFERENCE"] = "Déconnexion";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Connectez un compte professionnel Instagram pour recevoir les messages de vos clients dans le chat Bitrix24. Pour vous connecter, vous devez avoir une page publique Facebook ou en créer une, et y connecter un compte professionnel Instagram.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">les commentaires de vos clients sont postés directement sur le chat Bitrix24</li>.
					<li class=\"im-connector-settings-header-list-item\">les commentaires sont automatiquement acheminés vers les opérateurs disponibles</li>.
					<li class=\"im-connector-settings-header-list-item\">les clients sont automatiquement enregistrés directement dans votre CRM</li>.
				</ul>

";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN"] = "Connectez-vous avec votre compte Facebook pour proposer une réparation.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Notez que la page sera déconnectée de votre Bitrix24 si vous vous connectez en utilisant un compte qui n'est pas lié à cette page.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_ADDITIONAL_DESCRIPTION"] = "Vous devrez créer une page Facebook publique ou connecter celle que vous avez déjà. Votre compte professionnel Instagram doit être connecté à cette page Facebook. Seul l'administrateur de la page peut connecter à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_3"] = "Réagissez immédiatement aux commentaires";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_4"] = "Les commentaires sont répartis entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_SUBTITLE"] = "Connectez votre page Instagram publique à votre Bitrix24 pour recevoir les commentaires que vos clients laissent sur Instagram. Répondez plus rapidement et améliorez la conversion.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_TITLE"] = "Recevez les commentaires de vos publications Instagram directement sur votre Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO_CONNECT_ID"] = "redirect=detail&code=4779109";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Comment connecter votre compte Instagram professionnel : </span>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Connectez-vous en utilisant un compte administrateur de page Facebook pour gérer depuis votre Bitrix24 les commentaires laissés sur un compte Instagram professionnel associé.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MEDIA"] = "contenu multimédia";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_SPECIFIC_PAGE"] = "Il manque un compte obligatoire ? Trouvez la raison possible dans #A#cet article#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OTHER_PAGES"] = "Autres comptes";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PERSONAL_ACCOUNT"] = "Compte personnel";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PREFIX_NAMING_PAGE"] = "via";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SELECT_THE_PAGE"] = "Sélectionnez un compte Instagram professionnel associé à la page Facebook publique que vous voulez connecter au canal ouvert Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'êtes pas administrateur d'une page Facebook avec un compte Instagram professionnel associé.<br>Créez tout de suite une page Facebook publique ou associez un compte Instagram à une page existante.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TITLE_NEW"] = "Commentaires sur les posts de comptes professionnels Instagram";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TO_CREATE_A_PAGE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_USER"] = "Compte";
