<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZATION"] = "Zaloguj";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZE"] = "Zaloguj";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_BUSINESS_ACCOUNT"] = "Konto biznesowe";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub odłączyć";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_PAGE"] = "Zmień";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED"] = "Podłączono Instagram";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED_PAGE"] = "Podłączono konto";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd. Sprawdź preferencje.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_FACEBOOK_PAGE"] = "Podłącz stronę Facebooka";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę</div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">podłączyć</span> konto firmowe</div>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONVERT_TO_BUSINESS_HELP"] = "Konwertuj na konto firmowe";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DEL_REFERENCE"] = "Odłącz";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Połącz konto firmowe na Instagramie, aby otrzymywać wiadomości od swoich klientów na czacie Bitrix24. Aby się połączyć, musisz mieć publiczną stronę na Facebooku lub ją utworzyć i połączyć z nią konto firmowe na Instagramie.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">komentarze od Twoich klientów są publikowane bezpośrednio na czacie Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">komentarze są automatycznie kierowane do dostępnych operatorów</li>
					<li class=\"im-connector-settings-header-list-item\">klienci są automatycznie zapisywani bezpośrednio w Twoim CRM</li>
				</ul>

";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN"] = "Aby dokonać poprawek, zaloguj się przy użyciu konta w serwisie Facebook.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Należy pamiętać, że zalogowanie się przy użyciu konta, które nie jest powiązane z tą stroną spowoduje odłączenie strony od Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_ADDITIONAL_DESCRIPTION"] = "Należy stworzyć publiczną stronę na Facebooku lub podłączyć już posiadaną. Twoje konto biznesowe na Instagramie musi być podłączone do owej strony na Facebooku. Wyłącznie administrator strony może podłączyć ją do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_3"] = "Natychmiast reaguj na komentarze";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_4"] = "Komentarze są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_SUBTITLE"] = "Podłącz swoją stronę na Instagramie do Bitrix24 i otrzymuj komentarze od klientów. Odpowiadaj szybciej i poprawiaj konwersję.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_TITLE"] = "Otrzymuj komentarze do swoich postów na Instagramie bezpośrednio w Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO_CONNECT_ID"] = "redirect=detail&code=4779109";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Jak podłączyć konto firmowe w serwisie Instagram:</span>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Aby zarządzać komentarzami pozostawionymi na powiązanym koncie firmowym w serwisie Instagram z Bitrix24, zaloguj się za pomocą konta administratora strony na Facebooku.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MEDIA"] = "media";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_SPECIFIC_PAGE"] = "Brakuje wymaganego konta? Możliwe przyczyny znajdziesz w #A#tym artykule#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OTHER_PAGES"] = "Inne konta";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PERSONAL_ACCOUNT"] = "Konto osobiste";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PREFIX_NAMING_PAGE"] = "za pomocą";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SELECT_THE_PAGE"] = "Wybierz konto firmowe w serwisie Instagram powiązane z publiczną stroną na Facebooku, którą chcesz podłączyć do Otwartego kanału Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Nie masz strony na Facebooku z powiązanym kontem firmowym w serwisie Instagram, której jesteś administratorem.<br>Utwórz teraz publiczną stronę na Facebooku lub podłącz konto Instagram do istniejącej strony.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TITLE_NEW"] = "Komentarze do wpisów na koncie firmowym na Instagramie";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TO_CREATE_A_PAGE"] = "Utwórz";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_USER"] = "Konto";
