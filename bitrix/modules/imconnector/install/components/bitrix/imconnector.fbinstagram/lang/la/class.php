<?
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Error de registro";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INVALID_OAUTH_ACCESS_TOKEN"] = "No se puede administrar la página pública porque se perdió el acceso. Debe conectarse de nuevo a su página pública.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_AUTHORIZATION_PAGE"] = "No se puede vincular a la página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_PAGE"] = "No se puede desvincular la página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_USER"] = "No se puede desvincular su cuenta de usuario";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_AUTHORIZATION_PAGE"] = "La página ha sido vinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_PAGE"] = "La página se ha desvinculado";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_USER"] = "Su cuenta de usuario se ha desvinculado";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REMOVED_REFERENCE_TO_PAGE"] = "Esta conexión se configuró para su uso con un grupo de trabajo que actualmente no tiene acceso de administrador. <br>
Configure la conexión de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REPEATING_ERROR"] = "Si el problema persiste, es posible que deba desconectar el canal y configurarlo de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario de nuevo.";
?>