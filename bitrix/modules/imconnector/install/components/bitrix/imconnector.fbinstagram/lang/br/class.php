<?
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erro de registro";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INVALID_OAUTH_ACCESS_TOKEN"] = "Não podemos gerenciar a página pública porque o acesso foi perdido. Você precisa se reconectar à sua página pública.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MODULE_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_AUTHORIZATION_PAGE"] = "Não é possível vincular a página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_PAGE"] = "Não é possível desvincular a página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_USER"] = "Não é possível desvincular sua conta de usuário";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_AUTHORIZATION_PAGE"] = "A página foi vinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_PAGE"] = "A página foi desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_USER"] = "Sua conta de usuário foi desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REMOVED_REFERENCE_TO_PAGE"] = "Este conector foi configurado para uso com um grupo de trabalho que atualmente você não tem acesso de administrador.<br>
Configure o conector novamente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REPEATING_ERROR"] = "Se o problema persistir, talvez você precise desconectar o canal e configurá-lo novamente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
?>