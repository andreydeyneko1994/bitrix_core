<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd. Sprawdź swoje preferencje.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Błąd podczas pobierania informacji z serwera";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MODULE_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_DEL_ENTITY"] = "Nie można odłączyć grupy, strony publicznej lub zdarzenia.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_DEL_USER"] = "Nie można odłączyć konta użytkownika";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OK_DEL_ENTITY"] = "Grupa, strona publiczna lub wydarzenie zostało odłączone.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OK_DEL_USER"] = "Konto użytkownika zostało odłączone";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_REMOVED_REFERENCE_TO_ENTITY"] = "Ten konektor został skonfigurowany do użytku w ramach grupy, strony publicznej lub wydarzenia do którego obecnie nie masz dostępu na prawach administratora.<br>
Prosimy o ponowną konfigurację konektora.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Proszę prześlij formularz ponownie.";
?>