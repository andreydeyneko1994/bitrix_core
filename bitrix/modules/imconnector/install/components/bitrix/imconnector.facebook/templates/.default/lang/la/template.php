<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_MENU_TITLE"] = "Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_SETUP_FIRST_WARNING"] = "Conecte Facebook a los canales abiertos para usar el catálogo de productos en los chats en vivo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_TITLE"] = "Catálogo de productos de Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Se puede editar o desactivar en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Facebook conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Ocurrió un error. Por favor revisa su configuración.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Conecte la página de Facebook de su empresa a Open Channels y comuníquese con los usuarios de Facebook por medio de Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de los mensajes entrantes según las reglas de la cola</li>

     <li class=\"im-connector-settings-header-list-item\">interfaz del chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones se guardan en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Es necesario estar autorizado a su cuenta de Facebook para modificar los ajustes";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Por favor tenga en cuenta que cuando se autoriza con una cuenta de Facebook que no administra la página actual se desconectará del Canal Abierto de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_ADDITIONAL_DESCRIPTION"] = "Debe crear una página pública de Facebook o conectar la que ya tiene. Solo el administrador de la página puede conectarlo a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_1"] = "Guarde los contactos y el historial de comunicaciones en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_2"] = "Guíe al cliente a lo largo del embudo de ventas en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_3"] = "Responda a sus clientes cuando y donde prefieran";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_4"] = "Las consultas de los clientes se distribuyen entre los agentes de ventas según las reglas de la cola.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_SUBTITLE"] = "Conecte su página pública de Facebook a su Bitrix24 para recibir consultas de sus clientes en Facebook. Responda más rápidamente y mejore la conversión.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_TITLE"] = "Responda a las preguntas de sus clientes en Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Por favor, autorice con la cuenta de Facebook que gestiona sus páginas para recibir mensajes de su clientes desde Bitrix24

";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "¿Falta una página necesaria? Encuentre una posible razón en #A#este artículo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_ALERT"] = "Debe habilitar los productos en los canales abiertos antes de conectar el catálogo de productos.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT_HELP"] = "Haga clic en \"#BUTTON#\" y siga las instrucciones.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DESCRIPTION"] = "Todos los productos se sincronizarán automáticamente con su catálogo de Facebook.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DIFFERENT_IDS"] = "Para manejar correctamente los productos, indique la página de Facebook que está conectada específicamente a los canales abiertos.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DISCONNECT"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TEXT"] = "Haga clic en \"#BUTTON#\" y seleccione el #A_START#permiso del catálogo#A_END# en la ventana emergente.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TITLE"] = "Deje que su Bitrix24 envíe selecciones de productos al estilo de Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_REMOVE"] = "¿Realmente quiere desconectar el catálogo?";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_SUCCESS"] = "Catálogo conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_TITLE"] = "Conectar el catálogo de productos a Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Otras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Por favor seleccione la página de Facebook que debe ser conectada al Canal Abierto de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Usted no es administrador de ninguna página de Facebook.<br>Usted puede crear su página de Facebook en este momento.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Comunicarse con los usuarios de Facebook por medio de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT"] = "Amplíe la ventana de mensajería de 24 horas a 7 días";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Tenga en cuenta que Facebook no permite enviar anuncios (incluidos descuentos, cupones) ni mensajes automáticos a los clientes una vez que expira la ventana de 24 horas. Su cuenta se bloqueará si no sigue estos lineamientos. #START_HELP_DESC#Detalles#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_OFF"] = "La ventana de mensajería no se amplió de 24 horas a 7 días";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_ON"] = "La ventana de mensajería se amplió de 24 horas a 7 días";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Cuenta";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_SUCCESS"] = "Acceso permitido.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_WRONG_USER"] = "Error al solicitar permisos de acceso al catálogo. Esta solicitud debe provenir de un usuario que se conectó a Facebook en primer lugar.";
