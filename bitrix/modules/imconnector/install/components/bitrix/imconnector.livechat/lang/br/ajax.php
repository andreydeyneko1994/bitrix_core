<?
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "O módulo \"Canais Abertos\" não está instalado.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["IMCONNECTOR_PERMISSION_DENIED"] = "Você não tem permissão para acessar este recurso.";
?>