<?
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "Moduł \"Otwarte kanały\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
$MESS["IMCONNECTOR_PERMISSION_DENIED"] = "Nie masz uprawnień dostępu do tej funkcji.";
?>