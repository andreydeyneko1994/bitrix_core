<?php
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_CONNECTOR_ERROR_STATUS"] = "Il y a eu une erreur. Veuillez vérifier vos préférences.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_MODULE_NOT_INSTALLED"] = "Le module \"External IM Connectors\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_NO_ACTIVE_CONNECTOR"] = "Le connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
