<?php
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_LIMIT"] = "Seu plano atual restringe o número de Canais Abertos ativos. Por favor, desative qualquer outro Canal Aberto ativo atualmente antes de ativar este.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_PERMISSION"] = "Permissões insuficientes para modificar este Canal Aberto.";
