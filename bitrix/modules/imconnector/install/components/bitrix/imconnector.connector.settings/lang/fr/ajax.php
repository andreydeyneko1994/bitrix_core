<?php
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_LIMIT"] = "Votre offre actuelle restreint le nombre de Canaux ouverts actifs. Veuillez en désactiver avant d'activer celui-ci.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_PERMISSION"] = "Permissions insuffisantes pour modifier ce Canal ouvert.";
