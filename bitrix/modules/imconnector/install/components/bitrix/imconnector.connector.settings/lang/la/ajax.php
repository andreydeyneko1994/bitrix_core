<?php
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_LIMIT"] = "Su plan actual restringe la cantidad de canales abiertos que pueden estar activos. Desactive cualquier otro canal abierto que esté activo actualmente antes de activar este.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_PERMISSION"] = "No hay suficientes permisos para modificar este canal abierto.";
