<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE_VIRTUAL_WHATSAPP"] = "Répondez à vos clients comme ils le souhaitent. Si un client trouve WhatsApp plus pratique que les autres moyens de communication, utilisez WhatsApp avec votre Bitrix24 et répondez-lui immédiatement. ";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE_VIRTUAL_WHATSAPP"] = "Envoyez des messages automatisés à vos clients via WhatsApp ou SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_HEADER"] = "Parlez à vos clients dans Instant WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DESCRIPTION"] = "Répondez à vos clients comme ils le souhaitent. Si un client trouve WhatsApp plus pratique que les autres moyens de communication, utilisez WhatsApp avec votre Bitrix24 et répondez-lui immédiatement.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DETAILS"] = "Détails";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_1"] = "Connectez-vous maintenant, vous n'avez pas besoin d'inscrire votre entreprise auprès de WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_2"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_3"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_4"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_5"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_DESCRIPTION"] = "Votre client utilisera le widget de votre site pour ouvrir WhatsApp sur son téléphone ou son ordinateur. Le champ de saisie du message contiendra l'ID d'un agent commercial chargé de communiquer avec le client. Dès que le client envoie son message, une fenêtre de chat en direct apparaît dans le formulaire de la transaction pour vous permettre de communiquer avec le client dans Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_TITLE"] = "Comment cela fonctionne ?";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SUBTITLE"] = "Répondez à vos clients comme ils le souhaitent. Si un client trouve WhatsApp plus pratique que les autres moyens de communication, utilisez WhatsApp avec votre Bitrix24 et répondez-lui immédiatement.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_TITLE"] = "Parlez à vos clients dans Instant WhatsApp";
