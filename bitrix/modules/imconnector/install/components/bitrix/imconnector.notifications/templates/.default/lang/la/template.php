<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "Mensajes SMS y de WhatsApp en Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_ADDITIONAL_DESCRIPTION"] = "Los mensajes automatizados basados en plantillas están disponibles para las herramientas de CRM + Tienda en línea y Recibir pago en el formulario de detalles de la oferta.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Conecte el Centro de notificaciones de Bitrix24 a Open Channel para enviar notificaciones móviles a sus clientes";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Los mensajes basados en plantillas pueden usarse con la nueva Tienda en línea y la acción Recibir pago en el formulario de la negociación.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Detalles";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_1"] = "Envíe enlaces de pago mediante WhatsApp o SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_2"] = "Ofrezca tarjetas bancarias, Apple Pay, Google Pay y otros métodos de pago.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_3"] = "Sin proveedores externos ni controles de WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_DESCRIPTION"] = "El cliente puede responder a su mensaje automatizado y podrá seguir comunicándose con él. Recibirá los mensajes del cliente en el chat en vivo, mientras que el cliente verá sus respuestas en su WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_TITLE"] = "Siga comunicándose con el cliente mediante WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE"] = "Consienta a sus clientes con un servicio sin complicaciones.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE"] = "Envíe mensajes automatizados a sus clientes mediante WhatsApp o SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "El Open Channel de su empresa ya está conectado al Centro de notificaciones de Bitrix24";
