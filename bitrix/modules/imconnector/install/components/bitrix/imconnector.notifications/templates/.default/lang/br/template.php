<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "Mensagens SMS e WhatsApp no Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_ADDITIONAL_DESCRIPTION"] = "Mensagens automáticas baseadas em modelos estão disponíveis para ferramentas CRM + Loja Online e Receber Pagamento no formulário de informações do negócio.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Conecte a Central de Notificações Bitrix24 ao Canal Aberto para enviar notificações por celular aos seus clientes";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Mensagens baseadas em modelos podem ser usadas na nova Loja Online e na ação Receber Pagamento no formulário de negócio.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_1"] = "Envie links de pagamento pelo WhatsApp ou por SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_2"] = "Ofereça cartão bancário, Apple Pay, Google Pay e outras formas de pagamento";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_3"] = "Sem provedores externos ou verificações do WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_DESCRIPTION"] = "O cliente pode optar por responder sua mensagem automática e você pode continuar se comunicando com ele: você receberá as mensagens do cliente no bate-papo ao vivo e o cliente verá as suas respostas no WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_TITLE"] = "Continue se comunicando com o cliente no WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE"] = "Satisfaça seus clientes com um serviço descomplicado.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE"] = "Envie mensagens automáticas para os seus clientes pelo WhatsApp ou por SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "O Canal Aberto da sua empresa já está conectado à Central de Notificações Bitrix24";
