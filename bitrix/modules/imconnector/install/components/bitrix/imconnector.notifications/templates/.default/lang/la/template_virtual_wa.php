<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE_VIRTUAL_WHATSAPP"] = "Responda a sus clientes en la forma que prefieran. Si un cliente opina que WhatsApp es más conveniente que otros medios de comunicación, utilice WhatsApp en su Bitrix24 y responda de inmediato. ";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE_VIRTUAL_WHATSAPP"] = "Envíe mensajes automatizados a sus clientes mediante WhatsApp o SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_HEADER"] = "Hable con sus clientes en WhatsApp instantáneo";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DESCRIPTION"] = "Responda a sus clientes en la forma que prefieran. Si un cliente opina que WhatsApp es más conveniente que otros medios de comunicación, utilice WhatsApp en su Bitrix24 y responda de inmediato.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DETAILS"] = "Detalles";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_1"] = "Conéctese ahora, no es necesario que registre su empresa en WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_2"] = "Guarde los contactos y el historial de comunicaciones en el CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_3"] = "Guíe al cliente a lo largo del embudo de ventas en el CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_4"] = "Responda a sus clientes cuando y donde prefieran";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_5"] = "Las consultas de los clientes se distribuyen entre los agentes de ventas según las reglas de la cola";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_DESCRIPTION"] = "Su cliente usará el widget en su sitio web para abrir WhatsApp en su teléfono o computadora. El campo de entrada del mensaje contendrá el ID de un agente de ventas asignado para comunicarse con el cliente. Tan pronto como el cliente envíe su mensaje, aparecerá una ventana de chat en vivo en el formulario de oferta para que se comunique con el cliente dentro de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_TITLE"] = "¿Cómo funciona?";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SUBTITLE"] = "Responda a sus clientes en la forma que prefieran. Si un cliente opina que WhatsApp es más conveniente que otros medios de comunicación, utilice WhatsApp en su Bitrix24 y responda de inmediato.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_TITLE"] = "Hable con sus clientes en WhatsApp instantáneo";
