<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "Messages SMS et WhatsApp dans Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_ADDITIONAL_DESCRIPTION"] = "Des messages automatisés basés sur des modèles sont disponibles pour les outils CRM+Boutique en ligne et Recevoir des paiements dans le formulaire de détail des transactions.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Connectez le Centre de notifications Bitrix24 au canal ouvert pour envoyer des notifications mobiles à vos clients";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Les messages basés sur des modèles peuvent être utilisés avec la nouvelle boutique en ligne et l'action Recevoir un paiement dans le formulaire de transaction.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Détails";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_1"] = "Envoyez des liens de paiement par WhatsApp ou SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_2"] = "Proposez la carte bancaire, Apple Pay, Google Pay et d'autres modes de paiement";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_3"] = "Pas de fournisseurs externes ou de contrôles WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_DESCRIPTION"] = "Le client peut choisir de répondre à votre message automatisé, auquel cas vous pouvez continuer à communiquer avec lui : vous recevrez les messages du client sur le chat en direct, tandis que le client verra vos réponses dans son WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_TITLE"] = "Continuez à communiquer avec le client sur WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE"] = "Faites plaisir à vos clients en leur proposant un service sans tracas.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE"] = "Envoyez des messages automatisés à vos clients via WhatsApp ou SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "Le canal ouvert de votre entreprise est déjà connecté au Centre de notifications Bitrix24";
