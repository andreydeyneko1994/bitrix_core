<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE_VIRTUAL_WHATSAPP"] = "Converse com seus clientes da maneira que eles quiserem. Se um cliente achar o WhatsApp mais conveniente do que outros meios de comunicação, use o WhatsApp com o seu Bitrix24 e responda imediatamente. ";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE_VIRTUAL_WHATSAPP"] = "Envie mensagens automáticas para os seus clientes pelo WhatsApp ou por SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_HEADER"] = "Converse com seus clientes no WhatsApp Instantâneo";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DESCRIPTION"] = "Converse com seus clientes da maneira que eles quiserem. Se um cliente achar o WhatsApp mais conveniente do que outros meios de comunicação, use o WhatsApp com o seu Bitrix24 e responda imediatamente.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DETAILS"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_1"] = "Conecte-se agora, não há necessidade de cadastrar sua empresa no WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_2"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_3"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_4"] = "Responda aos seus clientes quando e onde eles preferirem";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_5"] = "As consultas dos clientes são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_DESCRIPTION"] = "Seu cliente usará o widget no seu site para abrir o WhatsApp no celular ou computador dele. O campo de inserção da mensagem conterá o ID de um representante de vendas designado para se comunicar com o cliente. Assim que o cliente envia a mensagem, uma janela de bate-papo ao vivo aparece no formulário de negócio para você se comunicar com o cliente dentro do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_TITLE"] = "Como isso funciona?";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SUBTITLE"] = "Converse com seus clientes da maneira que eles quiserem. Se um cliente achar o WhatsApp mais conveniente do que outros meios de comunicação, use o WhatsApp com o seu Bitrix24 e responda imediatamente.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_TITLE"] = "Converse com seus clientes no WhatsApp Instantâneo";
