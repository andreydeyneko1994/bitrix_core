<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "Wiadomości SMS i WhatsApp w Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_ADDITIONAL_DESCRIPTION"] = "Zautomatyzowane wiadomości oparte na szablonach są dostępne w ramach narzędzi CRM+Online Store oraz narzędzi do otrzymywania płatności w formularzu szczegółów deala.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Podłącz Centrum powiadomień Bitrix24 do Otwartego kanału, aby wysyłać powiadomienia mobilne do klientów";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Wiadomości opartych na szablonach można używać z nowym sklepem internetowym i z działaniem Odbierz płatność w formularzu dealu.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Szczegóły";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_1"] = "Wysyłaj linki do płatności za pośrednictwem WhatsApp lub wiadomości SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_2"] = "Oferuj płatności kartą, Apple Pay, Google Pay i innymi metodami";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_LIST_ITEM_3"] = "Brak powiadomień moderacji dotyczących dostawców zewnętrznych oraz WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_DESCRIPTION"] = "Klient może chcieć odpowiedzieć na automatyczną wiadomość. Wówczas komunikacja jest utrzymywana: będziesz otrzymywać wiadomości od klienta na czacie na żywo, podczas gdy klient będzie widzieć odpowiedzi za pośrednictwem WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SECOND_TITLE"] = "Kontynuuj komunikację z klientem za pośrednictwem WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE"] = "Zapewniaj klientom bezproblemową obsługę.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE"] = "Wysyłaj zautomatyzowane wiadomości do klientów za pośrednictwem WhatsApp lub wiadomości SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "Otwarty kanał Twojej firmy jest już podłączony do Centrum powiadomień Bitrix24";
