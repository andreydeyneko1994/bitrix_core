<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Los archivos que está intentando subir no son válidos. Solo se admiten archivos JPG y PNG.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE"] = "Error al activar el conector";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_SAVE"] = "Error al guardar los datos. Revise la entrada e intente de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_OK_SAVE"] = "La información se guardó correctamente.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SESSION_HAS_EXPIRED"] = "Su sesión caducó. Envíe el formulario de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_TOS_AGREE"] = "Acepto";
