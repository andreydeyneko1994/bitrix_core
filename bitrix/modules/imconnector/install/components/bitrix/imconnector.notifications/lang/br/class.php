<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Os arquivos que você está tentando enviar não são válidos. Apenas arquivos JPG e PNG são suportados.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE"] = "Erro ao ativar connector";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_SAVE"] = "Erro ao salvar os dados. Verifique seus dados e tente novamente.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_OK_SAVE"] = "A informação foi salva com sucesso.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_TOS_AGREE"] = "Concordo";
