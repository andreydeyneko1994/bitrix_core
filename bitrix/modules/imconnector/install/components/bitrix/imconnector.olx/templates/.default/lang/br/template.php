<?php
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP_NEW"] = "Você deve #LINK_START#criar uma conta#LINK_END# ou usar a que já tem. Se você não tem uma conta, ajudaremos você a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Você deve #LINK_START#criar uma conta OLX#LINK_END# ou usar a que já tem. Se você não tem uma conta, ajudaremos você a criar uma em poucos passos e conectá-la ao seu Bitrix24.";
