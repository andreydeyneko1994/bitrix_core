<?php
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP_NEW"] = "Vous devez #LINK_START#créer un compte#LINK_END# ou utiliser celui que vous avez déjà. Si vous n'avez pas de compte, nous vous aiderons à le créer et à le connecter à votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Vous devez #LINK_START#créer un compte OLX#LINK_END# ou utiliser celui que vous avez déjà. Si vous n'avez pas de compte, nous vous aiderons à le créer en quelques étapes et à le connecter à votre Bitrix24.";
