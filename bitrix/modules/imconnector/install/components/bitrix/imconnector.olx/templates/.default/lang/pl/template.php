<?php
$MESS["IMCONNECTOR_COMPONENT_OLX_AUTHORIZATION"] = "Uwierzytelnianie";
$MESS["IMCONNECTOR_COMPONENT_OLX_AUTHORIZE"] = "Zaloguj się";
$MESS["IMCONNECTOR_COMPONENT_OLX_CHANGE_ANY_TIME"] = "W każdej chwili istnieje możliwość edytowania lub odłączenia";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECTED"] = "OLX podłączony";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę</div>
				<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">podłączyć</span> konto OLX</div>";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP"] = "Przed podłączeniem musisz <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">utworzyć konto OLX</a>
 lub podłączyć istniejące. Możemy pomóc utworzyć konto OLX i podłączyć je do Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP_NEW"] = "#LINK_START#Utwórz konto#LINK_END# lub użyj już istniejącego. Jeśli nie masz konta, pomożemy Ci je utworzyć i podłączyć do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_TITLE"] = "Podłącz OLX do Otwartego kanału";
$MESS["IMCONNECTOR_COMPONENT_OLX_FINAL_FORM_DESCRIPTION"] = "OLX został pomyślnie podłączony do Otwartego kanału. Teraz wszystkie wiadomości wysłane na Twoje konto OLX będą przekazywane do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "#LINK_START#Utwórz konto na OLX#LINK_END# lub użyj już istniejącego. Jeśli nie masz konta, pomożemy Ci je utworzyć w kilku krokach i podłączyć do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_OLX_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Instrukcje podłączania:</span>";
$MESS["IMCONNECTOR_COMPONENT_OLX_LOG_IN_OAUTH"] = "Zaloguj się na swoje konto OLX.";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_DOMAIN"] = "Domena:";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_EMAIL"] = "E-mail użytkownika:";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_ID"] = "ID użytkownika:";
