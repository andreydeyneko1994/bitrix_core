<?
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "Nie można utworzyć czatu dla wiadomości przychodzącej";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "Nie można dodać wiadomości";
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "Nie można utworzyć użytkownika lub zmapować go na użytkownika komunikatora zewnętrznego.";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "Nie można uzyskać ID użytkownika IM.";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "Nowy czat z #USER_NAME# przez #CONNECTOR#";
?>