<?
$MESS["IMCONNECTOR_COMMENT_IN_FACEBOOK"] = "Komentarz na Facebooku";
$MESS["IMCONNECTOR_CONTACT_NAME"] = "Imię i nazwisko: ";
$MESS["IMCONNECTOR_CONTACT_PHONE"] = "Telefon: ";
$MESS["IMCONNECTOR_FORWARDED_MESSAGE"] = "Przekazano wiadomość: ";
$MESS["IMCONNECTOR_GUEST_USER"] = "Gość";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST"] = "Link do oryginalnego posta: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_FACEBOOK"] = "Link do oryginalnego posta na Facebooku: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_INSTAGRAM"] = "Link do oryginalnego posta na Instagramie: #LINK#";
$MESS["IMCONNECTOR_MAPS_NAME"] = "Mapa";
$MESS["IMCONNECTOR_WALL_DATE_TEXT"] = "opublikowany dnia";
$MESS["IMCONNECTOR_WALL_TEXT"] = "Post na tablicy, autor";
$MESS["IMCONNECTOR_WARNING_LARGE_FILE"] = "[b]Uwaga![/b]
[i]Użytkownik podjął próbę wysłania dużego pliku, którego nie możemy dostarczyć.
Poproś o przesłanie pliku dowolną inną metodą (na przykład: skorzystaj z usługi udostępniania plików).[/i]

";
?>