<?
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE"] = "Uwaga! Serwis Instagram zaktualizował swój interfejs API. 
Musisz ponownie podłączyć swoje konto Instagram przy użyciu nowego kanału centrum kontaktowego: [URL = #CONNECTOR_URL#]Instagram Business[/URL]. 

Zdecydowanie zalecamy ponowne podłączenie, ponieważ starszy konektor zostanie wkrótce dezaktywowany.

[URL=https://helpdesk.bitrix24.com/open/4779313/]Szczegóły[/URL]";
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE_OUT"] = "Uwaga! Serwis Instagram zaktualizował swój interfejs API.
Musisz ponownie podłączyć swoje konto Instagram przy użyciu nowego kanału centrum kontaktowego: <a onclick=\"top.BX.SidePanel.Instance.open('#CONNECTOR_URL#', {width: 700})\" style=\"cursor: pointer\">Instagram Business</a>. 

Zdecydowanie zalecamy ponowne podłączenie, ponieważ starszy konektor zostanie wkrótce dezaktywowany.

<a onclick=\"top.BX.Helper.show('redirect=detail&code=4779313');\" style=\"cursor: pointer\">Szczegóły</a>";
?>