<?php
$MESS["IMCONNECTOR_FACEBOOK_ADDITIONAL_DATA"] = "Dodatkowe szczegóły:";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ minęło 7 dni oczekiwania na odpowiedź klienta. #A_START#Szczegóły#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ wygasło 24-godzinne okno oczekiwania na odpowiedź klienta. Możesz przedłużyć okno wiadomości do 7 dni. Wówczas należy ponownie wysłać tę wiadomość. #A_START#Szczegóły#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ klient nie odpowiedział w dozwolonym czasie. #A_START#Szczegóły#A_END#";
