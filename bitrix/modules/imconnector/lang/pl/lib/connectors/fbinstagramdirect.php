<?php
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ minęło 7 dni oczekiwania na odpowiedź klienta. #A_START#Szczegóły#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ wygasło 24-godzinne okno oczekiwania na odpowiedź klienta. Możesz przedłużyć okno wiadomości do 7 dni. Wówczas należy ponownie wysłać tę wiadomość. #A_START#Szczegóły#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "Wiadomość nie została dostarczona, ponieważ klient nie odpowiedział w dozwolonym czasie. #A_START#Szczegóły#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_FOR_COMMENT"] = "Nie można odpowiedzieć na komentarz Instagram Direct, ponieważ początkowy komentarz został usunięty lub transmisja nadrzędna zakończyła się.";
$MESS["IMCONNECTOR_MESSAGE_LINK_COMMENT_MEDIA_INSTAGRAM"] = "Użytkownik zostawił komentarz do posta na Instagramie. [URL=#URL#]Wyświetl post[/URL]";
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "Klient przesłał Ci wiadomość. Jednak ten typ wiadomości nie jest tymczasowo obsługiwany. Aby przeczytać wiadomość, użyj aplikacji Instagram.";
