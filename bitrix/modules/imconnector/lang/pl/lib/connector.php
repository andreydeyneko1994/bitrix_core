<?php
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Nazwy domeny określone w module Kernel i aktualna domena się różnią. Nazwa domeny #DOMAIN# określona w module Kernel jest użyta dla odpowiedzi serwera. Aby zmienić domenę, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">zmodyfikuj pole \"URL strony\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "Pole modułu Kernel \"URL strony\" jest puste. Aktualna nazwa domeny  #DOMAIN# będzie użyta do odpowiedzi serwera. Aby zmienić nazwę domeny, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">zmodyfikuj \"URL strony\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_AVITO"] = "Avito";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Komentarze";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAM"] = "Instagram Business";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAMDIRECT"] = "Instagram Direct";
$MESS["IMCONNECTOR_NAME_CONNECTOR_IMESSAGE_NEW"] = "Apple Messages for Business";
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Czat na żywo";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NOTIFICATIONS_2"] = "SMS i WhatsApp w Bitrix24";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OK"] = "Odnoklassniki";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OLX"] = "OLX";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegram";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WECHAT"] = "WeChat";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WHATSAPPBYTWILIO"] = "WhatsApp";
