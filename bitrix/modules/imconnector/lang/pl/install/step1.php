<?
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "Adres publiczny witryny internetowej:";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "Konektory wymagają adresu publicznego witryny do działania zgodnie z przeznaczeniem.";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "Jeśli dostęp do sieci z zewnątrz jest ograniczony, włącz dostęp jedynie do określonych stron. Szczegółowe informacje można znaleźć w #LINK_START#dokumentacji#LINK_END#.";
?>