<?php
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Podłącz zewnętrzne komunikatory z pomocą tego modułu.";
$MESS["IMCONNECTOR_MODULE_NAME"] = "Konektory zewnętrznych komunikatów";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Deinstalacja modułu";
