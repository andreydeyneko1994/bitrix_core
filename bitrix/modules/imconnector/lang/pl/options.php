<?php
$MESS["IMCONNECTOR_ERROR_PUBLIC"] = "Nieprawidłowy adres publiczny.";
$MESS["IMCONNECTOR_ERROR_PUBLIC_CHECK"] = "Błąd podczas sprawdzania adresu publicznego: #ERROR#";
$MESS["IMCONNECTOR_FIELD_DEBUG_TITLE"] = "Włącz tryb debugowania";
$MESS["IMCONNECTOR_FIELD_LIST_CONNECTOR_TITLE"] = "Używane konektory";
$MESS["IMCONNECTOR_FIELD_URI_CLIENT_TITLE"] = "Adres publiczny witryny internetowej";
$MESS["IMCONNECTOR_FIELD_URI_SERVER_TITLE"] = "Adres serwera (przykład: im.bitrix.info)";
$MESS["IMCONNECTOR_TAB_SETTINGS"] = "Konektory komunikatora";
