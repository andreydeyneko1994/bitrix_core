<?php
$MESS["IMCONNECTOR_ERROR_PUBLIC"] = "O endereço público está incorreto.";
$MESS["IMCONNECTOR_ERROR_PUBLIC_CHECK"] = "Erro ao verificar endereço público: #ERROR#";
$MESS["IMCONNECTOR_FIELD_DEBUG_TITLE"] = "Ativar modo de depuração";
$MESS["IMCONNECTOR_FIELD_LIST_CONNECTOR_TITLE"] = "Conectores em uso";
$MESS["IMCONNECTOR_FIELD_URI_CLIENT_TITLE"] = "Endereço público do site";
$MESS["IMCONNECTOR_FIELD_URI_SERVER_TITLE"] = "Endereço do servidor (exemplo: im.bitrix.info)";
$MESS["IMCONNECTOR_TAB_SETTINGS"] = "Conectores do Messenger";
