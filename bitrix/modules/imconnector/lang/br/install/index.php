<?php
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Instalação do Módulo";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Conectar messengers externos usando este módulo.";
$MESS["IMCONNECTOR_MODULE_NAME"] = "Conectores de Messenger Externo";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Desinstalação do Módulo";
