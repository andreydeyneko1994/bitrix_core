<?
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "Endereço público do site:";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "Os conectores exigem que o endereço do site público seja executado conforme pretendido.";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "Se o acesso externo à sua rede for restrito, ative o acesso a apenas algumas páginas. Consulte #LINK_START#documentação#LINK_END# para informações.";
?>