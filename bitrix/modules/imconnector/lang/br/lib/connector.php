<?php
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Os nomes de domínio especificados nas configurações do módulo Kernel e o domínio atual são diferentes. O nome de domínio #DOMAIN# especificado nas configurações do módulo Kernel é utilizado para resposta do servidor. Para alterar o nome de domínio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifique o campo \"URL do site\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "O campo \"URL do site\" do módulo Kernel está vazio. O nome de domínio atual #DOMAIN# será utilizado para resposta do servidor. Para alterar o nome de domínio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifique o campo \"URL do site\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_AVITO"] = "Avito";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Comentários";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAM"] = "Instagram Business";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAMDIRECT"] = "Instagram Direct";
$MESS["IMCONNECTOR_NAME_CONNECTOR_IMESSAGE_NEW"] = "Apple Messages for Business";
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Bate-papo ao vivo";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NOTIFICATIONS_2"] = "SMS e WhatsApp Bitrix24";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OK"] = "Odnoklassniki";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OLX"] = "OLX";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegram";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WECHAT"] = "WeChat";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WHATSAPPBYTWILIO"] = "WhatsApp";
