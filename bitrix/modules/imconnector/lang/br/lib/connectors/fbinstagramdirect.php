<?php
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "A mensagem não foi entregue porque a janela de 7 dias de espera pela resposta do cliente expirou. #A_START#Detalhes#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "A mensagem não foi entregue porque a janela de 24 horas de espera pela resposta do cliente expirou. Você pode prorrogar a janela de mensagens para 7 dias. Então você terá que enviar esta mensagem novamente. #A_START#Detalhes#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "A mensagem não foi entregue porque o cliente não respondeu no prazo permitido. #A_START#Detalhes#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_FOR_COMMENT"] = "Não é possível responder ao comentário do Instagram Direct porque o comentário inicial foi excluído ou a transmissão original terminou.";
$MESS["IMCONNECTOR_MESSAGE_LINK_COMMENT_MEDIA_INSTAGRAM"] = "O usuário deixou um comentário na postagem do Instagram. [URL=#URL#]Visualizar postagem[/URL]";
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "O cliente enviou uma mensagem para você. No entanto, esse tipo de mensagem não é suportado temporariamente. Use seu aplicativo do Instagram para ler a mensagem.";
