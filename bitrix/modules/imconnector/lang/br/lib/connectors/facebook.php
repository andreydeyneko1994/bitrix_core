<?php
$MESS["IMCONNECTOR_FACEBOOK_ADDITIONAL_DATA"] = "Dados recebidos:";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "A mensagem não foi entregue porque a janela de 7 dias de espera pela resposta do cliente expirou. #A_START#Detalhes#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "A mensagem não foi entregue porque a janela de 24 horas de espera pela resposta do cliente expirou. Você pode prorrogar a janela de mensagens para 7 dias. Então você terá que enviar esta mensagem novamente. #A_START#Detalhes#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "A mensagem não foi entregue porque o cliente não respondeu no prazo permitido. #A_START#Detalhes#A_END#";
