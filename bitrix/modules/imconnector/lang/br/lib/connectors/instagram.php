<?
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE"] = "Atenção! O Instagram atualizou sua API. 
Você precisa conectar sua conta do Instagram novamente usando o novo canal de contact center: [UR =#CONNECTOR_URL#]Instagram Business[/URL]. 

É altamente recomendável que você se reconecte agora porque o conector herdado será desativado em breve.

[URL=https://helpdesk.bitrix24.com/open/4779313/]Detalhes[/URL]";
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE_OUT"] = "Atenção! O Instagram atualizou sua API.
Você tem que conectar sua conta do Instagram novamente usando o novo canal de contact center: <a onclick=\"top.BX.SidePanel.Instance.open('#CONNECTOR_URL#', {width: 700})\" style=\"cursor: pointer\">Instagram Business</a>. 

É altamente recomendável que você se reconecte agora porque o conector herdado será desativado em breve.

<a onclick=\"top.BX.Helper.show('redirect=detail&code=4779313');\" style=\"cursor: pointer\">Detalhes</a>";
?>