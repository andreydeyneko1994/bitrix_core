<?
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "Não é possível criar bate-papo na mensagem recebida";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "Não é possível adicionar mensagem";
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "Não é possível criar ou obter usuário mapeado para usuário messenger externo.";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "Falha ao obter o ID do usuário de IM.";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "Novo bate-papo com #USER_NAME# via #CONNECTOR#";
?>