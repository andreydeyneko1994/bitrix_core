<?
$MESS["IMCONNECTOR_COMMENT_IN_FACEBOOK"] = "Comentário do Facebook";
$MESS["IMCONNECTOR_CONTACT_NAME"] = "Nome completo: ";
$MESS["IMCONNECTOR_CONTACT_PHONE"] = "Telefone: ";
$MESS["IMCONNECTOR_FORWARDED_MESSAGE"] = "Mensagem enviada: ";
$MESS["IMCONNECTOR_GUEST_USER"] = "Convidado";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST"] = "Link para a postagem original: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_FACEBOOK"] = "Link para a postagem original no Facebook: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_INSTAGRAM"] = "Link para a postagem original no Instagram: #LINK#";
$MESS["IMCONNECTOR_MAPS_NAME"] = "Mapa";
$MESS["IMCONNECTOR_WALL_DATE_TEXT"] = "postado em";
$MESS["IMCONNECTOR_WALL_TEXT"] = "Postagem no mural por";
$MESS["IMCONNECTOR_WARNING_LARGE_FILE"] = "[b]Atenção![/b]
[i]Um usuário tentou enviar um arquivo grande que não podemos entregar.
Peça que enviem o arquivo usando qualquer outro método (por exemplo: use um serviço de compartilhamento de arquivos).[/i]

";
?>