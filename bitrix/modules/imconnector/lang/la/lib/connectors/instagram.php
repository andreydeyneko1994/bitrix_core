<?
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE"] = "¡Advertencia! Instagram actualizó su API. 
Debe conectar de nuevo su cuenta de Instagram utilizando el nuevo canal del Contact Center: [URL=#CONNECTOR_URL#]Instagram para negocios[/URL]. 

Le recomendamos encarecidamente que se conecte de nuevo en este momento porque el conector heredado se desactivará pronto.

[URL=https://helpdesk.bitrix24.com/open/4779313/]Detalles[/URL]";
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE_OUT"] = "¡Advertencia! Instagram actualizó su API. 
Debe conectar de nuevo su cuenta de Instagram utilizando el nuevo canal del Contact Center <a onclick=\"top.BX.SidePanel.Instance.open('#CONNECTOR_URL#', {width: 700})\" style=\"cursor: pointer\">Instagram para negocios</a>. 

Le recomendamos encarecidamente que se conecte de nuevo en este momento porque el conector heredado se desactivará pronto.

<a onclick=\"top.BX.Helper.show('redirect=detail&code=4779313');\" style=\"cursor: pointer\">Detalles</a>";
?>