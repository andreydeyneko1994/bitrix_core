<?php
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "El mensaje no se envió porque expiró el periodo de espera de 7 días para la respuesta del cliente. #A_START#Detalles#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "El mensaje no se envió porque expiró el periodo de espera de 24 horas para la respuesta del cliente. Puede ampliar la ventana de mensajería a 7 días. Tendrá que enviar este mensaje de nuevo. #A_START#Detalles#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "El mensaje no se envió porque el cliente no respondió en el plazo permitido. #A_START#Detalles#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_FOR_COMMENT"] = "No se puede responder al comentario de Instagram Direct porque se eliminó el comentario inicial o finalizó la transmisión principal.";
$MESS["IMCONNECTOR_MESSAGE_LINK_COMMENT_MEDIA_INSTAGRAM"] = "El usuario dejó un comentario en la publicación de Instagram. [URL=#URL#]Ver la publicación[/URL]";
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "El cliente le envió un mensaje. Sin embargo, este tipo de mensaje no se admite temporalmente. Utilice su aplicación de Instagram para leer el mensaje.";
