<?php
$MESS["IMCONNECTOR_FACEBOOK_ADDITIONAL_DATA"] = "Datos recibidos:";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "El mensaje no se envió porque expiró el periodo de espera de 7 días para la respuesta del cliente. #A_START#Detalles#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "El mensaje no se envió porque expiró el periodo de espera de 24 horas para la respuesta del cliente. Puede ampliar la ventana de mensajería a 7 días. Tendrá que enviar este mensaje de nuevo. #A_START#Detalles#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "El mensaje no se envió porque el cliente no respondió en el plazo permitido. #A_START#Detalles#A_END#";
