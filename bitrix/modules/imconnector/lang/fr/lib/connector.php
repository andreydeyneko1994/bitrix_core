<?php
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Les noms de domaine spécifiés dans les paramètres du module Noyau et le domaine actuel sont différents. Le nom de domaine #DOMAIN# spécifié dans les paramètres du module Noyau est utilisé pour la réponse du serveur. Pour changer le nom de domaine, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifiez le champ \"URL du site\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "Le champ \"URL du site\" du module Noyeau est vide. Le nom du domaine actuel #DOMAIN# sera utilisé pour la réponse du serveur. Pour changer le nom de domaine, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifiez le champ \"URL du site\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_AVITO"] = "Avito";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Commentaires";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAM"] = "Instagram professionnel";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FBINSTAGRAMDIRECT"] = "Instagram Direct";
$MESS["IMCONNECTOR_NAME_CONNECTOR_IMESSAGE_NEW"] = "Apple Messages for Business";
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Chat live";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NOTIFICATIONS_2"] = "Bitrix24 SMS et WhatsApp";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OK"] = "Odnoklassniki";
$MESS["IMCONNECTOR_NAME_CONNECTOR_OLX"] = "OLX";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegram";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WECHAT"] = "WeChat";
$MESS["IMCONNECTOR_NAME_CONNECTOR_WHATSAPPBYTWILIO"] = "WhatsApp";
