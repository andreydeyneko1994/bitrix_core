<?
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE"] = "Attention ! Instagram a mis à jour son API. 
Vous devez reconnecter votre compte Instagram en utilisant le canal du nouveau centre de contact : [URL=#CONNECTOR_URL#]Instagram professionnel[/URL]. 

Nous vous conseillons vivement de reconnecter tout de suite parce que le connecteur hérité sera bientôt désactivé.

[URL=https://helpdesk.bitrix24.com/open/4779313/]Plus d'informations[/URL]";
$MESS["CONNECTORS_INSTAGRAM_NEW_CONNECTOR_NOTIFY_MESSAGE_OUT"] = "Attention ! Instagram a mis à jour son API. 
Vous devez reconnecter votre compte Instagram en utilisant le canal du nouveau centre de contact : <a onclick=\"top.BX.SidePanel.Instance.open('#CONNECTOR_URL#', {width: 700})\" style=\"cursor: pointer\">Instagram professionnel</a>. 

Nous vous conseillons vivement de reconnecter tout de suite parce que le connecteur hérité sera bientôt désactivé.

<a onclick=\"top.BX.Helper.show('redirect=detail&code=4779313');\" style=\"cursor: pointer\">Plus d'informations</a>";
?>