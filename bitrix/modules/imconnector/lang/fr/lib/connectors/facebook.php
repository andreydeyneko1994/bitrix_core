<?php
$MESS["IMCONNECTOR_FACEBOOK_ADDITIONAL_DATA"] = "Détails supplémentaires fournis :";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "Le message n'a pas été remis car la fenêtre de 7 jours d'attente de la réponse du client a expiré. #A_START#Détails#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "Le message n'a pas été remis car la fenêtre de 24 heures d'attente de la réponse du client a expiré. Vous pouvez étendre la fenêtre de messagerie à 7 jours. Vous devrez alors renvoyer ce message. #A_START#Détails#A_END#";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "Le message n'a pas été remis car le client n'a pas répondu dans le délai imparti. #A_START#Détails#A_END#";
