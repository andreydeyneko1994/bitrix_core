<?php
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "Le message n'a pas été remis car la fenêtre de 7 jours d'attente de la réponse du client a expiré. #A_START#Détails#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "Le message n'a pas été remis car la fenêtre de 24 heures d'attente de la réponse du client a expiré. Vous pouvez étendre la fenêtre de messagerie à 7 jours. Vous devrez alors renvoyer ce message. #A_START#Détails#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "Le message n'a pas été remis car le client n'a pas répondu dans le délai imparti. #A_START#Détails#A_END#";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_FOR_COMMENT"] = "Impossible de répondre à un commentaire Instagram Direct car le commentaire initial a été supprimé ou la diffusion parente a pris fin.";
$MESS["IMCONNECTOR_MESSAGE_LINK_COMMENT_MEDIA_INSTAGRAM"] = "L'utilisateur a laissé un commentaire sur la publication Instagram. [URL=#URL#]Afficher le message[/URL]";
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "Le client vous a envoyé un message. Cependant, ce type de message n'est temporairement pas pris en charge. Veuillez utiliser votre application Instagram pour lire le message.";
