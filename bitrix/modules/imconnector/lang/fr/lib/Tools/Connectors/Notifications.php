<?php
$MESS["IMCONNECTOR_NOTIFICATIONS_TOS_AGREEMENT_NAME"] = "Conditions d'utilisation du centre de notification Bitrix24";
$MESS["IMCONNECTOR_NOTIFICATIONS_VIRTUAL_WHATSAPP_DEFAULT_MESSAGE"] = "Veuillez envoyer ce message pour entrer en contact avec l'agent commercial #PORTAL_CODE#. Nous vous remercions !";
