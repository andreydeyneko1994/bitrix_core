<?php
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Installation du module";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Connectez des messageries externes en utilisant ce module.";
$MESS["IMCONNECTOR_MODULE_NAME"] = "Connecteurs de messagerie externes";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Désinstallation du module";
