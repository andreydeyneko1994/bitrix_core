<?
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "Adresse public du site : ";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "Les connecteurs nécessite une adresse de site public pour être exécutés comme prévu.";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "Si l'accès externe à votre réseau est restreint, activez-le seulement pour certaines pages. Veuillez consulter la #LINK_START#documentation#LINK_END# pour plus de détails.";
?>