<?php
$MESS["DAV_INSTALL_DESCRIPTION"] = "Módulo de acesso a coleção e objeto";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Instalação de módulo";
$MESS["DAV_PERM_D"] = "acesso negado";
$MESS["DAV_PERM_R"] = "ler";
$MESS["DAV_PERM_W"] = "escrever";
