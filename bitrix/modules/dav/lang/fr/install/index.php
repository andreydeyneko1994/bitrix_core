<?php
$MESS["DAV_ERROR_EDITABLE"] = "Votre édition ne fournit pas ce module.";
$MESS["DAV_INSTALL_DESCRIPTION"] = "Module du support d'accès aux objets et collections";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Installation de la solution";
$MESS["DAV_PERM_D"] = "accès refusé";
$MESS["DAV_PERM_R"] = "lecture";
$MESS["DAV_PERM_W"] = "note";
