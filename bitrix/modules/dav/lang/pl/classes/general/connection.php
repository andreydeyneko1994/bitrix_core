<?
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "Typ Połączenia";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "Następujące wartości typu połączenia są możliwe: caldav, ical.";
$MESS["DAV_EXP_ENTITY_ID"] = "ID Jednostki";
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "ID jednostki musi być liczbą całkowitą.";
$MESS["DAV_EXP_ENTITY_TYPE"] = "Typ jednostki";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "Następujące wartości typu jednostki są możliwe: użytkownik, grupa.";
$MESS["DAV_EXP_NAME"] = "Nazwa";
$MESS["DAV_EXP_SERVER_HOST"] = "Adres";
$MESS["DAV_EXP_SERVER_PORT"] = "Port";
$MESS["DAV_EXP_SERVER_SCHEME"] = "Schemat";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "Następujące wartości schematu połączenia są możliwe: http, https.";
?>