<?
$MESS["DAVCGERR_INVALID_ARG"] = "Wartość '#PARAM#' jest nieprawidłowa.";
$MESS["DAVCGERR_INVALID_ARG1"] = "Wartość '#PARAM#' musi być #VALUE#.";
$MESS["DAVCGERR_INVALID_TYPE"] = "Wartość '#PARAM#' jest nieprawidłowego typu.";
$MESS["DAVCGERR_INVALID_TYPE1"] = "Wartość '#PARAM#' musi być typu '#VALUE#'.";
$MESS["DAVCGERR_NULL_ARG"] = "Wartość '#PARAM#' jest niezdefiniowana.";
?>