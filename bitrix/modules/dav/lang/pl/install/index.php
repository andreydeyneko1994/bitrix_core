<?php
$MESS["DAV_INSTALL_DESCRIPTION"] = "Obiekt I Kolekcja Modułu Dostępu";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Instalacja Modułu";
$MESS["DAV_PERM_D"] = "Odmowa dostępu";
$MESS["DAV_PERM_R"] = "Odczyt";
$MESS["DAV_PERM_W"] = "Napisz";
