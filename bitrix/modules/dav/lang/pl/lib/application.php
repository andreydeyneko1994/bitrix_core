<?
$MESS["DAV_APP_COMMENT"] = "Utworzone automatycznie";
$MESS["DAV_APP_SYSCOMMENT"] = "Synchronizacja DAV";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "Synchronizacja DAV: #TYPE#";
$MESS["DAV_APP_TYPE_caldav"] = "kalendarz";
$MESS["DAV_APP_TYPE_carddav"] = "kontakty";
$MESS["dav_app_calendar"] = "Kalendarz";
$MESS["dav_app_calendar_desc"] = "Zsynchronizuj swoje wydarzenia w kalendarzu aby zobaczyć planowane wydarzenia i aktywności. Wybierz cel synchronizacji aby skonfigurować i kliknij  \"Uzyskaj hasło\".";
$MESS["dav_app_calendar_phone"] = "Telefon Komórkowy";
$MESS["dav_app_card"] = "Pracownicy";
$MESS["dav_app_card_desc"] = "Synchronizuj swoją listę pracowników z telefonami komórkowymi kontaktów lub innymi aplikacjami.";
$MESS["dav_app_doc"] = "Dokumenty";
$MESS["dav_app_doc_desc"] = "Niektóre wersje MS Office wymagają loginu i hasła do edycji dokumentów. Użyj swojego loginu; aby uzyskać hasło kliknij \"Uzyskaj hasło\".";
$MESS["dav_app_doc_office"] = "MS Office";
?>