<?php
$MESS["DAV_ERROR_EDITABLE"] = "Su edición no proporciona este módulo.";
$MESS["DAV_INSTALL_DESCRIPTION"] = "Objeto y Colección del Módulo de Acceso";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Instalación del Módulo";
$MESS["DAV_PERM_D"] = "acceso denegado";
$MESS["DAV_PERM_R"] = "leer";
$MESS["DAV_PERM_W"] = "escribir";
