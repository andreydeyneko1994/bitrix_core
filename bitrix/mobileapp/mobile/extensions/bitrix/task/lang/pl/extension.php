<?php
$MESS["MOBILE_TASKS_LIST_TITLE"] = "Zadania";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_DAY"] = "– #TIME# dni";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_HOUR"] = "– #TIME# godz.";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_MINUTE"] = "– #TIME# min";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_MONTH"] = "– #TIME# mies.";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_WEEK"] = "– #TIME# tyg.";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_EXPIRED_YEAR"] = "– #TIME# lat";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_MORE_THAN_TWO_WEEKS"] = "Ponad dwa tygodnie";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_NEXT_WEEK"] = "Następny tydzień";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_NO_DEADLINE"] = "Brak terminu";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_THIS_WEEK"] = "Bieżący tydzień";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_TODAY"] = "Dzisiaj";
$MESS["MOBILE_TASKS_TASK_CARD_DEADLINE_STATE_TOMORROW"] = "Jutro";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_DEFERRED"] = "Odłożone";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_EXPIRED"] = "Przeterminowane";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_EXPIRED_SOON"] = "Niemal przeterminowane";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_NEW"] = "Nowe";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_SUPPOSEDLY_COMPLETED"] = "Oczekujące zatwierdzenia";
$MESS["MOBILE_TASKS_TASK_CARD_STATE_WITHOUT_DEADLINE"] = "Bez terminu";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_APPROVE"] = "Akceptacja zadania";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CANCEL"] = "Anuluj";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_DEADLINE"] = "Termin ostateczny";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_GROUP"] = "Dołącz do projektu";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_CHANGE_RESPONSIBLE"] = "Przypisz";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DELEGATE"] = "Deleguj";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DISAPPROVE"] = "Powrót do weryfikacji";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_DONT_FOLLOW"] = "Nie obserwuj";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_MORE"] = "Więcej";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_MUTE"] = "Wycisz";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PAUSE"] = "Wstrzymaj";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PIN"] = "Przypnij";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_PING"] = "Ping";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_READ"] = "Potwierdź odczyt";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_REMOVE"] = "Usuń";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_RENEW"] = "Wznów";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_START"] = "Start";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_UNMUTE"] = "Wyłącz wyciszenie";
$MESS["MOBILE_TASKS_TASK_CARD_VIEW_ACTION_UNPIN"] = "Odepnij";
$MESS["TASKS_ROLE_ACCOMPLICE"] = "Pomaga";
$MESS["TASKS_ROLE_AUDITOR"] = "Obserwuje";
$MESS["TASKS_ROLE_ORIGINATOR"] = "Ustawione przeze mnie";
$MESS["TASKS_ROLE_RESPONSIBLE"] = "Bieżące";
$MESS["TASKS_ROLE_VIEW_ALL"] = "Wszystkie zadania";
