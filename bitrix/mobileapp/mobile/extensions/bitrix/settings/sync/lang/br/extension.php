<?
$MESS["SE_SYNC_CAL_TITLE"] = "Sincronizar Calendário";
$MESS["SE_SYNC_CONTACTS_TITLE"] = "Sincronizar Contatos";
$MESS["SE_SYNC_PROFILE_DESCRIPTION"] = "A sincronização de contatos e calendário é feita adicionando perfis de serviço ao seu iOS. Para interromper a sincronização, abra a exibição de perfis e exclua o perfil de sincronização. O nome do perfil é o mesmo do seu domínio Bitrix24.";
$MESS["SE_SYNC_SUBTITLE_TITLE"] = "Calendários e Contatos";
$MESS["SE_SYNC_TITLE"] = "Sincronizar";
?>