<?
$MESS["SE_SYNC_CAL_TITLE"] = "Synchronizuj kalendarz";
$MESS["SE_SYNC_CONTACTS_TITLE"] = "Synchronizuj kontakty";
$MESS["SE_SYNC_PROFILE_DESCRIPTION"] = "Synchronizacja kontaktów i kalendarza jest zapewniona poprzez dodanie profili usług do systemu iOS. Aby zatrzymać synchronizację, otwórz widok profili i usuń profil synchronizacji. Nazwa profilu jest taka sama jak twoja domena Bitrix24.";
$MESS["SE_SYNC_SUBTITLE_TITLE"] = "Kalendarze i kontakty";
$MESS["SE_SYNC_TITLE"] = "Synchronizuj";
?>