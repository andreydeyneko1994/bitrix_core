<?
$MESS["SE_SYS_ALLOW_INVITE_USERS"] = "Tout le monde peut inviter";
$MESS["SE_SYS_ALLOW_INVITE_USERS_DESC"] = "L'activation de cette option permettra à tout employé enregistré sur Bitrix24 d'inviter de nouveaux employés.";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "Exécuter en arrière plan";
$MESS["SE_SYS_INVITE"] = "Invitations à Bitrix24";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "Augmenter l'intervalle des notifications";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "Utilisez cette option pour économiser de la batterie. Vous recevrez moins de notifications du service et de mises à jour des compteurs. Cela n'affectera pas les notifications basées sur le contenu.";
$MESS["SE_SYS_TITLE"] = "Autres paramètres";
?>