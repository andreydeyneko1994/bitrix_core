<?
$MESS["SE_SYS_ALLOW_INVITE_USERS"] = "Todos podem convidar";
$MESS["SE_SYS_ALLOW_INVITE_USERS_DESC"] = "A ativação dessa opção permitirá que qualquer funcionário cadastrado no Bitrix24 convide novos funcionários.";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "Executar em segundo plano";
$MESS["SE_SYS_INVITE"] = "Convites para Bitrix24";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "Aumentar o intervalo de notificação";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "Use esta opção para economizar bateria. Você receberá menos notificações de serviço e atualizações do contador. Isso não afetará as notificações baseadas em conteúdo.";
$MESS["SE_SYS_TITLE"] = "Outras configurações";
?>