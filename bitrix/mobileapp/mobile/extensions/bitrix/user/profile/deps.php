<?php

return [
	'extensions' => [
		"files/const",
		"files/converter",
		"files/entry",
		'tasks:task',
		'disk',
		'user/account'
	],
	'components' => [
		'user.profile',
	],
];
