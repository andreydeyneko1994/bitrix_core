<?
$MESS["ACTION_DELETE"] = "Supprimer";
$MESS["INVITE_USERS_ERROR"] = "Erreur";
$MESS["LOAD_MORE_RESULT"] = "Afficher plus";
$MESS["LOAD_MORE_USERS"] = "Charger plus";
$MESS["RECENT_SEARCH"] = "Recherche récente";
$MESS["SEARCH_EMPTY_RESULT"] = "Malheureusement, votre demande de recherche n'a donné aucun résultat";
$MESS["SEARCH_LOADING"] = "Recherche...";
$MESS["SEARCH_PLACEHOLDER"] = "Saisissez le nom ou le service";
$MESS["USER_LIST_COMPANY"] = "Employés";
$MESS["USER_LIST_CONTACTS"] = "Contacts";
$MESS["USER_LIST_NO_NAME"] = "Sans nom";
$MESS["USER_LOADING"] = "Chargement...";
?>