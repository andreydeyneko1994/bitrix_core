<?
$MESS["RECENT_SEARCH"] = "Ostatnie wyszukiwanie";
$MESS["USER_DISK_ACCESS_DENIED"] = "Nie masz uprawnień do wyświetlania plików tego użytkownika.";
$MESS["USER_DISK_CONFIRM_NO"] = "Nie";
$MESS["USER_DISK_CONFIRM_YES"] = "Tak";
$MESS["USER_DISK_EMPTY_FOLDER"] = "Folder jest pusty";
$MESS["USER_DISK_ERROR"] = "Nie można pobrać listy plików. Coś poszło źle.";
$MESS["USER_DISK_FILE_NOT_SEND"] = "Błąd podczas wysyłania pliku";
$MESS["USER_DISK_FILE_SENT"] = "Plik został wysłany do użytkownika";
$MESS["USER_DISK_GET_PUBLIC_LINK"] = "Link publiczny";
$MESS["USER_DISK_LINK_COPIED"] = "Link publiczny został skopiowany do schowka";
$MESS["USER_DISK_LINK_COPIED_FAIL"] = "Niestety, nie byliśmy w stanie utworzyć linku publicznego. Spróbuj ponownie później.";
$MESS["USER_DISK_MENU_SORT"] = "Sortuj";
$MESS["USER_DISK_MENU_SORT_DATE_CREATE"] = "Według daty modyfikacji";
$MESS["USER_DISK_MENU_SORT_DATE_UPDATE"] = "Według daty utworzenia";
$MESS["USER_DISK_MENU_SORT_MIX"] = "Sortowanie mieszane";
$MESS["USER_DISK_MENU_SORT_NAME"] = "Według nazwy";
$MESS["USER_DISK_MENU_SORT_TYPE"] = "Według typu";
$MESS["USER_DISK_MENU_UPLOAD"] = "Prześlij plik...";
$MESS["USER_DISK_OPEN"] = "Otwórz";
$MESS["USER_DISK_PUBLIC_LINK_GETTING"] = "Tworzenie linku publicznego...";
$MESS["USER_DISK_REMOVE"] = "Usuń";
$MESS["USER_DISK_REMOVE_FILE_CONFIRM"] = "Czy chcesz przenieść plik „%@” do kosza?";
$MESS["USER_DISK_REMOVE_FOLDER_CONFIRM"] = "Czy chcesz przenieść folder „%@” do kosza?";
$MESS["USER_DISK_ROLLBACK"] = "Czy przywrócić usunięty element „%@”?";
$MESS["USER_DISK_SEND_TO_USER"] = "Udostępnij użytkownikowi";
$MESS["USER_DISK_SHARE"] = "Udostępnij";
?>