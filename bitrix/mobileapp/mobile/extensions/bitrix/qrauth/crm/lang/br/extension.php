<?php
$MESS["CRM_DESKTOP_OPEN"] = "Abra o CRM no seu navegador em um computador desktop. Depois de concluir a configuração inicial, você pode voltar aqui para continuar usando o CRM.";
$MESS["CRM_TITLE"] = "Comece a usar o CRM fornecendo configuração inicial e canais de conexão.";
$MESS["QR_EXTERNAL_AUTH"] = "Fazer login com código QR";
