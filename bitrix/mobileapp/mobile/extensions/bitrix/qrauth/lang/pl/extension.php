<?php
$MESS["ACCEPT_QR_AUTH"] = "Tak, kontynuuj";
$MESS["AUTH_WAIT"] = "Logowanie za...";
$MESS["DECLINE_QR_AUTH"] = "Nie";
$MESS["GET_MORE"] = "Uzyskaj jeszcze więcej narzędzi biznesowych w pełnej wersji Bitrix24.";
$MESS["OPEN_BROWSER"] = "Otwórz [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] na komputerze";
$MESS["QR_HOW_TO_AUTH"] = "Jak otworzyć pełną wersję? ";
$MESS["QR_SCANNER_HINT"] = "Ta funkcja jest dostępna tylko w pełnej wersji";
$MESS["QR_WARNING"] = "Zamierzasz zalogować się do [B]#DOMAIN#[/B] w Bitrix24 za pomocą kodu QR. Kontynuować?";
$MESS["SCAN_QR"] = "Zeskanuj [B]kod QR[/B] za pomocą aparatu smartfona";
$MESS["SCAN_QR_BUTTON"] = "Zeskanuj kod QR";
$MESS["STEP_CAMERA_TITLE"] = "Zeskanuj kod QR za pomocą aparatu smartfona";
$MESS["STEP_OPEN_SITE"] = "Otwórz [B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B] w przeglądarce";
$MESS["STEP_PRESS_CLOUD"] = "Kliknij [B]Zaloguj się[/B] i [IMG WIDTH=\"22\" HEIGHT=\"22\"]#URL#[/IMG][B]Kod QR[/B]";
$MESS["STEP_PRESS_SELF_HOSTED"] = "Kliknij [B]Zaloguj się za pomocą kodu QR[/B]";
$MESS["STEP_SCAN"] = "Zeskanuj kod QR";
$MESS["WRONG_QR"] = "Nieprawidłowy kod QR";
