<?php
$MESS["ACCEPT_QR_AUTH"] = "Sim, prosseguir";
$MESS["AUTH_WAIT"] = "Fazendo login em...";
$MESS["DECLINE_QR_AUTH"] = "Não";
$MESS["GET_MORE"] = "Obtenha ainda mais ferramentas de negócios na versão completa do Bitrix24.";
$MESS["OPEN_BROWSER"] = "Abra [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] no seu computador";
$MESS["QR_HOW_TO_AUTH"] = "Como abro a versão completa? ";
$MESS["QR_SCANNER_HINT"] = "Este recurso está disponível apenas na versão completa";
$MESS["QR_WARNING"] = "Você vai fazer login no Bitrix24 de [B]#DOMAIN#[/B] usando o código QR. Continuar?";
$MESS["SCAN_QR"] = "Faça a leitura do [B]código QR[/B] com a câmera do seu celular";
$MESS["SCAN_QR_BUTTON"] = "Ler código QR";
$MESS["STEP_CAMERA_TITLE"] = "Faça a leitura do código QR com a câmera do seu celular";
$MESS["STEP_OPEN_SITE"] = "Abra [B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B] no seu navegador";
$MESS["STEP_PRESS_CLOUD"] = "Clique em [B]Login[/B] e [IMG WIDTH=\"22\" HEIGHT=\"22\"]#URL#[/IMG][B]Código QR[/B]";
$MESS["STEP_PRESS_SELF_HOSTED"] = "Clique em [B]Login com Código QR[/B]";
$MESS["STEP_SCAN"] = "Leia o código QR";
$MESS["WRONG_QR"] = "O código QR está incorreto";
