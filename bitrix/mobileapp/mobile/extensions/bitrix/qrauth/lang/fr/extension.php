<?php
$MESS["ACCEPT_QR_AUTH"] = "Oui, continuer";
$MESS["AUTH_WAIT"] = "Connexion...";
$MESS["DECLINE_QR_AUTH"] = "Non";
$MESS["GET_MORE"] = "Obtenez encore plus d'outils commerciaux dans la version complète de Bitrix24.";
$MESS["OPEN_BROWSER"] = "Ouvrez [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] sur votre ordinateur";
$MESS["QR_HOW_TO_AUTH"] = "Comment ouvrir la version complète ? ";
$MESS["QR_SCANNER_HINT"] = "Cette fonctionnalité est disponible uniquement dans la version complète";
$MESS["QR_WARNING"] = "Vous êtes sur le point de vous connecter au Bitrix24 de [B]#DOMAIN#[/B] à l'aide du code QR. Continuer ?";
$MESS["SCAN_QR"] = "Scannez le [B]code QR[/B] avec l'appareil photo de votre téléphone portable";
$MESS["SCAN_QR_BUTTON"] = "Scanner le code QR";
$MESS["STEP_CAMERA_TITLE"] = "Scannez le code QR avec l'appareil photo de votre téléphone portable";
$MESS["STEP_OPEN_SITE"] = "Ouvrez [B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B] dans votre navigateur";
$MESS["STEP_PRESS_CLOUD"] = "Cliquez sur [B]Connexion[/B] et [IMG WIDTH=\"22\" HEIGHT=\"22\"]#URL#[/IMG][B]code QR[/B]";
$MESS["STEP_PRESS_SELF_HOSTED"] = "Cliquez sur [B]Connexion avec le code QR[/B]";
$MESS["STEP_SCAN"] = "Scannez le code QR";
$MESS["WRONG_QR"] = "Le code QR est incorrect";
