<?php
$MESS["MOBILE_PROJECT_LIST_EMPTY"] = "No se encontraron grupos";
$MESS["MOBILE_PROJECT_LIST_ERROR"] = "No se puede obtener la lista de grupos";
$MESS["MOBILE_PROJECT_LIST_TAB_NEWS"] = "Noticias";
$MESS["MOBILE_PROJECT_LIST_TAB_TASKS"] = "Tareas";
