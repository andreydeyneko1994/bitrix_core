<?
$MESS["SEARCH"] = "Recherche...";
$MESS["SEARCH_BACK"] = "Retour à la liste";
$MESS["SEARCH_CATEGORY_CHAT"] = "CHATS";
$MESS["SEARCH_CATEGORY_DEPARTMENT_USER"] = "Employés";
$MESS["SEARCH_CATEGORY_LINE"] = "CANAUX OUVERTS";
$MESS["SEARCH_CATEGORY_USER"] = "EMPLOYÉS";
$MESS["SEARCH_EMPTY"] = "Aucun résultat pour \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Charger plus...";
$MESS["SEARCH_MORE_READY"] = "Affichage des éléments trouvés. Charger plus...";
?>