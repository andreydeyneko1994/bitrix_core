<?
$MESS["SEARCH"] = "Wyszukaj...";
$MESS["SEARCH_BACK"] = "Powrót do listy";
$MESS["SEARCH_CATEGORY_CHAT"] = "CZATY";
$MESS["SEARCH_CATEGORY_DEPARTMENT_USER"] = "Pracownicy";
$MESS["SEARCH_CATEGORY_LINE"] = "OTWARTE KANAŁY";
$MESS["SEARCH_CATEGORY_USER"] = "PRACOWNICY";
$MESS["SEARCH_EMPTY"] = "Brak wyników dla „#TEXT#”";
$MESS["SEARCH_MORE"] = "Załaduj więcej...";
$MESS["SEARCH_MORE_READY"] = "Pokazywanie znalezionych elementów. Załaduj więcej...";
?>