<?
$MESS["SEARCH"] = "Pesquisar...";
$MESS["SEARCH_CHATS"] = "BATE-PAPOS";
$MESS["SEARCH_EMPLOYEES"] = "FUNCIONÁRIOS";
$MESS["SEARCH_EMPTY"] = "Nenhum resultado para \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Carregar mais...";
$MESS["SEARCH_MORE_READY"] = "Mostrando itens encontrados. Carregar mais...";
?>