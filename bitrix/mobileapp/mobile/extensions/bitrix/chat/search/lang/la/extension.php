<?
$MESS["SEARCH"] = "Buscar...";
$MESS["SEARCH_CHATS"] = "CHATS";
$MESS["SEARCH_EMPLOYEES"] = "EMPLEADOS";
$MESS["SEARCH_EMPTY"] = "No hay resultados para \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Cargar más...";
$MESS["SEARCH_MORE_READY"] = "Mostrando elementos encontrados. Carga más...";
?>