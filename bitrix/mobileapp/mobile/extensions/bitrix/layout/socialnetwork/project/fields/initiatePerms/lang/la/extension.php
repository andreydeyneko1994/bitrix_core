<?php
$MESS["MOBILE_LAYOUT_PROJECT_FIELDS_INITIATE_PERMS_HEADERS_TITLE"] = "Propietario y moderadores del proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_FIELDS_INITIATE_PERMS_MEMBERS_TITLE"] = "Todos los participantes del proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_FIELDS_INITIATE_PERMS_MENU_TITLE"] = "Permisos para invitar al proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_FIELDS_INITIATE_PERMS_OWNER_TITLE"] = "Solamente propietario del proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_FIELDS_INITIATE_PERMS_TITLE"] = "PERMISOS PARA INVITAR AL PROYECTO";
