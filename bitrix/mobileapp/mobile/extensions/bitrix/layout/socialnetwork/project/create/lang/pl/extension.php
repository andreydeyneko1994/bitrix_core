<?php
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_BUTTON"] = "Ustawienia zaawansowane";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS"] = "Podane informacje: #FIELDS#";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_DATE_FINISH"] = "Data końcowa";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_DATE_START"] = "Data początkowa";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_DESCRIPTION"] = "opis";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_INITIATE_PERMS"] = "Użytkownicy mogący wysyłać zaproszenia do projektów";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_MODERATORS"] = "Asystenci";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_OWNER"] = "Właściciel";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_SUBJECT"] = "Temat projektu";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ADVANCED_SETTINGS_FILLED_FIELDS_TAGS"] = "Tagi";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ERROR_AVATAR_IS_UPLOADING"] = "Trwa przesyłanie ikony projektu";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_ERROR_NO_TITLE"] = "Nie określono nazwy projektu";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_HEADER_BUTTON_CREATE"] = "Utwórz";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_HEADER_BUTTON_NEXT"] = "Następny";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_HEADER_TITLE_ADD_MEMBERS"] = "Dodaj uczestników";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_HEADER_TITLE_CREATE"] = "Utwórz projekt";
$MESS["MOBILE_LAYOUT_PROJECT_CREATE_HEADER_TITLE_CREATING"] = "Tworzenie projektu";
