<?php
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_CANCEL"] = "Cancelar";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_ERROR_AVATAR_IS_UPLOADING"] = "Aún se está cargando el ícono del proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_ERROR_NO_TITLE"] = "El nombre del proyecto no está especificado";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_HEADER_TITLE"] = "Editar proyecto";
$MESS["MOBILE_LAYOUT_PROJECT_EDIT_SAVE"] = "Guardar";
