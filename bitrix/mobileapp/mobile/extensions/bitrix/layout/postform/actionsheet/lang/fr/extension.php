<?php
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_ATTACH"] = "Joindre des fichiers";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_ATTACH_COUNTER"] = "Pièces jointes : #NUM#";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_BACKGROUND"] = "Modifier le fond";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_GRATITUDE"] = "Appréciation";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_IMPORTANT"] = "Annonce";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_MENTION"] = "Mentionner un employé, groupe de travail ou service";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_TITLE_HIDE"] = "Masquer le titre";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_TITLE_SHOW"] = "Ajouter un titre";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_VOTE"] = "Sondage";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_SELECTOR_ALL"] = "Pour tous";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_SELECTOR_TITLE"] = "Visible";
