<?php
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_ATTACH"] = "Adjuntar archivos";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_ATTACH_COUNTER"] = "Archivos adjuntos: #NUM#";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_BACKGROUND"] = "Cambiar el fondo";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_GRATITUDE"] = "Apreciación";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_IMPORTANT"] = "Anuncio";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_MENTION"] = "Mencionar empleado, grupo de trabajo o departamento";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_TITLE_HIDE"] = "Ocultar título";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_TITLE_SHOW"] = "Agregar título";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_ITEM_VOTE"] = "Encuesta";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_SELECTOR_ALL"] = "A todo";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_ACTIONSHEET_SELECTOR_TITLE"] = "Visible";
