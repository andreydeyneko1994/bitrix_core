<?php
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_ANSWER_MENU_CANCEL"] = "Cancelar";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_ANSWER_MENU_DELETE"] = "Eliminar";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_ANSWER_MENU_DOWN"] = "Mover abajo";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_ANSWER_MENU_UP"] = "Mover arriba";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_MENU_CANCEL"] = "Cancelar";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_MENU_DELETE"] = "Eliminar la pregunta";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_MENU_MULTIPLE_N"] = "Permitir solo una opción";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_MENU_MULTIPLE_Y"] = "Permitir opción múltiple";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_PLACEHOLDER_ANSWER"] = "Responder";
$MESS["MOBILE_EXT_LAYOUT_POSTFORM_VOTEPANEL_PLACEHOLDER_QUESTION"] = "Pregunta";
