<?php
$MESS["FIELDS_FILE_ADD_FILE"] = "Ajouter un fichier";
$MESS["FIELDS_FILE_ADD_FILES"] = "Ajouter les fichiers";
$MESS["FIELDS_FILE_ADD_IMAGE"] = "Ajouter une image";
$MESS["FIELDS_FILE_ADD_VIDEO"] = "Ajouter une vidéo";
$MESS["FIELDS_FILE_ATTACHMENTS_DIALOG_TITLE"] = "Fichiers joints : #NUM#";
$MESS["FIELDS_FILE_B24_DISK"] = "Bitrix24.Drive";
$MESS["FIELDS_FILE_CAMERA"] = "Prendre une photo";
$MESS["FIELDS_FILE_MEDIATEKA"] = "Sélectionner dans la galerie";
$MESS["FIELDS_FILE_MEDIA_TYPE_ALERT_TEXT"] = "Erreur lors du téléchargement du fichier";
$MESS["FIELDS_FILE_OPEN_GALLERY"] = "Afficher";
