<?php
$MESS["FIELDS_SELECTOR_CONTROL_SELECT"] = "Selecionar";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_DEFAULT_PLURAL_0"] = "#COUNT# item";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_DEFAULT_PLURAL_1"] = "#COUNT# itens";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_DEFAULT_PLURAL_2"] = "#COUNT# itens";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_MODERATORS_PLURAL_0"] = "#COUNT# assistente";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_MODERATORS_PLURAL_1"] = "#COUNT# assistentes";
$MESS["FIELDS_SELECTOR_GROUPING_TYPE_MODERATORS_PLURAL_2"] = "#COUNT# assistentes";
