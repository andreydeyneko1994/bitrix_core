<?php
$MESS["SIMPLELIST_LIST_EMPTY"] = "La lista está vacía.";
$MESS["SIMPLELIST_PULL_NOTIFICATION_ADD"] = "Nuevos elementos: %COUNT%";
$MESS["SIMPLELIST_PULL_NOTIFICATION_UPDATE"] = "Actualizar la lista...";
$MESS["SIMPLELIST_SEARCH_EMPTY"] = "No se encontraron entradas.";
