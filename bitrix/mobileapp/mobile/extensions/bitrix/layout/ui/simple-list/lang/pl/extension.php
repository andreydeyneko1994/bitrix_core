<?php
$MESS["SIMPLELIST_LIST_EMPTY"] = "Lista jest pusta.";
$MESS["SIMPLELIST_PULL_NOTIFICATION_ADD"] = "Nowe pozycje: %COUNT%";
$MESS["SIMPLELIST_PULL_NOTIFICATION_UPDATE"] = "Odśwież listę...";
$MESS["SIMPLELIST_SEARCH_EMPTY"] = "Nie znaleziono wpisów.";
