<?php
$MESS["SELECTOR_COMPONENT_CREATE_SECTION"] = "Créer une section";
$MESS["SELECTOR_COMPONENT_CREATING_SECTION"] = "Création d'une section...";
$MESS["SELECTOR_COMPONENT_PICK_SECTION_2"] = "Section";
$MESS["SELECTOR_COMPONENT_START_TYPING_TO_SEARCH_SECTION"] = "Commencez à saisir pour trouver une section";
