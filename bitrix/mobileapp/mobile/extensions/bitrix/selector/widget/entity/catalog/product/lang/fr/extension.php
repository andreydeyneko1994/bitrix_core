<?php
$MESS["SELECTOR_COMPONENT_CREATE_PRODUCT"] = "Créer un produit";
$MESS["SELECTOR_COMPONENT_CREATING_PRODUCT"] = "Création d'un produit...";
$MESS["SELECTOR_COMPONENT_PICK_PRODUCT_2"] = "Marchandise";
$MESS["SELECTOR_COMPONENT_START_TYPING_TO_SEARCH_PRODUCT"] = "Commencez à saisir pour trouver un produit";
