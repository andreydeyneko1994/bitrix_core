<?php
$MESS["SELECTOR_COMPONENT_CREATE_STORE"] = "Créer un entrepôt";
$MESS["SELECTOR_COMPONENT_CREATING_STORE"] = "Création d'un entrepôt...";
$MESS["SELECTOR_COMPONENT_PICK_STORE_2"] = "Entrepôt";
$MESS["SELECTOR_COMPONENT_START_TYPING_TO_SEARCH_STORE"] = "Commencez à saisir pour trouver un entrepôt";
