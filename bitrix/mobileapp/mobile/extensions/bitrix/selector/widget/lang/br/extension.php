<?php
$MESS["PROVIDER_SEARCH_CREATE_PLACEHOLDER"] = "Encontrar ou criar";
$MESS["PROVIDER_WIDGET_CREATE_ITEM"] = "Criar item";
$MESS["PROVIDER_WIDGET_CREATING_ITEM"] = "Criando item...";
$MESS["PROVIDER_WIDGET_DONE"] = "Concluído";
$MESS["PROVIDER_WIDGET_START_TYPING_TO_SEARCH"] = "Comece a digitar para encontrar itens";
