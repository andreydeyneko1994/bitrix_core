<?php
$MESS["CSPL_PRICE_EMPTY"] = "Nie określono";
$MESS["CSPL_PURCHASE_PRICE"] = "Cena zakupu";
$MESS["CSPL_SELLING_PRICE"] = "Cena sprzedaży";
$MESS["CSPL_STORE_AMOUNT_INCREMENT"] = "Kwit składowy";
$MESS["CSPL_STORE_EMPTY"] = "Nie wybrano magazynu";
