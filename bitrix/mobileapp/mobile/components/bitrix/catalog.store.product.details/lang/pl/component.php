<?php
$MESS["CSPD_ADD_SKU_OR_SERIAL_NUMBER"] = "Dodaj SKU lub S/N";
$MESS["CSPD_CLOSE"] = "Zamknij";
$MESS["CSPD_DONE"] = "Gotowe";
$MESS["CSPD_FIELDS_BARCODE"] = "Kod kreskowy";
$MESS["CSPD_FIELDS_MEASURES"] = "Jednostka miary";
$MESS["CSPD_FIELDS_PHOTOS"] = "Zdjęcia produktu";
$MESS["CSPD_FIELDS_PRODUCT_NAME"] = "Nazwa produktu";
$MESS["CSPD_FIELDS_PRODUCT_SECTIONS"] = "Powiąż z sekcją";
$MESS["CSPD_FIELDS_PURCHASING_PRICE"] = "Cena zakupu";
$MESS["CSPD_FIELDS_SELLING_PRICE"] = "Cena sprzedaży";
$MESS["CSPD_FIELDS_STORE"] = "Magazyn";
$MESS["CSPD_FIELDS_STORE_TO_AMOUNT"] = "Dostarczona ilość";
$MESS["CSPD_MORE_OPPORTUNITIES"] = "Opcje dodatkowe";
$MESS["CSPD_OPEN_PRODUCT_IN_DESKTOP_VERSION"] = "Otwórz produkt w pełnej wersji";
