<?php
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST"] = "Lista";
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST_SUBTEXT"] = "(todas as etapas)";
$MESS["CRM_STAGE_LIST_CHANGE_TUNNEL"] = "Alterar funil";
$MESS["CRM_STAGE_LIST_CREATE_STAGE"] = "Crie uma etapa";
$MESS["CRM_STAGE_LIST_SELECT_STAGE"] = "Clique para selecionar uma etapa";
$MESS["CRM_STAGE_LIST_TITLE"] = "Vendas";
$MESS["CRM_STAGE_LIST_TUNNEL"] = "Túnel";
