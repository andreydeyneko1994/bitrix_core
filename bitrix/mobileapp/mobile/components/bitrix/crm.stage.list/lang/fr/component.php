<?php
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST"] = "Liste";
$MESS["CRM_STAGE_LIST_ALL_STAGES_LIST_SUBTEXT"] = "(toutes les étapes)";
$MESS["CRM_STAGE_LIST_CHANGE_TUNNEL"] = "Changer l'entonnoir";
$MESS["CRM_STAGE_LIST_CREATE_STAGE"] = "Créer une étape";
$MESS["CRM_STAGE_LIST_SELECT_STAGE"] = "Cliquez pour sélectionner une étape";
$MESS["CRM_STAGE_LIST_TITLE"] = "Ventes";
$MESS["CRM_STAGE_LIST_TUNNEL"] = "Funnel";
