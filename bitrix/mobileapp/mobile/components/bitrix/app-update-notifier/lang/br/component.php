<?php
$MESS["APP_UPDATE_NOTIFIER_CLOSE"] = "Fechar";
$MESS["APP_UPDATE_NOTIFIER_NEED_UPDATE"] = "Seu aplicativo Bitrix24 tem que ser atualizado para usar esta seção.";
$MESS["APP_UPDATE_NOTIFIER_OPEN_APP_STORE"] = "Abrir App Store";
$MESS["APP_UPDATE_NOTIFIER_OPEN_PLAY_MARKET"] = "Abrir Play Market";
