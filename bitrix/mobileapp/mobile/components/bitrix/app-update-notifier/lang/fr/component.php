<?php
$MESS["APP_UPDATE_NOTIFIER_CLOSE"] = "Fermer";
$MESS["APP_UPDATE_NOTIFIER_NEED_UPDATE"] = "Votre application Bitrix24 doit être mise à jour pour utiliser cette section.";
$MESS["APP_UPDATE_NOTIFIER_OPEN_APP_STORE"] = "Ouvrir l'App Store";
$MESS["APP_UPDATE_NOTIFIER_OPEN_PLAY_MARKET"] = "Ouvrir Play Market";
