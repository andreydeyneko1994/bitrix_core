<?
$MESS["IM_USER_API_ERROR"] = "Erreur lors de l'exécution de la requête. Veuillez réessayer plus tard.";
$MESS["IM_USER_CONNECTION_ERROR"] = "Erreur de connexion à Bitrix24. Veuillez vérifier la connexion au réseau.";
$MESS["IM_USER_LIST_EMPTY"] = "- La liste d'utilisateurs est vide -";
$MESS["IM_USER_LIST_KICK"] = "Supprimer";
$MESS["IM_USER_LIST_OWNER"] = "Définir comme propriétaire";
?>