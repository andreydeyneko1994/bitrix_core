<?php

return [
	'extensions' => [
		'utils',
		'recipient',
		'helpers/component',
		'tasks:task',
		'menu/spotlight',
		'stress/more',
		'qrauth/utils',
		'intent'
	],
	'components' => ['users', 'tab.settings']
];
