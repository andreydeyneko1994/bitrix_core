<?php
$MESS["MB_BP_MAIN_STRESS_LEVEL"] = "Nível de estresse";
$MESS["MEASURE_STRESS"] = "Medir";
$MESS["MENU_EDIT_PROFILE"] = "Editar perfil";
$MESS["MENU_SETTINGS_INFO"] = "Olá! O formulário \"Configurações\" foi transferido para um menu pop-up. 
Você deseja que mostremos como acessá-lo?";
$MESS["MENU_SETTINGS_INFO_YES"] = "Sim, mostre-me por favor";
$MESS["MENU_VIEW_PROFILE"] = "Visualizar Perfil";
$MESS["WELLTORY_SPOTLIGHT"] = "Meça seu nível de estresse";
