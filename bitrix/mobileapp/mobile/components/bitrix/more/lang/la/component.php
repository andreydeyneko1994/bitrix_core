<?php
$MESS["MB_BP_MAIN_STRESS_LEVEL"] = "Nivel de estrés";
$MESS["MEASURE_STRESS"] = "Medir";
$MESS["MENU_EDIT_PROFILE"] = "Editar perfil";
$MESS["MENU_SETTINGS_INFO"] = "¡Hola! El formulario de \"Configuración\" se movió a un menú emergente. 
¿Deseas que te mostremos cómo acceder a él?";
$MESS["MENU_SETTINGS_INFO_YES"] = "Sí, muéstrenme por favor";
$MESS["MENU_VIEW_PROFILE"] = "Ver perfil";
$MESS["WELLTORY_SPOTLIGHT"] = "Mida su nivel de estrés";
