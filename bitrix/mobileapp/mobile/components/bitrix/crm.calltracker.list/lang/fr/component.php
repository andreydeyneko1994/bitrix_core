<?php
$MESS["CRM_CALL_TRACKER_ADD_TO_IGNORED_NOTIFICATION"] = "Numéro ajouté aux exceptions";
$MESS["CRM_CALL_TRACKER_ADD_TO_MENU_BUTTON"] = "Ajouter";
$MESS["CRM_CALL_TRACKER_CONTEXT_MENU_TITLE"] = "Actions";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_DETAILS"] = "Détails";
$MESS["CRM_CALL_TRACKER_INFO_BUTTON_ENABLE"] = "Activer";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Enregistrer pour plus tard";
$MESS["CRM_CALL_TRACKER_SUBTITLE_CONGRATULATIONS"] = "Vous recevez trop d'appels ? Ajoutez le suivi des appels au menu pour un accès plus rapide.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_EMPTY_LIST"] = "Cette vue affichera les appels dès que vous en recevrez un.";
$MESS["CRM_CALL_TRACKER_SUBTITLE_IS_NOT_SIMPLE_CRM"] = "Vous pouvez changer le mode CRM en vous connectant à votre Bitrix24 sur votre ordinateur.";
$MESS["CRM_CALL_TRACKER_TITLE_CONGRATULATIONS"] = "Félicitations ! Le suivi des appels a été activé";
$MESS["CRM_CALL_TRACKER_TITLE_EMPTY_LIST"] = "Tous les appels ont été traités !";
$MESS["CRM_CALL_TRACKER_TITLE_IS_NOT_SIMPLE_CRM"] = "Activer le mode CRM simple pour utiliser le suivi des appels";
$MESS["CRM_CALL_TRACKER_TITLE_LICENSE_RESTRICTIONS"] = "Le suivi des appels est réservé aux offres commerciales";
$MESS["CRM_CALL_TRACKER_TITLE_WELCOME"] = "Copiez les appels des clients du téléphone vers les transactions CRM en un clic";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Ajouter aux exceptions";
$MESS["CRM_CALL_TRACKER_UPDATED_NOTIFICATION"] = "La transaction a été enregistrée";
