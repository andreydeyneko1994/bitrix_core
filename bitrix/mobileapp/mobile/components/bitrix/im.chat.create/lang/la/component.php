<?
$MESS["IM_CHAT_TYPE_CHAT_NEW"] = "Chat privado";
$MESS["IM_CHAT_TYPE_OPEN_NEW"] = "Chat público";
$MESS["IM_CREATE_API_ERROR"] = "Error al crear el chat. Por favor, inténtelo de nuevo más tarde.";
$MESS["IM_CREATE_CONNECTION_ERROR"] = "Error al conectarse a Bitrix24. Verifique la conexión de red.";
$MESS["IM_DEPARTMENT_START"] = "Ingrese el nombre del departamento para comenzar la búsqueda.";
?>