<?
$MESS["IM_CHAT_TYPE_CHAT_NEW"] = "Chat privé";
$MESS["IM_CHAT_TYPE_OPEN_NEW"] = "Chat public";
$MESS["IM_CREATE_API_ERROR"] = "Erreur lors de la création du chat. Veuillez réessayer plus tard.";
$MESS["IM_CREATE_CONNECTION_ERROR"] = "Erreur de connexion à Bitrix24. Veuillez vérifier la connexion au réseau.";
$MESS["IM_DEPARTMENT_START"] = "Saisissez le nom du service pour démarrer la recherche.";
?>