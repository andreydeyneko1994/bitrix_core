<?
$MESS["ACTION_DELETE"] = "Usuń";
$MESS["INVITE_USERS"] = "Zaproś użytkowników";
$MESS["INVITE_USERS_ERROR"] = "Błąd";
$MESS["LOAD_MORE_RESULT"] = "Pokaż więcej";
$MESS["LOAD_MORE_USERS"] = "Załaduj więcej";
$MESS["RECENT_SEARCH"] = "Ostatnie wyszukiwanie";
$MESS["SEARCH_EMPTY_RESULT"] = "Niestety, to żądanie wyszukiwania nie zwróciło żadnych wyników";
$MESS["SEARCH_LOADING"] = "Wyszukaj...";
$MESS["SEARCH_PLACEHOLDER"] = "Wprowadź nazwę lub dział";
$MESS["USER_LOADING"] = "Ładowanie...";
?>