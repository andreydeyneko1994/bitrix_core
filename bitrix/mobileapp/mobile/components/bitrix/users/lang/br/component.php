<?
$MESS["ACTION_DELETE"] = "Excluir";
$MESS["INVITE_USERS"] = "Convidar usuários";
$MESS["INVITE_USERS_ERROR"] = "Erro";
$MESS["LOAD_MORE_RESULT"] = "Mostrar mais";
$MESS["LOAD_MORE_USERS"] = "Carregar mais";
$MESS["RECENT_SEARCH"] = "Pesquisa recente";
$MESS["SEARCH_EMPTY_RESULT"] = "Infelizmente, sua solicitação de pesquisa não deu resultados";
$MESS["SEARCH_LOADING"] = "Pesquisar...";
$MESS["SEARCH_PLACEHOLDER"] = "Insira o nome ou departamento";
$MESS["USER_LOADING"] = "Carregando...";
?>