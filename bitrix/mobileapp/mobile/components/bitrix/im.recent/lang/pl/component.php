<?php
$MESS["IM_B24DISK"] = "Bitrix24.Drive";
$MESS["IM_CAMERA_ROLL"] = "Zrób zdjęcie";
$MESS["IM_CHOOSE_FILE_TITLE"] = "Pliki";
$MESS["IM_CHOOSE_PHOTO"] = "Wybierz z galerii";
$MESS["IM_DIALOG_UNNAMED"] = "Ładowanie...";
$MESS["IM_EMPTY_BUTTON"] = "Rozpocznij rozmowę";
$MESS["IM_EMPTY_OL_TEXT_1"] = "Szukasz rozmów z klientami? Wszystkie są tu.";
$MESS["IM_EMPTY_OL_TEXT_2"] = "Otrzymasz powiadomienie, gdy tylko klient opublikuje wiadomość na Otwartym kanale.";
$MESS["IM_EMPTY_TEXT_1"] = "Rozpocznij rozmowę teraz!";
$MESS["IM_EMPTY_TEXT_CREATE"] = "Kliknij przycisk poniżej, aby utworzyć czat lub wybrać pracownika.";
$MESS["IM_EMPTY_TEXT_INVITE"] = "Aby zaprosić współpracowników do Bitrix24, kliknij przycisk poniżej!";
$MESS["IM_LIST_ACTION_ERROR"] = "Nie mogliśmy połączyć się z Twoim Bitrix24, prawdopodobnie z powodu słabego połączenia z Internetem. Spróbuj ponownie później.";
$MESS["IM_LIST_EMPTY"] = "Brak bieżących rozmów";
$MESS["IM_LIST_LOADING"] = "Ładowanie…";
$MESS["IM_LIST_NOTIFICATIONS"] = "Powiadomienia";
$MESS["IM_M_MESSAGE_SEND"] = "Wyślij";
$MESS["IM_M_TEXTAREA"] = "Utwórz wiadomość...";
$MESS["IM_PROMO_VIDEO_01042020_MOBILE"] = "Połączenia wideo zostały znacznie ulepszone!#BR##BR#Wypróbuj je teraz!#BR##BR# Używaj połączeń głosowych i wideo, aby oszczędzać czas.";
$MESS["IM_READ_ALL"] = "Czytaj wszystko";
$MESS["IM_REFRESH_ERROR"] = "Nie można połączyć się z Twoim Bitrix24. Być może na Twoim serwerze występują problemy z łącznością z Internetem. Spróbujemy ponownie za kilka sekund.";
$MESS["IM_REFRESH_TITLE"] = "Aktualizowanie...";
$MESS["IM_SCOPE_CHATS"] = "Czaty";
$MESS["IM_SCOPE_DEPARTMENTS"] = "Działy";
$MESS["IM_SCOPE_USERS"] = "Pracownicy";
$MESS["INVITE_RESEND_DONE"] = "Zaproszenie zostało pomyślnie wysłane.";
$MESS["OL_LIST_EMPTY"] = "Brak rozmów";
$MESS["OL_SECTION_ANSWERED"] = "Odebrano";
$MESS["OL_SECTION_NEW"] = "Bez odpowiedzi";
$MESS["OL_SECTION_PIN"] = "Przypięto";
$MESS["OL_SECTION_WORK_2"] = "W toku";
$MESS["SEARCH_RECENT"] = "OSTATNIE WYSZUKIWANIE";
$MESS["U_STATUS_OFFLINE"] = "Nieaktywny";
$MESS["U_STATUS_ONLINE"] = "Dostępny";
$MESS["WIDGET_CHAT_CREATE_TITLE"] = "Nowy czat";
