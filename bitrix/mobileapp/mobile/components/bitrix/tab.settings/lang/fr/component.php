<?
$MESS["ERROR"] = "Erreur";
$MESS["PRESET_APPLY_ERROR"] = "Impossible d'appliquer les paramètres du menu";
$MESS["PRESET_NAME_DEFAULT"] = "Par défaut";
$MESS["PRESET_NAME_MANUAL"] = "Paramètres personnalisés";
$MESS["PRESET_NAME_OL"] = "Agent de canal ouvert";
$MESS["PRESET_NAME_STREAM"] = "Communications";
$MESS["PRESET_NAME_TASK"] = "Tâches";
$MESS["PRESET_TITLE"] = "Préréglages";
$MESS["SETTINGS_TAB_ACTIVE_TITLE"] = "Éléments actifs";
$MESS["SETTINGS_TAB_APPLIED"] = "Vos préférences ont été appliquées ! 
Veuillez patienter...";
$MESS["SETTINGS_TAB_BUTTON_DONE"] = "Terminé";
$MESS["SETTINGS_TAB_CANT_MOVE"] = "L'élément \"#title#\" ne peut être déplacé";
$MESS["SETTINGS_TAB_INACTIVE_TITLE"] = "Éléments inactifs";
$MESS["SETTINGS_TAB_MAKE_INACTIVE"] = "Supprimer";
$MESS["TAB_NAME_CHAT"] = "Chats en direct";
$MESS["TAB_NAME_MENU"] = "Menu";
$MESS["TAB_NAME_NOTIFY"] = "Notifications";
$MESS["TAB_NAME_OL"] = "Canaux ouverts";
$MESS["TAB_NAME_STREAM"] = "Flux d'activités";
$MESS["TAB_NAME_TASK"] = "Tâches";
?>