<?php
$MESS["IM_USER_API_ERROR"] = "Error al ejecutar la solicitud. Por favor, inténtelo de nuevo más tarde.";
$MESS["IM_USER_CONNECTION_ERROR"] = "Error al conectarse a Bitrix24. Verifique la conexión de red.";
$MESS["IM_USER_LIST_EMPTY"] = "- La lista de usuarios está vacía -";
$MESS["IM_USER_LIST_KICK"] = "Quitar";
$MESS["IM_USER_LIST_OWNER"] = "Establecer como propietario";
