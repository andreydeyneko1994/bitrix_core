<?php
$MESS["IM_DEPARTMENT_START"] = "Digite o nome do departamento para iniciar a pesquisa.";
$MESS["IM_USER_SELECTOR_API_ERROR"] = "Erro ao conectar novos participantes. Por favor, tente novamente mais tarde.";
$MESS["IM_USER_SELECTOR_CONNECTION_ERROR"] = "Erro ao conectar o Bitrix24. Por favor, verifique a conexão de rede.";
