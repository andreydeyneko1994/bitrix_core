<?php
$MESS["IM_DEPARTMENT_START"] = "Ingrese el nombre del departamento para comenzar la búsqueda.";
$MESS["IM_USER_SELECTOR_API_ERROR"] = "Error al conectar nuevos participantes. Por favor, inténtelo de nuevo más tarde.";
$MESS["IM_USER_SELECTOR_CONNECTION_ERROR"] = "Error al conectarse a Bitrix24. Verifique la conexión de red.";
