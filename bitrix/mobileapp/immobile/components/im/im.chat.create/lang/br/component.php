<?php
$MESS["IM_CHAT_TYPE_CHAT_NEW"] = "Bate-papo privado";
$MESS["IM_CHAT_TYPE_OPEN_NEW"] = "Bate-papo público";
$MESS["IM_CREATE_API_ERROR"] = "Erro ao criar o bate-papo. Por favor, tente novamente mais tarde.";
$MESS["IM_CREATE_CONNECTION_ERROR"] = "Erro ao conectar o Bitrix24. Por favor, verifique a conexão de rede.";
$MESS["IM_DEPARTMENT_START"] = "Digite o nome do departamento para iniciar a pesquisa.";
