<?php
$MESS["SEARCH"] = "Buscar...";
$MESS["SEARCH_CHATS"] = "CHATS";
$MESS["SEARCH_EMPLOYEES"] = "EMPLEADOS";
$MESS["SEARCH_EMPTY"] = "No hay resultados para \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Carga más...";
$MESS["SEARCH_MORE_READY"] = "Mostrando artículos encontrados Carga más...";
