<?php
$MESS["SEARCH"] = "Pesquisar...";
$MESS["SEARCH_BACK"] = "Voltar para a lista";
$MESS["SEARCH_CATEGORY_CHAT"] = "BATE-PAPOS";
$MESS["SEARCH_CATEGORY_DEPARTMENT_USER"] = "Funcionários";
$MESS["SEARCH_CATEGORY_LINE"] = "CANAIS ABERTOS";
$MESS["SEARCH_CATEGORY_USER"] = "FUNCIONÁRIOS";
$MESS["SEARCH_EMPTY"] = "Nenhum resultado para \"#TEXT#\"";
$MESS["SEARCH_MORE"] = "Carregar mais...";
$MESS["SEARCH_MORE_READY"] = "Mostrando itens encontrados. Carregar mais...";
