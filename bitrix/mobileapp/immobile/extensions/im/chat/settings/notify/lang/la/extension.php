<?php
$MESS["SE_NOTIFY_CATEGORY_IM_TITLE"] = "Chat y notificaciones";
$MESS["SE_NOTIFY_DEFAULT_TITLE"] = "Ajustes";
$MESS["SE_NOTIFY_DESC"] = "Notificaciones push y contador";
$MESS["SE_NOTIFY_EMPTY"] = "No se puede configurar esta categoría.";
$MESS["SE_NOTIFY_LOADING"] = "Cargando...";
$MESS["SE_NOTIFY_MAIN_COUNTER_DETAIL_DESC"] = "Seleccione las herramientas cuyos contadores le gustaría ver en el contador de resumen en el icono de la aplicación";
$MESS["SE_NOTIFY_MAIN_COUNTER_TITLE"] = "Contador de aplicaciones";
$MESS["SE_NOTIFY_NOTIFY_TYPES_TITLE"] = "Notificaciones";
$MESS["SE_NOTIFY_PUSH_TITLE"] = "Permitir notificaciones";
$MESS["SE_NOTIFY_SMART_FILTER_DESC"] = "Smart Filter hará que las notificaciones sean menos intrusivas. No recibirá notificaciones mientras navega por Internet o utiliza la aplicación de escritorio sin una pausa notable.";
$MESS["SE_NOTIFY_SMART_FILTER_TITLE"] = "Use el Filtro Smart";
$MESS["SE_NOTIFY_TITLE"] = "Notificaciones";
