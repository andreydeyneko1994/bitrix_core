<?php
$MESS["SE_NOTIFY_CATEGORY_IM_TITLE"] = "Czat i powiadomienia";
$MESS["SE_NOTIFY_DEFAULT_TITLE"] = "Ustawienia";
$MESS["SE_NOTIFY_DESC"] = "Powiadomienia wypychane i licznik";
$MESS["SE_NOTIFY_EMPTY"] = "Nie można skonfigurować tej kategorii.";
$MESS["SE_NOTIFY_LOADING"] = "Ładowanie...";
$MESS["SE_NOTIFY_MAIN_COUNTER_DETAIL_DESC"] = "Wybierz narzędzia, których liczniki chcesz zobaczyć w liczniku zbiorczym na ikonie aplikacji";
$MESS["SE_NOTIFY_MAIN_COUNTER_TITLE"] = "Licznik aplikacji";
$MESS["SE_NOTIFY_NOTIFY_TYPES_TITLE"] = "Powiadomienia";
$MESS["SE_NOTIFY_PUSH_TITLE"] = "Zezwól na powiadomienia";
$MESS["SE_NOTIFY_SMART_FILTER_DESC"] = "Inteligentny filtr zmniejszy uciążliwość powiadomień. Bez zauważalnej przerwy nie będziesz otrzymywać powiadomień podczas przeglądania Internetu ani korzystania z aplikacji na komputerze.";
$MESS["SE_NOTIFY_SMART_FILTER_TITLE"] = "Użyj inteligentnego filtru";
$MESS["SE_NOTIFY_TITLE"] = "Powiadomienia";
