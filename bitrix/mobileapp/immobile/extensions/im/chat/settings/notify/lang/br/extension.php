<?php
$MESS["SE_NOTIFY_CATEGORY_IM_TITLE"] = "Bate-papo e Notificações";
$MESS["SE_NOTIFY_DEFAULT_TITLE"] = "Configurações";
$MESS["SE_NOTIFY_DESC"] = "Notificações via push e contador";
$MESS["SE_NOTIFY_EMPTY"] = "Não é possível configurar esta categoria.";
$MESS["SE_NOTIFY_LOADING"] = "Carregando...";
$MESS["SE_NOTIFY_MAIN_COUNTER_DETAIL_DESC"] = "Selecione as ferramentas cujos contadores você gostaria de ver no contador de resumo no ícone do aplicativo";
$MESS["SE_NOTIFY_MAIN_COUNTER_TITLE"] = "Contador do Aplicativo";
$MESS["SE_NOTIFY_NOTIFY_TYPES_TITLE"] = "Notificações";
$MESS["SE_NOTIFY_PUSH_TITLE"] = "Permitir notificações";
$MESS["SE_NOTIFY_SMART_FILTER_DESC"] = "O Filtro Inteligente tornará as notificações menos intrusivas. Você não receberá notificações enquanto navega na Internet ou usa o aplicativo da área de trabalho sem uma pausa perceptível.";
$MESS["SE_NOTIFY_SMART_FILTER_TITLE"] = "Usar Filtro Inteligente";
$MESS["SE_NOTIFY_TITLE"] = "Notificações";
