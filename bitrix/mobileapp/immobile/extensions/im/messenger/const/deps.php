<?php

return [
	'bundle' => [
		'./src/event-type',
		'./src/feature-flag',
		'./src/recent',
		'./src/rest',
		'./src/message-type',
		'./src/dialog-type',
	],
];