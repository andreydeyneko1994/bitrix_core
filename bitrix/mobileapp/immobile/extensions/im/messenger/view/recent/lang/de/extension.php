<?php
$MESS["IMMOBILE_RECENT_VIEW_EMPTY_EMPTY_BUTTON"] = "Kommunikation starten";
$MESS["IMMOBILE_RECENT_VIEW_EMPTY_TEXT_1"] = "Jetzt Kommunikation starten!";
$MESS["IMMOBILE_RECENT_VIEW_EMPTY_TEXT_CREATE"] = "Klicken Sie auf die Schaltfläche unten, um einen Chat zu erstellen, oder wählen Sie einen Mitarbeiter aus.";
$MESS["IMMOBILE_RECENT_VIEW_EMPTY_TEXT_INVITE"] = "Klicken Sie auf die Schaltfläche unten, um Kollegen in Ihr Bitrix24 einzuladen.";
$MESS["IMMOBILE_RECENT_VIEW_READ_ALL"] = "Alle als gelesen markieren";
