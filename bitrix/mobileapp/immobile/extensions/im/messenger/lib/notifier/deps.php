<?php

return [
	'extensions' => [
		'type',
		'im:messenger/lib/event',
		'im:messenger/const',
		'im:chat/utils',
	],
];