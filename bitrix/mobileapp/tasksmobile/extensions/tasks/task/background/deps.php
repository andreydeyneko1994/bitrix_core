<?php

return [
	'components'=> [
		'tasks:tasks.list',
		'tasks:tasks.view',
	],
];