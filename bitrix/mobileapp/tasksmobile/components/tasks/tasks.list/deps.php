<?php

return [
	'extensions' => [
		'cache',
		'entity-ready',
		'uploader/const',
		'notify',
		'pull/client/events',
		'reload/listeners',
		'selector/recipient',
		'tasks:task',
		'tasks:task/uploader/constants',
		'tasks:task/uploader/storage',
		'utils',
	],
];