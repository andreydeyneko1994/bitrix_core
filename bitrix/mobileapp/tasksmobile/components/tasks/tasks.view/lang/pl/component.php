<?php
$MESS["TASKS_TASK_DETAIL_BTN_ACCEPT_TASK"] = "Akceptacja zadania";
$MESS["TASKS_TASK_DETAIL_BTN_ADD_FAVORITE_TASK"] = "Dodaj do ulubionych";
$MESS["TASKS_TASK_DETAIL_BTN_APPROVE_TASK"] = "Przyjmij jako zakończone";
$MESS["TASKS_TASK_DETAIL_BTN_CLOSE_TASK"] = "Zamknij";
$MESS["TASKS_TASK_DETAIL_BTN_DECLINE_TASK"] = "Odmowa";
$MESS["TASKS_TASK_DETAIL_BTN_DEFER_TASK"] = "Odłóż";
$MESS["TASKS_TASK_DETAIL_BTN_DELEGATE_TASK"] = "Deleguj";
$MESS["TASKS_TASK_DETAIL_BTN_DELETE_FAVORITE_TASK"] = "Usuń z ulubionych";
$MESS["TASKS_TASK_DETAIL_BTN_EDIT"] = "Edycja";
$MESS["TASKS_TASK_DETAIL_BTN_PAUSE_TASK"] = "Wstrzymaj";
$MESS["TASKS_TASK_DETAIL_BTN_REDO_TASK"] = "Powróć po zakończenie";
$MESS["TASKS_TASK_DETAIL_BTN_REMOVE"] = "Usuń";
$MESS["TASKS_TASK_DETAIL_BTN_RENEW_TASK"] = "Wznów";
$MESS["TASKS_TASK_DETAIL_BTN_START_TASK"] = "Start";
$MESS["TASKS_TASK_DETAIL_CONFIRM_REMOVE"] = "Czy na pewno usunąć zadanie?";
$MESS["TASKS_TASK_DETAIL_CONFIRM_REMOVE_NO"] = "Nie";
$MESS["TASKS_TASK_DETAIL_CONFIRM_REMOVE_YES"] = "Tak";
$MESS["TASKS_TASK_DETAIL_DEADLINE_DATE_PICKER"] = "termin ostateczny";
$MESS["TASKS_TASK_DETAIL_IMAGE_PICKER_BITRIX24_DISK"] = "Bitrix24.Drive";
$MESS["TASKS_TASK_DETAIL_IMAGE_PICKER_GALLERY"] = "Galeria";
$MESS["TASKS_TASK_DETAIL_TASK_ADD"] = "Dodaj zadanie";
$MESS["TASKS_TASK_DETAIL_TASK_ADD_SUBTASK"] = "Dodaj podzadanie";
$MESS["TASKS_TASK_DETAIL_TASK_EDIT_CANCEL"] = "Anuluj";
$MESS["TASKS_TASK_DETAIL_TASK_EDIT_TITLE"] = "Edycja";
$MESS["TASKS_TASK_DETAIL_TASK_NEW_SUBTASK_TITLE"] = "Nowe podzadanie";
$MESS["TASKS_TASK_DETAIL_TASK_NEW_TASK_TITLE"] = "Nowe zadanie";
$MESS["TASKS_TASK_DETAIL_TASK_PING_NOTIFICATION"] = "Wysłano ping";
$MESS["TASKS_TASK_DETAIL_TASK_WAS_REMOVED"] = "Usunięto zadanie.";
$MESS["TASKS_TASK_DETAIL_TASK_WAS_REMOVED_IN_ANOTHER_PLACE"] = "Zadanie zostało usunięte gdzieś indziej.";
$MESS["TASKS_TASK_DETAIL_TITLE_RESPONSIBLE"] = "osoba odpowiedzialna";
