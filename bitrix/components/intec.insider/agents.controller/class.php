<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use intec\Core;
use intec\core\base\InvalidParamException;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Encoding;
use intec\core\helpers\Json;
use intec\core\helpers\Type;
use intec\core\net\Url;
use intec\core\web\UploadedFile;
use intec\insider\models\Agent;
use intec\insider\models\agent\LogRecord;
use intec\insider\models\user\Activity;

class IntecInsiderAgentsControllerComponent extends CBitrixComponent
{
    protected $_agent;

    protected $_type;

    protected $_key;

    protected $_version;

    protected $_user;

    public function getAgent()
    {
        return $this->_agent;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function getKey()
    {
        return $this->_key;
    }

    public function getVersion()
    {
        return $this->_version;
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function decryptData($data, $binary = false)
    {
        try {
            return openssl_decrypt($binary ? $data : base64_decode($data), 'aes-256-cbc', $this->_key, OPENSSL_RAW_DATA, str_repeat(chr(0), 16));
        } catch (Exception $exception) {}

        return null;
    }

    public function decryptObject($object)
    {
        $result = null;

        if (Type::isArray($object)) {
            $result = [];

            foreach ($object as $fieldKey => $fieldValue)
                $result[$fieldKey] = $this->decryptObject($fieldValue);
        } else {
            $result = $this->decryptData($object);
        }

        return $result;
    }

    public function encryptData($data)
    {
        try {
            return base64_encode(openssl_encrypt($data, 'aes-256-cbc', $this->_key, OPENSSL_RAW_DATA, str_repeat(chr(0), 16)));
        } catch (Exception $exception) {}

        return null;
    }

    public function sendError()
    {
        global $APPLICATION;

        if (!defined('ERROR_404'))
            define('ERROR_404', 'Y');

        CHTTP::setStatus("404 Not Found");

        if ($APPLICATION->RestartWorkarea()) {
            require(Application::getDocumentRoot().'/404.php');
            exit();
        }
    }

    public function sendResponse($data = null)
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();
        $hash = Core::$app->security->generateRandomString(32);

        header('AgentHash: '.$this->encryptData($hash));
        header('AgentSign: '.$this->encryptData('agent'.$hash.'success'));

        echo $data !== null ? $this->encryptData($data) : null;
        exit();
    }

    public function sendJsonResponse($data = null)
    {
        return $this->sendResponse(Json::encode(Encoding::convert($data, Encoding::UTF8, Encoding::getDefault())));
    }

    public function actionReceiveCommands()
    {
        $commands = [];

        if ($this->_version !== '1.0.0')
            $commands[] = ['name' => 'update'];

        if ($this->_type === 'user') {
            $commands[] = [
                'name' => 'configure',
                'data' => [
                    'screenCaptureEnabled' => 'true',
                    'screenCaptureInterval' => '60000'
                ]
            ];
        }

        return $this->sendJsonResponse($commands);
    }

    public function actionReceiveUpdateLink()
    {
        $url = new Url(Core::$app->request->getAbsoluteUrl());
        $url->setPathString('/bitrix/components/intec.insider/agents.controller1/update.php');

        return $this->sendResponse($url->build());
    }

    public function actionRegister()
    {
        $request = Core::$app->request;
        $key = $request->post('key');
        $information = $request->post('information');
        $information = $this->decryptObject($information);

        try {
            $information = Json::decode($information);
            $information = Encoding::convert($information, null, Encoding::UTF8);
        } catch (InvalidParamException $exception) {
            $information = [];
        }

        if (!Type::isArray($information))
            $information = [];

        $information = ArrayHelper::merge([
            'userDomain' => 'Workgroup',
            'userName' => 'Anonymous',
            'operatingSystem' => 'Unknown',
            'address' => '127.0.0.1'
        ], $information);

        $agent = null;

        if (!empty($key)) {
            $key = $this->decryptObject($key);
            $agent = Agent::find()->where([
                'key' => $key
            ])->one();
        }

        if (empty($agent)) {
            $agent = new Agent();
            $agent->generateKey();
        }

        $agent->userDomain = $information['userDomain'];
        $agent->userName = $information['userName'];
        $agent->operatingSystem = $information['operatingSystem'];
        $agent->address = $information['address'];

        if ($agent->save()) {
            return $this->sendJsonResponse([
                'identifier' => $agent->id,
                'key' => $agent->key
            ]);
        }

        return $this->sendError();
    }

    public function actionSendActivities()
    {
        if ($this->_type !== 'user')
            return $this->sendError();

        if (empty($this->_user))
            return $this->sendResponse();

        if (!$this->_user->active)
            return $this->sendResponse('success');

        $request = Core::$app->request;
        $activities = $request->post('activities');
        $timezone = new DateTimeZone(date_default_timezone_get());

        if (Type::isArray($activities))
            foreach ($activities as $index => $activity) {
                if (!Type::isArray($activity))
                    continue;

                $activity = $this->decryptObject($activity);
                $activity = Encoding::convert($activity, null, Encoding::UTF8);
                $activity = ArrayHelper::merge([
                    'type' => null,
                    'date' => null
                ], $activity);

                $date = DateTime::createFromFormat('Y-m-d H:i:s T', $activity['date']);

                if (!$date)
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $activity['date']);

                if (!$date)
                    continue;

                $date->setTimezone($timezone);

                $model = new Activity();
                $model->userId = $this->_user->id;
                $model->agentId = $this->_agent->id;
                $model->type = $activity['type'];
                $model->date = $date->format('Y-m-d H:i:s');
                $data = [];

                if ($activity['type'] === 'screen') {
                    $file = UploadedFile::getInstanceByName('activities['.$index.'][image]');

                    if (empty($file) || empty($file->name))
                        continue;

                    $fileContent = file_get_contents($file->tempName);
                    $fileContent = $this->decryptData($fileContent, true);

                    file_put_contents($file->tempName, $fileContent);

                    $fileArray = CFile::MakeFileArray($file->tempName, $file->type, true);
                    $fileArray['name'] = $file->name;
                    $fileArray['MODULE_ID'] = 'intec.insider';

                    if (empty($fileContent))
                        continue;

                    if (CFile::CheckImageFile($fileArray) !== null)
                        continue;

                    $fileId = CFile::SaveFile($fileArray, 'insider');

                    if (!Type::isInteger($fileId))
                        continue;

                    $data['file'] = $fileId;
                } else if ($activity['type'] === 'mouse') {
                    $data['state'] = $activity['state'] === '1' ? true : false;
                } else if ($activity['type'] === 'keyboard') {
                } else if ($activity['type'] === 'processes') {
                    $processes = ArrayHelper::getValue($activity, ['processes']);

                    if (Type::isArray($processes))
                        foreach ($processes as $process) {
                            if (!Type::isArray($process))
                                continue;

                            $process = ArrayHelper::merge([
                                'name' => null,
                                'title' => null
                            ], $process);

                            if (empty($process['name']))
                                continue;

                            $data[] = [
                                'name' => $process['name'],
                                'title' => $process['title']
                            ];
                        }
                } else if ($activity['type'] === 'site') {
                    $activity = ArrayHelper::merge([
                        'title' => null,
                        'url' => null
                    ], $activity);

                    if (empty($activity['url']))
                        continue;

                    $data['title'] = $activity['title'];
                    $data['url'] = $activity['url'];

                    if (!preg_match('/.*?:\\/\\//i', $data['url']))
                        $data['url'] = 'http://'.$data['url'];
                } else if ($activity['type'] === 'state') {
                    $data['active'] = $activity['active'] === '1' ? true : false;
                } else {
                    continue;
                }

                $model->data = $data;
                $model->handled = 0;
                $model->save();
            }

        return $this->sendResponse("success");
    }

    public function actionSendLog()
    {
        $request = Core::$app->request;
        $records = $request->post('records');
        $timezone = new DateTimeZone(date_default_timezone_get());

        if (Type::isArray($records))
            foreach ($records as $record) {
                if (!Type::isArray($record))
                    continue;

                $record = $this->decryptObject($record);
                $record = Encoding::convert($record, null, Encoding::UTF8);
                $record = ArrayHelper::merge([
                    'level' => null,
                    'date' => null,
                    'message' => null
                ], $record);

                $date = DateTime::createFromFormat('Y-m-d H:i:s T', $record['date']);

                if (!$date)
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $record['date']);

                if (!$date)
                    continue;

                $date->setTimezone($timezone);

                $model = new LogRecord();
                $model->agentId = $this->_agent->id;
                $model->level = $record['level'];
                $model->date = $date->format('Y-m-d H:i:s');
                $model->message = $record['message'];
                $model->save();
            }

        return $this->sendResponse("success");
    }

    public function actionTest()
    {
        return $this->sendResponse('success');
    }

    public function executeComponent()
    {
        if (
            !Loader::includeModule('intec.core') ||
            !Loader::includeModule('intec.insider')
        ) return $this->sendError();

        $request = Core::$app->request;
        $headers = $request->getHeaders();

        if (!$request->getIsPost())
            return $this->sendError();

        $action = $request->post('action');
        $credentials = $headers->get('AgentCredentials');
        $version = $headers->get('AgentVersion');
        $hash = $headers->get('AgentHash');
        $sign = $headers->get('AgentSign');

        if (empty($action) || empty($credentials) || empty($version) || empty($sign) || empty($hash))
            return $this->sendError();

        try {
            $agent = Json::decode($credentials);
            $agent = Encoding::convert($agent, null, Encoding::UTF8);

            if (!Type::isArray($agent) || !isset($agent['type']))
                return $this->sendError();

            $this->_type = $agent['type'];

            if ($this->_type === 'user') {
                if (!isset($agent['data']))
                    return $this->sendError();

                /**
                 * @var Agent $agent
                 */
                $agent = Agent::find()->where([
                    'id' => $agent['data']
                ])->one();

                if (empty($agent))
                    return $this->sendError();

                $user = $agent->getUser(true);

                $this->_agent = $agent;
                $this->_key = $agent->key;
                $this->_user = $user;
            } else if ($this->_type === 'base') {
                $this->_key = Option::get('intec.insider', 'key', '', SITE_ID);
            } else {
                return $this->sendError();
            }
        } catch (InvalidParamException $exception) {
            return $this->sendError();
        } finally {
            unset($key);
            unset($user);
        }

        $action = $this->decryptData($action);
        $version = $this->decryptData($version);
        $hash = $this->decryptData($hash);
        $sign = $this->decryptData($sign);

        if (empty($action) || empty($version) || empty($hash) || $sign !== 'agent'.$credentials.$hash)
            return $this->sendError();

        $this->_version = $version;

        if (method_exists($this, 'action'.$action))
            call_user_func([$this, 'action'.$action]);

        return $this->sendError();
    }
}