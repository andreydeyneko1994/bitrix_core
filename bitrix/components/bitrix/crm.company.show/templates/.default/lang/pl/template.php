<?
$MESS["CRM_COMPANY_BIZPROC_LIST"] = "Proces Biznesowy";
$MESS["CRM_COMPANY_COMMENT"] = "Komentarz";
$MESS["CRM_COMPANY_CREATOR"] = "Utworzony przez";
$MESS["CRM_COMPANY_DATE_CREATE"] = "Utworzony";
$MESS["CRM_COMPANY_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_COMPANY_EMAIL"] = "E-mail";
$MESS["CRM_COMPANY_EMPLOYEES"] = "Pracownicy";
$MESS["CRM_COMPANY_EMPLOYEES_UNDEF"] = "[nie ustawiony]";
$MESS["CRM_COMPANY_EVENT_LIST"] = "Ostatnie zmiany";
$MESS["CRM_COMPANY_GET_USER_INFO_GENERAL_ERROR"] = "Nie można odzyskać informacji użytkownika.";
$MESS["CRM_COMPANY_IM"] = "Komunikator";
$MESS["CRM_COMPANY_INDUSTRY"] = "Branża";
$MESS["CRM_COMPANY_INDUSTRY_UNDEF"] = "[niezdefiniowane]";
$MESS["CRM_COMPANY_MODIFIER"] = "Edytowane przez";
$MESS["CRM_COMPANY_NOT_OPENED"] = "Ta firma nie jest publiczna.";
$MESS["CRM_COMPANY_OPENED"] = "Firma ta jest publiczna i każdy może ją wyświetlać.";
$MESS["CRM_COMPANY_PHONE"] = "Telefon";
$MESS["CRM_COMPANY_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_COMPANY_RESPONSIBLE_CHANGE"] = "zmień";
$MESS["CRM_COMPANY_REVENUE"] = "Roczne Przychody";
$MESS["CRM_COMPANY_SHOW_LEGEND"] = "firma ##ID#";
$MESS["CRM_COMPANY_SHOW_NAVIGATION_NEXT"] = "Następny";
$MESS["CRM_COMPANY_SHOW_NAVIGATION_PREV"] = "Poprzednie";
$MESS["CRM_COMPANY_SHOW_TITLE"] = "Firma ##ID# &mdash; #TITLE#";
$MESS["CRM_COMPANY_SIDEBAR_TITLE"] = "Informacje o firmie";
$MESS["CRM_COMPANY_TYPE"] = "Rodzaj";
$MESS["CRM_COMPANY_TYPE_UNDEF"] = "[niezdefiniowane]";
$MESS["CRM_COMPANY_WEB"] = "Strona internetowa";
$MESS["CRM_EDIT_BTN_TTL"] = "Kliknij, aby edytować";
$MESS["CRM_LOCK_BTN_TTL"] = "Nie można edytować tego elementu";
$MESS["CRM_TAB_1"] = "Firma";
$MESS["CRM_TAB_1_TITLE"] = "Właściwości firmy";
$MESS["CRM_TAB_2"] = "Kontakty";
$MESS["CRM_TAB_2_TITLE"] = "Kontakty firmy";
$MESS["CRM_TAB_3"] = "Deale";
$MESS["CRM_TAB_3_TITLE"] = "Deale firmy";
$MESS["CRM_TAB_4"] = "Zapis";
$MESS["CRM_TAB_4_TITLE"] = "Log firmy";
$MESS["CRM_TAB_5"] = "Zapis";
$MESS["CRM_TAB_5_TITLE"] = "Dziennik Deala";
$MESS["CRM_TAB_6"] = "Aktywność";
$MESS["CRM_TAB_6_TITLE"] = "Aktywność Firmy";
$MESS["CRM_TAB_7"] = "Proces Biznesowy";
$MESS["CRM_TAB_7_TITLE"] = "Procesy biznesowe leada";
$MESS["CRM_TAB_8"] = "Faktury";
$MESS["CRM_TAB_8_TITLE"] = "Faktury firmy";
$MESS["CRM_TAB_9"] = "Oferty";
$MESS["CRM_TAB_9_TITLE"] = "Oferty firmy";
$MESS["CRM_TAB_DETAILS"] = "Szczegóły";
$MESS["CRM_TAB_DETAILS_TITLE"] = "Szczegóły";
$MESS["CRM_TAB_EVENT"] = "Dziennik Zmian";
$MESS["CRM_TAB_EVENT_TITLE"] = "Dziennik zmian firmy";
$MESS["CRM_TAB_HISTORY"] = "Historia";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Dziennik zmian firmy";
$MESS["CRM_TAB_LEAD"] = "Leady";
$MESS["CRM_TAB_LEAD_TITLE"] = "Leady firmy";
$MESS["CRM_TAB_LIVE_FEED"] = "Tablica";
$MESS["CRM_TAB_LIVE_FEED_TITLE"] = "Tablica";
$MESS["CRM_TAB_REQUISITE"] = "Dane firmy";
$MESS["CRM_TAB_REQUISITE_TITLE"] = "Dane firmy";
$MESS["CRM_TAB_TREE"] = "Zależności";
$MESS["CRM_TAB_TREE_TITLE"] = "Odnośniki do innych elementów i pozycji";
?>