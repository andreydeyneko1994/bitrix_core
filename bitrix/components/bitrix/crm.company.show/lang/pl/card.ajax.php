<?
$MESS["CRM_COLUMN_COMPANY_TYPE"] = "Typ firmy";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_EMPLOYEES"] = "Pracownicy";
$MESS["CRM_COLUMN_PHONE"] = "Telefon";
$MESS["CRM_COLUMN_WEB"] = "Strona";
$MESS["CRM_OPER_EDIT"] = "Edytuj";
$MESS["CRM_OPER_SHOW"] = "Wyświetl";
$MESS["CRM_SECTION_COMPANY_INFO"] = "Informacje o Firmie";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Informacje kontaktowe";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Telefon";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Nieznana osoba dzwoniąca";
?>