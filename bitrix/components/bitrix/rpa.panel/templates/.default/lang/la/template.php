<?php
$MESS["RPA_PANEL_DELETE_CONFIRM_TEXT"] = "¿Seguro que desea eliminar este procesos de negocio? Esta acción no se puede deshacer.";
$MESS["RPA_PANEL_DELETE_CONFIRM_TITLE"] = "Eliminar el procesos de negocio";
