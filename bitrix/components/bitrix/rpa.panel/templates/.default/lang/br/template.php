<?
$MESS["RPA_PANEL_DELETE_CONFIRM_TEXT"] = "Você tem certeza que deseja excluir este fluxo de trabalho? Esta ação não pode ser desfeita.";
$MESS["RPA_PANEL_DELETE_CONFIRM_TITLE"] = "Excluir fluxo de trabalho";
?>