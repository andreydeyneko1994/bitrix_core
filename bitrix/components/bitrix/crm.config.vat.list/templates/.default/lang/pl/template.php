<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_ALERT"] = "Jeżeli jest to konieczne możesz wyłączyć tą opcję. Upewnij się, że wyłączenie tej opcji nie naruszy żadnych praw czy regulacji, których musisz przestrzegać.";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_TITLE"] = "Podatek od towarów może być stosowany do całej listy, to znaczy dla każdego przedmiotu w dokumencie lud do żadnego z przedmiotów. W obrębie jednego dokumentu nie mogą znajdować się takie przedmioty, dla których podatek ma zastosowanie, i takie których on nie obejmuje.";
$MESS["CRM_VAT_DELETE"] = "Usuń stawkę VAT";
$MESS["CRM_VAT_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_VAT_DELETE_TITLE"] = "Usunąć tę stawkę podatku";
$MESS["CRM_VAT_EDIT"] = "Edytuj VAT";
$MESS["CRM_VAT_EDIT_TITLE"] = "Otwórz ten podatek do edycji";
$MESS["CRM_VAT_SHOW"] = "Wyświetl podatek";
$MESS["CRM_VAT_SHOW_TITLE"] = "Wyświetl ten podatek";
?>