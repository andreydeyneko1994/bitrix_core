<?
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_COLUMN_ACTIVE"] = "Aktywne";
$MESS["CRM_COLUMN_C_SORT"] = "Sortuj";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Nazwa";
$MESS["CRM_COLUMN_RATE"] = "Stawka";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_VAT_DELETION_GENERAL_ERROR"] = "Błąd usuwania podatku.";
$MESS["CRM_VAT_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji podatku.";
?>