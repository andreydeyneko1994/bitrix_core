<?
$MESS["CRM_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module Blocs d'Information n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PRODUCTPROP_ADD"] = "Ajouter";
$MESS["CRM_PRODUCTPROP_ADD_TITLE"] = "Créer une nouvelle propriété du produit";
$MESS["CRM_PRODUCTPROP_LIST"] = "Propriétés";
$MESS["CRM_PRODUCTPROP_LIST_TITLE"] = "Voir toutes les propriétés de ce produit";
?>