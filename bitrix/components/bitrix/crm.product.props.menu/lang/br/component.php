<?
$MESS["CRM_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informações não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PRODUCTPROP_ADD"] = "Adicionar";
$MESS["CRM_PRODUCTPROP_ADD_TITLE"] = "Criar um nova propriedade de produto";
$MESS["CRM_PRODUCTPROP_LIST"] = "Propriedades";
$MESS["CRM_PRODUCTPROP_LIST_TITLE"] = "Visualizar todas as propriedades de produto";
?>