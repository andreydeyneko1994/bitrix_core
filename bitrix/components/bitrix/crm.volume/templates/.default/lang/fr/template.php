<?
$MESS["CRM_VOLUME_AGENT_FINISHED_COMMENT"] = "La tâche de nettoyage est terminée. Vous pouvez relancer l'analyse du disque.";
$MESS["CRM_VOLUME_CANCEL"] = "Annuler";
$MESS["CRM_VOLUME_CANCEL_WORKERS"] = "Annuler toutes les tâches de nettoyage";
$MESS["CRM_VOLUME_CLEARING"] = "Nettoyage du CRM";
$MESS["CRM_VOLUME_CLOSE_WARNING"] = "La page du portail doit rester ouverte pour finaliser le nettoyage du lecteur.";
$MESS["CRM_VOLUME_ERROR"] = "C'est une erreur.";
$MESS["CRM_VOLUME_MEASURE_ACCEPT"] = "Oui, je veux relancer l'analyse de disque";
$MESS["CRM_VOLUME_MEASURE_CONFIRM"] = "L'analyse de disque arrêtera le processus de nettoyage. Vous pourrez le relancer une fois le lecteur analysé.";
$MESS["CRM_VOLUME_MEASURE_CONFIRM_QUESTION"] = "Voulez-vous vraiment annuler le nettoyage pour relancer l'analyse de disque ?";
$MESS["CRM_VOLUME_MEASURE_DATA"] = "Démarrer l'analyse";
$MESS["CRM_VOLUME_MEASURE_DATA_REPEAT"] = "Répéter l'analyse";
$MESS["CRM_VOLUME_MEASURE_PROCESS"] = "L'analyse peut prendre un moment.";
$MESS["CRM_VOLUME_MEASURE_TITLE"] = "Analyse de l'espace utilisé...";
$MESS["CRM_VOLUME_NEED_RELOAD_COMMENT"] = "Les résultats de la précédente analyse peuvent être obsolètes.";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_MEASURE"] = "Annuler l'analyse";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_WORKERS"] = "Annuler la tâche de nettoyage";
$MESS["CRM_VOLUME_PERFORMING_MEASURE_DATA"] = "Récupération des données";
$MESS["CRM_VOLUME_PERFORMING_QUEUE"] = "Récupération des données. Étape #QUEUE_STEP# of #QUEUE_LENGTH#";
$MESS["CRM_VOLUME_SETUP_CLEANER"] = "Programme de nettoyage";
$MESS["CRM_VOLUME_START_COMMENT"] = "Bitrix24 va analyser vos documents et vous proposer de sélectionner les éléments dont vous n'avez plus besoin pour libérer de l'espace.";
$MESS["CRM_VOLUME_START_TITLE"] = "Espace libre";
$MESS["CRM_VOLUME_SUCCESS"] = "Réussi";
$MESS["CRM_VOLUME_TITLE"] = "Utilisation du lecteur";
$MESS["CRM_VOLUME_TOTAL_FILES"] = "Utilisation par les fichiers : #FILE_SIZE#";
$MESS["CRM_VOLUME_TOTAL_USEAGE"] = "Espace total utilisé : #FILE_SIZE#";
?>