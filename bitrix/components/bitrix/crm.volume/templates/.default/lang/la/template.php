<?
$MESS["CRM_VOLUME_AGENT_FINISHED_COMMENT"] = "Tarea de limpieza finalizada Tienes que escanear el drive de nuevo.";
$MESS["CRM_VOLUME_CANCEL"] = "Cancelar";
$MESS["CRM_VOLUME_CANCEL_WORKERS"] = "Cancelar todas las tareas de limpieza";
$MESS["CRM_VOLUME_CLEARING"] = "Limpieza del CRM";
$MESS["CRM_VOLUME_CLOSE_WARNING"] = "La página del portal debe permanecer abierta para completar la limpieza del drive.";
$MESS["CRM_VOLUME_ERROR"] = "Eso es un error.";
$MESS["CRM_VOLUME_MEASURE_ACCEPT"] = "Sí, escanearlo de nuevo";
$MESS["CRM_VOLUME_MEASURE_CONFIRM"] = "La exploración del drive detendrá el proceso de limpieza. Podrá volver a iniciarlo, solo después de haber escaneado el drive.";
$MESS["CRM_VOLUME_MEASURE_CONFIRM_QUESTION"] = "¿Usted está seguro que desea cancelar la limpieza y escanear la unidad de nuevo?";
$MESS["CRM_VOLUME_MEASURE_DATA"] = "Iniciar escaneo";
$MESS["CRM_VOLUME_MEASURE_DATA_REPEAT"] = "Repetir el escaneo";
$MESS["CRM_VOLUME_MEASURE_PROCESS"] = "El escaneo puede demorar un tiempo.";
$MESS["CRM_VOLUME_MEASURE_TITLE"] = "Analizando el espacio usado...";
$MESS["CRM_VOLUME_NEED_RELOAD_COMMENT"] = "Los resultados del examen anterior pueden estar desactualizados.";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_MEASURE"] = "Cancelar escaneo";
$MESS["CRM_VOLUME_PERFORMING_CANCEL_WORKERS"] = "Cancelar la tarea de limpieza";
$MESS["CRM_VOLUME_PERFORMING_MEASURE_DATA"] = "Recopilación de datos";
$MESS["CRM_VOLUME_PERFORMING_QUEUE"] = "Recolectando datos. Paso #QUEUE_STEP# of #QUEUE_LENGTH#";
$MESS["CRM_VOLUME_SETUP_CLEANER"] = "Programación de limpieza";
$MESS["CRM_VOLUME_START_COMMENT"] = "Bitrix24 escaneará sus documentos y le pedirá que seleccione los elementos que ya no necesita para liberar más espacio.";
$MESS["CRM_VOLUME_START_TITLE"] = "Espacio Libre";
$MESS["CRM_VOLUME_SUCCESS"] = "Éxito";
$MESS["CRM_VOLUME_TITLE"] = "Uso del drive";
$MESS["CRM_VOLUME_TOTAL_FILES"] = "Usado por archivos: #FILE_SIZE#";
$MESS["CRM_VOLUME_TOTAL_USEAGE"] = "Total de espacio utilizado: #FILE_SIZE#";
?>