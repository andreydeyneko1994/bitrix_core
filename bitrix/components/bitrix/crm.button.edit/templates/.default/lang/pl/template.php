<?php
$MESS["CRM_BUTTON_EDIT_AVATAR_LOADED"] = "Załadowane zdjęcia";
$MESS["CRM_BUTTON_EDIT_AVATAR_PRESET"] = "standardowe";
$MESS["CRM_BUTTON_EDIT_BTN_CHANGE"] = "zmień";
$MESS["CRM_BUTTON_EDIT_CLOSE"] = "Zamknij";
$MESS["CRM_BUTTON_EDIT_COLOR_BG"] = "Kolor tła";
$MESS["CRM_BUTTON_EDIT_COLOR_ICON"] = "Kolor ikony";
$MESS["CRM_BUTTON_EDIT_DELETE_CONFIRM"] = "Czy chcesz usunąć tę fotografię?";
$MESS["CRM_BUTTON_EDIT_DETAIL_CALLBACK_PHONE_NUMBER"] = "numer telefonu";
$MESS["CRM_BUTTON_EDIT_DETAIL_CRMFORM_FIELDS"] = "pola formularza";
$MESS["CRM_BUTTON_EDIT_DETAIL_OPENLINE_CHANNELS"] = "podłączone kanały";
$MESS["CRM_BUTTON_EDIT_DO_NOT_SHOW"] = "ukryj na urządzeniach mobilnych";
$MESS["CRM_BUTTON_EDIT_ERROR_ACTION"] = "Akcja została przerwana z powodu błędu.";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE"] = "Wybierz język";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE_TIP"] = "Uwaga! Zmiana języka nie spowoduje przetłumaczenia tekstu wprowadzonego przez użytkownika ani etykiet wpisanych ręcznie podczas tworzenia widżetu. Tekst ten można edytować w formularzu CRM, Otwartym Kanale lub odpowiednich stronach ustawień wywołania zwrotnego (Callback).";
$MESS["CRM_BUTTON_EDIT_MOBILE_DEVICES"] = "Urządzenia mobilne";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ADD"] = "dodaj Otwarty Kanał";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS"] = "Dodatkowe kanały";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS_EMPTY"] = "Możesz utworzyć Otwarty Kanał i połączyć istniejące źródła Otwartych Kanałów.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_CHOISE_CHANNEL_HINT"] = "wybierz źródła, które chcesz widzieć w widżecie. Widżety pokazują tylko wybrane źródła danego rodzaju, na przykład jedno źródło z Facebooka, jedno źródło z Vibera, etc.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_REMOVE"] = "rozłącz";
$MESS["CRM_BUTTON_EDIT_OPENLINE_USE_MULTI_LINES"] = "połącz źródła z innych Otwartych Kanałów";
$MESS["CRM_BUTTON_EDIT_WORK_TIME"] = "Preferencje dotyczące czasu pracy";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_DISABLED"] = "nieskonfigurowane";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_RULE"] = "Przetwarzanie wiadomości po godzinach";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_TEXT"] = "Wiadomość";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_DAY_OFF"] = "Weekendy";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ENABLED"] = "Pokaż tylko w godzinach pracy";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS"] = "Święta";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS_EXAMPLE"] = "Przykład: 01.01,04.07,01.11,25.12";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME"] = "Godziny pracy";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME_ZONE"] = "Wybierz strefę czasową";
$MESS["CRM_WEBFORM_EDIT_APPLY"] = "Zastosuj";
$MESS["CRM_WEBFORM_EDIT_CHANNELS"] = "Podłączone kanały";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD"] = "Utwórz";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD_DESC"] = "Nie ma aktualnie żadnych aktywnych kanałów. Otwórz ustawienia, aby włączyć kanały.";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT"] = "konfiguracja";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT_FORM"] = "skonfiguruj formularz";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT_WHATSAPP"] = "skonfiguruj WhatsApp";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_SETUP"] = "Ustawienia";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_SETUP_APPROVE"] = "Jeśli WhatsApp jest skonfigurowany, kliknij, aby przeładować stronę.";
$MESS["CRM_WEBFORM_EDIT_COPY_TO_CLIPBOARD"] = "Kopiuj do schowka";
$MESS["CRM_WEBFORM_EDIT_DESC"] = "Wybierz kanały do komunikowania się ze swoimi klientami. Możesz włączać i wyłączać kanały wedle potrzeby. Personalizuj wygląd widżetu. Kod widżetu będzie dostępny po zapisaniu formularza.";
$MESS["CRM_WEBFORM_EDIT_EDIT"] = "Edytuj";
$MESS["CRM_WEBFORM_EDIT_HELLO_ADD"] = "dodaj wiadomość powitalną";
$MESS["CRM_WEBFORM_EDIT_HELLO_CHANGE"] = "zmień";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_NAME"] = "Lisa Smith";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_TEXT"] = "Cześć! Jestem Twoją osobistą pomocniczką. Daj mi znać, jeśli będziesz mieć pytania.";
$MESS["CRM_WEBFORM_EDIT_HELLO_DESC"] = "Wybierz strony, które wyświetlą wiadomość powitalną dla klientów. Prześlij grafikę i wpisz tekst, aby przyciągnąć więcej klientów.";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE"] = "Tryb wyświetlania wiadomości powitalnej";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_EXCLUDE"] = "Na wszystkich stronach";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_INCLUDE"] = "Tylko na wybranych stronach";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE"] = "Z wyjątkiem tych stron";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE_ADDITIONAL"] = "i stron z indywidualnie konfigurowanymi wiadomościami";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_LIST"] = "Pokaż na tych stronach wiadomość powitalną";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY"] = "Pokaż opóźnienie";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_NO"] = "pokaż natychmiast";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_TIP"] = "Określa czas oczekiwania przed wyświetleniem wiadomości powitalnej po pojawieniu się widżetu.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TITLE"] = "Automatyczna wiadomość powitalna";
$MESS["CRM_WEBFORM_EDIT_HELLO_TUNE"] = "Skonfiguruj wiadomość powitalną";
$MESS["CRM_WEBFORM_EDIT_HINT_ANY"] = "* - gwiazdka oznacza jakikolwiek ciąg znaków.
Przykłady:
- Wszystkie strony w katalogu /catalog/ na stronie www.example.com
http://www.example.com/catalog/*

- Jakakolwiek strona pod adresem z /basket/ w ścieżce
*/basket/*

- Jakakolwiek strona posiadająca \"utm_source\" i \"utm_company\" podciągi w ścieżce
*utm_source*utm_company*

- Wszystkie strony w /catalog/ na wszystkich subdomenach example.com
http://*.example.com/catalog/*";
$MESS["CRM_WEBFORM_EDIT_NAME_PLACEHOLDER"] = "Nazwa widżetu";
$MESS["CRM_WEBFORM_EDIT_OFF"] = " ";
$MESS["CRM_WEBFORM_EDIT_ON"] = " ";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS"] = "Ustawienia wyświetlania widżetu";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_DEFAULT"] = "domyślnie na wszystkich stronach";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_SETUP"] = "konfiguracja";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_USER"] = "ustawienia własne";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO"] = "usuń podpis";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO_BX"] = "Powered by Bitrix24";
$MESS["CRM_WEBFORM_EDIT_SAVE"] = "Zapisz";
$MESS["CRM_WEBFORM_EDIT_SECTION_ALL"] = "Domyślna wiadomość powitalna dla wszystkich stron";
$MESS["CRM_WEBFORM_EDIT_SECTION_CUSTOM"] = "Niestandardowa wiadomość powitalna";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST"] = "Z powrotem do widżetów";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST1"] = "powrót do listy";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_LIST"] = "Wstecz";
$MESS["CRM_WEBFORM_EDIT_SHOW_CHOOSE_LOCATION"] = "Wybierz pozycję";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY"] = "Pokaż widżet";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_AT_ONCE"] = "natychmiast po załadowaniu strony";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_DELAY"] = "wprowadź opóźnienie";
$MESS["CRM_WEBFORM_EDIT_SHOW_ONLY_PAGES"] = "jedynie na wybranych stronach";
$MESS["CRM_WEBFORM_EDIT_SHOW_ON_ALL_PAGES"] = "na wszystkich stronach poza";
$MESS["CRM_WEBFORM_EDIT_SHOW_VIEW"] = "Widok";
$MESS["CRM_WEBFORM_EDIT_SITE_HELLO_TITLE"] = "Automatyczna wiadomość powitalna";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT"] = "Kod widżetu";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT_TIP"] = "Skopiuj kod i wklej na swojej stronie przed  &lt;/body&gt; zamykający tag.";
