<?
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Il vous faut une transaction et une facture pour créer la permission de poursuivre.";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Annuler";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Continuer";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Sélectionnez les entités dans lesquelles les champs manquants seront créés";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Ces champs seront créés";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données de devis. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Créer une entité basée sur le devis";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Erreur générique.";
$MESS["CRM_QUOTE_DETAIL_HISTORY_STUB"] = "Vous créez en ce moment un devis...";
$MESS["CRM_QUOTE_EDIT_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_QUOTE_EDIT_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_QUOTE_EDIT_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Préférences de la transaction";
?>