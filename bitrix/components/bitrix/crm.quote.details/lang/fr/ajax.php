<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Nouveau devis";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "Erreur lors de la suppression du devis.";
$MESS["CRM_QUOTE_NOT_FOUND"] = "Devis introuvable.";
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Une erreur est survenue pendant l'enregistrement des produits.";
?>