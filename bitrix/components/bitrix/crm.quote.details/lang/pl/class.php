<?php
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS"] = "Działania";
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS_EMAIL"] = "Wyślij wiadomość e-mail";
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS_PDF"] = "Pobierz plik PDF";
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS_PDF_BLANK"] = "Pobierz zwykły plik PDF";
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS_PRINT"] = "Drukuj";
$MESS["CRM_QUOTE_DETAILS_BUTTON_ACTIONS_PRINT_BLANK"] = "Drukuj zwykłą ofertę";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_ACCESS_DENIED"] = "Aby kontynuować, potrzebujesz uprawnień do tworzenia deala i faktury.";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_DIALOG_TITLE"] = "Utwórz obiekt z oferty";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_NO_CONFIG_ERROR"] = "Nie można znaleźć konfiguracji konwersji";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_SYNC_ENTITY_LIST"] = "Wybierz obiekty, w których utworzone zostaną brakujące pola";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_SYNC_FIELD_LIST"] = "Te pola zostaną utworzone";
$MESS["CRM_QUOTE_DETAILS_CONVERSION_SYNC_LEGEND"] = "Wybrane obiekty nie mają pól, które mogą przechowywać dane oferty. Wybierz obiekty, w których powstaną brakujące pola do wszystkich dostępnych informacji.";
$MESS["CRM_QUOTE_DETAILS_CONVERTED_FROM_DEAL"] = "<div class=\"crm-conv-info\">Stworzono ofertę na podstawie deala <a href='#URL#'>#TITLE#</a></div>";
$MESS["CRM_QUOTE_DETAILS_COPY_PAGE_URL"] = "Kopiuj link oferty do schowka";
$MESS["CRM_QUOTE_DETAILS_DELETE"] = "Usuń ofertę";
$MESS["CRM_QUOTE_DETAILS_DELETE_CONFIRMATION_MESSAGE"] = "Na pewno chcesz usunąć tę ofertę?";
$MESS["CRM_QUOTE_DETAILS_DELETE_CONFIRMATION_TITLE"] = "Usuń ofertę";
$MESS["CRM_QUOTE_DETAILS_EDITOR_MAIN_SECTION_TITLE"] = "O ofercie";
$MESS["CRM_QUOTE_DETAILS_JS_ERROR_NO_EMAIL_SETTINGS"] = "Nie znaleziono ustawień poczty e-mail";
$MESS["CRM_QUOTE_DETAILS_JS_ERROR_NO_TEMPLATES"] = "Nie znaleziono szablonów wydruku";
$MESS["CRM_QUOTE_DETAILS_JS_PRINT"] = "Drukuj";
$MESS["CRM_QUOTE_DETAILS_JS_SELECT_TEMPLATE"] = "Wybierz szablon";
$MESS["CRM_QUOTE_DETAILS_JS_TEMPLATE"] = "Szablon";
$MESS["CRM_QUOTE_DETAILS_MANUAL_OPPORTUNITY_CHANGE_MODE_TEXT"] = "Domyślnie kwota oferty jest obliczana jako suma cen produktów. Jednak oferta określa już inną kwotę. Możesz pozostawić bieżącą wartość niezmienioną lub potwierdzić ponowne obliczenie.";
$MESS["CRM_QUOTE_DETAILS_MANUAL_OPPORTUNITY_CHANGE_MODE_TITLE"] = "Zmień tryb obliczania kwoty oferty";
$MESS["CRM_QUOTE_DETAILS_PAGE_URL_COPIED"] = "Link oferty został skopiowany do schowka";
$MESS["CRM_QUOTE_DETAILS_SECTION_ADDITIONAL"] = "Więcej";
$MESS["CRM_QUOTE_DETAILS_SECTION_MAIN"] = "O ofercie";
$MESS["CRM_QUOTE_DETAILS_TIMELINE_HISTORY_STUB"] = "Tworzenie oferty...";
$MESS["CRM_QUOTE_DETAILS_TITLE"] = "Oferta ##QUOTE_NUMBER# z #BEGINDATE#";
$MESS["CRM_QUOTE_DETAILS_TITLE_COPY"] = "Kopiuj ofertę";
$MESS["CRM_QUOTE_DETAILS_TITLE_NEW"] = "Nowa oferta";
