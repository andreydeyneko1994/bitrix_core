<?
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Novo Orçamento";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "Erro ao excluir o orçamento.";
$MESS["CRM_QUOTE_NOT_FOUND"] = "O orçamento não foi encontrado.";
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Ocorreu um erro ao salvar produtos.";
?>