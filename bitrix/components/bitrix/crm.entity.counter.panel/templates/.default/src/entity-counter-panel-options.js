export type EntityCounterPanelOptions = {
	id: string,
	entityTypeId: number,
	userId: number,
	userName: string,
	serviceUrl: string,
	data: Array,
	codes: Array,
	extras: Object,
	filterLastPresetId: String,
	filterLastPresetData: Array
};
