<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> ohne Aktivitäten</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> mit überfälligen Aktivitäten</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> mit heutigen Aktivitäten</span></span>";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Aktivitäten";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "Ohne Aktivitäten";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Eingehende";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Überfällig";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "Für heute";
