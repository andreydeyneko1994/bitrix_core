<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">bez działań</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">z zaległymi działaniami</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">z dzisiejszymi działaniami</span></span>";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Aktywności";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "Brak działań";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Przychodzące";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Po terminie";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "Na dziś";
