<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">sem atividades</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">com atividades atrasadas</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">com atividades de hoje</span></span>";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Atividades";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "Nenhuma atividade";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Recebidos";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Atrasada";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "Para hoje";
