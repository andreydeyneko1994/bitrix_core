<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> без дел</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> с делами на сегодня</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\"> с просроченными делами</span></span>";

$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "Без дел";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Просрочены";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "На сегодня";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Дела";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Входящие";
