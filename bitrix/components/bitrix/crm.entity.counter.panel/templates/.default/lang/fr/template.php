<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">sans activités</span></span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">avec des activités en retard</span></span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class=\"crm-counter-inner\"><span class=\"crm-counter-number\">#VALUE#</span><span class=\"crm-counter-text\">avec des activités pour aujourd'hui</span></span>";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT"] = "Activités";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "Aucune activité";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "Entrant";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "Dépassé";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "Pour aujourd'hui";
