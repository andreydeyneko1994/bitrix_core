<?php
$MESS["CPD_CREATE_DOCUMENT_BUTTON"] = "Crear objeto del inventario";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_ADJUSTMENT"] = "Recibo de existencias";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_DEDUCT"] = "Cancelación";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_MOVING"] = "Transferir";
$MESS["CPD_CREATE_DOCUMENT_BUTTON_POPUP_SHIPMENT"] = "Orden de ventas";
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "No se pudo crear un diccionario con este nombre";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Agregar elementos a la lista del diccionario";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nuevo Producto";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "No se encontró el producto. Es probable que haya sido eliminado.";
$MESS["CPD_SETS_NOT_SUPPORTED_LINK"] = "Ir a la tienda en línea ahora";
$MESS["CPD_SETS_NOT_SUPPORTED_TITLE"] = "El tipo de producto del paquete seleccionado no es compatible con el nuevo formulario del producto. Puede ver y editar los conjuntos en el área de la Tienda en línea.<br>#LINK#";
