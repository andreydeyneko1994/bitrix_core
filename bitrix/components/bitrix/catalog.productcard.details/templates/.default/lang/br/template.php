<?php
$MESS["CPD_FEEDBACK_BUTTON"] = "Feedback";
$MESS["CPD_NEW_PROPERTY_ADDED"] = "A propriedade foi adicionada";
$MESS["CPD_NEW_VARIATION_ADDED"] = "A variante foi adicionada";
$MESS["CPD_SETTING_DISABLED"] = "A opção \"#NAME#\" está desativada";
$MESS["CPD_SETTING_ENABLED"] = "A opção \"#NAME#\" está ativada";
$MESS["CPD_TAB_BALANCE_TITLE"] = "Inventário";
$MESS["CPD_TAB_GENERAL_TITLE"] = "Geral";
