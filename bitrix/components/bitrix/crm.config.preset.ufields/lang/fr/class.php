<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Type de modèle incorrect";
$MESS["CRM_PRESET_UFIELDS_TITLE"] = "Champs personnalisés inutilisés";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Nom";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Nom";
$MESS["CRM_PRESET_UFIELD_FIELD_TYPE"] = "Type";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
?>