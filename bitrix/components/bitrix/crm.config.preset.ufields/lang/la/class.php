<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de plantilla incorrecto";
$MESS["CRM_PRESET_UFIELDS_TITLE"] = "Campos personalizados no utilizados";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Nombre";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Nombre";
$MESS["CRM_PRESET_UFIELD_FIELD_TYPE"] = "Tipo";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
?>