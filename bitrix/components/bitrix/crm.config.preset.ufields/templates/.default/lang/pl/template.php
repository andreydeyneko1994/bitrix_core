<?
$MESS["CRM_ALL"] = "Suma";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE"] = "Usuń";
$MESS["CRM_PRESET_UFIELDS_ACTION_MENU_DELETE_CONF"] = "Usunąć rekord?";
$MESS["CRM_PRESET_UFIELDS_NOTE"] = "Lista zawiera niestandardowe pola, które nie są używane przez żaden z szablonów. Możliwe, że te pola nie są już potrzebne. Usunięcie pola spowoduje również nieodwracalne usunięcie wszystkich powiązanych z nim danych.";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST"] = "Szablony";
$MESS["CRM_PRESET_UFIELDS_TOOLBAR_LIST_TITLE"] = "Szablony: #NAME#";
?>