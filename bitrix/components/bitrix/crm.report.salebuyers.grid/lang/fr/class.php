<?php
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_BUYER_TITLE"] = "Client";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_CONVERSION"] = "Conversion";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_ORDER_COUNT"] = "Nombre total de commandes";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_ORDER_WON_AMOUNT"] = "Montant";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_ORDER_WON_COUNT"] = "Réussi";
