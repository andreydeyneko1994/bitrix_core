<?php
$MESS["CT_BLL_TOOLBAR_ADD"] = "Ajouter";
$MESS["CT_BLL_TOOLBAR_ADD_DEFAULT"] = "Créer des workflows standards";
$MESS["CT_BLL_TOOLBAR_ADD_NEW"] = "Créer une nouvelle liste";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE"] = "Créer une nouvelle liste";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE_LIST"] = "Créer une nouvelle liste";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE_PROCESS"] = "Créer un nouveau flux de travail";
$MESS["CT_BLL_TOOLBAR_SHOW_LIVE_FEED"] = "Afficher dans le Flux d'activités";
$MESS["CT_BLL_TOOLBAR_SHOW_LIVE_FEED_NEW"] = "Afficher dans les Actualités";
$MESS["CT_BLL_TOOLBAR_TRANSITION_PROCESSES"] = "Catalogue des flux de travail";
