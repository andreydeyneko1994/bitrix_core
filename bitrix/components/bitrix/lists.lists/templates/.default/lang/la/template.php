<?php
$MESS["CT_BLL_TOOLBAR_ADD"] = "Agregar ";
$MESS["CT_BLL_TOOLBAR_ADD_DEFAULT"] = "Crear procesos de negocio estándar";
$MESS["CT_BLL_TOOLBAR_ADD_NEW"] = "Crear nuevo";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE"] = "Agregar nueva lista";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE_LIST"] = "Crear una nueva lista";
$MESS["CT_BLL_TOOLBAR_ADD_TITLE_PROCESS"] = "Crear un nuevo procesos de negocio";
$MESS["CT_BLL_TOOLBAR_SHOW_LIVE_FEED"] = "Mostrar en el noticias";
$MESS["CT_BLL_TOOLBAR_SHOW_LIVE_FEED_NEW"] = "Mostrar en el flujo de Noticias";
$MESS["CT_BLL_TOOLBAR_TRANSITION_PROCESSES"] = "Directorio de flujos de trabajo";
