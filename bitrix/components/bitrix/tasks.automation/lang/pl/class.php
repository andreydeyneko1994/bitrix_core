<?php
$MESS["TASKS_AUTOMATION_ACCESS_DENIED"] = "Odmowa dostępu do jednostki.";
$MESS["TASKS_AUTOMATION_GROUPS_CAPTION"] = "Projekty";
$MESS["TASKS_AUTOMATION_NOT_AVAILABLE"] = "Reguły automatyzacji są niedostępne";
$MESS["TASKS_AUTOMATION_TITLE"] = "Automatyzacja zadań";
