<?php
$MESS["TASKS_AUTOMATION_ACCESS_DENIED"] = "O acesso à entidade foi negado.";
$MESS["TASKS_AUTOMATION_GROUPS_CAPTION"] = "Projetos";
$MESS["TASKS_AUTOMATION_NOT_AVAILABLE"] = "As regras de automação estão indisponíveis";
$MESS["TASKS_AUTOMATION_TITLE"] = "Automação de tarefas";
