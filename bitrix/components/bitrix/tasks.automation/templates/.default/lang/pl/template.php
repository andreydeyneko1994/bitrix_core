<?
$MESS["TASKS_AUTOMATION_CMP_CHOOSE_GROUP"] = "Wybierz...";
$MESS["TASKS_AUTOMATION_CMP_ROBOT_HELP_TIP"] = "Reguła automatyzacji to sekwencja operacji wykonywanych automatycznie po przeniesieniu zadania do określonego statusu.
Reguły poprawiają wyniki biznesowe, zwiększają produktywność pracowników i usprawniają duże projekty.";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PERSONAL"] = "Statusy";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PLAN_1"] = "Terminarz";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PROJECTS"] = "Projekty";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT"] = "Edytuj reguły automatyzacji dla wszystkich zadań projektowych";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_PLAN_1"] = "Konfiguruj reguły automatyzacji dla wszystkich zadań z Terminarza";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_STATUSES"] = "Konfiguruj reguły automatyzacji dla wszystkich zadań, w które jesteś zaangażowany/-a";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_VIEW"] = "Bieżący status zadania: \"#TITLE#\"";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW"] = "Bieżące reguły automatyzacji używane przez zadania projektu";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_PLAN_1"] = "Bieżące reguły automatyzacji dla wszystkich zadań z Terminarza";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_STATUSES"] = "Bieżące reguły automatyzacji dla wszystkich zadań, w które jesteś zaangażowany/-a";
$MESS["TASKS_AUTOMATION_CMP_TRIGGER_HELP_TIP_2"] = "Wyzwalacz to dowolne działanie lub sytuacja, która powoduje przejście zadania do określonego statusu. W takim przypadku zostaje wykonana reguła automatyzacji (jeśli określono).";
?>