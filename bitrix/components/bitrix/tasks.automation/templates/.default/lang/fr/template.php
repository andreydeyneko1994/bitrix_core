<?
$MESS["TASKS_AUTOMATION_CMP_CHOOSE_GROUP"] = "Sélectionner...";
$MESS["TASKS_AUTOMATION_CMP_ROBOT_HELP_TIP"] = "Une règle d'automatisation est une séquence d'opérations à exécuter automatiquement quand une tâche est déplacée dans un statut spécifique. Les règles améliorent les performances de l'entreprise, améliorent la productivité des employés et fluidifient les projets de grande ampleur.";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PERSONAL"] = "Statuts";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PLAN_1"] = "Planificateur";
$MESS["TASKS_AUTOMATION_CMP_SELECTOR_ITEM_PROJECTS"] = "Projets";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT"] = "Éditer les règles d'automatisation pour toutes les tâches du projet";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_PLAN_1"] = "Configurez les règles d'automatisation pour toutes les tâches du Planificateur";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_EDIT_STATUSES"] = "Configurez les règles d'automatisation pour toutes les tâches dans lesquelles vous êtes impliqué(e)";
$MESS["TASKS_AUTOMATION_CMP_TITLE_TASK_VIEW"] = "Statut de la tâche actuelle : \"#TITLE#\"";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW"] = "Règles d'automatisation actuelles utilisées par les tâches du projet";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_PLAN_1"] = "Règles d'automatisation actuelles pour toutes les tâches du Planificateur";
$MESS["TASKS_AUTOMATION_CMP_TITLE_VIEW_STATUSES"] = "Règles d'automatisation actuelles pour toutes les tâches dans lesquelles vous êtes impliqué(e)";
$MESS["TASKS_AUTOMATION_CMP_TRIGGER_HELP_TIP_2"] = "Un déclencheur est une action ou une situation qui entraînera le déplacement d'une tâche vers un statut spécifique. Une fois le déplacement déclenché, une règle d'automatisation, s'il en existe une, est exécutée.";
?>