<?
$MESS["CRM_ORDER_NOT_FOUND"] = "Nie znaleziono zamówienia";
$MESS["CRM_PRODUCT_CURRENCY_EMPTY"] = "Nie określono waluty produktu";
$MESS["CRM_PRODUCT_NAME_EMPTY"] = "Brak nazwy produktu";
$MESS["CRM_PRODUCT_PRICE_EMPTY"] = "Cena jest wymagana";
$MESS["CRM_PRODUCT_QUANTITY_EMPTY"] = "Brak liczby produktów";
?>