<?
$MESS["CRM_ORDER_NOT_FOUND"] = "La commande est introuvable";
$MESS["CRM_PRODUCT_CURRENCY_EMPTY"] = "La devise du produit n'est pas précisée";
$MESS["CRM_PRODUCT_NAME_EMPTY"] = "Le nom du produit manque";
$MESS["CRM_PRODUCT_PRICE_EMPTY"] = "Le prix est obligatoire";
$MESS["CRM_PRODUCT_QUANTITY_EMPTY"] = "La quantité du produit manque";
?>