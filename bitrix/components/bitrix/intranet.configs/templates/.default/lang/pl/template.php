<?php
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Piątek";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Poniedziałek";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Sobota";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Niedziela";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Czwartek";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Wtorek";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Środa";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Załaduj logo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Usuń logo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "Czy jesteś pewien że chcesz usunąć logo?";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Pozwól każdemu na zaproszenie nowych użytkowników do Bitrix24";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Publikuj powiadomienia o nowych pracownikach na Aktualności";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Pozwól na szybką rejestrację";
$MESS["CONFIG_ALLOW_TOALL"] = "Pozwól na opcję \"Wszyscy użytkownicy\" na tablicy aktywności";
$MESS["CONFIG_BUY_TARIFF_BY_ALL"] = "Zezwól wszystkim użytkownikom na aktualizację planu Bitrix24";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo firmy";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Twoje logo musi być w pliku <b>PNG</b>.<br/>Maksymalne rozmiary to 222px x 55px.";
$MESS["CONFIG_CLIENT_LOGO_DESC_RETINA"] = "Twoje logo musi być w pliku <b>PNG</b>.<br/>Maksymalne rozmiary to 444px x 110px.";
$MESS["CONFIG_CLIENT_LOGO_RETINA"] = "Logo wysokiej rozdzielczości (siatkówka)";
$MESS["CONFIG_COLLECT_GEO_DATA"] = "Włącz zbieranie danych geolokalizacyjnych";
$MESS["CONFIG_COLLECT_GEO_DATA_CONFIRM"] = "Pamiętaj, że w niektórych jurysdykcjach wymagana jest wyraźna zgoda użytkownika na przetwarzanie danych geolokalizacyjnych urządzenia";
$MESS["CONFIG_COLLECT_GEO_DATA_OK"] = "Akceptuj";
$MESS["CONFIG_COMPANY_NAME"] = "Nazwa firmy";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nazwa firmy wyświetlana w nagłówku";
$MESS["CONFIG_CREATE_OVERDUE_CHATS"] = "Utwórz czat o zaległym zadaniu dla wszystkich uczestników zadania";
$MESS["CONFIG_CULTURE_OTHER"] = "Inne";
$MESS["CONFIG_DATE_FORMAT"] = "Format daty";
$MESS["CONFIG_DEFAULT_TOALL"] = "Użyj opcji \"Wszyscy użytkownicy\" jako domyślnego odbiorcy";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Generuj automatycznie pliki PDF i JPG dla dokumentów";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Wszyscy użytkownicy mogą edytować dokumenty dołączone do dyskusji, zadań, <br/>komentarzy lub innych zdarzeń. To domyślne zachowanie może być zmienione <br/>w każdym zdarzeniu indywidualnie.";
$MESS["CONFIG_DISK_ALLOW_USE_EXTENDED_FULLTEXT"] = "Wyszukaj dokumenty na Dysku";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Pozwól na linki publiczne";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Generuj automatycznie pliki MP4 i JPG dla plików wideo";
$MESS["CONFIG_DISK_EXTENDED_FULLTEXT_INFO"] = "Wyszukiwanie jest tymczasowo niedostępne. Skontaktuj się z działem pomocy technicznej, aby włączyć wyszukiwanie.";
$MESS["CONFIG_DISK_LIMIT_HISTORY_LOCK_POPUP_TEXT"] = "Nieograniczona historia dokumentów jest dostępna w <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planach płatnych</a>.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Wykorzystaj jeszcze więcej użytecznych funkcji z Zaawansowanym Dyskiem Bitrix24:<br/><br/>
+ Historia zmian w dokumentach (odnotowane modyfikacje: kto i kiedy ich dokonał)<br/>
+ Przywrócenie którejkolwiek z poprzednich wersji<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Dowiedz się więcej</a><br/><br/>
Dysk zaawansowany dostępny jest w wersji Standard lub wyższej.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Dostępne wyłącznie w wersji zaawansowanej Dysku Bitrix24.";
$MESS["CONFIG_DISK_LOCK_EXTENDED_FULLTEXT_POPUP_TEXT"] = "Dostępne na wybranych planach komercyjnych";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Funkcjonalności zaawansowanego Dysku:

<br/><br/>
- Blokowanie plików, nie pozwalające innym na edycję dokumentów, nad którymi aktualnie pracujesz.
<br/><br/>
- Wyłączenie linków publicznych, nie pozwalające na udostępnienie plików poza Bitrix24
<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Dowiedz się więcej</a>
<br/><br/>
Zaawansowany Dysk jest dostępny w komercyjnej licencji, zaczynając od wersji Bitrix24 Standard plan.
";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Dostępne jedynie w zaawansowanym Dysku Bitrix24";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Pozwól na blokowanie dokumentów";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Konwertuj plik zaraz po otwarciu";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Maks. liczba wpisów w historii dokumentu";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Używanie podglądu dokumentów";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail administratora strony<br>(domyślny adres wysyłania)";
$MESS["CONFIG_EXAMPLE"] = "Przykład";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Ekstranet";
$MESS["CONFIG_FEATURES_LISTS"] = "Listy";
$MESS["CONFIG_FEATURES_MEETING"] = "Spotkania i odprawy";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Administracyjne Workflow";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Zarządzanie czasem i raportami pracy";
$MESS["CONFIG_FEATURES_TITLE"] = "Usługi";
$MESS["CONFIG_GDRP_APP1"] = "GDPR dla pracowników";
$MESS["CONFIG_GDRP_APP2"] = "GDPR dla CRM";
$MESS["CONFIG_GDRP_LABEL1"] = "Aktualizacje produktów, oferty specjalne, zaproszenia na seminaria internetowe, <br>subskrypcje biuletynów";
$MESS["CONFIG_GDRP_LABEL2"] = "Materiały szkoleniowe";
$MESS["CONFIG_GDRP_LABEL3"] = "Akceptuję Umowę dotyczącą przetwarzania danych";
$MESS["CONFIG_GDRP_LABEL4"] = "Nazwa prawna klienta*";
$MESS["CONFIG_GDRP_LABEL5"] = "Nazwa głównej osoby kontaktowej*";
$MESS["CONFIG_GDRP_LABEL6"] = "Tytuł*";
$MESS["CONFIG_GDRP_LABEL7"] = "Data*";
$MESS["CONFIG_GDRP_LABEL8"] = "Adres e-mail do powiadomień*";
$MESS["CONFIG_GDRP_LABEL11"] = "(zgodnie z <a href=\"https://www.bitrix24.ru/about/advertising.php\" target=\"_blank\">Umową</a>)";
$MESS["CONFIG_GDRP_TITLE1"] = "Wyrażam zgodę na otrzymywanie wiadomości e-mail od Bitrix24 zawierających:";
$MESS["CONFIG_GDRP_TITLE2"] = "Jeśli akceptujesz umowę dotyczącą przetwarzania danych, musisz również podać następujące informacje:";
$MESS["CONFIG_GDRP_TITLE3"] = "Aplikacje do przetwarzania danych osobowych:";
$MESS["CONFIG_HEADER_GDRP"] = "Zgodność z RODO";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Ustawienia bezpieczeństwa";
$MESS["CONFIG_HEADER_SETTINGS"] = "Ustawienia";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Pozwól użytkownikom wysyłać wiadomości na czat ogólny.";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_ADMIN_RIGHTS"] = "Włącz automatyczne powiadomienie o wyznaczeniu i zmianie administratora <br>na czacie ogólnym";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Powiadom o nowych pracownikach na czacie ogólnym";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Powiadom o zwolnieniu pracowników na czacie generalnym";
$MESS["CONFIG_IP_HELP_TEXT2"] = "Użytkownicy bez uprawnień administratora nie uzyskają dostępu do tego Bitrix24 z dowolnego adresu IP, który nie jest zgodny z określonym zakresem. Po autoryzacji użytkownik zostanie przekierowany na stronę błędu z powiadomieniem, że dostęp z tego adresu IP jest zabroniony. Możesz śledzić próby wprowadzenia Bitrix24 z innych adresów IP w Dzienniku zdarzeń.";
$MESS["CONFIG_IP_TITLE"] = "Zastrzeżenie IP (pozwala na dostęp tylko z określonego adresu IP albo zakresu adresów. Przykład: 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_LIMIT_MAX_TIME_IN_DOCUMENT_HISTORY"] = "Wersje dokumentów są przechowywane przez #NUM# dni.";
$MESS["CONFIG_LOCATION_ADDRESS_FORMAT"] = "Format adresu";
$MESS["CONFIG_LOCATION_DEFAULT_SOURCE"] = "Domyślne źródło";
$MESS["CONFIG_LOCATION_SOURCES_SETTINGS"] = "Ustawienia źródła adresu i lokalizacji";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_BACKEND"] = "Klucz serwerowy Google Places API i Geocoding API";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "Klucz przeglądarki Google Maps JavaScript API, Places API i Geocoding API";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_NOTE"] = "Do używania map wymagany jest klucz Google API. Aby otrzymać klucz, <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">użyj formularza</a>.";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "Pokaż zdjęcia Google do widoku mapy Twoich miejsc (za tę opcję Google może naliczyć dodatkową opłatę)";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "Dla nieznanych adresów użyj usługi geokodowania (za tę opcję Google może naliczyć dodatkową opłatę)";
$MESS["CONFIG_LOCATION_SOURCE_OSM_SERVICE_URL"] = "Adres URL usługi";
$MESS["CONFIG_LOGO_24"] = "Dodaj \"24\" do loga firmy";
$MESS["CONFIG_MARKETPLACE_MORE"] = "Dowiedz się więcej";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migruj dane do Bitrix24 z innych systemów";
$MESS["CONFIG_MORE"] = "Czytaj więcej";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL"] = "Zezwól użytkownikom na instalowanie aplikacji z Marketplace24";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL1"] = "Zezwalaj użytkownikom na instalowanie aplikacji";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Zmień";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Uwaga: możesz zmienić swój adres Bitrix24 tylko raz.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Zmiana adresu Bitrix24";
$MESS["CONFIG_NAME_CURRENT_MAP_PROVIDER"] = "Bieżący dostawca map";
$MESS["CONFIG_NAME_FILEMAN_GOOGLE_API_KEY"] = "Klucz Google Maps określony w ustawieniach modułu Eksplorera Strony";
$MESS["CONFIG_NAME_FORMAT"] = "Format nazwy";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "Klucz został uzyskany dla domeny <b>#domain#</b>. Jeśli nie uda Ci się uruchomić Google Maps, zmień ustawienia klucza lub <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">zdobądź nowy</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Preferencje integracji Google API";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "Klucz integracji Google Maps API dla Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD2"] = "Klucz przeglądarki Google Maps JavaScript API, Places API i Geocoding API";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "Klucz Google API jest wymagany do używania map. Aby otrzymać swój klucz, proszę <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">użyć formularza</a>.";
$MESS["CONFIG_NAME_MAP_PROVIDER_SETTINGS"] = "Ustawienia dostawcy #PROVIDER#";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Zezwól użytkownikom mojego Bitrix24 komunikować się w globalnej sieci Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Ta funkcja będzie dostępna po zatwierdzeniu konta przez administratora.";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "Komunikacja w Bitrix24.Network jest dostępna wyłącznie dla komercyjnych użytkowników Bitrix24.<br/><br/>
Oto zalety podłączania Bitrix24.Network:<br/>
<ul>
<li>Wszystkie kontakty i partnerzy biznesowi są połączeni w jednej sieci</li>
<li>Szybka i wygodna komunikacja z klientami i użytkownikami zewnętrznymi</li>
<li>Bezproblemowa komunikacja między użytkownikami z różnych kont Bitrix24</li>
</ul>
<b>Wszystkie narzędzia Bitrix24.Network dostępne są od planu Bitrix24 Plus za #PRICE# miesięcznie.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Dostępne tylko na komercyjnych planach Bitrix24";
$MESS["CONFIG_ORGANIZATION"] = "Rodzaj twojej organizacji";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Firma i pracownicy";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Organizacja i pracownicy";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organizacja i użytkownicy";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Zanim migrujesz swoich pracowników do dwustopniowego uwierzytelnienia, ustaw je najpierw na swoim koncie.<br/><br/>Możesz je włączyć na stronie swojego profilu.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Obecnie używasz adresu e-mail i hasła aby zalogować się do Bitrix24. Dane biznesowe i osobiste są chronione przez technologię szyfrowania danych. Niezależnie od tego są narzędzia i osoby, które mogą się włamać do twojego komputera i skraść dane logowania.

Szukając ochrony przed podobnymi atakami daliśmy możliwość dodatkowego zabezpieczenia Bitrix24: dwustopniowe uwierzytelnienie.

Dwustopniowe uwierzytelnienie jest specjalną metodą do ochrony przed oprogramowaniem hakerskim i kradzieżą haseł. Podczas każdego logowania do systemu, będziesz musiał przejść dwa poziomy weryfikacji. Najpierw wprowadzisz swój adres e-mail i hasło, potem będziesz musiał wprowadzić jednorazowy kod, otrzymany na swoje urządzenie mobilne.

Dzięki temu twoje dane biznesowe będą bezpieczne, nawet jeśli zostaną skradzione hasła wszystkich pracowników.

Masz 5 dni na włączenie tej funkcjonalności.

Aby skonfigurować nową procedurę uwierzytelniania, przejdź do swojego profilu i wybierz stronę \"Ustawienia bezpieczeństwa\".

Jeśli nie posiadasz przenośnego urządzenia mobilnego aby uruchomić aplikację, lub masz jakikolwiek inny problem techniczny, daj nam znać pisząc komentarz pod tym postem.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Dwustopniowe uwierzytelnienie";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "Nie, dziękuję";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Udostępnij";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "To jest tekst, który możesz opublikować na tablicy<br/>do przeczytania dla swoich pracowników.<br/><br/>
Niech twoi współpracownicy dowiedzą się o dwustopniowym uwierzytelnieniu,<br/>procedurze konfiguracji i nowej metodzie uwierzytelnienia<br/>którą będą musieli użyć aby zalogować się do Bitrix24.";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "Co powiesz na publikację tego na tablicy aktywności?";
$MESS["CONFIG_OTP_SECURITY"] = "Włącz dwustopniowe uwierzytelnianie dla wszystkich pracowników";
$MESS["CONFIG_OTP_SECURITY2"] = "Włącz dwustopniowe uwierzytelnianie obowiązkowe dla wszystkich pracowników";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Określ czas, który mają wszyscy użytkownicy<br/>na włączenie dwustopniowego uwierzytelniania.";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Wymyśliliśmy przyjazną dla użytkownika procedurę dwustopniowego uwierzytelniania, którą może używać każdy pracownik bez fachowej pomocy<br/><br/> Do wszystkich pracowników zostanie wysłana wiadomość powiadamiająca ich że mają określony czas na włączenie dwustopniowego uwierzytelniania. Użytkownicy, którzy tego nie zrobią nie będą mogli się już więcej zalogować.";
$MESS["CONFIG_OTP_SECURITY_INFO_1"] = "<br/>Aby włączyć dwustopniowe uwierzytelnienie użytkownik musi zainstalować aplikację Bitrix24 OTP. Ta aplikacja może być pobrana z AppStore lub GooglePlay.<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_2"] = "Pracownicy, którzy nie posiadają urządzenia mobilnego muszą się wyposażyć w specjalne urządzenie - eToken Pass. Jest wielu dostawców tego urządzenia, na przykład:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>, 
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Innych możesz znaleźć na <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_3"] = "Ewentualnie możesz wyłączyć dwustopniowe uwierzytelnienie dla niektórych pracowników. To jednak zwiększa ryzyko nieautoryzowanego dostępu do bitrux24 jeśli ich hasła zostaną skradzione. Jako administrator możesz wyłączyć dwustopniowe uwierzytelnienie pracownikom w ich profilu.";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Odznaczenie tej opcji nie wyłączy używania jednorazowego hasła przez użytkowników, którzy mają włączone dwustopniowe uwierzytelnianie. 
<br/><br/>Wciąż będą musieli używać OTP podczas logowania do Bitrix24.
<br/><br/>Możesz wyłączyć dwustopniowe uwierzytelnianie w profilu użytkownika.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Format numeru telefonu: domyślny kraj";
$MESS["CONFIG_SAVE"] = "Zapisz";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "Ustawienia zostały zaktualizowane prawidłowo";
$MESS["CONFIG_SEND_OTP_PUSH"] = "Wyślij powiadomienie z kodem autoryzacyjnym do komunikatora internetowego <br>(tylko dla opartego na czasie OTP)";
$MESS["CONFIG_SHOW_FIRED_EMPLOYEES"] = "Pokaż zwolnionych pracowników";
$MESS["CONFIG_SHOW_YEAR"] = "Pokaż rok urodzenia w profilu użytkownika";
$MESS["CONFIG_SHOW_YEAR_FOR_FEMALE"] = "Wyświetl datę urodzenia w profilach kobiet";
$MESS["CONFIG_STRESSLEVEL_AVAILABLE"] = "Pozwala mierzyć i wyświetlać poziom stresu w profilu użytkownika";
$MESS["CONFIG_TIME_FORMAT"] = "Format czasu";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 godzinny";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 godzinny";
$MESS["CONFIG_TOALL_DEL"] = "usuń";
$MESS["CONFIG_TOALL_RIGHTS"] = "Ustawienia dla opcji \"Wszyscy pracownicy\"";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Dodaj";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK"] = "Śledź kliknięcia linków w wychodzących wiadomościach e-mail";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK_HINT"] = "Ta opcja jest dostępna dla wiadomości e-mail wysyłanych z CRM, CRM Marketing, Bitrix24 Email, reguł automatyzacji oraz umożliwia pracę z wyzwalaczem „Odczytywanie wiadomości e-mail”.";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ"] = "Śledź status odczytu wychodzących wiadomości e-mail";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ_HINT"] = "System śledzi odczyt wiadomości wychodzących przez odpowiednich odbiorców i oznacza je jako przeczytane.";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Włącz linki rich media";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Automatycznie podłącz dysk grupy kiedy<br/>użytkownik dołącza do grupy";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Włącz edytowanie dokumentów przez zewnętrzne<br/>usługi (Google Docs, MS Office Online i inne)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Pozwól indywidualnym użytkownikom i grupom na aktywowanie<br/>edycji dokumentów przez zewnętrzne usługi";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Dni wolne od pracy";
$MESS["CONFIG_WEEK_START"] = "Pierwszy dzień tygodnia";
$MESS["CONFIG_WORK_TIME"] = "Parametry czasu pracy";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Weekendy i święta";
$MESS["DAY_OF_WEEK_0"] = "Niedziela";
$MESS["DAY_OF_WEEK_1"] = "Poniedziałek";
$MESS["DAY_OF_WEEK_2"] = "Wtorek";
$MESS["DAY_OF_WEEK_3"] = "Środa";
$MESS["DAY_OF_WEEK_4"] = "Czwartek";
$MESS["DAY_OF_WEEK_5"] = "Piątek";
$MESS["DAY_OF_WEEK_6"] = "Sobota";
$MESS["config_rating_label_likeN"] = "Tekst nienaciśniętego przycisku \"Lubię\"";
$MESS["config_rating_label_likeY"] = "Tekst naciśniętego przycisku \"Lubię\"";
