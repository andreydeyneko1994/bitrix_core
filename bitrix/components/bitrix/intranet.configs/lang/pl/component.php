<?
$MESS["CONFIG_ACCESS_DENIED"] = "Brak dostępu";
$MESS["CONFIG_EMAIL_ERROR"] = "E-mail administratora strony jest nieprawidłowy.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "Format nazwy jest nieprawidłowy. Możliwe są następujące makra: #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["CONFIG_GDRP_EMAIL_ERROR"] = "Podano nieprawidłowy adres e-mail użytkownika.";
$MESS["CONFIG_GDRP_EMPTY_ERROR"] = "Proszę wypełnić wszystkie wymagane pola.";
$MESS["CONFIG_IP_ERROR"] = "Adres IP jest nieprawidłowy.";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "Podane hasła serwera SMTP nie są zgodne.";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Nieograniczone";
?>