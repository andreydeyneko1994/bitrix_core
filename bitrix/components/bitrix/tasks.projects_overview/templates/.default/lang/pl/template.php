<?
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_0"] = "Przełożony";
$MESS["TASKS_PROJECTS_OVERVIEW_HEADS_1"] = "Przełożeni";
$MESS["TASKS_PROJECT_OVERVIEW_ADD_PROJECT"] = "Dodaj projekt";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_0"] = "i <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# innych uczestników</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_1"] = "i <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# innych uczestników</a>";
$MESS["TASKS_PROJECT_OVERVIEW_MEMBERS_COUNT_PLURAL_2"] = "i <a href=\"javascript:;\" id=\"#ID#\" data-group-id=\"#GROUP_ID#\" class=\"js-id-projects-overview-members-list\">#COUNT# innych uczestników</a>";
$MESS["TASKS_TITLE_PROJECTS_OVERVIEW"] = "Projekty";
?>