<?
$MESS["CRM_ADS_RTG_ADD_AUDIENCE"] = "Criar público";
$MESS["CRM_ADS_RTG_APPLY"] = "Executar";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_DAYS"] = "dias";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE"] = "remover do público em";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE_YANDEX"] = "remover dos segmentos em";
$MESS["CRM_ADS_RTG_CABINET_FACEBOOK"] = "Conta de publicidade no Facebook";
$MESS["CRM_ADS_RTG_CABINET_GOOGLE"] = "Conta de publicidade Google AdWords";
$MESS["CRM_ADS_RTG_CABINET_VKONTAKTE"] = "Conta de publicidade VK";
$MESS["CRM_ADS_RTG_CABINET_YANDEX"] = "Conta de publicidade Yandex.Audience";
$MESS["CRM_ADS_RTG_CANCEL"] = "Cancelar";
$MESS["CRM_ADS_RTG_CLOSE"] = "Fechar";
$MESS["CRM_ADS_RTG_ERROR_ACTION"] = "A ação foi cancelada porque ocorreu um erro.";
$MESS["CRM_ADS_RTG_ERROR_NO_AUDIENCES"] = "Não foi encontrado nenhum público. Por favor, acesse %name% para criar um público.";
$MESS["CRM_ADS_RTG_LOGIN"] = "Conectar";
$MESS["CRM_ADS_RTG_LOGOUT"] = "Desconectar";
$MESS["CRM_ADS_RTG_REFRESH"] = "Atualização";
$MESS["CRM_ADS_RTG_REFRESH_TEXT"] = "Atualize o público-alvo.";
$MESS["CRM_ADS_RTG_REFRESH_TEXT_YANDEX"] = "Atualize os segmentos-alvo.";
$MESS["CRM_ADS_RTG_SELECT_ACCOUNT"] = "Selecionar conta de publicidade";
$MESS["CRM_ADS_RTG_SELECT_AUDIENCE"] = "Adicionar ao público";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA"] = "Adicionar telefone e e-mail aos segmentos";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_EMAIL"] = "E-mail";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_PHONE"] = "Telefone";
$MESS["CRM_ADS_RTG_TITLE"] = "Configurar Público-alvo";
?>