<?
$MESS["DOCGEN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Utwórz nową rolę";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "Nie znaleziono roli.";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Edytuj rolę #ROLE#";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_PANEL"] = "Uprawnienia pracowników do jednostek CRM są dostępne w planach płatnych.";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TEXT"] = "<div class=\"docgen-permissions-feature-popup\">W ramach bieżącego planu wszyscy pracownicy mają takie same uprawnienia. Rozważ przejście na jeden z planów płatnych, aby przypisywać różne role, działania i dane różnym użytkownikom.<br /><br /> Aby dowiedzieć się więcej o różnych planach, przejdź do <a target=\"_blank\" href=\"/settings/license_all.php\">strony porównania planów</a>.</div>";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TITLE"] = "Przypisz uprawnienia dostępu";
$MESS["DOCGEN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "Nie masz uprawnień do edycji uprawnień dostępu";
$MESS["DOCGEN_SETTINGS_PERMS_TITLE"] = "Skonfiguruj uprawnienia dostępu";
$MESS["DOCGEN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(nieznany ID dostępu)";
?>