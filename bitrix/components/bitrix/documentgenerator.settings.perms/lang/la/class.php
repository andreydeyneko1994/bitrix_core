<?
$MESS["DOCGEN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Crear un nuevo rol";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "No se encontró el rol.";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Editar el rol #ROL#";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_PANEL"] = "Los permisos de los empleados para las entidades CRM están disponibles en los planes comerciales.";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TEXT"] = "<div class=\"docgen-permissions-feature-popup\">Todos los empleados comparten los mismos permisos en su plan actual. Puede actualizar a alguno de los planes comerciales para asignar diferentes roles, acciones y datos a varios usuarios.<br /><br /> Para obtener más información sobre los diferentes planes, visite la <a target=\"_blank\" href=\"/settings/license_all.php\">página de comparación de planes</a>.</div>";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TITLE"] = "Asignar permisos de acceso";
$MESS["DOCGEN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "No tiene autorización para editar permisos de acceso";
$MESS["DOCGEN_SETTINGS_PERMS_TITLE"] = "Configurar los permisos de acceso";
$MESS["DOCGEN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(ID de acceso desconocida)";
?>