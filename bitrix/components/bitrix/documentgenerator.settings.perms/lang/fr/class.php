<?
$MESS["DOCGEN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Créer un nouveau rôle";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "Rôle introuvable.";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Éditer le rôle #ROLE#";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_PANEL"] = "Les permissions des employés pour les entités CRM sont disponibles avec les offres commerciales.";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TEXT"] = "<div class=\"docgen-permissions-feature-popup\">Avec votre offre en cours, tous les employés partagent les mêmes permissions. Vous devez passer à une offre commerciale pour pouvoir affecter différents rôles, actions et données à divers utilisateurs<br /><br /> Pour plus d'informations sur les différentes offres, veuillez consulter la <a target=\"_blank\" href=\"/settings/license_all.php\">page de comparaison des offres</a>.</div>";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TITLE"] = "Assigner des autorisations d'accès";
$MESS["DOCGEN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "Vous n'êtes pas autorisés à éditer les permissions d'accès";
$MESS["DOCGEN_SETTINGS_PERMS_TITLE"] = "Configurer les permissions d'accès";
$MESS["DOCGEN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(ID d'accès inconnu)";
?>