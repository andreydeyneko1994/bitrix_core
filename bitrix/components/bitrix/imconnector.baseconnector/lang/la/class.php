<?
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_CONNECTOR_ERROR_STATUS"] = "Hubo un error. Por favor revise sus preferencias";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
?>