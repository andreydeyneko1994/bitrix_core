<?php
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_DELETE"] = "Eliminar";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_TERMINATE"] = "Parar";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_DELETE"] = "¿Seguro que desea eliminar el script que está en ejecución?";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_TERMINATE"] = "¿Seguro que desea detener el script?";
