<?php
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_DELETE"] = "Usuń";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_BTN_TERMINATE"] = "zatrzymaj";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_DELETE"] = "Czy na pewno chcesz usunąć uruchomiony skrypt?";
$MESS["BIZPROC_SCRIPT_QUEUE_LIST_CONFIRM_TERMINATE"] = "Czy na pewno chcesz zatrzymać skrypt?";
