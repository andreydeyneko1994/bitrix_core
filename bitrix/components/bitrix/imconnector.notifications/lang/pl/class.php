<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Pliki, które próbujesz przesłać, są nieprawidłowe. Obsługiwane są wyłącznie pliki JPG i PNG.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE"] = "Błąd podczas aktywacji konektora";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_SAVE"] = "Błąd podczas zapisywania danych. Sprawdź wprowadzone dane i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_TOS_AGREE"] = "Zgadzam się";
