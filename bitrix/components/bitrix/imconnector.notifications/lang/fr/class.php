<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Le ou les fichiers que vous essayez de télécharger ne sont pas valides. Seuls les fichiers JPG et PNG sont pris en charge.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE"] = "Erreur d'activation du connecteur";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_NO_SAVE"] = "Erreur d'enregistrement des données. Veuillez vérifier votre entrée et réessayer.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_OK_SAVE"] = "Les informations ont bien été enregistrées.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_TOS_AGREE"] = "J'accepte";
