<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_SUBTITLE_VIRTUAL_WHATSAPP"] = "Rozmawiaj z klientami zgodnie z ich preferencjami. Jeśli klient uzna WhatsApp za wygodniejszy niż inne środki komunikacji, korzystaj z WhatsApp z Bitrix24 i odpowiadaj natychmiast. ";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_TITLE_VIRTUAL_WHATSAPP"] = "Wysyłaj zautomatyzowane wiadomości do klientów za pośrednictwem WhatsApp lub wiadomości SMS";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_HEADER"] = "Rozmawiaj z klientami za pośrednictwem Instant WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DESCRIPTION"] = "Rozmawiaj z klientami zgodnie z ich preferencjami. Jeśli klient uzna WhatsApp za wygodniejszy niż inne środki komunikacji, korzystaj z WhatsApp z Bitrix24 i odpowiadaj natychmiast.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_DETAILS"] = "Szczegóły";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_1"] = "Połącz się teraz, nie trzeba rejestrować swojej firmy w WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_2"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_3"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_4"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_LIST_ITEM_5"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_DESCRIPTION"] = "Klient użyje widżetu na Twojej stronie, aby otworzyć WhatsApp na swoim telefonie lub komputerze. Pole wprowadzania wiadomości będzie zawierało identyfikator przedstawiciela handlowego przypisanego do komunikacji z klientem. Gdy tylko klient wyśle wiadomość, w formularzu deala pojawi się okno czatu na żywo, aby móc komunikować się z klientem w Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SECOND_TITLE"] = "Jak to działa?";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_SUBTITLE"] = "Rozmawiaj z klientami zgodnie z ich preferencjami. Jeśli klient uzna WhatsApp za wygodniejszy niż inne środki komunikacji, korzystaj z WhatsApp z Bitrix24 i odpowiadaj natychmiast.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_VIRTUAL_WHATSAPP_INDEX_TITLE"] = "Rozmawiaj z klientami za pośrednictwem Instant WhatsApp";
