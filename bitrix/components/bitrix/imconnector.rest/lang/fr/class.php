<?
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Il y a eu une erreur. Veuillez vérifier vos préférences.";
$MESS["IMCONNECTOR_COMPONENT_REST_MODULE_NOT_INSTALLED"] = "Le module \"External IM Connectors\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_REST_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_REST_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
?>