<?
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Hubo un error. Por favor revise sus preferencias.";
$MESS["IMCONNECTOR_COMPONENT_REST_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\"  no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_REST_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_REST_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
?>