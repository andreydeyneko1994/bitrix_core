<?
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Houve um erro. Verifique suas preferências.";
$MESS["IMCONNECTOR_COMPONENT_REST_MODULE_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado.";
$MESS["IMCONNECTOR_COMPONENT_REST_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_REST_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
?>