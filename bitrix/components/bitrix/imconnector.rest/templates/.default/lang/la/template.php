<?php
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Se ha producido un error al utilizar el conector. Por favor, conecte de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Use el conector con su Canal Abierto para que los mensajes de sus clientes se publiquen en el chat de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_SUBTITLE"] = "Use el conector con su Canal Abierto para que los mensajes de sus clientes se publiquen en el chat de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Aprenda más en la página de configuración";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "El canal se ha configurado correctamente";
