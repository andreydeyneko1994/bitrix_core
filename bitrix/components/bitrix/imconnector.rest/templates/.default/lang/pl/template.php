<?php
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Połącz";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd podczas używania konektora. Połącz się ponownie.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Użyj konektora na swoim Otwartym kanale, aby wiadomości od klientów były publikowane na czacie Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_SUBTITLE"] = "Użyj konektora na Otwartym kanale, aby wiadomości od klientów były publikowane na czacie Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Dowiedz się więcej na stronie ustawień";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "Kanał został skonfigurowany";
