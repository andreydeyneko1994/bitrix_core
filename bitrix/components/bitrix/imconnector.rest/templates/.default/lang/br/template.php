<?php
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "Houve um erro ao usar o conector. Conecte-se novamente.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "Use o conector com seu Canal Aberto para que as mensagens de seus clientes sejam postadas no bate-papo do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_SUBTITLE"] = "Use o conector com seu Canal Aberto para que as mensagens de seus clientes sejam postadas no bate-papo do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "Saiba mais na página de configurações";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "O canal foi configurado com sucesso";
