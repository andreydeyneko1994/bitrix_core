<?php
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_BAD"] = "Mauvaise";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_CLIENT_TITLE"] = "Client";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_CLIENT_TITLECRM_REPORT_REGULAR_CUSTOMERS_GRID_DEAL_WON_COUNT"] = "Conclues";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_CONVERSION"] = "Conversion";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_DEAL_WON_AMOUNT"] = "Montant";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_EXCELLENT"] = "Excellente";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_GOOD"] = "Bonne";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_GRID_NOT_BAD"] = "Moyenne";
