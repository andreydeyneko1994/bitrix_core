<?
$MESS["CRM_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_TYPE_CONTACT"] = "Contact";
$MESS["CRM_TYPE_CREATION_SCENARIO"] = "Mode de création de transactions";
$MESS["CRM_TYPE_CREATION_SCENARIO_DESC"] = "Chaque nouvelle demande entrante sera enregistrée en tant que nouvelle transaction. Un contact ou une entreprise sera également créé automatiquement, selon vos préférences. Vous pouvez également sélectionner un pipeline pour ajouter ces nouvelles transactions.";
$MESS["CRM_TYPE_DEAL_CREATE_AND"] = "Une transaction sera créée et…";
$MESS["CRM_TYPE_DEAL_DIRECTION"] = "Pipeline de transactions";
$MESS["CRM_TYPE_NOT_CLOSE_DEAL"] = "Ne fermez pas les activités après la conversion";
$MESS["CRM_TYPE_SETTINGS"] = "Paramètres";
?>