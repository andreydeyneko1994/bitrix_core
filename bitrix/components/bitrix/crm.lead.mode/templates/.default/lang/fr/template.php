<?
$MESS["CRM_DEAL_SETTINGS"] = "Vous pouvez configurer le mode #LINK_START# de création de transactions #LINK_END#";
$MESS["CRM_RIGTHS_INFO"] = "Veuillez contacter votre administrateur pour changer le mode CRM";
$MESS["CRM_TYPE_CANCEL"] = "Annuler";
$MESS["CRM_TYPE_CHANGE"] = "Vous pouvez modifier votre préférence à tout moment dans les paramètres CRM";
$MESS["CRM_TYPE_CLASSIC"] = "CRM classique";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Prospects &rarr; transactions + contacts";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Recommandé pour les entreprises de taille moyenne à grande. Tout d'abord, toutes les demandes reçues deviennent des prospects. Ensuite, les équipes de vente essaient de convertir les prospects en transactions et contacts.";
$MESS["CRM_TYPE_MORE"] = "En savoir plus";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Un prospect est une personne ou quelque chose qui peut potentiellement devenir un client.<br/>Il existe deux manières de travailler avec CRM Bitrix24:";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "Vous utilisez actuellement le mode \"CRM simple\" qui permet de <br/>transformer immédiatement les requêtes des nouveaux clients en transactions.<br/>Deux options sont possibles : ";
$MESS["CRM_TYPE_SAVE"] = "Enregistrer";
$MESS["CRM_TYPE_SIMPLE"] = "CRM simple";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Transactions + contacts (pas de prospect)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Recommandé pour les services des ventes des petites entreprises. Tous les e-mails entrants, appels, demandes et chats deviennent immédiatement des transactions et/ou des contacts.";
$MESS["CRM_TYPE_TITLE"] = "Choisissez la façon dont vous voulez travailler avec votre CRM";
?>