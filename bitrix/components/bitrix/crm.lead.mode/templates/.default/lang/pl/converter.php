<?
$MESS["CRM_TYPE_COMPANY"] = "Firma";
$MESS["CRM_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_TYPE_CREATION_SCENARIO"] = "Tryb tworzenia deala";
$MESS["CRM_TYPE_CREATION_SCENARIO_DESC"] = "Każde nowe zamówienie klienta zostanie zapisane jako nowy deal. Kontakt lub firma zostaną również utworzone automatycznie, w zależności od ustawień. Możesz również wybrać lejek dla nowych deali.";
$MESS["CRM_TYPE_DEAL_CREATE_AND"] = "Utwórz deal i...";
$MESS["CRM_TYPE_DEAL_DIRECTION"] = "Lejek deala";
$MESS["CRM_TYPE_NOT_CLOSE_DEAL"] = "Nie zamykaj działań po konwersji";
$MESS["CRM_TYPE_SETTINGS"] = "Ustawienia";
?>