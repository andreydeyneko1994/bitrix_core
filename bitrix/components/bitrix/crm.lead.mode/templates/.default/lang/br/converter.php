<?
$MESS["CRM_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_TYPE_CONTACT"] = "Contato";
$MESS["CRM_TYPE_CREATION_SCENARIO"] = "Modo de criação de negócios";
$MESS["CRM_TYPE_CREATION_SCENARIO_DESC"] = "Cada nova consulta recebida será salva como um novo negócio. Um contato ou uma empresa também serão criados automaticamente, à sua escolha. Você também pode selecionar a que pipeline serão adicionados esses novos negócios.";
$MESS["CRM_TYPE_DEAL_CREATE_AND"] = "Criar negócio e…";
$MESS["CRM_TYPE_DEAL_DIRECTION"] = "Pipeline de negócio";
$MESS["CRM_TYPE_NOT_CLOSE_DEAL"] = "Não fechar as atividades após a conversão";
$MESS["CRM_TYPE_SETTINGS"] = "Configurações";
?>