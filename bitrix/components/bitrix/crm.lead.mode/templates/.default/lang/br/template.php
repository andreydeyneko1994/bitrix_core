<?
$MESS["CRM_DEAL_SETTINGS"] = "Você pode configurar o #LINK_START# modo de criação de negócios #LINK_END#";
$MESS["CRM_RIGTHS_INFO"] = "Entre em contato com seu administrador para alterar o modo CRM";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_CHANGE"] = "Você pode alterar sua preferência a qualquer momento nas configurações de CRM";
$MESS["CRM_TYPE_CLASSIC"] = "CRM clássico";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Leads &rarr; negócios + contatos";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Recomendado para empresas de médio e grande porte. Primeiro, todas as consultas recebidas tornam-se leads. Então, sua equipe de vendas tenta converter os leads em negócios e contatos.";
$MESS["CRM_TYPE_MORE"] = "Saiba mais";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Um lead é qualquer pessoa que possa potencialmente se tornar um cliente.<br/>Existem duas maneiras de trabalhar com o CRM Bitrix24:";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "Atualmente, você está usando o modo \"CRM Simples\", em que<br/>novas solicitações de clientes se transformam imediatamente em negócios.<br/>Duas opções são possíveis:";
$MESS["CRM_TYPE_SAVE"] = "Salvar";
$MESS["CRM_TYPE_SIMPLE"] = "CRM simples";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Negócios + contatos (sem leads)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Recomendado para departamentos de vendas de pequenas empresas. Todos os e-mails recebidos, chamadas, solicitações e bate-papos tornam-se imediatamente negócios e/ou contatos.";
$MESS["CRM_TYPE_TITLE"] = "Escolha a maneira de que você deseja trabalhar com o seu CRM";
?>