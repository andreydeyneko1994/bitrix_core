<?
$MESS["CRM_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_TYPE_CREATION_SCENARIO"] = "Modo de creación de negociaciones";
$MESS["CRM_TYPE_CREATION_SCENARIO_DESC"] = "Cada nueva consulta entrante se guardará como una nueva negociación. También se creará automáticamente un contacto o una compañía, según su preferencia. Además, puede seleccionar un pipeline para agregar estas nuevas negociaciones.";
$MESS["CRM_TYPE_DEAL_CREATE_AND"] = "Crear negociación y…";
$MESS["CRM_TYPE_DEAL_DIRECTION"] = "Pipeline de negociación";
$MESS["CRM_TYPE_NOT_CLOSE_DEAL"] = "No cerrar las actividades después de la conversión.";
$MESS["CRM_TYPE_SETTINGS"] = "Configuraciones";
?>