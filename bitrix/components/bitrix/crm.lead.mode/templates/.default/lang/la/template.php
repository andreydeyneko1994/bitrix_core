<?
$MESS["CRM_DEAL_SETTINGS"] = "Puede configurar #LINK_START# modo de creación de negociaciones #LINK_END#";
$MESS["CRM_RIGTHS_INFO"] = "Póngase en contacto con su administrador para cambiar el modo de CRM";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_CHANGE"] = "Puede cambiar sus preferencias en cualquier momento en la configuración del CRM";
$MESS["CRM_TYPE_CLASSIC"] = "CRM Clásico";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Prospecto &rarr; negociación + contacto";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Recomendado para empresas medianas y grandes. En primer lugar, todas las consultas entrantes se convierten en prospectos. Luego su equipo de ventas trata de convertir prospectos a negociaciones y contactos.";
$MESS["CRM_TYPE_MORE"] = "Más información";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Un prospecto es cualquier persona que tiene el potencial de convertirse en un cliente.<br/>Hay dos formas de trabajar con Bitrix24 CRM:";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "Actualmente está utilizando el modo \"CRM Simple\", donde<br/>las solicitudes de nuevos clientes se convierten inmediatamente en negociaciones.<br/>Dos opciones son posibles:";
$MESS["CRM_TYPE_SAVE"] = "Guardar";
$MESS["CRM_TYPE_SIMPLE"] = "CRM Simple";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Negociación + contacto (sin prospectos)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Recomendado para los departamentos de ventas de pequeñas empresas. Todos los correos electrónicos entrantes, llamadas, solicitudes y chats se convierten inmediatamente en negociaciones y/o contactos.";
$MESS["CRM_TYPE_TITLE"] = "Elija la forma en que desea trabajar con su CRM";
?>