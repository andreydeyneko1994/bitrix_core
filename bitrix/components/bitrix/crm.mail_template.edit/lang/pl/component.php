<?
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Jednostka";
$MESS["CRM_MAIL_ENTITY_TYPE2"] = "Powiązania";
$MESS["CRM_MAIL_ENTITY_TYPE_UNI"] = "Nie";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Błąd tworzenia szablonu e-mail.";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Szablon";
$MESS["CRM_MAIL_TEMPLATE_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_MAIL_TEMPLATE_DELETE_BTN"] = "Usuń szablon";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć ten szablon e-mail?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Błąd usuwania szablonu e-mail.";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "od";
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Aktywne";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "Nie znaleziono szablonu e-mail.";
$MESS["CRM_MAIL_TEMPLATE_SAVE_BTN"] = "Zapisz";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Dostępny";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PUBLIC"] = "Dostępne dla wszystkich";
$MESS["CRM_MAIL_TEMPLATE_SENDER_MENU"] = "Nadawca";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Indeks sortowania";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "Temat";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT_HINT"] = "Wpisz temat wiadomości";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Nazwa";
$MESS["CRM_MAIL_TEMPLATE_TITLE_HINT"] = "wprowadź nazwę";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Błąd aktualizacji szablonu e-mail.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>