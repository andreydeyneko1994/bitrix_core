<?
$MESS["CRM_TEMPLATE_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se a página atual ainda estiver sendo exibida, feche-a manualmente.";
$MESS["CRM_TEMPLATE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "O modelo de mensagem <a href='#URL#'>#TITLE#</a> foi criado. Agora você está sendo redirecionado para a página anterior. Se a página atual ainda estiver sendo exibida, feche-a manualmente.";
?>