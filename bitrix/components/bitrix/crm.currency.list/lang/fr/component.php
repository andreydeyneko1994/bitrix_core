<?
$MESS["CRM_COLUMN_ACCOUNTING"] = "Devise des rapports";
$MESS["CRM_COLUMN_AMOUNT_CNT"] = "Valeur nominale";
$MESS["CRM_COLUMN_CURRENCY_LIST_BASE"] = "Base";
$MESS["CRM_COLUMN_EXCH_RATE"] = "Taux d'échange";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_INVOICE_DEF"] = "Devise de facturation par défaut";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_SORT"] = "Trier";
$MESS["CRM_CURRENCY_DELETION_GENERAL_ERROR"] = "Lors de suppression de devises une erreur s'est produite.";
$MESS["CRM_CURRENCY_MARK_AS_BASE_GENERAL_ERROR"] = "Erreur de réglage de la devise de base.";
$MESS["CRM_CURRENCY_UPDATE_GENERAL_ERROR"] = "Une erreur a eu lieu lors de la mise à jour de la devise.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>