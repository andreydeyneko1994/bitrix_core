<?
$MESS["LEARNING_BTN_CONTINUE"] = "Kontynuuj";
$MESS["LEARNING_BTN_START"] = "Uruchom";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "można przejść do następnego pytania bez udzielnia odpowiedzi na bieżące pytanie. <b>Dozwolone</b>, aby zmodyfikować odpowiedź.";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "można przejść do następnego pytania bez udzielnia odpowiedzi na bieżące pytanie. <b>Niedozwolone</b> aby zmodyfikować odpowiedź.";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "nie można przejść do następnego pytania bez udzielenia odpowiedzi na bieżące pytanie. <b>Niedozwolone</b>, aby zmodyfikować odpowiedź.";
$MESS["LEARNING_PASSAGE_TYPE"] = "Rodzaj zdawanego testu";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Aby uzyskać dostęp do tego testu musisz zdać test #TEST_LINK# i udzielić co najmniej #TEST_SCORE#% maksymalnych wszystkich ocen.";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Dozwolonych prób";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "nieograniczona ilość";
$MESS["LEARNING_TEST_NAME"] = "Nazwa testu";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Limit czasu";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "min.";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "bez limitu";
?>