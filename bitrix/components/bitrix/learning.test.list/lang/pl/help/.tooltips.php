<?
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["SET_TITLE_TIP"] = "Zaznaczenie tej opcji ustawi tytuł strony na  <b>Testy</b>.";
$MESS["TESTS_PER_PAGE_TIP"] = "Maksymalna liczba testów, które mogą być wyświetlone na stronie. Inne testy będą dostępne przy użyciu nawigacji breadcrumb.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Ścieżka do głównej strony testów.";
?>