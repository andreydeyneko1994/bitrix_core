<?
$MESS["INTASK_C31T_ANY"] = "(jakikolwiek)";
$MESS["INTASK_C31T_DURATION"] = "Długość rezerwacji w godzinach";
$MESS["INTASK_C31T_FDATE"] = "Data";
$MESS["INTASK_C31T_FFLOOR"] = "Piętro";
$MESS["INTASK_C31T_FFREE"] = "Czas wolny";
$MESS["INTASK_C31T_FPLACE"] = "Miejsca siedzące";
$MESS["INTASK_C31T_FRESERVE"] = "Rezerwuj";
$MESS["INTASK_C31T_FROM"] = "od";
$MESS["INTASK_C31T_FROOM"] = "Pokój";
$MESS["INTASK_C31T_ROOM"] = "Pokój";
$MESS["INTASK_C31T_SDATE"] = "Data";
$MESS["INTASK_C31T_SEARCH"] = "Szukaj";
$MESS["INTASK_C31T_SPLACE"] = "Wymagane miejsca";
$MESS["INTASK_C31T_STIME"] = "Czas";
$MESS["INTASK_C31T_TO"] = "do";
$MESS["INTDT_NO_TASKS"] = "Brak zasobów";
?>