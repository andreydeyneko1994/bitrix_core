<?
$MESS["M_CRM_QUOTE_CONVERSION_NOTIFY"] = "Pola wymagane";
$MESS["M_CRM_QUOTE_EDIT_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_QUOTE_EDIT_CONTINUE_BTN"] = "Kontynuuj";
$MESS["M_CRM_QUOTE_EDIT_CONVERT_TITLE"] = "Oferta";
$MESS["M_CRM_QUOTE_EDIT_CREATE_TITLE"] = "Utwórz ofertę";
$MESS["M_CRM_QUOTE_EDIT_EDIT_TITLE"] = "Edycja";
$MESS["M_CRM_QUOTE_EDIT_SAVE_BTN"] = "Zapisz";
$MESS["M_CRM_QUOTE_EDIT_VIEW_TITLE"] = "Edycja oferty";
$MESS["M_CRM_QUOTE_MENU_CREATE_ON_BASE"] = "Utwórz ze źródła";
$MESS["M_CRM_QUOTE_MENU_DELETE"] = "Usuń";
$MESS["M_CRM_QUOTE_MENU_EDIT"] = "Edycja";
$MESS["M_CRM_QUOTE_MENU_HISTORY"] = "Historia";
$MESS["M_DETAIL_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_DETAIL_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_DETAIL_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
?>