<?
$MESS["CRM_CURRENCY_CLASSIFIER_ADD_UNKNOWN_ERROR"] = "Nieznany błąd przy tworzeniu waluty.";
$MESS["CRM_CURRENCY_CLASSIFIER_CURRENCY_NOT_FOUND"] = "Nie znaleziono waluty #currency#";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME_ERROR"] = "Nazwa waluty nie może być pusta ani dłuższa niż 50 znaków";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_INCORRECT_VALUE_ERROR"] = "Wartość jest niepoprawna lub zbyt duża";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_MAX_LENGTH_ERROR"] = "Wartość nie może przekraczać #max_length# znaków";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_AFTER"] = "Po kwocie";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_BEFORE"] = "Przed kwotą";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE_ERROR"] = "Kod symboliczny musi składać się z trzech liter alfabetu łacińskiego";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT_OWN"] = "Inna wartość";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CRM"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_CURRENCY_CLASSIFIER_UPDATE_UNKNOWN_ERROR"] = "Nieznany błąd przy aktualizacji waluty.";
?>