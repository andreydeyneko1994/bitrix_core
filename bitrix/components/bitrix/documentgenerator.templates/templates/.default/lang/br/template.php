<?
$MESS["DOCGEN_TEMPLATE_LIST_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir este modelo?";
$MESS["DOCGEN_TEMPLATE_LIST_FEEDBACK"] = "Feedback";
$MESS["DOCGEN_TEMPLATE_LIST_MORE"] = "mais";
$MESS["DOCGEN_TEMPLATE_LIST_MORE_INFO"] = "Saiba como configurar e carregar modelos de documentos personalizados";
$MESS["DOCGEN_TEMPLATE_LIST_UPLOAD"] = "Carregar";
?>