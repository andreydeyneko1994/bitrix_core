<?
$MESS["DOCGEN_TEMPLATE_LIST_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć ten szablon?";
$MESS["DOCGEN_TEMPLATE_LIST_FEEDBACK"] = "Informacja zwrotna";
$MESS["DOCGEN_TEMPLATE_LIST_MORE"] = "więcej";
$MESS["DOCGEN_TEMPLATE_LIST_MORE_INFO"] = "Dowiedz się jak skonfigurować i przesłać niestandardowe szablony dokumentów";
$MESS["DOCGEN_TEMPLATE_LIST_UPLOAD"] = "Prześlij";
?>