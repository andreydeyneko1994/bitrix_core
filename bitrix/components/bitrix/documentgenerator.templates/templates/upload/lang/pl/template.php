<?
$MESS["DOCGEN_TEMPLATE_ADD_ACTIVE"] = "Aktywny";
$MESS["DOCGEN_TEMPLATE_ADD_ADD_USER_LINK"] = "Dodaj więcej";
$MESS["DOCGEN_TEMPLATE_ADD_CANCEL"] = "Anuluj";
$MESS["DOCGEN_TEMPLATE_ADD_COMPLETE"] = "Plik został przesłany";
$MESS["DOCGEN_TEMPLATE_ADD_DOWNLOAD"] = "Pobierz szablon";
$MESS["DOCGEN_TEMPLATE_ADD_DRAG_HERE"] = "Przeciągnij plik tutaj lub <span class=\"docs-template-load-drag-line\" id=\"upload-template-text\">wybierz plik z komputera</span>";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_FILE"] = "Jako szablonów używaj tylko plików DOCX";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_NAME"] = "Wprowadź nazwę szablonu";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_PROVIDER"] = "Wybierz co najmniej jedną sekcję";
$MESS["DOCGEN_TEMPLATE_ADD_FEEDBACK"] = "Informacja zwrotna";
$MESS["DOCGEN_TEMPLATE_ADD_FILE"] = "Prześlij plik szablonu";
$MESS["DOCGEN_TEMPLATE_ADD_FORMATS"] = "Szablony dokumentów muszą mieć format DOCX.";
$MESS["DOCGEN_TEMPLATE_ADD_INSTALL"] = "Zainstaluj";
$MESS["DOCGEN_TEMPLATE_ADD_MAIN_PROVIDER"] = "Powiąż z sekcjami CRM";
$MESS["DOCGEN_TEMPLATE_ADD_NAME"] = "Nazwa";
$MESS["DOCGEN_TEMPLATE_ADD_PROGRESS"] = "Teraz przesyłanie...";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL"] = "Przywróć domyślną wersję szablonu";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_CONFIRM"] = "Czy na pewno chcesz zastąpić plik szablonem domyślnym?";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_CONFIRM_TITLE"] = "Przywróć szablon";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_SUCCESS"] = "Plik szablonu został zaktualizowany.";
$MESS["DOCGEN_TEMPLATE_ADD_SAVE"] = "Zapisz";
$MESS["DOCGEN_TEMPLATE_ADD_SORT"] = "Sortuj";
$MESS["DOCGEN_TEMPLATE_ADD_SUCCESS"] = "Szablon został przesłany";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR"] = "Użyj szablonu automatycznego numerowania";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR_CREATE"] = "utwórz";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR_EDIT"] = "konfiguruj";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_REGION"] = "Powiąż z krajem";
$MESS["DOCGEN_TEMPLATE_ADD_UPLOAD_NEW"] = "Prześlij nowy";
$MESS["DOCGEN_TEMPLATE_ADD_USERS"] = "Użytkownicy szablonu";
$MESS["DOCGEN_TEMPLATE_ADD_WITH_STAMPS"] = "Z podpisami i pieczątką";
?>