<?php
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_MUTE"] = "Som mudo. Os contadores de tarefas serão exibidos em cinza; eles não vão somar ao contador total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_UNMUTE"] = "Som ativado. Os contadores de tarefas serão mostrados em verde; eles serão adicionados ao contador total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_CHECKLIST_SECTION"] = "Lista de verificação";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_COMPLETED"] = "Mostrar itens concluídos da lista de verificação";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_ONLY_MINE"] = "Mostrar itens da lista de verificação relevantes apenas para mim";
