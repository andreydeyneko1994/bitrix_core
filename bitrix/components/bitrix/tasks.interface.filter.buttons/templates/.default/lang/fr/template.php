<?php
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_MUTE"] = "Coupez le son. Les compteurs de tâches seront affichés en gris ; ils ne seront pas ajoutés au compteur total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_MUTE_BUTTON_HINT_UNMUTE"] = "Rétablissez le son. Les compteurs de tâches seront affichés en vert ; ils seront ajoutés au compteur total.";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_CHECKLIST_SECTION"] = "Liste de contrôle";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_COMPLETED"] = "Afficher les éléments de liste de contrôle terminés";
$MESS["TASKS_INTERFACE_FILTER_BUTTONS_POPUP_MENU_SHOW_ONLY_MINE"] = "Afficher les éléments de liste de contrôle qui me concernent uniquement";
