<?
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "Początkowy numer zamówienia \"#NUMBER#\" jest nieprawidłowy.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "Prefiks zamówienia \"#PREFIX#\" jest nieprawidłowy.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu.";
?>