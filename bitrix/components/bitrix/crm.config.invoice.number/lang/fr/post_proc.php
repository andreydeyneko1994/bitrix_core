<?
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "Nombre initial '#NOMBRE#'incorrect pour la numérotation.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "Valeur incorrecte du préfixe '#PREFIX#' pour la numérotation de commande.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé.";
?>