<?
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "El número inicial del pedido \"#NUMBER#\" no es válido.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "El prefijo del pedido \"#PREFIX#\" no es válido.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
?>