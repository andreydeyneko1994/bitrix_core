<?php
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY"] = "Copier l'adresse";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY_SUCCESS"] = "L'adresse a été copiée dans le Presse-papiers";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_DESC"] = "Copiez une adresse et transférez-vous-la par e-mail pour créer des tâches et des publications de Flux des activités";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_HELP"] = "#LINK_START#Plus d'informations#LINK_END# sur cette fonctionnalité";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM"] = "Pour créer des publications sur le Flux d'activités";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM2"] = "Pour créer des publications dans le fil d'actualités";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_TASK"] = "Pour créer des tâches";
