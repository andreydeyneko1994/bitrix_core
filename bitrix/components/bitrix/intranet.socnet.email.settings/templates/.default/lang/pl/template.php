<?php
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY"] = "Skopiuj adres";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY_SUCCESS"] = "Adres został skopiowany do schowka";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_DESC"] = "Skopiuj adres i prześlij nań dalej swoje wiadomości e-mail, aby utworzyć zadania i posty na Aktualności";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_HELP"] = "#LINK_START#Dowiedz się więcej#LINK_END# o tej funkcji";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM"] = "Aby tworzyć posty na Aktualności";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM2"] = "Tworzenie postów na kanale informacji";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_TASK"] = "Aby tworzyć zadania";
