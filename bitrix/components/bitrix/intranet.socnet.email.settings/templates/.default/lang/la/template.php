<?php
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY"] = "Copiar la dirección";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY_SUCCESS"] = "La dirección se copió al Portapapeles";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_DESC"] = "Copie una dirección y reenvíe a ella sus correos electrónicos para crear tareas y publicaciones del noticias";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_HELP"] = "#LINK_START#Obtenga más información#LINK_END# sobre esta función";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM"] = "Para crear publicaciones del noticias";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM2"] = "Para crear publicaciones de Noticias";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_TASK"] = "Para crear tareas";
