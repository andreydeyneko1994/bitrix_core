<?php
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY"] = "Copiar endereço";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_COPY_SUCCESS"] = "O endereço foi copiado para a Área de Transferência";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_DESC"] = "Copie um endereço e encaminhe a ele seus e-mails para criar tarefas e postagens do Feed";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_HELP"] = "#LINK_START#Saiba mais#LINK_END# sobre esse recurso";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM"] = "Para criar postagens do Feed";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_STREAM2"] = "Para criar postagens no Feed de notícias";
$MESS["INTRANET_SOCNET_EMAIL_SETTINGS_TASK"] = "Para criar tarefas";
