<?php
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Parabéns!</b><br><br>Um aviso de inscrição foi enviado ao usuário especificado.<br><br>Para ver quem foi convidado, por favor, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra a lista de funcionários</a>.";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Formato de e-mail incorreto";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL"] = "Formato de e-mail incorreto";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL_OR_PHONE"] = "Formato incorreto de e-mail ou número de telefone";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Por favor, selecione um grupo para adicionar usuários externos para";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Por favor, selecione um grupo para convidar usuários externos para";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>Parabéns!</b><br><br>Um convite para se juntar ao seu Bitrix24 foi enviado para o endereço de e-mail do parceiro Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_COUNT_ERROR"] = "Número máximo de parceiros Bitrix24 excedido. Você pode dispensar um dos #LINK_START#parceiros Bitrix24#LINK_END# existentes e convidar um novo.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "Não há parceiro Bitrix24 com o e-mail especificado";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "Estou convidando você para participar do nosso Bitrix24! Você receberá permissões completas de parceiro Bitrix24 para nos ajudar a organizar e configurar a conta Bitrix24. Essas são permissões de acesso completo, exceto que você não pode adicionar ou excluir administradores do sistema.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Parabéns!</b><br><br>Convites Bitrix24 foram enviados para os endereços de e-mail selecionados.<br><br>Para ver quem foi convidado, por favor, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra a lista de funcionários</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>Parabéns!</b><br><br>Convites Bitrix24 foram enviados para os números selecionados.<br><br>Para ver quem foi convidado, por favor, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra a lista de funcionários</a>.";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>Parabéns!</b><br><br>As configurações de cadastro rápido foram atualizadas.";
$MESS["BX24_INVITE_DIALOG_SELF_SUCCESS"] = "As configurações de cadastro rápido foram atualizadas.";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Erro ao criar nova caixa de correio:";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "Sua senha e a senha de confirmação não correspondem";
$MESS["INTRANET_INVITE_DIALOG_ERROR_LENGTH"] = "Comprimento máximo de 50 caracteres excedido";
$MESS["INTRANET_INVITE_DIALOG_USER_COUNT_ERROR"] = "Excedeu o máximo de usuários convidados diários";
