<?php
$MESS["INTRANET_INVITE_DIALOG_MENU_ACTIVE_DIRECTORY"] = "Active Directory";
$MESS["INTRANET_INVITE_DIALOG_MENU_ADD"] = "Ajouter un nouvel utilisateur";
$MESS["INTRANET_INVITE_DIALOG_MENU_EXTRANET"] = "Inviter un utilisateur externe";
$MESS["INTRANET_INVITE_DIALOG_MENU_INTEGRATOR"] = "Inviter un partenaire Bitrix24";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL"] = "Inviter via e-mail";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_EMAIL_AND_PHONE"] = "Inviter via e-mail ou numéro de téléphone";
$MESS["INTRANET_INVITE_DIALOG_MENU_INVITE_WITH_GROUP_DP"] = "Inviter à rejoindre un service ou un groupe de travail";
$MESS["INTRANET_INVITE_DIALOG_MENU_MASS_INVITE"] = "Invitation en masse";
$MESS["INTRANET_INVITE_DIALOG_MENU_PHONE"] = "Inviter via SMS";
$MESS["INTRANET_INVITE_DIALOG_MENU_SELF"] = "Inviter via lien";
$MESS["INTRANET_INVITE_DIALOG_MENU_SOON"] = "bientôt";
