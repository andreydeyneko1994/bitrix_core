<?php
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>¡Felicidades!</b><br><br>Se envió una notificación de suscripción al usuario especificado.<br><br>Para ver quién fue invitado, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra la lista de empleados</a>.";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Formato de correo electrónico incorrecto";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL"] = "Formato de correo electrónico incorrecto";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL_OR_PHONE"] = "Formato de correo electrónico o número de teléfono incorrecto";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "Seleccione un grupo para agregar usuarios externos al";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "Seleccione un grupo para invitar a usuarios externos al";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>¡Felicidades!</b><br><br>Se envió una invitación al correo electrónico del socio de Bitrix24 para unirse a su Bitrix24.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_COUNT_ERROR"] = "Superó el número máximo de socios de Bitrix24. Puede eliminar a uno de los #LINK_START#socios de Bitrix24#LINK_END# # existentes e invitar a uno nuevo.";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "No hay ningún socio de Bitrix24 con el correo electrónico que especificó";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "¡Le invito a unirse a nuestro Bitrix24! Obtendrá todos los permisos de los socios de Bitrix24 para crear y configurar la cuenta de Bitrix24. Esos son permisos de acceso completo, excepto que no puede agregar o eliminar administradores del sistema.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>¡Felicidades!</b><br><br>Las invitaciones a Bitrix24 se enviaron a los correos electrónicos seleccionados.<br><br>Para ver quién fue invitado, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra la lista de empleados</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b><b>¡Felicidades!</b><br><br>Las invitaciones a Bitrix24 se enviaron a los números seleccionados.<br><br>Para ver quién fue invitado, <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">abra la lista de empleados</a>.";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>¡Felicidades!</b><br><br>Se actualizaron los ajustes de registro rápido.";
$MESS["BX24_INVITE_DIALOG_SELF_SUCCESS"] = "Se ha actualizó la configuración de registro rápido.";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "Error al crear un nuevo buzón:";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "Su contraseña y la contraseña de confirmación no coinciden";
$MESS["INTRANET_INVITE_DIALOG_ERROR_LENGTH"] = "Se excedió la longitud máxima de 50 caracteres";
$MESS["INTRANET_INVITE_DIALOG_USER_COUNT_ERROR"] = "Se excedió el número máximo de usuarios invitados diarios";
