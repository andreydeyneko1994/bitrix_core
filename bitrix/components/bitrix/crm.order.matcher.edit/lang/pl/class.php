<?php
$MESS["CRM_EMPTY_PERSON_TYPE_LIST"] = "Przed edycją formularza finalizacji zamówienia musisz stworzyć odpowiedni typ płatnika.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_ORDERFORM_EMPTY_LIST_LABEL"] = "Lista właściwości jest pusta";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT"] = "Klient";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC"] = "kontakt lub firma";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC2"] = "Niepowiązany klient";
$MESS["CRM_ORDERFORM_RELATION_DELIVERY"] = "Usługa dostawy";
$MESS["CRM_ORDERFORM_RELATION_PAY_SYSTEM"] = "System płatności";
$MESS["CRM_ORDERFORM_REQUIERD_FIELDS_ERROR"] = "Musisz dodać jedno lub więcej wymaganych pól";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_MERGE"] = "Scal";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_NONE"] = "Zezwól na duplikaty";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_REPLACE"] = "Zamień";
$MESS["CRM_ORDERFORM_TITLE"] = "Skonfiguruj formularz finalizacji zamówienia";
$MESS["CRM_ORDERFORM_TRADING_PLATFORM_GENERAL"] = "Ogólne";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
