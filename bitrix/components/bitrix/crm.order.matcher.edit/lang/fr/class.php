<?php
$MESS["CRM_EMPTY_PERSON_TYPE_LIST"] = "Vous devez créer un type de payeur approprié avant de modifier le formulaire de paiement.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_ORDERFORM_EMPTY_LIST_LABEL"] = "La liste des propriétés est vide";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT"] = "Client";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC"] = "contact ou société";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC2"] = "Client non associé";
$MESS["CRM_ORDERFORM_RELATION_DELIVERY"] = "Service de livraison";
$MESS["CRM_ORDERFORM_RELATION_PAY_SYSTEM"] = "Système de paiement";
$MESS["CRM_ORDERFORM_REQUIERD_FIELDS_ERROR"] = "Vous devez ajouter un ou plusieurs champs obligatoires";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_MERGE"] = "Fusionner";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_NONE"] = "Autoriser les doubles";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_REPLACE"] = "Remplacer";
$MESS["CRM_ORDERFORM_TITLE"] = "Configurer le formulaire de paiement";
$MESS["CRM_ORDERFORM_TRADING_PLATFORM_GENERAL"] = "Général";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
