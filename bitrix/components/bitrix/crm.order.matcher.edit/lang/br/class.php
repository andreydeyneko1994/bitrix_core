<?php
$MESS["CRM_EMPTY_PERSON_TYPE_LIST"] = "Você tem que criar um tipo de pagador adequado antes de editar o formulário de finalização de compra.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_ORDERFORM_EMPTY_LIST_LABEL"] = "A lista de propriedade está vazia";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT"] = "Cliente";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC"] = "contato ou empresa";
$MESS["CRM_ORDERFORM_ENTITY_SCHEME_CLIENT_DESC2"] = "Cliente não vinculado";
$MESS["CRM_ORDERFORM_RELATION_DELIVERY"] = "Serviço de entrega";
$MESS["CRM_ORDERFORM_RELATION_PAY_SYSTEM"] = "Sistema de pagamento";
$MESS["CRM_ORDERFORM_REQUIERD_FIELDS_ERROR"] = "Você tem que adicionar um ou mais campos obrigatórios";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_MERGE"] = "Mesclar";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_NONE"] = "Permitir duplicatas";
$MESS["CRM_ORDERFORM_RESULT_ENTITY_DC_REPLACE"] = "Substituir";
$MESS["CRM_ORDERFORM_TITLE"] = "Configurar formulário de pagamento";
$MESS["CRM_ORDERFORM_TRADING_PLATFORM_GENERAL"] = "Geral";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
