<?
$MESS["SET_TITLE_TIP"] = "Zaznaczenie tej opcji ustawi tytuł strony na <b>Profil ucznia</b>.";
$MESS["TRANSCRIPT_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony z wynikami egzaminu.";
$MESS["USER_PROPERTY_TIP"] = "Wybierz tutaj dodatkowe właściwości, które będą wyświetlane w profilu użytkownika.";
?>