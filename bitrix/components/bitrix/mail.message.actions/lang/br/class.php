<?php
$MESS["MAIL_MESSAGE_ACTIONS_CRM_BTN"] = "Salvar no CRM";
$MESS["MAIL_MESSAGE_ACTIONS_CRM_EXCLUDE_BTN"] = "Excluir do CRM";
$MESS["MAIL_MESSAGE_ACTIONS_EVENT_BTN"] = "Criar evento";
$MESS["MAIL_MESSAGE_ACTIONS_FEED_POST_BTN"] = "Discutir no Feed";
$MESS["MAIL_MESSAGE_ACTIONS_IM_BTN"] = "Discutir no bate-papo";
$MESS["MAIL_MESSAGE_ACTIONS_NOTIFY_ADDED_TO_CRM"] = "E-mail salvo no CRM";
$MESS["MAIL_MESSAGE_ACTIONS_NOTIFY_EXCLUDED_FROM_CRM"] = "E-mail excluído do CRM";
$MESS["MAIL_MESSAGE_ACTIONS_POST_TITLE"] = "E-mail: #SUBJECT#";
$MESS["MAIL_MESSAGE_ACTIONS_SUBJECT_PLACEHOLDER"] = "(sem assunto)";
$MESS["MAIL_MESSAGE_ACTIONS_TASK_BTN"] = "Criar tarefa";
$MESS["MAIL_MESSAGE_ACTIONS_TASK_TITLE"] = "E-mail: #SUBJECT#";
