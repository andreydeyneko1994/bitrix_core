<?
$MESS["M_CRM_CONTACT_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_CONTACT_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_CONTACT_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_CONTACT_EDIT_CONVERT_TITLE"] = "Contato";
$MESS["M_CRM_CONTACT_EDIT_CREATE_TITLE"] = "Novo contato";
$MESS["M_CRM_CONTACT_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_CONTACT_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_CONTACT_EDIT_VIEW_TITLE"] = "Ver contato";
$MESS["M_CRM_CONTACT_MENU_ACTIVITY"] = "Atividades";
$MESS["M_CRM_CONTACT_MENU_DEALS"] = "Negócios";
$MESS["M_CRM_CONTACT_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_CONTACT_MENU_EDIT"] = "Editar";
$MESS["M_CRM_CONTACT_MENU_HISTORY"] = "Histórico";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>