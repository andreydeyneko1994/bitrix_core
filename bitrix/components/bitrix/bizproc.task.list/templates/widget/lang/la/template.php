<?php
$MESS["BPTLWGT_MODULE_DISK"] = "Archivos";
$MESS["BPTLWGT_MODULE_IBLOCK"] = "Listas";
$MESS["BPTLWGT_MODULE_LISTS"] = "Solicitudes de proceso";
$MESS["BPTLWGT_MY_PROCESSES"] = "Mis solicitudes";
$MESS["BPTLWGT_RUNNING"] = "Pendiente";
$MESS["BPTLWGT_TITLE"] = "Flujos de trabajo";
