<?php
$MESS["MOBILE_LOG_COMMENTS"] = "Komentarze";
$MESS["MOBILE_LOG_COMMENTS_2"] = "Komentarze:";
$MESS["MOBILE_LOG_COMMENT_ACTION"] = "Komentarz";
$MESS["MOBILE_LOG_COMMENT_ADD_BUTTON_SEND"] = "Wyślij";
$MESS["MOBILE_LOG_COMMENT_ADD_TITLE"] = "Wyślij wiadomość...";
$MESS["MOBILE_LOG_COMMENT_BUTTON_MORE"] = "Pokazać komentarze <i>.</i> (#COMMENTS#)";
$MESS["MOBILE_LOG_COMMENT_DELETE"] = "Usuń";
$MESS["MOBILE_LOG_COMMENT_EDIT"] = "Edytuj";
$MESS["MOBILE_LOG_COMMENT_FORMAT_DATE"] = "d F, g:i a";
$MESS["MOBILE_LOG_COMMENT_FORMAT_DATE_YEAR"] = "d F Y, g:i a";
$MESS["MOBILE_LOG_COMMENT_LIKES_LIST"] = "Polub";
$MESS["MOBILE_LOG_COMMENT_REPLY"] = "Odpowiedź";
$MESS["MOBILE_LOG_DESTINATION_MORE_0"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_1"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_2"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_3"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_4"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_5"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_6"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_7"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_8"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_DESTINATION_MORE_9"] = "i #COUNT# więcej odbiorców";
$MESS["MOBILE_LOG_EXPAND"] = "Więcej";
$MESS["MOBILE_LOG_LIKE2_ACTION"] = "Lubię:";
$MESS["MOBILE_LOG_LIKE2_ACTION_PATTERN"] = "#LIKE#:";
$MESS["MOBILE_LOG_LIKE_ACTION"] = "Lubię to";
$MESS["MOBILE_LOG_LIKE_COUNT_USERS"] = "#COUNT# #USERS#";
$MESS["MOBILE_LOG_LIKE_ME"] = "Ty";
$MESS["MOBILE_LOG_LIKE_OTHERS"] = "#COUNT_USERS# lubi to";
$MESS["MOBILE_LOG_LIKE_YOU"] = "#YOU# lubię to";
$MESS["MOBILE_LOG_LIKE_YOU_OTHERS"] = "#YOU# i #COUNT_USERS# lubię to";
$MESS["MOBILE_LOG_MORE"] = "Więcej";
