<?
$MESS["M_CRM_CONVERT_SYNC_CONTINUE"] = "Kontynuuj";
$MESS["M_CRM_CONVERT_SYNC_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_CONVERT_SYNC_ENTITIES"] = "Wybierz dokumenty, które zostaną uzupełnione w brakujących polach";
$MESS["M_CRM_CONVERT_SYNC_FIELDS"] = "Te pola zostaną utworzone";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_DEAL"] = "Wybrane elementy nie mają pól, które mogą przechowywać dane deala. Proszę wybierz elementy, w których powstaną brakujące pola, mogące zebrać wszystkie dostępne informacje.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_LEAD"] = "Wybrane elementy nie mają pól, które mogą przechowywać dane leada. Proszę wybierz elementy, w których powstaną brakujące pola, mogące zebrać wszystkie dostępne informacje.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_QUOTE"] = "Wybrane elementy nie mają pól, które mogą przechowywać dane oferty. Proszę wybierz elementy, w których powstaną brakujące pola, mogące zebrać wszystkie dostępne informacje.";
$MESS["M_CRM_CONVERT_SYNC_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_CONVERT_SYNC_MORE"] = "Więcej";
$MESS["M_CRM_CONVERT_SYNC_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_CONVERT_SYNC_TITLE"] = "Utwórz ze źródła";
?>