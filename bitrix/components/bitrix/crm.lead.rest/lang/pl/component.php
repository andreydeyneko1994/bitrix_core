<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_REST_ERROR_BAD_AUTH"] = "Błąd autoryzacji";
$MESS["CRM_REST_ERROR_BAD_REQUEST"] = "Błąd żądania";
$MESS["CRM_REST_OK"] = "Lead został dodany.";
$MESS["UNKNOWN_ERROR"] = "Nieznany błąd.";
?>