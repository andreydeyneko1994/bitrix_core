<?
$MESS["CRM_CONFIG_PLG_DESC"] = "Conecte su tienda online a Bitrix24 para crear una nueva actividad cada vez que aparezca un nuevo pedido en CRM. Haga que su tienda online sea otra fuente de información de ventas.";
$MESS["CRM_CONFIG_PLG_DESC2"] = "Utilice Bitrix24 para aumentar la tasa de conversión y administrar clientes!";
$MESS["CRM_CONFIG_PLG_STEP1"] = "Haga clic en #A1#here#A2# para descargar e instalar la aplicación en su sitio web.";
$MESS["CRM_CONFIG_PLG_STEP2_1"] = "Crear y copiar el link";
$MESS["CRM_CONFIG_PLG_STEP2_2"] = "crear link";
$MESS["CRM_CONFIG_PLG_STEP2_3"] = "crear nuevo enlace";
$MESS["CRM_CONFIG_PLG_STEP2_4"] = "Necesitará el enlace para configurar su tienda online.";
$MESS["CRM_CONFIG_PLG_STEP3"] = "Pegue el enlace en el campo de configuración de la aplicación de su tienda online y guarde los cambios.";
$MESS["CRM_CONFIG_PLG_STEP4"] = "¡Su tienda online está conectada ahora!";
$MESS["CRM_CONFIG_PLG_STEP4_2"] = "Cada nuevo pedido ahora será registrada en Bitrix24 como una nueva actividad, el respectivo cliente registrado como cliente en el CRM.";
$MESS["CRM_CONFIG_PLG_TITLE"] = "Gestionar los pedidos de su tienda online en Bitrix24";
$MESS["CRM_CONFIG_PLG_TITLE_1CBITRIX"] = "1C Bitrix";
$MESS["CRM_CONFIG_PLG_TITLE_DRUPAL7"] = "Drupal 7";
$MESS["CRM_CONFIG_PLG_TITLE_JOOMLA"] = "Joomla";
$MESS["CRM_CONFIG_PLG_TITLE_MAGENTO2"] = "Magento 2";
$MESS["CRM_CONFIG_PLG_TITLE_WORDPRESS"] = "Wordpress";
?>