<?
$MESS["CRM_CONFIG_PLG_DESC1"] = "Exporte la base de datos del cliente de su tienda online a Bitrix24 y disfrute de numerosas funciones útiles de administración de clientes.";
$MESS["CRM_CONFIG_PLG_DESC2"] = "Convierta los pedidos de la tienda online en actividades de Bitrix24 y cree perfiles de clientes.";
$MESS["CRM_CONFIG_PLG_DESC3"] = "Reactivar pedidos abandonados utilizando herramientas de automatización de Bitrix24.";
$MESS["CRM_CONFIG_PLG_DESC4"] = "Un cliente visitó su sitio web pero nunca envió una orden? Bitrix24 capturará el evento, creará un prospecto y hará una llamada telefónica automatizada al cliente.";
$MESS["CRM_CONFIG_PLG_SELECT_CMS"] = "Seleccione la plataforma de su tienda online para conectarse a Bitrix24";
$MESS["CRM_CONFIG_PLG_SOON"] = "¡próximamente!";
$MESS["CRM_CONFIG_PLG_TITLE"] = "Tracker de Tienda Online";
?>