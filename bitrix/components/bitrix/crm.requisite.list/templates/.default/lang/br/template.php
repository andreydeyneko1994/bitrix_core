<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "Isso é um erro.";
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Bem sucedida";
$MESS["CRM_REQUISITE_COPY"] = "Copiar";
$MESS["CRM_REQUISITE_COPY_TITLE_COMPANY"] = "Copiar informações da empresa";
$MESS["CRM_REQUISITE_COPY_TITLE_CONTACT"] = "Copiar informações do contato";
$MESS["CRM_REQUISITE_DELETE"] = "Excluir";
$MESS["CRM_REQUISITE_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir o item?";
$MESS["CRM_REQUISITE_DELETE_TITLE_COMPANY"] = "Excluir informações da empresa";
$MESS["CRM_REQUISITE_DELETE_TITLE_CONTACT"] = "Excluir informações do contato";
$MESS["CRM_REQUISITE_EDIT"] = "Editar";
$MESS["CRM_REQUISITE_EDIT_TITLE_COMPANY"] = "Editar informações da empresa";
$MESS["CRM_REQUISITE_EDIT_TITLE_CONTACT"] = "Editar informações do contato";
$MESS["CRM_REQUISITE_POPUP_CANCEL_BUTTON_TITLE"] = "Fechar";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Não existem modelos existentes disponíveis.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Não existem modelos existentes disponíveis.";
$MESS["CRM_REQUISITE_POPUP_SAVE_BUTTON_TITLE"] = "Salvar";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Informações da empresa";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Informações do contato";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Adicionar novo";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Adicionar informações da empresa usando modelo";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Adicionar informações de contato usando modelo";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
$MESS["CRM_STATUS_INIT"] = "- status da SKU -";
?>