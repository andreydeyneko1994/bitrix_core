<?php
$MESS["TASKS_ERROR_COMPONENT_DESCRIPTION_DEFAULT"] = "Póngase en contacto con los participantes de la tarea o con el administrador de Bitrix24";
$MESS["TASKS_ERROR_COMPONENT_TITLE_DEFAULT"] = "No se encontró la tarea o se denegó el acceso";
