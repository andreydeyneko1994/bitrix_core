<?
$MESS["CRM_DEAL_EDIT_EVENT_CANCELED"] = "Działanie zostało anulowane. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli bieżąca strona jest nadal wyświetlana, zamknij ją ręcznie.";
$MESS["CRM_DEAL_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Utworzono deal <a href='#URL#'>#TITLE#</a>. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli bieżąca strona jest nadal wyświetlana, zamknij ją ręcznie.";
?>