<?
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Niewystarczające uprawnienia aby stworzyć fakturę lub ofertę.";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Kontynuuj";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wybierz obiekty, w których będą stworzone dodatkowe pola";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Następujące pola będą stworzone";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "Wybrane obiekty nie posiadają pól, do których można przekazać dane.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Stwórz element w Dealu";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Błąd ogólny.";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Nowy deal";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Deal ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_RECUR_SHOW_TITLE"] = "Szablon dealu ##ID# &mdash; #TITLE#";
$MESS["CRM_TAB_1"] = "Deal";
$MESS["CRM_TAB_1_TITLE"] = "Właściwości deala";
$MESS["CRM_TAB_2"] = "Zapis";
$MESS["CRM_TAB_2_TITLE"] = "Dziennik Deala";
$MESS["CRM_TAB_3"] = "Proces Biznesowy";
$MESS["CRM_TAB_3_TITLE"] = "Proces biznesowy deala";
?>