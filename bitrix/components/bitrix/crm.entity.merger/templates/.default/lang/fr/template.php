<?php
$MESS["CRM_ENTITY_MERGER_ENTITIES_NOT_FOUND"] = "Rien à fusionner ici.";
$MESS["CRM_ENTITY_MERGER_GO_TO_DUPLICATE_LIST"] = "Retour à la liste";
$MESS["CRM_ENTITY_MERGER_MARK_AS_NON_DUPLICATE"] = "pas un doublon";
$MESS["CRM_ENTITY_MERGER_MERGE_AND_EDIT"] = "Fusionner et modifier";
$MESS["CRM_ENTITY_MERGER_OPEN_ENTITY"] = "ouvrir";
$MESS["CRM_ENTITY_MERGER_POSTPONE"] = "Plus tard";
$MESS["CRM_ENTITY_MERGER_PRIMARY_ENTITY_NOT_FOUND"] = "Veuillez sélectionner l'enregistrement principal dans lequel fusionner les données de doublon.";
$MESS["CRM_ENTITY_MERGER_PROCESS"] = "Fusionner";
$MESS["CRM_ENTITY_MERGER_PROCESSED_AMOUNT"] = "fusionnés";
$MESS["CRM_ENTITY_MERGER_REMAINING_AMOUNT"] = "non traités";
$MESS["CRM_ENTITY_MERGER_RESULT_TITLE"] = "Résultat de la fusion";
$MESS["CRM_ENTITY_MERGER_UNRESOLVED_CONFLICTS_FOUND"] = "Certains conflits n'ont pu être résolus. Veuillez sélectionner les bonnes options.";
