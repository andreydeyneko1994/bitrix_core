<?php
$MESS["CRM_ENTITY_MERGER_ENTITIES_NOT_FOUND"] = "Nada para mesclar aqui.";
$MESS["CRM_ENTITY_MERGER_GO_TO_DUPLICATE_LIST"] = "Voltar para a lista";
$MESS["CRM_ENTITY_MERGER_MARK_AS_NON_DUPLICATE"] = "não é um duplicado";
$MESS["CRM_ENTITY_MERGER_MERGE_AND_EDIT"] = "Mesclar e editar";
$MESS["CRM_ENTITY_MERGER_OPEN_ENTITY"] = "abrir";
$MESS["CRM_ENTITY_MERGER_POSTPONE"] = "Depois";
$MESS["CRM_ENTITY_MERGER_PRIMARY_ENTITY_NOT_FOUND"] = "Por favor, selecione o registro principal a mesclar dados duplicados.";
$MESS["CRM_ENTITY_MERGER_PROCESS"] = "Mesclar";
$MESS["CRM_ENTITY_MERGER_PROCESSED_AMOUNT"] = "mesclados";
$MESS["CRM_ENTITY_MERGER_REMAINING_AMOUNT"] = "não processados";
$MESS["CRM_ENTITY_MERGER_RESULT_TITLE"] = "Mesclar resultado";
$MESS["CRM_ENTITY_MERGER_UNRESOLVED_CONFLICTS_FOUND"] = "Houve conflitos que não puderam ser resolvidos. Por favor, selecione as opções corretas.";
