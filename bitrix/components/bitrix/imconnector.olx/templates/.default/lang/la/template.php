<?php
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP_NEW"] = "Debe #LINK_START#crear una cuenta#LINK_END# o usar la que ya tiene. Si no tiene una cuenta, lo ayudaremos a crearla y conectarla a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Debe #LINK_START#crear una cuenta de OLX#LINK_END# o usar la que ya tiene. Si no tiene una cuenta, lo ayudaremos a crearla rápidamente y a conectarla a su Bitrix24.";
