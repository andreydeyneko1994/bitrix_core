<?
$MESS["CRM_TRACKING_SOURCE_UTM_ADDITIONAL_GOOGLE"] = "Parámetros UTM adicionales para Google Adwords";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN"] = "Nombre de la campaña publicitaria";
$MESS["CRM_TRACKING_SOURCE_UTM_CAMPAIGN_DESC_GOOGLE"] = "AdWords reemplazará a {network} con \"g\" (búsqueda en google), \"s\" (socio de búsqueda) o \"d\" (mostrar el organizador). Por ejemplo, niños_clothes, cheap_shoes etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_DESC_GOOGLE"] = "Valores posibles: e (coincidencia exacta), p (coincidencia en la frase) o b (coincidencia amplia modificada)";
$MESS["CRM_TRACKING_SOURCE_UTM_MATCHTYPE_GOOGLE"] = "Tipo de concordancia de la palabra clave";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM"] = "Tipo de campaña publicitaria";
$MESS["CRM_TRACKING_SOURCE_UTM_MEDIUM_DESC"] = "Tipo de tráfico. Por ejemplo, cpc, correo electrónico, pantalla, precio, redireccionamiento, afiliado, social, especial, etc.";
$MESS["CRM_TRACKING_SOURCE_UTM_NAME"] = "Nombre del conjunto de parámetros UTM";
$MESS["CRM_TRACKING_SOURCE_UTM_OBLIGATORY_FIELDS"] = "Parámetros requeridos";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT"] = "Colocación";
$MESS["CRM_TRACKING_SOURCE_UTM_PLACEMENT_DESC_GOOGLE"] = "Mostrar solo el organizador: dirección de la ubicación";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION"] = "Posición del anuncio";
$MESS["CRM_TRACKING_SOURCE_UTM_POSITION_DESC_GOOGLE"] = "Valores posibles: 1t2 (página 1, en la parte superior de la página, ubicación 2), 1s3 (página 1, en la página derecha, ubicación 3) o ninguno (mostrar el organizador)";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME"] = "Introduzca la URL del sitio web";
$MESS["CRM_TRACKING_SOURCE_UTM_SITE_NAME_DESC"] = "Página de destino a la que se redireccionará a un visitante";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE"] = "Fuente de campaña publicitaria";
$MESS["CRM_TRACKING_SOURCE_UTM_SOURCE_DESC"] = "Haga clic en la fuente. Por ejemplo, Instagram, Google, correo electrónico, boletín";
?>