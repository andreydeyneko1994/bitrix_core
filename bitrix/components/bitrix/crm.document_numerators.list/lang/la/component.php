<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_COLUMN_NUMERATOR_LIST_ID"] = "ID";
$MESS["CRM_COLUMN_NUMERATOR_LIST_NAME"] = "Nombre de la plantilla de numeración automática";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE"] = "Plantilla de número";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TEMPLATE_NAME"] = "Plantillas que utilizan esta plantilla de numeración";
$MESS["CRM_COLUMN_NUMERATOR_LIST_TYPE"] = "Tipo";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_NUMERATOR_LIST_CREATE_NUMERATOR"] = "Creare plantilla de numeración automática";
$MESS["CRM_NUMERATOR_LIST_DELETE"] = "Eliminar plantilla de numeración automática";
$MESS["CRM_NUMERATOR_LIST_DELETE_CONFIRM"] = "¿Usted está seguro que quiere eliminar \"%s\"?";
$MESS["CRM_NUMERATOR_LIST_DELETE_ERROR"] = "No se puede eliminar la plantilla de numeración automática \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_DELETE_TITLE"] = "Eliminar esta plantilla de numeración automática";
$MESS["CRM_NUMERATOR_LIST_EDIT"] = "Editar";
$MESS["CRM_NUMERATOR_LIST_EDIT_ERROR"] = "No se puede guardar la plantilla de numeración automática \"#NUMERATOR_NAME#\"";
$MESS["CRM_NUMERATOR_LIST_EDIT_TITLE"] = "Editar esta plantilla de numeración automática";
$MESS["CRM_NUMERATOR_LIST_FILTER_ENTITIES"] = "Enlace a la sección de CRM";
$MESS["CRM_NUMERATOR_LIST_INTERNAL_ERROR_TITLE"] = "Un error desconocido ocurrió.";
$MESS["CRM_NUMERATOR_LIST_PAGE_TITLE"] = "Plantillas de documentos de numeración automática";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["DOCUMENTGENERATOR_MODULE_NOT_INSTALLED"] = "El módulo Generador de documentos no está instalado.";
?>