<?php
$MESS["CRM_ML_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_ML_BUTTON_DISABLE"] = "Desativar";
$MESS["CRM_ML_CONFIRMATION"] = "Confirmar ação";
$MESS["CRM_ML_DEAL_FORECAST"] = "Previsão de negócios";
$MESS["CRM_ML_DEAL_SCORING_DISABLE"] = "Desativar pontuação de negócios para este pipeline";
$MESS["CRM_ML_DEAL_SUCCESS_PROBABILITY"] = "Probabilidade de sucesso";
$MESS["CRM_ML_DISABLE_DEAL_SCORING"] = "Desativar pontuação de negócios para este pipeline?";
$MESS["CRM_ML_DISABLE_LEAD_SCORING"] = "Desativar pontuação de leads?";
$MESS["CRM_ML_FEEDBACK"] = "Feedback";
$MESS["CRM_ML_FORECAST_DYNAMICS"] = "Dinâmica de previsão";
$MESS["CRM_ML_HELP"] = "Ajuda";
$MESS["CRM_ML_INFLUENCING_EVENT"] = "Eventos de influência";
$MESS["CRM_ML_LEAD_FORECAST"] = "Previsão de leads";
$MESS["CRM_ML_LEAD_SCORING_DISABLE"] = "Desativar pontuação de leads";
$MESS["CRM_ML_LEAD_SUCCESS_PROBABILITY"] = "Conversão para probabilidade de negócios";
$MESS["CRM_ML_MODEL_EVENT_UPDATE_DEAL"] = "Campos de negócio alterados";
$MESS["CRM_ML_MODEL_EVENT_UPDATE_LEAD"] = "Campos de Lead alterados";
$MESS["CRM_ML_MODEL_FAILED_DEALS_IN_TRAINING"] = "Negócios mal sucedidos na série de treinamento";
$MESS["CRM_ML_MODEL_FAILED_LEADS_IN_TRAINING"] = "Leads mal sucedidos na série de treinamento";
$MESS["CRM_ML_MODEL_FUTURE_DEAL_FORECAST"] = "Você verá a previsão nas informações de negócio assim que o modelo estiver pronto";
$MESS["CRM_ML_MODEL_FUTURE_LEAD_FORECAST"] = "Você verá a previsão nas informações de lead assim que o modelo estiver pronto";
$MESS["CRM_ML_MODEL_MODEL_WILL_BE_TRAINED_AGAIN"] = "Próximo treinamento em";
$MESS["CRM_ML_MODEL_MODEL_WILL_BE_TRAINED_IN_DAYS"] = "#DAYS# dias";
$MESS["CRM_ML_MODEL_NO_EVENTS_YET_DEAL"] = "Os eventos de influência serão mostrados quando o negócio ou as atividades de negócio mudarem";
$MESS["CRM_ML_MODEL_NO_EVENTS_YET_LEAD"] = "Os eventos de influência serão mostrados quando o lead ou as atividades de lead mudarem";
$MESS["CRM_ML_MODEL_QUALITY"] = "Precisão do modelo de previsão";
$MESS["CRM_ML_MODEL_QUALITY_HIGH"] = "Alta";
$MESS["CRM_ML_MODEL_QUALITY_LOW"] = "Baixa";
$MESS["CRM_ML_MODEL_QUALITY_MEDIUM"] = "Média";
$MESS["CRM_ML_MODEL_SUCCESSFUL_DEALS_IN_TRAINING"] = "Negócios bem sucedidos na série de treinamento";
$MESS["CRM_ML_MODEL_SUCCESSFUL_LEADS_IN_TRAINING"] = "Leads bem sucedidos na série de treinamento";
$MESS["CRM_ML_MODEL_TRAINING_DEALS"] = "Agora, a IA está analisando o seu CRM para prever futuros negócios";
$MESS["CRM_ML_MODEL_TRAINING_LEADS"] = "Agora, a IA está analisando o seu CRM para prever futuros leads";
$MESS["CRM_ML_SCORE_BALLOON"] = "#SCORE# (#DATE#)";
$MESS["CRM_ML_SCORING_CAN_START_TRAINING"] = "A previsão de negócios exige que você treine o modelo IA usando os negócios atuais concluídos.";
$MESS["CRM_ML_SCORING_DESCRIPTION_P1"] = "Quais são os critérios usados pelo seu gerente de vendas ao lidar com os negócios? Fila? Intuição? Tudo o que você precisa é captar e levar seus clientes mais promissores ao topo da sua lista.";
$MESS["CRM_ML_SCORING_DESCRIPTION_P2_2"] = "A pontuação IA analisará os atuais negócios para mostrar suas probabilidades de sucesso. O sistema ajudará seus funcionários a identificar áreas que exigem mais atenção. Não perca seu tempo com quem não tem intenção de se tornar seu cliente!";
$MESS["CRM_ML_SCORING_DESCRIPTION_TITLE_2"] = "Pontuação IA";
$MESS["CRM_ML_SCORING_ERROR_TOO_SOON_2"] = "A pontuação IA foi desativada conforme sua solicitação. Você poderá treinar o modelo IA e ativar a pontuação após #DATE#.";
$MESS["CRM_ML_SCORING_MODEL_QUALITY_HINT"] = "Saiba como melhorar a precisão da previsão";
$MESS["CRM_ML_SCORING_NOT_ENOUGH_DATA"] = "A Pontuação IA usa dados existentes. São necessários pelo menos 2000 negócios para fazer uma previsão. Avisaremos quando você tiver dados suficientes.";
$MESS["CRM_ML_SCORING_PREDICTION_HINT"] = "Saiba mais sobre este número";
$MESS["CRM_ML_SCORING_REENABLE_WARNING"] = "Você não poderá reativar a pontuação até #DATE#";
$MESS["CRM_ML_SCORING_TRAIN_FREE_OF_CHARGE"] = "Treine grátis";
$MESS["CRM_ML_SUCCESS_PROBABILITY_HIGH"] = "Alta";
$MESS["CRM_ML_SUCCESS_PROBABILITY_LOW"] = "Baixa";
$MESS["CRM_ML_SUCCESS_PROBABILITY_MEDIUM"] = "Média";
