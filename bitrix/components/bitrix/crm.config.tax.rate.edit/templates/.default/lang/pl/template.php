<?
$MESS["CRM_ANY"] = "Wszystkie";
$MESS["CRM_TAXRATE_FIELDS_ACTIVE"] = "Aktywny";
$MESS["CRM_TAXRATE_FIELDS_APPLY_ORDER"] = "Kolejność aplikacji";
$MESS["CRM_TAXRATE_FIELDS_IS_IN_PRICE"] = "Zawrzyj podatek w cenie";
$MESS["CRM_TAXRATE_FIELDS_LOCATION1"] = "Lokalizacje";
$MESS["CRM_TAXRATE_FIELDS_LOCATION2"] = "Grupy lokalizacji";
$MESS["CRM_TAXRATE_FIELDS_PERSON_TYPE_ID"] = "Rodzaj klienta";
$MESS["CRM_TAXRATE_FIELDS_TAX"] = "Podatek";
$MESS["CRM_TAXRATE_FIELDS_VALUE"] = "Stawka";
$MESS["CRM_TAXRATE_FIELDS_VALUE_CHECK"] = "Stawką podatku może być tylko liczba.";
$MESS["CRM_TAXRATE_SAVE_BUTTON"] = "Zapisz";
$MESS["CRM_TAXRATE_TITLE"] = "Dodaj stawkę podatku";
$MESS["CRM_TAXRATE_TITLE_EDIT"] = "Edytuj Stawkę Podatku";
?>