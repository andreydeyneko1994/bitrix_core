<?
$MESS["CRM_BPLIST"] = "Proces Biznesowy";
$MESS["CRM_BP_R_P"] = "Proces Biznesowy";
$MESS["CRM_BP_R_P_TITLE"] = "Wyświetl procesy biznesowe dla oferty";
$MESS["CRM_COLUMN_ALL"] = "(Wszystko)";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Osoba odpowiedzialna";
$MESS["CRM_COLUMN_BEGINDATE"] = "Data faktury";
$MESS["CRM_COLUMN_BINDING"] = "Powiązania";
$MESS["CRM_COLUMN_CLIENT"] = "Klient";
$MESS["CRM_COLUMN_CLOSED"] = "Zamknięte";
$MESS["CRM_COLUMN_CLOSEDATE"] = "Data wygaśnięcia";
$MESS["CRM_COLUMN_COMMENTS"] = "Komentarz przedstawiciela handlowego";
$MESS["CRM_COLUMN_COMPANY_ID"] = "Firma";
$MESS["CRM_COLUMN_COMPANY_LIST"] = "Firmy";
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Firma";
$MESS["CRM_COLUMN_CONTACT_FULL_NAME"] = "Kontakt";
$MESS["CRM_COLUMN_CONTACT_ID"] = "Kontakt";
$MESS["CRM_COLUMN_CONTACT_LIST"] = "Kontakty";
$MESS["CRM_COLUMN_CREATED_BY"] = "Utworzony przez";
$MESS["CRM_COLUMN_CURRENCY_ID"] = "Waluta";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Utworzony";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_COLUMN_DEAL_ID"] = "Deal";
$MESS["CRM_COLUMN_DEAL_LIST"] = "Deale";
$MESS["CRM_COLUMN_DEAL_TITLE"] = "Deal";
$MESS["CRM_COLUMN_ENTITIES_LINKS"] = "Powiązane z";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_LEAD_ID"] = "Lead";
$MESS["CRM_COLUMN_LEAD_LIST"] = "Leady";
$MESS["CRM_COLUMN_LEAD_TITLE"] = "Lead";
$MESS["CRM_COLUMN_MODIFY_BY"] = "Zmodyfikowane przez";
$MESS["CRM_COLUMN_MYCOMPANY_ID"] = "Dane twojej firmy";
$MESS["CRM_COLUMN_MYCOMPANY_ID1"] = "Moje dane firmy";
$MESS["CRM_COLUMN_MYCOMPANY_LIST"] = "Lista firm";
$MESS["CRM_COLUMN_MYCOMPANY_TITLE"] = "Dane twojej firmy";
$MESS["CRM_COLUMN_MYCOMPANY_TITLE1"] = "Moje dane firmy";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Razem";
$MESS["CRM_COLUMN_PRODUCT"] = "Produkt";
$MESS["CRM_COLUMN_PRODUCT_ID"] = "Produkt";
$MESS["CRM_COLUMN_QUOTE"] = "Oferta";
$MESS["CRM_COLUMN_QUOTE_NUMBER"] = "Liczba";
$MESS["CRM_COLUMN_STATUS_ID"] = "Status";
$MESS["CRM_COLUMN_SUM"] = "Kwota/Waluta";
$MESS["CRM_COLUMN_TITLE"] = "Temat";
$MESS["CRM_COLUMN_WEBFORM"] = "Utworzone przez formularz CRM";
$MESS["CRM_FILTER_MYCOMPANY_TITLE"] = "Nazwa twojej firmy";
$MESS["CRM_INTERNAL"] = "CRM";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRESET_CHANGE_MY"] = "Zmodyfikowane przeze mnie";
$MESS["CRM_PRESET_CHANGE_TODAY"] = "Zmodyfikowane Dziś";
$MESS["CRM_PRESET_CHANGE_YESTERDAY"] = "Zmodyfikowane wczoraj";
$MESS["CRM_PRESET_MY"] = "Moje oferty";
$MESS["CRM_PRESET_MY_IN_WORK"] = "Moje leady w toku";
$MESS["CRM_PRESET_NEW"] = "Nowe oferty";
$MESS["CRM_QUOTE_NAV_TITLE_LIST"] = "Wyświetl wszystkie oferty";
$MESS["CRM_QUOTE_NAV_TITLE_LIST_SHORT"] = "Oferty";
$MESS["CRM_TASKS"] = "Zadania";
$MESS["CRM_TASK_TAG"] = "crm";
$MESS["CRM_TASK_TITLE_PREFIX"] = "CRM:";
?>