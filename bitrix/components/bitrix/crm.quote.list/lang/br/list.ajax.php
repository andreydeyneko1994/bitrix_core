<?
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "O índice de pesquisa de orçamentos foi recriado. Orçamentos processados: #PROCESSED_ITEMS#.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "O índice de pesquisa de orçamentos não precisa ser recriado.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Orçamentos processados: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_QUOTE_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
?>