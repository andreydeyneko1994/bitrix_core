<?php
$MESS["TASKS_MOBILE_EFFICIENCY_COMPLETED"] = "Tareas completadas";
$MESS["TASKS_MOBILE_EFFICIENCY_IN_PROGRESS"] = "Total de tareas";
$MESS["TASKS_MOBILE_EFFICIENCY_LIMIT_EXCEEDED"] = "¡Ahora su Bitrix24 tiene 100 tareas! La página \"Eficiencia\" está disponible solo en los planes principales.";
$MESS["TASKS_MOBILE_EFFICIENCY_VIOLATIONS"] = "Comentarios de la tarea";
