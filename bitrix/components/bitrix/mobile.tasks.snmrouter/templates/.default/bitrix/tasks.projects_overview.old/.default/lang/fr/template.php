<?php
$MESS["TASKS_PROJECTS"] = "Projets";
$MESS["TASKS_PROJECTS_OVERVIEW_NO_DATA"] = "Les projets ne comptent aucune tâche pour le moment";
$MESS["TASKS_PROJECTS_WITH_MY_MEMBERSHIP"] = "Projets avec ma participation";
