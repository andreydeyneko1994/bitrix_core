<?php
$MESS["TASK_COLUMN_ALLOW_CHANGE_DEADLINE"] = "Osoba odpowiedzialna może zmienić termin ostateczny";
$MESS["TASK_COLUMN_ALLOW_TIME_TRACKING"] = "śledzenie czasu wykonania";
$MESS["TASK_COLUMN_CHANGED_DATE"] = "Zmodyfikowano";
$MESS["TASK_COLUMN_CLOSED_DATE"] = "Zakończone";
$MESS["TASK_COLUMN_CREATED_BY"] = "Stworzone przez";
$MESS["TASK_COLUMN_CREATED_DATE"] = "Utworzone";
$MESS["TASK_COLUMN_DEADLINE"] = "Termin ostateczny";
$MESS["TASK_COLUMN_FAVORITES"] = "Ulubione";
$MESS["TASK_COLUMN_GROUP_ID"] = "Grupa";
$MESS["TASK_COLUMN_MARK"] = "Ocena";
$MESS["TASK_COLUMN_PRIORITY"] = "Priorytet";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["TASK_COLUMN_STATUS"] = "Status";
$MESS["TASK_COLUMN_TIME_ESTIMATE"] = "Wymagany szacowany czas";
$MESS["TASK_COLUMN_TIME_SPENT_IN_LOGS"] = "Czas spędzony";
