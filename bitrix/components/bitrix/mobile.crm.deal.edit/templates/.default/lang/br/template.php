<?
$MESS["M_CRM_DEAL_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_DEAL_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_DEAL_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_DEAL_EDIT_CONVERT_TITLE"] = "Negócios";
$MESS["M_CRM_DEAL_EDIT_CREATE_TITLE"] = "Novo negócio";
$MESS["M_CRM_DEAL_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_DEAL_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_DEAL_EDIT_VIEW_TITLE"] = "Ver negócio";
$MESS["M_CRM_DEAL_MENU_ACTIVITY"] = "Atividades";
$MESS["M_CRM_DEAL_MENU_CREATE_ON_BASE"] = "Criar usando fonte";
$MESS["M_CRM_DEAL_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_DEAL_MENU_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_MENU_HISTORY"] = "Histórico";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>