<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "Hay elementos activos en esta etapa. Mueva las negociaciones antes de eliminar la etapa.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "Hay elementos activos en esta etapa. Mueva las facturas antes de eliminar la etapa.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "Hay elementos activos en esta etapa. Mueva las cotizaciones antes de eliminar la etapa.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = "Hay elementos activos en esta etapa. Mueva los prospectos antes de eliminar la etapa.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Eliminar la etapa";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Eliminar el estado";
?>