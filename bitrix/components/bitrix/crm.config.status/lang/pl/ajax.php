<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "W tym etapie są aktywne elementy. Przenieś deale przed usunięciem etapu.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "W tym statusie są aktywne elementy. Przenieś faktury przed usunięciem statusu.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "W tym statusie są aktywne elementy. Przenieś oferty przed usunięciem statusu.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = "W tym statusie są aktywne elementy. Przenieś leady przed usunięciem statusu.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Usuń etap";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Usuń status";
?>