<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "Il y a des éléments actifs dans cette étape. Déplacez les transactions avant de supprimer l'étape.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "Il y a des éléments actifs dans ce statut. Déplacez les factures avant de supprimer le statut.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "Il y a des éléments actifs dans ce statut. Déplacez les devis avant de supprimer le statut.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = " Il y a des éléments actifs dans ce statut. Déplacez les prospects avant de supprimer le statut.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Supprimer l'étape";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Supprimer le statut";
?>