<?
$MESS["CRM_STATUS_DELETION_ALERT_DEAL_STATUS"] = "Existem itens ativos nesta fase. Mova os negócios antes de excluir a fase.";
$MESS["CRM_STATUS_DELETION_ALERT_INVOICE_STATUS"] = "Existem itens ativos nesta fase. Mova as faturas antes de excluir a fase.";
$MESS["CRM_STATUS_DELETION_ALERT_QUOTE_STATUS"] = "Existem itens ativos nesta fase. Mova os orçamentos antes de excluir a fase.";
$MESS["CRM_STATUS_DELETION_ALERT_STATUS"] = "Existem itens ativos nesta fase. Mova os leads antes de excluir a fase.";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_DEAL_STATUS"] = "Excluir fase";
$MESS["CRM_STATUS_DELETION_ALERT_TITLE_STATUS"] = "Excluir status";
?>