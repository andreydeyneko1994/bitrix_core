<?
$MESS["TASKS_CANCEL"] = "Anuluj";
$MESS["TASKS_FILTER"] = "Filtr";
$MESS["TASKS_FILTER_ACTIVE_DATE"] = "Był aktywny";
$MESS["TASKS_FILTER_ADV_IN_REPORT"] = "w raporcie";
$MESS["TASKS_FILTER_BY_TAG"] = "Przez Tag";
$MESS["TASKS_FILTER_CLOSE_DATE"] = "Zamknięte";
$MESS["TASKS_FILTER_COMMON"] = "Normalny";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Utworzony";
$MESS["TASKS_FILTER_EXTENDED"] = "Rozszerzone";
$MESS["TASKS_FILTER_FIND"] = "Szukaj";
$MESS["TASKS_FILTER_ID"] = "ID";
$MESS["TASKS_FILTER_MARKED"] = "Oceniony jako";
$MESS["TASKS_FILTER_OVERDUED"] = "po terminie";
$MESS["TASKS_FILTER_PICK_DATE"] = "Wybierz datę w kalendarzu";
$MESS["TASKS_FILTER_SELECT"] = "wybierz";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "Pokaż zadania podwładnych";
$MESS["TASKS_FILTER_STATUS"] = "Status";
$MESS["TASKS_FILTER_STATUSES"] = "Statusy";
$MESS["TASKS_FILTER_WORKGROUP"] = "Grupa robocza";
?>