<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "Llamadas perdidas";
$MESS["TELEPHONY_REPORT_LOST_CALLS_COUNT"] = "Llamadas sin contestar";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DATE"] = "Fecha";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DYNAMICS"] = "Cambio vs. periodo anterior";
