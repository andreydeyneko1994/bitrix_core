<?
$MESS["TASKS_COLUMN_CREATED_DATE"] = "Fecha del inconveniente";
$MESS["TASKS_COLUMN_DINAMIC"] = "Dinámica";
$MESS["TASKS_COLUMN_EFFECTIVE"] = "Eficiencia";
$MESS["TASKS_COLUMN_ID"] = "ID";
$MESS["TASKS_COLUMN_MORE"] = "Más";
$MESS["TASKS_COLUMN_USER"] = "Empleado";
$MESS["TASKS_EFFECTIVE_DATE_FORMAT"] = "#DAY# #MONTH_NAME# #YEAR_IF_DIFF#";
$MESS["TASKS_FILTER_COLUMN_DATE"] = "Período";
$MESS["TASKS_FILTER_COLUMN_GROUP_ID"] = "Proyecto/Grupo";
$MESS["TASKS_FILTER_COLUMN_KPI"] = "Eficiencia";
$MESS["TASKS_MONTH_1"] = "Enero";
$MESS["TASKS_MONTH_10"] = "Octubre";
$MESS["TASKS_MONTH_11"] = "Noviembre";
$MESS["TASKS_MONTH_12"] = "Diciembre";
$MESS["TASKS_MONTH_2"] = "Febrero";
$MESS["TASKS_MONTH_3"] = "Marzo";
$MESS["TASKS_MONTH_4"] = "Abril";
$MESS["TASKS_MONTH_5"] = "Mayo";
$MESS["TASKS_MONTH_6"] = "Junio";
$MESS["TASKS_MONTH_7"] = "Julio";
$MESS["TASKS_MONTH_8"] = "Agosto";
$MESS["TASKS_MONTH_9"] = "Septiembre";
$MESS["TASKS_PRESET_CURRENT_DAY"] = "Día actual";
$MESS["TASKS_PRESET_CURRENT_MONTH"] = "Mes actual";
$MESS["TASKS_PRESET_CURRENT_QUARTER"] = "Trimestre actual";
$MESS["TASKS_PRESET_CURRENT_YEAR"] = "Año actual";
?>