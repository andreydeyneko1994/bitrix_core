<?
$MESS["TASKS_ADD_TASK"] = "Criar tarefa";
$MESS["TASKS_CIRCLE_EFFECTIVE_TITLE"] = "Eficiência";
$MESS["TASKS_COMPLETED"] = "Tarefa concluída";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "Como funciona?";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.com.br/open/6841647/";
$MESS["TASKS_EFFECTIVE_MORE"] = "Informações";
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "Eficiência";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "Eficiência";
$MESS["TASKS_IN_PROGRESS"] = "Total em andamento";
$MESS["TASKS_MORE_LINK_TEXT"] = "Informações";
$MESS["TASKS_MY_EFFECTIVE"] = "Minha Eficiência";
$MESS["TASKS_MY_EFFECTIVE_BY_DAY"] = "Eficiência diária";
$MESS["TASKS_MY_EFFECTIVE_FROM"] = "Estatísticas coletadas desde <strong>#DATE#</strong>";
$MESS["TASKS_NO_DATA_TEXT"] = "Não há dados";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE_MORE"] = "Quão eficiente você é?<br/>
<br/>
O Bitrix24 acompanha todas as tarefas designadas para medir quantas delas foram concluídas no prazo e sem objeções.<br/>
<br/>
O Indicador de Eficiência oferece vários benefícios. Primeiro, ajuda você a entender o desempenho individual e do grupo. Em segundo lugar, qualquer coisa que seja medida é mais fácil de melhorar. Por fim, você pode basear seu KPI nesse indicador e tomar decisões inteligentes sobre quem é promovido e recompensado.<br/>  
<br/>
Saiba mais <a href=\"https://helpdesk.bitrix24.com/open/6808471/\">aqui</a> 
";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "Eficiência, %";
$MESS["TASKS_VIOLATION"] = "Tarefas com objeções";
?>