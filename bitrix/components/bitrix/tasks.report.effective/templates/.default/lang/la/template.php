<?
$MESS["TASKS_ADD_TASK"] = "Crear tarea";
$MESS["TASKS_CIRCLE_EFFECTIVE_TITLE"] = "Eficiencia";
$MESS["TASKS_COMPLETED"] = "Tareas completadas";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "¿Cómo funciona?";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.com/open/6808471/";
$MESS["TASKS_EFFECTIVE_MORE"] = "Detalles";
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "Eficiencia";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "Eficiencia";
$MESS["TASKS_IN_PROGRESS"] = "Total en progreso";
$MESS["TASKS_MORE_LINK_TEXT"] = "Detalles";
$MESS["TASKS_MY_EFFECTIVE"] = "Mi Eficiencia";
$MESS["TASKS_MY_EFFECTIVE_BY_DAY"] = "Eficiencia diaria";
$MESS["TASKS_MY_EFFECTIVE_FROM"] = "Estadísticas recopiladas desde <strong>#DATE#</strong>";
$MESS["TASKS_NO_DATA_TEXT"] = "Sin datos";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE_MORE"] = "¿Qué tan eficiente eres?<br/>
<br/>
Bitrix24 sigue todas las tareas asignadas para medir cuántas de ellas se completaron a tiempo y sin inconveniente.<br/>
<br/>
El indicador de Eficiencia le ofrece una serie de beneficios. En primer lugar, le ayuda a comprender el rendimiento individual y grupal. En segundo lugar, cualquier cosa que se mida es más fácil de mejorar. Finalmente, puede basar su KPI en este indicador y tomar decisiones inteligentes sobre quién es promovido y recompensado.<br/>  
<br/>
Leer más <a href=\"https://helpdesk.bitrix24.com/open/6808471/\">aquí</a> ";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "Eficiencia,%";
$MESS["TASKS_VIOLATION"] = "Tareas con inconvenientes";
?>