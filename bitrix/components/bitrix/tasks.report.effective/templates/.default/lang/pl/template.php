<?
$MESS["TASKS_ADD_TASK"] = "Utwórz zadanie";
$MESS["TASKS_CIRCLE_EFFECTIVE_TITLE"] = "Wydajność";
$MESS["TASKS_COMPLETED"] = "Ukończone zadania";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "Jak to działa?";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.com/open/6808471/";
$MESS["TASKS_EFFECTIVE_MORE"] = "Szczegóły";
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "Wydajność";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "Wydajność";
$MESS["TASKS_IN_PROGRESS"] = "Łącznie w toku";
$MESS["TASKS_MORE_LINK_TEXT"] = "Szczegóły";
$MESS["TASKS_MY_EFFECTIVE"] = "Moja wydajność";
$MESS["TASKS_MY_EFFECTIVE_BY_DAY"] = "Wydajność dzienna";
$MESS["TASKS_MY_EFFECTIVE_FROM"] = "Statystyki zebrane od <strong>#DATE#</strong>";
$MESS["TASKS_NO_DATA_TEXT"] = "Brak danych";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE_MORE"] = "Jaka jest twoja wydajność?<br/>
<br/>
Bitrix24 śledzi wszystkie przydzielone zadania, aby zmierzyć, ile z nich zostało ukończonych na czas i bez zastrzeżeń.<br/>
<br/>
Wydajność ma wiele zalet jako wskaźnik. Po pierwsze pomaga zrozumieć wydajność indywidualną i grupową. Po drugie łatwiej jest pracować nad czymś, co zostało zmierzone. Po trzecie w oparciu o ten wskaźnik można obliczać KPI i podejmować inteligentne decyzje co do awansów i premii.<br/>  
<br/>
Więcej informacji można znaleźć <a href=\"https://helpdesk.bitrix24.com/open/6808471/\">tutaj</a> 
";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "Wydajność, %";
$MESS["TASKS_VIOLATION"] = "Zadania z zastrzeżeniami";
?>