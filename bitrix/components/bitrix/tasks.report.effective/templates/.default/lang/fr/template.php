<?
$MESS["TASKS_ADD_TASK"] = "Créer une tâche";
$MESS["TASKS_CIRCLE_EFFECTIVE_TITLE"] = "Efficacité";
$MESS["TASKS_COMPLETED"] = "Tâches accomplies";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "Comment cela fonctionne ?";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.fr/open/8460121/";
$MESS["TASKS_EFFECTIVE_MORE"] = "Détails";
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "Efficacité";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "Efficacité";
$MESS["TASKS_IN_PROGRESS"] = "Total en cours";
$MESS["TASKS_MORE_LINK_TEXT"] = "Détails";
$MESS["TASKS_MY_EFFECTIVE"] = "Mon efficacité";
$MESS["TASKS_MY_EFFECTIVE_BY_DAY"] = "Efficacité journalière";
$MESS["TASKS_MY_EFFECTIVE_FROM"] = "Les statistiques sont collectées depuis le <strong>#DATE#</strong>";
$MESS["TASKS_NO_DATA_TEXT"] = "Pas de données";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE_MORE"] = "À quel point vous êtes efficace ?<br/>
<br/>
Bitrix24 surveille toutes les tâches affectées pour mesure combien sont accomplies à temps et sans objection.<br/>
<br/>
L'indicateur d'efficacité vous apporte plusieurs avantages. Tout d'abord, il vous permet de comprendre les performances d'un individu ou d'un groupe. Ensuite, tout ce qui est mesuré est facile à améliorer. Enfin, vous pouvez baser votre KPI sur cet indicateur et prendre des décisions intelligentes au moment d'accorder une promotion ou une récompense.<br/>  
<br/>
Plus d'informations <a href=\"https://helpdesk.bitrix24.fr/open/8460121/\">ici</a>";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "Efficacité, %";
$MESS["TASKS_VIOLATION"] = "Tâches avec objections";
?>