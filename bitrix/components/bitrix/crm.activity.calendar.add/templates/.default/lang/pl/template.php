<?
$MESS["BX_CRM_CACA_PRIORITY"] = "Priorytet zdarzenia";
$MESS["BX_CRM_CACA_PRIORITY_HIGH"] = "Wysoki";
$MESS["BX_CRM_CACA_PRIORITY_LOW"] = "Niski";
$MESS["BX_CRM_CACA_PRIORITY_NORMAL"] = "Normalny";
$MESS["BX_CRM_CACA_REM_DAY"] = "dni";
$MESS["BX_CRM_CACA_REM_HOUR"] = "godziny";
$MESS["BX_CRM_CACA_REM_MIN"] = "minut(y)";
$MESS["CRM_CALENDAR_ADD_BUTTON"] = "Dodaj";
$MESS["CRM_CALENDAR_ADD_TITLE"] = "Dodaj połączenie telefoniczne lub wydarzenie do kalendarza";
$MESS["CRM_CALENDAR_DATE"] = "Data I Czas";
$MESS["CRM_CALENDAR_DESC"] = "Opis";
$MESS["CRM_CALENDAR_DESC_TITLE"] = "Wprowadź komentarz";
$MESS["CRM_CALENDAR_REMIND"] = "Przypomnij o wydarzeniu";
$MESS["CRM_CALENDAR_REMIND_FROM"] = "w";
$MESS["CRM_CALENDAR_TOPIC"] = "Temat";
?>