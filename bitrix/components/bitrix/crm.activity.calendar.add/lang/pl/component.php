<?
$MESS["BX_CRM_CACA_ERR_ADD_FAIL"] = "Błąd podczas dodawania nowej pozycji kalendarza";
$MESS["BX_CRM_CACA_ERR_DATE_COMPARE"] = "Data rozpoczęcia jest późniejsza niż data zakończenia";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_ABSENT"] = "Data rozpoczęcia nie jest określona";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_FORMAT"] = "Nieprawidłowy format daty rozpoczęcia";
$MESS["BX_CRM_CACA_ERR_DATE_TO_FORMAT"] = "Nieprawidłowy format daty zakończenia";
$MESS["BX_CRM_CACA_ERR_TOPIC_ABSENT"] = "Temat nie jest określony.";
?>