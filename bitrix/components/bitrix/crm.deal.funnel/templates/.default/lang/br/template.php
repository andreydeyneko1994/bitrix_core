<?
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "Pipeline";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "Tipo de Funil";
$MESS["DEAL_STAGES_LOSE"] = "Não Ativo";
$MESS["DEAL_STAGES_WON"] = "Atualmente ativo";
$MESS["FUNNEL_CHART_HIDE"] = "Ocultar gráfico";
$MESS["FUNNEL_CHART_NO_DATA"] = "Não há dados para criar gráfico";
$MESS["FUNNEL_CHART_SHOW"] = "Mostrar gráfico";
?>