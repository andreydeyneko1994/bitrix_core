<?
$MESS["SCP_ACCESS_DENIED"] = "Accès refusé.";
$MESS["SPP_SMSPROVIDER_APP_INTEGRATION_REQUIRED"] = "Besoin d'une intégration ?";
$MESS["SPP_SMSPROVIDER_APP_RECOMMEND"] = "Recommandation";
$MESS["SPP_SMSPROVIDER_APP_SEE_ALL"] = "Tout afficher";
$MESS["SPP_SMSPROVIDER_APP_TOTAL_APPLICATIONS"] = "Total des applications";
$MESS["SPP_SMSPROVIDER_SENDER_SETTINGS"] = "Configurer #SENDER_NAME#";
?>