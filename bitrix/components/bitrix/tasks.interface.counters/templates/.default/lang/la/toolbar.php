<?php
$MESS["TASKS_COUNTER_EXPIRED"] = "Vencido";
$MESS["TASKS_COUNTER_MORE"] = "Más";
$MESS["TASKS_COUNTER_MY"] = "Mis elementos:";
$MESS["TASKS_COUNTER_MY_TASKS"] = "Mis tareas:";
$MESS["TASKS_COUNTER_NEW_COMMENTS"] = "Comentarios";
$MESS["TASKS_COUNTER_OTHER"] = "Otro:";
$MESS["TASKS_COUNTER_OTHER_TASKS"] = "Otras tareas: #TITLE#";
$MESS["TASKS_COUNTER_READ_ALL"] = "Marcar todo como leído";
