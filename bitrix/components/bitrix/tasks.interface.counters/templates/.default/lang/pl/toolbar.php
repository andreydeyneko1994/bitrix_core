<?php
$MESS["TASKS_COUNTER_EXPIRED"] = "Po terminie";
$MESS["TASKS_COUNTER_MORE"] = "Więcej";
$MESS["TASKS_COUNTER_MY"] = "Moje pozycje:";
$MESS["TASKS_COUNTER_MY_TASKS"] = "Moje zadania:";
$MESS["TASKS_COUNTER_NEW_COMMENTS"] = "Komentarze";
$MESS["TASKS_COUNTER_OTHER"] = "Inne:";
$MESS["TASKS_COUNTER_OTHER_TASKS"] = "Inne zadania: #TITLE#";
$MESS["TASKS_COUNTER_READ_ALL"] = "Oznacz wszystkie jako przeczytane";
