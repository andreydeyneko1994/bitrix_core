<?php
$MESS["TASKS_COUNTER_EXPIRED"] = "En retard";
$MESS["TASKS_COUNTER_MORE"] = "Plus";
$MESS["TASKS_COUNTER_MY"] = "Mes éléments :";
$MESS["TASKS_COUNTER_MY_TASKS"] = "Mes tâches :";
$MESS["TASKS_COUNTER_NEW_COMMENTS"] = "Commentaires";
$MESS["TASKS_COUNTER_OTHER"] = "Autre :";
$MESS["TASKS_COUNTER_OTHER_TASKS"] = "Autres tâches : #TITLE#";
$MESS["TASKS_COUNTER_READ_ALL"] = "Tout marquer comme lu";
