<?
$MESS["M_CRM_CONFIG_USER_EMAIL_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_CONFIG_USER_EMAIL_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_CONFIG_USER_EMAIL_INPUT_LEGEND"] = "Wprowadź e-mail";
$MESS["M_CRM_CONFIG_USER_EMAIL_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_CONFIG_USER_EMAIL_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_CONFIG_USER_EMAIL_SAVE_BTN"] = "Zapisz";
?>