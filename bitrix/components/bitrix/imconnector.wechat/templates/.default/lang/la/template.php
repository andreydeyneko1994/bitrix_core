<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ENCRYPT_KEY"] = "Clave de cifrado para el mensaje:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ID"] = "ID del desarrollador (AppID):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_SECRET"] = "Contraseña del desarrollador (AppSecret):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CHANGE_ANY_TIME"] = "Puede editarlo o desconectarse en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECTED"] = "WeChat está conectado ahora";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Me gustaría</div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectarme</span> a una cuenta de WeChat</div>";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP"] = "Necesita tener una cuenta oficial para conectarse. Si no tiene una, debe <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">crear una cuenta oficial</a>. Podemos ayudarlo a crear una cuenta oficial y conectarla a su cuenta de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP_NEW"] = "Debe #LINK_START#crear una cuenta oficial#LINK_END# o usar una existente para conectarse. Si no tiene una cuenta, lo ayudaremos a crearla y conectarla a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_TITLE"] = "Conectar WeChat a un canal abierto";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_FINAL_FORM_DESCRIPTION"] = "WeChat se conectó a su canal abierto. Todos los mensajes que se publiquen en su cuenta oficial serán redirigidos a su cuenta de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Cómo obtener información de una cuenta oficial</span>:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_LANG"] = "Seleccionar el idioma de la cuenta";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NAME_CHAT_LINK"] = "Enlace al chat en vivo";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SERVER_IP_ADDRESS_DESCRIPTION"] = "Especifique estos valores en el campo correspondiente a la IP de la lista blanca en el perfil de su cuenta oficial (cada valor se encuentra en una nueva línea):";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_TOKEN_DESCRIPTION"] = "Especifique este valor en el campo correspondiente al Token en el perfil de su cuenta oficial:";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_WEBHOOK_DESCRIPTION"] = "Especifique esta dirección en el campo correspondiente a la URL en el perfil de su cuenta oficial:";
