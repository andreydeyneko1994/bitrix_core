<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ENCRYPT_KEY"] = "Clé de chiffrement des messages :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ID"] = "ID de développeur (AppID) :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_SECRET"] = "Mot de passe de développeur (AppSecret) :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CHANGE_ANY_TIME"] = "Vous pouvez éditer ou déconnecter à tout moment";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECTED"] = "WeChat est maintenant connecté";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Je voudrais</div>
	<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">connecter</span> un compte WeChat</div>";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP"] = "Vous devez disposer d'un compte officiel pour vous connecter. Si vous n'en avez pas, vous devez <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">en créer un</a>. Nous pouvons vous aider à créer un compte officiel et à le connecter à votre compte Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP_NEW"] = "Vous devrez #LINK_START#créer un compte officiel#LINK_END# ou utiliser un compte existant pour vous connecter. Si vous n'avez pas de compte, nous vous aiderons à en créer un et à le connecter à votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_TITLE"] = "Connecter WeChat à un Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_COPY"] = "Copier";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_FINAL_FORM_DESCRIPTION"] = "WeChat a été connecté à votre Canal Ouvert. Tous les messages publiés sur votre compte officiel seront redirigés vers votre compte Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Comment récupérer les informations d'un compte officiel</span> :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_LANG"] = "Sélectionnez la langue du compte";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NAME_CHAT_LINK"] = "Lien de chat en direct";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SERVER_IP_ADDRESS_DESCRIPTION"] = "Précisez ces valeurs dans le champ Liste blanche des IP de votre profil de compte officiel (chaque valeur sur une nouvelle ligne) :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_TOKEN_DESCRIPTION"] = "Précisez cette valeur dans le champ Jeton de votre profil de compte officiel :";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_WEBHOOK_DESCRIPTION"] = "Précisez cette adresse dans le champ URL de votre profil de compte officiel :";
