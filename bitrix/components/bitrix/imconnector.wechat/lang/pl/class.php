<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_MODULE_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_CONNECT"] = "Nie udało się ustanowić połączenia testowego przy użyciu podanych parametrów";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_DATA_SAVE"] = "Brak danych do zapisania";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_REGISTER"] = "Błąd rejestracji";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_SAVE"] = "Wystąpił błąd podczas zapisywania danych. Sprawdź dane wejściowe i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_CONNECT"] = "Połączenie testowe zostało pomyślnie ustanowione";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_REGISTER"] = "Rejestracja zakończona powodzeniem";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
