<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_MODULE_NOT_INSTALLED"] = "El módulo “External IM Connectors” no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_CONNECT"] = "No pudo establecerse la prueba de conexión utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_REGISTER"] = "Se produjo un error en el registro";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NO_SAVE"] = "Se produjo un error al guardar los datos. Revise la entrada e intente nuevamente.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_CONNECT"] = "La prueba de conexión se estableció correctamente";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_REGISTER"] = "El registro se realizó correctamente";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_OK_SAVE"] = "La información se guardó correctamente.";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario nuevamente.";
