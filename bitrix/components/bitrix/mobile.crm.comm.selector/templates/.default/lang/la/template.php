<?
$MESS["M_CRM_COMM_SELECTOR_DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["M_CRM_COMM_SELECTOR_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_COMM_SELECTOR_NOTHING_FOUND"] = "No se han encontrado entradas.";
$MESS["M_CRM_COMM_SELECTOR_PULL_TEXT"] = "Tire hacia abajo para actualizar...";
$MESS["M_CRM_COMM_SELECT_SEARCH_BUTTON"] = "Búsqueda";
?>