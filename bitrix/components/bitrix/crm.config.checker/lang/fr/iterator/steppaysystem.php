<?
$MESS["SALE_IS_NOT_INSTALLED"] = "Le module e-Store (sale) n'est pas installé. Veuillez l'installer.";
$MESS["STEP_PAYSYSTEM_DESCRIPTION"] = "Connectez des systèmes de paiement et recevez des paiements en ligne";
$MESS["STEP_PAYSYSTEM_ERROR1"] = "Aucun service de paiement correctement configuré n'a été trouvé.";
$MESS["STEP_PAYSYSTEM_TITLE"] = "Recevez des paiements en ligne";
?>