<?
$MESS["IMCONNECTOR_IS_NOT_INSTALLED"] = "Le module « External IM Connectors » n'est pas installé. Veuillez l'installer.";
$MESS["IMOPENLINES_IS_NOT_INSTALLED"] = "Le module Canal ouvert (imopenlines) n'est pas installé. Veuillez l'installer.";
$MESS["STEP_IMCONNECTOR_DESCRIPTION"] = "Connectez Facebook, Instagram et d'autres réseaux sociaux aux canaux ouverts Bitrix24";
$MESS["STEP_IMCONNECTOR_ERROR1"] = "Remarque : certains connecteurs utilisés n'ont pas été configurés.";
$MESS["STEP_IMCONNECTOR_TITLE"] = "Communiquez et vendez via les réseaux sociaux";
?>