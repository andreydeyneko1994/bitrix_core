<?
$MESS["STEP_TELEPHONY_DESCRIPTION"] = "Activez l'intégration de la téléphonie pour recevoir et passer des appels depuis votre Bitrix24";
$MESS["STEP_TELEPHONY_ERROR1"] = "Aucun numéro de téléphone configuré n'a été trouvé.";
$MESS["STEP_TELEPHONY_TITLE"] = "Appelez vos clients et traitez les appels entrants";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "Le module Téléphonie (voximplant) n'est pas installé. Veuillez l'installer.";
?>