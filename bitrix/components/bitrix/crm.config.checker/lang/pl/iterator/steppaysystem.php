<?
$MESS["SALE_IS_NOT_INSTALLED"] = "Moduł e-Sklepu (sale) nie jest zainstalowany. Zainstaluj ten moduł.";
$MESS["STEP_PAYSYSTEM_DESCRIPTION"] = "Podłącz systemy płatności i odbieraj płatności online";
$MESS["STEP_PAYSYSTEM_ERROR1"] = "Nie znaleziono prawidłowo skonfigurowanego systemu płatności.";
$MESS["STEP_PAYSYSTEM_TITLE"] = "Odbieraj płatności online";
?>