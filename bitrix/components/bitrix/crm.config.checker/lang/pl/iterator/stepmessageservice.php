<?
$MESS["STEP_MESSAGESERVICES_DESCRIPTION"] = "Podłącz dostawcę SMS i masowo wysyłaj wiadomości SMS do klientów";
$MESS["STEP_MESSAGESERVICES_IS_NOT_CONFIGURED"] = "Nie znaleziono prawidłowo skonfigurowanej usługi SMS.";
$MESS["STEP_MESSAGESERVICES_TITLE"] = "Wysyłanie wiadomości SMS";
$MESS["STEP_MESSAGESERVICE_ERROR_NONEXISTENT_PROVIDER"] = "Używany jest dostawca SMS, który nie istnieje w #provider#. ";
$MESS["STEP_MESSAGESERVICE_ERROR_NONWORKING_PROVIDER"] = "Dostawca #provider# jest używany, ale nie skonfigurowany.";
$MESS["STEP_MESSAGESERVICE_IS_NOT_INSTALLED"] = "Moduł usługi przesyłania wiadomości (messageservice) nie jest zainstalowany. Zainstaluj ten moduł.";
?>