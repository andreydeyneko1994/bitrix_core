<?
$MESS["STEP_TELEPHONY_DESCRIPTION"] = "Habilite la integración de telefonía para recibir y hacer llamadas directamente en su Bitrix24";
$MESS["STEP_TELEPHONY_ERROR1"] = "No se encontraron números de teléfono configurados.";
$MESS["STEP_TELEPHONY_TITLE"] = "Llame a sus clientes y procese las llamadas entrantes";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "El módulo de telefonía (voximplant) no está instalado. Instale el módulo.";
?>