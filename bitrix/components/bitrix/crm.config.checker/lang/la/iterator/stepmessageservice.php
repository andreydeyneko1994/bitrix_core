<?
$MESS["STEP_MESSAGESERVICES_DESCRIPTION"] = "Conecte un proveedor de SMS y envíe mensajes SMS masivos a sus clientes";
$MESS["STEP_MESSAGESERVICES_IS_NOT_CONFIGURED"] = "No se encontró ningún servicio de SMS configurado correctamente.";
$MESS["STEP_MESSAGESERVICES_TITLE"] = "Enviar mensajes SMS";
$MESS["STEP_MESSAGESERVICE_ERROR_NONEXISTENT_PROVIDER"] = "Utiliza un proveedor SMS que no existe en #provider#. ";
$MESS["STEP_MESSAGESERVICE_ERROR_NONWORKING_PROVIDER"] = "El proveedor #provider# es utilizado pero no está configurado.";
$MESS["STEP_MESSAGESERVICE_IS_NOT_INSTALLED"] = "El módulo del servicio de mensajería (messageservice) no está instalado. Instale el módulo.";
?>