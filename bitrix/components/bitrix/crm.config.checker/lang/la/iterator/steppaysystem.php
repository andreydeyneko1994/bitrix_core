<?
$MESS["SALE_IS_NOT_INSTALLED"] = "El módulo e-Store (sale) no está instalado. Instale el módulo.";
$MESS["STEP_PAYSYSTEM_DESCRIPTION"] = "Conecte los sistemas de pago y reciba pagos en línea";
$MESS["STEP_PAYSYSTEM_ERROR1"] = "No se encontró un sistema de pago configurado correctamente.";
$MESS["STEP_PAYSYSTEM_TITLE"] = "Recibir pagos en línea";
?>