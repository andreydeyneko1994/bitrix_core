<?
$MESS["STEP_MESSAGESERVICES_DESCRIPTION"] = "Conecte um provedor de SMS e envie mensagens SMS em massa para seus clientes";
$MESS["STEP_MESSAGESERVICES_IS_NOT_CONFIGURED"] = "Nenhum serviço SMS configurado corretamente encontrado.";
$MESS["STEP_MESSAGESERVICES_TITLE"] = "Enviar mensagens SMS";
$MESS["STEP_MESSAGESERVICE_ERROR_NONEXISTENT_PROVIDER"] = "É usado um provedor de SMS que não existe em #provider#. ";
$MESS["STEP_MESSAGESERVICE_ERROR_NONWORKING_PROVIDER"] = "O provedor #provider# é usado, mas não configurado.";
$MESS["STEP_MESSAGESERVICE_IS_NOT_INSTALLED"] = "O módulo Serviço de Mensagens (messageservice) não está instalado. Por favor, instale o módulo.";
?>