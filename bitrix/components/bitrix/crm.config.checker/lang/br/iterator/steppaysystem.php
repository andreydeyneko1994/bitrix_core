<?
$MESS["SALE_IS_NOT_INSTALLED"] = "O módulo Loja on-line (sale) não está instalado. Por favor, instale o módulo.";
$MESS["STEP_PAYSYSTEM_DESCRIPTION"] = "Conecte sistemas de pagamento e receba pagamentos online";
$MESS["STEP_PAYSYSTEM_ERROR1"] = "Nenhum sistema de pagamento configurado corretamente encontrado.";
$MESS["STEP_PAYSYSTEM_TITLE"] = "Receber pagamentos online";
?>