<?
$MESS["STEP_CRMFORM_DESCRIPTION"] = "Ative a integração de telefonia e configure os formulários de retorno de chamada";
$MESS["STEP_CRMFORM_ERROR1"] = "Não há telefones na Telefonia.";
$MESS["STEP_CRMFORM_ERROR2"] = "Há formulários de retorno de chamada que não listam um número de telefone ativo. Por favor, selecione um número de telefone para esses formulários.";
$MESS["STEP_CRMFORM_TITLE"] = "Ligue de volta para seus clientes";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "O módulo Telefonia não está instalado.";
?>