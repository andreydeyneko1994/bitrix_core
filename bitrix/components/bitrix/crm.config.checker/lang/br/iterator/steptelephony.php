<?
$MESS["STEP_TELEPHONY_DESCRIPTION"] = "Ative a integração de telefonia para receber e fazer chamadas diretamente no seu Bitrix24";
$MESS["STEP_TELEPHONY_ERROR1"] = "Nenhum número de telefone configurado encontrado.";
$MESS["STEP_TELEPHONY_TITLE"] = "Ligue para seus clientes e processe chamadas recebidas";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "O módulo Telefonia (voximplant) não está instalado. Por favor, instale o módulo.";
?>