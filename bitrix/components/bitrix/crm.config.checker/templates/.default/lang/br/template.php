<?php
$MESS["CMR_CONFIG_CHECKER_TITLE"] = "Configure os recursos que você precisa";
$MESS["CRM_BUTTON_APPLY"] = "Aplicar";
$MESS["CRM_BUTTON_CHECK"] = "Aplicar";
$MESS["CRM_CHANGE_CRM_FORM_NUMBER"] = "Número de telefone para usar nos formulários de retorno de chamada:";
$MESS["CRM_CONFIG"] = "Configurar";
$MESS["CRM_CONFIG_CHECKER_DONE"] = "Concluído";
$MESS["CRM_CONFIG_CHECKER_INPROCESS"] = "Pendente";
$MESS["CRM_CONFIG_CHECKER_NOT_ACTUAL"] = "Não usado";
$MESS["CRM_CONFIG_CHECKER_NOT_CHECKED"] = "Não marcado";
$MESS["CRM_PICK_UP_THE_NUMBER_FOR_CRMFORM"] = "Selecione um número";
$MESS["CRM_SEVERAL_NUMBERS_IS_IN_USE"] = "Os formulários usam números de telefone diferentes";
