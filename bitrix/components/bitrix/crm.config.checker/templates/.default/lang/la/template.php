<?php
$MESS["CMR_CONFIG_CHECKER_TITLE"] = "Configure las funciones que necesita";
$MESS["CRM_BUTTON_APPLY"] = "Aplicar";
$MESS["CRM_BUTTON_CHECK"] = "Aplicar";
$MESS["CRM_CHANGE_CRM_FORM_NUMBER"] = "Número de teléfono que se utilizará en los formularios de devolución de la llamada:";
$MESS["CRM_CONFIG"] = "Configurar";
$MESS["CRM_CONFIG_CHECKER_DONE"] = "Listo";
$MESS["CRM_CONFIG_CHECKER_INPROCESS"] = "Pendiente";
$MESS["CRM_CONFIG_CHECKER_NOT_ACTUAL"] = "No utilizado";
$MESS["CRM_CONFIG_CHECKER_NOT_CHECKED"] = "No revisado";
$MESS["CRM_PICK_UP_THE_NUMBER_FOR_CRMFORM"] = "Seleccionar un número";
$MESS["CRM_SEVERAL_NUMBERS_IS_IN_USE"] = "Los formularios utilizan diferentes números telefónicos";
