<?
$MESS["CRM_COLUMN_ACTIVE"] = "Aktywny";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "Kolejność aplikacji";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "Zawrzyj podatek w cenie";
$MESS["CRM_COLUMN_NAME"] = "Nazwa";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "Rodzaj klienta";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "Zmodyfikowany";
$MESS["CRM_COLUMN_VALUE"] = "Stawka";
$MESS["CRM_COMPANY_PT"] = "Firma";
$MESS["CRM_CONTACT_PT"] = "Kontakt";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "Błąd usuwania stawki podatku.";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji stawki podatku.";
?>