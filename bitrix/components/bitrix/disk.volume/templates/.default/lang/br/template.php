<?
$MESS["DISK_VOLUME_AVAILABLE_SPACE"] = "estará livre após a limpeza";
$MESS["DISK_VOLUME_CANCEL"] = "Cancelar";
$MESS["DISK_VOLUME_CANNT_DROP"] = "Nenhum arquivo encontrado para limpeza segura.";
$MESS["DISK_VOLUME_MAY_DROP"] = "Os itens encontrados podem ser excluídos com segurança.";
$MESS["DISK_VOLUME_MAY_DROP_TITLE"] = "Limpeza Segura";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN"] = "Lixeira";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN_NOTE"] = "Arquivos excluídos";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION"] = "Não utilizado";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION_NOTE"] = "Versões herdadas do arquivo";
$MESS["DISK_VOLUME_MEASURE_DATA"] = "Começar a Verificar";
$MESS["DISK_VOLUME_MEASURE_PROCESS"] = "A verificação da unidade pode levar algum tempo.";
$MESS["DISK_VOLUME_MEASURE_TITLE"] = "Busca de arquivos para limpeza segura...";
$MESS["DISK_VOLUME_RUN_CLEANER"] = "Executar Limpeza";
$MESS["DISK_VOLUME_START_COMMENT"] = "O Bitrix24 verificará seus documentos e mostrará um resumo dos arquivos que podem ser excluídos com segurança para liberar espaço.";
$MESS["DISK_VOLUME_START_COMMENT_EXPERT"] = "Você pode ativar o modo Expert para analisar todos os arquivos em sua unidade.";
$MESS["DISK_VOLUME_START_FILES_NONE"] = "Não há arquivos no seu Drive no momento. Assim que você começar a usar o seu Drive, o Britrix24 estará constantemente verificando e notificará você se houver arquivos que podem ser excluídos com segurança para liberar espaço.";
$MESS["DISK_VOLUME_START_TITLE"] = "Espaço Livre";
?>