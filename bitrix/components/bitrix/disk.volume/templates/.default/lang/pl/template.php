<?
$MESS["DISK_VOLUME_AVAILABLE_SPACE"] = "zostanie zwolniony po oczyszczeniu";
$MESS["DISK_VOLUME_CANCEL"] = "Anuluj";
$MESS["DISK_VOLUME_CANNT_DROP"] = "Nie znaleziono plików do bezpiecznego wyczyszczenia.";
$MESS["DISK_VOLUME_MAY_DROP"] = "Znalezione przedmioty można bezpiecznie usunąć.";
$MESS["DISK_VOLUME_MAY_DROP_TITLE"] = "Bezpieczne czyszczenie";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN"] = "Kosz";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN_NOTE"] = "Usunięte pliki";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION"] = "Wyłączone";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION_NOTE"] = "Starsze wersje pliku";
$MESS["DISK_VOLUME_MEASURE_DATA"] = "Rozpocznij skanowanie";
$MESS["DISK_VOLUME_MEASURE_PROCESS"] = "Skanowanie dysku może zająć trochę czasu.";
$MESS["DISK_VOLUME_MEASURE_TITLE"] = "Wyszukiwanie plików do bezpiecznego czyszczenia...";
$MESS["DISK_VOLUME_RUN_CLEANER"] = "Uruchom oczyszczanie";
$MESS["DISK_VOLUME_START_COMMENT"] = "Bitrix24 skanuje twoje dokumenty i pokazuje podsumowanie plików, które można bezpiecznie usunąć w celu zwolnienia miejsca.";
$MESS["DISK_VOLUME_START_COMMENT_EXPERT"] = "Możesz aktywować tryb Expert, aby analizować wszystkie pliki na dysku.";
$MESS["DISK_VOLUME_START_FILES_NONE"] = "Na Dysku nie ma obecnie żadnych plików. Gdy tylko zaczniesz korzystać z Dysku, Bitrix24 będzie go stale skanował i powiadamiał cię, czy istnieją pliki, które można bezpiecznie usunąć w celu zwolnienia miejsca.";
$MESS["DISK_VOLUME_START_TITLE"] = "Wolne miejsce";
?>