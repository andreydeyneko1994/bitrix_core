<?
$MESS["DISK_VOLUME_AVAILABLE_SPACE"] = "sera libéré après nettoyage";
$MESS["DISK_VOLUME_CANCEL"] = "Annuler";
$MESS["DISK_VOLUME_CANNT_DROP"] = "Aucun fichier pouvant être supprimé en toute sécurité n'a été trouvé.";
$MESS["DISK_VOLUME_MAY_DROP"] = "Les éléments trouvés peuvent être supprimés en toute sécurité.";
$MESS["DISK_VOLUME_MAY_DROP_TITLE"] = "Nettoyage sécurisé";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN"] = "Corbeille";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN_NOTE"] = "Fichiers supprimés";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION"] = "Inutilisé";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION_NOTE"] = "Versions de fichier hérité";
$MESS["DISK_VOLUME_MEASURE_DATA"] = "Démarrer l'analyse";
$MESS["DISK_VOLUME_MEASURE_PROCESS"] = "L'analyse du disque peut prendre du temps.";
$MESS["DISK_VOLUME_MEASURE_TITLE"] = "Recherche de fichiers à nettoyer en toute sécurité...";
$MESS["DISK_VOLUME_RUN_CLEANER"] = "Exécuter un nettoyage";
$MESS["DISK_VOLUME_START_COMMENT"] = "Bitrix24 analysera vos documents et affichera un résumé des fichiers que vous pouvez supprimer en toute sécurité pour libérer de l'espace.";
$MESS["DISK_VOLUME_START_COMMENT_EXPERT"] = "Vous pouvez activer le mode Expert pour analyser tous les fichiers de votre lecteur.";
$MESS["DISK_VOLUME_START_FILES_NONE"] = "Votre lecteur ne contient aucun fichier pour le moment. Dès que vous commencerez à utiliser votre lecteur, Bitrix24 l'analysera en permanence et vous informera si des fichiers peuvent être supprimés en toute sécurité pour libérer de l'espace.";
$MESS["DISK_VOLUME_START_TITLE"] = "Espace libre";
?>