<?
$MESS["DISK_SERVICE"] = "Drive";
$MESS["DISK_VOLUME_DESC"] = "Calcule l'espace disque occupé et propose d'utiliser l'outil de nettoyage de disque si nécessaire.";
$MESS["DISK_VOLUME_NAME"] = "Calculer l'espace disque utilisé";
?>