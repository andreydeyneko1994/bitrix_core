<?
$MESS["DISK_VOLUME_DATA_DELETED"] = "Les données ont été supprimées";
$MESS["DISK_VOLUME_DATA_DELETED_QUEUE"] = "Données supprimées. Étape #QUEUE_STEP#/#QUEUE_LENGTH#";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FILE"] = "Vos droits d'accès au fichier sont insuffisants.";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FOLDER"] = "Vos droits d'accès au dossier sont insuffisants.";
$MESS["DISK_VOLUME_FILE_DELETE_OK"] = "La fichier a été supprimé";
$MESS["DISK_VOLUME_FILE_VERSION_DELETE_OK"] = "Les versions inutilisées du fichier ont été supprimées";
$MESS["DISK_VOLUME_FOLDER_DELETE_OK"] = "Le dossier a été supprimé";
$MESS["DISK_VOLUME_FOLDER_EMPTY_OK"] = "Le contenu du dossier a été supprimé";
$MESS["DISK_VOLUME_GROUP_FILE_DELETE_OK"] = "Les fichiers ont été supprimés";
$MESS["DISK_VOLUME_GROUP_FILE_VERSION_DELETE_OK"] = "Les versions inutilisées du fichier ont été supprimées";
$MESS["DISK_VOLUME_NOTIFICATION_SEND_OK"] = "Un avertissement a été envoyé";
$MESS["DISK_VOLUME_NOTIFY_CHAT"] = "Attention ! Les fichiers de votre chat \"#TITLE#\" utilisent trop d'espace disque (#FILE_SIZE#).";
$MESS["DISK_VOLUME_NOTIFY_FOLDER"] = "Attention ! Vos fichiers du dossier <a href=\"#URL#\">\"#TITLE#\"</a> occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_RECOMMENDATION"] = "Conseil : déplacez les fichiers vers un autre stockage ; <a href=\"#URL_TRASHCAN#\">videz la corbeille</a> ; <a href=\"#URL_CLEAR#\">supprimez les fichiers inutilisés</a>. L'administrateur peut nettoyer votre stockage à sa propre discrétion si vous ne libérez pas vite de l'espace.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_GROUP"] = "Attention ! Vos fichiers du <a href=\"#URL#\">disque</a> du groupe de travail #TITLE# occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_STORAGE"] = "Attention ! Vos fichiers du disque <a href=\"#URL#\">\"#TITLE#\"</a> occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_TRASHCAN"] = "Attention ! Vos fichiers de la <a href=\"#URL#\">corbeille</a> occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_UPLOADED"] = "Attention ! Vos fichiers du dossier <a href=\"#URL#\">Fichiers téléversés</a> occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_USER"] = "Attention ! Vos fichiers du <a href=\"#URL#\">disque</a> occupent beaucoup d'espace : #FILE_SIZE#.";
$MESS["DISK_VOLUME_PERFORMING_QUEUE"] = "Récupération des données. Étape #QUEUE_STEP# of #QUEUE_LENGTH#";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_DELETE_OK"] = "Les versions inutilisées ont été supprimées";
?>