<?
$MESS["DISK_VOLUME_DATA_DELETED"] = "Os dados foram excluídos";
$MESS["DISK_VOLUME_DATA_DELETED_QUEUE"] = "Dados excluídos. Etapa #QUEUE_STEP#/#QUEUE_LENGTH#";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FILE"] = "Permissões insuficientes de acesso ao arquivo.";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FOLDER"] = "Permissões insuficientes de acesso à pasta.";
$MESS["DISK_VOLUME_FILE_DELETE_OK"] = "O arquivo foi excluído";
$MESS["DISK_VOLUME_FILE_VERSION_DELETE_OK"] = "As versões de arquivos não utilizadas foram excluídas";
$MESS["DISK_VOLUME_FOLDER_DELETE_OK"] = "A pasta foi excluída";
$MESS["DISK_VOLUME_FOLDER_EMPTY_OK"] = "Os conteúdos da pasta foram excluídos";
$MESS["DISK_VOLUME_GROUP_FILE_DELETE_OK"] = "Os arquivos foram excluídos";
$MESS["DISK_VOLUME_GROUP_FILE_VERSION_DELETE_OK"] = "As versões de arquivos não utilizadas foram excluídas";
$MESS["DISK_VOLUME_NOTIFICATION_SEND_OK"] = "O aviso foi enviado";
$MESS["DISK_VOLUME_NOTIFY_CHAT"] = "Aviso! Os arquivos do seu bate-papo \"#TITLE#\" estão usando muito espaço em disco (#FILE_SIZE#).";
$MESS["DISK_VOLUME_NOTIFY_FOLDER"] = "Aviso! Seus arquivos na pasta <a href=\"#URL#\">\"#TITLE#\"</a> estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_RECOMMENDATION"] = "Conselho: transfira os arquivos para algum outro armazenamento; <a href=\"#URL_TRASHCAN#\">esvazie a Lixeira</a>; <a href=\"#URL_CLEAR#\">exclua arquivos não utilizados</a>. O administrador pode limpar seu armazenamento a seu próprio critério, se você não liberar espaço em breve.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_GROUP"] = "Aviso! Seus arquivos no <a href=\"#URL#\">drive</a> do grupo de trabalho #TITLE# estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_STORAGE"] = "Aviso! Seus arquivos no drive <a href=\"#URL#\">\"#TITLE#\"</a> estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_TRASHCAN"] = "Aviso! Seus arquivos na <a href=\"#URL#\">Lixeira</a> estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_UPLOADED"] = "Aviso! Seus arquivos nos <a href=\"#URL#\">Arquivos Carregados</a> estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_USER"] = "Aviso! Seus arquivos no <a href=\"#URL#\">drive</a> estão ocupando muito espaço: #FILE_SIZE#.";
$MESS["DISK_VOLUME_PERFORMING_QUEUE"] = "Coleta de dados. Etapa #QUEUE_STEP# de #QUEUE_LENGTH#";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_DELETE_OK"] = "As versões não utilizadas foram excluídas";
?>