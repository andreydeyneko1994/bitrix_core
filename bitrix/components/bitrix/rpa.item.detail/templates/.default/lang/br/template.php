<?
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_NAME"] = "Completar o fluxo de trabalho";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_FAIL"] = "Negado";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_TITLE"] = "Selecionar o resultado do fluxo de trabalho.";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_SELECTOR_TITLE"] = "Completar o fluxo de trabalho:";
$MESS["RPA_ITEM_DETAIL_ITEM_EXTERNAL_UPDATE_NOTIFY"] = "O item foi alterado. Deseja recarregar a página?";
$MESS["RPA_ITEM_DETAIL_TAB_MAIN"] = "Comum";
$MESS["RPA_ITEM_DETAIL_TAB_ROBOTS"] = "Regras de automação";
?>