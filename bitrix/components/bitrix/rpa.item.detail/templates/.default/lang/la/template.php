<?php
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_NAME"] = "Completar el procesos de negocio";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_FAIL"] = "Declinado";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_POPUP_TITLE"] = "Seleccione el resultado del procesos de negocio.";
$MESS["RPA_ITEM_DETAIL_FINAL_STAGE_SELECTOR_TITLE"] = "Completar el procesos de negocio:";
$MESS["RPA_ITEM_DETAIL_ITEM_EXTERNAL_UPDATE_NOTIFY"] = "El elemento ha sido cambiado. ¿Quiere recargar la página?";
$MESS["RPA_ITEM_DETAIL_TAB_MAIN"] = "Común";
$MESS["RPA_ITEM_DETAIL_TAB_ROBOTS"] = "Reglas de automatización";
