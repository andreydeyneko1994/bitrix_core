<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ACTIVE_COMMENTS"] = "Commentaires connectés";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_CANCEL"] = "Annuler";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_BUTTON_OK"] = "Continuer";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_DESCRIPTION"] = "Ce compte est utilisé avec le connecteur Instagram Business sur votre Bitrix24. Cette connexion sera désactivée pour éviter les commentaires en double.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_CONFIRM_TITLE"] = "Connecter quand même ?";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Connectez un compte professionnel Instagram pour recevoir les messages de vos clients dans le chat Bitrix24. Pour vous connecter, vous devez avoir une page publique Facebook ou en créer une, et y connecter un compte professionnel Instagram.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">communication instantanée avec les visiteurs de votre compte Instagram</li>.
					<li class=\"im-connector-settings-header-list-item\">acheminement automatique des commentaires vers les opérateurs disponibles</li>.
					<li class=\"im-connector-settings-header-list-item\">communiquez avec vos clients directement depuis votre chat Bitrix24</li>.
					<li class=\"im-connector-settings-header-list-item\">enregistrer automatiquement les clients dans le CRM</li>
				</ul>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_ADDITIONAL_DESCRIPTION"] = "Vous devrez créer une page Facebook publique ou connecter celle que vous avez déjà. Votre compte professionnel Instagram doit être connecté à cette page Facebook. Seul l'administrateur de la page peut connecter à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_3"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_LIST_ITEM_4"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_SUBTITLE"] = "Chaque fois qu'un client laisse une question sur votre publication Instagram, vous la recevez sur votre Bitrix24. Répondez plus rapidement et améliorez la conversion.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INDEX_TITLE"] = "Messages directs Instagram dans Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_COMMENTS"] = "Commentaires non connectés";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TITLE_NEW"] = "Messages directs Instagram dans Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_DIRECT_AND_BUSINESS"] = "Connecter également les commentaires laissés sur les publications et les diffusions";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT"] = "Extension de la fenêtre de messagerie de 24 heures à 7 jours";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Notez que Facebook ne permet pas d'envoyer des publicités (y compris des remises, des coupons) ou des messages automatisés aux clients une fois que la fenêtre de 24 heures a expiré. Votre compte sera verrouillé si vous ne suivez pas ces directives. #START_HELP_DESC#Détails#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_OFF"] = "La fenêtre de messagerie n'a pas été étendue de 24 heures à 7 jours";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_TO_CONNECT_HUMAN_AGENT_ON"] = "Fenêtre de messagerie étendue de 24 heures à 7 jours";
