<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_DISABLED_ACCESS_INSTAGRAM_DIRECT_MESSAGES"] = "Pour connecter IG Direct, veuillez activer l'accès aux messages de votre compte Instagram. #A#Plus d'informations.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API"] = "Malheureusement, en raison de la réglementation de Facebook, seuls les utilisateurs disposant de comptes Instagram ayant un nombre d'abonnés compris entre 10 000 et 100 000 peuvent se connecter à IG Direct. #A#Détails.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API_3"] = "Malheureusement, votre IG Direct ne répond pas aux exigences de Facebook. #A#Détails#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erreur du serveur.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INVALID_OAUTH_ACCESS_TOKEN"] = "Impossible de gérer la page publique, l'accès a été perdu. Vous devez vous reconnecter à votre page publique.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_AUTHORIZATION_PAGE"] = "Impossible d'associer la page";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_PAGE"] = "Impossible de dissocier la page";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_USER"] = "Impossible de dissocier votre compte utilisateur";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_AUTHORIZATION_PAGE"] = "La page a été associée";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_PAGE"] = "La page a été dissociée";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_USER"] = "Votre compte utilisateur a été dissocié";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REMOVED_REFERENCE_TO_PAGE"] = "Ce connecteur a été configuré pour être utilisé avec un groupe de travail auquel vous n'avez actuellement pas d'accès administrateur.<br>Veuillez reconfigurer le connecteur.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REPEATING_ERROR"] = "Si le problème persiste, envisagez de déconnecter le canal et de le configurer une nouvelle fois.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
