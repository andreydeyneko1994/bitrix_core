<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_DISABLED_ACCESS_INSTAGRAM_DIRECT_MESSAGES"] = "Aby połączyć się z IG Direct, włącz dostęp do wiadomości na koncie Instagram. #A#Dowiedz się więcej.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API"] = "Niestety, ze względu na regulamin firmy Facebook do IG Direct mogą podłączyć się tylko użytkownicy z kontami na Instagramie mającymi od 10 000 do 100 000 subskrybentów. #A#Szczegóły.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API_3"] = "Niestety Twój IG Direct nie spełnia wymagań Facebooka. #A#Szczegóły#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Błąd serwera.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INVALID_OAUTH_ACCESS_TOKEN"] = "Nie można zarządzać stroną publiczną z powodu utraty dostępu. Należy ponownie połączyć się ze swoją stroną publiczną.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest nieaktywny.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_AUTHORIZATION_PAGE"] = "Nie można podłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_PAGE"] = "Nie można odłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_USER"] = "Nie można odłączyć konta użytkownika";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_AUTHORIZATION_PAGE"] = "Strona została podłączona";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_PAGE"] = "Strona została odłączona";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_USER"] = "Konto użytkownika zostało odłączone";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REMOVED_REFERENCE_TO_PAGE"] = "Ten konektor został skonfigurowany do użytku w grupie roboczej, do której aktualnie nie masz dostępu administratora.<br>
Ponownie skonfiguruj konektor.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REPEATING_ERROR"] = "Jeśli problem będzie się powtarzał, może być konieczne odłączenie kanału i jego ponowna konfiguracja.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
