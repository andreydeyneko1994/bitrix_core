<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_DISABLED_ACCESS_INSTAGRAM_DIRECT_MESSAGES"] = "Para conectar o IG Direct, permita o acesso às mensagens da sua conta do Instagram. #A#Saiba mais.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API"] = "Infelizmente, devido aos regulamentos do Facebook, apenas usuários com contas no Instagram com número de seguidores de 10.000 a 100.000 podem se conectar ao IG Direct. #A#Detalhes.#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_IG_ACCOUNT_IS_NOT_ELIGIBLE_API_3"] = "Infelizmente, seu IG Direct não atende aos requisitos do Facebook. #A#Detalhes#A_END#";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erro de servidor.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INVALID_OAUTH_ACCESS_TOKEN"] = "Não podemos gerenciar a página pública porque o acesso foi perdido. Você precisa se reconectar à sua página pública.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_AUTHORIZATION_PAGE"] = "Não é possível vincular a página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_PAGE"] = "Não é possível desvincular a página";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_NO_DEL_USER"] = "Não é possível desvincular sua conta de usuário";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_AUTHORIZATION_PAGE"] = "A página foi vinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_PAGE"] = "A página foi desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_OK_DEL_USER"] = "Sua conta de usuário foi desvinculada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REMOVED_REFERENCE_TO_PAGE"] = "Este conector foi configurado para uso com um grupo de trabalho que atualmente você não tem acesso de administrador.<br> Configure o conector novamente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_REPEATING_ERROR"] = "Se o problema persistir, talvez você precise desconectar o canal e configurá-lo novamente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
