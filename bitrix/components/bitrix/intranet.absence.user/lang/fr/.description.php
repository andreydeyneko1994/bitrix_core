<?
$MESS["INTR_ABSENCE_GROUP_NAME"] = "Tableau des absences";
$MESS["INTR_GROUP_NAME"] = "Intranet - portail d'entreprise";
$MESS["INTR_HR_GROUP_NAME"] = "HR";
$MESS["INTR_IAU_COMPONENT_DESCR"] = "Afficher le tableau des absences d'un utilisateur concret";
$MESS["INTR_IAU_COMPONENT_NAME"] = "Absences de l'utilisateur";
?>