<?
$MESS["IMOL_QUICK_ANSWERS_EDIT_ALL"] = "Todas";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CANCEL"] = "Cancelar";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CREATE"] = "Criar";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR"] = "Não é possível salvar a resposta automática. Tente novamente mais tarde.";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR_EMPTY_TEXT"] = "O texto não deve estar vazio";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SECTION_TITLE"] = "Criar resposta automática na seção";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SUCCESS"] = "Sua resposta automática foi salva!";
$MESS["IMOL_QUICK_ANSWERS_EDIT_TEXT_PLACEHOLDER"] = "Texto da resposta predefinida";
$MESS["IMOL_QUICK_ANSWERS_EDIT_UPDATE"] = "Atualizar";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_1"] = "salve uma mensagem postada como resposta automática clicando no botão <span class=\"imopenlines-iframe-quick-check-icon\"></span> ao lado da mensagem";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_2"] = "criar uma nova a partir do zero";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_3"] = "ou abrir o <a id=\"quick-info-all-url\" class=\"imopenlines-iframe-quick-link\">formulário de respostas automáticas</a>";
$MESS["IMOL_QUICK_ANSWERS_INFO_TITLE_NEW"] = "Use respostas automáticas para fornecer respostas rápidas a perguntas feitas com frequência";
$MESS["IMOL_QUICK_ANSWERS_NOT_FOUND"] = "Desculpe, mas não conseguimos encontrar nada";
$MESS["IMOL_QUICK_ANSWERS_SEARCH_PROGRESS"] = "Busca em andamento...";
?>