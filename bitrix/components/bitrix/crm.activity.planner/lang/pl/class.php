<?
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "Moduł Kalendarze nie został zainstalowany.";
$MESS["CRM_ACTIVITY_PLANNER_IMPORTANT_SLIDER"] = "Ważne";
$MESS["CRM_ACTIVITY_PLANNER_NO_ACTIVITY"] = "Nie znaleziono działania.";
$MESS["CRM_ACTIVITY_PLANNER_NO_PROVIDER"] = "Nie odnaleziono dostawcy działań.";
$MESS["CRM_ACTIVITY_PLANNER_NO_READ_PERMISSION"] = "Niewystarczające uprawnienia do przeglądania danych działania.";
$MESS["CRM_ACTIVITY_PLANNER_NO_UPDATE_PERMISSION"] = "Niewystarczające uprawnienia do edytowania danych działania.";
$MESS["CRM_ACT_EMAIL_REPLY_ADD_DOCS"] = "Zmień deal";
$MESS["CRM_ACT_EMAIL_REPLY_SET_DOCS"] = "Wybierz deal";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
?>