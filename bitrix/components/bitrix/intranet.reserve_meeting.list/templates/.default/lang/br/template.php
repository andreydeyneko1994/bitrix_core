<?
$MESS["INTASK_C23T_LOAD"] = "Carregando...";
$MESS["INTDT_ACTIONS"] = "Ações";
$MESS["INTDT_GRAPH"] = "Horários";
$MESS["INTDT_NO_TASKS"] = "Nenhuma sala de reunião";
$MESS["INTST_CANCEL"] = "Cancelar";
$MESS["INTST_CLOSE"] = "Fechar";
$MESS["INTST_DELETE"] = "Excluir";
$MESS["INTST_FOLDER_NAME"] = "Nome da Pasta";
$MESS["INTST_SAVE"] = "Salvar";
?>