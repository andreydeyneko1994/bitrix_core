<?php
$MESS["SONET_C5_NO_PERMS"] = "Você não tem permissão bara visualizar o perfil desse grupo";
$MESS["SONET_C5_PAGE_TITLE"] = "Sobre o grupo de trabalho";
$MESS["SONET_C5_PAGE_TITLE_PROJECT"] = "Sobre o projeto";
$MESS["SONET_C5_PAGE_TITLE_SCRUM"] = "Sobre a equipe Scrum";
$MESS["SONET_C5_TITLE"] = "Perfil";
$MESS["SONET_C6_BLOG_T"] = "Relatórios";
$MESS["SONET_C6_FORUM_T"] = "Discussões";
$MESS["SONET_C6_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_C6_TASKS_T"] = "Tarefas";
$MESS["SONET_MODULE_NOT_INSTALL"] = "O módulo de rede social não está instalado.";
$MESS["SONET_P_USER_NO_GROUP"] = "O grupo não foi encontrado";
