<?php
$MESS["SONET_C5_NO_PERMS"] = "Usted no tiene permiso para ver el perfil de este grupo.";
$MESS["SONET_C5_PAGE_TITLE"] = "Acerca del grupo de trabajo";
$MESS["SONET_C5_PAGE_TITLE_PROJECT"] = "Acerca del proyecto";
$MESS["SONET_C5_PAGE_TITLE_SCRUM"] = "Acerca del equipo Scrum";
$MESS["SONET_C5_TITLE"] = "Perfil";
$MESS["SONET_C6_BLOG_T"] = "Reportes";
$MESS["SONET_C6_FORUM_T"] = "Discusiones";
$MESS["SONET_C6_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_C6_TASKS_T"] = "Tareas";
$MESS["SONET_MODULE_NOT_INSTALL"] = "El módulo Red social no está instalado.";
$MESS["SONET_P_USER_NO_GROUP"] = "Grupo no fue encontrado.";
