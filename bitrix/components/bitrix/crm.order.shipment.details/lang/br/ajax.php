<?
$MESS["CRM_ORDER_SD_ERROR_SHIPPING_DEDUCTED"] = "Não é possível adicionar o produto porque o envio já foi concluído.";
$MESS["CRM_ORDER_SD_FORM_DATA_MISSING"] = "Faltam alguns dados do formulário";
$MESS["CRM_ORDER_SD_INSUFFICIENT_RIGHTS"] = "Permissões insuficientes.";
$MESS["CRM_ORDER_SD_SHIPMENT_NOT_FOUND"] = "O envio não foi encontrado";
?>