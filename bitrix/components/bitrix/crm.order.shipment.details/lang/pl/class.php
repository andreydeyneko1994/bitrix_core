<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_ORDER_ERROR_SHIPMENT_SERVICE_RESTRICTED"] = "Usługa dostawy nie spełnia kryteriów";
$MESS["CRM_ORDER_SD_NOT_CHOSEN"] = "Nie wybrano";
$MESS["CRM_ORDER_SD_ORDER_NOT_FOUND"] = "Nie znaleziono zamówienia.";
$MESS["CRM_ORDER_SD_SHIPMENT_NOT_FOUND"] = "Nie znaleziono wysyłki";
$MESS["CRM_ORDER_SD_UNKNOWN_DISCOUNT"] = "Nieznany upust";
$MESS["CRM_ORDER_SHIPMENT"] = "Wysyłka";
$MESS["CRM_ORDER_SHIPMENT_ACCOUNT_NUMBER"] = "Nr wysyłki";
$MESS["CRM_ORDER_SHIPMENT_ALLOW_DELIVERY"] = "Dostawa zatwierdzona";
$MESS["CRM_ORDER_SHIPMENT_BASE_PRICE_DELIVERY_WITH_CURRENCY"] = "Koszty wysyłki bez upustu";
$MESS["CRM_ORDER_SHIPMENT_COMMENTS"] = "Komentarz";
$MESS["CRM_ORDER_SHIPMENT_CREATION_PAGE_TITLE"] = "Utwórz wysyłkę";
$MESS["CRM_ORDER_SHIPMENT_CUSTOM_PRICE_DELIVERY"] = "Cena niestandardowa";
$MESS["CRM_ORDER_SHIPMENT_DATE_DEDUCTED"] = "Status wysłania zmodyfikowano";
$MESS["CRM_ORDER_SHIPMENT_DATE_MARKED"] = "Oznaczone jako problem";
$MESS["CRM_ORDER_SHIPMENT_DATE_RESPONSIBLE_ID"] = "Osoba odpowiedzialna zmieniona przez";
$MESS["CRM_ORDER_SHIPMENT_DEDUCTED"] = "Wysłano";
$MESS["CRM_ORDER_SHIPMENT_DELIVERY_DOC_DATE"] = "Powód anulowania wysyłki";
$MESS["CRM_ORDER_SHIPMENT_DELIVERY_DOC_NUM"] = "Dokument przewozowy #";
$MESS["CRM_ORDER_SHIPMENT_DELIVERY_LOGO"] = "Logo usługi dostawy";
$MESS["CRM_ORDER_SHIPMENT_DELIVERY_SERVICE"] = "Usługa dostawy";
$MESS["CRM_ORDER_SHIPMENT_DISCOUNTS"] = "Upusty";
$MESS["CRM_ORDER_SHIPMENT_DISCOUNT_PRICE_WITH_CURRENCY"] = "Upust";
$MESS["CRM_ORDER_SHIPMENT_EMP_ALLOW_DELIVERY"] = "Dostawa zatwierdzona przez";
$MESS["CRM_ORDER_SHIPMENT_EMP_DEDUCTED"] = "Status wysłania zmodyfikowany przez";
$MESS["CRM_ORDER_SHIPMENT_EMP_MARKED"] = "Oznaczone jako problem przez";
$MESS["CRM_ORDER_SHIPMENT_EMP_RESPONSIBLE"] = "Osobę odpowiedzialną zmieniono";
$MESS["CRM_ORDER_SHIPMENT_EXTRA_SERVICES"] = "Usługi dodatkowe";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DATE_INSERT"] = "Utworzono";
$MESS["CRM_ORDER_SHIPMENT_FIELD_ID"] = "ID";
$MESS["CRM_ORDER_SHIPMENT_FIELD_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["CRM_ORDER_SHIPMENT_MARKED"] = "Oznaczone jako problem";
$MESS["CRM_ORDER_SHIPMENT_ORDER_ID"] = "ID zamówienia";
$MESS["CRM_ORDER_SHIPMENT_PRICE_DELIVERY_CALCULATED_WITH_CURRENCY"] = "Obliczone koszty wysyłki";
$MESS["CRM_ORDER_SHIPMENT_PRICE_DELIVERY_WITH_CURRENCY"] = "Koszty wysyłki";
$MESS["CRM_ORDER_SHIPMENT_PRODUCT_LIST"] = "Produkty";
$MESS["CRM_ORDER_SHIPMENT_REASON_MARKED"] = "Opis problemu";
$MESS["CRM_ORDER_SHIPMENT_REASON_UNDO_DEDUCTED"] = "Powód anulowania wysyłki";
$MESS["CRM_ORDER_SHIPMENT_SUBTITLE_MASK"] = "##ID# - #DATE_INSERT#";
$MESS["CRM_ORDER_SHIPMENT_TAB_EVENT"] = "Historia";
$MESS["CRM_ORDER_SHIPMENT_TAB_PRODUCTS"] = "Produkty";
$MESS["CRM_ORDER_SHIPMENT_TAB_TREE"] = "Powiązane obiekty";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "ID wysyłki (#ID#), ##ACCOUNT_NUMBER#";
$MESS["CRM_ORDER_SHIPMENT_TITLE2"] = "Wysyłka ##ACCOUNT_NUMBER#";
$MESS["CRM_ORDER_SHIPMENT_TRACKING_DESCRIPTION"] = "Opis statusu wysyłki";
$MESS["CRM_ORDER_SHIPMENT_TRACKING_LAST_CHANGE"] = "Ostatni status wysyłki";
$MESS["CRM_ORDER_SHIPMENT_TRACKING_LAST_CHECK"] = "Status wysyłki sprawdzono ostatnio";
$MESS["CRM_ORDER_SHIPMENT_TRACKING_NUMBER"] = "Numer śledzenia";
$MESS["CRM_ORDER_SHIPMENT_TRACKING_STATUS"] = "Status wysyłki";
?>