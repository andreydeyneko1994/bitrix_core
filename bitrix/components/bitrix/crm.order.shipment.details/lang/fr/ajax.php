<?
$MESS["CRM_ORDER_SD_ERROR_SHIPPING_DEDUCTED"] = "Impossible d'ajouter le produit parce que la livraison a déjà été lancée.";
$MESS["CRM_ORDER_SD_FORM_DATA_MISSING"] = "Certaines données du formulaire manquent";
$MESS["CRM_ORDER_SD_INSUFFICIENT_RIGHTS"] = "Permissions insuffisantes.";
$MESS["CRM_ORDER_SD_SHIPMENT_NOT_FOUND"] = "Livraison introuvable";
?>