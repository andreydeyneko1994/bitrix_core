<?
$MESS["RPA_KANBAN_PERMISSION_ERROR"] = "Vous n’avez pas les autorisations pour modifier les préférences du flux de travail";
$MESS["RPA_KANBAN_POPUP_CANCEL"] = "Annuler";
$MESS["RPA_KANBAN_POPUP_SAVE"] = "Enregistrer";
$MESS["RPA_KANBAN_STAGE_NOT_FOUND"] = "L'étape est introuvable";
?>