<?php
$MESS["RPA_KANBAN_PERMISSION_ERROR"] = "No tiene permisos para editar las preferencias del procesos de negocio";
$MESS["RPA_KANBAN_POPUP_CANCEL"] = "Cancelar";
$MESS["RPA_KANBAN_POPUP_SAVE"] = "Guardar";
$MESS["RPA_KANBAN_STAGE_NOT_FOUND"] = "No se encontró la etapa";
