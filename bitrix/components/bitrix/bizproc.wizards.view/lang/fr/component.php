<?
$MESS["BPABL_PAGE_TITLE"] = "Processus d'affaires";
$MESS["BPWC_WLC_EMPTY_BPID"] = "Le code du processus d'entreprise n'est pas indiqué.";
$MESS["BPWC_WLC_EMPTY_IBLOCK"] = "Le code du bloc d'information n'est pas indiqué.";
$MESS["BPWC_WLC_ERROR"] = "Erreur";
$MESS["BPWC_WLC_WRONG_BP"] = "Le processus d'affaires n'a pas été trouvé.";
$MESS["BPWC_WLC_WRONG_IBLOCK"] = "Le bloc d'information spécifié dans les réglages du composant n'est pas trouvé";
$MESS["BPWC_WNC_EMPTY_IBLOCK_TYPE"] = "Le type du bloc d'information n'est pas spécifié.";
$MESS["BPWC_WNC_SESSID"] = "Erreur de sécurité. Il faut remplir une forme encore une fois.";
$MESS["BPWC_WNC_WRONG_IBLOCK_TYPE"] = "Un type du bloc d'information indiqué dans les réglages du composant n'est pas retrouvé";
?>