<?php
$MESS["VOX_LINES_ERROR_EMPTY_NAME"] = "Introduzca el nombre del grupo de números";
$MESS["VOX_LINES_ERROR_GROUP_NOT_FOUND"] = "No se encontró el grupo de números";
$MESS["VOX_LINES_ERROR_NAME_EXISTS"] = "Ya existe un grupo de números o una conexión SIP con este nombre";
$MESS["VOX_LINES_ERROR_NO_NUMBERS"] = "Seleccione al menos un número";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_FOUND"] = "No se encontró el número";
$MESS["VOX_LINES_ERROR_NUMBER_NOT_IN_GROUP"] = "El número no pertenece a ningún grupo de números conocidos";
