<?
$MESS["VOX_LINES_ADD_NUMBER_GROUP"] = "Créer un pool de numéros";
$MESS["VOX_LINES_BUNDLE_WILL_BE_DELETED"] = "Le lot sera déconnecté sous 24 heures.";
$MESS["VOX_LINES_BUTTON_CANCEL"] = "Annuler";
$MESS["VOX_LINES_BUTTON_CREATE"] = "Créer";
$MESS["VOX_LINES_CALLERID_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le numéro #NUMBER# ?";
$MESS["VOX_LINES_CONFIRM_ACTION"] = "Confirmer l'action";
$MESS["VOX_LINES_CONFIRM_BUNDLE_DISCONNECTION"] = "Un lot ne peut pas être partiellement déconnecté. Voulez-vous déconnecter tous les numéros du lot ?";
$MESS["VOX_LINES_CREATE_NUMBER_GROUP"] = "Créer un pool de numéros";
$MESS["VOX_LINES_ERROR"] = "Erreur";
$MESS["VOX_LINES_NUMBER_DELETE_CONFIRM"] = "Voulez-vous vraiment déconnecter le numéro #NUMBER# de votre Bitrix24 ?";
$MESS["VOX_LINES_NUMBER_GROUP_NAME"] = "Nom du pool de numéros";
$MESS["VOX_LINES_NUMBER_RENTED_IN_BUNDLE"] = "Ce numéro a été loué avec un lot de #COUNT# numéros : ";
$MESS["VOX_LINES_NUMBER_WILL_BE_DELETED"] = "Le numéro sera déconnecté sous 24 heures.";
$MESS["VOX_LINES_SELECT_UNASSIGNED_NUMBERS"] = "Sélectionner les numéros à ajouter au pool de numéros";
$MESS["VOX_LINES_SIP_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cette connexion ?";
?>