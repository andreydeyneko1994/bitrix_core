<?
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_1"] = "Szybkie wdrażanie dla nowych agentów sprzedaży. Reguły automatyzacji i wyzwalacze ustalają, kto co ma zrobić i kiedy.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_2"] = "Uwolnij swoich agentów sprzedaży od rutyny.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_3"] = "Kontroluj każdy krok procesu sprzedaży. Bitrix24 CRM natychmiast powiadamia przełożonych o wszelkich problemach dotyczących agentów, dokumentów lub wąskich gardeł.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_1"] = "Zakładka Automatyzacja jest dostępna we wszystkich właściwe jednostkach CRM.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_2"] = "Ustanawiaj reguły automatyzacji, aby wspomóc rozwój swojej firmy.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_3"] = "Testuj reguły automatyzacji oraz wyzwalacze i zobacz, jak działają w czasie rzeczywistym.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE"] = "Zautomatyzuj sprzedaż i zarządzanie klientami za pomocą CRM Bitrix24";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE_2"] = "Łatwo ustanawiaj reguły i wyzwalacze";
?>