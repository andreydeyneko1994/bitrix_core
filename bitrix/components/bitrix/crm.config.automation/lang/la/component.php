<?php
$MESS["CRM_AUTOMATION_TITLE"] = "Automatización";
$MESS["CRM_AUTOMATION_TITLE_1"] = "Automatización de ventas";
$MESS["CRM_BP_DEAL"] = "Negociación";
$MESS["CRM_BP_LEAD"] = "Prospecto";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
