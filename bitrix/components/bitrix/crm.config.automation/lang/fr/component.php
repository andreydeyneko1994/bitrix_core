<?php
$MESS["CRM_AUTOMATION_TITLE"] = "Automatisation";
$MESS["CRM_AUTOMATION_TITLE_1"] = "Automatisation des ventes";
$MESS["CRM_BP_DEAL"] = "Transaction";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
