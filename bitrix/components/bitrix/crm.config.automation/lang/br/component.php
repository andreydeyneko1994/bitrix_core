<?php
$MESS["CRM_AUTOMATION_TITLE"] = "Automação";
$MESS["CRM_AUTOMATION_TITLE_1"] = "Automação de vendas";
$MESS["CRM_BP_DEAL"] = "Negócio";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
