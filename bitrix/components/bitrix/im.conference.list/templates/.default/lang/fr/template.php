<?php
$MESS["CONFERENCE_LIST_BUTTON_CREATE"] = "Ajouter";
$MESS["CONFERENCE_LIST_CONTEXT_MENU_CHAT"] = "Chat";
$MESS["CONFERENCE_LIST_CONTEXT_MENU_DELETE"] = "Supprimer";
$MESS["CONFERENCE_LIST_CONTEXT_MENU_EDIT"] = "Éditer";
$MESS["CONFERENCE_LIST_EMPTY_BUTTON_CREATE"] = "Créer une conférence";
$MESS["CONFERENCE_LIST_EMPTY_TITLE"] = "Vidéoconférence #HD# pour 24 participants maximum";
$MESS["CONFERENCE_LIST_EMPTY_TITLE_HD"] = "HD";
$MESS["CONFERENCE_LIST_EMPTY_TITLE_NEW"] = "Vidéo conférence #HD#, jusqu'à #LIMIT# personnes";
$MESS["CONFERENCE_LIST_NOTIFICATION_LINK_COPIED"] = "Le lien de la vidéoconférence a été copié dans le Presse-papiers";
$MESS["CONFERENCE_LIST_TITLE"] = "Video Conferencing";
