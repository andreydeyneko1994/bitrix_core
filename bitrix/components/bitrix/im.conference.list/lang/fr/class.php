<?php
$MESS["CONFERENCE_LIST_ACTION_EDIT"] = "Éditer";
$MESS["CONFERENCE_LIST_ACTION_OPEN_CHAT"] = "Ouvrir chat";
$MESS["CONFERENCE_LIST_GRID_COLUMN_CONTROLS"] = "Gérer la conférence";
$MESS["CONFERENCE_LIST_GRID_COLUMN_DATE"] = "Date";
$MESS["CONFERENCE_LIST_GRID_COLUMN_HOST"] = "Hôte";
$MESS["CONFERENCE_LIST_GRID_COLUMN_NAME"] = "Conférence";
$MESS["CONFERENCE_LIST_GRID_COLUMN_RECORD"] = "Enregistrer la vidéo et le son";
$MESS["CONFERENCE_LIST_GRID_COLUMN_STATUS"] = "Statut";
$MESS["CONFERENCE_LIST_GRID_CONTROLS_COPY"] = "Lien";
$MESS["CONFERENCE_LIST_GRID_CONTROLS_FINISHED_TITLE"] = "Achevé(e)s";
$MESS["CONFERENCE_LIST_GRID_CONTROLS_START"] = "Commencer";
$MESS["CONFERENCE_LIST_PRESET_ALL"] = "Tout";
$MESS["CONFERENCE_LIST_PRESET_MY"] = "Mes articles";
$MESS["CONFERENCE_LIST_STATUS_ACTIVE"] = "En cours";
$MESS["CONFERENCE_LIST_STATUS_FINISHED"] = "Achevé(e)s";
$MESS["CONFERENCE_LIST_STATUS_NOT_STARTED"] = "Programmé";
