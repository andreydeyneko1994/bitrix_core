<?php
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_ESHOP_DEFAULT_NAME"] = "Sklep internetowy";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_MODULE_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_CONNECT"] = "Nie udało się ustanowić połączenia testowego przy użyciu podanych parametrów";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_DATA_SAVE"] = "Brak danych do zapisania";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_SAVE"] = "Wystąpił błąd podczas zapisywania danych. Sprawdź swoje dane wejściowe i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_CONNECT"] = "Połączenie testowe zostało pomyślnie ustanowione";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_REGISTER"] = "Rejestracja zakończona z powodzeniem";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Proszę prześlij formularz ponownie.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_WELCOME_MESSAGE_DEFAULT"] = "Cześć, tu czat bot #COMPANY_TITLE#. W czym mogę pomóc?";
