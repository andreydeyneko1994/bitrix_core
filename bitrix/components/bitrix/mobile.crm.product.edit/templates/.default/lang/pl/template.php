<?
$MESS["M_CRM_PRODUCT_EDIT_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_PRODUCT_EDIT_CREATE_TITLE"] = "Utwórz produkt";
$MESS["M_CRM_PRODUCT_EDIT_EDIT_TITLE"] = "Edycja";
$MESS["M_CRM_PRODUCT_EDIT_SAVE_BTN"] = "Zapisz";
$MESS["M_CRM_PRODUCT_EDIT_VIEW_TITLE"] = "Wyświetl produkt";
$MESS["M_CRM_PRODUCT_MENU_DELETE"] = "Usuń";
$MESS["M_CRM_PRODUCT_MENU_EDIT"] = "Edycja";
$MESS["M_DETAIL_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_DETAIL_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_DETAIL_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
?>