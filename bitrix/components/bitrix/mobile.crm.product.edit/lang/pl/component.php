<?
$MESS["CRM_FIELD_ACTIVE"] = "Aktywny";
$MESS["CRM_FIELD_CURRENCY"] = "Waluta";
$MESS["CRM_FIELD_MEASURE"] = "Jednostka miary";
$MESS["CRM_FIELD_PRICE"] = "Cena";
$MESS["CRM_FIELD_PRODUCT_DESCRIPTION"] = "Opis";
$MESS["CRM_FIELD_PRODUCT_NAME"] = "Nazwa";
$MESS["CRM_FIELD_SECTION"] = "Sekcja";
$MESS["CRM_FIELD_SORT"] = "Sortuj";
$MESS["CRM_FIELD_VAT_ID"] = "Stawka VAT";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "VAT zawarty";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[nie wybrano]";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRODUCT_ADD_UNKNOWN_ERROR"] = "Błąd tworzenia produktu.";
$MESS["CRM_PRODUCT_DELETE_UNKNOWN_ERROR"] = "Błąd usuwania produktu.";
$MESS["CRM_PRODUCT_FIELD_DETAIL_PICTURE"] = "Pełny obraz";
$MESS["CRM_PRODUCT_FIELD_PREVIEW_PICTURE"] = "Zdjęcie poglądowe";
$MESS["CRM_PRODUCT_NAV_TITLE_ADD"] = "Nowy produkt";
$MESS["CRM_PRODUCT_NAV_TITLE_EDIT"] = "Produkt: #NAME#";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "Produkty";
$MESS["CRM_PRODUCT_NOT_FOUND"] = "Produktu nie odnaleziono.";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Pobierz";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Powiększ";
$MESS["CRM_PRODUCT_UPDATE_UNKNOWN_ERROR"] = "Błąd aktualizacji produktu.";
$MESS["CRM_SECTION_PRODUCT_INFO"] = "Informacja o produkcie";
$MESS["CRM_SECTION_PRODUCT_SECTION"] = "Sekcja produktu";
?>