<?php
$MESS["CRM_DEDUPE_LIST_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_LIST_COL_ORGANIZATION"] = "Nom de l'entreprise";
$MESS["CRM_DEDUPE_LIST_COL_PERSON"] = "Nom complet";
$MESS["CRM_DEDUPE_LIST_COL_PHONE"] = "Téléphone";
$MESS["CRM_DEDUPE_LIST_COL_RESPONSIBLE"] = "Personne responsable";
$MESS["CRM_DEDUPE_LIST_COL_TITLE"] = "Dénomination";
$MESS["CRM_DEDUPE_LIST_DEFAUL_SCOPE_TITLE"] = "Non sélectionné";
$MESS["CRM_DEDUPE_LIST_INVALID_ENTITY_TYPE"] = "Le type '#TYPE_NAME#' est pas pris en charge dans ce contexte.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND"] = "Vous ne pouvez pas obtenir une liste des doublons par champ '#NAME#'. S'il vous plaît reconstruire la liste.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG"] = "Pas de doublons pour '#NAME#' trouvé.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG_PLURAL"] = "Pas de doublons trouvés pour champs  #NAMES#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
