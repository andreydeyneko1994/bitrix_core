<?php
$MESS["LM_ADD1"] = "Tous les employés";
$MESS["LM_ADD2"] = "Ajouter plus";
$MESS["TELEPHONY_ALREADY_CONNECT_RENT"] = "Vous avez un numéro loué configuré. Avant d'effectuer cette action, vous devez désactiver tous les numéros loués.";
$MESS["TELEPHONY_ALREADY_CONNECT_SIP"] = "Vous avez un PBX connecté. Avant d'effectuer cette action, vous devez enlever toutes les connexions PBX.";
$MESS["TELEPHONY_CALLERID_NUMBER"] = "Vous avez connecté le numéro #CALLER_ID#";
$MESS["TELEPHONY_CANCEL"] = "Annuler";
$MESS["TELEPHONY_CONFIG"] = "Appels sortants";
$MESS["TELEPHONY_CONFIG_CHECK_CRM"] = "Vérifier un numéro sortant dans le CRM";
$MESS["TELEPHONY_CONFIG_RECORD"] = "Enregistrer un enregistrement de toutes les conversations";
$MESS["TELEPHONY_CONFIG_RECORD_TITLE"] = "(tous les enregistrements de conversations seront disponibles dans les statistiques d'appels et enregistré dans le \"Drive de l'entreprise\" dans le dossier \"Enregistrements de téléphonie\")";
$MESS["TELEPHONY_CONFIG_RECORD_WARN"] = "Assurez-vous que l'utilisation de cette option ne viole pas la politique de l'entreprise ou les lois de votre pays ou des pays dont votre entreprise est tenue de respecter les lois.";
$MESS["TELEPHONY_CONFIRM"] = "Confirmer";
$MESS["TELEPHONY_CONFIRM_DATE"] = "Le numéro a été confirmé, il sera actif jusqu'au #DATE#, après quoi il sera automatiquement désactivé jusqu'à sa nouvelle confirmation. Vous pouvez étendre la date de la prochaine confirmation en le reconfirmant à tout moment.";
$MESS["TELEPHONY_CONFIRM_PHONE"] = "Votre numéro n'est pas vérifié";
$MESS["TELEPHONY_DELETE"] = "supprimer";
$MESS["TELEPHONY_DELETE_CONFIRM"] = "Voulez-vous vraiment confirmer ce numéro de téléphone ?";
$MESS["TELEPHONY_EDIT"] = "Modifier";
$MESS["TELEPHONY_EMPTY_PHONE"] = "Aucun numéro de téléphone indiqué";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "La personne appelée verra un numéro de téléphone relais";
$MESS["TELEPHONY_ERROR_BLOCK"] = "Le numéro que vous avez entré a été bloqué. Veuillez contacter le support technique.";
$MESS["TELEPHONY_ERROR_MONEY_LOW"] = "Votre solde actuel est trop bas pour réaliser cet appel.";
$MESS["TELEPHONY_ERROR_PHONE"] = "Le numéro entré est invalide. Le numéro doit être entré au format international.";
$MESS["TELEPHONY_ERROR_REMOVE"] = "Erreur de désactivation du numéro, veuillez réessayer.";
$MESS["TELEPHONY_EXAMPLE"] = "+1 (800) 800-88-88";
$MESS["TELEPHONY_JOIN"] = "Confirmer";
$MESS["TELEPHONY_JOIN_TEXT"] = "Vous pouvez changer le numéro en supprimant le numéro actuel et en confirmant un nouveau numéro.";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "Le numéro de téléphone n'a pas été vérifié. Vous devez vérifier ce numéro de téléphone pour rendre votre ID d'appel visible aux autres personnes.";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "Configurer le numéro de téléphone";
$MESS["TELEPHONY_OR"] = "ou";
$MESS["TELEPHONY_PHONE"] = "Ce numéro sera visible pour la personne appelée.";
$MESS["TELEPHONY_PUT_CODE"] = "Entrez le code de vérification : ";
$MESS["TELEPHONY_PUT_PHONE"] = "Saisir le numéro de téléphone de l'entreprise";
$MESS["TELEPHONY_PUT_PHONE_AGAING"] = "Entrer le numéro une nouvelle fois";
$MESS["TELEPHONY_RECALL"] = "Appeler à nouveau";
$MESS["TELEPHONY_REJOIN"] = "Reconfirmer le numéro";
$MESS["TELEPHONY_RETRY"] = "Réessayer";
$MESS["TELEPHONY_SAVE"] = "Enregistrer";
$MESS["TELEPHONY_USE_IVR"] = "Utiliser le menu IVR (Répondeur vocal interactif) &nbsp; &mdash; &nbsp;";
$MESS["TELEPHONY_USE_IVR_2"] = "Utiliser le menu vocal";
$MESS["TELEPHONY_VERIFY_ALERT"] = "Le préposé automatique peut effectuer plus d'un appel par minute. Veuillez réessayer plus tard.";
$MESS["TELEPHONY_VERIFY_CODE"] = "Un appel automatique sera réalisé pour ce numéro de téléphone et le code de vérification sera fourni oralement.";
$MESS["TELEPHONY_VERIFY_CODE_2"] = "Un appel automatique va maintenant être réalisé pour ce numéro de téléphone, le code de vérification sera fourni oralement.";
$MESS["TELEPHONY_VERIFY_CODE_3"] = "Vous devez entrer le code de confirmation correctement avec 10 essais, autrement le numéro entré sera bloqué.";
$MESS["TELEPHONY_VERIFY_CODE_4"] = "L'appel passé à votre nouveau numéro de téléphone entrant pour fournir le code de confirmation est un appel payant. Votre compte sera débité selon le tarif d'un appel sortant pour cet appel.";
$MESS["TELEPHONY_VERIFY_PHONE"] = "Confirmer le numéro entré";
$MESS["TELEPHONY_WRONG_CODE"] = "Code invalide";
$MESS["VI_CONFIG_ALLOW_TO_SELECT_NUMBER_FOR_OUTGOING_CALL"] = "Les employés peuvent sélectionner ce numéro pour les appels sortants";
$MESS["VI_CONFIG_BACKUP_LINE"] = "Transférer les appels vers le numéro de secours à partir de";
$MESS["VI_CONFIG_BACKUP_NUMBER"] = "Numéro de téléphone de secours";
$MESS["VI_CONFIG_CALLBACK_REDIAL"] = "Recomposer le rappel manqué";
$MESS["VI_CONFIG_CALLBACK_REDIAL_ATTEMPTS"] = "Tentatives de rappel";
$MESS["VI_CONFIG_CALLBACK_REDIAL_PERIOD"] = "Délai entre tentatives";
$MESS["VI_CONFIG_CALLBACK_REDIAL_PERIOD_SECONDS"] = "secondes";
$MESS["VI_CONFIG_CALLBACK_SETTINGS"] = "Rappel";
$MESS["VI_CONFIG_CONFIGURE_CRM_EXCEPTIONS_LIST"] = "Éditer les exceptions CRM";
$MESS["VI_CONFIG_CREATE_GROUP"] = "Créer un nouveau groupe de file d'attente";
$MESS["VI_CONFIG_CREATE_IVR"] = "Créer un nouveau menu";
$MESS["VI_CONFIG_EDIT_APPLY"] = "Appliquer";
$MESS["VI_CONFIG_EDIT_AUTO_ANSWER"] = "Messagerie vocale";
$MESS["VI_CONFIG_EDIT_AUTO_ANSWER_TIP"] = "Si la personne ne répond pas, le service de messagerie vocale sera activé.";
$MESS["VI_CONFIG_EDIT_BACK"] = "Précédent";
$MESS["VI_CONFIG_EDIT_CANCEL"] = "Annuler";
$MESS["VI_CONFIG_EDIT_CRM_CHECKING"] = "Vérifier le numéro dans tous les dossiers de CRM";
$MESS["VI_CONFIG_EDIT_CRM_CHECKING_OMITTED_CALL"] = "Si l'utilisateur Bitrix24 administrateur ne répond pas &nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_EDIT_CRM_CHECKING_OMITTED_CALL_NEW"] = "Si le responsable ne répond pas";
$MESS["VI_CONFIG_EDIT_CRM_CHECKING_TIP"] = "(un appel entrant peut être automatiquement transmis à l'associé des ventes responsable que si le client est identifié)";
$MESS["VI_CONFIG_EDIT_CRM_CREATE"] = "Si le nombre ne se trouve pas dans le CRM &nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_1"] = "proposer de créer un nouvel enregistrement dans la fenêtre d'appel";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_2"] = "créer automatiquement un nouveau prospect";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_CALL_TYPE"] = "Créer un prospect pour";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_CALL_TYPE_ALL"] = "Tous les appels";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_CALL_TYPE_INCOMING"] = "Appels entrants";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_CALL_TYPE_NEW"] = "Créer un prospect pour";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_CALL_TYPE_OUTGOING"] = "Appels sortants";
$MESS["VI_CONFIG_EDIT_CRM_CREATE_NEW"] = "Si le numéro n'est pas enregistré avec le CRM";
$MESS["VI_CONFIG_EDIT_CRM_FORWARD"] = "Transférer Les appels entrants à l'utilisateur Bitrix24 administrateur si l'appelant est identifié";
$MESS["VI_CONFIG_EDIT_CRM_FORWARD_2"] = "Envoyer un appel entrant à une personne responsable spécifiée dans le CRM si un contact est identifié comme connu";
$MESS["VI_CONFIG_EDIT_CRM_SOURCE"] = "Nouvelle source de prospects&nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_EDIT_CRM_SOURCE_NEW"] = "Nouvelle source de prospect";
$MESS["VI_CONFIG_EDIT_CRM_TRANSFER_CHANGE"] = "Changer automatiquement l'utilisateur responsable du prospect lors du transfert manuel d'un appel";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_1"] = "Transférer au groupe de file d'attente";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_1_2"] = "Poursuivre le traitement de l'appel";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_2_1"] = "Laisser un message vocal";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_3"] = "Transmettre au téléphone portable";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_3_2"] = "Transférer un appel à un collègue";
$MESS["VI_CONFIG_EDIT_DEALING_WITH_OMITTED_CALL_3_3"] = "Transfert appel à un gestionnaire";
$MESS["VI_CONFIG_EDIT_DOWNLOAD_TUNE"] = "Télécharger de la musique ou de l'enregistrement";
$MESS["VI_CONFIG_EDIT_DOWNLOAD_TUNE_TIP"] = "pas plus de 2 Mo au format mp3";
$MESS["VI_CONFIG_EDIT_ERROR"] = "Erreur";
$MESS["VI_CONFIG_EDIT_EXT_NUM_PROCESSING"] = "Traitement des numéros de poste";
$MESS["VI_CONFIG_EDIT_EXT_NUM_PROCESSING_HELP"] = "Cette fonctionnalité nécessite que la lecture du message de bienvenue soit activée dans les préférences du numéro de téléphone.";
$MESS["VI_CONFIG_EDIT_EXT_NUM_PROCESSING_OMITTED_CALL"] = "Si l'utilisateur Bitrix24 ne répond pas &nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_EDIT_EXT_NUM_PROCESSING_TIP"] = "Un appel entrant peut être redirigé vers un utilisateur Bitrix24 particulier si le client entre le numéro de poste correspondant. Vous pouvez configurer les numéros de poste des utilisateurs Bitrix24 dans leurs profils.";
$MESS["VI_CONFIG_EDIT_FORWARD_NUMBER"] = "Changer le numéro de transfert vers";
$MESS["VI_CONFIG_EDIT_FORWARD_NUMBER_TIP"] = "Par défaut, un appel sera transféré dans la connexion en cours. Vous pouvez sélectionner une autre connexion disponible pour transférer un appel.";
$MESS["VI_CONFIG_EDIT_FORWARD_TITLE"] = "Transférer au numéro";
$MESS["VI_CONFIG_EDIT_IVR_SELECTION"] = "Sélectionner le menu vocal";
$MESS["VI_CONFIG_EDIT_NO_ANSWER_ACTION_2"] = "Activer la messagerie vocale";
$MESS["VI_CONFIG_EDIT_NO_ANSWER_ACTION_4"] = "Mettre fin à l'appel";
$MESS["VI_CONFIG_EDIT_NO_ANSWER_ACTION_5"] = "Transférer l'appel au numéro spécifié";
$MESS["VI_CONFIG_EDIT_PLAY_WELCOME_MELODY"] = "Lire la musique d'accueil";
$MESS["VI_CONFIG_EDIT_RECORD"] = "Enregistrer tous les appels";
$MESS["VI_CONFIG_EDIT_RECORD_NOTICE"] = "Lecture de l'avertissement 'votre appel sera enregistré' pour les appels entrants";
$MESS["VI_CONFIG_EDIT_RECORD_STEREO"] = "Enregistrer un son stéréo";
$MESS["VI_CONFIG_EDIT_RECORD_STEREO_HINT"] = "Activez cette option si vous utilisez une application ou un service de reconnaissance vocale tiers. Le son stéréo n'est pas nécessaire si vous utilisez le moteur de reconnaissance vocale intégré de Bitrix24.";
$MESS["VI_CONFIG_EDIT_RECORD_TIP"] = "(tous les enregistrements de conversations seront disponibles dans les statistiques d'appels et enregistré dans le \"Drive de l'entreprise\" dans le dossier \"Enregistrements de téléphonie\")";
$MESS["VI_CONFIG_EDIT_RECORD_TIP2"] = "Assurez-vous que l'utilisation de cette option ne viole pas la politique de l'entreprise ou les lois de votre pays ou des pays dont votre entreprise est tenue de respecter les lois.";
$MESS["VI_CONFIG_EDIT_RECORD_TIP_3"] = "Tous les enregistrements d'appels sont enregistrés dans le Lecteur partagé (dossier « Enregistrements d'appels ») et mis à disposition dans les statistiques des appels.";
$MESS["VI_CONFIG_EDIT_ROUTE_INCOMING"] = "Routage d'appel entrant";
$MESS["VI_CONFIG_EDIT_SAVE"] = "Enregistrer";
$MESS["VI_CONFIG_EDIT_SET_DEFAULT_TUNE"] = "Retour à la musique originale";
$MESS["VI_CONFIG_EDIT_SIP_HEADER_PROCESSING"] = "Manipulation de To : en-tête SIP";
$MESS["VI_CONFIG_EDIT_SIP_HEADER_PROCESSING_TIP"] = "L'appel entrant sera transmis à un employé dont l'extension interne se trouve dans le To : en-tête SIP de la requête INVITE";
$MESS["VI_CONFIG_EDIT_TIMEMAN_SUPPORT"] = "Ne pas transférer d'appels aux utilisateurs qui n'ont pas commencé leur journée de travail ou qui sont en pause";
$MESS["VI_CONFIG_EDIT_TIMEMAN_SUPPORT_B24_2"] = "Cette option n'est pas disponible car la gestion du temps de travail est désactivée sur votre Bitrix24 ou indisponible avec l'offre que vous utilisez.";
$MESS["VI_CONFIG_EDIT_TIMEMAN_SUPPORT_CP"] = "Vous ne pouvez pas activer cette option parce que le module de gestion du temps n'est pas installé ou activé.";
$MESS["VI_CONFIG_EDIT_TRANSCRIBE"] = "Transcrire l'enregistrement de l'appel";
$MESS["VI_CONFIG_EDIT_TRANSCRIBE_LANGUAGE"] = "Langue de transcription";
$MESS["VI_CONFIG_EDIT_TRANSCRIBE_PROVIDER"] = "Service de transcription";
$MESS["VI_CONFIG_EDIT_TUNES"] = "Salutations et enregistrements";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE"] = "<strong>Langue</strong>&nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE2"] = "Langue de la musique d'attente";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_BR"] = "Portugais";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_DE"] = "Allemand";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_EN"] = "Anglais";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_ES"] = "Espagnol";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_RU"] = "Russe";
$MESS["VI_CONFIG_EDIT_TUNES_LANGUAGE_UA"] = "Ukrainien";
$MESS["VI_CONFIG_EDIT_TUNES_TIP"] = "Vous pouvez charger votre propre musique pour les appels ou utiliser la musique norme prévue dans le système.";
$MESS["VI_CONFIG_EDIT_UPLOAD_SUCCESS"] = "Succès ton de téléchargement";
$MESS["VI_CONFIG_EDIT_WORKTIME"] = "Réglages de l'heure";
$MESS["VI_CONFIG_EDIT_WORKTIME_DAYOFF"] = "Jours de congé";
$MESS["VI_CONFIG_EDIT_WORKTIME_DAYOFF_MELODY"] = "Message enregistré en dehors des heures d'ouverture";
$MESS["VI_CONFIG_EDIT_WORKTIME_DAYOFF_MELODY_TEXT"] = "Cet enregistrement sera joué pendant les heures creuses";
$MESS["VI_CONFIG_EDIT_WORKTIME_DAYOFF_NUMBER"] = "Transférer à ce numéro";
$MESS["VI_CONFIG_EDIT_WORKTIME_DAYOFF_RULE"] = "Traitement des appels en dehors des heures de travail";
$MESS["VI_CONFIG_EDIT_WORKTIME_ENABLE"] = "Heures de téléphone";
$MESS["VI_CONFIG_EDIT_WORKTIME_HOLIDAYS"] = "Jours fériés";
$MESS["VI_CONFIG_EDIT_WORKTIME_HOLIDAYS_EXAMPLE"] = "Exemple : 01.1, 07.4, 12.25";
$MESS["VI_CONFIG_EDIT_WORKTIME_HOLIDAYS_EXAMPLE_DAYS"] = "01.01,04.07,01.11,25.14";
$MESS["VI_CONFIG_EDIT_WORKTIME_HOLIDAYS_EXAMPLE_EXAMPLE"] = "Exemple";
$MESS["VI_CONFIG_EDIT_WORKTIME_TIME"] = "Heures de travail";
$MESS["VI_CONFIG_EDIT_WORKTIME_TIMEZONE"] = "Fuseau horaire";
$MESS["VI_CONFIG_GROUP_SETTINGS"] = "Paramètres du groupe de file d'attente";
$MESS["VI_CONFIG_GROUP_SETTINGS_HIDE"] = "Cacher les paramètres du groupe de file d'attente";
$MESS["VI_CONFIG_IVR_SETTINGS"] = "Réglages de menu";
$MESS["VI_CONFIG_LINE_ADD"] = "Ajouter";
$MESS["VI_CONFIG_LINE_ALLOWED_USERS"] = "Les employés sont autorisés à effectuer des appels avec ce numéro de téléphone";
$MESS["VI_CONFIG_LINE_PREFIX"] = "Préfixe pour les appels via ce numéro";
$MESS["VI_CONFIG_LINE_PREFIX_HINT"] = "Utiliser le préfixe pour effectuer des appels sortants depuis une ligne de téléphone actuelle en le composant avant de composer un numéro requis";
$MESS["VI_CONFIG_LINE_UNKNOWN_ACCESS_CODE"] = "(ID d'accès inconnu)";
$MESS["VI_CONFIG_LINK_CALLS_WARNING"] = "Ce numéro ne peut pas accepter les appels entrants. Les préférences du numéro comprennent uniquement les appels entrants et les rappels.";
$MESS["VI_CONFIG_LOCK_ALT"] = "Restrictions d'appel, cliquez pour afficher les détails";
$MESS["VI_CONFIG_LOCK_RECORD_ALT"] = "Vous pouvez effectuer #LIMIT# appels par mois, et il vous en reste #REMAINING#. Cliquez pour afficher les détails.";
$MESS["VI_CONFIG_MORE_TONES"] = "Afficher d'autres musiques";
$MESS["VI_CONFIG_NEW_RENT"] = "Félicitations ! Le numéro de téléphone est connecté à votre Bitrix24 !";
$MESS["VI_CONFIG_NEW_RESERVE"] = "Félicitations ! Le numéro de téléphone est réservé pour vous !";
$MESS["VI_CONFIG_NUMBER_USAGE_FOR_OUTGOING_CALL"] = "Utiliser le numéro pour les appels sortants";
$MESS["VI_CONFIG_REDIRECT_WITH_CLIENT_NUMBER"] = "Essayez de transmettre l'identification de l'appelant lors du renvoi d'un appel";
$MESS["VI_CONFIG_REDIRECT_WITH_CLIENT_NUMBER_TIP"] = "Si cette case est cochée, le système tente de transmettre l'ID de l'appelant lors du transfert d'un appel à un salarié. Si cette case n'est pas cochée, le numéro loué sera utilisé à la place.";
$MESS["VI_CONFIG_RESERVE_NOTICE"] = "Pour utiliser un numéro de téléphone loué, vous devez envoyer en ligne les documentations légales. #LINK_START#Envoyer maintenant la documentation#LINK_END#";
$MESS["VI_CONFIG_ROUTE_TO"] = "Routage";
$MESS["VI_CONFIG_ROUTE_TO_GROUP"] = "Router l'appel entrant vers le groupe de file d'attente";
$MESS["VI_CONFIG_ROUTE_TO_SELECT"] = "Transférer l'appel entrant au groupe de file d'attente &nbsp; &mdash; &nbsp;";
$MESS["VI_CONFIG_SELECT_GROUP"] = "Sélectionner le groupe de file d'attente";
$MESS["VI_CONFIG_SET_BACKUP_NUMBER"] = "Indiquez le numéro vers lequel transférer les appels si votre compte Bitrix24 est indisponible pour une raison ou une autre. Un numéro de secours indiqué dans les paramètres de téléphonie est utilisé par défaut.";
$MESS["VI_CONFIG_SET_USE_SPECIFIC_BACKUP_NUMBER_USE"] = "Activer le numéro de téléphone de secours";
$MESS["VI_CONFIG_SIP_ADD"] = "Ajouter";
$MESS["VI_CONFIG_SIP_ADD_CONNECTION_NUMBER"] = "ajouter";
$MESS["VI_CONFIG_SIP_CANCEL"] = "Annuler";
$MESS["VI_CONFIG_SIP_CLOUD_DEF"] = "PBX hébergé sur Cloud (#ID#)";
$MESS["VI_CONFIG_SIP_CLOUD_TITLE"] = "Paramètres de connexion pour le cloud PBX hébergé";
$MESS["VI_CONFIG_SIP_CONFIG_INFO"] = "Veuillez vous référer au #LINK_START#cours de formation#LINK_END# pour vous renseigner sur la configuration et la connexion.";
$MESS["VI_CONFIG_SIP_CONNECTION_NUMBERS"] = "Numéros de téléphone";
$MESS["VI_CONFIG_SIP_C_CONFIG"] = "Veuillez spécifier les paramètres de connexion pour appeler et accepter des appels dans votre Bitrix24 en utilisant votre fournisseur de cloud : ";
$MESS["VI_CONFIG_SIP_C_IN"] = "S'il vous plaît attendre pour la connexion à remplir avant d'accepter les appels entrants.";
$MESS["VI_CONFIG_SIP_C_NUMBER"] = "Nom de la connexion";
$MESS["VI_CONFIG_SIP_C_NUMBER_HINT"] = "champ facultatif";
$MESS["VI_CONFIG_SIP_C_STATUS"] = "Etat de la connexion";
$MESS["VI_CONFIG_SIP_C_STATUS_ERROR"] = "erreur";
$MESS["VI_CONFIG_SIP_C_STATUS_ERROR_DESC"] = "Erreur essayer de se connecter. S'il vous plaît vérifier vos préférences de connexion.";
$MESS["VI_CONFIG_SIP_C_STATUS_IN_PROGRESS"] = "En cours de réalisation";
$MESS["VI_CONFIG_SIP_C_STATUS_IN_PROGRESS_DESC"] = "Connexion en cours. Vous accepterez les appels entrants dans quelques instants.";
$MESS["VI_CONFIG_SIP_C_STATUS_SUCCESS"] = "connecté";
$MESS["VI_CONFIG_SIP_C_STATUS_SUCCESS_DESC"] = "Connexion a été effectuée. Votre Bitrix24 peut maintenant accepter les appels entrants.";
$MESS["VI_CONFIG_SIP_C_STATUS_WAIT"] = "obtention des données";
$MESS["VI_CONFIG_SIP_C_STATUS_WAIT_DESC"] = "Obtenir les données d'état actuelles. Veuillez attendre.";
$MESS["VI_CONFIG_SIP_DETECTION_HEADER_ORDER"] = "Priorité des en-têtes";
$MESS["VI_CONFIG_SIP_DETECT_INCOMING_NUMBER"] = "Obtenez le numéro de destination à partir des en-têtes SIP";
$MESS["VI_CONFIG_SIP_ERROR_1"] = "Dernière mise à jour : #DATE#";
$MESS["VI_CONFIG_SIP_ERROR_2"] = "Code de l'erreur : #CODE#";
$MESS["VI_CONFIG_SIP_ERROR_3"] = "Texte de l'erreur : #MESSAGE#";
$MESS["VI_CONFIG_SIP_IN"] = "Vous devrez configurer votre IP-PBX pour traiter les appels entrants. S'il vous plaît utiliser ces paramètres : ";
$MESS["VI_CONFIG_SIP_IN_TITLE"] = "Appels entrants";
$MESS["VI_CONFIG_SIP_LAST_UPDATED"] = "Dernière mise à jour : #DATE#";
$MESS["VI_CONFIG_SIP_OFFICE_DEF"] = "Office PBX (#ID#)";
$MESS["VI_CONFIG_SIP_OFFICE_TITLE"] = "Les paramètres de connexion de bureau PBX";
$MESS["VI_CONFIG_SIP_OUT"] = "Pour appeler depuis Bitrix24 via votre fournisseur de téléphonie, s'il vous plaît fournir vos données IP-PBX : ";
$MESS["VI_CONFIG_SIP_OUT_TITLE"] = "Appels sortants";
$MESS["VI_CONFIG_SIP_T_ADDITIONAL_FIELDS"] = "Champs supplémentaires";
$MESS["VI_CONFIG_SIP_T_AUTH_USER"] = "Se connecter en tant qu'utilisateur";
$MESS["VI_CONFIG_SIP_T_INC_LOGIN"] = "Identifiant de connexion";
$MESS["VI_CONFIG_SIP_T_INC_PASS"] = "Mot de passe";
$MESS["VI_CONFIG_SIP_T_INC_SERVER"] = "Serveur";
$MESS["VI_CONFIG_SIP_T_LOGIN"] = "Identifiant de connexion";
$MESS["VI_CONFIG_SIP_T_NUMBER"] = "Numéro de téléphone";
$MESS["VI_CONFIG_SIP_T_OUTBOUND_PROXY"] = "Serveur proxy";
$MESS["VI_CONFIG_SIP_T_PASS"] = "Mot de passe";
$MESS["VI_CONFIG_SIP_T_SERVER"] = "Serveur";
$MESS["VI_CONFIG_TEST_1"] = "USA";
$MESS["VI_CONFIG_TEST_7_499"] = "Russie, Moscou";
$MESS["VI_CONFIG_TEST_14"] = "Canada";
$MESS["VI_CONFIG_TEST_39"] = "Italie";
$MESS["VI_CONFIG_TEST_42"] = "République Tchèque";
$MESS["VI_CONFIG_TEST_44"] = "Grande Bretagne";
$MESS["VI_CONFIG_TEST_48"] = "Pologne";
$MESS["VI_CONFIG_TEST_61"] = "Australie";
$MESS["VI_CONFIG_TEST_81"] = "Japon";
$MESS["VI_CONFIG_TEST_97"] = "Israël";
$MESS["VI_CONFIG_TEST_ABOUT_PAGE"] = "Utilisez cette page pour changer les préférences de routage des appels afin de tester divers cas de figure et scénarios dont votre entreprise pourrait avoir besoin.";
$MESS["VI_CONFIG_TEST_BY_STEP"] = "Pour tester les appels entrants, effectuez un appel depuis un des numéros de test et utilisez votre numéro virtuel #NUMBER# comme extension";
$MESS["VI_CONFIG_TEST_CODE_NOTICE"] = "Faites attention au code de pays pour éviter les facturations indésirables pour les appels entrants.";
$MESS["VI_CONFIG_TEST_TITLE"] = "Le numéro virtuel de votre Bitrix24 est : #NUMBER#";
$MESS["VI_CONFIG_TRANSCRIPTION_HINT"] = "Outre l'enregistrement de l'appel, vous avez également la possibilité d'obtenir une transcription de l'appel au format texte. <br>La transcription de l'appel est un service supplémentaire payant. Les frais seront débités de votre compte de téléphonie après chaque appel. Veuillez consulter la page suivante pour voir la liste des prix de transcription : <a href=\"#URL#\" target=\"_blank\">#URL#</a>";
$MESS["VI_CONFIG_VOTE"] = "Évaluation de la qualité du service";
$MESS["VI_CONFIG_VOTE_TIP"] = "Donnez à vos clients la chance d'évaluer la qualité du service après avoir terminé leur conversation.";
$MESS["VI_CONFIG_WEEK_FR"] = "Vendredi";
$MESS["VI_CONFIG_WEEK_MO"] = "Lundi";
$MESS["VI_CONFIG_WEEK_SA"] = "Samedi";
$MESS["VI_CONFIG_WEEK_SU"] = "Dimanche";
$MESS["VI_CONFIG_WEEK_TH"] = "Jeudi";
$MESS["VI_CONFIG_WEEK_TU"] = "Mardi";
$MESS["VI_CONFIG_WEEK_WE"] = "Mercredi";
$MESS["VI_CONFIG_YOUR_SIP_PBX_NUMBERS"] = "Vos numéros PBX SIP";
$MESS["VOX_CONFIG_CALLERID_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le numéro #NUMBER# ?";
$MESS["VOX_CONFIG_CANCEL_DELETE_NUMBER"] = "Annuler";
$MESS["VOX_CONFIG_CONFIRM_ACTION"] = "Confirmer l'action";
$MESS["VOX_CONFIG_DELETE_NUMBER"] = "Déconnexion de numéro";
$MESS["VOX_CONFIG_NO_PAYBACK"] = "L'argent payé pour des numéros utilisés ne sera pas remboursé.";
$MESS["VOX_CONFIG_NUMBER_DELETE_CONFIRM"] = "Voulez-vous vraiment déconnecter le numéro #NUMBER# de votre Bitrix24 ?";
$MESS["VOX_CONFIG_NUMBER_DISCONNECTION"] = "Déconnexion de numéro";
$MESS["VOX_CONFIG_NUMBER_SET_TO_DELETE"] = "Le numéro sera déconnecté le #DATE#";
$MESS["VOX_CONFIG_TELEPHONY_24"] = "Téléphonie";
