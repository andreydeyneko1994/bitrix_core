<?php
$MESS["CRM_ENTITY_ED_PROG_CLOSE"] = "Completar";
$MESS["CRM_ENTITY_ED_PROG_DEAL_CLOSE"] = "Cerrar negociación";
$MESS["CRM_ENTITY_ED_PROG_HAS_INACCESSIBLE_FIELDS"] = "Se denegó el acceso a esta función porque hay campos que no están disponibles. Comuníquese con el administrador para obtener acceso y resolver el problema.";
$MESS["CRM_ENTITY_ED_PROG_LEAD_CLOSE"] = "Cerrar prospecto";
$MESS["CRM_ENTITY_ED_PROG_ORDER_CLOSE"] = "Completar el pedido";
$MESS["CRM_ENTITY_ED_PROG_ORDER_SHIPMENT_CLOSE"] = "Envío completo";
$MESS["CRM_ENTITY_ED_PROG_QUOTE_CLOSE"] = "Cotización completa";
