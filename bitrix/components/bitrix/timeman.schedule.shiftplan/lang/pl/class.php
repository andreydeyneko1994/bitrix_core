<?
$MESS["TIMEMAN_MODULE_NOT_INSTALLED"] = "Moduł <strong>Zarządzanie czasem pracy</strong> nie jest zainstalowany.";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM"] = "Czy na pewno chcesz usunąć #USER_NAME# z harmonogramu (wszystkie jego/jej zmiany zostaną również usunięte z harmonogramu)?";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_NO"] = "Nie";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_TITLE"] = "Usuń przypisanie pracownika";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_YES"] = "Tak";
$MESS["TM_SCHEDULE_PLAN_TITLE"] = "#SCHEDULE_NAME# — harmonogram";
$MESS["TM_SCHEDULE_PLAN_TODAY"] = "Dzisiaj";
$MESS["TM_SCHEDULE_PLAN_UNBIND_USER_CONFIRM"] = "Czy na pewno chcesz usunąć przypisanie pracownika z harmonogramu?";
$MESS["TM_SCHEDULE_PLAN_USER_ADD"] = "Dodaj pracownika";
$MESS["TM_SCHEDULE_SHIFT_PLAN_ACCESS_DENIED"] = "Niewystarczające uprawnienia.";
$MESS["TM_SCHEDULE_SHIFT_PLAN_SCHEDULE_NOT_FOUND"] = "Nie znaleziono harmonogramu";
$MESS["TM_SHIFT_PLAN_MENU_ADD_SHIFT_TITLE"] = "Dodaj zmianę";
$MESS["TM_SHIFT_PLAN_MENU_DELETE_SHIFT_TITLE"] = "Usuń zmianę";
?>