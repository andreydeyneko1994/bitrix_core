<?
$MESS["TIMEMAN_MODULE_NOT_INSTALLED"] = "O módulo <strong>Gerenciamento de Horas Trabalhadas</strong> não está instalado.";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM"] = "Tem certeza de que deseja excluir #USER_NAME# da agenda (todos os seus respectivos turnos também serão excluídos da agenda)?";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_NO"] = "Não";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_TITLE"] = "Cancelar atribuição do funcionário";
$MESS["TM_SCHEDULE_PLAN_DELETE_USER_CONFIRM_YES"] = "Sim";
$MESS["TM_SCHEDULE_PLAN_TITLE"] = "#SCHEDULE_NAME# - agendar";
$MESS["TM_SCHEDULE_PLAN_TODAY"] = "Hoje";
$MESS["TM_SCHEDULE_PLAN_UNBIND_USER_CONFIRM"] = "Tem certeza de que deseja cancelar a atribuição do funcionário na agenda?";
$MESS["TM_SCHEDULE_PLAN_USER_ADD"] = "Adicionar funcionário";
$MESS["TM_SCHEDULE_SHIFT_PLAN_ACCESS_DENIED"] = "Permissões insuficientes.";
$MESS["TM_SCHEDULE_SHIFT_PLAN_SCHEDULE_NOT_FOUND"] = "A agenda não foi encontrada";
$MESS["TM_SHIFT_PLAN_MENU_ADD_SHIFT_TITLE"] = "Adicionar turno";
$MESS["TM_SHIFT_PLAN_MENU_DELETE_SHIFT_TITLE"] = "Excluir turno";
?>