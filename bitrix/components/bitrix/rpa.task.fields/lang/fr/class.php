<?php
$MESS["RPA_AUTOMATION_TASK_FIELDS_SET"] = "Remplissez les informations";
$MESS["RPA_AUTOMATION_TASK_FIELDS_SHOW"] = "Examiner les informations";
$MESS["RPA_AUTOMATION_TASK_FIELDS_WARNING"] = "Certains champs ont été supprimés du flux de travail et ne peuvent être spécifiés";
