<?
$MESS["VI_ERROR"] = "Erreur lors de la récupération des données. Veuillez réessayer plus tard.";
$MESS["VI_ERROR_LICENSE"] = "Erreur lors de la vérification de la clé de licence. Veuillez vérifier votre saisie et réessayer.";
?>