<?
$MESS["DISK_FILE_HISTORY_VERSION_DELETE_VERSION_BUTTON"] = "Supprimer";
$MESS["DISK_FILE_HISTORY_VERSION_DELETE_VERSION_CONFIRM"] = "Voulez-vous vraiment supprimer cette version ?";
$MESS["DISK_FILE_HISTORY_VERSION_DELETE_VERSION_TITLE"] = "Supprimer la version";
$MESS["DISK_FILE_HISTORY_VERSION_RESTORE_BUTTON"] = "Restaurer";
$MESS["DISK_FILE_HISTORY_VERSION_RESTORE_CONFIRM"] = "Voulez-vous restaurer cette version du document ?";
$MESS["DISK_FILE_HISTORY_VERSION_RESTORE_TITLE"] = "Restauration de la version";
?>