<?
$MESS["CRM_ACTIVITY_BROWSER_ERROR"] = "Niestety używana przeglądarka nie obsługuje nagrywania dźwięku. <br> Zaleca się następujące przeglądarki: Mozilla Firefox i Google Chrome.";
$MESS["CRM_ACTIVITY_VISIT_ACTIVITY"] = "Aktywność";
$MESS["CRM_ACTIVITY_VISIT_CAMERA"] = "Aparat fotograficzny/kamera:";
$MESS["CRM_ACTIVITY_VISIT_CONTACT"] = "Kontakt";
$MESS["CRM_ACTIVITY_VISIT_CONTACT_OR_COMPANY"] = "Kontakt lub firma";
$MESS["CRM_ACTIVITY_VISIT_CREATE"] = "Utwórz:";
$MESS["CRM_ACTIVITY_VISIT_DEAL"] = "Deal";
$MESS["CRM_ACTIVITY_VISIT_FINISH"] = "Zakończ";
$MESS["CRM_ACTIVITY_VISIT_INVOICE"] = "Faktura";
$MESS["CRM_ACTIVITY_VISIT_LEAD"] = "Lead";
$MESS["CRM_ACTIVITY_VISIT_MINUTES"] = "min";
$MESS["CRM_ACTIVITY_VISIT_OWNER"] = "Z:";
$MESS["CRM_ACTIVITY_VISIT_RECORDING"] = "trwa nagrywanie";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_IN_PROGRESS"] = "Wyszukiwanie...";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_VK"] = "Znajdź profil VK";
$MESS["CRM_ACTIVITY_VISIT_SELECT"] = "Wybierz:";
$MESS["CRM_ACTIVITY_VISIT_TAB_SAVE_PHOTO"] = "Zdjęcie profilowe";
$MESS["CRM_ACTIVITY_VISIT_TAB_VISIT"] = "Nagrywanie na żywo";
$MESS["CRM_ACTIVITY_VISIT_VK"] = "VK";
?>