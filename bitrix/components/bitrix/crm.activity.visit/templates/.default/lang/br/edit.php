<?
$MESS["CRM_ACTIVITY_BROWSER_ERROR"] = "Infelizmente, o seu navegador não suporta gravação de áudio. <br> Os seguintes navegadores são recomendados: Mozilla Firefox e Google Chrome.";
$MESS["CRM_ACTIVITY_VISIT_ACTIVITY"] = "Atividade";
$MESS["CRM_ACTIVITY_VISIT_CAMERA"] = "Câmera:";
$MESS["CRM_ACTIVITY_VISIT_CONTACT"] = "Contato";
$MESS["CRM_ACTIVITY_VISIT_CONTACT_OR_COMPANY"] = "Contato ou empresa";
$MESS["CRM_ACTIVITY_VISIT_CREATE"] = "Criar:";
$MESS["CRM_ACTIVITY_VISIT_DEAL"] = "Negócio";
$MESS["CRM_ACTIVITY_VISIT_FINISH"] = "Concluir";
$MESS["CRM_ACTIVITY_VISIT_INVOICE"] = "Fatura";
$MESS["CRM_ACTIVITY_VISIT_LEAD"] = "Lead";
$MESS["CRM_ACTIVITY_VISIT_MINUTES"] = "min";
$MESS["CRM_ACTIVITY_VISIT_OWNER"] = "Com:";
$MESS["CRM_ACTIVITY_VISIT_RECORDING"] = "gravação em andamento";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_IN_PROGRESS"] = "Pesquisando...";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_VK"] = "Encontrar perfil VK";
$MESS["CRM_ACTIVITY_VISIT_SELECT"] = "Selecionar:";
$MESS["CRM_ACTIVITY_VISIT_TAB_SAVE_PHOTO"] = "Foto de perfil";
$MESS["CRM_ACTIVITY_VISIT_TAB_VISIT"] = "Gravação ao vivo";
$MESS["CRM_ACTIVITY_VISIT_VK"] = "VK";
?>