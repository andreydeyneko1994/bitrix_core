<?
$MESS["CRM_TRACKING_SITE_B24_AUTO_CONNECTED"] = "¡El canal %name% está conectado!";
$MESS["CRM_TRACKING_SITE_B24_AUTO_DESC"] = "Este canal es automático. No requiere ninguna configuración específica.";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_SITES"] = "No hay sitios web";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_STORES"] = "No hay tiendas";
$MESS["CRM_TRACKING_SITE_B24_REPLACEMENT"] = "Intercambio de número de teléfono y de correo electrónico para los sitios web de Bitrix24";
$MESS["CRM_TRACKING_SITE_B24_VIEW"] = "Ver el sitio web";
?>