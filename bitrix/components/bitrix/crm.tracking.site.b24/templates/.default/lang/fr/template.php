<?
$MESS["CRM_TRACKING_SITE_B24_AUTO_CONNECTED"] = "Le canal %name% est connecté !";
$MESS["CRM_TRACKING_SITE_B24_AUTO_DESC"] = "Ce canal est automatique. Il n'a pas besoin de configuration spécifique.";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_SITES"] = "Il n'y a aucun site";
$MESS["CRM_TRACKING_SITE_B24_EMPTY_STORES"] = "Il n'y a aucune boutique";
$MESS["CRM_TRACKING_SITE_B24_REPLACEMENT"] = "Échange de numéro de téléphone et d'e-mail pour sites Bitrix24";
$MESS["CRM_TRACKING_SITE_B24_VIEW"] = "Afficher le site";
?>