<?php
$MESS["INTERVAL_DAY_PLURAL"] = "dias";
$MESS["INTERVAL_DAY_SINGLE"] = "dia";
$MESS["INTERVAL_HOUR_PLURAL"] = "horas";
$MESS["INTERVAL_HOUR_SINGLE"] = "hora";
$MESS["INTERVAL_MINUTE_PLURAL"] = "min.";
$MESS["INTERVAL_MINUTE_SINGLE"] = "min.";
$MESS["INTERVAL_SECOND_PLURAL"] = "seg.";
$MESS["INTERVAL_SECOND_SINGLE"] = "seg.";
$MESS["MPL_CALL_IS_PROCESSED"] = "Chamada processada";
$MESS["MPL_MOBILE_CALL"] = "Ligar";
$MESS["MPL_MOBILE_INCOMING_CALL"] = "Chamada recebida";
$MESS["MPL_MOBILE_OUTBOUND_CALL"] = "Chamada realizada";
$MESS["MPL_MOBILE_PUBLISHING"] = "Publicação&hellip;";
$MESS["MPT_ADD_FIRST_COMMENT"] = "Adicionar primeiro comentário";
$MESS["MPT_PREVIOUS_COMMENTS"] = "Comentários anteriores";
