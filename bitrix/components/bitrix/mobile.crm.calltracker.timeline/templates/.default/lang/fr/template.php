<?php
$MESS["INTERVAL_DAY_PLURAL"] = "jours";
$MESS["INTERVAL_DAY_SINGLE"] = "jour";
$MESS["INTERVAL_HOUR_PLURAL"] = "heures";
$MESS["INTERVAL_HOUR_SINGLE"] = "heure";
$MESS["INTERVAL_MINUTE_PLURAL"] = "min.";
$MESS["INTERVAL_MINUTE_SINGLE"] = "min.";
$MESS["INTERVAL_SECOND_PLURAL"] = "sec.";
$MESS["INTERVAL_SECOND_SINGLE"] = "sec.";
$MESS["MPL_CALL_IS_PROCESSED"] = "Appel traité";
$MESS["MPL_MOBILE_CALL"] = "Appel";
$MESS["MPL_MOBILE_INCOMING_CALL"] = "Appel entrant";
$MESS["MPL_MOBILE_OUTBOUND_CALL"] = "Appel sortant";
$MESS["MPL_MOBILE_PUBLISHING"] = "Publication&hellip;";
$MESS["MPT_ADD_FIRST_COMMENT"] = "Ajoutez le premier commentaire";
$MESS["MPT_PREVIOUS_COMMENTS"] = "Commentaires précédents";
