<?
$MESS["RPA_STAGE_CREATE_TITLE"] = "Criar nova etapa";
$MESS["RPA_STAGE_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir esta etapa?";
$MESS["RPA_STAGE_DETAIL_TITLE"] = "Editar etapa #TITLE#";
$MESS["RPA_STAGE_EDITABLE_FIELDS_TITLE"] = "Campos editáveis";
$MESS["RPA_STAGE_MANDATORY_FIELDS_TITLE"] = "Campos obrigatórios";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "A etapa do fluxo de trabalho não foi encontrada.";
$MESS["RPA_STAGE_TYPE_ACCESS_DENIED"] = "Você não pode editar etapas neste fluxo de trabalho.";
$MESS["RPA_STAGE_VISIBLE_FIELDS_TITLE"] = "Campos visíveis";
?>