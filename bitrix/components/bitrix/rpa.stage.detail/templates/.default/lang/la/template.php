<?
$MESS["RPA_COMPONENT_STAGE_ACTIONS"] = "Acciones";
$MESS["RPA_COMPONENT_STAGE_EDITABLE_FIELDS"] = "Campos editables para esta etapa";
$MESS["RPA_COMPONENT_STAGE_FIELD_CHOOSE"] = "Seleccionar campos";
$MESS["RPA_COMPONENT_STAGE_FIELD_CODE"] = "Código";
$MESS["RPA_COMPONENT_STAGE_FIELD_NAME"] = "Nombre";
$MESS["RPA_COMPONENT_STAGE_FIELD_STAGES"] = "Posible etapa para pasar";
$MESS["RPA_COMPONENT_STAGE_MANDATORY_FIELDS"] = "Campos obligatorios para esta etapa";
$MESS["RPA_COMPONENT_STAGE_NEXT_POSSIBLE_STAGES"] = "Etapas siguientes";
$MESS["RPA_COMPONENT_STAGE_PERMISSION"] = "Permiso para";
$MESS["RPA_COMPONENT_STAGE_VISIBLE_FIELDS"] = "Campos visibles para esta etapa";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_CREATE"] = "Los usuarios pueden agregar elementos";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MODIFY"] = "Los usuarios pueden editar los elementos de la etapa";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_MOVE"] = "Los usuarios pueden cambiar los elementos de la etapa";
$MESS["RPA_COMPONENT_STAGE_WHO_CAN_VIEW"] = "Los usuarios pueden ver los elementos de la etapa";
?>