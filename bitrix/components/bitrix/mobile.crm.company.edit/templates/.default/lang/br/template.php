<?
$MESS["M_CRM_COMPANY_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_COMPANY_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_COMPANY_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_COMPANY_EDIT_CONVERT_TITLE"] = "Empresa";
$MESS["M_CRM_COMPANY_EDIT_CREATE_TITLE"] = "Adicionar Empresa";
$MESS["M_CRM_COMPANY_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_COMPANY_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_COMPANY_EDIT_VIEW_TITLE"] = "Ver empresa";
$MESS["M_CRM_COMPANY_MENU_ACTIVITY"] = "Atividades";
$MESS["M_CRM_COMPANY_MENU_DEALS"] = "Negócios";
$MESS["M_CRM_COMPANY_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_COMPANY_MENU_EDIT"] = "Editar";
$MESS["M_CRM_COMPANY_MENU_HISTORY"] = "Histórico";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>