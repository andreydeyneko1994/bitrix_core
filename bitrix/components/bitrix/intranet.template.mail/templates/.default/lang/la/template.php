<?php
$MESS["INTRANET_BITRIX24"] = "Bitrix24";
$MESS["INTRANET_CHANGE_NOTIFY_SETTINGS"] = "Editar la configuración de las notificaciones";
$MESS["INTRANET_INVITE_ACCEPT"] = "Aceptar la invitación";
$MESS["INTRANET_INVITE_FOOTER"] = "Esta es una notificación generada automáticamente. Recibió este correo electrónico porque lo invitaron a unirse a la cuenta de Bitrix24. Podrá editar la configuración de las notificaciones tan pronto como acepte la invitación.";
$MESS["INTRANET_INVITE_IMG_LINK"] = "http://www.bitrix24.com/images/mail/tutorials/tools_es.png";
$MESS["INTRANET_INVITE_INFO_TEXT"] = "#SPAN_START# Bitrix24 es una plataforma de colaboración líder,#SPAN_END# utilizada por más de 10,000,000 empresas en todo el mundo.";
$MESS["INTRANET_INVITE_INFO_TEXT3"] = "¿Qué es Bitrix24?";
$MESS["INTRANET_INVITE_MORE"] = "Conocer más";
$MESS["INTRANET_INVITE_TEXT"] = "#NAME# lo invita a #BLOCK_START#Bitrix#BLOCK_MIDDLE#24#BLOCK_END#";
$MESS["INTRANET_MAIL_EVENTS_UNSUBSCRIBE"] = "Cancelar la suscripción";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE"] = "Nuevo mensaje de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE_GROUP"] = "Nuevos mensajes de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_NOTIFY"] = "Nueva notificación de #NAME#";
$MESS["INTRANET_OPEN"] = "Ver";
$MESS["INTRANET_OPEN_NOTIFY"] = "Ver las notificaciones";
$MESS["INTRANET_SITE_LINK"] = "https://www.bitrix24.es/";
