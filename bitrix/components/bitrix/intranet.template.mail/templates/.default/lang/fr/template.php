<?php
$MESS["INTRANET_BITRIX24"] = "Bitrix24";
$MESS["INTRANET_CHANGE_NOTIFY_SETTINGS"] = "Éditer les paramètres des notifications";
$MESS["INTRANET_INVITE_ACCEPT"] = "Accepter l'invitation";
$MESS["INTRANET_INVITE_FOOTER"] = "Cette notification est générée automatiquement. Vous avez reçu cet e-mail parce que vous avez été invité à rejoindre un compte Bitrix24. Vous pouvez éditer les paramètres des notifications après avoir accepté l'invitation.";
$MESS["INTRANET_INVITE_IMG_LINK"] = "http://www.bitrix24.com/images/mail/tutorials/tools_fr.png";
$MESS["INTRANET_INVITE_INFO_TEXT"] = "#SPAN_START#Bitrix24 est une plateforme collaborative multifonction#SPAN_END# utilisée par plus de 10 000 000 d’entreprises du monde entier";
$MESS["INTRANET_INVITE_INFO_TEXT3"] = "Qu'est-ce que Bitrix24 ?";
$MESS["INTRANET_INVITE_MORE"] = "En savoir plus";
$MESS["INTRANET_INVITE_TEXT"] = "#NAME# vous invite à rejoindre #BLOCK_START#Bitrix#BLOCK_MIDDLE#24#BLOCK_END#";
$MESS["INTRANET_MAIL_EVENTS_UNSUBSCRIBE"] = "Se désabonner";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE"] = "Nouveau message de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE_GROUP"] = "Nouveaux messages de #NAME#";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_NOTIFY"] = "Nouvelle notification de #NAME#";
$MESS["INTRANET_OPEN"] = "Afficher";
$MESS["INTRANET_OPEN_NOTIFY"] = "Afficher les notifications";
$MESS["INTRANET_SITE_LINK"] = "https://www.bitrix24.fr/";
