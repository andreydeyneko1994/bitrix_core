<?
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Zadania i projekty";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "Czat";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Dokumenty";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Dokumenty grupy";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Kalendarz";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Teraz utwórz konto Bitrix24";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Ujednolicony zestaw do współpracy";
$MESS["DISK_EXT_LINK_DESCRIPTION"] = "Plik mógł nie być publikowany lub kliknąłeś na niewłaściwy link. <br/>Proszę skontaktować się z właścicielem pliku.";
$MESS["DISK_EXT_LINK_INVALID"] = "Link publiczny jest nieprawidłowy.";
$MESS["DISK_EXT_LINK_TEXT"] = "Nie znaleziono pliku";
$MESS["DISK_EXT_LINK_TITLE"] = "Nie znaleziono pliku";
?>