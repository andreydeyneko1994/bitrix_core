<?
$MESS["DISK_EXTERNAL_LINK_ERROR_DISABLED_MODE"] = "Os links públicos estão desativados neste portal.";
$MESS["DISK_EXTERNAL_OBJECT_ACT_COPY"] = "Copiar";
$MESS["DISK_EXTERNAL_OBJECT_ACT_DOWNLOAD"] = "Baixar";
$MESS["DISK_EXTERNAL_OBJECT_ACT_MARK_DELETED"] = "Excluir";
$MESS["DISK_EXTERNAL_OBJECT_ACT_OPEN"] = "Abrir";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_CREATE_TIME"] = "Criada em";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_FORMATTED_SIZE"] = "Tamanho";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_NAME"] = "Nome";
$MESS["DISK_EXTERNAL_OBJECT_COLUMN_UPDATE_TIME"] = "Modificada em";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_FORMATTED_SIZE"] = "Por tamanho";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_NAME"] = "Por nome";
$MESS["DISK_EXTERNAL_OBJECT_SORT_BY_UPDATE_TIME"] = "Por data";
?>