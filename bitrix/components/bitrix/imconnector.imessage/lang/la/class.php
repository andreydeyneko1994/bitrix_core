<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRMATION_RECONNECTION"] = "Un chat empresarial ya está conectado a este canal. ¿Desea
conectarlo nuevamente?";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_BUTTON_OK"] = "OK";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_DESCRIPTION_NEW"] = "Para conectar el Chat Apple Business con Bitrix24, es necesario que utilice o cree una cuenta de Apple y especifique a Bitrix24 como el Proveedor del servicio.<br /><br />Obtenga más información en la <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link imconnector-field-box-link-form-imessage\">sección de Ayuda</a>.<br /><br />Asegúrese de seguir todas las indicaciones de Apple antes de crear la conexión. <a onclick=\"top.BX.Helper.show('#ID_LIMIT#');\" class=\"imconnector-field-box-link imconnector-field-box-link-form-imessage\">Obtener más información</a><br /><br />¿Desea ir al sitio web de Apple para crear la conexión (se abrirá en esta ventana)?";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_DESCRIPTION_NEW_2"] = "Para conectar Apple Messages for Business con Bitrix24, es necesario que utilice (o cree) una cuenta de Apple y especifique a Bitrix24 como el Proveedor del servicio.<br /><br />Obtenga más información en la <a onclick=\"top.BX.Helper.show(\'#ID#\');\" class=\"imconnector-field-box-link imconnector-field-box-link-form-imessage\">sección de Ayuda</a>.<br /><br />Asegúrese de seguir todas las indicaciones de Apple antes de iniciar la conexión.<a onclick=\"top.BX.Helper.show(\'#ID_LIMIT#\');\" class=\"imconnector-field-box-link imconnector-field-box-link-form-imessage\">Leer la documentación</a>.<br /><br />¿Desea ir al sitio web de Apple para crear la conexión? La nueva página se abrirá en esta ventana.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_TITLE"] = "Confirme para continuar con el registro de Apple Business";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_CONNECTION_INFORMATION"] = "Información de la conexión";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_MANUALLY"] = "puede configurarse manualmente";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_OR"] = " o ";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_MODULE_NOT_INSTALLED"] = "El módulo \"Conectores IM externos\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_CONNECT"] = "No pudo establecerse la prueba de conexión utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_REGISTER"] = "Se produjo un error en el registro";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_SAVE"] = "Se produjo un error al guardar los datos. Revise la entrada e intente nuevamente";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_CONNECT"] = "La prueba de conexión se estableció correctamente";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_REGISTER"] = "El registro se realizó correctamente";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_SAVE"] = "La información se guardó correctamente.";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario nuevamente.";
