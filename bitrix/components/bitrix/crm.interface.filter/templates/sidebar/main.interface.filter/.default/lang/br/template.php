<?php
$MESS["interface_filter_days"] = "d.";
$MESS["interface_filter_note"] = "Resultados exibidos de acordo com os critérios do filtro.";
$MESS["interface_filter_note_clear"] = "Limpar Filtro";
$MESS["interface_grid_additional"] = "Mais filtros";
$MESS["interface_grid_find"] = "Pesquisar";
$MESS["interface_grid_find_title"] = "Encontrar registros que correspondam aos critérios de pesquisa";
$MESS["interface_grid_flt_cancel"] = "Cancelar";
$MESS["interface_grid_flt_cancel_title"] = "Mostrar todos os registros";
$MESS["interface_grid_hide_all"] = "Ocultar todos os filtros";
$MESS["interface_grid_no_no_no"] = "(não)";
$MESS["interface_grid_search"] = "Pesquisar";
$MESS["interface_grid_show_all"] = "Mostrar todos os filtros";
$MESS["main_interface_filter_save"] = "Salvar Como...";
$MESS["main_interface_filter_save_title"] = "Salvar Filtro Atual";
$MESS["main_interface_filter_saved"] = "Filtros Salvos";
$MESS["main_interface_filter_saved_apply"] = "Aplicar Filtro Salvo";
