<?php
$MESS["interface_filter_days"] = "j.";
$MESS["interface_filter_note"] = "Résultats affichés selon les critères de recherche.";
$MESS["interface_filter_note_clear"] = "Annuler le filtre";
$MESS["interface_grid_additional"] = "Plus de filtres";
$MESS["interface_grid_find"] = "Recherche";
$MESS["interface_grid_find_title"] = "Trouver les enregistrements d'après les critères de recherche";
$MESS["interface_grid_flt_cancel"] = "Annuler";
$MESS["interface_grid_flt_cancel_title"] = "Afficher tous les enregistrements";
$MESS["interface_grid_hide_all"] = "Masquer tous les filtres";
$MESS["interface_grid_no_no_no"] = "(non)";
$MESS["interface_grid_search"] = "Recherche";
$MESS["interface_grid_show_all"] = "Afficher tous les filtres";
$MESS["main_interface_filter_save"] = "Enregistrer sous...";
$MESS["main_interface_filter_save_title"] = "Enregistrer le filtre actuel";
$MESS["main_interface_filter_saved"] = "Filtres enregistrés";
$MESS["main_interface_filter_saved_apply"] = "Appliquer le filtre enregistré";
