<?
$MESS["CRM_ACTIVITY_EDIT_NOT_FOUND"] = "Nie można znaleźć działania ##ID#.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_DIFINED"] = "Typ właściciela jest niezdefiniowany.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_SUPPORTED"] = "Typ właściciela (#OWNER_TYPE#) nie jest obsługiwany w bieżącym kontekście.";
$MESS["CRM_ACTIVITY_EDIT_RESPONSIBLE_NOT_ASSIGNED"] = "[nieprzypisane]";
$MESS["CRM_ACTIVITY_EDIT_TYPE_IS_NOT_SUPPORTED"] = "Typ działania (#TYPE#) nie jest wspierany w bieżącym kontekście";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>