<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Ativo";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Classificação";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_VAT_FIELD_NAME"] = "Nome";
$MESS["CRM_VAT_FIELD_RATE"] = "Taxa";
$MESS["CRM_VAT_NO"] = "Não";
$MESS["CRM_VAT_NOT_FOUND"] = "A taxa de IVA não foi encontrada.";
$MESS["CRM_VAT_SECTION_MAIN"] = "Taxa de IVA";
$MESS["CRM_VAT_YES"] = "Sim";
?>