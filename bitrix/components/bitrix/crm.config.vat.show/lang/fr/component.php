<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Activité";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Trier";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_VAT_FIELD_NAME"] = "Dénomination";
$MESS["CRM_VAT_FIELD_RATE"] = "Coefficient";
$MESS["CRM_VAT_NO"] = "Non";
$MESS["CRM_VAT_NOT_FOUND"] = "La TVA n'est pas trouvée !";
$MESS["CRM_VAT_SECTION_MAIN"] = "Taux de TVA";
$MESS["CRM_VAT_YES"] = "Oui";
