<?php
$MESS["FACEID_TRACKER_CMP_AGR_BUTTON"] = "Akceptuję Regulamin";
$MESS["FACEID_TRACKER_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
 <ol class=\"tracker-agreement-popup-list\"> 
<li class=\"tracker-agreement-popup-list-item\"> 
  Usługę FindFace Service (Usługa) zapewnia firma N-TECH.LAB LTD zgodnie z następującą <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">Umową</a>. Naciśnięcie przycisku \"Akceptuję Regulamin\" jest jednoznaczne z wyrażeniem zgody na następujące wymaganie i zapewnieniem o ich wypełnieniu:
 <ul class=\"tracker-agreement-popup-inner-list\"> 
<li class=\"tracker-agreement-popup-inner-list-item\">
    Ściśle przestrzegać wszystkich przepisów prawa miejscowego dotyczących ochrony prywatności i wykorzystaniem danych osobowych w kraju Użytkownika przez cały czas korzystania z Usługi.
 </li>
 <li class=\"tracker-agreement-popup-inner-list-item\">
       Uzyskać zgodę (na piśmie lub w inny sposób zgodnie z przepisami prawa miejscowego każdego kraju, w którym prowadzona jest działalność) od każdej osoby, czyje dane osobowe będą przetwarzane za pomocą Usługi, w tym ich zdjęcia i wizerunki.
 </li>
 <li class=\"tracker-agreement-popup-inner-list-item\">
      Skorzystać z porady prawnej PRZED rozpoczęciem korzystania z Usługi w razie braku pewności w kwestii koniecznych pozwoleń i ich formy.
 </li>
 <li class=\"tracker-agreement-popup-inner-list-item\">
 Zagwarantować, że wszelkie informacje przesyłane do Usługi zostały pozyskane w pełni zgodnie z przepisami prawa miejscowego.
 </li>
 <li class=\"tracker-agreement-popup-inner-list-item\">Potwierdzić, że Użytkownik korzysta z Usługi na własne ryzyko i bez żadnego rodzaju gwarancji.
 </li>
 </ul>
 </li>
 <li class=\"tracker-agreement-popup-list-item\">
 Firma Bitrix24 zrzeka się odpowiedzialności za Usługę i nie składa żadnych zapewnień ani nie udziela gwarancji dotyczących bezbłędności czy dostępności, jak również nie udziela wsparcia technicznego ani konsultacji w sprawach związanych z Usługą.
    </li>    </ol>    <div class=\"tracker-agreement-popup-description\"> 
    Akceptacja umowy o świadczeniu Usługi, jest jednoznaczna z wyrażeniem zgody i potwierdzeniem, że firma Bitrix24 nie gromadzą ani nie przetwarzają danych przesyłanych przez Użytkownika oraz, że firma Bitrix24 nie zna okoliczności, w których przedmiotowe dane zostały zgromadzone.    </div>   </div>";
$MESS["FACEID_TRACKER_CMP_AGR_TITLE"] = "Regulamin użytkowania";
$MESS["FACEID_TRACKER_CMP_AUTH_ONLY"] = "Ten komponent wymaga uwierzytelniania.";
$MESS["FACEID_TRACKER_CMP_AUTO"] = "Auto";
$MESS["FACEID_TRACKER_CMP_AUTO_LEAD"] = "utwórz lead";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO"] = "zrób zdjęcie";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO_NEW"] = "zrób zdjęcie automatycznie";
$MESS["FACEID_TRACKER_CMP_CAMERA"] = "Kamera";
$MESS["FACEID_TRACKER_CMP_DESCR"] = "Funkcja śledzenia twarzy będzie rejestrować i zliczać osoby odwiedzające sklep lub biuro: suma osób odwiedzających dziś; liczba nowych odwiedzających; liczba powracających odwiedzających; liczba osób odwiedzających, które są już w CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1"] = "Błyskawiczne wyszukiwanie na podstawie zdjęcia wśród osób zarejestrowanych w Twoim CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1_NEW"] = "Funkcja śledzenia twarzy wykorzystuje technologię rozpoznawania twarzy do identyfikacji swoich klientów. Może zliczać wszystkie osoby odwiedzające dany sklep lub biuro, policzyć osoby nowe i powracające, a także ile z nich figuruje już w CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2"] = "Na podstawie zdjęcia Bitrix24 znajdzie profil w mediach społecznościowych i doda go do CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2_NEW"] = "Śledzenia twarzy może ustalić profil na portalu społecznościowym VK na podstawie zrobionego zdjęcia i dodać profil do CRM.";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT"] = "Zrób zdjęcie swojego klienta, aby dodać go do systemu. Bitrix24 rozpozna klienta przy kolejnym spotkaniu, dzięki czemu będziesz wiedzieć, że to klient powracający. <br><br>Zainstaluj w sklepie lub biurze kamerę w celu rejestracji odwiedzających i prowadzenia statystyk.";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\"> <div class=\"faceid-tracker-first-visit-desc-inner\"> <div class=\"faceid-tracker-first-visit-desc-title\">
Jak zacząć?
</div> <div class=\"faceid-tracker-first-visit-desc-block\"> <span class=\"faceid-tracker-first-visit-desc-item\">
Zrób pierwsze zdjęcie klienta lub klientki, a funkcja śledzenia twarzy \"zapamięta\" tę osobę. Gdy następnym razem odwiedzi sklep, system rozpozna ją jako powracającego klienta bądź powracającą klientkę.
 </span> </div> </div> <div class=\"faceid-tracker-first-visit-desc-inner\"> <div class=\"faceid-tracker-first-visit-desc-title\">
Jak korzystać z funkcji śledzenia twarzy jako licznika ruchu w sklepie</div> <div class=\"faceid-tracker-first-visit-desc-block\"> <span class=\"faceid-tracker-first-visit-desc-item\">
Zamontuj przy wejściu do sklepu lub biura kamerę
</span> <span class=\"faceid-tracker-first-visit-desc-item\">
Podłącz kamerę do dowolnego komputera</span> <span class=\"faceid-tracker-first-visit-desc-item\">
Uruchom na tym komputerze śledzenie twarzy</span> <span class=\"faceid-tracker-first-visit-desc-item\">
Włącz automatyczne fotografowanie</span> </div> </div> </div>
";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS"] = "Kredyt rozpoznawania:";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS_ADD"] = "Dodaj kredyt";
$MESS["FACEID_TRACKER_CMP_HEAD_VISITORS"] = "Odwiedzający";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_DEFAULT"] = "Kamera";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_ERROR"] = "Twoja przeglądarka nie obsługuje nagrywania wideo lub kamera nie jest podłączona.";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NOT_FOUND"] = "Nie znaleziono kamery";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NO_SUPPORT"] = "Twoja przeglądarka nie obsługuje kamery";
$MESS["FACEID_TRACKER_CMP_JS_FACE_NOT_FOUND"] = "Niestety nie byliśmy w stanie rozpoznać osoby. Proszę spróbować zrobić inne zdjęcie.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_ORIGINAL"] = "Przykładowe zdjęcie";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM"] = "Zapisz do CRM";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM_DONE"] = "Lead został zapisany";
$MESS["FACEID_TRACKER_CMP_JS_VK_FOUND_PEOPLE"] = "ludzie";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK_ACTION"] = "Znajdź profil VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_SELECT"] = "Wybierz";
$MESS["FACEID_TRACKER_CMP_LIST_MORE"] = "Pokaż więcej";
$MESS["FACEID_TRACKER_CMP_SETTINGS"] = "Rozszerzone parametry";
$MESS["FACEID_TRACKER_CMP_STATS"] = "Statystyki osoby odwiedzającej";
$MESS["FACEID_TRACKER_CMP_STATS_ALL"] = "Suma";
$MESS["FACEID_TRACKER_CMP_STATS_CRM"] = "Statystyka CRM";
$MESS["FACEID_TRACKER_CMP_STATS_CRM_SAVE"] = "Zapisano w CRM";
$MESS["FACEID_TRACKER_CMP_STATS_DETAILED"] = "Szczegóły statystyk";
$MESS["FACEID_TRACKER_CMP_STATS_NEW"] = "Nowa";
$MESS["FACEID_TRACKER_CMP_STATS_OLD"] = "Powracająca";
$MESS["FACEID_TRACKER_CMP_TITLE"] = "Przeglądaj osoby odwiedzające Twoje placówki stacjonarne.";
$MESS["FACEID_TRACKER_CMP_VK_FOUND_COUNT"] = "Znalezione osoby:";
$MESS["FACEID_TRACKER_CMP_VK_PROGRESS"] = "Wyszukiwanie";
$MESS["FACEID_TRACKER_CMP_VK_TITLE"] = "Szukaj profilu VK";
