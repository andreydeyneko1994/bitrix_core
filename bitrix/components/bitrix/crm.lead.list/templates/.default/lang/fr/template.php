<?php
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_LEAD_ACTION_DELETE"] = "Supprimer";
$MESS["CRM_LEAD_ADD_ACTIVITY"] = "Planifier";
$MESS["CRM_LEAD_ADD_ACTIVITY_TITLE"] = "Planifier une activité";
$MESS["CRM_LEAD_ADD_CALL"] = "Ajouter un appel";
$MESS["CRM_LEAD_ADD_CALL_SHORT"] = "Appel";
$MESS["CRM_LEAD_ADD_CALL_TITLE"] = "Ajouter un nouvel appel";
$MESS["CRM_LEAD_ADD_EMAIL"] = "Écrire une lettre";
$MESS["CRM_LEAD_ADD_EMAIL_TITLE"] = "Écrire une lettre";
$MESS["CRM_LEAD_ADD_MEETING"] = "Ajouter une rencontre";
$MESS["CRM_LEAD_ADD_MEETING_SHORT"] = "Réunion";
$MESS["CRM_LEAD_ADD_MEETING_TITLE"] = "Ajouter une nouvelle réunion";
$MESS["CRM_LEAD_ADD_QUOTE"] = "Nouveau devis";
$MESS["CRM_LEAD_ADD_QUOTE_TITLE"] = "Nouveau devis";
$MESS["CRM_LEAD_ASSIGN_TO"] = "Désigner le responsable";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_INDEX_REBUILD_STATE"] = "#processed# %";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_INDEX_REBUILD_TITLE"] = "Scanner les doublons de prospects";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_MERGE_STATE"] = "#processed# %";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_MERGE_TITLE"] = "Fusionner les prospects en double";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_VOL_DATA_PREPARE_STATE"] = "#processed# %";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_VOL_DATA_PREPARE_TITLE"] = "Préparation des données pour la recherche de prospects en double";
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Les prospects ont été convertis.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Échec de la conversion : #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Conversions réussies : #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_DLG_CLOSE_CONFIRMATION"] = "Conversion de prospect toujours en cours. Voulez-vous vraiment quitter cette page ?";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# sur #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Convertir un prospect";
$MESS["CRM_LEAD_BATCH_DELETION_COMPLETED"] = "Les prospects ont été supprimés.";
$MESS["CRM_LEAD_BATCH_DELETION_COUNT_FAILED"] = "Échec de la suppression : #number#.";
$MESS["CRM_LEAD_BATCH_DELETION_COUNT_SUCCEEDED"] = "Suppression réussie : #number#.";
$MESS["CRM_LEAD_BIZPROC"] = "les processus d'entreprise";
$MESS["CRM_LEAD_BIZPROC_LIST"] = "Nouveau processus d'entreprise";
$MESS["CRM_LEAD_BIZPROC_LIST_TITLE"] = "Lancer un nouveau processus d'entreprise";
$MESS["CRM_LEAD_BIZPROC_TITLE"] = "les processus d'entreprise";
$MESS["CRM_LEAD_BUILD_TIMELINE_DLG_TITLE"] = "Préparation de l'historique des prospects";
$MESS["CRM_LEAD_BUILD_TIMELINE_STATE"] = "#processed# de #total#";
$MESS["CRM_LEAD_CALENDAR"] = "Créer un appel/une rencontre";
$MESS["CRM_LEAD_CALENDAR_TITLE"] = "Ajouter un nouvel enregistrement dans le calendrier";
$MESS["CRM_LEAD_CONVERT"] = "Commencer la conversion";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Conversion du Prospect";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Vous avez besoin de contacts, entreprises et transactions pour créer des permissions de conversion de prospect.";
$MESS["CRM_LEAD_CONV_DIALOG_CANCEL_BTN"] = "Annuler";
$MESS["CRM_LEAD_CONV_DIALOG_CONTINUE_BTN"] = "Continuer";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Choisissez les entités pour y créer des champs supplémentaires";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quels champs seront créés";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "Les entités sélectionnées ne disposent pas de champ où placer les données des prospect. Veuillez choisir les entités pour y créer des champs supplémentaires afin d'enregistrer toutes les informations disponibles.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Convertir une Prospect";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Sélectionner";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Entreprises";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Contacts";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Dernier";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Rechercher";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Rien n'a été trouvé.";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Sélectionnez un contact et une entreprise";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Erreur de conversion générique.";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Sélectionner dans la liste...";
$MESS["CRM_LEAD_COPY"] = "Copier";
$MESS["CRM_LEAD_COPY_TITLE"] = "Copier le Prospect";
$MESS["CRM_LEAD_CREATE_CALL_LIST"] = "Créer une liste d'appel";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "Générer";
$MESS["CRM_LEAD_CREATE_ON_BASIS_TITLE"] = "Créer un contact, une entreprise et une transaction grâce à une prospect";
$MESS["CRM_LEAD_DELETE"] = "Supprimer";
$MESS["CRM_LEAD_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer ?";
$MESS["CRM_LEAD_DELETE_TITLE"] = "Suppression du Prospect";
$MESS["CRM_LEAD_EDIT"] = "Éditer";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Edition du Prospect";
$MESS["CRM_LEAD_EVENT"] = "Ajouter un évènement";
$MESS["CRM_LEAD_EVENT_SHORT"] = "Evènement";
$MESS["CRM_LEAD_EVENT_TITLE"] = "Créer un nouvel évènement";
$MESS["CRM_LEAD_EXCLUDE"] = "Ajouter aux exceptions";
$MESS["CRM_LEAD_EXCLUDE_CONFIRM"] = "Voulez-vous vraiment ajouter la transaction aux exceptions ?";
$MESS["CRM_LEAD_EXCLUDE_CONFIRM_HELP"] = "À propos de la liste des exceptions...";
$MESS["CRM_LEAD_EXCLUDE_TITLE"] = "Ajouter le prospect aux exceptions";
$MESS["CRM_LEAD_LIST_APPLY_BUTTON"] = "Appliquer";
$MESS["CRM_LEAD_LIST_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_LEAD_LIST_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_LEAD_LIST_CHOOSE_ACTION"] = "Sélectionner une action";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Préférences de la transaction";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_SUMMARY"] = "Cette action supprime les prospects sélectionnés. Cette opération peut prendre un certain temps";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_TITLE"] = "Supprimer les prospects";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_CALENDAR"] = "Calendrier";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_LIST"] = "Liste";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET"] = "Rapports";
$MESS["CRM_LEAD_LRP_DLG_BTN_CLOSE"] = "Fermer";
$MESS["CRM_LEAD_LRP_DLG_BTN_START"] = "Exécuter";
$MESS["CRM_LEAD_LRP_DLG_BTN_STOP"] = "Arrêter";
$MESS["CRM_LEAD_LRP_DLG_REQUEST_ERR"] = "Erreur lors du traitement de la requête.";
$MESS["CRM_LEAD_LRP_DLG_WAIT"] = "Attendez...";
$MESS["CRM_LEAD_MARK_AS_OPENED"] = "Faire disponible à tous";
$MESS["CRM_LEAD_MARK_AS_OPENED_NO"] = "Non";
$MESS["CRM_LEAD_MARK_AS_OPENED_YES"] = "Oui";
$MESS["CRM_LEAD_REBUILD_ACCESS_ATTRS"] = "Les autorisations d'accès mises à jour nécessitent que vous mettiez à jour les attributs d'accès actuels en utilisant la <a id='#ID#' target='_blank' href='#URL#'>page de gestion des autorisations</a>.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX"] = "Pour un bon fonctionnement du contrôle de doubles il est nécessaire <a id='#ID#' href='#URL#'> d'indexer</a> la liste de prospects.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_SUMMARY"] = "L'index des doubles des prospects sera modifié. Cette procédure peut prendre un certain temps.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_TITLE"] = "Réorganisation de l'index des doubles pour les prospects";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT"] = "Pour utiliser la recherche rapide, vous devez <a id=\"#ID#\" href=\"#URL#\">recréer l'index de recherche de clients potentiels</a>.";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_SUMMARY"] = "Ceci va recréer l'index de recherche de clients potentiels. Cette opération peut prendre un certain temps.";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_TITLE"] = "Recréer l'index de recherche de clients potentiels";
$MESS["CRM_LEAD_REBUILD_SECURITY_ATTRS_DLG_TITLE"] = "Mise à jour des droits d'accès";
$MESS["CRM_LEAD_REBUILD_SEMANTICS"] = "Les rapports vous demandent de <a id=\"#ID#\" href=\"#URL#\">mettre à jour les champs de clients potentiels du service</a> (ceci n'affectera aucune donnée utilisateur).";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_SUMMARY"] = "Les champs de clients potentiels du service vont maintenant être mis à jour. Ceci peut prendre un certain temps.";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_TITLE"] = "Mettre à jour les champs de clients potentiels du service";
$MESS["CRM_LEAD_REFRESH_ACCOUNT"] = "Mettre à jour les données du rapport";
$MESS["CRM_LEAD_REFRESH_ACCOUNTING_DLG_TITLE"] = "Mettre à jour les données du rapport";
$MESS["CRM_LEAD_SET_STATUS"] = "Etablir le statut";
$MESS["CRM_LEAD_SHOW"] = "Affichage";
$MESS["CRM_LEAD_SHOW_TITLE"] = "Affichage du Prospect";
$MESS["CRM_LEAD_START_CALL_LIST"] = "Commencer l'appel";
$MESS["CRM_LEAD_STEPWISE_STATE_TEMPLATE"] = "#processed# sur #total#";
$MESS["CRM_LEAD_SUBSCRIBE"] = "Écrire un message";
$MESS["CRM_LEAD_SUBSCRIBE_TITLE"] = "Écrire un message";
$MESS["CRM_LEAD_TASK"] = "Ajouter une tâche";
$MESS["CRM_LEAD_TASK_SHORT"] = "Tâche";
$MESS["CRM_LEAD_TASK_TITLE"] = "Ajouter une tâche";
$MESS["CRM_LEAD_UPDATE_CALL_LIST"] = "Ajouter à la liste d'appel";
$MESS["CRM_REBUILD_SEARCH_CONTENT_STATE"] = "#processed# de #total#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Abonné inconnu";
$MESS["CRM_STATUS_INIT"] = "- Statut -";
