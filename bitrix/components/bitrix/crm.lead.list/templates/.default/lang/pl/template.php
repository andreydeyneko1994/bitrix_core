<?php
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_LEAD_ACTION_DELETE"] = "Usuń";
$MESS["CRM_LEAD_ADD_ACTIVITY"] = "Plan";
$MESS["CRM_LEAD_ADD_ACTIVITY_TITLE"] = "Plan activity";
$MESS["CRM_LEAD_ADD_CALL"] = "Nowe połączenie telefoniczne";
$MESS["CRM_LEAD_ADD_CALL_SHORT"] = "Rozmowa";
$MESS["CRM_LEAD_ADD_CALL_TITLE"] = "Utwórz nowe połączenie telefoniczne";
$MESS["CRM_LEAD_ADD_EMAIL"] = "Nowy e-mail";
$MESS["CRM_LEAD_ADD_EMAIL_TITLE"] = "Utwórz nowy e-mail";
$MESS["CRM_LEAD_ADD_MEETING"] = "Nowe spotkanie";
$MESS["CRM_LEAD_ADD_MEETING_SHORT"] = "Spotkanie";
$MESS["CRM_LEAD_ADD_MEETING_TITLE"] = "Utwórz nowe spotkanie";
$MESS["CRM_LEAD_ADD_QUOTE"] = "Nowa oferta";
$MESS["CRM_LEAD_ADD_QUOTE_TITLE"] = "Nowa oferta";
$MESS["CRM_LEAD_ASSIGN_TO"] = "Wyznacz osobę odpowiedzialną";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_INDEX_REBUILD_STATE"] = "#processed#%";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_INDEX_REBUILD_TITLE"] = "Skanuj duplikaty leadów";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_MERGE_STATE"] = "#processed#%";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_MERGE_TITLE"] = "Scal zduplikowane leady";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_VOL_DATA_PREPARE_STATE"] = "#processed#%";
$MESS["CRM_LEAD_BACKGROUND_DUPLICATE_VOL_DATA_PREPARE_TITLE"] = "Przygotowywanie danych do wyszukiwania duplikatów leadów";
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "Leady zostały przetworzone.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "Konwersja nie powiodła się: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "Pomyślnie przekonwertowano: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_DLG_CLOSE_CONFIRMATION"] = "Nadal trwa konwersja leadu. Czy na pewno chcesz opuścić stronę?";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# z #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "Konwertuj leady";
$MESS["CRM_LEAD_BATCH_DELETION_COMPLETED"] = "Leady zostały usunięte.";
$MESS["CRM_LEAD_BATCH_DELETION_COUNT_FAILED"] = "Nie udało się usunąć: #number#.";
$MESS["CRM_LEAD_BATCH_DELETION_COUNT_SUCCEEDED"] = "Usunięto: #number#.";
$MESS["CRM_LEAD_BIZPROC"] = "Proces Biznesowy";
$MESS["CRM_LEAD_BIZPROC_LIST"] = "Nowy proces biznesowy";
$MESS["CRM_LEAD_BIZPROC_LIST_TITLE"] = "Przeprowadź nowy proces biznesowy";
$MESS["CRM_LEAD_BIZPROC_TITLE"] = "Proces Biznesowy";
$MESS["CRM_LEAD_BUILD_TIMELINE_DLG_TITLE"] = "Przygotowuję historię leadu";
$MESS["CRM_LEAD_BUILD_TIMELINE_STATE"] = "#processed# z #total#";
$MESS["CRM_LEAD_CALENDAR"] = "Dodaj połączenie telefoniczne lub wydarzenie";
$MESS["CRM_LEAD_CALENDAR_TITLE"] = "Dodaj nowy zapis do kalendarza";
$MESS["CRM_LEAD_CONVERT"] = "Konwertuj";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Konwertuj lead";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Potrzebujesz uprawnień do tworzenia kontaktu, firmy i deala, aby skonwertować lead.";
$MESS["CRM_LEAD_CONV_DIALOG_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_LEAD_CONV_DIALOG_CONTINUE_BTN"] = "Kontynuuj";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wybierz obiekty, w których będą stworzone dodatkowe pola";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Następujące pola będą stworzone";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "Wybrane obiekty nie posiadają pól, do których można przekazać dane.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Konwertuj lead";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Wybierz";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Firmy";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Kontakty";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Ostatni";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Szukaj";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Nie znaleziono wpisów";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Wybierz kontakt i firmę";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Ogólny błąd konwersji.";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Wybierz z listy …";
$MESS["CRM_LEAD_COPY"] = "Kopiuj";
$MESS["CRM_LEAD_COPY_TITLE"] = "Kopiuj lead";
$MESS["CRM_LEAD_CREATE_CALL_LIST"] = "Utwórz listę połączeń";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "Generuj";
$MESS["CRM_LEAD_CREATE_ON_BASIS_TITLE"] = "Utwórz kontakt, firmę i deal, korzystając z leada";
$MESS["CRM_LEAD_DELETE"] = "Usuń";
$MESS["CRM_LEAD_DELETE_CONFIRM"] = "Na pewno chcesz to usunąć?";
$MESS["CRM_LEAD_DELETE_TITLE"] = "Usuń lead";
$MESS["CRM_LEAD_EDIT"] = "Edytuj";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Edytuj lead";
$MESS["CRM_LEAD_EVENT"] = "Dodaj wydarzenie";
$MESS["CRM_LEAD_EVENT_SHORT"] = "Wydarzenie";
$MESS["CRM_LEAD_EVENT_TITLE"] = "Dodaj nowe wydarzenie";
$MESS["CRM_LEAD_EXCLUDE"] = "Dodaj do wyjątków";
$MESS["CRM_LEAD_EXCLUDE_CONFIRM"] = "Na pewno chcesz dodać deal do wyjątków?";
$MESS["CRM_LEAD_EXCLUDE_CONFIRM_HELP"] = "Informacje o liście wyjątków...";
$MESS["CRM_LEAD_EXCLUDE_TITLE"] = "Dodaj lead do wyjątków";
$MESS["CRM_LEAD_LIST_APPLY_BUTTON"] = "Zastosuj";
$MESS["CRM_LEAD_LIST_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_LEAD_LIST_BUTTON_SAVE"] = "Zapisz";
$MESS["CRM_LEAD_LIST_CHOOSE_ACTION"] = "Wybierz działanie";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Lejek";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Ustawienia deala";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_SUMMARY"] = "Spowoduje to usunięcie wybranych leadów. Ta operacja może zająć chwilę.";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_TITLE"] = "Usuń leady";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_CALENDAR"] = "Kalendarz";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET"] = "Raporty";
$MESS["CRM_LEAD_LRP_DLG_BTN_CLOSE"] = "Zamknij";
$MESS["CRM_LEAD_LRP_DLG_BTN_START"] = "Wykonaj";
$MESS["CRM_LEAD_LRP_DLG_BTN_STOP"] = "zatrzymaj";
$MESS["CRM_LEAD_LRP_DLG_REQUEST_ERR"] = "Błąd przetwarzania żądania.";
$MESS["CRM_LEAD_LRP_DLG_WAIT"] = "Proszę czekać…";
$MESS["CRM_LEAD_MARK_AS_OPENED"] = "Upublicznij";
$MESS["CRM_LEAD_MARK_AS_OPENED_NO"] = "Nie";
$MESS["CRM_LEAD_MARK_AS_OPENED_YES"] = "Tak";
$MESS["CRM_LEAD_REBUILD_ACCESS_ATTRS"] = "Zaktualizowane uprawnienia dostępu wymagają aktualizacji atrybutów dostępu przy użyciu <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">strony zarządzania uprawnieniami</a>.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX"] = "Kontrola duplikatów wymaga <a id=\"#ID#\" href=\"#URL#\">stworzenia indeksu</a> leadów w bazie danych.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_SUMMARY"] = "Duplikaty firmy zostaną ponownie zindeksowane. Ta operacja może zająć trochę czasu.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_TITLE"] = "Przebuduj indeks duplikatów leadu";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT"] = "Aby użyć szybkiego wyszukiwania, trzeba <a id=\"#ID#\" href=\"#URL#\">ponownie utworzyć indeks wyszukiwania ldeadów</a>.";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_SUMMARY"] = "Indeks wyszukiwania leadów zostanie ponownie utworzony. Może to chwilę potrwać.";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_TITLE"] = "Ponowne tworzenie indeksu wyszukiwania leadów";
$MESS["CRM_LEAD_REBUILD_SECURITY_ATTRS_DLG_TITLE"] = "Aktualizacja uprawnień dostępu";
$MESS["CRM_LEAD_REBUILD_SEMANTICS"] = "Raporty wymagają, abyś <a id=\"#ID#\" href=\"#URL#\">zaktualizował pola leada</a> (akcja nie wpływa na dane użytkownika).";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_SUMMARY"] = "Pola leada zostaną zaktualizowane. To może zająć trochę czasu.";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_TITLE"] = "Aktualizuj pola leada";
$MESS["CRM_LEAD_REFRESH_ACCOUNT"] = "Aktualizuj dane raportu";
$MESS["CRM_LEAD_REFRESH_ACCOUNTING_DLG_TITLE"] = "Aktualizuj dane raportu";
$MESS["CRM_LEAD_SET_STATUS"] = "Ustaw status";
$MESS["CRM_LEAD_SHOW"] = "Wyświetl";
$MESS["CRM_LEAD_SHOW_TITLE"] = "Wyświetl lead";
$MESS["CRM_LEAD_START_CALL_LIST"] = "Rozpocznij wybieranie";
$MESS["CRM_LEAD_STEPWISE_STATE_TEMPLATE"] = "#processed# z #total#";
$MESS["CRM_LEAD_SUBSCRIBE"] = "Wyślij wiadomość";
$MESS["CRM_LEAD_SUBSCRIBE_TITLE"] = "Wyślij wiadomość";
$MESS["CRM_LEAD_TASK"] = "Nowe zadanie";
$MESS["CRM_LEAD_TASK_SHORT"] = "Zadanie";
$MESS["CRM_LEAD_TASK_TITLE"] = "Nowe zadanie";
$MESS["CRM_LEAD_UPDATE_CALL_LIST"] = "Dodaj do listy połączeń";
$MESS["CRM_REBUILD_SEARCH_CONTENT_STATE"] = "#processed# z #total#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Pokaż ilość";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Połączenie";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Nieznany odbiorca";
$MESS["CRM_STATUS_INIT"] = "Status";
