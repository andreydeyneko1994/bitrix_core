<?
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_AUTOCREATION_DISABLED"] = "A conversão de grupo exige que a opção \"Criação automática de contatos, empresas, negócios, faturas e orçamentos\" seja ativada na página de configurações do CRM (Configurações> Outras Configurações> Geral> Configurações de geração automática)";
$MESS["CRM_LEAD_LIST_BATCH_CONVERSION_CONFIG_IS_NOT_SUPPORTED"] = "Os clientes potenciais não podem ser convertidos com as configurações selecionadas.";
$MESS["CRM_LEAD_LIST_DELETION_ACCESS_ERROR"] = "A operação de exclusão foi cancelada porque não havia permissões suficientes.";
$MESS["CRM_LEAD_LIST_DELETION_COMPLETED_SUMMARY"] = "O índice de pesquisa de clientes potenciais foi excluído. Clientes potenciais processados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_NOT_FOUND_ERROR"] = "A operação de exclusão foi cancelada porque não foram encontradas preferências de filtro. Atualize a página e tente novamente.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_OUTDATED_ERROR"] = "A operação de exclusão foi cancelada porque as preferências de filtro estão desatualizadas. Atualize a página e tente novamente.";
$MESS["CRM_LEAD_LIST_DELETION_PARAM_ERROR"] = "A operação de exclusão foi cancelada porque havia parâmetros inválidos.";
$MESS["CRM_LEAD_LIST_DELETION_PROGRESS_SUMMARY"] = "Clientes potenciais excluídos: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_COMPLETED_SUMMARY"] = "Duplicados de Leads foram re-indexadas. Itens processados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_NOT_REQUIRED_SUMMARY"] = "Índice de duplicados de Lead não precisa ser recriado.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_PROGRESS_SUMMARY"] = "Leads processados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "O índice de pesquisa de clientes potenciais foi recriado. Clientes potenciais processados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "O índice de pesquisa de clientes potenciais não precisa ser recriado.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Clientes potenciais processados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Concluída a atualização dos campos de serviço de clientes potenciais. Clientes potenciais processados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Os campos de serviço de clientes potenciais não precisam de atualização.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Clientes potenciais processados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Dados estatísticos de processamento realizados para clientes potenciais. Clientes potenciais processados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Os dados estatísticos para clientes potenciais estão atualizados.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Clientes potenciais processados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
?>