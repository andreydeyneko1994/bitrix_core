<?
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_LOAD"] = "Importar";
$MESS["CRM_EXCLUSION_IMPORT_FORMAT_DESC"] = "Digite os e-mails e/ou números de telefone, cada inserção em uma nova linha.<br>Além disso, você pode adicionar comentários: adicione um ponto e vírgula após um e-mail ou número e digite o texto do comentário. <br>Examplo:<br><br>john@example.com<br>peter@example.com;Peter (entrega)<br>+ 0112233445566<br>+01234567890;Ann";
$MESS["CRM_EXCLUSION_IMPORT_LOADING"] = "Carregando";
$MESS["CRM_EXCLUSION_IMPORT_RECIPIENTS"] = "E-mails e/ou números de telefone";
?>