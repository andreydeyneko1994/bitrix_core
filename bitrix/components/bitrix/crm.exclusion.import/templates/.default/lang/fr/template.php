<?
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_LOAD"] = "Importer";
$MESS["CRM_EXCLUSION_IMPORT_FORMAT_DESC"] = "Saisissez les e-mails et/ou les numéros de téléphone, une entrée par ligne.<br>De plus, vous pouvez ajouter des commentaires : ajoutez un point-virgule après un e-mail ou un numéro, et saisissez le texte du commentaire.<br>Par exemple : <br><br>john@exemple.com<br>peter@exemple.com;Peter (livraison)<br>+0112233445566<br>+01234567890;Ann";
$MESS["CRM_EXCLUSION_IMPORT_LOADING"] = "Chargement";
$MESS["CRM_EXCLUSION_IMPORT_RECIPIENTS"] = "E-mails et/ou numéros de téléphone";
?>