<?
$MESS["VIDEO_TRANSFORMATION_ERROR_DESC"] = "erreur de conversion";
$MESS["VIDEO_TRANSFORMATION_ERROR_TITLE"] = "Un problème est survenu...";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM"] = "Convertir de nouveau";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_NOT_ALLOWED"] = "Permissions insuffisantes";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_NOT_INSTALLED"] = "Le module \"Convertisseur de fichiers\" n'est pas installé.";
$MESS["VIDEO_TRANSFORMATION_ERROR_TRANSFORM_TRANSFORMED"] = "Cette vidéo a déjà été convertie.";
$MESS["VIDEO_TRANSFORMATION_IN_PROCESS_DESC"] = "cela prendra du temps...";
$MESS["VIDEO_TRANSFORMATION_IN_PROCESS_TITLE"] = "La vidéo sera disponible une fois qu'elle sera convertie";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_DESC"] = "Vous pouvez la convertir maintenant";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_TITLE"] = "Cette vidéo n'a pas encore été convertie";
$MESS["VIDEO_TRANSFORMATION_NOT_STARTED_TRANSFORM"] = "Convertir la vidéo";
?>