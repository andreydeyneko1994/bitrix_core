<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_CONTACT_CALLS"] = "Telefony";
$MESS["M_CRM_CONTACT_DEALS"] = "Aktywności";
$MESS["M_CRM_CONTACT_LIST_DELETE"] = "Usuń";
$MESS["M_CRM_CONTACT_LIST_EDIT"] = "Edycja";
$MESS["M_CRM_CONTACT_LIST_FILTER_CUSTOM"] = "Wyniki wyszukiwania";
$MESS["M_CRM_CONTACT_LIST_FILTER_NONE"] = "Wszystkie Kontakty";
$MESS["M_CRM_CONTACT_LIST_MORE"] = "Więcej ...";
$MESS["M_CRM_CONTACT_LIST_PRESET_CHANGE_MY"] = "Zmodyfikowane przeze mnie";
$MESS["M_CRM_CONTACT_LIST_PRESET_MY"] = "Moje kontakty";
$MESS["M_CRM_CONTACT_LIST_PRESET_USER"] = "Własny filtr";
?>