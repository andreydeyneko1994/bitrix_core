<?
$MESS["M_CRM_CONTACT_ADD"] = "Adicionar contato";
$MESS["M_CRM_CONTACT_ADD2"] = "Adicionar";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Adicionar manualmente";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Cartão de visita";
$MESS["M_CRM_CONTACT_BIZCARD_CLOSE"] = "Fechar";
$MESS["M_CRM_CONTACT_BIZCARD_GO_TO_TARIFF_TABLE"] = "Ver os preços";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Carregar imagem...";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Infelizmente, você excedeu sua cota mensal de reconhecimento. Você terá que fazer um upgrade para um plano superior para processar mais cartões de visita.";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Reconhecendo...";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Houve um erro. Tente novamente mais tarde.";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Escanear cartão de visita";
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Todos os contatos";
?>