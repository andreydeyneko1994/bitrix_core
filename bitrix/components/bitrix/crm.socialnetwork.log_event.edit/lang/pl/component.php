<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Niedozwolone.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_ID_NOT_DEFINED"] = "ID jednostki CRM nie jest określony.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_TYPE_NOT_DEFINED"] = "Typ jednostki CRM nie jest określony.";
$MESS["CRM_SL_EVENT_EDIT_HIDDEN_GROUP"] = "Ukryta grupa";
$MESS["CRM_SL_EVENT_NOT_AVAIBLE"] = "Wydarzenia nie są dostępne dla bieżącego użytkownika.";
$MESS["SONET_MODULE_NOT_INSTALLED"] = "Moduł Sieci Społecznościowej nie jest zainstalowany.";
?>