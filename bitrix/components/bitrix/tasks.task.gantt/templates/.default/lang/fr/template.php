<?
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Impossible de lier ces tâches";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "Les modifications que vous avez apportées peuvent être perdues.";
$MESS["TASKS_GANTT_MONTH_APR"] = "Avril";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Août";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Décembre";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Février";
$MESS["TASKS_GANTT_MONTH_JAN"] = "Janvier";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Juillet";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Juin";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Mars";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Mai";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Novembre";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Octobre";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Septembre";
$MESS["TASKS_GANTT_PRINT_SPOTLIGHT_TEXT"] = "Vous pouvez maintenant imprimer le graphique et définir le calendrier horaire.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "L'offre gratuite ne prend en charge que cinq dépendances de tâches. Activez les tâches étendues pour
utiliser un nombre illimité de dépendances.<br>
<br>
Quatre types de dépendances sont pris en charge : <br>
Dépendances classiques (Fin > Début)<br>
Début de tâche simultané (Début > Début)<br>
Fin de tâche simultané (Fin > Fin)<br>
Début à la fin (Début > Fin)<br>
<br>
<a href=\"https://bitrix24.com/pro/tasks.php\" target=\"_blank\">Plus d'informations</a><br>
<br>
Les tâches étendues, le CRM étendu et la téléphonie étendue sont réservées aux offres commerciales sélectionnées.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "Disponible uniquement avec les tâches étendues";
$MESS["TASKS_TITLE"] = "Tâches";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tâches du groupe de travail";
$MESS["TASKS_TITLE_MY"] = "Mes tâches";
?>