<?
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Não é possível vincular essas tarefas";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "As alterações que você fez podem estar perdidas.";
$MESS["TASKS_GANTT_MONTH_APR"] = "Abril";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Agosto";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Dezembro";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Fevereiro";
$MESS["TASKS_GANTT_MONTH_JAN"] = "Janeiro";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Julho";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Junho";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Março";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Maio";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Novembro";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Outubro";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Setembro";
$MESS["TASKS_GANTT_PRINT_SPOTLIGHT_TEXT"] = "Agora você pode imprimir o gráfico e definir o intervalo de tempo por hora.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "O plano gratuito suporta apenas cinco dependências de tarefas. Ative Tarefas Estendidas para usar
dependências ilimitadas.<br>
<br>
Quatro tipos de dependência são suportadas:<br>
Dependências clássicas (Concluir > Iniciar) <br>
Iniciar tarefa simultânea (Iniciar > Iniciar)<br>
Prazo de tarefa simultâneo (Concluir > Concluir) <br>
Iniciar após a conclusão (Iniciar > Concluir)<br>
<br>
<a href=\"https://bitrix24.com/pro/tasks.php\" target=\"_blank\">Saiba mais</a><br>
<br>
Tarefas Estendidas, CRM Estendido e Telefonia Estendida estão disponíveis nos planos comerciais selecionados.";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "Disponível apenas em Tarefas Estendidas";
$MESS["TASKS_TITLE"] = "Tarefas";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tarefas do grupo de trabalho";
$MESS["TASKS_TITLE_MY"] = "Minhas Tarefas";
?>