<?
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "Nazwa pola jest wymagana.";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "Proszę określić nazwę pola w #LANG_NAME#.";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "Mapowanie Pola";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "Ustawienia Siatki";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "Nowe Pole";
$MESS["CC_BLFE_TITLE_EDIT"] = "Parametry Pola: #NAME#";
$MESS["CC_BLFE_TITLE_NEW"] = "Nowe Pole";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "Została niewłaściwie określona lista dla właściwości typu \"Bind\".";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "Nowe Pole";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "Parametry Pola: #NAME#";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "ID pola jest niepoprawne.";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Typy";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>