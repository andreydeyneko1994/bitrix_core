<?
$MESS["CRM_FE_DELETE_TITLE"] = "Usuń";
$MESS["CRM_FE_ENUM_DEFAULT"] = "Wartość Domyślna";
$MESS["CRM_FE_ENUM_DEFAULTS"] = "Wartości Domyślne";
$MESS["CRM_FE_ENUM_IMPORT"] = "Importuj jako listę";
$MESS["CRM_FE_ENUM_IMPORT_HINT"] = "Każda linia utworzy element listy. Jeżeli wartość elementu nie pokrywa się z nazwą elementu, określ ją w nawiasie kwadratowym przed nazwą. Na przykład: \"[v1]Wartość 1\".";
$MESS["CRM_FE_ENUM_NO_DEFAULT"] = "(żaden)";
$MESS["CRM_FE_FIELD_COMMON_LABEL"] = "Nazwa";
$MESS["CRM_FE_FIELD_IS_REQUIRED"] = "Wymagany";
$MESS["CRM_FE_FIELD_MULTIPLE"] = "Wielokrotne";
$MESS["CRM_FE_FIELD_NAME"] = "Nazwa";
$MESS["CRM_FE_FIELD_SHOW_FILTER"] = "Pokaż we filtrze";
$MESS["CRM_FE_FIELD_SHOW_IN_LIST"] = "Pokaż na liście";
$MESS["CRM_FE_FIELD_SORT"] = "Sortowanie";
$MESS["CRM_FE_FIELD_TYPE"] = "Rodzaj";
$MESS["CRM_FE_FIELD_USE_MULTI_LANG_LABEL"] = "Przypisz nazwy we wszystkich językach";
$MESS["CRM_FE_LIST_ITEM_ADD"] = "Dodawanie";
$MESS["CRM_FE_SORT_DOWN_TITLE"] = "W dół";
$MESS["CRM_FE_SORT_UP_TITLE"] = "W górę";
$MESS["CRM_FE_TAB_EDIT"] = "Ustawienia";
$MESS["CRM_FE_TAB_EDIT_TITLE"] = "Ogólne Parametry";
$MESS["CRM_FE_TAB_LIST"] = "Lista";
$MESS["CRM_FE_TAB_LIST_TITLE"] = "Lista wartości właściwości";
$MESS["CRM_FE_TOOLBAR_ADD"] = "Dodaj pole";
$MESS["CRM_FE_TOOLBAR_ADD_TITLE"] = "Dodaj nowe pole";
$MESS["CRM_FE_TOOLBAR_DELETE"] = "Usuń Pole";
$MESS["CRM_FE_TOOLBAR_DELETE_TITLE"] = "Usuń pole i otwórz formularz pól";
$MESS["CRM_FE_TOOLBAR_DELETE_WARNING"] = "Na pewno chcesz usunąć to pole?";
$MESS["CRM_FE_TOOLBAR_FIELDS"] = "Pola";
$MESS["CRM_FE_TOOLBAR_FIELDS_TITLE"] = "Wyświetl dostępne pola";
?>