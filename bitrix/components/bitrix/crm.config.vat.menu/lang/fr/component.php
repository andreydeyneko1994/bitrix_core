<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_VAT_ADD"] = "Ajouter le taux";
$MESS["CRM_VAT_ADD_TITLE"] = "Passer à la création de la nouvelle TVA";
$MESS["CRM_VAT_DELETE"] = "Eliminer le taux";
$MESS["CRM_VAT_DELETE_DLG_BTNTITLE"] = "Elimination du taux de TVA";
$MESS["CRM_VAT_DELETE_DLG_MESSAGE"] = "Êtes-vous sûr de vouloir supprimer ce taux de TVA ?";
$MESS["CRM_VAT_DELETE_DLG_TITLE"] = "Elimination du taux de TVA";
$MESS["CRM_VAT_DELETE_TITLE"] = "Elimination du taux de TVA";
$MESS["CRM_VAT_EDIT"] = "Éditer le tarif";
$MESS["CRM_VAT_EDIT_TITLE"] = "Modifier le taux de TVA";
$MESS["CRM_VAT_LIST"] = "Taux TVA";
$MESS["CRM_VAT_LIST_TITLE"] = "Accéder à la liste des taux TVA";
$MESS["CRM_VAT_SETTINGS"] = "Paramètres";
$MESS["CRM_VAT_SETTINGS_TITLE"] = "Régler le système de taxes";
$MESS["CRM_VAT_SHOW"] = "Consulter le taux";
$MESS["CRM_VAT_SHOW_TITLE"] = "Voir le taux de TVA";
?>