<?
$MESS["SONET_C9_DO_CANCEL"] = "Annuler";
$MESS["SONET_C9_DO_DEL"] = "Supprimer un groupe";
$MESS["SONET_C9_DO_DEL_PROJECT"] = "Supprimer le projet";
$MESS["SONET_C9_SUBTITLE"] = "Voulez-vous vraiment supprimer ce groupe ?";
$MESS["SONET_C9_SUBTITLE_PROJECT"] = "Voulez-vous vraiment supprimer ce projet ?";
$MESS["SONET_C9_SUCCESS"] = "Vous avez supprimé le groupe.";
$MESS["SONET_C9_SUCCESS_PROJECT"] = "Le projet a été supprimé.";
?>