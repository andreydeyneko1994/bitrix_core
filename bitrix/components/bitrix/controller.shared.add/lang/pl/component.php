<?
$MESS["CSA_ERROR_ADD_SITE"] = "Błąd rejestrowania strony w sterowniku.";
$MESS["CSA_ERROR_ADD_SITE2"] = "Błąd rejestrowania strony w sterowniku: #ERROR#";
$MESS["CSA_ERROR_APACHE_TEMPLATE_NOT_FOUND"] = "Nie znaleziono szablonu konfiguracyjnego Apache (#FILE#).";
$MESS["CSA_ERROR_BAD_MYSQL_PATH"] = "Niepoprawny parametr: Ścieżka do wykonywalnego pliku mysql.";
$MESS["CSA_ERROR_CREATE_DIR"] = "Błąd tworzenia katalogu strony.";
$MESS["CSA_ERROR_DB_CONNECT"] = "Błąd połączenia z bazą danych: #ERROR#";
$MESS["CSA_ERROR_DB_CREATE"] = "Błąd tworzenia bazy danych (#DB_NAME#): #ERROR#";
$MESS["CSA_ERROR_DB_FILEDUMP"] = "Plik śmietnika wyjściowej bazy danych nie istnieje lub nie może być odczytany ze względu na brak uprawnień do odczytu.";
$MESS["CSA_ERROR_DB_IMPORT"] = "Błąd importowania bazy danych (#DB_NAME#).";
$MESS["CSA_ERROR_DIR_PERMISSIONS"] = "Niepoprawne uprawnienia przypisane do katalogów wynajętych stron.";
$MESS["CSA_ERROR_DOMAIN_NAME"] = "Nazwa domeny może zawierać jedynie łacińskie litery, liczby, kropki i łącznik.";
$MESS["CSA_ERROR_FILE_PERMISSIONS"] = "Niepoprawne uprawnienia przypisane do plików wynajętych stron.";
$MESS["CSA_ERROR_FILE_WRITE"] = "Błąd zapisu do #FILE#.";
$MESS["CSA_ERROR_MEMORY_LIMIT"] = "Niepoprawny limit pamięci przypisany do wynajętych stron.";
$MESS["CSA_ERROR_NAME_EXISTS"] = "Ta nazwa jest już w użyciu. Proszę wybrać inną nazwę.";
$MESS["CSA_ERROR_NGINX_TEMPLATE_NOT_FOUND"] = "Nie znaleziono szablonu konfiguracyjnego nginx (#FILE#).";
$MESS["CSA_ERROR_NOT_FOUND_APACHE_VHOST_DIR"] = "Katalog z plikami konfiguracyjnymi Apache nie istnieje lub nie może zostać zapisany.";
$MESS["CSA_ERROR_NOT_FOUND_NGINX_VHOST_DIR"] = "Katalog z plikami konfiguracyjnymi nginx nie istnieje lub nie może zostać zapisany.";
$MESS["CSA_ERROR_WRITE_APACHE_CONFIG"] = "Błąd zapisu do konfiguracyjnego pliku Apache.";
$MESS["CSA_ERROR_WRITE_NGINX_CONFIG"] = "Błąd zapisu do konfiguracyjnego pliku nginx.";
$MESS["CSA_LOG_ADD_CLIENT"] = "Dodawanie klienta przez bezpośrednie utworzenie";
$MESS["CSA_MODULE_NOT_INSTALLED"] = "Moduł Sterownika nie jest zainstalowany.";
$MESS["CSA_TITLE"] = "Dodawanie strony";
?>