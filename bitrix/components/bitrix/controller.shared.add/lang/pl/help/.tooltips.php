<?
$MESS["APACHE_ROOT_TIP"] = "Katalog zawierający konfiguracyjne pliki wirtualnych stron dla Apache";
$MESS["CONTROLLER_URL_TIP"] = "Określa sterownik URL strony";
$MESS["DIR_PERMISSIONS_TIP"] = "Określa uprawnienia do folderów zawierających wynajęte strony";
$MESS["FILE_PERMISSIONS_TIP"] = "Określa uprawnienia do plików wynajętych stron";
$MESS["MEMORY_LIMIT_TIP"] = "Limit pamięci dla wynajętych stron";
$MESS["MYSQL_DB_PATH_TIP"] = "Scieżka do pliku zawierającego śmietnik wstępnej bazy danych";
$MESS["MYSQL_PASSWORD_TIP"] = "Hasło użytkownika MySQL, który posiada uprawnienia do tworzenia bazy danych";
$MESS["MYSQL_PATH_TIP"] = "Ścieżka do wykonywalnego pliku mysql";
$MESS["MYSQL_USER_TIP"] = "Login użytkownika MySQL, który posiada uprawnienia do tworzenia bazy danych";
$MESS["NGINX_ROOT_TIP"] = "Katalog zawierający konfiguracyjne pliki wirtualnych stron dla nginx";
$MESS["PATH_PUBLIC_TIP"] = "Publiczne pliki wynajętych stron";
$MESS["PATH_VHOST_TIP"] = "Określa pełną ścieżkę do folderu zawierającego wynajęte strony";
$MESS["RELOAD_FILE_TIP"] = "Oznaczony plik do wyzwalania restartu serwera sieciowego";
$MESS["SET_TITLE_TIP"] = "Ustawia tytuł strony.";
$MESS["URL_SUBDOMAIN_TIP"] = "Określa subdomenę najwyższego poziomu";
?>