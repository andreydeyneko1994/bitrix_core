<?
$MESS["APACHE_ROOT_TIP"] = "Catalogue des fichiers de configurations des sites virtuels pour apache";
$MESS["CONTROLLER_URL_TIP"] = "URL du contrôleur des sites";
$MESS["DIR_PERMISSIONS_TIP"] = "Droits pour les répertoires des sites loués";
$MESS["FILE_PERMISSIONS_TIP"] = "Droits des fichiers pour des sites en collocation";
$MESS["MEMORY_LIMIT_TIP"] = "Limite de mémoire pour les sites loués";
$MESS["MYSQL_DB_PATH_TIP"] = "Chemin vers le fichier avec un vidage initial de la base de données";
$MESS["MYSQL_PASSWORD_TIP"] = "Mot de passe de l'utilisateur MySQL avec privilège de création des bases de données";
$MESS["MYSQL_PATH_TIP"] = "Chemin vers le fichier mysql en exécution";
$MESS["MYSQL_USER_TIP"] = "Nom de l'utilisateur MySQL avec privilège de création des bases de données";
$MESS["NGINX_ROOT_TIP"] = "La catalogue des fichiers de configuration des sites virtuels pour nginx";
$MESS["PATH_PUBLIC_TIP"] = "Fichiers de la partie publique des sites en location";
$MESS["PATH_VHOST_TIP"] = "Chemin complet vers le dossier pour l'emplacement des sites loués";
$MESS["RELOAD_FILE_TIP"] = "Fichier drapeau pour rechargement du serveur WEB";
$MESS["SET_TITLE_TIP"] = "Installer l'en-tête de la page.";
$MESS["URL_SUBDOMAIN_TIP"] = "Domaine du niveau haut";
?>