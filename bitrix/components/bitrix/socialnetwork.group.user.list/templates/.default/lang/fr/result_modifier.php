<?php
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST"] = "Accepter la demande";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_PROJECT_TITLE"] = "Accepter la demande à rejoindre le projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_SCRUM_TITLE"] = "Accepter la demande à rejoindre le scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_ACCEPT_REQUEST_TITLE"] = "Accepter la demande à rejoindre le groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST"] = "Annuler la demande";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_PROJECT_TITLE"] = "Annuler la demande de projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_SCRUM_TITLE"] = "Annuler la demande de scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_INCOMING_REQUEST_TITLE"] = "Annuler la demande de groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST"] = "Annuler l'invitation";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_PROJECT_TITLE"] = "Annuler l'invitation à rejoindre le projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_SCRUM_TITLE"] = "Annuler l'invitation à rejoindre le scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_DELETE_REQUEST_TITLE"] = "Annuler l'invitation à rejoindre le groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE"] = "Supprimer";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_PROJECT_TITLE"] = "Supprimer du projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_SCRUM_TITLE"] = "Supprimer du scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_EXCLUDE_TITLE"] = "Supprimer du groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST"] = "Refuser la demande";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_PROJECT_TITLE"] = "Refuser la demande à rejoindre le projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_SCRUM_TITLE"] = "Refuser la demande à rejoindre le scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REJECT_REQUEST_TITLE"] = "Refuser la demande à rejoindre le groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR"] = "Supprimer des modérateurs";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_PROJECT"] = "Supprimer des assistants responsables";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_PROJECT_TITLE"] = "Supprimer des assistants responsables de projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_SCRUM"] = "Supprimer de l'équipe de développement";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_SCRUM_TITLE"] = "Supprimer de l'équipe de développement";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_REMOVE_MODERATOR_TITLE"] = "Supprimer des modérateurs de groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR"] = "Nommer modérateur";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_PROJECT"] = "Nommer assistant responsable";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_PROJECT_TITLE"] = "Nommer assistant responsable du projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_SCRUM"] = "Ajouter à l'équipe de développement";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_SCRUM_TITLE"] = "Ajouter à l'équipe de développement";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_MODERATOR_TITLE"] = "Nommer modérateur du groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER"] = "Nommer propriétaire";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_PROJECT"] = "Nommer responsable";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_PROJECT_TITLE"] = "Nommer gestionnaire du projet";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_SCRUM"] = "Nommer propriétaire";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_SCRUM_TITLE"] = "Nommer propriétaire du produit";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_OWNER_TITLE"] = "Nommer propriétaire du groupe de travail";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_SET_SCRUM_MASTER"] = "Nommer maître Scrum";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_VIEW"] = "Afficher le profil";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_ACTION_VIEW_TITLE"] = "Afficher le profil de l'utilisateur";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_BUTTON_INVITE_TITLE"] = "Inviter";
$MESS["SOCIALNETWORK_GROUP_USER_LIST_TEMPLATE_DEPARTMENT_DISCONNECT"] = "déconnecter";
