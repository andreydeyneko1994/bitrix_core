<?
$MESS["CRM_CURRENCY_DEC_POINT"] = "Punkt dziesiętny";
$MESS["CRM_CURRENCY_FIELD_AMOUNT_CNT"] = "Wartość nominalna";
$MESS["CRM_CURRENCY_FIELD_DEFAULT_EXCH_RATE"] = "Kurs wymiany(domyślnie)";
$MESS["CRM_CURRENCY_FIELD_ID"] = "Waluta";
$MESS["CRM_CURRENCY_FIELD_SORT"] = "Indeks sortowania";
$MESS["CRM_CURRENCY_FORMAT_STRING"] = "Format waluty";
$MESS["CRM_CURRENCY_FULL_NAME"] = "Nazwa";
$MESS["CRM_CURRENCY_INVOICES_DEFAULT"] = "Domyślna waluta fakturowania";
$MESS["CRM_CURRENCY_NOT_FOUND"] = "Nie znaleziono waluty.";
$MESS["CRM_CURRENCY_SECTION_MAIN"] = "Waluta";
$MESS["CRM_CURRENCY_SHOW_BASE"] = "Waluta podstawowa";
$MESS["CRM_CURRENCY_THOUSANDS_SEP"] = "Niestandardowy separator tysięcy";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT"] = "Separator tysięcy";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_ANOTHER"] = "Inne";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_B"] = "Spacja nie-łamiąca";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_C"] = "przecinek";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_D"] = "Okres";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_N"] = "brak";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_S"] = "spacja";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>