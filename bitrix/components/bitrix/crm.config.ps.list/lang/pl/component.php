<?php
$MESS["CRM_COLUMN_ACTIVE"] = "Aktywne";
$MESS["CRM_COLUMN_NAME"] = "Nazwa";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Rodzaj klienta";
$MESS["CRM_COLUMN_SORT"] = "Sortuj";
$MESS["CRM_COMPANY_PT"] = "Firma";
$MESS["CRM_CONTACT_PT"] = "Kontakt";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Bład usuwania systemu płatności.";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2"] = "Używane do płatności online";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_BILL"] = "Używane w formularzu wydruku faktury";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_2_QUOTE"] = "Użyte w wydruku formularza wyceny";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "Brak limitu dla kwot płatności";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Zwraca ograniczenia w zależności od metody płatności";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Błąd aktualizacji systemu płatności.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Moduł e-Sklepu nie jest zainstalowany.";
