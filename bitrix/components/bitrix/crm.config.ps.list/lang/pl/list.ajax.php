<?
$MESS["CRM_PS_REQUISITES_TRANSFER_COMPLETED_SUMMARY"] = "Wykonano migrację opcji płatniczych do firm.";
$MESS["CRM_PS_REQUISITES_TRANSFER_NOT_REQUIRED_SUMMARY"] = "Szczegóły sprzedawcy nie są wymagane do migracji z opcji płatności do firm.";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY1"] = "Szczegóły szablonów dodane…";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY2"] = "Firmy i szczegóły utworzono…";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY3"] = "Przetworzone faktury: #PROCESSED_ITEMS# z #TOTAL_ITEMS#...";
?>