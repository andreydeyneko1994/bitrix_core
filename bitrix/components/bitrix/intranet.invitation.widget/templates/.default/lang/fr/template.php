<?php
$MESS["INTRANET_INVITATION_WIDGET_DESC"] = "pourquoi inviter ?";
$MESS["INTRANET_INVITATION_WIDGET_DISABLED_TEXT"] = "Seul votre administrateur Bitrix24 peut inviter de nouveaux utilisateurs à rejoindre votre Bitrix24. Veuillez le contacter pour inviter une personne ou obtenir une autorisation d'invitation afin de le faire vous-même.";
$MESS["INTRANET_INVITATION_WIDGET_EDIT"] = "Modifier";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES"] = "Utilisateurs";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_LIMIT"] = "Limite atteinte";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_NO_LIMIT"] = "Utilisateurs illimités";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET"] = "Extranet";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET_DESC"] = "qu'est-ce qu'un extranet ?";
$MESS["INTRANET_INVITATION_WIDGET_INVITE"] = "Inviter";
$MESS["INTRANET_INVITATION_WIDGET_INVITE_EMPLOYEE"] = "Inviter des utilisateurs";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ADMIN_INVITE"] = "Seuls les administrateurs peuvent inviter";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ALL_INVITE"] = "Tout le monde peut inviter";
$MESS["INTRANET_INVITATION_WIDGET_STRUCTURE"] = "Structure";
