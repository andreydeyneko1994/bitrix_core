<?php
$MESS["INTRANET_INVITATION_WIDGET_DESC"] = "Dlaczego warto zapraszać?";
$MESS["INTRANET_INVITATION_WIDGET_DISABLED_TEXT"] = "Wyłącznie administrator Bitrix24 może zapraszać nowych użytkowników do Twojego Bitrix24. Skontaktuj się z nim, aby zaprosić osobę lub uzyskać pozwolenie na samodzielne zaproszenie.";
$MESS["INTRANET_INVITATION_WIDGET_EDIT"] = "Edytuj";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES"] = "Użytkownicy";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_LIMIT"] = "Osiągnięto limit";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_NO_LIMIT"] = "Nieograniczona liczba użytkowników";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET"] = "Extranet";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET_DESC"] = "czym jest ekstranet?";
$MESS["INTRANET_INVITATION_WIDGET_INVITE"] = "Zaproś";
$MESS["INTRANET_INVITATION_WIDGET_INVITE_EMPLOYEE"] = "Zaproś użytkowników";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ADMIN_INVITE"] = "Tylko administratorzy mogą zapraszać";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ALL_INVITE"] = "Każdy może zapraszać";
$MESS["INTRANET_INVITATION_WIDGET_STRUCTURE"] = "Struktura";
