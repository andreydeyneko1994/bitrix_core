<?php
$MESS["INTRANET_INVITATION_WIDGET_DESC"] = "por que convidar?";
$MESS["INTRANET_INVITATION_WIDGET_DISABLED_TEXT"] = "Apenas o administrador do Bitrix24 pode convidar novos usuários para o Bitrix24. Entre em contato com ele para convidar uma pessoa ou obter permissão de convite para que você possa fazer isso sozinho.";
$MESS["INTRANET_INVITATION_WIDGET_EDIT"] = "Editar";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES"] = "Usuários";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_LIMIT"] = "Limite alcançado";
$MESS["INTRANET_INVITATION_WIDGET_EMPLOYEES_NO_LIMIT"] = "Usuários ilimitados";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET"] = "Extranet";
$MESS["INTRANET_INVITATION_WIDGET_EXTRANET_DESC"] = "o que é extranet?";
$MESS["INTRANET_INVITATION_WIDGET_INVITE"] = "Convidar";
$MESS["INTRANET_INVITATION_WIDGET_INVITE_EMPLOYEE"] = "Convidar usuários";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ADMIN_INVITE"] = "Somente administradores podem convidar";
$MESS["INTRANET_INVITATION_WIDGET_SETTING_ALL_INVITE"] = "Todos podem convidar";
$MESS["INTRANET_INVITATION_WIDGET_STRUCTURE"] = "Estrutura";
