<?
$MESS["M_SORT_ASC"] = "Rosnąco";
$MESS["M_SORT_BUTTON"] = "Sortuj";
$MESS["M_SORT_CANCEL"] = "Anuluj";
$MESS["M_SORT_DESC"] = "Malejąco";
$MESS["M_SORT_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_SORT_FIELDS"] = "Sortuj pola";
$MESS["M_SORT_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_SORT_NO_FIELDS"] = "Pola sortowania nieokreślone";
$MESS["M_SORT_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_SORT_TITLE"] = "Sortuj po";
?>