<?
$MESS["CRM_TAX_SETTINGS_CHOOSE"] = "Wybierz rodzaj podatku";
$MESS["CRM_TAX_SETTINGS_SAVE_BUTTON"] = "Zapisz";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Ustawienia podatku";
$MESS["CRM_TAX_TAX"] = "Podatek zależny od lokalizacji (podatek od sprzedaży)";
$MESS["CRM_TAX_TAX1"] = "Podatek zależny od lokalizacji (podatek od sprzedaży)";
$MESS["CRM_TAX_TAX_HINT"] = "Podatek ten jest stosowany do całkowitej kwoty zamówienia. Stawka podatku zależy od lokalizacji klienta.";
$MESS["CRM_TAX_TAX_HINT1"] = "Podatek ten jest stosowany do całkowitej kwoty zamówienia. Stawka podatku zależy od lokalizacji klienta.";
$MESS["CRM_TAX_VAT"] = "Podatek zależny od przedmiotu (VAT)";
$MESS["CRM_TAX_VAT_HINT"] = "Podatek ten jest określony i stosowany do każdego elementu z osobna.";
$MESS["CRM_TAX_VAT_HINT1"] = "Podatek ten jest określony i stosowany do każdego elementu z osobna.";
?>