<?
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS"] = "Niewystarczające uprawnienia do dodania pliku.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS2"] = "Niewystarczające uprawnienia do dodania zaktualizowanej wersji pliku.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FILE"] = "Nie można znaleźć pliku.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FOLDER"] = "Nie można znaleźć folderu.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_UPLOAD_VERSION"] = "Nie można zaktualizować pliku.";
?>