<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_QUOTE_LIST_CHANGE_STATUS"] = "Alterar status";
$MESS["M_CRM_QUOTE_LIST_COMPANY"] = "Vinculado à empresa";
$MESS["M_CRM_QUOTE_LIST_CONTACT"] = "Vinculado ao contato";
$MESS["M_CRM_QUOTE_LIST_CREATE_BASE"] = "Criar usando fonte";
$MESS["M_CRM_QUOTE_LIST_DEAL"] = "Vinculado à Negócio";
$MESS["M_CRM_QUOTE_LIST_DELETE"] = "Excluir";
$MESS["M_CRM_QUOTE_LIST_EDIT"] = "Editar";
$MESS["M_CRM_QUOTE_LIST_FILTER_NONE"] = "Todos os orçamentos";
$MESS["M_CRM_QUOTE_LIST_LEAD"] = "Vinculado ao Lead";
$MESS["M_CRM_QUOTE_LIST_MORE"] = "Mais...";
$MESS["M_CRM_QUOTE_LIST_PRESET_MY"] = "Meus orçamentos";
$MESS["M_CRM_QUOTE_LIST_PRESET_NEW"] = "Novos orçamentos";
$MESS["M_CRM_QUOTE_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_QUOTE_LIST_SEND"] = "Enviar";
?>