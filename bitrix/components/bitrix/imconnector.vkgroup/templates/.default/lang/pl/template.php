<?php
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Zmień";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Grupa podłączona";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Wydarzenie podłączone";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Podłączono stronę publiczną";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Grupa podłączona";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Połącz wydarzenie";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Podłącz stronę publiczną";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Odłącz";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Komunikator";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Zaloguj się przy użyciu konta, do którego podłączona jest odnośna grupa VK, strona publiczna lub wydarzenie, aby odbierać wiadomości od klientów na Bitrix24:";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Moje pozostałe grupy, strony publiczne lub wydarzenia na VK";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Inne grupy,<br>strony publiczne<br>lub wydarzenia";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Wybierz grupę, stronę publiczną albo wydarzenie na VK, aby podłączyć je do Otwartego kanału Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "Nie masz grupy, strony publicznej ani wydarzenia na VK, którego jesteś administratorem. <br>Utwórz teraz.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Utwórz";
