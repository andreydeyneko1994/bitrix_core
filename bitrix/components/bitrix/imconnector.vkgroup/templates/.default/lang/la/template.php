<?php
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_AUTHORIZATION"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_AUTHORIZE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE_ANY_TIME"] = "Se puede editar o desactivar en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED"] = "VK conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Evento conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Página pública conectada";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Conectar evento";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Conectar la página pública";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Conecte su grupo VK a los Canales Abiertos y comuníquelos desde su cuenta Bitrix24.</p>
    <ul class=\"im-connector-settings-header-list\">
     <li class=\"im-connector-settings-header-list-item\">Distribución automática de mensajes entrantes según las reglas de la cola.</li>
     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar Bitrix24</li>
     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial de CRM</li>
    </ul>";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Iniciar sesión utilizando una cuenta de un grupo VK, página pública o evento de interés que esté vinculada a recibir mensajes de sus clientes a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Mis otros grupos VK, páginas o eventos públicos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Otros grupos,<br>páginas públicas<br>o eventos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Seleccionar un grupo VK, página pública o evento para conectarse al Canal Abierto de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "No es necesario que un grupo VK, página pública o evento del cual usted era un administrador. <br> Crear uno ahora mismo.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TITLE"] = "Comunicarse con VK a través de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_USER"] = "Cuenta";
