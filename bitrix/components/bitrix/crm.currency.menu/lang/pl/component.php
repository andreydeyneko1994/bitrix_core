<?
$MESS["CRM_CURRENCY_ADD"] = "Dodaj walutę";
$MESS["CRM_CURRENCY_ADD_TITLE"] = "Utwórz nowy rekord waluty";
$MESS["CRM_CURRENCY_DELETE"] = "Usuń walutę";
$MESS["CRM_CURRENCY_DELETE_DLG_BTNTITLE"] = "Usuń walutę";
$MESS["CRM_CURRENCY_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć walutę?";
$MESS["CRM_CURRENCY_DELETE_DLG_TITLE"] = "Usuń walutę";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Usuń walutę";
$MESS["CRM_CURRENCY_EDIT"] = "Edytuj";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Edytuj ten rekord waluty";
$MESS["CRM_CURRENCY_LIST"] = "Wszystkie waluty";
$MESS["CRM_CURRENCY_LIST_TITLE"] = "Wyświetl listę wszystkich walut";
$MESS["CRM_CURRENCY_SHOW"] = "Wyświetl";
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Wyświetl walutę";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>