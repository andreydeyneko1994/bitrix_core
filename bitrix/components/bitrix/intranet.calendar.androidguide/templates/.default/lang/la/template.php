<?php
$MESS["EC_CALENDAR_ANDROID_POINT_ADD_EVENT_TEXT"] = "Agregue eventos al calendario de Bitrix24 desde su dispositivo";
$MESS["EC_CALENDAR_ANDROID_POINT_CALENDAR_SYNC_GOOD_TEXT"] = "Manténgase al día con los nuevos eventos en cualquier lugar y en cualquier momento";
$MESS["EC_CALENDAR_ANDROID_POINT_DOCS_DETAIL_LINK"] = "Más información sobre la sincronización";
$MESS["EC_CALENDAR_ANDROID_POINT_PERMISSION_TEXT"] = "Asegúrese de que la aplicación Bitrix24 tenga permiso para acceder al calendario de su dispositivo";
$MESS["EC_CALENDAR_ANDROID_PREVIEW_TEXT"] = "Su calendario Bitrix24 se sincronizó";
$MESS["EC_CALENDAR_ANDROID_START_WORK"] = "Iniciar ";
