<?php
$MESS["EC_CALENDAR_ANDROID_POINT_ADD_EVENT_TEXT"] = "Ajoutez des évènements au calendrier Bitrix24 depuis votre appareil";
$MESS["EC_CALENDAR_ANDROID_POINT_CALENDAR_SYNC_GOOD_TEXT"] = "Restez à tout moment au courant des nouveaux évènements, où que vous soyez";
$MESS["EC_CALENDAR_ANDROID_POINT_DOCS_DETAIL_LINK"] = "En savoir plus sur la synchronisation";
$MESS["EC_CALENDAR_ANDROID_POINT_PERMISSION_TEXT"] = "Assurez-vous que l'application Bitrix24 est autorisée à accéder au calendrier de votre appareil";
$MESS["EC_CALENDAR_ANDROID_PREVIEW_TEXT"] = "Votre calendrier Bitrix24 a été synchronisé";
$MESS["EC_CALENDAR_ANDROID_START_WORK"] = "Commencer";
