<?php
$MESS["EC_CALENDAR_ANDROID_POINT_ADD_EVENT_TEXT"] = "Dodaj wydarzenia do kalendarza Bitrix24 ze swojego urządzenia";
$MESS["EC_CALENDAR_ANDROID_POINT_CALENDAR_SYNC_GOOD_TEXT"] = "Bądź na bieżąco z nowymi wydarzeniami w dowolnym miejscu i czasie";
$MESS["EC_CALENDAR_ANDROID_POINT_DOCS_DETAIL_LINK"] = "Dowiedz się więcej o synchronizacji";
$MESS["EC_CALENDAR_ANDROID_POINT_PERMISSION_TEXT"] = "Upewnij się, że aplikacja Bitrix24 ma dostęp do kalendarza urządzenia";
$MESS["EC_CALENDAR_ANDROID_PREVIEW_TEXT"] = "Twój kalendarz Bitrix24 został zsynchronizowany";
$MESS["EC_CALENDAR_ANDROID_START_WORK"] = "Uruchom";
