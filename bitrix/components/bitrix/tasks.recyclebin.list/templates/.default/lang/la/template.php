<?php
$MESS["TASKS_RECYCLEBIN_CONVERT_DATA"] = "Las rutinas del motor de tareas se están actualizando en este momento. La recuperación de datos no está disponible temporalmente. Intente de nuevo más tarde.";
$MESS["TASKS_RECYCLEBIN_TITLE"] = "Papelera de reciclaje";
