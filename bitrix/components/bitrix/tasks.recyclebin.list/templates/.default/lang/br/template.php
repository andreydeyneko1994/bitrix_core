<?php
$MESS["TASKS_RECYCLEBIN_CONVERT_DATA"] = "As rotinas do mecanismo de tarefas estão sendo atualizadas agora. A recuperação de dados está temporariamente indisponível. Tente novamente mais tarde.";
$MESS["TASKS_RECYCLEBIN_TITLE"] = "Lixeira";
