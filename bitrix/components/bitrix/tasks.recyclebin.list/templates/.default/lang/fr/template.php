<?php
$MESS["TASKS_RECYCLEBIN_CONVERT_DATA"] = "Les routines du moteur de tâches sont en cours de mise à jour. La récupération des données est temporairement indisponible. Veuillez réessayer plus tard.";
$MESS["TASKS_RECYCLEBIN_TITLE"] = "Corbeille";
