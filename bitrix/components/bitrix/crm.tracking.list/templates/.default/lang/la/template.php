<?php
$MESS["CRM_TRACKING_LIST_CHANNELS"] = "Canales de comunicación";
$MESS["CRM_TRACKING_LIST_SOURCES"] = "Fuentes de tráfico";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP"] = "Ayuda para la configuración";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP_ORDER"] = "Pida a los socios asistencia para la configuración";
$MESS["CRM_TRACKING_START_CONFIGURATION_NEED_HELP"] = "¿Necesita ayuda para configurar el rastreador?";
$MESS["CRM_TRACKING_START_CONFIGURATION_ORDER"] = "Solicitar ayuda";
