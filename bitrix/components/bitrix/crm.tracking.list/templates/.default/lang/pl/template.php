<?php
$MESS["CRM_TRACKING_LIST_CHANNELS"] = "Kanały komunikacji";
$MESS["CRM_TRACKING_LIST_SOURCES"] = "Źródła ruchu";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP"] = "Pomoc w konfiguracji";
$MESS["CRM_TRACKING_START_CONFIGURATION_HELP_ORDER"] = "Poproś partnerów o pomoc w konfiguracji";
$MESS["CRM_TRACKING_START_CONFIGURATION_NEED_HELP"] = "Czy potrzebujesz pomocy w konfiguracji monitora?";
$MESS["CRM_TRACKING_START_CONFIGURATION_ORDER"] = "Poproś o pomoc";
