<?
$MESS["INTR_EMP_CANCEL"] = "Anuluj";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Anuluj wybór pracowników";
$MESS["INTR_EMP_EXTRANET"] = "Extranet";
$MESS["INTR_EMP_HEAD"] = "Kierownik działu";
$MESS["INTR_EMP_LAST"] = "Najnowsze elementy";
$MESS["INTR_EMP_NOTHING_FOUND"] = "wyszukiwanie nie przyniosło rezultatów";
$MESS["INTR_EMP_SEARCH"] = "Wyszukaj pracownika";
$MESS["INTR_EMP_SUBMIT"] = "wybierz";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Zaznacz wybranych pracowników";
$MESS["INTR_EMP_WAIT"] = "Ładowanie…";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Zamknij";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Wybierz pracownika";
?>