<?
$MESS["CRM_ORDER_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_ORDER_DA_ADD_COUPON_ERROR"] = "Error al agregar el cupón";
$MESS["CRM_ORDER_DA_BASKET_CODE_ABSENT"] = "Falta el ID del elemento del carrito de compras";
$MESS["CRM_ORDER_DA_BASKET_ID_BY_CODE_ERROR"] = "No se puede obtener el ID del artículo del carrito de compras";
$MESS["CRM_ORDER_DA_CART_NOT_FOUND"] = "El carrito de compras no fue encontrado";
$MESS["CRM_ORDER_DA_COUPONS_ABSENT"] = "Faltan cupones";
$MESS["CRM_ORDER_DA_CURRENCY_CHANGED"] = "Moneda del pedido actualizada. Por favor, compruebe que todos los precios se han convertido correctamente.";
$MESS["CRM_ORDER_DA_DELETE_COUPON_ERROR"] = "Error al eliminar el cupón";
$MESS["CRM_ORDER_DA_DELIVERY_INDEX_ERROR"] = "Indice de envío indefinido";
$MESS["CRM_ORDER_DA_GROUP_ACTION_ABSENT"] = "No se especificó ninguna acción grupal";
$MESS["CRM_ORDER_DA_INSUFFICIENT_RIGHTS"] = "Permisos insuficientes.";
$MESS["CRM_ORDER_DA_ORDER_ID_NEGATIVE"] = "Se requiere el ID de un pedido válido o datos de entrada para crear un nuevo pedido";
$MESS["CRM_ORDER_DA_PRODUCT_ID_NOT_DEFINED"] = "ID de producto no definido";
$MESS["CRM_ORDER_DA_PROPERTIES_ABSENT"] = "Falta la orden de propiedad de SKU";
$MESS["CRM_ORDER_DA_PROPERTIES_ORDER_ABSENT"] = "Falta el orden de propiedades SKU";
$MESS["CRM_ORDER_DA_QUANTITY_ABSENT"] = "La cantidad de producto no está definida";
$MESS["CRM_ORDER_NOT_FOUND"] = "El pedido no fue encontrado";
$MESS["CRM_ORDER_NOT_SELECTED"] = "el campo está vacío";
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "No se encontró el pago";
$MESS["CRM_ORDER_SHIPMENT_NOT_FOUND"] = "No se encontró el envío";
$MESS["CRM_ORDER_WRONG_FIELD_NAME"] = "Nombre de campo inválido";
$MESS["CRM_ORDER_WRONG_FIELD_VALUE"] = "Valor de campo no válido";
$MESS["CRM_ORDER_WRONG_PROPERTY_NAME"] = "Nombre de propiedad inválido";
$MESS["CRM_ORDER_WRONG_PROPERTY_TYPE"] = "Tipo de propiedad inválido";
?>