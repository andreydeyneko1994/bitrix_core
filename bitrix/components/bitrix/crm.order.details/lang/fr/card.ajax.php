<?
$MESS["CRM_ODCA_COMPANY"] = "Société";
$MESS["CRM_ODCA_CONTACT"] = "Contact";
$MESS["CRM_ODCA_DATE_UPDATED"] = "Date de modification";
$MESS["CRM_ODCA_GO_ORDER"] = "Afficher";
$MESS["CRM_ODCA_ORDER_NUM"] = "##ORDER_NUMBER#";
$MESS["CRM_ODCA_PRICE"] = "Prix";
$MESS["CRM_ODCA_PRODUCTS"] = "Produits";
$MESS["CRM_ODCA_STATUS"] = "Statut";
?>