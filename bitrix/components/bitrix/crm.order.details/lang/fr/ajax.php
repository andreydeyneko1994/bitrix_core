<?
$MESS["CRM_ORDER_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_ORDER_DA_ADD_COUPON_ERROR"] = "Erreur lors de l'ajout du coupon";
$MESS["CRM_ORDER_DA_BASKET_CODE_ABSENT"] = "L'ID de l'article du panier manque";
$MESS["CRM_ORDER_DA_BASKET_ID_BY_CODE_ERROR"] = "Impossible de récupérer l'ID de l'article du panier";
$MESS["CRM_ORDER_DA_CART_NOT_FOUND"] = "Le panier de la commande est introuvable";
$MESS["CRM_ORDER_DA_COUPONS_ABSENT"] = "Les coupons manquent";
$MESS["CRM_ORDER_DA_CURRENCY_CHANGED"] = "Devise de la commande mise à jour. Veuillez vérifier que tous les prix ont bien été correctement convertis.";
$MESS["CRM_ORDER_DA_DELETE_COUPON_ERROR"] = "Erreur lors de la suppression du coupon";
$MESS["CRM_ORDER_DA_DELIVERY_INDEX_ERROR"] = "Code postal de la livraison non défini";
$MESS["CRM_ORDER_DA_GROUP_ACTION_ABSENT"] = "Aucune action de groupe spécifiée";
$MESS["CRM_ORDER_DA_INSUFFICIENT_RIGHTS"] = "Permissions insuffisantes.";
$MESS["CRM_ORDER_DA_ORDER_ID_NEGATIVE"] = "Un ID de commande valide est requis, ou saisissez des données pour créer une nouvelle commande";
$MESS["CRM_ORDER_DA_PRODUCT_ID_NOT_DEFINED"] = "ID du produit non défini";
$MESS["CRM_ORDER_DA_PROPERTIES_ABSENT"] = "La commande de la propriété UGS manque";
$MESS["CRM_ORDER_DA_PROPERTIES_ORDER_ABSENT"] = "La commande de la propriété UGS manque";
$MESS["CRM_ORDER_DA_QUANTITY_ABSENT"] = "La quantité du produit n'est pas définie";
$MESS["CRM_ORDER_NOT_FOUND"] = "La commande est introuvable";
$MESS["CRM_ORDER_NOT_SELECTED"] = "le champ est vide";
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "Paiement introuvable";
$MESS["CRM_ORDER_SHIPMENT_NOT_FOUND"] = "Livraison introuvable";
$MESS["CRM_ORDER_WRONG_FIELD_NAME"] = "Nom de champ non valide";
$MESS["CRM_ORDER_WRONG_FIELD_VALUE"] = "Valeur du champ non valide";
$MESS["CRM_ORDER_WRONG_PROPERTY_NAME"] = "Nom de la propriété non valide";
$MESS["CRM_ORDER_WRONG_PROPERTY_TYPE"] = "Type de propriété non valide";
?>