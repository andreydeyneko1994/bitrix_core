<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_RECYCLE_BIN_PRESET_MAIN_ENTITIES"] = "Prospects, transactions, contacts et entreprises";
$MESS["CRM_RECYCLE_LIST_TTL_NOTICE"] = "Les fichiers envoyés dans la Corbeille sont conservés pendant #TTL_DAY# jours";
