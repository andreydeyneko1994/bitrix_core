<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_RECYCLE_BIN_PRESET_MAIN_ENTITIES"] = "Leads, Negócios, Contatos e Empresas";
$MESS["CRM_RECYCLE_LIST_TTL_NOTICE"] = "Arquivos excluídos para a Lixeira são mantidos por #TTL_DAY# dias";
