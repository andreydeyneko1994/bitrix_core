<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_RECYCLE_BIN_PRESET_MAIN_ENTITIES"] = "Prospectos, negociaciones, contactos y compañías";
$MESS["CRM_RECYCLE_LIST_TTL_NOTICE"] = "Los archivos que se enviaron a la papelera de reciclaje se guardan durante #TTL_DAY# días";
