<?php
$MESS["VOX_IVR_EDIT_ERROR_ACCESS_DENIED"] = "Você não tem permissão de acesso para editar as configurações de telefonia";
$MESS["VOX_IVR_EDIT_ERROR_IVR_DEPTH_TOO_LARGE"] = "O nível de aninhamento IVR excede o limite.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_NOT_AVAILABLE"] = "O menu de voz não está disponível no seu plano atual.";
$MESS["VOX_IVR_FILE_TOO_LARGE"] = "O tamanho do arquivo excede o valor máximo possível.";
$MESS["VOX_IVR_FILE_UPLOAD_ERROR"] = "Erro de carregamento de arquivo.";
