<?
$MESS["CRM_ML_MODEL_LIST_AVAILABLE_MODELS"] = "Modelos disponíveis";
$MESS["CRM_ML_MODEL_LIST_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_ML_MODEL_LIST_BUTTON_DISABLE"] = "Desativar";
$MESS["CRM_ML_MODEL_LIST_BUTTON_TRAIN_FREE_OF_CHARGE"] = "Treine grátis";
$MESS["CRM_ML_MODEL_LIST_CONFIRMATION"] = "Confirmar ação";
$MESS["CRM_ML_MODEL_LIST_DEAL_SCORING_DISABLE"] = "Desativar pontuação de negócios para este pipeline";
$MESS["CRM_ML_MODEL_LIST_DISABLE_DEAL_SCORING"] = "Desativar pontuação de negócios para este pipeline?";
$MESS["CRM_ML_MODEL_LIST_DISABLE_LEAD_SCORING"] = "Desativar pontuação de leads?";
$MESS["CRM_ML_MODEL_LIST_HELP"] = "Ajuda";
$MESS["CRM_ML_MODEL_LIST_LEAD_SCORING_DISABLE"] = "Desativar pontuação de leads";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P1"] = "É a intuição que o seu gerente de vendas está levando em consideração ao lidar com os negócios? Traga novos clientes para o topo da sua lista.";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P2"] = "A pontuação IA analisará os atuais negócios para mostrar suas probabilidades de sucesso. O sistema ajudará seus funcionários a identificar áreas que exigem mais atenção. Não perca seu tempo com quem não tem intenção de se tornar seu cliente!";
$MESS["CRM_ML_MODEL_LIST_SCORING_ENOUGH_DATA"] = "Seu CRM possui dados suficientes para treinar o modelo de pontuação IA.";
$MESS["CRM_ML_MODEL_LIST_SCORING_ERROR_TOO_SOON_2"] = "A pontuação IA foi desativada conforme sua solicitação. Você poderá treinar o modelo IA e ativar a pontuação após #DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_QUALITY"] = "Qualidade do modelo: #QUALITY#%.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_READY"] = "O modelo IA foi treinado e agora está pronto para previsão.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_TRAINING_DATE"] = "Treinado em: #TRAINING_DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_NOT_ENOUGH_DATA"] = "Seu CRM não possui dados suficientes para treinar o modelo de pontuação IA. Consulte este #LINK_START#artigo#LINK_END# para saber mais sobre o tamanho e o conteúdo de um conjunto de dados necessário para o treinamento.";
$MESS["CRM_ML_MODEL_LIST_SCORING_REENABLE_WARNING"] = "Você não poderá reativar a pontuação até #DATE#";
$MESS["CRM_ML_MODEL_LIST_SCORING_TITLE"] = "Pontuação IA";
$MESS["CRM_ML_MODEL_LIST_SCORING_TRAINING_IN_PROCESS"] = "O treinamento do modelo de pontuação IA está em andamento. Concluído: #PROGRESS#%";
?>