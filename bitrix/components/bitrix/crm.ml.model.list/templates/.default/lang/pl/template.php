<?
$MESS["CRM_ML_MODEL_LIST_AVAILABLE_MODELS"] = "Dostępne modele";
$MESS["CRM_ML_MODEL_LIST_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_ML_MODEL_LIST_BUTTON_DISABLE"] = "Wyłącz";
$MESS["CRM_ML_MODEL_LIST_BUTTON_TRAIN_FREE_OF_CHARGE"] = "Trenuj bezpłatnie";
$MESS["CRM_ML_MODEL_LIST_CONFIRMATION"] = "Potwierdź działanie";
$MESS["CRM_ML_MODEL_LIST_DEAL_SCORING_DISABLE"] = "Wyłącz ocenianie deali dla tego lejka";
$MESS["CRM_ML_MODEL_LIST_DISABLE_DEAL_SCORING"] = "Wyłączyć ocenianie deali dla tego lejka?";
$MESS["CRM_ML_MODEL_LIST_DISABLE_LEAD_SCORING"] = "Wyłączyć ocenianie leadów?";
$MESS["CRM_ML_MODEL_LIST_HELP"] = "Pomoc";
$MESS["CRM_ML_MODEL_LIST_LEAD_SCORING_DISABLE"] = "Wyłącz ocenianie leadów";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P1"] = "Czy przy obsłudze deali Twój menedżer sprzedaży bierze pod uwagę intuicję? Przenieś obiecujących klientów na szczyt listy.";
$MESS["CRM_ML_MODEL_LIST_SCORING_DESCRIPTION_P2"] = "Ocenianie AI przeanalizuje istniejące deale, aby pokazać ich prawdopodobieństwo sukcesu. System pomoże Twoim pracownikom zidentyfikować obszary wymagające największej uwagi. Nie trać czasu na tych, którzy nie mają zamiaru stać się Twoimi klientami!";
$MESS["CRM_ML_MODEL_LIST_SCORING_ENOUGH_DATA"] = "Twój system CRM ma wystarczające dane, aby wytrenować model oceniania AI.";
$MESS["CRM_ML_MODEL_LIST_SCORING_ERROR_TOO_SOON_2"] = "Zgodnie z Twoją prośbą ocenianie AI zostało wyłączone. Będziesz mógł trenować model AI i umożliwić ocenianie po #DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_QUALITY"] = "Jakość modelu: #QUALITY#%.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_READY"] = "Model AI został wytrenowany i jest teraz gotowy do prognozowania.";
$MESS["CRM_ML_MODEL_LIST_SCORING_MODEL_TRAINING_DATE"] = "Wytrenowany: #TRAINING_DATE#.";
$MESS["CRM_ML_MODEL_LIST_SCORING_NOT_ENOUGH_DATA"] = "Twój system CRM nie ma wystarczających danych, aby wytrenować model oceniania AI. Zapoznaj się z tym #LINK_START#artykułem#LINK_END#, aby dowiedzieć się więcej na temat wielkości i zawartości zestawu danych wymaganych do trenowania.";
$MESS["CRM_ML_MODEL_LIST_SCORING_REENABLE_WARNING"] = "Nie będzie można ponownie włączyć oceniania do #DATE#";
$MESS["CRM_ML_MODEL_LIST_SCORING_TITLE"] = "Ocenianie AI";
$MESS["CRM_ML_MODEL_LIST_SCORING_TRAINING_IN_PROCESS"] = "Trening modelu oceniania AI w toku. Wykonano: #PROGRESS#%";
?>