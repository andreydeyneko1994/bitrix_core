<?
$MESS["REQUISITE_ADD"] = "Ajouter des mentions";
$MESS["REQUISITE_ADD_TITLE"] = "Ajouter des mentions";
$MESS["REQUISITE_COPY"] = "Copier";
$MESS["REQUISITE_COPY_TITLE"] = "Copier les mentions";
$MESS["REQUISITE_DELETE"] = "Supprimer les mentions";
$MESS["REQUISITE_DELETE_DLG_BTNTITLE"] = "Supprimer";
$MESS["REQUISITE_DELETE_DLG_MESSAGE"] = "Voulez-vous vraiment supprimer cet élément ?";
$MESS["REQUISITE_DELETE_DLG_TITLE"] = "Supprimer les mentions";
$MESS["REQUISITE_DELETE_TITLE"] = "Supprimer les mentions";
?>