<?
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_DESC"] = "error de conversión";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TITLE"] = "Algo salió mal...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM"] = "Convertir de nuevo";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_ALLOWED"] = "Permisos insuficientes";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_INSTALLED"] = "El módulo \"Convertidor de archivos\" no está instalado.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_TRANSFORMED"] = "Este video ya fue convertido.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_DESC"] = "esto tomará un poco de tiempo...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_TITLE"] = "El video estará disponible una vez que se haya convertido,";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_DESC"] = "Puede convertirlo ahora";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TITLE"] = "Este video aún no ha sido convertido";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TRANSFORM"] = "Convertir el video";
?>