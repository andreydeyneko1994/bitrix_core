<?
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_DESC"] = "erro de conversão";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TITLE"] = "Algo deu errado...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM"] = "Converter novamente";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_ALLOWED"] = "Permissões insuficientes";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_NOT_INSTALLED"] = "O módulo \"Conversor de Arquivos\" não está instalado.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_ERROR_TRANSFORM_TRANSFORMED"] = "Este vídeo já foi convertido.";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_DESC"] = "vai levar algum tempo...";
$MESS["DISK_FILE_TRANSFORM_VIDEO_IN_PROCESS_TITLE"] = "O vídeo estará disponível assim que for convertido";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_DESC"] = "Você pode convertê-lo agora";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TITLE"] = "Este vídeo ainda não foi convertido";
$MESS["DISK_FILE_TRANSFORM_VIDEO_NOT_STARTED_TRANSFORM"] = "Converter Vídeo";
?>