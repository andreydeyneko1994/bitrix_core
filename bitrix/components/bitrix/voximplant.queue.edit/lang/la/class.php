<?php
$MESS["VI_CONFIG_ERROR_ACCESS_DENIED"] = "No tiene permiso para editar la configuración de telefonía.";
$MESS["VI_CONFIG_ERROR_EMPTY_NAME"] = "Se requiere el nombre de la cola";
$MESS["VI_CONFIG_ERROR_MAX_GROUP_COUNT_REACHED"] = "Su Bitrix24 alcanzó el número máximo de grupos de usuarios de telefonía";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_GROUP"] = "Este número de extensión ya lo está utilizando el grupo #NAME#.";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_USER"] = "Este número de extensión ya lo está utilizando #NAME#.";
$MESS["VI_CONFIG_ERROR_PHONE_NUMBER_TOO_LONG"] = "La longitud del número de extensión no debe exceder de 4 números.";
