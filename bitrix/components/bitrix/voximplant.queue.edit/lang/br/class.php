<?php
$MESS["VI_CONFIG_ERROR_ACCESS_DENIED"] = "Você não tem permissão para editar as configurações de telefonia.";
$MESS["VI_CONFIG_ERROR_EMPTY_NAME"] = "O nome do grupo de fila é obrigatório";
$MESS["VI_CONFIG_ERROR_MAX_GROUP_COUNT_REACHED"] = "Seu Bitrix24 atingiu o número máximo de grupos de usuários de telefonia";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_GROUP"] = "Este número de ramal já está em uso pelo grupo #NAME#.";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_USER"] = "Este número de ramal já está em uso por #NAME#.";
$MESS["VI_CONFIG_ERROR_PHONE_NUMBER_TOO_LONG"] = "O número do ramal não deve exceder 4 dígitos.";
