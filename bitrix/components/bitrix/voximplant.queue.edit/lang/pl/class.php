<?php
$MESS["VI_CONFIG_ERROR_ACCESS_DENIED"] = "Nie masz uprawnień do edycji ustawień telefonii.";
$MESS["VI_CONFIG_ERROR_EMPTY_NAME"] = "Wymagana jest nazwa grupy kolejkowej";
$MESS["VI_CONFIG_ERROR_MAX_GROUP_COUNT_REACHED"] = "Osiągnięto maksymalną liczbę grup użytkowników telefonii na koncie Bitrix24";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_GROUP"] = "Ten numer wewnętrzny jest już używany przez grupę #NAME#.";
$MESS["VI_CONFIG_ERROR_NUMBER_IN_USE_BY_USER"] = "Ten numer wewnętrzny jest już używany przez #NAME#.";
$MESS["VI_CONFIG_ERROR_PHONE_NUMBER_TOO_LONG"] = "Długość numeru wewnętrznego nie może przekraczać 4 cyfr.";
