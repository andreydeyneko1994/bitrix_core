<?
$MESS["INTR_ISBN_PARAM_DETAIL_URL"] = "Strona profilu użytkownika";
$MESS["INTR_ISBN_PARAM_NUM_USERS"] = "Pokaż użytkowników";
$MESS["INTR_ISH_PARAM_DATE_FORMAT"] = "Format daty";
$MESS["INTR_ISH_PARAM_DATE_FORMAT_NO_YEAR"] = "Format daty (bez roku)";
$MESS["INTR_ISH_PARAM_DATE_TIME_FORMAT"] = "Format daty i godziny";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISH_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Szablon Ścieżki do Strony Działu";
$MESS["INTR_ISH_PARAM_PATH_TO_VIDEO_CALL"] = "Strona rozmowy wideo";
$MESS["INTR_ISH_PARAM_PM_URL"] = "Strona wiadomości osobistych";
$MESS["INTR_ISH_PARAM_SHOW_LOGIN"] = "Pokaż Login, jeżeli nie są dostępne wymagane pola nazwy użytkownika";
$MESS["INTR_ISH_PARAM_SHOW_YEAR"] = "Pokaż rok urodzenia";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_M"] = "tylko mężczyźni";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_N"] = "Nikt";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_Y"] = "Wszystkie";
?>