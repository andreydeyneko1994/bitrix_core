<?
$MESS["INTRANET_USER_PROFILE_EMPTY_DEPARTMENT_ERROR"] = "Le service n'a pas été selectionné";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_NOTIFY"] = "Vous avez été redirigé depuis l'extranet vers l'intranet. Veuillez vous déconnecter, puis vous reconnecter pour que les modifications prennent effet.";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_SUCCESS"] = "L'employé a bien été transféré. Il peut à présent accéder au contenu interne de votre intranet en fonction de ses permissions.";
?>