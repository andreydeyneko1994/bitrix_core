<?
$MESS["INTRANET_USER_PROFILE_EMPTY_DEPARTMENT_ERROR"] = "O departamento não está selecionado";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_NOTIFY"] = "Você foi transferido da extranet para a intranet. Por favor, saia e faça login novamente para que as alterações se efetivem.";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_SUCCESS"] = "O funcionário foi transferido com sucesso. Agora, eles podem acessar seu conteúdo interno da intranet de acordo com suas permissões.";
?>