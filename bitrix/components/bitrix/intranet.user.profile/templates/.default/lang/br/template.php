<?php
$MESS["INTRANET_USER_PROFILE_ADDITIONAL_INFO"] = "Informações adicionais";
$MESS["INTRANET_USER_PROFILE_ADD_PHOTO"] = "Alterar foto";
$MESS["INTRANET_USER_PROFILE_ADMIN_MODE"] = "Modo Admin";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL"] = "Instalação do aplicativo";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL_TEXT"] = "Você receberá uma mensagem de texto contendo um link para baixar e instalar o aplicativo móvel Bitrix24.";
$MESS["INTRANET_USER_PROFILE_APP_PHONE"] = "Telefone";
$MESS["INTRANET_USER_PROFILE_APP_SEND"] = "Enviar";
$MESS["INTRANET_USER_PROFILE_AVATAR_CAMERA"] = "Tirar uma foto";
$MESS["INTRANET_USER_PROFILE_AVATAR_LOAD"] = "Carregar uma foto";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_ADD"] = "Enviar apreciações";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_TITLE"] = "Apreciações";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_MODIFY"] = "editar";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_BUTTON"] = "Fale sobre você";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_TEXT"] = "Compartilhe histórias interessantes da sua vida ou fale de você para os outros usuários, faça upload de fotos de momentos memoráveis.";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_TITLE"] = "Sobre mim";
$MESS["INTRANET_USER_PROFILE_CHAT"] = "Bate-papo";
$MESS["INTRANET_USER_PROFILE_CHAT_HISTORY"] = "Log de mensagem";
$MESS["INTRANET_USER_PROFILE_CLOSE"] = "Fechar";
$MESS["INTRANET_USER_PROFILE_CONTACT_INFO"] = "Informações de contato";
$MESS["INTRANET_USER_PROFILE_DELETE"] = "Excluir";
$MESS["INTRANET_USER_PROFILE_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir o usuário convidado?";
$MESS["INTRANET_USER_PROFILE_DESKTOP_APP"] = "Aplicativo desktop";
$MESS["INTRANET_USER_PROFILE_DISK_INSTALLED"] = "Bitrix24.Drive instalado";
$MESS["INTRANET_USER_PROFILE_DISK_SPACE"] = "espaço em disco: #VALUE#";
$MESS["INTRANET_USER_PROFILE_FIELD_LAST_NAME"] = "Sobrenome";
$MESS["INTRANET_USER_PROFILE_FIELD_MANAGERS"] = "Supervisor";
$MESS["INTRANET_USER_PROFILE_FIELD_NAME"] = "Nome";
$MESS["INTRANET_USER_PROFILE_FIELD_SECOND_NAME"] = "Nome do meio";
$MESS["INTRANET_USER_PROFILE_FIELD_SUBORDINATE"] = "Subordinados";
$MESS["INTRANET_USER_PROFILE_FIRE"] = "Dispensar";
$MESS["INTRANET_USER_PROFILE_FIRE_CONFIRM"] = "O funcionário não poderá acessar o Bitrix24 e não será visto na estrutura da empresa. Todos os seus dados (arquivos, mensagens, tarefas etc.) permanecerão intactos.
<br/><br/>
Tem certeza de que deseja dispensar o funcionário?";
$MESS["INTRANET_USER_PROFILE_HIRE"] = "Contratar";
$MESS["INTRANET_USER_PROFILE_HIRE_CONFIRM"] = "O funcionário poderá acessar o Bitrix24 e será visto na estrutura da empresa.
<br/><br/>
Tem certeza de que deseja conceder acesso ao funcionário?";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON"] = "Fale sobre seus interesses";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON_2"] = "Selecionar interesses";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_TEXT"] = "Crie ou junte-se a interesses. Encontre amigos que compartilham dos mesmos interesses que você.";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_F"] = "visto pela última vez #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_F"] = "ocioso desde #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_M"] = "ocioso desde #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_M"] = "visto pela última vez #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_MOBILE_APP"] = "Aplicativo móvel";
$MESS["INTRANET_USER_PROFILE_MORE"] = "Mostrar mais #NUM#";
$MESS["INTRANET_USER_PROFILE_MOVE"] = "Transferir";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM"] = "Seu Bitrix24 atingiu o número máximo possível de administradores.<br/>Seus privilégios de administrador serão revogados se você configurar esse usuário como administrador. Tem certeza de que deseja transferir permissões de administrador para este usuário?";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM_PROMO"] = "#LINK_START#Aproveite mais seu Bitrix24#LINK_END#";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET"] = "Transferir para intranet";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_TITLE"] = "Transferir para intranet";
$MESS["INTRANET_USER_PROFILE_NO"] = "Não";
$MESS["INTRANET_USER_PROFILE_PASSWORDS"] = "Senhas";
$MESS["INTRANET_USER_PROFILE_PHOTO_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir a foto?";
$MESS["INTRANET_USER_PROFILE_QUIT_ADMIN_MODE"] = "Sair do modo Admin";
$MESS["INTRANET_USER_PROFILE_REINVITE"] = "Convidar novamente";
$MESS["INTRANET_USER_PROFILE_REINVITE_SUCCESS"] = "O convite foi enviado";
$MESS["INTRANET_USER_PROFILE_REMOVE_ADMIN_RIGHTS"] = "Revogar permissões de administrador";
$MESS["INTRANET_USER_PROFILE_RIGHTS"] = "Permissões";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_MORE"] = "Saiba mais";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT1"] = "Aproveite mais o seu Bitrix24";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT2"] = "Designe vários administradores para garantir que o seu Bitrix24 seja gerenciado o tempo todo! Os administradores têm acesso total a todos os recursos, incluindo telefonia, Canais Abertos e configurações! Você pode designar a quantidade de administradores especificada no seu plano Bitrix24:";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TITLE"] = "Limite de administradores";
$MESS["INTRANET_USER_PROFILE_SECURITY"] = "Segurança";
$MESS["INTRANET_USER_PROFILE_SET_ADMIN_RIGHTS"] = "Atribuir permissões de administrador";
$MESS["INTRANET_USER_PROFILE_SET_INEGRATOR_RIGHTS"] = "Conceder direitos de parceiro Bitrix24";
$MESS["INTRANET_USER_PROFILE_SET_INTEGRATOR_RIGHTS_CONFIRM"] = "Os parceiros certificados Bitrix24 podem ajudar você a configurar ou ajustar o seu Bitrix24 aos fluxos de trabalho da sua empresa: CRM, Canais Abertos, documentação, telefonia, relatórios e outras ferramentas empresariais. 
#LINK_START#Saiba mais#LINK_END#
<br/><br/>
#NAME# receberá permissões de acesso total, com exceção dos direitos de convidar ou dispensar administradores. 
Tem certeza de que deseja conceder os direitos de parceiro Bitrix24 a esse usuário?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_BUTTON"] = "Como medir?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_INDICATOR_TEXT"] = "estresse";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_TITLE"] = "Meça seu nível de estresse";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_INVISIBLE"] = "Outros usuários não podem visualizar seu nível de estresse.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_VISIBLE"] = "Outros usuários podem visualizar seu nível de estresse.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_SHARE_LINK"] = "copiar para a Área de Transferência";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE"] = "Sincronizar";
$MESS["INTRANET_USER_PROFILE_TAGS_MODIFY"] = "editar";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_ADD"] = "Adicionar item";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_2"] = "No momento, não há interesses aos quais você possa se juntar. Crie seus interesses!";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_3"] = "Para adicionar interesses, digite-os e aperte Enter. Separe vários interesses com um espaço.";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_SEARCH_TITLE"] = "Resultado da pesquisa:";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_TITLE"] = "Itens populares:";
$MESS["INTRANET_USER_PROFILE_TAGS_TITLE"] = "Coisas que eu gosto";
$MESS["INTRANET_USER_PROFILE_TARIFF_COMPANY"] = "Professional";
$MESS["INTRANET_USER_PROFILE_TARIFF_PROJECT"] = "Free";
$MESS["INTRANET_USER_PROFILE_TARIFF_TEAM"] = "Standard";
$MESS["INTRANET_USER_PROFILE_TARIFF_TF"] = "Plus";
$MESS["INTRANET_USER_PROFILE_THEME"] = "Alterar fundo";
$MESS["INTRANET_USER_PROFILE_UNLIM"] = "ilimitado";
$MESS["INTRANET_USER_PROFILE_VIDEOCALL"] = "Chamada de vídeo";
$MESS["INTRANET_USER_PROFILE_VIEW_ACCESS_DENIED"] = "Acesso negado ao perfil do usuário.";
$MESS["INTRANET_USER_PROFILE_YES"] = "Sim";
$MESS["INTRANET_USER_PROFILE_admin"] = "Administrador";
$MESS["INTRANET_USER_PROFILE_email"] = "Usuário de e-mail";
$MESS["INTRANET_USER_PROFILE_employee"] = "Ações";
$MESS["INTRANET_USER_PROFILE_extranet"] = "Extranet";
$MESS["INTRANET_USER_PROFILE_fired"] = "Dispensar";
$MESS["INTRANET_USER_PROFILE_integrator"] = "Parceiro";
$MESS["INTRANET_USER_PROFILE_invited"] = "Convidado";
$MESS["INTRANET_USER_PROFILE_shop"] = "Usuário da loja virtual";
$MESS["INTRANET_USER_PROFILE_visitor"] = "Convidado";
$MESS["NTRANET_USER_PROFILE_MOBILE_GOOGLE_PLAY_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
