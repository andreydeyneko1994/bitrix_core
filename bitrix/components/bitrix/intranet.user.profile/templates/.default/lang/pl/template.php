<?php
$MESS["INTRANET_USER_PROFILE_ADDITIONAL_INFO"] = "Dodatkowe informacje";
$MESS["INTRANET_USER_PROFILE_ADD_PHOTO"] = "Zmień zdjęcie";
$MESS["INTRANET_USER_PROFILE_ADMIN_MODE"] = "Tryb administratora";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL"] = "Instalacja aplikacji";
$MESS["INTRANET_USER_PROFILE_APP_INSTALL_TEXT"] = "Otrzymasz wiadomość tekstową zawierającą link umożliwiający pobranie i zainstalowanie aplikacji mobilnej Bitrix24.";
$MESS["INTRANET_USER_PROFILE_APP_PHONE"] = "Telefon";
$MESS["INTRANET_USER_PROFILE_APP_SEND"] = "Wyślij";
$MESS["INTRANET_USER_PROFILE_AVATAR_CAMERA"] = "Zrób zdjęcie";
$MESS["INTRANET_USER_PROFILE_AVATAR_LOAD"] = "Prześlij zdjęcie";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_ADD"] = "Wyślij wyrazy uznania";
$MESS["INTRANET_USER_PROFILE_BLOG_GRAT_TITLE"] = "Wyrazy uznania";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_MODIFY"] = "edytuj";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_BUTTON"] = "Opowiedz o sobie";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_STUB_TEXT"] = "Podziel się ciekawymi historiami z życia lub opowiedz innym użytkownikom o sobie, prześlij zdjęcia z niezapomnianych chwil.";
$MESS["INTRANET_USER_PROFILE_BLOG_POST_TITLE"] = "O mnie";
$MESS["INTRANET_USER_PROFILE_CHAT"] = "Czat";
$MESS["INTRANET_USER_PROFILE_CHAT_HISTORY"] = "Dziennik wiadomości";
$MESS["INTRANET_USER_PROFILE_CLOSE"] = "Zamknij";
$MESS["INTRANET_USER_PROFILE_CONTACT_INFO"] = "Dane kontaktowe";
$MESS["INTRANET_USER_PROFILE_DELETE"] = "Usuń";
$MESS["INTRANET_USER_PROFILE_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć zaproszonego użytkownika?";
$MESS["INTRANET_USER_PROFILE_DESKTOP_APP"] = "Aplikacja desktopowa";
$MESS["INTRANET_USER_PROFILE_DISK_INSTALLED"] = "Bitrix24.Drive zainstalowano";
$MESS["INTRANET_USER_PROFILE_DISK_SPACE"] = "miejsce na dysku: #VALUE#";
$MESS["INTRANET_USER_PROFILE_FIELD_LAST_NAME"] = "Nazwisko";
$MESS["INTRANET_USER_PROFILE_FIELD_MANAGERS"] = "Przełożony";
$MESS["INTRANET_USER_PROFILE_FIELD_NAME"] = "Imię";
$MESS["INTRANET_USER_PROFILE_FIELD_SECOND_NAME"] = "Drugie imię";
$MESS["INTRANET_USER_PROFILE_FIELD_SUBORDINATE"] = "Podwładni";
$MESS["INTRANET_USER_PROFILE_FIRE"] = "Zwolnij";
$MESS["INTRANET_USER_PROFILE_FIRE_CONFIRM"] = "Pracownik nie będzie mógł zalogować się do Bitrix24 i nie będzie widoczny w strukturze firmy. Wszystkie jego dane (pliki, wiadomości, zadania itd.) pozostaną nienaruszone.
<br/><br/>
Czy na pewno chcesz zwolnić pracownika?";
$MESS["INTRANET_USER_PROFILE_HIRE"] = "Zatrudnij";
$MESS["INTRANET_USER_PROFILE_HIRE_CONFIRM"] = "Pracownik będzie mógł zalogować się do Bitrix24 i będzie widoczny w strukturze firmy.
<br/><br/>
Czy na pewno chcesz udzielić pracownikowi dostępu?";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON"] = "Opowiedz o swoich zainteresowaniach";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_BUTTON_2"] = "Wybierz zainteresowania";
$MESS["INTRANET_USER_PROFILE_INTERESTS_STUB_TEXT"] = "Twórz zainteresowania lub dołączaj do nich. Znajdź przyjaciół, którzy podzielają twoje zainteresowania.";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_F"] = "ostatnio wyświetlony #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_F"] = "bezczynny od #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_IDLE_M"] = "bezczynny od #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_LAST_SEEN_M"] = "ostatnio wyświetlony #LAST_SEEN#";
$MESS["INTRANET_USER_PROFILE_MOBILE_APP"] = "Aplikacja mobilna";
$MESS["INTRANET_USER_PROFILE_MORE"] = "Pokaż więcej #NUM#";
$MESS["INTRANET_USER_PROFILE_MOVE"] = "Przenieś";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM"] = "Twój Bitrix24 osiągnął maksymalną możliwą liczbę administratorów.<br/>Twoje uprawnienia administratora zostaną odwołane, jeśli ustawisz tego użytkownika jako administratora. Czy na pewno chcesz przenieść uprawnienia administratora na tego użytkownika?";
$MESS["INTRANET_USER_PROFILE_MOVE_ADMIN_RIGHTS_CONFIRM_PROMO"] = "#LINK_START#Uzyskaj więcej ze swojego Bitrix24#LINK_END#";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET"] = "Przenieś do intranetu";
$MESS["INTRANET_USER_PROFILE_MOVE_TO_INTRANET_TITLE"] = "Przenieś do intranetu";
$MESS["INTRANET_USER_PROFILE_NO"] = "Nie";
$MESS["INTRANET_USER_PROFILE_PASSWORDS"] = "Hasła";
$MESS["INTRANET_USER_PROFILE_PHOTO_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć zdjęcie?";
$MESS["INTRANET_USER_PROFILE_QUIT_ADMIN_MODE"] = "Wyjdź z trybu administratora";
$MESS["INTRANET_USER_PROFILE_REINVITE"] = "Zaproś ponownie";
$MESS["INTRANET_USER_PROFILE_REINVITE_SUCCESS"] = "Zaproszenie zostało wysłane";
$MESS["INTRANET_USER_PROFILE_REMOVE_ADMIN_RIGHTS"] = "Cofnij uprawnienia administratora";
$MESS["INTRANET_USER_PROFILE_RIGHTS"] = "Uprawnienia";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_MORE"] = "Dowiedz się więcej";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT1"] = "Uzyskaj więcej ze swojego Bitrix24";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TEXT2"] = "Aby mieć pewność, że Bitrix24 jest zarządzany przez cały czas, przypisz wielu administratorów,! Administratorzy mają pełny dostęp do wszystkich funkcji, w tym telefonii, otwartych kanałów i ustawień! Możesz przypisać taką liczbę administratorów, jaką określono w planie Bitrix24:";
$MESS["INTRANET_USER_PROFILE_RIGHTS_RESTR_TITLE"] = "Limit liczby administratorów";
$MESS["INTRANET_USER_PROFILE_SECURITY"] = "Zabezpieczenia";
$MESS["INTRANET_USER_PROFILE_SET_ADMIN_RIGHTS"] = "Przypisz uprawnienia administratora";
$MESS["INTRANET_USER_PROFILE_SET_INEGRATOR_RIGHTS"] = "Przyznaj Bitrix24 uprawnienia partnera";
$MESS["INTRANET_USER_PROFILE_SET_INTEGRATOR_RIGHTS_CONFIRM"] = "Certyfikowani partnerzy Bitrix24 mogą pomóc w konfiguracji lub zoptymalizowaniu Bitrix24 do przepływów pracy w Twojej firmie: CRM, Otwarte kanały, dokumentacja, telefonia, raporty i inne narzędzia biznesowe. 
#LINK_START#Dowiedz się więcej#LINK_END#
<br/><br/>
#NAME# otrzyma pełne uprawnienia dostępu, z wyjątkiem uprawnień do zapraszania lub usuwania administratorów. 
Czy na pewno przyznać temu użytkownikowi uprawnienia partnera Bitrix24?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_BUTTON"] = "Jak zmierzyć?";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_INDICATOR_TEXT"] = "stres";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_TITLE"] = "Zmierz swój poziom stresu";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_INVISIBLE"] = "Inni użytkownicy nie mogą zobaczyć Twojego poziomu stresu.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_VISIBLE"] = "Inni użytkownicy mogą zobaczyć Twój poziom stresu.";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_SHARE_LINK"] = "skopiuj do Schowka";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE"] = "Synchronizuj";
$MESS["INTRANET_USER_PROFILE_TAGS_MODIFY"] = "edytuj";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_ADD"] = "Dodaj element";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_2"] = "Obecnie nie ma zainteresowania, do którego możesz dołączyć. Utwórz swoje zainteresowania!";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_HINT_3"] = "Aby dodać zainteresowania, wprowadź je i naciśnij klawisz Enter. Rozdzielaj wiele zainteresowań spacjami.";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_SEARCH_TITLE"] = "Wynik wyszukiwania:";
$MESS["INTRANET_USER_PROFILE_TAGS_POPUP_TITLE"] = "Popularne tagi:";
$MESS["INTRANET_USER_PROFILE_TAGS_TITLE"] = "Rzeczy, które lubię";
$MESS["INTRANET_USER_PROFILE_TARIFF_COMPANY"] = "Professional";
$MESS["INTRANET_USER_PROFILE_TARIFF_PROJECT"] = "Free";
$MESS["INTRANET_USER_PROFILE_TARIFF_TEAM"] = "Standard";
$MESS["INTRANET_USER_PROFILE_TARIFF_TF"] = "Plus";
$MESS["INTRANET_USER_PROFILE_THEME"] = "Zmień tło";
$MESS["INTRANET_USER_PROFILE_UNLIM"] = "bez ograniczeń";
$MESS["INTRANET_USER_PROFILE_VIDEOCALL"] = "Połączenie wideo";
$MESS["INTRANET_USER_PROFILE_VIEW_ACCESS_DENIED"] = "Odmowa dostępu do profilu użytkownika.";
$MESS["INTRANET_USER_PROFILE_YES"] = "Tak";
$MESS["INTRANET_USER_PROFILE_admin"] = "Administrator";
$MESS["INTRANET_USER_PROFILE_email"] = "Użytkownik poczty elektronicznej";
$MESS["INTRANET_USER_PROFILE_employee"] = "Działania";
$MESS["INTRANET_USER_PROFILE_extranet"] = "Ekstranet";
$MESS["INTRANET_USER_PROFILE_fired"] = "Zwolniony";
$MESS["INTRANET_USER_PROFILE_integrator"] = "Partner";
$MESS["INTRANET_USER_PROFILE_invited"] = "Zaproszony";
$MESS["INTRANET_USER_PROFILE_shop"] = "Użytkownik sklepu internetowego";
$MESS["INTRANET_USER_PROFILE_visitor"] = "Gość";
$MESS["NTRANET_USER_PROFILE_MOBILE_GOOGLE_PLAY_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
