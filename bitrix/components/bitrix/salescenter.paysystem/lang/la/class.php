<?
$MESS["SP_MENU_ITEM_PARAMS"] = "Parámetros";
$MESS["SP_MENU_ITEM_RESTRICTION"] = "Restricciones";
$MESS["SP_MENU_ITEM_RESTRICTION_SET"] = "Mis restricciones";
$MESS["SP_MENU_ITEM_SORT"] = "Clasificar";
$MESS["SP_PAYMENT_SUB_TITLE"] = "(pago mediante #SUB_TITLE#)";
$MESS["SP_RP_CONFIRM_DEL_MESSAGE"] = "¿Seguro que desea eliminar esta restricción?";
$MESS["SP_RP_DELETE_DESCR"] = "Eliminar";
$MESS["SP_RP_EDIT_DESCR"] = "Editar";
$MESS["SP_SALESCENTER_SALE_ACCESS_DENIED"] = "Acceso denegado.";
?>