<?
$MESS["SP_MENU_ITEM_PARAMS"] = "Parametry";
$MESS["SP_MENU_ITEM_RESTRICTION"] = "Ograniczenia";
$MESS["SP_MENU_ITEM_RESTRICTION_SET"] = "Moje ograniczenia";
$MESS["SP_MENU_ITEM_SORT"] = "Sortuj";
$MESS["SP_PAYMENT_SUB_TITLE"] = "(płatność za pośrednictwem #SUB_TITLE#)";
$MESS["SP_RP_CONFIRM_DEL_MESSAGE"] = "Czy na pewno chcesz usunąć to ograniczenie?";
$MESS["SP_RP_DELETE_DESCR"] = "Usuń";
$MESS["SP_RP_EDIT_DESCR"] = "Edytuj";
$MESS["SP_SALESCENTER_SALE_ACCESS_DENIED"] = "Odmowa dostępu.";
?>