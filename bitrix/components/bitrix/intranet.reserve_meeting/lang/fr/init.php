<?
$MESS["INAF_F_DESCRIPTION"] = "Description";
$MESS["INAF_F_FLOOR"] = "Etage";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Dénomination";
$MESS["INAF_F_PHONE"] = "Numéro de téléphone";
$MESS["INAF_F_PLACE"] = "Nombre de places";
$MESS["INTASK_C29_EVENT_LENGTH"] = "Dépensé";
$MESS["INTASK_C29_PERIOD_ADDITIONAL"] = "En supplément";
$MESS["INTASK_C29_PERIOD_COUNT"] = "Périodicité";
$MESS["INTASK_C29_PERIOD_TYPE"] = "Type de la période";
$MESS["INTASK_C29_UF_PERSONS"] = "Personnes";
$MESS["INTASK_C29_UF_PREPARE_ROOM"] = "Préparation de la chambre";
$MESS["INTASK_C29_UF_RES_TYPE"] = "Type de rencontre";
$MESS["INTASK_C29_UF_RES_TYPEA"] = "Conférence";
$MESS["INTASK_C29_UF_RES_TYPEB"] = "Présentation";
$MESS["INTASK_C29_UF_RES_TYPEC"] = "Négociations";
$MESS["INTASK_C29_UF_RES_TYPED"] = "Autre";
?>