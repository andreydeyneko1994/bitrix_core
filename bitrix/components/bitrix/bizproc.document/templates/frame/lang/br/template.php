<?php
$MESS["IBEL_BIZPROC_ACTIVE_WORKFLOWS"] = "Fluxos de trabalho ativos";
$MESS["IBEL_BIZPROC_ACTIVE_WORKFLOWS_EMPTY"] = "Atualmente não há fluxos de trabalho ativos";
$MESS["IBEL_BIZPROC_CANCEL"] = "Cancelar";
$MESS["IBEL_BIZPROC_COMPLETED_WORKFLOWS"] = "Fluxos de trabalho concluídos";
$MESS["IBEL_BIZPROC_COMPLETED_WORKFLOWS_EMPTY"] = "Não há nenhum fluxo de trabalho";
$MESS["IBEL_BIZPROC_COMPLETED_WORKFLOWS_SHOW"] = "Mostrar";
$MESS["IBEL_BIZPROC_COMPLETED_WORKFLOWS_SHOW_MORE"] = "Mostrar mais";
$MESS["IBEL_BIZPROC_SAVE"] = "Salvar";
