<?
$MESS["CRM_ORDER_SPLT_ADD_PRODUCT"] = "Agregar producto";
$MESS["CRM_ORDER_SPLT_ADD_STORE"] = "Agregar al almacén";
$MESS["CRM_ORDER_SPLT_ARE_YOU_SURE_YOU_WANT"] = "¿Usted está seguro que quiere eliminar este elemento?";
$MESS["CRM_ORDER_SPLT_BARCODE"] = "Código de barras";
$MESS["CRM_ORDER_SPLT_BARCODES"] = "Códigos de barras";
$MESS["CRM_ORDER_SPLT_CHOOSE"] = "Seleccionar";
$MESS["CRM_ORDER_SPLT_CHOOSE_STORE"] = "Seleccione el almacén";
$MESS["CRM_ORDER_SPLT_CLOSE"] = "Cerrar";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM"] = "Quitar elemento";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM_FROM_SHIPMENT"] = "Quitar el elemento del envío";
$MESS["CRM_ORDER_SPLT_DELETE_STORE"] = "Eliminar el envío del almacén";
$MESS["CRM_ORDER_SPLT_FIND_BY_BARCODE"] = "Encontrar por código de barras";
$MESS["CRM_ORDER_SPLT_ITEMS_HAVE_BARCODES"] = "Los artículos del paquete tienen códigos de barras";
$MESS["CRM_ORDER_SPLT_MARKING_CODE"] = "Código de marca";
$MESS["CRM_ORDER_SPLT_TO_REMOVE"] = "Quitar";
?>