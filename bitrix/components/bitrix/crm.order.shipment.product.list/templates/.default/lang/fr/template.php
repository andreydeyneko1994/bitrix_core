<?
$MESS["CRM_ORDER_SPLT_ADD_PRODUCT"] = "Ajouter un produit";
$MESS["CRM_ORDER_SPLT_ADD_STORE"] = "Ajouter un entrepôt";
$MESS["CRM_ORDER_SPLT_ARE_YOU_SURE_YOU_WANT"] = "Voulez-vous vraiment supprimer cet article ?";
$MESS["CRM_ORDER_SPLT_BARCODE"] = "Code-barres";
$MESS["CRM_ORDER_SPLT_BARCODES"] = "Codes-barres";
$MESS["CRM_ORDER_SPLT_CHOOSE"] = "Sélectionner";
$MESS["CRM_ORDER_SPLT_CHOOSE_STORE"] = "Sélectionnez un entrepôt";
$MESS["CRM_ORDER_SPLT_CLOSE"] = "Fermer";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM"] = "Supprimer l'article";
$MESS["CRM_ORDER_SPLT_DELETE_ITEM_FROM_SHIPMENT"] = "Supprimer l'article de la livraison";
$MESS["CRM_ORDER_SPLT_DELETE_STORE"] = "Supprimer la livraison de l'entrepôt";
$MESS["CRM_ORDER_SPLT_FIND_BY_BARCODE"] = "Rechercher par code-barres";
$MESS["CRM_ORDER_SPLT_ITEMS_HAVE_BARCODES"] = "Les marchandises de paquet contiennent des codes-barres";
$MESS["CRM_ORDER_SPLT_MARKING_CODE"] = "Code de marquage";
$MESS["CRM_ORDER_SPLT_TO_REMOVE"] = "Supprimer";
?>