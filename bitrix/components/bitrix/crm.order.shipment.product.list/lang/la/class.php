<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Moneda no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_ORDER_SPLC_AMOUNT"] = "Para entregar";
$MESS["CRM_ORDER_SPLC_DIMENSIONS"] = "Dimensiones";
$MESS["CRM_ORDER_SPLC_ERR_URL_BUILDER_ABSENT"] = "No se encontró el generador de enlaces.";
$MESS["CRM_ORDER_SPLC_FAILED_TO_CREATE_OBJECT"] = "No se pudo crear un objeto de envío.";
$MESS["CRM_ORDER_SPLC_FAILED_TO_CREATE_OBJECT_B"] = "No se pudo crear un objeto de carrito de compras.";
$MESS["CRM_ORDER_SPLC_NAME"] = "Nombre";
$MESS["CRM_ORDER_SPLC_PICTURE"] = "Imagen";
$MESS["CRM_ORDER_SPLC_PROPERTIES"] = "Propiedades";
$MESS["CRM_ORDER_SPLC_QUANTITY"] = "Cantidad";
$MESS["CRM_ORDER_SPLC_SORTING"] = "Clasificar";
$MESS["CRM_ORDER_SPLC_STORE_BARCODE"] = "Código de barras";
$MESS["CRM_ORDER_SPLC_STORE_ID"] = "Almacén";
$MESS["CRM_ORDER_SPLC_STORE_QUANTITY"] = "Envío";
$MESS["CRM_ORDER_SPLC_STORE_REMAINING_QUANTITY"] = "En stock";
$MESS["CRM_ORDER_SPLC_WEIGHT"] = "Peso";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
