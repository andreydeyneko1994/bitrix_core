<?
$MESS["CRM_ACTIVITY_CALL_VI_CALLBACK_CALL"] = "Retorno de Chamada";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_CALL"] = "Chamada recebida";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_REDIRECT_CALL"] = "Chamada recebida encaminhada";
$MESS["CRM_ACTIVITY_CALL_VI_OUTGOING_CALL"] = "Chamada realizada";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "O módulo Telefonia não está instalado.";
?>