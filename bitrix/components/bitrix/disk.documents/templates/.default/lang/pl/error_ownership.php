<?php
$MESS["DISK_DOCUMENTS_ERROR_1"] = "Nie jesteś właścicielem. Odmowa dostępu. ";
$MESS["DISK_DOCUMENTS_ERROR_2"] = "Aby uzyskać dodatkowe uprawnienia do pliku, skontaktuj się z właścicielem dokumentu.";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "Dokumenty";
