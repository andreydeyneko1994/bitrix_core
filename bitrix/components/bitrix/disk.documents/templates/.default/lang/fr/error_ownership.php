<?php
$MESS["DISK_DOCUMENTS_ERROR_1"] = "Vous n'êtes pas le propriétaire. Accès refusé. ";
$MESS["DISK_DOCUMENTS_ERROR_2"] = "Veuillez contacter le propriétaire du document pour obtenir des autorisations de fichiers supplémentaires.";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "Documents";
