<?
$MESS["SALESCENTER_CONNECT"] = "Conectar";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_DESCRIPTION"] = "Brinde asistencia a sus clientes chateando con ellos en su chat de red social preferida directamente desde su Bitrix24. Envíe preguntas frecuentes, páginas de detalles de productos y formularios mientras chatea.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_HOW_DESCRIPTION"] = "Haga clic en el botón \"Ventas por chat\" en la ventana de chat de Bitrix24 para enviar la página requerida al cliente.";
$MESS["SALESCENTER_CONNECT_BLOCK_CONSULTATIONS_TITLE"] = "Asistencia rápida";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_DESCRIPTION"] = "Reciba pagos y envíe enlaces a páginas de productos a cualquier chat de redes sociales o mensajería instantánea directamente desde su Bitrix24. Permita que sus clientes disfruten de pagos rápidos sin abandonar el chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_CHAT_TITLE"] = "Pagos por chat";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_DESCRIPTION"] = "Reciba pagos y envíe enlaces a páginas de productos mediante mensajes SMS. Haga que los pagos sean fácilmente accesibles para sus clientes.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_HOW_DESCRIPTION"] = "Para enviar información de la compañía o del pedido por mensaje SMS, use la página de detalles del elemento. Haga clic en el icono \"Pagos por SMS\" y seleccione la página que desea enviar. El enlace de la página se agregará al texto del mensaje SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_PAYMENTS_SMS_TITLE"] = "Pagos por SMS";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_DESCRIPTION"] = "Ofrezca a sus clientes una forma rápida y fácil de hacer una cita o registrarse para un evento, seleccionar el tiempo deseado y realizar el pago mediante chats de redes sociales o mensajería instantánea. Simplemente envíe un formulario de CRM desde su Bitrix24 al chat.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_HOW_DESCRIPTION"] = "Haga clic en el botón \"Ventas por chat\" en la ventana de chat de Bitrix24 para enviar un formulario de CRM al cliente. Esta opción está disponible tanto en la versión web, como en la aplicación de escritorio.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_CHAT_TITLE"] = "Citas por chat";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_DESCRIPTION"] = "Ofrezca a sus clientes una forma rápida y fácil de hacer una cita o registrarse para un evento mediante mensajes SMS. Simplemente envíe un formulario de CRM justo después de colgar.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_HOW_DESCRIPTION"] = "Para enviar el formulario de cita por mensaje SMS, use la página de detalles del cliente. Haga clic en el ícono \"Ventas por SMS\" y seleccione el formulario que desea enviar. El enlace del formulario se agregará al texto del mensaje SMS.";
$MESS["SALESCENTER_CONNECT_BLOCK_SERVICES_SMS_TITLE"] = "Citas por SMS";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW"] = "Cómo funciona";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_DESCRIPTION"] = "Trabajará con sus clientes utilizando el chat en vivo de Bitrix24. Puede enviar páginas y recibir pedidos en el navegador o en la aplicación de escritorio.";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK"] = "¿Cómo funcionan las ventas por chat?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_LINK_SMS"] = "Conocer más";
$MESS["SALESCENTER_CONNECT_TEMPLATE_HOW_SOCIAL"] = "¿Cómo conectar redes sociales y mensajeros a su Bitrix24?";
$MESS["SALESCENTER_CONNECT_TEMPLATE_TITLE"] = "Ventas habilitadas por chat";
?>