<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_NUMBER_NUMBER_WARNING"] = "Nombre initial '#NOMBRE#'incorrect pour la numérotation.";
$MESS["CRM_NUMBER_PREFIX_WARNING"] = "Le préfixe de numérotation '#PREFIX#' est invalide.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé.";
?>