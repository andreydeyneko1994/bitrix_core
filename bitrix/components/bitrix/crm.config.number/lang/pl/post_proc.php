<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_NUMBER_NUMBER_WARNING"] = "Początkowy numer zamówienia \"#NUMBER#\" jest nieprawidłowy.";
$MESS["CRM_NUMBER_PREFIX_WARNING"] = "Prefiks numeracji \"#PREFIX#\" jest nieprawidłowy.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu.";
?>