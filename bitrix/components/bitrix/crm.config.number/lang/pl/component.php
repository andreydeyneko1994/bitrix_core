<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_NUMBER_TEMPLATE_0"] = "Wyłączone";
$MESS["CRM_NUMBER_TEMPLATE_1"] = "Rozpocznij numerację od";
$MESS["CRM_NUMBER_TEMPLATE_2"] = "Użyj prefiksu";
$MESS["CRM_NUMBER_TEMPLATE_3"] = "Losowa liczba";
$MESS["CRM_NUMBER_TEMPLATE_4"] = "ID użytkownika i numer dokumentu";
$MESS["CRM_NUMBER_TEMPLATE_5"] = "Okresowo wznawiaj od początku";
$MESS["CRM_PERMISSION_DENIED"] = "Niedozwolone.";
?>