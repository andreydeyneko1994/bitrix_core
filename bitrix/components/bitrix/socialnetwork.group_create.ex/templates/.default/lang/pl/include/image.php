<?php
$MESS["SONET_GCE_T_IMAGE3"] = "Ikona grupy roboczej";
$MESS["SONET_GCE_T_IMAGE3_PROJECT"] = "Ikona projektu";
$MESS["SONET_GCE_T_IMAGE3_SCRUM"] = "Ikona zespołu Scrum";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć ten obraz?";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_NO"] = "Nie";
$MESS["SONET_GCE_T_IMAGE_DELETE_CONFIRM_YES"] = "Tak";
$MESS["SONET_GCE_T_UPLOAD"] = "Prześlij";
