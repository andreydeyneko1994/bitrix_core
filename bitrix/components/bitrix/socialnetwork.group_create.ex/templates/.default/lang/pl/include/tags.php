<?php
$MESS["SONET_GCE_T_KEYWORDS"] = "Słowa kluczowe";
$MESS["SONET_GCE_T_KEYWORDS_ADD_TAG"] = "Dodaj więcej";
$MESS["SONET_GCE_T_TAG_ADD"] = "Dodaj tag";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_FOOTER_LABEL"] = "Utwórz tag:";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_HINT"] = "Czy na pewno?";
$MESS["SONET_GCE_T_TAG_SEARCH_FAILED"] = "Ten tag nie istnieje";
