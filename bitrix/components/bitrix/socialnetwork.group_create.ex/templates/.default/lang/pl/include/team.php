<?php
$MESS["SONET_GCE_T_ADD_DEPT_HINT"] = "Wybranie działu spowoduje, że wszyscy pracownicy działu zostaną członkami grupy roboczej bez potwierdzenia.";
$MESS["SONET_GCE_T_ADD_DEPT_HINT_PROJECT"] = "Wybranie działu spowoduje, że wszyscy pracownicy działu zostaną członkami projektu bez potwierdzenia.";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Dodaj pracownika";
$MESS["SONET_GCE_T_ADD_OWNER2"] = "Przypisz";
$MESS["SONET_GCE_T_ADD_USER"] = "Dodaj użytkownika";
$MESS["SONET_GCE_T_DEST_LINK_2"] = "Dodaj więcej";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS"] = "Moderatorzy grupy roboczej";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_PROJECT"] = "Moderatorzy projektu";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER"] = "Właściciel grupy roboczej";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER_PROJECT"] = "Właściciel projektu";
$MESS["SONET_GCE_T_SCRUM_OWNER"] = "Właściciel produktu";
$MESS["SONET_GCE_T_TEAM_TITLE"] = "Członkowie zespołu";
$MESS["SONET_GCE_T_TEAM_TITLE_USER"] = "Zespół";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_PROJECT"] = "Zespół";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_SCRUM"] = "Interesariusze";
