<?php
$MESS["SONET_GCE_T_PARAMETERS_TITLE"] = "Parametry grupy roboczej";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_PROJECT"] = "Parametry projektu";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_SCRUM"] = "Parametry zespołu Scrum";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Rozszerzone parametry";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE"] = "O grupie roboczej";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_PROJECT"] = "O projekcie";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_SCRUM"] = "O zespole Scrum";
