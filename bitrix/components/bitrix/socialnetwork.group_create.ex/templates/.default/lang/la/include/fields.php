<?php
$MESS["SONET_GCE_T_PARAMETERS_TITLE"] = "Parámetros del grupo de trabajo";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_PROJECT"] = "Parámetros del proyecto";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_SCRUM"] = "Parámetros del equipo Scrum";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Parámetros extendidos";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE"] = "Acerca del grupo de trabajo";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_PROJECT"] = "Acerca del proyecto";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_SCRUM"] = "Acerca del equipo Scrum";
