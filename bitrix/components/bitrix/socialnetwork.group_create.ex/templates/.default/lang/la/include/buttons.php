<?php
$MESS["SONET_GCE_T_DO_CREATE"] = "Crear grupo";
$MESS["SONET_GCE_T_DO_CREATE_PROJECT"] = "Crear proyecto";
$MESS["SONET_GCE_T_DO_EDIT_2"] = "Cambiar";
$MESS["SONET_GCE_T_DO_INVITE"] = "Enviar invitación";
$MESS["SONET_GCE_T_DO_NEXT"] = "Siguiente";
$MESS["SONET_GCE_T_T_BACK"] = "Volver";
$MESS["SONET_GCE_T_T_CANCEL"] = "Cancelar";
