<?php
$MESS["SONET_GCE_T_ADD_DEPT_HINT"] = "Selecionar um departamento fará com que todos os funcionários do departamento sejam membros do grupo de trabalho sem confirmação.";
$MESS["SONET_GCE_T_ADD_DEPT_HINT_PROJECT"] = "Selecionar um departamento fará com que todos os funcionários do departamento sejam membros do projeto sem confirmação.";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Adicionar funcionário";
$MESS["SONET_GCE_T_ADD_OWNER2"] = "Atribuir";
$MESS["SONET_GCE_T_ADD_USER"] = "Adicionar usuário";
$MESS["SONET_GCE_T_DEST_LINK_2"] = "Adicionar mais";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS"] = "Moderadores do grupo de trabalho";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_PROJECT"] = "Moderadores do projeto";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER"] = "Proprietário do grupo de trabalho";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER_PROJECT"] = "Proprietário do projeto";
$MESS["SONET_GCE_T_SCRUM_OWNER"] = "Product owner";
$MESS["SONET_GCE_T_TEAM_TITLE"] = "Membros da equipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER"] = "Equipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_PROJECT"] = "Equipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_SCRUM"] = "Partes interessadas";
