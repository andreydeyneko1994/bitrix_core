<?php
$MESS["SONET_GCE_T_DESCRIPTION"] = "Descrição do grupo de trabalho";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL"] = "Adicionar descrição do grupo de trabalho";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_PROJECT"] = "Adicionar descrição do projeto para os membros da equipe";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_SCRUM"] = "Adicionar descrição do scrum para os membros da equipe";
$MESS["SONET_GCE_T_DESCRIPTION_PROJECT"] = "Descrição do projeto";
$MESS["SONET_GCE_T_DESCRIPTION_SCRUM"] = "Descrição da equipe Scrum";
$MESS["SONET_GCE_T_DESCRIPTION_SWITCHER"] = "adicionar descrição";
$MESS["SONET_GCE_T_NAME3"] = "Nome do grupo de trabalho";
$MESS["SONET_GCE_T_NAME3_PROJECT"] = "Nome do projeto";
$MESS["SONET_GCE_T_NAME3_SCRUM"] = "Nome da equipe Scrum";
