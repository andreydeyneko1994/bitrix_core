<?php
$MESS["SONET_GCE_T_KEYWORDS"] = "Palavras-chave";
$MESS["SONET_GCE_T_KEYWORDS_ADD_TAG"] = "Adicionar mais";
$MESS["SONET_GCE_T_TAG_ADD"] = "Adicionar marcador";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_FOOTER_LABEL"] = "Criar marcador:";
$MESS["SONET_GCE_T_TAG_SEARCH_ADD_HINT"] = "Tem certeza?";
$MESS["SONET_GCE_T_TAG_SEARCH_FAILED"] = "Este marcador não existe";
