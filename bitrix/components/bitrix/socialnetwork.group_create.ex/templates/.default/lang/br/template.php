<?php
$MESS["SONET_GCE_T_AJAX_ERROR"] = "Erro ao processar os dados do formulário. Tente novamente.";
$MESS["SONET_GCE_T_DEST_EXTRANET"] = "Extranet";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_EMAIL_TITLE"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_LAST_NAME_TITLE"] = "Sobrenome";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_NAME_TITLE"] = "Nome";
$MESS["SONET_GCE_T_DEST_EXTRANET_ADD_SEND_PASSWORD_TITLE"] = "Enviar informações de log in para um e-mail específico";
$MESS["SONET_GCE_T_DEST_EXTRANET_EMAIL_SHORT"] = "E-mail";
$MESS["SONET_GCE_T_DEST_EXTRANET_INVITE_MESSAGE_TITLE"] = "Esta é a mensagem que as pessoas convidadas receberão:";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR"] = "e/ou #ACTION# uma nova";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_ADD"] = "adicionar";
$MESS["SONET_GCE_T_DEST_EXTRANET_SELECTOR_INVITE"] = "convidar";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_SCRUM_PROJECT"] = "Equipe SCRUM";
$MESS["SONET_GCE_T_STRING_FIELD_ERROR"] = "O campo é obrigatório.";
$MESS["SONET_GCE_T_SUCCESS_CREATE"] = "O grupo foi criado com sucesso";
$MESS["SONET_GCE_T_SUCCESS_EDIT"] = "Os parâmetros do grupo foram alterados com sucesso";
$MESS["SONET_GCE_T_WIZARD_DESCRIPTION"] = "Junte-se a outros usuários e use as ferramentas disponíveis para obter o máximo de resultados.";
$MESS["SONET_GCE_T_WIZARD_TITLE"] = "Gerencie sua equipe";
