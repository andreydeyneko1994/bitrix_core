<?php
$MESS["SONET_GCE_T_DO_CREATE"] = "Criar grupo";
$MESS["SONET_GCE_T_DO_CREATE_PROJECT"] = "Criar Projeto";
$MESS["SONET_GCE_T_DO_EDIT_2"] = "Alterar";
$MESS["SONET_GCE_T_DO_INVITE"] = "Enviar convite";
$MESS["SONET_GCE_T_DO_NEXT"] = "Próximo";
$MESS["SONET_GCE_T_T_BACK"] = "Voltar";
$MESS["SONET_GCE_T_T_CANCEL"] = "Cancelar";
