<?php
$MESS["SONET_GCE_T_PARAMETERS_TITLE"] = "Parâmetros do grupo de trabalho";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_PROJECT"] = "Parâmetros do projeto";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_SCRUM"] = "Parâmetros da equipe Scrum";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Parâmetros ampliados";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE"] = "Sobre o grupo de trabalho";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_PROJECT"] = "Sobre o projeto";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_SCRUM"] = "Sobre a equipe Scrum";
