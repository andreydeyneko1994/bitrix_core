<?php
$MESS["SONET_GCE_T_ADD_DEPT_HINT"] = "Sélectionner un service permettra à tous les employés de ce service de rejoindre le groupe de travail sans avoir besoin de confirmation.";
$MESS["SONET_GCE_T_ADD_DEPT_HINT_PROJECT"] = "Sélectionner un service permettra à tous les employés de ce service de rejoindre le projet sans avoir besoin de confirmation.";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Ajouter un employé";
$MESS["SONET_GCE_T_ADD_OWNER2"] = "Affecter";
$MESS["SONET_GCE_T_ADD_USER"] = "Ajouter un utilisateur";
$MESS["SONET_GCE_T_DEST_LINK_2"] = "Ajouter plus";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS"] = "Modérateurs du groupe de travail";
$MESS["SONET_GCE_T_DEST_TITLE_MODERATORS_PROJECT"] = "Modérateurs du projet";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER"] = "Propriétaire du groupe de travail";
$MESS["SONET_GCE_T_DEST_TITLE_OWNER_PROJECT"] = "Propriétaire du projet";
$MESS["SONET_GCE_T_SCRUM_OWNER"] = "Propriétaire du produit";
$MESS["SONET_GCE_T_TEAM_TITLE"] = "Membres de l'équipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER"] = "Équipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_PROJECT"] = "Équipe";
$MESS["SONET_GCE_T_TEAM_TITLE_USER_SCRUM"] = "Parties prenantes";
