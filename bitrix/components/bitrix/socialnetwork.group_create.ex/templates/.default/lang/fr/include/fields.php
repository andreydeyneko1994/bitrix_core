<?php
$MESS["SONET_GCE_T_PARAMETERS_TITLE"] = "Paramètres du groupe de travail";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_PROJECT"] = "Paramètres du projet";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_SCRUM"] = "Paramètres de l'équipe scrum";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Paramètres étendus";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE"] = "À propos du groupe de travail";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_PROJECT"] = "À propos du projet";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_SCRUM"] = "À propos de l'équipe Scrum";
