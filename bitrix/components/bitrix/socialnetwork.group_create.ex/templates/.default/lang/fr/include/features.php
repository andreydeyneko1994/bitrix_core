<?php
$MESS["SONET_GCE_TAB_2"] = "Possibilités";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION"] = "Outils du groupe de travail";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_PROJECT"] = "Outils du projet";
$MESS["SONET_GCE_T_FEATURES_DESCRIPTION_SCRUM"] = "Outils pour l'équipe scrum";
$MESS["SONET_GCE_T_FEATURES_HINT"] = "Sélectionnez les outils pour votre équipe. Vous pouvez toujours modifier votre sélection si nécessaire.";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "Paramètres étendus";
