<?php
$MESS["SONET_GCE_AJAX_DELETE_FILE_FAILED"] = "Impossible de supprimer le fichier";
$MESS["SONET_GCE_AJAX_ERROR_NO_FILE"] = "Le fichier n'est pas envoyé.";
$MESS["SONET_GCE_AJAX_ERROR_NO_IMAGE"] = "Le fichier n’est pas un fichier image";
$MESS["SONET_GCE_AJAX_ERROR_WRONG_WORKGROUP_ID"] = "Le groupe de travail/projet est introuvable";
$MESS["SONET_GCE_AJAX_SAVE_FILE_FAILED"] = "Impossible d'enregistrer le fichier.";
