<?php
$MESS["SONET_GCE_AJAX_DELETE_FILE_FAILED"] = "Nie można usunąć pliku";
$MESS["SONET_GCE_AJAX_ERROR_NO_FILE"] = "Plik nie został wysłany.";
$MESS["SONET_GCE_AJAX_ERROR_NO_IMAGE"] = "Plik nie jest plikiem obrazu";
$MESS["SONET_GCE_AJAX_ERROR_WRONG_WORKGROUP_ID"] = "Nie znaleziono grupy roboczej/projektu";
$MESS["SONET_GCE_AJAX_SAVE_FILE_FAILED"] = "Nie można zapisać pliku.";
