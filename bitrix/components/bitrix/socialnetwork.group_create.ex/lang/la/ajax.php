<?php
$MESS["SONET_GCE_AJAX_DELETE_FILE_FAILED"] = "No se puede borrar el archivo";
$MESS["SONET_GCE_AJAX_ERROR_NO_FILE"] = "No se envió el archivo.";
$MESS["SONET_GCE_AJAX_ERROR_NO_IMAGE"] = "El archivo no es un archivo de imagen";
$MESS["SONET_GCE_AJAX_ERROR_WRONG_WORKGROUP_ID"] = "No se encontró el grupo de trabajo/proyecto";
$MESS["SONET_GCE_AJAX_SAVE_FILE_FAILED"] = "No se puede guardar el archivo.";
