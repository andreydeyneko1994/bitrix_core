<?
$MESS["CRM_COLUMN_ASSIGNED_BY_ID"] = "Osoba odpowiedzialna";
$MESS["CRM_COLUMN_CREATED_BY"] = "Utworzony przez";
$MESS["CRM_COLUMN_CREATED_BY_ID"] = "Utworzony przez";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Data";
$MESS["CRM_COLUMN_ENTITY"] = "Pozycja CRM";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Tytuł";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Rodzaj";
$MESS["CRM_COLUMN_EVENT_DESC"] = "Opis";
$MESS["CRM_COLUMN_EVENT_NAME"] = "Typ wydarzenia";
$MESS["CRM_COLUMN_EVENT_TYPE"] = "Rodzaj";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Firma";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Deal";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_EVENT_DESC_AFTER"] = "Po…";
$MESS["CRM_EVENT_DESC_BEFORE"] = "Przed…";
$MESS["CRM_EVENT_DESC_MORE"] = "czytaj więcej";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Zmiany";
$MESS["CRM_EVENT_TYPE_SNS"] = "Wiadomość e-mail";
$MESS["CRM_EVENT_TYPE_USER"] = "Niestandardowe";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRESET_CREATE_MY"] = "Utworzone przeze mnie";
$MESS["CRM_PRESET_CREATE_TODAY"] = "Utworzone dzisiaj";
$MESS["CRM_PRESET_CREATE_YESTERDAY"] = "Utworzone wczoraj";
?>