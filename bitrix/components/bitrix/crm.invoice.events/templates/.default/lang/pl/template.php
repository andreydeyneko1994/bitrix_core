<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_EVENT_DELETE"] = "Usuń";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "Na pewno chcesz to usunąć?";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Usuń wydarzenie";
$MESS["CRM_EVENT_ENTITY_TYPE_COMPANY"] = "Firma";
$MESS["CRM_EVENT_ENTITY_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_EVENT_ENTITY_TYPE_DEAL"] = "Deal";
$MESS["CRM_EVENT_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_EVENT_TABLE_EMPTY"] = "Brak danych";
$MESS["CRM_EVENT_TABLE_FILES"] = "Pliki";
$MESS["CRM_EVENT_VIEW_ADD"] = "Dodaj wydarzenie";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Wydarzenie";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Pokaż/Ukryj filtr";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Filtr";
$MESS["CRM_IMPORT_EVENT"] = "Jeśli adres e-mail kontaktu jest określony w rekordzie CRM możesz automatycznie zapisać wszystkie związane z nim korespondencje e-mail, jako rekord Wydarzenia. Trzeba przekazać otrzymaną wiadomość na adres e-mail systemu <b>%EMAIL%</b>, a cały tekst, i załączone pliki zostaną dodane jako Wydarzenie dla tego Kontaktu.";
?>