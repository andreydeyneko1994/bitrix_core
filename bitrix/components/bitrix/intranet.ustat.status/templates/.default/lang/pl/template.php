<?
$MESS["INTRANET_USTAT_RATING_COMMON_TAB"] = "Ocena ogólna";
$MESS["INTRANET_USTAT_RATING_INVOLVE_TAB"] = "Nie powiązane";
$MESS["INTRANET_USTAT_RATING_LOADING"] = "Ładowanie…";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Obecny poziom aktywności firmy (złożony z aktywności wszystkich użytkowników w poprzednich godzinach).";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Obecne zaangażowanie użytkowników. To pokazuje procent wszystkich użytkowników którzy używali dzisiaj przynajmniej czterech różnych narzędzi w intranecie.";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "Ładowanie…";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Puls firmy";
?>