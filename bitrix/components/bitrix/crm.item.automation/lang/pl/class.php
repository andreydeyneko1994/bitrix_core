<?php
$MESS["CRM_ITEM_AUTOMATION_INVALID_ENTITY_TYPE"] = "Nieprawidłowy typ obiektu";
$MESS["CRM_ITEM_AUTOMATION_TITLE"] = "Reguły automatyzacji #ENTITY#";
$MESS["CRM_ITEM_AUTOMATION_TITLE_CATEGORY"] = "Reguły automatyzacji #ENTITY# (#CATEGORY#)";
$MESS["CRM_ITEM_AUTOMATION_WRONG_CATEGORY"] = "Kategoria jest nieprawidłowa";
