<?php
$MESS["CRM_ITEM_AUTOMATION_INVALID_ENTITY_TYPE"] = "Tipo de entidad inválido";
$MESS["CRM_ITEM_AUTOMATION_TITLE"] = "Reglas de automatización #ENTITY#";
$MESS["CRM_ITEM_AUTOMATION_TITLE_CATEGORY"] = "Reglas de automatización #ENTITY# (#CATEGORY#)";
$MESS["CRM_ITEM_AUTOMATION_WRONG_CATEGORY"] = "La categoría es inválida";
