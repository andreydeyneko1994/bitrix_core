<?php
$MESS["CRM_ITEM_AUTOMATION_INVALID_ENTITY_TYPE"] = "Type d'entité non valide";
$MESS["CRM_ITEM_AUTOMATION_TITLE"] = "Règles d'automatisation #ENTITY#";
$MESS["CRM_ITEM_AUTOMATION_TITLE_CATEGORY"] = "Règles d'automatisation #ENTITY# (#CATEGORY#)";
$MESS["CRM_ITEM_AUTOMATION_WRONG_CATEGORY"] = "Catégorie non valide";
