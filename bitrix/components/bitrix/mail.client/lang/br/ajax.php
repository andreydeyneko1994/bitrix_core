<?php
$MESS["MAIL_CLIENT_ACTIVITY_COMMUNICATIONS_EMPTY_ERROR"] = "Especifique os destinatários para salvar a mensagem de e-mail no CRM";
$MESS["MAIL_CLIENT_ACTIVITY_CREATE_ERROR"] = "Erro ao salvar mensagem de e-mail no CRM";
$MESS["MAIL_CLIENT_ACTIVITY_PERMISSION_DENIED_ERROR"] = "Permissões insuficientes para salvar mensagem de e-mail no CRM";
$MESS["MAIL_MESSAGE_BAD_SENDER"] = "Remetente inválido";
$MESS["MAIL_MESSAGE_EMPTY_RCPT"] = "Especificar destinatários";
$MESS["MAIL_MESSAGE_EMPTY_SENDER"] = "Especificar remetente";
$MESS["MAIL_MESSAGE_SEND_ERROR"] = "Não é possível enviar mensagem";
$MESS["MAIL_MESSAGE_TO_MANY_RECIPIENTS"] = "Você não pode adicionar mais de 10 destinatários";
