<?php
$MESS["MAIL_CLIENT_ACTIVITY_COMMUNICATIONS_EMPTY_ERROR"] = "Indiquez les destinataires pour enregistrer le message de l'e-mail dans le CRM";
$MESS["MAIL_CLIENT_ACTIVITY_CREATE_ERROR"] = "Erreur lors de l'enregistrement du message de l'e-mail dans le CRM";
$MESS["MAIL_CLIENT_ACTIVITY_PERMISSION_DENIED_ERROR"] = "Droits insuffisants pour enregistrer le message de l'e-mail dans le CRM";
$MESS["MAIL_MESSAGE_BAD_SENDER"] = "Expéditeur non valide";
$MESS["MAIL_MESSAGE_EMPTY_RCPT"] = "Préciser les destinataires";
$MESS["MAIL_MESSAGE_EMPTY_SENDER"] = "Spécifier l'expéditeur";
$MESS["MAIL_MESSAGE_SEND_ERROR"] = "Impossible d'envoyer le message";
$MESS["MAIL_MESSAGE_TO_MANY_RECIPIENTS"] = "Vous ne pouvez pas ajouter plus de 10 destinataires";
