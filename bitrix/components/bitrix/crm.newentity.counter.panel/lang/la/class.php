<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_NEW_ENT_COUNTER_COMPANY_CAPTION"] = "Nueva compañía";
$MESS["CRM_NEW_ENT_COUNTER_CONTACT_CAPTION"] = "Nuevo contacto";
$MESS["CRM_NEW_ENT_COUNTER_DEAL_CAPTION"] = "Nueva negociación";
$MESS["CRM_NEW_ENT_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "No se especifica el tipo de entidad.";
$MESS["CRM_NEW_ENT_COUNTER_LEAD_CAPTION"] = "Nuevo prospecto";
?>