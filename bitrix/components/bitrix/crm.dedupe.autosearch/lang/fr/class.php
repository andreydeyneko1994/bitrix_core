<?php
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_0"] = "Jamais";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_1"] = "Chaque jour";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_7"] = "Une fois par semaine";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_14"] = "Une fois toutes les deux semaines";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_30"] = "Une fois par mois";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_182"] = "Une fois tous les six mois";
