<?php
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_0"] = "Nunca";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_1"] = "Diariamente";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_7"] = "Uma vez por semana";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_14"] = "Uma vez a cada duas semanas";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_30"] = "Uma vez por mês";
$MESS["CRM_DP_AUTOSEARCH_INTERVAL_182"] = "Uma vez a cada seis meses";
