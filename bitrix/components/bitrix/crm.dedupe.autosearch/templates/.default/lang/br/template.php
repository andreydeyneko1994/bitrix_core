<?php
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_CONFLICTED_TEXT"] = "Você terá que revisar e mesclar manualmente #CONFLICTS_COUNT# duplicado(s).";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_BUTTON"] = "Revisar agora";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_LIST_LINK"] = "Encontrar duplicados";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT"] = "Duplicados mesclados automaticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_COMPANY"] = "Empresas duplicadas mescladas automaticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_CONTACT"] = "Contatos duplicados mesclados automaticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_LEAD"] = "Leads duplicados mesclados automaticamente: #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_CONFLICTS_FOUND"] = "#CONFLICTS_COUNT# duplicado(s) não foram mesclados.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_INTERVAL_TITLE"] = "Executar localizador de duplicados";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_NOTE"] = "O sistema tentará encontrar e mesclar duplicados de acordo com as opções selecionadas.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_TITLE"] = "Encontrar duplicados automaticamente";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_BUTTON"] = "Ativar";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT"] = "Você gostaria de ativar a função de localização automática e mescla de duplicados? Ela será executada com a freqüência que você especificar.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_COMPANY"] = "Encontramos #TOTAL_ENTITIES_COUNT# empresas duplicadas em seu CRM.<br>#FOUND_ITEMS_COUNT# delas são uma correspondência exata em todos os campos que você selecionou para olhar.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_CONTACT"] = "Encontramos #TOTAL_ENTITIES_COUNT# contatos duplicados em seu CRM.<br>#FOUND_ITEMS_COUNT# deles são uma correspondência exata em todos os campos que você selecionou para olhar.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_LEAD"] = "Encontramos #TOTAL_ENTITIES_COUNT# leads duplicados em seu CRM.<br>#FOUND_ITEMS_COUNT# deles são uma correspondência exata em todos os campos que você selecionou para olhar.";
