<?php
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_CONFLICTED_TEXT"] = "Vous devrez examiner et fusionner manuellement #CONFLICTS_COUNT# doublon(s).";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_BUTTON"] = "Examiner maintenant";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_RESOLVE_CONFLICT_LIST_LINK"] = "Doublons trouvés";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT"] = "Doublons automatiquement fusionnés : #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_COMPANY"] = "Doublons d'entreprises automatiquement fusionnés : #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_CONTACT"] = "Doublons de contacts automatiquement fusionnés : #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_COMPLETE_TEXT_LEAD"] = "Doublons de prospects automatiquement fusionnés : #FOUND_ITEMS_COUNT#";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_CONFLICTS_FOUND"] = "#CONFLICTS_COUNT# doublon(s) n'ont pas été fusionnés.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_INTERVAL_TITLE"] = "Exécuter le chercheur de doublons";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_NOTE"] = "Le système tentera de trouver et de fusionner les doublons en fonction des options sélectionnées.";
$MESS["CRM_DP_AUTOSEARCH_SETTINGS_TITLE"] = "Recherche automatique des doublons";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_BUTTON"] = "Activer";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT"] = "Voulez-vous activer la fonction de recherche et de fusion automatique des doublons ? Elle s'exécutera aussi souvent que vous le spécifiez.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_COMPANY"] = "Nous avons trouvé #TOTAL_ENTITIES_COUNT# doublons d'entreprises dans votre CRM.<br>#FOUND_ITEMS_COUNT# d'entre eux sont une correspondance exacte à travers tous les champs que vous avez sélectionné pour la recherche.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_CONTACT"] = "Nous avons trouvé #TOTAL_ENTITIES_COUNT# doublons de contacts dans votre CRM.<br>#FOUND_ITEMS_COUNT# d'entre eux sont une correspondance exacte à travers tous les champs que vous avez sélectionné pour la recherche.";
$MESS["CRM_DP_AUTOSEARCH_START_CONFIRMATION_TEXT_LEAD"] = "Nous avons trouvé #TOTAL_ENTITIES_COUNT# doublons de prospects dans votre CRM.<br>#FOUND_ITEMS_COUNT# d'entre eux sont une correspondance exacte à travers tous les champs que vous avez sélectionné pour la recherche.";
