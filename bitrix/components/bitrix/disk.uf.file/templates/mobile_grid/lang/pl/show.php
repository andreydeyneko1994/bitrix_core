<?php
$MESS["DISK_UF_FILE_DISABLE_AUTO_COMMENT"] = "Wyłącz automatyczne komentarze";
$MESS["DISK_UF_FILE_ENABLE_AUTO_COMMENT"] = "Włącz automatyczne komentarze";
$MESS["DISK_UF_FILE_IS_DELETED"] = "(usunięto)";
$MESS["DISK_UF_FILE_MOBILE_GRID_FILES_MORE_LINK"] = "Więcej plików: #NUM#";
$MESS["DISK_UF_FILE_MOBILE_GRID_TOGGLE_VIEW_GALLERY"] = "Pokaż jako galerię";
$MESS["DISK_UF_FILE_RUN_FILE_IMPORT"] = "Pobierz najnowszą wersję";
$MESS["DISK_UF_FILE_SETTINGS_DOCS"] = "Ustawienia do pracy z dokumentami";
$MESS["DISK_UF_FILE_STATUS_FAIL_LOADING"] = "Błąd podczas ładowania najnowszej wersji.";
$MESS["DISK_UF_FILE_STATUS_HAS_LAST_VERSION"] = "Posiadasz najnowszą wersję.";
$MESS["DISK_UF_FILE_STATUS_PROCESS_LOADING"] = "Ładowanie";
$MESS["DISK_UF_FILE_STATUS_SUCCESS_LOADING"] = "Najnowsza wersja została załadowana.";
$MESS["WDUF_FILES"] = "Pliki:";
$MESS["WDUF_FILE_EDIT"] = "Edytuj";
$MESS["WDUF_MORE_ACTIONS"] = "Więcej";
$MESS["WDUF_PHOTO"] = "Zdjęcie:";
