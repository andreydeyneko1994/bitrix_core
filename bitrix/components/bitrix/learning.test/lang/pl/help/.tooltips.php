<?
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["GRADEBOOK_TEMPLATE_TIP"] = "Ścieżka do strony zawierającej wyniki testu (np. dzienniczek ocen).";
$MESS["PAGE_NUMBER_VARIABLE_TIP"] = "Nazwa zmiennej dla ID pytań testowych.";
$MESS["PAGE_WINDOW_TIP"] = "Liczba pytań do wyświetlenia w łańcuchu nawigacji.";
$MESS["SET_TITLE_TIP"] = "Sprawdzenie tej opcji wstawi tytuł strony do nazwy użytkownika.";
$MESS["SHOW_TIME_LIMIT_TIP"] = "Jeżeli test nakłada ograniczenia czasowe, włączenie tej opcji spowoduje wyświetlenie licznika czasu.";
$MESS["TEST_ID_TIP"] = "Wybierz jeden z istniejących testów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID testu w polu obok.";
?>