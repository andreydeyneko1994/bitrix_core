<?
$MESS["SALESCENTER_ORDERS_ADD_ORDER"] = "Nouvelle commande";
$MESS["SALESCENTER_ORDERS_LIMITS_MESSAGE"] = "Veuillez passer à une offre commerciale pour continuer à gérer vos commandes et à recevoir vos paiements via l'Espace des ventes.";
$MESS["SALESCENTER_ORDERS_LIMITS_TITLE"] = "Vous avez atteint le nombre maximum de commandes que vous pouvez créer via l'Espace des ventes.";
$MESS["SALESCENTER_SESSION_ERROR"] = "L'ID de la session n'est pas indiqué";
?>