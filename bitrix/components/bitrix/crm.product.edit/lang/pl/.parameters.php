<?
$MESS["CRM_CATALOG_ID"] = "Katalog produktów";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[nie wybrano]";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Szablon ścieżki strony edycji produktu";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Szablon ścieżki strony produktu";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Szablon ścieżki strony widoku produktu";
$MESS["CRM_PRODUCT_ID"] = "ID Produktu";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nazwa zmiennej ID produktu";
?>