<?
$MESS["TASKS_ADD_SUBTASK"] = "Criar subtarefa";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Adicionar ao plano de dia de trabalho";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Adicionar ao Plano Diário";
$MESS["TASKS_COPY_TASK"] = "Copiar";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicar tarefa";
$MESS["TASKS_DEFER_TASK"] = "Adiar";
$MESS["TASKS_DELEGATE_TASK"] = "Delegar";
$MESS["TASKS_DELETE_CONFIRM"] = "Confirmar a exclusão?";
$MESS["TASKS_DELETE_TASK"] = "Excluir";
$MESS["TASKS_RENEW_TASK"] = "Retornar";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Você já está usando o controlador de hora para \"{{TITLE}}\". Esta tarefa será pausada. Continuar?";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "O gerenciador de tempo está sendo usado agora com outra tarefa.";
$MESS["TASKS_UNKNOWN"] = "Desconhecido";
?>