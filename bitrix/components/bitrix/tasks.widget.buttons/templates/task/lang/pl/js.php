<?
$MESS["TASKS_ADD_SUBTASK"] = "Stwórz podzadanie";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Dodaj do codziennego planu";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Dodaj do codziennego planu";
$MESS["TASKS_COPY_TASK"] = "Kopiuj";
$MESS["TASKS_COPY_TASK_EX"] = "Zduplikuj zadanie";
$MESS["TASKS_DEFER_TASK"] = "Odrocz";
$MESS["TASKS_DELEGATE_TASK"] = "Deleguj";
$MESS["TASKS_DELETE_CONFIRM"] = "Potwierdzić usunięcie?";
$MESS["TASKS_DELETE_TASK"] = "Usuń";
$MESS["TASKS_RENEW_TASK"] = "Wznów";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Już wykorzystujesz śledzenie czasu dla \"{{TITLE}}\". To zadanie zostanie zatrzymane. Kontynuować?";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Śledzenie czasu jest teraz wykorzystywane przy innym zadaniu.";
$MESS["TASKS_UNKNOWN"] = "Nieznane";
?>