<?
$MESS["TASKS_TTDP_LICENSE_BODY"] = "Odnośniki do zadań są dostępne w planach rozszerzonych.";
$MESS["TASKS_TTDP_LICENSE_TITLE"] = "Tworzenie odnośników do Zadań";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_ADD"] = "Dodaj poprzednie zadanie";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_COL_RELATED"] = "Poprzednie zadania";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_COL_REL_TYPE"] = "Dzianie";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_CURRENT_TASK"] = "Bieżące zadanie";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_DELETE"] = "Usuń";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN"] = "kiedy";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN_END"] = "zakończy się";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN_START"] = "rozpocznie się";
?>