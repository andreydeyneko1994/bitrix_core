<?
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD"] = "Dodaj przypomnienie";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD_ALT"] = "Dodaj";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD_REMARK"] = "przez komunikator lub e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_DAYS"] = "dni";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_DELETE"] = "Usuń";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_HOURS"] = "godziny";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_NO_DEADLINE"] = "Aby ustawić przypomnienie o terminie ostatecznym, określ termin ostateczny.";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_O"] = "do twórcy";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_O_EX"] = "Wiadomość zostanie wysłana do aktualnego twórcy";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_R"] = "do osoby odpowiedzialnej";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_R_EX"] = "Wiadomość zostanie wysłana do aktualnej osoby odpowiedzialnej";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_S"] = "do siebie";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_S_EX"] = "Wiadomość zostanie wysłana do aktualnego użytkownika";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_REMIND_BY"] = "przypomnienie używa";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_CREATOR"] = "do twórcy";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_RESPONSIBLE"] = "do osoby odpowiedzialnej";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_SELF"] = "do siebie";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_E"] = "przez e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_E_EX"] = "Wyślij wiadomość e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_J"] = "przez komunikator";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_J_EX"] = "Wyślij powiadomienie";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_A"] = "data";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_A_EX"] = "Wiadomość zostanie wysłana w określonym dniu";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_D"] = "termin ostateczny";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_D_EX"] = "Wiadomość zostanie wysłana w określonym czasie przed teminem ostatecznym";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_UPDATE"] = "Aktualizacja";
?>