<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Plantillas de detalles de la compañía o contacto incorrecto";
$MESS["CRM_REQUISITE_ENTITY_TYPE_INVALID"] = "Contacto o tipo de entidad de la compañía incorrecta";
$MESS["CRM_REQUISITE_FORM_ID_INVALID"] = "Contacto o ID de formulario de detalles de la compañía incorrecto";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Plantilla sin título";
?>