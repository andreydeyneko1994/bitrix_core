<?
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "Wystąpił błąd.";
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Sukces";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Brak dostępnych szablonów";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Brak dostępnych szablonów";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Dane firmy";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Dane kontaktu";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Wybierz nowy";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Dodaj dane firmy używając szablonu";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Dodaj dane kontaktu używając szablonu";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Twoje wyszukiwanie nie zwróciło wyników,";
?>