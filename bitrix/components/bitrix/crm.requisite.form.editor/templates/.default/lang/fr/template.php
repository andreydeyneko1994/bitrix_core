<?
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "C'est une erreur.";
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Réussi";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Il n'existe pas de modèles disponibles.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Il n'existe pas de modèles disponibles.";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Mentions de la société";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Mentions de contact";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Ajouter un nouveau";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Ajouter des mentions de la société à l'aide d'un modèle";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Ajouter des mentions de contact à l'aide d'un modèle";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Votre recherche n'a donné aucun résultat.";
?>