<?php
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_CSRF_ERROR"] = "ID de session non valide";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_MODULE_ERROR"] = "Tous les modules nécessaires n'ont pas été trouvés";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_PROCESSED_NO_PDF_ERROR"] = "Erreur lors de la conversion du document en PDF";
$MESS["CRM_DOCUMENT_VIEW_COMPONENT_SMS_TEXT"] = "Utilisez le lien #LINK# pour télécharger #TITLE#";
