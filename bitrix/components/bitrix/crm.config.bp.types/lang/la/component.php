<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Proceso de negocio no está instalado.";
$MESS["CRM_BP_COMPANY"] = "Compañía";
$MESS["CRM_BP_COMPANY_DESC"] = "Plantillas de business process para los \"Contactos\"";
$MESS["CRM_BP_CONTACT"] = "Contacto";
$MESS["CRM_BP_CONTACT_DESC"] = "Plantillas de business process para los \"Contactos\"";
$MESS["CRM_BP_DEAL"] = "Negociación";
$MESS["CRM_BP_DEAL_DESC"] = "Plantillas del business process de la negociación";
$MESS["CRM_BP_DYNAMIC_DESC"] = "#DYNAMIC_TYPE_NAME# plantillas para el flujo de trabajo";
$MESS["CRM_BP_ENTITY_LIST"] = "Lista";
$MESS["CRM_BP_LEAD"] = "Prospecto";
$MESS["CRM_BP_LEAD_DESC"] = "Plantillas del business process de prospectos";
$MESS["CRM_BP_QUOTE_DESC"] = "Plantillas para el flujo de trabajo de las cotizaciones";
$MESS["CRM_BP_SMART_INVOICE_DESC"] = "Plantillas del flujo de trabajo de las facturas";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
