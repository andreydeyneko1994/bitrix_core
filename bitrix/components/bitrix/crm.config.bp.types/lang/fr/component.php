<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module De processus d'entreprise n'a pas été installé.";
$MESS["CRM_BP_COMPANY"] = "Entreprise";
$MESS["CRM_BP_COMPANY_DESC"] = "Modèles de procédures d'entreprise pour 'Entreprises'";
$MESS["CRM_BP_CONTACT"] = "Client";
$MESS["CRM_BP_CONTACT_DESC"] = "Modèles de processus d'entreprise pour 'Contacts'";
$MESS["CRM_BP_DEAL"] = "Transaction";
$MESS["CRM_BP_DEAL_DESC"] = "Modèles des processus d'entreprise pour les 'Transactions'";
$MESS["CRM_BP_DYNAMIC_DESC"] = "Modèles de flux de travail #DYNAMIC_TYPE_NAME#";
$MESS["CRM_BP_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_BP_LEAD_DESC"] = "Modèles des processus d'entreprise pour les 'Prospects'";
$MESS["CRM_BP_QUOTE_DESC"] = "Modèles de flux de travail pour les devis";
$MESS["CRM_BP_SMART_INVOICE_DESC"] = "Modèles de flux de travail pour les factures";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
