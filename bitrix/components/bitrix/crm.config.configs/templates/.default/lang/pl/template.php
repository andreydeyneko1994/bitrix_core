<?php
$MESS["CRM_TAB_ACTIVITY_CONFIG"] = "Działania";
$MESS["CRM_TAB_ACTIVITY_CONFIG_TITLE"] = "Parametry działań";
$MESS["CRM_TAB_ACTIVITY_CONFIG_TITLE2"] = "Ustawienia aktywności";
$MESS["CRM_TAB_CONFIG"] = "Wysyłanie e-mail";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Konfiguruj parametry wysyłania e-mail";
$MESS["CRM_TAB_DEAL_CONFIG"] = "Deale";
$MESS["CRM_TAB_DEAL_CONFIG_TITLE"] = "Parametry deala";
$MESS["CRM_TAB_DUPLICATE_CONTROL"] = "Kontrola duplikatów";
$MESS["CRM_TAB_DUPLICATE_CONTROL_TITLE"] = "Parametry kontroli duplikatów";
$MESS["CRM_TAB_FORMAT"] = "Format";
$MESS["CRM_TAB_FORMAT_TITLE"] = "Opcje formatowania";
$MESS["CRM_TAB_GENERAL"] = "Ogólne";
$MESS["CRM_TAB_GENERAL_TITLE"] = "Ogólne parametry";
$MESS["CRM_TAB_HISTORY"] = "Historia";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Ustawienia historii";
$MESS["CRM_TAB_LIVEFEED"] = "Aktualności";
$MESS["CRM_TAB_LIVEFEED2"] = "Aktualności";
$MESS["CRM_TAB_LIVEFEED_TITLE"] = "Ustawienia Aktualności";
$MESS["CRM_TAB_LIVEFEED_TITLE2"] = "Ustawienia Aktualności";
$MESS["CRM_TAB_RECYCLE_BIN_CONFIG"] = "Kosz";
$MESS["CRM_TAB_RECYCLE_BIN_CONFIG_TITLE"] = "Ustawienia Kosza";
$MESS["CRM_TAB_REST_2"] = "Market";
$MESS["CRM_TAB_REST_TITLE"] = "Ustawienia aplikacji";
$MESS["CRM_TAB_STATUS_CONFIG"] = "Listy wyboru";
$MESS["CRM_TAB_STATUS_CONFIG_TITLE"] = "Ustawienia list wyboru";
