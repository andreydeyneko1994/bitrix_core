<?
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_PAGETITLE"] = "Mide el nivel de estrés";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1"] = "Instalar o abrir la aplicación móvil Bitrix24";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE"] = "App Store";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_APPLE_URL"] = "https://itunes.apple.com/app/bitrix24/id561683423";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE"] = "Google Play";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_GOOGLE_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_BUTTON"] = "enviar";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP1_SENDSMS_HINT"] = "Enviar el enlace a mi móvil";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP2"] = "Haga clic en la opción &laquo;Medir su nivel de estrés&raquo; que se encuentra a continuación o seleccione el elemento del menú &laquo;Nivel de estrés&raquo;";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_STEP3"] = "Haga clic en la opción &laquo;Medir ahora&raquo; y siga las instrucciones";
?>