<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Usuń szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_CONFIRM"] = "Jesteś pewien, że chcesz usunąć \"%s\"?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Usuń ten szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_EDIT"] = "Edytuj szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_EDIT_TITLE"] = "Edytuj ten szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_NEED_FOR_CONVERTING"] = "W systemie są wbudowane szablony email. Możesz <a href=\"#URL_EXECUTE_CONVERTING#\">dodać je do swojej listy</a> lub <a href=\"#URL_SKIP_CONVERTING#\">pominąć</a>.";
?>