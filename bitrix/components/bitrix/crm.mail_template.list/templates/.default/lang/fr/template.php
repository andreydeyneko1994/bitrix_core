<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_CONFIRM"] = "Êtes-vous sûr de vouloir supprimer '% s' ?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Supprimer ce modèle d'e-mail";
$MESS["CRM_MAIL_TEMPLATE_EDIT"] = "Modifier le modèle d'e-mail";
$MESS["CRM_MAIL_TEMPLATE_EDIT_TITLE"] = "Passer sur la page d'édition de ce modèle postal";
$MESS["CRM_MAIL_TEMPLATE_NEED_FOR_CONVERTING"] = "Les paramètres de l'envoi des lettres sont trouvés aux ajustages. Vous pouvez <a href='#URL_EXECUTE_CONVERTING#'>les ajouter sur la liste</a> ou <a href='#URL_SKIP_CONVERTING#'>ignorer</a>.";
?>