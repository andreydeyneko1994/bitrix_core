<?php
$MESS["CRM_COMPANY_DELETION_ERROR"] = "Erreur lors de la suppression de la société.";
$MESS["CRM_COMPANY_NOT_FOUND"] = "Société introuvable.";
