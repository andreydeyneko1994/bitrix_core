<?php
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_GENERAL"] = "Toutes les étapes et entonnoirs";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_JUNK"] = "Perdu";
$MESS["CRM_COMPANY_DETAIL_ATTR_GR_TYPE_PIPELINE"] = "En cours + gagnées";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL"] = "Obligatoire à partir de l'étape";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_FULL_1"] = "Requis à l'étape";
$MESS["CRM_COMPANY_DETAIL_ATTR_REQUIRED_SHORT"] = "Oblig.";
$MESS["CRM_COMPANY_DETAIL_HISTORY_STUB"] = "Vous ajoutez maintenant une société...";
