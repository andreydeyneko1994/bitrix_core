<?
$MESS["CRM_INVOICE_CUSTOM_SAVE_BUTTON_TITLE"] = "Zapisz i wróć";
$MESS["CRM_INVOICE_PS_PROPS_CONTENT"] = "Aby rozpocząć pracę z fakturami, wprowadź informacje o firmie, które będą wykorzystywane w fakturze.";
$MESS["CRM_INVOICE_PS_PROPS_GOTO"] = "Dodaj dane firmy";
$MESS["CRM_INVOICE_PS_PROPS_TITLE"] = "Szczegóły Firmy";
$MESS["CRM_INVOICE_RECUR_SHOW_TITLE"] = "Faktura cykliczna ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_INVOICE_SHOW_LEGEND"] = "faktura ##ACCOUNT_NUMBER#";
$MESS["CRM_INVOICE_SHOW_NEW_TITLE"] = "Nowa faktura";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Faktura ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_TAB_1"] = "Faktura";
$MESS["CRM_TAB_1_TITLE"] = "Parametry faktury";
?>