<?
$MESS["CRM_INVOICE_CUSTOM_SAVE_BUTTON_TITLE"] = "Salvar e Retornar";
$MESS["CRM_INVOICE_PS_PROPS_CONTENT"] = "Para começar a trabalhar com faturas, forneça informações sobre a empresa que serão utilizadas nas faturas.";
$MESS["CRM_INVOICE_PS_PROPS_GOTO"] = "Adicionar detalhes da empresa";
$MESS["CRM_INVOICE_PS_PROPS_TITLE"] = "Detalhes da empresa";
$MESS["CRM_INVOICE_RECUR_SHOW_TITLE"] = "Fatura recorrente ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_INVOICE_SHOW_LEGEND"] = "fatura ##ACCOUNT_NUMBER#";
$MESS["CRM_INVOICE_SHOW_NEW_TITLE"] = "Nova fatura";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Fatura ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_TAB_1"] = "Fatura";
$MESS["CRM_TAB_1_TITLE"] = "Parâmetros da fatura";
?>