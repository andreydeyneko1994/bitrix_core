<?
$MESS["CRM_COLUMN_ALLOW_DELIVERY"] = "Dopuszczona";
$MESS["CRM_COLUMN_BASE_PRICE_DELIVERY"] = "Cena podstawowa";
$MESS["CRM_COLUMN_COMMENTS"] = "Komentarz przedstawiciela handlowego";
$MESS["CRM_COLUMN_CURRENCY"] = "Waluta";
$MESS["CRM_COLUMN_CUSTOM_PRICE_DELIVERY"] = "Cena niestandardowa";
$MESS["CRM_COLUMN_DATE_ALLOW_DELIVERY"] = "Dostawa zatwierdzona";
$MESS["CRM_COLUMN_DATE_DEDUCTED"] = "Wysłano";
$MESS["CRM_COLUMN_DATE_INSERT"] = "Utworzono";
$MESS["CRM_COLUMN_DATE_MARKED"] = "Problem wystąpił";
$MESS["CRM_COLUMN_DATE_RESPONSIBLE_ID"] = "Osoba odpowiedzialna zmieniona przez";
$MESS["CRM_COLUMN_DEDUCTED"] = "Wysłano";
$MESS["CRM_COLUMN_DELIVERY_DOC_DATE"] = "Data dokumentu przewozowego";
$MESS["CRM_COLUMN_DELIVERY_DOC_NUM"] = "Dokument przewozowy #";
$MESS["CRM_COLUMN_DELIVERY_SERVICE"] = "Usługa dostawy";
$MESS["CRM_COLUMN_DISCOUNT_PRICE"] = "Upust";
$MESS["CRM_COLUMN_EMP_ALLOW_DELIVERY"] = "Dostawa zatwierdzona przez";
$MESS["CRM_COLUMN_EMP_DEDUCTED"] = "Status wysłania zmieniony przez";
$MESS["CRM_COLUMN_EMP_MARKED"] = "Oznaczone jako problem przez";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_MARKED"] = "Problem";
$MESS["CRM_COLUMN_ORDER_ID"] = "ID zamówienia";
$MESS["CRM_COLUMN_PRICE_DELIVERY_CURRENCY"] = "Cena";
$MESS["CRM_COLUMN_REASON_MARKED"] = "Opis problemu";
$MESS["CRM_COLUMN_REASON_UNDO_DEDUCTED"] = "Powód anulowania wysyłki";
$MESS["CRM_COLUMN_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_COLUMN_SHIPMENT_SUMMARY"] = "Dokument przewozowy";
$MESS["CRM_COLUMN_STATUS_ID"] = "Status";
$MESS["CRM_COLUMN_TRACKING_DESCRIPTION"] = "Opis statusu wysyłki";
$MESS["CRM_COLUMN_TRACKING_NUMBER"] = "Numer śledzenia";
$MESS["CRM_COLUMN_TRACKING_STATUS"] = "Status wysyłki";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SHIPMENT_DELETE_ERROR"] = "Błąd usuwania wysyłki";
$MESS["CRM_SHIPMENT_SUMMARY"] = "Wysyłka ##NUMBER# - #DATE#";
?>