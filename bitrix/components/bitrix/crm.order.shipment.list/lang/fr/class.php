<?
$MESS["CRM_COLUMN_ALLOW_DELIVERY"] = "Autorisé";
$MESS["CRM_COLUMN_BASE_PRICE_DELIVERY"] = "Prix de base";
$MESS["CRM_COLUMN_COMMENTS"] = "Commentaire du représentant des ventes";
$MESS["CRM_COLUMN_CURRENCY"] = "Devise";
$MESS["CRM_COLUMN_CUSTOM_PRICE_DELIVERY"] = "Prix personnalisé";
$MESS["CRM_COLUMN_DATE_ALLOW_DELIVERY"] = "Date d'autorisation de la livraison";
$MESS["CRM_COLUMN_DATE_DEDUCTED"] = "Date de l'expédition";
$MESS["CRM_COLUMN_DATE_INSERT"] = "Date de création";
$MESS["CRM_COLUMN_DATE_MARKED"] = "Le problème est survenu le";
$MESS["CRM_COLUMN_DATE_RESPONSIBLE_ID"] = "Responsable changé le";
$MESS["CRM_COLUMN_DEDUCTED"] = "Expédiée";
$MESS["CRM_COLUMN_DELIVERY_DOC_DATE"] = "Date du document de livraison";
$MESS["CRM_COLUMN_DELIVERY_DOC_NUM"] = "Document de livraison #";
$MESS["CRM_COLUMN_DELIVERY_SERVICE"] = "Service de livraison";
$MESS["CRM_COLUMN_DISCOUNT_PRICE"] = "Réduction";
$MESS["CRM_COLUMN_EMP_ALLOW_DELIVERY"] = "Livraison approuvée par";
$MESS["CRM_COLUMN_EMP_DEDUCTED"] = "Statut Expédiée modifié par";
$MESS["CRM_COLUMN_EMP_MARKED"] = "Marqué comme problème par";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_MARKED"] = "Problème";
$MESS["CRM_COLUMN_ORDER_ID"] = "ID de la commande";
$MESS["CRM_COLUMN_PRICE_DELIVERY_CURRENCY"] = "Prix";
$MESS["CRM_COLUMN_REASON_MARKED"] = "Description du problème";
$MESS["CRM_COLUMN_REASON_UNDO_DEDUCTED"] = "Raison d'annulation de la livraison";
$MESS["CRM_COLUMN_RESPONSIBLE"] = "Responsable";
$MESS["CRM_COLUMN_SHIPMENT_SUMMARY"] = "Document de livraison";
$MESS["CRM_COLUMN_STATUS_ID"] = "Statut";
$MESS["CRM_COLUMN_TRACKING_DESCRIPTION"] = "Description du statut de la livraison";
$MESS["CRM_COLUMN_TRACKING_NUMBER"] = "Numéro de suivi";
$MESS["CRM_COLUMN_TRACKING_STATUS"] = "Statut de la livraison";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devises n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_SHIPMENT_DELETE_ERROR"] = "Erreur lors de la suppression de la livraison";
$MESS["CRM_SHIPMENT_SUMMARY"] = "Livraison ##NUMBER# - #DATE#";
?>