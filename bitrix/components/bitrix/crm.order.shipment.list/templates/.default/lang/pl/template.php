<?
$MESS["CRM_ALL"] = "Łącznie";
$MESS["CRM_ORDER_DEDUCTED"] = "Wysłano";
$MESS["CRM_ORDER_NOT_DEDUCTED"] = "Nie wysłano";
$MESS["CRM_ORDER_SHIPMENT_DELETE"] = "Usuń";
$MESS["CRM_ORDER_SHIPMENT_DELETE_CONFIRM"] = "Na pewno usunąć ten element?";
$MESS["CRM_ORDER_SHIPMENT_DELETE_TITLE"] = "Usuń wysyłkę";
$MESS["CRM_ORDER_SHIPMENT_EDIT"] = "Edytuj";
$MESS["CRM_ORDER_SHIPMENT_EDIT_TITLE"] = "Edytuj wysyłkę";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD"] = "Dodaj wysyłkę";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD_SHORT"] = "Wysyłka";
$MESS["CRM_ORDER_SHIPMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_SHIPMENT_REBUILD_ACCESS_ATTRS"] = "Zaktualizowane uprawnienia dostępu wymagają aktualizacji bieżących atrybutów dostępu przy użyciu <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">strony zarządzania uprawnieniami</a>.";
$MESS["CRM_ORDER_SHIPMENT_SHOW"] = "Widok";
$MESS["CRM_ORDER_SHIPMENT_SHOW_TITLE"] = "Pokaż wysyłkę";
$MESS["CRM_ORDER_SHIPMENT_SUMMARY"] = "Wysyłka ##NUMBER# - #DATE#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Pokaż ilość";
?>