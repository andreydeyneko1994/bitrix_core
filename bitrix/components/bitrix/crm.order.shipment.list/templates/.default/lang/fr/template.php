<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_DEDUCTED"] = "Expédiée";
$MESS["CRM_ORDER_NOT_DEDUCTED"] = "Pas expédiée";
$MESS["CRM_ORDER_SHIPMENT_DELETE"] = "Supprimer";
$MESS["CRM_ORDER_SHIPMENT_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cet article ?";
$MESS["CRM_ORDER_SHIPMENT_DELETE_TITLE"] = "Supprimer la livraison";
$MESS["CRM_ORDER_SHIPMENT_EDIT"] = "Modifier";
$MESS["CRM_ORDER_SHIPMENT_EDIT_TITLE"] = "Modifier la livraison";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD"] = "Ajouter une livraison";
$MESS["CRM_ORDER_SHIPMENT_LIST_ADD_SHORT"] = "Livraison";
$MESS["CRM_ORDER_SHIPMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Liste";
$MESS["CRM_ORDER_SHIPMENT_REBUILD_ACCESS_ATTRS"] = "Les permissions d'accès actualisées nécessitent que vous mettiez à jour les attributs d'accès actuels en utilisant la <a id='#ID#' target='_blank' href='#URL#'> page de gestion des autorisations</a>.";
$MESS["CRM_ORDER_SHIPMENT_SHOW"] = "Afficher";
$MESS["CRM_ORDER_SHIPMENT_SHOW_TITLE"] = "Afficher la livraison";
$MESS["CRM_ORDER_SHIPMENT_SUMMARY"] = "Livraison ##NUMBER# - #DATE#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
?>