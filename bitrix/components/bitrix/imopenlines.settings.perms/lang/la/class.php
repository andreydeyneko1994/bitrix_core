<?
$MESS["IMOL_PERM_ACCESS_DENIED"] = "Permisos de acceso insuficientes";
$MESS["IMOL_PERM_LICENSING_ERROR"] = "Su plan no permite la administración de permisos de acceso para Canales Abiertos.";
$MESS["IMOL_PERM_UNKNOWN_ACCESS_CODE"] = "(ID de acceso desconocido)";
$MESS["IMOL_PERM_UNKNOWN_SAVE_ERROR"] = "Error al guardar datos";
?>