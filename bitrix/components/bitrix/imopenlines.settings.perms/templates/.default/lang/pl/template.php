<?php
$MESS["IMOL_PERM_ADD"] = "Dodaj";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "Dodaj uprawnienia dostępu";
$MESS["IMOL_PERM_DELETE"] = "Usuń";
$MESS["IMOL_PERM_EDIT"] = "Edytuj";
$MESS["IMOL_PERM_ERROR"] = "Błąd";
$MESS["IMOL_PERM_RESTRICTION"] = "Aby przypisywać uprawnienia dostępu pracownikom, rozszerz subskrypcję do jednego z <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">planów komercyjnych</a>.";
$MESS["IMOL_PERM_ROLE"] = "Rola";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "Anuluj";
$MESS["IMOL_PERM_ROLE_DELETE"] = "Usuń rolę";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "Czy na pewno usunąć tą rolę?";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "Błąd podczas usuwania roli.";
$MESS["IMOL_PERM_ROLE_LIST"] = "Role";
$MESS["IMOL_PERM_ROLE_OK"] = "OK";
$MESS["IMOL_PERM_SAVE"] = "Zapisz";
