<?php
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "Module du Concepteur des procédures d'entreprise non installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module De processus d'entreprise n'a pas été installé.";
$MESS["CRM_BP_COMPANY"] = "Entreprise";
$MESS["CRM_BP_CONTACT"] = "Client";
$MESS["CRM_BP_DEAL"] = "Transaction";
$MESS["CRM_BP_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_BP_INVOICE"] = "Facture";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Liste des modèles : #NAME#";
$MESS["CRM_BP_ORDER"] = "Commande";
$MESS["CRM_BP_WFEDIT_TITLE_ADD"] = "Nouveau modèle de flux de travail pour #NAME#";
$MESS["CRM_BP_WFEDIT_TITLE_EDIT"] = "Éditer le modèle \"#TEMPLATE#\" du flux de travail #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
