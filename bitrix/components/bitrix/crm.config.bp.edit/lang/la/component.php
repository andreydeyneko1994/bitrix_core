<?php
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "El módulo Business Process Editor no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Proceso de negocio no está instalado.";
$MESS["CRM_BP_COMPANY"] = "Compañía";
$MESS["CRM_BP_CONTACT"] = "Contacto";
$MESS["CRM_BP_DEAL"] = "Negociación";
$MESS["CRM_BP_ENTITY_LIST"] = "Tipos";
$MESS["CRM_BP_INVOICE"] = "Factura";
$MESS["CRM_BP_LEAD"] = "Prospecto";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Plantillas: #NAME#";
$MESS["CRM_BP_ORDER"] = "Pedido";
$MESS["CRM_BP_WFEDIT_TITLE_ADD"] = "Nueva plantilla de procesos de negocio para #NAME#";
$MESS["CRM_BP_WFEDIT_TITLE_EDIT"] = "Editar la plantilla \"#TEMPLATE#\" para el procesos de negocio #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
