<?
$MESS["SONET_C11_DO_ACT"] = "Wyślij Zaproszenie";
$MESS["SONET_C11_DO_SKIP"] = "Pomiń";
$MESS["SONET_C11_GROUP"] = "Grupa";
$MESS["SONET_C11_MESSAGE"] = "Twoja wiadomość";
$MESS["SONET_C11_MESSAGE_DEFAULT"] = "Chciałbym was zaprosić do grupy roboczej '#NAME#'";
$MESS["SONET_C11_MESSAGE_GROUP_LINK"] = "Otwórz grupę roboczą";
$MESS["SONET_C11_SUBTITLE"] = "Wiadomość zostanie wysłana zapraszając użytkowników do grupy. Użytkownicy zostaną odnotowani w tym miejscu po akceptacji zaproszenia.";
$MESS["SONET_C11_SUCCESS"] = "Twoje zaproszenie zostało wysłane. Użytkownicy zostaną tutaj odnotowani po potwierdzeniu swojego członkostwa.";
$MESS["SONET_C11_USER"] = "Użytkownicy";
$MESS["SONET_C11_USER_EXTRANET"] = "Użytkownicy Zewnętrzni:";
$MESS["SONET_C11_USER_INTRANET"] = "Pracownicy Firmy:";
$MESS["SONET_C11_USER_INTRANET_STRUCTURE"] = "Dodaj Ze Struktury Firmy";
$MESS["SONET_C33_T_ADD_FRIEND1"] = "Znajdź Użytkowników";
$MESS["SONET_C33_T_ERROR_LIST"] = "Wysłanie zaproszenia nie powiodło się dla następujących użytkowników:";
$MESS["SONET_C33_T_FRIENDS"] = "Znajomi";
$MESS["SONET_C33_T_NO_FRIENDS"] = "Brak znajomych.";
$MESS["SONET_C33_T_SUCCESS_LIST"] = "Zaproszenie zostało wysłane do następujących użytkowników:";
$MESS["SONET_C33_T_UNOTSET"] = "Użytkownicy nie są określeni.";
?>