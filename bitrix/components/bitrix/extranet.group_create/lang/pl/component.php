<?
$MESS["FL_FORUM_CHAIN"] = "Forum";
$MESS["IBLOCK_DEFAULT_UF"] = "Domyślna Galeria";
$MESS["SONET_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["SONET_CREATE_WEBDAV"] = "Utwórz Bibliotekę";
$MESS["SONET_FILES"] = "Pliki";
$MESS["SONET_FILES_LOG"] = "#AUTHOR_NAME# dodał plik #TITLE#.";
$MESS["SONET_FILES_LOG_TEXT"] = "Nowy plik #TITLE# na #URL#.";
$MESS["SONET_GALLERIES_IS_NOT_ACTIVE"] = "Twoje galerie zdjęć są obecnie nieaktywne. Proszę skontaktuj się z administratorem.";
$MESS["SONET_GALLERY_IS_NOT_ACTIVE"] = "Twoja galeria zdjęć jest obecnie nieaktywna. Proszę skontaktuj się z administratorem.";
$MESS["SONET_GALLERY_NOT_FOUND"] = "Brak zdjęć.";
$MESS["SONET_GROUP"] = "Grupa";
$MESS["SONET_GROUP_NOT_EXISTS"] = "Grupy nie istnieją.";
$MESS["SONET_GROUP_PREFIX"] = "Grupa:";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "Blok informacji nie jest określony.";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["SONET_LOADING"] = "Ładowanie…";
$MESS["SONET_MODULE_NOT_INSTALL"] = "Moduł Sieci Społecznościowej nie jest zainstalowany.";
$MESS["SONET_PHOTO"] = "Galeria";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# dodał zdjęcie #TITLE#";
$MESS["SONET_PHOTO_LOG_2"] = "Zdjęcia (#COUNT#)";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Nowe zdjęcia: #LINKS# i inne.";
$MESS["SONET_P_MODULE_IS_NOT_INSTALLED"] = "Moduł Galeria Zdjęć nie jest zainstalowany.";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "Moduł Biblioteki Dokumentów nie jest zainstalowany.";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Brak dokumentów.";
?>