<?
$MESS["WD_DAV_INSUFFICIENT_RIGHTS"] = "Nie masz uprawnień do tego działania.";
$MESS["WD_DAV_UNSUPORTED_METHOD"] = "Ta metoda nie jest obsługiwana.";
$MESS["WD_GROUP"] = "grupy robocze";
$MESS["WD_GROUP_SECTION_FILES_NOT_FOUND"] = "Nie znaleziono sekcji dokumentów grupy.";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "Nie znaleziono bloku informacji dokumentów grupy roboczej.";
$MESS["WD_IB_USER_IS_NOT_FOUND"] = "Nie znaleziono bloku informacji dokumentów użytkownika.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_NOT_SEF_MODE"] = "Szukaj obsługiwanych (SEF) URL's nie włączonych w parametrach.";
$MESS["WD_PRIVATE"] = "Prywatne dokumenty";
$MESS["WD_ROOT"] = "Źródło";
$MESS["WD_SHARED"] = "Udostępnione dokumkenty";
$MESS["WD_SN_MODULE_IS_NOT_INSTALLED"] = "Moduł Sieci Społecznościowej nie jest zainstalowany.";
$MESS["WD_SOCNET_LANG_NOT_FOUND"] = "Brakuje składników sieci społecznościowej. Sprawdź ścieżki katalogu głównego serwera w ustawieniach witryny.";
$MESS["WD_USER"] = "Dokumenty użytkownika";
$MESS["WD_USER_NOT_FOUND"] = "Nie znaleziono użytkownika.";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "Nie znaleziono sekcji dokumentów użytkownika.";
$MESS["WD_WD_MODULE_IS_NOT_INSTALLED"] = "Moduł Biblioteki Dokumentów nie jest zainstalowany.";
?>