<?
$MESS["WD_AG_HELP1"] = "Uruchom <i>Windows Explorer</i>.";
$MESS["WD_AG_HELP3"] = "Wybierz literę dysku dla folderu do połączenia.";
$MESS["WD_AG_HELP4"] = "Podaj ścieżkę do biblioteki w <i>Folder</i> polu:";
$MESS["WD_AG_HELP5"] = "Kliknij <i>Zakończ</i>.";
$MESS["WD_AG_HELP6"] = "Aby uzyskać więcej informaji na temat korzystania z biblioteki w Windows i Mac OS X, zajrzyj do biblioteki dokumentów #STARTLINK# sekcja pomocy #ENDLINK#.";
$MESS["WD_AG_MSGTITLE"] = "Aby połączyć bibliotekę jako dysk sieciowy";
$MESS["WD_NO_LIBRARIES"] = "Brak dostępnych bibliotek";
?>