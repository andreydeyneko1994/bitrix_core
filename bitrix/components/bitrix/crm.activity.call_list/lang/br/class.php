<?php
$MESS["CRM_CALL_LIST_ACCESS_DENIED"] = "Permissões insuficientes";
$MESS["CRM_CALL_LIST_ADD_MORE"] = "adicionar mais";
$MESS["CRM_CALL_LIST_DELETE"] = "Excluir";
$MESS["CRM_CALL_LIST_ENTITIES_ADDED"] = "#ENTITIES# adicionados à lista de chamadas";
$MESS["CRM_CALL_LIST_ENTITY_NAME"] = "Nome";
$MESS["CRM_CALL_LIST_ERROR_WRONG_ITEM_TYPE"] = "A lista de chamadas não pode incluir entidades de tipos diferentes";
$MESS["CRM_CALL_LIST_FILTERED"] = "#ENTITIES# foram selecionados de acordo com";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "#ENTITIES# foram selecionados manualmente";
$MESS["CRM_CALL_LIST_ITEM_CALL_RECORD"] = "Gravação";
$MESS["CRM_CALL_LIST_ITEM_CREATED"] = "Criado em";
$MESS["CRM_CALL_LIST_ITEM_FORM"] = "Formulário ##ID#";
$MESS["CRM_CALL_LIST_ITEM_STATUS"] = "Status";
$MESS["CRM_CALL_LIST_ITEM_WEBFORM_ACTIVITY"] = "Atividade de formulário CRM";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "O módulo Telefonia não está instalado.";
