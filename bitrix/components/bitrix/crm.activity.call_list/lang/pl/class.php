<?php
$MESS["CRM_CALL_LIST_ACCESS_DENIED"] = "Niewystarczające uprawnienia";
$MESS["CRM_CALL_LIST_ADD_MORE"] = "dodaj więcej";
$MESS["CRM_CALL_LIST_DELETE"] = "Usuń";
$MESS["CRM_CALL_LIST_ENTITIES_ADDED"] = "Dodano #ENTITIES# do listy połączeń";
$MESS["CRM_CALL_LIST_ENTITY_NAME"] = "Imię";
$MESS["CRM_CALL_LIST_ERROR_WRONG_ITEM_TYPE"] = "Lista połączeń nie może zawierać elementów różnych typów";
$MESS["CRM_CALL_LIST_FILTERED"] = "Wybrano #ENTITIES# według";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "Wybrano #ENTITIES# ręcznie";
$MESS["CRM_CALL_LIST_ITEM_CALL_RECORD"] = "Rekord";
$MESS["CRM_CALL_LIST_ITEM_CREATED"] = "Utworzone";
$MESS["CRM_CALL_LIST_ITEM_FORM"] = "##ID# Formularza";
$MESS["CRM_CALL_LIST_ITEM_STATUS"] = "Status";
$MESS["CRM_CALL_LIST_ITEM_WEBFORM_ACTIVITY"] = "Działanie formularza CRM";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "Moduł Telefonia nie jest zainstalowany.";
