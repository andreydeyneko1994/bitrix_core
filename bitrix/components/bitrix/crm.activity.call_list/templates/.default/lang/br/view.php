<?
$MESS["CRM_CALL_LIST_BUTTON_CONTINUE"] = "Continuar";
$MESS["CRM_CALL_LIST_BUTTON_START"] = "Começar a ligar";
$MESS["CRM_CALL_LIST_COMPLETE"] = "concluídas #COMPLETE# de #TOTAL#";
$MESS["CRM_CALL_LIST_DESCRIPTION"] = "Descrição";
$MESS["CRM_CALL_LIST_ENTITY_COMPANIES"] = "Empresas";
$MESS["CRM_CALL_LIST_ENTITY_CONTACTS"] = "Contatos";
$MESS["CRM_CALL_LIST_ENTITY_LEADS"] = "Leads";
$MESS["CRM_CALL_LIST_FILTER"] = "filtro";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "selecionado manualmente";
$MESS["CRM_CALL_LIST_PICKED_BY_FILTER"] = "selecionado de acordo com ";
$MESS["CRM_CALL_LIST_SELECTION_PARAMS"] = "Parâmetros de seleção";
$MESS["CRM_CALL_LIST_STATISTICS"] = "Estatísticas";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Assunto";
$MESS["CRM_CALL_LIST_TITLE"] = "Lista de chamadas";
$MESS["CRM_CALL_LIST_USE_FORM"] = "Utilizando formulário";
?>