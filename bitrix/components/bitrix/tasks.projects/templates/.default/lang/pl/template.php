<?php
$MESS["TASKS_PROJECTS_ADD_PROJECT"] = "Utwórz projekt";
$MESS["TASKS_PROJECTS_ENTITY_SELECTOR_TAG_SEARCH_FOOTER_ADD"] = "Dodaj tag:";
$MESS["TASKS_PROJECTS_FIRST_PROJECT_CREATION_TOUR_GUIDE_POPUP_TEXT"] = "Nowy projekt otworzy się automatycznie";
$MESS["TASKS_PROJECTS_FIRST_PROJECT_CREATION_TOUR_GUIDE_POPUP_TITLE"] = "Click to create a project";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_EMPTY"] = "Brak danych";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_LABEL_SCRUM_MASTER"] = "Scrum master";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_LABEL_SCRUM_OWNER"] = "Właściciel produktu";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_LABEL_SCRUM_TEAM"] = "Zespół programistów";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_TITLE_ALL"] = "Wszystkie";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_TITLE_HEADS"] = "Przełożeni";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_TITLE_MEMBERS"] = "Członkowie";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_TITLE_SCRUM_MEMBERS"] = "Interesariusze";
$MESS["TASKS_PROJECTS_MEMBERS_POPUP_TITLE_SCRUM_TEAM"] = "Zespół";
$MESS["TASKS_PROJECTS_SCRUM_ADD_PROJECT"] = "Utwórz";
$MESS["TASKS_PROJECTS_SCRUM_MIGRATION"] = "Migruj dane z innych systemów";
$MESS["TASKS_PROJECTS_SCRUM_TITLE"] = "Scrum";
$MESS["TASKS_PROJECTS_TITLE"] = "Projekty";
