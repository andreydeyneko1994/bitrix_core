<?php
$MESS["CPVD_NEW_VARIATION_TITLE"] = "Nouvelle variante";
$MESS["CPVD_NOT_FOUND_ERROR_TITLE"] = "La variante du produit est introuvable. Elle a peut-être été supprimée.";
