<?
$MESS["OP_NAME_TM_MANAGE"] = "Moje zarejestrowanie i wyrejestrowanie";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Zarejestrowanie i wyrejestrowanie innej osoby";
$MESS["OP_NAME_TM_READ"] = "Wyświetl wszystkie rekordy";
$MESS["OP_NAME_TM_READ_SCHEDULES_ALL"] = "Wyświetl wszystkie harmonogramy";
$MESS["OP_NAME_TM_READ_SHIFT_PLANS_ALL"] = "Wyświetl wszystkie harmonogramy zmian";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Wyświetl rekordy moje i podwładnych";
$MESS["OP_NAME_TM_SETTINGS"] = "Edytuj ustawienia modułu";
$MESS["OP_NAME_TM_UPDATE_SCHEDULES_ALL"] = "Edytuj wszystkie harmonogramy pracy";
$MESS["OP_NAME_TM_UPDATE_SHIFT_PLANS_ALL"] = "Edytuj wszystkie harmonogramy zmian";
$MESS["OP_NAME_TM_WRITE"] = "Edytuj wszystkie rekordy";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Edytuj rekordy moje i podwładnych";
$MESS["TASK_NAME_TIMEMAN_DENIED_CONVERTED_EDITABLE"] = "Odmowa dostępu";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS_CONVERTED_EDITABLE"] = "Pełny dostęp";
$MESS["TASK_NAME_TIMEMAN_READ_CONVERTED_EDITABLE"] = "Dział HR";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE_CONVERTED_EDITABLE"] = "Pracownik działu lub przełożony";
$MESS["TASK_NAME_TIMEMAN_WRITE_CONVERTED_EDITABLE"] = "Zarządzanie firmą";
$MESS["TIMEMAN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Utwórz rolę";
$MESS["TIMEMAN_SETTINGS_PERMS_CAN_NOT_EDIT_SYSTEM_TASK"] = "Uprawnienia dostępu do systemu są dostępne tylko do wyświetlania.";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "Nie znaleziono roli";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Edytuj rolę: #ROLE#";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_SETTINGS"] = "Użytkownik może edytować ustawienia";
$MESS["TIMEMAN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "Nie masz uprawnień do edycji uprawnień dostępu";
$MESS["TIMEMAN_SETTINGS_PERMS_TITLE"] = "Uprawnienia dostępu";
$MESS["TIMEMAN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(nieznany ID dostępu)";
?>