<?
$MESS["OP_NAME_TM_MANAGE"] = "Minha entrada e saída";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Entrada e saída de outras pessoas";
$MESS["OP_NAME_TM_READ"] = "Visualizar todos os registros";
$MESS["OP_NAME_TM_READ_SCHEDULES_ALL"] = "Ver todos os horários";
$MESS["OP_NAME_TM_READ_SHIFT_PLANS_ALL"] = "Ver todos os horários de turno";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Visualizar meus registros e dos meus subordinados";
$MESS["OP_NAME_TM_SETTINGS"] = "Editar configurações do módulo";
$MESS["OP_NAME_TM_UPDATE_SCHEDULES_ALL"] = "Editar todos os horários de trabalho";
$MESS["OP_NAME_TM_UPDATE_SHIFT_PLANS_ALL"] = "Editar todos os horários de turno";
$MESS["OP_NAME_TM_WRITE"] = "Editar todos os registros";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Editar meus registros e dos meus subordinados";
$MESS["TASK_NAME_TIMEMAN_DENIED_CONVERTED_EDITABLE"] = "Acesso negado";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS_CONVERTED_EDITABLE"] = "Acesso total";
$MESS["TASK_NAME_TIMEMAN_READ_CONVERTED_EDITABLE"] = "Departamento de RH";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE_CONVERTED_EDITABLE"] = "Funcionário ou supervisor do departamento";
$MESS["TASK_NAME_TIMEMAN_WRITE_CONVERTED_EDITABLE"] = "Administração da empresa";
$MESS["TIMEMAN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "Criar Função";
$MESS["TIMEMAN_SETTINGS_PERMS_CAN_NOT_EDIT_SYSTEM_TASK"] = "As permissões de acesso ao sistema estão disponíveis apenas para visualização.";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "A função não foi encontrada";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "Editar função #ROLE#";
$MESS["TIMEMAN_SETTINGS_PERMS_EDIT_SETTINGS"] = "O usuário pode editar configurações";
$MESS["TIMEMAN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "Você não tem permissão para editar permissões de acesso";
$MESS["TIMEMAN_SETTINGS_PERMS_TITLE"] = "Permissões de Acesso";
$MESS["TIMEMAN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "(ID de acesso desconhecido)";
?>