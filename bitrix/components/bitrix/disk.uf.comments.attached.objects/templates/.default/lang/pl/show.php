<?
$MESS["DISK_UF_FILE_DISABLE_AUTO_COMMENT"] = "Wyłącz automatyczne komentarze";
$MESS["DISK_UF_FILE_DOWNLOAD_ALL_FILES_BY_ARCHIVE"] = "Pobierz wszystkie pliki jako archiwum";
$MESS["DISK_UF_FILE_ENABLE_AUTO_COMMENT"] = "Włącz automatyczne komentarze";
$MESS["DISK_UF_FILE_RUN_FILE_IMPORT"] = "Pobierz ostatnią wersję";
$MESS["DISK_UF_FILE_SETTINGS_DOCS"] = "Ustawienia do pracy z dokumentami";
$MESS["DISK_UF_FILE_STATUS_FAIL_LOADING"] = "Błąd ładowania najnowszej wersji.";
$MESS["DISK_UF_FILE_STATUS_HAS_LAST_VERSION"] = "Posiadasz ostatnią wersję.";
$MESS["DISK_UF_FILE_STATUS_PROCESS_LOADING"] = "Ładowanie";
$MESS["DISK_UF_FILE_STATUS_SUCCESS_LOADING"] = "Najnowsza wersja została pomyślnie załadowana.";
$MESS["WDUF_FILES"] = "Pliki:";
$MESS["WDUF_FILE_EDIT"] = "Edytuj";
$MESS["WDUF_MORE_ACTIONS"] = "Więcej";
$MESS["WDUF_PHOTO"] = "Zdjęcie:";
?>