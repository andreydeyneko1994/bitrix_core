<?
$MESS["DISK_UF_ATTACHED_ACTION_SAVE_TO_OWN_FILES"] = "Guardar en Bitrix24.Drive";
$MESS["SONET_GROUP_PREFIX"] = "Grupo:";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "El archivo se ha adjuntado al mensaje.";
$MESS["WDUF_FILE_DOWNLOAD"] = "Descargar";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "actualmente está siendo editado en #SERVICE#";
$MESS["WDUF_FILE_PREVIEW"] = "Vista previa";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Revisión del historial ";
$MESS["WDUF_HISTORY_FILE"] = "Versión #NUMBER#:";
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Seleccione archivo de este equipo";
$MESS["WD_LOCAL_COPY_ONLY"] = "En este mensaje";
$MESS["WD_MY_LIBRARY"] = "Mi drive";
$MESS["WD_SAVED_PATH"] = "Guardado";
?>