<?
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module De processus d'entreprise n'a pas été installé.";
$MESS["CRM_BP_COMPANY"] = "Entreprise";
$MESS["CRM_BP_CONTACT"] = "Client";
$MESS["CRM_BP_DEAL"] = "Transaction";
$MESS["CRM_BP_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Liste des modèles : #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>