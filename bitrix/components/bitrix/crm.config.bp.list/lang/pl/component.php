<?
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_BP_COMPANY"] = "Firma";
$MESS["CRM_BP_CONTACT"] = "Kontakt";
$MESS["CRM_BP_DEAL"] = "Deal";
$MESS["CRM_BP_ENTITY_LIST"] = "Typy";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Szablony: #NAME#";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>