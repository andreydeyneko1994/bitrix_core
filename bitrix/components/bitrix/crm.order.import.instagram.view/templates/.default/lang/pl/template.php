<?
$MESS["CRM_OIIV_CANCEL"] = "Anuluj";
$MESS["CRM_OIIV_CREATE_STORE"] = "Utwórz sklep internetowy";
$MESS["CRM_OIIV_IMPORT"] = "Importuj";
$MESS["CRM_OIIV_IMPORT_FEEDBACK"] = "Informacja zwrotna";
$MESS["CRM_OIIV_IMPORT_MORE"] = "Importuj więcej";
$MESS["CRM_OIIV_IMPORT_NETWORK_ERROR"] = "Nieznany błąd podczas importowania danych. Spróbuj ponownie.";
$MESS["CRM_OIIV_INSTAGRAM_CONNECTED"] = "połączony";
$MESS["CRM_OIIV_NO_MEDIA"] = "Brak publikacji w profilu";
$MESS["CRM_OIIV_NO_PRICE"] = "podaj cenę";
$MESS["CRM_OIIV_PRODUCTS_ADDED_SUCCESSFUL"] = "Produkty zostały zaimportowane do Katalogu Handlowego CRM";
$MESS["CRM_OIIV_PRODUCT_CHANGE_PRICE_SUCCESSFUL"] = "Cena produktu została zmieniona";
$MESS["CRM_OIIV_PRODUCT_RENAME_SUCCESSFUL"] = "Nazwa produktu została zmieniona";
$MESS["CRM_OIIV_SELECT_ALL"] = "Zaznacz wszystko";
$MESS["CRM_OIIV_SELECT_MEDIA"] = "Wybierz publikacje do zaimportowania";
$MESS["CRM_OIIV_SELECT_NO_MEDIA"] = "Brak nowych publikacji";
$MESS["CRM_OIIV_STEP_IMPORTED"] = "Zaimportowane publikacje";
$MESS["CRM_OIIV_STEP_PROCESS_TITLE"] = "Import publikacji";
$MESS["CRM_OIIV_STEP_READY_TITLE"] = "Gotowe!";
$MESS["CRM_OIIV_STEP_STOPPED_TITLE"] = "Import został przerwany";
$MESS["CRM_OIIV_STOP"] = "Zatrzymaj";
$MESS["CRM_OIIV_TOTAL_IMPORTED"] = "Zaimportowane publikacje z profilu ogółem";
$MESS["CRM_OIIV_UNSELECT_ALL"] = "Odznacz wszystko";
?>