<?
$MESS["CRM_OIIV_CANCEL"] = "Cancelar";
$MESS["CRM_OIIV_CREATE_STORE"] = "Criar loja on-line";
$MESS["CRM_OIIV_IMPORT"] = "Importar";
$MESS["CRM_OIIV_IMPORT_FEEDBACK"] = "Feedback";
$MESS["CRM_OIIV_IMPORT_MORE"] = "Importar mais";
$MESS["CRM_OIIV_IMPORT_NETWORK_ERROR"] = "Erro desconhecido ao importar os dados. Por favor, tente novamente.";
$MESS["CRM_OIIV_INSTAGRAM_CONNECTED"] = "conectado";
$MESS["CRM_OIIV_NO_MEDIA"] = "Não há publicações no perfil";
$MESS["CRM_OIIV_NO_PRICE"] = "especificar preço";
$MESS["CRM_OIIV_PRODUCTS_ADDED_SUCCESSFUL"] = "Os produtos foram importados para o catálogo comercial do CRM";
$MESS["CRM_OIIV_PRODUCT_CHANGE_PRICE_SUCCESSFUL"] = "O preço do produto foi alterado";
$MESS["CRM_OIIV_PRODUCT_RENAME_SUCCESSFUL"] = "O nome do produto foi alterado";
$MESS["CRM_OIIV_SELECT_ALL"] = "Selecionar todas";
$MESS["CRM_OIIV_SELECT_MEDIA"] = "Selecionar postagem para importar";
$MESS["CRM_OIIV_SELECT_NO_MEDIA"] = "Não há publicações disponíveis";
$MESS["CRM_OIIV_STEP_IMPORTED"] = "Publicações importadas";
$MESS["CRM_OIIV_STEP_PROCESS_TITLE"] = "As publicações estão sendo importadas";
$MESS["CRM_OIIV_STEP_READY_TITLE"] = "Feito!";
$MESS["CRM_OIIV_STEP_STOPPED_TITLE"] = "Importação cancelada";
$MESS["CRM_OIIV_STOP"] = "Parar";
$MESS["CRM_OIIV_TOTAL_IMPORTED"] = "Total de publicações importadas do perfil";
$MESS["CRM_OIIV_UNSELECT_ALL"] = "Desmarcar todas";
?>