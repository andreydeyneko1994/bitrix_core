<?
$MESS["CRM_OIIV_COLUMN_CAPTION"] = "Podpis";
$MESS["CRM_OIIV_COLUMN_IMPORTED"] = "Zaimportowano";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE"] = "Rodzaj publikacji";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_CAROUSEL_ALBUM"] = "Karuzela";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_PICTURE"] = "Obraz";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_VIDEO"] = "Wideo";
$MESS["CRM_OIIV_COLUMN_NEW"] = "Nowy";
$MESS["CRM_OIIV_COLUMN_PERMALINK"] = "Stałe łącze";
$MESS["CRM_OIIV_COLUMN_TIMESTAMP"] = "Data publikacji";
$MESS["CRM_OIIV_DEFAULT_PRODUCT_NAME"] = "Bez nazwy";
$MESS["CRM_OIIV_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "To złącze jest nieaktywne.";
$MESS["CRM_OIIV_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "Import produktu został skonfigurowany do pracy w grupie, do której obecnie nie masz uprawnień administratora.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CRM"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_OIIV_PRESET_IMPORTED"] = "Zaimportowano";
$MESS["CRM_OIIV_PRESET_NOT_IMPORTED"] = "Nieimportowane";
$MESS["CRM_OIIV_PRESET_RECENT"] = "Nowy";
$MESS["CRM_OIIV_TITLE"] = "Importuj produkty do Sklepu Bitrix24";
?>