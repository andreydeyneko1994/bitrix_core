<?
$MESS["CRM_OIIV_COLUMN_CAPTION"] = "Légende";
$MESS["CRM_OIIV_COLUMN_IMPORTED"] = "Importés";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE"] = "Type de publication";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_CAROUSEL_ALBUM"] = "Carrousel";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_PICTURE"] = "Image";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_VIDEO"] = "Vidéo";
$MESS["CRM_OIIV_COLUMN_NEW"] = "Nouveaux";
$MESS["CRM_OIIV_COLUMN_PERMALINK"] = "Lien permanent";
$MESS["CRM_OIIV_COLUMN_TIMESTAMP"] = "Date de création";
$MESS["CRM_OIIV_DEFAULT_PRODUCT_NAME"] = "Sans titre";
$MESS["CRM_OIIV_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["CRM_OIIV_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "L'importation de produit a été configurée pour être utilisée avec un groupe auquel vous n'avez actuellement pas accès d'administrateur.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CRM"] = "Le module CRM n'est pas installé.";
$MESS["CRM_OIIV_PRESET_IMPORTED"] = "Importés";
$MESS["CRM_OIIV_PRESET_NOT_IMPORTED"] = "Non importés";
$MESS["CRM_OIIV_PRESET_RECENT"] = "Nouveaux";
$MESS["CRM_OIIV_TITLE"] = "Importer des produits dans la boutique en ligne Bitrix24";
?>