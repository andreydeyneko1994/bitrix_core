<?
$MESS["CRM_OIIV_COLUMN_CAPTION"] = "Legenda";
$MESS["CRM_OIIV_COLUMN_IMPORTED"] = "Importado";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE"] = "Tipo de publicação";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_CAROUSEL_ALBUM"] = "Carrossel";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_PICTURE"] = "Imagem";
$MESS["CRM_OIIV_COLUMN_MEDIA_TYPE_VIDEO"] = "Vídeo";
$MESS["CRM_OIIV_COLUMN_NEW"] = "Novo";
$MESS["CRM_OIIV_COLUMN_PERMALINK"] = "Link para a publicação";
$MESS["CRM_OIIV_COLUMN_TIMESTAMP"] = "Criado em";
$MESS["CRM_OIIV_DEFAULT_PRODUCT_NAME"] = "Sem título";
$MESS["CRM_OIIV_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["CRM_OIIV_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "A importação de produtos foi configurada para ser usada com um grupo a qual você atualmente não tem acesso administrativo.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_OIIV_MODULE_NOT_INSTALLED_CRM"] = "O módulo CRM não está instalado.";
$MESS["CRM_OIIV_PRESET_IMPORTED"] = "Importado";
$MESS["CRM_OIIV_PRESET_NOT_IMPORTED"] = "Não importado";
$MESS["CRM_OIIV_PRESET_RECENT"] = "Novo";
$MESS["CRM_OIIV_TITLE"] = "Importação de produtos para Loja On-line Bitrix24";
?>