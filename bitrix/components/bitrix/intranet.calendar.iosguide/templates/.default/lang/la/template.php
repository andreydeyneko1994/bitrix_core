<?php
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE"] = "Instalar perfil";
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE_IN_SETTINGS"] = "En la configuración del teléfono, instale el perfil que descargó";
$MESS["CAL_IOS_GUIDE_IS_INSTALLED_ON_DEVICE"] = "¡Ahora el calendario de Bitrix24 está sincronizado con el calendario de su teléfono!";
$MESS["CAL_IOS_GUIDE_IS_SUCCESS"] = "¡Éxito!";
$MESS["CAL_IOS_GUIDE_PREVIEW_INFO"] = "Para sincronizar el calendario de Bitrix24 en un dispositivo IOS, descargue e instale un perfil de Bitrix24 en su dispositivo. El perfil incluye todos los parámetros necesarios para sincronizar un calendario móvil.";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD"] = "Descargar perfil";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD_BITRIX"] = "Descargar perfil de Bitrix24";
$MESS["CAL_IOS_GUIDE_READ_HOW_IT_WORK"] = "Cómo funciona";
$MESS["EC_CALENDAR_IOS_GUIDE_IMAGEFILE"] = "ios_guide_en.gif";
