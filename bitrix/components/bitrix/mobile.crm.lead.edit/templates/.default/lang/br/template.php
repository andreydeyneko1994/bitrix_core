<?
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Selecionar empresa da lista...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Selecionar contato da lista...";
$MESS["M_CRM_LEAD_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_LEAD_EDIT_CREATE_TITLE"] = "Novo Lead";
$MESS["M_CRM_LEAD_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_LEAD_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_LEAD_EDIT_VIEW_TITLE"] = "Ver Lead";
$MESS["M_CRM_LEAD_MENU_ACTIVITY"] = "Atividades";
$MESS["M_CRM_LEAD_MENU_CREATE_ON_BASE"] = "Criar usando fonte";
$MESS["M_CRM_LEAD_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_LEAD_MENU_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_MENU_HISTORY"] = "Histórico";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>