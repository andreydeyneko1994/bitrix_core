<?
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Wybierz firmę z listy ...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Wybierz kontakt z listy ...";
$MESS["M_CRM_LEAD_EDIT_CANCEL_BTN"] = "Anuluj";
$MESS["M_CRM_LEAD_EDIT_CREATE_TITLE"] = "Nowy lead";
$MESS["M_CRM_LEAD_EDIT_EDIT_TITLE"] = "Edycja";
$MESS["M_CRM_LEAD_EDIT_SAVE_BTN"] = "Zapisz";
$MESS["M_CRM_LEAD_EDIT_VIEW_TITLE"] = "Wyświetl lead";
$MESS["M_CRM_LEAD_MENU_ACTIVITY"] = "Aktywności";
$MESS["M_CRM_LEAD_MENU_CREATE_ON_BASE"] = "Utwórz ze źródła";
$MESS["M_CRM_LEAD_MENU_DELETE"] = "Usuń";
$MESS["M_CRM_LEAD_MENU_EDIT"] = "Edycja";
$MESS["M_CRM_LEAD_MENU_HISTORY"] = "Historia";
$MESS["M_DETAIL_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_DETAIL_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_DETAIL_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
?>