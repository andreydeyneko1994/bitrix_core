<?
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Choisir une entreprise de la liste...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Choisir un contact de la liste...";
$MESS["M_CRM_LEAD_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_LEAD_EDIT_CREATE_TITLE"] = "Nouveau prospect";
$MESS["M_CRM_LEAD_EDIT_EDIT_TITLE"] = "Modifier";
$MESS["M_CRM_LEAD_EDIT_SAVE_BTN"] = "Enregistrer";
$MESS["M_CRM_LEAD_EDIT_VIEW_TITLE"] = "Voir le client potentiel";
$MESS["M_CRM_LEAD_MENU_ACTIVITY"] = "Activités";
$MESS["M_CRM_LEAD_MENU_CREATE_ON_BASE"] = "Créer en utilisant la source";
$MESS["M_CRM_LEAD_MENU_DELETE"] = "Supprimer";
$MESS["M_CRM_LEAD_MENU_EDIT"] = "Modifier";
$MESS["M_CRM_LEAD_MENU_HISTORY"] = "Historique";
$MESS["M_DETAIL_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_DETAIL_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
?>