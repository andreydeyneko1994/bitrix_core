<?
$MESS["CRM_LEAD_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se você ainda estiver nesta página, feche-a manualmente.";
$MESS["CRM_LEAD_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "O lead <a href='#URL#'>#TITLE#</a> foi criado. Agora você está sendo redirecionado para a página anterior. Se esta página ainda estiver sendo exibida, feche-a manualmente.";
?>