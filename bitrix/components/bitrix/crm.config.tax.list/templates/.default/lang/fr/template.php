<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_TAX_DELETE"] = "Eliminer l'impôt";
$MESS["CRM_TAX_DELETE_CONFIRM"] = "Êtes-vous sûr de vouloir supprimer '%s' ?";
$MESS["CRM_TAX_DELETE_TITLE"] = "Supprimer cet impôt";
$MESS["CRM_TAX_EDIT"] = "Modifier l'impôt";
$MESS["CRM_TAX_EDIT_TITLE"] = "Accéder à la page de l'édition de cet impôt";
$MESS["CRM_TAX_LOCATIONS"] = "Emplacements";
$MESS["CRM_TAX_LOCATIONS_CONTENT"] = "Veuillez créer ou importer des emplacements avant de créer des taxes.";
$MESS["CRM_TAX_LOCATIONS_CREATE"] = "Créer";
$MESS["CRM_TAX_LOCATIONS_IMPORT"] = "Importer";
$MESS["CRM_TAX_LOCATIONS_REDIRECT"] = "Aller";
$MESS["CRM_TAX_SHOW"] = "Passage à l'examen de la taxe";
$MESS["CRM_TAX_SHOW_TITLE"] = "Passer à la page de consultation de ce taux de TVA";
?>