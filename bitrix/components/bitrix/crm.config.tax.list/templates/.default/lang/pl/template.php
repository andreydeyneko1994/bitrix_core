<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_TAX_DELETE"] = "Usuń podatek";
$MESS["CRM_TAX_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_TAX_DELETE_TITLE"] = "Usuń ten podatek";
$MESS["CRM_TAX_EDIT"] = "Edytuj podatek";
$MESS["CRM_TAX_EDIT_TITLE"] = "Otwórz ten podatek do edycji";
$MESS["CRM_TAX_LOCATIONS"] = "Lokalizacje";
$MESS["CRM_TAX_LOCATIONS_CONTENT"] = "Proszę utworzyć lub zaimportować lokalizacje przed utworzeniem podatków.";
$MESS["CRM_TAX_LOCATIONS_CREATE"] = "Utwórz";
$MESS["CRM_TAX_LOCATIONS_IMPORT"] = "Importuj";
$MESS["CRM_TAX_LOCATIONS_REDIRECT"] = "Idź";
$MESS["CRM_TAX_SHOW"] = "Wyświetl podatek";
$MESS["CRM_TAX_SHOW_TITLE"] = "Wyświetl ten podatek";
?>