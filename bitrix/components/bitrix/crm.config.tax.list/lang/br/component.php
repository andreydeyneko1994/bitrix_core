<?
$MESS["CRM_COLUMN_CODE"] = "Código Mnemônico";
$MESS["CRM_COLUMN_DATE"] = "Modificado em";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Nome/Descrição";
$MESS["CRM_COLUMN_RATES"] = "Taxas de impostos";
$MESS["CRM_COLUMN_SITE"] = "Site";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "O módulo e-Store não está instalado.";
$MESS["CRM_TAX_DELETION_GENERAL_ERROR"] = "Ocorreu um erro ao deletar o imposto.";
$MESS["CRM_TAX_UPDATE_GENERAL_ERROR"] = "Ocorreu um erro ao atualizar o imposto.";
?>