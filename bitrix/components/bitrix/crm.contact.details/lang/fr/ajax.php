<?php
$MESS["CRM_CONTACT_DELETION_ERROR"] = "Erreur lors de la suppression du contact.";
$MESS["CRM_CONTACT_NOT_FOUND"] = "Contact introuvable.";
