<?php
$MESS["IMOL_PERM_RESTRICTION"] = "Veuillez passer sur une <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">offre commerciale</a> pour assigner les droits d'accès aux employés.";
$MESS["IMOL_ROLE_ACTION"] = "Action";
$MESS["IMOL_ROLE_ENTITY"] = "Entité";
$MESS["IMOL_ROLE_LABEL"] = "Rôle";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Certaines limites sont d'application. Cliquez ici pour en savoir plus.";
$MESS["IMOL_ROLE_PERMISSION"] = "Autorisation";
$MESS["IMOL_ROLE_SAVE"] = "Enregistrer";
