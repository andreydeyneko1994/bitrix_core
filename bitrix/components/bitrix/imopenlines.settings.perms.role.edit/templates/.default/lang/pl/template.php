<?php
$MESS["IMOL_PERM_RESTRICTION"] = "Aby przypisywać uprawnienia dostępu pracownikom, rozszerz subskrypcję do jednego z <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\">planów komercyjnych</a>.";
$MESS["IMOL_ROLE_ACTION"] = "Działanie";
$MESS["IMOL_ROLE_ENTITY"] = "Jednostka";
$MESS["IMOL_ROLE_LABEL"] = "Rola";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Obowiązują pewne ograniczenia. Kliknij tutaj, by dowiedzieć się więcej.";
$MESS["IMOL_ROLE_PERMISSION"] = "Zgoda";
$MESS["IMOL_ROLE_SAVE"] = "Zapisz";
