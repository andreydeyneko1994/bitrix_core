<?
$MESS["TASKS_NOTIFY_TASK_DELETED"] = "Se ha eliminado la plantilla";
$MESS["TASKS_TEMPLATE_POPUP_MENU_CHECKLIST_SECTION"] = "Lista de verificación";
$MESS["TASKS_TEMPLATE_POPUP_MENU_SHOW_COMPLETED"] = "Mostrar los elementos completados de la lista de verificación";
$MESS["TASKS_TEMPLATE_POPUP_MENU_SHOW_ONLY_MINE"] = "Mostrar los elementos de la lista de verificación que sean relevantes solo para mí";
$MESS["TASKS_TTTV_TEMPLATE_LCF"] = "plantilla de tarea";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_BUTTON_CANCEL"] = "Cancelar";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_BUTTON_CLOSE"] = "Cerrar";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_CONTENT"] = "Los cambios no se guardaron. ¿Seguro que desea cerrar el editor?";
$MESS["TASKS_TTV_CLOSE_SLIDER_CONFIRMATION_POPUP_HEADER"] = "Confirmar la acción";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_BUTTON_NO"] = "No";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_BUTTON_YES"] = "Sí";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_CONTENT"] = "¿Seguro que desea cancelar los cambios?";
$MESS["TASKS_TTV_DISABLE_CHANGES_CONFIRMATION_POPUP_HEADER"] = "Confirmar la acción";
?>