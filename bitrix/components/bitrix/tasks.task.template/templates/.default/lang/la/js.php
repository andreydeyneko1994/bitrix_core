<?
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_MULTIPLE_RESPONSIBLE_NOTICE"] = "Se creará una tarea separada para cada persona responsable";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_NO_PARENT_TEMPLATE_NOTICE"] = "La plantilla de tarea no puede tener una plantilla principal si la opción '#TPARAM_FOR_NEW_USER#' está chequeada.";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_CHECKLIST_SECTION"] = "Lista de verificación";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_SHOW_COMPLETED"] = "Mostrar los elementos completados de la lista de verificación";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_POPUP_MENU_SHOW_ONLY_MINE"] = "Mostrar los elementos de la lista de verificación que sean relevantes solo para mí";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_CHECKLIST_ADD_NEW_CHECKLIST"] = "Crear nuevo";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_CHECKLIST_HINT"] = "Seleccione en la descripción de la plantilla el texto que desea agregar a la lista de verificación";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TPARAM_TYPE"] = "Plantilla de tareas para un nuevo empleado";
?>