<?
$MESS["LEARNING_COURSES_LESSON_DELETE"] = "Usuń lekcję";
$MESS["LEARNING_COURSES_LESSON_DELETE_CONF"] = "Spowoduje to usunięcie wszystkich informacji powiązanych z tą lekcją! Kontynuować?";
$MESS["LEARNING_COURSES_LESSON_EDIT"] = "Edytuj lekcję";
$MESS["LEARNING_COURSES_LESSON_VIEW"] = "Podgląd lekcji";
$MESS["LEARNING_COURSES_QUEST_M_ADD"] = "Dodaj nowe pytanie (wielokrotnego wyboru)";
$MESS["LEARNING_COURSES_QUEST_S_ADD"] = "Dodaj nowe pytanie (jednokrotnego wyboru)";
$MESS["LEARNING_COURSE_DENIED"] = "Nie znaleziono kursu szkoleniowego lub odmowa dostępu.";
$MESS["LEARNING_LESSON_DENIED"] = "Nie znaleziono lekcji lub odmowa dostępu.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
?>