<?
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Sprawdź możliwe uprawnienia";
$MESS["LEARNING_COURSE_ID"] = "ID Kursu";
$MESS["LEARNING_DESC_NO"] = "Nie";
$MESS["LEARNING_DESC_YES"] = "Tak";
$MESS["LEARNING_LESSON_ID_NAME"] = "ID lekcji";
$MESS["LEARNING_PATH_TO_USER_PROFILE"] = "URL strony profilu użytkownika";
$MESS["LEARNING_SELF_TEST_TEMPLATE_NAME"] = "URL strony samodzielnego testu";
?>