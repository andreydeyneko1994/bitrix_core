<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "Moduł zadań nie jest zainstalowany.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Już wykorzystujesz śledzenie czasu dla \"{{TITLE}}\". To zadanie zostanie zatrzymane. Kontynuować?";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Śledzenie czasu jest teraz wykorzystywane przy innym zadaniu.";
?>