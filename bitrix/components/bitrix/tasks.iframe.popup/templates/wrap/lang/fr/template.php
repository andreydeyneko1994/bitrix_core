<?
$MESS["TASKS_TIP_TEMPLATE_COPY_CURRENT_URL"] = "Copier le lien de la tâche dans le Presse-papiers";
$MESS["TASKS_TIP_TEMPLATE_LINK_COPIED"] = "Le lien de la tâche a été copié dans le Presse-papiers";
$MESS["TASKS_TIP_TEMPLATE_LINK_COPIED_TEMPLATE_BAR_TITLE"] = "Modèles de tâche";
?>