<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_LEAD_LIST_ADD_DEAL"] = "Adicionar atividade";
$MESS["M_CRM_LEAD_LIST_CALL"] = "Fazer uma chamada";
$MESS["M_CRM_LEAD_LIST_CREATE_BASE"] = "Criar usando fonte";
$MESS["M_CRM_LEAD_LIST_CREATE_DEAL"] = "Negócio";
$MESS["M_CRM_LEAD_LIST_DECLINE"] = "Rejeitar";
$MESS["M_CRM_LEAD_LIST_DELETE"] = "Excluir";
$MESS["M_CRM_LEAD_LIST_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "Todos os Leads";
$MESS["M_CRM_LEAD_LIST_JUNK"] = "Descartado";
$MESS["M_CRM_LEAD_LIST_MAIL"] = "Enviar mensagem";
$MESS["M_CRM_LEAD_LIST_MEETING"] = "Marcar uma reunião";
$MESS["M_CRM_LEAD_LIST_MORE"] = "Mais";
$MESS["M_CRM_LEAD_LIST_NO_FILTER"] = "Todos os Lead";
$MESS["M_CRM_LEAD_LIST_PRESET_MY"] = "Meus Leads";
$MESS["M_CRM_LEAD_LIST_PRESET_NEW"] = "Novo Lead";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
?>