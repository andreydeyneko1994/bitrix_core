<?
$MESS["CRM_ACTIVITY_CURRENT"] = "Activité";
$MESS["CRM_ACTIVITY_DLG_BTN_CANCEL"] = "Quitter sans sauvegarde";
$MESS["CRM_ACTIVITY_EXECUTE"] = "Exécuter";
$MESS["CRM_ACTIVITY_HISTORY"] = "Journal";
$MESS["CRM_ACTIVITY_LIST_ADD_CALL"] = "Ajouter un appel";
$MESS["CRM_ACTIVITY_LIST_ADD_EMAIL"] = "Écrire un message";
$MESS["CRM_ACTIVITY_LIST_ADD_MEETING"] = "Ajouter une rencontre";
$MESS["CRM_ACTIVITY_LIST_ADD_TASK"] = "Ajouter une tâche";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_DEAD_LINE"] = "Date limite";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_PRIORITY"] = "Criticité";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_RESPONSIBLE"] = "Personne responsable";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_SUBJECT"] = "Dénomination";
$MESS["CRM_ACTIVITY_ROW_COL_TTL_TYPE"] = "Type";
$MESS["CRM_ACTIVITY_SHOW_ALL"] = "Afficher tout (#COUNT#)";
$MESS["CRM_CALL_STATUS_COMPLETED"] = "Achevé(e)s";
$MESS["CRM_CALL_STATUS_WAITING"] = "Attend l'exécution";
$MESS["CRM_FF_CANCEL"] = "Annuler";
$MESS["CRM_FF_CHANGE"] = "Éditer";
$MESS["CRM_FF_CLOSE"] = "Fermer";
$MESS["CRM_FF_LAST"] = "Dernier";
$MESS["CRM_FF_NO_RESULT"] = "Malheureusement, il n'y a pas d'éléments retrouvés d'après votre demande de recherche.";
$MESS["CRM_FF_OK"] = "Sélectionner";
$MESS["CRM_FF_SEARCH"] = "Rechercher";
$MESS["CRM_MEETING_STATUS_COMPLETED"] = "Achevé(e)s";
$MESS["CRM_MEETING_STATUS_WAITING"] = "Attend l'exécution";
$MESS["CRM_UNDEFINED_VALUE"] = "[non indiqué]";
?>