<?
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Ukończono przetwarzanie danych statystycznych dla działań. Przetworzone działania: #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Nie ma potrzeby aktualizacji statystyk działań.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Przetworzone działania: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "Indeks wyszukiwania działań został odtworzony. Przetworzone działania: #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "Indeks wyszukiwania działań nie wymaga odtworzenia.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Przetworzone działania: #PROCESSED_ITEMS# z #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Suma: #ROW_COUNT#";
?>