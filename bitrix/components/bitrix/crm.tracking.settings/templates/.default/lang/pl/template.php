<?
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW"] = "Okno atrybucji";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_DAYS"] = "dni";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE"] = "Użyj okna atrybucji dla leadów i deali";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE_HINT"] = "Leady i deale utworzone poza witryną otrzymają to samo źródło, co kontakt lub powiązana firma, jeśli data rejestracji nie poprzedza okna atrybucji.";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE"] = "Okno atrybucji";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE_HINT"] = "Liczba dni, w których dana osoba po raz pierwszy wchodzi w interakcję z kampanią reklamową, a następnie wykonuje akcję śledzoną przez Bitrix24, nazywa się oknem atrybucji. W razie potrzeby możesz zmienić czas trwania okresu atrybucji, aby zwiększyć dokładność raportu.";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN"] = "Domeny linkujące";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN_USE"] = "Dodaj źródło ruchu w sieci społecznościowej bez tagu utm_source";
?>