<?
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW"] = "Janela de atribuição";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_DAYS"] = "dias";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE"] = "Use a janela de atribuição para leads e negócios";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE_HINT"] = "Os leads e negócios criados fora do tráfego do site herdarão a fonte do contato ou da empresa associada a eles, se a data de cadastro não preceder à janela de atribuição.";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE"] = "Janela de atribuição";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE_HINT"] = "O número de dias entre o momento em que uma pessoa interagiu com a sua campanha publicitária pela primeira vez e o momento quando essa pessoa executou uma ação rastreada pelo Bitrix24 é chamado de janela de atribuição. Você pode ajustar o período de atribuição conforme necessário para aumentar a precisão dos relatórios.";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN"] = "Domínios referentes";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN_USE"] = "Adicionar fonte para o tráfego de redes sociais sem o parâmetro utm_source";
?>