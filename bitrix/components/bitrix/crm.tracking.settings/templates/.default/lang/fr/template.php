<?
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW"] = "Fenêtre d'attribution";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_DAYS"] = "jours";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE"] = "Utiliser la fenêtre d'attribution pour les prospects et les transactions";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE_HINT"] = "Les transactions et les prospects que vous créez en dehors du site héroteront la même source que le contact/la société, à moins que la date d'inscription ne soit antérieure à la fenêtre d'attribution.";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE"] = "Fenêtre d'attribution";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE_HINT"] = "Le nombre de jours entre la première interaction d'une personne avec votre campagne publicitaire et une action de cette personne effectuée par la suite et suivie par Bitrix24 est appelé la fenêtre d'attribution. Vous pouvez changer à volonté la durée de la période d'attribution pour augmenter la précision du rapport.";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN"] = "Domaines référents";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN_USE"] = "Ajouter une source pour le trafic de réseau social sans tag utm_source";
?>