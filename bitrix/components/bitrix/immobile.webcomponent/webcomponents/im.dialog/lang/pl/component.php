<?php
$MESS["CHAT_USER_LIST_EXIT"] = "Wyloguj";
$MESS["CHAT_USER_LIST_KICK"] = "Usuń";
$MESS["CHAT_USER_LIST_OWNER"] = "Ustaw jako właściciela";
$MESS["COMPONENT_LOADING_MESSAGES"] = "Załaduj wiadomości...";
$MESS["IM_M_CHAT_KICK"] = "Usuń z czatu";
