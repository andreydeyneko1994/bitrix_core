<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Proceso de negocio no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Moneda no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "El módulo REST no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "El formulario se encuentra inactivo.";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "El formulario no se ha encontrado.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "La dirección URL del formulario posiblemente ha cambiado.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_SUBTITLE"] = "Alguien le envió un enlace de vista previa. Inicie sesión en Bitrix24 o pídale al propietario que le envíe un enlace público.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_TITLE"] = "No ha iniciado sesión";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_SUBTITLE"] = "Alguien le envió un enlace de vista previa del formulario. Debe tener el permiso de acceso correspondiente, de lo contrario, pídale al propietario que le envíe un enlace público.";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_TITLE"] = "Acceso denegado";
