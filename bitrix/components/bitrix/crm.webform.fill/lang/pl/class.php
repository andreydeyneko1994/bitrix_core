<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "Moduł Rest nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "Formularz jest nieaktywny.";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "Formularz nie został znaleziony.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "URL formularza prawdopodobnie został zmieniony.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_SUBTITLE"] = "Ktoś wysłał Ci link do podglądu formularza. Zaloguj się do Bitrix24 lub poproś właściciela o link publiczny.";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_TITLE"] = "Nie jesteś zalogowany";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_SUBTITLE"] = "Ktoś wysłał Ci link do podglądu formularza. Musisz mieć odpowiednie uprawnienia dostępu. W przeciwnym razie poproś właściciela o link publiczny.";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_TITLE"] = "Odmowa dostępu";
