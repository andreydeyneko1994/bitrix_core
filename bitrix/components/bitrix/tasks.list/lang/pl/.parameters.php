<?
$MESS["INTL_ACTION_VAR"] = "Działanie ID zmienna nazwa";
$MESS["INTL_GROUP_VAR"] = "Grpa ID zmienna nazwa";
$MESS["INTL_ITEM_COUNT"] = "Przedmiotów na stronę";
$MESS["INTL_NAME_TEMPLATE"] = "Szablon wyśweitlania nazwy";
$MESS["INTL_OWNER_ID"] = "Właściciel ID";
$MESS["INTL_PAGE_VAR"] = "Strona ID zmienna nazwa";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Ścieżka grupy zadań";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Ścieżka do grupy zadań";
$MESS["INTL_PATH_TO_GROUP_TASKS_VIEW"] = "Ścieżka widoku zadania";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Ścieżka zadań użytkownika";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Ścieżka do zadań użytkownika";
$MESS["INTL_SET_NAVCHAIN"] = "Włącz ścieżkę";
$MESS["INTL_TASK_TYPE"] = "Typ Zadania";
$MESS["INTL_TASK_TYPE_GROUP"] = "Dla grupy";
$MESS["INTL_TASK_TYPE_USER"] = "Dla użytkownika";
$MESS["INTL_TASK_VAR"] = "Zadanie ID zmienna nazwa";
$MESS["INTL_USER_VAR"] = "Użytkownik ID zmienna nazwa";
$MESS["INTL_VARIABLE_ALIASES"] = "Zmienne aliasów";
$MESS["INTL_VIEW_VAR"] = "Widok ID zmienna nazwa";
?>