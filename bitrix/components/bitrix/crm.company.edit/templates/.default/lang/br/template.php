<?
$MESS["CRM_COMPANY_CREATE_TITLE"] = "Nova empresa";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "por dados bancários";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "por e-mail";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "por telefone";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "por informações da empresa";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "encontrado";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Coincidências encontradas";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_TTL_SUMMARY_TITLE"] = "por nome";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorar e salvar";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Cancelar";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Possíveis duplicados";
$MESS["CRM_COMPANY_EDIT_TITLE"] = "Empresa ##ID# &mdash; #TITLE#";
$MESS["CRM_TAB_1"] = "Empresa";
$MESS["CRM_TAB_1_TITLE"] = "Propriedades da empresa";
$MESS["CRM_TAB_2"] = "Registro";
$MESS["CRM_TAB_2_TITLE"] = "Registro da empresa";
$MESS["CRM_TAB_3"] = "Processos de negócio";
$MESS["CRM_TAB_3_TITLE"] = "Processos de negócios da empresa";
?>