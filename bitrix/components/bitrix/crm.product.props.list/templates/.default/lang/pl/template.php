<?
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_PRODUCTPROP_DELETE"] = "Usuń";
$MESS["CRM_PRODUCTPROP_DELETE_CONFIRM"] = "Na pewno chcesz usunąć '% s'?";
$MESS["CRM_PRODUCTPROP_DELETE_TITLE"] = "Usuń tę właściwość";
$MESS["CRM_PRODUCTPROP_EDIT"] = "Edytuj";
$MESS["CRM_PRODUCTPROP_EDIT_TITLE"] = "Otwórz tę właściwość dla edycji";
?>