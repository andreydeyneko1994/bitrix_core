<?
$MESS["CACHE_TIME_TIP"] = "W tym miejscu określ okres czasu, w trakcie którego pamięć podręczna jest aktualna.";
$MESS["CHAPTER_DETAIL_TEMPLATE_TIP"] = "Określ ścieżkę do strony rozdziału kursu.";
$MESS["CHAPTER_ID_TIP"] = "Wyrażenie, które które kwalifikuje do rozdziału ID.";
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["LESSON_DETAIL_TEMPLATE_TIP"] = "Określ ścieżkę do strony lekcji kursu.";
$MESS["SET_TITLE_TIP"] = "Sprawdzenie tej opcji wstawi tytuł strony do nazwy rozdziału.";
?>