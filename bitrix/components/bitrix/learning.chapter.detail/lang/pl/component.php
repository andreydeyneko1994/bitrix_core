<?
$MESS["LEARNING_CHAPTER_DENIED"] = "Nie znaleziono rozdziału lub odmowa dostępu.";
$MESS["LEARNING_COURSES_CHAPTER_ADD"] = "Dodaj nowy rozdział";
$MESS["LEARNING_COURSES_CHAPTER_DELETE"] = "Usuń rozdział";
$MESS["LEARNING_COURSES_CHAPTER_DELETE_CONF"] = "Spowoduje to usunięcie wszystkich informacji związanych z tym rozdziałem! Kontynuować?";
$MESS["LEARNING_COURSES_CHAPTER_EDIT"] = "Edytuj rozdział";
$MESS["LEARNING_COURSES_LESSON_ADD"] = "Dodaj nową lekcję";
$MESS["LEARNING_COURSE_DENIED"] = "Nie znaleziono kursu szkoleniowego lub odmowa dostępu.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
?>