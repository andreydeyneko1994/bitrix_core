<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_MENU_TITLE"] = "Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_SETUP_FIRST_WARNING"] = "Conecte o Facebook ao Canal Aberto para usar o catálogo de produtos em bate-papos ao vivo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_TITLE"] = "Catálogo de produtos do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Pode ser editado ou desligado a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Facebook conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Ocorreu um erro. Verifique suas configurações.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Conecte a página do Facebook de sua empresa aos Canais Abertos e comunique-se com usuários do Facebook via Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila</li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Você precisa para ser autorizado por sua conta do Facebook para modificar as configurações";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que, quando você autorizar com uma conta do Facebook a qual não administra a página atual, ela será desconectada do seu Canal Aberto Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_ADDITIONAL_DESCRIPTION"] = "Você terá que criar uma página pública no Facebook ou conectar a que já tem. Apenas o administrador da página pode conectá-la ao Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_1"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_2"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_3"] = "Responda aos seus clientes quando e onde eles preferirem";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_4"] = "As consultas dos clientes são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_SUBTITLE"] = "Conecte sua página pública do Facebook ao seu Bitrix24 para receber as consultas que seus clientes fazem no Facebook. Responda mais rápido e melhore a conversão.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_TITLE"] = "Responda as perguntas dos seus clientes do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Autorize a conta do Facebook que gerencia suas páginas para receber mensagens dos seus clientes Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "Está faltando uma página obrigatória? Encontre o possível motivo #A#neste artigo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_ALERT"] = "Você precisa habilitar os produtos nos Canais Abertos antes de conectar o catálogo de produtos.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT_HELP"] = "Clique em \"#BUTTON#\" e siga as instruções.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DESCRIPTION"] = "Todos os produtos são sincronizados automaticamente com o seu catálogo do Facebook.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DIFFERENT_IDS"] = "Para tratar adequadamente os produtos, indique a página do Facebook especificamente ligada aos Canais Abertos.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DISCONNECT"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TEXT"] = "Clique em \"#BUTTON#\" e selecione a #A_START#permissão do catálogo#A_END# na janela pop-up.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TITLE"] = "Deixe seu Bitrix24 enviar seleções de produtos no estilo do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_REMOVE"] = "Você realmente deseja desconectar o catálogo?";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_SUCCESS"] = "Catálogo conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_TITLE"] = "Conectar o catálogo de produtos ao Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Outras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Escolha a página do Facebook que deve ser conectada ao seu Canal Aberto Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não administra nenhuma página do Facebook.<br>Você pode criar sua página do Facebook agora mesmo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Comunique-se com usuários do Facebook via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT"] = "Prorrogue a janela de mensagens de 24 horas para 7 dias";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Observe que o Facebook não permite o envio de anúncios (incluindo descontos, cupons) ou mensagens automáticas para os clientes após a expiração da janela de 24 horas. Sua conta será bloqueada se você não seguir essas diretrizes. #START_HELP_DESC#Detalhes#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_OFF"] = "A janela de mensagens não foi prorrogada de 24 horas para 7 dias";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_ON"] = "Janela de mensagens prorrogada de 24 horas para 7 dias";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Conta";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_SUCCESS"] = "Acesso concedido.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_WRONG_USER"] = "Erro ao solicitar permissões de acesso ao catálogo. Essa solicitação deve ser originada de um usuário que conectou o Facebook em primeiro lugar.";
