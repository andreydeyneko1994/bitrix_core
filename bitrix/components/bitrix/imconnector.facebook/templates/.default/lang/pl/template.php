<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Zaloguj";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Zaloguj";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_MENU_TITLE"] = "Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_SETUP_FIRST_WARNING"] = "Aby korzystać z katalogu produktów w czatach na żywo, połącz Facebooka z Otwartym kanałem.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_TITLE"] = "Katalog produktów na Facebooku";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub wyłączyć";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Zmień";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Podłączono Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Strona połączona";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd. Proszę sprawdzić ustawienia.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Odłącz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Podłącz stronę firmy na Facebooku do Otwartych kanałów i komunikuj się z użytkownikami Facebooka za pomocą Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatyczne rozsyłanie wiadomości przychodzących zgodnie z regułami kolejki</li>

     <li class=\"im-connector-settings-header-list-item\">znany interfejs czatu Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">wszystkie konwersacje są zapisywane w historii systemu CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Aby modyfikować ustawienia konieczna jest autoryzacja za pomocą konta na Facebooku";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Uwaga: autoryzacja za pomocą konta na Facebooku, które administruje niniejszej strony, spowoduje odłączenie go od twojego Otwartego kanału Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_ADDITIONAL_DESCRIPTION"] = "Należy stworzyć publiczną stronę na Facebooku lub podłączyć już posiadaną. Wyłącznie administrator strony może podłączyć ją do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_3"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_4"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_SUBTITLE"] = "Podłącz swoją publiczną stronę na Facebooku do Bitrix24 i otrzymuj zapytania klientów z Facebooka. Odpowiadaj szybciej i poprawiaj konwersję.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_TITLE"] = "Odpowiadaj na pytania klientów z Facebooka";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Prosimy o autoryzację za pomocą konta na Facebooku, które służy do zarządzania twoimi stronami, w celu odbierania wiadomości od klientów Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "Brakuje wymaganej strony? Możliwe przyczyny znajdziesz w #A#tym artykule#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_ALERT"] = "Przed podłączeniem katalogu produktów należy włączyć produkty w Otwartych kanałach.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CANCEL"] = "Anuluj";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT"] = "Połącz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT_HELP"] = "Kliknij \"#BUTTON#\" i postępuj według instrukcji.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DESCRIPTION"] = "Wszystkie produkty są automatycznie synchronizowane z Twoim katalogiem na Facebooku.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DIFFERENT_IDS"] = "Dla prawidłowej obsługi produktów wskaż stronę na Facebooku połączoną z Otwartymi kanałami.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DISCONNECT"] = "Rozłącz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TEXT"] = "Kliknij \"#BUTTON#\" i wybierz #A_START#uprawnienie do katalogu#A_END# w wyskakującym okienku.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TITLE"] = "Pozwól, aby Twój Bitrix24 wysyłał wybrane produkty w stylu Facebooka";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_REMOVE"] = "Czy na pewno chcesz odłączyć katalog?";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_SUCCESS"] = "Podłączono katalog";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_TITLE"] = "Połącz katalog produktów z Facebookiem";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Inne strony";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Strona";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Komunikator";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Proszę wybrać stronę na Facebooku, która ma zostać podłączona do Otwartego kanału Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Nie administrujesz żadnej strony na Facebooku.<br>Możesz teraz utworzyć swoją stronę na Facebooku.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Komunikuj się z użytkownikami Facebooka przez Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT"] = "Wydłuż okno wiadomości z 24 godzin do 7 dni";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Pamiętaj, że Facebook nie zezwala na wysyłanie reklam (w tym rabatów, kuponów) ani automatycznych wiadomości do klientów po wygaśnięciu 24-godzinnego okna. Jeśli nie zastosujesz się do tych wytycznych, Twoje konto zostanie zablokowane. #START_HELP_DESC#Szczegóły#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_OFF"] = "Okno wiadomości nie zostało przedłużone z 24 godzin do 7 dni";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_ON"] = "Wydłużenie okna wiadomości z 24 godzin do 7 dni";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Utwórz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Konto";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN"] = "Połącz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_SUCCESS"] = "Przyznano dostęp.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_WRONG_USER"] = "Błąd podczas żądania uprawnień dostępu do katalogu. Żądanie musi pochodzić od użytkownika, który najpierw podłączył Facebooka.";
