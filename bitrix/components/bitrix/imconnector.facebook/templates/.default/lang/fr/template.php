<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZATION"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_AUTHORIZE"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_MENU_TITLE"] = "Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_SETUP_FIRST_WARNING"] = "Veuillez connecter Facebook à un Canal ouvert pour utiliser le catalogue de produits dans les chats en direct.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CATALOG_TAB_TITLE"] = "Catalogue de produits Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_ANY_TIME"] = "Peut être édité ou désactivé à tout moment";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED"] = "Facebook connecté";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Page connectée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos paramètres.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Dissocier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Connectez la page Facebook de votre entreprise aux canaux ouverts et communiquez avec les utilisateurs de Facebook via Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente</li>

     <li class=\"im-connector-settings-header-list-item\">interface de chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Vous devez être autorisé à modifier les paramètres avec votre compte Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Veuillez noter que lorsque vous autorisez un compte Facebook qui n'est pas l'administrateur de la page actuelle, celui-ci sera déconnecté de votre Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_ADDITIONAL_DESCRIPTION"] = "Vous devrez créer une page Facebook publique ou connecter celle que vous avez déjà. Seul l'administrateur de la page peut la connecter à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_3"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_LIST_ITEM_4"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_SUBTITLE"] = "Connectez votre page Facebook publique à votre Bitrix24 pour recevoir les demandes de renseignements que vos clients font sur Facebook. Répondez plus rapidement et améliorez la conversion.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_TITLE"] = "Répondez aux questions de vos clients Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Veuillez procéder à l'autorisation via un compte Facebook qui gère vos pages pour recevoir les messages de vos clients Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_SPECIFIC_PAGE"] = "Il manque une page obligatoire ? Trouvez la raison possible dans #A#cet article#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_ALERT"] = "Vous devez activer les produits dans Canaux ouverts avant de connecter le catalogue de produits.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CANCEL"] = "Annuler";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_CONNECT_HELP"] = "Cliquez sur \"#BUTTON#\" et suivez les instructions.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DESCRIPTION"] = "Tous les produits sont automatiquement synchronisés avec votre catalogue Facebook.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DIFFERENT_IDS"] = "Pour traiter correctement les produits, veuillez indiquer la page Facebook spécifiquement liée aux Canaux ouverts.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_DISCONNECT"] = "Déconnecter";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TEXT"] = "Cliquez sur \"#BUTTON#\" et sélectionnez #A_START#droits du catalogue#A_END# dans la fenêtre contextuelle.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_PERMISSION_TITLE"] = "Autorisez votre Bitrix24 à envoyer des sélections de produits de style Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_REMOVE"] = "Voulez-vous déconnecter le catalogue ?";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_SUCCESS"] = "Catalogue connecté";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OPENLINES_CATALOG_TITLE"] = "Connecter le catalogue de produits à Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Autres pages";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messagerie";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Veuillez choisir la page Facebook devant être connectée à votre Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'êtes administrateur d'aucune page Facebook.<br>Vous pouvez dès maintenant créer votre page Facebook.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TITLE"] = "Communiquez avec les utilisateurs Facebook via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT"] = "Extension de la fenêtre de messagerie de 24 heures à 7 jours";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_DESCRIPTION"] = "Notez que Facebook ne permet pas d'envoyer des publicités (y compris des remises, des coupons) ou des messages automatisés aux clients une fois que la fenêtre de 24 heures a expiré. Votre compte sera verrouillé si vous ne suivez pas ces directives. #START_HELP_DESC#Détails#END_HELP_DESC#";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_OFF"] = "La fenêtre de messagerie n'a pas été étendue de 24 heures à 7 jours";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CONNECT_HUMAN_AGENT_ON"] = "Fenêtre de messagerie étendue de 24 heures à 7 jours";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER"] = "Compte";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_SUCCESS"] = "Accès accordé";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_USER_RELOGIN_WRONG_USER"] = "Erreur lors de la demande des droits d'accès au catalogue. Cette demande doit provenir d'un utilisateur qui s'est connecté à Facebook en premier lieu.";
