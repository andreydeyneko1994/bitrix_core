<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Błąd rejestracji";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INVALID_OAUTH_ACCESS_TOKEN"] = "Nie można zarządzać stroną publiczną z powodu utraty dostępu. Należy ponownie połączyć się ze swoją stroną publiczną.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MENU_TAB_CATALOG"] = "Katalog produktów";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MENU_TAB_OPEN_LINES"] = "Otwarte Kanały";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MODULE_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "Nie można odłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_DEL_PAGE"] = "Nie można odłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_DEL_USER"] = "Nie można odłączyć konta użytkownika";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "Strona została podłączona";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_DEL_PAGE"] = "Strona została odłączona";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_DEL_USER"] = "Konto użytkownika zostało odłączone";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "Ten konektor został skonfigurowany do użytku w ramach grupy roboczej, do której obecnie nie masz dostępu na prawach administratora.<br>
Prosimy o ponowną konfigurację konektora.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REPEATING_ERROR"] = "Jeśli problem będzie się powtarzał, może być konieczne odłączenie kanału i jego ponowna konfiguracja.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Proszę prześlij formularz ponownie.";
