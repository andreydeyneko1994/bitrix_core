<?
$MESS["CRM_IMPORT_AGAIN"] = "Importer un autre fichier";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Cliquez pour importer un autre fichier";
$MESS["CRM_IMPORT_CANCEL"] = "Annuler";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Ne pas continuer et retourner à la liste des contacts";
$MESS["CRM_IMPORT_DONE"] = "Prêt";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Accéder à la liste des contacts";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Continuer >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Passer à l'étape suivante";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Retour";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Revenir à l'étape précédente";
$MESS["CRM_IMPORT_SNS"] = "Mis à part l'importation typique, vous pouvez importer en utilisant vCard.<br> Pour cela faire dans <b>MS Outlook</b>, allez à Contacts et sélectionnez les cartes de visite à envoyer. Sous Actions, choisissez <b>envoyer en tant que carte de visite <b>send as business card</b> et spécifier l'adresse e-mail du destinataire <b>%EMAIL%</b>";
$MESS["CRM_TAB_1"] = "Paramètres d'importation";
$MESS["CRM_TAB_1_TITLE"] = "Modifier les paramètres d'importation";
$MESS["CRM_TAB_2"] = "Correspondance des champs";
$MESS["CRM_TAB_2_TITLE"] = "Réglage de la correspondance des champs";
$MESS["CRM_TAB_3"] = "Importation";
$MESS["CRM_TAB_3_TITLE"] = "Résultat de l'importation";
$MESS["CRM_TAB_DUP_CONTROL"] = "Contrôle des duplicatas";
$MESS["CRM_TAB_DUP_CONTROL_TITLE"] = "Réglage de la gestion des doubles";
?>