<?php
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_MODULE_ABSENT"] = "Le module Catalogue commercial n'est pas installé.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_PRODUCT_ABSENT"] = "Le catalogue de produits CRM est introuvable";
$MESS["CRM_CATALOG_CONTROLLER_ERR_IBLOCK_MODULE_ABSENT"] = "Le module Blocs d'informations n'a pas été installé.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_PAGE_UNKNOWN"] = "Page non valide";
