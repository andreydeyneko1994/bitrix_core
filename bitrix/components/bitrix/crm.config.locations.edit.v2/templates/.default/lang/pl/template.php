<?
$MESS["CRM_CLE2_HEADING_GEO"] = "Deo dane";
$MESS["CRM_CLE2_HEADING_NAMES"] = "Tytuły zależne od języka";
$MESS["CRM_CLE2_NEW_ITEM"] = "Nowe";
$MESS["CRM_CLE2_NOT_SELECTED"] = "Nie wybrano";
$MESS["CRM_CLE2_REMOVE"] = "Usuń";
$MESS["CRM_CLE2_TAB_EXTERNAL"] = "Dane zewnętrzne";
$MESS["CRM_CLE2_TAB_EXTERNAL_MORE"] = "Więcej...";
$MESS["CRM_CLE2_TAB_EXTERNAL_TITLE"] = "Dane zewnętrzne lokalizacji";
$MESS["CRM_CLE2_TAB_PARAMS"] = "Parametry";
$MESS["CRM_CLE2_TAB_PARAMS_TITLE"] = "Parametry lokalizacji";
?>