<?
$MESS["CRM_CLE2_HEADING_GEO"] = "Données géographiques";
$MESS["CRM_CLE2_HEADING_NAMES"] = "Titres dépendant de la langue";
$MESS["CRM_CLE2_NEW_ITEM"] = "Créer";
$MESS["CRM_CLE2_NOT_SELECTED"] = "Non sélectionné";
$MESS["CRM_CLE2_REMOVE"] = "Supprimer";
$MESS["CRM_CLE2_TAB_EXTERNAL"] = "Données externes";
$MESS["CRM_CLE2_TAB_EXTERNAL_MORE"] = "Plus";
$MESS["CRM_CLE2_TAB_EXTERNAL_TITLE"] = "Situation de données externes";
$MESS["CRM_CLE2_TAB_PARAMS"] = "Paramètres";
$MESS["CRM_CLE2_TAB_PARAMS_TITLE"] = "Paramètres de l'emplacement";
?>