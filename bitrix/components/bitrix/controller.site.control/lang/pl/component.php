<?
$MESS["CC_BCSC_DELETE_ERROR"] = "Błąd usuwania strony #ID#.";
$MESS["CC_BCSC_EMAIL_ERROR"] = "Błąd zmiany #ID# e-mail administratora strony: #MESSAGE#";
$MESS["CC_BCSC_ERROR_MODULE"] = "Moduł Sterownika nie jest zainstalowany.";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "Błąd zmiany #ID# hasło strony: #MESSAGE#";
$MESS["CC_BCSC_RESERVE_ERROR"] = "Błąd zastrzeżenia strony: #MESSAGE#.";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "Strona z podanym adresem już istnieje.";
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "Brakuje URL strony lub jest niepoprawny.";
$MESS["CC_BCSC_TITLE"] = "Składowa Zarządzania Stroną.";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "Nieznana grupa.";
$MESS["CC_BCSC_UPDATE_ERROR"] = "Błąd aktualizacji strony #ID#: #MESSAGE#";
?>