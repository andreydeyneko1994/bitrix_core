<?
$MESS["ACCESS_RESTRICTION_TIP"] = "Restringir o acesso";
$MESS["ACTION_TIP"] = "Ação";
$MESS["COMMAND_TIP"] = "Comando";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especifica grupos de usuários com permissões de gerenciamento";
$MESS["SEPARATOR_TIP"] = "Separador de campo";
$MESS["SITE_URL_TIP"] = "URL do site";
?>