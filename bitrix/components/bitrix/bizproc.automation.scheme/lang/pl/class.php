<?php
$MESS["BIZPROC_AUTOMATION_SCHEME_RIGHTS_ERROR"] = "Niewystarczające uprawnienia.";
$MESS["BIZPROC_AUTOMATION_SCHEME_SCHEME_ERROR"] = "Funkcja kopiowania reguł automatyzacji nie jest dostępna dla tego typu dokumentu.";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_ACTION"] = "Wybrano nieznane działanie";
$MESS["BIZPROC_AUTOMATION_SCHEME_UNKNOWN_DOCUMENT"] = "Nieznany typ dokumentu";
