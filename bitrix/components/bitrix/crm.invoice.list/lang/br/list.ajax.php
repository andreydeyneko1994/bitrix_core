<?
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Processamento de dados estatísticos para faturas foi completado. Faturas processadas: #PROCESSED_ITEMS#.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Os dados estatísticos para faturas estão atualizados.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Faturas processadas: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_INVOICE_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
?>