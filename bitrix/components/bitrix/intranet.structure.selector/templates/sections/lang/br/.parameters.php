<?
$MESS["INTR_ISS_TPL_PARAM_COLUMNS"] = "Contagem de Colunas";
$MESS["INTR_ISS_TPL_PARAM_COLUMNS_FIRST"] = "Colunas na Primeira Página";
$MESS["INTR_ISS_TPL_PARAM_MAX_DEPTH"] = "Profundidade máx. da árvore (0 - todos)";
$MESS["INTR_ISS_TPL_PARAM_MAX_DEPTH_FIRST"] = "Profundidade máx. da árvore na primeira página (0 - todos)";
$MESS["INTR_ISS_TPL_PARAM_SHOW_SECTION_INFO"] = "Exibir Informação do Departamento";
?>