<?
$MESS["M_GRID_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_GRID_EMPTY_LIST"] = "La liste est vide.";
$MESS["M_GRID_EMPTY_SEARCH"] = "Aucune entrée n'a été trouvée.";
$MESS["M_GRID_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_GRID_MORE_BUTTON"] = "Plus";
$MESS["M_GRID_PULL_TEXT"] = "Tirez vers le bas pour rafraîchir...";
$MESS["M_GRID_SEARCH"] = "Recherche";
?>