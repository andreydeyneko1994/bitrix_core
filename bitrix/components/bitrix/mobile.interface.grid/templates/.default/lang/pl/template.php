<?
$MESS["M_GRID_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_GRID_EMPTY_LIST"] = "Lista jest pusta.";
$MESS["M_GRID_EMPTY_SEARCH"] = "Nie znaleziono wpisów.";
$MESS["M_GRID_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_GRID_MORE_BUTTON"] = "Więcej";
$MESS["M_GRID_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_GRID_SEARCH"] = "Szukaj";
?>