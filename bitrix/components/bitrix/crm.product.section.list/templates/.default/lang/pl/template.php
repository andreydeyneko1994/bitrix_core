<?
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE"] = "Usuń";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE_PROPMT"] = "Spowoduje to usunięcie wszystkich podsekcji i elementów podrzędnych. Na pewno chcesz usunąć tę sekcję?";
$MESS["CRM_PRODUCT_SECTION_ACTION_RENAME"] = "Zmień nazwę";
$MESS["CRM_SECTION_ADD_BTN_TEXT"] = "Dodaj";
$MESS["CRM_SECTION_ADD_DIALOG_TITLE"] = "Dodaj sekcję";
$MESS["CRM_SECTION_CANCEL_BTN_TEXT"] = "Anuluj";
$MESS["CRM_SECTION_DEFAULT_NAME"] = "Nowa sekcja";
$MESS["CRM_SECTION_EMPTY_NAME_ERROR"] = "Proszę wprowadzić nazwę sekcji.";
$MESS["CRM_SECTION_NAME"] = "Nazwa";
$MESS["CRM_SECTION_NAME_FIELD_TITLE"] = "Nazwa";
$MESS["CRM_SECTION_RENAME_BTN_TEXT"] = "Zmień nazwę";
$MESS["CRM_SECTION_RENAME_DIALOG_TITLE"] = "Zmień nazwę sekcji";
?>