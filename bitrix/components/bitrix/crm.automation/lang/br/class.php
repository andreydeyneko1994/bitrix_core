<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "O acesso à entidade foi negado.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "A automação não está disponível";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "Automação não está disponível no modo CRM Simples";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "O componente não suporta esta entidade CRM.";
$MESS["CRM_AUTOMATION_WEBHOOK_CREATE_FAILURE"] = "Não é possível criar webhook de entrada";
$MESS["CRM_AUTOMATION_WEBHOOK_NOT_AVAILABLE"] = "Webhooks de entrada não podem ser criados no seu plano atual";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
