<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "Odmowa dostępu do jednostki.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "Automatyzacja nie jest dostępna";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "Automatyzacja jest niedostępna w uproszczonym trybie CRM";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "Komponent nie obsługuje tej jednostki CRM.";
$MESS["CRM_AUTOMATION_WEBHOOK_CREATE_FAILURE"] = "Nie można utworzyć webhooka przychodzącego";
$MESS["CRM_AUTOMATION_WEBHOOK_NOT_AVAILABLE"] = "W bieżącym planie nie można tworzyć webhooków przychodzących";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
