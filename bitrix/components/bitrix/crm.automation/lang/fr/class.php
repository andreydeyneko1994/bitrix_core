<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module De processus d'entreprise n'a pas été installé.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "L'accès à l'entité a été refusé.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "L'automatisation n'est pas disponible";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "L'automatisation n'est pas disponible dans le mode CRM simple";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "Le composant ne supporte pas cette entité CRM.";
$MESS["CRM_AUTOMATION_WEBHOOK_CREATE_FAILURE"] = "Impossible de créer un webhook entrant";
$MESS["CRM_AUTOMATION_WEBHOOK_NOT_AVAILABLE"] = "Les webhooks entrants ne peuvent pas être créés avec l'offre que vous utilisez";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
