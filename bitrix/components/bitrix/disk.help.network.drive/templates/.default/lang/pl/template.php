<?
$MESS["DISK_NETWORK_DRIVE_CONNECTOR_HELP_MAPDRIVE"] = "<h3>Mapowanie biblioteki jako dysku sieciowego</h3>
<p>Aby połączyć bibliotekę jako dysk sieciowy użyj <b>managera plików</b>:
<ul>
<li>Uruchom Windows Explorer</b>;</li>
<li>Wybierz <i>Narzędzia > Mapuj dysk sieciowy</i>. Otworzy się kreator dysku sieciowego:
<br /><br /><a href=\"javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/network_storage.png',629,459,'Map Network Drive');\">
<img width=\"250\" height=\"183\" border=\"0\" src=\"#TEMPLATE_FOLDER#/images/en/network_storage_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
<li>W polu <b>Dysk</b>, określ literę, do której mapujesz folder;</li>
<li>W polu <b>Folder</b>, wprowadź ścieżkę do biblioteki: <i>http://&lt;your_server&gt;#TEMPLATE_LINK#</i>. Jeżeli chcesz, aby ten folder był dostępny po uruchomieniu systemu zaznacz opcję <b>Połącz ponownie przy logowaniu</b>;</li>
<li>Kliknij <b>Gotowe</b>. Jeżeli zostaniesz poproszony/a o nazwę użytkownika i hasło, wprowadź je i kliknij</li>
</ul>
</p>
<p>Następnie możesz otworzyć folder w Windows Explorer, gdzie foldr będzie widoczny jako dysk pod Mój Komputer lub w jakimkolwiek managerze plików.</p>";
$MESS["DISK_NETWORK_DRIVE_HELP_OSX"] = "<h3>Podłączanie biblioteki w Mac OS i Mac OS X</h3>
<ul>
<li>Wybierz <i>Finder Go->komenda połącz z serwerem</i>;</li>
<li>Wprowadź do adresu biblioteki w <b>Adres serwera</b>:</p>
<p><a href=\"javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/macos.png',465,550,'Mac OS X');\">
<img width=\"235\" height=\"278\" border=\"0\" src=\"#TEMPLATE_FOLDER#/images/en/macos_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
</ul>";
$MESS["DISK_NETWORK_DRIVE_MACOS_TITLE"] = "Mapuj bibliotekę dokumentów w systemie Mac OS X";
$MESS["DISK_NETWORK_DRIVE_REGISTERPATCH"] = "Obecne preferencje bezpieczeństwa wymagają <a href=\"#LINK#\">dokonania pewnych zmian w Rejestrze</a>, aby połączyć się z dyskiem sieciowym.";
$MESS["DISK_NETWORK_DRIVE_SHAREDDRIVE_TITLE"] = "Pokaż wskazówki do podłączenia jako dysk sieciowy";
$MESS["DISK_NETWORK_DRIVE_USECOMMANDLINE"] = "Aby połączyć bibliotekę jako dysk sieciowy z wykorzystaniem HTTPS/SSL, użyj <b>Start > Uruchom > cmd</b>.  Wpisz następujące komendy w wierszu komend:";
?>