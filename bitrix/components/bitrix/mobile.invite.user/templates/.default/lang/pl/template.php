<?
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Ptwierdź Rejestrację";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Wprowadź adresy e-mail osób, które chcesz zaprosić. Rozdziel wielokrotne wprowadzenia przecinkami lub spacjami.";
$MESS["BX24_INVITE_DIALOG_INVITE"] = "Zaproś";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Gratulacje!</b><br>Zaproszenia zostały wysłane na okreśone adresy e-mail.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Nie możesz zaprosić więcej użytkowników, ponieważ przekracza to limit warunków licencji.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Dołącz do nas na naszym koncie Bitrix24. To miejsce, w którym każdy może komunikować się, współpracować przy zadaniach i projektach, zarządzać klientami i robić wiele więcej.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Treść Zaproszenia";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Zaproś więcej użytkowników";
$MESS["BX24_INVITE_DIALOG_INVITE_TITLE"] = "Zaproś użytkownika";
$MESS["DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["LOAD_TEXT"] = "Aktualizowanie…";
$MESS["PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
?>