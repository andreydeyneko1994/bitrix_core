<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Adres e-mail jest niepoprawny.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Dołącz do nas na naszym koncie Bitrix24. To miejsce, w którym każdy może komunikować się, współpracować przy zadaniach i projektach, zarządzać klientami i robić wiele więcej.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Liczba zaproszonych osób przekracza warunki twojej licencji.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Użytkownik z tym adresem e-mail już istnieje.";
?>