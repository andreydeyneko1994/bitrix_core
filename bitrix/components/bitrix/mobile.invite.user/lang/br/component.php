<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Os endereços de e-mail estão incorretos.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "Junte-se a nós na nossa conta Bitrix24, onde todos podem colaborar em projetos, ter uma comunicação ideal, coordenar tarefas, gerenciar clientes e mais recursos.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "O número de pessoas convidadas excede os termos de licença.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Já existem os usuários com esses endereços de e-mail.";
?>