<?php
$MESS["CRM_TYPE_LIST_WELCOME_LINK"] = "Obtenga más información sobre la Automatización Inteligente de Procesos";
$MESS["CRM_TYPE_LIST_WELCOME_TEXT"] = "Vaya más allá de las negociaciones, prospectos y cotizaciones. Cree y automatice sus propias entidades (listas de proveedores, embudos de compra, registros de documentos, etc.).";
$MESS["CRM_TYPE_LIST_WELCOME_TITLE"] = "Crear una nueva Automatización Inteligente de Procesos";
