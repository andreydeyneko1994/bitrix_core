<?php
$MESS["CRM_TYPE_LIST_WELCOME_LINK"] = "Dowiedz się więcej o Inteligentnej automatyzacji procesów";
$MESS["CRM_TYPE_LIST_WELCOME_TEXT"] = "Sięgaj poza deale, leadów i oferty – twórz i automatyzuj własne obiekty (listy kontrahentów, lejki zakupowe, rejestr dokumentów itp.).";
$MESS["CRM_TYPE_LIST_WELCOME_TITLE"] = "Utwórz nową Inteligentną automatyzację procesów";
