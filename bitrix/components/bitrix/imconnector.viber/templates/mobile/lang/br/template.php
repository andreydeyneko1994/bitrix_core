<?php
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Chave:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "O Viber foi conectado ao seu Canal Aberto. Agora, todas as mensagens publicadas na sua conta pública serão redirecionadas para o seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_2"] = "<br>Agora, você pode acessar <a href='#URL_MOBILE#'>o aplicativo móvel Bitrix24</a> ou <a href='#URL#'>abrir sua conta Bitrix24 no navegador</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link do bate-papo de pessoa para pessoa";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Conta pública conectada";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Cole a chave Viber da Área de Transferência e clique em \"Salvar\".";
