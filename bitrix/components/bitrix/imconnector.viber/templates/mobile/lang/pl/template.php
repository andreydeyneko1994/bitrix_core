<?php
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Klucz:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Usługa Viber jest podłączona do Otwartego kanału. Teraz wszystkie wiadomości wysyłane do konta publicznego będą przekierowywane do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_2"] = "<br>Możesz teraz uzyskać dostęp do <a href='#URL_MOBILE#'>aplikacji mobilnej Bitrix24</a> lub <a href='#URL#'>otworzyć swoje konto Bitrix24 w przeglądarce</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link do czatu osoba-do-osoby";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Podłączone konto publiczne";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Wklej klucz Viber ze schowka i kliknij przycisk Zapisz.";
