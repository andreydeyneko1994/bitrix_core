<?php
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Chave:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHANGE_ANY_TIME"] = "Pode ser editado ou desligado a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTED"] = "Viber conectado";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Quero </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectar</span> uma conta</div>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_STEP"] = "Antes da conexão, você deve <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">criar uma conta Viber pública</a>,
 ou conectar uma já existente. Podemos ajudá-lo a criar uma conta pública e conectá-la ao seu Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_STEP_NEW"] = "Você deve #LINK_START#criar uma conta Viber pública#LINK_END# ou usar a que já tem. Se você não tem uma conta, nós ajudaremos você a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECT_TITLE"] = "Conecte o Viber ao seu Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION"] = "O Viber foi conectado ao seu Canal Aberto. Agora, todas as mensagens publicadas na sua conta pública serão redirecionadas para o seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_ADDITIONAL_DESCRIPTION"] = "Você deve <a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\'); return false;\">criar uma conta pública do Viber</a> ou usar a que já tem. Se você não tem uma conta, nós o ajudaremos a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Você deve #LINK_START#criar uma conta Viber pública#LINK_END# ou usar a que já tem. Se você não tem uma conta, nós ajudaremos você a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_LIST_ITEM_1"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_LIST_ITEM_2"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_LIST_ITEM_3"] = "Responda aos seus clientes quando e onde eles preferirem";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_LIST_ITEM_4"] = "As consultas dos clientes são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_SUBTITLE"] = "Responda aos seus clientes da maneira que eles quiserem. Se um cliente achar o Viber mais conveniente do que outros meios de comunicação, receba as mensagens dele no seu Bitrix24 e responda imediatamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_TITLE"] = "Converse com seus clientes no Viber";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Como obter uma chave de conta pública</span>:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Tenho a chave";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link do bate-papo de pessoa para pessoa";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Nome da conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Conexão de teste";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_ANDROID"] = "<span class=\"imconnector-field-box-text-bold\">APP</span> ANDROID";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_IOS"] = "<span class=\"imconnector-field-box-text-bold\">APP</span> IOS";
$MESS["IMCONNECTOR_COMPONENT_VIBER_USING_TITLE"] = "Estou usando";
