<?php
$MESS["SC_CRM_STORE_CONTAINER_CONTENT_2"] = "Agora o CRM é uma plataforma de vendas online totalmente funcional com métodos modernos de pagamento, recibos e entrega. Um processo de pagamento simples garante que o cliente conclua o pedido.<br>Ative o Pagamento.CRM e veja a conversão crescer!";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Informações";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Comece a vender";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE_2"] = "Assista a este vídeo para saber<br>como o Pagamento.CRM ajudará você a vender mais";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "Uma nova era de vendas online";
$MESS["SC_CRM_STORE_TITLE_2"] = "Pagamento.CRM";
