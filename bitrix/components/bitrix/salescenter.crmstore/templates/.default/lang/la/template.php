<?php
$MESS["SC_CRM_STORE_CONTAINER_CONTENT_2"] = "Ahora CRM es una plataforma de venta en línea totalmente funcional que ofrece métodos de pago, recibos y entregas de forma moderna. Un proceso de pago sencillo garantiza que el cliente complete su pedido.<br>¡Habilite CRM.Payment y observe cómo aumenta la conversión!";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Detalles";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Comience a vender";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE_2"] = "Vea este video para saber <br>cómo CRM.Payment le ayudará a vender más";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "Una nueva época de las ventas en línea";
$MESS["SC_CRM_STORE_TITLE_2"] = "CRM.Pago";
