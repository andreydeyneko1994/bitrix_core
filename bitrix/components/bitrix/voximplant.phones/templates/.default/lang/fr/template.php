<?php
$MESS["VI_PHONES_HELP_1"] = "<p>Connecter les téléphones SIP à votre Bitrix24 pour passer et recevoir des appels maintenant.</p>
<p>Améliorez votre environnement commercial classique en permettant à vos employés d'afficher les informations d'un appelant sur l'écran en utilisant les téléphones communs de bureau.</p>";
$MESS["VI_PHONES_HELP_2"] = "<h3>Votre téléphone de bureau : Aussi simple que 1-2-3 !</h3>
<p>Bitrix24 attribue une adresse de serveur, un identifiant et un mot de passe à chaque utilisateur. Utilisez-les
pour configurer votre téléphone SIP.</p>
<p>Une fois les préférences sauvegardées, votre téléphone est prêt à être utilisé avec Bitrix24.</p>
<p>Pour connecter un téléphone, ouvrez la page #LINK_USERS_START#Utilisateurs de téléphonie#LINK_END#
choisissez un utilisateur et activez la téléphonie pour eux.</p>
#IMAGE_CONNECT#
<p>Comme alternative aux téléphones SIP, vous pouvez utiliser un téléphone DECT fixe commun connecté à
une passerelle SIP qui convertit le signal au format SIP.</p>
<p>Un téléphone portable est une autre option pour ceux qui sont toujours en déplacement. Utilisez votre téléphone Windows,
iOS ou Android en y installant n'importe quelle application softphone.</p>
<p>Veuillez vous référer au #LINK_COURSE_1_START#cours de formation#LINK_END# pour la liste
des appareils vérifiés et des instructions de connexion.</p>
<h3>Utilisation de votre téléphonie</h3>
<p><b>Passer un appel depuis le CRM Bitrix24</b></p>
<p>Ne perdez pas votre temps précieux à appuyer sur des boutons. Cliquez simplement sur Appeler sur le CRM et,
en entendant le son de votre téléphone, décrochez-le. Le téléphone commencera momentanément à composer le numéro
du destinataire. Avec votre ordinateur affichant toujours la fenêtre d'appel, vous pouvez toujours voir
le profil du client ou consulter les informations détaillées.</p>
<p>Vous pouvez toujours le faire à l'ancienne si vous voulez : composez le numéro sur le téléphone et
vous verrez l'écran de l'ordinateur affichant la fenêtre d'appel, comme si vous aviez lancé l'appel
à partir du CRM.</p>
<p><b>Recevoir un appel</b></p>
<p>Un appel entrant atteint votre navigateur (ou l'application de bureau) et le
téléphone en même temps. Vous pouvez répondre sur l'un ou l'autre appareil ; la conversation sera
enregistrée quelle que soit votre réponse.</p>
<p>Lorsque vous converserez par téléphone, toutes les touches de contrôle d'appel (Maintenir,
Mute, renvoi d'appel) sont disponibles dans la fenêtre Messenger (ou application de bureau).</p>
<p>Notez que si votre téléphone de bureau est connecté, tous les appels sortants vont
passer par lui. Vous pouvez débrancher votre téléphone pendant que vous êtes absent pour passer
des appels à partir de votre navigateur ou de votre application de bureau.</p>
#IMAGE_CALL_WITHOUT_BROWER#
<p>Veuillez vous référer au #LINK_COURSE_2_START#cours de formation#LINK_END# pour
des détails sur la façon de passer et de recevoir des appels et des instructions de connexion.</p>";
$MESS["VI_PHONES_TITLE"] = "Connecter les téléphones SIP";
