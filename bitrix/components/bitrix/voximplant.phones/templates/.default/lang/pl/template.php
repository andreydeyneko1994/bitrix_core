<?
$MESS["VI_PHONES_HELP_1"] = "<p>Podłącz telefony SIP do swojego Bitrix24, aby wykonywać i odbierać połączenia juz teraz.</p>
<p>Wzmocnij swoje klasyczne środowisko biznesowe umożliwiając swoim pracownikom wyświetlanie informacji o dzwoniącym na ekranie w trakcie korzystania z telefonów desktopowych.</p>";
$MESS["VI_PHONES_HELP_2"] = "<h3>Konfiguracja telefonu w kilku prostych krokach</h3>
<p>Bitrix24 przyznaje adres serwera, login i hasło dla każdego użytkownika. Wykorzystaj je do skonfigurowania swojego telefonu SIP.</p>
<p>Z chwilą zapisania ustawień, Twój telefon jest gotowy do korzystania z Bitrix24.</p>
<p>Aby podłączyć telefon, otwórz stronę #LINK_USERS_START#Telefonia użytkowników#LINK_END#, wybierz użytkownika i włącz dla niego telefonię.</p>
#IMAGE_CONNECT#
<p>Jako alternatywę dla telefonów SIP, możesz wykorzystać zwykłą naziemną telefonię DECT podłączając do niej bramkę SIP, która konwertuje sygnał na format SIP.</p>
<p>Telefon komórkowy jest kolejną opcją dla będących ciągle w ruchu. Wykorzystaj swój telefon z oprogramowaniem Windows,
iOS lub Android instalując jakąkolwiek aplikację softphone.</p>
<p>Proszę odnieść się do #LINK_COURSE_1_START#szkolenia#LINK_END#, aby zapoznać się i zweryfikować listę urządzeń i instrukcjami połączeń.</p>
<h3>Używając swojego telefonu</h3>
<p><b>Wykonując połączenie z CRM Bitrix24</b></p>
<p>Nie trać swojego cennego czasu na wciskanie przycisków. Po prostu kliknij Połącz w CRM, a gdy usłyszysz dzwonienie telefonu, odbierz go. Telefon momentalnie rozpocznie wybieranie numeru. Na komputerze we włączonym oknie połączenia zawsze możesz zobaczyć profil klienta lub wyświetlić informacje szczegółowe.</p>
<p>Zawsze możesz zrobić to po staremu jeżeli chcesz: wybierz numer na telefonie i obserwuj okno połączenia na ekranie komputera, tak samo jakgdyby połączenie zostało zainicjowane z CRM.</p>
<p><b>Otrzymywanie połączenia</b></p>
<p>Połączenie przychodzące równocześnie pojawi się na twojej przeglądarce (lub aplikacji pulpitowej) i telefonie. Można je odebrać na obu urządzeniach; rozmowa będzie nagrywana niezależnie od sposobu w jaki zostannie odebrana.</p>
<p>Podczas rozmowy przez telefon wszystkie przyciski kontrolne (Wstrzymaj, Wycisz, przekazywanie połączenia) są dostępne w oknie komunikatora (lub aplikacji pulpitowej).</p>
<p>Zauważ, że jeżeli telefon pulpitowy jest podłączony, wszystkie połączenia wychodzące będą przechodzić przez niego. Możesz odłączyć swój telefon, kiedy cie nie ma, aby wykonywać połączenia przez przeglądarkę lub aplikację pulpitową.</p>
#IMAGE_CALL_WITHOUT_BROWER#
<p>Proszę odnieść się do #LINK_COURSE_2_START#szkolenia#LINK_END# odnośnie wykonywania i otrzymywania połączeń oraz instrukcji podłączenia.</p>";
$MESS["VI_PHONES_TITLE"] = "Połącz telefonię SIP";
?>