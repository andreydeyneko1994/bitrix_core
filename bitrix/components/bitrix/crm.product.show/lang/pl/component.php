<?
$MESS["CRM_FIELD_ACTIVE"] = "Aktywny";
$MESS["CRM_FIELD_CURRENCY"] = "Waluta";
$MESS["CRM_FIELD_DESCRIPTION"] = "Opis";
$MESS["CRM_FIELD_MEASURE"] = "Jednostka miary";
$MESS["CRM_FIELD_PRICE"] = "Cena";
$MESS["CRM_FIELD_SECTION"] = "Sekcja";
$MESS["CRM_FIELD_SORT"] = "Sortowanie";
$MESS["CRM_FIELD_VAT_ID"] = "Stawka VAT";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "z VAT";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[nie wybrano]";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRODUCT_FIELD_DETAIL_PICTURE"] = "Obraz";
$MESS["CRM_PRODUCT_FIELD_NAME"] = "Nazwa";
$MESS["CRM_PRODUCT_FIELD_PREVIEW_PICTURE"] = "Miniatura";
$MESS["CRM_PRODUCT_NAV_TITLE_ADD"] = "Nowy produkt";
$MESS["CRM_PRODUCT_NAV_TITLE_EDIT"] = "Produkt: #NAME#";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "Produkty";
$MESS["CRM_PRODUCT_NOT_FOUND"] = "Nie znaleziono produktu.";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Pobierz";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Powiększ";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[nie wybrano]";
$MESS["CRM_SECTION_PRODUCT_INFO"] = "Informacja o produkcie";
?>