<?php
$MESS["BIZPROC_GLOBALFIELDS_LIST_BTN_DELETE"] = "Supprimer";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_CONSTANT_DELETE"] = "Voulez-vous vraiment supprimer la constante ?";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_VARIABLE_DELETE"] = "Voulez-vous vraiment supprimer la variable ?";
