<?php
$MESS["BIZPROC_GLOBALFIELDS_LIST_BTN_DELETE"] = "Eliminar";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_CONSTANT_DELETE"] = "¿Seguro que desea eliminar la constante?";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CONFIRM_VARIABLE_DELETE"] = "¿Seguro que desea eliminar la variable?";
