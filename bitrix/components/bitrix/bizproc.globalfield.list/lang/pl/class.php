<?php
$MESS["BIZPROC_GLOBALFIELDS_LIST_CANT_DELETE_CONSTANT_RIGHT"] = "Niewystarczające uprawnienia do usunięcia stałych.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CANT_DELETE_VARIABLE_RIGHT"] = "Niewystarczające uprawnienia do usunięcia zmiennych.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CREATED_BY"] = "Utworzone przez";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CREATED_DATE"] = "Utworzono";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CREATE_CONSTANT"] = "Utwórz stałą";
$MESS["BIZPROC_GLOBALFIELDS_LIST_CREATE_VARIABLE"] = "Utwórz zmienną";
$MESS["BIZPROC_GLOBALFIELDS_LIST_DELETE"] = "Usuń";
$MESS["BIZPROC_GLOBALFIELDS_LIST_DESCRIPTION"] = "Opis";
$MESS["BIZPROC_GLOBALFIELDS_LIST_EDIT"] = "Edytuj";
$MESS["BIZPROC_GLOBALFIELDS_LIST_ERR_CANT_READ_CONSTANT"] = "Odmowa dostępu do odczytu listy stałych.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_ERR_CANT_READ_VARIABLE"] = "Odmowa dostępu do odczytu listy zmiennych.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_ERR_DOCUMENT_TYPE"] = "Nieprawidłowy typ dokumentu.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_ERR_MODE_NOT_DEFINED"] = "Nie wybrano trybu wykonawczego komponentu.";
$MESS["BIZPROC_GLOBALFIELDS_LIST_IS_MULTIPLE"] = "Wielokrotne";
$MESS["BIZPROC_GLOBALFIELDS_LIST_MODIFIED_BY"] = "Zmodyfikowane przez";
$MESS["BIZPROC_GLOBALFIELDS_LIST_MODIFIED_DATE"] = "Zmodyfikowano";
$MESS["BIZPROC_GLOBALFIELDS_LIST_NAME"] = "Nazwa";
$MESS["BIZPROC_GLOBALFIELDS_LIST_NAME_CONSTANT"] = "Nazwa stałej";
$MESS["BIZPROC_GLOBALFIELDS_LIST_NAME_VARIABLE"] = "Nazwa zmiennej";
$MESS["BIZPROC_GLOBALFIELDS_LIST_TITLE_CONSTANT"] = "Stałe";
$MESS["BIZPROC_GLOBALFIELDS_LIST_TITLE_VARIABLE"] = "Zmienne";
$MESS["BIZPROC_GLOBALFIELDS_LIST_TYPE"] = "Typ";
$MESS["BIZPROC_GLOBALFIELDS_LIST_VALUE"] = "Wartość";
$MESS["BIZPROC_GLOBALFIELDS_LIST_VISIBILITY"] = "Widoczność";
