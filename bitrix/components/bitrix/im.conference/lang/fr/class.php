<?php
$MESS["IM_COMPONENT_DEFAULT_OG_TITLE"] = "Vidéoconférence";
$MESS["IM_COMPONENT_OG_DESCRIPTION"] = "La qualité HD, ce qu'il y a de mieux pour rencontrer en personne.";
$MESS["IM_COMPONENT_OG_DESCRIPTION_2"] = "Rejoignez depuis n'importe quel appareil, sans inscription. Gratuit pour 24 utilisateurs.";
$MESS["IM_COMPONENT_OG_TITLE"] = "Utilisez les appels vidéo pour augmenter l'efficacité de votre entreprise !";
$MESS["IM_COMPONENT_OG_TITLE_2"] = "Vidéoconférences HD gratuites avec Bitrix24";
