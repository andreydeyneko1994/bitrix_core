<?php
$MESS["IM_COMPONENT_DEFAULT_OG_TITLE"] = "Videoconferencia";
$MESS["IM_COMPONENT_OG_DESCRIPTION"] = "Calidad HD, lo mejor para reuniones personales.";
$MESS["IM_COMPONENT_OG_DESCRIPTION_2"] = "Únase sin registrarse desde cualquier dispositivo. Es gratuito para 24 usuarios.";
$MESS["IM_COMPONENT_OG_TITLE"] = "¡Utilice las videollamadas para que su negocio sea aún más eficiente!";
$MESS["IM_COMPONENT_OG_TITLE_2"] = "Videoconferencias HD de Bitrix24 gratuitas";
