<?
$MESS["IMOL_MAIL_BACK_TO_TALK"] = "Kontynuuj konwersację";
$MESS["IMOL_MAIL_DONT_REPLY_NEW"] = "Wiadomość e-mail została wysłana przez system. Proszę na nią nie odpowiadać.";
$MESS["IMOL_MAIL_FORMAT_ERROR"] = "Błąd w trakcie tworzenia wiadomości.";
$MESS["IMOL_MAIL_MESSAGE_SYSTEM"] = "Wiadomość systemowa";
$MESS["IMOL_MAIL_WRITE_TO_LINE"] = "Skontaktuj się ponownie";
?>