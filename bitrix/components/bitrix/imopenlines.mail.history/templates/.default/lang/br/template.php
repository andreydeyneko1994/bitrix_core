<?
$MESS["IMOL_MAIL_BACK_TO_TALK"] = "Continuar conversa";
$MESS["IMOL_MAIL_DONT_REPLY_NEW"] = "A mensagem de e-mail foi enviada pelo sistema. Não responda.";
$MESS["IMOL_MAIL_FORMAT_ERROR"] = "Erro ao criar a mensagem.";
$MESS["IMOL_MAIL_MESSAGE_SYSTEM"] = "Mensagem do sistema";
$MESS["IMOL_MAIL_WRITE_TO_LINE"] = "Entre em contato novamente";
?>