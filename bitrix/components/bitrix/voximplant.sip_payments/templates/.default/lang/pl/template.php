<?
$MESS["VI_NOTICE_BUTTON_DONE"] = "Gotowe";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "Aby przyspieszyć pracę centrali SIP, musisz zmienić adres serwera dla połączeń przychodzących<br> z <b>incoming.#ACCOUNT_NAME#</b><br> na <b>ip.#ACCOUNT_NAME#</b><br> w parametrach połączenia PBX.";
$MESS["VI_SIP_BUTTON"] = "Rozszerzone";
$MESS["VI_SIP_BUTTON_BUY"] = "Połącz";
$MESS["VI_SIP_CONFIG"] = "Zarządzaj numerami SIP";
$MESS["VI_SIP_PAID_BEFORE"] = "Moduł wygaśnie #DATE#";
$MESS["VI_SIP_PAID_FREE"] = "Masz #COUNT# darmowych minut do skonfigurowania i przetestowania swojego sprzętu.";
$MESS["VI_SIP_PAID_NOTICE"] = "Numery SIP zostaną odłączone po wygaśnięciu subskrypcji modułu.";
$MESS["VI_SIP_PAID_NOTICE_2"] = "Po wyczerpaniu się darmowych minut, numery SIP zostaną odłączone do momentu opłacenia miesięcznej subskrybcji w SIP Connector.";
$MESS["VI_SIP_TITLE"] = "SIP Connector";
?>