<?
$MESS["SALESCENTER_FEATURE_MESSAGE"] = "L'Espace des ventes est indisponible avec votre offre d'abonnement. Veuillez passer à une offre commerciale majeure pour l'utiliser";
$MESS["SALESCENTER_FEATURE_MODULE_ERROR"] = "Le module « Sales Center » n'est pas installé";
$MESS["SALESCENTER_FEATURE_TITLE"] = "Disponible uniquement avec les offres commerciales majeures";
$MESS["SALESCENTER_LIMITS_MESSAGE"] = "Félicitations ! <br />Vous avez créé 100 commandes via un chat en direct ou des SMS.<br /><br />Veuillez passer à une offre commerciale pour continuer à gérer vos commandes et recevoir des paiements via l'Espace des ventes.";
$MESS["SALESCENTER_LIMITS_TITLE"] = "Disponible uniquement avec certaines offres commerciales";
?>