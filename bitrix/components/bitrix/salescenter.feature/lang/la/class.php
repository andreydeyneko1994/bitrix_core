<?
$MESS["SALESCENTER_FEATURE_MESSAGE"] = "El Centro de ventas no está disponible para su plan de suscripción. Actualice a uno de los planes comerciales principales para utilizar el Centro de ventas.";
$MESS["SALESCENTER_FEATURE_MODULE_ERROR"] = "El módulo \"Centro de ventas\" no está instalado";
$MESS["SALESCENTER_FEATURE_TITLE"] = "Disponible solo para los planes comerciales principales";
$MESS["SALESCENTER_LIMITS_MESSAGE"] = "¡Felicidades! <br />Creó 100 pedidos utilizando el Chat en vivo y SMS.<br /><br />Actualice a uno de los planes comerciales para continuar con el manejo de pedidos y recibir pagos mediante el Centro de ventas.";
$MESS["SALESCENTER_LIMITS_TITLE"] = "Disponible solamente en los planes comerciales seleccionados";
?>