<?php
$MESS["CRM_STORE_DOCUMENT_SD_CART_NOT_FOUND"] = "O carrinho do pedido não foi encontrado";
$MESS["CRM_STORE_DOCUMENT_SD_FORM_DATA_MISSING"] = "Faltam alguns dados do formulário";
$MESS["CRM_STORE_DOCUMENT_SD_INSUFFICIENT_RIGHTS"] = "Permissões insuficientes.";
$MESS["CRM_STORE_DOCUMENT_SD_ORDER_ID_NEGATIVE"] = "É necessário um ID de pedido válido ou inserção de dados para criar um novo pedido";
$MESS["CRM_STORE_DOCUMENT_SD_PRODUCT_NOT_FOUND"] = "Insira pelo menos um produto";
$MESS["CRM_STORE_DOCUMENT_SD_SHIPMENT_NOT_FOUND"] = "O envio não foi encontrado";
