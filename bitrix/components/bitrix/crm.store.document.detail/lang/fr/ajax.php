<?php
$MESS["CRM_STORE_DOCUMENT_SD_CART_NOT_FOUND"] = "Le panier de la commande est introuvable";
$MESS["CRM_STORE_DOCUMENT_SD_FORM_DATA_MISSING"] = "Certaines données du formulaire manquent";
$MESS["CRM_STORE_DOCUMENT_SD_INSUFFICIENT_RIGHTS"] = "Droits insuffisants.";
$MESS["CRM_STORE_DOCUMENT_SD_ORDER_ID_NEGATIVE"] = "Un ID de commande valide est requis, ou saisissez des données pour créer une nouvelle commande";
$MESS["CRM_STORE_DOCUMENT_SD_PRODUCT_NOT_FOUND"] = "Veuillez saisir au moins un produit";
$MESS["CRM_STORE_DOCUMENT_SD_SHIPMENT_NOT_FOUND"] = "Livraison introuvable";
