<?php
$MESS["TELEPHONY_CALLERID_NUMBER"] = "Połączono numer #CALLER_ID#";
$MESS["TELEPHONY_CONFIRM_DATE"] = "Wprowadzony numer został potwierdzony, będzie aktywny do #DATE#, po której zostanie automatycznie deaktywowany do czasu ponownego potwierdzenia. Możesz wydłużyć datę następnego potwierdzenia w każdej chwili przez ponowne potwierdzenie.";
$MESS["TELEPHONY_EMPTY_PHONE"] = "Nie wprowadzono żadnego numeru telefonu";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "Dzwoniąca osoba zobaczy przekazany numer telefonu";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "Numer telefonu nie został zweryfikowany. Musisz zweryfikować ten numer, aby był on widoczny dla osób, do których dzwonisz.";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "Konfiguruj numer telefonu";
$MESS["TELEPHONY_PUT_PHONE"] = "Wprowadź numer telefonu firmy";
