<?php
$MESS["TELEPHONY_CALLERID_NUMBER"] = "Você ligou para o número #CALLER_ID#";
$MESS["TELEPHONY_CONFIRM_DATE"] = "O número foi confirmado, ele estará ativo até #DATE#, após essa data ele será desativado automaticamente até que seja reconfirmado. Você pode estender a data da próxima confirmação, reconfirmando o número a qualquer tempo.";
$MESS["TELEPHONY_EMPTY_PHONE"] = "Nenhum número de telefone informado";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "A pessoa chamada verá um número de telefone transmitido";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "O número de telefone não foi verificado. Você tem que verificar este número de telefone para tornar seu ID de autor da chamada visível para outras pessoas.";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "Configurar o número de telefone";
$MESS["TELEPHONY_PUT_PHONE"] = "Digite o número do telefone da empresa";
