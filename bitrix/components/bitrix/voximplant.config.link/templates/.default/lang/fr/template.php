<?php
$MESS["TELEPHONY_CALLERID_NUMBER"] = "Vous avez connecté le numéro #CALLER_ID#";
$MESS["TELEPHONY_CONFIRM_DATE"] = "Le numéro a été confirmé, il sera actif jusqu'au #DATE#, après quoi il sera automatiquement désactivé jusqu'à sa nouvelle confirmation. Vous pouvez étendre la date de la prochaine confirmation en le reconfirmant à tout moment.";
$MESS["TELEPHONY_EMPTY_PHONE"] = "Aucun numéro de téléphone indiqué";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "La personne appelée verra un numéro de téléphone relais";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "Le numéro de téléphone n'a pas été vérifié. Vous devez vérifier ce numéro de téléphone pour rendre votre ID d'appel visible aux autres personnes.";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "Configurer le numéro de téléphone";
$MESS["TELEPHONY_PUT_PHONE"] = "Saisir le numéro de téléphone de l'entreprise";
