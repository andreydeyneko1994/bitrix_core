<?php
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_MODULE_ABSENT"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_PRODUCT_ABSENT"] = "O catálogo de produtos do CRM não foi encontrado";
$MESS["CRM_CATALOG_CONTROLLER_ERR_IBLOCK_MODULE_ABSENT"] = "O módulo Blocos de Informação não está instalado.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_PAGE_UNKNOWN"] = "Página inválida";
