<?php
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_MODULE_ABSENT"] = "Moduł katalogu produktów nie jest zainstalowany.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_CATALOG_PRODUCT_ABSENT"] = "Nie znaleziono katalogu produktów CRM";
$MESS["CRM_CATALOG_CONTROLLER_ERR_IBLOCK_MODULE_ABSENT"] = "Moduł bloków informacyjnych nie jest zainstalowany.";
$MESS["CRM_CATALOG_CONTROLLER_ERR_PAGE_UNKNOWN"] = "Nieprawidłowa strona";
