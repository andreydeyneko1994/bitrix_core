<?
$MESS["BPABS_EMPTY_DOC_ID"] = "Nie określonego ID dokumentu, dla którego ma zostać utworzony proces biznesowy.";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Wymagany jest typ dokumentu.";
$MESS["BPABS_NO_PERMS"] = "Nie masz uprawnień do przeprowadzenia procesu biznesowego dla tego dokumentu.";
$MESS["BPABS_TITLE"] = "Uruchom Proces Biznesowy";
$MESS["BPATT_NO_MODULE_ID"] = "Wymagane jest ID modułu.";
?>