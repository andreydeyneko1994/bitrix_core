<?php
$MESS["SPOD_ACCESS_DENIED"] = "Acesso restrito";
$MESS["SPP_CANNOT_CREATE_PAYMENT"] = "Não é possível criar pagamento";
$MESS["SPP_ORDER_NOT_FOUND"] = "O pedido não foi encontrado";
$MESS["SPP_PAYMENT_NOT_FOUND"] = "O pagamento não foi encontrado";
$MESS["SPP_PAYSYSTEM_NOT_FOUND"] = "Sistema de pagamento não encontrado";
