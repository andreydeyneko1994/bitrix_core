<?php
$MESS["SPOD_ACCESS_DENIED"] = "Dostęp ograniczony";
$MESS["SPP_CANNOT_CREATE_PAYMENT"] = "Nie można utworzyć płatności";
$MESS["SPP_ORDER_NOT_FOUND"] = "Nie znaleziono zamówienia";
$MESS["SPP_PAYMENT_NOT_FOUND"] = "Nie znaleziono płatności";
$MESS["SPP_PAYSYSTEM_NOT_FOUND"] = "Nie znaleziono systemu płatności";
