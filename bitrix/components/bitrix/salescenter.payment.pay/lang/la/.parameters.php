<?
$MESS["SPP_ALLOW_PAYMENT_REDIRECT"] = "Redireccionar a la página de pago después de seleccionar el método de pago";
$MESS["SPP_TEMPLATE_MODE"] = "Modo de visualización";
$MESS["SPP_TEMPLATE_MODE_DARK_VALUE"] = "Oscuro";
$MESS["SPP_TEMPLATE_MODE_LIGHT_VALUE"] = "Claro";
?>