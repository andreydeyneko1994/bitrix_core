<?php
$MESS["SPP_CHECK_PRINT_TITLE"] = "Pokwitowanie ##ID# z #DATE_CREATE# zostanie wkrótce utworzone";
$MESS["SPP_CHECK_TITLE"] = "Wyświetl pokwitowanie ##CHECK_ID# z #DATE_CREATE#";
$MESS["SPP_EMPTY_TEMPLATE_FOOTER"] = "Wybierz inny system płatności, aby zapłacić online";
$MESS["SPP_EMPTY_TEMPLATE_PAY_SYSTEM_NAME_FIELD"] = "Metoda płatności:";
$MESS["SPP_EMPTY_TEMPLATE_SUM_WITH_CURRENCY_FIELD"] = "Suma zamówienia:";
$MESS["SPP_EMPTY_TEMPLATE_TITLE"] = "Dziękujemy za zamówienie!";
$MESS["SPP_INFO_BUTTON"] = "Informacje";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT"] = "Niestety napotkaliśmy błąd. Wybierz inną opcję płatności lub skontaktuj się z przedstawicielem handlowym.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_FOOTER"] = "Wybierz inną metodę płatności lub skontaktuj się z przedstawicielem handlowym.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_HEADER"] = "Niestety napotkaliśmy błąd.";
$MESS["SPP_PAID"] = "Opłacono";
$MESS["SPP_PAID_TITLE"] = "Płatność ##ACCOUNT_NUMBER# z #DATE_INSERT#";
$MESS["SPP_PAY_BUTTON"] = "Zapłać";
$MESS["SPP_PAY_RELOAD_BUTTON_NEW"] = "Wybierz inną metodę";
$MESS["SPP_SELECT_PAYMENT_TITLE_NEW_NEW"] = "Wybierz metodę płatności";
$MESS["SPP_SUM"] = "Kwota: #SUM#";
