<?php
$MESS["SPP_CHECK_PRINT_TITLE"] = "Le reçu ##ID# du #DATE_CREATE# sera bientôt créé";
$MESS["SPP_CHECK_TITLE"] = "Afficher le reçu ##CHECK_ID# du #DATE_CREATE#";
$MESS["SPP_EMPTY_TEMPLATE_FOOTER"] = "Veuillez sélectionner un système de paiement différent pour payer en ligne";
$MESS["SPP_EMPTY_TEMPLATE_PAY_SYSTEM_NAME_FIELD"] = "Mode de paiement :";
$MESS["SPP_EMPTY_TEMPLATE_SUM_WITH_CURRENCY_FIELD"] = "Total de la commande :";
$MESS["SPP_EMPTY_TEMPLATE_TITLE"] = "Merci pour votre commande !";
$MESS["SPP_INFO_BUTTON"] = "Informations";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT"] = "Malheureusement, une erreur est survenue. Veuillez sélectionner une option de paiement différente ou contacter le responsable des ventes.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_FOOTER"] = "Veuillez sélectionner une option de paiement différente ou contacter un représentant commercial.";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_HEADER"] = "Nous avons malheureusement rencontré une erreur.";
$MESS["SPP_PAID"] = "Payé";
$MESS["SPP_PAID_TITLE"] = "Paiement ##ACCOUNT_NUMBER# du #DATE_INSERT#";
$MESS["SPP_PAY_BUTTON"] = "Payer";
$MESS["SPP_PAY_RELOAD_BUTTON_NEW"] = "Sélectionner une autre méthode";
$MESS["SPP_SELECT_PAYMENT_TITLE_NEW_NEW"] = "Sélectionner un mode de paiement";
$MESS["SPP_SUM"] = "Montant : #SUM#";
