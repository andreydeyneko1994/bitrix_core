<?
$MESS["SHOP_MENU_BUYER_GROUP_TITLE"] = "Groupes de clients";
$MESS["SHOP_MENU_ORDER_FORM_SETTINGS_TITLE"] = "Configurer le formulaire de commande";
$MESS["SHOP_MENU_ORDER_TITLE"] = "Commandes";
$MESS["SHOP_MENU_PRODUCT_MARKETING_COUPONS"] = "Coupons";
$MESS["SHOP_MENU_PRODUCT_MARKETING_DISCOUNT"] = "Règles du panier d'achats";
$MESS["SHOP_MENU_PRODUCT_MARKETING_PRESET"] = "Transactions commerciales préinstallées";
$MESS["SHOP_MENU_PRODUCT_MARKETING_TITLE"] = "Marketing du produit";
$MESS["SHOP_MENU_SETTINGS_SALE_SETTINGS"] = "Paramètres de la boutique en ligne";
$MESS["SHOP_MENU_SETTINGS_STATUS"] = "Statuts";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER"] = "Statuts des commandes";
$MESS["SHOP_MENU_SETTINGS_STATUS_ORDER_SHIPMENT"] = "Statuts des livraisons";
$MESS["SHOP_MENU_SETTINGS_USER_FIELDS"] = "Champs personnalisés";
$MESS["SHOP_MENU_SHOP_LIST_TITLE"] = "Boutiques en ligne";
$MESS["SHOP_MENU_SHOP_ROLES_TITLE"] = "Droits d'accès";
$MESS["SHOP_MENU_SHOP_TITLE"] = "Boutiques en ligne";
?>