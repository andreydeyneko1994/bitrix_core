<?
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Wyświetl e-maile";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Powrót do konfiguracji";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Teraz możesz zacząć przeszukiwać swoją skrzynkę odbiorczą lub wrócić do strony konfiguracji e-mail.";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "Gratulacje! Twoje skrzynki pocztowe zostały pomyślnie podłączone.";
?>