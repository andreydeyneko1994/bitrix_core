<?php
$MESS["SOD_COMMON_DISCOUNT"] = "Oszczędzasz";
$MESS["SOD_COMMON_SUM_NEW"] = "Pozycji łącznie";
$MESS["SOD_DELIVERY"] = "Dostawa";
$MESS["SOD_FREE"] = "Za darmo";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "Płatność ##ACCOUNT_NUMBER# z #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "Płatność ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Suma";
$MESS["SOD_TAX"] = "Stawka podatku";
$MESS["SOD_TOTAL_WEIGHT"] = "Waga całkowita";
$MESS["SOD_TPL_SUMOF"] = "kwota zamówienia";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "Pobierz";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "Otwórz";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "Dalej";
$MESS["SPOD_DOCUMENT_TITLE"] = "Twoja faktura";
