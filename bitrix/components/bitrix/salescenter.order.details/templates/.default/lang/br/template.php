<?php
$MESS["SOD_COMMON_DISCOUNT"] = "Você economiza";
$MESS["SOD_COMMON_SUM_NEW"] = "Total de itens";
$MESS["SOD_DELIVERY"] = "Entrega";
$MESS["SOD_FREE"] = "Grátis";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "Pagamento ##ACCOUNT_NUMBER# de #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "Pagamento ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Total";
$MESS["SOD_TAX"] = "Taxa de imposto";
$MESS["SOD_TOTAL_WEIGHT"] = "Peso total";
$MESS["SOD_TPL_SUMOF"] = "valor do pedido";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "Download";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "Abrir";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "Enviar";
$MESS["SPOD_DOCUMENT_TITLE"] = "Sua fatura";
