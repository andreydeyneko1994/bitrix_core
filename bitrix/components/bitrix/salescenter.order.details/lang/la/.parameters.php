<?
$MESS["SOD_HEADER_TITLE"] = "Mostrar el encabezado del pedido";
$MESS["SOD_SHOW_HEADER"] = "Mostrar el encabezado";
$MESS["SOD_TEMPLATE_MODE"] = "Modo de visualización";
$MESS["SOD_TEMPLATE_MODE_DARK_VALUE"] = "Oscuro";
$MESS["SOD_TEMPLATE_MODE_LIGHT_VALUE"] = "Claro";
?>