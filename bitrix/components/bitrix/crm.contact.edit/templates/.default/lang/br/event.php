<?
$MESS["CRM_CONTACT_EDIT_EVENT_CANCELED"] = "A ação foi cancelada. Agora você está sendo redirecionado para a página anterior. Se você ainda estiver nesta página, feche-a manualmente.";
$MESS["CRM_CONTACT_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "O contato <a href='#URL#'>#TITLE#</a> foi criado com sucesso. Agora você está sendo redirecionado para a página anterior. Se você ainda estiver nesta página, feche-a manualmente.";
?>