<?
$MESS["CRM_CONTACT_CREATE_TITLE"] = "Nowy kontakt";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "według danych bankowych";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "Poprzez e-mail";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "według pełnej nazwy";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "według telefonu";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "według danych kontaktowych";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "znalezione";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Znalezione dopasowania";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignoruj i zachowaj";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Anuluj";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Możliwe klony";
$MESS["CRM_CONTACT_EDIT_TITLE"] = "Kontakt ##ID# &mdash; #NAME#";
$MESS["CRM_IMPORT_SNS"] = "Możesz też użyć vCard aby dodawać kontakty.<br>Otwórz \"Kontakty\" w <b>MS Outlook</b> i wybierz odpowiednie kontakty; potem wybierz \"<b>Akcje %ARROW% Wyślij jako Business Card</b>\" na górnej belce i wyślij do <b>%EMAIL%</b>";
$MESS["CRM_TAB_1"] = "Kontakt";
$MESS["CRM_TAB_1_TITLE"] = "Właściwości kontaktu";
$MESS["CRM_TAB_2"] = "Zapis";
$MESS["CRM_TAB_2_TITLE"] = "Dziennik Kontaktu";
$MESS["CRM_TAB_3"] = "Proces Biznesowy";
$MESS["CRM_TAB_3_TITLE"] = "Proces biznesowy deala";
?>