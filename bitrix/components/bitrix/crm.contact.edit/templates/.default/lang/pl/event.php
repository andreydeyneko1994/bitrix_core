<?
$MESS["CRM_CONTACT_EDIT_EVENT_CANCELED"] = "Działanie zostało anulowane. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli nadal jesteś na tej stronie, zamknij ją ręcznie.";
$MESS["CRM_CONTACT_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Kontakt <a href='#URL#'>#TITLE#</a> został utworzony. Teraz nastąpi przekierowanie do poprzedniej strony. Jeśli nadal jesteś na tej stronie, zamknij ją ręcznie.";
?>