<?
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "Plik został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "Plik został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "Folder został usunięty";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "Folder został usunięty";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_COPY_OBJECT"] = "Nie można skopiować obiektu.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Nie można było utworzyć ani odnaleźć linku publicznego do obiektu.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_FIND_OBJECT"] = "Nie można znaleźć obiektu.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_MOVE_OBJECT"] = "Nie można przenieść obiektu.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_RESTORE_OBJECT"] = "Nie można przywrócić obiektu.";
?>