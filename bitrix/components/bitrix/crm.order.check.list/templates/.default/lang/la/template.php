<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CHECK_LIST_ADD"] = "Agregar recibo";
$MESS["CRM_CHECK_LIST_ADD_SHORT"] = "Recibo";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS"] = "Actualizar el estado";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS_TITLE"] = "Actualizar el estado del recibo";
$MESS["CRM_ORDER_CHECK_DELETE"] = "Eliminar";
$MESS["CRM_ORDER_CHECK_DELETE_CONFIRM"] = "¿Está seguro de que quiere eliminar este elemento?";
$MESS["CRM_ORDER_CHECK_DELETE_TITLE"] = "Eliminar recibo";
$MESS["CRM_ORDER_CHECK_SHOW"] = "Ver";
$MESS["CRM_ORDER_CHECK_SHOW_TITLE"] = "Ver recibo";
$MESS["CRM_ORDER_CHECK_TITLE"] = "Recibo ##ID# de #DATE_CREATE#";
$MESS["CRM_ORDER_CHECK_URL"] = "Enlace del recibo";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "Pago ##ACCOUNT_NUMBER# de #DATE_BILL#";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "Envío ##ACCOUNT_NUMBER# de #DATE_INSERT#";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar cantidad";
?>