<?php
$MESS["CRM_COLUMN_ORDER_CHECK_DATE_CREATE"] = "Data utworzenia";
$MESS["CRM_COLUMN_ORDER_CHECK_ID"] = "ID";
$MESS["CRM_COLUMN_ORDER_CHECK_LINK"] = "Link do pokwitowania";
$MESS["CRM_COLUMN_ORDER_CHECK_ORDER_ID"] = "ID zamówienia";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_DESCR"] = "Płatność";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_ID"] = "ID płatności";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_DESCR"] = "Wysyłka";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_ID"] = "ID wysyłki";
$MESS["CRM_COLUMN_ORDER_CHECK_STATUS"] = "Status pokwitowania";
$MESS["CRM_COLUMN_ORDER_CHECK_SUM"] = "Kwota pokwitowania";
$MESS["CRM_COLUMN_ORDER_CHECK_TITLE"] = "Dokument płatności";
$MESS["CRM_COLUMN_ORDER_CHECK_TYPE"] = "Rodzaj pokwitowania";
$MESS["CRM_ERROR_WRONG_ORDER"] = "Nie znaleziono zamówienia";
$MESS["CRM_ERROR_WRONG_ORDER_ID"] = "Nieprawidłowy ID zamówienia";
$MESS["CRM_ORDER_CHECK_LIST_TITLE"] = "Pokwitowania płatności dla ##ACCOUNT_NUMBER# of #DATE_BILL# (#PAY_SYSTEM_NAME#)";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
