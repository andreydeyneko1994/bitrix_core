<?
$MESS["CRM_COMPANY_MERGE_HEADER_TEMPLATE"] = "Date de création : #DATE_CREATE#";
$MESS["CRM_COMPANY_MERGE_PAGE_TITLE"] = "Fusionner les entreprises";
$MESS["CRM_COMPANY_MERGE_RESULT_LEGEND"] = "Sélectionnez le prospect prioritaire dans la liste. Il sera utilisé comme base pour le profil de prospect. Vous pouvez lui ajouter plus de données à partir d'autres prospects.";
?>