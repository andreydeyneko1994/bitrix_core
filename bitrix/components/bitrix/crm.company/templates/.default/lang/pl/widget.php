<?
$MESS["CRM_COMPANY_WGT_DEMO_CONTENT"] = "Jeżeli wciąż nie masz żadnej firmy, <a href=\"#URL#\" class=\"#CLASS_NAME#\">utwórz nową</a> teraz!";
$MESS["CRM_COMPANY_WGT_DEMO_TITLE"] = "To jest widok demonstracyjny. Zamknij go, aby przeglądać dane Twojej firmy.";
$MESS["CRM_COMPANY_WGT_PAGE_TITLE"] = "Firmy: Raport podsumowujący";
?>