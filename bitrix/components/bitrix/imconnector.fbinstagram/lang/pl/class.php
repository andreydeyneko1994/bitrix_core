<?
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Błąd rejestracji";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INVALID_OAUTH_ACCESS_TOKEN"] = "Nie można zarządzać stroną publiczną z powodu utraty dostępu. Należy ponownie połączyć się ze swoją stroną publiczną.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MODULE_NOT_INSTALLED"] = "Moduł \"Konektory zewnętrznych IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_AUTHORIZATION_PAGE"] = "Nie można podłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_PAGE"] = "Nie można odłączyć strony";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_USER"] = "Nie można odłączyć konta użytkownika";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_AUTHORIZATION_PAGE"] = "Strona została podłączona";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_PAGE"] = "Strona została odłączona";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_USER"] = "Konto użytkownika zostało odłączone";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REMOVED_REFERENCE_TO_PAGE"] = "Ten konektor został skonfigurowany do użytku w grupie roboczej, do której aktualnie nie masz dostępu administracyjnego.<br>
Ponownie skonfiguruj konektor.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REPEATING_ERROR"] = "Jeśli problem nie ustąpi, konieczne może być odłączenie kanału i jego ponowne skonfigurowanie.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Ponownie prześlij formularz.";
?>