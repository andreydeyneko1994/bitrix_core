<?
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erreur d'inscription";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INVALID_OAUTH_ACCESS_TOKEN"] = "Impossible de gérer la page publique parce que l'accès a été perdu. Vous devez vous reconnecter à votre page publique.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MODULE_NOT_INSTALLED"] = "Le module \"External IM Connectors\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_AUTHORIZATION_PAGE"] = "Impossible de lier la page";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_PAGE"] = "Impossible de dissocier la page";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_DEL_USER"] = "Impossible de dissocier votre compte utilisateur";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_AUTHORIZATION_PAGE"] = "La page a été liée";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_PAGE"] = "La page a été dissociée";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OK_DEL_USER"] = "Votre compte utilisateur a été dissocié";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REMOVED_REFERENCE_TO_PAGE"] = "Ce connecteur a été configuré pour être utilisé avec un groupe de travail auquel vous n'avez actuellement pas d'accès administrateur.<br>
Veuillez reconfigurer le connecteur.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_REPEATING_ERROR"] = "Si le problème persiste, envisagez de déconnecter le canal et de le reconfigurer.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
?>