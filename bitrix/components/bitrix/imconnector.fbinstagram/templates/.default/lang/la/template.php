<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZATION"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_AUTHORIZE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_BUSINESS_ACCOUNT"] = "Cuenta empresarial";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_ANY_TIME"] = "Puede editarlo o desconectarse en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CHANGE_PAGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED"] = "Instagram conectado";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTED_PAGE"] = "Cuenta conectada";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Hubo un error. Compruebe sus preferencias.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_FACEBOOK_PAGE"] = "Conectarse a la página de Facebook";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Quiero </div><div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">conectarme</span> a una cuenta de negocios</div>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_CONVERT_TO_BUSINESS_HELP"] = "Convertir a una cuenta empresarial";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DEL_REFERENCE"] = "Desconectarse";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_DESCRIPTION_NEW"] = "<p class=\"im-connector-settings-header-description\">Conecte su cuenta comercial de Instagram para recibir mensajes de sus clientes en el chat de Bitrix24. Para conectarse, debe tener una página pública de Facebook o crear una, y conectar una cuenta comercial de Instagram a ella.</p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">Los comentarios de sus clientes se publicarán directamente en el chat de Bitrix24</li>
					<li class=\"im-connector-settings-header-list-item\">Los comentarios se enrutarán automáticamente a los operadores disponibles</li>
					<li class=\"im-connector-settings-header-list-item\">Los clientes se guardarán automáticamente en su CRM</li>
				</ul>

";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN"] = "Inicie sesión con su cuenta de Facebook para hacer modificaciones.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Tenga en cuenta que la página se desconectará de Bitrix24 si inicia sesión con una cuenta que no está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_ADDITIONAL_DESCRIPTION"] = "Debe crear una página pública de Facebook o conectar la que ya tiene. Su cuenta comercial de Instagram debe estar conectada a esta página de Facebook. Solo el administrador de la página puede conectarlo a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_1"] = "Guarde los contactos y el historial de comunicaciones en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_2"] = "Guíe al cliente a lo largo del embudo de ventas en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_3"] = "Reaccione de inmediato a los comentarios";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_LIST_ITEM_4"] = "Los comentarios se distribuyen entre los agentes de ventas según las reglas de la cola.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_SUBTITLE"] = "Conecte su página de Instagram a su Bitrix24 para recibir comentarios de sus clientes. Responda más rápidamente y mejore la conversión.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_TITLE"] = "Reciba comentarios a sus publicaciones de Instagram directamente en su Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INFO_CONNECT_ID"] = "redirect=detail&code=4779109";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Cómo conectar su cuenta de negocios de Instagram:</span>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Inicie sesión con la cuenta de administrador de la página de Facebook para administrar los comentarios que le dejan en una cuenta de negocios de Instagram asociada a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_MEDIA"] = "media";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_NO_SPECIFIC_PAGE"] = "¿Falta una cuenta necesaria? Encuentre una posible razón en #A#este artículo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_OTHER_PAGES"] = "Otras cuentas";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PERSONAL_ACCOUNT"] = "Cuenta personal";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_PREFIX_NAMING_PAGE"] = "utilizando";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_SELECT_THE_PAGE"] = "Seleccione una cuenta de negocios de Instagram vinculada al perfil de Facebook que desea conectar al Canal Abierto de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "No tiene una página de Facebook asociada con una cuenta de negocios de Instagram en la que sea el administrador.<br>Cree un perfil de Facebook en este momento o vincule una cuenta de Instagram a una página existente.";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TITLE_NEW"] = "Comentarios en publicaciones de las cuentas comerciales de Instagram";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_TO_CREATE_A_PAGE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_USER"] = "Cuenta";
