<?php
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_LIMIT"] = "Twój obecny plan ogranicza liczbę aktywnych Otwartych kanałów. Przed aktywowaniem tego kanału zdezaktywuj wszelkie inne aktualnie aktywne Otwarte kanały.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_LINE_ACTIVATION_ERROR_PERMISSION"] = "Niewystarczające uprawnienia do modyfikowania tego Otwartego kanału.";
