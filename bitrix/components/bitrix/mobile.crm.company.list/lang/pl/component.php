<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_COMPANY_CALLS"] = "Telefony";
$MESS["M_CRM_COMPANY_DEALS"] = "Aktywności";
$MESS["M_CRM_COMPANY_LIST_DELETE"] = "Usuń";
$MESS["M_CRM_COMPANY_LIST_EDIT"] = "Edycja";
$MESS["M_CRM_COMPANY_LIST_FILTER_CUSTOM"] = "Wyniki wyszukiwania";
$MESS["M_CRM_COMPANY_LIST_FILTER_NONE"] = "Wszystkie firmy";
$MESS["M_CRM_COMPANY_LIST_MORE"] = "Więcej ...";
$MESS["M_CRM_COMPANY_LIST_PRESET_CHANGE_MY"] = "Zmodyfikowane przeze mnie";
$MESS["M_CRM_COMPANY_LIST_PRESET_MY"] = "Moje firmy";
$MESS["M_CRM_COMPANY_LIST_PRESET_USER"] = "Własny filtr";
?>