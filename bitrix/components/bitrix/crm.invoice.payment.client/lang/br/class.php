<?
$MESS["CIPC_EMPTY_PAY_SYSTEM"] = "Nenhum sistema de pagamento selecionado";
$MESS["CIPC_ERROR_PAYMENT_EXECUTION"] = "Erro ao executar o pagamento";
$MESS["CIPC_TITLE_COMPONENT"] = "Pagamento de fatura";
$MESS["CIPC_WRONG_ACCOUNT_NUMBER"] = "A fatura com o número especificado não foi encontrada";
$MESS["CIPC_WRONG_LINK"] = "Não foram encontradas inserções.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
?>