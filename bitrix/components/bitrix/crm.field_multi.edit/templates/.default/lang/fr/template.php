<?
$MESS["CRM_STATUS_BUTTONS_CANCEL"] = "Annuler";
$MESS["CRM_STATUS_BUTTONS_SAVE"] = "Enregistrer";
$MESS["CRM_STATUS_LIST_ADD"] = "Ajouter";
$MESS["CRM_STATUS_LIST_DELETE"] = "Supprimer";
$MESS["CRM_STATUS_LIST_DOWN"] = "Déplacer vers le bas";
$MESS["CRM_STATUS_LIST_RECOVERY_NAME"] = "Retourner le nom d'origine";
$MESS["CRM_STATUS_LIST_UP"] = "Déplacer vers le haut";
?>