<?php
$MESS["INTRANET_INVITATION_GUEST_ADD_MORE"] = "Ajouter plus";
$MESS["INTRANET_INVITATION_GUEST_EMPTY_DATA"] = "Les champs du formulaire sont vides.";
$MESS["INTRANET_INVITATION_GUEST_ENTER_EMAIL"] = "saisissez une adresse e-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_EMAIL"] = "Adresse e-mail";
$MESS["INTRANET_INVITATION_GUEST_FIELD_LAST_NAME"] = "Nom de famille";
$MESS["INTRANET_INVITATION_GUEST_FIELD_NAME"] = "Prénom";
$MESS["INTRANET_INVITATION_GUEST_HINT"] = "Vous pouvez également ajouter votre partenaire ou client via l'e-mail.";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON"] = "Inviter";
$MESS["INTRANET_INVITATION_GUEST_INVITE_BUTTON_MORE"] = "Inviter d'autres utilisateurs";
$MESS["INTRANET_INVITATION_GUEST_TITLE"] = "Envoyer l'invitation à l'adresse e-mail";
$MESS["INTRANET_INVITATION_GUEST_WRONG_DATA"] = "Certains ou tous les champs du formulaire sont incorrects.";
