<?
$MESS["VOX_QUEUE_CLOSE"] = "Anuluj";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Wystąpił błąd podczas usuwania grupy.";
$MESS["VOX_QUEUE_IS_USED"] = "Ta grupa jest obecnie używana przez:";
$MESS["VOX_QUEUE_IVR"] = "IVR";
$MESS["VOX_QUEUE_LIST_ADD"] = "Dodaj grupę";
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Razem kolejek";
$MESS["VOX_QUEUE_NUMBER"] = "Numer";
?>