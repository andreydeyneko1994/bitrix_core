<?php
$MESS["VOX_QUEUE_LIST_CREATE_GROUP"] = "Stwórz kolejkę";
$MESS["VOX_QUEUE_LIST_DELETE"] = "Usuń";
$MESS["VOX_QUEUE_LIST_EDIT"] = "Edytuj";
$MESS["VOX_QUEUE_LIST_ID"] = "ID";
$MESS["VOX_QUEUE_LIST_NAME"] = "Nazwa";
$MESS["VOX_QUEUE_LIST_PHONE_NUMBER"] = "Numer wewnętrzny";
$MESS["VOX_QUEUE_LIST_TYPE"] = "Typ";
$MESS["VOX_QUEUE_LIST_TYPE_ALL"] = "Do wszystkich";
$MESS["VOX_QUEUE_LIST_TYPE_EVENLY"] = "Równomiernie";
$MESS["VOX_QUEUE_LIST_TYPE_STRICTLY"] = "Według kolejności";
