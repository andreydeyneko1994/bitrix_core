<?php
$MESS["VOX_QUEUE_LIST_CREATE_GROUP"] = "Créer une file d'attente";
$MESS["VOX_QUEUE_LIST_DELETE"] = "Supprimer";
$MESS["VOX_QUEUE_LIST_EDIT"] = "Modifier";
$MESS["VOX_QUEUE_LIST_ID"] = "ID";
$MESS["VOX_QUEUE_LIST_NAME"] = "Nom";
$MESS["VOX_QUEUE_LIST_PHONE_NUMBER"] = "Numéro d'extension";
$MESS["VOX_QUEUE_LIST_TYPE"] = "Type";
$MESS["VOX_QUEUE_LIST_TYPE_ALL"] = "À tout le monde";
$MESS["VOX_QUEUE_LIST_TYPE_EVENLY"] = "De manière égale";
$MESS["VOX_QUEUE_LIST_TYPE_STRICTLY"] = "Exactement comme mis en file d'attente";
