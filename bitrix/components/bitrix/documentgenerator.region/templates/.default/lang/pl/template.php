<?
$MESS["DOCGEN_REGION_EDIT_DELETE_CONFIRM"] = "Czy chcesz usunąć ten kraj?";
$MESS["DOCGEN_REGION_EDIT_EMPTY"] = "Nie wybrano";
$MESS["DOCGEN_REGION_EDIT_ERROR_TITLE_EMPTY"] = "Brak pola „Nazwa”";
$MESS["DOCGEN_REGION_EDIT_FORMAT_DATE"] = "Format daty";
$MESS["DOCGEN_REGION_EDIT_FORMAT_NAME"] = "Format nazwy";
$MESS["DOCGEN_REGION_EDIT_FORMAT_TIME"] = "Format czasu";
$MESS["DOCGEN_REGION_EDIT_LANGUAGE"] = "Użyj ustawień kraju";
$MESS["DOCGEN_REGION_EDIT_PHRASES_TITLE"] = "Komunikaty zależne od języka";
$MESS["DOCGEN_REGION_EDIT_TITLE"] = "Nazwa";
?>