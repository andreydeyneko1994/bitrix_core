<?
$MESS["DOCGEN_REGION_EDIT_DELETE_CONFIRM"] = "Você deseja excluir este país?";
$MESS["DOCGEN_REGION_EDIT_EMPTY"] = "Não selecionado";
$MESS["DOCGEN_REGION_EDIT_ERROR_TITLE_EMPTY"] = "Está faltando o campo \"Nome\"";
$MESS["DOCGEN_REGION_EDIT_FORMAT_DATE"] = "Formato da data";
$MESS["DOCGEN_REGION_EDIT_FORMAT_NAME"] = "Formato do nome";
$MESS["DOCGEN_REGION_EDIT_FORMAT_TIME"] = "Formato da hora";
$MESS["DOCGEN_REGION_EDIT_LANGUAGE"] = "Usar configurações do país";
$MESS["DOCGEN_REGION_EDIT_PHRASES_TITLE"] = "Mensagens dependentes do idioma";
$MESS["DOCGEN_REGION_EDIT_TITLE"] = "Nome";
?>