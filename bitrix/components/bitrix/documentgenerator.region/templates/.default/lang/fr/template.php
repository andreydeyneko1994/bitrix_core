<?
$MESS["DOCGEN_REGION_EDIT_DELETE_CONFIRM"] = "Voulez-vous supprimer ce pays ?";
$MESS["DOCGEN_REGION_EDIT_EMPTY"] = "Pas sélectionné";
$MESS["DOCGEN_REGION_EDIT_ERROR_TITLE_EMPTY"] = "Le champ \"Nom\" est manquant";
$MESS["DOCGEN_REGION_EDIT_FORMAT_DATE"] = "Format de la date";
$MESS["DOCGEN_REGION_EDIT_FORMAT_NAME"] = "Format du nom";
$MESS["DOCGEN_REGION_EDIT_FORMAT_TIME"] = "Format de l'heure";
$MESS["DOCGEN_REGION_EDIT_LANGUAGE"] = "Utiliser les paramètres du pays";
$MESS["DOCGEN_REGION_EDIT_PHRASES_TITLE"] = "Messages dépendants de la langue";
$MESS["DOCGEN_REGION_EDIT_TITLE"] = "Nom";
?>