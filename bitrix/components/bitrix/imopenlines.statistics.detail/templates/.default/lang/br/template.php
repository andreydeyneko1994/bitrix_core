<?
$MESS["CT_BLL_SELECTED"] = "Contagem de registros";
$MESS["OL_STAT_BACK"] = "Voltar";
$MESS["OL_STAT_BACK_TITLE"] = "Voltar";
$MESS["OL_STAT_EXCEL"] = "Exportar para o Microsoft Excel";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_CLOSE"] = "Fechar";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_START"] = "Prosseguir";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_STOP"] = "Parar";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_REQUEST_ERR"] = "Erro de processamento de pedido.";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_WAIT"] = "Por favor, aguarde...";
$MESS["OL_STAT_FILTER_CANCEL"] = "Redefinir filtro";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Redefinir filtro";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Estatísticas";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Redefinir filtro do funcionário";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Redefinir filtro do funcionário";
?>