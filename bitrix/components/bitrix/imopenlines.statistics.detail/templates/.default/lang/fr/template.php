<?
$MESS["CT_BLL_SELECTED"] = "Compteur d'enregistrements";
$MESS["OL_STAT_BACK"] = "Retour";
$MESS["OL_STAT_BACK_TITLE"] = "Revenir en arrière";
$MESS["OL_STAT_EXCEL"] = "Exporter vers Microsoft Excel";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_CLOSE"] = "Fermer";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_START"] = "Continuer";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_BTN_STOP"] = "Arrêter";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_REQUEST_ERR"] = "Erreur lors du traitement de la requête.";
$MESS["OL_STAT_EXCEL_EXPORT_POPUP_WAIT"] = "Veuillez patienter…";
$MESS["OL_STAT_FILTER_CANCEL"] = "Réinitialiser le filtre";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Réinitialiser le filtre";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Statistiques";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Réinitialiser le filtre employé";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Réinitialiser le filtre employé";
?>