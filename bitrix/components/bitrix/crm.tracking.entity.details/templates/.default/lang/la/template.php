<?
$MESS["CRM_TRACKING_ENTITY_DETAILS_BTN_HIDE"] = "Ocultar";
$MESS["CRM_TRACKING_ENTITY_DETAILS_BTN_SETUP"] = "Configurar";
$MESS["CRM_TRACKING_ENTITY_DETAILS_DESC"] = "Evalúe la calidad del tráfico de anuncios.";
$MESS["CRM_TRACKING_ENTITY_DETAILS_HEAD"] = "Conectarse a %tagStart% Sales Intelligence %tagEnd%";
$MESS["CRM_TRACKING_ENTITY_DETAILS_TITLE"] = "Sales Intelligence";
?>