<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZATION"] = "Uwierzytelnianie";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZE"] = "Zaloguj";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub wyłączyć";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Zmień";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED"] = "Podłączono Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Strona podłączona";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Wystąpił błąd. Proszę sprawdzić ustawienia.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Odłącz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Podłącz swoją stronę na Facebooku do Otwartych kanałów i zarządzaj komentarzami do swoich postów, zdjęć i filmów na Facebooku bezpośrednio z konta Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">komunikacja w czasie rzeczywistym</li>

     <li class=\"im-connector-settings-header-list-item\">automatyczne rozsyłanie wiadomości przychodzących zgodnie z regułami kolejki</li>

     <li class=\"im-connector-settings-header-list-item\">znany interfejs czatu Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">wszystkie konwersacje są zapisywane w historii systemu CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Aby dokonać poprawek, zaloguj się przy użyciu konta na Facebooku.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Zalogowanie przy użyciu konta, które nie jest powiązane z tą stroną, spowoduje odłączenie strony od Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_ADDITIONAL_DESCRIPTION"] = "Należy stworzyć publiczną stronę na Facebooku lub podłączyć już posiadaną. Wyłącznie administrator strony może podłączyć ją do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Podłącz swoją stronę na Facebooku do Otwartego kanału, aby zarządzać komentarzami do wpisów, zdjęć i filmów ze swojego Bitrix24. W tym celu musisz mieć swoją stronę na Facebooku, albo ją utworzyć. Musisz być administratorem tej strony.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_3"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_4"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_SUBTITLE"] = "Podłącz swoją publiczną stronę na Facebooku do Bitrix24, aby zarządzać komentarzami i postami bez opuszczania Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_TITLE"] = "Zarządzaj swoją stroną publiczną na Facebooku z Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Zaloguj się przy użyciu konta administratora strony na Facebooku, aby zarządzać komentarzami bezpośrednio z Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_NO_SPECIFIC_PAGE"] = "Brakuje wymaganej strony? Możliwe przyczyny znajdziesz w #A#tym artykule#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Inne strony";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Wybierz publiczną stronę na Facebooku, aby połączyć się z Otwartym kanałem Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Nie masz strony na Facebooku, której jesteś administratorem. <br>Utwórz ją teraz.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TITLE"] = "Zarządzaj swoją stroną na Facebooku przez Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Utwórz";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_USER"] = "Konto";
