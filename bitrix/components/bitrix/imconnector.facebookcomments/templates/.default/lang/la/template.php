<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZATION"] = "Autenticación";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_ANY_TIME"] = "Se puede editar o desactivar en cualquier momento";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED"] = "Facebook conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Ocurrió un error. Por favor revisa tu configuración.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Conecte su página de Facebook a Canal Abierto y administre los comentarios a sus publicaciones, fotos y videos de Facebook. Es su cuenta de Bitrix24.</p>
     <ul class=\"im-connector-settings-header-list\">
     <li class=\"im-connector-settings-header-list-item\">comunicaciones en tiempo real</li>
     <li class=\"im-connector-settings-header-list-item\">Distribución automática de mensajes entrantes según las reglas de la cola.</li>
     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar Bitrix24</li>
     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial de CRM</li>
    </ul>";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Inicie sesión con su cuenta de Facebook para hacer modificaciones.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que la página se desconectará de Bitrix24 si inicia sesión con una cuenta que no está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_ADDITIONAL_DESCRIPTION"] = "Debe crear una página pública de Facebook o conectar la que ya tiene. Solo el administrador de la página puede conectarlo a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Conecta la página de Facebook de tu empresa a Canal Abierto para administrar comentarios dejados a publicaciones, fotos y videos de tu Bitrix24. Para conectarse, necesitas tener una página de Facebook existente o crear una. Tienes que ser el administrador de esa página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_1"] = "Guarde los contactos y el historial de comunicaciones en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_2"] = "Guíe al cliente a lo largo del embudo de ventas en el CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_3"] = "Responda a sus clientes cuando y donde prefieran";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_4"] = "Las consultas de los clientes se distribuyen entre los agentes de ventas según las reglas de la cola.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_SUBTITLE"] = "Conecte su página pública de Facebook a su Bitrix24 para administrar comentarios y publicaciones sin salir de su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_TITLE"] = "Administre su página pública de Facebook desde Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INFO"] = "Información";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Inicie sesión con la cuenta de administración de la página de Facebook para administrar los comentarios directamente desde Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_NO_SPECIFIC_PAGE"] = "¿Falta una página necesaria? Encuentre una posible razón en #A#este artículo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Otras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Seleccione una página de Facebook pública para conectarse a Bitrix24 Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Usted no tiene una página de Facebook de la que usted es un administrador. <br>Cree uno ahora mismo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TITLE"] = "Administre su página de Facebook desde Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_USER"] = "Cuenta";
