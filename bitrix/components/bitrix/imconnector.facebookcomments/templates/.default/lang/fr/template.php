<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZATION"] = "Authentification";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZE"] = "Connexion";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_ANY_TIME"] = "Peut être édité ou désactivé à tout moment";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED"] = "Facebook connecté";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Page connectée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos paramètres.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Dissocier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Connectez votre page Facebook aux canaux ouverts et gérez les commentaires laissés sur vos publications, photos et vidéos Facebook directement de puis votre compte Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">communications en temps réel</li>

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente</li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Connectez-vous avec votre compte Facebook pour offrir une réparation.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Notez que la page sera déconnectée de votre Bitrix24 si vous vous connectez en utilisant un compte qui n'est pas lié à cette page.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_ADDITIONAL_DESCRIPTION"] = "Vous devrez créer une page Facebook publique ou connecter celle que vous avez déjà. Seul l'administrateur de la page peut la connecter à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Connectez la page Facebook de votre entreprise au Canal ouvert pour gérer les commentaires laissés sur les publications, photos et vidéos depuis votre Bitrix24. Pour effectuer la connexion, vous devez posséder une page Facebook qui existe déjà ou en créer une. Vous devez être un administrateur de cette page.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_3"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_4"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_SUBTITLE"] = "Connectez votre page publique Facebook à votre Bitrix24 pour gérer les commentaires et les messages sans quitter votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_TITLE"] = "Gérez votre page Facebook publique depuis Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Connectez-vous en utilisant le compte administrateur de la page Facebook pour gérer les commentaires directement depuis votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_NO_SPECIFIC_PAGE"] = "Il manque une page obligatoire ? Trouvez la raison possible dans #A#cet article#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Autres pages";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Sélectionnez une page Facebook publique pour la connecter au Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'avez aucune page Facebook pour laquelle vous êtes un administrateur. <br>Créez-en un(e) dès maintenant.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TITLE"] = "Gérez votre page Facebook depuis Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_USER"] = "Compte";
