<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZATION"] = "Autenticação";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_AUTHORIZE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_ANY_TIME"] = "Pode ser editado ou desligado a qualquer momento";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED"] = "Facebook conectado";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Ocorreu um erro. Verifique suas configurações.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DESCRIPTION"] = "<p class=\"im-connector-settings-header-description\">Conecte sua página do Facebook aos Canais Abertos e gerencie comentários das suas postagens, fotos e vídeos no Facebook diretamente de sua conta Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">comunicações em tempo real</li>

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila</li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas são salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Fazer login usando sua conta do Facebook para fazer alterações.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que a página será desconectada do Bitrix24 se você fizer login usando uma conta que não está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_ADDITIONAL_DESCRIPTION"] = "Você terá que criar uma página pública no Facebook ou conectar a que já tem. Apenas o administrador da página pode conectá-la ao Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Conecte a página do Facebook da sua empresa ao Canal Aberto para gerenciar comentários deixados nas postagens, fotos e vídeos do seu Bitrix24. Para conectar, você precisa ter uma página do Facebook já existente ou criar uma. Você tem que ser o administrador da página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_1"] = "Salve contatos e histórico de comunicação no CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_2"] = "Oriente o cliente através do funil de vendas no CRM";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_3"] = "Responda aos seus clientes quando e onde eles preferirem";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_LIST_ITEM_4"] = "As consultas dos clientes são distribuídas entre os representantes de vendas de acordo com as regras da fila";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_SUBTITLE"] = "Conecte sua página pública do Facebook ao Bitrix24 para gerenciar comentários e postagens sem sair do seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_TITLE"] = "Gerencie sua página pública do Facebook a partir do Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INFO"] = "Informações";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Fazer login usando a conta de administrador da página do Facebook para gerenciar comentários diretamente a partir do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_NO_SPECIFIC_PAGE"] = "Está faltando uma página obrigatória? Encontre o possível motivo #A#neste artigo#A_END#.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Outras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Selecione uma página pública do Facebook para conectar ao Canal Aberto Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não tem uma página do Facebook na qual você é administrador. <br>Crie uma agora mesmo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TITLE"] = "Gerencie sua página do Facebook a partir do Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_USER"] = "Conta";
