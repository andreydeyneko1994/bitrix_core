<?php
$MESS["CRM_REPORT_VC_W_C_CHART_NOT_AVAILABLE"] = "Absents";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ACTIONS"] = "Actions";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_AVG_COST"] = "prix moyen";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COMING_SOON"] = "À venir !";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_CONV_FULL"] = "Conversion des ventes";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COST"] = "prix";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_COST"] = "Le pourcentage change en coût relatif à la période précédente.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_QUANTITY"] = "Le pourcentage change en quantité relative à la période précédente.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_MOVE_TO_STAGE"] = "déplacé à l'étape";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_QUANTITY_SMALL"] = "quantité";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ROI"] = "ROI publicitaire";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP"] = "Configurer les sources";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP_BTN"] = "Configurer";
