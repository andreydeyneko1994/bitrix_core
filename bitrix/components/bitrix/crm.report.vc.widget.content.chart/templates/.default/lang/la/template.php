<?php
$MESS["CRM_REPORT_VC_W_C_CHART_NOT_AVAILABLE"] = "No disponible";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ACTIONS"] = "Acciones";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_AVG_COST"] = "precio promedio";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COMING_SOON"] = "¡Próximamente!";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_CONV_FULL"] = "Conversión de ventas";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COST"] = "precio";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_COST"] = "El cambio porcentual en el costo relativo al periodo anterior.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_QUANTITY"] = "El cambio porcentual en la cantidad relativo al periodo anterior.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_MOVE_TO_STAGE"] = "se movió a la etapa";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_QUANTITY_SMALL"] = "cantidad";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ROI"] = "ROI publicitario";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP"] = "Configurar las fuentes";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP_BTN"] = "Configurar";
