<?php
$MESS["CRM_REPORT_VC_W_C_CHART_NOT_AVAILABLE"] = "Niedostępny";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ACTIONS"] = "Działania";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_AVG_COST"] = "średnia cena";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COMING_SOON"] = "Już wkrótce!";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_CONV_FULL"] = "Konwersja sprzedaży";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_COST"] = "cena";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_COST"] = "Procentowa zmiana kosztów w stosunku do poprzedniego okresu.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_HINT_QUANTITY"] = "Procentowa zmiana ilości w stosunku do poprzedniego okresu.";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_MOVE_TO_STAGE"] = "przesunięto do etapu";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_QUANTITY_SMALL"] = "ilość";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_ROI"] = "ROI w reklamy";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP"] = "Konfiguruj źródła";
$MESS["CRM_REPORT_VC_W_C_CHART_STAGE_SETUP_BTN"] = "Konfiguruj";
