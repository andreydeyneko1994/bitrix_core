<?php
$MESS["CRM_REPORT_VC_W_C_CHART_CAPTION_SUMMARY"] = "Wszystkie źródła";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_CONVERSION"] = "Konwersja, %";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_COSTS"] = "Koszty";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_INCOME"] = "Zysk";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_ROI"] = "ROI";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SHORT_ACTION"] = "Działania";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SHORT_DEAL_SUCCESS"] = "Wygrane";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SHORT_VIEWS"] = "Wyświetlenia";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SOURCE_CODE"] = "Źródło";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SOURCE_COLOR"] = "Kolor";
$MESS["CRM_REPORT_VC_W_C_CHART_COLUMN_SUM"] = "Łącznie";
$MESS["CRM_REPORT_VC_W_C_CHART_CONV_BAD"] = "Zła";
$MESS["CRM_REPORT_VC_W_C_CHART_CONV_GOOD"] = "Dobra";
$MESS["CRM_REPORT_VC_W_C_CHART_CONV_NONE"] = "Brak";
$MESS["CRM_REPORT_VC_W_C_CHART_CONV_NORMAL"] = "Normalna";
$MESS["CRM_REPORT_VC_W_C_CHART_EXPENSES_ADD"] = "dodaj";
$MESS["CRM_REPORT_VC_W_C_CHART_GRID_SUMMARY"] = "Łącznie";
$MESS["CRM_REPORT_VC_W_C_CHART_GRID_SUMMARY_AD"] = "Kupione według źródła";
$MESS["CRM_REPORT_VC_W_C_CHART_SUM"] = "Łącznie";
