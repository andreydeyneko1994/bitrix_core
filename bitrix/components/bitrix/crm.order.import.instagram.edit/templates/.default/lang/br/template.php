<?
$MESS["CRM_OIIE_AUTHORIZATION"] = "Autenticação";
$MESS["CRM_OIIE_AUTHORIZE"] = "Fazer login";
$MESS["CRM_OIIE_CHANGE_ANY_TIME"] = "Você pode alterar ou desabilitar a importação de produtos a qualquer momento";
$MESS["CRM_OIIE_CREATE_WITHOUT_CONNECTION"] = "Criar uma loja on-line não conectada";
$MESS["CRM_OIIE_DEL_REFERENCE"] = "Desabilitar";
$MESS["CRM_OIIE_DESCRIPTION"] = "Configure seu catálogo para ser sincronizado com a conta comercial do Instagram. <br> Novas postagens do Instagram serão adicionadas ao catálogo assim que forem publicadas.";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED"] = "Facebook conectado";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED_ACCOUNT"] = "Autenticação de Facebook";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED_PAGE"] = "Página do Facebook conectada";
$MESS["CRM_OIIE_IMPORT_FEEDBACK"] = "Feedback";
$MESS["CRM_OIIE_INFO"] = "Informações";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTED"] = "Instagram conectado";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTED_ACCOUNT"] = "Conta comercial do Instagram conectada";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTION_TITLE"] = "Conectar Instagram";
$MESS["CRM_OIIE_INSTAGRAM_OTHER_ACCOUNTS"] = "Outras contas comerciais";
$MESS["CRM_OIIE_INSTAGRAM_TITLE"] = "Conectar Instagram";
$MESS["CRM_OIIE_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Apenas uma conta comercial pode ser conectada à Loja on-line Bitrix24. Faça login usando o perfil de administrador da página do Facebook que está vinculada à conta comercial necessária do Instagram.";
$MESS["CRM_OIIE_RTFM_NOTE"] = "Saiba #LINK_START#como isso funciona#LINK_END#";
$MESS["CRM_OIIE_SELECT_THE_PAGE"] = "Selecione a conta comercial do Instagram que deseja conectar à sua Loja on-line Bitrix24";
$MESS["CRM_OIIE_SETTINGS_CHANGE_SETTING"] = "alterar";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_CHANGE_PAGE"] = "A importação de produtos será desconectada da conta comercial atual do Instagram e será conectada à conta selecionada. Os produtos importados anteriormente não serão excluídos. <br> <br> Continuar?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_CHANGE_PAGE_TITLE"] = "Confirmar alteração da conta comercial";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DELETE_PAGE"] = "A importação de produtos será desconectada da conta comercial atual do Instagram. Os produtos importados anteriormente não serão excluídos. <br> <br> Continuar?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DELETE_PAGE_TITLE"] = "Confirmar desconexão da conta comercial";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE"] = "A importação de produtos será desativada e todas as suas preferências de importação serão excluídas. Os produtos importados anteriormente não serão excluídos. <br> <br> Continuar?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE_TITLE"] = "Confirmar desconexão";
$MESS["CRM_OIIE_SETTINGS_DISABLE"] = "desabilitar";
$MESS["CRM_OIIE_SETTINGS_TO_CONNECT"] = "Conectar";
$MESS["CRM_OIIE_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não tem nenhuma página do Facebook com uma conta comercial do Instagram associada que você administre. <br> Crie uma página pública do Facebook agora ou vincule uma conta do Instagram.";
$MESS["CRM_OIIE_TO_CREATE_A_PAGE"] = "Criar";
?>