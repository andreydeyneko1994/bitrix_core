<?
$MESS["CRM_OIIE_AUTHORIZATION"] = "Authentication";
$MESS["CRM_OIIE_AUTHORIZE"] = "Connecter";
$MESS["CRM_OIIE_CHANGE_ANY_TIME"] = "Vous pouvez modifier ou désactiver l'importation de produits à tout moment";
$MESS["CRM_OIIE_CREATE_WITHOUT_CONNECTION"] = "Créer une boutique en ligne non connectée";
$MESS["CRM_OIIE_DEL_REFERENCE"] = "Désactiver";
$MESS["CRM_OIIE_DESCRIPTION"] = "Configurez votre catalogue pour qu'il soit synchronisé avec le compte professionnel Instagram.<br>Les nouvelles publications Instagram seront ajoutées au catalogue dès qu'elles seront publiées.";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED"] = "Facebook connecté";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED_ACCOUNT"] = "Authentification Facebook";
$MESS["CRM_OIIE_FACEBOOK_CONNECTED_PAGE"] = "La page Facebook est connectée";
$MESS["CRM_OIIE_IMPORT_FEEDBACK"] = "Commentaire";
$MESS["CRM_OIIE_INFO"] = "Information";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTED"] = "Instagram connecté";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTED_ACCOUNT"] = "Le compte professionnel Instagram est connecté";
$MESS["CRM_OIIE_INSTAGRAM_CONNECTION_TITLE"] = "Connectez Instagram";
$MESS["CRM_OIIE_INSTAGRAM_OTHER_ACCOUNTS"] = "Autres comptes professionnels";
$MESS["CRM_OIIE_INSTAGRAM_TITLE"] = "Connectez Instagram";
$MESS["CRM_OIIE_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Seul un compte professionnel peut être connecté à la boutique en ligne Bitrix24. Connectez-vous à l'aide du profil d'administrateur de la page Facebook lié à un compte professionnel Instagram requis.";
$MESS["CRM_OIIE_RTFM_NOTE"] = "Découvrez #LINK_START#comment cela fonctionne#LINK_END#";
$MESS["CRM_OIIE_SELECT_THE_PAGE"] = "Sélectionnez un compte professionnel Instagram requis pour vous connecter à votre boutique en ligne Bitrix24";
$MESS["CRM_OIIE_SETTINGS_CHANGE_SETTING"] = "modifier";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_CHANGE_PAGE"] = "
L'importation de produits sera déconnectée du compte professionnel Instagram actuel et connectée au compte professionnel sélectionné. Les produits précédemment importés ne seront pas supprimés.< br>< br>Continuer ?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_CHANGE_PAGE_TITLE"] = "Confirmer le changement de compte professionnel";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DELETE_PAGE"] = "L'importation de produits sera déconnectée du compte professionnel Instagram actuel. Les produits précédemment importés ne seront pas supprimés.< br>< br>Continuer ?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DELETE_PAGE_TITLE"] = "Confirmer la déconnexion du compte professionnel";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE"] = "L'importation du produit sera désactivée et tous vos paramètres d'importation seront supprimés. Les produits précédemment importés ne seront pas supprimés.<br>< br>Continuer ?";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_OIIE_SETTINGS_CONFIRM_DISABLE_TITLE"] = "Confirmer la déconnexion";
$MESS["CRM_OIIE_SETTINGS_DISABLE"] = "désactiver";
$MESS["CRM_OIIE_SETTINGS_TO_CONNECT"] = "Connecter";
$MESS["CRM_OIIE_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'avez pas de page Facebook avec un compte professionnel Instagram associé dont vous êtes administrateur.<br>Créez une page publique sur Facebook dès maintenant ou associez un compte Instagram.";
$MESS["CRM_OIIE_TO_CREATE_A_PAGE"] = "Créer";
?>