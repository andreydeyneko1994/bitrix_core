<?
$MESS["CRM_OIIE_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "Impossible de lier la page";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_PAGE"] = "Impossible de dissocier la page";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_USER"] = "Impossible de dissocier votre compte d'utilisateur";
$MESS["CRM_OIIE_FACEBOOK_NO_IMPORT_AVAILABLE"] = "Importation de produit désactivée";
$MESS["CRM_OIIE_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "La page a été liée";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_PAGE"] = "La page a été dissociée";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_USER"] = "Votre compte d'utilisateur a été dissocié";
$MESS["CRM_OIIE_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "L'importation de produit a été configurée pour être utilisée avec un groupe auquel vous n'avez actuellement pas accès d'administrateur.";
$MESS["CRM_OIIE_FACEBOOK_REPEATING_ERROR"] = "Si le problème persiste, vous devrez peut-être déconnecter le canal et le configurer à nouveau.";
$MESS["CRM_OIIE_FACEBOOK_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez soumettre à nouveau le formulaire.";
$MESS["CRM_OIIE_GO_TO_IMPORT"] = "La page a été connectée. <a href=\"#LINK#\">Procéder à l'importation du produit</a>";
$MESS["CRM_OIIE_IMPORT_CONNECTED"] = "L'importation de produit a été activée";
$MESS["CRM_OIIE_IMPORT_DISCONNECTED"] = "L'importation de produit a été désactivée";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CRM"] = "Le module CRM n'est pas installé.";
$MESS["CRM_OIIE_SETTINGS_NO_DISABLE"] = "Erreur lors de la désactivation de l'importation du produit";
$MESS["CRM_OIIE_TITLE"] = "Connectez Instagram à votre boutique en ligne";
?>