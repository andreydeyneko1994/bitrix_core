<?
$MESS["CRM_OIIE_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "Não foi possível vincular a página";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_PAGE"] = "Não foi possível desvincular a página";
$MESS["CRM_OIIE_FACEBOOK_NO_DEL_USER"] = "Não foi possível desvincular sua conta de usuário";
$MESS["CRM_OIIE_FACEBOOK_NO_IMPORT_AVAILABLE"] = "Importação de produtos desabilitada";
$MESS["CRM_OIIE_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "A página foi vinculada";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_PAGE"] = "A página foi desvinculada";
$MESS["CRM_OIIE_FACEBOOK_OK_DEL_USER"] = "Sua conta de usuário foi desvinculada";
$MESS["CRM_OIIE_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "A importação de produtos foi configurada para ser usada com o grupo a qual você atualmente não tem acesso administrativo.";
$MESS["CRM_OIIE_FACEBOOK_REPEATING_ERROR"] = "Se o problema persistir, pode ser necessário desconectar o canal e configurá-lo novamente.";
$MESS["CRM_OIIE_FACEBOOK_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["CRM_OIIE_GO_TO_IMPORT"] = "A página foi conectada. <a href=\"#LINK#\">Continuar com a importação de produtos</a>";
$MESS["CRM_OIIE_IMPORT_CONNECTED"] = "Importação de produtos foi habilitada";
$MESS["CRM_OIIE_IMPORT_DISCONNECTED"] = "Importação de produtos foi desabilitada";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_OIIE_MODULE_NOT_INSTALLED_CRM"] = "O módulo CRM não está instalado.";
$MESS["CRM_OIIE_SETTINGS_NO_DISABLE"] = "Erro ao desabilitar a importação de produtos";
$MESS["CRM_OIIE_TITLE"] = "Conectar Instagram à sua Loja On-line";
?>