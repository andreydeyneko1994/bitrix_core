<?
$MESS["CRM_ENTITY_QPV_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_ENTITY_QPV_CLIENT_NOT_ASSIGNED"] = "nieprzypisane";
$MESS["CRM_ENTITY_QPV_DEAL_NOT_ASSIGNED"] = "nieprzypisane";
$MESS["CRM_ENTITY_QPV_ENTITY_FIELDS_NOT_FOUND"] = "Nie można znaleźć jednostki.";
$MESS["CRM_ENTITY_QPV_ENTITY_ID_NOT_DEFINED"] = "ID jednostki nie jest określone.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_DEFINED"] = "Typ jednostki nie jest określony.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_SUPPORTED"] = "Określny typ jednostki nie jest obsługiwany.";
$MESS["CRM_ENTITY_QPV_HIDDEN_COMPANY"] = "Ukryta firma";
$MESS["CRM_ENTITY_QPV_HIDDEN_CONTACT"] = "Ukryty kontakt";
$MESS["CRM_ENTITY_QPV_LOCATION_NOT_ASSIGNED"] = "nieprzypisane";
$MESS["CRM_ENTITY_QPV_MULTI_FIELD_NOT_ASSIGNED"] = "nieokreślone";
$MESS["CRM_ENTITY_QPV_NOT_AUTHORIZED"] = "Użytkownik nie zalogowany";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "wartość niezdefiniowana";
$MESS["CRM_ENTITY_QPV_PAY_SYSTEM_NOT_ASSIGNED"] = "nieprzypisane";
$MESS["CRM_ENTITY_QPV_QUOTE_NOT_ASSIGNED"] = "nieprzypisane";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
?>