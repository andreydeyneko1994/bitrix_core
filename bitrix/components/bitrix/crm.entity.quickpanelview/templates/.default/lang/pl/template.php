<?
$MESS["CRM_ENTITY_QPV_COMPANY_HEADER"] = "Firma";
$MESS["CRM_ENTITY_QPV_COMPANY_NOT_SELECTED"] = "Nie wybrano żadnej firmy";
$MESS["CRM_ENTITY_QPV_CONTACT_HEADER"] = "Kontakt";
$MESS["CRM_ENTITY_QPV_CONTACT_NOT_SELECTED"] = "Nie wybrano żadnego kontaktu";
$MESS["CRM_ENTITY_QPV_CONTROL_FIELD_DATA_NOT_SAVED"] = "Wartość \"#FIELD#\" nie została zapisana.";
$MESS["CRM_ENTITY_QPV_DD_BIN_PROMPTING"] = "Aby schować pole, przenieś je do <a id=\"#DEMO_BTN_ID#\" href=\"#\">kosza na śmieci na prawo od formularza</a> lub kliknij na ikonę obok nazwy pola aby otworzyć menu kontekstowe. <a id=\"#CLOSE_BTN_ID#\" href=\"#\">Zamknij</a>";
$MESS["CRM_ENTITY_QPV_DELETE_CONTEXT_MENU_ITEM"] = "Usuń";
$MESS["CRM_ENTITY_QPV_DELETION_CONFIRMATION"] = "Na pewno chcesz ukryć to pole?";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_ALREADY_EXISTS"] = "To pole jest już zawarte w kolumnie";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_NOT_SUPPORTED"] = "Przepraszamy, ale przeniesienie tego pola do panelu powyżej nie jest jeszcze obsługiwane.";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_TITLE"] = "Błąd przenoszenia pola";
$MESS["CRM_ENTITY_QPV_EDIT_CONTEXT_MENU_ITEM"] = "Edytuj";
$MESS["CRM_ENTITY_QPV_INFO_DLG_BTN_CONTINUE"] = "Kontynuuj";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "nie wybrano";
$MESS["CRM_ENTITY_QPV_RESET_FOR_ALL_MENU_ITEM"] = "Resetuj preferencje dla wszystkich użytkowników";
$MESS["CRM_ENTITY_QPV_RESET_MENU_ITEM"] = "Wyczyść ustawienia";
$MESS["CRM_ENTITY_QPV_RESPONSIBLE_CHANGE"] = "zmień";
$MESS["CRM_ENTITY_QPV_SAVE_FOR_ALL_MENU_ITEM"] = "Zapisz preferencje dla wszystkich użytkowników";
$MESS["CRM_ENTITY_QPV_SIP_MGR_ENABLE_CALL_RECORDING"] = "Zapis rozmowy";
$MESS["CRM_ENTITY_QPV_SIP_MGR_UNKNOWN_RECIPIENT"] = "Nierozpozany numer";
$MESS["CRM_ENTITY_QPV_SUM_HEADER"] = "Razem";
$MESS["CRM_ENTITY_QPV__SIP_MGR_MAKE_CALL"] = "Połączenie";
?>