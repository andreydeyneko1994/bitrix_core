<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Statusy";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Uprawnienia niewystarczające do konfigurowania tuneli i lejków";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Pipeliny nie są dostępne";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Pipeliny #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Lejki i tunele sprzedażowe: deale";
