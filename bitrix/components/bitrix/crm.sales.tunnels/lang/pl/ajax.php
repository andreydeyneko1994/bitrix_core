<?php
$MESS["CRM_SALES_STAGE_CREATE_ERROR"] = "Nie można utworzyć etapu";
$MESS["CRM_SALES_STAGE_UPDATE_ERROR"] = "Nie można zaktualizować etapu";
$MESS["CRM_SALES_TUNNELS_ROBOTS_NOT_SUPPORTED"] = "Ten typ nie obsługuje automatyzacji";
$MESS["CRM_SALES_TUNNELS_STAGE_HAS_DEALS"] = "Ten etap zawiera aktywne elementy. Przed usunięciem etapu przenieś te deale.";
$MESS["CRM_SALES_TUNNELS_STAGE_IS_SYSTEM"] = "Nie można usunąć etapu";
$MESS["CRM_SALES_TUNNELS_STAGE_NOT_FOUND"] = "Nie można uzyskać informacji o etapie";
$MESS["CRM_ST_ACCESS_ERROR"] = "Odmowa dostępu";
