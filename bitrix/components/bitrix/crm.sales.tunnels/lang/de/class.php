<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Status";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Sie haben nicht genügend Rechte, um Funnels und Tunnels zu konfigurieren";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Funnels und Tunnels sind nicht verfügbar";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Funnels und Tunnels #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Funnels und Sales Tunnels: Aufträge";
