<?php
$MESS["CRM_SALES_STAGE_CREATE_ERROR"] = "Impossible de créer l'étape";
$MESS["CRM_SALES_STAGE_UPDATE_ERROR"] = "Impossible d'actualiser l'étape";
$MESS["CRM_SALES_TUNNELS_ROBOTS_NOT_SUPPORTED"] = "Ce type ne supporte pas l'automatisation";
$MESS["CRM_SALES_TUNNELS_STAGE_HAS_DEALS"] = "Cette étape compte des éléments actifs. Déplacez les transactions avant de la supprimer.";
$MESS["CRM_SALES_TUNNELS_STAGE_IS_SYSTEM"] = "Impossible de supprimer l'étape";
$MESS["CRM_SALES_TUNNELS_STAGE_NOT_FOUND"] = "Échec de la récupération des informations sur l'étape";
$MESS["CRM_ST_ACCESS_ERROR"] = "Accès refusé";
