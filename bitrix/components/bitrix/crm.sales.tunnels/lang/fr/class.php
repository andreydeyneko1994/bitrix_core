<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Statuts";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Permissions insuffisantes pour configurer les funnels et les entonnoirs";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Les pipelines ne sont pas disponibles";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Pipelines #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Entonnoirs et tunnels de vente : transactions";
