<?php
$MESS["CRM_SALES_STAGE_CREATE_ERROR"] = "No se puede crear la etapa";
$MESS["CRM_SALES_STAGE_UPDATE_ERROR"] = "No se puede actualizar la etapa";
$MESS["CRM_SALES_TUNNELS_ROBOTS_NOT_SUPPORTED"] = "Este tipo no es compatible con la automatización";
$MESS["CRM_SALES_TUNNELS_STAGE_HAS_DEALS"] = "Hay elementos activos en esta etapa. Mueva las negociaciones antes de eliminar la etapa.";
$MESS["CRM_SALES_TUNNELS_STAGE_IS_SYSTEM"] = "No se puede eliminar la etapa";
$MESS["CRM_SALES_TUNNELS_STAGE_NOT_FOUND"] = "No se puede obtener información de la etapa";
$MESS["CRM_ST_ACCESS_ERROR"] = "Acceso denegado";
