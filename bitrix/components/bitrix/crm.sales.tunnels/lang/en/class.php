<?php
$MESS["CRM_SALES_STATUSES_TITLE"] = "Statuses";
$MESS["CRM_SALES_TUNNELS_ACCESS_DENIED"] = "Insufficient permission to configure funnels and tunnels";
$MESS["CRM_SALES_TUNNELS_ENTITY_CATEGORY_DISABLED"] = "Funnels and tunnels are not available";
$MESS["CRM_SALES_TUNNELS_TITLE"] = "Funnels and tunnels #NAME#";
$MESS["CRM_SALES_TUNNELS_TITLE_DEAL"] = "Funnels and sales tunnels: Deals";
