<?php
$MESS["CRM_MENU_RIGHTS_CATEGORY_ALL_FOR_ALL"] = "Przyznaj dostęp wszystkim użytkownikom";
$MESS["CRM_MENU_RIGHTS_CATEGORY_COPY_FROM_TUNNELS"] = "Skopiuj uprawnienia dostępu z lejka";
$MESS["CRM_MENU_RIGHTS_CATEGORY_CUSTOM"] = "Niestandardowe ustawienia dostępu";
$MESS["CRM_MENU_RIGHTS_CATEGORY_NONE_FOR_ALL"] = "Zamknij dostęp dla wszystkich użytkowników";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ALL"] = "Przyznaj dostęp wyłącznie do mojego deala";
$MESS["CRM_ST_ACTION_COPY"] = "kopiuj";
$MESS["CRM_ST_ACTION_MOVE"] = "przenieś";
$MESS["CRM_ST_ADD_FUNNEL_BUTTON"] = "Dodaj lejek";
$MESS["CRM_ST_ADD_NEW_CATEGORY_BUTTON_LABEL"] = "Dodaj lejek sprzedażowy";
$MESS["CRM_ST_CATEGORY_DRAG_BUTTON"] = "Przeciągnij, aby posortować lejki";
$MESS["CRM_ST_CONFIRM_STAGE_REMOVE_TEXT"] = "Na pewno usunąć ten etap?";
$MESS["CRM_ST_DOT_TITLE"] = "Przeciągnij, aby utworzyć tunel";
$MESS["CRM_ST_EDIT_CATEGORY_TITLE"] = "Edytuj nazwę";
$MESS["CRM_ST_EDIT_RIGHTS_CATEGORY"] = "Edytuj ustawienia";
$MESS["CRM_ST_ERROR_POPUP_CLOSE_BUTTON_LABEL"] = "Zamknij";
$MESS["CRM_ST_ERROR_POPUP_TITLE"] = "Błąd";
$MESS["CRM_ST_GENERATOR_HELP_BUTTON"] = "Przeczytaj o zwiększaniu sprzedaży";
$MESS["CRM_ST_GENERATOR_SETTINGS_LINK_LABEL"] = "Zwiększanie sprzedaży";
$MESS["CRM_ST_HELP_BUTTON"] = "Pomoc";
$MESS["CRM_ST_NOTIFICATION_CHANGES_SAVED"] = "Zapisano zmiany.";
$MESS["CRM_ST_REMOVE"] = "Usuń";
$MESS["CRM_ST_REMOVE_CATEGORY"] = "Usuń lejek";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_BUTTON_LABEL"] = "usuń";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_CANCEL_BUTTON_LABEL"] = "Anuluj";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_DESCRIPTION"] = "Usunięcie lejka spowoduje usunięcie wszystkich skonfigurowanych tuneli";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_TITLE"] = "Usuń lejek \"#name#\"";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_REMOVE_BUTTON_LABEL"] = "usuń lejek";
$MESS["CRM_ST_ROBOTS_HELP_BUTTON"] = "Przeczytaj o automatyzacji systemu CRM";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Automatyzacja sprzedaży CRM Bitrix24<br><br>
	Twórz scenariusze do systemu CRM, aby windować deale: przydzielaj zadania, planuj spotkania, uruchamiaj ukierunkowane kampanie reklamowe i wystawiaj faktury. Reguły automatyzacji poprowadzą menedżera do kolejnych kroków na każdym etapie deala, aby zakończyć zadanie.<br><br>
	Wyzwalacze automatycznie reagują na działania klienta (odwiedzanie witryny, wypełnienie formularza, opublikowanie komentarza w sieci społecznościowej lub nawiązanie połączenia) i uruchamiają regułę automatyzacji, która pomaga w przemianie leada w deala.<br><br>
	Reguły automatyzacji i wyzwalacze wykonują wszystkie rutynowe operacje bez konieczności szkolenia menedżerów: skonfiguruj raz i kontynuuj pracę!<br><br>
	Dodaj reguły automatyzacji i zwiększ sprzedaż!<br><br>
	To narzędzie jest dostępne wyłącznie w płatnych subskrypcjach Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Reguły automatyzacji";
$MESS["CRM_ST_ROBOT_ACTION_COPY"] = "Kopiuj pomocnika";
$MESS["CRM_ST_ROBOT_ACTION_MOVE"] = "Przenieś pomocnika";
$MESS["CRM_ST_ROBOT_SETTINGS_LINK_LABEL"] = "Skonfiguruj reguły automatyzacji";
$MESS["CRM_ST_SAVE_ERROR"] = "Wystąpił błąd podczas zapisywania lejka";
$MESS["CRM_ST_SETTINGS"] = "Skonfiguruj";
$MESS["CRM_ST_STAGES_DISABLED"] = "Etapy wyłączone";
$MESS["CRM_ST_STAGES_GROUP_FAIL"] = "Niepowodzenie";
$MESS["CRM_ST_STAGES_GROUP_IN_PROGRESS"] = "W toku";
$MESS["CRM_ST_STAGES_GROUP_SUCCESS"] = "Sukces";
$MESS["CRM_ST_TITLE_EDITOR_PLACEHOLDER"] = "Nowy lejek";
$MESS["CRM_ST_TUNNEL_BUTTON_LABEL"] = "Tunel";
$MESS["CRM_ST_TUNNEL_BUTTON_TITLE"] = "Edytuj lub usuń tunel";
$MESS["CRM_ST_TUNNEL_EDIT_ACCESS_DENIED"] = "Uprawnienia niewystarczające do utworzenia tunelu.";
