<?php
$MESS["CRM_MENU_RIGHTS_CATEGORY_ALL_FOR_ALL"] = "Accorder l'accès à tous les utilisateurs";
$MESS["CRM_MENU_RIGHTS_CATEGORY_COPY_FROM_TUNNELS"] = "Copier les autorisations d'accès de l'entonnoir";
$MESS["CRM_MENU_RIGHTS_CATEGORY_CUSTOM"] = "Paramètres d'accès personnalisés";
$MESS["CRM_MENU_RIGHTS_CATEGORY_NONE_FOR_ALL"] = "Révoquer l'accès de tous les utilisateurs";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ALL"] = "Accorder l'accès seulement à ma transaction";
$MESS["CRM_ST_ACTION_COPY"] = "сopier";
$MESS["CRM_ST_ACTION_MOVE"] = "déplacer";
$MESS["CRM_ST_ADD_FUNNEL_BUTTON"] = "Ajouter un entonnoir";
$MESS["CRM_ST_ADD_NEW_CATEGORY_BUTTON_LABEL"] = "Ajouter un entonnoir des ventes";
$MESS["CRM_ST_CATEGORY_DRAG_BUTTON"] = "Faites glisser pour trier les entonnoirs";
$MESS["CRM_ST_CONFIRM_STAGE_REMOVE_TEXT"] = "Voulez-vous vraiment supprimer cette étape ?";
$MESS["CRM_ST_DOT_TITLE"] = "Faites glisser pour créer un funnel";
$MESS["CRM_ST_EDIT_CATEGORY_TITLE"] = "Modifier le nom";
$MESS["CRM_ST_EDIT_RIGHTS_CATEGORY"] = "Modifier les paramètres";
$MESS["CRM_ST_ERROR_POPUP_CLOSE_BUTTON_LABEL"] = "Fermer";
$MESS["CRM_ST_ERROR_POPUP_TITLE"] = "Erreur";
$MESS["CRM_ST_GENERATOR_HELP_BUTTON"] = "Plus d'informations sur l'augmentation des ventes";
$MESS["CRM_ST_GENERATOR_SETTINGS_LINK_LABEL"] = "Augmentation des ventes";
$MESS["CRM_ST_HELP_BUTTON"] = "Aide";
$MESS["CRM_ST_NOTIFICATION_CHANGES_SAVED"] = "Les modifications ont été enregistrées.";
$MESS["CRM_ST_REMOVE"] = "Supprimer";
$MESS["CRM_ST_REMOVE_CATEGORY"] = "Supprimer l'entonnoir";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_BUTTON_LABEL"] = "supprimer";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_CANCEL_BUTTON_LABEL"] = "Annuler";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_DESCRIPTION"] = "La suppression d'un entonnoir supprimera tous les funnels configurés";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_TITLE"] = "Supprimer l'entonnoir « #name# »";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_REMOVE_BUTTON_LABEL"] = "supprimer l'entonnoir";
$MESS["CRM_ST_ROBOTS_HELP_BUTTON"] = "Plus d'informations sur l'automatisation du CRM";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	Ventes automatisées du CRM Bitrix24<br><br>
	Créez des scénarios pour CRM afin de faire progresser vos transactions : assignez des tâches, prévoyez des réunions, lancez des campagnes publicitaires ciblées et créez des factures. Les règles d'automatisation guideront votre gestionnaire aux prochaines étapes de chaque transaction afin de finaliser le travail.<br><br>
	Les déclencheurs réagissent automatiquement aux actions du client (quand le site est consulté, quand un formulaire est rempli, quand un commentaire de réseau social est publié ou quand un appel est passé) et lancent une règle d'automatisation qui aide à transformer le prospect en transaction.<br><br>
	Les règles d'automatisation et les déclencheurs s'occupent de toutes les opérations routinières sans formation pour vos responsables : configurez une fois et c'est bon !<br><br>
	Ajoutez des règles d'automatisation et améliorez vos ventes !<br><br>
	Cet outil est réservé aux abonnements commerciaux Bitrix24.
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "Règles d'automatisation";
$MESS["CRM_ST_ROBOT_ACTION_COPY"] = "Copier l'assistant";
$MESS["CRM_ST_ROBOT_ACTION_MOVE"] = "Déplacer l'assistant";
$MESS["CRM_ST_ROBOT_SETTINGS_LINK_LABEL"] = "Configurer les règles d'automatisation";
$MESS["CRM_ST_SAVE_ERROR"] = "Une erreur est survenue lors de l'enregistrement de l'entonnoir";
$MESS["CRM_ST_SETTINGS"] = "Configurer";
$MESS["CRM_ST_STAGES_DISABLED"] = "Étapes désactivées";
$MESS["CRM_ST_STAGES_GROUP_FAIL"] = "Échec";
$MESS["CRM_ST_STAGES_GROUP_IN_PROGRESS"] = "En cours";
$MESS["CRM_ST_STAGES_GROUP_SUCCESS"] = "Réussite";
$MESS["CRM_ST_TITLE_EDITOR_PLACEHOLDER"] = "Nouvel entonnoir";
$MESS["CRM_ST_TUNNEL_BUTTON_LABEL"] = "Funnel";
$MESS["CRM_ST_TUNNEL_BUTTON_TITLE"] = "Modifier ou supprimer le funnel";
$MESS["CRM_ST_TUNNEL_EDIT_ACCESS_DENIED"] = "Permission insuffisante pour créer un funnel.";
