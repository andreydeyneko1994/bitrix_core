<?
$MESS["ME_ACTION"] = "Commencer la réunion";
$MESS["ME_AGENDA"] = "Ordre du jour";
$MESS["ME_CHANGE"] = "changer";
$MESS["ME_CLOSE"] = "Terminer la réunion";
$MESS["ME_COMMENTS"] = "Commentaire";
$MESS["ME_COPY"] = "Créer la réunion suivante";
$MESS["ME_CURRENT_STATE"] = "Statut";
$MESS["ME_DATE_START"] = "Commencer";
$MESS["ME_DESCR_TITLE"] = "Description de la réunion";
$MESS["ME_EDIT_TITLE"] = "Éditer";
$MESS["ME_FILES"] = "Fichiers";
$MESS["ME_GROUP"] = "Projet";
$MESS["ME_KEEPER"] = "Secrétaire";
$MESS["ME_LIST_TITLE"] = "Vers la liste des réunions";
$MESS["ME_MEMBERS"] = "Membres";
$MESS["ME_OWNER"] = "Organisateur";
$MESS["ME_PLACE"] = "Emplacement";
$MESS["ME_PREPARE"] = "Reprendre la réunion";
$MESS["ME_REFUSED"] = "Refusé(e)s";
?>