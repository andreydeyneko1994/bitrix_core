<?
$MESS["ME_ADD_EX"] = "Dodaj ze spotkania";
$MESS["ME_AGENDA"] = "Program";
$MESS["ME_AGENDA_ADD"] = "Dodaj temat";
$MESS["ME_AGENDA_ADD_DESCRIPTION"] = "Dodaj opis";
$MESS["ME_AGENDA_ADD_TASK"] = "Zrób zadanie";
$MESS["ME_AGENDA_CONFIRM_AGENDA"] = "Usuń temat tego programu";
$MESS["ME_AGENDA_CONFIRM_PROTO"] = "Usuń pozycję minut";
$MESS["ME_AGENDA_DEADLINE_DEFAULT"] = "Data wykonania";
$MESS["ME_AGENDA_HIDE_COMMENTS"] = "Ukryj komentarze";
$MESS["ME_AGENDA_NO_RESPONSIBLE"] = "(nie określone)";
$MESS["ME_AGENDA_REPORT"] = "Raport";
$MESS["ME_AGENDA_TASK"] = "Zadanie";
$MESS["ME_AGENDA_TASK_ADDED"] = "Powiąż zadanie";
$MESS["ME_AGENDA_TT_ADDSUB"] = "Dodaj podtemat";
$MESS["ME_AGENDA_TT_CANCEL"] = "Anuluj";
$MESS["ME_AGENDA_TT_CHECK"] = "Zaznacz Jako Zakończone";
$MESS["ME_AGENDA_TT_COMMENTS_C"] = "Komentarze";
$MESS["ME_AGENDA_TT_DELETE"] = "Usuń";
$MESS["ME_AGENDA_TT_DRAG"] = "Zamień tematy";
$MESS["ME_AGENDA_TT_EDIT"] = "Edytuj";
$MESS["ME_AGENDA_TT_SAVE"] = "Zapisz";
$MESS["ME_AGENDA_TT_SHIFT"] = "Przesuń tematy";
$MESS["ME_AGENDA_TT_TASKS_C"] = "Powiązane zadania";
$MESS["ME_PROTO"] = "minut(y)";
$MESS["ME_PROTO_ADD"] = "Dodaj pozycję minut";
$MESS["ME_PROTO_OUTSIDE"] = "Tematy poza programem";
$MESS["ME_TASK_ADD"] = "Dodaj z zadania";
?>