<?php
$MESS["SONET_C30_COMMENT_SUBMIT"] = "Enviar";
$MESS["SONET_C30_COUNTER_TEXT_1"] = "Mensajes nuevos:";
$MESS["SONET_C30_DIALOG_CANCEL_BUTTON"] = "Cancelar";
$MESS["SONET_C30_DIALOG_CLOSE_BUTTON"] = "Cerrar";
$MESS["SONET_C30_DIALOG_SUBMIT_BUTTON"] = "Guardar";
$MESS["SONET_C30_DIALOG_TRANSPORT_TITLE"] = "Notificación:";
$MESS["SONET_C30_FEED_NOTIFICATION_NOTASKS_BUTTON_MORE"] = "Detalles";
$MESS["SONET_C30_FEED_NOTIFICATION_NOTASKS_BUTTON_OK"] = "OK";
$MESS["SONET_C30_FEED_NOTIFICATION_NOTASKS_DESC2"] = "Ahora las tareas no se mostrarán en el Feed, en cambio, puede acceder a ellas en el área de Tareas. Seguirá recibiendo notificaciones sobre las tareas en el mensajero instantáneo.";
$MESS["SONET_C30_FEED_NOTIFICATION_NOTASKS_TITLE"] = "Pruebe la nueva vista de tareas basada en el estado activo";
$MESS["SONET_C30_INHERITED"] = "Por defecto";
$MESS["SONET_C30_MENU_TITLE_CREATETASK"] = "Crear tarea";
$MESS["SONET_C30_MENU_TITLE_DELETE"] = "Eliminar del noticias";
$MESS["SONET_C30_MENU_TITLE_DELETE2"] = "Eliminar del feed";
$MESS["SONET_C30_MENU_TITLE_DELETE_CONFIRM"] = "¿Seguro que quieres borrar este mensaje y todos los comentarios agregado a él?";
$MESS["SONET_C30_MENU_TITLE_DELETE_FAILURE"] = "Error al eliminar el mensaje.";
$MESS["SONET_C30_MENU_TITLE_HREF"] = "Abrir mensaje";
$MESS["SONET_C30_MENU_TITLE_LINK"] = "Enlace del mensaje";
$MESS["SONET_C30_MENU_TITLE_LINK2"] = "Copiar link";
$MESS["SONET_C30_MENU_TITLE_TRANSPORT"] = "Notificaciones";
$MESS["SONET_C30_MORE"] = "Más eventos";
$MESS["SONET_C30_NO_SUBSCRIPTIONS"] = "No hay suscripciones disponibles";
$MESS["SONET_C30_T_EMPTY"] = "No hay mensajes en el noticias";
$MESS["SONET_C30_T_EMPTY_SEARCH"] = "No se ha encontrado nada";
$MESS["SONET_C30_T_LINK_COPIED"] = "link copiado";
$MESS["SONET_C30_T_MESSAGE_EXPAND"] = "Expandir";
$MESS["SONET_C30_T_MESSAGE_HIDE"] = "ocultar";
$MESS["SONET_C30_T_MESSAGE_SHOW"] = "mostrar";
$MESS["SONET_C30_T_MORE_WAIT"] = "Cargando...";
$MESS["SONET_C30_T_RELOAD_NEEDED"] = "Actualizar el noticias";
$MESS["SONET_C30_T_RELOAD_NEEDED2"] = "Actualizar el Feed";
$MESS["SONET_C30_T_USER"] = "Usuario";
