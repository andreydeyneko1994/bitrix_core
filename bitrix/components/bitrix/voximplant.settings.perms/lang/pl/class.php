<?
$MESS["VOXIMPLANT_PERM_ACCESS_DENIED"] = "Niewystarczające uprawnienia dostępu";
$MESS["VOXIMPLANT_PERM_LICENSING_ERROR"] = "Twoja licencja nie zezwala na edycję uprawnień dostępu.";
$MESS["VOXIMPLANT_PERM_UNKNOWN_ACCESS_CODE"] = "(nieznany ID dostępu)";
$MESS["VOXIMPLANT_PERM_UNKNOWN_SAVE_ERROR"] = "Błąd podczas zapisywania danych";
?>