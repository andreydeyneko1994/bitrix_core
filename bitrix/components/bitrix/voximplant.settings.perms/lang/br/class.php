<?
$MESS["VOXIMPLANT_PERM_ACCESS_DENIED"] = "Permissões de acesso insuficientes";
$MESS["VOXIMPLANT_PERM_LICENSING_ERROR"] = "Sua licença não permite a edição de permissões de acesso.";
$MESS["VOXIMPLANT_PERM_UNKNOWN_ACCESS_CODE"] = "(ID de acesso desconhecido)";
$MESS["VOXIMPLANT_PERM_UNKNOWN_SAVE_ERROR"] = "Erro ao salvar os dados";
?>