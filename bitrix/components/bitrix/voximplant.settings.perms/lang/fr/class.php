<?
$MESS["VOXIMPLANT_PERM_ACCESS_DENIED"] = "Autorisations d'accès insuffisantes";
$MESS["VOXIMPLANT_PERM_LICENSING_ERROR"] = "Votre licence ne vous permet pas d'éditer les permissions d'accès.";
$MESS["VOXIMPLANT_PERM_UNKNOWN_ACCESS_CODE"] = "(ID d'accès inconnu)";
$MESS["VOXIMPLANT_PERM_UNKNOWN_SAVE_ERROR"] = "Erreur d'enregistrement des données";
?>