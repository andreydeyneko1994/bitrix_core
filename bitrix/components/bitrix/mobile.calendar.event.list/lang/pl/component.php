<?
$MESS["CALENDAR_MODULE_IS_NOT_INSTALLED"] = "Moduł \"Kalendarz Wydarzeń\" nie jest zainstalowany.";
$MESS["EVENTS_GROUP_LATE"] = "Później";
$MESS["EVENTS_GROUP_TODAY"] = "dzisiaj";
$MESS["EVENTS_GROUP_TOMORROW"] = "Jutro";
$MESS["MB_CAL_EVENTS_COUNT"] = "Razem wydarzeń: #COUNT#";
$MESS["MB_CAL_EVENT_ALL_DAY"] = "Cały dzień";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "d.m.Y";
$MESS["MB_CAL_EVENT_DATE_FROM_TO"] = "Od #DATE_FROM# do #DATE_TO#";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G:i";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "h:i a";
$MESS["MB_CAL_EVENT_TIME_FROM_TO_TIME"] = "od #TIME_FROM# do #TIME_TO#";
$MESS["MB_CAL_NO_EVENTS"] = "Brak nadchodzących wydarzeń";
?>