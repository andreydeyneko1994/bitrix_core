<?
$MESS["NEXT_BASED_ON_DEAL_MULTY"] = "Foi criado modelo para negócios recorrentes: #ID_LINE# ";
$MESS["NEXT_BASED_ON_DEAL_ONCE"] = "Foi criado modelo para negócio recorrente ##ID# ";
$MESS["NEXT_DEAL_EMPTY"] = "Nada selecionado";
$MESS["NEXT_EXECUTION_DEAL_HINT"] = "O próximo negócio será criado em #DATE_EXECUTION#";
$MESS["SIGN_NUM_WITH_DEAL_ID"] = "##DEAL_ID#";
?>