<?
$MESS["RPA_STAGES_COMMON_STAGES_LABEL"] = "Étapes supplémentaires";
$MESS["RPA_STAGES_FAIL_STAGE_LABEL"] = "Étapes échouées";
$MESS["RPA_STAGES_FAIL_STAGE_TITLE"] = "Échec";
$MESS["RPA_STAGES_FINAL_STAGES_TITLE"] = "Étapes finales";
$MESS["RPA_STAGES_FIRST_STAGE_LABEL"] = "Étape initiale";
$MESS["RPA_STAGES_NEW_STAGE_NAME"] = "Nouvelle étape";
$MESS["RPA_STAGES_STAGE_ADD"] = "Ajouter une étape";
$MESS["RPA_STAGES_STAGE_CHANGE_TITLE"] = "Modifier le nom";
$MESS["RPA_STAGES_STAGE_PANEL_COLOR"] = "Modifier la couleur";
$MESS["RPA_STAGES_STAGE_PANEL_RELOAD"] = "Réinitialiser la vue";
$MESS["RPA_STAGES_SUCCESS_STAGE_LABEL"] = "Étape réussie";
$MESS["RPA_STAGES_SUCCESS_STAGE_TITLE"] = "Succès";
?>