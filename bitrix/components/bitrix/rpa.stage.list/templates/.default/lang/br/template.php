<?
$MESS["RPA_STAGES_COMMON_STAGES_LABEL"] = "Etapas adicionais";
$MESS["RPA_STAGES_FAIL_STAGE_LABEL"] = "Etapas mal sucedidas";
$MESS["RPA_STAGES_FAIL_STAGE_TITLE"] = "Mal sucedida";
$MESS["RPA_STAGES_FINAL_STAGES_TITLE"] = "Etapas finais";
$MESS["RPA_STAGES_FIRST_STAGE_LABEL"] = "Etapa inicial";
$MESS["RPA_STAGES_NEW_STAGE_NAME"] = "Nova etapa";
$MESS["RPA_STAGES_STAGE_ADD"] = "Adicionar etapa";
$MESS["RPA_STAGES_STAGE_CHANGE_TITLE"] = "Editar nome";
$MESS["RPA_STAGES_STAGE_PANEL_COLOR"] = "Alterar cor";
$MESS["RPA_STAGES_STAGE_PANEL_RELOAD"] = "Redefinir exibição";
$MESS["RPA_STAGES_SUCCESS_STAGE_LABEL"] = "Etapa bem sucedida";
$MESS["RPA_STAGES_SUCCESS_STAGE_TITLE"] = "Bem sucedida";
?>