<?
$MESS["TASKS_IMPORT_DONE"] = "A importação foi concluída";
$MESS["TASKS_IMPORT_ERROR"] = "Erro de importação";
$MESS["TASKS_IMPORT_ERROR_FILE_NOT_SELECTED"] = "Nenhum arquivo selecionado";
$MESS["TASKS_IMPORT_ERROR_FILE_WRONG_EXTENSION"] = "Formato de arquivo inválido";
$MESS["TASKS_IMPORT_FILE_NOT_SELECTED"] = "Nenhum arquivo selecionado";
$MESS["TASKS_IMPORT_LINE"] = "Linha ";
$MESS["TASKS_IMPORT_POPUP_WINDOW_TITLE"] = "Selecionar codificação correta";
$MESS["TASKS_IMPORT_STOPPED"] = "Importação abortada";
?>