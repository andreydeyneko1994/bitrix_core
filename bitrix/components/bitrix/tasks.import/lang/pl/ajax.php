<?php
$MESS["TASKS_IMPORT_DEFAULT_CHECKLIST_NAME"] = "Lista kontrolna nr 1";
$MESS["TASKS_IMPORT_ERRORS_UNKNOWN_DELETE_ERROR"] = "Błąd podczas usuwania nieprawidłowego zadania";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_HASH"] = "Nie można uzyskać dostępu do przesłanego pliku";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_PATH"] = "Nieprawidłowa ścieżka pliku do przesłania";
