<?php
$MESS["TASKS_IMPORT_DEFAULT_CHECKLIST_NAME"] = "Lista de verificación #1";
$MESS["TASKS_IMPORT_ERRORS_MODULE_SOCIAL_NETWORK_NOT_INCLUDED"] = "El módulo Red social no está habilitado.";
$MESS["TASKS_IMPORT_ERRORS_MODULE_TASKS_NOT_INCLUDED"] = "El módulo Tareas no está habilitado.";
$MESS["TASKS_IMPORT_ERRORS_UNKNOWN_DELETE_ERROR"] = "Error al eliminar una tarea no válida";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_HASH"] = "No se puede acceder al archivo cargado";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_PATH"] = "Ruta de archivo de carga inválida";
