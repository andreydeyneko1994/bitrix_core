<?php
$MESS["TASKS_IMPORT_DEFAULT_CHECKLIST_NAME"] = "Liste de contrôle #1";
$MESS["TASKS_IMPORT_ERRORS_UNKNOWN_DELETE_ERROR"] = "Erreur lors de la suppression de la tâche non valide";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_HASH"] = "Impossible d'accéder au fichier téléversé";
$MESS["TASKS_IMPORT_ERRORS_WRONG_FILE_PATH"] = "Chemin de téléchargement de fichier non valide";
