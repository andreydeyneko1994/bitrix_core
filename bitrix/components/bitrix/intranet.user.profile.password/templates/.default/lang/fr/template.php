<?php
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CANCEL"] = "Annuler";
$MESS["INTRANET_USER_PROFILE_PASSWORD_BUTTON_CONTINUE"] = "Continuer";
$MESS["INTRANET_USER_PROFILE_PASSWORD_CLOSE"] = "Fermer";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT"] = "Déconnecter sur tous les appareils";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_SUCCESS"] = "Vous avez bien déconnecté tous les appareils.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TEXT"] = "Cela vous déconnectera des Bitrix24 de tous les navigateurs et applications, sauf de celui/celle depuis lequel/laquelle vous consultez ce message.";
$MESS["INTRANET_USER_PROFILE_PASSWORD_LOGOUT_TITLE"] = "Déconnexion";
$MESS["INTRANET_USER_PROFILE_SECURITY_MENU_TITLE"] = "Configurer";
