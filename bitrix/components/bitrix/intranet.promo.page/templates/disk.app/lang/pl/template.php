<?
$MESS["INTRANET_DISK_PROMO_DESC"] = "Dziś Internet jest dostępny niemal zawsze i wszędzie. Mimo to zdarza się, że nie mamy dostępu do przestrzeni dyskowej on-line: na przykład w samolocie. Nie oznacza to, że trzeba przerwać pracę! Aplikacja desktopowa Bitrix24.Drive zsynchronizuje wszystkie Twoje pliki z kopiami lokalnymi na komputerze Edytuj swoje pliki offline – wszystkie zmiany zostaną automatycznie zsynchronizowane z Chmurą, gdy tylko dostępny będzie Internet.";
$MESS["INTRANET_DISK_PROMO_DESC_SUB"] = "Zainstaluj na swoim komputerze i kontynuuj pracę w dowolnym miejscu i o każdej porze!";
$MESS["INTRANET_DISK_PROMO_HEADER"] = "Dodaj swojemu systemowi Bitrix24 jeszcze więcej funkcji zarządzania plikami.";
$MESS["INTRANET_DISK_PROMO_STEP1_TITLE"] = "Pobierz i zainstaluj aplikację #LINK_START#Bitrix24#LINK_END#";
$MESS["INTRANET_DISK_PROMO_STEP2_DESC"] = "Uruchom aplikację w celu bezobsługowego zsynchronizowania plików. Można wybrać foldery, które mają być dostępne lokalnie albo w przeglądarce.";
$MESS["INTRANET_DISK_PROMO_STEP2_TITLE"] = "Połącz Bitrix24.Drive";
$MESS["INTRANET_DISK_PROMO_TITLE_MACOS"] = "Aplikacja desktopowa na MacOS Bitrix24";
$MESS["INTRANET_DISK_PROMO_TITLE_WINDOWS"] = "Aplikacja desktopowa na Windows Bitrix24";
?>