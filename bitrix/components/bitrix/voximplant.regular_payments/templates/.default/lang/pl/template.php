<?
$MESS["VI_REGULAR_CONFIG_RENT"] = "Zarządzaj wynajętymi numerami";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR #MONEY#";
$MESS["VI_REGULAR_FEE_RUR"] = "RUB #MONEY#";
$MESS["VI_REGULAR_FEE_UAH"] = "#MONEY# UAH.";
$MESS["VI_REGULAR_FEE_USD"] = "\$#MONEY#";
$MESS["VI_REGULAR_NOTICE"] = "Uwaga: automatyczne przedłużenie numerów telefonów jest włączone.";
$MESS["VI_REGULAR_NO_MONEY"] = "Nie masz wystarczającego kredytu do wykonania automatycznej, cyklicznej płatności. Musisz doładować swój bilans do #DATE#.";
$MESS["VI_REGULAR_NO_VERIFY"] = "Prawo wymaga dostareczenia dokumentacji prawnej, aby wykorzystywać wynajęte numery. <br><br>Musisz dodać dokumenty do  #DATE#, w przeciwnym razie twoje numery zostaną odłączone. <br><br>#URL_START#Dodaj dokumenty teraz#URL_END#";
$MESS["VI_REGULAR_TABLE_FEE"] = "Miesięczna opłata";
$MESS["VI_REGULAR_TABLE_NUMBER"] = "Liczba";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "Termin płatności";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "Wyłączone, wymagana opłata";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "Włączone";
$MESS["VI_REGULAR_TITLE"] = "Cykliczne płatności";
?>