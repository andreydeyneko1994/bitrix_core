<?
$MESS["VI_REGULAR_CONFIG_RENT"] = "Gérer les Numéros Loués";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR #MONEY#";
$MESS["VI_REGULAR_FEE_RUR"] = "RUB #MONEY#";
$MESS["VI_REGULAR_FEE_UAH"] = "UAH #MONEY#.";
$MESS["VI_REGULAR_FEE_USD"] = "\$#MONEY#";
$MESS["VI_REGULAR_NOTICE"] = "Attention : la prolongation automatique de numéros de téléphone est lancée.";
$MESS["VI_REGULAR_NO_MONEY"] = "Vous ne disposez pas de suffisamment de crédit pour le paiement récurrent automatisé. Vous devez recharger votre balance jusqu'au #DATE#.";
$MESS["VI_REGULAR_NO_VERIFY"] = "La loi exige que vous fournissiez la documentation légale pour utiliser les numéros loués.<br><br>Vous devez télécharger les documents jusqu'au #DATE#, sinon vos numéros seront déconnectés. <br><br>#URL_START#Télécharger les documents maintenant#URL_END#";
$MESS["VI_REGULAR_TABLE_FEE"] = "Frais mensuels";
$MESS["VI_REGULAR_TABLE_NUMBER"] = "Numéro";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "Payée jusqu'à";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "Désactivé, le paiement est requis";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "Activé";
$MESS["VI_REGULAR_TITLE"] = "Paiements récurrents";
?>