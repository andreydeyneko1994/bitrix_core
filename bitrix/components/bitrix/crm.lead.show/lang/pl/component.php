<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Moduł Procesów Biznesowych nie jest zainstalowany.";
$MESS["CRM_ALL"] = "Razem";
$MESS["CRM_FIELD_ADDRESS"] = "Adres";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "Odpowiedzialny";
$MESS["CRM_FIELD_COMMENTS"] = "Komentarz";
$MESS["CRM_FIELD_COMPANY_ID"] = "Firma";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Nazwa Firmy";
$MESS["CRM_FIELD_COMPANY_TITLE_2"] = "Firma";
$MESS["CRM_FIELD_CONTACT_ID"] = "Kontakt";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "Utworzony przez";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Waluta";
$MESS["CRM_FIELD_DATE_CREATE"] = "Utworzony";
$MESS["CRM_FIELD_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_FIELD_ENTITY_TREE"] = "Zależności";
$MESS["CRM_FIELD_EXCH_RATE"] = "Kurs wymiany";
$MESS["CRM_FIELD_FIND"] = "Szukaj";
$MESS["CRM_FIELD_FULL_NAME"] = "Kontakt";
$MESS["CRM_FIELD_HONORIFIC"] = "Pozdrowienie";
$MESS["CRM_FIELD_LAST_NAME"] = "Nazwisko";
$MESS["CRM_FIELD_LEAD_ACTIVITY"] = "Zadania";
$MESS["CRM_FIELD_LEAD_ACTIVITY_LIST"] = "Działania";
$MESS["CRM_FIELD_LEAD_BIZPROC"] = "Proces Biznesowy";
$MESS["CRM_FIELD_LEAD_CALENDAR"] = "Zapisy kalendarza";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "Firmy leada";
$MESS["CRM_FIELD_LEAD_CONTACTS"] = "Kontakty leada";
$MESS["CRM_FIELD_LEAD_DEAL"] = "Deale leada";
$MESS["CRM_FIELD_LEAD_EVENT"] = "Wydarzenia leada";
$MESS["CRM_FIELD_LEAD_QUOTE"] = "Oferty leada";
$MESS["CRM_FIELD_LIVE_FEED"] = "Tablica";
$MESS["CRM_FIELD_MESSENGER"] = "Komunikator";
$MESS["CRM_FIELD_MODIFY_BY_ID"] = "Zmodyfikowane przez";
$MESS["CRM_FIELD_NAME"] = "Imię";
$MESS["CRM_FIELD_OPENED"] = "Dostępny dla wszystkich";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Wartość";
$MESS["CRM_FIELD_PHONE"] = "Telefon";
$MESS["CRM_FIELD_POST"] = "Stanowisko";
$MESS["CRM_FIELD_PRODUCT_ID"] = "Produkt";
$MESS["CRM_FIELD_PRODUCT_ROWS"] = "Produkty leada";
$MESS["CRM_FIELD_SECOND_NAME"] = "Drugie Imię";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Informacja o źródle";
$MESS["CRM_FIELD_SOURCE_ID"] = "Źródło";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Informacja o statusie";
$MESS["CRM_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_FIELD_TITLE"] = "Nazwa leada";
$MESS["CRM_FIELD_UTM"] = "parametry UTM";
$MESS["CRM_FIELD_WEB"] = "Strona";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "Generuj:";
$MESS["CRM_LEAD_EVENT_INFO"] = "#EVENT_NAME# &rarr; #NEW_VALUE#";
$MESS["CRM_LEAD_NAV_TITLE_ADD"] = "Dodaj lead";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "Lead: #NAME#";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "Lista leadów";
$MESS["CRM_LEAD_NAV_TITLE_LIST_SHORT"] = "Leady";
$MESS["CRM_LEAD_SHOW_FIELD_BIRTHDATE"] = "Data urodzin";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_SECTION_ACTIVITIES"] = "Aktywność";
$MESS["CRM_SECTION_ACTIVITY_CALENDAR"] = "Połączenia telefoniczne i wydarzenia";
$MESS["CRM_SECTION_ACTIVITY_MAIN"] = "Działania leada";
$MESS["CRM_SECTION_ACTIVITY_TASK"] = "Zadania";
$MESS["CRM_SECTION_ADDITIONAL"] = "Więcej";
$MESS["CRM_SECTION_BIZPROC"] = "Proces Biznesowy";
$MESS["CRM_SECTION_BIZPROC_MAIN"] = "Procesy biznesowe leada";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Informacje kontaktowe";
$MESS["CRM_SECTION_CONTACT_INFO_2"] = "Informacje kontaktowe";
$MESS["CRM_SECTION_DETAILS"] = "Szczegóły";
$MESS["CRM_SECTION_EVENT_MAIN"] = "Dziennik leada";
$MESS["CRM_SECTION_LEAD"] = "Lead";
$MESS["CRM_SECTION_LIVE_FEED"] = "Tablica";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "Produkty";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[nieprzypisane]";
