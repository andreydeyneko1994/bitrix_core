<?php
$MESS["MD_COMPONENT_CALLTRACKER"] = "Monitor połączeń";
$MESS["MD_COMPONENT_IM_OPENLINES"] = "Otwarte kanały";
$MESS["MD_COMPONENT_IM_RECENT"] = "Czaty";
$MESS["MD_COMPONENT_MORE"] = "Menu";
$MESS["MD_COMPONENT_NOTIFY"] = "Powiadomienia";
$MESS["MD_COMPONENT_TASKS_LIST"] = "Zadania";
$MESS["MD_DISK_TABLE_FOLDERS_FILES"] = "Foldery: #FOLDERS#  Pliki: #FILES#";
$MESS["MD_EMPLOYEES_ALL"] = "Wszyscy Pracownicy";
$MESS["MD_EMPLOYEES_TITLE"] = "Pracownicy";
$MESS["MD_EXTRANET"] = "Extranet";
$MESS["MD_GENERATE_BY_MOBILE"] = "Wygenerowane przez aplikację";
$MESS["MD_GROUPS_TITLE"] = "grupy robocze";
$MESS["MD_MOBILE_APPLICATION"] = "Aplikacje mobilne";
$MESS["TASKS_ROLE_ACCOMPLICE"] = "Pomagam";
$MESS["TASKS_ROLE_AUDITOR"] = "Obserwuję";
$MESS["TASKS_ROLE_ORIGINATOR"] = "Ustawione przeze mnie";
$MESS["TASKS_ROLE_RESPONSIBLE"] = "W toku";
$MESS["TASKS_ROLE_VIEW_ALL"] = "Wszystkie zadania";
