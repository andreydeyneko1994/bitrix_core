<?
$MESS["LEARNING_MAIN_SEARCH_COURSE_TITLE"] = "Szukaj kursu";
$MESS["LEARNING_MAIN_SEARCH_ERROR"] = "Błąd parametrów wyszukiwania:";
$MESS["LEARNING_MAIN_SEARCH_NOTHING_FOUND"] = "Nie znaleziono wyników.";
$MESS["LEARNING_MAIN_SEARCH_SEARCH_CHAPTER"] = "w rozdziale";
$MESS["LEARNING_MAIN_SEARCH_SEARCH_COURSE"] = "w kursie";
$MESS["LEARNING_MAIN_SEARCH_SEARCH_LESSON"] = "w lekcji";
$MESS["LEARNING_MAIN_SEARCH_TITLE"] = "Wyszukiwanie Kursu";
$MESS["LEARNING_MODULE_NOT_INSTALL"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_RESULT_PAGES"] = "Wynik";
$MESS["SEARCH_MODULE_NOT_INSTALL"] = "Moduł wyszukiwania nie jest zainstalowany.";
?>