<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ANSWER_CLIENT"] = "O servidor não pode acessar o portal. Por favor, verifique suas configurações de rede.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_CONNECT"] = "Não foi possível estabelecer a conexão de teste usando os parâmetros fornecidos";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_DATA_SAVE"] = "Não há dados para salvar";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_SAVE"] = "Erro ao salvar os dados. Verifique seus dados e tente novamente.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_CONNECT"] = "A conexão de teste foi estabelecida com sucesso";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_SAVE"] = "A informação foi salva com sucesso.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SETTINGS_INCORRECT"] = "Parâmetros especificados incorretos";
