<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ANSWER_CLIENT"] = "El servidor no puede acceder al portal. Verifique la configuración de su red.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_CONNECT"] = "No pudo establecerse la prueba de conexión utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_SAVE"] = "Se produjo un error al guardar los datos. Revise la entrada e intente nuevamente.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_CONNECT"] = "La prueba de conexión se estableció correctamente";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_SAVE"] = "La información se guardó correctamente.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario nuevamente.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SETTINGS_INCORRECT"] = "Los parámetros que se especificaron son incorrectos";
