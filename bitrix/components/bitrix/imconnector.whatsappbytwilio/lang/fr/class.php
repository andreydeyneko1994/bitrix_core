<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ACTIVE_CONNECTOR"] = "Le connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_ANSWER_CLIENT"] = "Le serveur ne peut accéder au portail. Veuillez vérifier vos paramètres réseau.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_CONNECT"] = "Impossible d'établir la connexion test avec les paramètres fournis";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_DATA_SAVE"] = "Il n'y a aucune donnée à enregistrer";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_NO_SAVE"] = "Erreur d'enregistrement des données. Veuillez vérifier votre entrée et réessayer.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_CONNECT"] = "La connexion test a bien été établie";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_OK_SAVE"] = "Les informations ont bien été enregistrées.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SETTINGS_INCORRECT"] = "Paramètres spécifiés incorrects";
