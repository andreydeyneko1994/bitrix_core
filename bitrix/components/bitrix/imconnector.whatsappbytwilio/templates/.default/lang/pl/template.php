<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_ACCOUNT_LINK"] = "Link do czatu na żywo";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_ACCOUNT_PHONE"] = "Numer telefonu podłączonego konta";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_AUTH_TOKEN"] = "Token autoryzacji:";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CHANGE_ANY_TIME"] = "Można w każdej chwili edytować lub wyłączyć";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECTED"] = "Połączono z WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Chcę</div>
				<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">podłączyć</span> WhatsApp przez Twilio</div>";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_STEP"] = "Skorzystaj z następujących <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">instrukcji</a>, aby podłączyć konto WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_STEP_NEW"] = "Aby podłączyć WhatsApp, #LINK_START#postępuj według instrukcji#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_TITLE"] = "Podłącz WhatsApp do Otwartego kanału";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_COPY"] = "Kopiuj";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_FINAL_FORM_DESCRIPTION"] = "WhatsApp został podłączony do Otwartego kanału. Od teraz wszystkie wiadomości będziesz widzieć w swoim Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_ADDITIONAL_DESCRIPTION"] = "Aby podłączyć WhatsApp do Bitrix24, <a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\');return false;\">postępuj zgodnie z instrukcjami</a>.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "Aby podłączyć WhatsApp, #LINK_START#postępuj według instrukcji#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_1"] = "Zapisuj kontakty i historię komunikacji w CRM";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_2"] = "Poprowadź klienta przez lejek sprzedażowy w CRM";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_3"] = "Odpowiadaj klientom w ich preferowanych miejscach i czasie";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_4"] = "Zapytania klientów są rozdzielane wśród przedstawicieli handlowych zgodnie z zasadami kolejki";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_SUBTITLE"] = "Odpowiadaj klientom zgodnie z ich preferencjami. Jeśli klient uzna WhatsApp za wygodniejsze niż inne środki komunikacji, odbieraj jego wiadomości w Bitrix24 i natychmiast na nie odpowiadaj.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_TITLE"] = "Rozmawiaj z klientami za pośrednictwem WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INFO"] = "Informacje";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Jak podłączyć konto firmowe WhatsApp</span>:";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_I_KNOW_TOKEN"] = "Mam token";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_PHONE"] = "Numer telefonu konta (w formacie międzynarodowym: +99999999999):";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SID"] = "SID konta:";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SIMPLE_FORM_DESCRIPTION"] = "Określ ten adres w polu Webhook w konsoli Twilio:";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_TESTED"] = "Test połączenia";
