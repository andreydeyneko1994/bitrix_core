<?php
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_ACCOUNT_LINK"] = "Lien de chat en direct";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_ACCOUNT_PHONE"] = "Numéro de téléphone du compte connecté";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_AUTH_TOKEN"] = "Jeton d'authentification :";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CHANGE_ANY_TIME"] = "Peut être édité ou désactivé à tout moment";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECTED"] = "WhatsApp connecté";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_HELP"] = "<div class=\"imconnector-field-button-subtitle\">Je voudrais</div>
				<div class=\"imconnector-field-button-name\"><span class=\"imconnector-field-box-text-bold\">connecter</span> un compte WhatsApp</div>";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_STEP"] = "Utilisez les <a onclick=\"top.BX.Helper.show('#ID#');\" class=\"imconnector-field-box-link\">instructions</a> suivantes pour connecter un compte WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_STEP_NEW"] = "#LINK_START#Suivez les instructions#LINK_END# pour connecter WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_CONNECT_TITLE"] = "Connecter WhatsApp à votre Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_COPY"] = "Copier";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_FINAL_FORM_DESCRIPTION"] = "WhatsApp a été connecté à votre Canal ouvert. Dorénavant, vous verrez tous les messages dans votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_ADDITIONAL_DESCRIPTION"] = "<a href=\"#\" onclick=\"top.BX.Helper.show(\'#ID#\');return false;\">Suivez les instructions</a> pour connecter WhatsApp à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "#LINK_START#Suivez les instructions#LINK_END# pour connecter WhatsApp.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_1"] = "Enregistrez les contacts et l'historique des communications dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_2"] = "Guidez le client à travers l'entonnoir de vente dans le CRM";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_3"] = "Répondez à vos clients quand et où ils le souhaitent";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_LIST_ITEM_4"] = "Les demandes des clients sont réparties entre les commerciaux en fonction des règles de la file d'attente";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_SUBTITLE"] = "Répondez à vos clients comme ils le souhaitent. Si un client trouve WhatsApp plus pratique que les autres moyens de communication, recevez ses messages sur votre Bitrix24 et répondez-lui immédiatement.";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INDEX_TITLE"] = "Parlez à vos clients dans WhatsApp";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INFO"] = "Informations";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_INSTRUCTION_TITLE"] = "<span class=\"imconnector-field-box-text-bold\">Comment connecter votre compte WhatsApp professionnel</span> :";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_I_KNOW_TOKEN"] = "J'ai le jeton";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_PHONE"] = "Numéro de téléphone du compte (utilisez le format international : +99999999999) :";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SID"] = "SID de compte :";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_SIMPLE_FORM_DESCRIPTION"] = "Précisez cette adresse dans le champ \"Webhook\" de la console Twilio :";
$MESS["IMCONNECTOR_COMPONENT_WHATSAPPBYTWILIO_TESTED"] = "Test de connexion";
