<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID"] = "Payer";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID_N"] = "Annuler le paiement";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_DLG_TITLE"] = "Préparer l'historique des paiements";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_STATE"] = "#processed# sur #total#";
$MESS["CRM_ORDER_PAYMENT_DELETE"] = "Supprimer";
$MESS["CRM_ORDER_PAYMENT_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cet article ?";
$MESS["CRM_ORDER_PAYMENT_DELETE_TITLE"] = "Supprimer le paiement";
$MESS["CRM_ORDER_PAYMENT_EDIT"] = "Modifier";
$MESS["CRM_ORDER_PAYMENT_EDIT_TITLE"] = "Modifier le paiement";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD"] = "Ajouter un paiement";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD_SHORT"] = "Paiement";
$MESS["CRM_ORDER_PAYMENT_LIST_CHOOSE_ACTION"] = "Sélectionner une action";
$MESS["CRM_ORDER_PAYMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Liste";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_CLOSE"] = "Fermer";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_START"] = "Exécuter";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_STOP"] = "Arrêter";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_REQUEST_ERR"] = "Erreur lors du traitement de la requête.";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_WAIT"] = "Veuillez patienter…";
$MESS["CRM_ORDER_PAYMENT_PAID"] = "Payée";
$MESS["CRM_ORDER_PAYMENT_PAYMENT_SUMMARY"] = "Paiement ##ACCOUNT_NUMBER# du #DATE_BILL#";
$MESS["CRM_ORDER_PAYMENT_REBUILD_ACCESS_ATTRS"] = "Les permissions d'accès actualisées nécessitent que vous mettiez à jour les attributs d'accès actuels en utilisant la <a id='#ID#' target='_blank' href='#URL#'> page de gestion des autorisations</a>.";
$MESS["CRM_ORDER_PAYMENT_SHOW"] = "Afficher";
$MESS["CRM_ORDER_PAYMENT_SHOW_TITLE"] = "Afficher le paiement";
$MESS["CRM_ORDER_PAYMENT_UNPAID"] = "Non payée";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
?>