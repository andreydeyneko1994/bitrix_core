<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID"] = "Pagar";
$MESS["CRM_ORDER_PAYMENT_ACTION_PAID_N"] = "Cancelar pagamento";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_DLG_TITLE"] = "Limpar histórico de pagamento";
$MESS["CRM_ORDER_PAYMENT_BUILD_TIMELINE_STATE"] = "#processed# de #total#";
$MESS["CRM_ORDER_PAYMENT_DELETE"] = "Excluir";
$MESS["CRM_ORDER_PAYMENT_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir este item?";
$MESS["CRM_ORDER_PAYMENT_DELETE_TITLE"] = "Excluir pagamento";
$MESS["CRM_ORDER_PAYMENT_EDIT"] = "Editar";
$MESS["CRM_ORDER_PAYMENT_EDIT_TITLE"] = "Editar pagamento";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD"] = "Adicionar pagamento";
$MESS["CRM_ORDER_PAYMENT_LIST_ADD_SHORT"] = "Pagamento";
$MESS["CRM_ORDER_PAYMENT_LIST_CHOOSE_ACTION"] = "Selecionar ação";
$MESS["CRM_ORDER_PAYMENT_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_CLOSE"] = "Fechar";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_START"] = "Executar";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_REQUEST_ERR"] = "Erro ao processar a solicitação.";
$MESS["CRM_ORDER_PAYMENT_LRP_DLG_WAIT"] = "Aguarde...";
$MESS["CRM_ORDER_PAYMENT_PAID"] = "Pago";
$MESS["CRM_ORDER_PAYMENT_PAYMENT_SUMMARY"] = "Pagamento ##ACCOUNT_NUMBER# de #DATE_BILL#";
$MESS["CRM_ORDER_PAYMENT_REBUILD_ACCESS_ATTRS"] = "As permissões de acesso atualizadas exigem que você atualize os atuais atributos de acesso usando a <a id=\"#ID#\" target=\"_blank\" href= \"#URL#\">página de gerenciamento de permissões</a>.";
$MESS["CRM_ORDER_PAYMENT_SHOW"] = "Visualizar";
$MESS["CRM_ORDER_PAYMENT_SHOW_TITLE"] = "Visualizar pagamento";
$MESS["CRM_ORDER_PAYMENT_UNPAID"] = "Não pago";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
?>