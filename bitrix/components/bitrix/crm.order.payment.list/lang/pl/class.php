<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Moduł Katalog Produktów nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Moduł Waluty nie jest zainstalowany.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Moduł e-Sklepu nie jest zainstalowany.";
$MESS["CRM_PAYMENT_DELETE_ERROR"] = "Błąd usuwania płatności";
$MESS["CRM_PAYMENT_HEADER_ACCOUNT_NUMBER"] = "Płatność #";
$MESS["CRM_PAYMENT_HEADER_CURRENCY"] = "Waluta";
$MESS["CRM_PAYMENT_HEADER_DATE_BILL"] = "Utworzono";
$MESS["CRM_PAYMENT_HEADER_DATE_PAID"] = "Data płatności";
$MESS["CRM_PAYMENT_HEADER_DATE_PAY_BEFORE"] = "Zapłać przed";
$MESS["CRM_PAYMENT_HEADER_ID"] = "ID płatności";
$MESS["CRM_PAYMENT_HEADER_ORDER_ID"] = "ID zamówienia";
$MESS["CRM_PAYMENT_HEADER_PAID"] = "Status";
$MESS["CRM_PAYMENT_HEADER_PAYMENT_SUMMARY"] = "Płatność";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_FULL"] = "Metoda płatności";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_NAME"] = "System płatności";
$MESS["CRM_PAYMENT_HEADER_PAY_VOUCHER_NUM"] = "Numer dokumentu płatności";
$MESS["CRM_PAYMENT_HEADER_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_PAYMENT_HEADER_SUM"] = "Kwota";
$MESS["CRM_PAYMENT_HEADER_USER_ID"] = "Klient";
$MESS["CRM_PAYMENT_NAME"] = "Wysyłka ##ACCOUNT_NUMBER# of #DATE_BILL#";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>