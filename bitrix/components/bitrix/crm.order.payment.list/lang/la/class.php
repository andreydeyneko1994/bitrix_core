<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catálogo Comercial no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Moneda no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PAYMENT_DELETE_ERROR"] = "Error al eliminar el pago";
$MESS["CRM_PAYMENT_HEADER_ACCOUNT_NUMBER"] = "Pago #";
$MESS["CRM_PAYMENT_HEADER_CURRENCY"] = "Moneda";
$MESS["CRM_PAYMENT_HEADER_DATE_BILL"] = "Creado el";
$MESS["CRM_PAYMENT_HEADER_DATE_PAID"] = "Fecha de pago";
$MESS["CRM_PAYMENT_HEADER_DATE_PAY_BEFORE"] = "Pagar antes";
$MESS["CRM_PAYMENT_HEADER_ID"] = "ID de pago";
$MESS["CRM_PAYMENT_HEADER_ORDER_ID"] = "ID del pedido";
$MESS["CRM_PAYMENT_HEADER_PAID"] = "Estado";
$MESS["CRM_PAYMENT_HEADER_PAYMENT_SUMMARY"] = "Pago";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_FULL"] = "Método de pago";
$MESS["CRM_PAYMENT_HEADER_PAY_SYSTEM_NAME"] = "Sistema de pago";
$MESS["CRM_PAYMENT_HEADER_PAY_VOUCHER_NUM"] = "Número de documento de pago";
$MESS["CRM_PAYMENT_HEADER_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_PAYMENT_HEADER_SUM"] = "Importe";
$MESS["CRM_PAYMENT_HEADER_USER_ID"] = "Comprador";
$MESS["CRM_PAYMENT_NAME"] = "Envío ##ACCOUNT_NUMBER# de #DATE_BILL#";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
?>