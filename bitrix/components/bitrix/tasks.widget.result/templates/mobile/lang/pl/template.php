<?php
$MESS["TASKS_RESULT_TUTORIAL_MESSAGE"] = "Możesz użyć dowolnego ze swoich komentarzy jako raportu, końcowego lub pośredniego, z zadania. Kliknij komentarz i wybierz \"Oznacz jako podsumowanie statusu zadania\".";
$MESS["TASKS_RESULT_TUTORIAL_TITLE"] = "Kreator zadania wymaga dostarczenia raportu z zadania.";
