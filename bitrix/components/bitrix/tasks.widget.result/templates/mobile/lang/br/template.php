<?php
$MESS["TASKS_RESULT_TUTORIAL_MESSAGE"] = "Você pode usar qualquer um dos seus comentários como um relatório da tarefa, final ou intermediário. Clique no comentário e selecione \"Marcar como resumo do status da tarefa\".";
$MESS["TASKS_RESULT_TUTORIAL_TITLE"] = "O criador da tarefa exige que você forneça um relatório da tarefa.";
