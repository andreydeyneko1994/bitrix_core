<?php
$MESS["TASKS_RESULT_TUTORIAL_MESSAGE"] = "Vous pouvez utiliser n'importe lequel de vos commentaires comme un rapport de tâche, final ou intermédiaire. Cliquez sur le commentaire et sélectionnez \"Marquer comme résumé de statut de tâche\".";
$MESS["TASKS_RESULT_TUTORIAL_TITLE"] = "Le créateur de la tâche exige que vous fournissiez un rapport de tâche.";
