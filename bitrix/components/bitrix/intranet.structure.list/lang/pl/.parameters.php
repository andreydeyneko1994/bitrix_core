<?
$MESS["INTR_ISL_GROUP_FILTER"] = "Filter parametrów";
$MESS["INTR_ISL_PARAM_DETAIL_URL"] = "Strona widoku szczegółów";
$MESS["INTR_ISL_PARAM_DISPLAY_USER_PHOTO"] = "Pokaż zdjęcie użytkownika";
$MESS["INTR_ISL_PARAM_FILTER_1C_USERS"] = "Pokaż tylko użytkowników zsynchronizowanych z 1C";
$MESS["INTR_ISL_PARAM_FILTER_NAME"] = "Nazwa filtru";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY"] = "Filtr według działów";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALUE_Y"] = "bezpośrednie";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALYE_N"] = "Powtarzane";
$MESS["INTR_ISL_PARAM_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["INTR_ISL_PARAM_NAV_TITLE"] = "Tytuł ścieżki";
$MESS["INTR_ISL_PARAM_NAV_TITLE_DEFAULT"] = "Pracownicy";
$MESS["INTR_ISL_PARAM_SHOW_DEP_HEAD_ADDITIONAL"] = "Pokaż przełożonemu, kiedy filtr działu jest aktywny";
$MESS["INTR_ISL_PARAM_SHOW_ERROR_ON_NULL"] = "Wyświetl ostrzeżenie o braku wyniku";
$MESS["INTR_ISL_PARAM_SHOW_NAV_BOTTOM"] = "Pokaż ścieżki poniżej wyników";
$MESS["INTR_ISL_PARAM_SHOW_NAV_TOP"] = "Pokaż ścieżkę powyżej wyników";
$MESS["INTR_ISL_PARAM_SHOW_UNFILTERED_LIST"] = "Pokaż listę plików niefiltrowanych";
$MESS["INTR_ISL_PARAM_USERS_PER_PAGE"] = "Użytkownicy na stronie";
?>