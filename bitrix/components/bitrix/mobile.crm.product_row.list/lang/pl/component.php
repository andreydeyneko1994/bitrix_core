<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PRODUCT_LIST_OWNER_ID_NOT_DEFINED"] = "ID właściciela nie jest określone.";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_DEFINED"] = "Czcionka właściciela jest nieokreślona..";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_SUPPORTED"] = "Określona czcionka właściciela nie jest obsługiwana.";
?>