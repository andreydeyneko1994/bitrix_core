<?
$MESS["CRM_PERMS_AUTOMATION_DISABLED_HELP"] = "Cette option n'a aucun effet si l'option \"L'utilisateur peut éditer les paramètres\" est activée";
$MESS["CRM_PERMS_BUTTONS_APPLY"] = "Appliquer";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Enregistrer";
$MESS["CRM_PERMS_DLG_BTN"] = "Supprimer";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "Etes-vous sûr de vouloir supprimer ?";
$MESS["CRM_PERMS_DLG_TITLE"] = "Suppression du rôle";
$MESS["CRM_PERMS_FILED_NAME"] = "Rôle";
$MESS["CRM_PERMS_HEAD_ADD"] = "Ajouter";
$MESS["CRM_PERMS_HEAD_AUTOMATION"] = "Automatisation";
$MESS["CRM_PERMS_HEAD_DELETE"] = "Supprimer";
$MESS["CRM_PERMS_HEAD_ENTITY"] = "Entité";
$MESS["CRM_PERMS_HEAD_EXPORT"] = "Exporter";
$MESS["CRM_PERMS_HEAD_IMPORT"] = "Importer";
$MESS["CRM_PERMS_HEAD_READ"] = "Lire";
$MESS["CRM_PERMS_HEAD_WRITE"] = "Mettre à jour";
$MESS["CRM_PERMS_PERM_ADD"] = "Laisser modifier les paramètres";
$MESS["CRM_PERMS_PERM_INHERIT"] = "Hériter";
$MESS["CRM_PERMS_RESTRICTION"] = "Pour attribuer à vos employés les différents droits d'accès CRM, passez <a target=\"_blank\" href=\"https://www.bitrix24.fr/prices/\">à l'une des offres commerciales</a>.";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Suppression du rôle";
?>