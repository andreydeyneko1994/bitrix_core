<?
$MESS["CRM_PERMS_AUTOMATION_DISABLED_HELP"] = "Ta opcja nie działa, jeśli włączona jest opcja \"Użytkownik może edytować ustawienia\"";
$MESS["CRM_PERMS_BUTTONS_APPLY"] = "Zastosuj";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Zapisz";
$MESS["CRM_PERMS_DLG_BTN"] = "Usuń";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "Na pewno chcesz to usunąć?";
$MESS["CRM_PERMS_DLG_TITLE"] = "Usuń rolę";
$MESS["CRM_PERMS_FILED_NAME"] = "funkcja";
$MESS["CRM_PERMS_HEAD_ADD"] = "Dodawanie";
$MESS["CRM_PERMS_HEAD_AUTOMATION"] = "Automatyzacja";
$MESS["CRM_PERMS_HEAD_DELETE"] = "Usuń";
$MESS["CRM_PERMS_HEAD_ENTITY"] = "Jednostka";
$MESS["CRM_PERMS_HEAD_EXPORT"] = "Eksportuj";
$MESS["CRM_PERMS_HEAD_IMPORT"] = "Importuj";
$MESS["CRM_PERMS_HEAD_READ"] = "Odczyt";
$MESS["CRM_PERMS_HEAD_WRITE"] = "Aktualizacja";
$MESS["CRM_PERMS_PERM_ADD"] = "Użytkownik może edytować ustawienia";
$MESS["CRM_PERMS_PERM_INHERIT"] = "Dziedziczone";
$MESS["CRM_PERMS_RESTRICTION"] = "Aby przypisać pracownikom różne uprawnienia dostępu do CRM, przejdź na <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/\"> jeden z planów komercyjnych </a>.";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Usuń rolę";
?>