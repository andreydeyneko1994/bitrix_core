<?
$MESS["M_CRM_INVOICE_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_INVOICE_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_INVOICE_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_INVOICE_EDIT_CONVERT_TITLE"] = "Fatura";
$MESS["M_CRM_INVOICE_EDIT_CREATE_TITLE"] = "Criar fatura";
$MESS["M_CRM_INVOICE_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_INVOICE_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_INVOICE_EDIT_VIEW_TITLE"] = "Ver fatura";
$MESS["M_CRM_INVOICE_MENU_CREATE_ON_BASE"] = "Criar usando fonte";
$MESS["M_CRM_INVOICE_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_INVOICE_MENU_EDIT"] = "Editar";
$MESS["M_CRM_INVOICE_MENU_HISTORY"] = "Histórico";
$MESS["M_CRM_INVOICE_MENU_SEND_EMAIL"] = "Enviar e-mail";
$MESS["M_CRM_INVOICE_SELECT"] = "Selecionar";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
?>