<?
$MESS["M_CRM_INVOICE_CONVERSION_NOTIFY"] = "Champs requis";
$MESS["M_CRM_INVOICE_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_INVOICE_EDIT_CONTINUE_BTN"] = "Continuer";
$MESS["M_CRM_INVOICE_EDIT_CONVERT_TITLE"] = "Facture";
$MESS["M_CRM_INVOICE_EDIT_CREATE_TITLE"] = "Création du facture";
$MESS["M_CRM_INVOICE_EDIT_EDIT_TITLE"] = "Modifier";
$MESS["M_CRM_INVOICE_EDIT_SAVE_BTN"] = "Enregistrer";
$MESS["M_CRM_INVOICE_EDIT_VIEW_TITLE"] = "Voir la facture";
$MESS["M_CRM_INVOICE_MENU_CREATE_ON_BASE"] = "Créer en utilisant la source";
$MESS["M_CRM_INVOICE_MENU_DELETE"] = "Supprimer";
$MESS["M_CRM_INVOICE_MENU_EDIT"] = "Modifier";
$MESS["M_CRM_INVOICE_MENU_HISTORY"] = "Historique";
$MESS["M_CRM_INVOICE_MENU_SEND_EMAIL"] = "Envoyer un e-mail";
$MESS["M_CRM_INVOICE_SELECT"] = "Sélectionner";
$MESS["M_DETAIL_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_DETAIL_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
?>