<?
$MESS["TASKS_TUFP_FIELD_ADD"] = "Dodaj pole";
$MESS["TASKS_TUFP_FIELD_EDIT"] = "Edycja pola";
$MESS["TASKS_TUFP_FIELD_HIDE"] = "Ukryj pole";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "Wymagane";
$MESS["TASKS_TUFP_FIELD_MULTIPLE"] = "Wielokrotne";
$MESS["TASKS_TUFP_FIELD_MULTIPLE_HINT"] = "Ta właściwość może zostać wybrana jedynie podczas tworzenia nowego pola";
$MESS["TASKS_TUFP_FIELD_UN_HIDE"] = "Pokaż pole";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED"] = "Własne pola są dostępne jedynie w wybranych planach płatnych.";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED_MANDATORY"] = "Wymagane pola są dostępne w wybranych planach płatnych.";
$MESS["TASKS_TUFP_NO_FIELDS_TO_SHOW"] = "Brak niestandardowych pól do wyświetlenia";
$MESS["TASKS_TUFP_SHOW_DETAILS"] = "Szczegóły";
?>