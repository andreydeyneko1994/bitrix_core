<?
$MESS["TASKS_TUFP_FIELD_ADD"] = "Ajouter un champ";
$MESS["TASKS_TUFP_FIELD_EDIT"] = "Modifier le champ";
$MESS["TASKS_TUFP_FIELD_HIDE"] = "Masquer le champ";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "Requis";
$MESS["TASKS_TUFP_FIELD_MULTIPLE"] = "Multiple";
$MESS["TASKS_TUFP_FIELD_MULTIPLE_HINT"] = "Cette propriété peut être sélectionnée uniquement en créant un nouveau champ.";
$MESS["TASKS_TUFP_FIELD_UN_HIDE"] = "Afficher le champ";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED"] = "Les champs personnalisés sont disponibles avec les offres commerciales sélectionnés.";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED_MANDATORY"] = "Les champs requis sont disponibles avec les offres commerciales sélectionnées.";
$MESS["TASKS_TUFP_NO_FIELDS_TO_SHOW"] = "Il n'y a aucun champ personnalisé à afficher";
$MESS["TASKS_TUFP_SHOW_DETAILS"] = "Informations";
?>