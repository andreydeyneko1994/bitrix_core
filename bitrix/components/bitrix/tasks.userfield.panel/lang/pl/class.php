<?php
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "Nazwa pola nie jest określona";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "Nie możesz zarządzać własnymi polami w aktualnej taryfie.";
$MESS["TASKS_TUFE_UF_UNEXPECTED_ERROR"] = "Nie można zapisać pola z powodu nieznanego błędu.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "Nie możesz używać pól własnych w aktualnej taryfie.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "Nie można stworzyć wymaganego pola.";
