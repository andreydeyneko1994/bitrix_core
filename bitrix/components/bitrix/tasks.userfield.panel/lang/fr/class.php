<?php
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "Le nom du champ n'est pas spécifié";
$MESS["TASKS_TUFE_UF_ADMIN_RESTRICTED"] = "Les champs personnalisés ne sont accessibles qu'aux administrateurs";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "Vous ne pouvez pas gérer les champs personnalisés avec votre abonnement.";
$MESS["TASKS_TUFE_UF_NAME_GENERATION_FAILED"] = "Impossible de créer un nom de champ personnalisé";
$MESS["TASKS_TUFE_UF_NOT_FOUND"] = "Le champ personnalisé est introuvable";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_CREATING_ERROR"] = "Impossible de créer certains champs associés";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_UPDATING_ERROR"] = "Impossible de mettre à jour certains champs associés";
$MESS["TASKS_TUFE_UF_UNEXPECTED_ERROR"] = "Impossible d'enregistrer le champ en raison d'une erreur inconnue.";
$MESS["TASKS_TUFE_UF_UNKNOWN_ENTITY_CODE"] = "Code d'entité inconnu";
$MESS["TASKS_TUFE_UF_UNKNOWN_ID"] = "ID du champ personnalisé inconnu";
$MESS["TASKS_TUFE_UF_UNKNOWN_TYPE"] = "Type de champ personnalisé inconnu";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "Vous ne pouvez pas utiliser les champs personnalisés avec votre abonnement.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "Impossible de créer un champ requis.";
