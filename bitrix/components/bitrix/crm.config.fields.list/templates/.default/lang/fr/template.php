<?
$MESS["CRM_FIELDS_LIST_IS_REQUIRED"] = "Oblig.";
$MESS["CRM_FIELDS_LIST_MULTIPLE"] = "Multiple";
$MESS["CRM_FIELDS_LIST_NAME"] = "Dénomination";
$MESS["CRM_FIELDS_LIST_SHOW_IN_LIST"] = "Afficher en liste";
$MESS["CRM_FIELDS_LIST_SORT"] = "Classification";
$MESS["CRM_FIELDS_LIST_TYPE"] = "Type";
$MESS["CRM_FIELDS_TOOLBAR_ADD"] = "Ajouter un champ";
$MESS["CRM_FIELDS_TOOLBAR_ADD_TITLE"] = "Ajouter un nouveau champ";
$MESS["CRM_FIELDS_TOOLBAR_TYPES"] = "Liste de modèles";
$MESS["CRM_FIELDS_TOOLBAR_TYPES_TITLE"] = "Liste des types disponibles";
?>