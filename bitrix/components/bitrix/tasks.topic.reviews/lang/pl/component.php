<?
$MESS["COMM_COMMENT_OK"] = "Komentarz został zapisany pomyślnie";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "Komentarz został dodany pomyślnie. Zostanie on pokazany po zatwierdzeniu go przez moderatora.";
$MESS["F_ERR_FID_EMPTY"] = "Forum opinii nie jest ustawione";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "Forum opinii #FORUM# nie istnieje";
$MESS["F_ERR_FORUM_NO_ACCESS"] = "Nie masz pozwolenia do przeglądania komentarzy.";
$MESS["F_ERR_TID_EMPTY"] = "Zadanie nie określone.";
$MESS["F_ERR_TID_IS_NOT_EXIST"] = "Nie znaleziono zadania #TASK_ID#.";
$MESS["F_NO_MODULE"] = "Moduł Forum nie jest zainstalowany";
$MESS["F_NO_MODULE_TASKS"] = "Moduł Zadania nie jest zainstalowany.";
$MESS["NAV_OPINIONS"] = "Opinie";
?>