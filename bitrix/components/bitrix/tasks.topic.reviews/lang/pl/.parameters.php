<?
$MESS["F_DATE_TIME_FORMAT"] = "Format daty i godziny";
$MESS["F_DETAIL_TEMPLATE"] = "Strona elementu bloku informacji";
$MESS["F_DISPLAY_PANEL"] = "Wyświetl przyciski panelu dla tego komponentu";
$MESS["F_FORUM_ID"] = "ID Forum";
$MESS["F_MESSAGES_PER_PAGE"] = "Liczba Wiadomości na stronę";
$MESS["F_NAME_TEMPLATE"] = "Nazwa Formatu";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Nazwa szablonu pager`a";
$MESS["F_PATH_TO_SMILE"] = "Ścieżka do folderu z Emotikonami (względem folderu głównego)";
$MESS["F_POST_FIRST_MESSAGE"] = "Użyj tekstu tego elementu do rozpoczęcia tematu";
$MESS["F_POST_FIRST_MESSAGE_TEMPLATE"] = "Szablon pierwszego postu w temacie";
$MESS["F_PREORDER"] = "Wyświetlanie wiadomości w porządku do przodu";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Strona profilu";
$MESS["F_READ_TEMPLATE"] = "Strona Czytania Tematu";
$MESS["F_TASK_ID"] = "ID Zadania";
$MESS["F_USE_CAPTCHA"] = "Użyj CAPTCHA";
$MESS["RATING_TYPE"] = "Projekt przycisku oceniania";
$MESS["RATING_TYPE_CONFIG"] = "domyślne";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Lubię (obraz)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Lubię (tekst)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Lubię/Nie lubię (obraz)";
$MESS["RATING_TYPE_STANDART_TEXT"] = "Lubię/Nie lubię (tekst)";
$MESS["SHOW_RATING"] = "Włącz ocenianie";
$MESS["SHOW_RATING_CONFIG"] = "domyślne";
?>