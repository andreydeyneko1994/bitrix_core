<?
$MESS["COMM_COMMENT_OK"] = "Komentarz został zapisany pomyślnie.";
$MESS["F_ERR_ADD_MESSAGE"] = "Błąd tworzenia postu.";
$MESS["F_ERR_ADD_TOPIC"] = "Błąd tworzenia tematu.";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "Brak wystarczających uprawnień do dodania komentarzy.";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "Proszę wprowadzić swój komentarz.";
$MESS["F_ERR_REMOVE_COMMENT"] = "Błąd przy próbie usuwania komentarza.";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "Twoja sesja wygasła. Proszę ponownie opublikować swoją wiadomość.";
$MESS["F_FORUM_MESSAGE_CNT"] = "Komentarze";
$MESS["F_FORUM_TOPIC_ID"] = "Temat forum";
$MESS["POSTM_CAPTCHA"] = "Kod CAPTCHA jest nieprawidłowy.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Dodała komentarz do zadania \"#TASK_TITLE#\", tekst komentarza: \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "Dodał komentarz do zadania \"#TASK_TITLE#\", tekst komentarza: \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Utworzone zadanie";
?>