<?php
$MESS["CRM_DEAL_CONVERSION_ID_NOT_DEFINED"] = "ID deala nie został odnaleziony.";
$MESS["CRM_DEAL_CONVERSION_NOT_FOUND"] = "Deal nie został odnaleziony.";
$MESS["CRM_DEAL_DELETION_ERROR"] = "Błąd usuwania deala.";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_ERROR"] = "Nie można przenieść umowy do nowego lejka. Transakcja może zostać przeniesiona tylko po zakończeniu wszystkich aktywnych workflowów. Możesz zatrzymać lub zakończyć przepływ pracy na karcie Procesy biznesowe. Upewnij się również, że transakcja, którą chcesz przenieść, ma przypisaną osobę odpowiedzialną i ustawioną fazę umowy.";
$MESS["CRM_DEAL_PRODUCT_ROWS_SAVING_ERROR"] = "Wystąpił błąd zapisywania produktów.";
$MESS["CRM_DEAL_RECURRING_DATE_START_ERROR"] = "Data następnego utworzenia jest wcześniejsza niż data bieżąca. Zmień parametry powracającego dealu.";
