<?php
$MESS["CRM_DEAL_COPY_PAGE_TITLE"] = "Kopiuj deal";
$MESS["CRM_DEAL_CREATION_PAGE_TITLE"] = "Nowy deal";
$MESS["CRM_DEAL_FIELD_ADD_OBSERVER"] = "Dodaj obserwatora";
$MESS["CRM_DEAL_FIELD_ASSIGNED_BY_ID"] = "Odpowiedzialny";
$MESS["CRM_DEAL_FIELD_BEGINDATE"] = "Data początkowa";
$MESS["CRM_DEAL_FIELD_CATEGORY_RECURRING"] = "Lejek";
$MESS["CRM_DEAL_FIELD_CLIENT"] = "Klient";
$MESS["CRM_DEAL_FIELD_CLOSEDATE"] = "Data końcowa";
$MESS["CRM_DEAL_FIELD_COMMENTS"] = "Komentarz";
$MESS["CRM_DEAL_FIELD_CONTACT_LEGEND"] = "Kontakty powiązane z dealem";
$MESS["CRM_DEAL_FIELD_CURRENCY_ID"] = "Waluta";
$MESS["CRM_DEAL_FIELD_DATE_CREATE"] = "Utworzono";
$MESS["CRM_DEAL_FIELD_DATE_MODIFY"] = "Zmodyfikowano";
$MESS["CRM_DEAL_FIELD_ID"] = "ID";
$MESS["CRM_DEAL_FIELD_LOCATION_ID"] = "Lokalizacja";
$MESS["CRM_DEAL_FIELD_NUM_SIGN"] = "##DEAL_ID#";
$MESS["CRM_DEAL_FIELD_OBSERVERS"] = "Obserwatorzy";
$MESS["CRM_DEAL_FIELD_OPENED"] = "Dostępne dla wszystkich";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY"] = "Wartość";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY_WITH_CURRENCY"] = "Kwota i waluta";
$MESS["CRM_DEAL_FIELD_PAYMENT_DOCUMENTS"] = "Dokumentacja płatnicza";
$MESS["CRM_DEAL_FIELD_PROBABILITY"] = "Prawdopodobieństwo";
$MESS["CRM_DEAL_FIELD_PRODUCTS"] = "Produkty";
$MESS["CRM_DEAL_FIELD_RECURRING"] = "Powtórz";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_FROM_CURRENT"] = "Szablon został stworzony dla cyklicznego dealu ##RECURRING_ID#";
$MESS["CRM_DEAL_FIELD_RECURRING_CREATED_MANY_FROM_CURRENT"] = "Szablon został stworzony dla cyklicznych deali: #RECURRING_LIST#";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_DAY"] = "dzień (dni)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL"] = "Okres niestandardowy";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_INTERVAL_TITLE"] = "Powtarzaj co";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_MONTH"] = "mies.";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_WEEK"] = "tydzień (tygodni/e)";
$MESS["CRM_DEAL_FIELD_RECURRING_CUSTOM_YEAR"] = "rok";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_BEGINDATE_OFFSET_TITLE"] = "Przesunięcie daty rozpoczęcia";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_CREATION_CLOSEDATE_OFFSET_TITLE"] = "Przesunięcie daty zamknięcia";
$MESS["CRM_DEAL_FIELD_RECURRING_DATE_NEXT_EXECUTION"] = "Następna próba nastąpi #NEXT_DATE#";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERYDAY"] = "Codziennie";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_MONTH"] = "Co miesiąc";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_WEEK"] = "Co tydzień";
$MESS["CRM_DEAL_FIELD_RECURRING_EVERY_YEAR"] = "Co rok";
$MESS["CRM_DEAL_FIELD_RECURRING_MANY_TIMES"] = "Regularnie";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_DATE"] = "Po osiągnięciu daty zakończenia";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TIMES"] = "Po osiągnięciu maks. liczby powtórzeń";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_LIMIT_TITLE"] = "Przestań powtarzać";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_FINAL_NO_LIMIT"] = "Nigdy";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_DATE_TITLE"] = "Zakończ w dniu";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_LIMIT_TIMES_TITLE"] = "Maks. liczba powtórzeń";
$MESS["CRM_DEAL_FIELD_RECURRING_MULTIPLE_START_DATE_TITLE"] = "Zacznij w dniu";
$MESS["CRM_DEAL_FIELD_RECURRING_MUTLTIPLE_PERIOD_TITLE"] = "Interwał powtarzania";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_BEGINDATE_VALUE_TITLE"] = "Wartość daty rozpoczęcia nowego dealu";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_CLOSEDATE_VALUE_TITLE"] = "Wartość daty zamknięcia nowego dealu";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_CURRENT_FIELD"] = "bieżąca wartość pola";
$MESS["CRM_DEAL_FIELD_RECURRING_NEW_VALUE_DATE_CREATION_OFFSET"] = "jako przesunięcie od daty utworzenia";
$MESS["CRM_DEAL_FIELD_RECURRING_NOTHING_SELECTED"] = "Brak wyboru";
$MESS["CRM_DEAL_FIELD_RECURRING_NOT_REPEAT"] = "Nie powtarzaj";
$MESS["CRM_DEAL_FIELD_RECURRING_ONCE_TIME"] = "Tylko raz";
$MESS["CRM_DEAL_FIELD_RECURRING_RESTRICTED"] = "Niewystarczające uprawnienia";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TITLE"] = "Utwórz deal w";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_DAY"] = "dzień (dni)";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_MONTH"] = "mies.";
$MESS["CRM_DEAL_FIELD_RECURRING_SINGLE_TYPE_WEEK"] = "tydzień (tygodni/e)";
$MESS["CRM_DEAL_FIELD_RECURRING_TITLE"] = "Szablon dealu: #TITLE#";
$MESS["CRM_DEAL_FIELD_SOURCE_DESCRIPTION"] = "Informacja o źródle";
$MESS["CRM_DEAL_FIELD_SOURCE_ID"] = "Źródło";
$MESS["CRM_DEAL_FIELD_STAGE_ID"] = "Etap";
$MESS["CRM_DEAL_FIELD_TITLE"] = "Nazwa";
$MESS["CRM_DEAL_FIELD_TYPE_ID"] = "Typ dealu";
$MESS["CRM_DEAL_FIELD_UTM"] = "parametry UTM";
$MESS["CRM_DEAL_NOT_FOUND"] = "Nie znaleziono deala.";
$MESS["CRM_DEAL_QUOTE_LINK"] = "Utworzono deal na bazie oferty <a target='_blank' href='#URL#'>#TITLE#</a>";
$MESS["CRM_DEAL_REPEATED_APPROACH"] = "Ponowne zapytanie";
$MESS["CRM_DEAL_RETURNING"] = "Powtarzalny deal";
$MESS["CRM_DEAL_SECTION_ADDITIONAL"] = "Więcej";
$MESS["CRM_DEAL_SECTION_MAIN"] = "O dealu";
$MESS["CRM_DEAL_SECTION_PRODUCTS"] = "Produkty";
$MESS["CRM_DEAL_SECTION_RECURRING"] = "Cykliczny deal";
$MESS["CRM_DEAL_SOURCE_NOT_SELECTED"] = "Nie wybrano";
$MESS["CRM_DEAL_TAB_AUTOMATION"] = "Automatyzacja";
$MESS["CRM_DEAL_TAB_BIZPROC"] = "Workflowy";
$MESS["CRM_DEAL_TAB_EVENT"] = "Historia";
$MESS["CRM_DEAL_TAB_INVOICES"] = "Faktury";
$MESS["CRM_DEAL_TAB_ORDERS"] = "Zamówienia";
$MESS["CRM_DEAL_TAB_PRODUCTS"] = "Produkty";
$MESS["CRM_DEAL_TAB_QUOTE"] = "Oferty";
$MESS["CRM_DEAL_TAB_TREE"] = "Zależności";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
