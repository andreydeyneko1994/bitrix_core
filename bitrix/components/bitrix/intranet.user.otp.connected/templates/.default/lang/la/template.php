<?
$MESS["INTRANET_USER_OTP_ACTIVATE"] = "Habilitar";
$MESS["INTRANET_USER_OTP_ACTIVE"] = "Habilitado";
$MESS["INTRANET_USER_OTP_AUTH"] = "Autenticación de dos pasos";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE"] = "Mi numero de teléfono ha cambiado";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE_1"] = "Mi teléfono móvil cambió";
$MESS["INTRANET_USER_OTP_CODES"] = "Códigos de recuperación";
$MESS["INTRANET_USER_OTP_CODES_SHOW"] = "Ver";
$MESS["INTRANET_USER_OTP_CONNECT"] = "Conectar";
$MESS["INTRANET_USER_OTP_DEACTIVATE"] = "Deshabilitar";
$MESS["INTRANET_USER_OTP_LEFT_DAYS"] = "(se habilitará en #NUM#)";
$MESS["INTRANET_USER_OTP_NOT_ACTIVE"] = "Deshabilitado";
$MESS["INTRANET_USER_OTP_NOT_EXIST"] = "No configurado";
$MESS["INTRANET_USER_OTP_NO_DAYS"] = "para siempre";
$MESS["INTRANET_USER_OTP_PROROGUE"] = "Posponer";
$MESS["INTRANET_USER_OTP_SETUP"] = "Configurar";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_CLOSE"] = "Cerrar";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_PASSWORDS"] = "Configurar las contraseñas de las aplicaciones";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT"] = "<b>¡Felicidades!</b>
<br/><br/>
Ya configuró con éxito la autenticación de dos pasos para su cuenta.
<br/><br/>
La autenticación de dos pasos implica dos etapas de verificación posteriores. La primera etapa requiere su contraseña principal. La segunda etapa incluye un código de un solo uso que deberá introducir en la segunda pantalla de autenticación.
<br/><br/>
Tenga en cuenta que tendrá que usar contraseñas de aplicaciones para el intercambio seguro de datos (por ejemplo, con calendarios externos). Encontrará las herramientas para administrar las contraseñas de las aplicaciones en su perfil de usuario.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT2"] = "Recuerde que debe guardar sus códigos de recuperación. Es posible que los necesite si pierde su dispositivo móvil o si no puede obtener un código por cualquier otro motivo.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>¡Felicidades!</b>
<br/><br/>
Ya configuró con éxito la autenticación de dos pasos para su cuenta.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "Tenga en cuenta que el intercambio de datos con servicios externos (calendarios, etc.) requerirá el uso de las contraseñas específicas de la aplicación. Puede administrarlas en su perfil.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "<b>¡Felicidades!</b>
<br/><br/>
Ya configuró la autenticación de dos pasos para su cuenta.
<br/><br/>
Recuerde guardar los <b>códigos de recuperación</b>. Es posible que deba iniciar sesión si pierde su dispositivo móvil o si no puede obtener el código de un solo uso.<br/><br/>";
?>