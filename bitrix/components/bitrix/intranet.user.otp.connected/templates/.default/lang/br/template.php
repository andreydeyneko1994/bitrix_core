<?
$MESS["INTRANET_USER_OTP_ACTIVATE"] = "Ativar";
$MESS["INTRANET_USER_OTP_ACTIVE"] = "Ativado";
$MESS["INTRANET_USER_OTP_AUTH"] = "Autenticação de dois passos";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE"] = "Meu número de telefone mudou";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE_1"] = "Meu dispositivo móvel mudou";
$MESS["INTRANET_USER_OTP_CODES"] = "Códigos de recuperação";
$MESS["INTRANET_USER_OTP_CODES_SHOW"] = "Visualizar";
$MESS["INTRANET_USER_OTP_CONNECT"] = "Conectar";
$MESS["INTRANET_USER_OTP_DEACTIVATE"] = "Desativar";
$MESS["INTRANET_USER_OTP_LEFT_DAYS"] = "(será ativado em #NUM#)";
$MESS["INTRANET_USER_OTP_NOT_ACTIVE"] = "Desativado";
$MESS["INTRANET_USER_OTP_NOT_EXIST"] = "Não configurado";
$MESS["INTRANET_USER_OTP_NO_DAYS"] = "para sempre";
$MESS["INTRANET_USER_OTP_PROROGUE"] = "Adiar";
$MESS["INTRANET_USER_OTP_SETUP"] = "Configurar";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_CLOSE"] = "Fechar";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_PASSWORDS"] = "Configurar senhas do aplicativo";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT"] = "<b>Parabéns!</b>
<br/><br/>
Você configurou com sucesso a autenticação de dois passos para sua conta.
<br/><br/>
A autenticação de dois passos envolve dois estágios subsequentes de verificação. O primeiro requer sua senha principal. O segundo estágio inclui um código avulso que você terá que digitar na segunda tela de autenticação.
<br/><br/>
Observe que você terá que usar senhas do aplicativo para alterar os dados de segurança (por exemplo, com calendários externos). Você encontrará as ferramentas para gerenciar as senhas do aplicativo no seu perfil de usuário.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT2"] = "Lembre-se de proteger seus códigos de recuperação. Você pode precisar deles se perder seu dispositivo móvel ou não puder obter um código por qualquer outra razão.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>Parabéns!</b> 
<br/><br/> 
Agora, você configurou com sucesso a autenticação em duas etapas para sua conta.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "Observe que o intercâmbio de dados com serviços externos (calendários etc.) vai exigir o uso de senhas específicas do aplicativo. Você pode administrá-los no seu perfil.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "<b>Parabéns!</b>
<br/><br/>
Agora, você configurou a autenticação em duas etapas para sua conta.
<br/><br/>
Lembre-se de manter os <b>códigos de recuperação</b> que você pode precisar para fazer login se perder seu dispositivo móvel ou se o código avulso não puder ser obtido de outra forma.<br/><br/>";
?>