<?
$MESS["INTRANET_USER_OTP_ACTIVATE"] = "Włącz";
$MESS["INTRANET_USER_OTP_ACTIVE"] = "Włączono";
$MESS["INTRANET_USER_OTP_AUTH"] = "Dwustopniowe uwierzytelnienie";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE"] = "Mój numer telefonu się zmienił";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE_1"] = "Zmieniłem urządzenie przenośne";
$MESS["INTRANET_USER_OTP_CODES"] = "Kody odzyskiwania";
$MESS["INTRANET_USER_OTP_CODES_SHOW"] = "Wyświetl";
$MESS["INTRANET_USER_OTP_CONNECT"] = "Połącz";
$MESS["INTRANET_USER_OTP_DEACTIVATE"] = "Wyłącz";
$MESS["INTRANET_USER_OTP_LEFT_DAYS"] = "(będzie włączone w ciągu #NUM#)";
$MESS["INTRANET_USER_OTP_NOT_ACTIVE"] = "Wyłączono";
$MESS["INTRANET_USER_OTP_NOT_EXIST"] = "Nie skonfigurowano";
$MESS["INTRANET_USER_OTP_NO_DAYS"] = "zawsze";
$MESS["INTRANET_USER_OTP_PROROGUE"] = "Odrocz";
$MESS["INTRANET_USER_OTP_SETUP"] = "Konfiguruj";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_CLOSE"] = "Zamknij";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_PASSWORDS"] = "Konfiguruj hasła aplikacji";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT"] = "<b>Gratulacje!</b>
<br/><br/>
Dwustopniowe uwierzytelnienie zostało skonfigurowane na tym koncie.
<br/><br/>
Dwustopniowe uwierzytelnienie obejmuje dwa kolejne etapy weryfikacji. Pierwszy wymaga podania głównego hasła. Na drugim konieczne jest podanie jednorazowego kodu.
<br/><br/>
Konieczne będzie stosowanie haseł aplikacji w celu zabezpieczenia wymiany danych (np. z kalendarzami zewnętrznymi). Narzędzia do zarządzania hasłami znajdziesz w swoim profilu użytkownika.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT2"] = "Pamiętaj, aby zabezpieczyć swoje kody odzyskiwania. Możesz ich potrzebować, gdy zgubisz urządzenie mobilne lub nie będziesz w stanie uzyskać kodu z innych powodów.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>Gratulacje!</b>
<br/><br/>
Dwustopniowe uwierzytelnienie zostało skonfigurowane na tym koncie.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "Należy pamiętać, że do wymiany danych z usługami zewnętrznymi (kalendarzami itd.) konieczne będzie użycie haseł aplikacji. Możesz zarządzać nimi w swoim profilu.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "<b>Gratulacje!</b>
<br/><br/>
Dwustopniowe uwierzytelnienie zostało skonfigurowane na tym koncie.
<br/><br/>
Pamiętaj, aby zachować <b>kody odzyskiwania</b>, których możesz potrzebować do zalogowania się, gdy zgubisz swoje urządzenie mobilne albo nie będziesz mógł użyć jednorazowego kodu z innego powodu.<br/><br/>";
?>