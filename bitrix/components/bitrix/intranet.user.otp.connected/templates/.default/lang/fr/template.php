<?
$MESS["INTRANET_USER_OTP_ACTIVATE"] = "Activer";
$MESS["INTRANET_USER_OTP_ACTIVE"] = "Désactivé";
$MESS["INTRANET_USER_OTP_AUTH"] = "Identification en deux étapes";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE"] = "Mon numéro de téléphone a changé";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE_1"] = "Mon appareil mobile a changé";
$MESS["INTRANET_USER_OTP_CODES"] = "Codes de récupération";
$MESS["INTRANET_USER_OTP_CODES_SHOW"] = "Afficher";
$MESS["INTRANET_USER_OTP_CONNECT"] = "Connexion";
$MESS["INTRANET_USER_OTP_DEACTIVATE"] = "Désactiver";
$MESS["INTRANET_USER_OTP_LEFT_DAYS"] = "(sera activé dans #NUM#)";
$MESS["INTRANET_USER_OTP_NOT_ACTIVE"] = "Désactivé";
$MESS["INTRANET_USER_OTP_NOT_EXIST"] = "Non configuré";
$MESS["INTRANET_USER_OTP_NO_DAYS"] = "toujours";
$MESS["INTRANET_USER_OTP_PROROGUE"] = "Reporter";
$MESS["INTRANET_USER_OTP_SETUP"] = "Configurer";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_CLOSE"] = "Fermer";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_PASSWORDS"] = "Configurer les mots de passe d'application";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT"] = "<b>Félicitations!</b>
<br/><br/>
Vous avez réussi à configurer une authentification à deux étapes pour votre compte.
<br/><br/>
L'authentification à deux-étape implique deux étapes ultérieures de vérification. Le premier nécessite votre mot de passe principal. La deuxième étape comprend un code à un moment où vous devrez entrer sur le deuxième écran d'authentification.
<br/><br/>
Notez que vous aurez à utiliser des mots de passe d'application pour l'échange sécurisé de données (par exemple, avec des calendriers externes). Vous trouverez les outils pour gérer les mots de passe d'application dans votre profil d'utilisateur .";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT2"] = "N'oubliez pas de sécuriser vos codes de récupération. Vous pourriez en avoir besoin si vous avez perdu votre appareil mobile ou ne pouvez pas obtenir un code pour toute autre raison.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>Félicitations !</b>
<br/><br/>
Vous avez réussi à configurer l'authentification en deux étapes pour votre compte.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "Notez que l'échange de données avec les services extérieurs (calendriers, etc.) nécessite l'utilisation d'une application spécifique les mots de passe. Vous pouvez gérer ces dans votre profil.";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "<b>Félicitations !</b>
<br/><br/>
Vous avez maintenant configuré l'authentification en deux étapes sur votre compte.
<br/><br/>
N'oubliez pas de garder les <b>codes de récupération</b>. Vous pourriez en avoir besoin pour vous connecter si votre appareil mobile était perdu ou qu'aucun code à utilisation unique ne pouvait être obtenu.<br/><br/>";
?>