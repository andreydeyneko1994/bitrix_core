<?php
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_CRITERION"] = "Wybierz pola do porównania";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_SCOPE"] = "Kraj";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_TITLE"] = "Zduplikowane ustawienia skanowania";
$MESS["CRM_DEDUPE_WIZARD_SELECT_ALL"] = "Wybierz wszystkie";
$MESS["CRM_DEDUPE_WIZARD_UNSELECT_ALL"] = "Odznacz wszystko";
