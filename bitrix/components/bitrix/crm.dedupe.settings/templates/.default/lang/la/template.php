<?php
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_CRITERION"] = "Seleccionar campos para comparar";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_SCOPE"] = "País";
$MESS["CRM_DEDUPE_WIZARD_SCANNING_CONFIG_TITLE"] = "Configuración de escaneo duplicado";
$MESS["CRM_DEDUPE_WIZARD_SELECT_ALL"] = "Seleccionar todo";
$MESS["CRM_DEDUPE_WIZARD_UNSELECT_ALL"] = "Deseleccionar todo";
