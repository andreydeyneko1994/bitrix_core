<?
$MESS["CRM_COLUMN_CITY_NAME"] = "Ville";
$MESS["CRM_COLUMN_COUNTRY_NAME"] = "Pays";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_REGION_NAME"] = "Arrondissement";
$MESS["CRM_COLUMN_SORT"] = "Trier";
$MESS["CRM_INTS_TASKS_NAV"] = "Inscriptions";
$MESS["CRM_LOC_DELETION_GENERAL_ERROR"] = "Erreur de suppression de l'ordre.";
$MESS["CRM_LOC_UPDATE_GENERAL_ERROR"] = "Erreur de la mise à jour de l'emplacement.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module Boutique en ligne n'est pas installé.";
?>