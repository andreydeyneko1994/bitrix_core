<?
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Moduł Sieci Społecznościowej nie jest zainstalowany.";
$MESS["INTL_APPLY_MESSAGE"] = "Zadanie \"#NAME#\" zostało zaakceptowane.

[url=#URL_VIEW#]View details[/url]";
$MESS["INTL_CAN_NOT_APPLY"] = "Nie możesz wykonać tego zadania.";
$MESS["INTL_CAN_NOT_FINISH"] = "Nie możesz wykonać zadania.";
$MESS["INTL_CAN_NOT_REJECT"] = "Nie możesz odrzucić tego zadania.";
$MESS["INTL_CAN_NOT_REJECT_OWN"] = "Nie możesz odrzucić własnego zadania.";
$MESS["INTL_EMPTY_FOLDER_NAME"] = "Nazwa folderu jest nieokreślona.";
$MESS["INTL_ERROR_DELETE_TASK"] = "Błąd podczas usuwania zadania.";
$MESS["INTL_FINISH_MESSAGE"] = "Zadanie \"#NAME#\" zostało zakończone.

[url=#URL_VIEW#]View details[/url]";
$MESS["INTL_FOLDER_DELETE_ERROR"] = "Błąd podczas usuwania folderu.";
$MESS["INTL_FOLDER_NOT_FOUND"] = "Nie znaleziono folderu.";
$MESS["INTL_NO_FOLDER_ID"] = "ID folderu jest nieokreślone.";
$MESS["INTL_NO_FOLDER_PERMS"] = "Nie masz uprawnień do edycji folderów.";
$MESS["INTL_NO_VIEW_PERMS"] = "Nie masz uprawnień do edytowania wspólnych widoków.";
$MESS["INTL_REJECT_MESSAGE"] = "Zadanie \"#NAME#\" nie zostało zaakceptowane.

[url=#URL_VIEW#]View details[/url]";
$MESS["INTL_SECURITY_ERROR"] = "Wystąpił błą zabezpieczeń.";
$MESS["INTL_TASK_INTERNAL_ERROR"] = "Wewnętrzny błąd.";
$MESS["INTL_TASK_NOT_FOUND"] = "Nie znaleziono zadania.";
$MESS["INTL_VIEW_NOT_FOUND"] = "Nie znaleziono widoku.";
$MESS["INTL_WRONG_VIEW"] = "Widok jest nieprawidłowy.";
?>