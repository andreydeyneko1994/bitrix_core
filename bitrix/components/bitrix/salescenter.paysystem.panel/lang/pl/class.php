<?
$MESS["SPP_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["SPP_ACTIONBOX_APPS"] = "Marketplace";
$MESS["SPP_PAYSYSTEM_ADD"] = "Dodaj system płatności";
$MESS["SPP_PAYSYSTEM_APP_INTEGRATION_REQUIRED"] = "Potrzebna jest integracja?";
$MESS["SPP_PAYSYSTEM_APP_RECOMMEND"] = "Poleć";
$MESS["SPP_PAYSYSTEM_APP_SEE_ALL"] = "Wyświetl wszystkie";
$MESS["SPP_PAYSYSTEM_APP_TOTAL_APPLICATIONS"] = "Łącznie aplikacji";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Płatność gotówką";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Inne systemy płatności";
$MESS["SPP_PAYSYSTEM_PAYPAL_TITLE"] = "Płatność przez PayPal";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Konfiguruj #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_SKB_SKB_TITLE"] = "System szybkich płatności";
$MESS["SPP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_TITLE"] = "Karta kredytowa/debetowa";
?>