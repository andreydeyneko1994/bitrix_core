<?
$MESS["SPP_ACCESS_DENIED"] = "Accès refusé.";
$MESS["SPP_ACTIONBOX_APPS"] = "Marketplace";
$MESS["SPP_PAYSYSTEM_ADD"] = "Ajouter un système de paiement";
$MESS["SPP_PAYSYSTEM_APP_INTEGRATION_REQUIRED"] = "Besoin d'une intégration ?";
$MESS["SPP_PAYSYSTEM_APP_RECOMMEND"] = "Recommandation";
$MESS["SPP_PAYSYSTEM_APP_SEE_ALL"] = "Tout afficher";
$MESS["SPP_PAYSYSTEM_APP_TOTAL_APPLICATIONS"] = "Total des applications";
$MESS["SPP_PAYSYSTEM_CASH_TITLE"] = "Paiement en espèces";
$MESS["SPP_PAYSYSTEM_ITEM_EXTRA"] = "Autres systèmes de paiement";
$MESS["SPP_PAYSYSTEM_PAYPAL_TITLE"] = "Paiement PayPal";
$MESS["SPP_PAYSYSTEM_SETTINGS"] = "Configurer #PAYSYSTEM_NAME#";
$MESS["SPP_PAYSYSTEM_SKB_SKB_TITLE"] = "Système de paiement rapide";
$MESS["SPP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_TITLE"] = "Carte de crédit/débit";
?>