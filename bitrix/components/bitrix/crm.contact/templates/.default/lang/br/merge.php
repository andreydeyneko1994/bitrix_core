<?
$MESS["CRM_CONTACT_MERGE_HEADER_TEMPLATE"] = "Criado em #DATE_CREATE#";
$MESS["CRM_CONTACT_MERGE_PAGE_TITLE"] = "Mesclar contatos";
$MESS["CRM_CONTACT_MERGE_RESULT_LEGEND"] = "Selecione o contato prioritário na lista. Ele será usado como base para o perfil de contato. Você pode adicionar mais dados de outros contatos.";
?>