<?
$MESS["CRM_CONTACT_MERGE_HEADER_TEMPLATE"] = "Creado el #DATE_CREATE#";
$MESS["CRM_CONTACT_MERGE_PAGE_TITLE"] = "Fusionar contactos";
$MESS["CRM_CONTACT_MERGE_RESULT_LEGEND"] = "Seleccione la prioridad de los contactos de la lista. Se utilizará como base para el perfil de los contactos. Puede agregar más datos de los otros contactos.";
?>