<?php
$MESS["CRM_EVENT_ADD_BUTTON"] = "Dodaj";
$MESS["CRM_EVENT_ADD_FILE"] = "Dołącz plik";
$MESS["CRM_EVENT_ADD_ID"] = "Typ wydarzenia";
$MESS["CRM_EVENT_ADD_TITLE"] = "Dodaj nowe wydarzenie";
$MESS["CRM_EVENT_DATE"] = "Data wydarzenia";
$MESS["CRM_EVENT_DESC_TITLE"] = "Wpisz do opisu wydarzenia";
$MESS["CRM_EVENT_MESSAGE_OBSOLETE"] = "Typ \"E-mail został wysłany\" jest przestarzały; istnieje tylko dla zgodności wstecznej. Użyj polecenia \"Wyślij wiadomość\".";
$MESS["CRM_EVENT_PHONE_OBSOLETE"] = "Typ \"Połączenie telefoniczne\" jest przestarzały; istnieje tylko dla zgodności wstecznej. Użyj polecenia \"Nowe połączenie\".";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Status oferty";
$MESS["CRM_EVENT_STAGE_ID"] = "Etap deala";
$MESS["CRM_EVENT_STATUS_ID"] = "Status leada";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Firma";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Kontakt";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Deal";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Lead";
$MESS["CRM_EVENT_TITLE_ORDER"] = "Zamówienie";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Oferta";
