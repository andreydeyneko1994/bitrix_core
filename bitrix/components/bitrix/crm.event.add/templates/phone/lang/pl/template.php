<?
$MESS["CRM_CALL_DESCR"] = "Kontakt #NAME# #PHONE#";
$MESS["CRM_EVENT_ADD_BUTTON"] = "Dodaj";
$MESS["CRM_EVENT_ADD_FILE"] = "Dołącz plik";
$MESS["CRM_EVENT_ADD_TITLE"] = "Rejestr połączeń telefonicznych";
$MESS["CRM_EVENT_DATE"] = "Data połączenia telefonicznego";
$MESS["CRM_EVENT_DESC_TITLE"] = "Opis rozmowy";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Status oferty";
$MESS["CRM_EVENT_STAGE_ID"] = "Etap deala";
$MESS["CRM_EVENT_STATUS_ID"] = "Status leada";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Firma";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Kontakt";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Deal";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Lead";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Oferta";
$MESS["CRM_NO_PHONES"] = "nie znaleziono żadnych telefonów";
$MESS["CRM_PHONE_LIST"] = "Telefony";
?>