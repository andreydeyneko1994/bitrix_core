<?
$MESS["CRM_TRACKING_CHANNEL_POOL_AV"] = "Disponível";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_EMAIL"] = "Endereços de e-mail disponíveis";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_PHONE"] = "Números disponíveis";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD"] = "Adicionar";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_EMAIL"] = "adicionar e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_PHONE"] = "adicionar número";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ALLOCATE"] = "Atribuição automática";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_CANCEL"] = "Cancelar";
$MESS["CRM_TRACKING_CHANNEL_POOL_DELETE_CONFIRM"] = "Você deseja excluir %code%? Essa ação não pode ser desfeita.";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_EMAIL"] = "Atribuir endereços de e-mail a fontes de anúncios";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_PHONE"] = "Atribuir números de telefone a fontes de anúncios";
$MESS["CRM_TRACKING_CHANNEL_POOL_FORMAT_PHONE"] = "Formato do número de telefone";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_EMAIL"] = "Atribuir endereços de e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_PHONE"] = "Atribuir números de telefone";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_EMAIL"] = "Endereços de e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_PHONE"] = "Números de telefone";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_EMAIL"] = "Digitar endereço de e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_PHONE"] = "Digitar número";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_SUCCESS"] = "Este número suporta rastreamento de chamadas.";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_UNKNOWN"] = "Pode ser que este número não suporta rastreamento de chamadas. Clique para verificar.";
?>