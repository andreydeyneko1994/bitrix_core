<?
$MESS["CRM_TRACKING_CHANNEL_POOL_AV"] = "Dostępny";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_EMAIL"] = "Dostępne adresy e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_AV_ITEMS_PHONE"] = "Dostępne numery";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD"] = "Dodaj";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_EMAIL"] = "dodaj adres e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ADD_PHONE"] = "dodaj numer";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_ALLOCATE"] = "Przypisz automatycznie";
$MESS["CRM_TRACKING_CHANNEL_POOL_BTN_CANCEL"] = "Anuluj";
$MESS["CRM_TRACKING_CHANNEL_POOL_DELETE_CONFIRM"] = "Czy chcesz usunąć %code%? Tego działania nie można cofnąć.";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_EMAIL"] = "Przypisz adresy e-mail do źródeł reklam";
$MESS["CRM_TRACKING_CHANNEL_POOL_DESC_PHONE"] = "Przypisz numery telefonów do źródeł reklam";
$MESS["CRM_TRACKING_CHANNEL_POOL_FORMAT_PHONE"] = "Format numeru telefonu";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_EMAIL"] = "Przypisz adresy e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_HEAD_PHONE"] = "Przypisz numery telefonu";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_EMAIL"] = "Adresy e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEMS_PHONE"] = "Numery telefonu";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_EMAIL"] = "Wprowadź adres e-mail";
$MESS["CRM_TRACKING_CHANNEL_POOL_ITEM_DEMO_PHONE"] = "Wprowadź numer";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_SUCCESS"] = "Ten numer obsługuje śledzenie połączeń.";
$MESS["CRM_TRACKING_CHANNEL_POOL_PHONE_STATUS_UNKNOWN"] = "Ten numer może nie obsługiwać śledzenia połączeń. Kliknij, aby sprawdzić.";
?>