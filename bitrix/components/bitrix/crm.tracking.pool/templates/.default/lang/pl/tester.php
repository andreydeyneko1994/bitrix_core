<?php
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_FAIL"] = "Numer nie obsługuje śledzenia połączeń";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_SUCCESS"] = "Numer obsługuje śledzenie połączeń";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_UNKNOWN"] = "Nie udało się wyśledzić połączenia na ten numer";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE"] = "Data ostatniej weryfikacji śledzenia połączeń";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE_NO"] = "brak";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_CALL"] = "Zadzwoń na \"%number%\"";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_END"] = "Odczekaj pierwsze dzwonki i rozłącz się";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_WAIT"] = "Oczekiwanie na połączenie przychodzące";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER"] = "Sprawdzanie numeru";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_DESC"] = "Sprawdź, czy Twój numer telefonu i operator telefonii obsługują śledzenie połączeń. W tym celu wprowadź numer telefonu, z którego będziesz dzwonić i wykonaj połączenie.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_INPUT"] = "Wprowadź numer telefonu, z którego zadzwonisz";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_START"] = "Rozpocznij sprawdzanie";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_STOP"] = "Zatrzymaj sprawdzanie";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT"] = "Obsługiwane śledzenie połączeń";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT_DESC"] = "Na ten numer otrzymano połączenia przychodzące. Śledzenie połączeń jest dostępne.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN"] = "Brak połączeń przychodzących na ten numer";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN_DESC"] = "Na ten numer nie przyszło jeszcze żadne połączenie przychodzące. Śledzenie połączeń może być niedostępne. Możesz teraz sprawdzić obsługę śledzenia połączeń dla tego numeru lub poczekać na połączenia przychodzące od klientów.";
