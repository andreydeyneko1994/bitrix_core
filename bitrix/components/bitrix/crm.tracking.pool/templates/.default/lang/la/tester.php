<?php
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_FAIL"] = "El número no permite el rastreo de llamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_SUCCESS"] = "El número permite el rastreo de llamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_RESULT_UNKNOWN"] = "No se pudo rastrear la llamada a este número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE"] = "Fecha de verificación del rastreo de la última llamada";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_DATE_NO"] = "ninguno";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_CALL"] = "Llamar al \"%number%\"";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_END"] = "Espere a que suene y cuelgue";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_INST_WAIT"] = "Esperando la llamada entrante";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER"] = "Verificación de número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_DESC"] = "Compruebe si su número de teléfono y su proveedor de telefonía permiten el rastreo de llamadas. Para ello, introduzca el número de teléfono desde el que llamará y haga una llamada.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_NUMBER_INPUT"] = "Introduzca el número de teléfono desde el que llamará";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_START"] = "Iniciar la comprobación";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TEST_STOP"] = "Detener la comprobación";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT"] = "Soporte para el rastreo de llamadas";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_SUPPORT_DESC"] = "Recibió llamadas entrantes en este número. El rastreo de llamadas está disponible.";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN"] = "No hay llamadas entrantes para este número";
$MESS["CRM_TRACKING_CHANNEL_POOL_TESTER_TITLE_UNKNOWN_DESC"] = "Aún no ha recibido ninguna llamada entrante en este número. Es posible que el rastreo de llamadas no esté disponible. Puede consultar en este momento el soporte para el rastreo de llamadas de este número o esperar las llamadas entrantes de sus clientes.";
