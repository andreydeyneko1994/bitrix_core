<?
$MESS["CRM_MAIL_TEMPLATE_ADD"] = "Dodaj szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_ADD_TITLE"] = "Utwórz nowy szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Usuń szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_BTNTITLE"] = "Usuń szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Na pewno chcesz usunąć ten szablon e-mail?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_TITLE"] = "Usuń szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Usuń szablon e-mail";
$MESS["CRM_MAIL_TEMPLATE_LIST"] = "Szablony e-mail";
$MESS["CRM_MAIL_TEMPLATE_LIST_TITLE"] = "Wyświetl wszystkie szablony e-mail";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
?>