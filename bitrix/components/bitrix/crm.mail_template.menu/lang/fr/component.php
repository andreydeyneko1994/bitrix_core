<?
$MESS["CRM_MAIL_TEMPLATE_ADD"] = "Ajouter un modèle d'e-mail";
$MESS["CRM_MAIL_TEMPLATE_ADD_TITLE"] = "Passage à la création d'un nouveeau modèle d'e-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_BTNTITLE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Êtes-vous sûr de vouloir supprimer ce modèle d'e-mail ?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_TITLE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_LIST"] = "Liste des modèles postaux";
$MESS["CRM_MAIL_TEMPLATE_LIST_TITLE"] = "Passage vers la liste de modèles d'e-mail";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>