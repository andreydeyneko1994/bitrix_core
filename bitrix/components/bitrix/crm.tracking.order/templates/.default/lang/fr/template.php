<?php
$MESS["CRM_TRACKING_ORDER_CONNECT"] = "Connectez un canal";
$MESS["CRM_TRACKING_ORDER_CONNECTED"] = "Le canal est connecté !";
$MESS["CRM_TRACKING_ORDER_DESC"] = "Voulez-vous recevoir les commandes de votre boutique en ligne sous la forme de transactions de CRM ? Suivez les sources publicitaires et chemins des clients pour ces transactions !";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE"] = "Connectez le site";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE_DESC"] = "Assurez-vous que votre site web est connecté dans le canal \"Propre site\".";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE"] = "Ajoutez le code à la page de commande";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC"] = "Ajoutez le code de suivi sur la page de création de commande ou de confirmation de paiement.";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC_LINK"] = "En savoir plus";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE"] = "Exemple de code";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_ID"] = "ORDER_ID";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_SUM"] = "ORDER_TOTAL";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD"] = "Sélectionnez un champ de la transaction";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_DESC"] = "Votre ID de commande de la boutique en ligne peut être enregistré dans la transaction. Spécifiez le champ de la transaction où mettre l'ID de commande.";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NAME"] = "Champ de la transaction";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NONE"] = "Non sélectionné";
