<?php
$MESS["CRM_TRACKING_ORDER_CONNECT"] = "Conecte o canal";
$MESS["CRM_TRACKING_ORDER_CONNECTED"] = "O canal foi conectado!";
$MESS["CRM_TRACKING_ORDER_DESC"] = "Receber pedidos da sua loja virtual como negócios do CRM? Acompanhe as fontes do anúncio e as jornadas do cliente para esses negócios!";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE"] = "Conectar um site";
$MESS["CRM_TRACKING_ORDER_STEP_CONNECT_SITE_DESC"] = "Certifique-se de que o site da sua loja on-line está conectado no canal \"Site próprio\".";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE"] = "Coloque o código na página de pagamento";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC"] = "Coloque o código de rastreamento na página de criação do pedido ou confirmação de pagamento.";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_DESC_LINK"] = "Saiba mais";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE"] = "Exemplo do código";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_ID"] = "ORDER_ID";
$MESS["CRM_TRACKING_ORDER_STEP_INSTALL_CODE_EXAMPLE_SUM"] = "ORDER_TOTAL";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD"] = "Selecione o campo do negócio";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_DESC"] = "O ID do pedido criado na sua loja on-line pode ser salvo no negócio. Especifique aqui o campo do negócio para salvar o ID do pedido.";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NAME"] = "Campo do negócio";
$MESS["CRM_TRACKING_ORDER_STEP_SELECT_FIELD_NONE"] = "Não selecionado";
