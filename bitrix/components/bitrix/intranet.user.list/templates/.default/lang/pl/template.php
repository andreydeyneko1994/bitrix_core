<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_CONFIRM"] = "Pracownik nie będzie mógł zalogować się do Bitrix24 i nie będzie widoczny w strukturze firmy. Jednakże, wszystkie dane użytkownika (pliki, wiadomości, zadania itd.) pozostaną nienaruszone.

Na pewno odwołać dostęp dla tego użytkownika?";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_CONFIRM"] = "Zaproszenie zostanie usunięte nieodwracalnie.

Na pewno chcesz usunąć tego pracownika?";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_SUCCESS"] = "Zaproszenie zostało wysłane";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_CONFIRM"] = "Pracownik będzie mógł zalogować się do Bitrix24 i będzie widoczny w strukturze firmy.

Na pewno udzielić pracownikowi dostępu do Bitrix24?";
$MESS["INTRANET_USER_LIST_STEPPER_TITLE"] = "Reindeksacja użytkowników";
?>