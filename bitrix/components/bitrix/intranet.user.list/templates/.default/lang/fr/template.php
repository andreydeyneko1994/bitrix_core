<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_CONFIRM"] = "L'employé ne pourra plus se connecter à Bitrix24 et n'apparaîtra plus dans la structure de l'entreprise. Cependant, toutes ses données d'utilisateur (fichiers, messages, tâches, etc.) resteront intactes.

Voulez-vous vraiment révoquer l'accès de l'employé ?";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_CONFIRM"] = "L'Invitation d'utilisateur sera définitivement supprimée.

Voulez-vous vraiment supprimer l'employé ?";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_SUCCESS"] = "L'invitation a été envoyée";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_CONFIRM"] = "L'employé pourra se connecter à Bitrix24 et apparaîtra dans la structure de l'entreprise.

Voulez-vous vraiment accorder un accès à Bitrix24 à l'utilisateur ?";
$MESS["INTRANET_USER_LIST_STEPPER_TITLE"] = "Réindexage les utilisateurs";
?>