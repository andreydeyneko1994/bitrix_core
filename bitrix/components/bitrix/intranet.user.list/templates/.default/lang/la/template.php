<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_CONFIRM"] = "El empleado no podrá iniciar sesión en Bitrix24 y ya no aparecerá en la estructura de la compañía. Sin embargo, todos los datos del usuario (archivos, mensajes, tareas, etc.) se conservarán.

¿Está seguro de que desea revocar el acceso del usuario?";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_CONFIRM"] = "La invitación del usuario se eliminará de forma irreversible.

¿Está seguro de que quiere eliminar al empleado?";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_SUCCESS"] = "Se envió la invitación";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_CONFIRM"] = "El empleado podrá iniciar sesión en Bitrix24 y aparecerá en la estructura de la compañía.

¿Está seguro de que desea conceder acceso al usuario a Bitrix24?";
$MESS["INTRANET_USER_LIST_STEPPER_TITLE"] = "Reindexando a los usuarios";
?>