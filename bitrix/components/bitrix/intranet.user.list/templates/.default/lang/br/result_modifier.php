<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE"] = "Dispensar";
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_TITLE"] = "Dispensar o funcionário";
$MESS["INTRANET_USER_LIST_ACTION_DELETE"] = "Excluir";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_TITLE"] = "Excluir convite";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE"] = "Enviar mensagem";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_HISTORY"] = "Log de mensagem";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_HISTORY_TITLE"] = "Exibir log de mensagem do usuário";
$MESS["INTRANET_USER_LIST_ACTION_MESSAGE_TITLE"] = "Enviar mensagem para o usuário";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE"] = "Convidar novamente";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_TITLE"] = "Convidar o usuário novamente";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE"] = "Contratar";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_TITLE"] = "Contratar o usuário novamente";
$MESS["INTRANET_USER_LIST_ACTION_TASK"] = "Atribuir tarefa";
$MESS["INTRANET_USER_LIST_ACTION_TASK_TITLE"] = "Atribuir uma tarefa ao usuário";
$MESS["INTRANET_USER_LIST_ACTION_VIDEOCALL"] = "Chamada de vídeo";
$MESS["INTRANET_USER_LIST_ACTION_VIDEOCALL_TITLE"] = "Iniciar chamada de vídeo com o usuário";
$MESS["INTRANET_USER_LIST_ACTION_VIEW"] = "Visualizar perfil";
$MESS["INTRANET_USER_LIST_ACTION_VIEW_TITLE"] = "Visualizar perfil do usuário";
$MESS["INTRANET_USER_LIST_BUTTON_INVITE_TITLE"] = "Convidar funcionários";
$MESS["INTRANET_USER_LIST_GENDER_F"] = "feminino";
$MESS["INTRANET_USER_LIST_GENDER_M"] = "masculino";
$MESS["INTRANET_USER_LIST_MENU_EXPORT_EXCEL_TITLE"] = "Exportar para o Microsoft Excel";
$MESS["INTRANET_USER_LIST_MENU_SYNC_CARDDAV_TITLE"] = "Sincronizar com CardDAV";
$MESS["INTRANET_USER_LIST_MENU_SYNC_OUTLOOK_TITLE"] = "Sincronizar com o Microsoft Outlook";
?>