<?
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_CONFIRM"] = "O funcionário não poderá fazer login no Bitrix24 e não será mais mostrado na estrutura da empresa. No entanto, todos os dados do usuário (arquivos, mensagens, tarefas etc.) permanecerão.

Tem certeza de que deseja revogar o acesso do usuário?";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_CONFIRM"] = "O convite do usuário será excluído irreversivelmente.

Tem certeza de que deseja excluir o funcionário?";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_SUCCESS"] = "O convite foi enviado";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_CONFIRM"] = "O funcionário poderá fazer login no Bitrix24 e aparecer na estrutura da empresa.

Tem certeza de que deseja dar acesso ao Bitrix24 ao usuário?";
$MESS["INTRANET_USER_LIST_STEPPER_TITLE"] = "Reindexar usuários";
?>