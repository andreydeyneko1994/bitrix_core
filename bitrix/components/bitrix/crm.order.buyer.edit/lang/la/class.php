<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_ACTIVE"] = "Activo";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_CONFIRM_PASSWORD"] = "Confirmar nueva contraseña";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_CONFIRM_PASSWORD_NEW"] = "Confirmar contraseña";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_DATE_REGISTER"] = "Registrado en";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_EMAIL"] = "Email";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_LAST_LOGIN"] = "Último inicio de sesión en";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_LAST_NAME"] = "Apellido";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_LOGIN"] = "Iniciar sesión";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_NAME"] = "Nombre";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_PASSWORD"] = "Nueva contraseña";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_PASSWORD_NEW"] = "Contraseña";
$MESS["CRM_ORDER_BUYER_EDIT_COLUMN_SECOND_NAME"] = "Segundo nombre";
$MESS["CRM_ORDER_BUYER_EDIT_CREATE_TITLE"] = "Crear cliente";
$MESS["CRM_ORDER_BUYER_EDIT_EDIT_TITLE"] = "Editar cliente";
$MESS["CRM_ORDER_BUYER_EDIT_LOAD_USER_ERROR"] = "El cliente no existe o acceso denegado.";
$MESS["CRM_ORDER_BUYER_EDIT_SAVE_ERROR"] = "Se produjo un error al guardar el cliente.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
?>