<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_TAX_FIELD_CODE"] = "Code mnémonique";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Description";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_LID"] = "Site web";
$MESS["CRM_TAX_FIELD_NAME"] = "Nom";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_TAX_NOT_FOUND"] = "L'impôt n'est pas retrouvé!";
$MESS["CRM_TAX_RATE_LIST"] = "Liste des taux de TVA";
$MESS["CRM_TAX_SECTION_MAIN"] = "Impôt";
