<?php
$MESS["CRM_KANBAN_ED_CANCEL"] = "Cancelar";
$MESS["CRM_KANBAN_ED_CHANGE"] = "cambiar";
$MESS["CRM_KANBAN_ED_CHANGE_USER"] = "Cambiar";
$MESS["CRM_KANBAN_HAS_INACCESSIBLE_FIELDS"] = "Se denegó el acceso a esta función porque hay campos que no están disponibles. Comuníquese con el administrador para obtener acceso y resolver el problema.";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Comentario";
$MESS["CRM_KANBAN_POPUP_CONFIRM"] = "Confirmar acción";
$MESS["CRM_KANBAN_POPUP_CONFIRM_DELETE"] = "¿Quiere eliminar la entidad? Esta acción no se puede deshacer.";
$MESS["CRM_KANBAN_POPUP_DATE"] = "Fecha";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Factura #";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Parámetros de cierre de factura:";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Crear basado en el prospecto:";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Seleccionar";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Cancelar";
$MESS["CRM_KANBAN_POPUP_PARAMS_DELETE"] = "Eliminar";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Guardar";
