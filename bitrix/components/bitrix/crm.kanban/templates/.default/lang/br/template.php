<?php
$MESS["CRM_KANBAN_ED_CANCEL"] = "Cancelar";
$MESS["CRM_KANBAN_ED_CHANGE"] = "alterar";
$MESS["CRM_KANBAN_ED_CHANGE_USER"] = "Alterar";
$MESS["CRM_KANBAN_HAS_INACCESSIBLE_FIELDS"] = "O acesso a esta função foi negado porque há campos indisponíveis para você. Por favor, entre em contato com o administrador para obter acesso e resolver o problema.";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Comentário";
$MESS["CRM_KANBAN_POPUP_CONFIRM"] = "Confirmar ação";
$MESS["CRM_KANBAN_POPUP_CONFIRM_DELETE"] = "Deseja excluir a entidade? Essa ação não pode ser desfeita.";
$MESS["CRM_KANBAN_POPUP_DATE"] = "Data";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Fatura #";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Parâmetros finais da fatura:";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Criar com base no Lead:";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Selecionar";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Cancelar";
$MESS["CRM_KANBAN_POPUP_PARAMS_DELETE"] = "Excluir";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Salvar";
