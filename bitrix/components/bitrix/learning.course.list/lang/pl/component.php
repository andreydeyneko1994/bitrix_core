<?
$MESS["LEARNING_COURSES_COURSE_ADD"] = "Dodaj nowy kurs";
$MESS["LEARNING_COURSES_NAV"] = "Kursy";
$MESS["LEARNING_COURSE_LIST"] = "Lista kursu";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Moduł e-Nauki nie jest zainstalowany.";
$MESS["LEARNING_PANEL_CONTROL_PANEL"] = "Panel Sterowania";
$MESS["LEARNING_PANEL_CONTROL_PANEL_ALT"] = "Wykonaj działanie w panelu kontrolnym";
$MESS["comp_course_list_toolbar_add"] = "Dodaj nowy kurs";
$MESS["comp_course_list_toolbar_add_title"] = "Dodaj nowy kurs w Panelu Kontrolnym";
$MESS["comp_course_list_toolbar_list"] = "Zarządzanie Kursem";
$MESS["comp_course_list_toolbar_list_title"] = "Lista Kursu w Panelu Kontrolnym";
?>