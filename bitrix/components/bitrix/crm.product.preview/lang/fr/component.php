<?php
$MESS["CRM_FIELD_ACTIVE"] = "Activité";
$MESS["CRM_FIELD_PRICE"] = "Prix";
$MESS["CRM_FIELD_SECTION"] = "Section";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[non sélectionné]";
$MESS["CRM_TITLE_PRODUCT"] = "Produit";
