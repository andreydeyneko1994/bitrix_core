<?php
$MESS["DISK_ACTION_SAVE_TO_OWN_FILES"] = "Guardar en Bitrix24.Drive";
$MESS["DISK_FOLDER_FILTER_CREATED_BY"] = "Creado por";
$MESS["DISK_FOLDER_FILTER_CREATE_TIME"] = "Creado el";
$MESS["DISK_FOLDER_FILTER_ID"] = "ID";
$MESS["DISK_FOLDER_FILTER_NAME"] = "Nombre";
$MESS["DISK_FOLDER_FILTER_PRESETS_RECENTLY_UPDATED"] = "Cambiado recientemente";
$MESS["DISK_FOLDER_FILTER_PRESETS_SHARED_FROM_ME"] = "Compartido por mi";
$MESS["DISK_FOLDER_FILTER_PRESETS_SHARED_TO_ME"] = "Compartido conmigo";
$MESS["DISK_FOLDER_FILTER_PRESETS_WITH_EXTERNAL_LINK"] = "Con enlaces públicos";
$MESS["DISK_FOLDER_FILTER_SEARCH_BY_CONTENT"] = "Contenido del documento";
$MESS["DISK_FOLDER_FILTER_SEARCH_IN_CURRENT_FOLDER"] = "En la carpeta actual";
$MESS["DISK_FOLDER_FILTER_SHARED"] = "Compartido";
$MESS["DISK_FOLDER_FILTER_SHARED_FROM_ME"] = "Por mi";
$MESS["DISK_FOLDER_FILTER_SHARED_TO_ME"] = "Conmigo";
$MESS["DISK_FOLDER_FILTER_UPDATE_TIME"] = "Modificado";
$MESS["DISK_FOLDER_FILTER_WITH_EXTERNAL_LINK"] = "Con enlaces públicos";
$MESS["DISK_FOLDER_FILTER_WITH_EXTERNAL_LINK_YES"] = "Si";
$MESS["DISK_FOLDER_LIST_ACT_BIZPROC_OLD_TEMPLATE"] = "(versión antigua)";
$MESS["DISK_FOLDER_LIST_ACT_CONNECT"] = "Conectarse al Drive";
$MESS["DISK_FOLDER_LIST_ACT_COPIED_INTERNAL_LINK"] = "Enlace copiado al Portapapeles";
$MESS["DISK_FOLDER_LIST_ACT_COPY"] = "Copiar";
$MESS["DISK_FOLDER_LIST_ACT_COPY_INTERNAL_LINK"] = "Copiar enlace interno";
$MESS["DISK_FOLDER_LIST_ACT_DETAILS"] = "Detalles";
$MESS["DISK_FOLDER_LIST_ACT_DOWNLOAD"] = "Descargar";
$MESS["DISK_FOLDER_LIST_ACT_EDIT"] = "Editar";
$MESS["DISK_FOLDER_LIST_ACT_GET_EXT_LINK"] = "Obtener enlace público";
$MESS["DISK_FOLDER_LIST_ACT_LOCK"] = "Bloquear";
$MESS["DISK_FOLDER_LIST_ACT_MARK_DELETED"] = "Eliminar";
$MESS["DISK_FOLDER_LIST_ACT_MOVE"] = "Mover";
$MESS["DISK_FOLDER_LIST_ACT_OPEN"] = "Abrir";
$MESS["DISK_FOLDER_LIST_ACT_RENAME"] = "Renombrar";
$MESS["DISK_FOLDER_LIST_ACT_RIGHTS_SETTINGS"] = "Permisos de acceso";
$MESS["DISK_FOLDER_LIST_ACT_SHARE_COMPLEX"] = "Compartir";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_HISTORY"] = "Revisión del historial";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL"] = "Compartiendo parámetros";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL_2"] = "Compartir";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL_3"] = "Compartir con otros usuarios";
$MESS["DISK_FOLDER_LIST_ACT_START_BIZPROC"] = "Procesos de negocios";
$MESS["DISK_FOLDER_LIST_ACT_UNLOCK"] = "Desbloquear";
$MESS["DISK_FOLDER_LIST_B24_APPEND_DISK_SPACE"] = "Agregar espacio en el disco";
$MESS["DISK_FOLDER_LIST_B24_LABEL_DISK_SPACE"] = "Gratis: #FREE_SPACE# of #DISK_SIZE#";
$MESS["DISK_FOLDER_LIST_B24_URL_DISK_SPACE"] = "http://www.bitrix24.es/prices/space.php#space100";
$MESS["DISK_FOLDER_LIST_COLUMN_BIZPROC"] = "Procesos de Negocios";
$MESS["DISK_FOLDER_LIST_COLUMN_CREATE_TIME"] = "Creado el";
$MESS["DISK_FOLDER_LIST_COLUMN_CREATE_USER"] = "Creado por";
$MESS["DISK_FOLDER_LIST_COLUMN_FORMATTED_SIZE"] = "Tamaño";
$MESS["DISK_FOLDER_LIST_COLUMN_ID"] = "ID";
$MESS["DISK_FOLDER_LIST_COLUMN_NAME"] = "Nombre";
$MESS["DISK_FOLDER_LIST_COLUMN_UPDATE_TIME"] = "Modificado el";
$MESS["DISK_FOLDER_LIST_COLUMN_UPDATE_USER"] = "Modificado por";
$MESS["DISK_FOLDER_LIST_DEFAULT_ACTION"] = "Seleccione la acción";
$MESS["DISK_FOLDER_LIST_DETACH_BUTTON"] = "Desconectar";
$MESS["DISK_FOLDER_LIST_DETACH_FILE_CONFIRM"] = "El archivo \"#NAME#\" se eliminarán de su disco si se desconecta de él.";
$MESS["DISK_FOLDER_LIST_DETACH_FILE_TITLE"] = "Desconectar archivos compartidos";
$MESS["DISK_FOLDER_LIST_DETACH_FOLDER_CONFIRM"] = "La carpeta \"#NAME#\" y todos los archivos se eliminarán de su disco si se desconecta de él.";
$MESS["DISK_FOLDER_LIST_DETACH_FOLDER_TITLE"] = "Desconectar carpetas compartidas";
$MESS["DISK_FOLDER_LIST_DETAIL_SHARE_INFO"] = "Compartido con";
$MESS["DISK_FOLDER_LIST_DETAIL_SHARE_SECTION"] = "Compartido";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_SHARING"] = "No se puede encontrar lo compartido.";
$MESS["DISK_FOLDER_LIST_FOLDER_LABEL_BTN_SHARE"] = "Compartir";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC"] = "Procesos de negocios";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TASKS"] = "Tareas";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TASKS_TITLE"] = "Ver las tareas";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TITLE"] = "Ver documentos del proceso de negocios";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_COPY"] = "Copiado exitosamente";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_DELETE"] = "Eliminado correctamente";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_MOVE"] = "Trasladado exitosamente";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_RENAME"] = "Se ha cambiado el nombre al elemento";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_APPROVE_RESHARING"] = "Los usuarios pueden volver a compartir esta carpeta";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_CANCEL"] = "Cancelar";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_ACCESS"] = "Dejar de compartir";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS"] = "Desconectar carpeta compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS_SIMPLE"] = "Desconectar";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS_SIMPLE_CANCEL"] = "Cancelar";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_OPEN_ACCESS"] = "Compartir";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_CAN_EDIT"] = "Otros usuarios no tienen permiso de edición";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION"] = "Con:";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION_1"] = "Agregar empleados";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION_2"] = "Agregar más";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_GROUP"] = "(grupo)";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_USER"] = "(propietario)";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_USER_COMMON_SECTION"] = "(creado por)";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_PLACEHOLDER"] = "Descripción";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_COMMON_SHARED_SECTION_PROCESS_DIE_ACCESS"] = "Desconectar carpeta compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_COMMON_SHARED_SECTION_PROCESS_DIE_ACCESS_SUCCESS"] = "Carpeta compartida desconectada ";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_CONNECTED_TITLE"] = "Drive de grupo";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_PROCESS_DIE_ACCESS"] = "Desconectar carpeta compartida del grupo";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_PROCESS_DIE_ACCESS_SUCCESS"] = "Carpeta compartida desconectada del grupo";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_HEADER_ACCESS"] = "Administrar carpeta compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_HEADER_MANAGE_ACCESS"] = "Invitar";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_ACCESS"] = "Abrir ahora acceso compartido";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_ACCESS_SUCCESS"] = "La carpeta '#FOLDERNAME#' es ahora compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_DIE_ACCESS"] = "Acceso compartido cerrado";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_DIE_ACCESS_SUCCESS"] = "Ya no es carpeta compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_1"] = "#COUNT# más usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_2_4"] = "#COUNT# más usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_5_20"] = "#COUNT# más usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_21"] = "#COUNT# más usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_1"] = "Acceso denegado: #COUNT# usuario";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_2_4"] = "Accesos denegados: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_5_20"] = "Accesos denegados: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_21"] = "Accesos denegados: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_1"] = "Editar accesos: #COUNT# usuario";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_2_4"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_5_20"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_21"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_1"] = "Editar accesos: #COUNT# usuario";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_2_4"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_5_20"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_21"] = "Editar accesos: #COUNT# usuarios";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ALL_ACCESS_DESCR"] = "Cerrar el acceso compartido a esta carpeta se eliminarán las copias locales de la carpeta y sus archivos en las máquinas de los usuarios que actualmente tienen acceso a esta carpeta. Su copia local se mantendrá intacta.";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ALL_ACCESS_SIMPLE"] = "Dejar de compartir";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ONCE_ACCESS_DESCR"] = "Cerrar el acceso compartido borrará la copia local de la carpeta y sus archivos en la máquina del usuario.";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ONCE_ACCESS_SIMPLE"] = "Dejar de compartir";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_SELF_ACCESS_SIMPLE"] = "Desconectar carpeta compartida";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_GROUP_DIE_SELF_ACCESS_SIMPLE"] = "Desconectar carpeta compartida del grupo";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_GROUP_DIE_SELF_ACCESS_SIMPLE_DESCR"] = "Esta acción elimina la carpeta del grupo y todos los archivos que hay en ella.";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_SHARING"] = "Compartir esta carpeta";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_USAGE_SHARING"] = "Parámetros de las carpetas compartidas";
$MESS["DISK_FOLDER_LIST_LABEL_UPDATE_TIME"] = "cambiado el #DATE#";
$MESS["DISK_FOLDER_LIST_MAKE_SHARE_SECTION"] = "Compartir";
$MESS["DISK_FOLDER_LIST_SORT_BY_DELETE_TIME"] = "Por fecha de eliminación";
$MESS["DISK_FOLDER_LIST_SORT_BY_FORMATTED_SIZE"] = "Por tamaño";
$MESS["DISK_FOLDER_LIST_SORT_BY_ID"] = "Por ID";
$MESS["DISK_FOLDER_LIST_SORT_BY_NAME"] = "Por nombre";
$MESS["DISK_FOLDER_LIST_SORT_BY_UPDATE_TIME"] = "Por fecha";
$MESS["DISK_FOLDER_LIST_SORT_BY_UPDATE_TIME_2"] = "Por fecha de cambio";
$MESS["DISK_FOLDER_LIST_TITLE_GRID_TOOLBAR_DEST_LABEL"] = "Nivel superior";
$MESS["DISK_FOLDER_LIST_TRASH_CANCEL_DELETE_BUTTON"] = "Cancelar";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_BUTTON"] = "Mover a la papelera de reciclaje";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_DELETED_FILE_CONFIRM"] = "¿Desea eliminar \"#NAME#\" permanentemente?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_DELETED_FOLDER_CONFIRM"] = "¿Desea eliminar la carpeta? \"#NAME#\" permanentemente?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "¿Quieres pasar \"#NAME#\" a la papelera de reciclaje, o eliminarlo de forma irreversible?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "¿Desea mover la carpeta \"#NAME#\" a la papelera de reciclaje, o eliminarlo de forma irreversible?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_FILE_CONFIRM"] = "¿Quieres pasar \"#NAME#\" a la papelera de reciclaje?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_FOLDER_CONFIRM"] = "¿Desea mover la carpeta \"#NAME#\" a la papelera de reciclaje?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_GROUP_CONFIRM"] = "¿Quieres mover los elementos seleccionados a la papelera de reciclaje?";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_TITLE"] = "Confirmar eliminación";
$MESS["DISK_FOLDER_LIST_TRASH_DESTROY_BUTTON"] = "Eliminar irreversiblemente";
$MESS["DISK_FOLDER_LIST_TRASH_DESTROY_GROUP_CONFIRM"] = "¿Desea eliminar los elementos seleccionados permanentemente?";
$MESS["DISK_FOLDER_LIST_UNSHARE_SECTION_CONFIRM"] = "Esto eliminará la carpeta y todos los archivos que hay en ella.";
$MESS["DISK_TRASHCAN_ACT_DESTROY"] = "Eliminar";
$MESS["DISK_TRASHCAN_ACT_RESTORE"] = "Restaurar";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_TIME"] = "Eliminado el";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_USER"] = "Eliminado por";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_DELETE_TIME"] = "Eliminado el";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_PRESETS_RECENTLY_DELETED"] = "Eliminado recientemente";
$MESS["DISK_TRASHCAN_FOLDER_FILTER_PRESETS_RECENTLY_UPDATED"] = "Cambiado recientemente";
$MESS["DISK_TRASHCAN_NAME"] = "Papelera de reciclaje";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "¿Usted esta seguro que quiere eliminarlo \"#NAME#\" permanentemente?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "¿Usted esta seguro que quiere eliminar la carpeta \"#NAME#\" permanentemente?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_DESCR_MULTIPLE"] = "¿Desea recuperar los elementos seleccionados?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FILE_CONFIRM"] = "¿Desea restaurar el documento \"#NAME#\"?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FOLDER_CONFIRM"] = "¿Desea restaurar la carpeta \"#NAME#\"?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_SUCCESS"] = "Los elementos se recuperaron";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_TITLE"] = "Confirmar Restauración";
