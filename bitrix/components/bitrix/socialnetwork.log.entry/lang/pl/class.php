<?php
$MESS["SONET_LOG_ENTRY_COMMENT_ADD_ERROR"] = "Nie można dodać komentarza.";
$MESS["SONET_LOG_ENTRY_COMMENT_DELETE_ERROR"] = "Nie można usunąć komentarza.";
$MESS["SONET_LOG_ENTRY_COMMENT_EDIT_ERROR"] = "Nie można edytować komentarza.";
