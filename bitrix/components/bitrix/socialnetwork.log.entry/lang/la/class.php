<?php
$MESS["SONET_LOG_ENTRY_COMMENT_ADD_ERROR"] = "No se puede agregar el comentario.";
$MESS["SONET_LOG_ENTRY_COMMENT_DELETE_ERROR"] = "No se puede eliminar el comentario.";
$MESS["SONET_LOG_ENTRY_COMMENT_EDIT_ERROR"] = "No se puede editar el comentario.";
