<?php
$MESS["CRM_DEFAULT_TITLE"] = "Sans nom";
$MESS["CRM_DELETE_ERROR"] = "Échec de suppression.";
$MESS["CRM_EXT_SALE_CD_EDIT"] = "Éditer";
$MESS["CRM_EXT_SALE_CD_PRINT"] = "Enregistrement imprimant";
$MESS["CRM_EXT_SALE_CD_VIEW"] = "Affichage";
$MESS["CRM_EXT_SALE_DEJ_CLOSE"] = "Fermer";
$MESS["CRM_EXT_SALE_DEJ_ORDER"] = "Commande";
$MESS["CRM_EXT_SALE_DEJ_PRINT"] = "Enregistrement imprimant";
$MESS["CRM_EXT_SALE_DEJ_SAVE"] = "Enregistrer";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_QUOTE_EDIT_CONTACT_MARKING_TITLE"] = "Rendre contact primaire";
$MESS["CRM_QUOTE_EDIT_CONTACT_SELECTOR_HEADER"] = "Contacts indiqués dans le devis";
$MESS["CRM_QUOTE_EDIT_FIELD_CLIENT"] = "Client";
$MESS["CRM_QUOTE_FIELD_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_QUOTE_FIELD_BEGINDATE"] = "Créé le";
$MESS["CRM_QUOTE_FIELD_BP_EMPTY_EVENT"] = "ne pas effectuer";
$MESS["CRM_QUOTE_FIELD_BP_EVENTS"] = "Commande";
$MESS["CRM_QUOTE_FIELD_BP_PARAMETERS"] = "Paramètres du processus d'entreprise";
$MESS["CRM_QUOTE_FIELD_BP_STATE_MODIFIED"] = "Date de l'état courant";
$MESS["CRM_QUOTE_FIELD_BP_STATE_NAME"] = "Statut courant";
$MESS["CRM_QUOTE_FIELD_BP_TEMPLATE_DESC"] = "Description";
$MESS["CRM_QUOTE_FIELD_CLOSED"] = "Devis cloturé";
$MESS["CRM_QUOTE_FIELD_CLOSEDATE"] = "Achèvement";
$MESS["CRM_QUOTE_FIELD_COMMENTS"] = "Commentaire (ne sera pas visible dans le devis)";
$MESS["CRM_QUOTE_FIELD_COMPANY_ID"] = "Entreprise";
$MESS["CRM_QUOTE_FIELD_CONTACT_ID"] = "Client";
$MESS["CRM_QUOTE_FIELD_CONTENT"] = "Contenu";
$MESS["CRM_QUOTE_FIELD_CURRENCY_ID"] = "Devise";
$MESS["CRM_QUOTE_FIELD_DEAL_ID"] = "Transaction";
$MESS["CRM_QUOTE_FIELD_FILES"] = "Fichiers";
$MESS["CRM_QUOTE_FIELD_LEAD_ID"] = "Prospect";
$MESS["CRM_QUOTE_FIELD_LOCATION_ID"] = "Emplacement";
$MESS["CRM_QUOTE_FIELD_LOCATION_ID_DESCRIPTION"] = "Saisissez le nom de la ville ou du pays";
$MESS["CRM_QUOTE_FIELD_MYCOMPANY_ID"] = "Mentions de votre société";
$MESS["CRM_QUOTE_FIELD_MYCOMPANY_ID1"] = "Mentions de ma société";
$MESS["CRM_QUOTE_FIELD_OPENED"] = "Accessible pour tous";
$MESS["CRM_QUOTE_FIELD_OPENED_TITLE"] = "Tous les utilisateurs peuvent consulter le devis";
$MESS["CRM_QUOTE_FIELD_OPPORTUNITY"] = "Total";
$MESS["CRM_QUOTE_FIELD_PRODUCT_ROWS"] = "Produits du devis";
$MESS["CRM_QUOTE_FIELD_QUOTE_NUMBER"] = "Devis #";
$MESS["CRM_QUOTE_FIELD_SALE_ORDER"] = "Lier au e-commerce";
$MESS["CRM_QUOTE_FIELD_SALE_ORDER1"] = "Commande du magasin";
$MESS["CRM_QUOTE_FIELD_STATUS_ID"] = "Statut du devis";
$MESS["CRM_QUOTE_FIELD_TERMS"] = "Conditions";
$MESS["CRM_QUOTE_FIELD_TITLE_QUOTE"] = "En-tête";
$MESS["CRM_SECTION_ADDITIONAL"] = "Plus";
$MESS["CRM_SECTION_CLIENT_INFO"] = "Au sujet du client";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "Produits du devis";
$MESS["CRM_SECTION_QUOTE_INFO"] = "À propos du devis";
$MESS["CRM_SECTION_QUOTE_SELLER"] = "Au sujet du revendeur";
$MESS["PRODUCT_ROWS_SAVING_ERROR"] = "Erreur survenue au cours de la sauvegarde de produits.";
$MESS["UNKNOWN_ERROR"] = "Erreur inconnue.";
