<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PERMISSION_DENIED"] = "Odmowa dostępu";
$MESS["M_CRM_INVOICE_LIST_CHANGE_STATUS"] = "Zmień status";
$MESS["M_CRM_INVOICE_LIST_COMPANY"] = "Połączone z firmą";
$MESS["M_CRM_INVOICE_LIST_CONTACT"] = "Połączone z kontaktem";
$MESS["M_CRM_INVOICE_LIST_DEAL"] = "Połączone z dealem";
$MESS["M_CRM_INVOICE_LIST_DELETE"] = "Usuń";
$MESS["M_CRM_INVOICE_LIST_EDIT"] = "Edycja";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Wyniki wyszukiwania";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Wszystkie faktury";
$MESS["M_CRM_INVOICE_LIST_MORE"] = "Więcej ...";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_PAID"] = "Moje zapłacone faktury";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_UNPAID"] = "Moje oczekujące faktury";
$MESS["M_CRM_INVOICE_LIST_QUOTE"] = "Połączony z ofertą";
$MESS["M_CRM_INVOICE_LIST_SEND"] = "Wyślij";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Własny filtr";
?>