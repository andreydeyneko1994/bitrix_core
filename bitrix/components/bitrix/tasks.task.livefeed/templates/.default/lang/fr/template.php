<?
$MESS["TASKS_SONET_LOG_DESCRIPTION"] = "description";
$MESS["TASKS_SONET_LOG_LABEL_TITLE"] = "Tâche";
$MESS["TASKS_SONET_LOG_RESPONSIBLE_ID"] = "Responsable";
$MESS["TASKS_SONET_LOG_STATUS"] = "Statut";
$MESS["TASKS_SONET_LOG_STATUS_CHANGED"] = "Le statut de la tâche est modifié";
$MESS["TASKS_SONET_LOG_TAGS"] = "Mots-clés : ";
?>