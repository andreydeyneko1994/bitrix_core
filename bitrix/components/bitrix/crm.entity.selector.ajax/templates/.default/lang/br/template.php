<?
$MESS["REST_CRM_FF_CANCEL"] = "Cancelar";
$MESS["REST_CRM_FF_CHANGE"] = "Editar";
$MESS["REST_CRM_FF_CHOISE"] = "Selecionar";
$MESS["REST_CRM_FF_CLOSE"] = "Fechar";
$MESS["REST_CRM_FF_COMPANY"] = "Empresas";
$MESS["REST_CRM_FF_CONTACT"] = "Contatos";
$MESS["REST_CRM_FF_DEAL"] = "Negócios";
$MESS["REST_CRM_FF_LAST"] = "Último";
$MESS["REST_CRM_FF_LEAD"] = "Leads";
$MESS["REST_CRM_FF_NO_RESULT"] = "Infelizmente, sua solicitação de pesquisa não deu resultados.";
$MESS["REST_CRM_FF_OK"] = "Selecionar";
$MESS["REST_CRM_FF_QUOTE"] = "Orçamentos";
$MESS["REST_CRM_FF_SEARCH"] = "Pesquisar";
?>