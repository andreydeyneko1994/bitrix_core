<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_ORDER_PAYMENT_BLOCK_CHECK_TITLE"] = "Entradas";
$MESS["CRM_ORDER_PAYMENT_BLOCK_MAIN_TITLE"] = "Pago";
$MESS["CRM_ORDER_PAYMENT_BLOCK_VOUCHERS_TITLE"] = "Documentos de pago";
$MESS["CRM_ORDER_PAYMENT_BUDGET"] = "Saldo interno de la cuenta del cliente";
$MESS["CRM_ORDER_PAYMENT_CASHBOX_STATUS_E"] = "Error";
$MESS["CRM_ORDER_PAYMENT_CASHBOX_STATUS_N"] = "No impreso";
$MESS["CRM_ORDER_PAYMENT_CASHBOX_STATUS_P"] = "Imprimir";
$MESS["CRM_ORDER_PAYMENT_CASHBOX_STATUS_Y"] = "Impreso";
$MESS["CRM_ORDER_PAYMENT_CHECK_LOOK"] = "Ver";
$MESS["CRM_ORDER_PAYMENT_CHECK_TITLE"] = "Recibo ##ID# of #DATE_CREATE#";
$MESS["CRM_ORDER_PAYMENT_CREATION_PAGE_TITLE"] = "Crear pago";
$MESS["CRM_ORDER_PAYMENT_EMPTY_ORDER_ID"] = "El ID del pedido está vacío.";
$MESS["CRM_ORDER_PAYMENT_FIELD_ACCOUNT_NUMBER"] = "Pago #";
$MESS["CRM_ORDER_PAYMENT_FIELD_CLIENT"] = "Cliente";
$MESS["CRM_ORDER_PAYMENT_FIELD_COMPANY"] = "Recibir el pago a";
$MESS["CRM_ORDER_PAYMENT_FIELD_DATE_BILL"] = "Creada el";
$MESS["CRM_ORDER_PAYMENT_FIELD_DATE_RESPONSIBLE_ID"] = "Persona responsable cambiada el";
$MESS["CRM_ORDER_PAYMENT_FIELD_EMP_RESPONSIBLE"] = "Persona responsable cambiada por";
$MESS["CRM_ORDER_PAYMENT_FIELD_ID"] = "ID";
$MESS["CRM_ORDER_PAYMENT_FIELD_ORDER_ID"] = "Pedido #";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_COMMENT"] = "Comentario";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_RETURN_COMMENT"] = "Comentario";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_RETURN_DATE"] = "Fecha de devolución";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_RETURN_LINK"] = "Agregar recibo de devolución";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_RETURN_NUM"] = "Recibo de devolución #";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_SYSTEM"] = "Método de pago";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_VOUCHER_DATE"] = "Fecha de pago";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_VOUCHER_LINK"] = "Agregar documento de pago";
$MESS["CRM_ORDER_PAYMENT_FIELD_PAY_VOUCHER_NUM"] = "Documento de pago #";
$MESS["CRM_ORDER_PAYMENT_FIELD_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_ORDER_PAYMENT_FIELD_STATUS"] = "Estado";
$MESS["CRM_ORDER_PAYMENT_FIELD_SUM_WITH_CURRENCY"] = "Total de la orden";
$MESS["CRM_ORDER_PAYMENT_NOT_FOUND"] = "No se encontró el pago";
$MESS["CRM_ORDER_PAYMENT_SUBTITLE_MASK"] = "##ID# - #DATE_INSERT#";
$MESS["CRM_ORDER_PAYMENT_TAB_CHECK"] = "Entradas";
$MESS["CRM_ORDER_PAYMENT_TAB_EVENT"] = "Historial";
$MESS["CRM_ORDER_PAYMENT_TAB_TREE"] = "Entidades vinculadas";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "ID del pago (#ID#), ##ACCOUNT_NUMBER#";
$MESS["CRM_ORDER_PAYMENT_TITLE2"] = "Pago ##ACCOUNT_NUMBER#";
?>