<?
$MESS["BPWC_NO_CRM_MODULE"] = "Le module CRM n'est pas installé.";
$MESS["CRM_EXT_SALE_DEJ_TITLE1"] = "Assistant de l'intégration";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue de marchandises n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devises n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Le module Blocs d'Information n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
?>