<?php
$MESS["BPATT_ALL"] = "Total";
$MESS["BPATT_AUTO_EXECUTE"] = "Autoexecução";
$MESS["BPATT_HELP1_TEXT"] = "O processo de negócio conduzido por status é um processo de negócio contínuo com distribuição de permissão de acesso para lidar com documentos em diferentes status.";
$MESS["BPATT_HELP2_TEXT"] = "O processo de negócio sequencial é um processo de negócio simples para executar uma série de atividades consecutivas em um documento.";
$MESS["BPATT_MODIFIED"] = "Modificado";
$MESS["BPATT_NAME"] = "Nome";
$MESS["BPATT_USER"] = "Modificado por";
$MESS["DISK_BIZPROC_LIST_CREATE_BP"] = "Criar";
$MESS["PROMPT_OLD_TEMPLATE"] = "A recente atualização da Unidade tornou alguns dos modelos de processo comercial obsoletos. Você ainda pode usá-los. No entanto, os novos formulários de edição de modelo serão um pouco diferentes. Os modelos herdados estão destacados.";
$MESS["WD_EMPTY"] = "Não há nenhum modelo de Processo Comercial. Criar padrão <a href=#HREF#>de Processos Comerciais</a>.";
$MESS["WD_EMPTY_NEW"] = "Não há nova versão de modelos de Processo Comercial. <a href=#HREF#>Criar</a>.";
