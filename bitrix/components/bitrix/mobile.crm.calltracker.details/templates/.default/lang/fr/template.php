<?php
$MESS["CRM_ADD_CONTACT"] = "Ajouter un contact";
$MESS["CRM_AMOUNT_AND_CURRENCY"] = "Opportunité";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Enregistrer pour plus tard";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Ajouter aux exceptions";
$MESS["CRM_COMPANY"] = "Entreprise";
$MESS["CRM_COMPANY_PLACEHOLDER"] = "Saisissez le nom de l'entreprise";
$MESS["CRM_CONTACT"] = "Contact";
$MESS["CRM_CONTACT_PLACEHOLDER"] = "Saisissez le nom du contact";
