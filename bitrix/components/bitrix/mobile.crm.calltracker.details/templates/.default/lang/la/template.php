<?php
$MESS["CRM_ADD_CONTACT"] = "Agregar contacto";
$MESS["CRM_AMOUNT_AND_CURRENCY"] = "Oportunidad";
$MESS["CRM_CALL_TRACKER_POSTPONE"] = "Guardar para más tarde";
$MESS["CRM_CALL_TRACKER_TO_IGNORED"] = "Agregar a las excepciones";
$MESS["CRM_COMPANY"] = "Compañía";
$MESS["CRM_COMPANY_PLACEHOLDER"] = "Introduzca el nombre de la empresa";
$MESS["CRM_CONTACT"] = "Contacto";
$MESS["CRM_CONTACT_PLACEHOLDER"] = "Introduzca el nombre del contacto";
