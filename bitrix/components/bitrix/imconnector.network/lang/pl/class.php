<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Błąd pobierania pliku. Jedynie .JPG i .PNG pliki są wspierane.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "Moduł Zewnętrznego Konektora Messengera nie jest zainstalowany";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMOPENLINES_NOT_INSTALLED"] = "Moduł \"Otwarty Kanał\" nie jest włączony";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE"] = "Błąd aktywacji konektora";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE_CONNECTOR"] = "Ten konektor nie jest aktywny";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_SAVE"] = "Nie można zapisać. Proszę sprawdź wszystkie dane i spróbuj ponownie.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_OK_SAVE"] = "Zapisane";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SESSION_HAS_EXPIRED"] = "Sesja wygasła. Proszę prześlij dane ponownie.";
?>