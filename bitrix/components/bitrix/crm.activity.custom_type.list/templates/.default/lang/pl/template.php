<?
$MESS["CRM_ACT_CUST_TYPE_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_SAVE"] = "Zapisz";
$MESS["CRM_ACT_CUST_TYPE_DEFAULT_NAME"] = "Nowy typ działania";
$MESS["CRM_ACT_CUST_TYPE_DELETE"] = "Usuń";
$MESS["CRM_ACT_CUST_TYPE_DELETE_CONFIRM"] = "Czy na pewno chcesz usunąć \"#NAME#\"?";
$MESS["CRM_ACT_CUST_TYPE_DELETE_TITLE"] = "Usuń lejek";
$MESS["CRM_ACT_CUST_TYPE_EDIT"] = "Edytuj";
$MESS["CRM_ACT_CUST_TYPE_EDIT_TITLE"] = "Edytuj typ działania";
$MESS["CRM_ACT_CUST_TYPE_ERROR_TITLE"] = "Zapisz błąd";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME"] = "Nazwa";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "Brakuje pola \"Nazwa\".";
$MESS["CRM_ACT_CUST_TYPE_FIELD_SORT"] = "Sortowanie";
$MESS["CRM_ACT_CUST_TYPE_TITLE_CREATE"] = "Utwórz nowy typ działania";
$MESS["CRM_ACT_CUST_TYPE_TITLE_EDIT"] = "Edytuj typ działania";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT"] = "Edytuj niestandardowe pola";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT_TITLE"] = "Edytuj pola niestandardowe dla wybranego typu działania";
$MESS["CRM_ALL"] = "Wszystkich";
?>