<?
$MESS["BM_BIRTHDAY"] = "Data de Nascimento";
$MESS["BM_DEPARTMENT"] = "Departamento ";
$MESS["BM_DIRECTOR"] = "Subordinado a";
$MESS["BM_DIRECTOR_OF"] = "Supervisa";
$MESS["BM_NO_USERS"] = "Sem usuários";
$MESS["BM_PHONE"] = "Telefone";
$MESS["BM_PHONE_INT"] = "Interno";
$MESS["BM_PHONE_MOB"] = "Móvel";
$MESS["BM_TO_USER_LIST"] = "Voltar a Lista";
$MESS["BM_USR_CNT"] = "Número de colaboradores";
$MESS["BM_WRITE"] = "Escrever";
?>