<?php
$MESS["BM_BIRTHDAY"] = "Fecha de nacimiento";
$MESS["BM_DEPARTMENT"] = "Departamento";
$MESS["BM_DIRECTOR"] = "Subordinado A";
$MESS["BM_DIRECTOR_OF"] = "Supervisores";
$MESS["BM_NO_USERS"] = "No hay usuarios";
$MESS["BM_PHONE"] = "Teléfono";
$MESS["BM_PHONE_INT"] = "Interno";
$MESS["BM_PHONE_MOB"] = "Móvil";
$MESS["BM_TO_USER_LIST"] = "Regresar a la Lista";
$MESS["BM_USR_CNT"] = "No. de empleados";
$MESS["BM_WRITE"] = "Escribir";
