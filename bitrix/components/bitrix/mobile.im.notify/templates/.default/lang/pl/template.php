<?php
$MESS["NM_DOWNTEXT"] = "Zwolnij, aby odświeżyć";
$MESS["NM_EMPTY"] = "Brak nowych powiadomień";
$MESS["NM_FOLD"] = "Zwiń";
$MESS["NM_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g:i a";
$MESS["NM_LOADTEXT"] = "Ponowne ładowanie…";
$MESS["NM_MORE"] = "Szczegóły";
$MESS["NM_MORE_USERS"] = "Jeszcze tyle osób: #COUNT#";
$MESS["NM_PULLTEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["NM_SYSTEM_USER"] = "Komunikat systemowy";
$MESS["NM_TITLE"] = "Powiadomienia";
$MESS["NM_TITLE_2"] = "Aktualizowanie…";
$MESS["NM_UNFOLD"] = "Rozwiń";
