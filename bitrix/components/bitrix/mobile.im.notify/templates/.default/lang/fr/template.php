<?php
$MESS["NM_DOWNTEXT"] = "Relâcher pour mettre à jour...";
$MESS["NM_EMPTY"] = "Il n'y a pas de nouvelles notifications";
$MESS["NM_FOLD"] = "Réduire";
$MESS["NM_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g:i a";
$MESS["NM_LOADTEXT"] = "Mise à jour...";
$MESS["NM_MORE"] = "Détails";
$MESS["NM_MORE_USERS"] = "#COUNT# autres personnes";
$MESS["NM_PULLTEXT"] = "Tirez vers le bas pour rafraîchir...";
$MESS["NM_SYSTEM_USER"] = "Gestion des paramètres système";
$MESS["NM_TITLE"] = "Notifications";
$MESS["NM_TITLE_2"] = "Mise à jour...";
$MESS["NM_UNFOLD"] = "Développer";
