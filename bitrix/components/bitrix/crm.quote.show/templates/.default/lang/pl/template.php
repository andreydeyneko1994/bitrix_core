<?
$MESS["CRM_DEAL_LINK"] = "Deal";
$MESS["CRM_EDIT_BTN_TTL"] = "Kliknij, aby edytować";
$MESS["CRM_LOCK_BTN_TTL"] = "Nie można edytować tego elementu";
$MESS["CRM_QUOTE_CANCEL_BTN_TTL"] = "Anuluj";
$MESS["CRM_QUOTE_CLIENT"] = "Klient";
$MESS["CRM_QUOTE_CLIENT_EMAIL"] = "E-mail";
$MESS["CRM_QUOTE_CLIENT_INFO"] = "Informacje kontaktowe";
$MESS["CRM_QUOTE_CLIENT_NOT_ASSIGNED"] = "[nie przypisane]";
$MESS["CRM_QUOTE_CLIENT_PHONE"] = "Telefon";
$MESS["CRM_QUOTE_COMMENT"] = "Komentarz";
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Potrzebujesz uprawnień do tworzenia deala i faktury, aby kontynuować.";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Anuluj";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Kontynuuj";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Wybierz obiekty, w których będą stworzone dodatkowe pola";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Następujące pola będą stworzone";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "Wybrane obiekty nie posiadają pól, do których można przekazać dane.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Utwórz element z oferty";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Błąd ogólny.";
$MESS["CRM_QUOTE_DATE_CREATE"] = "Utworzony";
$MESS["CRM_QUOTE_DATE_MODIFY"] = "Zmodyfikowany";
$MESS["CRM_QUOTE_DISK_ATTACHED_FILES"] = "Załączniki";
$MESS["CRM_QUOTE_DURATION"] = "Data wygaśnięcia";
$MESS["CRM_QUOTE_FILES"] = "Pliki";
$MESS["CRM_QUOTE_NOT_OPENED"] = "Ta oferta nie jest publiczna.";
$MESS["CRM_QUOTE_NO_PRINT_TEMPLATES_ERROR"] = "Nie zdefiniowano szablonu druku.";
$MESS["CRM_QUOTE_NO_PRINT_URL_ERROR"] = "Nie znaleziono URL procesora druku.";
$MESS["CRM_QUOTE_OPENED"] = "Ta oferta może być oglądana przez innych.";
$MESS["CRM_QUOTE_OPPORTUNITY_SHORT"] = "Kwota";
$MESS["CRM_QUOTE_PRINT_BTN_TTL"] = "Drukuj";
$MESS["CRM_QUOTE_PRINT_DLG_TTL"] = "Drukuj Oferty";
$MESS["CRM_QUOTE_PRINT_TEMPLATE_FIELD"] = "Szablon";
$MESS["CRM_QUOTE_SHOW_BUTTON_CANCEL"] = "Anuluj";
$MESS["CRM_QUOTE_SHOW_BUTTON_SAVE"] = "Zapisz";
$MESS["CRM_QUOTE_SHOW_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Lejek";
$MESS["CRM_QUOTE_SHOW_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Ustawienia deala";
$MESS["CRM_QUOTE_SHOW_LEGEND"] = "oferta ##QUOTE_NUMBER#";
$MESS["CRM_QUOTE_SHOW_TAB_1"] = "Oferta";
$MESS["CRM_QUOTE_SHOW_TAB_1_TITLE"] = "Właściwości oferty";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Oferta ##QUOTE_NUMBER# z #BEGINDATE#";
$MESS["CRM_QUOTE_SIDEBAR_STATUS"] = "Status";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Dołącz plik";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Przeciągnij jeden lub więcej plików tutaj";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Niedozwolone.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Plik o tej nazwie już istnieje. Można nadal użyć bieżącego folderu, w tym przypadku istniejąca wersja dokumentu zostanie zapisana w historii.";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Ładowanie";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Załaduj pliki";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "lub wybierz plik z komputera";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Wybierz z biblioteki";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Pliki";
$MESS["CRM_TAB_2"] = "Kontakt";
$MESS["CRM_TAB_2_TITLE"] = "Kontakt oferty";
$MESS["CRM_TAB_3"] = "Firma";
$MESS["CRM_TAB_3_TITLE"] = "Frima oferty";
$MESS["CRM_TAB_4"] = "Lead";
$MESS["CRM_TAB_4_TITLE"] = "Lead oferty";
$MESS["CRM_TAB_6"] = "Działania";
$MESS["CRM_TAB_6_TITLE"] = "Działania oferty";
$MESS["CRM_TAB_7"] = "Proces Biznesowy";
$MESS["CRM_TAB_7_TITLE"] = "Procesy biznesowe oferty";
$MESS["CRM_TAB_8"] = "Faktura";
$MESS["CRM_TAB_8_TITLE"] = "Faktura oferty";
$MESS["CRM_TAB_8_TITLE_V2"] = "Faktury oferty";
$MESS["CRM_TAB_8_V2"] = "Faktury";
$MESS["CRM_TAB_DEAL"] = "Deale";
$MESS["CRM_TAB_DEAL_TITLE"] = "Deale ofert";
$MESS["CRM_TAB_DETAILS"] = "Szczegóły";
$MESS["CRM_TAB_DETAILS_TITLE"] = "Szczegóły";
$MESS["CRM_TAB_HISTORY"] = "Historia";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Historia aktualizacji oferty";
$MESS["CRM_TAB_LIVE_FEED"] = "Gwarantowane";
$MESS["CRM_TAB_LIVE_FEED_TITLE"] = "Gwarantowany deal";
$MESS["CRM_TAB_PRODUCT_ROWS"] = "Produkty";
$MESS["CRM_TAB_PRODUCT_ROWS_TITLE"] = "Produkty w ofercie";
$MESS["CRM_TAB_TREE"] = "Zależności";
$MESS["CRM_TAB_TREE_TITLE"] = "Odnośniki do innych elementów i pozycji";
?>