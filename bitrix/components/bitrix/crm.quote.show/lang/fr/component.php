<?
$MESS["CRM_FIELD_ENTITY_TREE"] = "Dépendances";
$MESS["CRM_FIELD_QUOTE_DEAL"] = "Transactions";
$MESS["CRM_FIELD_UTM"] = "Paramètres UTM";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_QUOTE_ADD_INVOICE_TITLE"] = "Établir une facture à la base du devis";
$MESS["CRM_QUOTE_DEAL_LINK"] = "Devis créé à partir de la transaction <a href='#URL#'>#TITLE#</a>";
$MESS["CRM_QUOTE_FIELD_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_QUOTE_FIELD_BEGINDATE"] = "Créé le";
$MESS["CRM_QUOTE_FIELD_CLOSED"] = "Devis cloturé";
$MESS["CRM_QUOTE_FIELD_CLOSEDATE"] = "Achèvement";
$MESS["CRM_QUOTE_FIELD_COMMENTS"] = "Commentaire (ne sera pas visible dans le devis)";
$MESS["CRM_QUOTE_FIELD_COMPANY_ADDRESS"] = "Adresse réelle";
$MESS["CRM_QUOTE_FIELD_COMPANY_ADDRESS_LEGAL"] = "Adresse juridique";
$MESS["CRM_QUOTE_FIELD_COMPANY_EMAIL"] = "Adresses Adresse emailde l'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_EMPLOYEES"] = "Employés";
$MESS["CRM_QUOTE_FIELD_COMPANY_INDUSTRY"] = "Sphère de l'activité";
$MESS["CRM_QUOTE_FIELD_COMPANY_PHONE"] = "Téléphones de la compagnie";
$MESS["CRM_QUOTE_FIELD_COMPANY_REVENUE"] = "Chiffre d'affaires annuel";
$MESS["CRM_QUOTE_FIELD_COMPANY_TITLE"] = "Entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_TYPE"] = "Type de l'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_WEB"] = "Sites de la compagnie";
$MESS["CRM_QUOTE_FIELD_CONTACT_ADDRESS"] = "Adresse";
$MESS["CRM_QUOTE_FIELD_CONTACT_EMAIL"] = "adresse email du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_IM"] = "Messageries du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_PHONE"] = "Téléphones du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_POST"] = "Fonction";
$MESS["CRM_QUOTE_FIELD_CONTACT_SOURCE"] = "Source du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_TITLE"] = "Client";
$MESS["CRM_QUOTE_FIELD_CONTACT_TYPE"] = "Type de contact";
$MESS["CRM_QUOTE_FIELD_CONTENT"] = "Contenu";
$MESS["CRM_QUOTE_FIELD_CREATED_BY_ID"] = "Créé par";
$MESS["CRM_QUOTE_FIELD_CURRENCY_ID"] = "Devise";
$MESS["CRM_QUOTE_FIELD_DATE_CREATE"] = "Créé le";
$MESS["CRM_QUOTE_FIELD_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_QUOTE_FIELD_DEAL_ID"] = "Transaction";
$MESS["CRM_QUOTE_FIELD_FILES"] = "Fichiers";
$MESS["CRM_QUOTE_FIELD_LEAD_ID"] = "Prospect";
$MESS["CRM_QUOTE_FIELD_LOCATION_ID"] = "Emplacement";
$MESS["CRM_QUOTE_FIELD_MODIFY_BY_ID"] = "Modifié(e)s par";
$MESS["CRM_QUOTE_FIELD_MYCOMPANY_ID"] = "Mentions de votre société";
$MESS["CRM_QUOTE_FIELD_MYCOMPANY_ID1"] = "Mentions de ma société";
$MESS["CRM_QUOTE_FIELD_OPENED"] = "Accessible pour tous";
$MESS["CRM_QUOTE_FIELD_OPPORTUNITY"] = "Total";
$MESS["CRM_QUOTE_FIELD_PRODUCT_ROWS"] = "Produits du devis";
$MESS["CRM_QUOTE_FIELD_QUOTE_EVENT"] = "Histoire des changements de la proposition";
$MESS["CRM_QUOTE_FIELD_QUOTE_INVOICE"] = "Factures du devis";
$MESS["CRM_QUOTE_FIELD_QUOTE_NUMBER"] = "Devis #";
$MESS["CRM_QUOTE_FIELD_SALE_ORDER1"] = "Commande du magasin";
$MESS["CRM_QUOTE_FIELD_STATUS_ID"] = "Statut du devis";
$MESS["CRM_QUOTE_FIELD_TERMS"] = "Termes";
$MESS["CRM_QUOTE_FIELD_TITLE_QUOTE"] = "En-tête";
$MESS["CRM_QUOTE_SHOW_CONTACT_SELECTOR_HEADER"] = "Contacts mentionnés dans le devis";
$MESS["CRM_QUOTE_SHOW_FIELD_CLIENT"] = "Client";
$MESS["CRM_SECTION_ADDITIONAL"] = "Plus";
$MESS["CRM_SECTION_CLIENT_INFO"] = "Au sujet du client";
$MESS["CRM_SECTION_DATE"] = "Achèvement";
$MESS["CRM_SECTION_DETAILS"] = "Détails";
$MESS["CRM_SECTION_EVENT_MAIN"] = "Histoire des changements de la proposition";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "Produits du devis";
$MESS["CRM_SECTION_QUOTE"] = "À propos du devis";
$MESS["CRM_SECTION_QUOTE_SELLER"] = "À propos du vendeur";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[non affecté]";
?>