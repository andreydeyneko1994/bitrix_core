<?
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Pessoa responsável";
$MESS["CRM_FIELD_DATE_BILL"] = "Data da fatura";
$MESS["CRM_FIELD_DEAL"] = "Negócio";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Valor";
$MESS["CRM_FIELD_QUOTE"] = "Orçamento";
$MESS["CRM_FIELD_STATUS"] = "Status";
$MESS["CRM_TITLE_INVOICE"] = "Fatura";
$MESS["DATE_PAY_BEFORE"] = "Pagar antes de";
?>