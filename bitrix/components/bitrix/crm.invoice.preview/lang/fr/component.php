<?php
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Personne responsable";
$MESS["CRM_FIELD_DATE_BILL"] = "Date de la facture";
$MESS["CRM_FIELD_DEAL"] = "Transaction";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Montant";
$MESS["CRM_FIELD_QUOTE"] = "Devis";
$MESS["CRM_FIELD_STATUS"] = "Statut";
$MESS["CRM_TITLE_INVOICE"] = "Facture";
$MESS["DATE_PAY_BEFORE"] = "Date limite de paiement";
