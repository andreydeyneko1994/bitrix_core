<?
$MESS["CRM_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Ne pas enregistrer les paramètres actuels pour intégrer e-mail";
$MESS["CRM_BUTTON_DELETE"] = "Supprimer";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "Supprimer des réglages courants d'intégration avec la messagerie";
$MESS["CRM_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Enregistrer les paramètres d'intégration avec l'e-mail";
$MESS["CRM_TAB_CONFIG"] = "Intégration avec la messagerie";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Réglage de l'intégration avec la messagerie par la technologie Envoyer&Sauvagarder";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "Création d'une intégration avec la poste selon la technologie Send&Save";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "Paramètres d'intégration du Send&Save ?-mail";
?>