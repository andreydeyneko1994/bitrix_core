<?
$MESS["BPWC_WVCT_2LIST"] = "De processus d'entreprise";
$MESS["BPWC_WVCT_APPLY"] = "Appliquer";
$MESS["BPWC_WVCT_CANCEL"] = "Annuler";
$MESS["BPWC_WVCT_EMPTY"] = "Ce processus d'affaires n'a pas de paramètres à configurer.";
$MESS["BPWC_WVCT_SAVE"] = "Enregistrer";
$MESS["BPWC_WVCT_SUBTITLE"] = "Nouvelle configuration du processus d'affaires";
?>