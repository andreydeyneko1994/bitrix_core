<?php
$MESS["TASKS_PANEL_TAB_ALL"] = "Wszystkie";
$MESS["TASKS_PANEL_TAB_APPLICATIONS_2"] = "Market";
$MESS["TASKS_PANEL_TAB_CONFIG_PERMISSIONS"] = "Uprawnienia dostępu";
$MESS["TASKS_PANEL_TAB_EFFECTIVE"] = "Wydajność";
$MESS["TASKS_PANEL_TAB_EMPLOYEE_PLAN"] = "Zaangażowanie";
$MESS["TASKS_PANEL_TAB_KANBAN"] = "Kanban";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Kontrola";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Projekty";
$MESS["TASKS_PANEL_TAB_RECYCLEBIN"] = "Kosz";
$MESS["TASKS_PANEL_TAB_REPORTBOARD"] = "Tablica raportów";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Raporty";
$MESS["TASKS_PANEL_TAB_SCRUM"] = "Scrum";
$MESS["TASKS_PANEL_TAB_TASKS"] = "Zadania";
$MESS["TASKS_PANEL_TAB_TEMPLATES"] = "Szablony";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE"] = "<strong>Wydajność</strong> to wskaźnik produktywności, który pokazuje, jak dobrze ludzie pracują nad swoimi zadaniami.";
