<?
$MESS["INTET_ADD"] = "Dodawanie";
$MESS["INTET_APPLY"] = "Zastosuj";
$MESS["INTET_CANCEL"] = "Anuluj";
$MESS["INTET_CONFIRM"] = "Akceptuj";
$MESS["INTET_CREATE_TITLE"] = "Nowe zadanie";
$MESS["INTET_CURRENT_STATUS"] = "Bieżący Status";
$MESS["INTET_EDIT_TITLE"] = "Edytuj zadanie ##ID#";
$MESS["INTET_FINISH_BUTTON"] = "Zakończ";
$MESS["INTET_NOT_SET"] = "(nie ustawione)";
$MESS["INTET_REJECT"] = "Odrzuć";
$MESS["INTET_RESPONSIBLE"] = "Odpowiedzialny";
$MESS["INTET_SAVE"] = "Zapisz";
$MESS["INTET_SEND_COMMAND"] = "Wyślij polecenie";
$MESS["INTET_U_DEL"] = "Usuń";
$MESS["INTET_VE_INS_SUBTASK"] = "Dodaj podzadanie";
$MESS["INTET_VE_INS_SUBTASK1"] = "Podzadanie";
$MESS["INTET_VE_INS_SUBTASK2"] = "Nazwa podzadania";
$MESS["INTET_VIEW_TITLE"] = "Widok zadania ##ID#";
?>