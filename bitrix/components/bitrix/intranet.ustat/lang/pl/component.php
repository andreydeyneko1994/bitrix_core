<?
$MESS["INTRANET_USTAT_PERIOD_BUTTON_MONTH"] = "Miesiąc";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_TODAY"] = "dzisiaj";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_WEEK"] = "Tydzień";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_YEAR"] = "Rok";
$MESS["INTRANET_USTAT_PERIOD_TITLE"] = "Zakres daty:";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_GENERAL"] = "Procent użytkowników, którzy zostali w CRM w ciągu danego okresu.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Ten numer jest/może być ograniczony przez liczbę użytkowników mających dostęp do CRM.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_GENERAL"] = "Całkowita liczba plików ładowanych lub modyfikowanych za pomocą Bitrix24.Drive w danym okresie";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Procent użytkowników realizacji Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_GENERAL"] = "Całkowita liczba wiadomości wysyłanych i połączeń dokonanych w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Pokazuje procent użytkowników, którzy wysłali wiadomości lub wykonali połączenia audio i wideo w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_GENERAL"] = "Całkowita liczba \"Like'ów\" przy wiadomościach, komentarzach, zadaniach i innych elementach w intranecie w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy używali funkcji \"Like\" w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_GENERAL"] = "Całkowita liczba akcji w mobilnych aplikacjach w danym okresie (wiadomości, zadanie zmiany, CRM, itd.).";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy korzystali aplikacji mobilnych w danym okresie";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_GENERAL"] = "Wszystkie opublikowane wiadomości i komentarze w określonym okresie";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Pokazuje procent użytkowników wykorzystujących funkcjonalność sieci społecznościowej w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_GENERAL"] = "Całkowita liczba zmienionych zadań (utworzony, skomentował na, zmodyfikowany, itp.) w danym okresie.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Procent użytkowników, którzy korzystali z zadań w danym okresie.";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Firma";
$MESS["INTRANET_USTAT_TOGGLE_PEOPLE"] = "Ludzie";
?>