<?php
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TEXT"] = "Los compradores y clientes, ya sean potenciales, existentes o de larga data, son el negocio. Nuestro trabajo es aprender a trabajar con cada cliente de la manera más eficiente posible. Cada compra, cada interacción: todo lo relacionado con un cliente debe registrarse y estar disponible.

El CRM en Bitrix24 es ideal para hacer justamente eso, y podemos usarlo sin abandonar el entorno familiar de la intranet. Y sin tener que recordar las contraseñas de algún otro servicio.

[VIDEO WIDTH=510 HEIGHT=390]https://www.youtube.com/watch?v=N6K3udpfEOA[/VIDEO]

[IMG WIDTH=500 HEIGHT=315]https://www.bitrix24.com/images/ustat/en/crm1.png[/IMG]

[B]Cliente y base de contacto[/B]

Toda la información sobre los clientes (tanto compañías como personas) puede registrarse y buscarse fácilmente en la base de contactos del CRM. Además, se pueden enumerar todas las interacciones y programar interacciones futuras. Las compras o transacciones, llamadas 'Negociaciones' en el CRM, pueden mantenerse individualmente y asociarse con cada cliente.

[IMG WIDTH=500 HEIGHT=347]https://www.bitrix24.com/images/ustat/en/crm2.png[/IMG]

Cada nuevo cliente que aparece debe agregarse al CRM. ¡No olvide hacer esto! Los clientes potenciales (Prospectos) se pueden poner a mano, traídos automáticamente desde el contacto de su sitio web o formulario de comentarios, por correo electrónico o importados de un archivo de datos.

[IMG WIDTH=500 HEIGHT=278]https://www.bitrix24.com/images/ustat/en/crm3.png[/IMG]

[B]Trabajando con clientes[/B]

Recuerde que el CRM no es sólo una base de contacto. Es una herramienta para ayudar a atraer y rastrear clientes a través del proceso de compra o toma de decisiones. Puede planificar llamadas y reuniones, establecer tareas o escribir correos electrónicos directamente desde el CRM.

[IMG WIDTH=500 HEIGHT=385]https://www.bitrix24.com/images/ustat/en/crm4.png[/IMG]

[B]Asignación de contactos a asociados de ventas[/B]

Puede asignar prospectos a diferentes asociados de ventas de forma automática. Todo lo que tiene que hacer es establecer un Proceso de Negocios que 'clasifique' los prospectos según las condiciones necesarias y luego configure a la persona responsable de su equipo de ventas. Por ejemplo, si la 'oportunidad' en el prospecto es más de \$ 9999, puede enviar esa ventaja directamente al gerente de ventas VIP.

Las negociaciones en el CRM deben crearse tan pronto como se obtenga información sobre una compra potencial. La negociación no solo rastrea el progreso del proceso de toma de decisiones, sino que proporciona datos para el embudo de ventas y hace que la creación de facturas sea sencilla..

[IMG WIDTH=500 HEIGHT=309]https://www.bitrix24.com/images/ustat/en/crm5.png[/IMG]

[B]Interacciones en curso con los clientes[/B]

Los datos recopilados en el CRM lo ayudan en futuras interacciones con los clientes. También puede planificar reuniones, realizar llamadas de seguimiento y trabajar con clientes en masa o individualmente, ya que puede ordenar y filtrar los registros del cliente según sea necesario. Actividades es la sección que le muestra todas sus próximas interacciones, y el contador al lado le dice cuántas requieren atención lo antes posible!

[IMG WIDTH=500 HEIGHT=171]https://www.bitrix24.com/images/ustat/en/crm6.png[/IMG]

El sistema le permite enviar correos electrónicos (desde su dirección de correo electrónico) y crear facturas en interfaces muy prácticas. Haga clic en el número de teléfono de un contacto para iniciar una llamada de telefonía IP. Cuando envía una factura a un cliente, la factura se incluye como un archivo adjunto en PDF.

[IMG WIDTH=500 HEIGHT=335]https://www.bitrix24.com/images/ustat/en/crm7.png[/IMG]

No olvide que puede analizar sus datos en cualquier momento - el [B]embudo de ventas[/B] muestra negociaciones en sus diversas etapas y le permite filtrar negociaciones mostradas por varios criterios.

[IMG WIDTH=500 HEIGHT=359]https://www.bitrix24.com/images/ustat/en/crm8.png[/IMG]

[B]CRM móvil[/B]

Por cierto, para aquellos de ustedes que están en el camino y se reúnen con clientes en el campo, la aplicación móvil tiene el CRM. Puede obtener toda la información del cliente, editar los detalles de negociaciones o actualizar otra información, y luego crear una factura inmediatamente.

[IMG WIDTH=500 HEIGHT=320]https://www.bitrix24.com/images/ustat/en/crm9.jpg[/IMG]

[B]Evalúe su trabajo en el CRM[/B]

Use el CRM tanto como pueda - las recompensas de tener datos organizados de ventas son innumerables. Para evaluar cómo su actividad del CRM se compara con su departamento o compañía en general, consulte el pulso de la compañía.

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/crm10.png[/IMG]

[B]Para más información[/B]

Si quiere saber más detalles sobre el CRM, le recomiendo [URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=05423]curso de capacitación en línea[/URL].

No se olvide de dar me gusta en esta publicación! :)

[B]Comencemos ahora![/B]

Para comenzar, intente ingresar cierta información en el CRM sobre un cliente con el que trabaja. Esta es una herramienta fundamental para aumentar la eficiencia de ventas y mejorar el servicio al cliente!";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TITLE"] = "CRM: trabajándo con clientes";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TEXT"] = "¿Cómo estamos colaborando en documentos ahora? ¿Cómo dos o más de nuestras personas son coautores de un comunicado de prensa o un programa de capacitación? Nos enviamos correos electrónicos, de ida y vuelta, con diferentes versiones y, a veces, con grupos cambiantes de destinatarios. Es fácil desordenarse así, probablemente lo hayas notado. 

Con la unidad compartida local, hay poca forma de saber quién realizó los cambios y, lo que es más importante, no puede acceder al documento si no está en la oficina y alguien puede tener una copia nueva en su máquina que aún no ha sido cargado.

La intranet social simplifica enormemente la colaboración en documentos.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=wSqOpmqCu6M[/VIDEO]

[B]Compartir un archivo[/B]

No hay nada complicado en compartir archivos - incluso puedes crear un archivo en la intranet y compartirlo de inmediato. Para compartir un archivo con colegas para su discusión, mejora o simplemente para su información, puede comenzar simplemente comenzando una nueva publicación en el noticiases. Suba el archivo o cree uno nuevo (vea la esquina inferior derecha de la imagen a continuación).

[IMG WIDTH=500 HEIGHT=579]https://www.bitrix24.com/images/ustat/en/disk1.png[/IMG]

Todos los destinatarios de este mensaje podrán editar el documento si la casilla de verificación que se muestra arriba está marcada. Además, el documento se abrirá de inmediato en una ventana de vista previa cuando alguien haga clic en él.

[IMG WIDTH=500 HEIGHT=562]https://www.bitrix24.com/images/ustat/en/disk2.png[/IMG]

[B]Editando un documento en línea[/B]

Para editar documentos, no tiene que descargarlos, guardarlos y luego cargarlos. Todo lo que necesita hacer desde la ventana de vista previa es elegir editarlo usando Google Docs o MS Office Online. Puedes elegir cualquiera de los desplegables.

[IMG WIDTH=500 HEIGHT=331]https://www.bitrix24.com/images/ustat/en/disk3.png[/IMG]

Para previsualizar o editar documentos de esta manera, deberá iniciar sesión en Microsoft o Google. Estos son servicios gratuitos.

Ahora puede editar texto y guardar cambios.

Después de editar el documento y guardarlo, dentro de noticias se creará un nuevo comentario que indica que usted ha creado una nueva versión del documento.

[IMG WIDTH=500 HEIGHT=428]https://www.bitrix24.com/images/ustat/en/disk4.png[/IMG]

[B]Dónde guardar documentos - cómo configurar un intercambio de archivos[/B]

Para asegurarse de que sus archivos estén siempre disponibles para sus colegas, le recomendamos utilizar Bitrix24.Drive. Esta es una aplicación de sincronización de archivos que se encuentra dentro de la aplicación de escritorio Bitrix. Sus archivos locales se sincronizarán con la biblioteca de documentos de Intranet Bitrix24.

Vaya al elemento de menú Mi espacio de trabajo> Archivos e instale la aplicación de escritorio Bitrix24 en su máquina local.

[IMG WIDTH=500 HEIGHT=194]https://www.bitrix24.com/images/ustat/en/disk5.png[/IMG]

La App de escritorio se puede descargar en www.bitrix24.com:
[LIST]
[*]Para MacOS [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.dmg[/URL]
[*]Para Windows [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]
Bitrix24.Drive creará una carpeta en su administrador de archivos que puede usar como cualquier otra carpeta. Sin embargo, el contenido de esa carpeta se sincronizará con una biblioteca de archivos en Bitrix24. Si está trabajando sin conexión, los archivos se guardarán de la forma habitual y, tan pronto como se restaure la conexión a Bitrix24, el sistema se sincronizará automáticamente.

[IMG WIDTH=500 HEIGHT=397]https://www.bitrix24.com/images/ustat/en/disk6.png[/IMG]

Si guarda o edita un archivo en esta carpeta, aparecerá un archivo idéntico en su biblioteca Mi Drive. Del mismo modo, si abre el archivo desde su portal y lo edita, el archivo de su computadora se editará inmediatamente.

[IMG WIDTH=500 HEIGHT=358]https://www.bitrix24.com/images/ustat/en/disk7.png[/IMG]

[B]Cómo permitir o denegar el acceso a los archivos[/B]

La [I]Mi Drive[/I] biblioteca es su almacenamiento de archivos personal, y puede poner límites de acceso en la biblioteca en general, o en archivos individuales. El acceso se puede otorgar a cualquier subconjunto de sus compañeros de trabajo, incluidos los grupos de trabajo o departamentos.

Puede ajustar el acceso en cada documento según sea necesario y puede crear enlaces de descarga especiales para que las personas que se encuentren fuera del portal puedan descargar el documento dado. Estos enlaces de descarga pueden tener un límite de tiempo o estar protegidos por contraseñas.

[IMG WIDTH=500 HEIGHT=319]https://www.bitrix24.com/images/ustat/en/disk8.png[/IMG]

[B]Evalúa tu actividad en Bitrix24.Drive y documentos[/B]

Para evaluar qué tan activo está trabajando con documentos en relación con su departamento y la empresa en general, abra Company Pulse y compare su rango con sus compañeros de trabajo.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/disk9.png[/IMG]

[B]Para descubrir mas[/B]

La documentación relativa a las bibliotecas de documentos y al Bitrix24.Drive se puede encontrar aquí:

[LIST]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=05754]Trabajando con archivos[/URL]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5760]Trabajando con Bitrix24.Drive[/URL]
[/LIST]
El Bitrix24.Drive y el editor en línea disponible en la intranet nos permiten editar documentos y colaborar más rápida y fácilmente. Comencemos a usar estas herramientas ahora!

[B]¡Empieza ahora![/B]

Para la práctica, sugiero que todos los que lean esto abran el documento adjunto a esta publicación y agreguen su nombre al archivo. Creo que lo encontrarás más fácil de lo que esperabas!

No olvides agregar esta publicación a tus Favoritos :)";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TITLE"] = "Edición de documentos... en 60 segundos";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TEXT"] = "Todos: ¿qué herramientas usas cuando discutes problemas urgentes entre ellos? Correo, teléfono, sms, Skype, ICQ, Facebook? Tener opciones es genial, pero ¿sabe dónde encontrar la información 2 semanas después? Así es como puede funcionar en Bitrix24.

[B]Chat[/B]

[IMG WIDTH=500 HEIGHT=339]https://www.bitrix24.com/images/ustat/en/im1.png[/IMG]

Tenga en cuenta - que en nuestro sitio web tiene su propio chat, que ya incluye a todos nuestros empleados (¡y sin contactos personales!). Los chats con cada persona o en cada grupo se almacenan individualmente y hay un botón del historial donde se pueden recuperar. El chat puede ser de 1-a-1, chat en grupo, o incluso puedes hacer llamadas de voz y video desde esta interfaz!

[B]Los contactos ya están ahí[/B]

Comenzar en el chat es fácil porque no tiene que agregar a nadie: todos los empleados ya están incluidos en su lista de contactos. Cualquier colega se puede encontrar simplemente escribiendo en el campo; la pestaña Recientes muestra tus últimas conversaciones, por lo que es más fácil encontrar a las personas con las que hablas más.  

[IMG WIDTH=500 HEIGHT=339]https://www.bitrix24.com/images/ustat/en/im2.png[/IMG]

Tenga en cuenta que si alguien está en línea (es decir, disponible en este momento), el indicador al lado de su imagen será verde. Si están fuera de línea o se marcan como ocupados, será rojo.

Todo en el chat ocurre en tiempo real. Puede ver si su contraparte está escribiendo un mensaje y obtendrá la confirmación de que los mensajes han sido leídos.

[IMG WIDTH=500 HEIGHT=345]https://www.bitrix24.com/images/ustat/en/im3.png[/IMG]

[B]Grupo de chat[/B]

Si necesita hablar con varias personas a la vez, puede evitar interrumpir la jornada laboral de todos y simplemente crear un chat grupal. El chat mismo se guardará y se puede volver a abrir más tarde para que no tenga que volver a crear el mismo grupo.

[IMG WIDTH=500 HEIGHT=349]https://www.bitrix24.com/images/ustat/en/im4.png[/IMG]

[B]Historial de mensajes[/B]

Todos los chats se guardan en la intranet, y en cualquier momento puede encontrarlos fácilmente, abrir el \"historial de chat\" en la esquina superior derecha y buscar una palabra o frase. 

[IMG WIDTH=500 HEIGHT=345]https://www.bitrix24.com/images/ustat/en/im5.png[/IMG]

[B]Chat de móvil[/B]

El chat es accesible a través de la aplicación móvil, junto con otras funciones de mensajería, como notificaciones, recepción de invitaciones para reuniones y actualizaciones relacionadas con me gusta, tareas y comentarios.

La aplicación móvil Bitrix24 se puede instalar fácilmente en los smartphones y tablets de la [URL=https://itunes.apple.com/app/bitrix24/id561683423]Apple AppStore[/URL] y [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android]Google Play[/URL].

[IMG WIDTH=500 HEIGHT=461]https://www.bitrix24.com/images/ustat/en/im6.jpg[/IMG]

[B]Videollamadas[/B]

A veces necesitas más que chatear, necesitas discusión e interacción. El chat de video y audio en la misma interfaz de mensajería proporciona exactamente eso. No solo incluye hasta 4 usuarios, sino que es gratis y una excelente manera de unir a trabajadores distantes.

[IMG WIDTH=500 HEIGHT=305]https://www.bitrix24.com/images/ustat/en/im7.png[/IMG]

Puede silenciar su propio micrófono si es necesario.

El video chat es compatible con los navegadores compatibles con WebRTC, como Chrome. Si trabaja con un navegador diferente, puede usar el chat de video a través de la aplicación de escritorio.

[B]App de escritorio[/B]

Puede mantenerse en contacto incluso sin un navegador abierto, utilizando la aplicación de escritorio Bitrix24. Es compatible con todas las funciones del mensajero, incluidas las videollamadas, y tiene funciones adicionales de sincronización de archivos para archivos personales. También se encuentra en su barra de herramientas y le permite saber cuándo entran nuevos mensajes.

[IMG WIDTH=500 HEIGHT=313]https://www.bitrix24.com/images/ustat/en/im8.jpg[/IMG]

[LIST]
[*]For MacOS [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.dmg[/URL]
[*]For Windows [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]
Resumiendo lo que hemos dicho hasta ahora: tenemos un instrumento que puede reemplazar y unir todo tipo de mensajeros, llamadas telefónicas internas e incluso SMS, todos los cuales todavía se usan de una forma u otra. Usando el chat y las videollamadas en Bitrix24, podemos comunicarnos exactamente según sea necesario, dependiendo de la urgencia de la conversación.

[B]Evalúa su actividad en el messenger[/B]

Puede ver el nivel de su actividad en el messenger y compararlo con su departamento y compañía en Company Pulse.

[IMG WIDTH=500 HEIGHT=270]https://www.bitrix24.com/images/ustat/en/im9.png[/IMG]

[B]Para obtener más información[/B]

Si desea obtener más información sobre cómo trabajar con el messenger, el chat o las videollamadas, consulte la siguiente documentación:

[LIST]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5170]Chat[/URL]
[*][URL=https://www.bitrix24.com/features/mobile-and-desktop-apps.php]Aplicativos de escritorio y chat móvil[/URL]
[/LIST]
Si todavía tiene preguntas, puede contactarme por video :)

Mantenga esta publicación en sus favoritos hasta que esté familiarizado con el messenger y sus características.

Comencemos por hacer las comunicaciones más fáciles y agradables.";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TITLE"] = "Colaborar con mucho gusto!";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TEXT"] = "Amigos míos! Dejemos de ser tímidos y empecemos a hablar de todas las cosas interesantes que pasan en la compañía. Por supuesto, nuestra intranet no es exactamente una red social, pero se tiene 'likes' y es una gran manera de asegurarse de que usted sabe lo que está pasando. 

Revise los mensajes, coméntelos, mira las fotos y archivos y coloca 'like' a los que le gusten.

[IMG WIDTH=500 HEIGHT=341]https://www.bitrix24.com/images/ustat/en/likes1.png[/IMG]

[B]¿Cómo mostrar las cosas que te gustan?[/B]

Si un correo o mensaje de un colega está en el noticias, puede marcarlo como 'me Gusta' con el me gusta que el mesaje tiene debajo. Sólo tomará un segundo, pero hará saber a la persona que escribe que la contribución fue valiosa para alguien y permitirá a otros saber que podría ser útil para ellos.

[B]¿A quien le gusta qué?[/B]

Puede ver una lista de los usuarios que han puesto 'like' a un mensaje o comentario, con solo pasar el mouse sobre la estrella junto a la palabra 'like'. Estos 'likes' son importantes debido a que la función de búsqueda le dará prioridad a contenidos que más 'likes' acumulen, haciéndolos relevantes en cualquier búsqueda. 

[IMG WIDTH=500 HEIGHT=255]https://www.bitrix24.com/images/ustat/en/likes2.png[/IMG]

[B]¿Por qué existe un sistema de clasificación con 'likes'?[/B]

Búsqueda social - significa buscar resultados que están influenciados por los 'likes', ayudando a los usuarios a encontrar documentos y discusiones que pueden ser particularmente más útiles. 

[IMG WIDTH=500 HEIGHT=426]https://www.bitrix24.com/images/ustat/en/likes3.png[/IMG]

Los mensajes con más 'likes' serán mostrados en el grupo de 'mensajes populares' en la página principal.  

[B]¿Qué más le puede gustar?[/B]

No sólo mensajes y comentarios, pueden ser marcados con 'likes'. Usted puede colocarle 'likes' a tareas cuando estás son culminadas, de tal manera que motive a sus colegas a mantener un ritmo duro de trabajo. Archivos y fotos también pueden ser marcados de esta manera.

[IMG WIDTH=500 HEIGHT=322]https://www.bitrix24.com/images/ustat/en/likes4.png[/IMG]

[B]Evaluar su actividad de me gusta[/B]

Para ver como activamente usted usa los 'likes', puede usar Company Pulse y comparar su scrore con todo su departamento o toda la compañía de ser necesario. 

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/likes5.png[/IMG]

[B]Comience a usar 'likes'![/B]

Usted puede empezar a usar el botón 'like' en este mismo post. No olvide poner 'likes' a tareas y archivos que usted piense que son importantes. Aliente a sus colegas a trabajar más en equipo, el botón 'like' lo hace simple. 
No olvide colocarle 'like' a este mensaje :)";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TITLE"] = "Me gusta! Me gusta! Me gusta! ";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TEXT"] = "¡Hola a todos! A menudo tenemos que trabajar fuera de la oficina, en viajes de negocios, reuniones con clientes, ferias comerciales, etc. No siempre es posible tener su computadora portátil con usted, pero sí necesita estar en contacto con sus colegas. Por lo tanto, sugerimos usar la aplicación móvil de Bitrix24; es fácil de instalar en un smartphone o tablet en la [url=https://itunes.apple.com/app/bitrix24/id561683423]Apple AppStore[/url] o [url=https://play.google.com/store/apps/details?id=com.bitrix24.android]Google Play[/url].

[IMG]https://www.bitrix24.com/images/ustat/en/mob1.gif[/IMG]

Ahora puede obtener actualizaciones, leer el noticias, comentar publicaciones, recibir notificaciones y responder a sus colegas sin importar dónde se encuentre.

[B]Los contactos de compañeros de trabajo están siempre con usted[/B]

Tiene la información de contacto completa y siempre actualizada de todos los empleados de la compañía en la aplicación móvil, por lo que si necesita contactar a alguien rápidamente, incluso si es una persona a la que no llama con frecuencia, puede encontrarla en su teléfono tan fácilmente como en la intranet.

[IMG]https://www.bitrix24.com/images/ustat/en/mob3.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob4.png[/IMG]

Envíe un mensaje a sus colegas y aparecerá en la intranet y recibirá notificaciones sobre comentarios a través de su dispositivo móvil si se encuentra fuera de la oficina..

[IMG]https://www.bitrix24.com/images/ustat/en/mob5.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob6.png[/IMG]

[B]¿Qué puede hacer la App móvil?[/B]

La aplicación móvil le brinda acceso a todas las herramientas principales en la intranet de Bitrix24: noticias, comentarios, \"Me gusta\", calendarios con una lista de próximas reuniones, tareas, búsqueda y bibliotecas de documentos. Independientemente de si está en su oficina o no, siempre estará al tanto de lo que está pasando y podrá participar en las discusiones en tiempo real.

[IMG]https://www.bitrix24.com/images/ustat/en/mob7.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob8.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob9.png[/IMG]

Si se encuentra lejos de la oficina, aún puede establecer tareas, comentarlas y monitorear el progreso en ellas.

[IMG]https://www.bitrix24.com/images/ustat/en/mob10.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob11.png[/IMG]

[B]CRM móvil[/B]

Con toda probabilidad, la mejor parte - ¿trabajar en CRM desde la App móvil? La aplicación es completamente funcional, de modo que cuando esté en movimiento, puede obtener información sobre los clientes, realizar llamadas, consultar el catálogo de productos, modificar negociaciones e incluso crear facturas. Imagínese lo conveniente que es - cambiar el estado del cliente potencial o negociar allí mismo en la oficina del cliente!

[IMG WIDTH=500 HEIGHT=320]https://www.bitrix24.com/images/ustat/en/mob12.jpg[/IMG]

[B]Evalúe su trabajo en la aplicación móvil[/B]

Puede monitorear qué tan activamente utiliza la aplicación móvil y compararla con su compañía o departamento en el pulso de la compañía.

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/mob13.png[/IMG]

Información adicional sobre la aplicación móvil está disponible en [URL=https://www.bitrix24.com/features/mobile-and-desktop-apps.php]Bitrix24 website[/URL].

No olvide agregar esta publicación a sus Favoritos :)

Espero que con la aplicación móvil nos volvamos más eficientes y que, incluso cuando estemos fuera de la oficina, podamos mantener todo en movimiento.";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TITLE"] = "Movilizar de inmediato!";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TEXT"] = "Todos podemos ver que la tecnología se desarrolla con una velocidad deslumbrante. Las redes sociales han desplazado a otras formas de comunicación debido a su conveniencia y la simplicidad de intercambiar mensajes y medios a través de computadoras y dispositivos móviles. Entonces, usemos las mismas técnicas en nuestro lugar de trabajo.

[IMG WIDTH=500 HEIGHT=338]https://www.bitrix24.com/images/ustat/en/socnet1.png[/IMG]

[B]¿Por qué necesitamos una red social interna?[/B]

[LIST]
[*]primero, necesitamos consolidar nuestros datos, documentos, conocimientos y contactos en un solo sistema;
[*]esto simplificará y acelerará las operaciones regulares (es mucho más rápido tener un grupo grande que pondere un tema utilizando un hilo de discusión en lugar de un correo electrónico masivo);
[*]ya no perderá mensajes importantes;
[*]puede ofrecer ideas, compartir opiniones, publicar información útil; sus mensajes serán vistos por todos (o solo por aquellos que usted coloca en el [I]Para:[/I] campo);
[*]y, por último, puede darle 'me gusta' publicar y aumentar la moral :)
[/LIST]

Sugiero iniciar conociendo la funcionalidad de nuestra intranet social en este momento. Le diré cómo hacerlo.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=fN6BsyjOOtY[/VIDEO]

[B]¿Cómo enviar un mensaje?[/B]

En nuestra intranet puede compartir cualquier información relevante para el trabajo. Los mensajes se pueden enviar a todos los empleados, a un grupo individual o a un solo usuario. Los destinatarios verán su mensaje en general [I]Noticias[/I].

[IMG WIDTH=500 HEIGHT=313]https://www.bitrix24.com/images/ustat/en/socnet2.png[/IMG]

Puede adjuntar archivos, imágenes, agregar enlaces, etiquetas e incluso videos a sus mensajes, y puede mencionar a colegas individuales:

[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/socnet3.png[/IMG]

[B]¿Cómo crear o ingresar a un Grupo?[/B]

Los grupos de trabajo son una herramienta útil para proyectos. Para crear su propio grupo e invitar miembros a él, use el botón 'Agregar' (el botón verde en la esquina superior izquierda). Para unirse a un grupo existente, puede abrirlo desde la sección de menú [I]Grupos de trabajo[/I]  y hacer clic en la opción para unirse al grupo (según corresponda, algunos grupos son privados).

[B]Mostrar agradecimiento[/B]

No olvide llamar la atención sobre los logros de sus colegas, empleados e incluso ejecutivos: haga clic en \"Más\" en el menú de mensajes y seleccione \"Apreciación\", seleccione a quién quiere hacer honor públicamente y seleccione el icono apropiado debajo del texto del mensaje.

[IMG WIDTH=500 HEIGHT=350]https://www.bitrix24.com/images/ustat/en/socnet4.png[/IMG]

[B]Evaluar Bitrix24 como una herramienta interna[/B]

Además, puede verificar qué tan bien ha aprendido su equipo a utilizar las distintas herramientas dentro de Bitrix24; sólo use la función pulso de la compañía y observe la actividad.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/socnet5.png[/IMG]

Pulso de la compañía lo ayuda a mantener un seguimiento preciso de como su red social se va integrando a las actividades cotidianas de su compañía. Cada empleado tiene su propio score, el mismo que puede ser comparado con otros colegas de su departamento o de toda la compañía. 

[B]Si quiere saber más[/B]

Si desea obtener más información acerca de las capacidades de la intranet, lo invitamos a mirar el [url=http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5179]training course[/url].

[B]¡Iniciar ahora![/B]

Podemos comenzar ahora discutiendo este mensaje - publicar un comentario, darme un \"me gusta\" -  Me gustará eso ;) y no olvide agregar esta publicación a sus favoritos ;)

";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unidad de la fuerza de trabajo";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TEXT"] = "Mire detenidamente su escritorio: ¿está seguro de que no tiene ningún número al lado de su lista de tareas? ;) O cualquier nota en el diario o recordatorios en el calendario, o tal vez banderas en el buzón? ¿Cómo puede evitar olvidarse de todas las tareas y hacer que todo termine?

Hoy los invito a probar una nueva herramienta: Tareas en Bitrix24 esta herramienta proporciona una manera rápida y fácil de organizar las tareas para usted o sus colegas. Recuerde, lo más importante es que Bitrix24 enviará recordatorios sobre las tareas para asegurarse de que se realicen a tiempo. 

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=WyDI_eZzRb4[/VIDEO]

[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/task1.png[/IMG]

Si alguien todavía no está familiarizado con las Tareas, así es como funcionan: 

[B]¿Cómo puedo crear una tarea para mí o para otra persona?[/B]

Para crear una tarea, haga clic en Agregar > Tareas y en la ventana que aparece, poner una descripción de la tarea, asignarla a una persona responsable y establecer una fecha límite. Si es necesario, seleccione participantes adicionales y agregue archivos. Guarde los cambios. Si corresponde, coloque la tarea en un grupo de trabajo para que un equipo de proyecto pueda verla.

[IMG WIDTH=500 HEIGHT=359]https://www.bitrix24.com/images/ustat/en/task2.png[/IMG]

[B]Cómo colaborar usando tareas[/B]

Discuta las soluciones y los detalles de las tareas con sus colegas; simplemente agregue un comentario a una tarea y se agregará a la secuencia de actividad, y otros pueden reaccionar instantáneamente. Es mucho más fácil y más rápido que una discusión por correo electrónico o una reunión que consume mucho tiempo. Además, toda la historia de la tarea siempre estará a su alcance: los comentarios y los archivos se almacenan en la tarea.

[IMG WIDTH=500 HEIGHT=398]https://www.bitrix24.com/images/ustat/en/task3.png[/IMG]

[B]Cómo mantener los plazos de las tareas[/B]

Para asegurarse de que no se pierdan los plazos, Bitrix24 incluye un contador que le indica qué necesita su atención más inmediata. Está disponible en las pestañas 'Mis Tareas' y en 'Tareas' en su perfil personal.  

[IMG WIDTH=500 HEIGHT=340]https://www.bitrix24.com/images/ustat/en/task4.png[/IMG]

El contador muestra tareas incompletas: tareas vencidas, tareas que aún no se han analizado y aquellas que no tienen una fecha límite establecida. Intente mantener el número de contador lo más bajo posible: establezca plazos razonables, muévalo si es necesario y cierre todas las tareas cuando se hayan completado. Cuanto más se ponga a sistematizar sus tareas, más significativo será el número en el mostrador, y si es cero, sabrá que lo está haciendo realmente bien :)

[B]Cómo hacer un seguimiento del tiempo dedicado a las tareas[/B]

Monitorear el tiempo dedicado a las tareas es fácil: simplemente agregue la tarea a su plan diario y cuando comience a trabajar en ella, mueva el estado de la tarea de manera apropiada. También puede agregar el tiempo gastado en el formulario de tareas en sí mismo.

Cuando elige la opción \"tiempo de seguimiento utilizado\" cuando está creando una tarea, se realizará un seguimiento de la duración de la misma.

[IMG WIDTH=500 HEIGHT=446]https://www.bitrix24.com/images/ustat/en/task5.png[/IMG]

Ahora, cuando haces clic en \"Iniciar ejecución\" en la tarea, comienza el seguimiento del tiempo. 

[IMG]https://www.bitrix24.com/images/ustat/en/task6.png[/IMG]

El tiempo se cuenta hasta que presione el botón de pausa (o termine la tarea). Si hace clic en pausa, el seguimiento del tiempo se detiene y puede tomar un descanso o detener su jornada laboral.

[IMG WIDTH=500 HEIGHT=399]https://www.bitrix24.com/images/ustat/en/task7.png[/IMG]

[B]Si quieres saber más[/B]

Si desea conocer más detalles sobre esta herramienta, le recomiendo [URL=http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=04690]curso de capacitación en línea[/URL], que está especialmente diseñado para usuarios del servicio en la nube de Bitrix24.

[B]Evaluar tu trabajo con tareas[/B]

The Company Pulse muestra qué tan bien usted y/o sus colegas han integrado las tareas en su rutina diaria:

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/task8.png[/IMG]

Sus ideas, comentarios e impresiones son bienvenidos - solo deje un comentario a este mensaje. 

Buena suerte usando las tareas - no se olvide de darle me gusta a este mensaje :)

P.D. Interesante - cuál será tu PRIMERA tarea en Bitrix24?";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TITLE"] = "¡Tareas y asignación de tareas!";
