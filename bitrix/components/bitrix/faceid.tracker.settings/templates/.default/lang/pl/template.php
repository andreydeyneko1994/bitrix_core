<?
$MESS["FACEID_FTS_AUTO_LEAD_SETTING"] = "Jeżeli odwiedzający nie został odnaleziony w CRM";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_A"] = "utwórz automatycznie nowy lead";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_M"] = "utwórz ręcznie";
$MESS["FACEID_FTS_CONFIG_EDIT_SAVE"] = "Zapisz";
$MESS["FACEID_FTS_DELETE_ALL"] = "Usuń wszystkich gości z aplikacji Śledzenie Twarzy";
$MESS["FACEID_FTS_DELETE_ALL_CONFIRM"] = "Czy na pewność chcesz wyczyścić historię i usunąć wszystkich gości?";
$MESS["FACEID_FTS_LEAD_SOURCE_SETTING"] = "Źródło nowego leada";
$MESS["FACEID_FTS_SOCNET_ENABLED"] = "Włącz przeszukiwanie sieci społecznościowej";
$MESS["FACEID_FTS_TO_FTRACKER"] = "Uruchom funkcję śledzenia twarzy";
?>