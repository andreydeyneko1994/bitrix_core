<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_SUBTITLE"] = "Informações de envio";
$MESS["CRM_ORDER_SHIPMENT_DOCUMENT_TITLE"] = "Documento de envio";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_DATE"] = "Data do documento de envio";
$MESS["CRM_ORDER_SHIPMENT_FIELD_DELIVERY_DOC_NUM"] = "Documento de envio No.";
$MESS["CRM_ORDER_SHIPMENT_FIELD_TRACKING_NUMBER"] = "Número de rastreamento";
?>