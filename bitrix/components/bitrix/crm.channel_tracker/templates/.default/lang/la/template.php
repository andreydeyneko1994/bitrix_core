<?
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_1"] = "Usted puede volver a ver el video haciendo clic en este ícono.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_2"] = "Conecte más canales de comunicación a su Bitrix24 CRM: Telefonía, E-mail, Canales Abiertos, Formularios de CRM.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_EMPLOYEE"] = "Use su contador personal para medir su rendimiento: el CRM realiza un seguimiento de sus tareas completadas y restantes para hoy. Su objetivo es no tener tareas sin terminar al final del día.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_SUPERVISOR"] = "Use contadores para ver el progreso actual de sus empleados: tareas completadas y restantes para hoy. Sus empleados funcionan bien si no hay tareas restantes al final del día.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_4"] = "Usted puede habilitar la vista de demostración en cualquier momento.";
$MESS["CRM_CH_TRACKER_WGT_DEMO_TITLE"] = "Esta es una vista demo. Ocultarlo para analizar sus datos.";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET"] = "Objetivo de Ventas";
?>