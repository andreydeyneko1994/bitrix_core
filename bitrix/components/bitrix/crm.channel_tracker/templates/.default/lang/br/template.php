<?
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_1"] = "Você pode ver o vídeo novamente ao clicar neste ícone.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_2"] = "Conecte mais canais de comunicação ao seu CRM Bitrix24: Telefonia, E-mail, Canais Abertos, Formulários de CRM.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_EMPLOYEE"] = "Use seu contador pessoal para avaliar seu desempenho: o CRM mantém controle de suas tarefas concluídas e restantes de hoje. Seu objetivo é não ter nenhuma tarefa inacabada no final do dia.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_SUPERVISOR"] = "Use contadores para visualizar o progresso atual dos seus funcionários: tarefas concluídas e restantes de hoje. Os seus funcionários trabalharam bem se não houver nenhuma tarefa restante no final do dia.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_4"] = "Você pode habilitar a exibição de demo a qualquer momento.";
$MESS["CRM_CH_TRACKER_WGT_DEMO_TITLE"] = "Esta é uma exibição de demonstração. Ocultar para analisar seus dados.";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET"] = "Meta de Vendas";
?>