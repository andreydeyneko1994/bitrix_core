<?
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_1"] = "Vous pouvez regarder la vidéo une nouvelle fois en cliquant sur cette icône.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_2"] = "Connectez plus de canaux de communication à votre CRM Bitrix24 : Téléphonie, E-mail, Canaux Ouverts, Formulaires CRM.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_EMPLOYEE"] = "Utilisez votre compteur personnel afin d'évaluer votre performance : le CRM effectue le suivi de vos tâches terminées et celles qui restent à finir pour aujourd'hui. Votre objectif est de n'avoir aucune tâche à finir à la fin de la journée.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_SUPERVISOR"] = "Utilisez les compteurs pour voir le progrès actuel de vos employés : tâches terminées et celles qui restent à finir pour aujourd'hui. Vos employés sont performants s'il ne reste aucune tâche à finir à la fin de la journée.";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_4"] = "Vous pouvez activer l'affichage démo à tout moment.";
$MESS["CRM_CH_TRACKER_WGT_DEMO_TITLE"] = "Ceci est un affichage démo. Masquez-le pour analyser vos données.";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET"] = "Objectif des ventes";
?>