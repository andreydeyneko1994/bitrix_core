<?
$MESS["INTDT_ACTIONS"] = "Działania";
$MESS["INTDT_ALL_TASKS"] = "Wszystkie zadania";
$MESS["INTDT_ASSIGNED_TASKS"] = "Zadania przypisane do mnie";
$MESS["INTDT_CREATED_TASKS"] = "Zadania utworzone przeze mnie";
$MESS["INTDT_DC_FOLDER"] = "Kliknij podwójnie aby otworzyć folder";
$MESS["INTDT_DC_TASK"] = "Kliknij podwójnie aby zobaczyć szczegóły zadania";
$MESS["INTDT_DC_UP"] = "Kliknij dwukrotnie aby wybrać wyższy poziom";
$MESS["INTDT_NO_TASKS"] = "Nie znaleziono zadania.";
$MESS["INTDT_PERSONAL_TASKS"] = "Osobiste zadania";
$MESS["INTDT_SHOW"] = "Pokaż";
?>