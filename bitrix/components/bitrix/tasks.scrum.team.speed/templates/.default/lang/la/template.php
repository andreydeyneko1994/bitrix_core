<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_DONE_COLUMN"] = "Completado";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_NOT_DATA_LABEL"] = "No hay elementos que hayan ocurrido en el periodo seleccionado.";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_PLAN_COLUMN"] = "Planeado";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_AVERAGE_LABEL"] = "Promedio de puntos de historia completados:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MAX_LABEL"] = "Máximo de puntos de historia completados:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MIN_LABEL"] = "Mínimo de puntos de historia completados:";
