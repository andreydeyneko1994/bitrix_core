<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_DONE_COLUMN"] = "Concluído";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_NOT_DATA_LABEL"] = "Não há itens que ocorreram no período selecionado.";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_PLAN_COLUMN"] = "Planejado";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_AVERAGE_LABEL"] = "Média de story points concluídos:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MAX_LABEL"] = "Máximo de story points concluídos:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MIN_LABEL"] = "Mínimo de story points concluídos:";
