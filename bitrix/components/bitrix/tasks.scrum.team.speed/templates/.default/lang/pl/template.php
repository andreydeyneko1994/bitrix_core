<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_DONE_COLUMN"] = "Zakończone";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_NOT_DATA_LABEL"] = "Brak pozycji, które miały miejsce w wybranym okresie.";
$MESS["TASKS_SCRUM_TEAM_SPEED_CHART_PLAN_COLUMN"] = "Zaplanowane";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_AVERAGE_LABEL"] = "Średnia liczba zakończonych story pointów:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MAX_LABEL"] = "Maksymalna liczba zakończonych story pointów:";
$MESS["TASKS_SCRUM_TEAM_SPEED_STATS_MIN_LABEL"] = "Minimalna liczba zakończonych story pointów:";
