<?
$MESS["INTL_IBLOCK"] = "Blok informacji";
$MESS["INTL_IBLOCK_TYPE"] = "Typ Bloku Informacji";
$MESS["INTL_MEETING_ID"] = "ID zasobu";
$MESS["INTL_MEETING_VAR"] = "Zmienne dla ID zasobu";
$MESS["INTL_PAGE_ID"] = "Nazwa strony";
$MESS["INTL_PAGE_VAR"] = "Zmienna strony";
$MESS["INTL_PATH_TO_MEETING"] = "Strona harmonogramu zasobu";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Strona główna rezerwacji zasobu";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Strona edycji parametrów zasobu";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Strona rezerwacji zasobów";
$MESS["INTL_PATH_TO_SEARCH"] = "Strona wyszukiwania zasobów";
$MESS["INTL_SET_NAVCHAIN"] = "Ustaw Ścieżkę Nawigacji";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupy użytkowników mogące anulować rezerwację zasobu";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupy użytkowników mogące edytować grafik zasobu";
$MESS["INTL_USERGROUPS_RESERVE"] = "Grupy użytkowników mogące rezerwować zasób";
$MESS["INTL_VARIABLE_ALIASES"] = "Zmienne aliasów";
?>