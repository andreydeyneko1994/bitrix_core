<?
$MESS["CHAPTER_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony rozdziałów kursu.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony szczegółów kursu.";
$MESS["COURSE_ID_TIP"] = "Zaznacz tutaj jeden z istniejących kursów. Jeżeli wybierzesz <b><i>(inne)</i></b>, musisz sprecyzować ID kursu w polu obok.";
$MESS["LESSON_DETAIL_TEMPLATE_TIP"] = "Ścieżka do strony lekcji kursu.";
$MESS["SELF_TEST_TEMPLATE_TIP"] = "Ścieżka do strony samosprawdzalnego testu.";
$MESS["SET_TITLE_TIP"] = "Sprawdzenie tej opcji ustawi stronę tytułową do nazwy rozdziału kursu.";
$MESS["TESTS_LIST_TEMPLATE_TIP"] = "Ścieżka do strony wyświetlającej wszystkie testy kursu.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Ścieżka do głównej strony testów.";
?>