<?php
$MESS["EC_IBLOCK_ID_EMPTY"] = "Żaden blok informacji nie jest zaznaczony.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Moduł Bloków Informacji nie jest zainstalowany.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Moduł intranetowy nie jest zainstalowany.";
$MESS["INAF_F_DESCRIPTION"] = "Opis";
$MESS["INAF_F_FLOOR"] = "Piętro";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Tytuł";
$MESS["INAF_F_PHONE"] = "Telefon";
$MESS["INAF_F_PLACE"] = "Miejsca siedzące";
$MESS["INAF_MEETING_NOT_FOUND"] = "Nie znaleziono zasobu.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Grafik zasobu";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Rezerwacja zasobu";
$MESS["INTR_IRMM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Nie masz uprawnień do wyświetlania bloku informacji zasobów.";
