<?
$MESS["INTASK_C25T_CLEAR"] = "Anuluj rezerwację";
$MESS["INTASK_C25T_CLEAR_CONF"] = "Rezerwacja zostanie anlowana! Kontynuować?";
$MESS["INTASK_C25T_D1"] = "Poniedziałek";
$MESS["INTASK_C25T_D2"] = "Wtorek";
$MESS["INTASK_C25T_D3"] = "Środa";
$MESS["INTASK_C25T_D4"] = "Czwartek";
$MESS["INTASK_C25T_D5"] = "Piątek";
$MESS["INTASK_C25T_D6"] = "Sobota";
$MESS["INTASK_C25T_D7"] = "Niedziela";
$MESS["INTASK_C25T_DBL_CLICK"] = "Kliknij dwukrotnie, aby zarezerwować zasób";
$MESS["INTASK_C25T_EDIT"] = "Edytuj";
$MESS["INTASK_C25T_FLOOR"] = "Piętro";
$MESS["INTASK_C25T_NEXT_WEEK"] = "Następny tydzień";
$MESS["INTASK_C25T_PHONE"] = "Telefon";
$MESS["INTASK_C25T_PLACE"] = "Miejsca siedzące";
$MESS["INTASK_C25T_PRIOR_WEEK"] = "Poprzedni tydzień";
$MESS["INTASK_C25T_RESERVED_BY"] = "Zarezerwowane przez";
$MESS["INTASK_C25T_SET"] = "ustaw";
$MESS["INTASK_C25T_TIME"] = "Czas";
?>