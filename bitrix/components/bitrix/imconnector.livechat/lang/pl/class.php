<?
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "Moduł \"Zewnętrzne Konektory IM\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "Moduł \"Otwarty Kanał\" nie jest zainstalowany.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Ten konektor jest wyłączony.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_OK_SAVE"] = "Informacja została zapisana.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Twoja sesja wygasła. Proszę prześlij formularz ponownie.";
?>