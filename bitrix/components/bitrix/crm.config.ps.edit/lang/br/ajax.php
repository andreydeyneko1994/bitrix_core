<?
$MESS["CRM_PS_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_PS_ALREADY_CONFIGURED"] = "A chave privada já existe.";
$MESS["CRM_PS_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PS_NOT_CONFIGURED"] = "Por favor, configure o manipulador antes de tentar gerar uma chave privada.";
$MESS["CRM_PS_NOT_SUPPORTED"] = "A solicitação de certificado pode ser feita apenas na versão baseada em nuvem.";
?>