<?
$MESS["CRM_PS_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["CRM_PS_ALREADY_CONFIGURED"] = "Klucz prywatny już istnieje.";
$MESS["CRM_PS_MODULE_NOT_INSTALLED"] = "Moduł CRM nie jest zainstalowany.";
$MESS["CRM_PS_NOT_CONFIGURED"] = "Przed próbą wygenerowania klucza prywatnego skonfiguruj moduł obsługi.";
$MESS["CRM_PS_NOT_SUPPORTED"] = "Żądanie certyfikatu można wykonać tylko w wersji w chmurze.";
?>