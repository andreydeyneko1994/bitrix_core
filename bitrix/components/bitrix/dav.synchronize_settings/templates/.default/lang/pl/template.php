<?
$MESS["DAV_ACCOUNTS_EXPORT_DEPARTMENT"] = "Dział";
$MESS["DAV_ACCOUNTS_SECTION"] = "Użytkownicy systemowi";
$MESS["DAV_BUTTON_SAVE"] = "Zapisz";
$MESS["DAV_CARDDAV_SETTINGS_HELP"] = "Moduł DAV zapewnia narzędzia pozwalające zsynchronizować kontakty ze wszystkimi urządzeniami i aplikacjami wspierającymi protokół CardDAV. Dla przykładu, iPhone i iPad w pełni wspierają CardDAV.
<br><br>

<h3>Włączanie Połąćzenia CardDAV na Twoim urządzeniu Apple</h3>

By włączyć wsparcie dla CardDAV na urządzeniach z Apple, postępuj według następujących kroków:
<ol>
<li>Włącz <b>Ustawienia</b> na Twoim urządzeniu Apple i kliknij zakładkę <b>Konta i hasła</b></li>
<li>Naciśnij <b>Dodaj Konto</b>.</li>
<li>Wybierz <b>CardDa</b> jako typ konta</li>
<li>W preferencjach, wpisz adres strony (#SERVER#)  i wpisz login oraz hasło.</li>
<li>WYbierz <b>Podstawowa autoryzacja</b> jako rodzaj autoryzacji.</li>
<li>Aby wybrać opcjonalny numer portu, zapisz konto, a potem otwórz je celem edycji.</li>
</ol>

Twoje wszystkie kontakty pojawią się z aplikacji Contacts. Aby włączyć lub wyłączyć synchronizację wybranych kontaktów, użyj następujących ustawień.<br>";
$MESS["DAV_COMMON_SECTION"] = "Ustawienia Ogólne";
$MESS["DAV_COMPANIES"] = "Firmy CRM";
$MESS["DAV_CONTACTS"] = "Kontakty CRM";
$MESS["DAV_DEFAULT_COLLECTION_TO_SYNC"] = "Domyślnie synchronizuj";
$MESS["DAV_ENABLE"] = "Włącz";
$MESS["DAV_EXPORT_FILTER"] = "Filtr";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Użytkownicy Ekstranetu";
$MESS["DAV_MAX_COUNT"] = "Limit";
$MESS["main_app_pass_comment"] = "Skomentuj";
$MESS["main_app_pass_comment_ph"] = "Dodaj opis";
$MESS["main_app_pass_create_pass"] = "Utwórz hasło";
$MESS["main_app_pass_create_pass_close"] = "Anuluj";
$MESS["main_app_pass_create_pass_text"] = "Użyj hasła do zsynchronizowania z wybraną aplikacją. Hasło nie będzie trzymane w otwartym formularzu; dlatego jest dostępne tylko w momencie uzyskiwania go przez użytkownika. 
Nie zamykaj okna dopóki nie wprowadzisz lub nie skopiujesz hasła.";
$MESS["main_app_pass_created"] = "Utworzono";
$MESS["main_app_pass_del"] = "Usuń";
$MESS["main_app_pass_del_pass"] = "Na pewno chcesz usunąc hasło?";
$MESS["main_app_pass_del_pass_text"] = "Synchronizacja zostanie przerwana, ponieważ aplikacja nie będzie wsytanie uzyskać dostępu do danych ze względu na błąd uwierzytelnienia.";
$MESS["main_app_pass_get_pass"] = "Pozyskaj Hasło";
$MESS["main_app_pass_last"] = "Ostatnie logowanie";
$MESS["main_app_pass_last_ip"] = "Ostatnie IP";
$MESS["main_app_pass_link"] = "Połącz";
$MESS["main_app_pass_manage"] = "Zarządzaj";
$MESS["main_app_pass_other"] = "Inne";
$MESS["main_app_pass_text1"] = "Aby bezpiecznie zsynchronizować twoje dane z zewnętrznymi aplikacjami, użyj specjalnego hasła, które możesz uzyskać na tej stronie.";
$MESS["main_app_pass_text2"] = "Każde z tych narzędzi obsługuje aplikacje wspomagające synchronizację. Wybierz aplikację, dla której chcesz skonfigurować transfer danych.";
$MESS["main_app_pass_title"] = "Co to jest?";
?>