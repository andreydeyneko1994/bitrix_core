<?
$MESS["DOCGEN_DOCUMENTS_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir este documento?";
$MESS["DOCGEN_DOCUMENTS_DELETE_ERROR"] = "Erro ao excluir documento: ";
$MESS["DOCGEN_DOCUMENTS_DELETE_SUCCESS"] = "O documento foi excluído";
?>