<?php
$MESS["TIMEMAN_DURATION_TITLE"] = "Durée";
$MESS["TIMEMAN_EDIT_BREAK_LENGTH_TITLE"] = "Durée de la pause";
$MESS["TIMEMAN_EDIT_CLOCK_SET_CUSTOM_DATE"] = "Changer de jour";
$MESS["TIMEMAN_EDIT_REASON_TITLE"] = "Motif de la modification";
$MESS["TIMEMAN_POPUP_WORK_TIME_END_TITLE"] = "Pointage en partant";
$MESS["TIMEMAN_POPUP_WORK_TIME_START_TITLE"] = "Pointage en arrivant";
