<?
$MESS["M_CRM_PRODUCT_ADD"] = "Utwórz produkt";
$MESS["M_CRM_PRODUCT_LIST_BUTTON_UP"] = "W górę";
$MESS["M_CRM_PRODUCT_LIST_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_PRODUCT_LIST_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_PRODUCT_LIST_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_PRODUCT_LIST_ROOT_SECTION_LEGEND"] = "Kategoria główna";
$MESS["M_CRM_PRODUCT_LIST_SEARCH_BUTTON"] = "Szukaj";
$MESS["M_CRM_PRODUCT_LIST_SEARCH_PLACEHOLDER"] = "Wyszukaj przez nazwę";
$MESS["M_CRM_PRODUCT_LIST_SELECTOR_LEGEND"] = "Wybierz produkt";
$MESS["M_CRM_PRODUCT_LIST_SHOW_SECTION"] = "Wszystkie produkty w kategorii";
$MESS["M_CRM_PRODUCT_LIST_TAB_PRODUCT"] = "Produkty";
$MESS["M_CRM_PRODUCT_LIST_TAB_SECTION"] = "Kategorie";
$MESS["M_CRM_PRODUCT_LIST_TITLE"] = "Wszystkie produkty";
?>