<?
$MESS["MB_CALENDAR_HELP_GO_TO_LIST_BUT"] = "Nie dziękuję, przenieś mnie tylko do kalendarza Bitrix24";
$MESS["MB_CALENDAR_HELP_HELP_NAME"] = "Pomoc";
$MESS["MB_CALENDAR_HELP_HOW"] = "Tak, pokaż mi jak!";
$MESS["MB_CALENDAR_HELP_PHRASE_2"] = "Optymalizuj swój kalendarz dla wygody!";
$MESS["MB_CALENDAR_HELP_PHRASE_3"] = "<p>Pracuj w znanym środowisku: wyświetl wszystkie wydarzenia kalendarza Bitrix24 w swoim kalendarzu iOS.</p><p>Twórz wydarzenia w którymkolwiek kalendarzu, a pojawią się w obu.</p>";
$MESS["MB_CALENDAR_HELP_TITLE"] = "Synchronizuj swoje kalendarze!";
$MESS["MB_CALENDAR_HELP_WEEK_DAY"] = "wt.";
?>