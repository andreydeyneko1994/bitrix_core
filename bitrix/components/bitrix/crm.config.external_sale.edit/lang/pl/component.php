<?
$MESS["BPABL_PAGE_TITLE"] = "Kontakty sklepu";
$MESS["BPWC_WLC_WRONG_BP"] = "Nie znaleziono rekordu.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Pole 'Login' jest wymagane.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Pole 'Hasło' jest wymagane.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Pole 'URL' jest wymagane.";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Twoja edycja nie pozwala na dodawanie nowych sklepów internetowych.";
$MESS["CRM_PERMISSION_DENIED"] = "Nie masz uprawnień dostępu do tego formularza.";
?>