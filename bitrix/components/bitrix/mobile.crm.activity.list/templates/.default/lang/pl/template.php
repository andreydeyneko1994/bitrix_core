<?
$MESS["M_CRM_ACTIVITY_LIST_CREATE_CALL"] = "Nowe połączenie telefoniczne";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_EMAIL"] = "Nowy e-mail";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_MEETING"] = "Nowe spotkanie";
$MESS["M_CRM_ACTIVITY_LIST_DOWN_TEXT"] = "Zwolnij, aby odświeżyć";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_CUSTOM"] = "Wyniki wyszukiwania";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_NONE"] = "Wszystkie działania";
$MESS["M_CRM_ACTIVITY_LIST_IMPORTANT"] = "Ważne";
$MESS["M_CRM_ACTIVITY_LIST_LOAD_TEXT"] = "Aktualizowanie…";
$MESS["M_CRM_ACTIVITY_LIST_NOTHING_FOUND"] = "Nie znaleziono aktywności.";
$MESS["M_CRM_ACTIVITY_LIST_PULL_TEXT"] = "Przeciągnij, aby odświeżyć";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_FILTER_NONE"] = "Wszystkie";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_LEGEND"] = "(Działania)";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_BUTTON"] = "Szukaj";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_PLACEHOLDER"] = "Wyszukaj przez nazwę";
$MESS["M_CRM_ACTIVITY_LIST_TIME_NOT_DEFINED"] = "[nie ustawiony]";
?>