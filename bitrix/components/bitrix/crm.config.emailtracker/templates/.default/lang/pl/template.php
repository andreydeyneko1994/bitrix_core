<?
$MESS["INTR_MAIL_AFTER_CONNECT"] = "Po połączeniu";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Błąd podczas przetwarzania wniosku.";
$MESS["INTR_MAIL_CHECK"] = "Sprawdź";
$MESS["INTR_MAIL_CHECK_ERROR"] = "Błąd";
$MESS["INTR_MAIL_CHECK_JUST_NOW"] = "właśnie sprawdzone";
$MESS["INTR_MAIL_CHECK_SUCCESS"] = "Wygrane deale";
$MESS["INTR_MAIL_CHECK_TEXT"] = "Ostatnio sprawdzane: #DATE#";
$MESS["INTR_MAIL_CHECK_TEXT_NA"] = "Brak dostępnych danych statusu skrzynki pocztowej";
$MESS["INTR_MAIL_CRM_BLACKLIST"] = "Czarna lista";
$MESS["INTR_MAIL_CRM_BLACKLIST_HINT"] = "E-maile i domeny, do odrzucenia";
$MESS["INTR_MAIL_CRM_BLACKLIST_PROMPT"] = "oddzielaj e-maile i domeny przecinkiem lub znakiem nowego wiersza";
$MESS["INTR_MAIL_CRM_NEW_LEAD_FOR_HINT"] = "Nowe e-maile otrzymane z tych adresów doprowadzą do automatycznego stworzenia nowych leadów w CRM";
$MESS["INTR_MAIL_CRM_NEW_LEAD_FOR_PROMPT"] = "Adresy e-mail należy oddzielić przecinkami bądź należy wprowadzić jeden adres w każdej linijce";
$MESS["INTR_MAIL_CRM_OPTIONS"] = "Przetwarzanie wiadomości e-mail CRM";
$MESS["INTR_MAIL_CRM_PRECONNECT"] = "Ukończ podłączanie swojego e-maila do CRM";
$MESS["INTR_MAIL_CRM_RESP_ADD"] = "Dodaj więcej";
$MESS["INTR_MAIL_CRM_RESP_SET"] = "Wybierz osoby odpowiedzialne";
$MESS["INTR_MAIL_CRM_SYNC_AGE_3"] = "3 dni";
$MESS["INTR_MAIL_CRM_SYNC_AGE_I"] = "całego czasu";
$MESS["INTR_MAIL_CRM_SYNC_OLD"] = "Pobierz dane dla";
$MESS["INTR_MAIL_ERROR_EXT"] = "Szczegóły";
$MESS["INTR_MAIL_FORM_ERROR"] = "Błąd przetwarzania formularza.";
$MESS["INTR_MAIL_IMAP_DIRS_DESCR_2"] = "Wybierz foldery, które chcesz powiązać z CRM. Wiadomość z nieznanego kontaktu automatycznie utworzy nowy lead w systemie. Wiadomość ze znanego źródła (lub odpowiedź na nią) zostanie dołączona do danych klienta jako nowa aktywność.";
$MESS["INTR_MAIL_IMAP_DIRS_ERROR"] = "Wybierz foldery do synchronizacji";
$MESS["INTR_MAIL_IMAP_DIRS_IN"] = "Odbierz wiadomości z przychodzące folderów";
$MESS["INTR_MAIL_IMAP_DIRS_LIST"] = "Przetwarzanie folderów";
$MESS["INTR_MAIL_IMAP_DIRS_LOADER"] = "Ładowanie folderów...";
$MESS["INTR_MAIL_IMAP_DIRS_MORE"] = "Pokaż wszystkie";
$MESS["INTR_MAIL_IMAP_DIRS_OUT_2"] = "Wybierz wysłane wiadomości z folderu";
$MESS["INTR_MAIL_IMAP_DIRS_SAVE"] = "Zapisz";
$MESS["INTR_MAIL_IMAP_DIRS_TITLE"] = "Preferencje przetwarzania e-mail";
$MESS["INTR_MAIL_INP_CANCEL"] = "Anuluj";
$MESS["INTR_MAIL_INP_CHECK_INTERVAL"] = "Sprawdź nowe wiadomości e-mail co";
$MESS["INTR_MAIL_INP_CRM_ENTITY_RESP"] = "Kolejka dystrybucji kontaktów i leadów";
$MESS["INTR_MAIL_INP_CRM_ENTITY_SOURCE"] = "Lead i źródło kontaktów";
$MESS["INTR_MAIL_INP_CRM_LEAD_RESP"] = "Zarządzanie leadami";
$MESS["INTR_MAIL_INP_CRM_LEAD_SOURCE"] = "Źródło leada";
$MESS["INTR_MAIL_INP_CRM_NEW_CONTACT"] = "Twórz kontakty za pomocą załączonej vCard";
$MESS["INTR_MAIL_INP_CRM_NEW_ENTITY_IN"] = "Dla przychodzących wiadomości z nowego adresu e-mail, utwórz #ENTITY#";
$MESS["INTR_MAIL_INP_CRM_NEW_ENTITY_OUT"] = "Dla wychodzących wiadomości na nowy adres e-mail, utwórz #ENTITY#";
$MESS["INTR_MAIL_INP_CRM_NEW_LEAD"] = "Utwórz lead dla nowego adresu e-mail";
$MESS["INTR_MAIL_INP_CRM_NEW_LEAD_ALLWAYS"] = "Utwórz nowy lead dla każdej nowej wiadomości z listy #LIST#";
$MESS["INTR_MAIL_INP_CRM_NEW_LEAD_ALLWAYS_LIST"] = "adresy";
$MESS["INTR_MAIL_INP_EDIT_SAVE"] = "Zapisz";
$MESS["INTR_MAIL_INP_EMAIL"] = "E-mail";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Nieprawidłowy adres e-mail";
$MESS["INTR_MAIL_INP_EMAIL_EMPTY"] = "wpisz e-mail";
$MESS["INTR_MAIL_INP_ENCRYPT"] = "Zabezpiecz połączenie";
$MESS["INTR_MAIL_INP_ENCRYPT_NO"] = "Nie";
$MESS["INTR_MAIL_INP_ENCRYPT_SKIP"] = "Tak (nie sprawdzaj certyfikatu)";
$MESS["INTR_MAIL_INP_ENCRYPT_YES"] = "Tak";
$MESS["INTR_MAIL_INP_LOGIN"] = "Login";
$MESS["INTR_MAIL_INP_LOGIN_EMPTY"] = "wprowadź login";
$MESS["INTR_MAIL_INP_NAME"] = "Nazwa";
$MESS["INTR_MAIL_INP_PASS"] = "Hasło";
$MESS["INTR_MAIL_INP_PASS_EMPTY"] = "wprowadź hasło";
$MESS["INTR_MAIL_INP_PORT"] = "Port";
$MESS["INTR_MAIL_INP_SAVE"] = "Dołącz";
$MESS["INTR_MAIL_INP_SERVER"] = "Adres serwera IMAP";
$MESS["INTR_MAIL_INP_SERVER_BAD"] = "błędny adres";
$MESS["INTR_MAIL_INP_SERVER_EMPTY"] = "wprowadź adres serwera";
$MESS["INTR_MAIL_MAILBOX_DELETE"] = "Odłącz skrzynkę pocztową";
$MESS["INTR_MAIL_MAILBOX_DELETE_SHORT"] = "Rozłącz";
$MESS["INTR_MAIL_MAILBOX_EDIT_SUCCESS"] = "Parametry połączenia zostały zapisane pomyślnie.";
$MESS["INTR_MAIL_MAILBOX_OPTIONS"] = "Parametry połączenia";
$MESS["INTR_MAIL_MAILBOX_STATUS"] = "Status skrzyki pocztowej";
$MESS["INTR_MAIL_REMOVE_CONFIRM"] = "Odłączyć skrzynkę pocztową?";
$MESS["INTR_MAIL_REMOVE_CONFIRM_TEXT"] = "Na pewno chcesz usunąć skrzynkę pocztową?";
$MESS["INTR_MAIL_TRACKER_DESCR"] = "Podłącz swój e-mail firmowy do systemu Bitrix24, aby przechowywać korespondencję z klientami w CRM. Wszystkie e-maile przychodzące i wychodzące są zapisywane w profilu klienta jako nowe działania. Jeżeli dla nowego klienta nie istnieje wpis w CRM, system utworzy nowy lead i doda nowe działanie z przypisanym do niego mailem.";
$MESS["MAIL_MAIL_CRM_LICENSE_DESCR_AGE"] = "Tylko korespondencja z ostatnich trzech dni może zostać dodana do CRM w twoim planie. Przejdź na jeden z <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planów płatnych</a>, aby móc dodać wszystkie wiadomości do CRM.";
$MESS["MAIL_MAIL_CRM_LICENSE_DESCR_INTERVAL"] = "W bieżącym planie możesz sprawdzać pocztę e-mail co 10 minut. Aby częściej sprawdzać, czy są nowe wiadomości, przejdź na jeden z <a href=\"/settings/license_all.php\" target=\"_blank\">wybranych planów płatnych</a>.";
$MESS["MAIL_MAIL_CRM_LICENSE_TITLE"] = "Śledzenie adresu e-mail";
?>