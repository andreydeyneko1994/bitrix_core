<?
$MESS["C1C_REP"] = "m:Report";
$MESS["C1C_REP_ID"] = "m:ReportID";
$MESS["C1C_TABLELINE"] = "<m:Table Row>";
$MESS["C1C_TABLELINE_F"] = "Campo1";
$MESS["C1C_TABLELINE_F2"] = "Campo2";
$MESS["C1C_TABLELINE_LIST"] = "ReportList";
$MESS["C1C_TABLELINE_REP"] = "Relatório sucinto";
$MESS["CONNECTION_ERROR1"] = "Não é possível abrir conexão para";
$MESS["CONNECTION_ERROR2"] = ", por favor verifique os parâmetros de conexão.";
$MESS["INTRANET_MODULE_NOT_INSTALLED"] = "Edição intranet do bitrix CMS é necessário para executar este componente.";
$MESS["LINK_REPORT"] = "Relatório";
$MESS["LINK_REPORTLIST"] = "Lista de relatórios";
$MESS["LINK_SHORTREPORT"] = "Relatório sucinto";
?>