<?
$MESS["DATA_DELETE"] = "Usuwanie danych demo…";
$MESS["DELETE_STEP_TITLE"] = "Usuwanie danych";
$MESS["FINISH_STEP_TITLE"] = "Kończenie czyszczenia";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "Czyszczenie zostało zakończone.";
$MESS["INST_ERROR_NOTICE"] = "Proszę powtórzyć bieżący krok. Jeśli ten błąd będzie się powtarzał, pomiń ten krok.";
$MESS["INST_ERROR_OCCURED"] = "Uwaga! Wystąpił błąd w tym kroku instalacji.";
$MESS["INST_RETRY_BUTTON"] = "Ponów";
$MESS["INST_SKIP_BUTTON"] = "Pomiń";
$MESS["INST_TEXT_ERROR"] = "Wiadomość błędu";
$MESS["NEXT_BUTTON"] = "Następny";
$MESS["PREVIOUS_BUTTON"] = "Wstecz";
$MESS["WELCOME_STEP_TITLE"] = "Inicjowanie czyszczenia";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Instalowanie danych...";
$MESS["wiz_go"] = "Otwórz Portal";
?>