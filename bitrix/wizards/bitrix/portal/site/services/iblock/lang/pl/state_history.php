<?
$MESS["STATE_FORM_1"] = "Formularz zmiany w statusie pracownika";
$MESS["STATE_FORM_2"] = "Wybierz pracownika, którego status się zmienił";
$MESS["STATE_FORM_3"] = "Data zmiany w statusie";
$MESS["STATE_FORM_4"] = "Typ zmiany";
$MESS["STATE_FORM_5"] = "Stanowisko";
$MESS["STATE_FORM_6"] = "Dział";
$MESS["STATE_FORM_7"] = "Krótki opis zmiany w statusie";
$MESS["STATE_FORM_8"] = "Stan";
?>