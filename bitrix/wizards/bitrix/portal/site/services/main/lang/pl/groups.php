<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Członkowie tej grupy mają dostęp do Panelu Kontrolnego.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Użytkownicy Panelu Kontrolnego";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Członek tej grupy może tworzyć nowe grupy robocze.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Grupa robicza administratorów";
$MESS["DIRECTION_GROUP_DESC"] = "Zarząd firmy.";
$MESS["DIRECTION_GROUP_NAME"] = "Zarząd";
$MESS["EMPLOYEES_GROUP_DESC"] = "Wszyscy pracownicy firmy zarejestrowani w portalu.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Pracownicy";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Personel działu Sprzedaży i Marketingu";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Sprzedaż i Marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Personel Kadr";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Dział HR";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Administratorzy portalu mają dostęp do wszystkich usług portalu.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Administratorzy portalu";
$MESS["SUPPORT_GROUP_DESC"] = "Specjalista helpdesk";
$MESS["SUPPORT_GROUP_NAME"] = "Hepldesk";
?>