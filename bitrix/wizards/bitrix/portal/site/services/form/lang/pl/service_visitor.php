<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["SERVICE_VISITOR_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_VISITOR_FORM_NAME"] = "Przepustki dla gości";
$MESS["VISITOR_ACCESS_MENU_NAME"] = "Przepustki dla gości";
$MESS["VISITOR_CAR_QUESTION"] = "Rejestracja samochodu";
$MESS["VISITOR_COMPANY_QUESTION"] = "Firma";
$MESS["VISITOR_DATE_FROM_QUESTION"] = "Wizyta rozpoczeła się";
$MESS["VISITOR_DATE_TO_QUESTION"] = "Wizyta zakończyła się";
$MESS["VISITOR_NAME_QUESTION"] = "Pełna nazwa gościa";
$MESS["VISITOR_REASON_QUESTION"] = "Powód odwiedzin";
$MESS["VISITOR_VISITS_QUESTION"] = "Liczba odwiedzin";
?>