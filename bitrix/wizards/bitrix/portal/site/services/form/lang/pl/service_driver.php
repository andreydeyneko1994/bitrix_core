<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["DATE_QUESTION"] = "Data";
$MESS["DESTINATION_QUESTION"] = "Przeznaczenie";
$MESS["DRIVER_SERVICES_MENU_NAME"] = "Kierowcy";
$MESS["DURATION_QUESTION"] = "Czas trwania (godziny)";
$MESS["PLACES_QUESTION"] = "Miejsca siedzące";
$MESS["SERVICE_DRIVER_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_DRIVER_FORM_NAME"] = "Kierowcy";
$MESS["TIME_QUESTION"] = "Czas";
$MESS["VEHICLE_TYPE_ANSWER1"] = "samochód";
$MESS["VEHICLE_TYPE_ANSWER2"] = "ciężarówka";
$MESS["VEHICLE_TYPE_ANSWER3"] = "autobus";
$MESS["VEHICLE_TYPE_ANSWER4"] = "VIP";
$MESS["VEHICLE_TYPE_QUESTION"] = "Typ pojazdu";
?>