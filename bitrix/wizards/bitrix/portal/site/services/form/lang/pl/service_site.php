<?
$MESS["ADMIN_NOTE_QUESTION"] = "Odpowiedź";
$MESS["COMMENT_QUESTION"] = "Komentarz";
$MESS["DATE_QUESTION"] = "Termin zamówienia";
$MESS["DEPARTMENT_QUESTION"] = "Dział";
$MESS["OTHER_QUESTION"] = "Przedmioty dodatkowe";
$MESS["ROOM_QUESTION"] = "Pokój";
$MESS["SERVICE_SITE_FORM_BUTTON"] = "Złóż zamówienie";
$MESS["SERVICE_SITE_FORM_NAME"] = "Aranżacja miejsca pracy";
$MESS["STOCK_ANSWER1"] = "Tabela";
$MESS["STOCK_ANSWER2"] = "Krzesło";
$MESS["STOCK_ANSWER3"] = "Komputer";
$MESS["STOCK_ANSWER4"] = "Telefon";
$MESS["STOCK_ANSWER5"] = "inne";
$MESS["STOCK_QUESTION"] = "Przedmioty na stanowisku pracy";
$MESS["WORK_SITE_MENU_NAME"] = "Aranżacja miejsca pracy";
?>