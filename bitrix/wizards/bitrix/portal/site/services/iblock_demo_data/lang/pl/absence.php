<?
$MESS["W_IB_ABSENCE_1_FINISH"] = "Działa";
$MESS["W_IB_ABSENCE_1_NAME"] = "Zwykły urlop";
$MESS["W_IB_ABSENCE_1_PREV1"] = "Zwykły urlop dla";
$MESS["W_IB_ABSENCE_1_PREV2"] = "rok.";
$MESS["W_IB_ABSENCE_1_STATE"] = "Na urlopie";
$MESS["W_IB_ABSENCE_2_NAME"] = "Wyjazd służbowy";
$MESS["W_IB_ABSENCE_2_PREV"] = "Wyjazd służbowy do oddziału firmy.";
$MESS["W_IB_ABSENCE_3_NAME"] = "Chorobowe";
$MESS["W_IB_ABSENCE_3_PREV"] = "Płatne chorobowe.";
$MESS["W_IB_ABSENCE_3_STATE"] = "Na chorobowym";
?>