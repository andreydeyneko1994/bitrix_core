<?php
$MESS["COPYRIGHT"] = "Direitos autorais &copy; 2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "O assistente requer que o JavaScript esteja ativado no seu sistema. O JavaScript está desativado ou não é suportado pelo seu navegador. Altere as configurações do navegador e <a href=\"\">tente novamente</a>.";
$MESS["WIZARD_TITLE"] = "Configuração Bitrix24.CRM";
