<?php
$MESS["COPYRIGHT"] = "Copyright &copy; 2001-#CURRENT_YEAR# Bitrix24";
$MESS["INST_JAVASCRIPT_DISABLED"] = "The wizard requires that JavaScript is enabled on your system. JavaScript is disabled or not supported by your browser. Please alter the browser settings and <a href=\"\">try again</a>.";
$MESS["WIZARD_TITLE"] = "Bitrix24.CRM Configuration";
