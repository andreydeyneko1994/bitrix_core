<?
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["EVENTS_SECTION_NAME"] = "Calendarios";
$MESS["EVENTS_TYPE_NAME"] = "Calendario";
$MESS["LIBRARY_ELEMENT_NAME"] = "Archivos";
$MESS["LIBRARY_SECTION_NAME"] = "Folders";
$MESS["LIBRARY_TYPE_NAME"] = "Documentos";
$MESS["NEWS_ELEMENT_NAME"] = "Nuevos artículos";
$MESS["NEWS_TYPE_NAME"] = "Noticias";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["PHOTOS_SECTION_NAME"] = "Albums";
$MESS["PHOTOS_TYPE_NAME"] = "Fotogalería";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_SECTION_NAME"] = "Secciones";
$MESS["SERVICES_TYPE_NAME"] = "Servicios";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["STRUCTURE_SECTION_NAME"] = "Secciones";
$MESS["STRUCTURE_TYPE_NAME"] = "Estructura de la compañía";
?>