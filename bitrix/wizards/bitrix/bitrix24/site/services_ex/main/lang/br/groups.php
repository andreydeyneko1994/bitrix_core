<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Os administradores têm permissão total para acessar, gerenciar e editar os recursos do site da extranet.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administradores do Site da Extranet";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Todos os usuários que têm permissão para criar grupos de usuários no site da extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Com permissão para criar grupos de usuários da extranet";
$MESS["EXTRANET_GROUP_DESC"] = "Todos os usuários que têm acesso ao site da extranet.";
$MESS["EXTRANET_GROUP_NAME"] = "Usuários da Extranet";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
$MESS["EXTRANET_SUPPORT_GROUP_DESC"] = "Pessoas encarregadas de prestar assistência no site da extranet.";
$MESS["EXTRANET_SUPPORT_GROUP_NAME"] = "Suporte da Extranet";
?>