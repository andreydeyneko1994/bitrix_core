<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Comentarios: Extranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Comentarios sobre los archivos de usuario: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Comentarios sobre User And Group Galerías de fotos: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Comentarios sobre tareas de usuarios y grupos: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Foros ocultos: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Foros de usuarios individuales y grupos del sitio web extranet.";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Foros de Usuarios y Grupos: Extranet";
?>