<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Comentários: Extranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Comentários sobre Arquivos de Usuário: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Comentários sobre Galerias de Fotos de Usuários e Grupos: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Comentários sobre Tarefas de Usuário e Grupo: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Fóruns Ocultos: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Fóruns de usuários individuais e grupos do site da extranet.";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Fóruns de Usuários e Grupos: Extranet";
?>