<?
$MESS["BLG_NAME"] = "Blog de";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Conseil";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Astuces";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Je vais essayer !";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Intéressant !";
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Groupe de réseau social";
$MESS["BLOG_DEMO_MESSAGE_BODY4"] = "[VIDEO TYPE=YOUTUBE WIDTH=640 HEIGHT=360]//www.youtube.com/embed/oBImUPaE0dw?feature=oembed[/VIDEO]

Hourra ! Vous êtes sur le point de doubler votre productivité grâce à Bitrix24. Veuillez regarder cette vidéo d'une minute pour découvrir ce qu'est Bitrix24, comment inviter vos collègues à vous rejoindre et où trouver de l'aide, si nécessaire.";
$MESS["BLOG_DEMO_MESSAGE_TITLE4"] = "Bienvenue sur Bitrix24 !";
$MESS["BPC_SONET_COMMENT_TITLE"] = "a ajouté un commentaire au message du blog \"#TITLE#\"";
$MESS["BPC_SONET_POST_TITLE"] = "a ajouté un message au blog \"#TITLE#\"";
?>