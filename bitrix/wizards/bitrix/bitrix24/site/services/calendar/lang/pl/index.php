<?php
$MESS["CAL_TYPE_GROUP_NAME"] = "Kalendarze grupowe";
$MESS["CAL_TYPE_LOCATION_NAME"] = "Dostępność sali konferencyjnej";
$MESS["CAL_TYPE_USER_NAME"] = "Kalendarze użytkownika";
$MESS["CAL_YEAR_HOLIDAYS"] = "01.01,04.07,01.11,25.12";
$MESS["EC_COMPANY_CALENDAR_"] = "Kalendarz firmy";
$MESS["EC_COMPANY_CALENDAR_GOV_ORGANIZATION"] = "Kalendarze organizacji";
$MESS["EC_COMPANY_CALENDAR_PUBLIC_ORGANIZATION"] = "Kalendarze organizacji";
$MESS["W_IB_CALENDAR_EMP_ABS"] = "Spotkanie z klientem w jego biurze";
