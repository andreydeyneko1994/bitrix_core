<?php
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Zamknięte deale";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Ostatnie wydarzenia";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Moje leady";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nowe firmy";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Nowe Kontakty";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Nowe leady";
$MESS["CRM_ROLE_ADMIN"] = "Administrator";
$MESS["CRM_ROLE_CHIF"] = "Szef działu";
$MESS["CRM_ROLE_DIRECTOR"] = "Dyrektor Zarządzający";
$MESS["CRM_ROLE_MAN"] = "Menedżer";
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
