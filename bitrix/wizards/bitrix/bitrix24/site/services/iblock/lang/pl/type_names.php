<?
$MESS["EVENTS_ELEMENT_NAME"] = "Wydarzenia";
$MESS["EVENTS_SECTION_NAME"] = "Kalendarze";
$MESS["EVENTS_TYPE_NAME"] = "Kalendarz";
$MESS["LISTS_ELEMENT_NAME"] = "Elementy";
$MESS["LISTS_SECTION_NAME"] = "Sekcje";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Elementy";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Sekcje";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listy sieci społecznych";
$MESS["LISTS_TYPE_NAME"] = "Listy";
$MESS["NEWS_ELEMENT_NAME"] = "Wiadomości";
$MESS["NEWS_TYPE_NAME"] = "Wiadomości";
$MESS["PHOTOS_ELEMENT_NAME"] = "Zdjęcia";
$MESS["PHOTOS_SECTION_NAME"] = "Albumy";
$MESS["PHOTOS_TYPE_NAME"] = "Galeria Zdjęć";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementy";
$MESS["SERVICES_SECTION_NAME"] = "Sekcje";
$MESS["SERVICES_TYPE_NAME"] = "Usługi";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementy";
$MESS["STRUCTURE_SECTION_NAME"] = "Sekcje";
$MESS["STRUCTURE_TYPE_NAME"] = "Struktura Firmy";
?>