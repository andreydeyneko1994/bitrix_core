<?
$MESS["ABSENCE_FORM_1"] = "Nieobecność";
$MESS["ABSENCE_FORM_2"] = "Wybierz nieobecnego pracownika";
$MESS["ABSENCE_FORM_3"] = "Typ nieobecności";
$MESS["ABSENCE_FORM_4"] = "Powód nieobecności";
$MESS["ABSENCE_FORM_5"] = "Okres nieobecności";
$MESS["ABSENCE_FORM_6"] = "Data rozpoczęcia";
$MESS["ABSENCE_FORM_7"] = "Data zakończenia";
?>