<?
$MESS["SONET_LOG_GUEST"] = "Gość";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# dodał/a nowe zdjęcie \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# dodał zdjęcie";
$MESS["SONET_PHOTO_LOG_2"] = "zdjęcia (#COUNT#)";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Nowe zdjęcia załadowane do \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Nowe zdjęcia: <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Zobacz album</a>.";
$MESS["W_IB_USER_PHOTOG_TAB1"] = "edit1--#--Zdjęcie--,--ACTIVE--#--  Aktywny--,--NAME--#--*Tytuł--,--IBLOCK_ELEMENT_PROP_VALUE--#----Własność--,--PREVIEW_PICTURE--#--  Zdjęcie poglądowe--,--DETAIL_PICTURE--#--  Zdjęcie szczegółowe--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB2"] = "--#--  Oryginalny--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB3"] = "--#--  Ocena--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB4"] = "--#--  Głosy--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB5"] = "--#--  Suma głosów--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB6"] = "--#--  Plik został usunięty--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB7"] = "--#--  Obiekt opublikowany--;--edit6--#--Opis--,--PREVIEW_TEXT--#--  Tekst poglądowy--,--DETAIL_TEXT--#--  Opis szczegółowy--;--edit2--#--Sekcje--,--SECTIONS--#--  Sekcje--;--edit3--#--Dodatkowe informacje--,--SORT--#--  Indeks sortowania--,--CODE--#--";
?>