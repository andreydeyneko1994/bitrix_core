<?
$MESS["COMMENTS_GROUP_NAME"] = "Fórum para comentários";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Comentários para biblioteca de Arquivos Comuns";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Fórum Público para os funcionários da organização. Discuta seu negócio e troque opiniões aqui.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Atenção! Agora, os funcionários da organização podem manter o armazenamento de arquivo em sua área privada.

Você encontrará informações detalhadas sobre administração do armazenamento de arquivo e o método de mapeamento de armazenamento para uma unidade de rede na seção de ajuda: \"Meu Perfil - Arquivos\".

Caso tenha quaisquer dúvidas relativas à configuração de armazenamento de arquivo, envie suas solicitações para os engenheiros de suporte técnico usando o formulário de solicitação de suporte.";
$MESS["GENERAL_FORUM_NAME"] = "Fórum geral";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Notícias do Portal";
$MESS["GENERAL_GROUP_NAME"] = "Fóruns gerais";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Comentários para Arquivos";
$MESS["HIDDEN_GROUP_NAME"] = "Fóruns ocultos";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Discussão da galeria de fotos";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Fóruns pessoais e de grupo";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Usuários e grupos ";
?>