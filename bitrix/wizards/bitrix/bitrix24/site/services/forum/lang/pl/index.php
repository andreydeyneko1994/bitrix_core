<?
$MESS["COMMENTS_GROUP_NAME"] = "Forum dla komentarzy";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Komentarze do Biblioteki Dokumentów";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Forum publiczne dla pracowników firmy. W tym miejscu możesz dyskutować i wymieniać się opiniami na temat pracy.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Uwaga! Pracownicy firmy mogą teraz utrzymywać swoje pliki w ich prywatnym obszarze.

Szczegóły na temat zarządzania przestrzenią dyskową i metod mapowania do dysku sieciowego odnajdziesz w sekcji pomocy: \"Mój Profil - Pliki\".

Jeżeli masz jakieś pytania odnośnie konfiguracji przestrzeni dyskowej, proszę prześlij zapytanie do pomocy technicznej, korzystając z formularza.";
$MESS["GENERAL_FORUM_NAME"] = "Forum ogólne";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Aktualności Portalowe";
$MESS["GENERAL_GROUP_NAME"] = "Forum ogólne";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Komentarze do Plików";
$MESS["HIDDEN_GROUP_NAME"] = "Ukryte fora";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Dyskusja galerii zdjęć";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Forum osobiste i grup";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Użytkownicy i Grupy ";
?>