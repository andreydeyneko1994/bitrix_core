<?
$MESS["COMMENTS_GROUP_NAME"] = "Forum dla komentarzy";
$MESS["GENERAL_GROUP_NAME"] = "Forum ogólne";
$MESS["HIDDEN_GROUP_NAME"] = "Ukryte fora";
$MESS["SONET_GROUP_DESCRIPTION_NEW_1"] = "Używaj tej grupy aby dyskutować nad bieżącymi projektami. Nowi członkowie muszą być potwierdzeni przez właściciela grupy lub administratora aby uzyskać dostęp. Ta grupa jest widoczna dla każdego.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_2"] = "Używaj tej grupy do dyskusji, która wymaga kategoryzacji (np. tematy finansowe). Tylko zaproszeni użytkownicy mają dostęp. Ta grupa jest widoczna tylko dla jej członków.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_3"] = "Ta grupa jest dla każdego. Dyskutuj o czym chcesz: sport, muzyka, podróże, święta. Ta grupa jest widoczna dla każdego, każdy może do niej dołączyć bez zaproszenia.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_41"] = "Ta prywatna widoczna grupa jest przeznaczona dla użytkowników zewnętrznych. Do grupy Ekstranet należą podwykonawcy, partnerzy, klienci i inni użytkownicy, którzy nie są pracownikami firmy. W celu dołączenia do grupy wymagane jest zaproszenie. Ta grupa jest jako jedyna widoczna dla użytkowników zewnętrznych. Nie mogą oni wyświetlać ani w inny sposób uzyskiwać dostępu do żadnych innych informacji w Bitrix24.";
$MESS["SONET_GROUP_KEYWORDS_NEW_1"] = "projekty, plan dnia, badania";
$MESS["SONET_GROUP_KEYWORDS_NEW_2"] = "sklep, sprzedaż, pieniądze, finanse, oferty";
$MESS["SONET_GROUP_KEYWORDS_NEW_3"] = "sport, podróże, zabawa, piłka nożna, muzyka";
$MESS["SONET_GROUP_KEYWORDS_NEW_4"] = "projekty, freelance, partnerzy";
$MESS["SONET_GROUP_NAME_NEW_1"] = "Prywatna widoczna grupa";
$MESS["SONET_GROUP_NAME_NEW_2"] = "Prywatna ukryta grupa ";
$MESS["SONET_GROUP_NAME_NEW_3"] = "Otwarta grupa publiczna";
$MESS["SONET_GROUP_NAME_NEW_4"] = "Ekstranet: grupa dla zewnętrznych użytkowników";
$MESS["SONET_GROUP_SUBJECT_0"] = "Grupy Robocze";
$MESS["SONET_TASK_DESCRIPTION_1"] = "Wypełnij dane profilowe i dodaj swoje zdjęcie.";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Zaproś nowych pracowników do dołączenia do portalu intranetowego";
$MESS["SONET_TASK_TITLE_1"] = "Wypełnij dane profilowe";
$MESS["SONET_TASK_TITLE_2"] = "Zaproś nowych pracowników";
?>