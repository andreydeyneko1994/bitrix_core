<?
$MESS["COMMENTS_GROUP_NAME"] = "Foro para comentarios";
$MESS["GENERAL_GROUP_NAME"] = "Foros generales";
$MESS["HIDDEN_GROUP_NAME"] = "Foros ocultos";
$MESS["SONET_GROUP_DESCRIPTION_NEW_1"] = "Utilice este grupo para discutir proyectos actuales. Los nuevos miembros deben ser confirmados por el propietario del grupo o el administrador para obtener acceso. Este grupo es visible para todos.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_2"] = "Utilice para discusiones que requieran compartimentación (por ejemplo, temas financieros). Sólo las personas invitadas pueden tener acceso. Este grupo sólo es visible para sus miembros.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_3"] = "Este grupo es para todos. Discuta lo que quiera: deporte, música, viajes, vacaciones. Este grupo es visible para todos; Cualquier persona puede unirse a ella, no requiere invitación.";
$MESS["SONET_GROUP_DESCRIPTION_NEW_41"] = "Este grupo privado visible es para usuarios externos. El grupo de Extranet alberga subcontratistas, socios, clientes y otros usuarios que no son empleados de la compañía. Se requiere una invitación para unirse al grupo. Este grupo es el único visible para los usuarios externos. No pueden ver ni acceder a ninguna otra información en su Bitrix24.";
$MESS["SONET_GROUP_KEYWORDS_NEW_1"] = "Proyectos, rutina diaria, investigación";
$MESS["SONET_GROUP_KEYWORDS_NEW_2"] = "Market, mercadería, dinero, finanzas, licitación";
$MESS["SONET_GROUP_KEYWORDS_NEW_3"] = "Deporte, viaje, diversión, fútbol, ??música";
$MESS["SONET_GROUP_KEYWORDS_NEW_4"] = "Proyectos, freelance, partners";
$MESS["SONET_GROUP_NAME_NEW_1"] = "Grupo visible privado";
$MESS["SONET_GROUP_NAME_NEW_2"] = "Grupo privado oculto";
$MESS["SONET_GROUP_NAME_NEW_3"] = "Abrir grupo público";
$MESS["SONET_GROUP_NAME_NEW_4"] = "Extranet: grupo para usuarios externos";
$MESS["SONET_GROUP_SUBJECT_0"] = "Grupos de trabajo";
$MESS["SONET_TASK_DESCRIPTION_1"] = "Rellena tus datos de perfil y sube tu foto.";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Invitar a nuevos empleados a unirse al portal de la intranet";
$MESS["SONET_TASK_TITLE_1"] = "Rellenar datos de perfil";
$MESS["SONET_TASK_TITLE_2"] = "Invitar a nuevos empleados";
?>