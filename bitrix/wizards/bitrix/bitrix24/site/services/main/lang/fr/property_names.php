<?
$MESS["UF_1C"] = "Utilisateur de 1C";
$MESS["UF_CRM_CAL_EVENT"] = "CRM Items";
$MESS["UF_CRM_TASK"] = "CRM Items";
$MESS["UF_DEPARTMENT"] = "Départements";
$MESS["UF_DISTRICT"] = "Arrondissement";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_INN"] = "INN";
$MESS["UF_INTERESTS"] = "Intérêts";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_PHONE_INNER"] = "Numéro d'extension";
$MESS["UF_SKILLS"] = "Compétences";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_STATE_HISTORY"] = "Historique des statuts";
$MESS["UF_STATE_LAST"] = "Dernier statut";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_WEB_SITES"] = "Autres sites internet";
$MESS["UF_XING"] = "Xing";
?>