<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Los administradores tienen todos los permisos de acceder, manejar y editar recursos del sitio web extranet.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administradores del Sitio web Extranet";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Todos los usuarios que tienen permiso de crear grupos de usuarios en el sitio web extranet";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Permitido crear grupos de usuario de extranet";
$MESS["EXTRANET_GROUP_DESC"] = "Todos los usuarios que tienen acceso al sitio web extranet";
$MESS["EXTRANET_GROUP_NAME"] = "Usuarios de extranet";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
?>