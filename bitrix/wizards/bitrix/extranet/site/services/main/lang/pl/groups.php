<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Administratorzy mają pełne uprawnienia do dostępu, zarządzania i edycji zasobów strony ekstranetu.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administratorzy Stron Ekstranetu";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Wszyscy użytkownicy, którzy mają uprawnienia do tworzenia grup użytkowników na stronie ekstranetu.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Zezwolono na tworzenie ekstranetowych grup użytkownika";
$MESS["EXTRANET_GROUP_DESC"] = "Wszyscy użytkownicy, którzy mają dostęp do strony ekstranetu.";
$MESS["EXTRANET_GROUP_NAME"] = "Użytkownicy Ekstranetu";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
?>