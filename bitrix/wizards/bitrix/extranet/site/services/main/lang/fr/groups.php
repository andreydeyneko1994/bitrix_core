<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Les administrateurs du site extranet disposent des pleins droits d'administration des services du site extranet.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administrateurs de site extranet";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Tous les utilisateurs ayant le droit de créer des groupes sur le site extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Peuvent créer des groupes de travail extranet";
$MESS["EXTRANET_GROUP_DESC"] = "Tous les utilisateurs ayant accès au site extranet.";
$MESS["EXTRANET_GROUP_NAME"] = "Utilisateurs d'Extranet";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
?>