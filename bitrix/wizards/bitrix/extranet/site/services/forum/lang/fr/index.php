<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Forums pour les commentaires: Extranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Commentaires de fichiers d'utilisateurs et de groupes: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Commentaires pour les galeries photo des utilisateurs et des groupes: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Commentaires pour les tâches des utilisateurs et des groupes: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Forums cachés - extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Forums des utilisateurs et groupes du site extranet individuels.";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Forums des utilisateurs et des groupes - extranet";
?>