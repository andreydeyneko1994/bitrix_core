<?
$MESS["FINISH_STEP_CONTENT"] = "<b>Félicitations !</b><br />
<br />
La configuration du site extranet est achevé.<br /><br />";
$MESS["FINISH_STEP_TITLE"] = "Fin de réglage";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "L'ajustage est terminé";
$MESS["INST_ERROR_NOTICE"] = "Répétez l'installation du pas courant. Si l'erreur persiste, sautez le pas.";
$MESS["INST_ERROR_OCCURED"] = "Attention ! Une erreur s'est produite à cette étape de l'installation du produit.";
$MESS["INST_JAVASCRIPT_DISABLED"] = "Pour installer l'assistant il faut activer JavaScript. Apparemment, soit JavaScript n'est pas soutenu par le navigateur soit il est désactivé. Modifiez les paramètres du navigateur et ensuite <a href=''>essayez de nouveau</a>.";
$MESS["INST_RETRY_BUTTON"] = "Refaire le pas";
$MESS["INST_SKIP_BUTTON"] = "Laisser passer";
$MESS["INST_TEXT_ERROR"] = "Texte de l'erreur";
$MESS["NEXT_BUTTON"] = "Suivant";
$MESS["PREVIOUS_BUTTON"] = "Précédent";
$MESS["SELECT_TEMPLATE_SUBTITLE"] = "Veuillez choisir un modèle de design dans la liste pour votre portail";
$MESS["SELECT_TEMPLATE_TITLE"] = "Design du site extranet";
$MESS["SELECT_THEME_SUBTITLE"] = "Sélectionnez le schéma de couleurs pour le site extranet";
$MESS["SELECT_THEME_TITLE"] = "Couleur du thème";
$MESS["WELCOME_STEP_TITLE"] = "Commencement du réglage";
$MESS["WELCOME_TEXT"] = "Cet assistant vous aidera à ajuster en 6 étapes le site extranet pour le Portail corporatif et à commencer le travail avec son contenu et réglages.<br /><br /> Il vous faut choisir le design du site extranet parmi les variantes proposées ainsi que la palette de couleurs et à choisir les réglages de base.";
$MESS["WELCOME_TEXT_SHORT"] = "L'assistant d'installation de l'Extranet vous guidera pendant les quatre étapes.<br /><br />Vous devez préciser les paramètres initiaux de votre page Extranet.";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Elimination de données...";
$MESS["site_name_suffix"] = ": Extranet";
$MESS["wiz_allow_anonym"] = "Autoriser les utilisateurs non autorisés à visualiser le portail";
$MESS["wiz_allow_register"] = "Autoriser des utilisateurs de s'enregistrer d'une façon indépendante";
$MESS["wiz_company_logo"] = "Logotype de la compagnie : ";
$MESS["wiz_company_name"] = "Dénomination de la entreprise : ";
$MESS["wiz_cur_intranet_template_description"] = "Appliquer le modèle actuel du site intranet pour le site extranet";
$MESS["wiz_cur_intranet_template_name"] = "Modèle courante du site intranet";
$MESS["wiz_demo_structure"] = "Installer l'exemple de démonstration de la structure du site extranet";
$MESS["wiz_galochka"] = "Il est conseillé de laisser cette option marquée, parce que la création de structure de données pour le site Extranet est un processus qui vous prend beaucoup de main-d'uvre.";
$MESS["wiz_galochka_structure"] = "Attention ! En cas de réinstallation de la structure, tous les blocs d'information, forums etc. pour le site d'Extranet seront restaurés. Réinstaller les données ?";
$MESS["wiz_go"] = "Accéder au Site";
$MESS["wiz_install"] = "Installation";
$MESS["wiz_install_data"] = "Installation de la solution";
$MESS["wiz_settings"] = "Configuration du site";
$MESS["wiz_site_folder"] = "Dossier pour le site extranet : ";
$MESS["wiz_site_folder_already_exists"] = "Le dossier est déjà utilisé par un autre site.";
$MESS["wiz_site_folder_error"] = "Site Extranet peut pas être installé dans le dossier racine.";
$MESS["wiz_site_id"] = "Code du site d' Intranet : ";
$MESS["wiz_site_id_already_exists"] = "Le site du même code existe déjà.";
$MESS["wiz_site_id_error"] = "L'identificateur du site doit contenir 2 symboles.";
$MESS["wiz_slogan"] = "Mon Entreprise: Extranet";
$MESS["wiz_structure_data"] = "Réinstaller la structure de données.";
$MESS["wiz_template"] = "Choisissez le modèle du site";
$MESS["wiz_template_color"] = "Sélectionnez le schéma de couleurs du modèle";
?>