<?
$MESS["SERVICE_BLOG"] = "Blog";
$MESS["SERVICE_FILEMAN"] = "Eksplorator Strony";
$MESS["SERVICE_FILES"] = "Pliki Strony Ekstranetu";
$MESS["SERVICE_FORM"] = "Formularze Sieciowe";
$MESS["SERVICE_FORUM"] = "Forum";
$MESS["SERVICE_INTRANET"] = "Ustawienia Portalu Intranetu";
$MESS["SERVICE_LEARNING"] = "e-Nauka";
$MESS["SERVICE_MAIN_SETTINGS"] = "Ustawienia Strony Ekstranetu";
$MESS["SERVICE_PHOTOGALLERY"] = "Galeria Zdjęć";
$MESS["SERVICE_SEARCH"] = "Szukaj";
$MESS["SERVICE_SOCIALNETWORK"] = "Sieć Społecznościowa";
$MESS["SERVICE_STATISTIC"] = "Statystyki";
$MESS["SERVICE_USERS"] = "Użytkownicy";
$MESS["SERVICE_VOTE"] = "Ankiety";
$MESS["SERVICE_WORKFLOW"] = "Workflow";
?>