<?php
$MESS["SERVICE_BLOG"] = "Blog";
$MESS["SERVICE_FILEMAN"] = "Explorador del sitio web";
$MESS["SERVICE_FILES"] = "Archivos de extranet del sitio web";
$MESS["SERVICE_FORM"] = "Formularios Webs";
$MESS["SERVICE_FORUM"] = "Foro";
$MESS["SERVICE_INTRANET"] = "Configuraciones del Portal de Intranet ";
$MESS["SERVICE_LEARNING"] = "enseñanza virtual";
$MESS["SERVICE_MAIN_SETTINGS"] = "Configuración de la extranet del sitio web";
$MESS["SERVICE_PHOTOGALLERY"] = "Galería de Fotos";
$MESS["SERVICE_SEARCH"] = "Búsqueda";
$MESS["SERVICE_SOCIALNETWORK"] = "Red social";
$MESS["SERVICE_STATISTIC"] = "Estadisticas";
$MESS["SERVICE_USERS"] = "Usuarios";
$MESS["SERVICE_VOTE"] = "Encuestas";
$MESS["SERVICE_WORKFLOW"] = "Procesos de negocio";
