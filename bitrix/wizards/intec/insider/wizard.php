<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<? include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/install/wizard_sol/wizard.php') ?>
<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;

Loc::loadMessages(__FILE__);

class BeginStep extends CWizardStep
{
    public static function GetId() { return 'Begin'; }

    public static function GetDependencies() {
        return [
            'intec.core',
            'intec.insider'
        ];
    }

    function InitStep()
    {
        parent::InitStep();

        $this->SetStepID(static::GetId());
        $this->SetTitle(Loc::getMessage('wizard.steps.begin.title'));
        $this->content .= Loc::getMessage('wizard.steps.begin.description');
        $this->SetNextStep(SiteStep::GetId());

        $wizard = $this->GetWizard();
    }

    function ShowStep()
    {
        $next = true;
        $dependencies = static::GetDependencies();

        foreach ($dependencies as $dependency) {
            if (!Loader::includeModule($dependency)) {
                $this->content = Loc::getMessage('wizard.steps.begin.noModule', [
                    '#MODULE_ID#' => $dependency
                ]);

                $next = false;
            }
        }

        if (!$next)
            $this->SetNextStep(null);
    }
}

class SiteStep extends CSelectSiteWizardStep
{
    public static function GetId() { return 'Site'; }

    function InitStep()
    {
        parent::InitStep();

        $this->SetStepID(static::GetId());
        $this->SetPrevStep(BeginStep::GetId());
        $this->SetNextStep(TemplateStep::GetId());
    }
}

class TemplateStep extends CSelectTemplateWizardStep
{
    public static function GetId() { return 'Template'; }

    function InitStep()
    {
        parent::InitStep();

        $this->SetStepID(static::GetId());
        $this->SetPrevStep(SiteStep::GetId());
        $this->SetNextStep(InstallStep::GetId());
    }
}

class InstallStep extends CDataInstallWizardStep
{
    public static function GetId() { return 'Install'; }

    function InitStep()
    {
        parent::InitStep();

        $this->SetStepID(static::GetId());
    }
}

class FinishStep extends CFinishWizardStep
{
    public static function GetId() { return 'End'; }

    function InitStep()
    {
        parent::InitStep();
    }

    function ShowStep()
    {
        parent::ShowStep();

        $wizard = $this->GetWizard();
        $sSiteID = WizardServices::GetCurrentSiteID($wizard->GetVar('siteID'));
        $sSiteDir = '/';
        $arSite = CSite::GetByID($sSiteID);
        $arSite = $arSite->GetNext();

        if (!empty($arSite))
            $sSiteDir = $arSite['DIR'];

        $this->CreateNewIndex();

        COption::SetOptionString("main", "wizard_solution", $wizard->solutionName, false, $sSiteID);

        $path = $_SERVER['DOCUMENT_ROOT'].$sSiteDir.'.wizard.json';

        if (is_file($path))
            unlink($path);
    }
}