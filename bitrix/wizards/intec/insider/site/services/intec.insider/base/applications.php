<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?

use Bitrix\Main\Localization\Loc;
use intec\insider\models\Application;

return [

    [
        'code' => 'business',
        'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business'),
        'items' =>
            [

                [
                    'code' => 'business.documents',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.documents'),
                    'items' =>
                        [

                            [
                                'code' => 'microsoftWord',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftWord'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => '(winword|word)',
                                'title' => 'Microsoft Word',
                            ],

                            [
                                'code' => 'adobeReader',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.adobeReader'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'adobe.*reader',
                                'title' => 'Adobe Reader',
                            ],

                            [
                                'code' => 'microsoftPowerPoint',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftPowerPoint'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'powerpnt',
                                'title' => 'Microsoft Power Point',
                            ],

                            [
                                'code' => 'googleDocuments',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleDocuments'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'docs\\.google\\.com\\/.*document',
                                'title' => 'Google Documents',
                            ],

                            [
                                'code' => 'adobeAcrobat',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.adobeAcrobat'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'adobe.*acrobat',
                                'title' => 'Adobe Acrobat',
                            ],

                            [
                                'code' => 'foxitPDFEditor',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.foxitPDFEditor'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'foxit.*pdf.*editor',
                                'title' => 'Foxit PDF Editor',
                            ],

                            [
                                'code' => 'foxitReader',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.foxitReader'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'foxit.*pdf.*reader',
                                'title' => 'Foxit Reader',
                            ],

                            [
                                'code' => 'googlePresentation',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googlePresentation'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'docs\\.google\\.com\\/.*presentation',
                                'title' => 'Google Presentation',
                            ],

                            [
                                'code' => 'microsoftPowerPointOnline',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftPowerPointOnline'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'onedrive\\.live\\.com\\/.*app=PowerPoint',
                                'title' => 'Microsoft Power Point Online',
                            ],

                            [
                                'code' => 'microsoftWordOnline',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftWordOnline'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'onedrive\\.live\\.com\\/.*app=word',
                                'title' => 'Microsoft Word Online',
                            ],

                            [
                                'code' => 'microsoftOneNote',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftOneNote'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'onenote',
                                'title' => 'Microsoft OneNote',
                            ],

                            [
                                'code' => 'coolPDFReader',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.coolPDFReader'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'cool.*pdf.*reader',
                                'title' => 'Cool PDF Reader',
                            ],

                            [
                                'code' => 'libreOfficeWriter',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.libreOfficeWriter'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'libreoffice.*writer',
                                'title' => 'LibreOffice Writer',
                            ],

                            [
                                'code' => 'microsoftVisio',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftVisio'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'visio',
                                'title' => 'Microsoft Visio',
                            ],

                            [
                                'code' => 'openOfficeWriter',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.openOfficeWriter'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'libreoffice.*writer',
                                'title' => 'OpenOffice Writer',
                            ],
                        ],
                ],

                [
                    'code' => 'business.other',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other'),
                    'items' =>
                        [

                            [
                                'code' => 'business.other.marketing',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other.marketing'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'yandexDirect',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandexDirect'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'direct\\.yandex\\.',
                                            'title' => 'Yandex Direct',
                                        ],

                                        [
                                            'code' => 'facebookBusiness',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.facebookBusiness'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'business\\.facebook\\.',
                                            'title' => 'Facebook Business',
                                        ],

                                        [
                                            'code' => 'googleAdwords',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleAdwords'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'adwords\\.google\\.',
                                            'title' => 'Google Adwords',
                                        ],

                                        [
                                            'code' => 'googleAnalytics',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleAnalytics'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'analytics\\.google\\.',
                                            'title' => 'Google Analytics',
                                        ],

                                        [
                                            'code' => 'googleWebmasters',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleWebmasters'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'www\\.google\\.com\\.*webmasters',
                                            'title' => 'Google Webmasters',
                                        ],

                                        [
                                            'code' => 'mailChimp',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.mailChimp'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'mailchimp\\.com',
                                            'title' => 'Mail Chimp',
                                        ],

                                        [
                                            'code' => 'uniSender',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.uniSender'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'unisender\\.com',
                                            'title' => 'UniSender',
                                        ],

                                        [
                                            'code' => 'yandexMetrics',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandexMetrics'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'metrika\\.yandex\\.',
                                            'title' => 'Yandex Metrics',
                                        ],

                                        [
                                            'code' => 'yandexWebmaster',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandexWebmaster'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'webmaster\\.yandex\\.',
                                            'title' => 'Yandex Webmaster',
                                        ],

                                        [
                                            'code' => 'surveygizmo',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.surveygizmo'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'surveygizmo\\.com',
                                            'title' => 'surveygizmo.com',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.other.translators',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other.translators'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'googleTranslate',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleTranslate'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'translate\\.google\\.',
                                            'title' => 'Google Translate',
                                        ],

                                        [
                                            'code' => 'yandexTranslator',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandexTranslator'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'translate\\.yandex\\.',
                                            'title' => 'Yandex.Translator',
                                        ],

                                        [
                                            'code' => 'translate',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.translate'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'translate\\.ru',
                                            'title' => 'translate.ru',
                                        ],

                                        [
                                            'code' => 'lingvo',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.lingvo'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'lingvo',
                                            'title' => 'Lingvo',
                                        ],

                                        [
                                            'code' => 'lingvoOnline',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.lingvoOnline'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'lingvo-online\\.ru',
                                            'title' => 'lingvo-online.ru',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.other.garants',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other.garants'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'garantSystem',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.garantSystem'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'garant',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.garantSystem.title'),
                                        ],

                                        [
                                            'code' => 'consultantPlus',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.consultantPlus'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'consultant.*plus',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.consultantPlus.title'),
                                        ],

                                        [
                                            'code' => 'consultant',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.consultant'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'consultant\\.ru',
                                            'title' => 'consultant.ru',
                                        ],

                                        [
                                            'code' => 'garant',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.garant'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'garant\\.ru',
                                            'title' => 'garant.ru',
                                        ],

                                        [
                                            'code' => 'nalog',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.nalog'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'nalog\\.ru',
                                            'title' => 'nalog.ru',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.other.communications',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other.communications'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'skype',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.skype'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'skype',
                                            'title' => 'Skype',
                                        ],

                                        [
                                            'code' => 'rzdTickets',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.rzdTickets'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'rzd\\.ru',
                                            'title' => 'RZD tickets',
                                        ],

                                        [
                                            'code' => 'mts',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.mts'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'mts\\.ru',
                                            'title' => 'mts.ru',
                                        ],

                                        [
                                            'code' => 'lkCse',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.lkCse'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'lk\\.cse\\.ru',
                                            'title' => 'lk.cse.ru',
                                        ],

                                        [
                                            'code' => 'ruGett',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.ruGett'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'ru\\.gett\\.com',
                                            'title' => 'ru.gett.com',
                                        ],

                                        [
                                            'code' => 'cse',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.cse'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'cse\\.ru',
                                            'title' => 'cse.ru',
                                        ],

                                        [
                                            'code' => 'aviasales',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.aviasales'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'aviasales\\.ru',
                                            'title' => 'aviasales.ru',
                                        ],

                                        [
                                            'code' => 'microsoftLync',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftLync'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'lync',
                                            'title' => 'Microsoft Lync',
                                        ],

                                        [
                                            'code' => 'microsoftOfficeCommunicator',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftOfficeCommunicator'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'office.*communicator',
                                            'title' => 'Microsoft Office Communicator',
                                        ],

                                        [
                                            'code' => 'proburoEn',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.proburoEn'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'proburo\\.com',
                                            'title' => 'proburo.com',
                                        ],

                                        [
                                            'code' => 'proburo',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.proburo'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'proburo\\.ru',
                                            'title' => 'proburo.ru',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.other.scan',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.other.scan'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'windowsFaxAndScan',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.windowsFaxAndScan'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'fax.*scans',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.windowsFaxAndScan.title'),
                                        ],

                                        [
                                            'code' => 'hpLaserJetScanSoftware',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.hpLaserJetScanSoftware'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'hp.*laserjet',
                                            'title' => 'HP LaserJet Scan Software',
                                        ],

                                        [
                                            'code' => 'abbyyFineReader',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.abbyyFineReader'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'abbyy.*finereader',
                                            'title' => 'ABBYY FineReader',
                                        ],

                                        [
                                            'code' => 'epsonScan',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.epsonScan'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'epson.*scan',
                                            'title' => 'EPSON Scan',
                                        ],
                                    ],
                            ],
                        ],
                ],

                [
                    'code' => 'business.corp',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp'),
                    'items' =>
                        [

                            [
                                'code' => 'business.corp.crm',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp.crm'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'salesforce',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.salesforce'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'salesforce\\.com',
                                            'title' => 'Salesforce',
                                        ],

                                        [
                                            'code' => 'amoCRM',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.amoCRM'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'amocrm\\.ru',
                                            'title' => 'AmoCRM',
                                        ],

                                        [
                                            'code' => 'megaplan',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.megaplan'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'megaplan\\.ru',
                                            'title' => 'Megaplan',
                                        ],

                                        [
                                            'code' => 'sugarCRM',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.sugarCRM'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'sugarcrm\\.com',
                                            'title' => 'SugarCRM',
                                        ],

                                        [
                                            'code' => 'bitrix24',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.bitrix24'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'bitrix24\\.net',
                                            'title' => 'Bitrix24',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.corp.serviceDesk',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp.serviceDesk'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'companysupport',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.companysupport'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'help\\.companysupport\\.ru',
                                            'title' => 'help.companysupport.ru',
                                        ],

                                        [
                                            'code' => 'manageengine',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.manageengine'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'sdpondemand\\.manageengine\\.com',
                                            'title' => 'sdpondemand.manageengine.com',
                                        ],

                                        [
                                            'code' => 'zoho',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.zoho'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'accounts\\.zoho\\.com',
                                            'title' => 'accounts.zoho.com',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.corp.intranet',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp.intranet'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'microsoftonline',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftonline'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'login\\.microsoftonline\\.com',
                                            'title' => 'login.microsoftonline.com',
                                        ],

                                        [
                                            'code' => 'companyservices',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.companyservices'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'companyservices\\.ru',
                                            'title' => 'companyservices.ru',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.corp.planning',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp.planning'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'basecamp',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.basecamp'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'basecamp\\.com',
                                            'title' => 'Basecamp',
                                        ],

                                        [
                                            'code' => 'harvest',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.harvest'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => ' getharvest\\.com',
                                            'title' => 'Harvest',
                                        ],

                                        [
                                            'code' => 'pivotalTracker',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.pivotalTracker'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'pivotaltracker\\.com',
                                            'title' => 'Pivotal Tracker',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.corp.accounting',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.corp.accounting'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'documents6',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.documents6'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'documents.*6',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.documents6.title'),
                                        ],

                                        [
                                            'code' => '1c',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.1c'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => '1cv',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.1c.title'),
                                        ],

                                        [
                                            'code' => 'ipfrx5',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.ipfrx5'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'ipfrx5\\.exe',
                                            'title' => 'ipfrx5.exe',
                                        ],

                                        [
                                            'code' => 'checkxml',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.checkxml'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'checkxml\\.exe',
                                            'title' => 'checkxml.exe',
                                        ],

                                        [
                                            'code' => 'cbr',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.cbr'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'cbr\\.ru',
                                            'title' => 'cbr.ru',
                                        ],

                                        [
                                            'code' => 'kontur',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.kontur'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'extern\\.kontur\\.ru',
                                            'title' => 'extern.kontur.ru',
                                        ],

                                        [
                                            'code' => 'alfabank',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.alfabank'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'ibank\\.alfabank\\.ru',
                                            'title' => 'ibank.alfabank.ru',
                                        ],

                                        [
                                            'code' => 'taxcom',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.taxcom'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'online\\.taxcom\\.ru',
                                            'title' => 'online.taxcom.ru',
                                        ],
                                    ],
                            ],
                        ],
                ],

                [
                    'code' => 'business.development',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.development'),
                    'items' =>
                        [

                            [
                                'code' => 'business.development.documentation',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.development.documentation'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'googleReference',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleReference'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'support\\.google\\.com',
                                            'title' => 'Google reference',
                                        ],

                                        [
                                            'code' => 'oracleDocumetation',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.oracleDocumetation'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'docs\\.oracle\\.com',
                                            'title' => 'Oracle Documetation',
                                        ],

                                        [
                                            'code' => 'microsoftDeveloperNetwork',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftDeveloperNetwork'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'msdn\\.microsoft\\.com',
                                            'title' => 'Microsoft Developer Network',
                                        ],

                                        [
                                            'code' => 'chromeDevelopment',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.chromeDevelopment'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'developer\\.chrome\\.com',
                                            'title' => 'Chrome Development',
                                        ],

                                        [
                                            'code' => 'stackOverflow',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.stackOverflow'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'stackoverflow\\.com',
                                            'title' => 'Stack Overflow',
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.development.engineering',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.development.engineering'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'archiCAD',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.archiCAD'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'ArchiCAD',
                                            'title' => 'ArchiCAD',
                                        ],

                                        [
                                            'code' => 'blender',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.blender'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'Blender',
                                            'title' => 'Blender',
                                        ],

                                        [
                                            'code' => 'kompas3d',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.kompas3d'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'compas.*3d',
                                            'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.kompas3d.title'),
                                        ],
                                    ],
                            ],

                            [
                                'code' => 'business.development.systems',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.development.systems'),
                                'items' =>
                                    [

                                        [
                                            'code' => 'notepad++',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.notepad++'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'notepad\\+\\+',
                                            'title' => 'Notepad++',
                                        ],

                                        [
                                            'code' => 'adobeAfterEffects',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.adobeAfterEffects'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'Adobe After Effects',
                                            'title' => 'Adobe After Effects',
                                        ],

                                        [
                                            'code' => 'adobeIllustrator',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.adobeIllustrator'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'Adobe Illustrator',
                                            'title' => 'Adobe Illustrator',
                                        ],

                                        [
                                            'code' => 'adobePhotoshop',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.adobePhotoshop'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'Adobe Photoshop',
                                            'title' => 'Adobe Photoshop',
                                        ],

                                        [
                                            'code' => 'corelDRAW',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.corelDRAW'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'CorelDRAW',
                                            'title' => 'CorelDRAW',
                                        ],

                                        [
                                            'code' => 'eclipse',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.eclipse'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'eclipse',
                                            'title' => 'Eclipse',
                                        ],

                                        [
                                            'code' => 'microsoftVisualStudio',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftVisualStudio'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'Microsoft Visual Studio',
                                            'title' => 'Microsoft Visual Studio',
                                        ],

                                        [
                                            'code' => 'github',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.github'),
                                            'type' => Application::TYPE_WEBSITE,
                                            'expression' => 'github\\.com',
                                            'title' => 'github.com',
                                        ],

                                        [
                                            'code' => 'git',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.git'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'git\\.exe',
                                            'title' => 'Git',
                                        ],

                                        [
                                            'code' => 'phpStorm',
                                            'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.phpStorm'),
                                            'type' => Application::TYPE_APPLICATION,
                                            'expression' => 'phpstorm',
                                            'title' => 'PhpStorm',
                                        ],
                                    ],
                            ],
                        ],
                ],

                [
                    'code' => 'business.tables',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.tables'),
                    'items' =>
                        [

                            [
                                'code' => 'microsoftExcel',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftExcel'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'excel',
                                'title' => 'Microsoft Excel',
                            ],

                            [
                                'code' => 'googleSheets',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleSheets'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'docs\\.google\\.com\\.*spreadsheet',
                                'title' => 'Google sheets',
                            ],

                            [
                                'code' => 'microsoftAccess',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftAccess'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'Microsoft Access',
                                'title' => 'Microsoft Access',
                            ],

                            [
                                'code' => 'microsoftExcelOnline',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftExcelOnline'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'onedrive\\.live\\.com\\.*app=Excel',
                                'title' => 'Microsoft Excel Online',
                            ],
                        ],
                ],

                [
                    'code' => 'business.email',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.business.email'),
                    'items' =>
                        [

                            [
                                'code' => 'microsoftOutlook',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftOutlook'),
                                'type' => Application::TYPE_APPLICATION,
                                'expression' => 'outlook',
                                'title' => 'Microsoft Outlook',
                            ],

                            [
                                'code' => 'microsoftMail',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftMail'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'mail\\.live\\.com',
                                'title' => 'Microsoft mail',
                            ],

                            [
                                'code' => 'yahooMail',
                                'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yahooMail'),
                                'type' => Application::TYPE_WEBSITE,
                                'expression' => 'mail\\.yahoo\\.com',
                                'title' => 'Yahoo mail',
                            ],
                        ],
                ],
            ],
    ],

    [
        'code' => 'browsers',
        'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.browsers'),
        'items' =>
            [

                [
                    'code' => 'firefox',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.firefox'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'firefox\\.exe',
                    'title' => 'Firefox',
                ],

                [
                    'code' => 'googleChrome',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.googleChrome'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'chrome\\.exe',
                    'title' => 'Google Chrome',
                ],

                [
                    'code' => 'internetExplorer',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.internetExplorer'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'iexplore\\.exe',
                    'title' => 'Internet Explorer',
                ],

                [
                    'code' => 'yandexBrowser',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandexBrowser'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'yandex\\.exe',
                    'title' => 'Yandex',
                ],

                [
                    'code' => 'opera',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.opera'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'opera\\.exe',
                    'title' => 'Opera',
                ],

                [
                    'code' => 'safari',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.safari'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'safari\\.exe',
                    'title' => 'Safari',
                ],

                [
                    'code' => 'microsoftEdge',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.microsoftEdge'),
                    'type' => Application::TYPE_APPLICATION,
                    'expression' => 'MicrosoftEdge\\.exe',
                    'title' => 'Microsoft Edge',
                ],
            ],
    ],

    [
        'code' => 'socials',
        'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.socials'),
        'items' =>
            [

                [
                    'code' => 'vk',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.vk'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'vk\\.com',
                    'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.vk.title'),
                ],

                [
                    'code' => 'facebook',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.facebook'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'facebook\\.',
                    'title' => 'Facebook',
                ],

                [
                    'code' => 'twitter',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.twitter'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'twitter\\.',
                    'title' => 'Twitter',
                ],

                [
                    'code' => 'odnoklassniki',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.odnoklassniki'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'ok\\.ru',
                    'title' => Loc::getMessage('wizard.services.intecInsider.base.applications.odnoklassniki.title'),
                ],

                [
                    'code' => 'google+',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.google+'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'plus\\.google\\.',
                    'title' => 'Google+',
                ],

                [
                    'code' => 'linkedin',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.linkedin'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'linkedin\\.com',
                    'title' => 'linkedin.com',
                ],

                [
                    'code' => 'instagram',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.instagram'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'instagram\\.com',
                    'title' => 'instagram.com',
                ],

                [
                    'code' => 'pinterest',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.pinterest'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'pinterest\\.com',
                    'title' => 'pinterest.com',
                ],

                [
                    'code' => 'vkontakte',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.vkontakte'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'vkontakte\\.ru',
                    'title' => 'vkontakte.ru',
                ],

                [
                    'code' => 'ask',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.ask'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'ask\\.com',
                    'title' => 'ask.com',
                ],
            ],
    ],

    [
        'code' => 'searches',
        'name' => Loc::getMessage('wizard.services.intecInsider.base.applicationsGroups.searches'),
        'items' =>
            [
                [
                    'code' => 'google',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.google'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'google',
                    'title' => 'Google',
                ],

                [
                    'code' => 'yandex',
                    'name' => Loc::getMessage('wizard.services.intecInsider.base.applications.yandex'),
                    'type' => Application::TYPE_WEBSITE,
                    'expression' => 'yandex',
                    'title' => 'Yandex',
                ],
            ],
    ]
];