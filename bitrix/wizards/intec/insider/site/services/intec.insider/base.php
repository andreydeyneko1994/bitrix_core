<? include(__DIR__.'/../.begin.php') ?>
<?

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use intec\Core;
use intec\core\db\ActiveRecords;
use intec\insider\models\Application;
use intec\insider\models\application\Group as ApplicationGroup;
use intec\insider\models\Department;
use intec\insider\models\Distraction;
use intec\insider\models\distraction\ApplicationLink;
use intec\insider\models\Position;
use intec\insider\models\Schedule;
use intec\insider\models\user\ContactType as UserContactType;

Loc::loadMessages(__FILE__);

$serverKey = Option::get('intec.insider', 'key', '', WIZARD_SITE_ID);

if (empty($serverKey)) {
    $serverKey = Core::$app->security->generateRandomString(32);

    Option::set('intec.insider', 'key', $serverKey, WIZARD_SITE_ID);
}

/** @var ActiveRecords $schedules */
$schedules = Schedule::find()->all();

if ($schedules->count === 0) {
    $schedule = new Schedule();
    $schedule->name = Loc::getMessage('wizard.services.intecInsider.base.schedule');
    $schedule->data = [
        ['enabled' => true, 'intervals' => [['from' => '08:00', 'to' => '13:00'], ['from' => '14:00', 'to' => '17:00']]],
        ['enabled' => true, 'intervals' => [['from' => '08:00', 'to' => '13:00'], ['from' => '14:00', 'to' => '17:00']]],
        ['enabled' => true, 'intervals' => [['from' => '08:00', 'to' => '13:00'], ['from' => '14:00', 'to' => '17:00']]],
        ['enabled' => true, 'intervals' => [['from' => '08:00', 'to' => '13:00'], ['from' => '14:00', 'to' => '17:00']]],
        ['enabled' => true, 'intervals' => [['from' => '08:00', 'to' => '13:00'], ['from' => '14:00', 'to' => '17:00']]],
        ['enabled' => false, 'intervals' => []],
        ['enabled' => false, 'intervals' => []]
    ];

    $schedule->save();
}

/** @var ActiveRecords $applications */
$applications = Application::find()->all();
/** @var ActiveRecords $applicationsGroups */
$applicationsGroups = ApplicationGroup::find()->all();
/** @var ActiveRecords $distractions */
$distractions = Distraction::find()->all();

if ($applications->count === 0 && $applicationsGroups->count === 0 && $distractions->count === 0) {
    $items = include (__DIR__.'/base/applications.php');
    $import = function ($items, $parentId = null) use (&$applications, &$applicationsGroups, &$import) {
        foreach ($items as $item) {
            if (isset($item['items'])) {
                $applicationGroup = new ApplicationGroup();
                $applicationGroup->name = $item['name'];
                $applicationGroup->groupId = $parentId;
                $applicationGroup->save();
                $applicationsGroups->set($item['code'], $applicationGroup);

                $import($item['items'], $applicationGroup->id);
            } else {
                $application = new Application();
                $application->name = $item['name'];
                $application->groupId = $parentId;
                $application->type = $item['type'];
                $application->expression = $item['expression'];
                $application->title = $item['title'];
                $application->save();

                $applications->set($item['code'], $application);
            }
        }
    };

    $import($items);

    $distraction = new Distraction();
    $distraction->name = Loc::getMessage('wizard.services.intecInsider.base.distractions.browsers');
    $distraction->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('opera')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('googleChrome')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('firefox')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('microsoftEdge')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('internetExplorer')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('yandexBrowser')->id;
    $distractionLink->save();

    $distractionLink = new ApplicationLink();
    $distractionLink->distractionId = $distraction->id;
    $distractionLink->applicationId = $applications->get('safari')->id;
    $distractionLink->save();
}

/** @var ActiveRecords $departments */
$departments = Department::find()->all();

if ($departments->count === 0) {
    $department = new Department();
    $department->name = Loc::getMessage('wizard.services.intecInsider.base.departments.development');
    $department->save();

    $department = new Department();
    $department->name = Loc::getMessage('wizard.services.intecInsider.base.departments.marketing');
    $department->save();

    $department = new Department();
    $department->name = Loc::getMessage('wizard.services.intecInsider.base.departments.SEO');
    $department->save();

    $department = new Department();
    $department->name = Loc::getMessage('wizard.services.intecInsider.base.departments.design');
    $department->save();
}

/** @var ActiveRecords $positions */
$positions = Position::find()->all();

if ($positions->count === 0) {
    $position = new Position();
    $position->name = Loc::getMessage('wizard.services.intecInsider.base.positions.developer');
    $position->save();

    $position = new Position();
    $position->name = Loc::getMessage('wizard.services.intecInsider.base.positions.marketolog');
    $position->save();

    $position = new Position();
    $position->name = Loc::getMessage('wizard.services.intecInsider.base.positions.SEO');
    $position->save();

    $position = new Position();
    $position->name = Loc::getMessage('wizard.services.intecInsider.base.positions.designer');
    $position->save();
}

/** @var ActiveRecords $usersContactsTypes */
$usersContactsTypes = UserContactType::find()->all();

if ($usersContactsTypes->count === 0) {
    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.whatsApp');
    $userContactType->save();

    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.viber');
    $userContactType->save();

    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.skype');
    $userContactType->save();

    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.telegram');
    $userContactType->save();

    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.vkontakte');
    $userContactType->save();

    $userContactType = new UserContactType();
    $userContactType->name = Loc::getMessage('wizard.services.intecInsider.base.usersContactsTypes.odnoklassniki');
    $userContactType->save();
}

?>
<? include(__DIR__.'/../.end.php') ?>