<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arServices = [
    'main' => [
        'NAME' => Loc::getMessage('wizard.services.main'),
        'STAGES' => [
            'files.php',
            'menu.php',
            'template.php'
        ]
    ],
    'intec.insider' => [
        'NAME' => Loc::getMessage('wizard.services.insider'),
        'STAGES' => [
            'base.php'
        ]
    ]
];
