<? include(__DIR__.'/../.begin.php') ?>
<?

use intec\core\base\Collection;
use intec\core\helpers\FileHelper;

/**
 * @var Collection $data
 * @var CWizardBase $wizard
 * @var CWizardStep $this
 */

$macros['SITE_DIR'] = WIZARD_SITE_DIR;
$macros['SITE_DIR_MACROS'] = WIZARD_SITE_DIR;

$pathFrom = FileHelper::normalizePath(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID, '/').'/';
$pathTo = FileHelper::normalizePath(WIZARD_SITE_PATH, '/').'/';

if (FileHelper::isDirectory($pathFrom)) {
    CopyDirFiles($pathFrom, $pathTo, $rewrite = true, $recursive = true, $remove = false);
    CWizardUtil::ReplaceMacrosRecursive($pathTo, $macros);
    CWizardUtil::ReplaceMacros($pathTo.'_index.php', $macros);
}

$data->set('macros', $macros);
?>
<? include(__DIR__.'/../.end.php') ?>