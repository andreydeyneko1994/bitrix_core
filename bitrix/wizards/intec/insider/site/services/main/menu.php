<? include(__DIR__.'/../.begin.php') ?>
<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use intec\core\base\Collection;

/**
 * @var Collection $data
 * @var string $mode
 * @var CWizardBase $wizard
 * @var CWizardStep $this
 */

Loader::includeModule('fileman');
Loc::loadMessages(__FILE__);

$arMenuTypes = GetMenuTypes(WIZARD_SITE_ID);
$arMenuTypes['left'] = Loc::getMessage('wizard.services.main.menu.menu.left');
SetMenuTypes($arMenuTypes, WIZARD_SITE_ID);

?>
<? include(__DIR__.'/../.end.php') ?>