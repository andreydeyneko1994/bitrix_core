<? include(__DIR__.'/../.begin.php') ?>
<?

use Bitrix\Main\Localization\Loc;
use intec\core\base\Collection;
use intec\core\helpers\FileHelper;
use intec\core\io\Path;

/**
 * @var Collection $data
 * @var CWizardBase $wizard
 * @var CWizardStep $this
 */

Loc::loadMessages(__FILE__);

$templateId = WIZARD_TEMPLATE_ID.'_'.WIZARD_SITE_ID;
$templatePath = Path::from('@root/'.WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH.'/site').'/'.WIZARD_TEMPLATE_ID);
$templatePathTo = Path::from('@root/'.BX_PERSONAL_ROOT.'/templates/'.$templateId);
$macros = $data->get('macros');

unset($macros['SITE_DIR']);

if (FileHelper::isDirectory($templatePath->getValue('/'))) {
    CopyDirFiles(
        $templatePath->getValue('/'),
        $templatePathTo->getValue('/'),
        $rewrite = true,
        $recursive = true,
        $remove = false
    );

    $template = CSiteTemplate::GetList([], [
        'ID' => $templateId
    ])->Fetch();

    if (empty($template))
        die(Loc::getMessage('wizard.services.main.template.errors.template'));

    $site = CSite::GetList($by = 'def', $order = 'desc', ['LID' => WIZARD_SITE_ID]);
    $site = $site->Fetch();

    if (!empty($site)) {
        (new CSite())->Update($site['ID'], [
            'NAME' => $wizard->GetVar('siteName'),
            'TEMPLATE' => [[
                'CONDITION' => '',
                'SORT' => 150,
                'TEMPLATE' => $templateId
            ]]
        ]);
    }
} else {
    die(Loc::getMessage('wizard.services.main.template.errors.template'));
}

?>
<? include(__DIR__.'/../.end.php') ?>