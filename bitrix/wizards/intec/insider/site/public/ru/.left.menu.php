<?
$aMenuLinks = Array(
    Array(
        "Статистика",
        "",
        Array(),
        Array(
            "type" => "title"
        ),
        ""
    ),
    Array(
        "Главная",
        "/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M3.5 10.1333C3.33431 10.1333 3.2 9.99902 3.2 9.83333V3.5C3.2 3.33431 3.33431 3.2 3.5 3.2H8.16667C8.33235 3.2 8.46667 3.33431 8.46667 3.5V9.83333C8.46667 9.99902 8.33235 10.1333 8.16667 10.1333H3.5ZM3.5 16.8C3.33431 16.8 3.2 16.6657 3.2 16.5V13.5C3.2 13.3343 3.33431 13.2 3.5 13.2H8.16667C8.33235 13.2 8.46667 13.3343 8.46667 13.5V16.5C8.46667 16.6657 8.33235 16.8 8.16667 16.8H3.5ZM11.8333 16.8C11.6676 16.8 11.5333 16.6657 11.5333 16.5V10.1667C11.5333 10.001 11.6676 9.86667 11.8333 9.86667H16.5C16.6657 9.86667 16.8 10.001 16.8 10.1667V16.5C16.8 16.6657 16.6657 16.8 16.5 16.8H11.8333ZM11.5333 3.5C11.5333 3.33431 11.6676 3.2 11.8333 3.2H16.5C16.6657 3.2 16.8 3.33431 16.8 3.5V6.5C16.8 6.66569 16.6657 6.8 16.5 6.8H11.8333C11.6676 6.8 11.5333 6.66569 11.5333 6.5V3.5Z\" stroke-width=\"1.4\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Сотрудники",
        "/statistics/users/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M6.81154 6.11106C6.81154 4.34905 8.23842 2.92217 10.0004 2.92217C11.7624 2.92217 13.1893 4.34904 13.1893 6.11106C13.1893 7.87307 11.7624 9.29995 10.0004 9.29995C8.23842 9.29995 6.81154 7.87307 6.81154 6.11106ZM2.92266 14.8611C2.92266 14.4716 3.11218 14.0787 3.54765 13.6768C3.98788 13.2705 4.63116 12.904 5.39842 12.5966C6.93407 11.9814 8.79002 11.6722 10.0004 11.6722C11.2108 11.6722 13.0668 11.9814 14.6025 12.5966C15.3697 12.904 16.013 13.2705 16.4532 13.6768C16.8887 14.0787 17.0782 14.4716 17.0782 14.8611V17.0777H2.92266V14.8611Z\" stroke-width=\"1.4\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Программы",
        "/statistics/applications/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M2.50033 3.2H16.667C16.7387 3.2 16.8003 3.2616 16.8003 3.33333V8.33333C16.8003 8.40507 16.7387 8.46667 16.667 8.46667H2.50033C2.42859 8.46667 2.36699 8.40507 2.36699 8.33333V3.33333C2.36699 3.2616 2.42859 3.2 2.50033 3.2ZM2.50033 11.5333H16.667C16.7387 11.5333 16.8003 11.5949 16.8003 11.6667V16.6667C16.8003 16.7384 16.7387 16.8 16.667 16.8H2.50033C2.42859 16.8 2.36699 16.7384 2.36699 16.6667V11.6667C2.36699 11.5949 2.42859 11.5333 2.50033 11.5333Z\" stroke-width=\"1.4\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Табель",
        "/statistics/schedule/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M13.3337 1.6665V4.99984\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M6.66667 1.6665V4.99984\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M2.5 7.49967H17.5\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15.8333 3.33301H4.16667C3.24583 3.33301 2.5 4.07884 2.5 4.99967V15.833C2.5 16.7538 3.24583 17.4997 4.16667 17.4997H15.8333C16.7542 17.4997 17.5 16.7538 17.5 15.833V4.99967C17.5 4.07884 16.7542 3.33301 15.8333 3.33301Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M7.49967 7.5V17.5\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M12.4997 7.5V17.5\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M2.5 12.4997H17.5\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Календарь",
        "/statistics/calendar/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M13.3337 1.6665V4.99984\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M6.66667 1.6665V4.99984\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M2.5 7.49967H17.5\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15.8333 3.33301H4.16667C3.24583 3.33301 2.5 4.07884 2.5 4.99967V15.833C2.5 16.7538 3.24583 17.4997 4.16667 17.4997H15.8333C16.7542 17.4997 17.5 16.7538 17.5 15.833V4.99967C17.5 4.07884 16.7542 3.33301 15.8333 3.33301Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M8.83301 11.5165L10.0497 10.4165\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M8.93359 14.5832H11.1669\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M10.0505 10.4165V14.5832\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Инциденты",
        "/statistics/incidents/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M9.99967 10.9336V7.81689\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M9.99884 13.4373C9.88384 13.4373 9.79051 13.5307 9.79134 13.6457C9.79134 13.7607 9.88468 13.854 9.99968 13.854C10.1147 13.854 10.208 13.7607 10.208 13.6457C10.208 13.5307 10.1147 13.4373 9.99884 13.4373\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M11.691 3.44916L18.0735 14.6192C18.8151 15.9175 17.8776 17.5333 16.3826 17.5333H3.61763C2.12179 17.5333 1.18429 15.9175 1.92679 14.6192L8.30929 3.44916C9.05679 2.13999 10.9435 2.13999 11.691 3.44916Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Учет рабочего времени",
        "/statistics/visits/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M14.7137 6.11912C17.3172 8.72261 17.3172 12.9437 14.7137 15.5472C12.1102 18.1507 7.88912 18.1507 5.28563 15.5472C2.68213 12.9437 2.68213 8.72261 5.28563 6.11912C7.88912 3.51563 12.1102 3.51563 14.7137 6.11912\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M9.99967 7.5V10.8333\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M8.33301 1.66667H11.6663\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M17.4997 5.00016L15.833 3.3335L16.6664 4.16683L14.7139 6.11933\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "",
        "",
        Array(),
        Array(
            "type" => "delimiter"
        ),
        ""
    ),
    Array(
        "Настройка",
        "",
        Array(),
        Array(
            "type" => "title"
        ),
        ""
    ),
    Array(
        "Сотрудники",
        "/settings/users/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M6.81154 6.11106C6.81154 4.34905 8.23842 2.92217 10.0004 2.92217C11.7624 2.92217 13.1893 4.34904 13.1893 6.11106C13.1893 7.87307 11.7624 9.29995 10.0004 9.29995C8.23842 9.29995 6.81154 7.87307 6.81154 6.11106ZM2.92266 14.8611C2.92266 14.4716 3.11218 14.0787 3.54765 13.6768C3.98788 13.2705 4.63116 12.904 5.39842 12.5966C6.93407 11.9814 8.79002 11.6722 10.0004 11.6722C11.2108 11.6722 13.0668 11.9814 14.6025 12.5966C15.3697 12.904 16.013 13.2705 16.4532 13.6768C16.8887 14.0787 17.0782 14.4716 17.0782 14.8611V17.0777H2.92266V14.8611Z\" stroke-width=\"1.4\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Отделы",
        "/settings/departments/",
        Array(),
        Array(
            "icon" => "<svg width=\"22\" height=\"22\" viewBox=\"0 0 22 22\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M14.6124 18.5031H4.88663C4.11881 18.5031 3.49689 17.8812 3.49689 17.1134V7.38767C3.49689 6.61985 4.11881 5.99792 4.88663 5.99792H14.6132C15.3802 5.99792 16.0021 6.61985 16.0021 7.38767V17.1142C16.0021 17.8812 15.3802 18.5031 14.6124 18.5031Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M11.2234 9.47104C12.0371 10.2847 12.0371 11.6044 11.2234 12.4189C10.4098 13.2326 9.09005 13.2326 8.27555 12.4189C7.46104 11.6053 7.46187 10.2855 8.27555 9.47104C9.08922 8.65653 10.4089 8.65737 11.2234 9.47104\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M13.501 16.3638C13.3918 16.0895 13.2226 15.8428 13.0058 15.6418V15.6418C12.6399 15.3017 12.1605 15.1133 11.6603 15.1133C10.8266 15.1133 8.67237 15.1133 7.83869 15.1133C7.33848 15.1133 6.85995 15.3025 6.49313 15.6418V15.6418C6.27637 15.8428 6.10714 16.0895 5.99792 16.3638\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M5.99792 5.99787V4.88657C5.99792 4.11875 6.61985 3.49683 7.38767 3.49683H17.1142C17.8812 3.49683 18.5031 4.11875 18.5031 4.88657V14.6131C18.5031 15.3801 17.8812 16.002 17.1134 16.002H16.0021\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Должности",
        "/settings/positions/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M8.60095 7.45765C9.21114 8.06784 9.21114 9.05716 8.60095 9.66736C7.99075 10.2775 7.00143 10.2775 6.39124 9.66736C5.78105 9.05716 5.78105 8.06784 6.39124 7.45765C7.00143 6.84745 7.99075 6.84745 8.60095 7.45765\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M2 14.5882V5.44071C2 4.64518 2.7769 4 3.73485 4H17.3C18.2393 4 19 4.63176 19 5.41176V14.5882C19 15.3682 18.2393 16 17.3 16H3.7C2.76075 16 2 15.3682 2 14.5882Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M12.416 8.59722H15.1938\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M14.0827 11.3751H12.416\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M9.99306 13.2502C9.87153 12.9454 9.68403 12.6717 9.44306 12.4488V12.4488C9.03542 12.0711 8.50069 11.8613 7.94514 11.8613H7.04792C6.49236 11.8613 5.95764 12.0711 5.55 12.4488V12.4488C5.30903 12.6717 5.12153 12.9454 5 13.2502\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Программы",
        "/settings/applications/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M2.50033 3.2H16.667C16.7387 3.2 16.8003 3.2616 16.8003 3.33333V8.33333C16.8003 8.40507 16.7387 8.46667 16.667 8.46667H2.50033C2.42859 8.46667 2.36699 8.40507 2.36699 8.33333V3.33333C2.36699 3.2616 2.42859 3.2 2.50033 3.2ZM2.50033 11.5333H16.667C16.7387 11.5333 16.8003 11.5949 16.8003 11.6667V16.6667C16.8003 16.7384 16.7387 16.8 16.667 16.8H2.50033C2.42859 16.8 2.36699 16.7384 2.36699 16.6667V11.6667C2.36699 11.5949 2.42859 11.5333 2.50033 11.5333Z\" stroke-width=\"1.4\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Группы программ",
        "/settings/applications/groups/",
        Array(),
        Array(
            "icon" => "<svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M6.45833 8.125H4.16667C3.24583 8.125 2.5 7.37917 2.5 6.45833V4.16667C2.5 3.24583 3.24583 2.5 4.16667 2.5H6.45833C7.37917 2.5 8.125 3.24583 8.125 4.16667V6.45833C8.125 7.37917 7.37917 8.125 6.45833 8.125Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15.8333 8.125H13.5417C12.6208 8.125 11.875 7.37917 11.875 6.45833V4.16667C11.875 3.24583 12.6208 2.5 13.5417 2.5H15.8333C16.7542 2.5 17.5 3.24583 17.5 4.16667V6.45833C17.5 7.37917 16.7542 8.125 15.8333 8.125Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M6.45833 17.5H4.16667C3.24583 17.5 2.5 16.7542 2.5 15.8333V13.5417C2.5 12.6208 3.24583 11.875 4.16667 11.875H6.45833C7.37917 11.875 8.125 12.6208 8.125 13.5417V15.8333C8.125 16.7542 7.37917 17.5 6.45833 17.5Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15.8333 17.5H13.5417C12.6208 17.5 11.875 16.7542 11.875 15.8333V13.5417C11.875 12.6208 12.6208 11.875 13.5417 11.875H15.8333C16.7542 11.875 17.5 12.6208 17.5 13.5417V15.8333C17.5 16.7542 16.7542 17.5 15.8333 17.5Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Отвлечения",
        "/settings/distractions/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M9.78027 6.84912V10.4666L12.6239 12.2003\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M11.5556 3.1552C10.1882 2.84175 8.71589 2.92964 7.3221 3.50754C3.75127 4.98689 2.05569 9.08039 3.53427 12.6512C5.01284 16.222 9.10712 17.9176 12.6779 16.439C15.8793 15.1129 17.5656 11.686 16.8158 8.41771\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M14.667 2.19434H17.0004L14.667 5.30549H17.0004\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Расписание",
        "/settings/schedules/",
        Array(),
        Array(
            "icon" => "<svg viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path d=\"M7.91748 10H14.1666\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M7.91748 12.9034H14.1666\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M7.91748 7.06999H14.1666\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M5.87506 7.02839C5.85173 7.02839 5.8334 7.04756 5.8334 7.07089C5.8334 7.09423 5.85256 7.11256 5.8759 7.11256C5.89923 7.11256 5.9184 7.09339 5.9184 7.07089C5.91756 7.04756 5.8984 7.02839 5.87506 7.02839\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M5.87506 9.95754C5.85173 9.95754 5.8334 9.97671 5.8334 10C5.8334 10.0234 5.85256 10.0425 5.8759 10.0425C5.89923 10.0425 5.9184 10.0234 5.9184 10C5.9184 9.97671 5.8984 9.95754 5.87506 9.95754\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M5.87506 12.8616C5.85173 12.8616 5.8334 12.8808 5.8334 12.9041C5.8334 12.9275 5.85256 12.9458 5.8759 12.9458C5.89923 12.9458 5.9184 12.9266 5.9184 12.9041C5.91756 12.8808 5.8984 12.8616 5.87506 12.8616\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M17.5 5.83333V14.1667C17.5 16.0075 16.0075 17.5 14.1667 17.5H5.83333C3.9925 17.5 2.5 16.0075 2.5 14.1667V5.83333C2.5 3.9925 3.9925 2.5 5.83333 2.5H14.1667C16.0075 2.5 17.5 3.9925 17.5 5.83333Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    ),
    Array(
        "Агенты",
        "/settings/agents/",
        Array(),
        Array(
            "icon" => "<svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">
<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M2.59831 10.3891C2.46747 10.1466 2.46747 9.85246 2.59831 9.60996C4.17497 6.69413 7.08747 4.16663 9.99997 4.16663C12.9125 4.16663 15.825 6.69413 17.4016 9.61079C17.5325 9.85329 17.5325 10.1475 17.4016 10.39C15.825 13.3058 12.9125 15.8333 9.99997 15.8333C7.08747 15.8333 4.17497 13.3058 2.59831 10.3891Z\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
<path d=\"M11.7677 8.23223C12.744 9.20854 12.744 10.7915 11.7677 11.7678C10.7914 12.7441 9.20851 12.7441 8.2322 11.7678C7.25589 10.7915 7.25589 9.20854 8.2322 8.23223C9.20851 7.25592 10.7914 7.25592 11.7677 8.23223\" stroke-width=\"1.5\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>
</svg>
"
        ),
        ""
    )
);
?>