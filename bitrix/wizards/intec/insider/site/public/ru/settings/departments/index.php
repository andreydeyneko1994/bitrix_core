<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Настройки - Отделы");

/**
 * @global $APPLICATION
 */

$APPLICATION->SetTitle("Настройки - Отделы");

?>
<?php $APPLICATION->IncludeComponent('intec.insider:section', 'settings.departments', Array(

), false); ?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php") ?>
