<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Настройки - Приложения");

/**
 * @global $APPLICATION
 */

$APPLICATION->SetTitle("Настройки - Группы приложений");

?>
<?php $APPLICATION->IncludeComponent('intec.insider:section', 'settings.applications.groups', Array(

), false); ?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php") ?>
