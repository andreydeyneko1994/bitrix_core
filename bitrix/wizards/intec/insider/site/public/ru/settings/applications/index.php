<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Настройки - Приложения");

/**
 * @global $APPLICATION
 */

$APPLICATION->SetTitle("Настройки - Приложения");

?>
<?php $APPLICATION->IncludeComponent('intec.insider:section', 'settings.applications', Array(

), false); ?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php") ?>
