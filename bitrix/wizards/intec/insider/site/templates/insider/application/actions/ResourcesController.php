<?php
namespace intec\insider\application\actions;

use CFile;
use intec\core\helpers\Type;
use intec\core\web\UploadedFile;

class ResourcesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action, $parameters)
    {
        global $USER;

        if (parent::beforeAction($action, $parameters))
            return $USER->IsAdmin();

        return false;
    }

    public function actionLoadPicture($id)
    {
        if (empty($id))
            return $this->errorResponse('pictureIdUndefined', 'Picture id undefined', [
                'id' => $id
            ]);

        $result = CFile::GetFileArray($id);

        if (!$result)
            return $this->errorResponse('pictureNotFound', 'Picture not found', [
                'id' => $id
            ]);

        return $this->successResponse($result['SRC']);
    }

    public function actionRemovePicture($id)
    {
        if (empty($id))
            return $this->errorResponse('pictureIdUndefined', 'Picture id undefined', [
                'id' => $id
            ]);

        CFile::Delete($id);

        return $this->successResponse();
    }

    public function actionUploadPicture($files)
    {
        if (!Type::isArray($files) || count($files) !== 1)
            return $this->errorResponse('pictureUploadFailed', 'Picture upload failed');

        /** @var UploadedFile $file */
        $file = $files[0];
        $fileArray = [
            'name' => $file->name,
            'size' => $file->size,
            'tmp_name' => $file->tempName,
            'type' => $file->type,
            'MODULE_ID' => 'intec.insider'
        ];

        if (CFile::CheckImageFile($fileArray) !== null)
            return $this->errorResponse('pictureFileIncorrect', 'Picture file incorrect');

        $fileId = CFile::SaveFile($fileArray, 'insider');

        if (!Type::isInteger($fileId))
            return $this->errorResponse('pictureUploadFailed', 'Picture upload failed');

        $result = CFile::GetFileArray($fileId);

        if (!$result)
            return $this->errorResponse('pictureUploadFailed', 'Picture upload failed');

        return $this->successResponse([
            'id' => $fileId,
            'source' => $result['SRC']
        ]);
    }
}