<?php
namespace intec\insider\application\actions;

use intec\core\db\ActiveQuery;
use intec\core\db\ActiveRecord;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Type;

class RecordController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action, $parameters)
    {
        global $USER;

        if (parent::beforeAction($action, $parameters))
            return $USER->IsAdmin();

        return false;
    }

    public function getClass($model)
    {
        $class = '\\intec\\insider\\models\\'.$model;

        if (!class_exists($class) || !is_subclass_of($class, ActiveRecord::className()))
            $class = null;

        return $class;
    }

    /**
     * Действие. Возвращает количество записей в базе данных.
     * @param string $model
     * @param array $where
     * @param integer $limit
     * @param integer $offset
     * @param array $orderBy
     * @return integer
     */
    public function actionCount($model, $where, $limit, $offset, $orderBy)
    {
        $class = $this->getClass($model);

        if ($class === null)
            return $this->errorResponse('unresolvedModel', 'Unresolved model', $model);

        /** @var ActiveQuery $query */
        $query = $class::find();

        if (!empty($where))
            $query->where($where);

        if (!empty($limit))
            $query->limit($limit);

        if (!empty($offset))
            $query->offset($offset);

        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $direction)
                $orderBy[$field] = $direction === 'desc' ? SORT_DESC : SORT_ASC;

            $query->orderBy($orderBy);
        }

        return $this->successResponse($query->count());
    }

    /**
     * Действие. Ищет запись в базе данных.
     * @param string $model
     * @param array $where
     * @param integer $limit
     * @param integer $offset
     * @param array $orderBy
     * @return array
     */
    public function actionFind($model, $where, $limit, $offset, $orderBy)
    {
        $result = [];
        $class = $this->getClass($model);

        if ($class === null)
            return $this->errorResponse('unresolvedModel', 'Unresolved model', $model);

        /** @var ActiveQuery $query */
        $query = $class::find();

        if (!empty($where))
            $query->where($where);

        if (!empty($limit))
            $query->limit($limit);

        if (!empty($offset))
            $query->offset($offset);

        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $direction)
                $orderBy[$field] = $direction === 'desc' ? SORT_DESC : SORT_ASC;

            $query->orderBy($orderBy);
        }

        $records = $query->all(null, false);

        foreach ($records as $record)
            $result[] = $record->getAttributes();

        return $this->successResponse($result);
    }

    /**
     * Действие. Обновляет запись из базы данных.
     * @param string $model
     * @param array $fields
     * @return array
     */
    public function actionRefresh($model, $fields)
    {
        $class = $this->getClass($model);

        if ($class === null)
            return $this->errorResponse('unresolvedModel', 'Unresolved model', $model);

        if (empty($fields) || !Type::isArray($fields))
            return $this->errorResponse('emptyFields', 'Empty fields');

        /** @var $record ActiveRecord $record */
        $record = new $class();

        foreach ($fields as $key => $value) {
            if ($record->hasAttribute($key))
                $record->setAttribute($key, $value);
        }

        $keyFields = $record->getPrimaryKey(true);
        $isValidKey = !empty($keyFields);
        $record = null;

        foreach ($keyFields as $key => $value) {
            if ($value === null) {
                $isValidKey = false;
                break;
            }
        }

        if ($isValidKey)
            $record = $class::find()->where($keyFields)->one();

        if (empty($record))
            return $this->errorResponse('recordNotFound', 'Record not found', $isValidKey ? $keyFields : null);

        return $this->successResponse($record->getAttributes());
    }

    /**
     * Действие. Удаляет запись из базы данных.
     * @param string $model
     * @param array $fields
     * @return array
     */
    public function actionRemove($model, $fields)
    {
        $class = $this->getClass($model);

        if ($class === null)
            return $this->errorResponse('unresolvedModel', 'Unresolved model', $model);

        if (empty($fields) || !Type::isArray($fields))
            return $this->errorResponse('emptyFields', 'Empty fields');

        /** @var $record ActiveRecord $record */
        $record = new $class();

        foreach ($fields as $key => $value) {
            if ($record->hasAttribute($key))
                $record->setAttribute($key, $value);
        }

        $keyFields = $record->getPrimaryKey(true);
        $isValidKey = !empty($keyFields);
        $record = null;

        foreach ($keyFields as $key => $value) {
            if ($value === null) {
                $isValidKey = false;
                break;
            }
        }

        if ($isValidKey) {
            $record = $class::find()->where($keyFields)->one();

            if (!empty($record))
                $record->delete();
        }

        return $this->successResponse();
    }

    /**
     * Действие. Сохраняет запись в базу данных.
     * @param string $model
     * @param array $fields
     * @return array
     */
    public function actionSave($model, $fields)
    {
        $class = $this->getClass($model);

        if ($class === null)
            return $this->errorResponse('unresolvedModel', 'Unresolved model', $model);

        if (empty($fields) || !Type::isArray($fields))
            return $this->errorResponse('emptyFields', 'Empty fields');

        /** @var $record ActiveRecord $record */
        $record = new $class();

        foreach ($fields as $key => $value) {
            if ($record->hasAttribute($key))
                $record->setAttribute($key, $value);
        }

        $keyFields = $record->getPrimaryKey(true);
        $isValidKey = !empty($keyFields);

        foreach ($keyFields as $key => $value) {
            if ($value === null) {
                $isValidKey = false;
                break;
            }
        }

        if ($isValidKey) {
            $foundedRecord = $class::find()->where($keyFields)->one();

            if ($foundedRecord) {
                $record = $foundedRecord;

                foreach ($fields as $key => $value) {
                    if ($record->hasAttribute($key))
                        $record->setAttribute($key, $value);
                }
            }
        }

        if (!$record->save()) {
            $error = $record->getFirstErrors();
            $error = ArrayHelper::getFirstValue($error);

            return $this->errorResponse('save', $error);
        }

        return $this->successResponse($record->getAttributes());
    }
}
