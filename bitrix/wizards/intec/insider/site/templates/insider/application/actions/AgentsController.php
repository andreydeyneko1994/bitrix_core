<?php
namespace intec\insider\application\actions;

use intec\core\helpers\ArrayHelper;
use intec\insider\models\Agent;

class AgentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action, $parameters)
    {
        global $USER;

        if (parent::beforeAction($action, $parameters))
            return $USER->IsAdmin();

        return false;
    }

    /**
     * Действие. Создает агента с ключем.
     * @return array
     */
    public function actionCreate($userId = null)
    {
        $agent = new Agent();
        $agent->generateKey();

        if (!empty($userId))
            $agent->userId = $userId;

        if (!$agent->save()) {
            $error = $agent->getFirstErrors();
            $error = ArrayHelper::getFirstValue($error);

            return $this->errorResponse('save', $error);
        }

        return $this->successResponse($agent->getAttributes());
    }
}
