<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader;
use intec\Core;
use intec\core\base\InvalidParamException;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Encoding;
use intec\core\helpers\Json;
use intec\core\helpers\Type;
use intec\core\web\UploadedFile;
use intec\insider\handling\controllers\Router;

if (!Loader::includeModule('intec.core'))
    exit;

if (!Loader::includeModule('intec.insider'))
    exit;

ini_set('max_execution_time', 0);

include(__DIR__.'/actions/Controller.php');

$request = Core::$app->request;
$router = new Router();
$router->directory = __DIR__.'/actions';
$router->namespace = '\intec\insider\application\actions';
$router->separator = '.';

$actions = $request->post('actions');

if (!Type::isArray($actions))
    exit;

$result = [];

foreach ($actions as $index => $action) {
    $route = ArrayHelper::getValue($action, 'route');
    $parameters = ArrayHelper::getValue($action, 'parameters');

    if (empty($route) && !Type::isNumeric($route)) {
        $result[] = [
            'code' => 'badAction',
            'data' => null,
            'message' => 'Bad action',
            'status' => 'error'
        ];

        continue;
    }

    if (!empty($parameters)) {
        try {
            $parameters = Json::decode($parameters);
            $parameters = Encoding::convert($parameters, null, Encoding::UTF8);
        } catch (InvalidParamException $exception) {
            $parameters = [];
        }
    } else {
        $parameters = [];
    }

    try {
        $route = $router->resolve($route);

        if ($route !== null) {
            if (!isset($parameters['files'])) {
                $files = UploadedFile::getInstancesByName('actions[' . $index . '][files]');
                $parameters['files'] = $files;
            }

            $result[] = $route->run($parameters);
        } else {
            $result[] = [
                'code' => 'unresolvedAction',
                'data' => [
                    'route' => $route,
                    'parameters' => $parameters
                ],
                'message' => 'Unresolved action',
                'status' => 'error'
            ];
        }
    } catch (Exception $exception) {
        $result[] = [
            'code' => 'actionException',
            'data' => [
                'exception' => $exception->getMessage(),
                'parameters' => $parameters
            ],
            'message' => 'Action exception',
            'status' => 'error'
        ];
    }
}

$result = Encoding::convert($result, Encoding::UTF8, Encoding::getDefault());
$result = Json::encode($result);

echo $result;
exit;
