<?php
namespace intec\insider\application\actions;

abstract class Controller extends \intec\insider\handling\controllers\Controller
{
    /**
     * Отправляет успешный ответ.
     * @param mixed|null $data Данные.
     * @return array
     */
    public function successResponse($data = null)
    {
        return [
            'status' => 'success',
            'data' => $data
        ];
    }

    /**
     * Отправляет ответ с ошибкой.
     * @param string|null $code
     * @param string|null $message
     * @param mixed|null $data
     * @return array
     */
    public function errorResponse($code = null, $message = null, $data = null)
    {
        return [
            'status' => 'error',
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }
}