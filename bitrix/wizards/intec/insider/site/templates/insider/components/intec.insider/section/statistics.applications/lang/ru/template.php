<?php

$MESS['c.section.statistics.applications.title'] = 'Статистика приложений';
$MESS['c.section.statistics.applications.charts.worked.title'] = 'Отработано';
$MESS['c.section.statistics.applications.charts.productive.title'] = 'Продуктивных часов';
$MESS['c.section.statistics.applications.charts.idle.title'] = 'Простой';
$MESS['c.section.statistics.applications.charts.distractions.title'] = 'Отвлечения';
$MESS['c.section.statistics.applications.fields.application'] = 'Приложение';
$MESS['c.section.statistics.applications.fields.statistics'] = 'Статистика';
$MESS['c.section.statistics.applications.popular.title'] = 'Популярные';
$MESS['c.section.statistics.applications.disallowed.title'] = 'Запрещенные';
$MESS['c.section.statistics.applications.history.time'] = 'Время';
$MESS['c.section.statistics.applications.history.productive'] = 'Продуктивное:';
$MESS['c.section.statistics.applications.history.distractions'] = 'Отвлечения:';
