<?php

$MESS['c.section.statistics.users.title'] = 'Статистика сотрудников';
$MESS['c.section.statistics.users.charts.worked.title'] = 'Отработано';
$MESS['c.section.statistics.users.charts.productive.title'] = 'Продуктивных часов';
$MESS['c.section.statistics.users.charts.idle.title'] = 'Простой';
$MESS['c.section.statistics.users.charts.distractions.title'] = 'Отвлечения';
$MESS['c.section.statistics.users.fields.initials'] = 'Сотрудник';
$MESS['c.section.statistics.users.fields.statistics'] = 'Статистика';
$MESS['c.section.statistics.users.fields.lateness'] = 'Опоздания';
$MESS['c.section.statistics.users.fields.absence'] = 'Прогулы';
$MESS['c.section.statistics.users.fields.earlyDeparture'] = 'Ранний уход';
$MESS['c.section.statistics.users.fields.incidents'] = 'Инцидентов';
$MESS['c.section.statistics.users.history.worked'] = 'Отработано';
$MESS['c.section.statistics.users.history.productive'] = 'Продуктивно:';
$MESS['c.section.statistics.users.history.idle'] = 'Простой:';
$MESS['c.section.statistics.users.history.distractions'] = 'Отвлечения:';
