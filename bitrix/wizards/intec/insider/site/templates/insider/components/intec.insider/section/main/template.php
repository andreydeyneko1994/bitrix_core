<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);
$links = [
    'applications' => SITE_DIR.'statistics/applications/',
    'incidents' => SITE_DIR.'statistics/incidents/',
    'schedule' => SITE_DIR.'statistics/schedule/',
    'users' => SITE_DIR.'statistics/users/',
    'visits' => SITE_DIR.'statistics/visits/'
];

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'absenceStatistics': function () {
                    return this.$statistics.createReports(this.reports).getAbsenceEventsStatistics();
                },
                'applicationsStatisticsItems': function () {
                    var self = this;
                    var distractions = self.applicationsStatisticsType === 'distractions';
                    var reports = self.$statistics.createReports(self.reports);
                    var result = [];

                    _.each(self.applications, function (application) {
                        var item;

                        if (application.type !== 'application')
                            return;

                        item = {
                            'application': application,
                            'statistics': reports.getApplicationStatistics(application),
                            'value': null
                        };

                        if (!distractions) {
                            if (item.statistics.productive.asSeconds() > 0)
                                result.push(item);
                        } else {
                            if (item.statistics.distractions.asSeconds() > 0)
                                result.push(item);
                        }
                    });

                    result.sort(function (item1, item2) {
                        if (!distractions) {
                            return item2.statistics.productive.asSeconds() - item1.statistics.productive.asSeconds();
                        } else {
                            return item2.statistics.distractions.asSeconds() - item1.statistics.distractions.asSeconds();
                        }
                    });

                    result = result.slice(0, 5);

                    _.each(result, function (item) {
                        if (!distractions) {
                            item.value = item.statistics.productive.format('HH _', {
                                'precision': 2
                            });
                        } else {
                            item.value = item.statistics.distractions.format('HH _', {
                                'precision': 2
                            });
                        }
                    });

                    return result;
                },
                'applicationsStatisticsTypes': function () {
                    return [{
                        'name': this.$localization.message('c.section.main.applicationsStatistics.types.popular'),
                        'value': 'popular'
                    }, {
                        'name': this.$localization.message('c.section.main.applicationsStatistics.types.distractions'),
                        'value': 'distractions'
                    }];
                },
                'commonStatistics': function () {
                    return this.$statistics.createReports(this.reports).getCommonStatistics();
                },
                'commonStatisticsChart': function () {
                    var items = this.commonStatisticsChartItems;
                    var statistics = this.commonStatistics;
                    var result = {
                        'type': 'radialBar',
                        'options': {
                            'colors': [],
                            'chart': {
                                'height': '280px'
                            },
                            'labels': [],
                            'plotOptions': {
                                'radialBar': {
                                    'dataLabels': {
                                        'name': {
                                            'fontFamily': 'inherit',
                                            'fontSize': '12px',
                                            'fontWeight': 400,
                                            'offsetY': 20
                                        },
                                        'value': {
                                            'color': '#000',
                                            'fontFamily': 'inherit',
                                            'fontSize': '19px',
                                            'fontWeight': 600,
                                            'offsetY': -20
                                        },
                                        'total': {
                                            'show': true,
                                            'color': '#727982',
                                            'fontFamily': 'inherit',
                                            'fontSize': '12px',
                                            'fontWeight': 400,
                                            'label': this.$localization.message('c.section.main.commonStatistics.charts.total.title'),
                                            'formatter': function () {
                                                return statistics.productive.duration.format('HH _', {
                                                    'precision': 2
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        'series': []
                    };

                    _.each(items, function (item) {
                        result.options.colors.push(item.color);
                        result.options.labels.push(item.title);
                        result.series.push(item.value);
                    });

                    return result;
                },
                'commonStatisticsChartItems': function () {
                    var statistics = this.commonStatistics;
                    var result = [];

                    if (!this.isFilled)
                        return result;

                    result.push({
                        'color': '#f0b734',
                        'statistics': statistics.worked,
                        'title': this.$localization.message('c.section.main.commonStatistics.charts.worked.title'),
                        'value': (statistics.worked.duration.asSeconds() / statistics.worked.total.asSeconds()) * 100
                    });

                    result.push({
                        'color': '#bebebe',
                        'statistics': statistics.idle,
                        'title': this.$localization.message('c.section.main.commonStatistics.charts.idle.title'),
                        'value': (statistics.idle.duration.asSeconds() / statistics.idle.total.asSeconds()) * 100
                    });

                    result.push({
                        'color': '#00c294',
                        'statistics': statistics.productive,
                        'title': this.$localization.message('c.section.main.commonStatistics.charts.productive.title'),
                        'value': (statistics.productive.duration.asSeconds() / statistics.productive.total.asSeconds()) * 100
                    });

                    result.push({
                        'color': '#fc7d7d',
                        'statistics': statistics.distractions,
                        'title': this.$localization.message('c.section.main.commonStatistics.charts.distractions.title'),
                        'value': (statistics.distractions.duration.asSeconds() / statistics.distractions.total.asSeconds()) * 100
                    });

                    _.each(result, function (item) {
                        if (_.isNaN(item.value))
                            item.value = 0;

                        item.value = _.toNumber(item.value.toFixed(2));
                    });

                    return result;
                },
                'dateStart': function () {
                    return this.isFilled ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return this.isFilled ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'eventsStatisticsItems': function () {
                    var self = this;
                    var result = [];

                    _.each(self.reports, function (report) {
                        _.each(report.data.events, function (event) {
                            var item = _.find(result, function (item) {
                                if (item.type === 'custom')
                                    return item.title === event.name;

                                return item.type === event.type;
                            });

                            if (_.isNil(item)) {
                                item = {
                                    'duration': event.duration,
                                    'events': [],
                                    'theme': null,
                                    'title': event.name,
                                    'type': event.type
                                };

                                result.push(item);
                            }

                            item.events.push(event);
                        });
                    });

                    _.each(result, function (item) {
                        if (item.type === 'custom') {
                            item.theme = 'default-green';
                        } else {
                            if (item.type === 'absence' || item.type === 'lateness' || item.type === 'earlyDeparture') {
                                item.theme = 'default-red';
                            } else if (item.type === 'compensatoryHoliday') {
                                item.theme = 'default-orange';
                            } else if (item.type === 'sickLeave') {
                                item.theme = 'default-violet';
                            } else if (item.type === 'vacation') {
                                item.theme = 'default-blue';
                            }

                            item.title = self.$localization.message('c.section.main.eventsStatistics.types.' + item.type);
                        }
                    });

                    return result;
                },
                'incidentsStatisticsItems': function () {
                    var self = this;
                    var result = [];

                    _.each(self.incidents, function (incident) {
                        var item = {
                            'application': null,
                            'department': null,
                            'incident': incident,
                            'position': null,
                            'user': incident.getUser(self.users)
                        };

                        if (!_.isNil(item.user)) {
                            if (incident.type === 'application' || incident.type === 'site')
                                item.application = _.find(self.applications, function (application) {
                                    return application.id === incident.data;
                                });

                            item.department = item.user.getDepartment(self.departments);
                            item.position = item.user.getPosition(self.positions);

                            result.push(item);
                        }
                    });

                    return result;
                },
                'isCommonStatisticsFilled': function () {
                    return _.some(this.commonStatisticsChartItems, function (item) {
                        return item.value !== 0;
                    });
                },
                'isFilled': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd);
                },
                'latenessStatisticsItems': function () {
                    var self = this;
                    var result = [];

                    _.each(self.users, function (user) {
                        var statistics = self.$statistics.createReports(user.getReports(self.reports)).getAbsenceEventsStatistics();
                        var item = {
                            'department': null,
                            'position': null,
                            'user': user,
                            'duration': statistics.lateness.duration.asSeconds()
                        };

                        if (item.duration > 0) {
                            item.department = user.getDepartment(self.departments);
                            item.position = user.getPosition(self.positions);

                            result.push(item);
                        }
                    });

                    result.sort(function (item1, item2) {
                        return item2.duration - item1.duration;
                    });

                    _.each(result, function (item) {
                        item.duration = moment.duration(item.duration, 'seconds');
                    });

                    return result;
                },
                'sitesStatisticsItems': function () {
                    var self = this;
                    var distractions = self.sitesStatisticsType === 'distractions';
                    var reports = self.$statistics.createReports(self.reports);
                    var result = [];

                    _.each(self.applications, function (application) {
                        var item;

                        if (application.type !== 'website')
                            return;

                        item = {
                            'application': application,
                            'statistics': reports.getApplicationStatistics(application),
                            'value': null
                        };

                        if (!distractions) {
                            if (item.statistics.productive.asSeconds() > 0)
                                result.push(item);
                        } else {
                            if (item.statistics.distractions.asSeconds() > 0)
                                result.push(item);
                        }
                    });

                    result.sort(function (item1, item2) {
                        if (!distractions) {
                            return item2.statistics.productive.asSeconds() - item1.statistics.productive.asSeconds();
                        } else {
                            return item2.statistics.distractions.asSeconds() - item1.statistics.distractions.asSeconds();
                        }
                    });

                    result = result.slice(0, 5);

                    _.each(result, function (item) {
                        if (!distractions) {
                            item.value = item.statistics.productive.format('HH _', {
                                'precision': 2
                            });
                        } else {
                            item.value = item.statistics.distractions.format('HH _', {
                                'precision': 2
                            });
                        }
                    });

                    return result;
                },
                'sitesStatisticsTypes': function () {
                    return [{
                        'name': this.$localization.message('c.section.main.sitesStatistics.types.popular'),
                        'value': 'popular'
                    }, {
                        'name': this.$localization.message('c.section.main.sitesStatistics.types.distractions'),
                        'value': 'distractions'
                    }];
                }
            },
            'data': {
                'applications': [],
                'applicationsStatisticsType': 'popular',
                'departments': [],
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'usersId': []
                },
                'incidents': [],
                'links': <?= JavaScript::toObject($links) ?>,
                'positions': [],
                'range': 'day',
                'reports': [],
                'sitesStatisticsType': 'popular',
                'users': []
            },
            'methods': {
                'refresh': function () {
                    var self = this;
                    var complete;
                    var error;
                    var filters;

                    if (!self.isFilled) {
                        self.reset();

                        return;
                    }

                    filters = [
                        'and'
                    ];

                    if (self.filters.usersId.length > 0)
                        filters.push(['in', 'id', self.filters.usersId]);

                    complete = function (data) {
                        self.endLoading();

                        return data;
                    };

                    error = function (reason) {
                        self.reset();

                        return complete(reason);
                    };

                    self.beginLoading();

                    return self.$models.User.find({
                        'where': filters
                    }).run().then(function (users) {
                        var usersId;

                        if (users.length === 0) {
                            self.reset();

                            return complete();
                        }

                        usersId = _.map(users, function (user) {
                            return user.id;
                        });

                        return self.$actions.runBatch([
                            self.$models.Application.find(),
                            self.$models.Department.find(),
                            self.$models.Position.find(),
                            self.$models.UserReport.find({
                                'where': [
                                    'and',
                                    ['in', 'userId', usersId],
                                    ['>=', 'date', self.dateStart.format('YYYY-MM-DD HH:mm:ss')],
                                    ['<=', 'date', self.dateEnd.format('YYYY-MM-DD HH:mm:ss')]
                                ]
                            }),
                            self.$models.UserIncident.find({
                                'where': [
                                    'and',
                                    ['in', 'userId', usersId],
                                    ['in', 'type', ['absence', 'application', 'idle', 'site']],
                                    ['=', 'allowed', 0],
                                    ['=', 'accepted', 0]
                                ],
                                'limit': 10,
                                'orderBy': {
                                    'startDate': 'desc'
                                }
                            })
                        ]).then(function (responses) {
                            self.reset();
                            self.applications = responses[0].getData([]);
                            self.departments = responses[1].getData([]);
                            self.positions = responses[2].getData([]);
                            self.users = users;
                            self.reports = responses[3].getData([]);
                            self.incidents = responses[4].getData([]);

                            return complete();
                        }, error);
                    }, error);
                },
                'reset': function () {
                    this.applications = [];
                    this.departments = [];
                    this.incidents = [];
                    this.positions = [];
                    this.reports = [];
                    this.users = [];
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-template-layout" type="panel" vertical-scroll>
        <template v-slot:panel>
            <component is="v-layout-panel">
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch-calendar-range" v-model="range"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item" full></component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" multiple theme="panel" v-bind:values="filters.usersId" v-on:input-values="filters.usersId = $event"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-layout-panel" vertical>
                <component is="v-layout-panel-item">
                    <component is="v-layout-panel">
                        <component is="v-layout-panel-item" size="66.666666%">
                            <component is="v-layout-panel" vertical>
                                <component is="v-layout-panel-item">
                                    <component is="v-control-card" style="height: 307px;" v-bind:horizontal-scroll="false" v-bind:vertical-scroll="false">
                                        <template v-slot:title>
                                            {{ $localization.message('c.section.main.commonStatistics.title') }}
                                        </template>
                                        <template v-slot:default>
                                            <component
                                                is="v-fragment-users-reports-chart"
                                                show-scale
                                                v-if="reports.length > 0"
                                                v-bind:date="filters.dateStart"
                                                v-bind:range="range"
                                                v-bind:reports="reports"
                                                v-bind:scaled="range === 'day'"
                                            ></component>
                                            <component is="v-fragment-stub" v-else></component>
                                        </template>
                                    </component>
                                </component>
                                <component is="v-layout-panel-item">
                                    <component is="v-layout-panel">
                                        <component is="v-layout-panel-item" size="40%">
                                            <component is="v-control-card" footer-flat style="height: 428px;">
                                                <template v-slot:title>
                                                    {{ $localization.message('c.section.main.eventsStatistics.title') }}
                                                </template>
                                                <template v-slot:default>
                                                    <component is="v-layout-panel" indents="6" vertical v-if="eventsStatisticsItems.length > 0">
                                                        <component
                                                            is="v-layout-panel-item"
                                                            v-for="(item, index) in eventsStatisticsItems"
                                                            v-bind:key="index"
                                                        >
                                                            <component is="v-control-badge" v-bind:theme="item.theme">
                                                                {{ item.title + ': ' + item.events.length }}
                                                            </component>
                                                        </component>
                                                    </component>
                                                    <component is="v-fragment-stub" v-else></component>
                                                </template>
                                                <template v-slot:footer>
                                                    <component is="v-control-button" block theme="smooth-1" v-bind:href="links.schedule">
                                                        {{ $localization.message('c.section.main.commonStatistics.button.text') }}
                                                    </component>
                                                </template>
                                            </component>
                                        </component>
                                        <component is="v-layout-panel-item" size="60%">
                                            <component is="v-control-card" flat footer-flat style="height: 428px;">
                                                <template v-slot:title>
                                                    {{ $localization.message('c.section.main.latenessStatistics.title') }}
                                                </template>
                                                <template v-slot:default>
                                                    <template v-if="latenessStatisticsItems.length > 0">
                                                        <component
                                                            is="v-fragment-user-lateness-event"
                                                            v-for="(item, index) in latenessStatisticsItems"
                                                            v-bind="item"
                                                            v-bind:borderless="index === 0"
                                                            v-bind:key="index"
                                                        ></component>
                                                    </template>
                                                    <component is="v-fragment-stub" v-else></component>
                                                </template>
                                                <template v-slot:footer>
                                                    <component is="v-control-button" block theme="smooth-1" v-bind:href="links.visits">
                                                        {{ $localization.message('c.section.main.latenessStatistics.button.text') }}
                                                    </component>
                                                </template>
                                            </component>
                                        </component>
                                    </component>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" size="33.333333%">
                            <component is="v-control-card" flat footer-flat style="height: 755px;">
                                <template v-slot:title>
                                    {{ $localization.message('c.section.main.commonStatistics.title') }}
                                </template>
                                <template v-slot:default>
                                    <component is="apexchart" v-bind="commonStatisticsChart" style="height: 260px;"></component>
                                    <component is="v-fragment-chart-history" v-for="(item, index) in commonStatisticsChartItems" v-bind:key="index" v-bind:color="item.color">
                                        <template v-slot:title>
                                            {{ item.title }}
                                        </template>
                                        <template v-slot:additional-left>
                                            {{ item.statistics.duration.format('HH _ mm _ ss _') }}
                                        </template>
                                        <template v-slot:additional-right>
                                            {{ item.value + '%' }}
                                        </template>
                                    </component>
                                </template>
                                <template v-slot:footer>
                                    <component is="v-control-button" block theme="smooth-1" v-bind:href="links.users">
                                        {{ $localization.message('c.section.main.commonStatistics.button.text') }}
                                    </component>
                                </template>
                            </component>
                        </component>
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-layout-panel">
                        <component is="v-layout-panel-item" size="66.666666%">
                            <component is="v-control-card" flat footer-flat style="height: 889px;">
                                <template v-slot:title>
                                    {{ $localization.message('c.section.main.incidentsStatistics.title') }}
                                </template>
                                <template v-slot:default>
                                    <template v-if="incidentsStatisticsItems.length > 0">
                                        <component is="v-fragment-user-incident" v-for="(item, index) in incidentsStatisticsItems" v-bind="item" v-bind:borderless="index === 0" v-bind:key="index"></component>
                                    </template>
                                    <component is="v-fragment-stub" v-else></component>
                                </template>
                                <template v-slot:footer>
                                    <component is="v-control-button" block theme="smooth-1" v-bind:href="links.incidents">
                                        {{ $localization.message('c.section.main.incidentsStatistics.button.text') }}
                                    </component>
                                </template>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" size="33.333333%">
                            <component is="v-layout-panel" vertical>
                                <component is="v-layout-panel-item">
                                    <component is="v-control-card" flat footer-flat style="height: 434px;">
                                        <template v-slot:title>
                                            {{ $localization.message('c.section.main.applicationsStatistics.title') }}
                                        </template>
                                        <template v-slot:additional>
                                            <component is="v-control-list-switch" v-bind:items="applicationsStatisticsTypes" v-model="applicationsStatisticsType"></component>
                                        </template>
                                        <template v-slot:default>
                                            <component is="v-control-table" v-if="applicationsStatisticsItems.length > 0">
                                                <template v-slot:default>
                                                    <component
                                                        is="v-control-table-row"
                                                        v-for="(item, index) in applicationsStatisticsItems"
                                                        v-bind:key="index"
                                                    >
                                                        <component is="v-control-table-cell">
                                                            {{ item.application.name }}
                                                        </component>
                                                        <component is="v-control-table-cell" flexible nowrap>
                                                            {{ item.value }}
                                                        </component>
                                                    </component>
                                                </template>
                                            </component>
                                            <component is="v-fragment-stub" v-else></component>
                                        </template>
                                        <template v-slot:footer>
                                            <component is="v-control-button" block theme="smooth-1" v-bind:href="links.applications">
                                                {{ $localization.message('c.section.main.applicationsStatistics.button.text') }}
                                            </component>
                                        </template>
                                    </component>
                                </component>
                                <component is="v-layout-panel-item">
                                    <component is="v-control-card" flat footer-flat style="height: 434px;">
                                        <template v-slot:title>
                                            {{ $localization.message('c.section.main.sitesStatistics.title') }}
                                        </template>
                                        <template v-slot:additional>
                                            <component is="v-control-list-switch" v-bind:items="sitesStatisticsTypes" v-model="sitesStatisticsType"></component>
                                        </template>
                                        <template v-slot:default>
                                            <component is="v-control-table" v-if="sitesStatisticsItems.length > 0">
                                                <template v-slot:default>
                                                    <component
                                                        is="v-control-table-row"
                                                        v-for="(item, index) in sitesStatisticsItems"
                                                        v-bind:key="index"
                                                    >
                                                        <component is="v-control-table-cell">
                                                            {{ item.application.name }}
                                                        </component>
                                                        <component is="v-control-table-cell" flexible nowrap>
                                                            {{ item.value }}
                                                        </component>
                                                    </component>
                                                </template>
                                            </component>
                                            <component is="v-fragment-stub" v-else></component>
                                        </template>
                                        <template v-slot:footer>
                                            <component is="v-control-button" block theme="smooth-1" v-bind:href="links.applications">
                                                {{ $localization.message('c.section.main.sitesStatistics.button.text') }}
                                            </component>
                                        </template>
                                    </component>
                                </component>
                            </component>
                        </component>
                    </component>
                </component>
            </component>
        </template>
    </component>
</template>
