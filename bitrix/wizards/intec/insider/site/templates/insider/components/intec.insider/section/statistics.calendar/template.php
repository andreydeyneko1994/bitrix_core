<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'dateStart': function () {
                    return !_.isNil(this.filters.dateStart) ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return !_.isNil(this.filters.dateEnd) ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'isFilled': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd) && !_.isNil(this.filters.userId);
                },
                'month': function () {
                    return !_.isNil(this.dateStart) ? this.dateStart.month() : null;
                },
                'year': function () {
                    return !_.isNil(this.dateStart) ? this.dateStart.year() : null;
                }
            },
            'data': {
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'userId': null
                },
                'range': 'month',
                'user': null
            },
            'methods': {
                'openUserFilter': function () {
                    this.$refs.userFilterPopover.open();
                },
                'refresh': function () {
                    var self = this;

                    if (!self.isFilled) {
                        self.reset();

                        return;
                    }

                    self.beginLoading();

                    return self.$models.User.findOne({
                        'where': {
                            'id': self.filters.userId
                        }
                    }).run().then(function (user) {
                        self.user = user;
                        self.endLoading();

                        return user;
                    }, function (reason) {
                        self.endLoading();

                        return reason;
                    });
                },
                'reset': function () {
                    this.user = null;
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel" wrap>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" theme="panel" v-model="filters.userId"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-control-card" horizontal-scroll>
                <component
                    is="v-part-users-calendar"
                    v-if="isFilled && user"
                    v-bind:month="month"
                    v-bind:user-id="user.id"
                    v-bind:year="year"
                    v-on:refresh-start="beginLoading"
                    v-on:refresh-end="endLoading"
                ></component>
                <component is="v-fragment-stub" v-else></component>
            </component>
        </template>
    </component>
</template>
