<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsFilter', {
                    'resolveItem': function (item) {
                        return item.application.name;
                    },
                    'resolveItems': function () {
                        return this.items;
                    }
                })
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'dateStart': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'disallowedItems': function () {
                    var self = this;
                    var result = [];

                    _.each(self.items, function (item) {
                        if (item.statistics.distractions.asSeconds() > 0)
                            result.push(item);
                    });

                    result.sort(function (item1, item2) {
                        return item2.statistics.distractions.asSeconds() - item1.statistics.distractions.asSeconds();
                    });

                    return result.slice(0, 5);
                },
                'isDateFiltersSet': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd);
                },
                'items': function () {
                    var self = this;
                    var result = [];
                    var reports = self.$statistics.createReports(self.reports);

                    _.each(self.applications, function (application) {
                        var item = {
                            'application': application,
                            'statistics': reports.getApplicationStatistics(application)
                        };

                        if (item.statistics.total > 0)
                            result.push(item);
                    });

                    return result;
                },
                'popularItems': function () {
                    var self = this;
                    var result = [];

                    _.each(self.items, function (item) {
                        if (item.statistics.productive.asSeconds() > 0)
                            result.push(item);
                    });

                    result.sort(function (item1, item2) {
                        return item2.statistics.productive.asSeconds() - item1.statistics.productive.asSeconds();
                    });

                    return result.slice(0, 5);
                },
                'statistics': function () {
                    var self = this;
                    var reports = self.$statistics.createReports(self.reports);
                    var result = {
                        'applications': reports.getApplicationsStatistics(self.applications),
                        'common': reports.getCommonStatistics()
                    };

                    result.applications.count = self.applications.length;

                    return result;
                },
                'title': function () {
                    var result = this.$localization.message('c.section.statistics.applications.title');

                    if (this.isDateFiltersSet) {
                        if (this.range === 'day') {
                            result += ' ' + this.dateStart.format('DD MMM YYYY');
                        } else {
                            result += ' ' + this.dateStart.format('DD MMM YYYY') + ' - ' + this.dateEnd.format('DD MMM YYYY');
                        }
                    }

                    return result;
                }
            },
            'data': {
                'applications': [],
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'usersId': []
                },
                'range': 'day',
                'reports': []
            },
            'methods': {
                'getItemChartBarValues': function (item) {
                    return [{
                        'color': '#00c294',
                        'text': item.statistics.productive.format('H _ m _ s _', {
                            'trim': true
                        }),
                        'title': this.$localization.message('c.section.statistics.applications.history.productive'),
                        'value': item.statistics.productive.asSeconds()
                    }, {
                        'color': '#fc7d7d',
                        'text': item.statistics.distractions.format('H _ m _ s _', {
                            'trim': true
                        }),
                        'title': this.$localization.message('c.section.statistics.applications.history.distractions'),
                        'value': item.statistics.distractions.asSeconds()
                    }];
                },
                'getItemDescription': function (item) {
                    var result = [];

                    if (item.position)
                        result.push(item.position.name);

                    if (item.department)
                        result.push(item.department.name);

                    return _.join(result, ' / ');
                },
                'getItemPercents': function (item) {
                    var delimiter = this.statistics.applications.total.asSeconds();

                    if (delimiter === 0)
                        return '0.00%';

                    return ((item.statistics.total.asSeconds() / delimiter) * 100).toFixed(2) + '%';
                },
                'isItemHasDescription': function (item) {
                    return !!(item.department || item.position);
                },
                'refresh': function () {
                    var self = this;
                    var complete;
                    var error;
                    var filters;

                    if (!self.isDateFiltersSet) {
                        self.reset();

                        return;
                    }

                    filters = [
                        'and',
                        ['>=', 'date', this.filters.dateStart],
                        ['<=', 'date', this.filters.dateEnd]
                    ];

                    if (self.filters.usersId.length > 0)
                        filters.push(['in', 'userId', self.filters.usersId]);

                    self.beginLoading();

                    complete = function (data) {
                        self.endLoading();

                        return data;
                    };

                    error = function (data) {
                        self.reset();

                        return complete(data);
                    };

                    return self.$actions.runBatch([
                        self.$models.Application.find()
                    ]).then(function (responses) {
                        self.applications = responses[0].getData([]);
                        self.reports = [];

                        if (self.applications.length > 0) {
                            return self.$models.UserReport.find({
                                'where': filters
                            }).run().then(function (data) {
                                self.reports = data;

                                return complete(data);
                            }, error);
                        }

                        return complete(responses);
                    }, error);
                },
                'reset': function () {
                    this.applications = [];
                    this.reports = [];
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-template-layout" type="panel" vertical-scroll>
        <template v-slot:panel>
            <component is="v-layout-panel" wrap>
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch-calendar-range" v-model="range"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" multiple theme="panel" v-bind:values="filters.usersId" v-on:input-values="filters.usersId = $event"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-layout-panel" vertical>
                <component is="v-layout-panel-item">
                    <component is="v-layout-panel" wrap>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#f0b734"
                                    show-percents
                                    v-bind:maximal="statistics.common.worked.total.asSeconds()"
                                    v-bind:value="statistics.common.worked.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.applications.charts.worked.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.common.worked.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#0fe8af"
                                    show-percents
                                    v-bind:maximal="statistics.common.productive.total.asSeconds()"
                                    v-bind:value="statistics.common.productive.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.applications.charts.productive.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.common.productive.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#d2d2d2"
                                    show-percents
                                    v-bind:maximal="statistics.common.idle.total.asSeconds()"
                                    v-bind:value="statistics.common.idle.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.applications.charts.idle.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.common.idle.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#fd858a"
                                    show-percents
                                    v-bind:maximal="statistics.common.distractions.total.asSeconds()"
                                    v-bind:value="statistics.common.distractions.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.applications.charts.distractions.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.common.distractions.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-layout-panel" align="start">
                        <component is="v-layout-panel-item" full>
                            <component is="v-control-card" adaptable flat>
                                <template v-slot:title>
                                    {{ title }}
                                </template>
                                <template v-slot:additional>
                                    <component
                                        is="v-control-text-input"
                                        size="25"
                                        theme="simple"
                                        v-bind:placeholder-text="$localization.message('application.actions.search')"
                                        v-model="filter"
                                    >
                                        <template v-slot:append>
                                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z" fill="#C2CFE0" stroke="none" />
                                            </svg>
                                        </template>
                                    </component>
                                </template>
                                <component is="v-control-table" interactive v-if="filteredItems.length > 0">
                                    <template v-slot:head>
                                        <component is="v-control-table-row">
                                            <component is="v-control-table-cell" flexible nowrap>
                                                {{ $localization.message('c.section.statistics.applications.fields.application') }}
                                            </component>
                                            <component is="v-control-table-cell" flexible nowrap>
                                                {{ $localization.message('c.section.statistics.applications.fields.statistics') }}
                                            </component>
                                            <component is="v-control-table-cell"></component>
                                            <component is="v-control-table-cell" flexible nowrap></component>
                                        </component>
                                    </template>
                                    <template v-slot:default>
                                        <component is="v-control-table-row" v-for="(item, index) in filteredItems" v-bind:key="index">
                                            <component is="v-control-table-cell" flexible nowrap>
                                                {{ item.application.name }}
                                            </component>
                                            <component is="v-control-table-cell" flexible nowrap>
                                                {{ item.statistics.total.format('H _ m _ s _', {
                                                    'trim': true
                                                }) }}
                                            </component>
                                            <component is="v-control-table-cell">
                                                <component
                                                    is="v-control-chart-bar"
                                                    history
                                                    v-bind:maximal="statistics.applications.total.asSeconds()"
                                                    v-bind:multiple="true"
                                                    v-bind:values="getItemChartBarValues(item)"
                                                >
                                                    <template v-slot:history-title>
                                                        {{ $localization.message('c.section.statistics.applications.history.time') }}
                                                    </template>
                                                    <template v-slot:history-text>
                                                        {{ item.statistics.total.format('H _ m _ s _', {
                                                            'trim': true
                                                        }) }}
                                                    </template>
                                                </component>
                                            </component>
                                            <component is="v-control-table-cell" flexible nowrap>
                                                {{ getItemPercents(item) }}
                                            </component>
                                        </component>
                                    </template>
                                </component>
                                <component is="v-fragment-stub" style="min-height: 350px;" v-else v-bind:absolute="false"></component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" size="25%">
                            <component is="v-layout-panel" vertical>
                                <component is="v-layout-panel-item">
                                    <component is="v-control-card" adaptable flat>
                                        <template v-slot:title>
                                            {{ $localization.message('c.section.statistics.applications.popular.title') }}
                                        </template>
                                        <component is="v-control-table" v-if="popularItems.length > 0">
                                            <component is="v-control-table" interactive>
                                                <component is="v-control-table-row" v-for="(item, index) in popularItems" v-bind:key="index">
                                                    <component is="v-control-table-cell">
                                                        {{ item.application.name }}
                                                    </component>
                                                    <component is="v-control-table-cell" flexible nowrap>
                                                        {{ item.statistics.productive.format('H _') }}
                                                    </component>
                                                </component>
                                            </component>
                                        </component>
                                        <component is="v-fragment-stub" style="min-height: 144px;" v-else v-bind:absolute="false"></component>
                                    </component>
                                </component>
                                <component is="v-layout-panel-item">
                                    <component is="v-control-card" adaptable flat>
                                        <template v-slot:title>
                                            {{ $localization.message('c.section.statistics.applications.disallowed.title') }}
                                        </template>
                                        <component is="v-control-table" v-if="disallowedItems.length > 0">
                                            <component is="v-control-table" interactive>
                                                <component is="v-control-table-row" v-for="(item, index) in disallowedItems" v-bind:key="index">
                                                    <component is="v-control-table-cell">
                                                        {{ item.application.name }}
                                                    </component>
                                                    <component is="v-control-table-cell" flexible nowrap>
                                                        {{ item.statistics.distractions.format('H _') }}
                                                    </component>
                                                </component>
                                            </component>
                                        </component>
                                        <component is="v-fragment-stub" style="min-height: 143px;" v-else v-bind:absolute="false"></component>
                                    </component>
                                </component>
                            </component>
                        </component>
                    </component>
                </component>
            </component>
        </template>
    </component>
</template>
