<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'dateStart': function () {
                    return !_.isNil(this.filters.dateStart) ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return !_.isNil(this.filters.dateEnd) ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'isFilled': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd);
                },
                'usersId': function () {
                    return _.map(this.users, function (user) {
                        return user.id;
                    });
                },
                'week': function () {
                    return !_.isNil(this.dateStart) ? this.dateStart.week() : null;
                },
                'year': function () {
                    return !_.isNil(this.dateStart) ? this.dateStart.year() : null;
                }
            },
            'data': {
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'usersId': []
                },
                'range': 'week',
                'users': []
            },
            'methods': {
                'openUserFilter': function () {
                    this.$refs.userFilterPopover.open();
                },
                'refresh': function () {
                    var self = this;
                    var filters;

                    if (!self.isFilled) {
                        self.reset();

                        return;
                    }

                    filters = [
                        'and'
                    ];

                    if (self.filters.usersId.length > 0)
                        filters.push(['in', 'id', self.filters.usersId]);

                    self.beginLoading();

                    return self.$models.User.find({
                        'where': filters
                    }).run().then(function (users) {
                        self.users = users;
                        self.endLoading();

                        return users;
                    }, function (reason) {
                        self.endLoading();

                        return reason;
                    });
                },
                'reset': function () {
                    this.users = [];
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel" wrap>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" multiple theme="panel" v-bind:values="filters.usersId" v-on:input-values="filters.usersId = $event"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-control-card" flat>
                <component
                    is="v-part-users-schedule-table"
                    v-if="isFilled && usersId.length > 0"
                    v-bind:users-id="usersId"
                    v-bind:week="week"
                    v-bind:year="year"
                    v-on:refresh-start="beginLoading"
                    v-on:refresh-end="endLoading"
                ></component>
                <component is="v-fragment-stub" v-else></component>
            </component>
        </template>
    </component>
</template>
