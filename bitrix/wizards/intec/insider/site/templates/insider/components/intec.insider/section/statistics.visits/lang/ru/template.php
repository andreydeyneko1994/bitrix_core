<?php

$MESS['c.section.statistics.visits.title'] = 'Статистика сотрудников';
$MESS['c.section.statistics.visits.charts.lateness.title'] = 'Опоздания';
$MESS['c.section.statistics.visits.charts.earlyDeparture.title'] = 'Ранние уходы';
$MESS['c.section.statistics.visits.charts.work.title'] = 'Среднее время на работе';
$MESS['c.section.statistics.visits.fields.initials'] = 'Сотрудник';
$MESS['c.section.statistics.visits.fields.latenessDuration'] = 'Продолжительность опозданий';
$MESS['c.section.statistics.visits.fields.latenessCount'] = 'Кол-во опозданий';
$MESS['c.section.statistics.visits.fields.absenceDuration'] = 'Продолжительность прогулов';
$MESS['c.section.statistics.visits.fields.absenceCount'] = 'Кол-во прогулов';
$MESS['c.section.statistics.visits.fields.earlyDepartureDuration'] = 'Продолжительность ранних уходов';
$MESS['c.section.statistics.visits.fields.earlyDepartureCount'] = 'Кол-во ранних уходов';
$MESS['c.section.statistics.visits.fields.workDuration'] = 'Средняя продолжительность работы';
