<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsFilter', {
                    'resolveItem': function (item) {
                        return item.user.getName();
                    },
                    'resolveItems': function () {
                        return this.items;
                    }
                }),
                application.mixin('partsItemsPagination')
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'dateStart': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'isDateFiltersSet': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd);
                },
                'items': function () {
                    var self = this;
                    var result = [];

                    _.each(self.users, function (user) {
                        var department = user.getDepartment(self.departments);
                        var incidents = user.getIncidents(self.incidents);
                        var position = user.getPosition(self.positions);

                        _.each(incidents, function (incident) {
                            var item = {
                                'user': user,
                                'department': department,
                                'incident': incident,
                                'position': position
                            };

                            result.push(item);
                        });
                    });

                    result.sort(function (item1, item2) {
                        var date1 = item1.incident.getStartDate();
                        var date2 = item2.incident.getStartDate();

                        if (date1.isAfter(date2)) {
                            return 1;
                        } else if (date1.isBefore(date2)) {
                            return -1;
                        }

                        return 0;
                    });

                    return result;
                },
                'types': function () {
                    return [{
                        'name': this.$localization.message('c.section.statistics.incidents.filter.types.disallowed'),
                        'value': 'disallowed'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.types.allowed'),
                        'value': 'allowed'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.types.all'),
                        'value': 'all'
                    }];
                },
                'statuses': function () {
                    return [{
                        'name': this.$localization.message('c.section.statistics.incidents.filter.statuses.new'),
                        'value': 'new'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.statuses.handled'),
                        'value': 'handled'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.statuses.rejected'),
                        'value': 'rejected'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.statuses.accepted'),
                        'value': 'accepted'
                    }, {
                        'name': this.$localization.message('c.section.statistics.incidents.filter.statuses.all'),
                        'value': 'all'
                    }];
                },
                'title': function () {
                    var result = this.$localization.message('c.section.statistics.incidents.title');

                    if (this.isDateFiltersSet) {
                        if (this.range === 'day') {
                            result += ' ' + this.dateStart.format('DD MMM YYYY');
                        } else {
                            result += ' ' + this.dateStart.format('DD MMM YYYY') + ' - ' + this.dateEnd.format('DD MMM YYYY');
                        }
                    }

                    return result;
                }
            },
            'data': {
                'applications': [],
                'departments': [],
                'changingItem': null,
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'status': 'new',
                    'type': 'disallowed',
                    'usersId': []
                },
                'incidents': [],
                'positions': [],
                'range': 'day',
                'users': []
            },
            'methods': {
                'acceptItem': function (item) {
                    this.changingItem = null;

                    item.incident.accept();
                    item.incident.save().run();
                },
                'changeItem': function (item) {
                    this.changingItem = item;
                },
                'getApplication': function (id) {
                    var result = null;

                    _.each(this.applications, function (application) {
                        if (application.id === id) {
                            result = application;

                            return false;
                        }
                    });

                    return result;
                },
                'getItemContent': function (item) {
                    var application;
                    var site;

                    if (item.incident.type === 'application') {
                        application = this.getApplication(item.incident.data);

                        if (!_.isNil(application)) {
                            return application.name;
                        }
                    } else if (item.incident.type === 'site') {
                        application = this.getApplication(item.incident.data);

                        if (!_.isNil(application)) {
                            site = item.incident.additionalData;

                            if (!_.isNil(site.title) && !_.isNil(site.url)) {
                                return site.title + ' (' + site.url + ')';
                            } else {
                                return application.name;
                            }
                        }
                    } else {
                        return item.incident.getDuration().format('H _ m _ s _');
                    }

                    return null;
                },
                'getItemDate': function (item) {
                    return item.incident.getStartDate().format('HH:mm:ss');
                },
                'getItemDescription': function (item) {
                    var result = [];

                    if (item.position)
                        result.push(item.position.name);

                    if (item.department)
                        result.push(item.department.name);

                    return _.join(result, ' / ');
                },
                'getItemSite': function (item) {
                    var site;

                    if (item.incident.type === 'site') {
                        site = item.incident.additionalData;

                        if (!_.isNil(site.title) && !_.isNil(site.url))
                            return site;
                    }

                    return null;
                },
                'getItemType': function (item) {
                    var application;
                    var result = this.$localization.message('c.section.statistics.incidents.types.' + item.incident.type);

                    if (['application', 'site'].indexOf(item.incident.type) !== -1) {
                        application = this.getApplication(item.incident.data);

                        if (!_.isNil(application))
                            result += ' ' + application.name;
                    }

                    return result;
                },
                'isItemAccepted': function (item) {
                    return item.incident.isAccepted();
                },
                'isItemChanging': function (item) {
                    return this.isItemNew(item) || this.changingItem === item;
                },
                'isItemHandled': function (item) {
                    return item.incident.isHanlded();
                },
                'isItemHasDescription': function (item) {
                    return !!(item.department || item.position);
                },
                'isItemHasSite': function (item) {
                    return this.getItemSite(item) !== null;
                },
                'isItemNew': function (item) {
                    return item.incident.isNew();
                },
                'isItemRejected': function (item) {
                    return item.incident.isRejected();
                },
                'refresh': function () {
                    var self = this;
                    var complete;
                    var error;
                    var filters;
                    var usersFilters;

                    if (!self.isDateFiltersSet) {
                        self.reset();

                        return;
                    }

                    filters = [
                        'and',
                        ['in', 'type', ['absence', 'application', 'idle', 'site']],
                        ['>=', 'startDate', this.filters.dateStart],
                        ['<=', 'startDate', this.filters.dateEnd]
                    ];

                    usersFilters = [
                        'and'
                    ];

                    if (self.filters.status === 'new') {
                        filters.push(['=', 'handled', 0]);
                    } else if (self.filters.status === 'handled') {
                        filters.push(['=', 'handled', 1]);
                    } else if (self.filters.status === 'accepted') {
                        filters.push(['=', 'handled', 1]);
                        filters.push(['=', 'accepted', 1]);
                    } else if (self.filters.status === 'rejected') {
                        filters.push(['=', 'handled', 1]);
                        filters.push(['=', 'accepted', 0]);
                    }

                    if (self.filters.type === 'disallowed') {
                        filters.push(['=', 'allowed', 0]);
                    } else if (self.filters.type === 'allowed') {
                        filters.push(['=', 'allowed', 1]);
                    }

                    if (self.filters.usersId.length > 0)
                        usersFilters.push(['in', 'id', self.filters.usersId]);

                    self.beginLoading();

                    complete = function (data) {
                        self.endLoading();

                        return data;
                    };

                    error = function (data) {
                        self.reset();

                        return complete(data);
                    };

                    return self.$actions.runBatch([
                        self.$models.Application.find(),
                        self.$models.Department.find(),
                        self.$models.Position.find(),
                        self.$models.User.find({
                            'where': usersFilters
                        })
                    ]).then(function (responses) {
                        var indexes;

                        self.incidents = [];
                        self.applications = responses[0].getData([]);
                        self.departments = responses[1].getData([]);
                        self.positions = responses[2].getData([]);
                        self.users = responses[3].getData([]);

                        if (self.users.length > 0) {
                            indexes = _.map(self.users, function (user) {
                                return user.id;
                            });

                            return self.$models.UserIncident.count({
                                'where': _.concat(filters, [
                                    ['in', 'userId', indexes]
                                ])
                            }).run().then(function (count) {
                                self.recordsCount = count;

                                return self.$models.UserIncident.find({
                                    'where': _.concat(filters, [
                                        ['in', 'userId', _.map(self.users, function (user) {
                                            return user.id;
                                        })]
                                    ]),
                                    'limit': self.pageSize,
                                    'offset': self.pageOffset,
                                    'orderBy': {
                                        'startDate': 'asc',
                                        'userId': 'asc'
                                    }
                                }).run().then(function (data) {
                                    self.incidents = data;

                                    return complete(data);
                                }, error);
                            }, error);


                        } else {
                            return complete(responses);
                        }
                    }, error);
                },
                'rejectItem': function (item) {
                    this.changingItem = null;

                    item.incident.reject();
                    item.incident.save().run();
                },
                'reset': function () {
                    this.applications = [];
                    this.departments = [];
                    this.incidents = [];
                    this.positions = [];
                    this.users = [];
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.pageNumber = 1;
                        this.refresh();
                    }
                },
                'pageNumber': function () {
                    this.refresh();
                },
                'pageSize': function () {
                    this.refresh();
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel" wrap>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch" v-bind:items="statuses" v-model="filters.status"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch" v-bind:items="types" v-model="filters.type"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" multiple theme="panel" v-bind:values="filters.usersId" v-on:input-values="filters.usersId = $event"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-layout-panel" vertical>
                <component is="v-layout-panel-item" full>
                    <component is="v-control-card" flat>
                        <template v-slot:title>
                            {{ title }}
                        </template>
                        <template v-slot:additional>
                            <component
                                is="v-control-text-input"
                                size="25"
                                theme="simple"
                                v-bind:placeholder-text="$localization.message('application.actions.search')"
                                v-model="filter"
                            >
                                <template v-slot:append>
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z" fill="#C2CFE0" stroke="none" />
                                    </svg>
                                </template>
                            </component>
                        </template>
                        <component is="v-control-table" interactive v-if="filteredItems.length > 0">
                            <template v-slot:head>
                                <component is="v-control-table-row">
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ $localization.message(range === 'day' ? 'c.section.statistics.incidents.fields.time' : 'c.section.statistics.incidents.fields.date') }}
                                    </component>
                                    <component is="v-control-table-cell">
                                        {{ $localization.message('c.section.statistics.incidents.fields.initials') }}
                                    </component>
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.incidents.fields.type') }}
                                    </component>
                                    <component is="v-control-table-cell">
                                        {{ $localization.message('c.section.statistics.incidents.fields.content') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible>
                                        {{ $localization.message('c.section.statistics.incidents.fields.status') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible>
                                        {{ $localization.message('c.section.statistics.incidents.fields.control') }}
                                    </component>
                                </component>
                            </template>
                            <template v-slot:default>
                                <component is="v-control-table-row" v-for="(item, index) in filteredItems" v-bind:key="index">
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ getItemDate(item) }}
                                    </component>
                                    <component is="v-control-table-cell">
                                        <component is="v-fragment-user-title">
                                            <template v-slot:picture>
                                                <component is="v-fragment-picture-view" v-bind:picture="item.user.pictureId" theme="round"></component>
                                            </template>
                                            <template v-slot:name>
                                                {{ item.user.getName() }}
                                            </template>
                                            <template v-slot:description v-if="isItemHasDescription(item)">
                                                {{ getItemDescription(item) }}
                                            </template>
                                        </component>
                                    </component>
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ getItemType(item) }}
                                    </component>
                                    <component is="v-control-table-cell">
                                        <template v-if="isItemHasSite(item)">
                                            <template v-for="site in [getItemSite(item)]">
                                                <component is="v-control-link" target="_blank" v-bind:href="site.url">
                                                    {{ site.title }}
                                                </component>
                                            </template>
                                        </template>
                                        <template v-else>
                                            {{ getItemContent(item) }}
                                        </template>
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible>
                                        <component is="v-control-badge" theme="default-green" v-if="isItemAccepted(item)">
                                            {{ $localization.message('c.section.statistics.incidents.statuses.accepted') }}
                                        </component>
                                        <component is="v-control-badge" theme="default-red" v-else-if="isItemRejected(item)">
                                            {{ $localization.message('c.section.statistics.incidents.statuses.rejected') }}
                                        </component>
                                        <component is="v-control-badge" theme="default-blue" v-else>
                                            {{ $localization.message('c.section.statistics.incidents.statuses.new') }}
                                        </component>
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible>
                                        <div class="intec-grid intec-grid-nowrap intec-grid-a-h-start intec-grid-a-v-center intec-grid-i-h-3" v-if="isItemChanging(item)">
                                            <div class="intec-grid-item-auto">
                                                <component is="v-control-button" theme="accept-icon" v-on:click="acceptItem(item)"></component>
                                            </div>
                                            <div class="intec-grid-item-auto">
                                                <component is="v-control-button" theme="reject-icon" v-on:click="rejectItem(item)"></component>
                                            </div>
                                        </div>
                                        <component is="v-control-link" v-else v-on:click="changeItem(item)">
                                            {{ $localization.message('application.actions.change') }}
                                        </component>
                                    </component>
                                </component>
                            </template>
                        </component>
                        <component is="v-fragment-stub" v-else></component>
                    </component>
                </component>
                <component is="v-layout-panel-item" v-if="items.length > 0">
                    <component
                        is="v-control-pagination"
                        v-bind:length="pageCount"
                        v-bind:size="pageSize"
                        v-bind:total-visible="7"
                        v-model="pageNumber"
                        v-on:input-size="pageSize = $event"
                    ></component>
                </component>
            </component>
        </template>
    </component>
</template>
