<?php

$MESS['c.systemAuthAuthorize.default.title'] = 'Войти';
$MESS['c.systemAuthAuthorize.default.description'] = 'Войдите в систему';
$MESS['c.systemAuthAuthorize.default.fields.login'] = 'Логин';
$MESS['c.systemAuthAuthorize.default.fields.password'] = 'Пароль';
$MESS['c.systemAuthAuthorize.default.fields.captcha'] = 'Проверочное слово';
$MESS['c.systemAuthAuthorize.default.remember'] = 'Запомнить меня';
$MESS['c.systemAuthAuthorize.default.buttons.login'] = 'Войти';
$MESS['c.systemAuthAuthorize.default.buttons.forgotPassword'] = 'Забыли пароль?';
