<?php

$MESS['c.section.settings.agents.title'] = 'Агенты';
$MESS['c.section.settings.agents.actions.downloadForServer'] = 'Скачать для сервера';
$MESS['c.section.settings.agents.fields.user'] = 'Пользователь';
$MESS['c.section.settings.agents.fields.isLinked'] = 'Привязан';
$MESS['c.section.settings.agents.fields.userDomain'] = 'Домен';
$MESS['c.section.settings.agents.fields.userName'] = 'Учетная запись';
$MESS['c.section.settings.agents.fields.operatingSystem'] = 'Операционная система';
$MESS['c.section.settings.agents.fields.address'] = 'Адрес';
$MESS['c.section.settings.agents.fields.key'] = 'Ключ';
