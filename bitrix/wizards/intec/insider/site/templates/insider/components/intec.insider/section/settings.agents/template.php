<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsActions', {
                    'actions': {
                        'remove': {
                            'handler': function (agents, action) {
                                action.handle = true;

                                return _.map(agents, function (agent) {
                                    return agent.remove();
                                });
                            },
                            'multiple': true
                        }
                    },
                    'afterAction': function (action) {
                        var self = this;

                        if (action.handle) {
                            self.beginLoading();
                            self.$actions.runBatch(action.result).then(function () {
                                self.refresh();
                            }, function () {
                                self.refresh();
                            });
                        }
                    }
                }),
                application.mixin('partsItemsSelection', {
                    'resolveItem': function (agent) {
                        return agent.id;
                    },
                    'resolveItems': function () {
                        return this.agents;
                    }
                })
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
                this.refresh();
            },
            'data': {
                'agents': [],
                'linkingAgent': null,
                'linkingUserId': null,
                'users': []
            },
            'methods': {
                'applyLinking': function () {
                    if (!_.isNil(this.linkingAgent)) {
                        this.beginLoading();
                        this.linkingAgent.userId = this.linkingUserId;
                        this.linkingAgent.save().run().then(this.refresh, this.refresh);
                    }

                    this.linkingAgent = null;
                    this.linkingUserId = null;
                },
                'beginLinking': function (agent) {
                    this.linkingAgent = agent;
                    this.linkingUserId = agent.userId;

                    if (this.$refs.userLinkPopover)
                        this.$refs.userLinkPopover.open();
                },
                'createAgent': function () {
                    var self = this;

                    self.beginLoading();
                    self.$actions.create('agents.create').run().then(function () {
                        self.refresh();
                    }, function () {
                        self.refresh();
                    });
                },
                'getAgentUser': function (agent) {
                    return agent.getUser(this.users);
                },
                'getAgentUserName': function (agent) {
                    var user = this.getAgentUser(agent);

                    return !_.isNil(user) ?
                        user.getName() :
                        '(' + this.$localization.message('application.answers.no') + ')';
                },
                'getAgentDownloadLink': function (agent) {
                    var result = '/bitrix/admin/insider_agents_download.php?siteId=' + this.site.id;

                    if (!_.isNil(agent))
                        result += '&agent=' + agent.id;

                    return result;
                },
                'refresh': function () {
                    var self = this;

                    self.beginLoading();

                    return self.$actions.runBatch([
                        self.$models.Agent.find(),
                        self.$models.User.find()
                    ]).then(function (responses) {
                        if (responses[0].isSuccess())
                            self.agents = responses[0].data;

                        if (responses[1].isSuccess())
                            self.users = responses[1].data;

                        self.endLoading();

                        return responses;
                    }, function (reasons) {
                        self.endLoading();

                        return reasons;
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component
        is="v-part-users-filter-popover"
        ref="userLinkPopover"
        v-model="linkingUserId"
        v-on:apply="applyLinking"
    ></component>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel">
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel" v-on:click="createAgent">
                        {{ $localization.message('application.actions.add') }}
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel-remove" v-bind:disabled="!hasSelectedItems" v-on:click="doMultipleAction('remove', getSelectedItems())"></component>
                </component>
                <component is="v-layout-panel-item" full></component>
                <component is="v-layout-panel-item">
                    <component is="v-control-link" v-bind:href="getAgentDownloadLink()">
                        <span>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15V1" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M19 15C19 17.209 17.209 19 15 19H5C2.791 19 1 17.209 1 15" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M15 10L9.99899 15.001L4.99899 10" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </span>
                        <span>
                            {{ $localization.message('c.section.settings.agents.actions.downloadForServer') }}
                        </span>
                    </component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-control-card" flat>
                <template v-slot:title>
                    {{ $localization.message('c.section.settings.agents.title') }}
                </template>
                <component is="v-control-table">
                    <template v-slot:head>
                        <component is="v-control-table-row">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:value="isAllItemsSelected"
                                    v-on:checked="selectAllItems()"
                                    v-on:unchecked="deselectAllItems()"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible></component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.user') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.isLinked') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.userDomain') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.userName') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.operatingSystem') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.address') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.agents.fields.key') }}
                            </component>
                        </component>
                    </template>
                    <template v-slot:default>
                        <component is="v-control-table-row" v-for="(agent, index) in agents" v-bind:key="index">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:value="isItemSelected(agent)"
                                    v-on:checked="selectItem(agent)"
                                    v-on:unchecked="deselectItem(agent)"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible>
                                <component is="v-control-dropdown-menu">
                                    <template v-slot:items>
                                        <component is="v-control-dropdown-menu-item" v-bind:href="getAgentDownloadLink(agent)">
                                            {{ $localization.message('application.actions.download') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="beginLinking(agent)">
                                            {{ $localization.message('application.actions.link') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('remove', agent)">
                                            {{ $localization.message('application.actions.remove') }}
                                        </component>
                                    </template>
                                </component>
                            </component>
                            <component is="v-control-table-cell">
                                {{ getAgentUserName(agent) }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.isLinked() ? $localization.message('application.answers.yes') : $localization.message('application.answers.no') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.userDomain }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.userName }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.operatingSystem }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.address }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ agent.key }}
                            </component>
                        </component>
                    </template>
                </component>
            </component>
        </template>
    </component>
</template>
