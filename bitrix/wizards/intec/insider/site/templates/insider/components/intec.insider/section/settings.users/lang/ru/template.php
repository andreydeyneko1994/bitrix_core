<?php

$MESS['c.section.settings.users.title'] = 'Сотрудники';
$MESS['c.section.settings.users.filter.activities.active'] = 'Активные';
$MESS['c.section.settings.users.filter.activities.all'] = 'Все';
$MESS['c.section.settings.users.fields.initials'] = 'Ф.И.О.';
$MESS['c.section.settings.users.fields.active'] = 'Активность';
$MESS['c.section.settings.users.fields.email'] = 'Эл. почта';
