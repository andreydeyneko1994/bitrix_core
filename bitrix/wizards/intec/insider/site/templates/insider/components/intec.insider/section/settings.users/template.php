<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsActions', {
                    'actions': {
                        'activate': {
                            'handler': function (users, action) {
                                action.handle = true;

                                return _.map(users, function (user) {
                                    user.active = 1;

                                    return user.save();
                                });
                            },
                            'multiple': true
                        },
                        'deactivate': {
                            'handler': function (users, action) {
                                action.handle = true;

                                return _.map(users, function (user) {
                                    user.active = 0;

                                    return user.save();
                                });
                            },
                            'multiple': true
                        },
                        'edit': {
                            'handler': function (user) {
                                this.editUser(user);
                            }
                        },
                        'remove': {
                            'handler': function (users, action) {
                                action.handle = true;

                                return _.map(users, function (user) {
                                    return user.remove();
                                });
                            },
                            'multiple': true
                        },
                        'view': {
                            'handler': function (user) {
                                this.viewUser(user);
                            }
                        }
                    },
                    'afterAction': function (action) {
                        var self = this;

                        if (action.handle) {
                            self.beginLoading();
                            self.$actions.runBatch(action.result).then(function () {
                                self.refresh();
                            }, function () {
                                self.refresh();
                            });
                        }
                    }
                }),
                application.mixin('partsItemsFilter', {
                    'resolveItem': function (user) {
                        return user.getName();
                    },
                    'resolveItems': function () {
                        return this.users;
                    }
                }),
                application.mixin('partsItemsSelection', {
                    'resolveItem': function (user) {
                        return user.id;
                    },
                    'resolveItems': function () {
                        return this.filteredItems;
                    }
                })
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
                this.refresh();
            },
            'computed': {
                'activities': function () {
                    return [{
                        'name': this.$localization.message('c.section.settings.users.filter.activities.active'),
                        'value': true
                    }, {
                        'name': this.$localization.message('c.section.settings.users.filter.activities.all'),
                        'value': false
                    }];
                }
            },
            'data': {
                'filters': {
                    'active': false
                },
                'users': []
            },
            'methods': {
                'createUser': function () {
                    this.$refs.editPopover.open();
                },
                'editUser': function (user) {
                    this.$refs.editPopover.open(user.id);
                },
                'refresh': function () {
                    var self = this;
                    var filters = {};

                    self.beginLoading();

                    if (self.filters.active)
                        filters.active = 1;

                    return self.$actions.runBatch([
                        self.$models.User.find({
                            'where': filters
                        })
                    ]).then(function (responses) {
                        if (responses[0].isSuccess())
                            self.users = responses[0].data;

                        self.endLoading();

                        return responses;
                    }, function (reasons) {
                        self.endLoading();

                        return reasons;
                    });
                },
                'viewUser': function (user) {
                    this.$refs.viewPopover.open(user.id);
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-part-users-edit-popover" ref="editPopover" v-on:apply="refresh"></component>
    <component is="v-part-users-view-popover" ref="viewPopover"></component>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel">
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel" v-on:click="createUser">
                        {{ $localization.message('application.actions.add') }}
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-dropdown-button" theme="panel" v-bind:disabled="!hasSelectedItems">
                        <template v-slot:default>
                            {{ $localization.message('application.actions.edit') }}
                        </template>
                        <template v-slot:items>
                            <component is="v-control-dropdown-button-item" v-on:click="doMultipleAction('activate', getSelectedItems())">
                                {{ $localization.message('application.actions.activate') }}
                            </component>
                            <component is="v-control-dropdown-button-item" v-on:click="doMultipleAction('deactivate', getSelectedItems())">
                                {{ $localization.message('application.actions.deactivate') }}
                            </component>
                        </template>
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel-remove" v-bind:disabled="!hasSelectedItems" v-on:click="doMultipleAction('remove', getSelectedItems())"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch" v-bind:items="activities" v-model="filters.active"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-control-card" flat>
                <template v-slot:title>
                    {{ $localization.message('c.section.settings.users.title') }}
                </template>
                <template v-slot:additional>
                    <component
                        is="v-control-text-input"
                        size="25"
                        theme="simple"
                        v-bind:placeholder-text="$localization.message('application.actions.search')"
                        v-model="filter"
                    >
                        <template v-slot:append>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z" fill="#C2CFE0" stroke="none" />
                            </svg>
                        </template>
                    </component>
                </template>
                <component is="v-control-table">
                    <template v-slot:head>
                        <component is="v-control-table-row">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:disabled="!hasFilteredItems"
                                    v-bind:value="isAllItemsSelected"
                                    v-on:checked="selectAllItems()"
                                    v-on:unchecked="deselectAllItems()"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible></component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.users.fields.initials') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.users.fields.active') }}
                            </component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.users.fields.email') }}
                            </component>
                        </component>
                    </template>
                    <template v-slot:default>
                        <component is="v-control-table-row" v-for="(user, index) in filteredItems" v-bind:key="index">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:value="isItemSelected(user)"
                                    v-on:checked="selectItem(user)"
                                    v-on:unchecked="deselectItem(user)"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible>
                                <component is="v-control-dropdown-menu">
                                    <template v-slot:items>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('edit', user)">
                                            {{ $localization.message('application.actions.edit') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('view', user)">
                                            {{ $localization.message('application.actions.view') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('activate', user)" v-if="!user.active">
                                            {{ $localization.message('application.actions.activate') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('deactivate', user)" v-else>
                                            {{ $localization.message('application.actions.deactivate') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('remove', user)">
                                            {{ $localization.message('application.actions.remove') }}
                                        </component>
                                    </template>
                                </component>
                            </component>
                            <component is="v-control-table-cell">
                                <div class="intec-grid intec-grid-nowrap intec-grid-a-v-center intec-grid-i-h-10">
                                    <div class="intec-grid-item-auto">
                                        <component is="v-fragment-picture-view" v-bind:picture="user.pictureId" theme="round" width="45px" height="45px"></component>
                                    </div>
                                    <div class="intec-grid-item-auto">
                                        {{ user.getName() }}
                                    </div>
                                </div>
                            </component>
                            <component is="v-control-table-cell">
                                <component
                                    is="v-control-switch"
                                    v-bind:true-value="1"
                                    v-bind:false-value="0"
                                    v-bind:value="user.active"
                                    v-on:checked="doAction('activate', user)"
                                    v-on:unchecked="doAction('deactivate', user)"
                                ></component>
                            </component>
                            <component is="v-control-table-cell">
                                {{ user.email }}
                            </component>
                        </component>
                    </template>
                </component>
            </component>
        </template>
    </component>
</template>
