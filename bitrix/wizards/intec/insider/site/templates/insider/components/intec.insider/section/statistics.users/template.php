<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsFilter', {
                    'resolveItem': function (item) {
                        return item.user.getName();
                    },
                    'resolveItems': function () {
                        return this.items;
                    }
                })
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
            },
            'mounted': function () {
                this.$nextTick(function () {
                    if (this.$refs.calendar) {
                        this.$refs.calendar.switchCurrent();
                        this.$refs.calendar.apply();
                    }
                });
            },
            'computed': {
                'dateStart': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateStart, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'dateEnd': function () {
                    return this.isDateFiltersSet ? moment(this.filters.dateEnd, 'YYYY-MM-DD HH:mm:ss') : null;
                },
                'isDateFiltersSet': function () {
                    return !_.isNil(this.filters.dateStart) && !_.isNil(this.filters.dateEnd);
                },
                'items': function () {
                    var self = this;
                    var result = [];

                    _.each(self.users, function (user) {
                        var reports;
                        var item = {
                            'user': user,
                            'department': null,
                            'position': null,
                            'statistics': null
                        };

                        reports = self.$statistics.createReports(user.getReports(self.reports));

                        if (reports.length === 0)
                            return;

                        item.department = user.getDepartment(self.departments);
                        item.position = user.getPosition(self.positions);
                        item.statistics = {
                            'common': reports.getCommonStatistics(),
                            'events': reports.getAbsenceEventsStatistics()
                        };

                        result.push(item);
                    });

                    return result;
                },
                'statistics': function () {
                    return this.$statistics.createReports(this.reports).getCommonStatistics();
                },
                'title': function () {
                    var result = this.$localization.message('c.section.statistics.users.title');

                    if (this.isDateFiltersSet) {
                        if (this.range === 'day') {
                            result += ' ' + this.dateStart.format('DD MMM YYYY');
                        } else {
                            result += ' ' + this.dateStart.format('DD MMM YYYY') + ' - ' + this.dateEnd.format('DD MMM YYYY');
                        }
                    }

                    return result;
                }
            },
            'data': {
                'departments': [],
                'filters': {
                    'dateStart': null,
                    'dateEnd': null,
                    'usersId': []
                },
                'positions': [],
                'range': 'day',
                'reports': [],
                'users': []
            },
            'methods': {
                'getItemChartBarValues': function (item) {
                    return [{
                        'color': '#00c294',
                        'text': item.statistics.common.productive.duration.format('H _ m _ s _', {
                            'trim': true
                        }),
                        'title': this.$localization.message('c.section.statistics.users.history.productive'),
                        'value': item.statistics.common.productive.duration.asSeconds()
                    }, {
                        'color': '#d2d2d2',
                        'text': item.statistics.common.idle.duration.format('H _ m _ s _', {
                            'trim': true
                        }),
                        'title': this.$localization.message('c.section.statistics.users.history.idle'),
                        'value': item.statistics.common.idle.duration.asSeconds()
                    }, {
                        'color': '#fc7d7d',
                        'text': item.statistics.common.distractions.duration.format('H _ m _ s _', {
                            'trim': true
                        }),
                        'title': this.$localization.message('c.section.statistics.users.history.distractions'),
                        'value': item.statistics.common.distractions.duration.asSeconds()
                    }];
                },
                'getItemDescription': function (item) {
                    var result = [];

                    if (item.position)
                        result.push(item.position.name);

                    if (item.department)
                        result.push(item.department.name);

                    return _.join(result, ' / ');
                },
                'isItemHasDescription': function (item) {
                    return !!(item.department || item.position);
                },
                'refresh': function () {
                    var self = this;
                    var complete;
                    var error;
                    var filters;
                    var usersFilters;

                    if (!self.isDateFiltersSet) {
                        self.reset();

                        return;
                    }

                    filters = [
                        'and',
                        ['>=', 'date', this.filters.dateStart],
                        ['<=', 'date', this.filters.dateEnd]
                    ];

                    usersFilters = [
                        'and'
                    ];

                    if (self.filters.usersId.length > 0)
                        usersFilters.push(['in', 'id', self.filters.usersId]);

                    self.beginLoading();

                    complete = function (data) {
                        self.endLoading();

                        return data;
                    };

                    error = function (data) {
                        self.reset();

                        return complete(data);
                    };

                    return self.$actions.runBatch([
                        self.$models.Department.find(),
                        self.$models.Position.find(),
                        self.$models.User.find({
                            'where': usersFilters
                        })
                    ]).then(function (responses) {
                        self.reports = [];
                        self.departments = responses[0].getData([]);
                        self.positions = responses[1].getData([]);
                        self.users = responses[2].getData([]);

                        if (self.users.length > 0) {
                            return self.$models.UserReport.find({
                                'where': _.concat(filters, [
                                    ['in', 'userId', _.map(self.users, function (user) {
                                        return user.id;
                                    })]
                                ])
                            }).run().then(function (data) {
                                self.reports = data;

                                return complete(data);
                            }, error);
                        } else {
                            return complete(responses);
                        }
                    }, error);
                },
                'reset': function () {
                    this.departments = [];
                    this.positions = [];
                    this.reports = [];
                    this.users = [];
                },
                'viewUser': function (user) {
                    this.$refs.viewPopover.open(user.id);
                }
            },
            'watch': {
                'filters': {
                    'deep': true,
                    'handler': function () {
                        this.refresh();
                    }
                },
                'range': function () {
                    this.$nextTick(function () {
                        if (this.$refs.calendar) {
                            this.$refs.calendar.switchCurrent();
                            this.$refs.calendar.apply();
                        }
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-part-users-view-popover" ref="viewPopover"></component>
    <component is="v-template-layout" vertical-scroll type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel" wrap>
                <component is="v-layout-panel-item">
                    <component is="v-control-list-switch-calendar-range" v-model="range"></component>
                </component>
                <component is="v-layout-panel-item">
                    <component
                        is="v-control-select-calendar"
                        ref="calendar"
                        buttons
                        ranged
                        v-bind:range="range"
                        v-bind:value-start="filters.dateStart"
                        v-bind:value-end="filters.dateEnd"
                        v-on:input-start="filters.dateStart = $event"
                        v-on:input-end="filters.dateEnd = $event"
                    ></component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-part-users-filter-button" multiple theme="panel" v-bind:values="filters.usersId" v-on:input-values="filters.usersId = $event"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-layout-panel" vertical>
                <component is="v-layout-panel-item">
                    <component is="v-layout-panel" wrap>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#f0b734"
                                    show-percents
                                    v-bind:maximal="statistics.worked.total.asSeconds()"
                                    v-bind:value="statistics.worked.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.users.charts.worked.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.worked.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#0fe8af"
                                    show-percents
                                    v-bind:maximal="statistics.productive.total.asSeconds()"
                                    v-bind:value="statistics.productive.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.users.charts.productive.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.productive.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#d2d2d2"
                                    show-percents
                                    v-bind:maximal="statistics.idle.total.asSeconds()"
                                    v-bind:value="statistics.idle.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.users.charts.idle.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.idle.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                        <component is="v-layout-panel-item" v-bind:size="{'1600': '50%', '*': '25%'}">
                            <component is="v-control-card" adaptable>
                                <component
                                    is="v-control-chart-bar"
                                    color="#fd858a"
                                    show-percents
                                    v-bind:maximal="statistics.distractions.total.asSeconds()"
                                    v-bind:value="statistics.distractions.duration.asSeconds()"
                                >
                                    <template v-slot:title>
                                        {{ $localization.message('c.section.statistics.users.charts.distractions.title') }}
                                    </template>
                                    <template v-slot:additional-left>
                                        {{ statistics.distractions.duration.format('H _ m _ s _', {
                                            'trim': true
                                        }) }}
                                    </template>
                                </component>
                            </component>
                        </component>
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-card" adaptable flat>
                        <template v-slot:title>
                            {{ title }}
                        </template>
                        <template v-slot:additional>
                            <component
                                is="v-control-text-input"
                                size="25"
                                theme="simple"
                                v-bind:placeholder-text="$localization.message('application.actions.search')"
                                v-model="filter"
                            >
                                <template v-slot:append>
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z" fill="#C2CFE0" stroke="none" />
                                    </svg>
                                </template>
                            </component>
                        </template>
                        <component is="v-control-table" interactive v-if="filteredItems.length > 0">
                            <template v-slot:head>
                                <component is="v-control-table-row">
                                    <component is="v-control-table-cell">
                                        {{ $localization.message('c.section.statistics.users.fields.initials') }}
                                    </component>
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.users.fields.statistics') }}
                                    </component>
                                    <component is="v-control-table-cell"></component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.users.fields.lateness') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.users.fields.absence') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.users.fields.earlyDeparture') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ $localization.message('c.section.statistics.users.fields.incidents') }}
                                    </component>
                                </component>
                            </template>
                            <template v-slot:default>
                                <component is="v-control-table-row" v-for="(item, index) in filteredItems" v-bind:key="index" v-on:click="viewUser(item.user)">
                                    <component is="v-control-table-cell">
                                        <component is="v-fragment-user-title">
                                            <template v-slot:picture>
                                                <component is="v-fragment-picture-view" v-bind:picture="item.user.pictureId" theme="round"></component>
                                            </template>
                                            <template v-slot:name>
                                                {{ item.user.getName() }}
                                            </template>
                                            <template v-slot:description v-if="isItemHasDescription(item)">
                                                {{ getItemDescription(item) }}
                                            </template>
                                        </component>
                                    </component>
                                    <component is="v-control-table-cell" flexible nowrap>
                                        {{ item.statistics.common.worked.duration.format('H _ m _', {
                                            'trim': true
                                        }) }}
                                    </component>
                                    <component is="v-control-table-cell">
                                        <component
                                            is="v-control-chart-bar"
                                            history
                                            v-bind:maximal="item.statistics.common.worked.total.asSeconds()"
                                            v-bind:multiple="true"
                                            v-bind:values="getItemChartBarValues(item)"
                                            style="min-width: 175px;"
                                        >
                                            <template v-slot:history-title>
                                                {{ $localization.message('c.section.statistics.users.history.worked') }}
                                            </template>
                                            <template v-slot:history-text>
                                                {{ item.statistics.common.worked.duration.format('H _ m _ s _', {
                                                    'trim': true
                                                }) }}
                                            </template>
                                        </component>
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ item.statistics.events.lateness.duration.format('m _') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ item.statistics.events.absence.duration.format('m _') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        {{ item.statistics.events.earlyDeparture.duration.format('m _') }}
                                    </component>
                                    <component is="v-control-table-cell" align="center" flexible nowrap>
                                        <component is="v-control-badge" theme="default-red">
                                            {{ item.statistics.common.incidents.count }}
                                        </component>
                                    </component>
                                </component>
                            </template>
                        </component>
                        <component is="v-fragment-stub" style="min-height: 350px;" v-else v-bind:absolute="false"></component>
                    </component>
                </component>
            </component>
        </template>
    </component>
</template>
