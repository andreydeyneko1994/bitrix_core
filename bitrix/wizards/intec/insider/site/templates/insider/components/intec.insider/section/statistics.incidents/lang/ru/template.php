<?php

$MESS['c.section.statistics.incidents.title'] = 'Статистика инцидентов';
$MESS['c.section.statistics.incidents.fields.time'] = 'Время';
$MESS['c.section.statistics.incidents.fields.date'] = 'Дата';
$MESS['c.section.statistics.incidents.fields.initials'] = 'Сотрудник';
$MESS['c.section.statistics.incidents.fields.type'] = 'Тип';
$MESS['c.section.statistics.incidents.fields.content'] = 'Содержание';
$MESS['c.section.statistics.incidents.fields.status'] = 'Статус';
$MESS['c.section.statistics.incidents.fields.control'] = 'Контроль';
$MESS['c.section.statistics.incidents.filter.types.disallowed'] = 'Запрещенные';
$MESS['c.section.statistics.incidents.filter.types.allowed'] = 'Разрешенные';
$MESS['c.section.statistics.incidents.filter.types.all'] = 'Все';
$MESS['c.section.statistics.incidents.filter.statuses.new'] = 'Новые';
$MESS['c.section.statistics.incidents.filter.statuses.handled'] = 'Обработанные';
$MESS['c.section.statistics.incidents.filter.statuses.accepted'] = 'Допустимые';
$MESS['c.section.statistics.incidents.filter.statuses.rejected'] = 'Недопустимые';
$MESS['c.section.statistics.incidents.filter.statuses.all'] = 'Все';
$MESS['c.section.statistics.incidents.statuses.new'] = 'Новый';
$MESS['c.section.statistics.incidents.statuses.accepted'] = 'Допустимый';
$MESS['c.section.statistics.incidents.statuses.rejected'] = 'Недопустимый';
$MESS['c.section.statistics.incidents.types.absence'] = 'Прогул';
$MESS['c.section.statistics.incidents.types.application'] = 'Приложение';
$MESS['c.section.statistics.incidents.types.idle'] = 'Отсутствие на месте';
$MESS['c.section.statistics.incidents.types.site'] = 'Сайт';
