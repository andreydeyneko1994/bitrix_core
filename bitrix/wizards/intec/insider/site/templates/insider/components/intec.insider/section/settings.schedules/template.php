<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\JavaScript;
use intec\insider\template\Application;

$application = Application::getInstance();
$messages = Loc::loadLanguageFile(__FILE__);

?>
<?php $application->beginPart() ?>
<script type="text/javascript">
    (function () {
        application.extend({
            'mixins': [
                application.mixin('partsItemsActions', {
                    'actions': {
                        'edit': {
                            'handler': function (schedule) {
                                this.editSchedule(schedule);
                            }
                        },
                        'remove': {
                            'handler': function (schedules, action) {
                                action.handle = true;

                                return _.map(schedules, function (schedule) {
                                    return schedule.remove();
                                });
                            },
                            'multiple': true
                        }
                    },
                    'afterAction': function (action) {
                        var self = this;

                        if (action.handle) {
                            self.beginLoading();
                            self.$actions.runBatch(action.result).then(function () {
                                self.refresh();
                            }, function () {
                                self.refresh();
                            });
                        }
                    }
                }),
                application.mixin('partsItemsFilter', {
                    'resolveItem': function (schedule) {
                        return schedule.name;
                    },
                    'resolveItems': function () {
                        return this.schedules;
                    }
                }),
                application.mixin('partsItemsSelection', {
                    'resolveItem': function (schedule) {
                        return schedule.id;
                    },
                    'resolveItems': function () {
                        return this.filteredItems;
                    }
                })
            ],
            'created': function () {
                this.$localization.load(<?= JavaScript::toObject($messages) ?>);
                this.refresh();
            },
            'data': {
                'schedules': []
            },
            'methods': {
                'createSchedule': function () {
                    this.$refs.editPopover.open();
                },
                'editSchedule': function (schedule) {
                    this.$refs.editPopover.open(schedule.id);
                },
                'refresh': function () {
                    var self = this;

                    self.beginLoading();

                    return self.$actions.runBatch([
                        self.$models.Schedule.find()
                    ]).then(function (responses) {
                        if (responses[0].isSuccess())
                            self.schedules = responses[0].data;

                        self.endLoading();

                        return responses;
                    }, function (reasons) {
                        self.endLoading();

                        return reasons;
                    });
                }
            }
        });
    })();
</script>
<?php $application->endPart() ?>
<template v-slot:default>
    <component is="v-part-schedule-edit-popover" ref="editPopover" v-on:apply="refresh"></component>
    <component is="v-template-layout" type="panel">
        <template v-slot:panel>
            <component is="v-layout-panel">
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel" v-on:click="createSchedule">
                        {{ $localization.message('application.actions.add') }}
                    </component>
                </component>
                <component is="v-layout-panel-item">
                    <component is="v-control-button" theme="panel-remove" v-bind:disabled="!hasSelectedItems" v-on:click="doMultipleAction('remove', getSelectedItems())"></component>
                </component>
            </component>
        </template>
        <template v-slot:default>
            <component is="v-control-card" flat>
                <template v-slot:title>
                    {{ $localization.message('c.section.settings.schedules.title') }}
                </template>
                <template v-slot:additional>
                    <component
                        is="v-control-text-input"
                        size="25"
                        theme="simple"
                        v-bind:placeholder-text="$localization.message('application.actions.search')"
                        v-model="filter"
                    >
                        <template v-slot:append>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z" fill="#C2CFE0" stroke="none" />
                            </svg>
                        </template>
                    </component>
                </template>
                <component is="v-control-table">
                    <template v-slot:head>
                        <component is="v-control-table-row">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:disabled="!hasFilteredItems"
                                    v-bind:value="isAllItemsSelected"
                                    v-on:checked="selectAllItems()"
                                    v-on:unchecked="deselectAllItems()"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible></component>
                            <component is="v-control-table-cell">
                                {{ $localization.message('c.section.settings.schedules.fields.name') }}
                            </component>
                        </component>
                    </template>
                    <template v-slot:default>
                        <component is="v-control-table-row" v-for="(schedule, index) in filteredItems" v-bind:key="index">
                            <component is="v-control-table-cell" flexible>
                                <component
                                    is="v-control-checkbox"
                                    v-bind:value="isItemSelected(schedule)"
                                    v-on:checked="selectItem(schedule)"
                                    v-on:unchecked="deselectItem(schedule)"
                                ></component>
                            </component>
                            <component is="v-control-table-cell" flexible>
                                <component is="v-control-dropdown-menu">
                                    <template v-slot:items>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('edit', schedule)">
                                            {{ $localization.message('application.actions.edit') }}
                                        </component>
                                        <component is="v-control-dropdown-menu-item" v-on:click="doAction('remove', schedule)">
                                            {{ $localization.message('application.actions.remove') }}
                                        </component>
                                    </template>
                                </component>
                            </component>
                            <component is="v-control-table-cell">
                                {{ schedule.name }}
                            </component>
                        </component>
                    </template>
                </component>
            </component>
        </template>
    </component>
</template>
