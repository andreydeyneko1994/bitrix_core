<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\Html;

?>
<?php foreach ($arResult as $arItem) { ?>
<?php
    $bLink = false;

    if (!empty($arItem['LINK']) && !$arItem['SELECTED'])
        $bLink = true;
?>
    <?= Html::tag($bLink ? 'a' : 'div', Html::encode($arItem['TEXT']), [
        'class' => 'insider-template-panel-menu-item',
        'href' => $bLink ? $arItem['LINK'] : null,
        'data' => [
            'active' => $arItem['SELECTED'] ? 'true' : 'false'
        ]
    ]) ?>
<?php } ?>
