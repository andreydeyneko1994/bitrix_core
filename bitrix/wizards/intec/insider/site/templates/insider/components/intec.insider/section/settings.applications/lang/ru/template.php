<?php

$MESS['c.section.settings.applications.title'] = 'Программы';
$MESS['c.section.settings.applications.filter.types.all'] = 'Все';
$MESS['c.section.settings.applications.fields.name'] = 'Наименование';
$MESS['c.section.settings.applications.fields.type'] = 'Тип';
$MESS['c.section.settings.applications.fields.group'] = 'Группа';
