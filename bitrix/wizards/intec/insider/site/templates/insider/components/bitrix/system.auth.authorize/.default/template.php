<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\Html;

?>
<div class="insider-page" data-page="authorize">
    <div class="insider-page-content">
        <div class="insider-page-title">
            <?= Loc::getMessage('c.systemAuthAuthorize.default.title') ?>
        </div>
        <div class="insider-page-description">
            <?= Loc::getMessage('c.systemAuthAuthorize.default.description') ?>
        </div>
        <?php if (!empty($arParams['~AUTH_RESULT'])) { ?>
            <div class="insider-page-error">
                <div class="insider-page-error-content">
                    <?= $arParams['~AUTH_RESULT']['MESSAGE'] ?>
                </div>
            </div>
        <?php } else if (!empty($arResult['ERROR_MESSAGE'])) { ?>
            <div class="insider-page-error">
                <div class="insider-page-error-content">
                    <?= $arResult['ERROR_MESSAGE'] ?>
                </div>
            </div>
        <?php } ?>
        <div class="insider-page-form">
            <?= Html::beginTag('form', [
                'action' => $arResult['AUTH_URL'],
                'method' => 'post',
                'name' => 'form_auth',
                'target' => '_top'
            ]) ?>
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="AUTH" />
                <?php if (strlen($arResult['BACKURL']) > 0) { ?>
                    <?= Html::input('hidden', 'backurl', $arResult['BACKURL']) ?>
                <?php } ?>
                <?php foreach ($arResult["POST"] as $sKey => $sValue) { ?>
                    <?= Html::input('hidden', $sKey, $sValue) ?>
                <?php } ?>
                <div class="insider-page-form-fields">
                    <div class="insider-page-form-field">
                        <div class="insider-page-form-field-title">
                            <?= Loc::getMessage('c.systemAuthAuthorize.default.fields.login') ?>
                        </div>
                        <div class="insider-page-form-field-content">
                            <input class="insider-page-form-input" type="text" name="USER_LOGIN" maxlength="255" value="<?= $arResult['LAST_LOGIN'] ?>" />
                        </div>
                    </div>
                    <div class="insider-page-form-field">
                        <div class="insider-page-form-field-title">
                            <?= Loc::getMessage('c.systemAuthAuthorize.default.fields.password') ?>
                        </div>
                        <div class="insider-page-form-field-content">
                            <input class="insider-page-form-input" autocomplete="off" type="password" name="USER_PASSWORD" maxlength="255" />
                        </div>
                    </div>
                    <?php if (!empty($arResult['CAPTCHA_CODE'])) { ?>
                        <div class="insider-page-form-field">
                            <div class="insider-page-form-field-title">
                                <?= Loc::getMessage('c.systemAuthAuthorize.default.fields.captcha') ?>
                            </div>
                            <div class="insider-page-form-field-content">
                                <?= Html::input('hidden', 'captcha_sid', $arResult['CAPTCHA_CODE']) ?>
                                <div class="insider-page-form-captcha">
                                    <div class="insider-page-form-captcha-field">
                                        <input class="insider-page-form-input" type="text" name="captcha_word" maxlength="50" />
                                    </div>
                                    <div class="insider-page-form-captcha-picture">
                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult['CAPTCHA_CODE']?>" alt="CAPTCHA" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="insider-page-form-field">
                        <div class="insider-page-form-field-content">
                            <label class="insider-page-form-switch">
                                <span class="insider-page-form-switch-control">
                                    <input type="checkbox" name="USER_REMEMBER" value="Y" />
                                    <span class="insider-page-form-switch-selector"></span>
                                </span>
                                <span class="insider-page-form-switch-text">
                                    <?= Loc::getMessage('c.systemAuthAuthorize.default.remember') ?>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="insider-page-form-field">
                        <div class="insider-page-form-field-content">
                            <button class="insider-page-form-button" type="submit">
                                <?= Loc::getMessage('c.systemAuthAuthorize.default.buttons.login') ?>
                            </button>
                        </div>
                    </div>
                    <div class="insider-page-form-field">
                        <div class="insider-page-form-field-content">
                            <a class="insider-page-form-link" href="/bitrix/admin/index.php#forgot_password">
                                <?= Loc::getMessage('c.systemAuthAuthorize.default.buttons.forgotPassword') ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?= Html::endTag('form') ?>
        </div>
    </div>
</div>
