<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

?>
<div class="insider-template-menu-wrapper">
    <?php foreach ($arResult as $arItem) { ?>
    <?php
        $sType = ArrayHelper::getValue($arItem, ['PARAMS', 'type']);
        $sType = ArrayHelper::fromRange(['item', 'delimiter', 'title'], $sType);
        $sIcon = ArrayHelper::getValue($arItem, ['PARAMS', 'icon']);

        $bLink = false;

        if (!empty($arItem['LINK']) && !$arItem['SELECTED'])
            $bLink = true;
    ?>
        <?php if ($sType === 'delimiter') { ?>
            <?= Html::tag('div', null, [
                'class' => 'insider-template-menu-delimiter'
            ]) ?>
        <?php } else if ($sType === 'title') { ?>
            <?= Html::tag('div', Html::encode($arItem['TEXT']), [
                'class' => 'insider-template-menu-title'
            ]) ?>
        <?php } else { ?>
            <?= Html::beginTag($bLink ? 'a' : 'div', [
                'class' => 'insider-template-menu-item',
                'href' => $bLink ? $arItem['LINK'] : null,
                'data' => [
                    'active' => $arItem['SELECTED'] ? 'true' : 'false'
                ]
            ]) ?>
                <div class="insider-template-menu-item-wrapper">
                    <div class="insider-template-menu-item-icon">
                        <?= $sIcon ?>
                    </div>
                    <div class="insider-template-menu-item-text">
                        <?= Html::encode($arItem['TEXT']) ?>
                    </div>
                </div>
            <?= Html::endTag($bLink ? 'a' : 'div') ?>
        <?php } ?>
    <?php } ?>
</div>
