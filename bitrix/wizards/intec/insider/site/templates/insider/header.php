<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use intec\Core;
use intec\core\helpers\JavaScript;
use intec\core\io\Path;
use intec\insider\template\Application;

global $APPLICATION;
global $USER;

if (!Loader::includeModule('intec.core'))
    exit;

if (!Loader::includeModule('intec.insider'))
    exit;

$directory = __DIR__;

Core::setAlias('@intec/insider/template', $directory.'/classes');
Core::$app->web->css->addFile($directory.'/css/application.css');
Core::$app->web->css->addFile($directory.'/css/grid.css');
Core::$app->web->css->addFile($directory.'/css/transitions.css');
Core::$app->web->css->addFile($directory.'/fonts/Montserrat/style.css');
Core::$app->web->js->loadExtensions([
    'intec_core',
    'axios',
    'qs',
    'moment',
    'vue',
    'vue_vuetify',
    'vue_vuescroll',
    'vue_script2'
]);

Core::$app->web->js->addFile($directory.'/js/application.js');

$application = new Application();
$page = !$USER->IsAdmin() || defined('ERROR_404') && ERROR_404 === 'Y';

?><!DOCTYPE html>
<html>
    <head>
        <title><?php $APPLICATION->ShowTitle() ?></title>
        <?php $APPLICATION->ShowHead() ?>
        <meta name="viewport" content="initial-scale=1.0, width=device-width">
        <?php if (!$page) { ?>
            <script type="text/javascript">
                (function (document, window) {
                    var configuration = <?= JavaScript::toObject([
                        'data' => [
                            'authorizedUser' => [
                                'firstName' => $USER->GetFirstName(),
                                'fullName' => $USER->GetFullName(),
                                'lastName' => $USER->GetLastName(),
                                'login' => $USER->GetLogin()
                            ],
                            'directory' => (new Path($directory.'/application'))->toRelative()->asAbsolute()->getValue('/'),
                            'language' => LANGUAGE_ID,
                            'site' => [
                                'id' => SITE_ID,
                                'directory' => SITE_DIR,
                                'links' => [
                                    'logout' => '?logout=yes&'.bitrix_sessid_get('sessid')
                                ]
                            ],
                            'template' => [
                                'id' => SITE_TEMPLATE_ID,
                                'directory' => SITE_TEMPLATE_PATH
                            ]
                        ],
                        'modules' => [
                            'actions' => [
                                'url' => (new Path($directory.'/application/actions.php'))->toRelative()->asAbsolute()->getValue('/')
                            ],
                            'localization' => [
                                'messages' => Loc::loadLanguageFile(__DIR__.'/application.php')
                            ]
                        ]
                    ]) ?>;

                    window.application = window.application(configuration);

                    document.addEventListener('DOMContentLoaded', function () {
                        window.application = window.application.compose();
                        window.application = new Vue(window.application);
                        window.application.$mount('#application');
                    });
                })(document, window);
            </script>
        <?php } ?>
    </head>
    <body>
        <div class="insider" id="application">
            <?php if (!$page) { ?>
                <component is="v-app">
                    <component is="v-template" ref="template" v-bind:loading="isLoading">
                        <template v-slot:logotype="slot">
                            <img v-if="slot.isExpanded" src="<?= SITE_TEMPLATE_PATH.'/images/logotype.png' ?>" />
                            <img v-else src="<?= SITE_TEMPLATE_PATH.'/images/logotype.small.png' ?>" />
                        </template>
                        <?php if ($USER->IsAuthorized()) { ?>
                            <template v-slot:menu>
                                <?php $APPLICATION->IncludeComponent('bitrix:menu', 'template.menu', [
                                    'ALLOW_MULTI_SELECT' => 'N',
                                    'CHILD_MENU_TYPE' => 'left',
                                    'DELAY' => 'N',
                                    'MAX_LEVEL' => '1',
                                    'MENU_CACHE_GET_VARS' => [],
                                    'MENU_CACHE_TIME' => '3600000',
                                    'MENU_CACHE_TYPE' => 'A',
                                    'MENU_CACHE_USE_GROUPS' => 'Y',
                                    'ROOT_MENU_TYPE' => 'left',
                                    'USE_EXT' => 'Y'
                                ], false, [
                                    'HIDE_ICONS' => 'Y'
                                ]) ?>
                            </template>
                        <?php } ?>
            <?php } ?>
