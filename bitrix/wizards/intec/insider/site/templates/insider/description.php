<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use Bitrix\Main\Localization\Loc;

$arTemplate = array (
    'NAME' => Loc::getMessage('intec.insider.template.name'),
    'DESCRIPTION' => Loc::getMessage('intec.insider.template.description'),
    'SORT' => '',
    'TYPE' => '',
);
