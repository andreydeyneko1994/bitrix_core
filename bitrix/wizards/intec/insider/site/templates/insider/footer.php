<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\insider\template\Application;

global $APPLICATION;

$application = Application::getInstance();
$page = !$USER->IsAdmin() || defined('ERROR_404') && ERROR_404 === 'Y';

?>
            <?php if (!$page) { ?>
                    </component>
                </component>
            <?php } ?>
        </div>
        <?= !$page ? $application->render() : null ?>
    </body>
</html>