<?php
namespace intec\insider\template;

use intec\core\base\BaseObject;
use intec\core\base\InvalidConfigException;

class Application extends BaseObject
{
    protected static $_instance;

    /**
     * @return static
     * @throws InvalidConfigException
     */
    public static function getInstance()
    {
        if (static::$_instance === null)
            throw new InvalidConfigException();

        return static::$_instance;
    }

    protected $_parts;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->_parts = [];

        static::$_instance = $this;
    }

    public function beginPart()
    {
        ob_start();
    }

    public function endPart()
    {
        $content = ob_get_contents();
        ob_end_clean();

        if (!empty($content))
            $this->_parts[] = $content;
    }

    public function render()
    {
        $content = '';

        foreach ($this->_parts as $part)
            $content .= $part;

        return $content;
    }
}