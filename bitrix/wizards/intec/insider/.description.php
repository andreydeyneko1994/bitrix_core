<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arSteps = [
    'BeginStep',
    'SiteStep',
    'TemplateStep',
    'InstallStep',
    'FinishStep'
];

$arWizardDescription = array(
    'NAME' => Loc::getMessage('intec.insider.wizard.name'),
    'DESCRIPTION' => Loc::getMessage('intec.insider.wizard.description'),
    'VERSION' => '1.0.0',
    'START_TYPE' => 'WINDOW',
    'WIZARD_TYPE' => 'INSTALL',
    'IMAGE' => '/images/'.LANGUAGE_ID.'/solution.png',
    'PARENT' => 'wizard_sol',
    'TEMPLATES' => array(
        array('SCRIPT' => 'wizard_sol')
    ),
    'STEPS' => $arSteps
);