<?
$MESS['wizard.steps.begin.title'] = 'Начало';
$MESS['wizard.steps.begin.description'] = 'Добро пожаловать в мастер развертывания сервиса "Инсайдер"';
$MESS['wizard.steps.begin.noModule'] = 'Не установлен модуль "<a target="_blank" href="http://marketplace.1c-bitrix.ru/solutions/#MODULE_ID#/">#MODULE_ID#</a>", возможно он уже есть в <a target="_blank" href="/bitrix/admin/partner_modules.php">списке</a> ваших решений, в таком случае вам необходимо просто установить его!';