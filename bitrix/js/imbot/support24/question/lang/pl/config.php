<?php
$MESS["IMBOT_SUPPORT24_QUESTION_BUTTON_ASK_TITLE"] = "Zadaj pytanie";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_DESCRIPTION"] = "Aby zadać kolejne pytanie, kliknij przycisk poniżej. To okno pokaże wszystkie zadane tutaj pytania.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_EMPTY_TITLE"] = "Masz więcej pytań?";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_RESTRICTION_NOT_ADMIN"] = "Niewystarczające uprawnienia. Wyłącznie administrator może zadawać więcej pytań.";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH"] = "Wyszukiwanie ...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCHING"] = "Wyszukiwanie...";
$MESS["IMBOT_SUPPORT24_QUESTION_LIST_SEARCH_NOT_FOUND"] = "Przepraszamy, ale nie mogliśmy niczego znaleźć";
