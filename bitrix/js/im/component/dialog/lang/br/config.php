<?php
$MESS["IM_DIALOG_CLIPBOARD_COPY_SUCCESS"] = "Copiado para a Área de Transferência";
$MESS["IM_DIALOG_EMPTY"] = "- Não há mensagens -";
$MESS["IM_DIALOG_ERROR_DESC"] = "Por favor, tente novamente mais tarde.";
$MESS["IM_DIALOG_ERROR_TITLE"] = "Infelizmente, não conseguimos carregar o bate-papo em tempo real :(";
$MESS["IM_DIALOG_LOADING"] = "Por favor, aguarde";
$MESS["IM_MESSENGER_DIALOG_LOAD_MESSAGES"] = "Carregando mensagens...";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT"] = "Visualizado por: #USERS#";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT_PLURAL"] = "#USER# e [LINK] mais #COUNT# usuário(s)[/LINK]";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_USER"] = "Visualizado: #DATE#";
$MESS["IM_MESSENGER_DIALOG_WRITES_MESSAGE"] = "#USER# está digitando uma mensagem...";
$MESS["IM_QUOTE_PANEL_DEFAULT_TITLE"] = "Mensagem do sistema";
