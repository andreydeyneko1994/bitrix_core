<?php
$MESS["IM_DIALOG_CLIPBOARD_COPY_SUCCESS"] = "Copié dans le presse-papiers";
$MESS["IM_DIALOG_EMPTY"] = "- Aucun message -";
$MESS["IM_DIALOG_ERROR_DESC"] = "Veuillez réessayer plus tard.";
$MESS["IM_DIALOG_ERROR_TITLE"] = "Malheureusement, nous n'avons pas pu charger le chat en direct :(";
$MESS["IM_DIALOG_LOADING"] = "Veuillez patienter";
$MESS["IM_MESSENGER_DIALOG_LOAD_MESSAGES"] = "Chargement des messages...";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT"] = "Vu par : #USERS#";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT_PLURAL"] = "#USER# et [LINK]#COUNT# autre(s) utilisateur(s)[/LINK]";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_USER"] = "Vu : #DATE#";
$MESS["IM_MESSENGER_DIALOG_WRITES_MESSAGE"] = "#USER# écrit un message...";
$MESS["IM_QUOTE_PANEL_DEFAULT_TITLE"] = "Message système";
