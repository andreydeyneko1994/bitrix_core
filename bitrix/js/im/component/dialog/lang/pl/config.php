<?php
$MESS["IM_DIALOG_CLIPBOARD_COPY_SUCCESS"] = "Skopiowano do schowka";
$MESS["IM_DIALOG_EMPTY"] = "- Brak wiadomości -";
$MESS["IM_DIALOG_ERROR_DESC"] = "Spróbuj ponownie później.";
$MESS["IM_DIALOG_ERROR_TITLE"] = "Niestety, nie możemy załadować czatu na żywo :(";
$MESS["IM_DIALOG_LOADING"] = "Czekaj";
$MESS["IM_MESSENGER_DIALOG_LOAD_MESSAGES"] = "Ładowanie wiadomości...";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT"] = "Wyświetlone przez: #USERS#";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_CHAT_PLURAL"] = "#USER# i [LINK]#COUNT# więcej użytkowników[/LINK]";
$MESS["IM_MESSENGER_DIALOG_MESSAGES_READED_USER"] = "Wyświetlono: #DATE#";
$MESS["IM_MESSENGER_DIALOG_WRITES_MESSAGE"] = "#USER# pisze wiadomość...";
$MESS["IM_QUOTE_PANEL_DEFAULT_TITLE"] = "Komunikat systemowy";
