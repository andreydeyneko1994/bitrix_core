<?php
$MESS["IM_RECENT_USERS_TYPING"] = "están escribiendo mensajes...";
$MESS["IM_RECENT_USER_TYPING"] = "está escribiendo un mensaje...";
$MESS["IM_RECENT_WEEKDAY_0"] = "Lun";
$MESS["IM_RECENT_WEEKDAY_1"] = "Mar";
$MESS["IM_RECENT_WEEKDAY_2"] = "Mié";
$MESS["IM_RECENT_WEEKDAY_3"] = "Jue";
$MESS["IM_RECENT_WEEKDAY_4"] = "Vie";
$MESS["IM_RECENT_WEEKDAY_5"] = "Sáb";
$MESS["IM_RECENT_WEEKDAY_6"] = "Dom";
