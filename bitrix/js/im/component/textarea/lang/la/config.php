<?php
$MESS["BX_MESSENGER_TEXTAREA_BUTTON_SEND"] = "Enviar mensaje";
$MESS["BX_MESSENGER_TEXTAREA_FILE"] = "Enviar archivo";
$MESS["BX_MESSENGER_TEXTAREA_GIPHY"] = "Explorar imágenes GIF";
$MESS["BX_MESSENGER_TEXTAREA_PLACEHOLDER"] = "Escribir un mensaje...";
$MESS["BX_MESSENGER_TEXTAREA_SMILE"] = "Seleccionar emoticón";
