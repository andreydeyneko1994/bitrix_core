<?php
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_CHECKBOX_LABEL"] = "Utiliser le mode Diffusion";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_HINT"] = "En mode Diffusion, vous pouvez réserver la diffusion de vidéo et de son aux intervenants sélectionnés. Les autres participants (invités) peuvent regarder la diffusion et communiquer dans le chat.";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_LABEL"] = "Mode Diffusion";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_OFF"] = "Non";
$MESS["BX_IM_COMPONENT_CONFERENCE_BROADCAST_MODE_ON"] = "Oui";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CANCEL"] = "Annuler";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CHAT"] = "Chat";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_CREATE"] = "Ajouter";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_EDIT"] = "Éditer";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_INVITATION_COPY"] = "Copier l'invitation";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_SAVE"] = "Enregistrer";
$MESS["BX_IM_COMPONENT_CONFERENCE_BUTTON_START"] = "Commencer";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_INVITATION"] = "#CREATOR# vous invite à assister à la conférence
#TITRE#
Utilisez ce lien pour vous inscrire : #LINK#";
$MESS["BX_IM_COMPONENT_CONFERENCE_DEFAULT_TITLE"] = "Nouvelle conférence";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION"] = "Dépensé";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_HOURS"] = "heures";
$MESS["BX_IM_COMPONENT_CONFERENCE_DURATION_MINUTES"] = "minutes";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_COPIED"] = "Invitation copiée";
$MESS["BX_IM_COMPONENT_CONFERENCE_INVITATION_TITLE"] = "Message d'invitation à rejoindre la vidéoconférence";
$MESS["BX_IM_COMPONENT_CONFERENCE_NETWORK_ERROR"] = "Nous n'avons pas pu connecter votre Bitrix24, probablement en raison d'une faible connexion à Internet. Veuillez réessayer plus tard.";
$MESS["BX_IM_COMPONENT_CONFERENCE_NO_PASSWORD"] = "Non";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_CHECKBOX_LABEL"] = "Protéger par le mot de passe";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_EXISTS"] = "Oui";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_LABEL"] = "Mot de passe";
$MESS["BX_IM_COMPONENT_CONFERENCE_PASSWORD_PLACEHOLDER"] = "Saisissez le mot de passe";
$MESS["BX_IM_COMPONENT_CONFERENCE_PRESENTER_SELECTOR_LABEL"] = "Intervenants";
$MESS["BX_IM_COMPONENT_CONFERENCE_PUSH_ERROR"] = "Veuillez mettre à jour votre serveur Push pour utiliser la conférence";
$MESS["BX_IM_COMPONENT_CONFERENCE_START_DATE_AND_TIME"] = "Date et heure de l'évènement";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_LABEL"] = "Nom";
$MESS["BX_IM_COMPONENT_CONFERENCE_TITLE_PLACEHOLDER"] = "Saisissez un nom";
$MESS["BX_IM_COMPONENT_CONFERENCE_USER_SELECTOR_LABEL"] = "Participants";
$MESS["BX_IM_COMPONENT_CONFERENCE_VOXIMPLANT_ERROR_WITH_LINK"] = "Vous devez configurer l'environnement pour utiliser la fonction de conférence. <a onclick=\"top.BX.Helper.show('redirect=detail&code=11392174')\"> Plus d'informations</a>";
