<?php
$MESS["IM_MESSENGER_MESSAGE_FILE_DELETED"] = "La fichier a été supprimé";
$MESS["IM_MESSENGER_MESSAGE_MENU_TITLE"] = "Cliquez pour ouvrir le menu d'action, ou cliquez en maintenant #SHORTCUT# pour citer le message";
$MESS["IM_MESSENGER_MESSAGE_RETRY_TITLE"] = "Cliquez pour renvoyer le message";
$MESS["IM_MESSENGER_MESSAGE_USER_ANONYM"] = "Anonyme";
