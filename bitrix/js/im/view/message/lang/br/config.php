<?php
$MESS["IM_MESSENGER_MESSAGE_FILE_DELETED"] = "O arquivo foi excluído.";
$MESS["IM_MESSENGER_MESSAGE_MENU_TITLE"] = "Clique para abrir o menu de ação ou clique enquanto segura #SHORTCUT# para mencionar a mensagem";
$MESS["IM_MESSENGER_MESSAGE_RETRY_TITLE"] = "Clique para enviar a mensagem novamente";
$MESS["IM_MESSENGER_MESSAGE_USER_ANONYM"] = "Anônimo";
