<?php
$MESS["IM_MESSENGER_MESSAGE_FILE_DELETED"] = "Plik został usunięty";
$MESS["IM_MESSENGER_MESSAGE_MENU_TITLE"] = "Kliknij, aby otworzyć menu działań lub kliknij trzymając #SHORTCUT#, aby zacytować wiadomość";
$MESS["IM_MESSENGER_MESSAGE_RETRY_TITLE"] = "Kliknij, aby ponownie wysłać wiadomość";
$MESS["IM_MESSENGER_MESSAGE_USER_ANONYM"] = "Anonimowo";
