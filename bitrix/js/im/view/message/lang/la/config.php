<?php
$MESS["IM_MESSENGER_MESSAGE_FILE_DELETED"] = "La carpeta se ha eliminado";
$MESS["IM_MESSENGER_MESSAGE_MENU_TITLE"] = "Haga clic para abrir el menú de acción, o haga clic mientras mantiene presionado #SHORTCUT# para citar el mensaje";
$MESS["IM_MESSENGER_MESSAGE_RETRY_TITLE"] = "Haga clic para enviar el mensaje nuevamente";
$MESS["IM_MESSENGER_MESSAGE_USER_ANONYM"] = "Anónimo";
