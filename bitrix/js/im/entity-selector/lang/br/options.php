<?php
$MESS["IM_ENTITY_SELECTOR_ANNOUNCEMENT_SUPERTITLE"] = "Bate-papo de comunicado";
$MESS["IM_ENTITY_SELECTOR_BOT_SUPERTITLE"] = "Chat Bot";
$MESS["IM_ENTITY_SELECTOR_CALENDAR_SUPERTITLE"] = "Bate-papo do evento";
$MESS["IM_ENTITY_SELECTOR_CALL_SUPERTITLE"] = "Bate-papo de chamada";
$MESS["IM_ENTITY_SELECTOR_CHANNEL_SUPERTITLE"] = "Bate-papo público";
$MESS["IM_ENTITY_SELECTOR_CRM_SUPERTITLE"] = "Bate-papo de negócio";
$MESS["IM_ENTITY_SELECTOR_GROUP_SUPERTITLE"] = "Bate-papo privado";
$MESS["IM_ENTITY_SELECTOR_LINES_SUPERTITLE"] = "Open Channel";
$MESS["IM_ENTITY_SELECTOR_SONET_GROUP_SUPERTITLE"] = "Bate-papo do grupo de trabalho";
$MESS["IM_ENTITY_SELECTOR_SUPPORT24_NOTIFIER_SUPERTITLE"] = "Suporte24";
$MESS["IM_ENTITY_SELECTOR_TASKS_SUPERTITLE"] = "Bate-papo da tarefa";
$MESS["IM_ENTITY_SELECTOR_VIDEOCONF_SUPERTITLE"] = "Bate-papo de videoconferência";
