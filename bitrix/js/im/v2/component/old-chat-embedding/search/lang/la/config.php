<?php
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_GROUP"] = "Chat privado";
$MESS["IM_SEARCH_ITEM_CHAT_TYPE_OPEN"] = "Chat público";
$MESS["IM_SEARCH_RESULT_NOT_FOUND"] = "No se encontraron entradas.";
$MESS["IM_SEARCH_RESULT_NOT_FOUND_DESCRIPTION"] = "Intente con una frase de búsqueda diferente.";
$MESS["IM_SEARCH_SECTION_CHAT_USERS"] = "Chats con el usuario";
$MESS["IM_SEARCH_SECTION_NETWORK"] = "Canales abiertos externos";
$MESS["IM_SEARCH_SECTION_NETWORK_BUTTON"] = "Búsqueda de Bitrix24.Network";
$MESS["IM_SEARCH_SECTION_OPENLINES"] = "Canales abiertos";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_LESS"] = "Mostrar menos";
