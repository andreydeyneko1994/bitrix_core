<?php
$MESS["IM_SEARCH_SECTION_CHAT_PARTICIPANTS"] = "Membros do bate-papo";
$MESS["IM_SEARCH_SECTION_DEPARTMENTS"] = "Departamentos";
$MESS["IM_SEARCH_SECTION_RECENT"] = "Pesquisa recente";
$MESS["IM_SEARCH_SECTION_TITLE_SHOW_MORE"] = "Mostrar mais";
$MESS["IM_SEARCH_SECTION_USERS_AND_CHATS"] = "Usuários e bate-papos";
