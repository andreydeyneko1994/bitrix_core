<?php
$MESS["IM_RECENT_ACTIVE_CALL_HANGUP"] = "Raccrocher";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN"] = "Rejoindre";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_AUDIO"] = "Son uniquement";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_VIDEO"] = "Avec vidéo";
$MESS["IM_RECENT_BIRTHDAY"] = "a son anniversaire aujourd'hui !";
$MESS["IM_RECENT_BIRTHDAY_DATE"] = "aujourd'hui";
$MESS["IM_RECENT_CHAT_SELF"] = "c'est vous";
$MESS["IM_RECENT_CHAT_TYPE_GROUP"] = "Chat privé";
$MESS["IM_RECENT_CHAT_TYPE_OPEN"] = "Chat public";
$MESS["IM_RECENT_CONNECT_ERROR"] = "Nous n'avons pas pu connecter votre Bitrix24, probablement en raison d'une faible connexion à Internet. Veuillez réessayer plus tard.";
$MESS["IM_RECENT_DEFAULT_USER_TITLE"] = "Utilisateur";
$MESS["IM_RECENT_EMPTY"] = "Il n'y a pas de chats";
$MESS["IM_RECENT_INVITATION_NOT_ACCEPTED"] = "L'invitation n'a pas encore été acceptée";
$MESS["IM_RECENT_MESSAGE_DRAFT"] = "Brouillon";
$MESS["IM_RECENT_NEW_USER_POPUP_TEXT"] = "Je suis dans votre équipe maintenant !";
