<?php
$MESS["IM_RECENT_ACTIVE_CALL_HANGUP"] = "Rozłącz";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN"] = "Dołącz";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_AUDIO"] = "Tylko dźwięk";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_VIDEO"] = "Z wideo";
$MESS["IM_RECENT_BIRTHDAY"] = "ma dziś urodziny!";
$MESS["IM_RECENT_BIRTHDAY_DATE"] = "dzisiaj";
$MESS["IM_RECENT_CHAT_SELF"] = "to ty";
$MESS["IM_RECENT_CHAT_TYPE_GROUP"] = "Czat prywatny";
$MESS["IM_RECENT_CHAT_TYPE_OPEN"] = "Czat publiczny";
$MESS["IM_RECENT_CONNECT_ERROR"] = "Nie mogliśmy połączyć się z Twoim Bitrix24, prawdopodobnie z powodu słabego połączenia z Internetem. Spróbuj ponownie później.";
$MESS["IM_RECENT_DEFAULT_USER_TITLE"] = "Użytkownik";
$MESS["IM_RECENT_EMPTY"] = "Brak czatów";
$MESS["IM_RECENT_INVITATION_NOT_ACCEPTED"] = "Zaproszenie nie zostało przyjęte";
$MESS["IM_RECENT_MESSAGE_DRAFT"] = "Wersja robocza";
$MESS["IM_RECENT_NEW_USER_POPUP_TEXT"] = "Teraz jestem w Twoim zespole!";
