<?php
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR"] = "Saisissez une adresse e-mail correcte";
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR_EMAIL_IS_ALREADY_EXISTS"] = "Un <a title=\"Open contact view form\" data-role=\"contact-email\">Contact</a> utilise déjà cette adresse e-mail";
$MESS["MAIL_DIALOG_EDIT_CONTACT_TITLE_BAR_ADD"] = "Nouveau contact";
