<?php
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR"] = "Wprowadź prawidłowy adres e-mail";
$MESS["MAIL_DIALOG_EDIT_CONTACT_EMAIL_ERROR_EMAIL_IS_ALREADY_EXISTS"] = "<a title=\"Open contact view form\" data-role=\"contact-email\">Kontakt</a> o tym adresie e-mail już istnieje";
$MESS["MAIL_DIALOG_EDIT_CONTACT_TITLE_BAR_ADD"] = "Nowy kontakt";
