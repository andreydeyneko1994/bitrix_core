<?php
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_CLOSE"] = "Zamknij";
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_GOTO"] = "Otwórz";
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_OPEN_HERE"] = "Mam aplikację WhatsApp na komputerze";
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_OPEN_MOBILE"] = "Otwórz WhatsApp na moim telefonie";
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_SCAN_QR_CODE"] = "Zeskanuj kod QR telefonem";
$MESS["IMCONNECTOR_NOTIFICATIONS_WIDGET_SELECT_COMMUNICATION_WAY"] = "Jak chcesz korzystać z WhatsApp?";
