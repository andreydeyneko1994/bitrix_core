<?
$MESS["JS_DISK_DOC_PROCESS_CREATE_DESCR_SAVE_DOC_F"] = "Vous êtes actuellement en train de modifier un nouveau document dans une des fenêtres. Dès que vous aurez fini, cliquez sur \"#SAVE_AS_DOC#\" pour ajouter le document à votre Intranet.";
$MESS["JS_DISK_DOC_PROCESS_EDIT_IN_SERVICE"] = "Modifier avec #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_DESCR_SAVE_DOC_F"] = "Vous éditez actuellement ce document dans l'une des fenêtres. Une fois que vous avez terminé, cliquez sur \"#SAVE_DOC#\" pour télécharger le document sur votre portail.";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_PROCESS_SAVE_DOC"] = "Enregistrer le document";
$MESS["JS_DISK_DOC_PROCESS_NOW_CREATING_IN_SERVICE"] = "Créer un document avec #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_DOWNLOAD_FROM_SERVICE"] = "Télécharger le document depuis #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_EDITING_IN_SERVICE"] = "Modification avec #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_SAVE"] = "Enregistrer";
$MESS["JS_DISK_DOC_PROCESS_SAVE_AS"] = "Enregistrer sous";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM"] = "Essayez d'<a href='#' id='#ID#'>ouvrir le document</a> avec Bitrix24.Desktop";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_OR_DOWNLOAD"] = "ou <a href=\"#DOWNLOAD_LINK#\" target=\"_blank\">téléchargez-le</a>.";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_TITLE"] = "Échec de l'ouverture du fichier. Essayez de <a href='#' id='#ID#'>l'ouvrir</a> en utilisant Bitrix24.Drive.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP"] = "Exécuter l'application Bitrix24";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DESCR"] = "Malheureusement, nous n'avons pas pu ouvrir le fichier en édition. Assurez-vous que l'application Bitrix24 est installée. <br><br>Si l'application est installée, veuillez attendre que le document soit ouvert ou réessayer plus tard.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DOWNLOAD"] = "Télécharger l'application";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_HELP"] = "Plus d'informations sur l'utilisation des documents";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_TITLE"] = "Modifier";
?>