<?
$MESS["JS_DISK_DOC_PROCESS_CREATE_DESCR_SAVE_DOC_F"] = "Obecnie edytujesz nowy dokument w jednym z okien. Gdy skończysz kliknij \"#SAVE_AS_DOC#\", aby dodać dokument do Intranetu.";
$MESS["JS_DISK_DOC_PROCESS_EDIT_IN_SERVICE"] = "Edytuj z #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_DESCR_SAVE_DOC_F"] = "Obecnie edytujesz ten dokument w jednym z okien. Gdy skończysz kliknij \"#SAVE_DOC#\", aby wgrać dokument do Intranetu.";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_PROCESS_SAVE_DOC"] = "Zapisz dokument";
$MESS["JS_DISK_DOC_PROCESS_NOW_CREATING_IN_SERVICE"] = "Utwórz dokument z #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_DOWNLOAD_FROM_SERVICE"] = "Pobierz dokument z #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_NOW_EDITING_IN_SERVICE"] = "Edycja z #SERVICE#";
$MESS["JS_DISK_DOC_PROCESS_SAVE"] = "Zapisz";
$MESS["JS_DISK_DOC_PROCESS_SAVE_AS"] = "Zapisz jako";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM"] = "Spróbuj <a href='#' id='#ID#'>otworzyć dokument</a> za pomocą Bitrix24.Desktop";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_OR_DOWNLOAD"] = "lub <a href=\"#DOWNLOAD_LINK#\" target=\"_blank\">pobrania</a>.";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_TITLE"] = "Nie można otworzyć pliku. Spróbuj <a href='#' id='#ID#'>otwierania</a> za pomocą Bitrix24.Drive.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP"] = "Uruchom aplikację Bitrix24";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DESCR"] = "Niestety nie mogliśmy otworzyć pliku do edycji. Upewnij się, że masz zainstalowaną aplikację Bitrix24. <br><br>Jeśli aplikacja jest zainstalowana, poczekaj na otwarcie dokumentu lub spróbuj ponownie później.";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DOWNLOAD"] = "Pobierz aplikację";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_HELP"] = "Dowiedz się więcej o korzystaniu z dokumentów";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_TITLE"] = "Edytuj";
?>