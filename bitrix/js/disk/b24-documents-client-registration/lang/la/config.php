<?php
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_BUTTON"] = "Registrarse";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_AFTER_REG"] = "Desafortunadamente, su licencia no es compatible con #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_COMMON"] = "No se puede registrar. Intente de nuevo más tarde.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_DOMAIN_VERIFICATION"] = "No se puede validar el dominio (#DOMAIN#) al registrar #NAME#. Una de las posibles razones es que el sitio esté alojado en la Intranet y no esté disponible en la web.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_SELECT_SERVER_LABEL"] = "Seleccionar servidor:";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_WARNING"] = "Tenga en cuenta que debe tener una licencia activa para utilizar #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_ERROR_COMMON"] = "Error: No se puede desconectar. Intente de nuevo.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_MSG"] = "¿Seguro que desea dejar de usar #NAME#? <br>Siempre podrá conectarse de nuevo más tarde. <br><br> Atención: Es posible que se pierdan los cambios que se efectuaron en los documentos que están abiertos abiertos actualmente.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_TITLE"] = "Desconectar #NAME#";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_UNREGISTER_BTN"] = "Desconectar";
