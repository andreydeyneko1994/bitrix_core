<?php
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_BUTTON"] = "Cadastro";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_AFTER_REG"] = "Infelizmente, sua licença não suporta #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_COMMON"] = "Não é possível registrar. Por favor, tente novamente mais tarde.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_ERROR_DOMAIN_VERIFICATION"] = "Não é possível validar o domínio (#DOMAIN#) ao registrar #NAME#. Uma das possíveis razões é que o site pode estar hospedado na Intranet e não disponível na web.";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_SELECT_SERVER_LABEL"] = "Selecionar servidor:";
$MESS["JS_B24_DOCUMENTS_CLIENT_REGISTRATION_WARNING"] = "Observe que você precisa ter uma licença ativa para usar #NAME#.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_ERROR_COMMON"] = "Erro: não é possível desconectar. Por favor, tente novamente.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_MSG"] = "Você tem certeza que deseja parar de usar #NAME#? <br> Você sempre poderá conectar novamente mais tarde. <br><br> Atenção: você pode perder alterações feitas nos documentos atualmente abertos.";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_TITLE"] = "Desconectar #NAME#";
$MESS["JS_B24_DOCUMENTS_CLIENT_UNREGISTRATION_UNREGISTER_BTN"] = "Desconectar";
