<?
$MESS["DISK_JS_EL_DESCRIPTION_DAY_NUMERAL_1"] = "#COUNT# dni";
$MESS["DISK_JS_EL_DESCRIPTION_DAY_NUMERAL_21"] = "#COUNT# dni";
$MESS["DISK_JS_EL_DESCRIPTION_DAY_NUMERAL_2_4"] = "#COUNT# dni";
$MESS["DISK_JS_EL_DESCRIPTION_DAY_NUMERAL_5_20"] = "#COUNT# dni";
$MESS["DISK_JS_EL_DESCRIPTION_EXTERNAL_LINK_WITH_DEATH_TIME"] = "czas ograniczony";
$MESS["DISK_JS_EL_DESCRIPTION_EXTERNAL_LINK_WITH_PASSWORD"] = "chronione hasłem";
$MESS["DISK_JS_EL_DESCRIPTION_HOUR_NUMERAL_1"] = "#COUNT# godz.";
$MESS["DISK_JS_EL_DESCRIPTION_HOUR_NUMERAL_21"] = "#COUNT# godz.";
$MESS["DISK_JS_EL_DESCRIPTION_HOUR_NUMERAL_2_4"] = "#COUNT# godz.";
$MESS["DISK_JS_EL_DESCRIPTION_HOUR_NUMERAL_5_20"] = "#COUNT# godz.";
$MESS["DISK_JS_EL_DESCRIPTION_MINUTE_NUMERAL_1"] = "#COUNT# min";
$MESS["DISK_JS_EL_DESCRIPTION_MINUTE_NUMERAL_21"] = "#COUNT# min";
$MESS["DISK_JS_EL_DESCRIPTION_MINUTE_NUMERAL_2_4"] = "#COUNT# min";
$MESS["DISK_JS_EL_DESCRIPTION_MINUTE_NUMERAL_5_20"] = "#COUNT# min";
?>