<?php
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_DESCR_OFFICE365"] = "Pour afficher le fichier, ouvrez-le dans une nouvelle fenêtre";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_FILE_OFFICE365"] = "Ouvrir";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_HELP_HINT_OFFICE365"] = "<a href=\"#\" onclick='top.BX.Helper.show(\"redirect=detail&code=9562101\");event.preventDefault();'>Plus d'informations</a> sur commenté travailler sur des fichiers avec Office365.";
$MESS["JS_VIEWER_DOCUMENT_ITEM_SHOW_FILE_DIALOG_OAUTH_NOTICE"] = "Veuillez vous connecter à votre compte <a id=\"bx-js-disk-run-oauth-modal\" href=\"#\">#SERVICE#</a> pour afficher le fichier.";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVED"] = "Document #name# mis à jour";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVE_PROCESS"] = "Enregistrement du document : #name#";
