<?php
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_DESCR_OFFICE365"] = "Para visualizar o arquivo, abra-o em uma nova janela";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_FILE_OFFICE365"] = "Abrir";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_HELP_HINT_OFFICE365"] = "<a href=\"#\" onclick='top.BX.Helper.show(\"redirect=detail&code=9562101\");event.preventDefault();'>Saiba mais</a> sobre como trabalhar com arquivos no Office365.";
$MESS["JS_VIEWER_DOCUMENT_ITEM_SHOW_FILE_DIALOG_OAUTH_NOTICE"] = "Faça login na sua <a id=\"bx-js-disco-run-oauth-modal\" href=\"#\">#SERVICE#</a> conta para visualizar o arquivo.";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVED"] = "Documento #name# atualizado";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVE_PROCESS"] = "Salvando o documento: #name#";
