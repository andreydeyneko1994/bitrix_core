<?php
$MESS["JS_DISK_SHARING_LEGACY_POPUP_LABEL_NAME_ADD_RIGHTS_USER"] = "Dodaj więcej";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_LABEL_NAME_RIGHTS"] = "Uprawnienia dostępu";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_LABEL_NAME_RIGHTS_USER"] = "Użytkownicy";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_LABEL_OWNER"] = "Właściciel";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_OK_FILE_SHARE_MODIFIED"] = "Uprawnienia do współdzielonego pliku #FILE# zostały zmienione";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_TITLE_MODAL_2"] = "Udostępnianie";
$MESS["JS_DISK_SHARING_LEGACY_POPUP_TITLE_MODAL_3"] = "Udostępnij innym użytkownikom";
