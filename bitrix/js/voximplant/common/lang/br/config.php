<?php
$MESS["VOX_JS_COMMON_AGREE"] = "Concordo";
$MESS["VOX_JS_COMMON_CANCEL"] = "Cancelar";
$MESS["VOX_JS_COMMON_CLOSE"] = "Fechar";
$MESS["VOX_JS_COMMON_ERROR"] = "Erro";
$MESS["VOX_JS_COMMON_OK"] = "OK";
$MESS["VOX_JS_COMMON_TERMS_OF_SERVICE"] = "Termos de serviço";
