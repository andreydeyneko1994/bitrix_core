<?php
$MESS["VOX_JS_COMMON_AGREE"] = "Acepto";
$MESS["VOX_JS_COMMON_CANCEL"] = "Cancelar";
$MESS["VOX_JS_COMMON_CLOSE"] = "Cerrar";
$MESS["VOX_JS_COMMON_ERROR"] = "Error";
$MESS["VOX_JS_COMMON_OK"] = "OK";
$MESS["VOX_JS_COMMON_TERMS_OF_SERVICE"] = "Términos de Servicio";
