<?
$MESS["VOX_CALLER_ID_BUTTON_CONFIRM"] = "Confirmer";
$MESS["VOX_CALLER_ID_BUTTON_LINK"] = "Connecter";
$MESS["VOX_CALLER_ID_BUTTON_PROLONG"] = "Prolonger la connexion";
$MESS["VOX_CALLER_ID_BUTTON_REPEAT_CALL"] = "Rappeler";
$MESS["VOX_CALLER_ID_ENTER_CODE"] = "Saisissez le code de vérification";
$MESS["VOX_CALLER_ID_HINT_P1"] = "Un appel automatique sera effectué pour ce numéro de téléphone et le code de vérification sera fourni oralement.";
$MESS["VOX_CALLER_ID_HINT_P2"] = "Cet appel automatisé n'est pas gratuit. Vous serez facturé en conséquence.";
$MESS["VOX_CALLER_ID_HINT_P3"] = "Vous avez droit à 10 tentatives pour saisir correctement le code de vérification, après quoi le numéro spécifié sera déconnecté.";
$MESS["VOX_CALLER_ID_PROVIDE_INTERNATIONAL_NUMBER"] = "Saisissez le numéro au format international";
$MESS["VOX_CALLER_ID_TITLE"] = "Connecter un numéro";
$MESS["VOX_CALLER_ID_UNVERIFIED"] = "Le numéro connecté n'est pas confirmé";
$MESS["VOX_CALLER_ID_VERIFIED_UNTIL"] = "Numéro connecté jusqu'au #DATE#";
?>