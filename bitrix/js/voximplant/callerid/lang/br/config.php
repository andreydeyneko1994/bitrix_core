<?
$MESS["VOX_CALLER_ID_BUTTON_CONFIRM"] = "Confirmar";
$MESS["VOX_CALLER_ID_BUTTON_LINK"] = "Conectar";
$MESS["VOX_CALLER_ID_BUTTON_PROLONG"] = "Estender conexão";
$MESS["VOX_CALLER_ID_BUTTON_REPEAT_CALL"] = "Ligar novamente";
$MESS["VOX_CALLER_ID_ENTER_CODE"] = "Digite o código de verificação";
$MESS["VOX_CALLER_ID_HINT_P1"] = "Será feita uma chamada automática para este número de telefone e o código de verificação será fornecido por comando de voz.";
$MESS["VOX_CALLER_ID_HINT_P2"] = "Esta chamada automatizada não é gratuita. Você será cobrado de acordo.";
$MESS["VOX_CALLER_ID_HINT_P3"] = "Você tem 10 tentativas para inserir o código de verificação corretamente, após as quais o número especificado será desconectado.";
$MESS["VOX_CALLER_ID_PROVIDE_INTERNATIONAL_NUMBER"] = "Digite o número no formato internacional";
$MESS["VOX_CALLER_ID_TITLE"] = "Conectar um número";
$MESS["VOX_CALLER_ID_UNVERIFIED"] = "Número conectado não confirmado";
$MESS["VOX_CALLER_ID_VERIFIED_UNTIL"] = "Número conectado até #DATE#";
?>