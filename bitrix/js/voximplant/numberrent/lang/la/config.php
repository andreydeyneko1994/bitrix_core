<?php
$MESS["VI_CANCEL_DELETE_NUMBER_ERROR"] = "El proveedor del número de teléfono devolvió un error al intentar cancelar la desconexión. Intente de nuevo más tarde.";
$MESS["VI_DELETE_NUMBER_ERROR"] = "El proveedor del número de teléfono devolvió un error al desconectarse. Intente de nuevo más tarde.";
$MESS["VI_NUMBER_BUNDLE_WILL_BE_DELETED"] = "El paquete se desconectará en 24 horas.";
$MESS["VI_NUMBER_CONFIRM_ACTION"] = "Confirmar la acción";
$MESS["VI_NUMBER_CONFIRM_BUNDLE_DISCONNECTION"] = "El paquete no se puede desconectar parcialmente. ¿Quiere desconectar todos los números que hay en el paquete?";
$MESS["VI_NUMBER_ERROR"] = "Error";
$MESS["VI_NUMBER_NUMBER_DELETE_CONFIRM"] = "¿Seguro que desea desconectar el número #NUMBER# de su Bitrix24?";
$MESS["VI_NUMBER_NUMBER_RENTED_IN_BUNDLE"] = "Este número se alquiló junto con un paquete de #COUNT# números:";
$MESS["VI_NUMBER_NUMBER_WILL_BE_DELETED"] = "El número se desconectará en el transcurso de 24 horas.";
$MESS["VI_NUMBER_RENT_CLOSE"] = "Cerrar";
$MESS["VI_NUMBER_RENT_CONFIGURE_NUMBER"] = "Configurar el número";
$MESS["VI_NUMBER_RENT_CONFIGURE_NUMBERS"] = "Configurar los números";
$MESS["VI_NUMBER_RENT_CONGRATULATIONS"] = "¡Felicidades!";
$MESS["VI_NUMBER_RENT_DOCUMENTS_REQUIRED"] = "Para comenzar a utilizar el número alquilado, debe cargar la documentación legal.";
$MESS["VI_NUMBER_RENT_NUMBERS_ATTACHED"] = "¡Los números #NUMBERS# están conectados a su Bitrix24!";
$MESS["VI_NUMBER_RENT_NUMBER_ATTACHED"] = "¡El número #NUMBER# está conectado a su Bitrix24!";
$MESS["VI_NUMBER_RENT_NUMBER_RESERVED"] = "¡El número #NUMBER# está reservado para usted!";
$MESS["VI_NUMBER_RENT_UPLOAD_DOCUMENTS"] = "Subir documentación";
