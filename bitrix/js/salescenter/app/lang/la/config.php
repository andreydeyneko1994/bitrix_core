<?php
$MESS["SALESCENTER_ACTION_ADD_CUSTOM_TITLE"] = "Agregar página personalizada";
$MESS["SALESCENTER_APP_CHAT_MESSAGE_TITLE"] = "El cliente recibe un mensaje de chat";
$MESS["SALESCENTER_APP_CONTACT_BLOCK_TITLE_MESSAGE_2"] = "El cliente recibirá un mensaje en el número #PHONE#";
$MESS["SALESCENTER_APP_CONTACT_BLOCK_TITLE_MESSAGE_2_PAST_TIME"] = "El cliente recibió un mensaje en el número #PHONE#";
$MESS["SALESCENTER_APP_CONTACT_BLOCK_TITLE_SMS_2_PAST_TIME"] = "El cliente recibió un SMS en el número #PHONE#";
$MESS["SALESCENTER_AUTOMATION_BLOCK_TEXT"] = "Acciones posteriores al pago";
$MESS["SALESCENTER_AUTOMATION_BLOCK_TITLE"] = "Automatización";
$MESS["SALESCENTER_AUTOMATION_DELIVERY_FINISHED"] = "Acción en caso de entrega exitosa";
$MESS["SALESCENTER_BANNER_BTN_CONFIGURE"] = "Configurar";
$MESS["SALESCENTER_BANNER_BTN_HIDE"] = "Ocultar";
$MESS["SALESCENTER_BANNER_TEXT"] = "Su tienda online actualmente no cuenta con ningún método de pago. Conecte sistemas de pago para permitir que sus clientes paguen sus pedidos sin abandonar el chat.";
$MESS["SALESCENTER_BANNER_TITLE"] = "Configurar los pagos en línea";
$MESS["SALESCENTER_CANCEL"] = "Cancelar";
$MESS["SALESCENTER_CASHBOX_BLOCK_SETTINGS_TITLE"] = "Cómo configurar una caja registradora";
$MESS["SALESCENTER_CASHBOX_BLOCK_TITLE"] = "Seleccionar caja registradora en línea";
$MESS["SALESCENTER_CASHBOX_SET_BLOCK_TITLE"] = "Las cajas registradoras en línea están funcioando";
$MESS["SALESCENTER_CHECK_RECYCLE"] = "Revisar la papelera de reciclaje";
$MESS["SALESCENTER_CREATE_SHIPMENT"] = "Crear envío";
$MESS["SALESCENTER_DATA_UPDATE_ERROR"] = "Error al actualizar los datos.";
$MESS["SALESCENTER_DEFAULT_TITLE"] = "Ventas habilitadas por chat";
$MESS["SALESCENTER_DELIVERY_BLOCK_TEXT"] = "Servicios de envío";
$MESS["SALESCENTER_DELIVERY_BLOCK_TITLE"] = "Envío";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_CREATE_NEW_DOCUMENT"] = "Crear utilizando la plantilla";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_CREATE_NEW_TEMPLATE"] = "Configurar las plantillas";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_DOCUMENT_CREATED_LATER_SUFFIX"] = "#TITLE# (se creará con el pago)";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_DOCUMENT_NEW_SUFFIX"] = "#TITLE# (nuevo)";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_TITLE"] = "Imprimir formulario";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_WITHOUT_SIGNS"] = "Sin firma ni sello";
$MESS["SALESCENTER_DOCUMENT_SELECTOR_BLOCK_WITH_SIGNS"] = "Con firma y sello";
$MESS["SALESCENTER_ERROR_TEXT"] = "Compruebe la configuración o elimine la página de la lista";
$MESS["SALESCENTER_ERROR_TITLE"] = "No se puede mostrar la página";
$MESS["SALESCENTER_FACEBOOK_CATALOG_POPUP_SEND_B24_COMPILATION_LINK_BUTTON"] = "Enviar página de Bitrix24";
$MESS["SALESCENTER_FACEBOOK_CATALOG_POPUP_SET_BUTTON"] = "Configurar";
$MESS["SALESCENTER_FACEBOOK_CATALOG_POPUP_TITLE"] = "El catálogo de productos no está conectado a Facebook";
$MESS["SALESCENTER_FACEBOOK_CATALOG_POPUP_TITLE_1"] = "El catálogo de productos no está conectado a Facebook";
$MESS["SALESCENTER_HOW"] = "¿Cómo funciona?";
$MESS["SALESCENTER_INFO_CREATE"] = "Crear";
$MESS["SALESCENTER_INFO_ORDER_PAGE_DELETED"] = "No se encontró la página de pago. Es posible que haya sido eliminada.";
$MESS["SALESCENTER_INFO_PUBLIC"] = "Publicar";
$MESS["SALESCENTER_INFO_TEXT_BOTTOM_PUBLIC"] = "Para procesar pedidos, publique su tienda de ventas habilitadas por chat y la página de pedidos";
$MESS["SALESCENTER_JS_POPUP_CLOSE"] = "Cerrar";
$MESS["SALESCENTER_LEFT_CATALOG"] = "Catálogo";
$MESS["SALESCENTER_LEFT_CREATE_LINK_AND_SEND"] = "Crear y enviar un enlace a la página de pago";
$MESS["SALESCENTER_LEFT_CREATE_SHIPMENT"] = "Solo entrega";
$MESS["SALESCENTER_LEFT_FORMS_ALL"] = "Formularios CRM";
$MESS["SALESCENTER_LEFT_ORDERS"] = "Pedidos";
$MESS["SALESCENTER_LEFT_ORDER_ADD"] = "Crear un pedido";
$MESS["SALESCENTER_LEFT_PAGES"] = "Páginas de ventas habilitadas por chat";
$MESS["SALESCENTER_LEFT_PAYMENTS"] = "Pagos";
$MESS["SALESCENTER_LEFT_PAYMENT_ADD"] = "Recibir un pago";
$MESS["SALESCENTER_LEFT_PAYMENT_ADD_2"] = "CRM.Pago";
$MESS["SALESCENTER_LEFT_PAYMENT_AND_DELIVERY"] = "CRM.Pago y envío";
$MESS["SALESCENTER_LEFT_PAYMENT_BY_SMS"] = "Recibir pago por SMS";
$MESS["SALESCENTER_LEFT_PAYMENT_COMPANY_CONTACTS_SHORTER_VERSION"] = "Editar la información de contacto";
$MESS["SALESCENTER_LEFT_PAYMENT_COMPANY_CONTACTS_V3"] = "Proporcione su información de contacto";
$MESS["SALESCENTER_LEFT_PAYMENT_FREE_MESSAGES"] = "Mensajes de WhatsApp y SMS gratuitos";
$MESS["SALESCENTER_LEFT_PAYMENT_HOW_WORKS"] = "Cómo funciona";
$MESS["SALESCENTER_LEFT_PAYMENT_INTEGRATION"] = "Desarrollo de pedidos";
$MESS["SALESCENTER_LEFT_PAYMENT_OFFER_SCRIPT"] = "Sugerir un caso de uso";
$MESS["SALESCENTER_LEFT_SEND_BY_EMAIL"] = "Enviar por correo electrónico";
$MESS["SALESCENTER_LEFT_SEND_BY_SMS"] = "Enviar por SMS";
$MESS["SALESCENTER_LEFT_TAKE_PAYMENT"] = "Pago";
$MESS["SALESCENTER_LEFT_TAKE_PAYMENT_AND_CREATE_SHIPMENT"] = "Pago y entrega";
$MESS["SALESCENTER_MODIFIED"] = "Actualizado #AGO#";
$MESS["SALESCENTER_ORDER_CREATE_NOTIFICATION"] = "Se creó el pedido ##ORDER_ID#";
$MESS["SALESCENTER_ORDER_SELECTOR_ORDER_NUM"] = "Pedido ##ORDER_ID#";
$MESS["SALESCENTER_ORDER_SELECTOR_TOOLTIP"] = "De manera predeterminada, el pago y el envío se crean para el pedido más reciente. Si el pago es parte de algún otro pedido, selecciónelo primero.";
$MESS["SALESCENTER_PAYMENT_TITLE"] = "Pago";
$MESS["SALESCENTER_PAYMENT_TYPE_ADD"] = "Habilitar los pagos en línea";
$MESS["SALESCENTER_PAYSYSTEM_BLOCK_SETTINGS_TITLE"] = "Cómo configurar un sistema de pago";
$MESS["SALESCENTER_PAYSYSTEM_BLOCK_TITLE"] = "Seleccionar sistema de pago";
$MESS["SALESCENTER_PAYSYSTEM_SET_BLOCK_TITLE"] = "Sistemas de pago están funcionando";
$MESS["SALESCENTER_PRODUCT_ADD_DISCOUNT"] = "Agregar un descuento";
$MESS["SALESCENTER_PRODUCT_ADD_PRODUCT"] = "Agregar un producto";
$MESS["SALESCENTER_PRODUCT_ADD_PRODUCT_FROM_CATALOG"] = "Seleccionar del catálogo";
$MESS["SALESCENTER_PRODUCT_BEFORE_SEARCH_TITLE"] = "Introduzca el nombre del producto o servicio";
$MESS["SALESCENTER_PRODUCT_BLOCK_PROD_EXIST_DLG_NO"] = "Cancelar";
$MESS["SALESCENTER_PRODUCT_BLOCK_PROD_EXIST_DLG_OK"] = "Agregar";
$MESS["SALESCENTER_PRODUCT_BLOCK_PROD_EXIST_DLG_TEXT"] = "El producto \"#NAME#\" ya existe en la lista de productos. ¿Quieres agregar otro producto?";
$MESS["SALESCENTER_PRODUCT_BLOCK_PROD_EXIST_DLG_TITLE"] = "Este producto ya está agregado";
$MESS["SALESCENTER_PRODUCT_BLOCK_TITLE"] = "Seleccione productos y servicios a pagar";
$MESS["SALESCENTER_PRODUCT_BLOCK_TITLE_PAYMENT_VIEW"] = "Artículos por pagar";
$MESS["SALESCENTER_PRODUCT_BLOCK_TITLE_SHIPMENT_VIEW"] = "Artículos por enviar";
$MESS["SALESCENTER_PRODUCT_BLOCK_TITLE_SHORT_SHIPMENT"] = "Seleccionar los artículos para envío";
$MESS["SALESCENTER_PRODUCT_CREATE"] = "crear un producto";
$MESS["SALESCENTER_PRODUCT_DISCOUNT_EDIT_PAGE_URL_TITLE"] = "configurar";
$MESS["SALESCENTER_PRODUCT_DISCOUNT_PRICE_TITLE"] = "Precio por unidad";
$MESS["SALESCENTER_PRODUCT_DISCOUNT_TITLE"] = "Descuento";
$MESS["SALESCENTER_PRODUCT_HIDE_DISCOUNT"] = "Ocultar el descuento";
$MESS["SALESCENTER_PRODUCT_IS_NOT_AVAILABLE"] = "Existencias insuficientes";
$MESS["SALESCENTER_PRODUCT_MEASURE"] = "Unidades de medida";
$MESS["SALESCENTER_PRODUCT_NAME"] = "Nombre";
$MESS["SALESCENTER_PRODUCT_NEW_LABEL"] = "nuevo";
$MESS["SALESCENTER_PRODUCT_NOT_FOUND"] = "No se encontraron entradas.";
$MESS["SALESCENTER_PRODUCT_PRICE"] = "Precio por unidad (#CURRENCY_NAME#)";
$MESS["SALESCENTER_PRODUCT_PRICE_2"] = "Precio";
$MESS["SALESCENTER_PRODUCT_QUANTITY"] = "Cantidad (#MEASURE_NAME#)";
$MESS["SALESCENTER_PRODUCT_TITLE"] = "Producto / Servicio";
$MESS["SALESCENTER_PRODUCT_TOTAL_DISCOUNT"] = "El cliente ahorra";
$MESS["SALESCENTER_PRODUCT_TOTAL_RESULT"] = "Cantidad por pagar";
$MESS["SALESCENTER_PRODUCT_TOTAL_SUM"] = "Cantidad";
$MESS["SALESCENTER_RIGHT_ACTIONS_BUTTON"] = "Acciones";
$MESS["SALESCENTER_RIGHT_ACTION_ADD"] = "Agregar una página";
$MESS["SALESCENTER_RIGHT_ACTION_ADD_CUSTOM"] = "URL personalizada";
$MESS["SALESCENTER_RIGHT_ACTION_ADD_SITE_B24"] = "Página de Bitrix24 Sites";
$MESS["SALESCENTER_RIGHT_ACTION_ADD_STORE_B24"] = "Página de tiendas online de Bitrix24";
$MESS["SALESCENTER_RIGHT_ACTION_COPY_URL"] = "Copiar URL";
$MESS["SALESCENTER_RIGHT_ACTION_DELETE"] = "Eliminar";
$MESS["SALESCENTER_RIGHT_ACTION_EDIT"] = "Editar";
$MESS["SALESCENTER_RIGHT_ACTION_HIDE"] = "Quitar de la lista";
$MESS["SALESCENTER_RIGHT_FRAME_DENIED"] = "Para ver la página, copie la URL de la página y ábrala en una pestaña nueva";
$MESS["SALESCENTER_RIGHT_NOT_ACTIVE"] = "No se publicó la página";
$MESS["SALESCENTER_SAVE_ORDER"] = "Guardar el pedido";
$MESS["SALESCENTER_SEND"] = "Enviar";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_BITRIX24_NOT_AVAILABLE"] = "La mensajería de Bitrix24 mediante SMS y WhatsApp no está disponible en su plan actual.";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_BITRIX24_NOT_CONNECTED"] = "Los mensajes de SMS y WhatsApp Bitrix24 no están habilitados";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_BITRIX24_NOT_CONNECTED_FIX"] = "Conectar";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_BITRIX24_NOT_CONNECTED_WARNING"] = "Conecte los servicios de WhatsApp y SMS en su Bitrix24 para enviar notificaciones de pago";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SENDER"] = "Enviar por SMS";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SENDER_ALERT_PHONE_EMPTY_SETTINGS"] = "Configurar";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SENDER_PHONE_CHANGE"] = "Se actualizó la información de contacto para #TITLE#";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SENDER_TEMPLATE_ERROR"] = "Agregue #LINK# para mostrar el enlace";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SENDER_TEMPLATE_WHAT_DOES_CLIENT_SEE"] = "¿Qué ve el cliente?";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SMS_PROVIDER_NOT_CONNECTED"] = "El servicio de SMS no está configurado";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SMS_PROVIDER_NOT_CONNECTED_FIX"] = "Conectar";
$MESS["SALESCENTER_SEND_ORDER_BY_SMS_SMS_PROVIDER_NOT_CONNECTED_WARNING"] = "Conecte el servicio de SMS externo";
$MESS["SALESCENTER_SEND_ORDER_VIA_BITRIX24"] = "Enviando mediante Bitrix24";
$MESS["SALESCENTER_SHIPMENT_DELIVERY_PRICE_RECEIVED"] = "Estimación del precio de entrega recibida";
$MESS["SALESCENTER_SHIPMENT_EXTRA_SERVICES"] = "Más";
$MESS["SALESCENTER_SHIPMENT_PRODUCT_BLOCK_DELIVERY_PRICE"] = "Precio de entrega";
$MESS["SALESCENTER_SHIPMENT_PRODUCT_BLOCK_TOTAL"] = "Total de artículos";
$MESS["SALESCENTER_SUBMENU_CLOSE"] = "Contraer";
$MESS["SALESCENTER_SUBMENU_OPEN"] = "Expandir";
$MESS["SALESCENTER_VIEW"] = "Ver";
$MESS["UI_SIDEPANEL_MENU_BUTTON_CLOSE"] = "Contraer";
$MESS["UI_SIDEPANEL_MENU_BUTTON_OPEN"] = "Expandir";
