<?php
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_1"] = "Merci pour votre commande !";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_2"] = "Total de la commande :";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_3"] = "Mode de paiement :";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_4"] = "Payer";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_5"] = "Payée";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_6"] = "Veuillez sélectionner un système de paiement différent pour payer en ligne";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_7"] = "Sélectionnez un autre mode de paiement";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_8"] = "Malheureusement, une erreur est survenue.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_9"] = "Sélectionnez une option de paiement différente ou contactez un commercial.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_10"] = "Information";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_11"] = "Montant : #SUM#";
