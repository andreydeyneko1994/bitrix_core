<?php
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_1"] = "¡Gracias por hacer su pedido!";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_2"] = "Total del pedido:";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_3"] = "Método de pago:";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_4"] = "Paga";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_5"] = "Pagado";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_6"] = "Seleccione un método de pago diferente para pagar en línea";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_7"] = "Seleccionar un método de pago diferente";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_8"] = "Desafortunadamente ocurrió un error.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_9"] = "Seleccione una opción de pago diferente o comuníquese con uno de nuestros representantes de ventas.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_10"] = "Información";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_11"] = "Cantidad: #SUM#";
