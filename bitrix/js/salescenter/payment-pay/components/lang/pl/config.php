<?php
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_1"] = "Dziękujemy za zamówienie!";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_2"] = "Suma zamówienia:";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_3"] = "Metoda płatności:";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_4"] = "Zapłać";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_5"] = "Opłacono";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_6"] = "Aby zapłacić online, wybierz inny system płatności";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_7"] = "Wybierz inną metodę płatności";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_8"] = "Niestety wystąpił błąd.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_9"] = "Wybierz inną metodę płatności lub skontaktuj się z jednym z naszych przedstawicieli handlowych.";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_10"] = "Informacja";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_11"] = "Kwota: #SUM#";
