<?php
$MESS["SC_STORE_SETTINGS_CANCEL"] = "Cancelar";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME"] = "Nome da sua empresa";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME_HINT"] = "Os clientes verão esse nome na página de pagamento";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE"] = "Telefone da empresa";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD"] = "Adicionar";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD_SMALL"] = "adicionar";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DEFAULT"] = "Primeiro número de telefone nos dados da empresa";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DELIMETER"] = "selecionar número de telefone";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_EMPTY"] = "Nenhum número de telefone da empresa";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_NUMBER"] = "Telefone de contato";
$MESS["SC_STORE_SETTINGS_COMPANY_TITLE"] = "Nome da empresa";
$MESS["SC_STORE_SETTINGS_SAVE"] = "Salvar";
