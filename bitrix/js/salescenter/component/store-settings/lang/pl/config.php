<?php
$MESS["SC_STORE_SETTINGS_CANCEL"] = "Anuluj";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME"] = "Nazwa Twojej firmy";
$MESS["SC_STORE_SETTINGS_COMPANY_NAME_HINT"] = "Klienci zobaczą tę nazwę na stronie płatności";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE"] = "Telefon firmowy";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD"] = "Dodaj";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_ADD_SMALL"] = "dodaj";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DEFAULT"] = "Pierwszy numer telefonu w danych firmy";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_DELIMETER"] = "wybierz numer telefonu";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_EMPTY"] = "Brak firmowego numeru telefonu";
$MESS["SC_STORE_SETTINGS_COMPANY_PHONE_NUMBER"] = "Telefon kontaktowy";
$MESS["SC_STORE_SETTINGS_COMPANY_TITLE"] = "Nazwa firmy";
$MESS["SC_STORE_SETTINGS_SAVE"] = "Zapisz";
