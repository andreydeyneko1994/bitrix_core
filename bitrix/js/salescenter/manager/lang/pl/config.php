<?
$MESS["SALESCENTER_ACTION_ADD_CUSTOM_TITLE"] = "Dodaj stronę niestandardową";
$MESS["SALESCENTER_MANAGER_ADD_URL_SUCCESS"] = "Dodano stronę";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_DESCRIPTION"] = "Dodaj informacje o swojej firmie i zacznij sprzedawać za pośrednictwem komunikatorów i sieci społecznościowych.";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_GO_BUTTON"] = "Edytuj informacje o firmie";
$MESS["SALESCENTER_MANAGER_CONNECT_POPUP_TITLE"] = "Twój sklep internetowy został utworzony!";
$MESS["SALESCENTER_MANAGER_COPY_URL_SUCCESS"] = "Link skopiowano do schowka";
$MESS["SALESCENTER_MANAGER_DELETE_URL_SUCCESS"] = "Strona została usunięta";
$MESS["SALESCENTER_MANAGER_ERROR_POPUP"] = "Wystąpił błąd";
$MESS["SALESCENTER_MANAGER_HIDE_URL_SUCCESS"] = "Strona została ukryta";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_COMPLETE"] = "Utworzono nową stronę";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_POPUP_TITLE"] = "Utwórz nową stronę";
$MESS["SALESCENTER_MANAGER_NEW_PAGE_WAIT"] = "Czekaj, tworzona jest nowa strona";
$MESS["SALESCENTER_MANAGER_UPDATE_URL_SUCCESS"] = "Strona została zaktualizowana";
?>