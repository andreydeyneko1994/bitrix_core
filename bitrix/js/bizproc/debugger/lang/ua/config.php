<?php
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_1"] = "Зараз активний фільтр [b]«Угоди в налагодженні»[/ b]. Тут показуватимуться ваші угоди для налагодження роботів.";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_2"] = "Щоб побачити свої угоди, змініть фільтр на [b]«Угоди в роботі»[/b] або [b]«Мої угоди»[/b].";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TITLE"] = "Не хвилюйтеся, усі ваші угоди на місці";
