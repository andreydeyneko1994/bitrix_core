<?php
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_1"] = "O filtro [b]Apenas negócios de teste[/b] está ativo no momento. Essa visualização mostra os negócios que o sistema usa para testar suas regras de automação.";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_2"] = "Para visualizar seus negócios, altere o filtro para [b]Negócios em andamento[/b] ou [b]Meus negócios[/b].";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TITLE"] = "Não se preocupe, seus negócios estão seguros";
