<?php
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_1"] = "Le filtre [b]Offres en débogage[/b] est actuellement actif. Cette vue montre les transactions que le système utilise pour tester vos règles d'automatisation.";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TEXT_LINE_2"] = "Pour afficher vos transactions, changez le filtre en [b]Transactions en cours[/b] ou [b]Mes transactions[/b].";
$MESS["BIZPROC_JS_DEBUGGER_FILTER_TOUR_TITLE"] = "Ne vous inquiétez pas, vos transactions sont saines et sauves";
