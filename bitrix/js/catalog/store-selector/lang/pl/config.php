<?php
$MESS["CATALOG_STORE_SELECTOR_BEFORE_SEARCH_TITLE"] = "Znajdź lub utwórz magazyn";
$MESS["CATALOG_STORE_SELECTOR_EMPTY_STORE_TITLE"] = "Bez nazwy";
$MESS["CATALOG_STORE_SELECTOR_IS_EMPTY_SUBTITLE"] = "Utworzyć nowy magazyn?";
$MESS["CATALOG_STORE_SELECTOR_IS_EMPTY_TITLE"] = "Nie znaleziono magazynu";
$MESS["CATALOG_STORE_SELECTOR_VIEW_NAME_TITLE"] = "Magazyn";
