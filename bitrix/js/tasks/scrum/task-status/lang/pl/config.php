<?php
$MESS["TST_ERROR_POPUP_TITLE"] = "Wystąpił błąd";
$MESS["TST_PARENT_COMPLETE_CANCEL_CAPTION"] = "Nie kończ";
$MESS["TST_PARENT_COMPLETE_MESSAGE"] = "Wszystkie podzadania \"<b>#name#</b>\" zostały zakończone.<br><br>Czy chcesz zamknąć zadanie \"<b>#name#</b>\", czy kontynuować nad nim pracę?";
$MESS["TST_PARENT_COMPLETE_NOTIFY"] = "Zadanie nadrzędne zostało pomyślnie zakończone.";
$MESS["TST_PARENT_COMPLETE_OK_CAPTION"] = "Zakończ";
$MESS["TST_PARENT_PROCEED_CAPTION"] = "Kontynuuj";
$MESS["TST_PARENT_PROCEED_NOTIFY"] = "Możesz kontynuować pracę nad zadaniem nadrzędnym w ramach kanbana sprintu.";
$MESS["TST_PARENT_RENEW_CANCEL_CAPTION"] = "Nie wznawiaj";
$MESS["TST_PARENT_RENEW_MESSAGE"] = "Podzadanie \"#sub-name#\" zostało wznowione.<br><br>Czy chcesz wznowić zadanie \"<b>#name#</b>\"?";
$MESS["TST_PARENT_RENEW_NOTIFY"] = "Zadanie nadrzędne zostało wznowione.";
$MESS["TST_PARENT_RENEW_OK_CAPTION"] = "Wznów";
