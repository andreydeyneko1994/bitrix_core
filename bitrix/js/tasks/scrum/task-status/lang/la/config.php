<?php
$MESS["TST_ERROR_POPUP_TITLE"] = "Se produjo un error";
$MESS["TST_PARENT_COMPLETE_CANCEL_CAPTION"] = "No finalizar";
$MESS["TST_PARENT_COMPLETE_MESSAGE"] = "Todas las subtareas de \"<b>#name#</b>\" se completaron.<br><br>¿Quiere cerrar la tarea \"<b>#name#</b>\" o seguir trabajando en ella?";
$MESS["TST_PARENT_COMPLETE_NOTIFY"] = "La tarea principal se completó correctamente.";
$MESS["TST_PARENT_COMPLETE_OK_CAPTION"] = "Completar";
$MESS["TST_PARENT_PROCEED_CAPTION"] = "Continuar";
$MESS["TST_PARENT_PROCEED_NOTIFY"] = "Puede seguir trabajando en la tarea principal en el kanban del sprint.";
$MESS["TST_PARENT_RENEW_CANCEL_CAPTION"] = "No reanudar";
$MESS["TST_PARENT_RENEW_MESSAGE"] = "Se reanudó la subtarea \"#sub-name#\".<br><br>¿Desea reanudar la tarea \"<b>#name#</b>\"?";
$MESS["TST_PARENT_RENEW_NOTIFY"] = "La tarea principal se reanudó.";
$MESS["TST_PARENT_RENEW_OK_CAPTION"] = "Reanudar";
