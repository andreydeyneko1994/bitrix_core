<?
$MESS["SONET_EXT_SELECTOR_CREATE_SONETGROUP_BUTTON_CANCEL"] = "Anuluj";
$MESS["SONET_EXT_SELECTOR_CREATE_SONETGROUP_BUTTON_CREATE"] = "Utwórz";
$MESS["SONET_EXT_SELECTOR_CREATE_SONETGROUP_TITLE"] = "Nie ma grupy o tej nazwie. Czy chcesz utworzyć grupę „#TITLE#”?";
$MESS["SONET_EXT_SELECTOR_INVITE_EMAIL_CRM_CREATE_CONTACT"] = "Utwórz kontakt CRM";
$MESS["SONET_EXT_SELECTOR_INVITE_EMAIL_USER_BUTTON_OK"] = "OK";
$MESS["SONET_EXT_SELECTOR_INVITE_EMAIL_USER_PLACEHOLDER_LAST_NAME"] = "Nazwisko";
$MESS["SONET_EXT_SELECTOR_INVITE_EMAIL_USER_PLACEHOLDER_NAME"] = "Imię";
$MESS["SONET_EXT_SELECTOR_INVITE_EMAIL_USER_TITLE"] = "Ta wiadomość zostanie udostępniona pocztą elektroniczną użytkownikowi";
?>