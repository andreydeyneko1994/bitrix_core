<?php
$MESS["SOCNET_ENTITY_SELECTOR_CREATE_PROJECT"] = "Criar um grupo de trabalho";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT"] = "<employee>Convidar um usuário</employee><span>ou</span><project>criar um grupo de trabalho</project>";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT_OR_GUEST"] = "<employee>Convidar um usuário</employee><span>ou</span><project>criar um grupo de trabalho</project><span>ou</span><guest>convidar um participante</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_GUEST_HINT"] = "Você também pode adicionar seu parceiro ou cliente por e-mail.";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_USERS_TAB_TITLE"] = "Pessoas convidadas";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE"] = "Convidar um usuário";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_GUEST"] = "<employee>Convidar um usuário</employee><span>ou</span><guest>convidar um participante</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_GUEST"] = "Convidar um participante";
$MESS["SOCNET_ENTITY_SELECTOR_PROJECT_OR_GUEST"] = "<project>Criar um grupo de trabalho</project><span>ou</span><guest>convidar um participante</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_TAG_FOOTER_LABEL"] = "Comece a digitar para criar um novo marcador";
