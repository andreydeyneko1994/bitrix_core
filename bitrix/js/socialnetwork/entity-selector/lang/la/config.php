<?php
$MESS["SOCNET_ENTITY_SELECTOR_CREATE_PROJECT"] = "Crear un grupo de trabajo";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT"] = "<employee>Invitar a un usuario</employee><span>o</span><project>crear un grupo de trabajo</project>";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT_OR_GUEST"] = "<employee>Invitar a un 
usuario</employee><span>o</span><project>crear un grupo de trabajo</project><span>o</span><guest>invitar a un visitante</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_GUEST_HINT"] = "También puede agregar a su socio o cliente mediante un correo electrónico.";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_USERS_TAB_TITLE"] = "Personas invitadas";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE"] = "Invitar a un usuario";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_GUEST"] = "<employee>Invitar a un usuario</employee><span>o</span><guest>Invitar a un huésped</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_GUEST"] = "Invitar a un huésped";
$MESS["SOCNET_ENTITY_SELECTOR_PROJECT_OR_GUEST"] = "<project>Crear un grupo de trabajo</project><span>o</span><guest>invitar a un visitante</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_TAG_FOOTER_LABEL"] = "Empiece a escribir para crear una nueva etiqueta";
