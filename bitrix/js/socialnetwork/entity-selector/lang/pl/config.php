<?php
$MESS["SOCNET_ENTITY_SELECTOR_CREATE_PROJECT"] = "Utwórz grupę roboczą";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT"] = "<employee>Zaproś użytkownika</employee><span>lub</span><project>utwórz grupę roboczą</project>";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT_OR_GUEST"] = "<employee>Zaproś użytkownika</employee><span>lub</span><project>utwórz grupę roboczą</project><span>lub</span><guest>zaproś gościa</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_GUEST_HINT"] = "Możesz również dodać partnera lub klienta za pośrednictwem poczty e-mail.";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_USERS_TAB_TITLE"] = "Zaproszone osoby";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE"] = "Zaproś użytkownika";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_GUEST"] = "<employee>Zaproś użytkownika</employee><span>lub</span><guest>zaproś gościa</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_GUEST"] = "Zaproś gościa";
$MESS["SOCNET_ENTITY_SELECTOR_PROJECT_OR_GUEST"] = "<project>Utwórz grupę roboczą</project><span>lub</span><guest>zaproś gościa</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_TAG_FOOTER_LABEL"] = "Zacznij pisać, aby utworzyć nowy tag";
