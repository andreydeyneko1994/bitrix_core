<?php
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "La tarea \"#TASK_NAME#\" ha sido creada en un mensaje de noticias.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_FORUM_TOPIC"] = "La tarea \"#TASK_NAME#\" ha sido creada basado en una publicación del foro.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_TASK"] = "La tarea \"#TASK_NAME#\" se ha creado en base a otra tarea.";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "una nueva version de archivo fue cargada";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "una nueva version de archivo fue cargada";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "una nueva version de archivo fue cargada";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "editado el archivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "editado el archivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "editado el archivo";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Compartido con: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Compartido con: #SHARE_LIST#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BITRIX24_NEW_USER"] = "La tarea se creó con base en la #A_BEGIN#adición de un nuevo usuario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_COMMENT"] = "La tarea fue creada en base a un #A_BEGIN#Activity Stream post comment#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_POST"] = "La tarea fue creada en base a una #A_BEGIN#Post en el noticias#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_CALENDAR_EVENT"] = "La tarea fue creada en base a un #A_BEGIN#evento del calendario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST"] = "La tarea fue creada en base a un #A_BEGIN#Comentario del post del noticias#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_CALENDAR"] = "La tarea fue creada en base a un #A_BEGIN#comentario del evento del calendario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_FORUM_TOPIC"] = "La tarea fue creada en base a un #A_BEGIN#comentario de la publicación del foro#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_LISTS_NEW_ELEMENT"] = "La tarea fue creada en base a un #A_BEGIN#Comentario de la entrada de procesos de negocio#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_PHOTO_PHOTO"] = "La tarea fue creada en base a un #A_BEGIN#comentario de la foto del album#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TASK"] = "La tarea fue creada en base a #A_BEGIN#comentario de la tarea#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_ENTRY"] = "La tarea fue creada en base a un #A_BEGIN#actualización de horas de trabajo#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_REPORT"] = "La tarea fue creada en base a un #A_BEGIN#comentario del reporte de trabajo#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_WIKI"] = "La tarea fue creada en base a un #A_BEGIN#Comentario de la página de Wiki#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_TOPIC"] = "La tarea fue creada en base a #A_BEGIN#forum post#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_INTRANET_NEW_USER"] = "La tarea se creó con base en la #A_BEGIN#adición de un nuevo usuario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LISTS_NEW_ELEMENT"] = "La tarea fue creada en base a un a #A_BEGIN#entrada del procesos de negocio#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT"] = "La tarea fue creada en base a un #A_BEGIN#Comentario del noticias#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_BITRIX24_NEW_USER"] = "La tarea se creó con base en un #A_BEGIN#comentario que se dejó para la adición de un nuevo usuario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_INTRANET_NEW_USER"] = "La tarea se creó con base en un #A_BEGIN#comentario que se dejó para la adición de un nuevo usuario#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_ENTRY"] = "La tarea fue creada en base a un #A_BEGIN#post del noticias#A_END# obtenido de una fuente externa";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_ALBUM"] = "La tarea fue creada en base a un #A_BEGIN#Photo Gallery album#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_PHOTO"] = "La tarea fue creada en base a un #A_BEGIN#album de fotos#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TASK"] = "La tarea fue creada en base a #A_BEGIN#otra tarea#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_ENTRY"] = "La tarea fue creada en base a un #A_BEGIN#actualización de horas de trabajo#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_REPORT"] = "La tarea fue creada en base a un #A_BEGIN#reporte de trabajo#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_WIKI"] = "La tarea fue creada en base a un #A_BEGIN#Página de wiki#A_END#";
