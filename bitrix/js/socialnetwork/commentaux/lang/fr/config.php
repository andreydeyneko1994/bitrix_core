<?php
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "La tâche \"#TASK_NAME#\" a été créée sur un message du Flux d'activités.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_FORUM_TOPIC"] = "La tâche \"#TASK_NAME#\" a été créée d'après une publication du forum.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_TASK"] = "La tâche \"#TASK_NAME#\" a été créée d'après une autre tâche.";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "a modifié le fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "a modifié le fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "a modifié le fichier";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Partagé avec : #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Partagé avec : #SHARE_LIST#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BITRIX24_NEW_USER"] = "La tâche a été créée d'après une #A_BEGIN#entrée d'ajout de nouvel utilisateur#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_COMMENT"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de publication du flux d'activités#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_BLOG_POST"] = "La tâche a été créée d'après une #A_BEGIN#publication du flux d'activités#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_CALENDAR_EVENT"] = "La tâche a été créée d'après un #A_BEGIN#évènement du calendrier#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de publication du flux d'activités#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_CALENDAR"] = "La tâche a été créée d'après un #A_BEGIN#commentaire d'évènement du calendrier#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_FORUM_TOPIC"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de publication du forum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_LISTS_NEW_ELEMENT"] = "La tâche a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée d'un flux de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_PHOTO_PHOTO"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de photo d'album#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TASK"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de tâche#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_ENTRY"] = "La tâche a été créée d'après une #A_BEGIN#mise à jour des heures de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_TIMEMAN_REPORT"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de rapport de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_POST_WIKI"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de page wiki#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_FORUM_TOPIC"] = "La tâche a été créée d'après une #A_BEGIN#publication du forum#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_INTRANET_NEW_USER"] = "La tâche a été créée d'après une #A_BEGIN#entrée d'ajout de nouvel utilisateur#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LISTS_NEW_ELEMENT"] = "La tâche a été créée d'après une #A_BEGIN#entrée de flux de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT"] = "La tâche a été créée d'après un #A_BEGIN#commentaire de publication du flux d'activités#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_BITRIX24_NEW_USER"] = "La tâche a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée d'ajout de nouvel utilisateur#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_COMMENT_INTRANET_NEW_USER"] = "La tâche a été créée d'après un #A_BEGIN#commentaire laissé sur une entrée d'ajout de nouvel utilisateur#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_LOG_ENTRY"] = "La tâche a été créée d'après une #A_BEGIN#publication du flux d'activités#A_END# obtenue d'une source externe";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_ALBUM"] = "La tâche a été créée d'après un #A_BEGIN#album de la galerie de photos#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_PHOTO_PHOTO"] = "La tâche a été créée d'après une #A_BEGIN#photo de l'album#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TASK"] = "La tâche a été créée d'après une #A_BEGIN#autre tâche#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_ENTRY"] = "La tâche a été créée d'après une #A_BEGIN#mise à jour des heures de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_TIMEMAN_REPORT"] = "La tâche a été créée d'après un #A_BEGIN#rapport de travail#A_END#";
$MESS["SONET_EXT_COMMENTAUX_CREATE_TASK_WIKI"] = "La tâche a été créée d'après une #A_BEGIN#page du wiki#A_END#";
