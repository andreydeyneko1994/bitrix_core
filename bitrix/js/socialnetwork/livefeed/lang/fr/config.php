<?php
$MESS["SONET_EXT_LIVEFEED_DELETE_SUCCESS"] = "Le poste a été supprimé.";
$MESS["SONET_EXT_LIVEFEED_FOLLOW_TITLE_N"] = "S'abonner";
$MESS["SONET_EXT_LIVEFEED_FOLLOW_TITLE_Y"] = "Se désabonner";
$MESS["SONET_EXT_LIVEFEED_MENU_TITLE_FAVORITES_N"] = "Ajouter aux favoris";
$MESS["SONET_EXT_LIVEFEED_MENU_TITLE_FAVORITES_Y"] = "Supprimer des favoris";
$MESS["SONET_EXT_LIVEFEED_MENU_TITLE_PINNED_N"] = "Épingler";
$MESS["SONET_EXT_LIVEFEED_MENU_TITLE_PINNED_Y"] = "Détacher";
$MESS["SONET_EXT_LIVEFEED_PINNED_CANCEL_BUTTON"] = "détacher";
$MESS["SONET_EXT_LIVEFEED_PINNED_CANCEL_DESCRIPTION"] = "Elle est maintenant toujours en haut des actualités.";
$MESS["SONET_EXT_LIVEFEED_PINNED_CANCEL_TITLE"] = "Ce message est épinglé.";
$MESS["SONET_EXT_LIVEFEED_PIN_TITLE_N"] = "Épingler";
$MESS["SONET_EXT_LIVEFEED_PIN_TITLE_Y"] = "Détacher";
$MESS["SONET_EXT_LIVEFEED_POST_BACKGROUND_EDIT_WARNING_BUTTON_CANCEL"] = "Annuler";
$MESS["SONET_EXT_LIVEFEED_POST_BACKGROUND_EDIT_WARNING_BUTTON_SUBMIT"] = "Continuer";
$MESS["SONET_EXT_LIVEFEED_POST_BACKGROUND_EDIT_WARNING_DESCRIPTION"] = "Le message a été créé sur l'application mobile. Sa mise en forme sera perdue si vous le modifiez ici.";
$MESS["SONET_EXT_LIVEFEED_POST_BACKGROUND_EDIT_WARNING_TITLE"] = "Modification de message";
