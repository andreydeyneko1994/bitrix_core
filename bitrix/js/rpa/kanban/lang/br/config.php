<?
$MESS["RPA_KANBAN_COLUMN_ADD_TASK_BTN"] = "Adicionar atribuição";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_BTN"] = "Excluir";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_CONFIRM"] = "Você tem certeza que deseja excluir a atribuição?";
$MESS["RPA_KANBAN_FIELDS_MODIFY_SETTINGS"] = "Personalizar criação rápida de campos do formulário";
$MESS["RPA_KANBAN_FIELDS_VIEW_SETTINGS"] = "Personalizar exibição de campos do formulário";
$MESS["RPA_KANBAN_MOVE_EMPTY_MANDATORY_FIELDS_ERROR"] = "O item possui campos obrigatórios vazios, mas você não tem permissão para editar o item.";
$MESS["RPA_KANBAN_MOVE_ITEM_HAS_TASKS_ERROR"] = "Você não pode mover o item porque ele contém atribuições incompletas.";
$MESS["RPA_KANBAN_MOVE_ITEM_PERMISSION_NOTIFY"] = "Você não pode alterar a etapa do item para #ITEM# quando sua etapa atual for #STAGE#";
$MESS["RPA_KANBAN_MOVE_PERMISSION_NOTIFY"] = "Você não tem permissão para alterar a etapa do item da etapa #STAGE#";
$MESS["RPA_KANBAN_MOVE_WRONG_STAGE_NOTIFY"] = "Não é possível mover o item da etapa #STAGE_FROM# para #STAGE_TO#";
$MESS["RPA_KANBAN_QUICK_FORM_CANCEL_BUTTON"] = "Cancelar";
$MESS["RPA_KANBAN_QUICK_FORM_SAVE_BUTTON"] = "Salvar";
$MESS["RPA_KANBAN_TASKS"] = "Atribuições";
?>