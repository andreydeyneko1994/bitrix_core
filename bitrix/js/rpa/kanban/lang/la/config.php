<?
$MESS["RPA_KANBAN_COLUMN_ADD_TASK_BTN"] = "Agregar asignación";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_BTN"] = "Eliminar";
$MESS["RPA_KANBAN_COLUMN_DELETE_TASK_CONFIRM"] = "¿Seguro que desea eliminar la asignación?";
$MESS["RPA_KANBAN_FIELDS_MODIFY_SETTINGS"] = "Personalizar la creación rápida de los campos del formulario";
$MESS["RPA_KANBAN_FIELDS_VIEW_SETTINGS"] = "Personalizar la visualización de los campos del formulario";
$MESS["RPA_KANBAN_MOVE_EMPTY_MANDATORY_FIELDS_ERROR"] = "El elemento tiene campos obligatorios vacíos pero no tiene permiso para editarlo.";
$MESS["RPA_KANBAN_MOVE_ITEM_HAS_TASKS_ERROR"] = "No puede mover el elemento porque contiene tareas incompletas.";
$MESS["RPA_KANBAN_MOVE_ITEM_PERMISSION_NOTIFY"] = "No puede cambiar la etapa del elemento para #ITEM# cuando su etapa actual es #STAGE#";
$MESS["RPA_KANBAN_MOVE_PERMISSION_NOTIFY"] = "No tiene permiso para cambiar la etapa del elemento de la etapa #STAGE#";
$MESS["RPA_KANBAN_MOVE_WRONG_STAGE_NOTIFY"] = "No se puede mover el elemento de la etapa #STAGE_FROM# a #STAGE_TO#";
$MESS["RPA_KANBAN_QUICK_FORM_CANCEL_BUTTON"] = "Cancelar";
$MESS["RPA_KANBAN_QUICK_FORM_SAVE_BUTTON"] = "Guardar";
$MESS["RPA_KANBAN_TASKS"] = "Asignaciones";
?>