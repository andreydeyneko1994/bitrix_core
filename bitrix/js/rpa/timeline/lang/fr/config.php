<?php
$MESS["RPA_TIMELINE_TASKS_FIELDS_TO_SET"] = "Remplir les champs";
$MESS["RPA_TIMELINE_TASKS_OPEN_TASK"] = "Démarrer";
$MESS["RPA_TIMELINE_TASKS_SEPARATOR_AND"] = "et";
$MESS["RPA_TIMELINE_TASKS_SEPARATOR_OR"] = "ou";
$MESS["RPA_TIMELINE_TASKS_TITLE"] = "Tâche";
