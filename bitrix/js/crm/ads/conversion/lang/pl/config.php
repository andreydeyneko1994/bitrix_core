<?php
$MESS["CRM_ADS_CONVERSION_DEAL_CONTENT_TITLE"] = "Wysyłaj działania klientów z dealów";
$MESS["CRM_ADS_CONVERSION_DEAL_SLIDER_ITEM_TITLE"] = "Pipeliny deala";
$MESS["CRM_ADS_CONVERSION_DEAL_SLIDER_TITLE"] = "Deale";
$MESS["CRM_ADS_CONVERSION_DETAIL"] = "Śledź działania użytkowników";
$MESS["CRM_ADS_CONVERSION_ERROR_ACCESS_DENY"] = "Odmowa dostępu. Skontaktuj się z administratorem.";
$MESS["CRM_ADS_CONVERSION_ERROR_CRM_NOT_INSTALLED"] = "Nie zainstalowano modułu CRM.";
$MESS["CRM_ADS_CONVERSION_ERROR_SAVE"] = "Wystąpił błąd. Spróbuj ponownie później.";
$MESS["CRM_ADS_CONVERSION_ERROR_SEO_NOT_INSTALLED"] = "Nie zainstalowano modułu SEO.";
$MESS["CRM_ADS_CONVERSION_ERROR_SOCIAL_NOT_INSTALLED"] = "Nie zainstalowano modułu \"Sieć społecznościowa\".";
$MESS["CRM_ADS_CONVERSION_ERROR_TITLE_SERVICE_ERROR"] = "Usługa niedostępna.";
$MESS["CRM_ADS_CONVERSION_ERROR_UNKNOWN_ERROR"] = "Nieznany błąd";
$MESS["CRM_ADS_CONVERSION_FORM_CONTENT_TITLE"] = "Wysyłaj działania klientów z formularzy";
$MESS["CRM_ADS_CONVERSION_FORM_SLIDER_ITEM_TITLE"] = "Formularze";
$MESS["CRM_ADS_CONVERSION_FORM_SLIDER_TITLE"] = "Formularze";
$MESS["CRM_ADS_CONVERSION_LEAD_CONTENT_TITLE"] = "Wysyłaj działania klientów z leadów";
$MESS["CRM_ADS_CONVERSION_LEAD_OPTION"] = "Włącz konwersję leadów!";
$MESS["CRM_ADS_CONVERSION_LEAD_SLIDER_TITLE"] = "Leady";
$MESS["CRM_ADS_CONVERSION_LOGIN"] = "Połącz";
$MESS["CRM_ADS_CONVERSION_LOGIN_CONNECT_TITLE"] = "Podłącz konto na Facebooku";
$MESS["CRM_ADS_CONVERSION_LOGIN_LINK"] = "Dowiedz się więcej";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TEXT_LIST_1"] = "Transmituj wydarzenia internetowe i offline z poziomu systemu CRM";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TEXT_LIST_2"] = "Wysyłaj działania klientów z systemu CRM na Facebooka";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TEXT_LIST_3"] = "Twórz odbiorców reklam za pomocą wszystkich obiektów CRM";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TEXT_LIST_4"] = "Wyświetlaj reklamy potencjalnym leadom i kontaktom, które nie są jeszcze Twoimi klientami";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TEXT_LIST_5"] = "Integracja wymaga konta firmowego.<br/> Konto firmowe zostanie utworzone automatycznie";
$MESS["CRM_ADS_CONVERSION_LOGIN_OPPORTUNITY_TITLE"] = "Użyj nowego narzędzia biznesowego opartego na API konwersji Facebooka i:";
$MESS["CRM_ADS_CONVERSION_LOGOUT"] = "Rozłącz";
$MESS["CRM_ADS_CONVERSION_PAYMENT_CONTENT_TITLE"] = "Wysyłaj działania klientów z płatności";
$MESS["CRM_ADS_CONVERSION_PAYMENT_OPTION"] = "Włącz konwersję płatności!";
$MESS["CRM_ADS_CONVERSION_PAYMENT_SLIDER_TITLE"] = "Płatności";
