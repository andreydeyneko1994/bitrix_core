<?
$MESS["CRM_ML_SCORING_BUTTON_TITLE"] = "AI24 Scoring";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_DEAL"] = "¡Ahora su CRM tiene más de 2000 negociaciones!<br> Ya contamos con datos suficientes para capacitar el modelo de scoring. Haga clic en \"AI Scoring\" para ver la probabilidad de las negociaciones actuales.";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_LEAD"] = "¡Ahora su CRM tiene más de 2000 prospectos!<br> Ya contamos con datos suficientes para capacitar el modelo de scoring. Haga clic en \"AI Scoring\" para ver la probabilidad de los prospectos actuales.";
?>