<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "No se puede copiar el enlace al Portapapeles. Intente de nuevo.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Error al crear el enlace público del documento";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "El archivo PDF aún se está creando. Intente de nuevo más tarde. Puede descargar el archivo DOCX respectivo en este momento.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "No se puede imprimir el documento porque aún se está creando el archivo PDF. Intente de nuevo más tarde.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Error al actualizar el documento";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "Cancelar";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "Cosas para hacer";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "Guardar";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Enlace copiado al Portapapeles";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Enviado";
