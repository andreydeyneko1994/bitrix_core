<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "Der Link kann in die Zwischenablage nicht kopiert werden. Versuchen Sie bitte erneut.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "Fehler beim Erstellen des öffentlichen Links zum Dokument";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "Die PDF-Datei wird immer noch erstellt. Versuchen Sie bitte später erneut. Sie können die entsprechende DOCX-Datei sofort herunterladen.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "Das Dokument kann nicht gedruckt werden, weil die PDF-Datei immer noch erstellt wird. Versuchen Sie bitte später erneut.";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "Fehler beim Aktualisieren des Dokuments";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "Link in die Zwischenablage kopiert";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "Gesendet";
