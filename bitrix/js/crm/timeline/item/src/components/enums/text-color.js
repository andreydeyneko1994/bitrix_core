export class TextColor {
	static GREEN = 'green';
	static BASE_50 = 'base-50';
	static BASE_70 = 'base-70';
	static BASE_90 = 'base-90';
}