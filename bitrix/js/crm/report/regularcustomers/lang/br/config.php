<?php
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_AMOUNT"] = "valor";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_CLIENTS_COUNT"] = "Total de clientes: #COUNT#";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_COMPANY_COUNT"] = "empresas";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_CONTACT_COUNT"] = "contatos";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_EARNINGS_PERCENT"] = "porcentagem das vendas brutas";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_OPEN_COMPANIES"] = "Ver empresas";
$MESS["CRM_REPORT_REGULAR_CUSTOMERS_OPEN_CONTACTS"] = "Visualizar Contatos";
