<?php
$MESS["CRM_REPORT_TRACKING_AD_REPORT_BUILD"] = "Creando informe";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_CLOSE"] = "Cerrar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_ERROR_TITLE"] = "Eso es un error.";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_SETTINGS_HINT"] = "Debe configurar los ajustes de los anuncios para obtener un informe correcto";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_ENABLED"] = "Habilitar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_PAUSE"] = "Pausar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_0"] = "Rendimiento de la campaña";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_1"] = "Rendimiento del grupo de anuncios";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_100"] = "Rendimiento de las palabras clave";
