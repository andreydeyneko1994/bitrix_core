<?php
$MESS["CRM_REPORT_TRACKING_AD_REPORT_BUILD"] = "Criando relatório";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_CLOSE"] = "Fechar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_ERROR_TITLE"] = "Isso é um erro.";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_SETTINGS_HINT"] = "Você deve definir as configurações de anúncio para obter um relatório correto";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_ENABLED"] = "Ativar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_PAUSE"] = "Pausar";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_0"] = "Desempenho da campanha";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_1"] = "Desempenho do grupo de anúncios";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_100"] = "Desempenho de palavra-chave";
