<?php
$MESS["CRM_ZOOM_CREATE_MEETING_SERVER_RETURNS_ERROR"] = "Não é possível criar reunião devido a um erro do servidor:";
$MESS["CRM_ZOOM_ERROR_EMPTY_TITLE"] = "O nome da reunião não pode estar vazio.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DATETIME"] = "O horário de início não pode ser inferior a agora.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DURATION"] = "Duração incorreta da reunião.";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DATE_CAPTION"] = "Data e horário de início";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_CAPTION"] = "Duração";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_HOURS"] = "horas";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_MINUTES"] = "minutos";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE"] = "Nome";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE_PLACEHOLDER"] = "Reunião do Zoom";
