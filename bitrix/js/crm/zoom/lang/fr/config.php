<?php
$MESS["CRM_ZOOM_CREATE_MEETING_SERVER_RETURNS_ERROR"] = "Impossible de créer une réunion en raison d'une erreur de serveur :";
$MESS["CRM_ZOOM_ERROR_EMPTY_TITLE"] = "Le nom de la réunion ne peut rester vide.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DATETIME"] = "L'heure de début ne peut être antérieure à maintenant.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DURATION"] = "Durée de réunion incorrecte.";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DATE_CAPTION"] = "Date et l'heure du début";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_CAPTION"] = "Dépensé";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_HOURS"] = "heures";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_MINUTES"] = "minutes";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE"] = "Nom";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE_PLACEHOLDER"] = "Réunion Zoom";
