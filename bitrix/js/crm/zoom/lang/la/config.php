<?php
$MESS["CRM_ZOOM_CREATE_MEETING_SERVER_RETURNS_ERROR"] = "No se puede crear la reunión debido a un error en el servidor:";
$MESS["CRM_ZOOM_ERROR_EMPTY_TITLE"] = "El nombre de la reunión no puede estar vacío.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DATETIME"] = "La hora de inicio no puede ser anterior a este momento.";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DURATION"] = "Duración de la reunión incorrecta.";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DATE_CAPTION"] = "Fecha y hora de inicio";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_CAPTION"] = "Duracion";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_HOURS"] = "horas";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_MINUTES"] = "minutos";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE"] = "Nombre";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE_PLACEHOLDER"] = "Reunión de Zoom";
