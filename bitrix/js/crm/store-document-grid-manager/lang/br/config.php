<?php
$MESS["DOCUMENT_GRID_CANCEL"] = "Cancelar";
$MESS["DOCUMENT_GRID_CONTINUE"] = "Continuar";
$MESS["DOCUMENT_GRID_DOCUMENT_CANCEL_CONTENT"] = "Você tem certeza que deseja cancelar o processamento desta entidade?";
$MESS["DOCUMENT_GRID_DOCUMENT_CANCEL_TITLE"] = "Cancelar processamento";
$MESS["DOCUMENT_GRID_DOCUMENT_CONDUCT_CONTENT"] = "Você tem certeza que deseja processar esta entidade?";
$MESS["DOCUMENT_GRID_DOCUMENT_CONDUCT_TITLE"] = "Processo";
$MESS["DOCUMENT_GRID_DOCUMENT_DELETE_CONTENT"] = "Você tem certeza que deseja excluir esta entidade?";
$MESS["DOCUMENT_GRID_DOCUMENT_DELETE_TITLE"] = "Excluir entidade";
