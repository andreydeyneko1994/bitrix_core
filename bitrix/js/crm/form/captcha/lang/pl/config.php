<?php
$MESS["CRM_FORM_CAPTCHA_JS_ACCESS_DENIED"] = "Odmowa dostępu. Skontaktuj się z administratorem Bitrix24.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_HOWTO"] = "Jak pozyskać klucze?";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TEXT"] = "Możesz zdecydować się na użycie własnych kluczy ochrony przed spamem. Będą one wówczas używane we wszystkich formularzach CRM.";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TITLE"] = "Użyj niestandardowych kluczy reCAPTCHA v2";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TEXT"] = "Konfiguracja zakończona. Możesz teraz korzystać z ochrony przed spamem.";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TITLE"] = "Automatyczna konfiguracja";
$MESS["CRM_FORM_CAPTCHA_JS_TITLE"] = "Ustawienia ochrony przed spamem";
