<?php
$MESS["CLIENT_REQUISITES_ADDRESS_SHOW_ADDRESS"] = "Afficher l'adresse";
$MESS["REQUISITE_LABEL_DETAILS_SELECT"] = "sélectionner dans la liste";
$MESS["REQUISITE_LABEL_DETAILS_TEXT"] = "détails";
$MESS["REQUISITE_LIST_EMPTY_PRESET_LIST"] = "Aucun modèle d'information trouvé";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_CONTENT"] = "Voulez-vous vraiment supprimer les informations et toutes les adresses associées ?";
$MESS["REQUISITE_LIST_ITEM_DELETE_CONFIRMATION_TITLE"] = "Supprimer les mentions";
$MESS["REQUISITE_LIST_ITEM_ERROR_CAPTION"] = "Erreur";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_CONTENT"] = "Voulez-vous vraiment effacer les champs des informations ?";
$MESS["REQUISITE_LIST_ITEM_HIDE_CONFIRMATION_TITLE"] = "Effacer les champs des informations";
$MESS["REQUISITE_TOOLTIP_ADD"] = "Ajouter des mentions";
$MESS["REQUISITE_TOOLTIP_ADD_BANK_DETAILS"] = "ajouter les informations bancaires";
$MESS["REQUISITE_TOOLTIP_BANK_DETAILS_TITLE"] = "Informations bancaires";
$MESS["REQUISITE_TOOLTIP_DELETE"] = "supprimer";
$MESS["REQUISITE_TOOLTIP_EDIT"] = "changer";
$MESS["REQUISITE_TOOLTIP_SHOW_DETAILS"] = "détails";
