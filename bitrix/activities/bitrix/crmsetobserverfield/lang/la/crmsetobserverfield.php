<?php
$MESS["CRM_SOF_ACTION_ADD_OBSERVERS"] = "agregar";
$MESS["CRM_SOF_ACTION_ON_OBSERVERS"] = "Aplicar una acción a los observadores";
$MESS["CRM_SOF_ACTION_REMOVE_OBSERVERS"] = "eliminar";
$MESS["CRM_SOF_ACTION_REPLACE_OBSERVERS"] = "cambiar";
$MESS["CRM_SOF_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_SOF_OBSERVERS"] = "Observadores";
