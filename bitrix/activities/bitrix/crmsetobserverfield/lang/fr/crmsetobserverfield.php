<?php
$MESS["CRM_SOF_ACTION_ADD_OBSERVERS"] = "ajouter";
$MESS["CRM_SOF_ACTION_ON_OBSERVERS"] = "Appliquer l'action aux observateurs";
$MESS["CRM_SOF_ACTION_REMOVE_OBSERVERS"] = "supprimer";
$MESS["CRM_SOF_ACTION_REPLACE_OBSERVERS"] = "changer";
$MESS["CRM_SOF_EMPTY_PROP"] = "Le paramètre requis est vide : #PROPERTY#";
$MESS["CRM_SOF_OBSERVERS"] = "Observateurs";
