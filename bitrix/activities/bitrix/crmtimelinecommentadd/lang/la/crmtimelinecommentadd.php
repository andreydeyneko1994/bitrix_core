<?php
$MESS["BPCTLCA_COMMENT_TEXT"] = "Comentario";
$MESS["BPCTLCA_COMMENT_USER"] = "Creado por";
$MESS["BPCTLCA_CREATION_ERROR"] = "No se puede agregar el comentario";
$MESS["BPCTLCA_EMPTY_COMMENT_TEXT"] = "Comentario \"falta la propiedad\".";
$MESS["BPCTLCA_NO_COMMENT"] = "El texto del comentario no fue especificado.";
