<?php
$MESS["BPCTLCA_COMMENT_TEXT"] = "Comentário";
$MESS["BPCTLCA_COMMENT_USER"] = "Criado por";
$MESS["BPCTLCA_CREATION_ERROR"] = "Não é possível adicionar comentário";
$MESS["BPCTLCA_EMPTY_COMMENT_TEXT"] = "Comentário \"está faltando a propriedade\".";
$MESS["BPCTLCA_NO_COMMENT"] = "O texto do comentário não foi especificado.";
