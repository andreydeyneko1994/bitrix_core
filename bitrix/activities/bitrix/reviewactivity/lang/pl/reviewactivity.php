<?php
$MESS["BPAA_ACT_APPROVERS_NONE"] = "brak";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "Pole '#COMMENT_LABEL#' jest wymagane.";
$MESS["BPAA_ACT_NO_ACTION"] = "Wybrano niewłaściwą akcję.";
$MESS["BPAR_ACT_BUTTON2"] = "Gotowe";
$MESS["BPAR_ACT_COMMENT"] = "Komentarze";
$MESS["BPAR_ACT_INFO"] = "Ukończono #PERCENT#% (#REVIEWED# z #TOTAL#)";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "Atrybut 'Użytkownicy' nie jest określony.";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "Brakuje atrybutu 'Nazwa'.";
$MESS["BPAR_ACT_REVIEWED"] = "Czytanie elementu zostało zakończone.";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "Użytkownik #PERSON# przeczytał element #COMMENT#";
$MESS["BPAR_ACT_TRACK2"] = "Element został przeczytany przez #VAL# użytkowników";
