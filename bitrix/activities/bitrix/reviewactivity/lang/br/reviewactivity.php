<?php
$MESS["BPAA_ACT_APPROVERS_NONE"] = "nenhum";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "O campo '#COMMENT_LABEL#' é obrigatório.";
$MESS["BPAA_ACT_NO_ACTION"] = "Ação invalidada selecionada.";
$MESS["BPAR_ACT_BUTTON2"] = "Feito";
$MESS["BPAR_ACT_COMMENT"] = "Comentários";
$MESS["BPAR_ACT_INFO"] = "Concluído #PERCENT#% (#REVIEWED# of #TOTAL#)";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "A propriedade 'Usuários' não está especificada.";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "A propriedade 'Nome' está faltando.";
$MESS["BPAR_ACT_REVIEWED"] = "A ação \"Ler o elemento\" está concluída";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "O usuário #PERSON# leu o elemento #COMMENT#";
$MESS["BPAR_ACT_TRACK2"] = "O elemento foi lido por #VAL# usuários";
