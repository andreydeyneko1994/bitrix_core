<?
$MESS["BPFC_PD_ADD"] = "Agregar condición";
$MESS["BPFC_PD_AND"] = "Y";
$MESS["BPFC_PD_CALENDAR"] = "Calendario";
$MESS["BPFC_PD_CONDITION"] = "Condición";
$MESS["BPFC_PD_CONTAIN"] = "contiene";
$MESS["BPFC_PD_DELETE"] = "Eliminar";
$MESS["BPFC_PD_EMPTY"] = "el campo está vacío";
$MESS["BPFC_PD_EQ"] = "igual a  ";
$MESS["BPFC_PD_FIELD"] = "Propiedad o campo";
$MESS["BPFC_PD_GE"] = "no menor que";
$MESS["BPFC_PD_GT"] = "más que";
$MESS["BPFC_PD_IN"] = "está en";
$MESS["BPFC_PD_LE"] = "no mayor que";
$MESS["BPFC_PD_LT"] = "menos que";
$MESS["BPFC_PD_NE"] = "no igual a";
$MESS["BPFC_PD_NO"] = "No";
$MESS["BPFC_PD_NOT_EMPTY"] = "el campo no está vacío";
$MESS["BPFC_PD_OR"] = "O";
$MESS["BPFC_PD_PARAMS"] = "Parámetros";
$MESS["BPFC_PD_VALUE"] = "Valor";
$MESS["BPFC_PD_VARS"] = "Variables";
$MESS["BPFC_PD_YES"] = "Si";
?>