<?
$MESS["CRM_SSS_ORDER_ERROR"] = "La entidad actual no es una entidad de tipo \"Pedido\"";
$MESS["CRM_SSS_ORDER_NOT_FOUND"] = "No se pueden obtener datos del pedido";
$MESS["CRM_SSS_TARGET_STATUS_EMPTY"] = "El estado final no se especifica";
$MESS["CRM_SSS_TARGET_STATUS_NAME"] = "Nuevo estado";
?>