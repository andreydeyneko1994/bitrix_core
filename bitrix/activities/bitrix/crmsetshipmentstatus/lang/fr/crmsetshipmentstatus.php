<?
$MESS["CRM_SSS_ORDER_ERROR"] = "L'entité actuelle n'est pas une entité de type \"Commande\"";
$MESS["CRM_SSS_ORDER_NOT_FOUND"] = "Récupération des données de la commande impossible";
$MESS["CRM_SSS_TARGET_STATUS_EMPTY"] = "Le statut final n'est pas spécifié";
$MESS["CRM_SSS_TARGET_STATUS_NAME"] = "Nouveau statut";
?>