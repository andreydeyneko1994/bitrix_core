<?php
$MESS["CRM_BP_GPR_DESC"] = "Отримати дані про товарну позицію";
$MESS["CRM_BP_GPR_NAME"] = "Інформація про товарну позицію";
$MESS["CRM_BP_GPR_RETURN_ROW_DISCOUNT_RATE"] = "Знижка";
$MESS["CRM_BP_GPR_RETURN_ROW_DISCOUNT_SUM"] = "Сума знижки";
$MESS["CRM_BP_GPR_RETURN_ROW_MEASURE_NAME"] = "Од. виміру";
$MESS["CRM_BP_GPR_RETURN_ROW_PRICE_ACCOUNT"] = "Ціна";
$MESS["CRM_BP_GPR_RETURN_ROW_PRODUCT_ID"] = "ID товару";
$MESS["CRM_BP_GPR_RETURN_ROW_PRODUCT_NAME"] = "Назва товару";
$MESS["CRM_BP_GPR_RETURN_ROW_QUANTITY"] = "Кількість";
$MESS["CRM_BP_GPR_RETURN_ROW_SUM_ACCOUNT"] = "Сума";
$MESS["CRM_BP_GPR_RETURN_ROW_SUM_ACCOUNT_MONEY"] = "Сума (текст)";
$MESS["CRM_BP_GPR_RETURN_ROW_TAX_INCLUDED"] = "Включений у ціну";
$MESS["CRM_BP_GPR_RETURN_ROW_TAX_RATE"] = "Податок";
