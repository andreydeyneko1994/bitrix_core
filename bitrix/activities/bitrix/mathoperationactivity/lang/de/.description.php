<?php
$MESS["BPMOA_DESCR_DESCR"] = "Mathematische Operationen";
$MESS["BPMOA_DESCR_DESCR_1"] = "Berechnet den angegebenen Ausdruck mithilfe der Werte von Variablen: Addieren, Subtrahieren, Multiplizieren, Dividieren.";
$MESS["BPMOA_DESCR_NAME"] = "Mathematische Operationen";
$MESS["BPMOA_DESCR_NAME_1"] = "Mathematische Operation ausführen";
$MESS["BPMOA_DESCR_ROBOT_TITLE"] = "Mathematische Operationen";
