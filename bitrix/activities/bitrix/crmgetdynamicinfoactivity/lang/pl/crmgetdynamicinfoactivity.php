<?php
$MESS["CRM_GDIA_DYNAMIC_TYPE_ID"] = "Typ pozycji SPA";
$MESS["CRM_GDIA_ENTITY_EXISTENCE_ERROR"] = "Wybrane filtry nie wygenerowały żadnych wyników wyszukiwania";
$MESS["CRM_GDIA_ENTITY_TYPE_ERROR"] = "Wybrano nieprawidłowy typ obiektu";
$MESS["CRM_GDIA_FILTERING_FIELDS_PROPERTY"] = "Filtr pól";
$MESS["CRM_GDIA_RETURN_FIELDS_SELECTION"] = "Wybierz pola";
