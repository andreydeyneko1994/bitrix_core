<?php
$MESS["CRM_GDIA_DYNAMIC_TYPE_ID"] = "Tipo de elemento SPA";
$MESS["CRM_GDIA_ENTITY_EXISTENCE_ERROR"] = "Los filtros que seleccionó no arrojaron resultados de búsqueda";
$MESS["CRM_GDIA_ENTITY_TYPE_ERROR"] = "Se seleccionó un tipo de entidad incorrecto";
$MESS["CRM_GDIA_FILTERING_FIELDS_PROPERTY"] = "Filtro del campo";
$MESS["CRM_GDIA_RETURN_FIELDS_SELECTION"] = "Seleccionar campos";
