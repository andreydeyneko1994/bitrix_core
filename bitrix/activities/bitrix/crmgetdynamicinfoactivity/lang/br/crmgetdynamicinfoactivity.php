<?php
$MESS["CRM_GDIA_DYNAMIC_TYPE_ID"] = "Tipo de item SPA";
$MESS["CRM_GDIA_ENTITY_EXISTENCE_ERROR"] = "Os filtros que você selecionou não produziram resultados de pesquisa";
$MESS["CRM_GDIA_ENTITY_TYPE_ERROR"] = "Tipo de entidade incorreto selecionado";
$MESS["CRM_GDIA_FILTERING_FIELDS_PROPERTY"] = "Filtro de campo";
$MESS["CRM_GDIA_RETURN_FIELDS_SELECTION"] = "Selecionar campos";
