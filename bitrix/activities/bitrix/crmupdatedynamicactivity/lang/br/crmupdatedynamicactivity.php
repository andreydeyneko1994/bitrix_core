<?php
$MESS["CRM_UDA_ADD_CONDITION"] = "Adicionar condição";
$MESS["CRM_UDA_DELETE_CONDITION"] = "Excluir";
$MESS["CRM_UDA_DYNAMIC_TYPE"] = "Tipo de item SPA";
$MESS["CRM_UDA_ENTITY_EXISTENCE_ERROR"] = "#TYPE_NAME# ID da entidade #ENTITY_ID# não existe";
$MESS["CRM_UDA_ENTITY_ID_ERROR"] = "O ID da entidade deve ser maior do que zero";
$MESS["CRM_UDA_ENTITY_TYPE_ERROR"] = "Tipo de entidade incorreto selecionado";
