<?
$MESS["BPWA_CONDITION_NOT_SET"] = "Condición de bucle no especificada";
$MESS["BPWA_CYCLE_LIMIT"] = "Máxima iteración loop superada";
$MESS["BPWA_INVALID_CONDITION_TYPE"] = "El tipo de condición no fue encontrada.";
$MESS["BPWA_NO_CONDITION"] = "La condición esta desaparecida.";
?>