<?
$MESS["BPDUA_ACCESS_DENIED"] = "Odmowa dostępu dla wszystkich poza administratorami portalu.";
$MESS["BPDUA_EMPTY_ENTITY_ID"] = "Pamięć nie jest określona.";
$MESS["BPDUA_EMPTY_ENTITY_TYPE"] = "Typ pamięci nie jest określony.";
$MESS["BPDUA_EMPTY_SOURCE_FILE"] = "Nie wybrano żadnego z załadowanych plików.";
$MESS["BPDUA_SOURCE_ERROR"] = "Błąd pobierania pliku do przesłania.";
$MESS["BPDUA_TARGET_ERROR"] = "Nie udało się ustalić lokalizacji przesyłania.";
$MESS["BPDUA_UPLOAD_ERROR"] = "Błąd ładowania pliku.";
?>