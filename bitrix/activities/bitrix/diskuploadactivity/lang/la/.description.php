<?php
$MESS["BPDUA_DESCR_CATEGORY"] = "Drive";
$MESS["BPDUA_DESCR_DESCR2"] = "Cargar archivo al drive de almacenamiento";
$MESS["BPDUA_DESCR_DETAIL_URL"] = "Ver URL";
$MESS["BPDUA_DESCR_DOWNLOAD_URL"] = "URL de descarga";
$MESS["BPDUA_DESCR_NAME2"] = "Subir al Drive";
$MESS["BPDUA_DESCR_OBJECT_ID"] = "ID's de los archivos del drive";
