<?php
$MESS["CRM_CDA_DYNAMIC_TYPE_ID"] = "Type SPA";
$MESS["CRM_CDA_DYNAMIC_TYPE_ID_ERROR"] = "Le type d'entité sélectionné est incorrect";
$MESS["CRM_CDA_ITEM_CREATION_ERROR"] = "Erreur lors de la création de l'élément CRM";
$MESS["CRM_CDA_TYPE_ID"] = "Type d'élément CRM";
