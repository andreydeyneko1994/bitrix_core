<?
$MESS["BPDCM_ACCESS_DENIED"] = "Odmowa dostępu dla wszystkich poza administratorami portalu.";
$MESS["BPDCM_ADD_FOLDER_ERROR"] = "Nie można utworzyć nowego folderu.";
$MESS["BPDCM_EMPTY_ENTITY_ID"] = "Pamięć nie jest określona.";
$MESS["BPDCM_EMPTY_ENTITY_TYPE"] = "Brakuje typu pamięci.";
$MESS["BPDCM_EMPTY_SOURCE_ID"] = "Nie wskazano obiektu do skopiowania lub przeniesienia.";
$MESS["BPDCM_OPERATION_ERROR"] = "Błąd podczas wykonywania operacji.";
$MESS["BPDCM_SOURCE_ERROR"] = "Nie udało się ustalić obiektu źródłowego.";
$MESS["BPDCM_TARGET_ERROR"] = "Nie udało się ustalić lokalizacji nowego folderu.";
?>