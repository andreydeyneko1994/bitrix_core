<?
$MESS["BPCRU_PD_MAX_LEVEL"] = "Poziom Przełożonego (im więcej tym wyżej)";
$MESS["BPCRU_PD_MAX_LEVEL_1"] = "1 (bezpośredni przełożony)";
$MESS["BPCRU_PD_NO"] = "Nie";
$MESS["BPCRU_PD_SKIP_ABSENT"] = "Podnieś na wyższy poziom jeśli nieobecny";
$MESS["BPCRU_PD_SKIP_RESERVE"] = "Zastosuj do rezerwowych użytkowników";
$MESS["BPCRU_PD_SKIP_TIMEMAN"] = "Pomiń sprawdzonych pracowników";
$MESS["BPCRU_PD_TYPE"] = "Rodzaj";
$MESS["BPCRU_PD_TYPE_BOSS"] = "Przełożony";
$MESS["BPCRU_PD_TYPE_ORDER"] = "sekwencyjnie";
$MESS["BPCRU_PD_TYPE_RANDOM"] = "nie wybrany";
$MESS["BPCRU_PD_USER2"] = "Użytkownicy kopii zapasowych";
$MESS["BPCRU_PD_USER_BOSS"] = "Dla użytkownika";
$MESS["BPCRU_PD_USER_RANDOM"] = "Od użytkowników";
$MESS["BPCRU_PD_YES"] = "Tak";
?>