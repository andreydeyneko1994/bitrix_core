<?
$MESS["RPA_BP_RA_FIELD_APPROVE_BTN_TEXT"] = "Salvar";
$MESS["RPA_BP_RA_FIELD_ERROR_FIELDS"] = "Todos os campos são obrigatórios.";
$MESS["RPA_BP_RA_FIELD_FIELDS_TO_SET"] = "Campos a preencher";
$MESS["RPA_BP_RA_VALIDATION_ERROR_FIELDS_TO_SET"] = "Pelo menos um campo é obrigatório.";
?>