<?
$MESS["RPA_BP_RA_FIELD_APPROVE_BTN_TEXT"] = "Enregistrer";
$MESS["RPA_BP_RA_FIELD_ERROR_FIELDS"] = "Tous les champs sont obligatoires.";
$MESS["RPA_BP_RA_FIELD_FIELDS_TO_SET"] = "Champs à remplir";
$MESS["RPA_BP_RA_VALIDATION_ERROR_FIELDS_TO_SET"] = "Au moins un champ est obligatoire.";
?>