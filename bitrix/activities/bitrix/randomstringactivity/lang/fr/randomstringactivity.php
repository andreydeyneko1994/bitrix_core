<?
$MESS["BPRNDSA_ALPHABET_ALPHALOWER"] = "Caractères en minuscules";
$MESS["BPRNDSA_ALPHABET_ALPHAUPPER"] = "Caractères en majuscules";
$MESS["BPRNDSA_ALPHABET_NAME"] = "Alphabet";
$MESS["BPRNDSA_ALPHABET_NUM"] = "Chiffres";
$MESS["BPRNDSA_ALPHABET_SPECIAL"] = "Caractères spéciaux";
$MESS["BPRNDSA_EMPTY_ALPHABET"] = "L'alphabet n'est pas spécifié";
$MESS["BPRNDSA_EMPTY_SIZE"] = "La taille de la chaîne n'est pas spécifiée";
$MESS["BPRNDSA_SIZE_NAME"] = "Taille de la chaîne";
?>