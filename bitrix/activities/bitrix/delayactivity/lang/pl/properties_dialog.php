<?php
$MESS["CPAD_DP_TIME"] = "Czas Wstrzymania";
$MESS["CPAD_DP_TIME1"] = "Data";
$MESS["CPAD_DP_TIME_D"] = "dni";
$MESS["CPAD_DP_TIME_H"] = "godziny";
$MESS["CPAD_DP_TIME_LOCAL"] = "Czas lokalny";
$MESS["CPAD_DP_TIME_M"] = "minut(y)";
$MESS["CPAD_DP_TIME_S"] = "sekundy";
$MESS["CPAD_DP_TIME_SELECT"] = "Tryb";
$MESS["CPAD_DP_TIME_SELECT_DELAY"] = "Okres";
$MESS["CPAD_DP_TIME_SELECT_TIME"] = "Czas";
$MESS["CPAD_DP_TIME_SERVER"] = "Czas serwera";
$MESS["CPAD_DP_WRITE_TO_LOG"] = "Zapisz informacje o wstrzymaniu w dzienniku workflowów";
$MESS["CPAD_PD_TIMEOUT_LIMIT"] = "Minimalny czas oczekiwania";
