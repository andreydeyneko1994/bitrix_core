<?
$MESS["BPDA_DESCR_DESCR"] = "L'opération 'Pause de l'exécution' permet de reporter l'exécution de l'opération suivante au temps indiqué.";
$MESS["BPDA_DESCR_NAME"] = "Pause dans l'exécution";
?>