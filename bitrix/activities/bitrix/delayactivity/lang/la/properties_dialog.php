<?php
$MESS["CPAD_DP_TIME"] = "Tiempo pausado";
$MESS["CPAD_DP_TIME1"] = "Fecha";
$MESS["CPAD_DP_TIME_D"] = "días";
$MESS["CPAD_DP_TIME_H"] = "horas";
$MESS["CPAD_DP_TIME_LOCAL"] = "Hora local";
$MESS["CPAD_DP_TIME_M"] = "minutos";
$MESS["CPAD_DP_TIME_S"] = "segundos";
$MESS["CPAD_DP_TIME_SELECT"] = "Modo";
$MESS["CPAD_DP_TIME_SELECT_DELAY"] = "Periódo";
$MESS["CPAD_DP_TIME_SELECT_TIME"] = "Tiempo";
$MESS["CPAD_DP_TIME_SERVER"] = "Tiempo de servidor";
$MESS["CPAD_DP_WRITE_TO_LOG"] = "Guardar la información de pausa en el registro del flujo de trabajo";
$MESS["CPAD_PD_TIMEOUT_LIMIT"] = "Tiempo mínimo de aprobación";
