<?php
$MESS["CPAD_DP_TIME"] = "Pausar Hora";
$MESS["CPAD_DP_TIME1"] = "Data";
$MESS["CPAD_DP_TIME_D"] = "dias";
$MESS["CPAD_DP_TIME_H"] = "horas";
$MESS["CPAD_DP_TIME_LOCAL"] = "Horário local";
$MESS["CPAD_DP_TIME_M"] = "minutos";
$MESS["CPAD_DP_TIME_S"] = "segundos";
$MESS["CPAD_DP_TIME_SELECT"] = "Modo";
$MESS["CPAD_DP_TIME_SELECT_DELAY"] = "Período";
$MESS["CPAD_DP_TIME_SELECT_TIME"] = "Hora";
$MESS["CPAD_DP_TIME_SERVER"] = "Horário do servidor";
$MESS["CPAD_DP_WRITE_TO_LOG"] = "Salvar informações de pausa no log do fluxo de trabalho";
$MESS["CPAD_PD_TIMEOUT_LIMIT"] = "Tempo mínimo de espera";
