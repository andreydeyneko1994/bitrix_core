<?
$MESS["BPDA_EMPTY_PROP"] = "O valor \"Período\" não está especificado.";
$MESS["BPDA_TRACK"] = "Pausado por #PERIOD#";
$MESS["BPDA_TRACK1"] = "Aprovado até #PERIOD#";
$MESS["BPDA_TRACK2"] = "O período de aprovação não está especificado.";
$MESS["BPDA_TRACK3"] = "Pausa ignorada. A data de pausa configurada era anterior à data atual.";
$MESS["BPDA_TRACK4"] = "Adiado para #PERIOD1#, até #PERIOD2#";
?>