<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Fase inicial";
$MESS["CRM_CDA_COPY_PRODUCTS_ERROR"] = "Não é possível copiar itens de produto do negócio de fonte";
$MESS["CRM_CDA_CYCLING_ERROR"] = "O negócio não pode ser copiado devido a possível loop infinito";
$MESS["CRM_CDA_CYCLING_EXCEPTION_MESSAGE"] = "Regra de automação não iniciada";
$MESS["CRM_CDA_DEAL_TITLE"] = "Nome do negócio";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Transferir para pipeline";
$MESS["CRM_CDA_NEW_DEAL_TITLE"] = "#SOURCE_TITLE# (Cópia)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Não é possível obter dados do negócio de fonte";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "A etapa inicial selecionada pertence a algum outro pipeline";
