<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Etapa inicial";
$MESS["CRM_CDA_COPY_PRODUCTS_ERROR"] = "No se pueden copiar los elementos del productos desde el origen de la negocación";
$MESS["CRM_CDA_CYCLING_ERROR"] = "La negociación no pudo copiarse debido a un posible bucle infinito";
$MESS["CRM_CDA_CYCLING_EXCEPTION_MESSAGE"] = "Regla de automatización no ejecutada";
$MESS["CRM_CDA_DEAL_TITLE"] = "Nombre de la negociación";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Mover al pipeline";
$MESS["CRM_CDA_NEW_DEAL_TITLE"] = "#SOURCE_TITLE# (Copy)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "No se pueden obtener datos de origen de la negociación";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "La etapa inicial seleccionada pertenece a algún otro canal";
