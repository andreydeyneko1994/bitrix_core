<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Personne responsable";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Étape initiale";
$MESS["CRM_CDA_COPY_PRODUCTS_ERROR"] = "Impossible de copier les éléments de la transaction source";
$MESS["CRM_CDA_CYCLING_ERROR"] = "La transaction n'a pu être copiée en raison d'une possible boucle infinie";
$MESS["CRM_CDA_CYCLING_EXCEPTION_MESSAGE"] = "La règle d'automatisation n'est pas lancée";
$MESS["CRM_CDA_DEAL_TITLE"] = "Nom de la transaction";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Déplacer dans le pipeline";
$MESS["CRM_CDA_NEW_DEAL_TITLE"] = "#SOURCE_TITLE# (Copie)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Impossible de récupérer les données de la transaction source";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "L'étape initiale sélectionnée appartient à un autre pipeline";
