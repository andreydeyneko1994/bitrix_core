<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Osoba odpowiedzialna";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Etap początkowy";
$MESS["CRM_CDA_COPY_PRODUCTS_ERROR"] = "Nie można skopiować produktów produktu z dealu źródłowego";
$MESS["CRM_CDA_CYCLING_ERROR"] = "Dealu nie można skopiować z powodu możliwej nieskończonej pętli";
$MESS["CRM_CDA_CYCLING_EXCEPTION_MESSAGE"] = "Nie uruchomiono reguły automatyzacji";
$MESS["CRM_CDA_DEAL_TITLE"] = "Nazwa deala";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Przenieś do lejka";
$MESS["CRM_CDA_NEW_DEAL_TITLE"] = "#SOURCE_TITLE# (Kopiuj)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Nie można uzyskać danych o dealu źródłowym";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "Wybrany etap początkowy należy do innego lejka";
