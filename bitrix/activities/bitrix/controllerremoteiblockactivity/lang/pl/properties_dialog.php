<?
$MESS["BPCRIA_NO_MODULE"] = "Moduł Strownika Strony nie jest zainstalowany";
$MESS["BPCRIA_SITES_FILTER_ALL"] = "Wszystkie strony";
$MESS["BPCRIA_SITES_FILTER_GROUPS"] = "Strony z określonych grup";
$MESS["BPCRIA_SITES_FILTER_SITES"] = "Wybór stron z jednej grupy";
$MESS["BPCRIA_SITES_FILTER_TYPE"] = "Wybór typu strony";
$MESS["BPCRIA_SITES_GROUPS"] = "Grupy kontrolowanych stron";
$MESS["BPCRIA_SITES_SITES"] = "Strony";
$MESS["BPCRIA_SYNC_IMMEDIATE"] = "natychmiast";
$MESS["BPCRIA_SYNC_TASKS"] = "używając zadań zdalnego klienta";
$MESS["BPCRIA_SYNC_TIME"] = "Synchronizuj elementy";
?>