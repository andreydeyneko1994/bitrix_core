<?
$MESS["TASKS_CHANGE_STAGE_EMPTY_PROP"] = "Etapa final não especificada";
$MESS["TASKS_CHANGE_STAGE_RECURSION"] = "Erro de execução: possível recursão infinita na mudança de status";
$MESS["TASKS_CHANGE_STAGE_STAGE"] = "Nova etapa";
$MESS["TASKS_CHANGE_STAGE_STAGE_ERROR"] = "Etapa de destino incorreta";
$MESS["TASKS_CHANGE_STAGE_TERMINATED"] = "Concluído porque a etapa foi alterada";
?>