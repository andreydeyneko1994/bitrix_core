<?php
$MESS["CRM_CHANGE_NEW_RESPONSIBLE_ID"] = "Pessoa responsável alterada para";
$MESS["CRM_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "A pessoa responsável não está especificada.";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE"] = "Selecionar nova pessoa responsável";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_F"] = "primeiro disponível";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_R"] = "em ordem aleatória";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_S"] = "na ordem especificada";
$MESS["CRM_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Mudança em nome de";
$MESS["CRM_CHANGE_RESPONSIBLE_NEW"] = "Nova pessoa responsável";
$MESS["CRM_CHANGE_RESPONSIBLE_SKIP_ABSENT"] = "Pular pessoas ausentes";
