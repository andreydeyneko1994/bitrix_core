<?php
$MESS["CRM_CHANGE_NEW_RESPONSIBLE_ID"] = "Persona responsable cambiada a";
$MESS["CRM_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "La persona responsable no se especifica.";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE"] = "Seleccionar a una nueva persona responsable";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_F"] = "la primera disponible";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_R"] = "en orden aleatorio";
$MESS["CRM_CHANGE_RESPONSIBLE_GETTER_TYPE_S"] = "en un orden específico";
$MESS["CRM_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Cambiar en nombre de";
$MESS["CRM_CHANGE_RESPONSIBLE_NEW"] = "Nueva persona responsable";
$MESS["CRM_CHANGE_RESPONSIBLE_SKIP_ABSENT"] = "Omitir a las personas ausentes";
