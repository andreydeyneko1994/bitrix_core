<?php
$MESS["CRM_CTA_COMPLETED_BY"] = "Terminer la tâche en tant qu'utilisateur";
$MESS["CRM_CTA_COMPLETED_TASKS"] = "Tâches accomplies";
$MESS["CRM_CTA_COMPLETE_TASK"] = "Terminer les tâches créées pendant l'étape";
$MESS["CRM_CTA_INCORRECT_STAGE"] = "Statut ou étape de réalisation incorrects";
