<?php
$MESS["CRM_CVTDA_COMPANY"] = "Entreprise";
$MESS["CRM_CVTDA_CONTACT"] = "Contact";
$MESS["CRM_CVTDA_DEAL"] = "Transaction";
$MESS["CRM_CVTDA_DEAL_CATEGORY_ID"] = "Pipeline de la transaction";
$MESS["CRM_CVTDA_DEFAULT_CONTACT_NAME"] = "Sans nom";
$MESS["CRM_CVTDA_DISABLE_ACTIVITY_COMPLETION"] = "Ne pas fermer l'activité après conversion";
$MESS["CRM_CVTDA_EMPTY_PROP"] = "Les entités à créer ne sont pas spécifiées";
$MESS["CRM_CVTDA_INCORRECT_DOCUMENT"] = "Impossible d'effectuer la conversion des éléments de ce type";
$MESS["CRM_CVTDA_INVOICE"] = "Facture";
$MESS["CRM_CVTDA_ITEMS"] = "Créer en utilisant la source";
$MESS["CRM_CVTDA_QUOTE"] = "Devis";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_DEAL"] = "Les entités suivantes doivent être créées avec la transaction : #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_LEAD"] = "Les entités suivantes doivent être créées avec le client potentiel : #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_DEAL"] = "La conversion de la transaction doit être effectuée";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_LEAD"] = "La conversion du client potentiel doit être effectuée";
$MESS["CRM_CVTDA_RESPONSIBLE"] = "Personne responsable";
$MESS["CRM_CVTDA_WIZARD_NOT_FOUND"] = "Impossible d'initialiser l'assistant de conversion";
