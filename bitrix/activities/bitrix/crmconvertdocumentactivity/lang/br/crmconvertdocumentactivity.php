<?php
$MESS["CRM_CVTDA_COMPANY"] = "Empresa";
$MESS["CRM_CVTDA_CONTACT"] = "Contato";
$MESS["CRM_CVTDA_DEAL"] = "Negócio";
$MESS["CRM_CVTDA_DEAL_CATEGORY_ID"] = "Pipeline de negócio";
$MESS["CRM_CVTDA_DEFAULT_CONTACT_NAME"] = "Sem nome";
$MESS["CRM_CVTDA_DISABLE_ACTIVITY_COMPLETION"] = "Não fechar a atividade após conversão";
$MESS["CRM_CVTDA_EMPTY_PROP"] = "As entidades a criar não estão especificadas";
$MESS["CRM_CVTDA_INCORRECT_DOCUMENT"] = "Não é possível converter elementos desse tipo";
$MESS["CRM_CVTDA_INVOICE"] = "Fatura";
$MESS["CRM_CVTDA_ITEMS"] = "Criar usando fonte";
$MESS["CRM_CVTDA_QUOTE"] = "Orçamento";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_DEAL"] = "As seguintes entidades precisam ser criadas usando o Negócio: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_LEAD"] = "As seguintes entidades precisam ser criadas usando o Lead: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_DEAL"] = "O negócio precisa ser convertido";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_LEAD"] = "O Lead precisa ser convertido";
$MESS["CRM_CVTDA_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_CVTDA_WIZARD_NOT_FOUND"] = "Não é possível inicializar o Assistente de Conversão.";
