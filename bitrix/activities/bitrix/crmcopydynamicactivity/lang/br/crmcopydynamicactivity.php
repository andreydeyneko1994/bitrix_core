<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_CDA_CHANGE_STAGE"] = "Etapa inicial";
$MESS["CRM_CDA_CYCLING_ERROR"] = "Não é possível copiar a Automação Inteligente de Processos porque há um possível loop infinito nas ações";
$MESS["CRM_CDA_EMPTY_PROP"] = "O parâmetro obrigatório está vazio: #PROPERTY#";
$MESS["CRM_CDA_ITEM_TITLE"] = "Nome da SPA";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "Transferir para pipeline";
$MESS["CRM_CDA_NEW_ITEM_TITLE"] = "#SOURCE_TITLE# (Cópia)";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "Não é possível obter dados da SPA de origem";
$MESS["CRM_CDA_STAGE_EXISTENCE_ERROR"] = "A etapa selecionada não existe";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "A etapa inicial selecionada pertence a algum outro pipeline";
