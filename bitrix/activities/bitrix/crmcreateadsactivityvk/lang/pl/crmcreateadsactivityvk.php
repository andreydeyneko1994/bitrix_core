<?php
$MESS["CRM_CREATE_ADS_ACCOUNT_ID"] = "Wybierz konto reklamowe";
$MESS["CRM_CREATE_ADS_AUDIENCE_ID"] = "Dodaj do odbiorców";
$MESS["CRM_CREATE_ADS_CLIENT_ID"] = "Skonfiguruj grupę docelową";
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "Brakuje wymaganego pola";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Ta wartość musi być liczbą";
