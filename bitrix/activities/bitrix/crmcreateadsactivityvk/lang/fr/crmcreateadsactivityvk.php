<?php
$MESS["CRM_CREATE_ADS_ACCOUNT_ID"] = "Sélectionner le compte publicitaire";
$MESS["CRM_CREATE_ADS_AUDIENCE_ID"] = "Ajouter à l'audience";
$MESS["CRM_CREATE_ADS_CLIENT_ID"] = "Configurer l'audience cible";
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "Le champ requis est manquant";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Cette valeur doit être un chiffre";
