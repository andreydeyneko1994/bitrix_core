<?php
$MESS["CRM_CREATE_ADS_ACCOUNT_ID"] = "Selecionar conta de publicidade";
$MESS["CRM_CREATE_ADS_AUDIENCE_ID"] = "Adicionar ao público";
$MESS["CRM_CREATE_ADS_CLIENT_ID"] = "Configurar Público-alvo";
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "O campo obrigatório está faltando";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Este valor deve ser um número";
