<?php
$MESS["BPVICA_DESCR_DESCR"] = "Automatischer Anruf mit einer Sprachnachricht";
$MESS["BPVICA_DESCR_DESCR_1"] = "Der Voice-Bot ruft Kunden zu einem bestimmten Zeitpunkt oder entsprechend bestimmten Regeln an.";
$MESS["BPVICA_DESCR_NAME"] = "Automatischer Anruf";
$MESS["BPVICA_DESCR_NAME_1"] = "Einen Anruf machen";
$MESS["BPVICA_DESCR_RESULT"] = "Anrufergebnis";
$MESS["BPVICA_DESCR_RESULT_CODE"] = "Code des Anrufendes";
$MESS["BPVICA_DESCR_RESULT_TEXT"] = "Anrufergebnis (Text)";
