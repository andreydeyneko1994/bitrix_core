<?php
$MESS["BPVICA_DESCR_DESCR"] = "Wstępnie nagrane lub oparte na tekście informacyjne automatyczne połączenie telefoniczne";
$MESS["BPVICA_DESCR_NAME"] = "Automatyczne połączenie informacyjne (robocall)";
$MESS["BPVICA_DESCR_RESULT"] = "Wynik połączenia";
$MESS["BPVICA_DESCR_RESULT_CODE"] = "Kod zakończenia połączenia";
$MESS["BPVICA_DESCR_RESULT_TEXT"] = "Wynik połączenia (tekst)";
