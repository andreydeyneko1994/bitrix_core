<?
$MESS["BPVICA_PD_AUDIO_FILE"] = "Adres URL pliku audio (mp3)";
$MESS["BPVICA_PD_CALL_TYPE"] = "Połączenie z wykorzystaniem";
$MESS["BPVICA_PD_CALL_TYPE_AUDIO"] = "Pliku audio";
$MESS["BPVICA_PD_CALL_TYPE_TEXT"] = "Tekstu";
$MESS["BPVICA_PD_NO"] = "Nie";
$MESS["BPVICA_PD_NO_OUTPUT_NUMBER"] = "Aby skorzystać z tej opcji, prosimy o wynajem numeru telefonu lub podłączenie telefonii SIP.";
$MESS["BPVICA_PD_NUMBER"] = "Numeru docelowego";
$MESS["BPVICA_PD_OUTPUT_NUMBER"] = "Numeru wyjściowego";
$MESS["BPVICA_PD_TEXT"] = "Konwersja tekstu na mowę";
$MESS["BPVICA_PD_USE_DOCUMENT_PHONE_NUMBER"] = "Użyj bieżącego numer telefonu jednostki CRM";
$MESS["BPVICA_PD_VOICE_LANGUAGE"] = "Język i głos";
$MESS["BPVICA_PD_VOICE_SPEED"] = "Szybkość mowy";
$MESS["BPVICA_PD_VOICE_VOLUME"] = "Głośność mowy";
$MESS["BPVICA_PD_WAIT_FOR_RESULT"] = "Czekaj na wynik";
$MESS["BPVICA_PD_YES"] = "Tak";
?>