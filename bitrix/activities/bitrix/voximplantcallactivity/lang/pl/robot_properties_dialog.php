<?php
$MESS["BPVICA_RPD_AUDIO_FILE"] = "Adres URL pliku audio (mp3)";
$MESS["BPVICA_RPD_CALL_TYPE"] = "Połączenie z wykorzystaniem";
$MESS["BPVICA_RPD_CALL_TYPE_AUDIO"] = "Pliku audio";
$MESS["BPVICA_RPD_CALL_TYPE_TEXT"] = "Tekst";
$MESS["BPVICA_RPD_NO_OUTPUT_NUMBER"] = "Aby skorzystać z tej opcji, prosimy o wynajem numeru telefonu lub podłączenie telefonii SIP.";
$MESS["BPVICA_RPD_NUMBER"] = "Numeru docelowego";
$MESS["BPVICA_RPD_OUTPUT_NUMBER"] = "Numeru wyjściowego";
$MESS["BPVICA_RPD_VOICE_LANGUAGE"] = "Język i głos";
$MESS["BPVICA_RPD_VOICE_SPEED"] = "Szybkość mowy";
$MESS["BPVICA_RPD_VOICE_VOLUME"] = "Głośność mowy";
