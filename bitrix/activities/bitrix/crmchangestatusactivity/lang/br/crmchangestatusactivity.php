<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "O status do alvo não está especificado";
$MESS["CRM_CHANGE_STATUS_INCORRECT_STAGE"] = "Etapa ou status de destino incorreto";
$MESS["CRM_CHANGE_STATUS_MODIFIED_BY"] = "Mudança em nome de";
$MESS["CRM_CHANGE_STATUS_RECURSION"] = "Erro de execução: possível recursão infinita na mudança de status";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Novo estágio";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Novo status";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Concluído porque o status foi alterado";
?>