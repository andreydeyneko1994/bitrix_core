<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "Status docelowy nie został określony";
$MESS["CRM_CHANGE_STATUS_INCORRECT_STAGE"] = "Nieprawidłowy etap docelowy lub status";
$MESS["CRM_CHANGE_STATUS_MODIFIED_BY"] = "Zmień w imieniu";
$MESS["CRM_CHANGE_STATUS_RECURSION"] = "Błąd wykonania: możliwa nieskończona rekursja zmiany statusu";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Nowy etap";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Nowy status";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Zakończone z powodu zmiany statusu";
?>