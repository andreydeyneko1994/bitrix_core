<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "Statut de la cible non spécifié";
$MESS["CRM_CHANGE_STATUS_INCORRECT_STAGE"] = "Stade ou état de la cible incorrects";
$MESS["CRM_CHANGE_STATUS_MODIFIED_BY"] = "Modifier de la part de";
$MESS["CRM_CHANGE_STATUS_RECURSION"] = "Erreur lors de l'exécution : récursion infinie possible dans le changement de statut";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Nouvelle étape";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Nouveau statut";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Terminé parce que le statut a été modifié";
?>