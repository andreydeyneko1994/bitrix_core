<?
$MESS['CRM_CHANGE_STATUS_EMPTY_PROP'] = "Не вказано кінцевий статус";
$MESS['CRM_CHANGE_STATUS_INCORRECT_STAGE'] = "Некоректна цільова стадія/статус";
$MESS['CRM_CHANGE_STATUS_MODIFIED_BY'] = "Змінити від імені";
$MESS['CRM_CHANGE_STATUS_RECURSION'] = "Помилка виконання: підозра на рекурсивну зміну статусів";
$MESS['CRM_CHANGE_STATUS_STAGE'] = "Змінити на стадію";
$MESS['CRM_CHANGE_STATUS_STATUS'] = "Змінити на статус";
$MESS['CRM_CHANGE_STATUS_TERMINATED'] = "Завершено по зміні статусу";
?>