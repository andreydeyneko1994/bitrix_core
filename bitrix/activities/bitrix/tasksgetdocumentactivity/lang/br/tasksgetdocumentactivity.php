<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Apenas os administradores do Bitrix24 podem acessar propriedades de ação.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "ID da tarefa";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "A ID da tarefa é obrigatório";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "Não é possível obter informações do documento";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "Nenhum campo de documento selecionado";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Selecionar campos";
