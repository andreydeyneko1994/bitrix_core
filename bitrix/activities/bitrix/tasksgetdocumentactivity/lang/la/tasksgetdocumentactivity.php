<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Solo los administradores de Bitrix24 pueden acceder a las propiedades de la acción.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "Tarea ID";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "Se requiere el ID de la tarea";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "No se puede obtener la información del documento";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "No se seleccionaron campos del documento";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Seleccionar campos";
