<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Seuls les administrateurs Bitrix24 peuvent accéder aux propriétés des actions.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "ID de la tâche";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "L'ID de tâche est requis";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "Impossible de récupérer les informations du document";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "Aucun champ de document sélectionné";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Sélectionnez les champs";
