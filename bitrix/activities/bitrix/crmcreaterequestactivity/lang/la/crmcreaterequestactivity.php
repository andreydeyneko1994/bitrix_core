<?
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE_ON_ENTITY_ST_CHG"] = "Auto completar la actividad, cuando el estado se actualiza";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Descripción";
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Asunto";
?>