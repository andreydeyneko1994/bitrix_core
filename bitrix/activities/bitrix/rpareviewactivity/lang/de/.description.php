<?php
$MESS["RPA_BP_REV_DESCR_DESCRIPTION"] = "Erstellt eine Aufgabe, wo von verantwortlicher Person eine Bestätigung darüber gefordert wird, dass die zusätzlichen Informationen gelesen wurden.";
$MESS["RPA_BP_REV_DESCR_LAST_REVIEWER"] = "Zuletzt gelesen von";
$MESS["RPA_BP_REV_DESCR_NAME"] = "Informationen lesen";
