<?php
$MESS["RPA_BP_REV_DESCR_DESCRIPTION"] = "Creates a task that prompts the responsible person to confirm they have read additional information.";
$MESS["RPA_BP_REV_DESCR_LAST_REVIEWER"] = "Last read by";
$MESS["RPA_BP_REV_DESCR_NAME"] = "Review information";
