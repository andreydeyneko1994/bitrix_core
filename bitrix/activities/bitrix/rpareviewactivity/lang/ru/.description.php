<?php

$MESS['RPA_BP_REV_DESCR_NAME'] = 'Ознакомиться с информацией';
$MESS['RPA_BP_REV_DESCR_LAST_REVIEWER'] = 'Последний ознакомившийся';
$MESS['RPA_BP_REV_DESCR_DESCRIPTION'] = 'Формирует задание, в котором ответственный должен подтвердить факт ознакомления с предложенной информацией';
