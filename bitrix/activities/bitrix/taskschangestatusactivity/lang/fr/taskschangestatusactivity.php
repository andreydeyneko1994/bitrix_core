<?
$MESS["TASKS_CHANGE_STATUS_COMPLETED"] = "Terminée";
$MESS["TASKS_CHANGE_STATUS_DEFERRED"] = "Repoussée";
$MESS["TASKS_CHANGE_STATUS_EMPTY_PROP"] = "Le statut final n'est pas spécifié";
$MESS["TASKS_CHANGE_STATUS_IN_PROGRESS"] = "En cours";
$MESS["TASKS_CHANGE_STATUS_MODIFIED_BY"] = "Modifier de la part de";
$MESS["TASKS_CHANGE_STATUS_NO_PERMISSIONS"] = "Permissions insuffisantes pour modifier le statut de la tâche";
$MESS["TASKS_CHANGE_STATUS_PENDING"] = "En attente";
$MESS["TASKS_CHANGE_STATUS_STATUS"] = "Nouveau statut";
?>