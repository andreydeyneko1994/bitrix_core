<?
$MESS["TASKS_CHANGE_STATUS_COMPLETED"] = "Concluída";
$MESS["TASKS_CHANGE_STATUS_DEFERRED"] = "Adiada";
$MESS["TASKS_CHANGE_STATUS_EMPTY_PROP"] = "O status final não está especificado";
$MESS["TASKS_CHANGE_STATUS_IN_PROGRESS"] = "Em andamento";
$MESS["TASKS_CHANGE_STATUS_MODIFIED_BY"] = "Mudança em nome de";
$MESS["TASKS_CHANGE_STATUS_NO_PERMISSIONS"] = "Permissões insuficientes para alterar status da tarefa";
$MESS["TASKS_CHANGE_STATUS_PENDING"] = "Pendente";
$MESS["TASKS_CHANGE_STATUS_STATUS"] = "Novo status";
?>