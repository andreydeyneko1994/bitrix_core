<?php
$MESS["BIZPROC_AUTOMATION_SGVA_ACCESS_DENIED"] = "Seuls les administrateurs Bitrix24 peuvent accéder aux propriétés des règles d'automatisation.";
$MESS["BIZPROC_AUTOMATION_SGVA_DELETE"] = "Supprimer";
$MESS["BIZPROC_AUTOMATION_SGVA_VARIABLE_LIST"] = "Sélectionner une variable globale";
