<?php
$MESS["BIZPROC_AUTOMATION_SGVA_ACCESS_DENIED"] = "Solo los administradores de Bitrix24 pueden acceder a las reglas de automatización.";
$MESS["BIZPROC_AUTOMATION_SGVA_DELETE"] = "Eliminar";
$MESS["BIZPROC_AUTOMATION_SGVA_VARIABLE_LIST"] = "Seleccionar una variable global";
