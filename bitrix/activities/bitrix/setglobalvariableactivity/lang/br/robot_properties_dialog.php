<?php
$MESS["BIZPROC_AUTOMATION_SGVA_ACCESS_DENIED"] = "Apenas os administradores do Bitrix24 podem acessar propriedades de regra de automação.";
$MESS["BIZPROC_AUTOMATION_SGVA_DELETE"] = "Excluir";
$MESS["BIZPROC_AUTOMATION_SGVA_VARIABLE_LIST"] = "Selecionar variável global";
