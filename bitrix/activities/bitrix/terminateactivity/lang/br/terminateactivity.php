<?php
$MESS["BPTA1_KILL_WF_NAME"] = "Excluir dados do fluxo de trabalho";
$MESS["BPTA1_STATE_TITLE"] = "Execução interrompida.";
$MESS["BPTA1_STATE_TITLE_NAME"] = "Texto de Status";
$MESS["BPTA1_TERMINATE"] = "Abortar fluxo de trabalho";
$MESS["BPTA1_TERMINATE_ALL"] = "Todos os fluxos de trabalho de modelo";
$MESS["BPTA1_TERMINATE_ALL_EXCEPT_CURRENT"] = "Todos os fluxos de trabalho de modelo, exceto o atual";
$MESS["BPTA1_TERMINATE_CURRENT"] = "Atual";
