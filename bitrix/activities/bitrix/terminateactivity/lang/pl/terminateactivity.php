<?php
$MESS["BPTA1_KILL_WF_NAME"] = "Usuń dane workflowu";
$MESS["BPTA1_STATE_TITLE"] = "Wykonanie przerwano.";
$MESS["BPTA1_STATE_TITLE_NAME"] = "Tekst statusu";
$MESS["BPTA1_TERMINATE"] = "Informacje o workflowie";
$MESS["BPTA1_TERMINATE_ALL"] = "Wszystkie workflowy szablonów";
$MESS["BPTA1_TERMINATE_ALL_EXCEPT_CURRENT"] = "Wszystkie workflowy szablonów z wyjątkiem bieżącego";
$MESS["BPTA1_TERMINATE_CURRENT"] = "Bieżący";
