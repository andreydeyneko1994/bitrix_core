<?php
$MESS["CRM_CRA_ACTION_ADD"] = "Adicionar";
$MESS["CRM_CRA_ACTION_REMOVE"] = "Excluir";
$MESS["CRM_CRA_ACTION_REPLACE"] = "Substituir";
$MESS["CRM_CRA_ACTION_TYPE"] = "Tipo de ação";
$MESS["CRM_CRA_ELEMENT_EXISTENCE_ERROR"] = "O item que você selecionou não existe";
$MESS["CRM_CRA_ELEMENT_NOT_CHOSEN_ERROR"] = "Item vinculado não selecionado";
$MESS["CRM_CRA_PARENT_ID"] = "ID do item";
$MESS["CRM_CRA_PARENT_TYPE"] = "Vinculado a";
