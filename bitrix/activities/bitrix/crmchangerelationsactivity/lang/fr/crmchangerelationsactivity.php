<?php
$MESS["CRM_CRA_ACTION_ADD"] = "Ajouter";
$MESS["CRM_CRA_ACTION_REMOVE"] = "Supprimer";
$MESS["CRM_CRA_ACTION_REPLACE"] = "Remplacer";
$MESS["CRM_CRA_ACTION_TYPE"] = "démarrage de l'action";
$MESS["CRM_CRA_ELEMENT_EXISTENCE_ERROR"] = "L'élément que vous avez sélectionné n'existe pas";
$MESS["CRM_CRA_ELEMENT_NOT_CHOSEN_ERROR"] = "Élément associé non sélectionné";
$MESS["CRM_CRA_PARENT_ID"] = "ID de l'élément";
$MESS["CRM_CRA_PARENT_TYPE"] = "Lié à";
