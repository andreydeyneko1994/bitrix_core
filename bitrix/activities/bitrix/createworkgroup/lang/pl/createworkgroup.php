<?
$MESS["BPCWG_EMPTY_GROUP_NAME"] = "Brakuje nazwy grupy.";
$MESS["BPCWG_EMPTY_OWNER"] = "Brakuje właściciela grupy.";
$MESS["BPCWG_EMPTY_USERS"] = "Członkowie grupy nie są określeni.";
$MESS["BPCWG_ERROR_CREATE_GROUP"] = "Błąd tworzenia grupy.";
$MESS["BPCWG_ERROR_SUBJECT_ID"] = "Nie udało się uzyskać listy tematów.";
$MESS["BPCWG_FIELD_REQUIED"] = "Pole '#FIELD#' jest wymagane.";
?>