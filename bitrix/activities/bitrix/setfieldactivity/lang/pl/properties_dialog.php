<?
$MESS["BPSFA_PD_ADD"] = "Dodaj Warunek";
$MESS["BPSFA_PD_CALENDAR"] = "Kalendarz";
$MESS["BPSFA_PD_CANCEL"] = "Anuluj";
$MESS["BPSFA_PD_CANCEL_HINT"] = "Anuluj";
$MESS["BPSFA_PD_CREATE"] = "Dodaj pole";
$MESS["BPSFA_PD_DELETE"] = "Usuń";
$MESS["BPSFA_PD_EMPTY_CODE"] = "Brakuje pola kodu";
$MESS["BPSFA_PD_EMPTY_NAME"] = "Brakuje pola nazwy";
$MESS["BPSFA_PD_FIELD"] = "Pole";
$MESS["BPSFA_PD_F_CODE"] = "ID";
$MESS["BPSFA_PD_F_LIST"] = "Wartości";
$MESS["BPSFA_PD_F_MULT"] = "Wielokrotne";
$MESS["BPSFA_PD_F_NAME"] = "Nazwa";
$MESS["BPSFA_PD_F_REQ"] = "Wymagany";
$MESS["BPSFA_PD_F_TYPE"] = "Rodzaj";
$MESS["BPSFA_PD_MODIFIED_BY"] = "Zmień w imieniu";
$MESS["BPSFA_PD_NO"] = "Nie";
$MESS["BPSFA_PD_SAVE"] = "Zapisz";
$MESS["BPSFA_PD_SAVE_HINT"] = "Utwórz pole";
$MESS["BPSFA_PD_WRONG_CODE"] = "W kodzie pola mogą znajdować się tylko łacińskie litery i liczby oraz nie może zaczynać się od cyfry.";
$MESS["BPSFA_PD_YES"] = "Tak";
?>