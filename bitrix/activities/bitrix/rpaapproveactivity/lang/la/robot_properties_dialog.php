<?
$MESS["RPA_BP_APR_SPD_BTN_ACTION"] = "Moverse a la etapa";
$MESS["RPA_BP_APR_SPD_BTN_ADD_BTN"] = "Agregar botón";
$MESS["RPA_BP_APR_SPD_BTN_COLOR"] = "Color";
$MESS["RPA_BP_APR_SPD_BTN_NAME"] = "Nombre";
$MESS["RPA_BP_APR_SPD_HELP_ABSENCE"] = "Los sustitutos participarán si todos los usuarios en la etapa actual no están disponibles.";
$MESS["RPA_BP_APR_SPD_HELP_EXECUTIVE"] = "Seleccione usuarios que tendrán la última palabra en la aprobación. Si existe al menos un \"Aprobador final\" en la cadena de supervisores de aprobación, la asignación de aprobación se detiene después de que esa persona haya tomado la decisión.";
$MESS["RPA_BP_APR_SPD_HELP_HEADS"] = "El proceso de aprobación seguirá la cadena de supervisores del empleado hasta la parte superior de la cadena, o hasta las personas especificadas como \"Aprobador final\".";
$MESS["RPA_BP_APR_SPD_RESPONSIBLE"] = "Personas responsables";
$MESS["RPA_BP_APR_SPD_TASK"] = "Asignaciones";
?>