<?
$MESS["RPA_BP_APR_SPD_BTN_ACTION"] = "Przesuń do etapu";
$MESS["RPA_BP_APR_SPD_BTN_ADD_BTN"] = "Przycisk dodawania";
$MESS["RPA_BP_APR_SPD_BTN_COLOR"] = "Kolor";
$MESS["RPA_BP_APR_SPD_BTN_NAME"] = "Nazwa";
$MESS["RPA_BP_APR_SPD_HELP_ABSENCE"] = "Zastępcy będą uczestniczyli w zatwierdzeniu, jeśli wszyscy użytkownicy na bieżącym etapie nie będą dostępni.";
$MESS["RPA_BP_APR_SPD_HELP_EXECUTIVE"] = "Wskaż ostatnią osobę, która zatwierdzi workflow. Jeśli w łańcuchu kierowników istnieje przynajmniej jeden \"Ostatni zatwierdzający”, proces zatwierdzenia zostanie przerwany po decyzji tej osoby.";
$MESS["RPA_BP_APR_SPD_HELP_HEADS"] = "Zatwierdzenie przejdzie przez łańcuch kierowników pracownika, który uruchomił workflow, do najwyższego poziomu lub do osób określonych jako \"Ostatni Zatwierdzający\"";
$MESS["RPA_BP_APR_SPD_RESPONSIBLE"] = "Osoby odpowiedzialne";
$MESS["RPA_BP_APR_SPD_TASK"] = "Zadanie";
?>