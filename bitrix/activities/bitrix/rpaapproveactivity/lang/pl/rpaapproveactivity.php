<?
$MESS["RPA_BP_APR_ACT_NO_ACTION"] = "Nie określono działania.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY1"] = "Nie wybrano osób odpowiedzialnych za zadanie.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY2"] = "Nie określono typu zatwierdzenia.";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY3"] = "Nieprawidłowy typ zatwierdzenia";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY4"] = "Nie określono nazwy zadania.";
$MESS["RPA_BP_APR_ERROR_STAGE_ID"] = "Niepoprawny etap docelowy";
$MESS["RPA_BP_APR_FIELD_ACTIONS"] = "Przyciski";
$MESS["RPA_BP_APR_FIELD_ALTER_RESPONSIBLE"] = "Wskaż zastępcę";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_NO"] = "Odrzuć";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_YES"] = "Zatwierdź";
$MESS["RPA_BP_APR_FIELD_APPROVE_FIXED_COUNT"] = "Osoby do zatwierdzenia";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE"] = "Typ zatwierdzenia";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ALL"] = "Wszystkie";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ANY"] = "Dowolne wyszczególnione";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_FIXED"] = "Dowolne";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_QUEUE"] = "Wszystkie w kolejce";
$MESS["RPA_BP_APR_FIELD_DESCRIPTION"] = "Tekst";
$MESS["RPA_BP_APR_FIELD_EXECUTIVE_RESPONSIBLE"] = "Ostateczne zatwierdzenie";
$MESS["RPA_BP_APR_FIELD_FIELDS_TO_SHOW"] = "Pokaż pola";
$MESS["RPA_BP_APR_FIELD_NAME"] = "Nazwa";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE"] = "Rodzaj odpowiedzialnych";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_HEADS"] = "Kierownicy pracownika";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_PLAIN"] = "Kolejka użytkownika";
$MESS["RPA_BP_APR_FIELD_SKIP_ABSENT"] = "Pomiń nieobecnych użytkowników";
$MESS["RPA_BP_APR_FIELD_USERS"] = "Osoby odpowiedzialne";
$MESS["RPA_BP_APR_RUNTIME_TERMINATED"] = "Zmieniono etap";
?>