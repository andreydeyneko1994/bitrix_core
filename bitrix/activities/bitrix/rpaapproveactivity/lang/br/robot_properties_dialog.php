<?
$MESS["RPA_BP_APR_SPD_BTN_ACTION"] = "Mover para a etapa";
$MESS["RPA_BP_APR_SPD_BTN_ADD_BTN"] = "Botão adicionar";
$MESS["RPA_BP_APR_SPD_BTN_COLOR"] = "Cor";
$MESS["RPA_BP_APR_SPD_BTN_NAME"] = "Nome";
$MESS["RPA_BP_APR_SPD_HELP_ABSENCE"] = "Os usuários de reserva participarão se todos os usuários no estágio atual não estiverem disponíveis.";
$MESS["RPA_BP_APR_SPD_HELP_EXECUTIVE"] = "Selecione usuários que terão a última palavra na aprovação. Se pelo menos um \"Aprovador final\" existir na cadeia de supervisores da aprovação, o processo de aprovação será interrompido após a pessoa tomar a decisão.";
$MESS["RPA_BP_APR_SPD_HELP_HEADS"] = "O processo de aprovação seguirá a cadeia de supervisores do funcionário até o topo da cadeia ou até as pessoas especificadas como \"Aprovador final\".";
$MESS["RPA_BP_APR_SPD_RESPONSIBLE"] = "Pessoas responsáveis";
$MESS["RPA_BP_APR_SPD_TASK"] = "Atribuição";
?>