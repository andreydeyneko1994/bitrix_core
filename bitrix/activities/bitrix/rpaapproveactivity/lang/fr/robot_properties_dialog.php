<?
$MESS["RPA_BP_APR_SPD_BTN_ACTION"] = "Déplacer à l'étape";
$MESS["RPA_BP_APR_SPD_BTN_ADD_BTN"] = "Ajouter un bouton";
$MESS["RPA_BP_APR_SPD_BTN_COLOR"] = "Couleur";
$MESS["RPA_BP_APR_SPD_BTN_NAME"] = "Nom";
$MESS["RPA_BP_APR_SPD_HELP_ABSENCE"] = "Les remplaçants autorisés participeront à l'approbation si tous les utilisateurs ne sont pas disponibles à l'étape actuelle.";
$MESS["RPA_BP_APR_SPD_HELP_EXECUTIVE"] = "Indiquez les employés qui ont le droit à l'approbation finale. Si au moins un \"Approbateur final\" existe dans la chaîne de superviseurs d'approbation, alors l'affectation d'approbation s'arrête après que cette personne a pris la décision.";
$MESS["RPA_BP_APR_SPD_HELP_HEADS"] = "Le processus d'approbation suivra la chaîne des superviseurs des employés jusqu'au plus haut niveau ou jusqu'aux personnes spécifiées comme \"Approbateur final\".";
$MESS["RPA_BP_APR_SPD_RESPONSIBLE"] = "Responsables";
$MESS["RPA_BP_APR_SPD_TASK"] = "Affectation";
?>