<?
$MESS["BPMA_ATTACHMENT"] = "Anexos";
$MESS["BPMA_ATTACHMENT_DISK"] = "Arquivo do drive";
$MESS["BPMA_ATTACHMENT_FILE"] = "Arquivos";
$MESS["BPMA_ATTACHMENT_TYPE"] = "Tipo de anexo";
$MESS["BPMA_EMPTY_PROP1"] = "O parâmetro \"Remetente\" está faltando.";
$MESS["BPMA_EMPTY_PROP2"] = "O parâmetro \"Destinatário\" está faltando.";
$MESS["BPMA_EMPTY_PROP3"] = "O parâmetro \"Assunto\" está faltando.";
$MESS["BPMA_EMPTY_PROP4"] = "O parâmetro \"Codificação\" está faltando.";
$MESS["BPMA_EMPTY_PROP5"] = "O parâmetro \"Modelo de Mensagem\" está faltando.";
$MESS["BPMA_EMPTY_PROP6"] = "O parâmetro \"Modelo de Mensagem\" está incorreto.";
$MESS["BPMA_EMPTY_PROP7"] = "O parâmetro \"Mensagem de Texto\" está faltando.";
$MESS["BPMA_MAIL_SUBJECT"] = "Assunto";
$MESS["BPMA_MAIL_USER_FROM"] = "De";
$MESS["BPMA_MAIL_USER_TO"] = "Para";
?>