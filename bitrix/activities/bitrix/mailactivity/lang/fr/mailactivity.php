<?
$MESS["BPMA_ATTACHMENT"] = "Pièces jointes";
$MESS["BPMA_ATTACHMENT_DISK"] = "Fichiers du lecteur";
$MESS["BPMA_ATTACHMENT_FILE"] = "Mon disque";
$MESS["BPMA_ATTACHMENT_TYPE"] = "Type de pièce jointe";
$MESS["BPMA_EMPTY_PROP1"] = "Le paramètre 'Expéditeur' n'est pas indiqué.";
$MESS["BPMA_EMPTY_PROP2"] = "La propriété 'Destinataire' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP3"] = "La propriété 'Thème du message' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP4"] = "La propriété 'Encodage de message' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP5"] = "La propriété 'Type de message' n'est pas indiqué.";
$MESS["BPMA_EMPTY_PROP6"] = "Valeur de la propriété ' Type de message' est incorrecte.";
$MESS["BPMA_EMPTY_PROP7"] = "Propriété 'Texte du message' n'est pas indiqué.";
$MESS["BPMA_MAIL_SUBJECT"] = "Objet";
$MESS["BPMA_MAIL_USER_FROM"] = "De";
$MESS["BPMA_MAIL_USER_TO"] = "À";
?>