<?
$MESS["BPDUV_ACCESS_DENIED"] = "Odmowa dostępu dla wszystkich poza administratorami portalu.";
$MESS["BPDUV_EMPTY_SOURCE_FILE"] = "Nie wybrano żadnego z załadowanych plików.";
$MESS["BPDUV_EMPTY_SOURCE_ID"] = "Źródło nie jest określone.";
$MESS["BPDUV_SOURCE_FILE_ERROR"] = "Błąd pobierania pliku do przesłania.";
$MESS["BPDUV_SOURCE_ID_ERROR"] = "Nie znaleziono pliku źródłowego.";
$MESS["BPDUV_UPLOAD_ERROR"] = "Błąd przesyłania nowej wersji.";
?>