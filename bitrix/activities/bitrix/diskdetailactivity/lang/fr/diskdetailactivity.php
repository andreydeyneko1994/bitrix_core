<?
$MESS["BPDD_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDD_EMPTY_SOURCE_ID"] = "Le fichier source n'est pas spécifié.";
$MESS["BPDD_SOURCE_ID_ERROR"] = "L'objet source n'a pas été trouvé.";
?>