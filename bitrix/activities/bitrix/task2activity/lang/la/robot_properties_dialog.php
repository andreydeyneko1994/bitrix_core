<?php
$MESS["TASKS_BP_RPD_ADDITIONAL"] = "más";
$MESS["TASKS_BP_RPD_AS_CHILD_TASK"] = "Crear como subtarea de esta tarea";
$MESS["TASKS_BP_RPD_HOLD_TO_CLOSE"] = "Proceso de pausa mientras se ejecuta la tarea";
$MESS["TASKS_BP_RPD_PRIORITY"] = "Alta prioridad";
