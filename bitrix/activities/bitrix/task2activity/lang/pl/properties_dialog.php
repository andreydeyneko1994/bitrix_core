<?php
$MESS["BPCGHLP_HOLD_TO_CLOSE"] = "Zatrzymaj proces w trakcie działania zadania";
$MESS["BPCGHLP_NO"] = "Nie";
$MESS["BPCGHLP_YES"] = "Tak";
$MESS["TASKS_BP_AUTO_LINK_TO_CRM_ENTITY"] = "Dołącz do obecnej jednostki CRM";
$MESS["TASKS_BP_PD_AS_CHILD_TASK"] = "Utwórz jako podzadanie tego zadania";
