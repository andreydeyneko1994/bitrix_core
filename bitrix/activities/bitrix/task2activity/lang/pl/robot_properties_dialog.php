<?php
$MESS["TASKS_BP_RPD_ADDITIONAL"] = "więcej";
$MESS["TASKS_BP_RPD_AS_CHILD_TASK"] = "Utwórz jako podzadanie tego zadania";
$MESS["TASKS_BP_RPD_HOLD_TO_CLOSE"] = "Zatrzymaj proces w trakcie działania zadania";
$MESS["TASKS_BP_RPD_PRIORITY"] = "Wysoki priorytet";
