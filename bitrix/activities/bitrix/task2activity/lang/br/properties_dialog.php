<?php
$MESS["BPCGHLP_HOLD_TO_CLOSE"] = "Pausar processo enquanto a tarefa está sendo executada";
$MESS["BPCGHLP_NO"] = "Não";
$MESS["BPCGHLP_YES"] = "Sim";
$MESS["TASKS_BP_AUTO_LINK_TO_CRM_ENTITY"] = "Vincular a atual entidade CRM";
$MESS["TASKS_BP_PD_AS_CHILD_TASK"] = "Criar como subtarefa desta tarefa";
