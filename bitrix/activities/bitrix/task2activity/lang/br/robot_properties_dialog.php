<?php
$MESS["TASKS_BP_RPD_ADDITIONAL"] = "mais";
$MESS["TASKS_BP_RPD_AS_CHILD_TASK"] = "Criar como subtarefa desta tarefa";
$MESS["TASKS_BP_RPD_HOLD_TO_CLOSE"] = "Pausar processo enquanto a tarefa está sendo executada";
$MESS["TASKS_BP_RPD_PRIORITY"] = "Alta Prioridade";
