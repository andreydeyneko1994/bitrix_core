<?php
$MESS["CRM_CRL_CREATED_LEAD"] = "Prospecto creado";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "No se pueden obtener datos de la negociaciones";
$MESS["CRM_CRL_LEAD_TITLE"] = "Título de prospecto";
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "Sin título";
$MESS["CRM_CRL_NO_CLIENTS"] = "No se puede encontrar contactos para enlazar al prospecto repetido";
$MESS["CRM_CRL_RESPONSIBLE"] = "Persona responsable";
