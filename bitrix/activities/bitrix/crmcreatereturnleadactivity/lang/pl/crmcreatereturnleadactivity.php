<?php
$MESS["CRM_CRL_CREATED_LEAD"] = "Utworzono leada";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "Nie można uzyskać danych dealu";
$MESS["CRM_CRL_LEAD_TITLE"] = "Nazwa leada";
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "Bez tytułu";
$MESS["CRM_CRL_NO_CLIENTS"] = "Nie można znaleźć kontaktów do powiązania z powtórnym leadem";
$MESS["CRM_CRL_RESPONSIBLE"] = "Osoba odpowiedzialna";
