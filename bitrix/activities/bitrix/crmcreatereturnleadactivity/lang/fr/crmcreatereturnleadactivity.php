<?php
$MESS["CRM_CRL_CREATED_LEAD"] = "Prospect créé";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "Impossible de récupérer les données de la transaction";
$MESS["CRM_CRL_LEAD_TITLE"] = "Nom du prospect";
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "Sans nom";
$MESS["CRM_CRL_NO_CLIENTS"] = "Impossible de trouver les contacts à lier au prospect répété";
$MESS["CRM_CRL_RESPONSIBLE"] = "Personne responsable";
