<?php
$MESS["CRM_CRL_CREATED_LEAD"] = "Cliente potencial criado";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "Não é possível obter dados do negócio";
$MESS["CRM_CRL_LEAD_TITLE"] = "Nome do Lead";
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "Sem título";
$MESS["CRM_CRL_NO_CLIENTS"] = "Não é possível encontrar contatos para vincular ao cliente potencial repetido";
$MESS["CRM_CRL_RESPONSIBLE"] = "Pessoa responsável";
