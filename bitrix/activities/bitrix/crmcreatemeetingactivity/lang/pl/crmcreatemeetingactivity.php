<?
$MESS["CRM_CREATE_MEETING_AUTO_COMPLETE"] = "Automatycznie uruchamiane po ukończeniu workflowu";
$MESS["CRM_CREATE_MEETING_DESCRIPTION"] = "Opis";
$MESS["CRM_CREATE_MEETING_EMPTY_PROP"] = "Wymagany parametr jest pusty: #PROPERTY#";
$MESS["CRM_CREATE_MEETING_END_TIME"] = "Data końcowa";
$MESS["CRM_CREATE_MEETING_IS_IMPORTANT"] = "Ważne";
$MESS["CRM_CREATE_MEETING_LOCATION"] = "Lokalizacja";
$MESS["CRM_CREATE_MEETING_NOTIFY_TYPE"] = "Okres przypominania";
$MESS["CRM_CREATE_MEETING_NOTIFY_VALUE"] = "Przypomnij za";
$MESS["CRM_CREATE_MEETING_RESPONSIBLE_ID"] = "Osoba odpowiedzialna";
$MESS["CRM_CREATE_MEETING_START_TIME"] = "Data początkowa";
$MESS["CRM_CREATE_MEETING_SUBJECT"] = "Temat spotkania";
?>