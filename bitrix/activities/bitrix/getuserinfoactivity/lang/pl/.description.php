<?
$MESS["BPGUIA_DESCR_DESCR"] = "Informacje o pracowniku";
$MESS["BPGUIA_DESCR_NAME"] = "Informacje o pracowniku";
$MESS["BPGUIA_IS_ABSENT"] = "Poza biurem (zgodnie z harmonogramem nieobecności)";
$MESS["BPGUIA_TIMEMAN_STATUS"] = "Status dnia roboczego";
$MESS["BPGUIA_USER_ACTIVE"] = "Aktywny";
$MESS["BPGUIA_USER_EMAIL"] = "E-mail";
$MESS["BPGUIA_USER_LAST_NAME"] = "Nazwisko";
$MESS["BPGUIA_USER_LOGIN"] = "Login";
$MESS["BPGUIA_USER_NAME"] = "Imię";
$MESS["BPGUIA_USER_PERSONAL_BIRTHDAY"] = "Data urodzenia";
$MESS["BPGUIA_USER_PERSONAL_CITY"] = "Miasto";
$MESS["BPGUIA_USER_PERSONAL_MOBILE"] = "Telefon Komórkowy";
$MESS["BPGUIA_USER_PERSONAL_WWW"] = "Strona internetowa";
$MESS["BPGUIA_USER_SECOND_NAME"] = "Drugie imię";
$MESS["BPGUIA_USER_UF_DEPARTMENT"] = "Dział (lista ID)";
$MESS["BPGUIA_USER_UF_FACEBOOK"] = "Facebook";
$MESS["BPGUIA_USER_UF_LINKEDIN"] = "LinkedIn";
$MESS["BPGUIA_USER_UF_PHONE_INNER"] = "Numer wewnętrzny";
$MESS["BPGUIA_USER_UF_SKYPE"] = "Skype";
$MESS["BPGUIA_USER_UF_TWITTER"] = "Twitter";
$MESS["BPGUIA_USER_UF_WEB_SITES"] = "Inne strony internetowe";
$MESS["BPGUIA_USER_UF_XING"] = "Xing";
$MESS["BPGUIA_USER_WORK_PHONE"] = "Telefon służbowy";
$MESS["BPGUIA_USER_WORK_POSITION"] = "Stanowisko";
?>