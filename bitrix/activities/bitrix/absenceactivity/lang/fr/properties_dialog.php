<?
$MESS["BPSNAA2_PD_CDESCR"] = "Description de l'évènement";
$MESS["BPSNAA2_PD_CFROM"] = "A partir de la date";
$MESS["BPSNAA2_PD_CFSTATE"] = "En état d'achèvement";
$MESS["BPSNAA2_PD_CNAME"] = "Le nom de l'évènement";
$MESS["BPSNAA2_PD_CSTATE"] = "Arrondissement";
$MESS["BPSNAA2_PD_CTO"] = "Date de clôture";
$MESS["BPSNAA2_PD_CTYPES"] = "Type d'absence";
$MESS["BPSNAA2_PD_CUSER"] = "Utilisateur";
?>