<?php
$MESS["BPMC_PD_ACTIVITY_RESULTS"] = "Resultados adicionais";
$MESS["BPMC_PD_ADD"] = "Adicionar condição";
$MESS["BPMC_PD_AND"] = "E";
$MESS["BPMC_PD_CONDITION"] = "Condição";
$MESS["BPMC_PD_CONSTANTS"] = "Constantes";
$MESS["BPMC_PD_DELETE"] = "Excluir";
$MESS["BPMC_PD_DOCUMENT_FIELDS"] = "Campos do elemento";
$MESS["BPMC_PD_FIELD"] = "Fonte";
$MESS["BPMC_PD_FIELD_CHOOSE"] = "selecionar";
$MESS["BPMC_PD_OR"] = "OU";
$MESS["BPMC_PD_PARAMS"] = "Parâmetros";
$MESS["BPMC_PD_VALUE"] = "Valor";
$MESS["BPMC_PD_VARS"] = "Variáveis";
