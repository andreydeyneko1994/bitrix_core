<?php
$MESS["BPMC_PD_ACTIVITY_RESULTS"] = "Dodatkowe wyniki";
$MESS["BPMC_PD_ADD"] = "Dodaj warunek";
$MESS["BPMC_PD_AND"] = "I";
$MESS["BPMC_PD_CONDITION"] = "Warunek";
$MESS["BPMC_PD_CONSTANTS"] = "Stałe";
$MESS["BPMC_PD_DELETE"] = "Usuń";
$MESS["BPMC_PD_DOCUMENT_FIELDS"] = "Pola elementów";
$MESS["BPMC_PD_FIELD"] = "Źródło";
$MESS["BPMC_PD_FIELD_CHOOSE"] = "wybierz";
$MESS["BPMC_PD_OR"] = "LUB";
$MESS["BPMC_PD_PARAMS"] = "Parametry";
$MESS["BPMC_PD_VALUE"] = "Wartość";
$MESS["BPMC_PD_VARS"] = "Zmienne";
