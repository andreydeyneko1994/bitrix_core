<?php
$MESS["BPMC_PD_ACTIVITY_RESULTS"] = "Résultats supplémentaires";
$MESS["BPMC_PD_ADD"] = "Ajouter une condition";
$MESS["BPMC_PD_AND"] = "ET";
$MESS["BPMC_PD_CONDITION"] = "Condition";
$MESS["BPMC_PD_CONSTANTS"] = "Constantes";
$MESS["BPMC_PD_DELETE"] = "Supprimer";
$MESS["BPMC_PD_DOCUMENT_FIELDS"] = "Champs de l'élément";
$MESS["BPMC_PD_FIELD"] = "Source";
$MESS["BPMC_PD_FIELD_CHOOSE"] = "sélectionner";
$MESS["BPMC_PD_OR"] = "OU";
$MESS["BPMC_PD_PARAMS"] = "Paramètres";
$MESS["BPMC_PD_VALUE"] = "Valeur";
$MESS["BPMC_PD_VARS"] = "Variables";
