<?php
$MESS["CRM_GRI_BINDING"] = "Vinculado a";
$MESS["CRM_GRI_ENTITY_EXISTENCE_ERROR"] = "Nenhuma entidade #ELEMENT_TYPE# está vinculada a este item";
$MESS["CRM_GRI_PARENT_TYPE_ERROR"] = "Tipo de entidade incorreto selecionado";
$MESS["CRM_GRI_PARENT_TYPE_EXISTENCE_ERROR"] = "O tipo que você selecionou não tem visualização nos fluxos de trabalho";
$MESS["CRM_GRI_RELATION_EXISTENCE_ERROR"] = "O link que você selecionou não existe";
