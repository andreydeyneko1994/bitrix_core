<?php
$MESS["CRM_GRI_BINDING"] = "Lié à";
$MESS["CRM_GRI_ENTITY_EXISTENCE_ERROR"] = "Aucune entité de type #ELEMENT_TYPE# n'est associée à cet élément";
$MESS["CRM_GRI_PARENT_TYPE_ERROR"] = "Le type d'entité sélectionné est incorrect";
$MESS["CRM_GRI_PARENT_TYPE_EXISTENCE_ERROR"] = "Le type que vous avez sélectionné n'est pas visible dans les flux de travail";
$MESS["CRM_GRI_RELATION_EXISTENCE_ERROR"] = "Le lien que vous avez sélectionné n'existe pas";
