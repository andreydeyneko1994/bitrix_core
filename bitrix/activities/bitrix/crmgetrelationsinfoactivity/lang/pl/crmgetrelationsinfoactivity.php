<?php
$MESS["CRM_GRI_BINDING"] = "Powiązane z";
$MESS["CRM_GRI_ENTITY_EXISTENCE_ERROR"] = "Z tym elementem nie są powiązane żadne jednostki #ELEMENT_TYPE#";
$MESS["CRM_GRI_PARENT_TYPE_ERROR"] = "Wybrano nieprawidłowy typ jednostki";
$MESS["CRM_GRI_PARENT_TYPE_EXISTENCE_ERROR"] = "Wybrany typ nie ma widoku w worflowach";
$MESS["CRM_GRI_RELATION_EXISTENCE_ERROR"] = "Wybrany link nie istnieje";
