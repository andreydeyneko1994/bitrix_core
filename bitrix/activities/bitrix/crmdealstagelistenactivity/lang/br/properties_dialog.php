<?
$MESS["BPCDSA_PD_DEAL"] = "ID do Negócio ";
$MESS["BPCDSA_PD_STAGE"] = "Estágio";
$MESS["BPCDSA_PD_STAGE_DESCR"] = "Selecione uma ou mais etapas a aguardar. O fluxo de trabalho irá aguardar até que o negócio tenha se movido para a etapa final ou selecionada.";
?>