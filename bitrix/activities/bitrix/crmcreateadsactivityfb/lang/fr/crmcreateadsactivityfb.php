<?php
$MESS["CRM_CREATE_ADS_EMPTY_PROP"] = "Le champ requis est manquant";
$MESS["CRM_CREATE_ADS_FB_RESTRICTED"] = "La règle d'automatisation n'est pas disponible dans votre région.";
$MESS["CRM_CREATE_ADS_WRONG_ARM"] = "Cette valeur doit être un chiffre";
