<?
$MESS["SNBPA_BLOG_NAME"] = "Blog";
$MESS["SNBPA_EMPTY_OWNER"] = "Autor wiadomości jest nieokreślony.";
$MESS["SNBPA_EMPTY_POST_MESSAGE"] = "Tekst wiadomości jest pusty.";
$MESS["SNBPA_EMPTY_USERS"] = "Nie wskazano odbiorców wiadomości.";
$MESS["SNBPA_OWNER_ID"] = "Nadawca";
$MESS["SNBPA_POST_MESSAGE"] = "Tekst wiadomości";
$MESS["SNBPA_POST_SITE"] = "Strona docelowa";
$MESS["SNBPA_POST_TITLE"] = "Tytuł wiadomości";
$MESS["SNBPA_USERS_TO"] = "Odbiorcy";
?>