<?
$MESS["CRM_ACTIVITY_ERROR_DT"] = "Nieprawidłowy rodzaj dokumentu.";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_ID"] = "ID jednostki";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_LIST_FIELDS"] = "Wybierz pole jednostki.";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_TYPE"] = "Rodzaj jednostki";
$MESS["CRM_ACTIVITY_ERROR_FIELD_REQUIED"] = "Pole '#FIELD#' jest wymagane.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_TYPE"] = "W polu '#FIELD#' określono nieprawidłowy typ.";
$MESS["CRM_ACTIVITY_FIELD_MAIN_NO"] = "Nie";
$MESS["CRM_ACTIVITY_FIELD_MAIN_YES"] = "Tak";
$MESS["CRM_ACTIVITY_FIELD_USER_TITLE"] = "(Użytkownik)";
$MESS["CRM_ACTIVITY_LABLE_PRINTABLE_VERSION"] = "Wersja do druku:";
$MESS["CRM_ACTIVITY_LABLE_SELECT_FIELDS"] = "Wybierz pola:";
?>