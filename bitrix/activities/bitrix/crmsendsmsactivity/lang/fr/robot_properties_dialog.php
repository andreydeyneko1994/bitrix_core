<?
$MESS["CRM_SSMSA_RPD_CHOOSE_PROVIDER"] = "sélectionner un fournisseur SMS...";
$MESS["CRM_SSMSA_RPD_MARKETPLACE"] = "Sélectionner un autre fournisseur";
$MESS["CRM_SSMSA_RPD_PROVIDER_CANT_USE"] = "Le fournisseur n'est pas disponible parce qu'il n'a pas été configuré";
$MESS["CRM_SSMSA_RPD_PROVIDER_IS_DEMO"] = "Le fournisseur est en mode démo";
$MESS["CRM_SSMSA_RPD_PROVIDER_MANAGE_URL"] = "Configurer le Fournisseur";
$MESS["CRM_SSMSA_SMS_SYMBOLS"] = "Caractères";
$MESS["CRM_SSMSA_SMS_SYMBOLS_FROM"] = "sur";
?>