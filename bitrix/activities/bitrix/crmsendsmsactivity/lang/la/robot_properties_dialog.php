<?
$MESS["CRM_SSMSA_RPD_CHOOSE_PROVIDER"] = "seleccione proveedor de SMS...";
$MESS["CRM_SSMSA_RPD_MARKETPLACE"] = "Seleccione otro proveedor";
$MESS["CRM_SSMSA_RPD_PROVIDER_CANT_USE"] = "El proveedor no está disponible porque no se ha configurado";
$MESS["CRM_SSMSA_RPD_PROVIDER_IS_DEMO"] = "El proveedor está en modo demo";
$MESS["CRM_SSMSA_RPD_PROVIDER_MANAGE_URL"] = "Configurar el proveedor";
$MESS["CRM_SSMSA_SMS_SYMBOLS"] = "Caracteres";
$MESS["CRM_SSMSA_SMS_SYMBOLS_FROM"] = "de";
?>