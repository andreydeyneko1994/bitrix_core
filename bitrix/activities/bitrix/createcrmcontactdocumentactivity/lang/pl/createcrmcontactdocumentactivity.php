<?
$MESS["BPCDA_FIELD_NOT_FOUND"] = "Brakuje właściwości \"#NAME#\".";
$MESS["BPCDA_FIELD_REQUIED"] = "Pole '#Field#' jest wymagane.";
$MESS["BPCDA_MODULE_NOT_LOADED"] = "Moduł CRM nie mógł być załadowany.";
$MESS["BPCDA_WRONG_TYPE"] = "Typ parametru '#PARAM#' jest niezdefiniowany.";
?>