<?
$MESS["BPDAF_ACCESS_DENIED"] = "Odmowa dostępu dla wszystkich poza administratorami portalu.";
$MESS["BPDAF_ADD_FOLDER_ERROR"] = "Nie można tworzyć nowego folderu.";
$MESS["BPDAF_EMPTY_ENTITY_ID"] = "Pamięć nie jest określona.";
$MESS["BPDAF_EMPTY_ENTITY_TYPE"] = "Brakuje typu pamięci.";
$MESS["BPDAF_EMPTY_FOLDER_NAME"] = "Nazwa folderu nie jest określona.";
$MESS["BPDAF_TARGET_ERROR"] = "Nie udało się ustalić lokalizacji nowego folderu.";
?>