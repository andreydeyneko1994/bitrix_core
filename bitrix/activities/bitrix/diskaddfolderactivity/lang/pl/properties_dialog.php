<?
$MESS["BPDAF_PD_ENTITY"] = "Utwórz w";
$MESS["BPDAF_PD_ENTITY_ID_COMMON"] = "Dysk";
$MESS["BPDAF_PD_ENTITY_ID_FOLDER"] = "Folder";
$MESS["BPDAF_PD_ENTITY_ID_SG"] = "Grupa";
$MESS["BPDAF_PD_ENTITY_ID_USER"] = "Użytkownik";
$MESS["BPDAF_PD_ENTITY_TYPE_COMMON"] = "Dysk publiczny";
$MESS["BPDAF_PD_ENTITY_TYPE_FOLDER"] = "Folder na Dysku";
$MESS["BPDAF_PD_ENTITY_TYPE_SG"] = "Grupa sieci społecznościowej dysku";
$MESS["BPDAF_PD_ENTITY_TYPE_USER"] = "Dysk użytkownika";
$MESS["BPDAF_PD_FOLDER_AUTHOR"] = "Stworzone przez";
$MESS["BPDAF_PD_FOLDER_NAME"] = "Nazwa folderu";
$MESS["BPDAF_PD_LABEL_CHOOSE"] = "Wybierz:";
$MESS["BPDAF_PD_LABEL_DISK_CHOOSE"] = "Wybierz folder";
$MESS["BPDAF_PD_LABEL_DISK_EMPTY"] = "Brak wybranego folderu";
?>