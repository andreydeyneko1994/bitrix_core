<?
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "Не заповнено обов'язковий параметр: #PROPERTY#";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Тема";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Дата початку";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Дата завершення";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Важлива справа";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Опис";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Нагадати за";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Інтервал нагадування";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Відповідальний";
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Виконати автоматично по завершенню процесу";
?>