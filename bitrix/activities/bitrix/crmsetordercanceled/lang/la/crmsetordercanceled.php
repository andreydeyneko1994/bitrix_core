<?
$MESS["CRM_SOCCL_COMMENT_NAME"] = "Razón de la cancelación";
$MESS["CRM_SOCCL_ORDER_ERROR"] = "La entidad actual no es una entidad de tipo \"Pedido\"";
$MESS["CRM_SOCCL_ORDER_IS_CANCELED"] = "El pedido ya ha sido cancelado.";
$MESS["CRM_SOCCL_ORDER_NOT_FOUND"] = "No se pueden obtener datos del pedido";
$MESS["CRM_SOCCL_STATUS_ERROR"] = "Estado de cancelación no especificado";
$MESS["CRM_SOCCL_STATUS_NAME"] = "Estado de cancelación";
$MESS["CRM_SOCCL_TERMINATE"] = "Completado porque el estado ha cambiado";
?>