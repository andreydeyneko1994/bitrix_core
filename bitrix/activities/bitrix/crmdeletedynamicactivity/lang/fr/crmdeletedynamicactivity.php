<?php
$MESS["CRM_DDA_DELETE_ERROR"] = "Erreur lors de la suppression de l'article";
$MESS["CRM_DDA_DYNAMIC_ID"] = "ID SPA";
$MESS["CRM_DDA_DYNAMIC_TYPE"] = "Type SPA";
$MESS["CRM_DDA_ELEMENT_ID"] = "ID de l'élément CRM";
$MESS["CRM_DDA_ELEMENT_TYPE"] = "Type d'élément CRM";
$MESS["CRM_DDA_ENTITY_ERROR"] = "Aucune entité n'utilise cet ID";
$MESS["CRM_DDA_TYPE_ID_ERROR"] = "Type d'entité non valide";
