<?php
$MESS["CRM_DDA_DELETE_ERROR"] = "Error al eliminar el elemento";
$MESS["CRM_DDA_DYNAMIC_ID"] = "ID de SPA";
$MESS["CRM_DDA_DYNAMIC_TYPE"] = "Tipo de SPA";
$MESS["CRM_DDA_ELEMENT_ID"] = "ID del elemento del CRM";
$MESS["CRM_DDA_ELEMENT_TYPE"] = "Tipo de elemento del CRM";
$MESS["CRM_DDA_ENTITY_ERROR"] = "No existe una entidad con este ID";
$MESS["CRM_DDA_TYPE_ID_ERROR"] = "Tipo de entidad inválido";
