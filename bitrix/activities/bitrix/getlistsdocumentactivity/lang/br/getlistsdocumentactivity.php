<?
$MESS["BPGLDA_DOC_TYPE"] = "Tipo de entidade";
$MESS["BPGLDA_DT_LISTS"] = "Listas";
$MESS["BPGLDA_DT_LISTS_SOCNET"] = "Grupos de trabalho e listas de projetos";
$MESS["BPGLDA_DT_PROCESSES"] = "Processos";
$MESS["BPGLDA_ELEMENT_ID"] = "ID do item";
$MESS["BPGLDA_ERROR_DT"] = "Tipo incorreto de entidade.";
$MESS["BPGLDA_ERROR_ELEMENT_ID"] = "O ID do item está faltando";
$MESS["BPGLDA_ERROR_EMPTY_DOCUMENT"] = "Não é possível obter informações da entidade";
$MESS["BPGLDA_ERROR_FIELDS"] = "Nenhum campo de entidade selecionado";
$MESS["BPGLDA_FIELDS_LABEL"] = "Selecionar campos";
$MESS["BPGLDA_WRONG_TYPE"] = "O tipo de parâmetro '#PARAM#' está indefinido.";
?>