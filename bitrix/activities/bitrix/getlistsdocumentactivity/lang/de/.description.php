<?php
$MESS["BPGLDA_DESCR_DESCR"] = "Elementfelder von der Liste abrufen";
$MESS["BPGLDA_DESCR_DESCR_1"] = "Sendet Information zum Listenelement an andere Automatisierungsregeln.";
$MESS["BPGLDA_DESCR_NAME"] = "Element der Liste lesen";
$MESS["BPGLDA_DESCR_NAME_1"] = "Information zum Listenelement bekommen";
