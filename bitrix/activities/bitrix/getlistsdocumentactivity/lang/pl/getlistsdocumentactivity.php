<?
$MESS["BPGLDA_DOC_TYPE"] = "Typ jednostki";
$MESS["BPGLDA_DT_LISTS"] = "Listy";
$MESS["BPGLDA_DT_LISTS_SOCNET"] = "Listy grup roboczych i projektów";
$MESS["BPGLDA_DT_PROCESSES"] = "Procesy";
$MESS["BPGLDA_ELEMENT_ID"] = "ID elementu";
$MESS["BPGLDA_ERROR_DT"] = "Nieprawidłowy typ jednostki.";
$MESS["BPGLDA_ERROR_ELEMENT_ID"] = "Brakuje ID elementu";
$MESS["BPGLDA_ERROR_EMPTY_DOCUMENT"] = "Nie można uzyskać informacji o jednostce";
$MESS["BPGLDA_ERROR_FIELDS"] = "Nie wybrano pól jednostek";
$MESS["BPGLDA_FIELDS_LABEL"] = "Wybierz pola";
$MESS["BPGLDA_WRONG_TYPE"] = "Typ parametru „#PARAM#” jest niezdefiniowany.";
?>