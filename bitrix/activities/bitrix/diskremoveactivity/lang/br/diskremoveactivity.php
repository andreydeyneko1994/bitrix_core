<?
$MESS["BPDRMV_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDRMV_EMPTY_SOURCE_ID"] = "Objeto de origem não especificado.";
$MESS["BPDRMV_REMOVE_ERROR"] = "Não foi possível excluir o objeto.";
$MESS["BPDRMV_SOURCE_ERROR"] = "O objeto de origem não foi encontrado.";
?>