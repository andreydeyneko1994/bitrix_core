<?php
$MESS["BPSA_EMPTY_PERMS"] = "A permissão para elemento com este status não está especificada.";
$MESS["BPSA_INVALID_CHILD"] = "Um 'Status de Atividade' poderá conter somente as ações 'Atividade de Inicialização de Status', ' Atividade de Finalização de Status' ou 'Atividade iniciada por evento'.";
$MESS["BPSA_INVALID_CHILD_1"] = "O 'Status de Atividade' pode conter somente 'StatusAtividadeInicializada', 'StatusAtividadeFinalizada' ou 'AtividadeConduzidaPorEvento'.";
$MESS["BPSA_TRACK1"] = "Permissão para elemento neste status #VAL#";
