<?php
$MESS["BPSWFA_PD_ACCESS_DENIED"] = "Seuls les administrateurs du portail peuvent accéder aux propriétés des actions.";
$MESS["BPSWFA_PD_DOCUMENT_ID"] = "ID de l'élément";
$MESS["BPSWFA_PD_DOCUMENT_TYPE"] = "Type d'élément";
$MESS["BPSWFA_PD_ENTITY"] = "Entité";
$MESS["BPSWFA_PD_TEMPLATE"] = "Modèle";
$MESS["BPSWFA_PD_USE_SUBSCRIPTION"] = "En attente de la fin du flux de travail";
