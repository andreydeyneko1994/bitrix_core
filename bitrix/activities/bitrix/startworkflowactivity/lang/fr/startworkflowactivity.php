<?php
$MESS["BPSWFA_ACCESS_DENIED"] = "Seuls les administrateurs du portail peuvent accéder aux propriétés des actions.";
$MESS["BPSWFA_DOCTYPE_ERROR"] = "Le type de document ne correspond pas à celui spécifié dans le modèle de flux de travail";
$MESS["BPSWFA_DOCTYPE_NOT_FOUND_ERROR"] = "Impossible de trouver l'entité cible (ID incorrect ?)";
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "L'ID de l'élément n'est pas indiqué";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "Pas de modèle sélectionné.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID de l'entité";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Type d'élément";
$MESS["BPSWFA_SELFSTART_ERROR"] = "Le modèle ne peut être exécuté de manière récursive";
$MESS["BPSWFA_START_ERROR"] = "Erreur de début : #MESSAGE#";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Paramètres du flux de travail";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "Le paramètre requis est vide : #NAME#";
