<?php
$MESS["BPSWFA_PD_ACCESS_DENIED"] = "Sólo los administradores del portal pueden acceder a las propiedades de la acción.";
$MESS["BPSWFA_PD_DOCUMENT_ID"] = "ID del elemento";
$MESS["BPSWFA_PD_DOCUMENT_TYPE"] = "Tipo de elemento";
$MESS["BPSWFA_PD_ENTITY"] = "Entidad";
$MESS["BPSWFA_PD_TEMPLATE"] = "Plantilla";
$MESS["BPSWFA_PD_USE_SUBSCRIPTION"] = "Esperando por finalización del workflow";
