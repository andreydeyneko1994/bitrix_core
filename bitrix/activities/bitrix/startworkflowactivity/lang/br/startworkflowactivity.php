<?php
$MESS["BPSWFA_ACCESS_DENIED"] = "Apenas os administradores do portal podem acessar propriedades de ação.";
$MESS["BPSWFA_DOCTYPE_ERROR"] = "O tipo de documento não corresponde ao especificado no modelo de fluxo de trabalho";
$MESS["BPSWFA_DOCTYPE_NOT_FOUND_ERROR"] = "Não foi possível encontrar a entidade de destino (ID incorreto?)";
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "O ID do elemento não está especificado";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "Nenhum modelo selecionado.";
$MESS["BPSWFA_RPD_DOCUMENT_ID"] = "ID da entidade";
$MESS["BPSWFA_RPD_DOCUMENT_TYPE"] = "Tipo de item";
$MESS["BPSWFA_SELFSTART_ERROR"] = "O modelo não pode ser executado recursivamente";
$MESS["BPSWFA_START_ERROR"] = "Erro de inicialização: #MESSAGE#";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Parâmetros do fluxo de trabalho";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "O parâmetro obrigatório está vazio: #NAME#";
