<?php
$MESS["CRM_CMPR_COPIED_ROWS"] = "Produits copiés";
$MESS["CRM_CMPR_COPY_PRODUCTS_ERROR"] = "Impossible de copier les éléments du produit";
$MESS["CRM_CMPR_DST_ENTITY_ID"] = "ID cible";
$MESS["CRM_CMPR_DST_ENTITY_TYPE"] = "Cible";
$MESS["CRM_CMPR_MOVED_ROWS"] = "Produits déplacés";
$MESS["CRM_CMPR_MOVE_PRODUCTS_ERROR"] = "Impossible de déplacer les éléments du produit";
$MESS["CRM_CMPR_NO_DST_ENTITY"] = "Impossible de trouver l'entité cible pour copier ou déplacer des éléments de produit";
$MESS["CRM_CMPR_NO_SOURCE_PRODUCTS"] = "Impossible de récupérer les produits de l'entité source";
$MESS["CRM_CMPR_OPERATION"] = "Action";
$MESS["CRM_CMPR_OPERATION_CP"] = "Copie";
$MESS["CRM_CMPR_OPERATION_MV"] = "Déplacer";
$MESS["CRM_CMPR_SAME_ENTITY_ERROR"] = "Les entités actuelles et cibles ne peuvent pas être les mêmes";
