<?php
$MESS["CRM_CMPR_COPIED_ROWS"] = "Productos copiados";
$MESS["CRM_CMPR_COPY_PRODUCTS_ERROR"] = "No se pueden copiar los artículos del producto";
$MESS["CRM_CMPR_DST_ENTITY_ID"] = "ID del destino";
$MESS["CRM_CMPR_DST_ENTITY_TYPE"] = "Destino";
$MESS["CRM_CMPR_MOVED_ROWS"] = "Productos movidos";
$MESS["CRM_CMPR_MOVE_PRODUCTS_ERROR"] = "No se pueden mover los artículos del producto";
$MESS["CRM_CMPR_NO_DST_ENTITY"] = "No se puede encontrar la entidad de destino para copiar o mover los artículos del producto";
$MESS["CRM_CMPR_NO_SOURCE_PRODUCTS"] = "No se pueden obtener los elementos del producto de la entidad de origen";
$MESS["CRM_CMPR_OPERATION"] = "Acción";
$MESS["CRM_CMPR_OPERATION_CP"] = "Copiar";
$MESS["CRM_CMPR_OPERATION_MV"] = "Mover";
$MESS["CRM_CMPR_SAME_ENTITY_ERROR"] = "Las entidades actuales y de destino no pueden ser iguales";
