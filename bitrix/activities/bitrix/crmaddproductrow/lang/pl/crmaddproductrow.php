<?php
$MESS["CRM_APR_ADD_ERROR"] = "Nie można dodać produktu";
$MESS["CRM_APR_DOCUMENT_ERROR"] = "Działanie nie jest dostępne dla bieżącej jednostki";
$MESS["CRM_APR_GET_PRODUCT_ERROR"] = "Nie można uzyskać informacji o produkcie";
$MESS["CRM_APR_PRODUCT_ID"] = "ID produktu";
$MESS["CRM_APR_ROW_DISCOUNT_RATE"] = "Upust, %";
$MESS["CRM_APR_ROW_PRICE_ACCOUNT"] = "Cena";
$MESS["CRM_APR_ROW_QUANTITY"] = "Ilość";
