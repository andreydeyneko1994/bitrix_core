<?
$MESS["CRM_CRLC_CONTACT_NAME_DEFAULT"] = "Sans nom";
$MESS["CRM_CRLC_LEAD_HAS_CONTACT"] = "Le prospect appartient déjà à un contact";
$MESS["CRM_CRLC_LEAD_NOT_EXISTS"] = "Récupération des données du prospect impossible";
$MESS["CRM_CRLC_LEAD_WRONG_STATUS"] = "Impossible de créer le contact à partir du statut de réussite du prospect. Veuillez utiliser la règle d'automatisation de conversion automatique.";
$MESS["CRM_CRLC_RESPONSIBLE"] = "Personne responsable";
?>