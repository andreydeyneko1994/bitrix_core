<?
$MESS["CRM_CRLC_CONTACT_NAME_DEFAULT"] = "Bez tytułu";
$MESS["CRM_CRLC_LEAD_HAS_CONTACT"] = "Lead już należy do kontaktu";
$MESS["CRM_CRLC_LEAD_NOT_EXISTS"] = "Nie można uzyskać danych leadu";
$MESS["CRM_CRLC_LEAD_WRONG_STATUS"] = "Nie można utworzyć kontaktu ze statusu sukcesu leadu. Użyj reguły automatyzacji konwersji.";
$MESS["CRM_CRLC_RESPONSIBLE"] = "Osoba odpowiedzialna";
?>