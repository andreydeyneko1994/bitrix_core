<?
$MESS["CRM_CRLC_CONTACT_NAME_DEFAULT"] = "Sem título";
$MESS["CRM_CRLC_LEAD_HAS_CONTACT"] = "O cliente potencial já pertence a um contato";
$MESS["CRM_CRLC_LEAD_NOT_EXISTS"] = "Não é possível obter dados do cliente potencial";
$MESS["CRM_CRLC_LEAD_WRONG_STATUS"] = "Não é possível criar contato a partir do status de sucesso do lead. Por favor, use a regra de automação de conversão.";
$MESS["CRM_CRLC_RESPONSIBLE"] = "Pessoa responsável";
?>