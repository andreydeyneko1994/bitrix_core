<?
$MESS["TASKS_CHANGE_RESPONSIBLE_EMPTY_PROP"] = "Osoba odpowiedzialna nie jest określona.";
$MESS["TASKS_CHANGE_RESPONSIBLE_MODIFIED_BY"] = "Zmień w imieniu";
$MESS["TASKS_CHANGE_RESPONSIBLE_NEW"] = "Nowa osoba odpowiedzialna";
$MESS["TASKS_CHANGE_RESPONSIBLE_NO_PERMISSIONS"] = "Niewystarczające uprawnienia do zmiany osoby odpowiedzialnej.";
?>