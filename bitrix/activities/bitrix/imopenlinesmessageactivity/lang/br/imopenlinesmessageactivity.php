<?php
$MESS["IMOL_MA_ATTACHMENT"] = "Anexos";
$MESS["IMOL_MA_ATTACHMENT_DISK"] = "Drive";
$MESS["IMOL_MA_ATTACHMENT_TYPE"] = "Tipo de anexo";
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "O parâmetro 'Mensagem de Texto' está faltando.";
$MESS["IMOL_MA_IS_SYSTEM"] = "Mensagem oculta (modo silencioso)";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "A mensagem postada não estará visível para contato externo (modo silencioso)";
$MESS["IMOL_MA_MESSAGE"] = "Texto da mensagem";
$MESS["IMOL_MA_NO_CHAT"] = "O bate-papo do cliente não foi encontrado";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "Não foi encontrado nenhum cliente com Canal Aberto conectado";
$MESS["IMOL_MA_TIMELINE_ERROR"] = "A mensagem não foi enviada. #ERROR_TEXT#";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "O elemento atual não é compatível com este tipo de atividade";
