<?
$MESS["BPULDA_DOC_TYPE"] = "Type d'entité";
$MESS["BPULDA_DT_LISTS"] = "Listes";
$MESS["BPULDA_DT_LISTS_SOCNET"] = "Listes des flux de travail et des projets";
$MESS["BPULDA_DT_PROCESSES"] = "Processus";
$MESS["BPULDA_ELEMENT_ID"] = "ID de l'élément";
$MESS["BPULDA_ERROR_DT"] = "Type d'entité incorrect.";
$MESS["BPULDA_ERROR_ELEMENT_ID"] = "L'ID de l'élément est manquant";
$MESS["BPULDA_ERROR_FIELDS"] = "Les champs d'élément ne sont pas spécifiés";
$MESS["BPULDA_FIELD_REQUIED"] = "Le champ '#FIELD#' est requis.";
$MESS["BPULDA_WRONG_TYPE"] = "Le type du paramètre '#PARAM#' n'est pas défini.";
?>