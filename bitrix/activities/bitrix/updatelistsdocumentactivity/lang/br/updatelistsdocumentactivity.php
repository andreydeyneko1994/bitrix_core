<?
$MESS["BPULDA_DOC_TYPE"] = "Tipo de entidade";
$MESS["BPULDA_DT_LISTS"] = "Listas";
$MESS["BPULDA_DT_LISTS_SOCNET"] = "Grupos de trabalho e listas de projetos";
$MESS["BPULDA_DT_PROCESSES"] = "Processos";
$MESS["BPULDA_ELEMENT_ID"] = "ID do item";
$MESS["BPULDA_ERROR_DT"] = "Tipo incorreto de entidade.";
$MESS["BPULDA_ERROR_ELEMENT_ID"] = "O ID do item está faltando";
$MESS["BPULDA_ERROR_FIELDS"] = "Os campos do item não estão especificados";
$MESS["BPULDA_FIELD_REQUIED"] = "O campo '#FIELD#’ é obrigatório.";
$MESS["BPULDA_WRONG_TYPE"] = "O tipo de parâmetro '#PARAM#' está indefinido.";
?>