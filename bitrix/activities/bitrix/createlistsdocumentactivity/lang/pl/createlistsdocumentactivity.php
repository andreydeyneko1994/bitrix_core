<?
$MESS["BPCLDA_DOC_TYPE_1"] = "Typ jednostki";
$MESS["BPCLDA_DT_LISTS"] = "Listy";
$MESS["BPCLDA_DT_LISTS_SOCNET_1"] = "Listy grup roboczych i projektów";
$MESS["BPCLDA_DT_PROCESSES"] = "Przepływy pracy";
$MESS["BPCLDA_ERROR_DT_1"] = "Nieprawidłowy typ jednostki.";
$MESS["BPCLDA_FIELD_REQUIED"] = "Pole '#FIELD#' jest wymagane.";
$MESS["BPCLDA_WRONG_TYPE"] = "Typ parametru '#PARAM#' jest niezdefiniowany.";
?>