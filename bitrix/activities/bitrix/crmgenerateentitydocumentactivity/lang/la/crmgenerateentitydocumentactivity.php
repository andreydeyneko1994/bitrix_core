<?php
$MESS["CRM_GEDA_EMPTY_TEMPLATE_ID"] = "No se especifica la plantilla del documento";
$MESS["CRM_GEDA_MODULE_DOCGEN_ERROR"] = "El módulo Generador de documentos no está instalado.";
$MESS["CRM_GEDA_NAME_CREATE_ACTIVITY"] = "Agregar actividad";
$MESS["CRM_GEDA_NAME_DELETE"] = "Eliminar";
$MESS["CRM_GEDA_NAME_MY_COMPANY_BANK_DETAIL_ID"] = "Datos bancarios de mi empresa";
$MESS["CRM_GEDA_NAME_MY_COMPANY_ID"] = "Mi compañía";
$MESS["CRM_GEDA_NAME_MY_COMPANY_REQUISITE_ID"] = "Detalles de mi empresa";
$MESS["CRM_GEDA_NAME_PUBLIC_URL"] = "Crear un enlace público";
$MESS["CRM_GEDA_NAME_TEMPLATE_ID"] = "Plantilla";
$MESS["CRM_GEDA_NAME_USE_SUBSCRIPTION"] = "Espera a que se complete la conversión del PDF";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG"] = "El procesos de negocio está a la espera de que el documento finalice la conversión.";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG_COMPLETE"] = "Documento convertido";
$MESS["CRM_GEDA_NAME_WITH_STAMPS"] = "Con firma y sello.";
$MESS["CRM_GEDA_PROVIDER_NOT_FOUND"] = "No se puede encontrar el controlador para este tipo de entidad";
$MESS["CRM_GEDA_TRANSFORMATION_ERROR"] = "Error al convertir el documento: ";
