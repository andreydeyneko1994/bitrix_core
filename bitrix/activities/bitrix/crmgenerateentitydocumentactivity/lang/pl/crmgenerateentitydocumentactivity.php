<?php
$MESS["CRM_GEDA_EMPTY_TEMPLATE_ID"] = "Szablon strony nie został określony";
$MESS["CRM_GEDA_MODULE_DOCGEN_ERROR"] = "Moduł Generator dokumentów nie jest zainstalowany.";
$MESS["CRM_GEDA_NAME_CREATE_ACTIVITY"] = "Dodaj aktywność";
$MESS["CRM_GEDA_NAME_DELETE"] = "Usuń";
$MESS["CRM_GEDA_NAME_MY_COMPANY_BANK_DETAIL_ID"] = "Dane bankowe firmy";
$MESS["CRM_GEDA_NAME_MY_COMPANY_ID"] = "Moja firma";
$MESS["CRM_GEDA_NAME_MY_COMPANY_REQUISITE_ID"] = "Dane firmy";
$MESS["CRM_GEDA_NAME_PUBLIC_URL"] = "Utwórz link publiczny";
$MESS["CRM_GEDA_NAME_TEMPLATE_ID"] = "Szablon";
$MESS["CRM_GEDA_NAME_USE_SUBSCRIPTION"] = "Poczekaj na zakończenie konwersji do PDF";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG"] = "Workflow czeka na zakończenie konwersji dokumentu";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG_COMPLETE"] = "Dokument został przekonwertowany";
$MESS["CRM_GEDA_NAME_WITH_STAMPS"] = "Z podpisem i pieczątką";
$MESS["CRM_GEDA_PROVIDER_NOT_FOUND"] = "Nie można znaleźć procedury obsługi dla tego typu obiektu";
$MESS["CRM_GEDA_TRANSFORMATION_ERROR"] = "Błąd podczas konwertowania dokumentu: ";
