<?php
$MESS["CRM_GEDA_EMPTY_TEMPLATE_ID"] = "O modelo do documento não está especificado";
$MESS["CRM_GEDA_MODULE_DOCGEN_ERROR"] = "O módulo Documentgenerator não está instalado.";
$MESS["CRM_GEDA_NAME_CREATE_ACTIVITY"] = "Adicionar atividade";
$MESS["CRM_GEDA_NAME_DELETE"] = "Excluir";
$MESS["CRM_GEDA_NAME_MY_COMPANY_BANK_DETAIL_ID"] = "Dados bancários da minha empresa";
$MESS["CRM_GEDA_NAME_MY_COMPANY_ID"] = "Minha empresa";
$MESS["CRM_GEDA_NAME_MY_COMPANY_REQUISITE_ID"] = "Informações da minha empresa";
$MESS["CRM_GEDA_NAME_PUBLIC_URL"] = "Criar link público";
$MESS["CRM_GEDA_NAME_TEMPLATE_ID"] = "Modelo";
$MESS["CRM_GEDA_NAME_USE_SUBSCRIPTION"] = "Aguarde a conversão do PDF ser concluída";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG"] = "O fluxo de trabalho está aguardando o documento concluir a conversão";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG_COMPLETE"] = "Documento convertido";
$MESS["CRM_GEDA_NAME_WITH_STAMPS"] = "Com assinatura e carimbo";
$MESS["CRM_GEDA_PROVIDER_NOT_FOUND"] = "Não é possível encontrar manipulador para este tipo de entidade";
$MESS["CRM_GEDA_TRANSFORMATION_ERROR"] = "Erro ao converter documento: ";
