<?
$MESS["BPSNMA_EMPTY_FROM"] = "Brakuje parametru 'Nadawca'.";
$MESS["BPSNMA_EMPTY_MESSAGE"] = "Brakuje parametru 'Wiadomość Tekstowa'.";
$MESS["BPSNMA_EMPTY_TO"] = "Brakuje parametru 'Odbiorca'.";
$MESS["BPSNMA_FORMAT_ROBOT"] = "Reguła automatyzacji powiadomień";
$MESS["BPSNMA_FROM"] = "Nadawca";
$MESS["BPSNMA_MESSAGE"] = "Tekst wiadomości";
$MESS["BPSNMA_TO"] = "Odbiorca";
?>