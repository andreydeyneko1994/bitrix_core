<?
$MESS["CRM_RB_ERROR_DURATION"] = "A data de término é anterior à data de início";
$MESS["CRM_RB_ERROR_DURATION_EMPTY"] = "A duração não está especificada";
$MESS["CRM_RB_ERROR_FIELD"] = "O campo não está especificado";
$MESS["CRM_RB_ERROR_RESOURCE_USERS"] = "Os empregados não estão especificados";
$MESS["CRM_RB_ERROR_START_DATE"] = "A data de início não está especificada";
$MESS["CRM_RB_RESOURCE_DURATION"] = "Duração";
$MESS["CRM_RB_RESOURCE_FIELD"] = "Campo";
$MESS["CRM_RB_RESOURCE_NAME"] = "Serviço";
$MESS["CRM_RB_RESOURCE_START_DATE"] = "Data de início";
$MESS["CRM_RB_RESOURCE_USERS"] = "Funcionários";
?>