<?
$MESS["CRM_RB_ERROR_DURATION"] = "La date de fin est antérieure à la date de départ";
$MESS["CRM_RB_ERROR_DURATION_EMPTY"] = "La durée n'est pas spécifiée";
$MESS["CRM_RB_ERROR_FIELD"] = "Le champ n'est pas spécifié";
$MESS["CRM_RB_ERROR_RESOURCE_USERS"] = "Les employés ne sont pas spécifiés";
$MESS["CRM_RB_ERROR_START_DATE"] = "La date de début n'est pas spécifiée";
$MESS["CRM_RB_RESOURCE_DURATION"] = "Durée";
$MESS["CRM_RB_RESOURCE_FIELD"] = "Champ";
$MESS["CRM_RB_RESOURCE_NAME"] = "Service";
$MESS["CRM_RB_RESOURCE_START_DATE"] = "Date de début";
$MESS["CRM_RB_RESOURCE_USERS"] = "Employés";
?>