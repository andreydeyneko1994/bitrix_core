<?
$MESS["CRM_RB_ERROR_DURATION"] = "Data zakończenia jest wcześniejsza niż data rozpoczęcia";
$MESS["CRM_RB_ERROR_DURATION_EMPTY"] = "Czas trwania nie jest określony";
$MESS["CRM_RB_ERROR_FIELD"] = "Pole nie jest określone";
$MESS["CRM_RB_ERROR_RESOURCE_USERS"] = "Pracownicy nie są określeni";
$MESS["CRM_RB_ERROR_START_DATE"] = "Data rozpoczęcia nie jest określona";
$MESS["CRM_RB_RESOURCE_DURATION"] = "Czas trwania";
$MESS["CRM_RB_RESOURCE_FIELD"] = "Pole";
$MESS["CRM_RB_RESOURCE_NAME"] = "Usługa";
$MESS["CRM_RB_RESOURCE_START_DATE"] = "Data rozpoczęcia";
$MESS["CRM_RB_RESOURCE_USERS"] = "Pracownicy";
?>