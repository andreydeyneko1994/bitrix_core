<?
$MESS["CRM_RB_ERROR_DURATION"] = "La fecha de finalización es anterior a la fecha de inicio";
$MESS["CRM_RB_ERROR_DURATION_EMPTY"] = "La duración no está especificada";
$MESS["CRM_RB_ERROR_FIELD"] = "El campo no está especificado";
$MESS["CRM_RB_ERROR_RESOURCE_USERS"] = "Los empleados no están especificados";
$MESS["CRM_RB_ERROR_START_DATE"] = "La fecha de inicio no está especificada";
$MESS["CRM_RB_RESOURCE_DURATION"] = "Duración";
$MESS["CRM_RB_RESOURCE_FIELD"] = "Campo";
$MESS["CRM_RB_RESOURCE_NAME"] = "Servicio";
$MESS["CRM_RB_RESOURCE_START_DATE"] = "Fecha de inicio";
$MESS["CRM_RB_RESOURCE_USERS"] = "Empleados";
?>