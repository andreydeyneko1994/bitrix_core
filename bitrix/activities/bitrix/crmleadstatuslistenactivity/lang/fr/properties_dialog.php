<?
$MESS["BPCLSLA_PD_LEAD"] = "ID du prospect";
$MESS["BPCLSLA_PD_STATUS"] = "Statut";
$MESS["BPCLSLA_PD_STATUS_DESCR"] = "Sélectionnez un ou plusieurs statuts à attendre. Le flux de travail attendra que le prospect soit déplacé dans le statut sélectionné ou le statut final.";
?>