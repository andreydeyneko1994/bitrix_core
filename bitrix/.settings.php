<?php
return array (
  'pull_s1' => 'BEGIN GENERATED PUSH SETTINGS. DON\'T DELETE COMMENT!!!!',
  'pull' => 
  array (
    'value' => 
    array (
      'path_to_listener' => 'http://#DOMAIN#/bitrix/sub/',
      'path_to_listener_secure' => 'https://#DOMAIN#/bitrix/sub/',
      'path_to_modern_listener' => 'http://#DOMAIN#/bitrix/sub/',
      'path_to_modern_listener_secure' => 'https://#DOMAIN#/bitrix/sub/',
      'path_to_mobile_listener' => 'http://#DOMAIN#:8893/bitrix/sub/',
      'path_to_mobile_listener_secure' => 'https://#DOMAIN#:8894/bitrix/sub/',
      'path_to_websocket' => 'ws://#DOMAIN#/bitrix/subws/',
      'path_to_websocket_secure' => 'wss://#DOMAIN#/bitrix/subws/',
      'path_to_publish' => 'http://bitrix_server:8895/bitrix/pub/',
      'path_to_publish_web' => 'http://#DOMAIN#/bitrix/rest/',
      'path_to_publish_web_secure' => 'https://#DOMAIN#/bitrix/rest/',
      'nginx_version' => '4',
      'nginx_command_per_hit' => '100',
      'nginx' => 'Y',
      'nginx_headers' => 'N',
      'push' => 'Y',
      'websocket' => 'Y',
      'signature_key' => 'EnuC6UFy3XIAvHDVXT4fketNANK1yfP65MaehStsNqyd9wVxSeFqt2sAQK9Tm3ECuUu2FmjlENoUtmZRiB5KzhOz3LLonOKKdzG6CvXCMDFg0ZPFRQzJfUlw88PE0l4n',
      'signature_algo' => 'sha1',
      'guest' => 'N',
    ),
  ),
  'pull_e1' => 'END GENERATED PUSH SETTINGS. DON\'T DELETE COMMENT!!!!',
  'utf_mode' => 
  array (
    'value' => true,
    'readonly' => true,
  ),
  'cache_flags' => 
  array (
    'value' => 
    array (
      'config_options' => 3600,
      'site_domain' => 3600,
    ),
    'readonly' => false,
  ),
  'cookies' => 
  array (
    'value' => 
    array (
      'secure' => false,
      'http_only' => true,
    ),
    'readonly' => false,
  ),
  'exception_handling' => 
  array (
    'value' => 
    array (
      'debug' => true,
      'handled_errors_types' => 20853,
      'exception_errors_types' => 20853,
      'ignore_silence' => false,
      'assertion_throws_exception' => true,
      'assertion_error_type' => 256,
      'log' => 
      array (
        'class_name' => 'Artvending\\CustomExceptionHandlerLog',
        'required_file' => 'local/php_interface/classes/CustomExceptionHandlerLog.php',
      ),
    ),
    'readonly' => false,
  ),
  'crypto' => 
  array (
    'value' => 
    array (
      'crypto_key' => 'd9c885cce8ad4c26f504a2c1425e3f4c',
    ),
    'readonly' => true,
  ),
  'connections' => 
  array (
    'value' => 
    array (
      'default' => 
      array (
        'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
        'host' => '127.0.0.1',
        'database' => 'artvending',
        'login' => 'root',
        'password' => '',
        'options' => 2,
      ),
    ),
    'readonly' => true,
  ),
);
